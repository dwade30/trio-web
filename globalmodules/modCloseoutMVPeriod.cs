﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication.Extensions;
using Wisej.Web;

namespace Global
{
	public class modCloseoutMVPeriod
	{
		private static clsDRWrapper rsPC1
		{
			get
			{
				if (Statics.rsPC1_AutoInitialized == null)
				{
					Statics.rsPC1_AutoInitialized = new clsDRWrapper();
				}
				return Statics.rsPC1_AutoInitialized;
			}
			set
			{
				Statics.rsPC1_AutoInitialized = value;
			}
		}

		public static void CloseMVPeriod(ref string strOperator)
		{
			int lngID = 0;
			// vbPorter upgrade warning: GetOut As Variant --> As DialogResult
			DialogResult GetOut;
			clsDRWrapper rsUser = new clsDRWrapper();
			clsDRWrapper rsTellers = new clsDRWrapper();
			clsDRWrapper rsMV = new clsDRWrapper();
			rsMV.OpenRecordset("SELECT * FROM WMV", "TWMV0000.vb1");
			// TODO: Field [UseTellerCloseout] not found!! (maybe it is an alias?)
			if (FCConvert.ToString(rsMV.Get_Fields("UseTellerCloseout")) == "Y")
			{
				Statics.blnUseTellerCloseout = true;
			}
			else
			{
				Statics.blnUseTellerCloseout = false;
			}
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + strOperator + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) > 2)
				{
					MessageBox.Show("You do not have a high enough Operator Level to Close out a Period.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (strOperator != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to Close out a Period.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			GetOut = MessageBox.Show("Please be sure that all users are out of the Motor Vehicle System before Closing Out Period.", "Everyone Out of Motor Vehicle", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
			if (GetOut == DialogResult.Cancel)
			{
				MessageBox.Show("Process was stopped by you.", "Process Not Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			Information.Err().Clear();
			bool executePeriodEndError = false;
			try
			{
				// On Error GoTo PeriodEndError
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Closing Period";
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// Set Closeout ID by adding a new record in table
				rsPC1.OpenRecordset("SELECT * FROM PeriodCloseout", "TWMV0000.vb1");
				var tempDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
				Statics.DateTime = DateTime.Parse(tempDate);
				
				rsPC1.AddNew();
				rsPC1.Set_Fields("OpID", strOperator);
				rsPC1.Set_Fields("IssueDate", Statics.DateTime);
				rsPC1.Update();
				lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
				if (Statics.blnUseTellerCloseout)
				{
					CloseoutTellers();
					SetPeriodCloseoutID(ref lngID);
				}
				// Set Period CloseoutID for ActivityMaster
				Statics.strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutID for Special Registrations
				Statics.strSQL = "SELECT * FROM SpecialRegistration WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutID for Transit Plates
				Statics.strSQL = "SELECT * FROM TransitPlates WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutID for Booster Permits
				Statics.strSQL = "SELECT * FROM Booster WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutID for Gift Certificates
				Statics.strSQL = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
					goto PeriodEndError;
				// Set Period CloseoutID for InventoryAdjustments
				Statics.strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND AdjustmentCode <> 'P'";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutID for ExciseTax
				Statics.strSQL = "SELECT * FROM ExciseTax WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutID for ExceptionReport
				Statics.strSQL = "SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutID for TitleApplications
				Statics.strSQL = "SELECT * FROM TitleApplications WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
					goto PeriodEndError;
				// Set Period CloseoutID for TemporaryPlateRegister
				Statics.strSQL = "SELECT * FROM TemporaryPlateRegister WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref Statics.strSQL, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutID for CloseoutInventory
				Statics.strSQL = "SELECT * FROM CloseoutInventory";
				if (PerformInventoryCloseout(ref Statics.strSQL, ref lngID) == false)
					goto PeriodEndError;
				rsPC1.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngID), "TWMV0000.vb1");
				rsPC1.Edit();
				rsPC1.Set_Fields("IssueDate", Statics.DateTime);
				rsPC1.Update();
				//Application.DoEvents();
				rsPC1.Reset();
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = 0;
				return;
			}
			catch (Exception ex)
			{
				executePeriodEndError = true;
				goto PeriodEndError;
			}
			PeriodEndError:
			if (executePeriodEndError)
			{
				string strErrMessage;
				// 05/16/2008 CJG
				// err gets cleared when you run the following code before showing the error message, so save the message
				strErrMessage = "Period Closeout was not successful.  Error number " + FCConvert.ToString(Information.Err().Number) + " was encountered.  (" + Information.Err().Description + ")";
				rsPC1.FindFirstRecord("ID", lngID);
				rsPC1.Delete();
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = 0;
				MessageBox.Show(strErrMessage, "Error processing closeout.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Information.Err().Clear();
			}
		}

		private static void CloseoutTellers()
		{
			clsDRWrapper rsActivity = new clsDRWrapper();
			clsDRWrapper rsTransit = new clsDRWrapper();
			clsDRWrapper rsSpecial = new clsDRWrapper();
			clsDRWrapper rsBooster = new clsDRWrapper();
			clsDRWrapper rsTellers = new clsDRWrapper();
			clsDRWrapper rsAdjustment = new clsDRWrapper();
			clsDRWrapper rsException = new clsDRWrapper();
			clsDRWrapper rsGiftCert = new clsDRWrapper();
			string[] Tellers = null;
			int intCounter;
			int intCounter2;
			bool blnClosed;
			clsDRWrapper rsPC1 = new clsDRWrapper();
			int lngID = 0;
			string strSQL = "";
			string strOPID = "";
			int intUnclosedTellers = 0;
			clsDRWrapper rsRecieved = new clsDRWrapper();
			clsDRWrapper rsInventory = new clsDRWrapper();
			rsTellers.OpenRecordset("SELECT TellerID FROM TellerCloseoutTable WHERE TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
			rsActivity.OpenRecordset("SELECT DISTINCT OpID FROM ActivityMaster WHERE TellerCloseoutID = 0", "TWMV0000.vb1");
			rsTransit.OpenRecordset("SELECT DISTINCT OpID FROM TransitPlates WHERE TellerCloseoutID = 0", "TWMV0000.vb1");
			rsSpecial.OpenRecordset("SELECT DISTINCT OpID FROM SpecialRegistration WHERE TellerCloseoutID = 0", "TWMV0000.vb1");
			rsBooster.OpenRecordset("SELECT DISTINCT OpID FROM Booster WHERE TellerCloseoutID = 0", "TWMV0000.vb1");
			rsAdjustment.OpenRecordset("SELECT DISTINCT OpID FROM InventoryAdjustments WHERE TellerCloseoutID = 0", "TWMV0000.vb1");
			rsException.OpenRecordset("SELECT DISTINCT OpID FROM ExceptionReport WHERE TellerCloseoutID = 0", "TWMV0000.vb1");
			rsGiftCert.OpenRecordset("SELECT DISTINCT OpID FROM GiftCertificateRegister WHERE TellerCloseoutID = 0", "TWMV0000.vb1");
			blnClosed = false;
			if (rsTellers.EndOfFile() != true && rsTellers.BeginningOfFile() != true)
			{
				rsTellers.MoveLast();
				rsTellers.MoveFirst();
				intUnclosedTellers = 0;
				if (rsActivity.EndOfFile() != true && rsActivity.BeginningOfFile() != true)
				{
					rsActivity.MoveLast();
					rsActivity.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						do
						{
							if (Strings.UCase(FCConvert.ToString(rsActivity.Get_Fields_String("OpID"))) == Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
							{
								blnClosed = true;
								break;
							}
							rsTellers.MoveNext();
						}
						while (rsTellers.EndOfFile() != true);
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsActivity.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsActivity.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsActivity.MoveNext();
					}
					while (rsActivity.EndOfFile() != true);
				}
				if (rsTransit.EndOfFile() != true && rsTransit.BeginningOfFile() != true)
				{
					rsTransit.MoveLast();
					rsTransit.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID"))) == Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsTransit.MoveNext();
					}
					while (rsTransit.EndOfFile() != true);
				}
				if (rsSpecial.EndOfFile() != true && rsSpecial.BeginningOfFile() != true)
				{
					rsSpecial.MoveLast();
					rsSpecial.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID"))) == Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsSpecial.MoveNext();
					}
					while (rsSpecial.EndOfFile() != true);
				}
				if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
				{
					rsBooster.MoveLast();
					rsBooster.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID"))) == Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsBooster.MoveNext();
					}
					while (rsBooster.EndOfFile() != true);
				}
				if (rsAdjustment.EndOfFile() != true && rsAdjustment.BeginningOfFile() != true)
				{
					rsAdjustment.MoveLast();
					rsAdjustment.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID"))) == Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsAdjustment.MoveNext();
					}
					while (rsAdjustment.EndOfFile() != true);
				}
				if (rsException.EndOfFile() != true && rsException.BeginningOfFile() != true)
				{
					rsException.MoveLast();
					rsException.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID"))) == Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsException.MoveNext();
					}
					while (rsException.EndOfFile() != true);
				}
			}
			else
			{
				intUnclosedTellers = 0;
				if (rsActivity.EndOfFile() != true && rsActivity.BeginningOfFile() != true)
				{
					rsActivity.MoveLast();
					rsActivity.MoveFirst();
					do
					{
						strOPID = Strings.UCase(FCConvert.ToString(rsActivity.Get_Fields_String("OpID")));
						// check to see if there is a closeout entry for this teller or if this is the first one
						rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
						if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
						{
							// do nothing
						}
						else
						{
							// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
						}
						// make a closeout entry in the TellerCloseoutTable table in the database
						rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
						rsPC1.AddNew();
						rsPC1.Set_Fields("TellerID", strOPID);
						rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
						rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
						rsPC1.Set_Fields("PeriodCloseoutID", 0);
						rsPC1.Update();
						lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
						// Set Teller CloseoutID for ActivityMaster
						strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for Special Registrations
						strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for Transit Plates
						strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for Booster Permits
						strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for Gift Certificates
						strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for InventoryAdjustments
						strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for ExciseTax
						strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for ExceptionReport
						strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for TitleApplications
						strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for TemporaryPlateRegister
						strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
						SetTellerID(ref strSQL, ref lngID);
						// Set Teller CloseoutID for Inventory
						strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
						SetTellerID(ref strSQL, ref lngID);
						intUnclosedTellers += 1;
						Array.Resize(ref Tellers, intUnclosedTellers + 1);
						Tellers[intUnclosedTellers - 1] = strOPID;
						rsActivity.MoveNext();
					}
					while (rsActivity.EndOfFile() != true);
				}
				if (rsTransit.EndOfFile() != true && rsTransit.BeginningOfFile() != true)
				{
					rsTransit.MoveLast();
					rsTransit.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsTransit.MoveNext();
					}
					while (rsTransit.EndOfFile() != true);
				}
				if (rsSpecial.EndOfFile() != true && rsSpecial.BeginningOfFile() != true)
				{
					rsSpecial.MoveLast();
					rsSpecial.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsSpecial.MoveNext();
					}
					while (rsSpecial.EndOfFile() != true);
				}
				if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
				{
					rsBooster.MoveLast();
					rsBooster.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsBooster.MoveNext();
					}
					while (rsBooster.EndOfFile() != true);
				}
				if (rsAdjustment.EndOfFile() != true && rsAdjustment.BeginningOfFile() != true)
				{
					rsAdjustment.MoveLast();
					rsAdjustment.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsAdjustment.MoveNext();
					}
					while (rsAdjustment.EndOfFile() != true);
				}
				if (rsException.EndOfFile() != true && rsException.BeginningOfFile() != true)
				{
					rsException.MoveLast();
					rsException.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID"))) == Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Strings.UCase(strOPID) + "'", "TWMV0000.vb1");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOPID);
								rsPC1.Set_Fields("TellerCloseoutDate", DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable", "TWMV0000.vb1");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOPID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSQL = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							SetTellerID(ref strSQL, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOPID + "'";
							SetTellerID(ref strSQL, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOPID;
						}
						else
						{
							strOPID = Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOPID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOPID + "'", "TWMV0000.vb1");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsException.MoveNext();
					}
					while (rsException.EndOfFile() != true);
				}
			}
			rsTellers.Reset();
			rsActivity.Reset();
			rsTransit.Reset();
			rsSpecial.Reset();
			rsBooster.Reset();
			rsAdjustment.Reset();
			rsException.Reset();
			rsPC1.Reset();
			rsRecieved.Reset();
			rsInventory.Reset();
			rsGiftCert.Reset();
		}

		private static void SetPeriodCloseoutID(ref int PeriodID)
		{
			clsDRWrapper rsTellers = new clsDRWrapper();
			rsTellers.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE PeriodCloseoutID = 0", "TWMV0000.vb1");
			if (rsTellers.EndOfFile() != true && rsTellers.BeginningOfFile() != true)
			{
				rsTellers.MoveLast();
				rsTellers.MoveFirst();
				do
				{
					rsTellers.Edit();
					rsTellers.Set_Fields("PeriodCloseoutID", PeriodID);
					rsTellers.Update();
					rsTellers.MoveNext();
				}
				while (rsTellers.EndOfFile() != true);
			}
		}

		private static bool SetCloseoutID(ref string str1, ref int lng1)
		{
			bool SetCloseoutID = false;
			SetCloseoutID = false;
			clsDRWrapper rs1 = new clsDRWrapper();
			rs1.OpenRecordset(str1, "TWMV0000.vb1");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				while (!rs1.EndOfFile())
				{
					rs1.Edit();
					if (str1 == "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1")
					{
						rs1.Set_Fields("OldMVR3", lng1);
					}
					else
					{
						rs1.Set_Fields("PeriodCloseoutID", lng1);
					}
					rs1.Update();
					rs1.MoveNext();
				}
				SetCloseoutID = true;
			}
			else
			{
				SetCloseoutID = true;
			}
			return SetCloseoutID;
		}

		public static bool PerformInventoryCloseout(ref string str1, ref int lng1)
		{
			bool PerformInventoryCloseout = false;
			PerformInventoryCloseout = false;
			int lngHoldNumber = 0;
			int lngHoldHigh = 0;
			int lngNewNumber = 0;
			clsDRWrapper rsIOH = new clsDRWrapper();
			clsDRWrapper rs1 = new clsDRWrapper();
			clsDRWrapper rs2 = new clsDRWrapper();
			string strHoldType = "";
			int lngCount = 0;
			string strCode = "";
			string strOriginalPrefix = "";
			string strStartPlate = "";
			rs1.OpenRecordset(str1, "TWMV0000.vb1");
			rsIOH.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout", "TWMV0000.vb1");
			// MVR-3's
			if (Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'M' And (((Status = 'A' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'M' And (Status = 'A' OR Status = 'P') ORDER BY Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
				if (rsIOH.NoMatch == true)
				{
					rsIOH.AddNew();
				}
				else
				{
					rsIOH.Edit();
				}
				rsIOH.Set_Fields("Type", rs2.Get_Fields_String("Code"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
				{
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + rs2.RecordCount());
				}
				else
				{
					rsIOH.Set_Fields("Number", rs2.RecordCount());
				}
				rsIOH.Set_Fields("PeriodCloseoutID", lng1);
				rsIOH.Update();
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number")) - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields("Number")))
					{
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", rs2.Get_Fields_String("Code"));
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						// TODO: Check the table for the column [High] and replace with corresponding Get_Field method
						lngHoldHigh = FCConvert.ToInt32(rs1.Get_Fields("High"));
						rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", "MXS00");
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Boosters
			if (Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'B' And ((Status = 'A' AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'B' And Status = 'A' ORDER BY Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
				if (rsIOH.NoMatch == true)
				{
					rsIOH.AddNew();
				}
				else
				{
					rsIOH.Edit();
				}
				rsIOH.Set_Fields("Type", rs2.Get_Fields_String("Code"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
				{
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + rs2.RecordCount());
				}
				else
				{
					rsIOH.Set_Fields("Number", rs2.RecordCount());
				}
				rsIOH.Set_Fields("PeriodCloseoutID", lng1);
				rsIOH.Update();
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number")) - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields("Number")))
					{
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", rs2.Get_Fields_String("Code"));
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						// TODO: Check the table for the column [High] and replace with corresponding Get_Field method
						lngHoldHigh = FCConvert.ToInt32(rs1.Get_Fields("High"));
						rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", "BXSXX");
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Special Reg Permits
			if (Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,2) = 'RX' And ((Status = 'A' AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,2) = 'RX' And Status = 'A' ORDER BY Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
				if (rsIOH.NoMatch == true)
				{
					rsIOH.AddNew();
				}
				else
				{
					rsIOH.Edit();
				}
				rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
				{
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + rs2.RecordCount());
				}
				else
				{
					rsIOH.Set_Fields("Number", rs2.RecordCount());
				}
				rsIOH.Set_Fields("PeriodCloseoutID", lng1);
				rsIOH.Update();
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number")) - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields("Number")))
					{
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						// TODO: Check the table for the column [High] and replace with corresponding Get_Field method
						lngHoldHigh = FCConvert.ToInt32(rs1.Get_Fields("High"));
						rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", "RXSXX");
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Plate's
			string strPrefix = "";
			string strSuffix = "";
			if (Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' And (((Status = 'A' OR Status = 'H' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,4,2), Prefix + convert(nvarchar, Number) + Suffix", "TWMV0000.vb1");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' And (Status = 'A' OR Status = 'H' OR Status = 'P') ORDER BY substring(Code,4,2), Prefix + convert(nvarchar, Number) + Suffix", "TWMV0000.vb1");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = rs2.Get_Fields_String("Code");
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number")) - 1;
				// TODO: Field [prefix] not found!! (maybe it is an alias?)
				// TODO: Field [prefix] not found!! (maybe it is an alias?)
				if (Strings.Trim(FCConvert.ToString(rs2.Get_Fields("prefix"))) != "")
					strPrefix = FCConvert.ToString(rs2.Get_Fields("prefix"));
				else
					strPrefix = "";
				if (Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
					strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
				else
					strSuffix = "";
				strOriginalPrefix = strPrefix;
				// TODO: Field [FormattedInventory] not found!! (maybe it is an alias?)
				strStartPlate = FCConvert.ToString(rs2.Get_Fields("FormattedInventory"));
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					// TODO: Field [prefix] not found!! (maybe it is an alias?)
					if (strPrefix.Length > FCConvert.ToString(rs2.Get_Fields("prefix")).Length && Strings.Right(strPrefix, 1) == "0")
					{
						// TODO: Field [prefix] not found!! (maybe it is an alias?)
						strPrefix = Strings.Left(strPrefix, (strPrefix.Length - (strPrefix.Length - FCConvert.ToString(rs2.Get_Fields("prefix")).Length)));
					}
					// TODO: Field [prefix] not found!! (maybe it is an alias?)
					// TODO: Field [prefix] not found!! (maybe it is an alias?)
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if ((Strings.Trim(FCConvert.ToString(rs2.Get_Fields("prefix"))) != "" && strPrefix != FCConvert.ToString(rs2.Get_Fields("prefix"))) || (Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "" && strSuffix != FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) || (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields("Number"))))
					{
						rs2.MovePrevious();
						rsIOH.AddNew();
						rsIOH.Set_Fields("Type", strCode);
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
						{
							// TODO: Field [Number] not found!! (maybe it is an alias?)
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngNewNumber - lngHoldNumber);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						// TODO: Field [prefix] not found!! (maybe it is an alias?)
						// TODO: Field [prefix] not found!! (maybe it is an alias?)
						if (Strings.Trim(FCConvert.ToString(rs2.Get_Fields("prefix"))) != "")
							strPrefix = FCConvert.ToString(rs2.Get_Fields("prefix"));
						else
							strPrefix = "";
						if (Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
							strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
						else
							strSuffix = "";
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", strStartPlate);
						// strOriginalPrefix & lngHoldNumber & strSuffix
						// TODO: Field [FormattedInventory] not found!! (maybe it is an alias?)
						rs1.Set_Fields("High", rs2.Get_Fields("FormattedInventory"));
						// strPrefix & lngNewNumber - 1 & strSuffix
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						rs2.MoveNext();
						// TODO: Field [prefix] not found!! (maybe it is an alias?)
						// TODO: Field [prefix] not found!! (maybe it is an alias?)
						if (Strings.Trim(FCConvert.ToString(rs2.Get_Fields("prefix"))) != "")
							strPrefix = FCConvert.ToString(rs2.Get_Fields("prefix"));
						else
							strPrefix = "";
						if (Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
							strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
						else
							strSuffix = "";
						strOriginalPrefix = strPrefix;
						// TODO: Field [FormattedInventory] not found!! (maybe it is an alias?)
						strStartPlate = FCConvert.ToString(rs2.Get_Fields("FormattedInventory"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						strCode = rs2.Get_Fields_String("Code");
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
					{
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", strStartPlate);
					// strOriginalPrefix & lngHoldNumber & strSuffix
					// TODO: Field [FormattedInventory] not found!! (maybe it is an alias?)
					rs1.Set_Fields("High", rs2.Get_Fields("FormattedInventory"));
					// If .Fields("Number") = lngNewNumber Then
					// rs1.Fields("High") = strPrefix & lngNewNumber & strSuffix
					// Else
					// rs1.Fields("High") = strPrefix & lngNewNumber - 1 & strSuffix
					// End If
					rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Single Sickers
			if (Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'S' And (((Status = 'A' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'S' And (Status = 'A' OR Status = 'P') ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = rs2.Get_Fields_String("Code");
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number")) - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields("Number")) || strCode != rs2.Get_Fields_String("Code"))
					{
						rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
						if (rsIOH.NoMatch == true)
						{
							rsIOH.AddNew();
						}
						else
						{
							rsIOH.Edit();
						}
						rsIOH.Set_Fields("Type", strCode);
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
						{
							// TODO: Field [Number] not found!! (maybe it is an alias?)
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngNewNumber - lngHoldNumber);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						strCode = rs2.Get_Fields_String("Code");
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
					{
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Combination Sickers
			if (Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'C' And (((Status = 'A' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'C' And (Status = 'A' OR Status = 'P') ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = rs2.Get_Fields_String("Code");
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number")) - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields("Number")) || strCode != rs2.Get_Fields_String("Code"))
					{
						rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
						if (rsIOH.NoMatch == true)
						{
							rsIOH.AddNew();
						}
						else
						{
							rsIOH.Edit();
						}
						rsIOH.Set_Fields("Type", strCode);
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
						{
							// TODO: Field [Number] not found!! (maybe it is an alias?)
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngNewNumber - lngHoldNumber);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						strCode = rs2.Get_Fields_String("Code");
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
					{
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Double Sickers
			if (Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'D' And (((Status = 'A' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'D' And (Status = 'A' OR Status = 'P') ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = rs2.Get_Fields_String("Code");
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number")) - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields("Number")) || strCode != rs2.Get_Fields_String("Code"))
					{
						rsIOH.AddNew();
						rsIOH.Set_Fields("Type", strCode);
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						strCode = rs2.Get_Fields_String("Code");
						rs1.Update();
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields_String("Code"));
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
					{
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Truck Decals
			if (Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'D' And ((Status = 'A' AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,3,2), Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'D' And Status = 'A' ORDER BY substring(Code,3,2), Prefix, Number, Suffix", "TWMV0000.vb1");
			}
			string holdInventoryType = "";
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
				holdInventoryType = rs2.Get_Fields_String("Code");
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number")) - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields("Number")) || holdInventoryType != rs2.Get_Fields_String("Code"))
					{
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", holdInventoryType);
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						lngCount = lngNewNumber - lngHoldNumber;
						rs1.Update();
						rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + holdInventoryType, ",");
						if (rsIOH.NoMatch == true)
						{
							rsIOH.AddNew();
						}
						else
						{
							rsIOH.Edit();
						}
						rsIOH.Set_Fields("Type", holdInventoryType);
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
						{
							// TODO: Field [Number] not found!! (maybe it is an alias?)
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngCount);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngCount);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields("Number"));
						holdInventoryType = rs2.Get_Fields_String("Code");
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					// TODO: Field [Number] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsIOH.Get_Fields("Number")) > 0)
					{
						// TODO: Field [Number] not found!! (maybe it is an alias?)
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", Statics.DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// 
			PerformInventoryCloseout = true;
			//Application.DoEvents();
			rs1.Reset();
			rs2.Reset();
			return PerformInventoryCloseout;
		}

		public static bool SetTellerID(ref string str1, ref int lng1)
		{
			bool SetTellerID = false;
			SetTellerID = false;
			clsDRWrapper rs1 = new clsDRWrapper();
			rs1.OpenRecordset(str1, "TWMV0000.vb1");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				while (!rs1.EndOfFile())
				{
					rs1.Edit();
					rs1.Set_Fields("TellerCloseoutID", lng1);
					rs1.Update();
					rs1.MoveNext();
				}
				SetTellerID = true;
			}
			else
			{
				SetTellerID = true;
			}
			return SetTellerID;
		}

		public class StaticVariables
		{
			// ********************************************************
			// LAST UPDATED   :               07/10/2003              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// DATE           :               06/13/2003              *
			// WRITTEN BY     :               Jim Bertolino           *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			public DateTime DateTime;
			//private static clsDRWrapper rsPC1 = new clsDRWrapper();
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			public clsDRWrapper rsPC1_AutoInitialized;
			public bool blnUseTellerCloseout;
			public string strSQL = string.Empty;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
