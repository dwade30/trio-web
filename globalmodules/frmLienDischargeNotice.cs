﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmLienDischargeNotice.
	/// </summary>
	public partial class frmLienDischargeNotice : BaseForm
	{
		public frmLienDischargeNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienDischargeNotice InstancePtr
		{
			get
			{
				return (frmLienDischargeNotice)Sys.GetInstance(typeof(frmLienDischargeNotice));
			}
		}

		protected frmLienDischargeNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/08/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/03/2006              *
		// ********************************************************
		clsDRWrapper rsLien = new clsDRWrapper();
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		int lngLienRecordNumber;
		bool boolPrintAllSaved;
		bool boolPrintArchive;
		bool boolAbateDefault;
		string strAbate;
		string strPayment;
		bool boolDefaultAppearedDate;
		int lngTownKey;
		// grid cols
		int lngColData;
		int lngColTitle;
		int lngColHidden;
		// grid rows
		int lngRowTName;
		int lngRowMuni;
		int lngRowCounty;
		int lngRowOwner1;
		int lngRowOwner2;
		int lngRowPayDate;
		int lngRowFileDate;
		int lngRowBook;
		int lngRowPage;
		int lngRowHisHer;
		int lngRowSignerName;
		int lngRowTitle;
		int lngRowSignerDesignation;
		int lngRowCommissionExpiration;
		int lngRowMapLot;
		int lngRowSignedDate;
		int lngRowAbatementDefault;
		int lngRowAppearedDate;
		int lngTempAccountNumber;
		DateTime dtPaymentDate;
		// VBto upgrade warning: dtPassPayDate As DateTime	OnWrite(DateTime, short, string)
		public void Init(DateTime dtPassPayDate, int lngPassLRN = 0, string strYear = "", bool boolPassPrintAllSaved = false, bool boolAbateDefault = false, bool boolDoNotCheckTotal = false, bool boolArchive = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTest = new clsDRWrapper();
				bool boolRecords = false;
				double dblYearTot = 0;
				double dblXInt = 0;
				string strFields = "";
				dtPaymentDate = dtPassPayDate;
				boolPrintAllSaved = boolPassPrintAllSaved;
				boolPrintArchive = boolArchive;
				if (boolPrintAllSaved)
				{
					// if all saved are to be printed, then check to see if there are any
					if (boolPrintArchive)
					{
						rsTest.OpenRecordset("SELECT top 1 * FROM DischargeNeededArchive", modExtraModules.strCLDatabase);
					}
					else
					{
						rsTest.OpenRecordset("SELECT top 1 * FROM DischargeNeeded WHERE Printed = 0", modExtraModules.strCLDatabase);
					}
					boolRecords = !rsTest.EndOfFile();
					lngColData = 2;
					lngColHidden = 1;
					lngColTitle = 0;
					lngRowOwner1 = -1;
					lngRowOwner2 = -1;
					lngRowPayDate = -1;
					lngRowBook = -1;
					lngRowPage = -1;
					lngRowMapLot = -1;
					lngRowFileDate = -1;
					lngRowAbatementDefault = -1;
					lngRowTName = 0;
					lngRowTitle = 1;
					lngRowMuni = 2;
					lngRowCounty = 3;
					lngRowSignedDate = 4;
					lngRowHisHer = 5;
					lngRowSignerName = 6;
					lngRowSignerDesignation = 7;
					lngRowCommissionExpiration = 8;
					lngRowAppearedDate = 9;
				}
				else
				{
					lngLienRecordNumber = lngPassLRN;
					lngColData = 2;
					lngColHidden = 1;
					lngColTitle = 0;
					lngRowTName = 0;
					lngRowTitle = 1;
					lngRowMuni = 2;
					lngRowCounty = 3;
					lngRowOwner1 = 4;
					lngRowOwner2 = 5;
					lngRowPayDate = 6;
					lngRowFileDate = 7;
					lngRowSignedDate = 8;
					lngRowBook = 9;
					lngRowPage = 10;
					lngRowMapLot = 11;
					lngRowAbatementDefault = 12;
					lngRowHisHer = 13;
					lngRowSignerName = 14;
					lngRowSignerDesignation = 15;
					lngRowCommissionExpiration = 16;
					lngRowAppearedDate = 17;
					if (strYear != "")
					{
						lblYear.Text = "Year : " + modExtraModules.FormatYear(strYear);
						lblYear.Visible = true;
					}
					else
					{
						lblYear.Visible = false;
					}
					strFields = "Account,Name1,Name2,BillingYear,Book,Page,MapLot,TranCode,LienRec.ID,LienRec.RateKey as LienRateKey,LienRec.InterestAppliedThroughDate,PrintedLDN";
					rsLien.OpenRecordset("SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber WHERE LienRec.ID = " + FCConvert.ToString(lngLienRecordNumber), modExtraModules.strCLDatabase);
					if (!rsLien.EndOfFile())
					{
						// keep going
						// TODO: Check the table for the column [TranCode] and replace with corresponding Get_Field method
						lngTownKey = FCConvert.ToInt32(rsLien.Get_Fields("TranCode"));
					}
					else
					{
						FCMessageBox.Show("The lien record number " + FCConvert.ToString(lngLienRecordNumber) + " could not be found.", MsgBoxStyle.Exclamation, "Missing Lien Record");
						Close();
						return;
					}
					rsTest.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsLien.Get_Fields_Int32("ID")), modExtraModules.strCLDatabase);
					if (!rsTest.EndOfFile())
					{
						dblYearTot = modGlobal.Round(modCLCalculations.CalculateAccountCLLien(rsTest, DateTime.Today, ref dblXInt), 2);
					}
					else
					{
						FCMessageBox.Show("The lien record number " + FCConvert.ToString(lngLienRecordNumber) + " could not be found.", MsgBoxStyle.Exclamation, "Missing Lien Record");
						Close();
						return;
					}
					if (dblYearTot > 0 && !boolDoNotCheckTotal)
					{
						// kk04012015 trocr-442 Change default response to No
						if (FCMessageBox.Show("This lien has not been completely paid off, are you sure that you would like to print the Lien Discharge Notice?", MsgBoxStyle.Question | MsgBoxStyle.DefaultButton2 | MsgBoxStyle.YesNo, "Unpad Lien") == DialogResult.No)
						{
							Close();
							return;
						}
						else
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalFunctions.AddCYAEntry_242("CL", "Printing a LDN with a balance due.", "Account # : " + FCConvert.ToString(rsLien.Get_Fields("Account")), "Lien # : " + FCConvert.ToString(rsTest.Get_Fields_Int32("ID")), "Year : " + FCConvert.ToString(rsLien.Get_Fields_Int32("BillingYear")));
						}
					}
				}
				FormatGrid(true);
				if (lngLienRecordNumber > 0 || (boolPrintAllSaved && boolRecords))
				{
					this.Show(FormShowEnum.Modal);
				}
				else
				{
					if (!(boolPrintAllSaved && boolRecords))
					{
						FCMessageBox.Show("No saved records to be printed.", MsgBoxStyle.Exclamation, "No Records");
					}
					else
					{
						FCMessageBox.Show("Cannot find a lien record for that year.", MsgBoxStyle.Exclamation, "Cannot Find Lien");
					}
					Close();
					// MAL@20071011: Make sure program keeps focus
					App.MainForm.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing");
			}
		}

		private void frmLienDischargeNotice_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmLienDischargeNotice_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoaded = true;
			FormatGrid(true);
			//FC:FINAL:DDU:#i161 put LoadSettings method in form load
			LoadSettings();
			strAbate = "Paid With Abatement";
			strPayment = "Paid With Payment";
		}

		private void frmLienDischargeNotice_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmLienDischargeNotice_Resize(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				FormatGrid();
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SaveSettings()
		{
			// this will save all of the settings for the lien discharge notice
			clsDRWrapper rsTemp = new clsDRWrapper();
			// VBto upgrade warning: dtComExp As DateTime	OnWrite(DateTime, short)
			DateTime dtComExp;
			bool boolTemp/*unused?*/;
			// VBto upgrade warning: dtAppear As DateTime	OnWrite(DateTime, short)
			DateTime dtAppear;
			clsDRWrapper rsArchive = new clsDRWrapper();
			if (ValidateAnswers())
			{
				rsSettings.OpenRecordset("SELECT * FROM Control_DischargeNotice", modExtraModules.strCLDatabase);
				if (rsSettings.EndOfFile())
				{
					rsSettings.AddNew();
				}
				else
				{
					rsSettings.Edit();
				}
				// county
				rsSettings.Set_Fields("County", vsData.TextMatrix(lngRowCounty, lngColData));
				// treasurer name
				rsSettings.Set_Fields("Treasurer", vsData.TextMatrix(lngRowTName, lngColData));
				// treasurer title
				if (Strings.Trim(vsData.TextMatrix(lngRowTitle, lngColData)) != "")
				{
					rsSettings.Set_Fields("TreasurerTitle", vsData.TextMatrix(lngRowTitle, lngColData));
				}
				else
				{
					rsSettings.Set_Fields("TreasurerTitle", " ");
				}
				// his/her
				if (Strings.UCase(Strings.Left(vsData.TextMatrix(lngRowHisHer, lngColData), 3)) == "HIS")
				{
					rsSettings.Set_Fields("Male", true);
				}
				else
				{
					rsSettings.Set_Fields("Male", false);
				}
				// write the signers name and designation for default info
				// Signer's name
				rsSettings.Set_Fields("SignerName", Strings.Trim(vsData.TextMatrix(lngRowSignerName, lngColData)));
				// Signer's Designation
				rsSettings.Set_Fields("SignerDesignation", Strings.Trim(vsData.TextMatrix(lngRowSignerDesignation, lngColData)));
				// Commission Expiration Date    'XXXXXX Can't allow 12:00:00 to go through
				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
				{
					if (Information.IsDate(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)))
					{
						rsSettings.Set_Fields("CommissionExpiration", vsData.TextMatrix(lngRowCommissionExpiration, lngColData));
					}
					else
					{
						rsSettings.Set_Fields("CommissionExpiration", 0);
					}
				}
				else
				{
					rsSettings.Set_Fields("CommissionExpiration", 0);
				}
				rsSettings.Update();
				if (boolPrintAllSaved)
				{
					if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
					{
						dtComExp = DateAndTime.DateValue(vsData.TextMatrix(lngRowCommissionExpiration, lngColData));
					}
					else
					{
						dtComExp = DateTime.FromOADate(0);
					}
					if (Strings.InStr(1, vsData.TextMatrix(lngRowAppearedDate, lngColData), "_") == 0 && Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) != "")
					{
						dtAppear = DateAndTime.DateValue(vsData.TextMatrix(lngRowAppearedDate, lngColData));
					}
					else
					{
						dtAppear = DateTime.FromOADate(0);
					}
					// remove all saved LDN that are not paid anymore
					if (!boolPrintArchive)
					{
						CheckSavedLDN();
						arAllLienDischargeNotice.InstancePtr.Init(vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowCounty, lngColData), vsData.TextMatrix(lngRowHisHer, lngColData), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowSignerDesignation, lngColData), dtComExp, vsData.TextMatrix(lngRowTitle, lngColData), DateAndTime.DateValue(vsData.TextMatrix(lngRowSignedDate, lngColData)), dtAppear, boolDefaultAppearedDate);
					}
					else
					{
						arAllLienDischargeNotice.InstancePtr.Init(vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowCounty, lngColData), vsData.TextMatrix(lngRowHisHer, lngColData), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowSignerDesignation, lngColData), dtComExp, vsData.TextMatrix(lngRowTitle, lngColData), DateAndTime.DateValue(vsData.TextMatrix(lngRowSignedDate, lngColData)), dtAppear, boolDefaultAppearedDate, true);
					}
					frmReportViewer.InstancePtr.Init(arAllLienDischargeNotice.InstancePtr,"",1,false,false,"Pages",false,"","TRIO Software", false,true,"",false,false,true);
					rsTemp.OpenRecordset("SELECT * FROM DischargeNeeded WHERE Printed = 0", modExtraModules.strCLDatabase);
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						rsArchive.Execute("DELETE FROM DischargeNeededArchive", modExtraModules.strCLDatabase);
						rsArchive.OpenRecordset("SELECT * FROM DischargeNeededArchive");
						while (!rsTemp.EndOfFile())
						{
							rsTemp.Edit();
							rsTemp.Set_Fields("UpdatedDate", DateTime.Today);
							rsTemp.Set_Fields("Printed", true);
							rsTemp.Update();
							rsArchive.AddNew();
							rsArchive.Set_Fields("DatePaid", rsTemp.Get_Fields_DateTime("DatePaid"));
							rsArchive.Set_Fields("UpdatedDate", rsTemp.Get_Fields_DateTime("UpdatedDate"));
							rsArchive.Set_Fields("Teller", rsTemp.Get_Fields_String("Teller"));
							rsArchive.Set_Fields("LienKey", rsTemp.Get_Fields_Int32("LienKey"));
							rsArchive.Set_Fields("BillKey", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("BillKey"))));
							rsArchive.Set_Fields("Printed", rsTemp.Get_Fields_Boolean("Printed"));
							rsArchive.Set_Fields("Batch", rsTemp.Get_Fields_Boolean("Batch"));
							rsArchive.Set_Fields("Canceled", rsTemp.Get_Fields_Boolean("Canceled"));
							// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
							rsArchive.Set_Fields("Book", rsTemp.Get_Fields("Book"));
							// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
							rsArchive.Set_Fields("Page", rsTemp.Get_Fields("Page"));
							rsArchive.Update();
							rsTemp.MoveNext();
						}
					}
				}
				else
				{
					// loads the lien discharge report
					vsData.Select(0, 0);
					if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
					{
						dtComExp = DateAndTime.DateValue(vsData.TextMatrix(lngRowCommissionExpiration, lngColData));
					}
					else
					{
						dtComExp = DateTime.FromOADate(0);
					}
					if (Strings.InStr(1, vsData.TextMatrix(lngRowAppearedDate, lngColData), "_") == 0 && Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) != "")
					{
						dtAppear = DateAndTime.DateValue(vsData.TextMatrix(lngRowAppearedDate, lngColData));
					}
					else
					{
						dtAppear = DateTime.FromOADate(0);
					}
					if (Strings.UCase(Strings.Trim(vsData.TextMatrix(lngRowAbatementDefault, lngColData))) == Strings.UCase(Strings.Trim(strAbate)))
					{
						boolAbateDefault = true;
					}
					else
					{
						boolAbateDefault = false;
					}
					// Doevents
					arLienDischargeNotice.InstancePtr.Init(vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowCounty, lngColData), vsData.TextMatrix(lngRowOwner1, lngColData), Strings.Trim(vsData.TextMatrix(lngRowOwner2, lngColData)), DateAndTime.DateValue(vsData.TextMatrix(lngRowPayDate, lngColData)), DateAndTime.DateValue(vsData.TextMatrix(lngRowFileDate, lngColData)), vsData.TextMatrix(lngRowBook, lngColData), vsData.TextMatrix(lngRowPage, lngColData), vsData.TextMatrix(lngRowHisHer, lngColData), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowSignerDesignation, lngColData), dtComExp, vsData.TextMatrix(lngRowMapLot, lngColData), vsData.TextMatrix(lngRowTitle, lngColData), lngTempAccountNumber, DateAndTime.DateValue(vsData.TextMatrix(lngRowSignedDate, lngColData)), boolAbateDefault, dtAppear, Conversion.Val(Strings.Right(lblYear.Text, 6)).ToString());
					// frmReportViewer.Init arLienDischargeNotice
					rsTemp.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(rsLien.Get_Fields_Int32("ID")), modExtraModules.strCLDatabase);
					if (!rsTemp.EndOfFile())
					{
						rsTemp.Edit();
						rsTemp.Set_Fields("UpdatedDate", DateTime.Today);
						rsTemp.Set_Fields("Printed", true);
						rsTemp.Update();
					}
					else
					{
						// MAL@20080115:Add to DischargeNeeded table if it doesn't exist
						// Tracker Reference: 11920
						rsTemp.AddNew();
						rsTemp.Set_Fields("DatePaid", rsLien.Get_Fields_DateTime("InterestAppliedThroughDate"));
						rsTemp.Set_Fields("UpdatedDate", DateTime.Today);
						rsTemp.Set_Fields("Teller", "Admin");
						rsTemp.Set_Fields("LienKey", rsLien.Get_Fields_Int32("ID"));
						rsTemp.Set_Fields("Printed", true);
						rsTemp.Update();
					}
					// kk 01092102 trocls-16  Can't update a recordset with a JOIN
					// rsLien.Edit
					// rsLien.Get_Fields("PrintedLDN") = True
					// rsLien.Update
					rsTemp.Execute("UPDATE LienRec SET PrintedLDN = 1 WHERE ID = " + FCConvert.ToString(rsLien.Get_Fields_Int32("ID")), modExtraModules.strCLDatabase);
				}
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
				clsDRWrapper rsCTRL = new clsDRWrapper();
				rsSettings.OpenRecordset("SELECT * FROM Control_DischargeNotice", modExtraModules.strCLDatabase);
				if (!boolPrintAllSaved)
				{
					// TODO: Field [LienRateKey] not found!! (maybe it is an alias?)
					rsCTRL.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsLien.Get_Fields("LienRateKey")), modExtraModules.strCLDatabase);
				}
				if (lngRowAbatementDefault > 0)
				{
					if (boolAbateDefault)
					{
						vsData.TextMatrix(lngRowAbatementDefault, lngColData, strAbate);
					}
					else
					{
						vsData.TextMatrix(lngRowAbatementDefault, lngColData, strPayment);
					}
				}
				if (rsSettings.EndOfFile() != true && rsSettings.BeginningOfFile() != true)
				{
					// load all of the rows
					// Row - 0  'titles
					vsData.TextMatrix(lngRowTitle, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("TreasurerTitle")));
					// Row - 1  'Treasurer Name
					vsData.TextMatrix(lngRowTName, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("Treasurer")));
					// Row - 3  'County
					// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
					vsData.TextMatrix(lngRowCounty, lngColData, FCConvert.ToString(rsSettings.Get_Fields("County")));
					// Row - 10 'His/Her
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("Male")))
					{
						vsData.TextMatrix(lngRowHisHer, lngColData, "His");
					}
					else
					{
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
					}
					// Signer's name - 2
					vsData.TextMatrix(lngRowSignerName, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("SignerName")));
					// Signer's Designation
					vsData.TextMatrix(lngRowSignerDesignation, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("SignerDesignation")));
					// Commission Expiration Date
					vsData.TextMatrix(lngRowCommissionExpiration, lngColData, rsSettings.Get_Fields_DateTime("CommissionExpiration").ToString("MM/dd/yyyy"));
				}
				else
				{
					rsSettings.AddNew();
					// add defaults....
					rsSettings.Update(true);
					// load all of the rows
					rsSettings.OpenRecordset("SELECT * FROM Control_LienProcess", modExtraModules.strCLDatabase);
					if (!rsSettings.EndOfFile())
					{
						// Row - 0  'titles
						vsData.TextMatrix(lngRowTitle, lngColData, "Treasurer");
						// Row - 1  'Treasurer Name
						vsData.TextMatrix(lngColTitle, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("CollectorName")));
						// Row - 3  'County
						// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
						vsData.TextMatrix(lngRowCounty, lngColData, FCConvert.ToString(rsSettings.Get_Fields("County")));
						// Row - 10 'His/Her
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
						// Signer's name
						vsData.TextMatrix(lngRowSignerName, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("Signer")));
						// Signer's Designation
						vsData.TextMatrix(lngRowSignerDesignation, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("Designation")));
						// Commission Expiration Date
						vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					}
					else
					{
						// Row - 0  'titles
						vsData.TextMatrix(lngRowTitle, lngColData, "Treasurer");
						// Row - 1  'Treasurer Name
						vsData.TextMatrix(lngRowTName, lngColData, "");
						// Row - 3  'County
						vsData.TextMatrix(lngRowCounty, lngColData, "");
						// Row - 10 'His/Her
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
						// Signer's name
						vsData.TextMatrix(lngRowSignerName, lngColData, "");
						// Signer's Designation
						vsData.TextMatrix(lngRowSignerDesignation, lngColData, "");
						// Commission Expiration Date
						vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					}
				}
				// Row - 2  'Muni Name
				vsData.TextMatrix(lngRowMuni, lngColData, GetMuniName(ref lngTownKey));
				if (!boolPrintAllSaved)
				{
					// Row - 4  'Name1
					vsData.TextMatrix(lngRowOwner1, lngColData, FCConvert.ToString(rsLien.Get_Fields_String("Name1")));
					// Row - 5  'Name2
					vsData.TextMatrix(lngRowOwner2, lngColData, FCConvert.ToString(rsLien.Get_Fields_String("Name2")));
					// Row - 6  'Payment Date and Signed Date
					// If dtPaymentDate = 0 Then
					dtPaymentDate = GetDefaultPaymentDate_2(FCConvert.ToInt32(rsLien.Get_Fields_Int32("ID")));
					vsData.TextMatrix(lngRowPayDate, lngColData, Strings.Format(dtPaymentDate, "MM/dd/yyyy"));
					vsData.TextMatrix(lngRowSignedDate, lngColData, Strings.Format(DateTime.Now, "MM/dd/yyyy"));
					vsData.TextMatrix(lngRowAppearedDate, lngColData, "");
					// Else
					// .TextMatrix(lngRowPayDate, lngColData) = Format(dtPaymentDate, "MM/dd/yyyy")
					// .TextMatrix(lngRowSignedDate, lngColData) = Format(dtPaymentDate, "MM/dd/yyyy")
					// .TextMatrix(lngRowAppearedDate, lngColData) = Format(dtPaymentDate, "MM/dd/yyyy")
					// End If
					// Row - 7  'Filing Date
					// use the date created field from the lien record
					//FC:FINAL:DDU #i117 conversion to corect datetime
					DateTime dt;
					if (DateTime.TryParse(FCConvert.ToString(rsCTRL.Get_Fields_DateTime("BillingDate")), out dt))
					{
						vsData.TextMatrix(lngRowFileDate, lngColData, Strings.Format(dt, "MM/dd/yyyy"));
					}
					else
					{
						vsData.TextMatrix(lngRowFileDate, lngColData, "");
					}
					// Row - 8  'Book
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					vsData.TextMatrix(lngRowBook, lngColData, FCConvert.ToString(rsLien.Get_Fields("Book")));
					// Row - 9  'Page
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					vsData.TextMatrix(lngRowPage, lngColData, FCConvert.ToString(rsLien.Get_Fields("Page")));
					// Row - Map Lot
					if (FCConvert.ToString(rsLien.Get_Fields_String("MapLot")) != "")
					{
						vsData.TextMatrix(lngRowMapLot, lngColData, FCConvert.ToString(rsLien.Get_Fields_String("MapLot")));
					}
					else
					{
						// if the bill record does not have the map lot, then get it from the RE Master file
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsCTRL.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(rsLien.Get_Fields("Account")), modExtraModules.strREDatabase);
						if (!rsCTRL.EndOfFile())
						{
							vsData.TextMatrix(lngRowMapLot, lngColData, FCConvert.ToString(rsCTRL.Get_Fields_String("RSMapLot")));
						}
						else
						{
							vsData.TextMatrix(lngRowMapLot, lngColData, "");
						}
					}
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					lngTempAccountNumber = FCConvert.ToInt32(rsLien.Get_Fields("Account"));
				}
				else
				{
					vsData.TextMatrix(lngRowSignedDate, lngColData, Strings.Format(DateTime.Now, "MM/dd/yyyy"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Settings");
			}
		}

		private void mnuFilePrintBlankForm_Click(object sender, System.EventArgs e)
		{
			arLienDischargeNotice.InstancePtr.Init("", modGlobalConstants.Statics.MuniName, "", "", "", DateTime.FromOADate(0), DateTime.FromOADate(0), "", "", "", "", "", DateTime.FromOADate(0), null, null, 0, default(DateTime), false, default(DateTime), null, true);
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			// this will keep the data and print the lien discharge notice
		}

		private void FormatGrid(bool boolReset = false)
		{
			// MAL@20071011: Added highlight backcolor to all required rows
			// Program Bug Reference: 6343
			int lngWid = 0;
			lngWid = vsData.WidthOriginal;
			if (boolPrintAllSaved)
			{
				if (boolReset)
				{
					vsData.Rows = 0;
					vsData.Rows = 10;
				}
				vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.38));
				// title
				vsData.ColWidth(lngColHidden, 0);
				// hidden row
				vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.58));
				// data
				vsData.TextMatrix(lngRowTName, lngColTitle, "Treasurer Name");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTName, 2, lngRowTName, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowMuni, lngColTitle, "Municipality");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMuni, 2, lngRowMuni, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowCounty, 2, lngRowCounty, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Treasurer Signing Date");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowSignedDate, 2, lngRowSignedDate, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowHisHer, lngColTitle, "His / Her  (Treasurer)");
				vsData.TextMatrix(lngRowSignerName, lngColTitle, "Signer's Name");
				vsData.TextMatrix(lngRowTitle, lngColTitle, "Treasurer Title");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTitle, 2, lngRowTitle, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowSignerDesignation, lngColTitle, "Signer's Designation");
				vsData.TextMatrix(lngRowCommissionExpiration, lngColTitle, "Commission Expiration");
				vsData.TextMatrix(lngRowAppearedDate, lngColTitle, "Date Appeared");
			}
			else
			{
				if (boolReset)
				{
					vsData.Rows = 0;
					vsData.Rows = 18;
				}
				vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.38));
				// title
				vsData.ColWidth(lngColHidden, 0);
				// hidden row
				vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.58));
				// data
				vsData.TextMatrix(lngRowTName, lngColTitle, "Treasurer Name");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTName, 2, lngRowTName, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowMuni, lngColTitle, "Municipality");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMuni, 2, lngRowMuni, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowCounty, 2, lngRowCounty, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowOwner1, lngColTitle, "Owner Name");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowOwner1, 2, lngRowOwner1, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowOwner2, lngColTitle, "Second Owner");
				vsData.TextMatrix(lngRowPayDate, lngColTitle, "Payment Date");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowPayDate, 2, lngRowPayDate, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowFileDate, lngColTitle, "Filing Date");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowFileDate, 2, lngRowFileDate, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Treasurer Signing Date");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowSignedDate, 2, lngRowSignedDate, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowBook, lngColTitle, "Book");
				vsData.TextMatrix(lngRowPage, lngColTitle, "Page");
				vsData.TextMatrix(lngRowMapLot, lngColTitle, "Map Lot");
				vsData.TextMatrix(lngRowAbatementDefault, lngColTitle, "How was the lien paid?");
				vsData.TextMatrix(lngRowHisHer, lngColTitle, "His / Her  (Treasurer)");
				vsData.TextMatrix(lngRowSignerName, lngColTitle, "Signer's Name");
				vsData.TextMatrix(lngRowTitle, lngColTitle, "Treasurer Title");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTitle, 2, 1, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowSignerDesignation, lngColTitle, "Signer's Designation");
				vsData.TextMatrix(lngRowCommissionExpiration, lngColTitle, "Commission Expiration");
				vsData.TextMatrix(lngRowAppearedDate, lngColTitle, "Date Appeared");
			}
			// set the grid height
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//if ((vsData.Rows * vsData.RowHeight(0)) + 80 > (fraVariables.Height * 0.8))
			//{
			//	vsData.Height = FCConvert.ToInt32((fraVariables.Height * 0.8));
			//	vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//	vsData.Height = (vsData.Rows * vsData.RowHeight(0)) + 80;
			//	vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void vsData_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsData.EditMask = string.Empty;
			vsData.ComboList = "";
			if (vsData.Row == lngRowPayDate || vsData.Row == lngRowFileDate || vsData.Row == lngRowCommissionExpiration || vsData.Row == lngRowSignedDate || vsData.Row == lngRowAppearedDate)
			{
				vsData.EditMask = "##/##/####";
			}
			else if (vsData.Row == lngRowAbatementDefault)
			{
				vsData.ComboList = "0;" + strPayment + "|1;" + strAbate;
			}
		}

		private void vsData_KeyPressEdit(int Row, int Col, ref Keys KeyAscii)
		{
			if (Row == lngRowPayDate || Row == lngRowFileDate || Row == lngRowBook || Row == lngRowPage || Row == lngRowCommissionExpiration || Row == lngRowAppearedDate)
			{
				// only allow numbers
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (FCConvert.ToInt32(KeyAscii) == 46))
				{
					// do nothing
				}
				else
				{
					KeyAscii = 0;
				}
			}
			else
			{
			}
		}

		private void vsData_RowColChange(object sender, EventArgs e)
		{
			if (vsData.Col == lngColData)
			{
				if (vsData.Row == lngRowTName)
				{
					// Treasurer Name
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowMuni)
				{
					// Muni Name
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowCounty)
				{
					// County
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowOwner1)
				{
					// Name1
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowOwner2)
				{
					// Name2
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowPayDate)
				{
					// Payment Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignedDate)
				{
					// Treas Signing Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowFileDate)
				{
					// Filing Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowBook)
				{
					// Book
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowPage)
				{
					// Page
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowHisHer)
				{
					// His/Her
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignerName || vsData.Row == lngRowTitle)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignerDesignation)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowCommissionExpiration)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else if (vsData.Row == lngRowAbatementDefault)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else if (vsData.Row == lngRowAppearedDate)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
				vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			ValidateAnswers = true;
			vsData.Select(0, 1);
			// Row - 0  'Treasurer Name
			if (Strings.Trim(vsData.TextMatrix(lngRowTName, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter the Treasurer's name.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowTName, lngColData);
				return ValidateAnswers;
			}
			// Row - 0  'Treasurer Title
			if (Strings.Trim(vsData.TextMatrix(lngRowTitle, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter the Treasurer's title.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowTitle, lngColData);
				return ValidateAnswers;
			}
			// Row - 1  'Muni Name
			if (Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter the Municipality name.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowMuni, lngColData);
				return ValidateAnswers;
			}
			// Row - 2  'County
			if (Strings.Trim(vsData.TextMatrix(lngRowCounty, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter the County name.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowCounty, lngColData);
				return ValidateAnswers;
			}
			// Row - 5  'Payment Date
			if (Strings.Trim(vsData.TextMatrix(lngRowSignedDate, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter a treasurer signing date.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowSignedDate, lngColData);
				return ValidateAnswers;
			}
			else
			{
				if (!Information.IsDate(vsData.TextMatrix(lngRowSignedDate, lngColData)))
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a valid treasurer signing date.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowSignedDate, lngColData);
					return ValidateAnswers;
				}
			}
			if (!boolPrintAllSaved)
			{
				// Row - 3  'Name1
				if (Strings.Trim(vsData.TextMatrix(lngRowOwner1, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the Owner's name.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowOwner1, lngColData);
					return ValidateAnswers;
				}
				// Row - 4  'Name2
				// no validations
				// Row - 5  'Payment Date
				if (Strings.Trim(vsData.TextMatrix(lngRowPayDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a payment date.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowPayDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowPayDate, lngColData)))
					{
						ValidateAnswers = false;
						FCMessageBox.Show("Please enter a valid payment date.", MsgBoxStyle.Exclamation, "Validation");
						vsData.Select(lngRowPayDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Row - 6  'Filing Date
				if (Strings.Trim(vsData.TextMatrix(lngRowFileDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a lien filing date.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowFileDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowFileDate, lngColData)))
					{
						ValidateAnswers = false;
						FCMessageBox.Show("Please enter a valid lien filing date.", MsgBoxStyle.Exclamation, "Validation");
						vsData.Select(lngRowFileDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Row - 7  'Book
				// Row - 8  'Page
			}
			else
			{
				// Appeared Date
				if (Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "" || Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "__/__/____" || Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "/  /")
				{
					//Prevented FCMessageBox to appear twice
					DialogResult messageBoxResult = FCMessageBox.Show("Would you like the appeared date to default to the payment date?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, null);
					if (messageBoxResult == DialogResult.Yes)
					{
						boolDefaultAppearedDate = true;
					}
					else if (messageBoxResult == DialogResult.No)
					{
						boolDefaultAppearedDate = false;
					}
					else if (messageBoxResult == DialogResult.Cancel)
					{
						boolDefaultAppearedDate = false;
						ValidateAnswers = false;
					}
				}
				else
				{
					// force this date
				}
			}
			// Row - 9  'His/Her
			// Row - 10 'Signer Name
			// If Trim(.TextMatrix(lngRowSignerName, lngColData)) = "" Then
			// ValidateAnswers = False
			// MsgBox "Please enter the Signer's name.", MsgBoxStyle.Exclamation, "Validation"
			// .Select lngRowSignerName, lngColData
			// Exit Function
			// End If
			// allow these to be blank
			// Row        'Signer's Title
			// If Trim(.TextMatrix(lngRowTitle, lngColData)) = "" Then
			// ValidateAnswers = False
			// MsgBox "Please enter the Signer's title.  (Treasurer)", MsgBoxStyle.Exclamation, "Validation"
			// .Select lngRowTitle, lngColData
			// Exit Function
			// End If
			// 
			// Row - 12 'Signer's Designation
			// If Trim(.TextMatrix(lngRowSignerDesignation, lngColData)) = "" Then
			// ValidateAnswers = False
			// MsgBox "Please enter the Signer's designation.", MsgBoxStyle.Exclamation, "Validation"
			// .Select lngRowSignerDesignation, lngColData
			// Exit Function
			// End If
			// 
			// Row - 13 ' Commistion expiration date
			if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == "")
			{
				// allow it to be blank
				// ValidateAnswers = False
				// MsgBox "Please enter a Commission Expiration date.", MsgBoxStyle.Exclamation, "Validation"
				// .Select lngRowCommissionExpiration, lngColData
				// Exit Function
			}
			else
			{
				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == "/  /")
				{
					vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					// change it back to blank
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)))
					{
						if (Conversion.Val(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == 0)
						{
							vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
							// change it back to blank
						}
						else
						{
							ValidateAnswers = false;
							FCMessageBox.Show("Please enter a valid Commission Expiration date.", MsgBoxStyle.Exclamation, "Validation");
							vsData.Select(lngRowCommissionExpiration, lngColData);
							return ValidateAnswers;
						}
					}
				}
			}
			return ValidateAnswers;
		}

		private void vsData_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsData.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsData.GetFlexRowIndex(e.RowIndex);
				int col = vsData.GetFlexColIndex(e.ColumnIndex);
				if (row == lngRowCommissionExpiration || row == lngRowAppearedDate || row == lngRowFileDate || row == lngRowPayDate || row == lngRowSignedDate)
				{
					if (vsData.EditText == "  /  /    " || vsData.EditText == "__/__/____" || Strings.Trim(vsData.EditText) == "/  /")
					{
						vsData.EditText = "";
						vsData.TextMatrix(row, col, "");
						vsData.EditMask = "";
					}
				}
			}
		}

		private void CheckSavedLDN()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This routine will go through the saved LDN file and remove the ones that are not paid anymore.
				// It will also show a message with a list of the accounts that have been changed
				clsDRWrapper rsLDN = new clsDRWrapper();
				clsDRWrapper rsLn1 = new clsDRWrapper();
				// 03-16-2012  Renamed this rsLien to not clash with module rsLien
				double dblXtraInt = 0;
				string strUnpaidLiens = "";
				int lngFound = 0;
				rsLDN.OpenRecordset("SELECT * FROM DischargeNeeded WHERE NOT (Printed = 1)", modExtraModules.strCLDatabase);
				// frmWait.Init "Please Wait..." & vbCrLf & "Calculating Eligibility", True, rsLDN.RecordCount, True
				while (!rsLDN.EndOfFile())
				{
					// frmWait.IncrementProgress
					App.DoEvents();
					rsLn1.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsLDN.Get_Fields_Int32("LienKey")), modExtraModules.strCLDatabase);
					if (modCLCalculations.CalculateAccountCLLien(rsLn1, DateTime.Today, ref dblXtraInt) > 0)
					{
						// check to see if the lien still has a 0 or lower balance
						lngFound += 1;
						rsLn1.OpenRecordset("SELECT Account, BillingYear FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(rsLDN.Get_Fields_Int32("LienKey")), modExtraModules.strCLDatabase);
						if (!rsLn1.EndOfFile())
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strUnpaidLiens += "Account : " + modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsLn1.Get_Fields("Account")), 5) + " BillingYear : " + modExtraModules.FormatYear(FCConvert.ToString(rsLn1.Get_Fields_Int32("BillingYear"))) + "\r\n";
						}
						rsLDN.Delete();
						// if not then delete it
					}
					rsLDN.MoveNext();
				}
				switch (lngFound)
				{
					case 0:
						{
							// do nothing
							break;
						}
					case 1:
						{
							FCMessageBox.Show("There has been 1 lien found that does not have a zero or negative balance.  It will not be printed." + "\r\n" + strUnpaidLiens, MsgBoxStyle.Exclamation, "Unpaid Liens Found");
							break;
						}
					default:
						{
							FCMessageBox.Show("There have been " + FCConvert.ToString(lngFound) + " liens found that do not have a zero or negative balance.  They will not be printed." + "\r\n" + strUnpaidLiens, MsgBoxStyle.Exclamation, "Unpaid Liens Found");
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Checking Saved LDN");
			}
		}

		private DateTime GetDefaultPaymentDate_2(int lngLRN)
		{
			return GetDefaultPaymentDate(ref lngLRN);
		}

		private DateTime GetDefaultPaymentDate(ref int lngLRN)
		{
			DateTime GetDefaultPaymentDate = System.DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDT = new clsDRWrapper();
				int lngAcct = 0;
				int lngYear = 0;
				rsDT.OpenRecordset("SELECT * FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(lngLRN), modExtraModules.strCLDatabase);
				if (!rsDT.EndOfFile())
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					lngAcct = FCConvert.ToInt32(rsDT.Get_Fields("Account"));
					lngYear = FCConvert.ToInt32(rsDT.Get_Fields_Int32("BillingYear"));
					rsDT.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAcct) + " AND [Year] = " + FCConvert.ToString(lngYear) + " ORDER BY RecordedTransactionDate Desc, ID", modExtraModules.strCLDatabase);
					if (!rsDT.EndOfFile())
					{
						GetDefaultPaymentDate = (DateTime)rsDT.Get_Fields_DateTime("RecordedTransactionDate");
					}
				}
				return GetDefaultPaymentDate;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Getting Default Payment Date");
			}
			return GetDefaultPaymentDate;
		}

		private string GetMuniName(ref int lngTownNumber)
		{
			string GetMuniName = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMulti = new clsDRWrapper();
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					rsMulti.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber = " + FCConvert.ToString(lngTownNumber), "CentralData");
					if (!rsMulti.EndOfFile())
					{
						GetMuniName = FCConvert.ToString(rsMulti.Get_Fields_String("TownName"));
					}
					else
					{
						GetMuniName = modGlobalConstants.Statics.MuniName;
					}
				}
				else
				{
					GetMuniName = modGlobalConstants.Statics.MuniName;
				}
				return GetMuniName;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetMuniName = modGlobalConstants.Statics.MuniName;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Getting Town Name");
			}
			return GetMuniName;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}

		private void cmdFilePrintBlankForm_Click(object sender, EventArgs e)
		{
			this.mnuFilePrintBlankForm_Click(sender, e);
		}
	}
}
