﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;

namespace Global
{
	public class cCESecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cCESecuritySetup() : base()
		{
			strThisModule = "CE";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Enter Code Enforcement", "", 1, false, ""));
			AddItem_2(CreateItem("Contractors", "", 5, true, ""));
			AddItem_2(CreateItem("Contractors", "Edit Contractors", 17, false, ""));
			AddItem_2(CreateItem("Description Records", "", 9, true, ""));
			AddItem_2(CreateItem("Description Records", "Edit Description Records", 11, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 6, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Customize", 8, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Description Records", 9, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Property Functions", 7, false, ""));
			AddItem_2(CreateItem("Inspections", "", 4, true, ""));
			AddItem_2(CreateItem("Inspections", "Edit Inspections", 15, false, ""));
			AddItem_2(CreateItem("Permits", "", 3, true, ""));
			AddItem_2(CreateItem("Permits", "Edit Permits", 13, false, ""));
			AddItem_2(CreateItem("Printing", "", 14, true, ""));
			AddItem_2(CreateItem("Printing", "Edit Forms and Documents", 16, false, ""));
			AddItem_2(CreateItem("Property", "", 2, true, ""));
			AddItem_2(CreateItem("Property", "Edit Properties", 12, false, ""));
			AddItem_2(CreateItem("Violations", "", 18, true, ""));
			AddItem_2(CreateItem("Violations", "Edit Violations", 19, false, ""));
		}
	}
}
