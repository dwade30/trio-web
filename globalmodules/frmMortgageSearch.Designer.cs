﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmMortgageSearch.
	/// </summary>
	partial class frmMortgageSearch : BaseForm
	{
		public fecherFoundation.FCGrid SearchGrid;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMortgageSearch));
			this.SearchGrid = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 520);
			this.BottomPanel.Size = new System.Drawing.Size(658, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.SearchGrid);
			this.ClientArea.Size = new System.Drawing.Size(658, 460);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(658, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(201, 30);
			this.HeaderText.Text = "Mortgage Search";
			// 
			// SearchGrid
			// 
			this.SearchGrid.AllowSelection = false;
			this.SearchGrid.AllowUserToResizeColumns = false;
			this.SearchGrid.AllowUserToResizeRows = false;
			this.SearchGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.SearchGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.SearchGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorSel = System.Drawing.Color.Empty;
			this.SearchGrid.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.SearchGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.SearchGrid.ColumnHeadersHeight = 30;
			this.SearchGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.SearchGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.SearchGrid.DragIcon = null;
			this.SearchGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.SearchGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.SearchGrid.FixedCols = 0;
			this.SearchGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.FrozenCols = 0;
			this.SearchGrid.GridColor = System.Drawing.Color.Empty;
			this.SearchGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.Location = new System.Drawing.Point(30, 30);
			this.SearchGrid.Name = "SearchGrid";
			this.SearchGrid.ReadOnly = true;
			this.SearchGrid.RowHeadersVisible = false;
			this.SearchGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.SearchGrid.RowHeightMin = 0;
			this.SearchGrid.Rows = 10;
			this.SearchGrid.ScrollTipText = null;
			this.SearchGrid.ShowColumnVisibilityMenu = false;
			this.SearchGrid.Size = new System.Drawing.Size(598, 403);
			this.SearchGrid.StandardTab = true;
			this.SearchGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.SearchGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.SearchGrid.TabIndex = 0;
			this.SearchGrid.DoubleClick += new System.EventHandler(this.SearchGrid_DblClick);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// frmMortgageSearch
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(658, 628);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmMortgageSearch";
			this.Text = "Mortgage Search";
			this.Load += new System.EventHandler(this.frmMortgageSearch_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMortgageSearch_KeyDown);
			this.Resize += new System.EventHandler(this.frmMortgageSearch_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
