﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cSetting
	{
		//=========================================================
		private string strSettingName = string.Empty;
		private string strSettingValue = string.Empty;
		private int lngID;
		private string strSettingType = string.Empty;
		private string strOwner = string.Empty;
		private string strOwnerType = string.Empty;

		public string SettingName
		{
			set
			{
				strSettingName = value;
			}
			get
			{
				string SettingName = "";
				SettingName = strSettingName;
				return SettingName;
			}
		}

		public string SettingValue
		{
			set
			{
				strSettingValue = value;
			}
			get
			{
				string SettingValue = "";
				SettingValue = strSettingValue;
				return SettingValue;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public string SettingType
		{
			set
			{
				strSettingType = value;
			}
			get
			{
				string SettingType = "";
				SettingType = strSettingType;
				return SettingType;
			}
		}

		public string Owner
		{
			set
			{
				strOwner = value;
			}
			get
			{
				string Owner = "";
				Owner = strOwner;
				return Owner;
			}
		}

		public string OwnerType
		{
			set
			{
				strOwnerType = value;
			}
			get
			{
				string OwnerType = "";
				OwnerType = strOwnerType;
				return OwnerType;
			}
		}
	}
}
