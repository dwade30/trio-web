﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;
using modGlobalRoutines = TWCL0000.modGlobal;


#elif TWBL0000
using TWBL0000;
using modExtraModules = TWBL0000.modGlobalVariables;
using modGlobal = TWBL0000.modGlobalRoutines;


#elif TWRE0000
using TWRE0000;
using modGlobal = TWRE0000.modGlobalVariables;
using modExtraModules = TWRE0000.modGlobalVariables;


#elif TWUT0000
using modGlobalRoutines = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmMortgageSearch.
	/// </summary>
	public partial class frmMortgageSearch : BaseForm
	{
		public frmMortgageSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMortgageSearch InstancePtr
		{
			get
			{
				return (frmMortgageSearch)Sys.GetInstance(typeof(frmMortgageSearch));
			}
		}

		protected frmMortgageSearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/24/2004              *
		// ********************************************************
		bool boolMortgageHolder;
		bool boolUTAccount;
		bool boolIsJoined;
		int lngColNumber;
		int lngColName;
		int lngColAddress;
		int lngColMapLot;
		int lngColExtra;

		private void frmMortgageSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmMortgageSearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMortgageSearch.Icon	= "frmMortgageSearch.frx":0000";
			//frmMortgageSearch.ScaleMode	= 0;
			//frmMortgageSearch.ScaleWidth	= 9725.806640625;
			//frmMortgageSearch.ScaleHeight	= 7440;
			//frmMortgageSearch.LinkTopic	= "Form1";
			//frmMortgageSearch.LockControls	= true;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//SearchGrid.BackColor	= "-2147483643";
			//			//SearchGrid.ForeColor	= "-2147483640";
			//SearchGrid.BorderStyle	= 1;
			//SearchGrid.FillStyle	= 0;
			//SearchGrid.Appearance	= 1;
			//SearchGrid.GridLines	= 1;
			//SearchGrid.WordWrap	= 0;
			//SearchGrid.ScrollBars	= 2;
			//SearchGrid.RightToLeft	= 0;
			//SearchGrid._cx	= 15319;
			//SearchGrid._cy	= 11255;
			//SearchGrid._ConvInfo	= 1;
			//SearchGrid.MousePointer	= 0;
			//SearchGrid.BackColorFixed	= -2147483633;
			//			//SearchGrid.ForeColorFixed	= -2147483630;
			//SearchGrid.BackColorSel	= -2147483635;
			//			//SearchGrid.ForeColorSel	= -2147483634;
			//SearchGrid.BackColorBkg	= -2147483636;
			//SearchGrid.BackColorAlternate	= -2147483643;
			//SearchGrid.GridColor	= -2147483633;
			//SearchGrid.GridColorFixed	= -2147483632;
			//SearchGrid.TreeColor	= -2147483632;
			//SearchGrid.FloodColor	= 192;
			//SearchGrid.SheetBorder	= -2147483642;
			//SearchGrid.FocusRect	= 0;
			//SearchGrid.HighLight	= 1;
			//SearchGrid.AllowSelection	= false;
			//SearchGrid.AllowBigSelection	= false;
			//SearchGrid.AllowUserResizing	= 1;
			//SearchGrid.SelectionMode	= 1;
			//SearchGrid.GridLinesFixed	= 2;
			//SearchGrid.GridLineWidth	= 1;
			//SearchGrid.RowHeightMin	= 0;
			//SearchGrid.RowHeightMax	= 0;
			//SearchGrid.ColWidthMin	= 0;
			//SearchGrid.ColWidthMax	= 0;
			//SearchGrid.ExtendLastCol	= true;
			//SearchGrid.FormatString	= "";
			//SearchGrid.ScrollTrack	= true;
			//SearchGrid.ScrollTips	= false;
			//SearchGrid.MergeCells	= 0;
			//SearchGrid.MergeCompare	= 0;
			//SearchGrid.AutoResize	= true;
			//SearchGrid.AutoSizeMode	= 0;
			//SearchGrid.AutoSearch	= 0;
			//SearchGrid.AutoSearchDelay	= 2;
			//SearchGrid.MultiTotals	= true;
			//SearchGrid.SubtotalPosition	= 1;
			//SearchGrid.OutlineBar	= 0;
			//SearchGrid.OutlineCol	= 0;
			//SearchGrid.Ellipsis	= 0;
			//SearchGrid.ExplorerBar	= 1;
			//SearchGrid.PicturesOver	= false;
			//SearchGrid.PictureType	= 0;
			//SearchGrid.TabBehavior	= 0;
			//SearchGrid.OwnerDraw	= 0;
			//SearchGrid.ShowComboButton	= true;
			//SearchGrid.TextStyle	= 0;
			//SearchGrid.TextStyleFixed	= 0;
			//SearchGrid.OleDragMode	= 0;
			//SearchGrid.OleDropMode	= 0;
			//SearchGrid.ComboSearch	= 3;
			//SearchGrid.AutoSizeMouse	= true;
			//SearchGrid.AllowUserFreezing	= 0;
			//SearchGrid.BackColorFrozen	= 0;
			//			//SearchGrid.ForeColorFrozen	= 0;
			//SearchGrid.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmMortgageSearch.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//lngColNumber = 0;
			//lngColName = 1;
			//lngColAddress = 2;
			//lngColMapLot = 3;
			//lngColExtra = 4;
		}

		private void frmMortgageSearch_Resize(object sender, System.EventArgs e)
		{
			ResizeSearchGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			App.MainForm.Show();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupSearchGrid(ref bool boolJoined)
		{
			//FC:FINAL:AM: moved code from load
			lngColNumber = 0;
			lngColName = 1;
			lngColAddress = 2;
			lngColMapLot = 3;
			lngColExtra = 4;
			int GridWidth = 0;
			SearchGrid.ExtendLastCol = false;
			GridWidth = SearchGrid.WidthOriginal;
			SearchGrid.Rows = 1;
			SearchGrid.Cols = 5;
            //FC:FINAL:CHN: Set column data type to fix ordering.
            SearchGrid.ColDataType(lngColNumber, FCGrid.DataTypeSettings.flexDTLong);
            SearchGrid.ColAlignment(lngColMapLot, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			SearchGrid.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			SearchGrid.ColAlignment(lngColNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			SearchGrid.ColAlignment(lngColAddress, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			SearchGrid.ColAlignment(lngColExtra, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			if (boolMortgageHolder)
			{
				SearchGrid.ExtendLastCol = true;
				if (boolJoined)
				{
					SearchGrid.ColWidth(lngColNumber, FCConvert.ToInt32(0.11 * GridWidth));
					SearchGrid.ColWidth(lngColName, FCConvert.ToInt32(0.29 * GridWidth));
					SearchGrid.ColWidth(lngColAddress, FCConvert.ToInt32(0.29 * GridWidth));
					SearchGrid.ColWidth(lngColMapLot, 0);
					SearchGrid.ColWidth(lngColExtra, FCConvert.ToInt32(0.29 * GridWidth));
				}
				else
				{
					SearchGrid.ColWidth(lngColNumber, FCConvert.ToInt32(0.11 * GridWidth));
					SearchGrid.ColWidth(lngColName, FCConvert.ToInt32(0.35 * GridWidth));
					SearchGrid.ColWidth(lngColAddress, FCConvert.ToInt32(0.35 * GridWidth));
					SearchGrid.ColWidth(lngColMapLot, 0);
					SearchGrid.ColWidth(lngColExtra, 0);
				}
				SearchGrid.ExtendLastCol = true;
				SearchGrid.TextMatrix(0, lngColNumber, "Number");
				SearchGrid.TextMatrix(0, lngColName, "Mortgage Holder");
				SearchGrid.TextMatrix(0, lngColAddress, "Holder Address");
				if (boolJoined)
				{
					SearchGrid.TextMatrix(0, lngColExtra, "RE Name");
				}
			}
			else if (boolUTAccount)
			{
				SearchGrid.ExtendLastCol = true;
				SearchGrid.ColWidth(lngColNumber, FCConvert.ToInt32(0.11 * GridWidth));
				SearchGrid.ColWidth(lngColName, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColAddress, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColMapLot, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColExtra, 0);
				SearchGrid.TextMatrix(0, lngColNumber, "Account");
				SearchGrid.TextMatrix(0, lngColName, "Name");
				SearchGrid.TextMatrix(0, lngColAddress, "Address");
				if (boolJoined)
				{
					SearchGrid.TextMatrix(0, lngColMapLot, "Mortgage Holder");
				}
				else
				{
					SearchGrid.TextMatrix(0, lngColMapLot, "Map/Lot");
				}
			}
			else
			{
				SearchGrid.ExtendLastCol = true;
				SearchGrid.ColWidth(lngColNumber, FCConvert.ToInt32(0.11 * GridWidth));
				SearchGrid.ColWidth(lngColName, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColAddress, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColMapLot, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColExtra, 0);
				SearchGrid.TextMatrix(0, lngColNumber, "Account");
				SearchGrid.TextMatrix(0, lngColName, "Name");
				SearchGrid.TextMatrix(0, lngColAddress, "Location");
				if (boolJoined)
				{
					SearchGrid.TextMatrix(0, lngColMapLot, "Mortgage Holder");
				}
				else
				{
					SearchGrid.TextMatrix(0, lngColMapLot, "Map/Lot");
				}
			}
		}

		private void ResizeSearchGrid()
		{
			int GridWidth = 0;
			GridWidth = SearchGrid.WidthOriginal;
			if (boolMortgageHolder)
			{
				SearchGrid.ColWidth(lngColNumber, FCConvert.ToInt32(0.11 * GridWidth));
				SearchGrid.ColWidth(lngColName, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColAddress, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColMapLot, 0);
				if (boolIsJoined)
				{
					SearchGrid.ColWidth(lngColExtra, FCConvert.ToInt32(0.29 * GridWidth));
				}
			}
			else
			{
				SearchGrid.ColWidth(lngColNumber, FCConvert.ToInt32(0.11 * GridWidth));
				SearchGrid.ColWidth(lngColName, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColAddress, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColMapLot, FCConvert.ToInt32(0.29 * GridWidth));
				SearchGrid.ColWidth(lngColExtra, 0);
			}
			SetGridHeight();
		}

		private int FillSearchGrid(ref string strSQL, ref bool boolJoined, string strDBName = "CentralData")
		{
			int FillSearchGrid = 0;
			// takes an sqlstring and fills the search grid
			clsDRWrapper clsSearch = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strMHAddress = "";
			try
			{
				// On Error GoTo ErrorHandler
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Searching");
				string strDBToUse;
				strDBToUse = strDBName;
				if (boolUTAccount)
				{
					clsSearch.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					// clsSearch.InsertName "OwnerPartyID", "Own1", False, True, False, "", False, "", True, ""
				}
				else
				{
					if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
					{
						clsSearch.OpenRecordset(strSQL, strDBToUse);

					}
					else
					{
						clsSearch.OpenRecordset(strSQL, strDBToUse);
						// strGNDatabase
					}
				}
				FillSearchGrid = clsSearch.RecordCount();
				if (FillSearchGrid > 0)
				{
					while (!clsSearch.EndOfFile())
					{
						App.DoEvents();
						if (boolMortgageHolder)
						{
							// the record set will hold only mortgage holder information
							strMHAddress = FCConvert.ToString(clsSearch.Get_Fields_String("Address1")) + " " + Strings.Trim(FCConvert.ToString(clsSearch.Get_Fields_String("Address2")));
							if (Strings.Trim(strMHAddress) != "")
							{
								strMHAddress = Strings.Trim(strMHAddress) + ", " + FCConvert.ToString(clsSearch.Get_Fields_String("City"));
							}
							else
							{
								strMHAddress = FCConvert.ToString(clsSearch.Get_Fields_String("City"));
							}
							if (boolJoined)
							{
								SearchGrid.AddItem("");
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColNumber, FCConvert.ToString(clsSearch.Get_Fields_Int32("id")));
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColName, FCConvert.ToString(clsSearch.Get_Fields_String("name")));
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColAddress, strMHAddress);
								// TODO: Field [Own1FullName] not found!! (maybe it is an alias?)
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColMapLot, FCConvert.ToString(clsSearch.Get_Fields("Own1FullName")));
							}
							else
							{
								SearchGrid.AddItem((FCConvert.ToString(clsSearch.Get_Fields_Int32("ID")) + "\t" + FCConvert.ToString(clsSearch.Get_Fields_String("name"))) + "\t" + strMHAddress);
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColNumber, FCConvert.ToString(clsSearch.Get_Fields_Int32("ID")));
								// MORTgageholders.
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColName, FCConvert.ToString(clsSearch.Get_Fields_String("name")));
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColAddress, strMHAddress);
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColMapLot, "");
							}
						}
						else if (boolUTAccount)
						{
							if (boolJoined)
							{
								SearchGrid.AddItem("");
								// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColNumber, FCConvert.ToString(clsSearch.Get_Fields("AccountNumber")));
								// TODO: Field [OwnerFullNameLF] not found!! (maybe it is an alias?)
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColName, FCConvert.ToString(clsSearch.Get_Fields("OwnerFullNameLF")));
							}
							else
							{
								SearchGrid.AddItem("");
								// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColNumber, FCConvert.ToString(clsSearch.Get_Fields("AccountNumber")));
								// TODO: Field [OwnerFullNameLF] not found!! (maybe it is an alias?)
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColName, FCConvert.ToString(clsSearch.Get_Fields("OwnerFullNameLF")));
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColMapLot, FCConvert.ToString(clsSearch.Get_Fields_String("Maplot")));
							}
						}
						else
						{
							// the recordset will hold only real estate account information
							if (boolJoined)
							{
								SearchGrid.AddItem("");
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColNumber, FCConvert.ToString(clsSearch.Get_Fields_Int32("RSAccount")));
								// TODO: Field [OwnerFullNameLF] not found!! (maybe it is an alias?)
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColName, FCConvert.ToString(clsSearch.Get_Fields("OwnerFullNameLF")));
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColAddress, FCConvert.ToString(clsSearch.Get_Fields_String("RSLOCNUMALPH")) + " " + FCConvert.ToString(clsSearch.Get_Fields_String("RSLOCStreet")));
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColMapLot, FCConvert.ToString(clsSearch.Get_Fields_String("RSMapLot")));
							}
							else
							{
								SearchGrid.AddItem("");
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColNumber, FCConvert.ToString(clsSearch.Get_Fields_Int32("RSAccount")));
								// SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColName) = clsSearch.Get_Fields("OwnerFullNameLF")
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColName, FCConvert.ToString(clsSearch.Get_Fields_String("FullNameLF")));
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColAddress, FCConvert.ToString(clsSearch.Get_Fields_String("RSLOCNUMALPH")) + " " + FCConvert.ToString(clsSearch.Get_Fields_String("RSLOCStreet")));
								SearchGrid.TextMatrix(SearchGrid.Rows - 1, lngColMapLot, FCConvert.ToString(clsSearch.Get_Fields_String("RSMapLot")));
							}
						}
						clsSearch.MoveNext();
					}
					// set the height of the grid depending on how many results were returned
					SetGridHeight();
				}
				frmWait.InstancePtr.Unload();
				return FillSearchGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				FillSearchGrid = 0;
				FCMessageBox.Show("Error # " + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Exclamation, "Fill Search Grid Error");
			}
			return FillSearchGrid;
		}

		public void Init(ref string strSQL, short intAccountType, bool boolJoined, bool boolMatchingRecords, string strDBName = "CentralData")
		{
			// inputs the sqlstring and a boolean telling whether we should open a mortgage or re account when double clicked
			int lngNum;
			boolMatchingRecords = true;
			switch (intAccountType)
			{
				case 1:
					{
						// Mortgage Holder
						boolMortgageHolder = true;
						boolUTAccount = false;
						break;
					}
				case 2:
					{
						// RE
						boolMortgageHolder = false;
						boolUTAccount = false;
						break;
					}
				case 3:
					{
						// UT
						boolMortgageHolder = false;
						boolUTAccount = true;
						break;
					}
			}
			//end switch
			boolIsJoined = boolJoined;
			SetupSearchGrid(ref boolJoined);
			lngNum = FillSearchGrid(ref strSQL, ref boolJoined, strDBName);
			if (lngNum > 0)
			{
				this.Show(App.MainForm);
			}
			else
			{
				boolMatchingRecords = false;
				FCMessageBox.Show("No records were found matching the criteria selected.", MsgBoxStyle.Exclamation, "No Records Found");
				Close();
			}
		}

		private void SearchGrid_AfterUserResize(int Row, int Col)
		{
			SetGridHeight();
		}

		private void SearchGrid_DblClick(object sender, EventArgs e)
		{
			int lngMR;
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngTemp = 0;
			lngMR = SearchGrid.MouseRow;
			if (lngMR < SearchGrid.Rows && lngMR > 0)
			{
				// lets open the account
				if (boolMortgageHolder)
				{
					// open the mortgageholder screen
					//! Load frmMortgageHolder;
					// send mortgage number and set bool to true to indicate it is a mortgage holder
					frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(SearchGrid.TextMatrix(lngMR, 0)), 1);
					frmMortgageHolder.InstancePtr.Show(App.MainForm);
				}
				else if (boolUTAccount)
				{
					// MAL@20070919: Add check for RE Account Association and limit the MortgageHolder form accordingly
					clsTemp.OpenRecordset("SELECT REAccount FROM Master WHERE NOT (Deleted = 1) AND AccountNumber = " + SearchGrid.TextMatrix(lngMR, 0), modExtraModules.strUTDatabase);
					if (!clsTemp.EndOfFile())
					{
						if (FCConvert.ToInt32(clsTemp.Get_Fields_Int32("REAccount")) > 0 && modGlobalConstants.Statics.gboolRE)
						{
							lngTemp = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("REAccount"));
							clsTemp.OpenRecordset("SELECT RSAccount FROM Master WHERE NOT (RSDeleted = 1) AND RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngTemp), modExtraModules.strREDatabase);
							if (!clsTemp.EndOfFile())
							{
								FCMessageBox.Show("This Utility account is linked to Real Estate account #" + FCConvert.ToString(clsTemp.Get_Fields_Int32("RSAccount")) + ".", MsgBoxStyle.Information, "Linked Account");
								//! Load frmMortgageHolder;
								// send RE Account associated with the UT Account and set to 2 to indicate it is a RE Account Number
								frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(clsTemp.Get_Fields_Int32("RSAccount")), 2, true);
                                //FC:FINAL:AM:#3395 - close first the form to activate the correct tab
                                Close();
                                frmMortgageHolder.InstancePtr.Show(App.MainForm);
								modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastUTNumber", modGlobalRoutines.PadToString(FCConvert.ToInt32(SearchGrid.TextMatrix(lngMR, 0)), 6));
								//Close();
							}
							else
							{
								FCMessageBox.Show("This Utility account was linked to Real Estate account #" + FCConvert.ToString(lngTemp) + ", but the link is invalid and will be removed.", MsgBoxStyle.Information, "Linked Account");
								clsTemp.OpenRecordset("SELECT * FROM Master WHERE NOT (Deleted = 1) AND AccountNumber = " + SearchGrid.TextMatrix(lngMR, 0), modExtraModules.strUTDatabase);
								clsTemp.Edit();
								clsTemp.Set_Fields("REAccount", 0);
								clsTemp.Update();
								// load the UT account by itself
								//! Load frmMortgageHolder;
								// send UT Account and set to 3 to indicate it is a UT Account Number
								frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(SearchGrid.TextMatrix(lngMR, 0)), 3, false);
                                //FC:FINAL:AM:#3395 - close first the form to activate the correct tab
                                Close();
                                frmMortgageHolder.InstancePtr.Show(App.MainForm);
								//Close();
							}
						}
						else
						{
							//! Load frmMortgageHolder;
							// send UT Account and set to 3 to indicate it is a UT Account Number
							frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(SearchGrid.TextMatrix(lngMR, 0)), 3, false);
                            //FC:FINAL:CHN - issue #1572:Change steps open new form and close current to get correct focus.
                            Close();
                            frmMortgageHolder.InstancePtr.Show(App.MainForm);
						}
					}
					// Load frmMortgageHolder
					// frmMortgageHolder.Init SearchGrid.TextMatrix(lngMR, 0), 3, False
					// frmMortgageHolder.Show , MDIParent
				}
				else
				{
                    // open the real estate screen
                    //! Load frmMortgageHolder;
                    // send re account and set bool to false to indicate it is a re number
                    //FC:FINAL:CHN - issue #1572:Change steps open new form and close current to get correct focus.
                    frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(SearchGrid.TextMatrix(lngMR, 0)), 2);
                    Close();
					frmMortgageHolder.InstancePtr.Show(App.MainForm);
				}
			}
		}

		private void SetGridHeight()
		{
			// this will find the correct grid height for the number of results returned
			int lngH;
			int lngW;
			int intCT;
			int lngTotW = 0;
			lngH = frmMortgageSearch.InstancePtr.HeightOriginal;
			lngW = SearchGrid.WidthOriginal;
			for (intCT = 0; intCT <= SearchGrid.Cols - 1; intCT++)
			{
				lngTotW += SearchGrid.ColWidth(intCT);
			}
			if (lngTotW > lngW)
			{
				// needs horiz scroll bar
				// If (SearchGrid.Rows * SearchGrid.RowHeight(0)) + 70 > lngH * 0.8 Then
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//if (((SearchGrid.Rows + 1) * SearchGrid.RowHeight(0)) > (lngH - (SearchGrid.Top * 2)))
				//{
				//	// if the number of rows is greater than what we can see on the screen
				//	// then just set the height to the 80% of the screen and turn the scroll bars on
				//	// SearchGrid.Height = lngH * 0.8
				//	SearchGrid.HeightOriginal = (lngH - (SearchGrid.Top * 2));
				//	SearchGrid.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
				//}
				//else
				//{
				//	// otherwise just show the rows that are filled
				//	SearchGrid.HeightOriginal = ((SearchGrid.Rows + 1) * SearchGrid.RowHeight(0));
				//	SearchGrid.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
				//}
			}
			else
			{
				// If (SearchGrid.Rows * SearchGrid.RowHeight(0)) + 70 > lngH * 0.8 Then
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//if (((SearchGrid.Rows * SearchGrid.RowHeight(0)) + 70) > (lngH - (SearchGrid.Top * 2)))
				//{
				//	// if the number of rows is greater than what we can see on the screen
				//	// then just set the height to the 80% of the screen and turn the scroll bars on
				//	// SearchGrid.Height = lngH * 0.8
				//	SearchGrid.HeightOriginal = (lngH - (SearchGrid.Top * 2));
				//	SearchGrid.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//	// otherwise just show the rows that are filled
				//	SearchGrid.HeightOriginal = (SearchGrid.Rows * SearchGrid.RowHeight(0)) + 70;
				//	SearchGrid.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
			}
		}
	}
}
