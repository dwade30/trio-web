﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;

namespace Global
{
	public class modConvertAmountToString
	{
		//=========================================================
		public static string ConvertAmountToString(Decimal amt)
		{
			string ConvertAmountToString = "";
			// vbPorter upgrade warning: ML As Decimal	OnWriteFCConvert.ToInt32(
			Decimal ML;
			// vbPorter upgrade warning: TH As Decimal	OnWrite
			Decimal TH;
			// vbPorter upgrade warning: HN As Decimal	OnWrite(short, string)
			Decimal HN;
			// vbPorter upgrade warning: TN As Decimal	OnWrite(double, string)
			Decimal TN;
			Decimal CN;
			// FC:FINAL:VGE - #765 'amt' replaced with 'Math.Floor(amt)' to avoid 'amthematical' rounding of FCConvert.ToInt32, for example from 99.99 to 100 (and not 99)
			ML = FCConvert.ToInt32(Math.Floor(amt)) / 1000000;
			TH = FCConvert.ToInt32(Math.Floor(amt) - (ML * 1000000)) / 1000;
			HN = FCConvert.ToInt32(Math.Floor(amt) - (ML * 1000000) - (TH * 1000)) / 100;
			if (Strings.InStr(1, FCConvert.ToString(HN), ".", CompareConstants.vbBinaryCompare) != 0)
			{
				HN = FCConvert.ToDecimal(Strings.Left(FCConvert.ToString(HN), Strings.InStr(1, FCConvert.ToString(HN), ".", CompareConstants.vbBinaryCompare)));
			}
			// FC:FINAL:VGE - #765 'amt' replaced with 'Math.Floor(amt)' to avoid 'amthematical' rounding of FCConvert.ToInt32, for example from 99.99 to 100 (and not 99)
			TN = FCConvert.ToDecimal(FCConvert.ToInt32(Math.Floor(amt) - (ML * 1000000) - (TH * 1000) - (HN * 100)) / 1.0);
			if (Strings.InStr(1, FCConvert.ToString(TN), ".", CompareConstants.vbBinaryCompare) != 0)
			{
				TN = FCConvert.ToDecimal(Strings.Left(FCConvert.ToString(TN), Strings.InStr(1, FCConvert.ToString(TN), ".", CompareConstants.vbBinaryCompare)));
			}
			CN = (amt - (ML * 1000000) - (TH * 1000) - (HN * 100) - (TN * 1)) * 100;
			if (ML != 0)
			{
				ConvertAmountToString = ConvertAmountToString + GetAmount(ref ML) + "MILLION ";
			}
			if (TH != 0)
			{
				ConvertAmountToString = ConvertAmountToString + GetAmount(ref TH) + "THOUSAND ";
			}
			if (HN != 0)
			{
				ConvertAmountToString = ConvertAmountToString + GetAmount(ref HN) + "HUNDRED ";
			}
			if (TN != 0)
			{
				// If amt > 99 Then
				// ConvertAmountToString = ConvertAmountToString & "AND " & GetAmount(TN)
				// Else
				ConvertAmountToString = ConvertAmountToString + GetAmount(ref TN);
				// End If
			}
			ConvertAmountToString = ConvertAmountToString + "AND " + Strings.Format(CN, "00") + "/100";
			return ConvertAmountToString;
		}
		// vbPorter upgrade warning: x As Decimal	OnReadFCConvert.ToInt32(
		public static string GetAmount(ref Decimal x)
		{
			string GetAmount = "";
			string[] VL = new string[19 + 1];
			string[] VLA = new string[9 + 1];
			int VL1 = 0;
			// vbPorter upgrade warning: VL2 As short --> As int	OnWrite(short, Decimal)
			int VL2 = 0;
			int VL2A = 0;
			int VL2B = 0;
			VL[0] = "UNUSED";
			VL[1] = "ONE";
			VL[2] = "TWO";
			VL[3] = "THREE";
			VL[4] = "FOUR";
			VL[5] = "FIVE";
			VL[6] = "SIX";
			VL[7] = "SEVEN";
			VL[8] = "EIGHT";
			VL[9] = "NINE";
			VL[10] = "TEN";
			VL[11] = "ELEVEN";
			VL[12] = "TWELVE";
			VL[13] = "THIRTEEN";
			VL[14] = "FOURTEEN";
			VL[15] = "FIFTEEN";
			VL[16] = "SIXTEEN";
			VL[17] = "SEVENTEEN";
			VL[18] = "EIGHTEEN";
			VL[19] = "NINETEEN";
			VLA[1] = "UNUSED";
			VLA[2] = "TWENTY";
			VLA[3] = "THIRTY";
			VLA[4] = "FORTY";
			VLA[5] = "FIFTY";
			VLA[6] = "SIXTY";
			VLA[7] = "SEVENTY";
			VLA[8] = "EIGHTY";
			VLA[9] = "NINETY";
			if (x.ToString().Length > 2)
			{
				VL1 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(FCConvert.ToString(x), 1, 1))));
				VL2 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(FCConvert.ToString(x), 2, 2))));
			}
			else
			{
				VL1 = 0;
				VL2 = FCConvert.ToInt32(x);
			}
			// 
			if (VL1 != 0)
			{
				GetAmount = GetAmount + VL[VL1] + " HUNDRED ";
			}
			// 
			if (VL2 != 0)
			{
				if (VL2 < 20)
				{
					GetAmount += VL[VL2] + " ";
				}
				else
				{
					VL2A = VL2 / 10;
					VL2B = VL2 - (VL2A * 10);
					GetAmount += VLA[VL2A];
					if (VL2B != 0)
					{
						GetAmount = GetAmount + "-" + VL[VL2B] + " ";
					}
					else
					{
						GetAmount = GetAmount + " ";
					}
				}
			}
			return GetAmount;
		}
	}
}
