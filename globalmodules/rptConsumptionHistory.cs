﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for rptConsumptionHistory.
	/// </summary>
	public partial class rptConsumptionHistory : BaseSectionReport
	{
		// nObj = 1
		//   0	rptConsumptionHistory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/07/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/01/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCount;
		int lngNegCount;
		string strAccount = "";

		public rptConsumptionHistory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Consumption History";
		}

		public static rptConsumptionHistory InstancePtr
		{
			get
			{
				return (rptConsumptionHistory)Sys.GetInstance(typeof(rptConsumptionHistory));
			}
		}

		protected rptConsumptionHistory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			rsData.Dispose();
			base.Dispose(disposing);
		}

		public void Init(ref int lngAccountStart, ref int lngAccountEnd)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// strAccount = " AND (ActualAccountNumber >= " & lngAccountStart & " AND ActualAccountNumber <= " & lngAccountEnd & ")"
				// rsData.OpenRecordset "SELECT * FROM Bill WHERE BillingRateKey > 0 " & strAccount & " ORDER BY BillingRateKey desc", strUTDatabase
				//rsData.OpenRecordset("SELECT * FROM MeterConsumption WHERE (AccountKey >= " + FCConvert.ToString(lngAccountStart) + " AND AccountKey <= " + FCConvert.ToString(lngAccountEnd) + ") ORDER BY BillDate desc", modExtraModules.strUTDatabase);
				//kk01232017 trout-1225  Fix this so it's not using the key
				rsData.OpenRecordset("SELECT * FROM MeterConsumption INNER JOIN Master ON Master.id = MeterConsumption.AccountKey WHERE (AccountNumber >= " + lngAccountStart + " AND AccountNumber <= " + lngAccountEnd + ") ORDER BY BillDate desc", modExtraModules.strUTDatabase);
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Data"
				if (rsData.EndOfFile())
				{
					MessageBox.Show("No consumption history found.", "No Consumption History", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Cancel();
					this.Close();
					return;
				}
				if (lngAccountStart == lngAccountEnd)
				{
					//lblCriteria.Text = "Account : " + FCConvert.ToString(modUTFunctions.GetUTAccountNumber(lngAccountStart));
                    lblCriteria.Text = "Account : " + lngAccountStart;
                }
				else
				{
					//lblCriteria.Text = "Account : " + FCConvert.ToString(modUTFunctions.GetUTAccountNumber(lngAccountStart)) + " to " + FCConvert.ToString(modUTFunctions.GetUTAccountNumber(lngAccountEnd));
                    lblCriteria.Text = "Account : " + lngAccountStart + " to " + lngAccountEnd;
                }
				lblCriteria.Text = lblCriteria.Text + "\r\n" + "Report showing readings in units of : " + FCConvert.ToString(modExtraModules.Statics.glngTownReadingUnits) + " cu ft.";
				frmReportViewer.InstancePtr.Init(this);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Consumption Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}
		//private void ActiveReport_KeyPress(ref short KeyAscii)
		//{
		//	// captures the escape ID
		//	if (KeyAscii == 27)
		//	{
		//		KeyAscii = 0;
		//		Close();
		//	}
		//}
		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			rsData.DisposeOf();
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				FillHeaderLabels();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			if (!rsData.EndOfFile())
			{
				fldMeterNumber.Text = FCConvert.ToString(rsData.Get_Fields_Int32("MeterNumber"));
				fldBillDate.Text = Strings.Format(rsData.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
				fldPrev.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Begin"));
				// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
				fldCurr.Text = FCConvert.ToString(rsData.Get_Fields("End"));
				// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
				if (rsData.Get_Fields("End") - rsData.Get_Fields_Int32("Begin") >= 0)
				{
					// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
					fldConsumption.Text = FCConvert.ToString(rsData.Get_Fields("End") - rsData.Get_Fields_Int32("Begin"));
				}
				else
				{
					fldConsumption.Text = "Lower Current";
				}
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("NegativeMeter")))
				{
					fldNeg.Text = "Y";
					if (fldConsumption.Text != "Lower Current")
					{
						lngNegCount += FCConvert.ToInt32(fldConsumption.Text);
					}
				}
				else
				{
					fldNeg.Text = "N";
					if (fldConsumption.Text != "Lower Current")
					{
						lngCount += FCConvert.ToInt32(fldConsumption.Text);
					}
				}
				rsData.MoveNext();
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotal.Text = lngCount.ToString();
			fldNegConsumption.Text = lngNegCount.ToString();
		}
	}
}
