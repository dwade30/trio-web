﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

#if TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	public class modARStatusPayments
	{
		public struct ARPayment
		{
			// these are the fields that each payment needs to save
			public int ID;
			// vbPorter upgrade warning: ActualAccountNumber As int	OnRead(string)
			public int ActualAccountNumber;
			public int AccountKey;
			public int BillNumber;
			public string InvoiceNumber;
			public int BillKey;
			public int BillType;
			public int Year;
			public int CHGINTNumber;
			public DateTime CHGINTDate;
			// vbPorter upgrade warning: ActualSystemDate As DateTime	OnWrite(short, DateTime)
			public DateTime ActualSystemDate;
			// vbPorter upgrade warning: EffectiveInterestDate As DateTime	OnWrite(DateTime, short)
			public DateTime EffectiveInterestDate;
			// vbPorter upgrade warning: RecordedTransactionDate As DateTime	OnWrite(DateTime, short)
			public DateTime RecordedTransactionDate;
			public string Teller;
			public string Reference;
			public string Control1;
			public string Control2;
			public string Control3;
			public string Code;
			// vbPorter upgrade warning: ReceiptNumber As string	OnWrite(short, string)
			public string ReceiptNumber;
			// vbPorter upgrade warning: Principal As Decimal	OnWrite(short, double)	OnReadFCConvert.ToDouble(
			public Decimal Principal;
			// vbPorter upgrade warning: CurrentInterest As Decimal	OnWrite(short, double, Decimal)	OnRead(double, Decimal)
			public Decimal CurrentInterest;
			// vbPorter upgrade warning: Tax As Decimal	OnWrite(short, double)	OnReadFCConvert.ToDouble(
			public Decimal Tax;
			// vbPorter upgrade warning: TransNumber As string	OnWriteFCConvert.ToInt16(
			public string TransNumber;
			public string PaidBy;
			public string Comments;
			public string CashDrawer;
			public string GeneralLedger;
			public string BudgetaryAccountNumber;
			public string BillCode;
			public short ReversalID;
			public int ResCode;
			public int ReverseKey;
			// kk03262015 troar-61  Save the PaymentID of the payment being reversed
			//FC:FINAL:DSE:#257 Use custom constructor to initialize fields from the structure
			public ARPayment(int unusedParam)
			{
				this.ID = 0;
				this.AccountKey = 0;
				this.ActualAccountNumber = 0;
				this.BillCode = String.Empty;
				this.BillKey = 0;
				this.BillNumber = 0;
				this.BillType = 0;
				this.BudgetaryAccountNumber = String.Empty;
				this.CashDrawer = String.Empty;
				this.CHGINTNumber = 0;
				this.Code = String.Empty;
				this.Comments = String.Empty;
				this.Control1 = String.Empty;
				this.Control2 = String.Empty;
				this.Control3 = String.Empty;
				this.CurrentInterest = 0;
				this.GeneralLedger = String.Empty;
				this.InvoiceNumber = String.Empty;
				this.PaidBy = String.Empty;
				this.Principal = 0;
				this.ReceiptNumber = String.Empty;
				this.Reference = String.Empty;
				this.ResCode = 0;
				this.ReversalID = 0;
				this.ReverseKey = 0;
				this.Tax = 0;
				this.Teller = String.Empty;
				this.TransNumber = String.Empty;
				this.Year = 0;
				this.ActualSystemDate = DateTime.FromOADate(0);
				this.CHGINTDate = DateTime.FromOADate(0);
				this.EffectiveInterestDate = DateTime.FromOADate(0);
				this.RecordedTransactionDate = DateTime.FromOADate(0);
			}
		};
		// DJW@11/17/2010 Changed array size from 1000 to 20000
		public const int MAX_ARPAYMENTS = 20000;
		// this will hold the payments from the payments screen
		//public static bool CreateARPaymentRecord(int lngIndex, ref clsDRWrapper rsCPy, ref int lngRow)
		//{
		//	bool CreateARPaymentRecord = false;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsTemp = new clsDRWrapper();
		//		clsDRWrapper rsCreatePy = new clsDRWrapper();
		//		int intTemp;
		//		int lngCount;
		//		bool boolFound;
		//		double dblXInt = 0;
		//		// this will save one payment record to the database
		//		if (MAX_ARPAYMENTS > lngIndex && lngIndex > 0)
		//		{
		//			// checks for out of bounds array call
		//			if (Statics.ARPaymentArray[lngIndex].BillKey == 0)
		//			{
		//				CreateARPaymentRecord = false;
		//				return CreateARPaymentRecord;
		//			}
		//			rsCPy.AddNew();
		//			rsCPy.Update();
		//			Statics.ARPaymentArray[lngIndex].ID = rsCPy.Get_Fields_Int32("ID");
		//			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(lngRow, frmARCLStatus.InstancePtr.lngPayGridColKeyNumber, FCConvert.ToString(Statics.ARPaymentArray[lngIndex].ID));
		//			rsCPy.Set_Fields("AccountKey", Statics.ARPaymentArray[lngIndex].AccountKey);
		//			rsCPy.Set_Fields("ActualAccountNumber", Statics.ARPaymentArray[lngIndex].ActualAccountNumber);
		//			rsCPy.Set_Fields("BillNumber", Statics.ARPaymentArray[lngIndex].BillNumber);
		//			rsCPy.Set_Fields("InvoiceNumber", Statics.ARPaymentArray[lngIndex].InvoiceNumber);
		//			rsCPy.Set_Fields("BillKey", Statics.ARPaymentArray[lngIndex].BillKey);
		//			rsCPy.Set_Fields("BillType", Statics.ARPaymentArray[lngIndex].BillType);
		//			if (!modExtraModules.IsThisCR())
		//			{
		//				// this checks to see where CL is shelling to
		//				// if it is not going to shell back to CR
		//				// then update all of the records and finish the transaction
		//				rsCPy.Set_Fields("ReceiptNumber", 0);
		//				// rsTemp.OpenRecordset "SELECT * FROM Bill WHERE Billkey = " & .BillKey, strardatabase
		//				rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ID = " + FCConvert.ToString(Statics.ARPaymentArray[lngIndex].BillKey), modExtraModules.strARDatabase);
		//				if (rsTemp.RecordCount() != 0)
		//				{
		//					rsTemp.Edit();
		//					// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//					rsTemp.Set_Fields("IntPaid", (rsTemp.Get_Fields("IntPaid") + Statics.ARPaymentArray[lngIndex].CurrentInterest));
		//					// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//					rsTemp.Set_Fields("TaxPaid", (rsTemp.Get_Fields("TaxPaid") + Statics.ARPaymentArray[lngIndex].Tax));
		//					// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//					rsTemp.Set_Fields("PrinPaid", (rsTemp.Get_Fields("PrinPaid") + Statics.ARPaymentArray[lngIndex].Principal));
		//					if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")) == 0)
		//					{
		//						rsTemp.Set_Fields("PrePaymentAmount", (rsTemp.Get_Fields_Decimal("PrePaymentAmount" + "")) + Statics.ARPaymentArray[lngIndex].Principal);
		//					}
		//					rsTemp.Set_Fields("IntPaidDate", Statics.AREffectiveDate);
		//					if (!rsTemp.Update())
		//					{
		//						MessageBox.Show("Error updating bill record #" + FCConvert.ToString(Statics.ARPaymentArray[lngIndex].BillKey) + ".", "Error In Create Payment Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//						CreateARPaymentRecord = false;
		//						goto END_OF_PAYMENT;
		//					}
		//					// This will check to make sure this account is still active, else it will set the status to Paid
		//					if (modARCalculations.CalculateAccountAR(rsTemp, Statics.ARPaymentArray[lngIndex].EffectiveInterestDate, ref dblXInt) == 0)
		//					{
		//						if (FCConvert.ToString(rsTemp.Get_Fields_String("BillStatus")) != "P" && FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")) != 0)
		//						{
		//							rsTemp.Edit();
		//							rsTemp.Set_Fields("BillStatus", "P");
		//							rsTemp.Update();
		//						}
		//					}
		//					else
		//					{
		//						if (FCConvert.ToString(rsTemp.Get_Fields_String("BillStatus")) != "A")
		//						{
		//							rsTemp.Edit();
		//							rsTemp.Set_Fields("BillStatus", "A");
		//							rsTemp.Update();
		//						}
		//					}
		//				}
		//				else
		//				{
		//					CreateARPaymentRecord = false;
		//					goto END_OF_PAYMENT;
		//				}
		//			}
		//			else
		//			{
		//				rsCPy.Set_Fields("ReceiptNumber", -1);
		//			}
		//			// check for a reversal, create the CHGINT line if needed and adjust the arrax index so it does not get created a second time
		//			for (lngCount = 1; lngCount <= MAX_ARPAYMENTS; lngCount++)
		//			{
		//				if (Statics.ARPaymentArray[lngCount].ReversalID != -1)
		//				{
		//					if (Statics.ARPaymentArray[lngCount].ReversalID == lngIndex)
		//					{
		//						rsCreatePy.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountPayments + ") AS Table1 WHERE AccountKey = 0", modExtraModules.strARDatabase);
		//						CreateARCHGINTPaymentRecord_20(FCConvert.ToInt16(lngCount), rsCreatePy, true);
		//						rsCreatePy.MoveFirst();
		//						Statics.ARPaymentArray[lngIndex].CHGINTNumber = FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("ID"));
		//						boolFound = true;
		//						break;
		//					}
		//				}
		//			}
		//			rsCPy.Set_Fields("CHGINTNumber", Statics.ARPaymentArray[lngIndex].CHGINTNumber);
		//			rsCPy.Set_Fields("ActualSystemDate", Statics.ARPaymentArray[lngIndex].ActualSystemDate);
		//			rsCPy.Set_Fields("EffectiveInterestDate", Statics.ARPaymentArray[lngIndex].EffectiveInterestDate);
		//			rsCPy.Set_Fields("RecordedTransactionDate", Statics.ARPaymentArray[lngIndex].RecordedTransactionDate);
		//			rsCPy.Set_Fields("Teller", Strings.Trim(Statics.ARPaymentArray[lngIndex].Teller + " "));
		//			rsCPy.Set_Fields("Reference", Strings.Trim(Statics.ARPaymentArray[lngIndex].Reference + " "));
		//			rsCPy.Set_Fields("Code", Strings.Trim(Statics.ARPaymentArray[lngIndex].Code + " "));
		//			rsCPy.Set_Fields("Principal", Statics.ARPaymentArray[lngIndex].Principal);
		//			rsCPy.Set_Fields("Interest", Statics.ARPaymentArray[lngIndex].CurrentInterest);
		//			rsCPy.Set_Fields("Tax", Statics.ARPaymentArray[lngIndex].Tax);
		//			rsCPy.Set_Fields("TransNumber", Statics.ARPaymentArray[lngIndex].TransNumber);
		//			rsCPy.Set_Fields("PaidBy", Statics.ARPaymentArray[lngIndex].PaidBy);
		//			rsCPy.Set_Fields("Comments", Statics.ARPaymentArray[lngIndex].Comments);
		//			rsCPy.Set_Fields("CashDrawer", Statics.ARPaymentArray[lngIndex].CashDrawer);
		//			rsCPy.Set_Fields("GeneralLedger", Statics.ARPaymentArray[lngIndex].GeneralLedger);
		//			rsCPy.Set_Fields("BudgetaryAccountNumber", Statics.ARPaymentArray[lngIndex].BudgetaryAccountNumber);
		//			if (!rsCPy.Update())
		//			{
		//				MessageBox.Show("Error creating payment record for account #" + FCConvert.ToString(Statics.ARPaymentArray[lngIndex].ActualAccountNumber) + ".", "Error In Create Payment Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//				CreateARPaymentRecord = false;
		//				goto END_OF_PAYMENT;
		//			}
		//			CreateARPaymentRecord = true;
		//		}
		//		else
		//		{
		//			MessageBox.Show("Out of Bounds Array Index", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		}
		//		return CreateARPaymentRecord;
		//		END_OF_PAYMENT:
		//		;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		MessageBox.Show("Account " + FCConvert.ToString(Statics.ARPaymentArray[lngIndex].ActualAccountNumber) + " has had an error while processing.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		rsCPy.Delete();
		//	}
		//	return CreateARPaymentRecord;
		//}
		//// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		//public static short AddARPaymentToList_21(int cRow, bool boolSkipRecalc = false, double dblOverrideAmount = 0, int lngReversalKey = 0, bool blnExcessPayment = false)
		//{
		//	return AddARPaymentToList(ref cRow, false, boolSkipRecalc, dblOverrideAmount, lngReversalKey, blnExcessPayment);
		//}

		//public static short AddARPaymentToList_24(int cRow, bool boolCalcInterest = false, bool boolSkipRecalc = false, double dblOverrideAmount = 0, int lngReversalKey = 0, bool blnExcessPayment = false)
		//{
		//	return AddARPaymentToList(ref cRow, boolCalcInterest, boolSkipRecalc, dblOverrideAmount, lngReversalKey, blnExcessPayment);
		//}

		//public static short AddARPaymentToList_26(int cRow, bool boolCalcInterest = false, bool boolSkipRecalc = false, double dblOverrideAmount = 0, int lngReversalKey = 0, bool blnExcessPayment = false)
		//{
		//	return AddARPaymentToList(ref cRow, boolCalcInterest, boolSkipRecalc, dblOverrideAmount, lngReversalKey, blnExcessPayment);
		//}

		//public static short AddARPaymentToList_606(int cRow, bool blnExcessPayment = false)
		//{
		//	return AddARPaymentToList(ref cRow, false, false, 0, 0, blnExcessPayment);
		//}

		//public static short AddARPaymentToList_615(int cRow, bool boolSkipRecalc = false, bool blnExcessPayment = false)
		//{
		//	return AddARPaymentToList(ref cRow, false, boolSkipRecalc, 0, 0, blnExcessPayment);
		//}

		//public static short AddARPaymentToList(ref int cRow, bool boolCalcInterest = false, bool boolSkipRecalc = false, double dblOverrideAmount = 0, int lngReversalKey = 0, bool blnExcessPayment = false)
		//{
		//	short AddARPaymentToList = 0;
		//	// this sub will add a row to the grid and fill the payment info into it
		//	double dblTot;
		//	// total left of monies paid
		//	// vbPorter upgrade warning: dblPrin As double	OnWrite(Decimal, double)
		//	double dblPrin;
		//	// principal total left
		//	// vbPorter upgrade warning: dblPrinNeed As double	OnWrite(double, Decimal, short)
		//	double dblPrinNeed = 0;
		//	// principal needed to totally pay off principal of bill
		//	// vbPorter upgrade warning: dblTax As double	OnWrite(Decimal, short)
		//	double dblTax;
		//	// vbPorter upgrade warning: dblTaxNeed As double	OnWrite(double, Decimal, short)
		//	double dblTaxNeed = 0;
		//	// vbPorter upgrade warning: dblInt As double	OnWrite(Decimal, short)
		//	double dblInt;
		//	// interest total left
		//	// vbPorter upgrade warning: dblIntNeed As double	OnWrite(Decimal, short)
		//	double dblIntNeed = 0;
		//	// interest needed to pay off interest of bill
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	clsDRWrapper rsPeriod = new clsDRWrapper();
		//	int intCT;
		//	string strTemp = "";
		//	string strInvoice;
		//	// holds the correct BillKey for the payment record
		//	int lngBill = 0;
		//	string strType;
		//	int lngPaymentRow;
		//	bool boolFound;
		//	bool boolNoLoop = false;
		//	int lngTesterRow = 0;
		//	int lngStarterRow = 0;
		//	string strOppType = "";
		//	int counter;
		//	Decimal curARChargedNotSavedInt;
		//	// vbPorter upgrade warning: dblTempP As double	OnReadFCConvert.ToDecimal(
		//	double dblTempP = 0;
		//	// vbPorter upgrade warning: dblTempT As double	OnReadFCConvert.ToDecimal(
		//	double dblTempT = 0;
		//	// vbPorter upgrade warning: dblTempI As double	OnReadFCConvert.ToDecimal(
		//	double dblTempI = 0;
		//	// force all unsused indexes of ARPaymentArray to have a -1 value in the ReversalID
		//	for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//	{
		//		if (Statics.ARPaymentArray[intCT].BillKey == 0)
		//			Statics.ARPaymentArray[intCT].ReversalID = -1;
		//	}
		//	// find the first empty slot
		//	for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//	{
		//		if (Statics.ARPaymentArray[intCT].BillKey == 0)
		//		{
		//			break;
		//		}
		//	}
		//	//FC:FINAL:DSE:#257 Use custom constructor to initialize fields from the structure
		//	Statics.ARPaymentArray[intCT] = new ARPayment(0);
		//	Statics.ARPaymentArray[intCT].ResCode = frmARCLStatus.InstancePtr.lngARResCode;
		//	// this is the type of payment that is being processed
		//	strType = Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[frmARCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1);
		//	strInvoice = FindDefaultARBill(ref Statics.lngCurrentCustomerIDAR);
		//	dblInt = FCConvert.ToDouble(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtInterest.Text));
		//	dblTax = FCConvert.ToDouble(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtTax.Text));
		//	dblPrin = FCConvert.ToDouble(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtPrincipal.Text));
		//	dblTot = dblInt + dblPrin + dblTax;
		//	// make sure that this has a valid account if needed
		//	if (frmARCLStatus.InstancePtr.txtCD.Text == "N" && frmARCLStatus.InstancePtr.txtCash.Text == "N")
		//	{
		//		// check to make sure that this is only negative interest
		//		if (modValidateAccount.AccountValidate(frmARCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0)))
		//		{
		//			// all set
		//		}
		//		else if (dblInt < 0 && dblPrin == 0 && dblTax == 0)
		//		{
		//			// this is ok too
		//		}
		//		else
		//		{
		//			MessageBox.Show("Please enter a valid account for this payment that will not affect today's cash or the cash drawer.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//			if (frmARCLStatus.InstancePtr.txtAcctNumber.Enabled && frmARCLStatus.InstancePtr.txtAcctNumber.Visible)
		//			{
		//				frmARCLStatus.InstancePtr.txtAcctNumber.Focus();
		//			}
		//			AddARPaymentToList = -1;
		//			return AddARPaymentToList;
		//		}
		//	}
		//	// ************************************************************************************
		//	// This is where I check to see if it is an Auto payment or a payment to one bill     *
		//	// ************************************************************************************
		//	bool executeSTARTNOLOOP = false;
		//	if (Strings.Trim(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString()) == "Auto")
		//	{
		//		// if "Auto" is choosen in the Year Combobox
		//		// If frmARCLStatus.vsPayments.rows > 1 Then
		//		// check to see if the other payments are auto payemnts
		//		// If frmARCLStatus.vsPayments.rows > 1 Then   'another payment is found from this year
		//		// so do not let anyone get through it
		//		// MsgBox "You cannot use a code of Auto because there are already payments made.", vbExclamation, "Existing Payments"
		//		// If frmARCLStatus.cmbCode.Enabled And frmARCLStatus.cmbCode.Visible Then
		//		// frmARCLStatus.cmbCode.SetFocus
		//		// End If
		//		// AddARPaymentToList = -1
		//		// Exit Function
		//		// End If
		//		// 
		//		// else if so let them through
		//		// End If
		//		strTemp = "SELECT * FROM  (" + Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " ORDER BY BillStatus, BillDate , InvoiceNumber , BillNumber";
		//		// make sure that there is a bill that can be found to add the payment to
		//		rsTemp.OpenRecordset(strTemp, modExtraModules.strARDatabase);
		//		if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//		{
		//		}
		//		else
		//		{
		//			MessageBox.Show("Error opening database.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			AddARPaymentToList = -1;
		//			return AddARPaymentToList;
		//		}
		//		// -----------------------------------------------------------------------------------
		//		boolNoLoop = true;
		//		while (!(dblTot <= 0 || rsTemp.EndOfFile()))
		//		{
		//			boolNoLoop = false;
		//			for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//			{
		//				// this finds a place in the array
		//				if (Statics.ARPaymentArray[intCT].BillKey == 0)
		//				{
		//					// to store the new payment record
		//					break;
		//				}
		//			}
		//			//FC:FINAL:DSE:#257 Use custom constructor to initialize fields from the structure
		//			Statics.ARPaymentArray[intCT] = new ARPayment(0);
		//			if (rsTemp.EndOfFile() != true)
		//			{
		//				lngBill = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//			}
		//			// get the next year and its total due values
		//			Statics.ARPaymentArray[intCT].BillKey = lngBill;
		//			Statics.ARPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber"));
		//			curARChargedNotSavedInt = CalculateARChargedInterestNotYetSaved(ref lngBill);
		//			// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//			dblPrinNeed = modGlobal.Round(FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("PrinOwed") - rsTemp.Get_Fields("PrinPaid")), 2);
		//			// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//			dblTaxNeed = modGlobal.Round(FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxOwed") - rsTemp.Get_Fields("TaxPaid")), 2);
		//			// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//			dblIntNeed = modGlobal.Round((FCConvert.ToDouble(rsTemp.Get_Fields("IntAdded")) * -1) - FCConvert.ToDouble(rsTemp.Get_Fields("IntPaid")) - Statics.dblARCurrentInt[rsTemp.Get_Fields_Int32("ID")], 2) - FCConvert.ToDouble(curARChargedNotSavedInt);
		//			for (counter = 1; counter <= intCT - 1; counter++)
		//			{
		//				if (Statics.ARPaymentArray[counter].BillKey == lngBill)
		//				{
		//					dblPrinNeed -= FCConvert.ToDouble(Statics.ARPaymentArray[counter].Principal);
		//					dblTaxNeed -= FCConvert.ToDouble(Statics.ARPaymentArray[counter].Tax);
		//					dblIntNeed -= FCConvert.ToDouble(Statics.ARPaymentArray[counter].CurrentInterest);
		//				}
		//			}
		//			if (dblPrinNeed + dblTaxNeed + dblIntNeed > 0)
		//			{
		//				// if it is actually a payment then
		//				if (intCT < MAX_ARPAYMENTS)
		//				{
		//					dblTempP = 0;
		//					dblTempT = 0;
		//					dblTempI = 0;
		//					// these are the money fields
		//					AdjustARPaymentAmounts(ref dblPrin, ref dblTax, ref dblTot, ref dblPrinNeed, ref dblTaxNeed, ref dblIntNeed, ref dblTempP, ref dblTempT, ref dblTempI);
		//					Statics.ARPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTempP);
		//					Statics.ARPaymentArray[intCT].Tax = FCConvert.ToDecimal(dblTempT);
		//					Statics.ARPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//					dblTot = modGlobal.Round(dblTot - dblTempP - dblTempT - dblTempI, 2);
		//					// this adds it to the array of payments
		//					Statics.ARPaymentArray[intCT].ActualAccountNumber = Statics.lngCurrentCustomerIDAR;
		//					Statics.ARPaymentArray[intCT].AccountKey = GetAccountKeyFromAccountNumber(Statics.lngCurrentCustomerIDAR);
		//					Statics.ARPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//					Statics.ARPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//					Statics.ARPaymentArray[intCT].InvoiceNumber = FCConvert.ToString(rsTemp.Get_Fields_String("InvoiceNumber"));
		//					Statics.ARPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber"));
		//					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
		//					Statics.ARPaymentArray[intCT].BillType = FCConvert.ToInt32(rsTemp.Get_Fields("BillType"));
		//					Statics.ARPaymentArray[intCT].Control1 = FCConvert.ToString(rsTemp.Get_Fields_String("Control1"));
		//					Statics.ARPaymentArray[intCT].Control2 = FCConvert.ToString(rsTemp.Get_Fields_String("Control2"));
		//					Statics.ARPaymentArray[intCT].Control3 = FCConvert.ToString(rsTemp.Get_Fields_String("Control3"));
		//					Statics.ARPaymentArray[intCT].Reference = FCConvert.ToString(rsTemp.Get_Fields_String("Reference"));
		//					Statics.ARPaymentArray[intCT].CashDrawer = frmARCLStatus.InstancePtr.txtCD.Text;
		//					if (Statics.ARPaymentArray[intCT].CashDrawer == "N")
		//					{
		//						// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//						Statics.ARPaymentArray[intCT].BudgetaryAccountNumber = frmARCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//						// frmARCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//					}
		//					else
		//					{
		//						Statics.ARPaymentArray[intCT].BudgetaryAccountNumber = "";
		//					}
		//					Statics.ARPaymentArray[intCT].Code = strType;
		//					Statics.ARPaymentArray[intCT].Comments = frmARCLStatus.InstancePtr.txtComments.Text;
		//					if (frmARCLStatus.InstancePtr.txtReference.Text == "REVERS")
		//					{
		//						Statics.ARPaymentArray[intCT].EffectiveInterestDate = GetLastARInterestDate(FCConvert.ToInt32(frmARCLStatus.InstancePtr.lblAccount.Text), Statics.ARPaymentArray[intCT].BillKey, Statics.AREffectiveDate, 0);
		//					}
		//					else
		//					{
		//						Statics.ARPaymentArray[intCT].EffectiveInterestDate = Statics.AREffectiveDate;
		//					}
		//					Statics.ARPaymentArray[intCT].GeneralLedger = frmARCLStatus.InstancePtr.txtCash.Text;
		//					Statics.ARPaymentArray[intCT].ReceiptNumber = "P";
		//					if (frmARCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//					{
		//						Statics.ARPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//					}
		//					else
		//					{
		//						Statics.ARPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmARCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//					}
		//					if (boolCalcInterest && lngReversalKey != 0)
		//					{
		//						// this is where the CHGINT Number needs to be put in the
		//						// lngReversalKey is the payment ID of the payment being reversed
		//						// intCT is the array index of the reversal payment
		//						// AddCHGINTToList frmRECLStatus.vsPayments.rows, lngReversalKey, True, intCT
		//					}
		//					else if (modGlobal.Round(Statics.dblARCurrentInt[rsTemp.Get_Fields_Int32("ID")], 2) + FCConvert.ToDouble(curARChargedNotSavedInt) != 0)
		//					{
		//						// AddCHGINTToList frmRECLStatus.vsPayments.rows, , True
		//						// this will create a CHGINT line for normal payments
		//						Statics.ARPaymentArray[intCT].CHGINTNumber = CreateARCHGINTLine(FCConvert.ToInt16(intCT), ref Statics.ARPaymentArray[intCT].EffectiveInterestDate);
		//					}
		//					Statics.ARPaymentArray[intCT].Reference = Strings.Trim(frmARCLStatus.InstancePtr.txtReference.Text + " ");
		//					Statics.ARPaymentArray[intCT].ReversalID = -1;
		//					frmARCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT, Statics.ARPaymentArray[intCT].InvoiceNumber);
		//					// actually fills the number into the payment grid
		//				}
		//				else
		//				{
		//					MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//				}
		//			}
		//			rsTemp.MoveNext();
		//		}
		//		if (boolNoLoop)
		//		{
		//			for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//			{
		//				// this finds a place in the array
		//				if (Statics.ARPaymentArray[intCT].BillKey == 0)
		//				{
		//					// to store the new payment record
		//					executeSTARTNOLOOP = true;
		//					goto STARTNOLOOP;
		//				}
		//			}
		//			// frmarCLStatus.FillPaymentGrid cRow, intCT, lngBill
		//		}
		//		if (rsTemp.EndOfFile() && modGlobal.Round(dblTot, 1) > 0)
		//		{
		//			// have to put the excess money in the most current year
		//			int intLastYear;
		//			if (blnExcessPayment)
		//			{
		//				intLastYear = CreateExcessARPaymentRecord_6(dblTot, true);
		//				MessageBox.Show("Amount paid in excess of total bill amount: " + Strings.Format(dblTot, "$#,##0.00") + "\r\n" + "If this is incorrect please delete the payments manually.", "Excess of Bill Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//			}
		//		}
		//		frmARCLStatus.InstancePtr.txtPrincipal.Text = "0.00";
		//		frmARCLStatus.InstancePtr.txtTax.Text = "0.00";
		//		frmARCLStatus.InstancePtr.txtInterest.Text = "0.00";
		//		// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//		// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//		// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//	}
		//	else
		//	{
		//		// if a bill is choosen
		//		// kk03262015 trocrs-21  Add Val() below to stop Type mismatch
		//		if (strType == "Y" || Strings.InStr(1, frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString(), "*", CompareConstants.vbBinaryCompare) != 0 || Conversion.Val(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString()) == 0)
		//		{
		//			// if this is a prepayment
		//			lngBill = PrePayARBill();
		//			// use the correct year (next year)
		//			strType = Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[frmARCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1);
		//		}
		//		else
		//		{
		//			// else use the bill choosen
		//			if (frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex >= 0)
		//			{
		//				lngBill = GetBKFromInvoice_2(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString());
		//			}
		//			else
		//			{
		//				MessageBox.Show("Please select a bill or use the prepayment (Y) option.", "Error Checking Bill.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//				return AddARPaymentToList;
		//			}
		//		}
		//		lngStarterRow = 1;
		//		executeSTARTNOLOOP = true;
		//		goto STARTNOLOOP;
		//	}
		//	STARTNOLOOP:
		//	;
		//	//FC:FINAL:DSE:#512 code shoule execute only when needed
		//	if (executeSTARTNOLOOP)
		//	{
		//		lngTesterRow = frmARCLStatus.InstancePtr.vsPayments.FindRow(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString(), lngStarterRow, frmARCLStatus.InstancePtr.lngPayGridColInvoice);
		//		if (lngTesterRow > 0)
		//		{
		//			if (Conversion.Val(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(lngTesterRow, frmARCLStatus.InstancePtr.lngPayGridColInvoice)) == Conversion.Val(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString()))
		//			{
		//				// another payment is found from this bill
		//				MessageBox.Show("Payments already exists from this bill.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//				AddARPaymentToList = -1;
		//				return AddARPaymentToList;
		//			}
		//			else
		//			{
		//				lngStarterRow = lngTesterRow + 1;
		//				goto STARTNOLOOP;
		//			}
		//		}
		//		if (rsTemp.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(GetRKFromInvoice_2(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString())), modExtraModules.strARDatabase))
		//		{
		//			strTemp = "SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " AND ID = " + FCConvert.ToString(lngBill);
		//		}
		//		else
		//		{
		//			MessageBox.Show("Error finding rate record " + FCConvert.ToString(lngBill) + ".", "ERROR Adding Payment To List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			AddARPaymentToList = -1;
		//			return AddARPaymentToList;
		//		}
		//		rsTemp.OpenRecordset(strTemp, modExtraModules.strARDatabase);
		//		if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//		{
		//		}
		//		else
		//		{
		//			MessageBox.Show("Error opening TWAR0000.VB1.", "ERROR Adding Payment To List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			AddARPaymentToList = -1;
		//			return AddARPaymentToList;
		//		}
		//		for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//		{
		//			// this finds a place in the array
		//			if (Statics.ARPaymentArray[intCT].BillKey == 0)
		//			{
		//				// to store the new payment record
		//				break;
		//			}
		//		}
		//		dblPrinNeed = 0;
		//		dblTaxNeed = 0;
		//		dblIntNeed = 0;
		//		//FC:FINAL:DSE:#257 Use custom constructor to initialize fields from the structure
		//		Statics.ARPaymentArray[intCT] = new ARPayment(0);
		//		// get the next year and its total due values
		//		Statics.ARPaymentArray[intCT].BillKey = lngBill;
		//		Statics.ARPaymentArray[intCT].InvoiceNumber = frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString().Replace("*", "");
		//		Statics.ARPaymentArray[intCT].BillNumber = GetRKFromInvoice_2(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString().Replace("*", ""));
		//		curARChargedNotSavedInt = CalculateARChargedInterestNotYetSaved(ref lngBill);
		//		// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//		dblPrinNeed = modGlobal.Round(FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("PrinOwed") - rsTemp.Get_Fields("PrinPaid")), 2);
		//		// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//		dblTaxNeed = modGlobal.Round(FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxOwed") - rsTemp.Get_Fields("TaxPaid")), 2);
		//		// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//		// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//		dblIntNeed = modGlobal.Round((FCConvert.ToDouble(rsTemp.Get_Fields("IntAdded")) * -1) - FCConvert.ToDouble(rsTemp.Get_Fields("IntPaid")) - Statics.dblARCurrentInt[lngBill], 2) - FCConvert.ToDouble(curARChargedNotSavedInt);
		//		if ((strType == "P" || !(strType == "C" && dblInt < 0)) && !boolSkipRecalc)
		//		{
		//			if (dblIntNeed <= 0 && dblTaxNeed <= 0)
		//			{
		//				dblPrin = dblTax + dblInt;
		//				dblTax = 0;
		//				dblInt = 0;
		//			}
		//			else
		//			{
		//				AdjustARPaymentAmounts(ref dblPrin, ref dblTax, ref dblInt, ref dblPrinNeed, ref dblTaxNeed, ref dblIntNeed, ref dblTempP, ref dblTempT, ref dblTempI);
		//				dblTot = modGlobal.Round(dblTot - dblTempP - dblTempT - dblTempI, 2);
		//			}
		//			if (modGlobal.Round(dblTot, 2) != 0)
		//			{
		//				// just set all of the excess amount to the principal of this bill
		//				dblTempP += dblTot;
		//				dblTot = 0;
		//			}
		//		}
		//		else
		//		{
		//			dblTempP = dblPrin;
		//			dblTempT = dblTax;
		//			dblTempI = dblInt;
		//		}
		//		if (intCT < MAX_ARPAYMENTS)
		//		{
		//			// this adds it to the array of payments
		//			Statics.ARPaymentArray[intCT].ActualAccountNumber = Statics.lngCurrentCustomerIDAR;
		//			Statics.ARPaymentArray[intCT].AccountKey = GetAccountKeyFromAccountNumber(Statics.lngCurrentCustomerIDAR);
		//			Statics.ARPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//			Statics.ARPaymentArray[intCT].BillKey = lngBill;
		//			Statics.ARPaymentArray[intCT].CashDrawer = frmARCLStatus.InstancePtr.txtCD.Text;
		//			Statics.ARPaymentArray[intCT].BillType = GetBTFromInvoice_2(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString().Replace("*", ""));
		//			if (Statics.ARPaymentArray[intCT].CashDrawer == "N")
		//			{
		//				// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//				Statics.ARPaymentArray[intCT].BudgetaryAccountNumber = frmARCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//				// frmARCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//			}
		//			else
		//			{
		//				// do nothing
		//			}
		//			Statics.ARPaymentArray[intCT].Code = strType;
		//			Statics.ARPaymentArray[intCT].Comments = frmARCLStatus.InstancePtr.txtComments.Text;
		//			if (frmARCLStatus.InstancePtr.txtReference.Text == "REVERS")
		//			{
		//				Statics.ARPaymentArray[intCT].EffectiveInterestDate = GetLastARInterestDate(FCConvert.ToInt32(frmARCLStatus.InstancePtr.lblAccount.Text), Statics.ARPaymentArray[intCT].BillKey, Statics.AREffectiveDate, 0);
		//			}
		//			else
		//			{
		//				Statics.ARPaymentArray[intCT].EffectiveInterestDate = Statics.AREffectiveDate;
		//			}
		//			Statics.ARPaymentArray[intCT].GeneralLedger = frmARCLStatus.InstancePtr.txtCash.Text;
		//			Statics.ARPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTempP);
		//			Statics.ARPaymentArray[intCT].Tax = FCConvert.ToDecimal(dblTempT);
		//			Statics.ARPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//			Statics.ARPaymentArray[intCT].ReceiptNumber = "P";
		//			if (frmARCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//			{
		//				Statics.ARPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//			}
		//			else
		//			{
		//				Statics.ARPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmARCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//			}
		//			if (boolCalcInterest && lngReversalKey != 0)
		//			{
		//				// this is where the CHGINT Number needs to be put in the
		//				// lngReversalKey is the payment ID of the payment being reversed
		//				// intCT is the array index of the reversal payment
		//				// AddCHGINTToList frmRECLStatus.vsPayments.rows, lngReversalKey, True, intCT
		//			}
		//			else if (modGlobal.Round(Statics.dblARCurrentInt[rsTemp.Get_Fields_Int32("ID")], 2) + FCConvert.ToDouble(curARChargedNotSavedInt) != 0)
		//			{
		//				// AddCHGINTToList frmRECLStatus.vsPayments.rows, , True
		//				// this will create a CHGINT line for normal payments
		//				Statics.ARPaymentArray[intCT].CHGINTNumber = CreateARCHGINTLine(FCConvert.ToInt16(intCT), ref Statics.ARPaymentArray[intCT].EffectiveInterestDate);
		//			}
		//			Statics.ARPaymentArray[intCT].ReverseKey = lngReversalKey;
		//			Statics.ARPaymentArray[intCT].Reference = Strings.Trim(frmARCLStatus.InstancePtr.txtReference.Text + " ");
		//			Statics.ARPaymentArray[intCT].ReversalID = -1;
		//			// .Teller =
		//			// .TransNumber =
		//			// .PaidBy =
		//			// .CHGINTNumber =
		//			frmARCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT, Statics.ARPaymentArray[intCT].InvoiceNumber);
		//			frmARCLStatus.InstancePtr.txtPrincipal.Text = "0.00";
		//			frmARCLStatus.InstancePtr.txtInterest.Text = "0.00";
		//			frmARCLStatus.InstancePtr.txtTax.Text = "0.00";
		//		}
		//		else
		//		{
		//			MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//		}
		//	}
		//	AddARPaymentToList = FCConvert.ToInt16(intCT);
		//	ARBillPending_6(lngBill, 1);
		//	return AddARPaymentToList;
		//}
		//// vbPorter upgrade warning: intPArrInd As short	OnWriteFCConvert.ToInt32(
		//public static void AddARCHGINTToList_15(int cRow, bool boolRev = true, short intPArrInd = -1)
		//{
		//	AddARCHGINTToList(cRow, boolRev, false, intPArrInd);
		//}

		//public static void AddARCHGINTToList(int cRow, bool boolRev = true, bool boolCHGINT = false, short intPArrInd = -1)
		//{
		//	// this will add the CHGINT line to the payment list
		//	// in order to return the values to the state before the last payment happened
		//	int intCT;
		//	int lngBill;
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	clsDRWrapper rsCHG = new clsDRWrapper();
		//	string strTemp;
		//	string PayCode = "";
		//	int CHGINTNumber;
		//	int lngRevPayKey;
		//	string strType;
		//	// this is the type of payment that is being processed
		//	strType = Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[frmARCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1);
		//	lngBill = GetBKFromInvoice_2(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString());
		//	// this makes sure that thre is a bill created for this year
		//	strTemp = "SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ID = " + FCConvert.ToString(lngBill);
		//	rsTemp.OpenRecordset(strTemp, modExtraModules.strARDatabase);
		//	if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//	{
		//	}
		//	else
		//	{
		//		MessageBox.Show("Error opening database, no bill record.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		return;
		//	}
		//	for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//	{
		//		// this finds a place in the array
		//		if (Statics.ARPaymentArray[intCT].BillKey == 0)
		//		{
		//			// to store the new payment record
		//			break;
		//		}
		//	}
		//	// get the ID for the CHGINT payment that corresponds to this payment or reversal
		//	CHGINTNumber = Math.Abs(Statics.lngARCHGINT);
		//	// rsTemp.Get_Fields_Int32("CHGINTNumber")
		//	if (CHGINTNumber != 0 || boolCHGINT)
		//	{
		//		if (intCT < MAX_ARPAYMENTS)
		//		{
		//			//FC:FINAL:DSE:#257 Use custom constructor to initialize fields from the structure
		//			Statics.ARPaymentArray[intCT] = new ARPayment(0);
		//			// this adds it to the array of payments
		//			Statics.ARPaymentArray[intCT].ActualAccountNumber = Statics.lngCurrentCustomerIDAR;
		//			Statics.ARPaymentArray[intCT].AccountKey = GetAccountKeyFromAccountNumber(Statics.lngCurrentCustomerIDAR);
		//			Statics.ARPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//			Statics.ARPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber"));
		//			Statics.ARPaymentArray[intCT].InvoiceNumber = FCConvert.ToString(rsTemp.Get_Fields_String("InvoiceNumber"));
		//			Statics.ARPaymentArray[intCT].BillKey = lngBill;
		//			// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
		//			Statics.ARPaymentArray[intCT].BillType = FCConvert.ToInt32(rsTemp.Get_Fields("BillType"));
		//			Statics.ARPaymentArray[intCT].CashDrawer = frmARCLStatus.InstancePtr.txtCD.Text;
		//			if (Statics.ARPaymentArray[intCT].CashDrawer == "N")
		//			{
		//				// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//				Statics.ARPaymentArray[intCT].BudgetaryAccountNumber = frmARCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//				// frmARCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//			}
		//			else
		//			{
		//			}
		//			Statics.ARPaymentArray[intCT].Code = "I";
		//			// Left$(frmarCLStatus.cmbCode.List(frmarCLStatus.cmbCode.ListIndex), 1)
		//			Statics.ARPaymentArray[intCT].Comments = frmARCLStatus.InstancePtr.txtComments.Text;
		//			if (boolRev)
		//			{
		//				Statics.ARPaymentArray[intCT].EffectiveInterestDate = GetLastARInterestDate(FCConvert.ToInt32(frmARCLStatus.InstancePtr.lblAccount.Text), lngBill, Statics.AREffectiveDate, 0);
		//			}
		//			else
		//			{
		//				Statics.ARPaymentArray[intCT].EffectiveInterestDate = Statics.AREffectiveDate;
		//			}
		//			Statics.ARPaymentArray[intCT].GeneralLedger = frmARCLStatus.InstancePtr.txtCash.Text;
		//			Statics.ARPaymentArray[intCT].Principal = 0;
		//			Statics.ARPaymentArray[intCT].ReceiptNumber = "P";
		//			if (frmARCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//			{
		//				Statics.ARPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//			}
		//			else
		//			{
		//				Statics.ARPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmARCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//			}
		//			rsCHG.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountPayments + ") AS Table1 WHERE ID = " + FCConvert.ToString(CHGINTNumber), modExtraModules.strARDatabase);
		//			if (rsCHG.EndOfFile() != true && rsCHG.BeginningOfFile() != true)
		//			{
		//				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//				Statics.ARPaymentArray[intCT].CurrentInterest = (FCConvert.ToDecimal(rsCHG.Get_Fields("Interest")) * -1);
		//			}
		//			else
		//			{
		//				Statics.ARPaymentArray[intCT].CurrentInterest = 0;
		//			}
		//			if (Statics.lngARCHGINT > 0)
		//			{
		//				Statics.ARPaymentArray[intCT].CHGINTNumber = Statics.lngARCHGINT;
		//				Statics.ARPaymentArray[intCT].ReversalID = -1;
		//			}
		//			else
		//			{
		//				Statics.ARPaymentArray[intCT].CHGINTNumber = 0;
		//				Statics.ARPaymentArray[intCT].ReversalID = intPArrInd;
		//				if (rsCHG.EndOfFile() != true)
		//				{
		//					// if this is a supplemental payment then this will be empty
		//					Statics.ARPaymentArray[intCT].CHGINTDate = (DateTime)rsCHG.Get_Fields_DateTime("CHGINTDate");
		//				}
		//				else
		//				{
		//					Statics.ARPaymentArray[intCT].CHGINTDate = Statics.AREffectiveDate;
		//				}
		//			}
		//			if (boolRev)
		//			{
		//				if (Statics.ARPaymentArray[intCT].CurrentInterest > 0)
		//				{
		//					Statics.ARPaymentArray[intCT].CurrentInterest = Statics.ARPaymentArray[intCT].CurrentInterest;
		//				}
		//				else
		//				{
		//					Statics.ARPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(FindARCHGINTValue(lngBill) * -1);
		//				}
		//			}
		//			else if (boolCHGINT)
		//			{
		//				Statics.ARPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(Statics.dblARCurrentInt[lngBill] * -1);
		//			}
		//			else
		//			{
		//				Statics.ARPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(Statics.dblARCurrentInt[lngBill]);
		//			}
		//			if (Statics.ARPaymentArray[intCT].CurrentInterest > 0 && !boolRev)
		//			{
		//				Statics.ARPaymentArray[intCT].Reference = "CHGINT";
		//				// Trim$(frmarCLStatus.txtReference & " ")
		//			}
		//			else if (Statics.ARPaymentArray[intCT].CurrentInterest > 0 && boolRev)
		//			{
		//				// MAL@20071030
		//				Statics.ARPaymentArray[intCT].Reference = "Interest";
		//			}
		//			else
		//			{
		//				Statics.ARPaymentArray[intCT].Reference = "EARNINT";
		//			}
		//			Statics.ARPaymentArray[intCT].Principal = 0;
		//			// this adds a row to the payment grid and fills the payment information into it
		//			frmARCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT * -1, Statics.ARPaymentArray[intCT].InvoiceNumber);
		//		}
		//		else
		//		{
		//			// maximum payments can be set
		//			MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//		}
		//	}
		//	// reset the values
		//	Statics.lngARCHGINT = 0;
		//}

		//public static void ResetARComboBoxes()
		//{
		//	// this is to reset the combo boxes on the payment frame
		//	int lngBill;
		//	int intCT;
		//	frmARCLStatus.InstancePtr.txtCD.Text = "Y";
		//	frmARCLStatus.InstancePtr.txtCash.Text = "Y";
		//	// this will find the payment option in the list
		//	for (intCT = 0; intCT <= frmARCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
		//	{
		//		if ("P" == Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//		{
		//			frmARCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//			break;
		//		}
		//	}
		//	// this finds the last year that is non zero
		//	lngBill = GetBKFromInvoice_2(FillARInvoiceNumber(ref Statics.lngCurrentCustomerIDAR));
		//	// For intCT = 0 To .cmbBillNumber.ListCount
		//	// If lngBill = GetBKFromInvoice(.cmbBillNumber.List(intCT)) Then
		//	// .cmbBillNumber.ListIndex = intCT
		//	// Exit For
		//	// End If
		//	// Next
		//	// If .cmbBillNumber.ListIndex < 0 Then
		//	frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = frmARCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1;
		//}

		//public static void FillARPaymentComboBoxes()
		//{
		//	FillARCodeCombo();
		//}

		//private static void FillARCodeCombo()
		//{
		//	frmARCLStatus.InstancePtr.cmbCode.Clear();
		//	frmARCLStatus.InstancePtr.cmbCode.AddItem("Y   - PrePayment");
		//	// .AddItem "A   - Abatement"
		//	frmARCLStatus.InstancePtr.cmbCode.AddItem("C   - Correction");
		//	// .AddItem "D   - Discount"
		//	frmARCLStatus.InstancePtr.cmbCode.AddItem("P   - Regular Payment");
		//	// .AddItem "R   - Refunded Abatement"
		//	// .AddItem "S   - Supplemental"
		//	// .AddItem "U   - Tax Club Payment"
		//}

		//public static void ResetARPaymentFrame()
		//{
		//	ResetARComboBoxes();
		//	// DJW 5/29/2008 Changed Date to AREffectiveDate
		//	frmARCLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(Statics.AREffectiveDate, "MM/dd/yyyy");
		//	frmARCLStatus.InstancePtr.txtReference.Text = "";
		//	frmARCLStatus.InstancePtr.txtPrincipal.Text = Strings.Format(0, "#,##0.00");
		//	frmARCLStatus.InstancePtr.txtInterest.Text = Strings.Format(0, "#,##0.00");
		//	frmARCLStatus.InstancePtr.txtComments.Text = "";
		//}

		//public static Decimal DetermineARPrincipal(ref clsDRWrapper rsCollection)
		//{
		//	Decimal DetermineARPrincipal = 0;
		//	Decimal curPDue;
		//	curPDue = rsCollection.Get_Fields_Decimal("PrinOwed");
		//	// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//	curPDue -= rsCollection.Get_Fields("PrinPaid");
		//	DetermineARPrincipal = curPDue;
		//	return DetermineARPrincipal;
		//}

		//public static Decimal DetermineARTax(ref clsDRWrapper rsCollection)
		//{
		//	Decimal DetermineARTax = 0;
		//	Decimal curPDue;
		//	curPDue = rsCollection.Get_Fields_Decimal("TaxOwed");
		//	// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//	curPDue -= rsCollection.Get_Fields("TaxPaid");
		//	DetermineARTax = curPDue;
		//	return DetermineARTax;
		//}

		//public static Decimal DetermineARInterest(ref clsDRWrapper rsCollection)
		//{
		//	Decimal DetermineARInterest = 0;
		//	Decimal curPDue;
		//	// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//	curPDue = rsCollection.Get_Fields("IntAdded");
		//	// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//	curPDue -= rsCollection.Get_Fields("IntPaid");
		//	DetermineARInterest = curPDue;
		//	return DetermineARInterest;
		//}

		//private static string FindDefaultARBill(ref int Acct)
		//{
		//	string FindDefaultARBill = "";
		//	double dblTemp;
		//	string strSQL;
		//	clsDRWrapper rsDefYear = new clsDRWrapper();
		//	int intCount;
		//	dblTemp = 0;
		//	strSQL = "SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(Acct) + " ORDER BY CASE WHEN BillStatus = 'D' THEN 2 ELSE 1 END , BillDate , InvoiceNumber , BillNumber ";
		//	rsDefYear.OpenRecordset(strSQL, modExtraModules.strARDatabase);
		//	if (rsDefYear.EndOfFile() != true && rsDefYear.BeginningOfFile() != true)
		//	{
		//		do
		//		{
		//			FindDefaultARBill = FCConvert.ToString(rsDefYear.Get_Fields_String("InvoiceNumber"));
		//			// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//			dblTemp = modGlobal.Round(FCConvert.ToDouble(rsDefYear.Get_Fields_Decimal("PrinOwed") + rsDefYear.Get_Fields_Decimal("TaxOwed") - rsDefYear.Get_Fields("IntAdded")), 2);
		//			// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//			dblTemp -= modGlobal.Round(FCConvert.ToDouble(rsDefYear.Get_Fields("PrinPaid") + rsDefYear.Get_Fields("TaxPaid") + rsDefYear.Get_Fields("IntPaid")), 2);
		//			if (dblTemp != 0)
		//			{
		//				// If rsDefYear.Get_Fields("InvoiceNumber") < FindDefaultARBill Then
		//				// FindDefaultARBill = rsDefYear.Get_Fields("InvoiceNumber")
		//				// End If
		//				return FindDefaultARBill;
		//			}
		//			rsDefYear.MoveNext();
		//		}
		//		while (!(rsDefYear.EndOfFile() == true));
		//		rsDefYear.MoveLast();
		//		FindDefaultARBill = FCConvert.ToString(rsDefYear.Get_Fields_String("InvoiceNumber"));
		//	}
		//	return FindDefaultARBill;
		//}

		//private static string FillARInvoiceNumber(ref int Acct)
		//{
		//	string FillARInvoiceNumber = "";
		//	// this will find all of the bills that are needed to be shown for this account by
		//	// checking the bill records and adding two for a prepay year and an "Auto" selection
		//	double dblTemp;
		//	string strSQL = "";
		//	clsDRWrapper rsDefYear = new clsDRWrapper();
		//	int intCount;
		//	bool boolNoRateKeyBill;
		//	dblTemp = 0;
		//	frmARCLStatus.InstancePtr.cmbBillNumber.Clear();
		//	// strSQL = "SELECT * FROM (" & strARCurrentAccountBills & ") AS Info LEFT JOIN RateKeys ON Info.BillNumber = RateKeys.ID ORDER BY BillNumber desc, Start"
		//	if (frmARCLStatus.InstancePtr.chkShowAll.CheckState == Wisej.Web.CheckState.Checked)
		//	{
		//		strSQL = "SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp ORDER BY InvoiceNumber desc";
		//	}
		//	else
		//	{
		//		strSQL = "SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE (IsNull(PrinOwed, 0) - IsNull(IntAdded, 0) + IsNull(TaxOwed, 0) - IsNull(PrinPaid, 0) - IsNull(TaxPaid, 0) - IsNull(IntPaid, 0)) <> 0 ORDER BY InvoiceNumber desc";
		//	}
		//	rsDefYear.OpenRecordset(strSQL, modExtraModules.strARDatabase);
		//	if (rsDefYear.BeginningOfFile() != true && rsDefYear.EndOfFile() != true)
		//	{
		//		FillARInvoiceNumber = FCConvert.ToString(rsDefYear.Get_Fields_String("InvoiceNumber"));
		//		// find the highest number bill
		//		do
		//		{
		//			// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//			dblTemp = FCConvert.ToDouble(rsDefYear.Get_Fields_Decimal("PrinOwed") - rsDefYear.Get_Fields("PrinPaid") + rsDefYear.Get_Fields_Decimal("TaxOwed") - rsDefYear.Get_Fields("TaxPaid") + rsDefYear.Get_Fields("IntAdded") - rsDefYear.Get_Fields("IntPaid"));
		//			if (dblTemp != 0)
		//			{
		//				if (Conversion.Val(rsDefYear.Get_Fields_String("InvoiceNumber")) < Conversion.Val(FillARInvoiceNumber))
		//				{
		//					FillARInvoiceNumber = FCConvert.ToString(rsDefYear.Get_Fields_String("InvoiceNumber"));
		//				}
		//			}
		//			if (fecherFoundation.FCUtils.IsNull(rsDefYear.Get_Fields_String("InvoiceNumber")) == false)
		//			{
		//				if (Conversion.Val(rsDefYear.Get_Fields_String("InvoiceNumber")) <= Conversion.Val(FillARInvoiceNumber))
		//				{
		//					frmARCLStatus.InstancePtr.cmbBillNumber.AddItem(rsDefYear.Get_Fields_String("InvoiceNumber"));
		//				}
		//			}
		//			rsDefYear.MoveNext();
		//		}
		//		while (!rsDefYear.EndOfFile());
		//		if (rsDefYear.EndOfFile())
		//			rsDefYear.MoveFirst();
		//		while (!rsDefYear.EndOfFile())
		//		{
		//			if (rsDefYear.Get_Fields_Decimal("PrinOwed") > 0)
		//			{
		//				break;
		//			}
		//			rsDefYear.MoveNext();
		//		}
		//	}
		//	boolNoRateKeyBill = false;
		//	for (intCount = 0; intCount <= frmARCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCount++)
		//	{
		//		if (FCConvert.ToDouble(frmARCLStatus.InstancePtr.cmbBillNumber.Items[intCount].ToString()) == 0)
		//		{
		//			boolNoRateKeyBill = true;
		//		}
		//	}
		//	if (!boolNoRateKeyBill)
		//	{
		//		// this will add a blank bill option to select and it will create a new blank bill for prepays
		//		frmARCLStatus.InstancePtr.cmbBillNumber.AddItem("0*");
		//	}
		//	// add the Auto Option
		//	frmARCLStatus.InstancePtr.cmbBillNumber.AddItem("Auto");
		//	return FillARInvoiceNumber;
		//}

		//public static void CreateAROppositionLine(int Row, bool boolReversal = false)
		//{
		//	int Parent = 0;
		//	int intCT;
		//	int lngBill = 0;
		//	string strCode = "";
		//	string strPaymentCode = "";
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	clsDRWrapper rsTimeChk = new clsDRWrapper();
		//	int lngPaymentNumber = 0;
		//	bool boolGrandTotal = false;
		//	bool boolPending;
		//	bool boolUsePending;
		//	// vbPorter upgrade warning: dblP As double	OnWrite(double, short, string)
		//	double dblP = 0;
		//	// vbPorter upgrade warning: dblTax As double	OnWrite(double, short, string)
		//	double dblTax = 0;
		//	// vbPorter upgrade warning: dblInt As double	OnWrite(Decimal, double, string)
		//	double dblInt = 0;
		//	bool boolSavePayment = false;
		//	int lngRows = 0;
		//	int lngChgInterest = 0;
		//	DateTime dtPassDate = DateTime.FromOADate(0);
		//	DateTime dtNewIntDate;
		//	string strWS = "";
		//	int lngBillKey = 0;
		//	Statics.lngARCHGINT = 0;
		//	if (Row > 0)
		//	{
		//		boolGrandTotal = FCConvert.CBool(frmARCLStatus.InstancePtr.GRID.RowOutlineLevel(Row) == 0 || (frmARCLStatus.InstancePtr.GRID.RowOutlineLevel(Row) == 2 && Strings.Trim(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColRef)) == "Total"));
		//		if (boolGrandTotal)
		//		{
		//			Parent = Row;
		//			lngRows = frmARCLStatus.InstancePtr.GRID.Rows - 1;
		//			if (Row == lngRows)
		//			{
		//				// this will set the year to auto
		//				for (intCT = 0; intCT <= frmARCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//				{
		//					if ("Auto" == Strings.Trim(frmARCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()).Replace("*", ""))
		//					{
		//						frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//			}
		//			else
		//			{
		//				// this will set the year to the correct year
		//				for (intCT = 0; intCT <= frmARCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//				{
		//					if (Strings.Trim(frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.LastParentRow(Row), 1)) == Strings.Trim(frmARCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()).Replace("*", ""))
		//					{
		//						frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//			}
		//			// collect info from the payment clicked on
		//			strCode = "=";
		//			strPaymentCode = "P";
		//		}
		//		else
		//		{
		//			Parent = frmARCLStatus.InstancePtr.LastParentRow(Row);
		//			// this finds the last parent row and stores it into Parent
		//			lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(frmARCLStatus.InstancePtr.GRID.TextMatrix(Parent, frmARCLStatus.InstancePtr.lngGridColBillKey))));
		//			// this will set the year
		//			for (intCT = 0; intCT <= frmARCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//			{
		//				if (Conversion.Val(frmARCLStatus.InstancePtr.GRID.TextMatrix(Parent, frmARCLStatus.InstancePtr.lngGRIDColInvoiceNumber)) == Conversion.Val(Strings.Trim(frmARCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()).Replace("*", "")))
		//				{
		//					frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
		//					break;
		//				}
		//			}
		//			// collect info from the payment clicked on
		//			strCode = frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColLineCode);
		//			strPaymentCode = frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColPaymentCode);
		//		}
		//		// default values
		//		// DJW 12/2/2008 Set codes to proper values
		//		if (!boolReversal)
		//		{
		//			frmARCLStatus.InstancePtr.txtCD.Text = "Y";
		//			frmARCLStatus.InstancePtr.txtCash.Text = "Y";
		//		}
		//		else
		//		{
		//			// Find the reversal payment and copy the CD and AC settings from the payment being reversed
		//			rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColPaymentKey), modExtraModules.strARDatabase);
		//			if (!rsTemp.EndOfFile())
		//			{
		//				if (FCConvert.ToString(rsTemp.Get_Fields_String("CashDrawer")) == "Y" && Conversion.Val(rsTemp.Get_Fields_Int32("DailyCloseOut")) == 0)
		//				{
		//					// if the payment is Y Y and has not been closed out then
		//					frmARCLStatus.InstancePtr.txtCD.Text = "Y";
		//					frmARCLStatus.InstancePtr.txtCash.Text = "Y";
		//				}
		//				else
		//				{
		//					frmARCLStatus.InstancePtr.txtCD.Text = "N";
		//					if (FCConvert.ToString(rsTemp.Get_Fields_String("GeneralLedger")) == "N")
		//					{
		//						frmARCLStatus.InstancePtr.txtCash.Text = "N";
		//						// check the account number as well
		//						frmARCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0, FCConvert.ToString(rsTemp.Get_Fields_String("BudgetaryAccountNumber")));
		//					}
		//					else
		//					{
		//						frmARCLStatus.InstancePtr.txtCash.Text = "Y";
		//					}
		//				}
		//			}
		//		}
		//		frmARCLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//		if (Strings.Trim(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColRef)) != "Total" && Strings.Trim(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColRef)) != "CURINT" && frmARCLStatus.InstancePtr.GRID.RowOutlineLevel(Row) != 0)
		//		{
		//			frmARCLStatus.InstancePtr.txtReference.Text = "REVERSE";
		//		}
		//		else
		//		{
		//			frmARCLStatus.InstancePtr.txtReference.Text = "";
		//		}
		//		if (strPaymentCode == "P")
		//		{
		//			// if it is a payment, then I have to find out if a CHGINT line was created for it
		//			// and if it is the last payment
		//			// this will find the payment option in the list
		//			for (intCT = 0; intCT <= frmARCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
		//			{
		//				if (boolGrandTotal)
		//				{
		//					if ("P" == Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//					{
		//						frmARCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//				else
		//				{
		//					if ("C" == Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//					{
		//						frmARCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//			}
		//			if (boolGrandTotal)
		//			{
		//				// do not check the payments...just show the total
		//			}
		//			else
		//			{
		//				// find out if this is a correction on the last payment or the account and if it had interest charged
		//				lngPaymentNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColPaymentKey))));
		//				// holds the ID of the payment in question
		//				rsTimeChk.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngPaymentNumber), modExtraModules.strARDatabase);
		//				// check to see if there are any records
		//				if (rsTimeChk.RecordCount() != 0)
		//				{
		//					lngChgInterest = FCConvert.ToInt32(rsTimeChk.Get_Fields_Int32("CHGINTNumber"));
		//				}
		//				rsTimeChk.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngChgInterest), modExtraModules.strARDatabase);
		//				if (rsTimeChk.RecordCount() != 0)
		//				{
		//					// If there is a CGHINT line for this account then open up all the payments for this account to find out if the payment chosen is the last payment to be applied
		//					rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountPayments + ") AS Table1 WHERE Reference <> 'CHGINT' AND BillKey = " + FCConvert.ToString(lngBill) + " AND isnull(ReceiptNumber, 0) <> -1 ORDER BY EffectiveInterestDate desc, ID desc", modExtraModules.strARDatabase);
		//					if (rsTemp.RecordCount() != 0)
		//					{
		//						if (Strings.Trim(rsTemp.Get_Fields_String("Reference") + " ") != "CNVRSN")
		//						{
		//							// and the latest one is not the conversion
		//							if (lngPaymentNumber == FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID")))
		//							{
		//								// is this payment the last one?
		//								if (Conversion.Val(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColCHGINTNumber)) != 0)
		//								{
		//									// did it have a CHGINT applied to it...
		//									// if it is, then
		//									rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngChgInterest), modExtraModules.strARDatabase);
		//									// create a correction for the CHGINT but do not store it in the boxes
		//									if (rsTemp.RecordCount() > 0)
		//									{
		//										lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
		//										if (rsTemp.Get_Fields_DateTime("CHGINTDate").ToOADate() != 0)
		//										{
		//											dtPassDate = (DateTime)rsTemp.Get_Fields_DateTime("CHGINTDate");
		//										}
		//										else
		//										{
		//											dtPassDate = DateTime.Today;
		//										}
		//									}
		//									// set lngARCHGINT so that it can be processed correctly when F9 is pressed
		//									Statics.lngARCHGINT = FCConvert.ToInt32(Conversion.Val(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColCHGINTNumber)) * -1);
		//									Statics.boolARUseOldCHGINTDate = true;
		//								}
		//							}
		//							else
		//							{
		//								MessageBox.Show("Only the last payment applied can be reversed when interest was charged.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//								return;
		//							}
		//						}
		//						else
		//						{
		//							MessageBox.Show("This transaction occured before the conversion, therefore it cannot be affected automatically", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//							return;
		//						}
		//					}
		//					else
		//					{
		//						MessageBox.Show("ERROR in Opposition Line Creation", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//						return;
		//					}
		//					// DJW 11202013 Added section to move back the interest date on bill
		//					if (Statics.boolARUseOldCHGINTDate)
		//					{
		//						dtNewIntDate = GetLastARInterestDate(Statics.lngCurrentCustomerIDAR, lngBill, dtPassDate, lngPaymentNumber);
		//						modGlobalConstants.Statics.dtOldInterestDate = dtNewIntDate;
		//						if (!modExtraModules.IsThisCR() && !fecherFoundation.FCUtils.IsNull(dtNewIntDate))
		//						{
		//							// kk 12192013 troar-61  Don't update bill in CR
		//							rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strARDatabase);
		//							if (rsTemp.RecordCount() > 0)
		//							{
		//								rsTemp.Edit();
		//								rsTemp.Set_Fields("IntPaidDate", dtNewIntDate);
		//								rsTemp.Update();
		//							}
		//						}
		//					}
		//				}
		//				else
		//				{
		//					// it is not then
		//					rsTimeChk.OpenRecordset("SELECT * FROM PaymentRec WHERE BillNumber = " + FCConvert.ToString(lngBill) + " AND Reference = 'CHGINT'", modExtraModules.strARDatabase);
		//					if (rsTimeChk.RecordCount() > 0)
		//					{
		//						MessageBox.Show("This transaction contains older information, please correct manually.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//						return;
		//					}
		//				}
		//			}
		//		}
		//		else if (strPaymentCode == "C")
		//		{
		//			strPaymentCode = "C";
		//			// this will find the payment option in the list
		//			for (intCT = 0; intCT <= frmARCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
		//			{
		//				if (strPaymentCode == Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//				{
		//					frmARCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//					break;
		//				}
		//			}
		//		}
		//		else
		//		{
		//			// check to see if the payment that is going to be reversed is before the conversion
		//			// this will find the payment option in the list
		//			for (intCT = 0; intCT <= frmARCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
		//			{
		//				// If strPaymentCode = Left$(.cmbCode.List(intCT), 1) Then
		//				if ("C" == Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//				{
		//					strPaymentCode = "C";
		//					frmARCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//					break;
		//				}
		//			}
		//		}
		//		// set the total variables
		//		if (strCode == "=" || strCode == "-1" || strPaymentCode == "S")
		//		{
		//			// totals
		//			dblP = FCConvert.ToDouble(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColPrincipal));
		//			dblTax = FCConvert.ToDouble(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColTax));
		//			dblInt = FCConvert.ToDouble(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColInterest)));
		//			if (strPaymentCode != "C")
		//			{
		//				dblInt = dblP + dblInt + dblTax;
		//				dblTax = 0;
		//				dblP = 0;
		//			}
		//			frmARCLStatus.InstancePtr.txtComments.Text = "";
		//		}
		//		else
		//		{
		//			dblP = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColPrincipal)) * -1, "#,##0.00"));
		//			dblTax = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColTax)) * -1, "#,##0.00"));
		//			dblInt = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.GRID.TextMatrix(Row, frmARCLStatus.InstancePtr.lngGRIDColInterest)) * -1, "#,##0.00"));
		//			if (strPaymentCode == "Y")
		//			{
		//				dblInt = dblP + dblInt + dblTax;
		//				dblP = 0;
		//				dblTax = 0;
		//			}
		//			frmARCLStatus.InstancePtr.txtComments.Text = "";
		//		}
		//		// if the user is creating an opposition line for a total line then
		//		if (strCode == "=")
		//		{
		//			// check for pending payments
		//			if (boolGrandTotal)
		//			{
		//				// this will take into account any pending payments
		//				for (intCT = 1; intCT <= frmARCLStatus.InstancePtr.vsPayments.Rows - 1; intCT++)
		//				{
		//					if (frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColCode) == "P")
		//					{
		//					}
		//					else
		//					{
		//					}
		//				}
		//			}
		//			else
		//			{
		//				// this will take into account any pending payments for the year of the bill
		//				for (intCT = 1; intCT <= frmARCLStatus.InstancePtr.vsPayments.Rows - 1; intCT++)
		//				{
		//					if (Conversion.Val(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColBillID)) == lngBill)
		//					{
		//						if (frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColCode) == "P")
		//						{
		//							dblP -= FCConvert.ToDouble(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColPrincipal));
		//							dblTax -= FCConvert.ToDouble(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColTax));
		//							dblInt -= FCConvert.ToDouble(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColInterest));
		//						}
		//						else
		//						{
		//							dblP += FCConvert.ToDouble(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColPrincipal));
		//							dblTax += FCConvert.ToDouble(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColTax));
		//							dblInt += FCConvert.ToDouble(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmARCLStatus.InstancePtr.lngPayGridColInterest));
		//						}
		//					}
		//				}
		//			}
		//		}
		//		else
		//		{
		//			// this is a partial payment opposition line
		//		}
		//		frmARCLStatus.InstancePtr.txtPrincipal.Text = Strings.Format(dblP, "#,##0.00");
		//		frmARCLStatus.InstancePtr.txtTax.Text = Strings.Format(dblTax, "#,##0.00");
		//		frmARCLStatus.InstancePtr.txtInterest.Text = Strings.Format(dblInt, "#,##0.00");
		//		if (boolSavePayment)
		//		{
		//			AddARPaymentToList_26(1, false, true);
		//		}
		//	}
		//}

		//public static bool ARPaymentGreaterThanTotalOwed()
		//{
		//	bool ARPaymentGreaterThanTotalOwed = false;
		//	// vbPorter upgrade warning: sumPayment As double	OnWriteFCConvert.ToDecimal(
		//	double sumPayment = 0;
		//	double sumOwed = 0;
		//	sumOwed += FCConvert.ToDouble(frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.Rows - 1, 11));
		//	sumPayment = FCConvert.ToDouble(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtPrincipal.Text) + FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtTax.Text) + FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtInterest.Text));
		//	ARPaymentGreaterThanTotalOwed = modGlobal.Round(sumPayment, 2) > modGlobal.Round(sumOwed, 2);
		//	return ARPaymentGreaterThanTotalOwed;
		//}

		//public static bool ARPaymentGreaterThanOwed_2(bool boolBill)
		//{
		//	return ARPaymentGreaterThanOwed(ref boolBill);
		//}

		//public static bool ARPaymentGreaterThanOwed(ref bool boolBill)
		//{
		//	bool ARPaymentGreaterThanOwed = false;
		//	// vbPorter upgrade warning: sumOwed As double	OnWrite(Decimal, string)
		//	double sumOwed = 0;
		//	// vbPorter upgrade warning: sumPayment As double	OnWriteFCConvert.ToDecimal(
		//	double sumPayment;
		//	if (boolBill)
		//	{
		//		if (Strings.Trim(frmARCLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1)) != "")
		//		{
		//			sumOwed = FCConvert.ToDouble(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1)));
		//		}
		//	}
		//	else
		//	{
		//		sumOwed = FCConvert.ToDouble(frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.Rows - 1, frmARCLStatus.InstancePtr.lngGRIDColTotal));
		//	}
		//	sumPayment = FCConvert.ToDouble(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtPrincipal.Text) + FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtTax.Text) + FCConvert.ToDecimal(frmARCLStatus.InstancePtr.txtInterest.Text));
		//	ARPaymentGreaterThanOwed = modGlobal.Round(sumPayment, 2) > modGlobal.Round(sumOwed, 2);
		//	return ARPaymentGreaterThanOwed;
		//}
		//// vbPorter upgrade warning: lngBillKey As short	OnWriteFCConvert.ToInt32(
		//private static double FindARCHGINTValue(int lngBillKey)
		//{
		//	double FindARCHGINTValue = 0;
		//	// this function takes the year and returns the last CHGINT line's value for interest
		//	int lngRow;
		//	bool boolFound;
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	string strTemp = "";
		//	string strSQL;
		//	boolFound = false;
		//	strSQL = "SELECT * FROM (" + Statics.strARCurrentAccountPayments + ") AS Table1 WHERE (Reference = 'CHGINT' OR Reference = 'CNVRSN') AND ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " ORDER BY RecordedTransactionDate desc, ActualSystemDate desc";
		//	rsTemp.OpenRecordset(strSQL, modExtraModules.strARDatabase);
		//	if (rsTemp.RecordCount() != 0)
		//	{
		//		strTemp = Strings.Trim(rsTemp.Get_Fields_String("Reference") + " ");
		//		if (strTemp == "CNVRSN")
		//		{
		//			MessageBox.Show("This transaction occured before the conversion, therefore it cannot be affected automatically", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//			boolFound = false;
		//		}
		//		else
		//		{
		//			boolFound = true;
		//			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//			FindARCHGINTValue = (double)rsTemp.Get_Fields("Interest");
		//		}
		//	}
		//	return FindARCHGINTValue;
		//}
		// vbPorter upgrade warning: intArrayIndex As short	OnWriteFCConvert.ToInt32(
		//public static int CreateARCHGINTLine(short intArrayIndex, ref DateTime dateTemp)
		//{
		//	int CreateARCHGINTLine = 0;
		//	// this function will create a charge interest line for the last payment in the series
		//	// and return its autonumber to the last payment's CHGINTNumber field
		//	clsDRWrapper rsCHGINT = new clsDRWrapper();
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	int intType;
		//	int lngIntIndex;
		//	lngIntIndex = Statics.ARPaymentArray[intArrayIndex].BillKey;
		//	if (modGlobal.Round(Statics.dblARCurrentInt[lngIntIndex], 2) != 0)
		//	{
		//		rsCHGINT.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountPayments + ") AS Table1 WHERE AccountKey = 0", modExtraModules.strARDatabase);
		//		// use the other bills info
		//		rsCHGINT.AddNew();
		//		rsCHGINT.Update();
		//		CreateARCHGINTLine = rsCHGINT.Get_Fields_Int32("ID");
		//		rsCHGINT.Set_Fields("AccountKey", Statics.ARPaymentArray[intArrayIndex].AccountKey);
		//		rsCHGINT.Set_Fields("ActualAccountNumber", Statics.ARPaymentArray[intArrayIndex].ActualAccountNumber);
		//		if (!modExtraModules.IsThisCR())
		//		{
		//			// check for cash receipting
		//			rsCHGINT.Set_Fields("ReceiptNumber", 0);
		//			// if this is from CR then do not update the billing and leave the Status as pending
		//			rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ID = " + FCConvert.ToString(Statics.ARPaymentArray[intArrayIndex].BillKey), modExtraModules.strARDatabase);
		//			// DJW 11/15/2013 Changed - to + when summing with dblARCurrentInt to get negative IntAdded amount
		//			if (rsTemp.RecordCount() != 0)
		//			{
		//				rsTemp.Edit();
		//				// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//				rsTemp.Set_Fields("IntAdded", (rsTemp.Get_Fields_Double("IntAdded") + Statics.dblARCurrentInt[lngIntIndex]));
		//				// this might be where I should adjust the effective int date
		//				rsTemp.Update();
		//			}
		//		}
		//		else
		//		{
		//			rsCHGINT.Set_Fields("ReceiptNumber", -1);
		//		}
		//		rsCHGINT.Set_Fields("CHGINTNumber", 0);
		//		if (Statics.ARPaymentArray[intArrayIndex].ReversalID == -1)
		//		{
		//			rsCHGINT.Set_Fields("CHGINTDate", Statics.ARPaymentArray[intArrayIndex].EffectiveInterestDate);
		//		}
		//		else
		//		{
		//			rsCHGINT.Set_Fields("CHGINTDate", Statics.dateAROldDate[lngIntIndex]);
		//		}
		//		rsCHGINT.Set_Fields("BillKey", Statics.ARPaymentArray[intArrayIndex].BillKey);
		//		rsCHGINT.Set_Fields("BillType", Statics.ARPaymentArray[intArrayIndex].BillType);
		//		rsCHGINT.Set_Fields("InvoiceNumber", Statics.ARPaymentArray[intArrayIndex].InvoiceNumber);
		//		rsCHGINT.Set_Fields("BillNumber", Statics.ARPaymentArray[intArrayIndex].BillNumber);
		//		rsCHGINT.Set_Fields("ActualSystemDate", Statics.ARPaymentArray[intArrayIndex].ActualSystemDate);
		//		rsCHGINT.Set_Fields("EffectiveInterestDate", Statics.ARPaymentArray[intArrayIndex].EffectiveInterestDate);
		//		rsCHGINT.Set_Fields("RecordedTransactionDate", Statics.ARPaymentArray[intArrayIndex].RecordedTransactionDate);
		//		rsCHGINT.Set_Fields("Teller", Statics.ARPaymentArray[intArrayIndex].Teller);
		//		rsCHGINT.Set_Fields("Code", "I");
		//		rsCHGINT.Set_Fields("Principal", 0);
		//		// DJW 5/27/2008 Took out multiply by -1
		//		rsCHGINT.Set_Fields("Interest", Statics.dblARCurrentInt[lngIntIndex]);
		//		// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//		if (FCConvert.ToInt32(rsCHGINT.Get_Fields("Interest")) > 0)
		//		{
		//			rsCHGINT.Set_Fields("Reference", "EARNINT");
		//		}
		//		else
		//		{
		//			rsCHGINT.Set_Fields("Reference", "CHGINT");
		//		}
		//		rsCHGINT.Set_Fields("TransNumber", Statics.ARPaymentArray[intArrayIndex].TransNumber);
		//		rsCHGINT.Set_Fields("PaidBy", Statics.ARPaymentArray[intArrayIndex].PaidBy);
		//		rsCHGINT.Set_Fields("Comments", "");
		//		rsCHGINT.Set_Fields("CashDrawer", Statics.ARPaymentArray[intArrayIndex].CashDrawer);
		//		rsCHGINT.Set_Fields("GeneralLedger", Statics.ARPaymentArray[intArrayIndex].GeneralLedger);
		//		rsCHGINT.Set_Fields("BudgetaryAccountNumber", Statics.ARPaymentArray[intArrayIndex].BudgetaryAccountNumber);
		//		dateTemp = Statics.AREffectiveDate;
		//		// dateAROldDate(.Year - utDEFAULTSUBTRACTIONVALUE)
		//		rsCHGINT.Update();
		//		CreateARCHGINTLine = rsCHGINT.Get_Fields_Int32("ID");
		//	}
		//	return CreateARCHGINTLine;
		//}

		//public static void CreateARPreview(ref string strInvoiceNumber)
		//{
		//	// this will create a preview for the bill passed in and show it to the user
		//	int lngStartRow = 0;
		//	int lngEndRow = 0;
		//	int indexPay;
		//	int rowPrev = 0;
		//	bool boolFound;
		//	FormatARPreviewGrid();
		//	lngStartRow = frmARCLStatus.InstancePtr.GRID.FindRow(strInvoiceNumber, 1, 1);
		//	if (lngStartRow < 0)
		//	{
		//		return;
		//	}
		//	lngEndRow = frmARCLStatus.InstancePtr.GRID.FindRow("=", lngStartRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode);
		//	frmARCLStatus.InstancePtr.vsPreview.Rows = lngEndRow - lngStartRow + 2;
		//	frmARCLStatus.InstancePtr.GRID.Select(lngStartRow, 1, lngEndRow, frmARCLStatus.InstancePtr.GRID.Cols - 1);
		//	frmARCLStatus.InstancePtr.vsPreview.Select(1, 1, frmARCLStatus.InstancePtr.vsPreview.Rows - 1, frmARCLStatus.InstancePtr.vsPreview.Cols - 1);
		//	frmARCLStatus.InstancePtr.vsPreview.Clip = frmARCLStatus.InstancePtr.GRID.Clip;
		//	// Add Payment Lines
		//	for (indexPay = 1; indexPay <= frmARCLStatus.InstancePtr.vsPayments.Rows - 1; indexPay++)
		//	{
		//		if (strInvoiceNumber == frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColInvoice) && frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColRef) != "CHGINT")
		//		{
		//			AddPaymentToARPreview(ref indexPay, ref rowPrev);
		//		}
		//	}
		//	frmARCLStatus.InstancePtr.vsPreview.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
		//	// merge the Original Bill line
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(1, 3, frmARCLStatus.InstancePtr.vsPreview.TextMatrix(1, 4));
		//	frmARCLStatus.InstancePtr.vsPreview.MergeRow(1, true, 3, 4);
		//	// merge the name field if needed
		//	if (frmARCLStatus.InstancePtr.vsPreview.TextMatrix(2, 3) == "Billed To:")
		//	{
		//		frmARCLStatus.InstancePtr.vsPreview.TextMatrix(2, 6, frmARCLStatus.InstancePtr.vsPreview.TextMatrix(2, 5));
		//		frmARCLStatus.InstancePtr.vsPreview.TextMatrix(2, 7, frmARCLStatus.InstancePtr.vsPreview.TextMatrix(2, 5));
		//		frmARCLStatus.InstancePtr.vsPreview.TextMatrix(2, 8, frmARCLStatus.InstancePtr.vsPreview.TextMatrix(2, 5));
		//		frmARCLStatus.InstancePtr.vsPreview.MergeRow(2, true,5, 8);
		//	}
		//	// Calculate Totals
		//	// Set The Colors
		//	frmARCLStatus.InstancePtr.vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, frmARCLStatus.InstancePtr.vsPreview.Rows - 1, 12, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
		//	// &H80000016    'grey
		//	// .vsPreview.BackColorAlternate = TRIOCOLORGRAYBACKGROUND
		//	frmARCLStatus.InstancePtr.vsPreview.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
		//	// Total Gridlines
		//	frmARCLStatus.InstancePtr.vsPreview.Select(frmARCLStatus.InstancePtr.vsPreview.Rows - 1, frmARCLStatus.InstancePtr.lngGRIDColPrincipal, frmARCLStatus.InstancePtr.vsPreview.Rows - 1, frmARCLStatus.InstancePtr.lngGRIDColTotal);
		//	frmARCLStatus.InstancePtr.vsPreview.CellBorder(Color.Blue, 0, 1, 0, 0, 0, 0);
		//	// turn the status grid off and the preview grid on
		//	frmARCLStatus.InstancePtr.GRID.Visible = false;
		//	frmARCLStatus.InstancePtr.vsPreview.Visible = true;
		//	frmARCLStatus.InstancePtr.mnuPaymentPreview.Text = "Exit Preview";
		//	frmARCLStatus.InstancePtr.vsPreview.Select(0, 0);
		//}

		//private static void AddPaymentToARPreview(ref int indexPay, ref int rowPrev)
		//{
		//	int intCT;
		//	string strSwap = "";
		//	int rowNew = 0;
		//	rowNew = frmARCLStatus.InstancePtr.vsPreview.Rows - 1;
		//	strSwap = "\t" + "\t" + frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColInvoice) + "\t" + frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColRef) + "\t" + frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColCode) + "\t" + frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColPrincipal) + "\t" + frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColTax) + "\t" + frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColInterest);
		//	frmARCLStatus.InstancePtr.vsPreview.AddItem("", rowNew);
		//	// .vsPreview.TextMatrix(rowNew, .lngGRIDColInvoiceNumber) = .vsPayments.TextMatrix(indexPay, frmARCLStatus.lngPayGridColInvoice)
		//	// .vsPreview.TextMatrix(rowNew, .lngGRIDColDate) = Date
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColRef, frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColRef));
		//	frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColCode, frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColCode));
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColPrincipal, frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColPrincipal));
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColInterest, frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColInterest));
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColTax, frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColTax));
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColTotal, frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColTotal));
		//	// fill in the total col
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(frmARCLStatus.InstancePtr.vsPreview.Rows - 2, frmARCLStatus.InstancePtr.lngGRIDColTotal, FCConvert.ToString(FCConvert.ToDecimal(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColPrincipal)) + FCConvert.ToDecimal(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColTax)) + FCConvert.ToDecimal(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmARCLStatus.InstancePtr.lngPayGridColInterest))));
		//	// fill in the sign of the payment
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColLineCode, "-");
		//	// calculate the bottom total line
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmARCLStatus.InstancePtr.lngGRIDColPrincipal, Conversion.Val(frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmARCLStatus.InstancePtr.lngGRIDColPrincipal)) - Conversion.Val(frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColPrincipal)));
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmARCLStatus.InstancePtr.lngGRIDColTax, Conversion.Val(frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmARCLStatus.InstancePtr.lngGRIDColTax)) - Conversion.Val(frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColTax)));
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmARCLStatus.InstancePtr.lngGRIDColInterest, Conversion.Val(frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmARCLStatus.InstancePtr.lngGRIDColInterest)) - Conversion.Val(frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColInterest)));
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmARCLStatus.InstancePtr.lngGRIDColTotal, Conversion.Val(frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmARCLStatus.InstancePtr.lngGRIDColTotal)) - Conversion.Val(frmARCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmARCLStatus.InstancePtr.lngGRIDColTotal)));
		//}

		//private static void FormatARPreviewGrid()
		//{
		//	int wid;
		//	int lngCol;
		//	frmARCLStatus.InstancePtr.vsPreview.Width = frmARCLStatus.InstancePtr.GRID.Width;
		//	wid = frmARCLStatus.InstancePtr.vsPreview.Width;
		//	frmARCLStatus.InstancePtr.vsPreview.Left = frmARCLStatus.InstancePtr.GRID.Left;
		//	frmARCLStatus.InstancePtr.vsPreview.Top = frmARCLStatus.InstancePtr.GRID.Top;
		//	frmARCLStatus.InstancePtr.vsPreview.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
		//	// this allows the name to spill into the next column
		//	frmARCLStatus.InstancePtr.vsPreview.Cols = frmARCLStatus.InstancePtr.GRID.Cols;
		//	// column headers
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, 0, "");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColInvoiceNumber, "Invoice");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColDate, "Date");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColRef, "Ref");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColPaymentCode, "C");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColPrincipal, "Principal");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColPTC, "PTC");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColTax, "Tax");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColInterest, "Interest");
		//	frmARCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmARCLStatus.InstancePtr.lngGRIDColTotal, "Total");
		//	frmARCLStatus.InstancePtr.vsPreview.ColFormat(frmARCLStatus.InstancePtr.lngGRIDColDate, "MM/dd/yy");
		//	frmARCLStatus.InstancePtr.vsPreview.ColFormat(frmARCLStatus.InstancePtr.lngGRIDColPrincipal, "#,##0.00");
		//	frmARCLStatus.InstancePtr.vsPreview.ColFormat(frmARCLStatus.InstancePtr.lngGRIDColPTC, "#,##0.00");
		//	frmARCLStatus.InstancePtr.vsPreview.ColFormat(frmARCLStatus.InstancePtr.lngGRIDColTax, "#,##0.00");
		//	frmARCLStatus.InstancePtr.vsPreview.ColFormat(frmARCLStatus.InstancePtr.lngGRIDColInterest, "#,##0.00");
		//	frmARCLStatus.InstancePtr.vsPreview.ColFormat(frmARCLStatus.InstancePtr.lngGRIDColTotal, "#,##0.00");
		//	frmARCLStatus.InstancePtr.vsPreview.ColAlignment(frmARCLStatus.InstancePtr.lngGRIDColInvoiceNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	frmARCLStatus.InstancePtr.vsPreview.ColAlignment(frmARCLStatus.InstancePtr.lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	frmARCLStatus.InstancePtr.vsPreview.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, frmARCLStatus.InstancePtr.lngGRIDColPrincipal, 0, frmARCLStatus.InstancePtr.lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
		//	for (lngCol = 0; lngCol <= frmARCLStatus.InstancePtr.GRID.Cols - 1; lngCol++)
		//	{
		//		frmARCLStatus.InstancePtr.vsPreview.ColWidth(lngCol, frmARCLStatus.InstancePtr.GRID.ColWidth(lngCol));
		//	}
		//}
		// vbPorter upgrade warning: ArrayIndex As short	OnWriteFCConvert.ToInt32(
		//public static bool CreateARCHGINTPaymentRecord_2(short ArrayIndex, clsDRWrapper rsCreatePy, bool boolRev = false)
		//{
		//	return CreateARCHGINTPaymentRecord(ref ArrayIndex, ref rsCreatePy, boolRev);
		//}

		//public static bool CreateARCHGINTPaymentRecord_20(short ArrayIndex, clsDRWrapper rsCreatePy, bool boolRev = false)
		//{
		//	return CreateARCHGINTPaymentRecord(ref ArrayIndex, ref rsCreatePy, boolRev);
		//}

		//public static bool CreateARCHGINTPaymentRecord(ref short ArrayIndex, ref clsDRWrapper rsCreatePy, bool boolRev = false)
		//{
		//	bool CreateARCHGINTPaymentRecord = false;
		//	// this adds a record to the database that offsets the charged interest line and
		//	// adjusts the PaidThroughDate
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	int lngR;
		//	// this will save one payment record to the database
		//	if (MAX_ARPAYMENTS > ArrayIndex)
		//	{
		//		// checks for out of bounds array call
		//		rsCreatePy.AddNew();
		//		rsCreatePy.Update();
		//		Statics.ARPaymentArray[ArrayIndex].ID = rsCreatePy.Get_Fields_Int32("ID");
		//		// when this is created, the ID has to be stored in the grid so that it does not create another CHGINT line for this one
		//		for (lngR = 1; lngR <= frmARCLStatus.InstancePtr.vsPayments.Rows - 1; lngR++)
		//		{
		//			if (Math.Abs(FCConvert.ToInt16(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(lngR, frmARCLStatus.InstancePtr.lngPayGridColArrayIndex))) == ArrayIndex)
		//			{
		//				frmARCLStatus.InstancePtr.vsPayments.TextMatrix(lngR, frmARCLStatus.InstancePtr.lngPayGridColKeyNumber, FCConvert.ToString(Statics.ARPaymentArray[ArrayIndex].ID));
		//			}
		//		}
		//		rsCreatePy.Set_Fields("ActualAccountNumber", Statics.ARPaymentArray[ArrayIndex].ActualAccountNumber);
		//		rsCreatePy.Set_Fields("AccountKey", Statics.ARPaymentArray[ArrayIndex].AccountKey);
		//		rsCreatePy.Set_Fields("BillKey", Statics.ARPaymentArray[ArrayIndex].BillKey);
		//		rsCreatePy.Set_Fields("BillType", Statics.ARPaymentArray[ArrayIndex].BillType);
		//		rsCreatePy.Set_Fields("InvoiceNumber", Statics.ARPaymentArray[ArrayIndex].InvoiceNumber);
		//		rsCreatePy.Set_Fields("BillNumber", Statics.ARPaymentArray[ArrayIndex].BillNumber);
		//		if (!modExtraModules.IsThisCR())
		//		{
		//			// check for cash receipting
		//			rsCreatePy.Set_Fields("ReceiptNumber", 0);
		//			rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ID = " + FCConvert.ToString(Statics.ARPaymentArray[ArrayIndex].BillKey), modExtraModules.strARDatabase);
		//			if (rsTemp.RecordCount() != 0)
		//			{
		//				rsTemp.Edit();
		//				// DJW 11/17/08 Reversed Int Added instead of adding to Int Paid on a reversal to solve probelm with flat interest not getting charged again
		//				if (boolRev)
		//				{
		//					// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//					rsTemp.Set_Fields("IntAdded", (rsTemp.Get_Fields("IntAdded") + Statics.ARPaymentArray[ArrayIndex].CurrentInterest));
		//				}
		//				else
		//				{
		//					// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//					rsTemp.Set_Fields("IntPaid", (rsTemp.Get_Fields("IntPaid") + Statics.ARPaymentArray[ArrayIndex].CurrentInterest));
		//				}
		//				// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//				rsTemp.Set_Fields("TaxPaid", (rsTemp.Get_Fields("TaxPaid") + Statics.ARPaymentArray[ArrayIndex].Tax));
		//				// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//				rsTemp.Set_Fields("PrinPaid", (rsTemp.Get_Fields("PrinPaid") + Statics.ARPaymentArray[ArrayIndex].Principal));
		//				rsTemp.Update();
		//			}
		//			else
		//			{
		//				CreateARCHGINTPaymentRecord = false;
		//				goto END_OF_PAYMENT;
		//			}
		//		}
		//		else
		//		{
		//			if (boolRev)
		//			{
		//				rsCreatePy.Set_Fields("ReceiptNumber", 0);
		//			}
		//			else
		//			{
		//				rsCreatePy.Set_Fields("ReceiptNumber", -1);
		//			}
		//		}
		//		// this is to check for a reversal
		//		if (boolRev)
		//		{
		//			rsCreatePy.Set_Fields("CHGINTNumber", 0);
		//		}
		//		else
		//		{
		//			rsCreatePy.Set_Fields("CHGINTNumber", Statics.ARPaymentArray[ArrayIndex].CHGINTNumber);
		//		}
		//		rsCreatePy.Set_Fields("CHGINTDate", Statics.ARPaymentArray[ArrayIndex].CHGINTDate);
		//		rsCreatePy.Set_Fields("ActualSystemDate", Statics.ARPaymentArray[ArrayIndex].ActualSystemDate);
		//		rsCreatePy.Set_Fields("EffectiveInterestDate", Statics.ARPaymentArray[ArrayIndex].EffectiveInterestDate);
		//		rsCreatePy.Set_Fields("RecordedTransactionDate", Statics.ARPaymentArray[ArrayIndex].RecordedTransactionDate);
		//		rsCreatePy.Set_Fields("Teller", Statics.ARPaymentArray[ArrayIndex].Teller);
		//		rsCreatePy.Set_Fields("Reference", Statics.ARPaymentArray[ArrayIndex].Reference);
		//		rsCreatePy.Set_Fields("Code", Statics.ARPaymentArray[ArrayIndex].Code);
		//		rsCreatePy.Set_Fields("Principal", Statics.ARPaymentArray[ArrayIndex].Principal);
		//		rsCreatePy.Set_Fields("Interest", Statics.ARPaymentArray[ArrayIndex].CurrentInterest);
		//		rsCreatePy.Set_Fields("TransNumber", Statics.ARPaymentArray[ArrayIndex].TransNumber);
		//		rsCreatePy.Set_Fields("PaidBy", Statics.ARPaymentArray[ArrayIndex].PaidBy);
		//		rsCreatePy.Set_Fields("Comments", Statics.ARPaymentArray[ArrayIndex].Comments);
		//		rsCreatePy.Set_Fields("CashDrawer", Statics.ARPaymentArray[ArrayIndex].CashDrawer);
		//		rsCreatePy.Set_Fields("GeneralLedger", Statics.ARPaymentArray[ArrayIndex].GeneralLedger);
		//		rsCreatePy.Set_Fields("BudgetaryAccountNumber", Statics.ARPaymentArray[ArrayIndex].BudgetaryAccountNumber);
		//		rsCreatePy.Update();
		//		CreateARCHGINTPaymentRecord = true;
		//	}
		//	else
		//	{
		//		MessageBox.Show("Out of Bounds Array Index", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	if (!modExtraModules.IsThisCR())
		//	{
		//		// if this is not from CR then adust the Interest Applied Through Date
		//		if (rsCreatePy.FindFirstRecord("ID", Statics.ARPaymentArray[ArrayIndex].ID))
		//		{
		//			rsTemp.Edit();
		//			// adjust the paid through date...
		//			rsTemp.Set_Fields("IntPaidDate", FindLastARPaidThroughDate_6(rsCreatePy.Get_Fields_Int32("BillKey"), Statics.ARPaymentArray[ArrayIndex].CHGINTNumber * -1));
		//			rsTemp.Update();
		//		}
		//		else
		//		{
		//		}
		//	}
		//	return CreateARCHGINTPaymentRecord;
		//	END_OF_PAYMENT:
		//	;
		//	MessageBox.Show("Account " + FCConvert.ToString(Statics.ARPaymentArray[ArrayIndex].ActualAccountNumber) + " has had an error while processing.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	rsCreatePy.Delete();
		//	return CreateARCHGINTPaymentRecord;
		//}

		//private static DateTime FindLastARPaidThroughDate_6(int lngBK, int lngCHGINTKey)
		//{
		//	return FindLastARPaidThroughDate(ref lngBK, ref lngCHGINTKey);
		//}

		//private static DateTime FindLastARPaidThroughDate(ref int lngBK, ref int lngCHGINTKey)
		//{
		//	DateTime FindLastARPaidThroughDate = System.DateTime.Now;
		//	// this will find the last paid through date on a CHGINT line and return it, else return the original bill date
		//	// and reset the CHGINTDate of the payment passed in so that future searches are not affected by it
		//	// lngBK is the BillKey for this account
		//	// lngCHGINTKey is the ID of the CHGINT that is being reversed
		//	clsDRWrapper rsCHGINT = new clsDRWrapper();
		//	string strCHGINT;
		//	int lngCT;
		//	DateTime dateReverse = DateTime.FromOADate(0);
		//	// dateReverse is the date that is currently the Effective Date from the CHGINT payment that is being reversed
		//	// vbPorter upgrade warning: dateOBill As DateTime	OnWriteFCConvert.ToInt16(
		//	DateTime dateOBill;
		//	// vbPorter upgrade warning: dateCNVRSN As DateTime	OnWriteFCConvert.ToInt16(
		//	DateTime dateCNVRSN;
		//	// get the original bill date
		//	strCHGINT = "SELECT * FROM Bill INNER JOIN RateKeys ON Bill.BillNumber = RateKeys.ID WHERE ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " AND ID = " + FCConvert.ToString(lngBK);
		//	rsCHGINT.OpenRecordset(strCHGINT, modExtraModules.strARDatabase);
		//	if (rsCHGINT.EndOfFile() != true)
		//	{
		//		dateOBill = (DateTime)rsCHGINT.Get_Fields_DateTime("IntStart");
		//	}
		//	else
		//	{
		//		dateOBill = DateTime.FromOADate(0);
		//	}
		//	dateCNVRSN = DateTime.FromOADate(0);
		//	strCHGINT = "SELECT * FROM (" + Statics.strARCurrentAccountPayments + ") AS Table1 WHERE ID = " + FCConvert.ToString(Math.Abs(lngCHGINTKey));
		//	rsCHGINT.OpenRecordset(strCHGINT, modExtraModules.strARDatabase);
		//	if (rsCHGINT.RecordCount() != 0)
		//	{
		//		dateReverse = (DateTime)rsCHGINT.Get_Fields_DateTime("CHGINTDate");
		//	}
		//	// compare the dates
		//	if (dateReverse.ToOADate() != 0)
		//	{
		//		// if there is another CHGINT payment other than the one that is being reversed
		//		if (dateCNVRSN.ToOADate() != 0)
		//		{
		//			// if there is a conversion done
		//			if (dateReverse.ToOADate() > dateReverse.ToOADate())
		//			{
		//				// if the CHGINT line is more recent then the conversion
		//				FindLastARPaidThroughDate = dateReverse;
		//				// then use the CHGINT date
		//			}
		//			else
		//			{
		//				// else
		//				FindLastARPaidThroughDate = dateCNVRSN;
		//				// then use the Conversion date
		//			}
		//		}
		//		else
		//		{
		//			// else
		//			FindLastARPaidThroughDate = dateReverse;
		//			// use the CHGINT date
		//		}
		//	}
		//	else
		//	{
		//		// if there are no CHGINT other than the one that is being reversed
		//		if (dateCNVRSN.ToOADate() == 0)
		//		{
		//			// if there is no conversion date
		//			FindLastARPaidThroughDate = dateOBill;
		//			// then use the original bill date
		//		}
		//		else
		//		{
		//			// if there is a conversion done
		//			if (dateCNVRSN.ToOADate() > dateOBill.ToOADate())
		//			{
		//				// if it is more current then the original bill (which it better be)
		//				FindLastARPaidThroughDate = dateCNVRSN;
		//				// then use the conversion date
		//			}
		//			else
		//			{
		//				// else
		//				FindLastARPaidThroughDate = dateOBill;
		//				// use the original bill date (this should not happen)
		//			}
		//		}
		//	}
		//	// reset the CHGINTDate of the payment passed in
		//	strCHGINT = "UPDATE (" + Statics.strARCurrentAccountPayments + ") AS Table1 SET CHGINTDate = 0 WHERE ID = " + FCConvert.ToString(Math.Abs(lngCHGINTKey));
		//	rsCHGINT.Execute(strCHGINT, modExtraModules.strARDatabase);
		//	return FindLastARPaidThroughDate;
		//}

		//public static bool ARYearCodeValidate()
		//{
		//	bool ARYearCodeValidate = false;
		//	// this function will return true if the Bill Number and Code comboboxes are a valid pair
		//	ARYearCodeValidate = true;
		//	//PPJ:FINAL:DSE #i529 Exception when SelectedIndex is -1
		//	if (frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex >= 0 && Strings.Left(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString(), 4) == "Auto")
		//	{
		//		if (Strings.Left(frmARCLStatus.InstancePtr.cmbCode.Items[frmARCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1) != "P")
		//		{
		//			ARYearCodeValidate = false;
		//		}
		//	}
		//	return ARYearCodeValidate;
		//}

		//public static string Return_AR_Payments()
		//{
		//	string Return_AR_Payments = "";
		//	// this function will return a comma delimited string that contains the ID of the payments
		//	// saved this past time in Collections so that CR can use the payments in its own
		//	int intCT;
		//	string strTemp;
		//	strTemp = "";
		//	for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//	{
		//		strTemp += FCConvert.ToString(Statics.ARPaymentArray[intCT].ID) + ", ";
		//	}
		//	strTemp = Strings.Left(strTemp, strTemp.Length - 2);
		//	return Return_AR_Payments;
		//}

		//public static void FillPendingARTransactions()
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsPend = new clsDRWrapper();
		//		clsDRWrapper rsTemp = new clsDRWrapper();
		//		ARPayment payTemp = new ARPayment();
		//		int intCT;
		//		frmARCLStatus.InstancePtr.vsPayments.Rows = 1;
		//		rsPend.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountPayments + ") AS Table1 WHERE isnull(ReceiptNumber, 0) = -1 ORDER BY RecordedTransactionDate desc", modExtraModules.strARDatabase);
		//		if (rsPend.EndOfFile() != true && rsPend.BeginningOfFile() != true)
		//		{
		//			do
		//			{
		//				for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//				{
		//					if (Statics.ARPaymentArray[intCT].BillKey == 0)
		//					{
		//						break;
		//					}
		//				}
		//				//FC:FINAL:DSE:#257 Use custom constructor to initialize fields from the structure
		//				Statics.ARPaymentArray[intCT] = new ARPayment(0);
		//				Statics.ARPaymentArray[intCT].ID = FCConvert.ToInt32(rsPend.Get_Fields_Int32("ID"));
		//				Statics.ARPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsPend.Get_Fields_Int32("BillKey"));
		//				// kk03172014 trocr-420  Need bill key set to store more than one record in array
		//				Statics.ARPaymentArray[intCT].InvoiceNumber = FCConvert.ToString(rsPend.Get_Fields_String("InvoiceNumber"));
		//				// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
		//				Statics.ARPaymentArray[intCT].BillType = FCConvert.ToInt32(rsPend.Get_Fields("BillType"));
		//				Statics.ARPaymentArray[intCT].RecordedTransactionDate = (DateTime)rsPend.Get_Fields_DateTime("RecordedTransactionDate");
		//				Statics.ARPaymentArray[intCT].Reference = FCConvert.ToString(rsPend.Get_Fields_String("Reference"));
		//				Statics.ARPaymentArray[intCT].Code = rsPend.Get_Fields_String("Code");
		//				if (Strings.Trim(rsPend.Get_Fields_String("GeneralLedger") + " ") == "")
		//				{
		//					Statics.ARPaymentArray[intCT].CashDrawer = rsPend.Get_Fields_String("CashDrawer") + "   Y";
		//				}
		//				else
		//				{
		//					Statics.ARPaymentArray[intCT].CashDrawer = rsPend.Get_Fields_String("CashDrawer") + "   " + Strings.Trim(rsPend.Get_Fields_String("GeneralLedger") + " ");
		//				}
		//				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//				Statics.ARPaymentArray[intCT].Principal = FCConvert.ToDecimal(rsPend.Get_Fields("Principal"));
		//				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//				Statics.ARPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(rsPend.Get_Fields("Interest"));
		//				// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//				Statics.ARPaymentArray[intCT].Tax = FCConvert.ToDecimal(rsPend.Get_Fields("Tax"));
		//				// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
		//				Statics.ARPaymentArray[intCT].BillType = FCConvert.ToInt32(rsPend.Get_Fields("BillType"));
		//				Statics.ARPaymentArray[intCT].CHGINTNumber = FCConvert.ToInt32(rsPend.Get_Fields_Int32("CHGINTNumber"));
		//				Statics.ARPaymentArray[intCT].AccountKey = FCConvert.ToInt32(rsPend.Get_Fields_Int32("AccountKey"));
		//				Statics.ARPaymentArray[intCT].ActualAccountNumber = FCConvert.ToInt32(rsPend.Get_Fields_Int32("ActualAccountNumber"));
		//				Statics.ARPaymentArray[intCT].EffectiveInterestDate = (DateTime)rsPend.Get_Fields_DateTime("EffectiveInterestDate");
		//				Statics.ARPaymentArray[intCT].ActualSystemDate = (DateTime)rsPend.Get_Fields_DateTime("ActualSystemDate");
		//				if (intCT <= MAX_ARPAYMENTS)
		//				{
		//					frmARCLStatus.InstancePtr.FillPaymentGrid(frmARCLStatus.InstancePtr.vsPayments.Rows, intCT, Statics.ARPaymentArray[intCT].InvoiceNumber, Statics.ARPaymentArray[intCT].ID);
		//					rsPend.MoveNext();
		//				}
		//				else
		//				{
		//					MessageBox.Show("Payment list is too full.  Too many Pending payments.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//				}
		//			}
		//			while (!rsPend.EndOfFile());
		//		}
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Pending Transaction", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//}
		//// vbPorter upgrade warning: intindex As short	OnWriteFCConvert.ToInt32(
		//public static void ResetARPayment(ref int intindex, bool boolCHGINTErase = true)
		//{
		//	// this will delete if from the database if it is already stored and clear the payment array
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	string strSQL = "";
		//	clsDRWrapper rsCHGINT = new clsDRWrapper();
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	if (intindex < 0)
		//	{
		//		intindex = Math.Abs(intindex);
		//	}
		//	strSQL = "SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(Statics.ARPaymentArray[intindex].ID);
		//	rsTemp.OpenRecordset(strSQL, modExtraModules.strARDatabase);
		//	if (rsTemp.EndOfFile() != true)
		//	{
		//		if (!modExtraModules.IsThisCR())
		//		{
		//			if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CHGINTNumber")) != 0)
		//			{
		//				rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + rsTemp.Get_Fields_Int32("CHGINTNumber"), modExtraModules.strARDatabase);
		//				rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + rsTemp.Get_Fields_Int32("BillKey"), modExtraModules.strARDatabase);
		//				if (rsBill.EndOfFile() != true && rsBill.BeginningOfFile() != true)
		//				{
		//					rsBill.Edit();
		//					// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//					rsBill.Set_Fields("IntAdded", rsBill.Get_Fields("IntAdded") + rsCHGINT.Get_Fields("Interest"));
		//					rsBill.Update();
		//				}
		//			}
		//		}
		//		strSQL = "DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(Statics.ARPaymentArray[intindex].ID);
		//		if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CHGINTNumber")) != 0 && boolCHGINTErase)
		//		{
		//			strSQL += " OR ID = " + rsTemp.Get_Fields_Int32("CHGINTNumber");
		//		}
		//		rsTemp.Execute(strSQL, modExtraModules.strARDatabase);
		//	}
		//	Statics.ARPaymentArray[intindex].ID = 0;
		//	Statics.ARPaymentArray[intindex].AccountKey = 0;
		//	Statics.ARPaymentArray[intindex].ActualAccountNumber = 0;
		//	Statics.ARPaymentArray[intindex].BillNumber = 0;
		//	Statics.ARPaymentArray[intindex].InvoiceNumber = "";
		//	Statics.ARPaymentArray[intindex].BillKey = 0;
		//	Statics.ARPaymentArray[intindex].BillType = 0;
		//	Statics.ARPaymentArray[intindex].CHGINTNumber = 0;
		//	Statics.ARPaymentArray[intindex].ActualSystemDate = DateTime.FromOADate(0);
		//	Statics.ARPaymentArray[intindex].EffectiveInterestDate = DateTime.FromOADate(0);
		//	Statics.ARPaymentArray[intindex].RecordedTransactionDate = DateTime.FromOADate(0);
		//	Statics.ARPaymentArray[intindex].Teller = "";
		//	Statics.ARPaymentArray[intindex].Reference = "";
		//	Statics.ARPaymentArray[intindex].Code = "";
		//	Statics.ARPaymentArray[intindex].ReceiptNumber = "";
		//	Statics.ARPaymentArray[intindex].ReversalID = -1;
		//	Statics.ARPaymentArray[intindex].Principal = 0;
		//	Statics.ARPaymentArray[intindex].Tax = 0;
		//	Statics.ARPaymentArray[intindex].CurrentInterest = 0;
		//	Statics.ARPaymentArray[intindex].TransNumber = FCConvert.ToString(0);
		//	Statics.ARPaymentArray[intindex].PaidBy = "";
		//	Statics.ARPaymentArray[intindex].Comments = "";
		//	Statics.ARPaymentArray[intindex].CashDrawer = "";
		//	Statics.ARPaymentArray[intindex].GeneralLedger = "";
		//	Statics.ARPaymentArray[intindex].BudgetaryAccountNumber = "";
		//	Statics.ARPaymentArray[intindex].BillCode = "";
		//}
		// vbPorter upgrade warning: dblTotal As double	OnReadFCConvert.ToDecimal(
		//private static short CreateExcessARPaymentRecord_6(double dblTotal, bool boolForceYear = false)
		//{
		//	return CreateExcessARPaymentRecord(ref dblTotal, boolForceYear);
		//}

		//private static short CreateExcessARPaymentRecord(ref double dblTotal, bool boolForceYear = false)
		//{
		//	short CreateExcessARPaymentRecord = 0;
		//	// this function will create a payment record for the extra amount of money and
		//	// return the year that the payment was put towards
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	string strType = "";
		//	int lngBill = 0;
		//	int intCT;
		//	string PayCode = "";
		//	string BillCode = "";
		//	int lngNewBill = 0;
		//	// this is the type of payment that is being processed
		//	if (boolForceYear)
		//	{
		//		// This will add a bill for the prepayment to be added to
		//		rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " AND IsNull(BillNumber, 0) = 0", modExtraModules.strARDatabase);
		//		if (rsTemp.EndOfFile())
		//		{
		//			lngNewBill = CreateBlankARBill(ref Statics.lngCurrentCustomerIDAR);
		//			// Doevents
		//			rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngNewBill), modExtraModules.strARDatabase);
		//		}
		//		// Force the type to a prepayment
		//		strType = "Y";
		//	}
		//	else
		//	{
		//		strType = "P";
		//		rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " ORDER BY InvoiceNumber Desc", modExtraModules.strARDatabase);
		//	}
		//	if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//	{
		//		lngBill = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//	}
		//	else
		//	{
		//		lngBill = GetBKFromInvoice_2(frmARCLStatus.InstancePtr.cmbBillNumber.Items[frmARCLStatus.InstancePtr.cmbBillNumber.Items.Count - 2].ToString());
		//		rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " AND ID = " + FCConvert.ToString(lngBill), modExtraModules.strARDatabase);
		//		if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//		{
		//		}
		//		else
		//		{
		//			MessageBox.Show("Error opening database.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			return CreateExcessARPaymentRecord;
		//		}
		//	}
		//	for (intCT = 1; intCT <= MAX_ARPAYMENTS; intCT++)
		//	{
		//		// this finds a place in the array
		//		if (Statics.ARPaymentArray[intCT].BillKey == 0)
		//		{
		//			// to store the new payment record
		//			break;
		//		}
		//	}
		//	//FC:FINAL:DSE:#257 Use custom constructor to initialize fields from the structure
		//	Statics.ARPaymentArray[intCT] = new ARPayment(0);
		//	Statics.ARPaymentArray[intCT].BillKey = lngBill;
		//	// get the next bill and its total due values
		//	if (intCT < MAX_ARPAYMENTS)
		//	{
		//		// this adds it to the array of payments
		//		Statics.ARPaymentArray[intCT].BillKey = lngBill;
		//		Statics.ARPaymentArray[intCT].InvoiceNumber = FCConvert.ToString(rsTemp.Get_Fields_String("InvoiceNumber"));
		//		// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
		//		Statics.ARPaymentArray[intCT].BillType = FCConvert.ToInt32(rsTemp.Get_Fields("BillType"));
		//		Statics.ARPaymentArray[intCT].ActualAccountNumber = Statics.lngCurrentCustomerIDAR;
		//		Statics.ARPaymentArray[intCT].AccountKey = GetAccountKeyFromAccountNumber(Statics.lngCurrentCustomerIDAR);
		//		Statics.ARPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//		Statics.ARPaymentArray[intCT].CashDrawer = frmARCLStatus.InstancePtr.txtCD.Text;
		//		if (Statics.ARPaymentArray[intCT].CashDrawer == "Y")
		//		{
		//			Statics.ARPaymentArray[intCT].BudgetaryAccountNumber = frmARCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0);
		//		}
		//		else
		//		{
		//			Statics.ARPaymentArray[intCT].BudgetaryAccountNumber = "";
		//		}
		//		Statics.ARPaymentArray[intCT].Code = strType;
		//		Statics.ARPaymentArray[intCT].Comments = frmARCLStatus.InstancePtr.txtComments.Text;
		//		Statics.ARPaymentArray[intCT].EffectiveInterestDate = Statics.AREffectiveDate;
		//		Statics.ARPaymentArray[intCT].GeneralLedger = "Y";
		//		Statics.ARPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTotal);
		//		Statics.ARPaymentArray[intCT].CurrentInterest = 0;
		//		Statics.ARPaymentArray[intCT].ReceiptNumber = "P";
		//		if (frmARCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//		{
		//			Statics.ARPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//		}
		//		else
		//		{
		//			Statics.ARPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmARCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//		}
		//		Statics.ARPaymentArray[intCT].Reference = "PREPAY-A";
		//		Statics.ARPaymentArray[intCT].ReversalID = -1;
		//		frmARCLStatus.InstancePtr.FillPaymentGrid(1, intCT, rsTemp.Get_Fields_String("InvoiceNumber"));
		//	}
		//	return CreateExcessARPaymentRecord;
		//}
		// vbPorter upgrade warning: lngBill As int	OnWrite(int, double)
		//public static bool ARBillPending_6(int lngBill, short intPending = 0, bool boolValue = false)
		//{
		//	return ARBillPending(ref lngBill, intPending, boolValue);
		//}

		//public static bool ARBillPending_26(int lngBill, short intPending = 0, bool boolValue = false)
		//{
		//	return ARBillPending(ref lngBill, intPending, boolValue);
		//}

		//public static bool ARBillPending(ref int lngBill, short intPending = 0, bool boolValue = false)
		//{
		//	bool ARBillPending = false;
		//	// this function can find the bill in question and return true if there are pending transactions
		//	// against it, set the astrisk or automatically set it
		//	int lngBillRow = 0;
		//	string strBill;
		//	int lngRW = 0;
		//	string strTest = "";
		//	int lngStartRow = 0;
		//	strBill = lngBill.ToString();
		//	lngStartRow = 1;
		//	lngBillRow = frmARCLStatus.InstancePtr.GRID.FindRow(strBill, 1, frmARCLStatus.InstancePtr.lngGRIDColInvoiceNumber);
		//	if (lngBillRow >= 0)
		//	{
		//		switch (intPending)
		//		{
		//			case 0:
		//				{
		//					// this function just tells if there is an asterisk in the grid
		//					if (frmARCLStatus.InstancePtr.GRID.TextMatrix(lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColPending) == "*")
		//					{
		//						ARBillPending = true;
		//					}
		//					else
		//					{
		//						ARBillPending = false;
		//					}
		//					break;
		//				}
		//			case 1:
		//				{
		//					// this function will check the payment grid and set the asterisk accordingly (automatic setting)
		//					TRY_AGAIN:
		//					;
		//					lngRW = frmARCLStatus.InstancePtr.vsPayments.FindRow(strBill, lngStartRow, frmARCLStatus.InstancePtr.lngGRIDCOLTree);
		//					if (lngRW != -1)
		//					{
		//						if (frmARCLStatus.InstancePtr.vsPayments.TextMatrix(lngRW, 3) == strTest)
		//						{
		//							ARBillPending = true;
		//							frmARCLStatus.InstancePtr.GRID.TextMatrix(lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColPending, "*");
		//							frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode), frmARCLStatus.InstancePtr.lngGRIDColPending, "*");
		//							frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmARCLStatus.InstancePtr.lngGRIDColPending, "*");
		//						}
		//						else
		//						{
		//							ARBillPending = false;
		//							frmARCLStatus.InstancePtr.GRID.TextMatrix(lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//							frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode), frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//							frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//							// check for more
		//							lngStartRow = lngRW + 1;
		//							goto TRY_AGAIN;
		//						}
		//					}
		//					else
		//					{
		//						ARBillPending = false;
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode), frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//					}
		//					break;
		//				}
		//			case 2:
		//				{
		//					// this will set the asterisk in the grid depending on the value passed in
		//					if (boolValue)
		//					{
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColPending, "*");
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode), frmARCLStatus.InstancePtr.lngGRIDColPending, "*");
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmARCLStatus.InstancePtr.lngGRIDColPending, "*");
		//					}
		//					else
		//					{
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode), frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//						frmARCLStatus.InstancePtr.GRID.TextMatrix(frmARCLStatus.InstancePtr.GRID.FindRow("=", lngBillRow, frmARCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmARCLStatus.InstancePtr.lngGRIDColPending, " ");
		//					}
		//					break;
		//				}
		//			case 3:
		//				{
		//					// this function will check to see if there is a pending transaction in the payment grid for the bill that was passed in
		//					if (frmARCLStatus.InstancePtr.vsPayments.FindRow(strBill, 1, frmARCLStatus.InstancePtr.lngGRIDCOLTree) != -1)
		//					{
		//						ARBillPending = true;
		//					}
		//					else
		//					{
		//						ARBillPending = false;
		//					}
		//					break;
		//				}
		//		}
		//		//end switch
		//	}
		//	frmARCLStatus.InstancePtr.GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, frmARCLStatus.InstancePtr.lngGRIDColPending, frmARCLStatus.InstancePtr.GRID.Rows - 1, frmARCLStatus.InstancePtr.lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	frmARCLStatus.InstancePtr.txtTotalPendingDue.Text = Strings.Format(TotalARPendingPaymentsDue(), "#,##0.00");
		//	return ARBillPending;
		//}

		//private static double TotalARPendingPaymentsDue()
		//{
		//	double TotalARPendingPaymentsDue = 0;
		//	// this function will total the payments that are pending and return the value
		//	double dblTotal;
		//	int i;
		//	dblTotal = 0;
		//	for (i = 0; i <= frmARCLStatus.InstancePtr.vsPayments.Rows - 1; i++)
		//	{
		//		if (Conversion.Val(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(i, frmARCLStatus.InstancePtr.lngPayGridColTotal)) != 0)
		//			dblTotal += FCConvert.ToDouble(frmARCLStatus.InstancePtr.vsPayments.TextMatrix(i, frmARCLStatus.InstancePtr.lngPayGridColTotal));
		//	}
		//	TotalARPendingPaymentsDue = FCUtils.Round(dblTotal, 2);
		//	return TotalARPendingPaymentsDue;
		//}

		//public static void CheckAllARPending()
		//{
		//	// this will check all bills and update the "Pending" marker
		//	int lngR;
		//	for (lngR = 1; lngR <= frmARCLStatus.InstancePtr.GRID.Rows - 1; lngR++)
		//	{
		//		if (frmARCLStatus.InstancePtr.GRID.RowOutlineLevel(lngR) == 1)
		//		{
		//			if (Conversion.Val(frmARCLStatus.InstancePtr.GRID.TextMatrix(lngR, 2)) != 0)
		//			{
		//				ARBillPending_26(FCConvert.ToInt32(Conversion.Val(frmARCLStatus.InstancePtr.GRID.TextMatrix(lngR, frmARCLStatus.InstancePtr.lngGRIDColInvoiceNumber))), 1, true);
		//			}
		//		}
		//	}
		//}

		//public static void ShowARInterestPaidThroughDate(ref string strTag)
		//{
		//	// this will show a messagebox with the effective interest date and the bill
		//	clsDRWrapper rsDate = new clsDRWrapper();
		//	string strMessage = "";
		//	rsDate.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " ORDER BY AccountKey", modExtraModules.strARDatabase);
		//	if (rsDate.EndOfFile() != true)
		//	{
		//		strMessage = "Invoice - Interest Paid Through Date" + "\r\n";
		//		do
		//		{
		//			strMessage += modGlobal.PadToString_8(rsDate.Get_Fields_String("InvoiceNumber"), 8) + " - " + Strings.Format(rsDate.Get_Fields_DateTime("IntPaidDate"), "MM/dd/yyyy") + "\r\n";
		//			rsDate.MoveNext();
		//		}
		//		while (!rsDate.EndOfFile());
		//	}
		//	MessageBox.Show(strMessage, strTag, MessageBoxButtons.OK, MessageBoxIcon.Information);
		//}

		//public static void AdjustARPaymentAmounts(ref double dblP, ref double dblT, ref double dblI, ref double dblPNeed, ref double dblTNeed, ref double dblINeed, ref double dblPPay, ref double dblTPay, ref double dblIPay)
		//{
		//	double dblTot;
		//	dblTot = dblP + dblT + dblI;
		//	// interest first
		//	if (dblINeed >= dblTot)
		//	{
		//		// if the need is greater than what is left
		//		dblIPay = dblTot;
		//		// then take all of it and set the total to 0
		//		dblTot = 0;
		//	}
		//	else
		//	{
		//		dblIPay = dblINeed;
		//		// else take what you need and leave the difference
		//		dblTot -= dblINeed;
		//		// is dblTot
		//	}
		//	// tax third
		//	if (dblTNeed >= dblTot)
		//	{
		//		dblTPay = dblTot;
		//		dblTot = 0;
		//	}
		//	else
		//	{
		//		dblTPay = dblTNeed;
		//		dblTot -= dblTNeed;
		//	}
		//	// principal last
		//	if (dblPNeed >= dblTot)
		//	{
		//		dblPPay = dblTot;
		//		dblTot = 0;
		//	}
		//	else
		//	{
		//		dblPPay = dblPNeed;
		//		dblTot -= dblPNeed;
		//	}
		//}

		//public static int GetARAccountNumber(ref int lngKey)
		//{
		//	int GetARAccountNumber = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsMaster = new clsDRWrapper();
		//		rsMaster.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strARDatabase);
		//		if (!rsMaster.EndOfFile())
		//		{
		//			// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
		//			GetARAccountNumber = FCConvert.ToInt32(rsMaster.Get_Fields("AccountNumber"));
		//		}
		//		else
		//		{
		//			GetARAccountNumber = 0;
		//		}
		//		return GetARAccountNumber;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		frmWait.InstancePtr.Unload();
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return GetARAccountNumber;
		//}

		public static void CreateARCHGINTForBill_6(int lngBill, int lngAcct, double dblAmt, DateTime? dtRTD = null)
		{
			CreateARCHGINTForBill(ref lngBill, ref lngAcct, ref dblAmt, dtRTD);
		}

		public static void CreateARCHGINTForBill(ref int lngBill, ref int lngAcct, ref double dblAmt, DateTime? dtRTDTemp = null)
		{
			DateTime dtRTD = dtRTDTemp ?? DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will create a charged interest line for the bill passed in
				// and will not update bill record
				clsDRWrapper rsP = new clsDRWrapper();
				clsDRWrapper rsB = new clsDRWrapper();
				rsP.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strARDatabase);
				rsB.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBill));
				rsP.AddNew();
				rsP.Set_Fields("AccountKey", lngAcct);
				rsP.Set_Fields("ActualAccountNumber", GetAccountNumberFromAccountKey(ref lngAcct));
				rsP.Set_Fields("BillKey", lngBill);
				rsP.Set_Fields("ActualSystemDate", DateTime.Today);
				rsP.Set_Fields("EffectiveInterestDate", DateTime.Today);
				rsP.Set_Fields("BillNumber", rsB.Get_Fields_Int32("BillNumber"));
				rsP.Set_Fields("InvoiceNumber", rsB.Get_Fields_String("InvoiceNumber"));
				// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
				rsP.Set_Fields("BillType", rsB.Get_Fields("BillType"));
				if (dtRTD.ToOADate() == 0)
				{
					// if nothing gets passed in then use the current date
					rsP.Set_Fields("RecordedTransactionDate", DateTime.Today);
				}
				else
				{
					rsP.Set_Fields("RecordedTransactionDate", dtRTD);
				}
				rsP.Set_Fields("Reference", "CHGINT");
				rsP.Set_Fields("Code", "I");
				rsP.Set_Fields("Interest", dblAmt * -1);
				rsP.Set_Fields("CashDrawer", "Y");
				rsP.Set_Fields("GeneralLedger", "Y");
				rsP.Update();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + FCConvert.ToString(Information.Err(ex).Number) + ".", "Error Creating Charged Interest", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		//public static bool ARCheckForFlatInterest(ref int lngAccountID)
		//{
		//	bool ARCheckForFlatInterest = false;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// This will check the whole account for the need of the flat rate to be added
		//		clsDRWrapper rsFlat = new clsDRWrapper();
		//		clsDRWrapper rsRate = new clsDRWrapper();
		//		double dblTotal = 0;
		//		double dblTempInt = 0;
		//		double dblCurrentInterest = 0;
		//		double dblCHGInt = 0;
		//		int lngKey = 0;
		//		ARCheckForFlatInterest = true;
		//		rsFlat.OpenRecordset("SELECT FlatInterestAmount FROM UtilityBilling", modExtraModules.strARDatabase);
		//		if (!rsFlat.EndOfFile())
		//		{
		//			if (rsFlat.Get_Fields_Decimal("FlatInterestAmount") > 0)
		//			{
		//				dblCHGInt = FCConvert.ToDouble(rsFlat.Get_Fields_Decimal("FlatInterestAmount"));
		//			}
		//			else
		//			{
		//				dblCHGInt = 0;
		//				return ARCheckForFlatInterest;
		//			}
		//		}
		//		else
		//		{
		//			dblCHGInt = 0;
		//			return ARCheckForFlatInterest;
		//		}
		//		rsFlat.OpenRecordset("SELECT * FROM Bill WHERE BillStatus = 'B' AND ActualAccountNumber = " + FCConvert.ToString(lngAccountID) + " AND IsNull(IntAdded, 0) = 0", modExtraModules.strARDatabase);
		//		while (!rsFlat.EndOfFile())
		//		{
		//			dblCurrentInterest = 0;
		//			dblTempInt = 0;
		//			rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsFlat.Get_Fields_Int32("BillNumber"), modExtraModules.strARDatabase);
		//			if (!rsRate.EndOfFile())
		//			{
		//				if (DateAndTime.DateDiff("D", (DateTime)rsRate.Get_Fields_DateTime("IntStart"), DateTime.Today) >= 0)
		//				{
		//					dblTotal = modARCalculations.CalculateAccountAR(rsFlat, DateTime.Today, ref dblTempInt);
		//					if (dblTotal > 0)
		//					{
		//						lngKey = FCConvert.ToInt32(rsFlat.Get_Fields_Int32("ID"));
		//						dblCurrentInterest = dblCHGInt;
		//					}
		//				}
		//			}
		//			if (dblCurrentInterest != 0)
		//			{
		//				// this will edit the bill records charged interest
		//				rsFlat.Edit();
		//				// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//				rsFlat.Set_Fields("IntAdded", rsFlat.Get_Fields("IntAdded") - dblCurrentInterest);
		//				rsFlat.Update();
		//				CreateARCHGINTForBill_6(lngKey, FCConvert.ToInt32(rsFlat.Get_Fields_Int32("AccountKey")), dblCurrentInterest);
		//			}
		//		}
		//		ARCheckForFlatInterest = true;
		//		return ARCheckForFlatInterest;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		ARCheckForFlatInterest = false;
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking For Flat Interest", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return ARCheckForFlatInterest;
		//}

		//public static int GetBKFromInvoice_2(string strInvoiceNumber)
		//{
		//	return GetBKFromInvoice(ref strInvoiceNumber);
		//}

		//public static int GetBKFromInvoice(ref string strInvoiceNumber)
		//{
		//	int GetBKFromInvoice = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsBK = new clsDRWrapper();
		//		rsBK.OpenRecordset("SELECT ID FROM Bill WHERE InvoiceNumber = '" + strInvoiceNumber + "'", modExtraModules.strARDatabase);
		//		if (!rsBK.EndOfFile())
		//		{
		//			GetBKFromInvoice = FCConvert.ToInt32(rsBK.Get_Fields_Int32("ID"));
		//		}
		//		else
		//		{
		//			GetBKFromInvoice = 0;
		//		}
		//		return GetBKFromInvoice;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Bill ID", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return GetBKFromInvoice;
		//}

		//public static int GetBTFromInvoice_2(string strInvoiceNumber)
		//{
		//	return GetBTFromInvoice(ref strInvoiceNumber);
		//}

		//public static int GetBTFromInvoice(ref string strInvoiceNumber)
		//{
		//	int GetBTFromInvoice = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		GetBTFromInvoice = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strInvoiceNumber, 3, 3))));
		//		return GetBTFromInvoice;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return GetBTFromInvoice;
		//}

		//public static int GetRKFromInvoice_2(string strInvoiceNumber)
		//{
		//	return GetRKFromInvoice(ref strInvoiceNumber);
		//}

		//public static int GetRKFromInvoice(ref string strInvoiceNumber)
		//{
		//	int GetRKFromInvoice = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsRK = new clsDRWrapper();
		//		rsRK.OpenRecordset("SELECT BillNumber FROM Bill WHERE InvoiceNumber = '" + strInvoiceNumber + "'", modExtraModules.strARDatabase);
		//		if (!rsRK.EndOfFile())
		//		{
		//			GetRKFromInvoice = FCConvert.ToInt32(rsRK.Get_Fields_Int32("BillNumber"));
		//		}
		//		else
		//		{
		//			GetRKFromInvoice = 0;
		//		}
		//		return GetRKFromInvoice;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return GetRKFromInvoice;
		//}
		// vbPorter upgrade warning: 'Return' As string	OnWriteFCConvert.ToInt16(
		public static string GetBillDescription(int lngBillType)
		{
			string GetBillDescription = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBT = new clsDRWrapper();
				rsBT.OpenRecordset("SELECT TypeTitle FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(lngBillType), modExtraModules.strARDatabase);
				if (!rsBT.EndOfFile())
				{
					GetBillDescription = FCConvert.ToString(rsBT.Get_Fields_String("TypeTitle"));
				}
				else
				{
					GetBillDescription = FCConvert.ToString(0);
				}
				return GetBillDescription;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Bill Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetBillDescription;
		}

		public static int GetAccountKeyFromAccountNumber(int lngAcct)
		{
			int GetAccountKeyFromAccountNumber = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBT = new clsDRWrapper();
				rsBT.OpenRecordset("SELECT ID FROM CustomerMaster WHERE CustomerID = " + FCConvert.ToString(lngAcct), modExtraModules.strARDatabase);
				if (!rsBT.EndOfFile())
				{
					GetAccountKeyFromAccountNumber = FCConvert.ToInt32(rsBT.Get_Fields_Int32("ID"));
				}
				else
				{
					GetAccountKeyFromAccountNumber = 0;
				}
				return GetAccountKeyFromAccountNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Account ID", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAccountKeyFromAccountNumber;
		}

		public static int GetAccountNumberFromAccountKey_2(int lngAcct)
		{
			return GetAccountNumberFromAccountKey(ref lngAcct);
		}

		public static int GetAccountNumberFromAccountKey(ref int lngAcct)
		{
			int GetAccountNumberFromAccountKey = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBT = new clsDRWrapper();
				rsBT.OpenRecordset("SELECT CustomerID FROM CustomerMaster WHERE ID = " + FCConvert.ToString(lngAcct), modExtraModules.strARDatabase);
				if (!rsBT.EndOfFile())
				{
					GetAccountNumberFromAccountKey = FCConvert.ToInt32(rsBT.Get_Fields_Int32("CustomerID"));
				}
				else
				{
					GetAccountNumberFromAccountKey = 0;
				}
				return GetAccountNumberFromAccountKey;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Account Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAccountNumberFromAccountKey;
		}

		public static string GetAccountEmail(int lngAcct)
		{
			string GetAccountEmail = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBT = new clsDRWrapper();
				// rsBT.OpenRecordset "SELECT EMail FROM CustomerMaster WHERE ID = " & lngAcct, strARDatabase
				rsBT.OpenRecordset("SELECT p.EMail FROM CustomerMaster as c CROSS APPLY " + rsBT.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE ID = " + FCConvert.ToString(lngAcct));
				if (!rsBT.EndOfFile())
				{
					GetAccountEmail = FCConvert.ToString(rsBT.Get_Fields_String("EMail"));
				}
				else
				{
					GetAccountEmail = "";
				}
				return GetAccountEmail;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Account Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAccountEmail;
		}
		// vbPorter upgrade warning: lngCurrentAccountAR As int	OnWrite(int, string)
		// vbPorter upgrade warning: lngCurrKey As int	OnWrite(double, int)
		//public static DateTime GetLastARInterestDate(int lngCurrentAccountAR, int lngBill, DateTime dtPassDate, int lngCurrKey)
		//{
		//	DateTime GetLastARInterestDate = System.DateTime.Now;
		//	// MAL@20070920: New function to update to the last Interest Paid Date after reversal
		//	bool blnSuccess;
		//	bool blnContinue;
		//	DateTime dtNewDate = DateTime.FromOADate(0);
		//	int lngRK = 0;
		//	int lngAcctKey = 0;
		//	int lngBillKey;
		//	int lngCIKey = 0;
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsRK = new clsDRWrapper();
		//	clsDRWrapper rsUpdate = new clsDRWrapper();
		//	blnContinue = true;
		//	// Optimistic
		//	rsBill.OpenRecordset("SELECT ID FROM CustomerMaster WHERE CustomerID = " + FCConvert.ToString(lngCurrentAccountAR), modExtraModules.strARDatabase);
		//	if (rsBill.RecordCount() > 0)
		//	{
		//		lngAcctKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
		//		blnContinue = true;
		//	}
		//	else
		//	{
		//		blnContinue = false;
		//	}
		//	lngBillKey = lngBill;
		//	if (blnContinue)
		//	{
		//		rsBill.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND CHGINTNumber <> 0", modExtraModules.strARDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			lngCIKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("CHGINTNumber"));
		//			if (lngCurrKey == 0)
		//			{
		//				lngCurrKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
		//			}
		//			blnContinue = true;
		//		}
		//		else
		//		{
		//			lngCurrKey = lngCurrKey;
		//			blnContinue = true;
		//		}
		//	}
		//	if (blnContinue)
		//	{
		//		rsBill.OpenRecordset("SELECT Max(EffectiveInterestDate) as LastDate, AccountKey FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND ID <> " + FCConvert.ToString(lngCIKey) + " AND ID <> " + FCConvert.ToString(lngCurrKey) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND RecordedTransactionDate < '" + FCConvert.ToString(dtPassDate) + "' GROUP BY AccountKey", modExtraModules.strARDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			// TODO: Field [LastDate] not found!! (maybe it is an alias?)
		//			dtNewDate = (DateTime)rsBill.Get_Fields("LastDate");
		//		}
		//		else
		//		{
		//			// No previous payments - use Rate Record
		//			rsRK.OpenRecordset("SELECT BillNumber FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strARDatabase);
		//			if (rsRK.RecordCount() > 0)
		//			{
		//				lngRK = FCConvert.ToInt32(rsRK.Get_Fields_Int32("BillNumber"));
		//				rsRK.OpenRecordset("SELECT IntStart FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strARDatabase);
		//				if (rsRK.RecordCount() > 0)
		//				{
		//					dtNewDate = (DateTime)rsRK.Get_Fields_DateTime("IntStart");
		//				}
		//			}
		//		}
		//		if (!fecherFoundation.FCUtils.IsNull(dtNewDate))
		//		{
		//			GetLastARInterestDate = dtNewDate;
		//		}
		//	}
		//	else
		//	{
		//		// No other payments - use Rate Record
		//		rsRK.OpenRecordset("SELECT BillNumber FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strARDatabase);
		//		if (rsRK.RecordCount() > 0)
		//		{
		//			lngRK = FCConvert.ToInt32(rsRK.Get_Fields_Int32("BillNumber"));
		//			rsRK.OpenRecordset("SELECT IntStart FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strARDatabase);
		//			if (rsRK.RecordCount() > 0)
		//			{
		//				dtNewDate = (DateTime)rsRK.Get_Fields_DateTime("IntStart");
		//			}
		//		}
		//		if (!fecherFoundation.FCUtils.IsNull(dtNewDate))
		//		{
		//			GetLastARInterestDate = dtNewDate;
		//		}
		//	}
		//	return GetLastARInterestDate;
		//}

		//public static Decimal CalculateARChargedInterestNotYetSaved(ref int lngPassedBillKey)
		//{
		//	Decimal CalculateARChargedInterestNotYetSaved = 0;
		//	clsDRWrapper rsPayment = new clsDRWrapper();
		//	int counter;
		//	// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		//	Decimal curTotal;
		//	curTotal = 0;
		//	for (counter = 1; counter <= MAX_ARPAYMENTS; counter++)
		//	{
		//		// this finds a place in the array
		//		if (Statics.ARPaymentArray[counter].BillKey == 0)
		//		{
		//			// to store the new payment record
		//			CalculateARChargedInterestNotYetSaved = curTotal;
		//			return CalculateARChargedInterestNotYetSaved;
		//		}
		//		else if (Statics.ARPaymentArray[counter].BillKey == lngPassedBillKey)
		//		{
		//			if (Statics.ARPaymentArray[counter].CHGINTNumber != 0)
		//			{
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(Statics.ARPaymentArray[counter].CHGINTNumber));
		//				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//				curTotal -= FCConvert.ToDecimal(rsPayment.Get_Fields("Interest"));
		//			}
		//		}
		//	}
		//	return CalculateARChargedInterestNotYetSaved;
		//}

		//public static void Format_ARvsPeriod()
		//{
		//	int wid = 0;
		//	wid = frmARCLStatus.InstancePtr.vsPeriod.WidthOriginal;
		//	frmARCLStatus.InstancePtr.vsPeriod.ColWidth(0, FCConvert.ToInt32(wid * 0.34));
		//	frmARCLStatus.InstancePtr.vsPeriod.ColWidth(1, FCConvert.ToInt32(wid * 0.61));
		//	frmARCLStatus.InstancePtr.vsPeriod.ColWidth(2, 0);
		//	frmARCLStatus.InstancePtr.vsPeriod.ColWidth(3, 0);
		//	frmARCLStatus.InstancePtr.vsPeriod.ColWidth(4, 0);
		//}

		//public static int CreateBlankARBill(ref int lngCustomerID)
		//{
		//	int CreateBlankARBill = 0;
		//	int intError = 0;
		//	// this function will create a blank bill on the account passed in
		//	// using the lear passed for RE or PP whatever is passed into strType
		//	// then it will return the billkey that was just created.
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		int lngBK = 0;
		//		clsDRWrapper rsMaster = new clsDRWrapper();
		//		clsDRWrapper rsNew = new clsDRWrapper();
		//		intError = 2;
		//		rsMaster.OpenRecordset("SELECT * FROM CustomerMaster WHERE CustomerID = " + FCConvert.ToString(lngCustomerID), modExtraModules.strARDatabase);
		//		if (!rsMaster.EndOfFile())
		//		{
		//			rsNew.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngCustomerID) + " AND IsNull(BillNumber, 0) = 0", modExtraModules.strARDatabase);
		//			if (!rsNew.EndOfFile())
		//			{
		//				MessageBox.Show("There is already a prepayment bill created.", "Bill Not Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//				lngBK = -1;
		//				return CreateBlankARBill;
		//			}
		//			intError = 4;
		//			rsNew.AddNew();
		//			rsNew.Update();
		//			intError = 5;
		//			intError = 7;
		//			rsNew.Set_Fields("AccountKey", GetAccountKeyFromAccountNumber(lngCustomerID));
		//			rsNew.Set_Fields("ActualAccountNumber", lngCustomerID);
		//			intError = 10;
		//			rsNew.Set_Fields("CreationDate", DateTime.Today);
		//			rsNew.Set_Fields("LastUpdatedDate", DateTime.Today);
		//			rsNew.Set_Fields("BillStatus", "D");
		//			rsNew.Set_Fields("BillNumber", 0);
		//			intError = 11;
		//			rsNew.Set_Fields("BName", rsMaster.Get_Fields_String("Name"));
		//			rsNew.Set_Fields("InvoiceNumber", "0");
		//			rsNew.Set_Fields("BAddress1", rsMaster.Get_Fields_String("Address1"));
		//			rsNew.Set_Fields("BAddress2", rsMaster.Get_Fields_String("Address2"));
		//			rsNew.Set_Fields("BAddress3", rsMaster.Get_Fields_String("Address3"));
		//			rsNew.Set_Fields("BCity", rsMaster.Get_Fields_String("City"));
		//			rsNew.Set_Fields("BState", rsMaster.Get_Fields_String("State"));
		//			rsNew.Set_Fields("BZip", rsMaster.Get_Fields_String("Zip"));
		//			rsNew.Set_Fields("BZip4", rsMaster.Get_Fields_String("Zip4"));
		//			intError = 19;
		//			if (rsNew.Update())
		//			{
		//				lngBK = FCConvert.ToInt32(rsNew.Get_Fields_Int32("ID"));
		//			}
		//			else
		//			{
		//				lngBK = -1;
		//			}
		//		}
		//		else
		//		{
		//			MessageBox.Show("Cannot find account " + FCConvert.ToString(lngCustomerID) + " in the AR Master file.", "Bill Not Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//			lngBK = -1;
		//		}
		//		CreateBlankARBill = lngBK;
		//		return CreateBlankARBill;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		CreateBlankARBill = -1;
		//		// denotes an error or noncompletion of the creation of the bill
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Blank Bill - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return CreateBlankARBill;
		//}

		//private static short PrePayARBill()
		//{
		//	short PrePayARBill = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this function will return the next billing year and create a bill record if needed
		//		clsDRWrapper rsRT = new clsDRWrapper();
		//		clsDRWrapper rsBR = new clsDRWrapper();
		//		int intTempYear;
		//		int lngNewBill = 0;
		//		rsBR.OpenRecordset("SELECT * FROM (" + Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(Statics.lngCurrentCustomerIDAR) + " AND IsNull(BillNumber, 0)  = 0", modExtraModules.strARDatabase);
		//		if (rsBR.BeginningOfFile() != true && rsBR.EndOfFile() != true)
		//		{
		//			// return the year only
		//			// do nothing
		//		}
		//		else
		//		{
		//			// create a new billing record with this account number and year
		//			lngNewBill = CreateBlankARBill(ref Statics.lngCurrentCustomerIDAR);
		//			// Doevents
		//			rsBR.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngNewBill), modExtraModules.strARDatabase);
		//		}
		//		rsBR.Edit();
		//		rsBR.Set_Fields("IntPaidDate", DateTime.Today);
		//		rsBR.Update();
		//		frmARCLStatus.InstancePtr.cmbCode.SelectedIndex = frmARCLStatus.InstancePtr.cmbCode.Items.Count - 1;
		//		PrePayARBill = FCConvert.ToInt16(rsBR.Get_Fields_Int32("ID"));
		//		// Return the Year
		//		return PrePayARBill;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Blank Bill", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return PrePayARBill;
		//}

		//public static void VerifyARBillAmounts(int lngBillKey)
		//{
		//	// Verify that All Bill Amounts Match the Current Payments
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsPayment = new clsDRWrapper();
		//	double dblPrinOwed_Bill = 0;
		//	double dblTaxOwed_Bill = 0;
		//	double dblIntAdded_Bill = 0;
		//	double dblPrinPaid_Bill = 0;
		//	double dblIntPaid_Bill = 0;
		//	double dblTaxPaid_Bill = 0;
		//	double dblPrinPaid_Pymt = 0;
		//	double dblIntPaid_Pymt = 0;
		//	double dblTaxPaid_Pymt = 0;
		//	double dblIntChg_Pymt = 0;
		//	double dblTotalOwed = 0;
		//	double dblTotalPaid = 0;
		//	double dblTotal;
		//	// Non-Liened Records
		//	rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey) + " ORDER BY ActualAccountNumber, ID", modExtraModules.strARDatabase);
		//	if (rsBill.RecordCount() > 0)
		//	{
		//		// Reset Values
		//		dblPrinPaid_Pymt = 0;
		//		dblIntPaid_Pymt = 0;
		//		dblTaxPaid_Pymt = 0;
		//		dblIntChg_Pymt = 0;
		//		//JEI:IT257: avoid invalid cast exception
		//		dblPrinOwed_Bill = FCConvert.ToDouble(rsBill.Get_Fields_Decimal("PrinOwed"));
		//		dblTaxOwed_Bill = FCConvert.ToDouble(rsBill.Get_Fields_Decimal("TaxOwed"));
		//		// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//		dblIntAdded_Bill = FCConvert.ToDouble(rsBill.Get_Fields("IntAdded")) * -1;
		//		dblTotalOwed = dblPrinOwed_Bill + dblTaxOwed_Bill + dblIntAdded_Bill;
		//		//JEI:IT257: avoid invalid cast exception
		//		// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//		dblPrinPaid_Bill = FCConvert.ToDouble(rsBill.Get_Fields("PrinPaid"));
		//		// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//		dblTaxPaid_Bill = FCConvert.ToDouble(rsBill.Get_Fields("TaxPaid"));
		//		// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//		dblIntPaid_Bill = FCConvert.ToDouble(rsBill.Get_Fields("IntPaid"));
		//		dblTotalPaid = dblPrinPaid_Bill + dblTaxPaid_Bill + dblIntPaid_Bill;
		//		dblTotal = dblTotalOwed - dblTotalPaid;
		//		// Get Charged Interest to Compare
		//		rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND (Reference = 'CHGINT' Or UPPER(Reference) = 'INTEREST') AND ReceiptNumber >= 0", modExtraModules.strARDatabase);
		//		if (rsPayment.RecordCount() > 0)
		//		{
		//			while (!rsPayment.EndOfFile())
		//			{
		//				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//				dblIntChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields("Interest")) * -1;
		//				rsPayment.MoveNext();
		//			}
		//		}
		//		rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Reference <> 'CHGINT' AND UPPER(Reference) <> 'INTEREST' AND Code <> '3' AND Code <> 'L' AND ReceiptNumber >= 0", modExtraModules.strARDatabase);
		//		if (rsPayment.RecordCount() > 0)
		//		{
		//			while (!rsPayment.EndOfFile())
		//			{
		//				//JEI:IT257: avoid exception
		//				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//				dblPrinPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
		//				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//				dblIntPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields("Interest"));
		//				// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//				dblTaxPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
		//				rsPayment.MoveNext();
		//			}
		//			// Principal ; Only Change the Paid Amount, Should Not Change the Charged Amount
		//			if (dblPrinPaid_Bill != dblPrinPaid_Pymt)
		//			{
		//				rsBill.Edit();
		//				rsBill.Set_Fields("PrinPaid", dblPrinPaid_Pymt);
		//				rsBill.Update();
		//			}
		//			// Charged Interest ; Check All Pieces (Paid and Charged)
		//			if (dblIntAdded_Bill != dblIntChg_Pymt)
		//			{
		//				rsBill.Edit();
		//				rsBill.Set_Fields("InterestAdded", dblIntChg_Pymt * -1);
		//				rsBill.Update();
		//			}
		//			// Check Payments
		//			if (dblIntPaid_Bill != dblIntPaid_Pymt)
		//			{
		//				rsBill.Edit();
		//				rsBill.Set_Fields("IntPaid", dblIntPaid_Pymt);
		//				rsBill.Update();
		//			}
		//		}
		//		else
		//		{
		//			// Do Nothing - No Payments, Nothing to Fix
		//		}
		//	}
		//	else
		//	{
		//		// The bill was not found
		//	}
		//}

		public class StaticVariables
		{
            public bool gboolShowLastARAccountInCR;
        }

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
