﻿using System;
using System.Drawing;
using fecherFoundation;
using TWSharedLibrary;
#if TWBD0000
using TWBD0000;


#elif TWPY0000
using TWPY0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for rptNewSampleCheck.
	/// </summary>
	public partial class rptNewSampleCheck : BaseSectionReport
	{
		// nObj = 1
		//   0	rptNewSampleCheck	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CheckUseCol = 2;
		const int CheckDescCol = 1;
		const int CheckXCol = 3;
		const int CheckYCol = 4;
		private float intCheckTop;
		private int intNumFields;
		private int intCheckType;
		// 0=Regular 1=Laser 2=Custom
		private int intLaserLineAdjustment;
		private bool boolDontChooseFormat;

		public rptNewSampleCheck()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "SampleCheck";
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static rptNewSampleCheck InstancePtr
		{
			get
			{
				return (rptNewSampleCheck)Sys.GetInstance(typeof(rptNewSampleCheck));
			}
		}

		protected rptNewSampleCheck _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(int lngFormatID, bool modalDialog)
		{
			modCustomChecks.Statics.intCustomCheckFormatToUse = lngFormatID;
			boolDontChooseFormat = true;
			//this.ShowDialog();
			frmReportViewer.InstancePtr.Init(this, "", 1, showModal: modalDialog);
		}

		private void ActiveReport_ReportStart(object sender, System.EventArgs e)
		{
			int X;
			string strFont = "";
			string strTemp = "";
			dynamic /*clsReportPrinterFunctions*/clsPrt/*= new clsReportPrinterFunctions()*/;
			GrapeCity.ActiveReports.SectionReportModel.TextBox ctl;
			clsDRWrapper rsFieldInfo = new clsDRWrapper();
			clsDRWrapper rsData = new clsDRWrapper();
			GrapeCity.ActiveReports.SectionReportModel.Shape ctl2;
			string strDatabaseName = "";
			if (Strings.Trim(App.EXEName) == "TWPY0000")
			{
				// GET THE AMOUNT OF LINE ADJUSTMENT
				strDatabaseName = "TWPY0000.vb1";
				rsFieldInfo.OpenRecordset("Select * from tblCheckFormat", "TWPY0000.vb1");
				if (rsFieldInfo.EndOfFile())
				{
					intLaserLineAdjustment = 0;
				}
				else
				{
					// TODO: Field [LaserAlignment] not found!! (maybe it is an alias?)
					intLaserLineAdjustment = FCConvert.ToInt32(Math.Round(Conversion.Val(rsFieldInfo.Get_Fields("LaserAlignment"))));
				}
				// GET THE TYPE OF CHECK
				rsFieldInfo.OpenRecordset("Select * from tblDefaultInformation", "TWPY0000.vb1");
				intCheckType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsFieldInfo.Get_Fields_String("CheckType"))));
				if (intCheckType == 0)
				{
					// THIS IS A REGULAR CHECK
					intCheckTop = 4860/1440F;
				}
				else if (intCheckType == 1)
				{
					// THIS IS A LASER CHECK
					intCheckTop = 4860/1440F;
				}
				else if (intCheckType == 2)
				{
					// THIS IS A CUSTOM CHECK
					if (!boolDontChooseFormat)
					{
						frmSelectCustomCheckFormat.InstancePtr.Init();
					}
					rsFieldInfo.OpenRecordset("Select * from CustomChecks WHERE ID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse), "TWPY0000.vb1");
					if (rsFieldInfo.EndOfFile())
					{
						intCheckTop = 4860/ 1440F;
					}
					else
					{
						if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsFieldInfo.Get_Fields_String("CheckPosition")))) == "T")
						{
							// SET THE PROPERTIES FOR A TOP CHECK
							intCheckTop = 0;
							ShapeTop.Top += 4860 / 1440F;
							lblTop.Top += 4860 / 1440F;
							if (FCConvert.ToBoolean(rsFieldInfo.Get_Fields_Boolean("UseDoubleStub")))
							{
								ShapeTop.Height = (ShapeBottom.Top + ShapeBottom.Height) - ShapeTop.Top;
								lblTop.Top = ShapeTop.Top + (ShapeTop.Height / 2);
								lblTop.Text = "Double Sized Stub";
								ShapeBottom.Visible = false;
								lblBottom.Visible = false;
							}
						}
						else if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsFieldInfo.Get_Fields_String("CheckPosition")))) == "M")
						{
							// SET THE PROPERTIES FOR A MIDDLE CHECK
							intCheckTop = 4860/ 1440F;
						}
						else if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsFieldInfo.Get_Fields_String("CheckPosition")))) == "B")
						{
							intCheckTop = 9720 / 1440F;
							ShapeBottom.Top -= (4860 / 1440F);
							lblBottom.Top -= (4860 / 1440F);
							if (FCConvert.ToBoolean(rsFieldInfo.Get_Fields_Boolean("UseDoubleStub")))
							{
								ShapeTop.Height = (ShapeBottom.Top + ShapeBottom.Height) - ShapeTop.Top;
								lblTop.Top = ShapeTop.Top + (ShapeTop.Height / 2);
								lblTop.Text = "Double Sized Stub";
								ShapeBottom.Visible = false;
								lblBottom.Visible = false;
							}
						}
						else
						{
							// ASSUME THAT THIS IS A MIDDLE CHECK
							intCheckTop = 4860 / 1440F;
						}
					}
				}
			}
			else if (App.EXEName == "TWBD0000")
			{
				// GET THE AMOUNT OF LINE ADJUSTMENT
				strDatabaseName = "TWBD0000.vb1";
				rsFieldInfo.OpenRecordset("Select * from Budgetary", strDatabaseName);
				intLaserLineAdjustment = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CHECKLASERADJUSTMENT"))));
				// GET THE TYPE OF CHECK
				intCheckType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsFieldInfo.Get_Fields_Int32("CheckFormat"))));
				if (intCheckType == 1)
				{
					// THIS IS A REGULAR CHECK
					intCheckTop = 4860 / 1440F;
					this.PageSettings.Margins.Top = 0;
					this.PageSettings.Margins.Bottom = 0;
					this.PageSettings.Margins.Left = 432 / 1440F;
					this.PageSettings.Margins.Right = 0;
				}
				else if (intCheckType == 2)
				{
					// THIS IS A LASER CHECK
					intCheckTop = 4860 / 1440F;
					this.PageSettings.Margins.Top = 432 / 1440F;
					this.PageSettings.Margins.Bottom = 0;
					this.PageSettings.Margins.Left = 432 / 1440F;
					this.PageSettings.Margins.Right = 0;
				}
				else if (intCheckType == 3)
				{
					// THIS IS A LASER CHECK
					intCheckTop = 0;
					ShapeTop.Top += 4860 / 1440F;
					lblTop.Top += 4860 / 1440F;
					this.PageSettings.Margins.Top = 432 / 1440F;
					this.PageSettings.Margins.Bottom = 0;
					this.PageSettings.Margins.Left = 432 / 1440F;
					this.PageSettings.Margins.Right = 0;
				}
				else if (intCheckType == 4)
				{
					// THIS IS A CUSTOM CHECK
					if (!boolDontChooseFormat)
					{
						frmSelectCustomCheckFormat.InstancePtr.Init();
					}
					rsFieldInfo.OpenRecordset("Select * from CustomChecks WHERE ID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse), strDatabaseName);
					if (rsFieldInfo.EndOfFile())
					{
						intCheckTop = 4860 / 1440F;
					}
					else
					{
						if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsFieldInfo.Get_Fields_String("CheckPosition")))) == "T")
						{
							// SET THE PROPERTIES FOR A TOP CHECK
							intCheckTop = 0;
							ShapeTop.Top += 4860 / 1440F;
							lblTop.Top += 4860 / 1440F;
							if (FCConvert.ToBoolean(rsFieldInfo.Get_Fields_Boolean("UseDoubleStub")))
							{
								ShapeTop.Height = (ShapeBottom.Top + ShapeBottom.Height) - ShapeTop.Top;
								lblTop.Top = ShapeTop.Top + (ShapeTop.Height / 2);
								lblTop.Text = "Double Sized Stub";
								ShapeBottom.Visible = false;
								lblBottom.Visible = false;
							}
						}
						else if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsFieldInfo.Get_Fields_String("CheckPosition")))) == "M")
						{
							// SET THE PROPERTIES FOR A MIDDLE CHECK
							intCheckTop = 4860 / 1440F;
						}
						else if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsFieldInfo.Get_Fields_String("CheckPosition")))) == "B")
						{
							// SET THE PROPERTIES FOR A BOTTOM CHECK
							intCheckTop = 9720 / 1440F;
							ShapeBottom.Top -= 4860 / 1440F;
							lblBottom.Top -= 4860 / 1440F;
							if (FCConvert.ToBoolean(rsFieldInfo.Get_Fields_Boolean("UseDoubleStub")))
							{
								ShapeTop.Height = (ShapeBottom.Top + ShapeBottom.Height) - ShapeTop.Top;
								lblTop.Top = ShapeTop.Top + (ShapeTop.Height / 2);
								lblTop.Text = "Double Sized Stub";
								ShapeBottom.Visible = false;
								lblBottom.Visible = false;
							}
						}
						else
						{
							// ASSUME THAT THIS IS A MIDDLE CHECK
							intCheckTop = 4860 / 1440F;
						}
					}
				}
			}
			// FIGURE IN THE LINE ADJUSTMENT
			intCheckTop += (intLaserLineAdjustment * 240) / 1440F;
			ShapeTop.Top += (intLaserLineAdjustment * 240) / 1440F;
			ShapeBottom.Top += (intLaserLineAdjustment * 240) / 1440F;
			lblTop.Top += (intLaserLineAdjustment * 240) / 1440F;
			lblBottom.Top += (intLaserLineAdjustment * 240) / 1440F;
			// GET THE FIELDS
			if ((App.EXEName == "TWBD0000" && intCheckType == 4) || (App.EXEName == "TWPY0000" && intCheckType == 2))
			{
				rsFieldInfo.OpenRecordset("Select * from CustomCheckFields WHERE FormatID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse), strDatabaseName);
			}
			else
			{
				rsFieldInfo.OpenRecordset("SELECT * FROM CustomChecks WHERE Description = 'Default'");
				if (rsFieldInfo.EndOfFile() != true && rsFieldInfo.BeginningOfFile() != true)
				{
					modCustomChecks.Statics.intCustomCheckFormatToUse = FCConvert.ToInt32(rsFieldInfo.Get_Fields_Int32("ID"));
					rsFieldInfo.OpenRecordset("Select * from CustomCheckFields WHERE FormatID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse), strDatabaseName);
				}
			}
			// CREATE THE FIELDS
			intNumFields = 0;
			do
			{
				if (FCConvert.ToBoolean(rsFieldInfo.Get_Fields_Boolean("Include")))
				{
					// then include the field
					intNumFields += 1;
                    strTemp = "CustomField" + FCConvert.ToString(intNumFields);
                    ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>(strTemp);
					if ((App.EXEName == "TWPY0000" && intCheckType == 2) || (App.EXEName == "TWBD0000" && intCheckType == 4))
					{
						// custom check
						ctl.Top =
                            ((240 * FCConvert.ToSingle(Conversion.Val(rsFieldInfo.Get_Fields_Double("YPosition"))) /
                              1440F)) + intCheckTop;
						ctl.Left = (144 * FCConvert.ToSingle(Conversion.Val(rsFieldInfo.Get_Fields_Double("XPosition")))) ;
					}
					else
					{
						// other
						ctl.Top = ((240 * FCConvert.ToSingle(Conversion.Val(rsFieldInfo.Get_Fields_Double("DefaultYPosition"))) / 1440F)) + intCheckTop;
						ctl.Left = (144 * FCConvert.ToSingle(Conversion.Val(rsFieldInfo.Get_Fields_Double("DefaultXPosition")))) / 1440F;
					}
					ctl.WordWrap = false;
					ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
					// PUT THE SHAPE CONTROL AROUND EACH ITEM
					rsData.OpenRecordset("SELECT * FROM CustomCheckFields WHERE FormatID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse) + " AND Description = '" + rsFieldInfo.Get_Fields_String("Description") + "'", strDatabaseName);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						if (Conversion.Val(rsData.Get_Fields_Int16("FieldHeight")) > 0 && Conversion.Val(rsData.Get_Fields_Int16("FieldWidth")) > 0)
						{
							ctl.Height = FCConvert.ToSingle(Conversion.Val(rsData.Get_Fields_Int16("FieldHeight"))) / 1440F;
							ctl.Width = FCConvert.ToSingle(Conversion.Val(rsData.Get_Fields_Int16("FieldWidth"))) / 1440F;
							Detail.Controls.Add(ctl2 = new GrapeCity.ActiveReports.SectionReportModel.Shape());
							ctl2.Top = ctl.Top;
							ctl2.Left = ctl.Left;
							ctl2.Height = ctl.Height;
							ctl2.Width = ctl.Width;
						}
						else
						{
							ctl.Width = 2880 / 1440F;
							ctl.Height = 240 / 1440F;
						}
					}
					//strTemp = "CustomField" + FCConvert.ToString(intNumFields);
					//ctl.Name = strTemp;
					Fields.Add(ctl.Name);
					if (strFont != string.Empty)
					{
						ctl.Font = new Font(strFont, ctl.Font.Size);
					}
					ctl.Text = rsFieldInfo.Get_Fields_String("Description");
				}
				rsFieldInfo.MoveNext();
			}
			while (rsFieldInfo.EndOfFile() != true);
			rsFieldInfo.Dispose();
			rsData.Dispose();
        }

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
		}

		
	}
}
