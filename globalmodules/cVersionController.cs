﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	public class cVersionController
	{
		//=========================================================
		public cVersionInfo GetVersion(string strTableName = "DBVersion", string strDBName = "")
		{
			cVersionInfo GetVersion = null;
			cVersionInfo tVer = new cVersionInfo();
			LoadVersion(ref tVer, strTableName, strDBName);
			GetVersion = tVer;
			return GetVersion;
		}

		public cVersionInfo MakeVersion(int intMajor, int intMinor = 0, int intRevision = 0, int intBuild = 0)
		{
			cVersionInfo MakeVersion = null;
			cVersionInfo tVer = new cVersionInfo();
			tVer.Major = intMajor;
			tVer.Minor = intMinor;
			tVer.Revision = intRevision;
			tVer.Build = intBuild;
			MakeVersion = tVer;
			return MakeVersion;
		}

		public void LoadVersion(ref cVersionInfo tVer, string strTableName = "DBVersion", string strDBName = "")
		{
			if (!(tVer == null))
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from " + strTableName, strDBName);
				if (!rsLoad.EndOfFile())
				{
					tVer.Major = rsLoad.Get_Fields_Int32("Major");
					tVer.Minor = rsLoad.Get_Fields_Int32("Minor");
					tVer.Build = rsLoad.Get_Fields_Int32("Build");
					tVer.Revision = rsLoad.Get_Fields_Int32("Revision");
				}
			}
		}

		public void SaveVersion(ref cVersionInfo tVer, string strTableName = "DBVersion", string strDBName = "")
		{
			if (!(tVer == null))
			{
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from " + strTableName, strDBName);
				if (rsSave.EndOfFile())
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("Major", tVer.Major);
				rsSave.Set_Fields("Minor", tVer.Minor);
				rsSave.Set_Fields("Revision", tVer.Revision);
				rsSave.Set_Fields("Build", tVer.Build);
				rsSave.Update();
			}
		}

		public void SaveVersionString(ref cVersionInfo tVer, string strTableName = "Customize", string strFieldName = "Version", string strDBName = "")
		{
			if (!(tVer == null))
			{
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from " + strTableName, strDBName);
				if (rsSave.EndOfFile())
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields(strFieldName, tVer.VersionString);
				rsSave.Update();
			}
		}

		public cVersionInfo GetVersionFromString(string strTableName = "DBVersion", string strFieldName = "Version", string strDBName = "")
		{
			cVersionInfo GetVersionFromString = null;
			cVersionInfo tVer = new cVersionInfo();
			LoadVersionFromString(ref tVer, strTableName, strFieldName, strDBName);
			GetVersionFromString = tVer;
			return GetVersionFromString;
		}

		public void LoadVersionFromString(ref cVersionInfo tVer, string strTableName = "DBVersion", string strFieldName = "Version", string strDBName = "")
		{
			string strVer;
			strVer = GetVersionString(strTableName, strFieldName, strDBName);
			tVer.Major = 0;
			tVer.Minor = 0;
			tVer.Build = 0;
			tVer.Revision = 0;
			if (strVer != "")
			{
				string[] strAry = null;
				strAry = Strings.Split(strVer, ".", -1, CompareConstants.vbBinaryCompare);
				tVer.Major = FCConvert.ToInt32(strAry[0]);
				if (Information.UBound(strAry, 1) > 0)
				{
					tVer.Minor = FCConvert.ToInt32(strAry[1]);
					if (Information.UBound(strAry, 1) > 1)
					{
						tVer.Revision = FCConvert.ToInt32(strAry[2]);
						if (Information.UBound(strAry, 1) > 2)
						{
							tVer.Build = FCConvert.ToInt32(strAry[3]);
						}
					}
				}
			}
		}

		public string GetVersionString(string strTableName = "Customize", string strFieldName = "Version", string strDBName = "")
		{
			string GetVersionString = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strReturn;
			strReturn = "";
			rsLoad.OpenRecordset("select " + strFieldName + " from " + strTableName, strDBName);
			if (!rsLoad.EndOfFile())
			{
				strReturn = FCConvert.ToString(rsLoad.Get_Fields(strFieldName));
			}
			GetVersionString = strReturn;
			return GetVersionString;
		}
	}
}
