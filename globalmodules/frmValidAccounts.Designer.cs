﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmLoadValidAccounts.
	/// </summary>
	partial class frmLoadValidAccounts : BaseForm
	{
		public fecherFoundation.FCGrid vsAcct;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLoadValidAccounts));
            this.vsAcct = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 390);
            this.BottomPanel.Size = new System.Drawing.Size(977, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsAcct);
            this.ClientArea.Size = new System.Drawing.Size(997, 504);
            this.ClientArea.Controls.SetChildIndex(this.vsAcct, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(997, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(163, 28);
            this.HeaderText.Text = "Valid Accounts";
            // 
            // vsAcct
            // 
            this.vsAcct.AllowBigSelection = false;
            this.vsAcct.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsAcct.Cols = 10;
            this.vsAcct.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
            this.vsAcct.Location = new System.Drawing.Point(30, 30);
            this.vsAcct.Name = "vsAcct";
            this.vsAcct.Rows = 1;
            this.vsAcct.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
            this.vsAcct.Size = new System.Drawing.Size(954, 360);
            this.vsAcct.TabIndex = 1001;
            this.vsAcct.Click += new System.EventHandler(this.vsAcct_Click);
            this.vsAcct.DoubleClick += new System.EventHandler(this.vsAcct_DblClick);
            this.vsAcct.KeyDown += new Wisej.Web.KeyEventHandler(this.vsAcct_KeyDown);
            this.vsAcct.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsAcct_KeyPress);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(438, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmLoadValidAccounts
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(997, 564);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmLoadValidAccounts";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Valid Accounts";
            this.Load += new System.EventHandler(this.frmLoadValidAccounts_Load);
            this.Resize += new System.EventHandler(this.frmLoadValidAccounts_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLoadValidAccounts_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
	}
}
