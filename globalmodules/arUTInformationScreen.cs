//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace Global
{
    /// <summary>
    /// Summary description for arUTInformationScreen.
    /// </summary>
    public partial class arUTInformationScreen : FCSectionReport
    {

        public arUTInformationScreen()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null)
                _InstancePtr = this;
        }

        public static arUTInformationScreen InstancePtr
        {
            get
            {
                return (arUTInformationScreen)Sys.GetInstance(typeof(arUTInformationScreen));
            }
        }

        protected arUTInformationScreen _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            base.Dispose(disposing);
        }

        // nObj = 1
        //   0	arUTInformationScreen	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               11/22/2005              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               11/22/2005              *
        // ********************************************************
        int lngRow;
        bool boolFirstTime;
        int lngMaxRow;
        int lngMaxCol;
        bool boolWater;

        public void Init(int lngPassRW, int lngPassCL, bool boolPassWater)
        {
            lngMaxRow = lngPassRW;
            lngMaxCol = lngPassCL;
            boolWater = boolPassWater;

            frmReportViewer.InstancePtr.Init(this);
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (boolFirstTime)
            {
                eArgs.EOF = true;
            }
            else
            {
				eArgs.EOF = false;
                boolFirstTime = true;
            }
        }

        private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
        {
            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        this.Close();
                        break;
                    }
            } //end switch
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
            SetHeaderString();

            lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
            lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            lblMuniName.Text = modGlobalConstants.Statics.MuniName;

        }

		private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

		private void PageHeader_Format(object sender, EventArgs e)
        {
            lblPage.Text = "Page " + this.PageNumber;
        }

        private void BindFields()
        {
            // this will print the row from the grid

            if (frmUTCLStatus.InstancePtr.vsRateInfo.Cols > 0)
            {
                fldOut10.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0);
                fldOut11.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0);
                fldOut12.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0);
                fldOut13.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0);
                fldOut14.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0);
                fldOut15.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0);
                fldOut16.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0);
                fldOut17.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0);
                fldOut18.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0);
                fldOut19.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0);
                fldOut110.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0);
                fldOut111.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0);
                fldOut112.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0);
                fldOut113.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0);
                fldOut114.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0);
                fldOut115.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0);
                fldOut116.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0);
                fldOut117.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0);
                fldOut118.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0);
                if (frmUTCLStatus.InstancePtr.vsRateInfo.Rows > 19)
                {
                    fldOut119.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0);
                }
                // fldOut120.Text = .TextMatrix(20, 0)
                // fldOut121.Text = .TextMatrix(21, 0)
            }

            if (frmUTCLStatus.InstancePtr.vsRateInfo.Cols > 1)
            {
                fldOut20.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1);
                fldOut21.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1);
                fldOut22.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1);
                fldOut23.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1);
                fldOut24.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1);
                fldOut25.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1);
                fldOut26.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1);
                fldOut27.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1);
                fldOut28.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1);
                fldOut29.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1);
                fldOut210.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1);
                fldOut211.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1);
                fldOut212.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1);
                fldOut213.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1);
                fldOut214.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1);
                fldOut215.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1);
                fldOut216.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1);
                fldOut217.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1);
                fldOut218.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1);
                if (frmUTCLStatus.InstancePtr.vsRateInfo.Rows > 19)
                {
                    fldOut219.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1);
                }
                // fldOut220.Text = .TextMatrix(20, 1)
                // fldOut221.Text = .TextMatrix(21, 1)
            }

            if (frmUTCLStatus.InstancePtr.vsRateInfo.Cols > 3)
            {
                fldOut30.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 3);
                fldOut31.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 3);
                fldOut32.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 3);
                fldOut33.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 3);
                fldOut34.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 3);
                fldOut35.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 3);
                fldOut36.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 3);
                fldOut37.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 3);
                fldOut38.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 3);
                fldOut39.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 3);
                fldOut310.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 3);
                fldOut311.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 3);
                fldOut312.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 3);
                fldOut313.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3);
                fldOut314.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3);
                fldOut315.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3);
                fldOut316.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3);
                fldOut317.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3);
                fldOut318.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3);
                if (frmUTCLStatus.InstancePtr.vsRateInfo.Rows > 19)
                {
                    fldOut319.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 3);
                }
                // fldOut320.Text = .TextMatrix(20, 3)
                // fldOut321.Text = .TextMatrix(21, 3)
            }
            if (frmUTCLStatus.InstancePtr.vsRateInfo.Cols > 4)
            {
                fldOut40.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4);
                fldOut41.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4);
                fldOut42.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4);
                fldOut43.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4);
                fldOut44.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4);
                fldOut45.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 4);
                fldOut46.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 4);
                fldOut47.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 4);
                fldOut48.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 4);
                fldOut49.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 4);
                fldOut410.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 4);
                fldOut411.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4);
                fldOut412.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 4);
                fldOut413.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 4);
                fldOut414.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 4);
                fldOut415.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4);
                fldOut416.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 4);
                fldOut417.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 4);
                fldOut418.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 4);
                if (frmUTCLStatus.InstancePtr.vsRateInfo.Rows > 19)
                {
                    fldOut419.Text = frmUTCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 4);
                }
                // fldOut420.Text = .TextMatrix(20, 4)
                // fldOut421.Text = .TextMatrix(21, 4)
            }

        }

        private int FindNextVisibleRow(ref int lngR)
        {
            int FindNextVisibleRow = 0;
            // this function will start at the row number passed in and return the next row to print
            // this function will return -1 if there are no more rows to show
            int lngNext;
            int intLvl;
            bool boolFound;
            int lngMax = 0;

            boolFound = false;
            lngNext = lngR + 1;
            if (boolWater)
            {

                lngMax = frmUTCLStatus.InstancePtr.WGRID.Rows;
                if (lngNext < lngMax)
                {
                    boolFound = false;
                    while (!(boolFound || frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(lngNext) < 2))
                    {
                        if (frmUTCLStatus.InstancePtr.WGRID.IsCollapsed(LastParentRow(lngNext)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                        {
                            lngNext += 1;
                            boolFound = false;
                            if (lngNext >= lngMax)
                            {
                                return FindNextVisibleRow;
                            }
                        }
                        else
                        {
                            boolFound = true;
                        }
                    }

                    FindNextVisibleRow = lngNext;
                }

            }
            else
            {

                lngMax = frmUTCLStatus.InstancePtr.SGRID.Rows;
                if (lngNext < lngMax)
                {
                    boolFound = false;
                    while (!(boolFound || frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(lngNext) < 2))
                    {
                        if (frmUTCLStatus.InstancePtr.SGRID.IsCollapsed(LastParentRow(lngNext)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                        {
                            lngNext += 1;
                            boolFound = false;
                            if (lngNext >= lngMax)
                            {
                                return FindNextVisibleRow;
                            }
                        }
                        else
                        {
                            boolFound = true;
                        }
                    }

                    FindNextVisibleRow = lngNext;
                }

            }
            if (FindNextVisibleRow == 0) FindNextVisibleRow = -1;
            return FindNextVisibleRow;
        }

        // vbPorter upgrade warning: CurRow As Variant --> As int
        public int LastParentRow(int CurRow)
        {
            int LastParentRow = 0;
            int l;
            int curLvl;
            l = CurRow;
            if (boolWater)
            {
                curLvl = frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(l);
                l -= 1;
                while (frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(l) > 1)
                {
                    l -= 1;
                }
                LastParentRow = l;
            }
            else
            {
                curLvl = frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(l);
                l -= 1;
                while (frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(l) > 1)
                {
                    l -= 1;
                }
                LastParentRow = l;
            }
            return LastParentRow;
        }

        private void SetHeaderString()
        {
            // this will set the correct header for this account
            string strTemp = "";
            int lngAcctNum;

            // DJW@01092013 TROUT-727 Changed lngAcctNum to reflect account number not accoutn key
            lngAcctNum = frmUTCLStatus.InstancePtr.CurrentAccount;

            strTemp += " Account " + FCConvert.ToString(lngAcctNum) + " " + frmUTCLStatus.InstancePtr.fraRateInfo.Text;

            strTemp += "\r\n" + "as of " + Strings.Format(modUTStatusPayments.Statics.UTEffectiveDate, "MM/dd/yyyy");
            lblHeader.Text = strTemp;
            lblName.Text = "Name: " + frmUTCLStatus.InstancePtr.lblOwnersName.Text;
            lblLocation.Text = "Location: " + frmUTCLStatus.InstancePtr.lblLocation.Text;
        }

        private void arUTInformationScreen_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //arUTInformationScreen.Caption	= "Account Information";
            //arUTInformationScreen.Icon	= "arUTInformationScreen.dsx":0000";
            //arUTInformationScreen.Left	= 0;
            //arUTInformationScreen.Top	= 0;
            //arUTInformationScreen.Width	= 11880;
            //arUTInformationScreen.Height	= 8595;
            //arUTInformationScreen.StartUpPosition	= 3;
            //arUTInformationScreen.SectionData	= "arUTInformationScreen.dsx":058A;
            //End Unmaped Properties
        }
    }
}