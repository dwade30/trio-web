﻿namespace Global
{
    partial class ClientPrintingSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                fecherFoundation.Sys.ClearInstance(this);
            }

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new Wisej.Web.Button();
            this.label1 = new Wisej.Web.Label();
            this.groupBox1 = new Wisej.Web.GroupBox();
            this.groupBox2 = new Wisej.Web.GroupBox();
            this.button2 = new Wisej.Web.Button();
            this.label4 = new Wisej.Web.Label();
            this.label3 = new Wisej.Web.Label();
            this.textBox1 = new Wisej.Web.TextBox();
            this.label2 = new Wisej.Web.Label();
            this.groupBox3 = new Wisej.Web.GroupBox();
            this.button3 = new Wisej.Web.Button();
            this.label5 = new Wisej.Web.Label();
            this.button4 = new Wisej.Web.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.AppearanceKey = "acceptButton";
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(30, 410);
            this.button1.Name = "button1";
            this.button1.Shortcut = Wisej.Web.Shortcut.F12;
            this.button1.Size = new System.Drawing.Size(138, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "Continue";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = Wisej.Web.Cursors.Hand;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(0, 86, 255);
            this.label1.Location = new System.Drawing.Point(20, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Download TRIO Assistant Application";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.AppearanceKey = "groupBoxLeftBorder";
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(30, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(635, 66);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.Text = "Step 1";
            // 
            // groupBox2
            // 
            this.groupBox2.AppearanceKey = "groupBoxLeftBorder";
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Location = new System.Drawing.Point(30, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(635, 113);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.Text = "Step 2";
            // 
            // button2
            // 
            this.button2.AppearanceKey = "actionButton";
            this.button2.Location = new System.Drawing.Point(518, 64);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 40);
            this.button2.TabIndex = 3;
            this.button2.Text = "Set KEY";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(269, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Start TRIO Assistant App and get the unique key";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "TRIO Assistant KEY";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(147, 65);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(365, 40);
            this.textBox1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(444, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "TRIO Assistant is not configured. Please follow the steps to enable TRIO Assistant";
            // 
            // groupBox3
            // 
            this.groupBox3.AppearanceKey = "groupBoxLeftBorder";
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(30, 270);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(635, 112);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.Text = "Step 3";
            // 
            // button3
            // 
            this.button3.AppearanceKey = "actionButton";
            this.button3.Location = new System.Drawing.Point(20, 65);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(229, 40);
            this.button3.TabIndex = 4;
            this.button3.Text = "Re-Check configuration";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(229, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Click Connect on the TRIO Assistant App";
            // 
            // button4
            // 
            this.button4.AppearanceKey = "actionButton";
            this.button4.Location = new System.Drawing.Point(177, 410);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(381, 40);
            this.button4.TabIndex = 6;
            this.button4.Text = "Cancel TRIO Assistant Setup";
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // ClientPrintingSetup
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.CancelButton = this.button4;
            this.ClientSize = new System.Drawing.Size(711, 475);
            this.CloseBox = false;
            this.Controls.Add(this.button4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ClientPrintingSetup";
            this.Text = "TRIO Assistant Setup";
            
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Wisej.Web.Button button1;
        private Wisej.Web.Label label1;
        private Wisej.Web.GroupBox groupBox1;
        private Wisej.Web.GroupBox groupBox2;
        private Wisej.Web.Button button2;
        private Wisej.Web.Label label4;
        private Wisej.Web.Label label3;
        private Wisej.Web.TextBox textBox1;
        private Wisej.Web.Label label2;
        private Wisej.Web.GroupBox groupBox3;
        private Wisej.Web.Button button3;
        private Wisej.Web.Label label5;
        private Wisej.Web.Button button4;
    }
}