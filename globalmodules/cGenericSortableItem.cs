﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cGenericSortableItem
	{
		//=========================================================
		private FCCollection collSortValues = new FCCollection();
		private object ItemValue;
		private int lngIdentifier;

		public int Identifier
		{
			set
			{
				lngIdentifier = value;
			}
			get
			{
				int Identifier = 0;
				Identifier = lngIdentifier;
				return Identifier;
			}
		}

		public FCCollection SortValues
		{
			get
			{
				FCCollection SortValues = null;
				SortValues = collSortValues;
				return SortValues;
			}
		}

		public object Item
		{
			set
			{
				ItemValue = value;
			}
			get
			{
				object Item = null;
				Item = ItemValue;
				return Item;
			}
		}

		public void ClearSortValues()
		{
			foreach (object tRec in collSortValues)
			{
				collSortValues.Remove(1);
			}
			// tRec
		}
	}
}
