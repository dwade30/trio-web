﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication.AccountGroups;
using SharedApplication.AccountGroups.Commands;
using SharedApplication.UtilityBilling.Commands;
using TWSharedLibrary;
using Wisej.Web;

#if TWPP0000
using TWPP0000;
using modGlobalVariables = TWPP0000.modGlobal;


#elif TWBL0000
using TWBL0000;


#elif TWRE0000
using TWRE0000;


#elif TWUT0000
using modGlobalVariables = TWUT0000.modMain;


#elif TWCR0000
using modGlobalVariables = TWCR0000.modGlobal;


#elif TWCL0000
using modGlobalVariables = TWCL0000.modGlobal;


#elif TWCL0001
using modGlobalVariables = TWCL0001.modGlobal;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmGroupBalance.
	/// </summary>
	public partial class frmGroupBalance : BaseForm
	{
		private System.ComponentModel.IContainer components;

		public frmGroupBalance()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGroupBalance InstancePtr
		{
			get
			{
				return (frmGroupBalance)Sys.GetInstance(typeof(frmGroupBalance));
			}
		}

		protected frmGroupBalance _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private string strREDB = string.Empty;
		private string strPPDB = string.Empty;
		private string strGNDB = string.Empty;
		private string strUTDB = string.Empty;
		private string strCLDB = string.Empty;
        private DateTime effectiveDate;

		private void frmGroupBalance_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridBalance();
		}

		private void frmGroupBalance_Resize(object sender, System.EventArgs e)
		{
			ResizeGridBalance();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void Init(int lngGroupID, DateTime? effectiveDate = null, string strREDBToUse = "twre0000.vb1", string strPPDBToUse = "twpp0000.vb1", string strGNDBToUse = "CentralData", string strUTDBToUse = "TWUT0000.vb1", string strCLDBToUse = "twcl0000.vb1")
		{
			strREDB = strREDBToUse;
			strPPDB = strPPDBToUse;
			strGNDB = strGNDBToUse;
			strUTDB = strUTDBToUse;
			strCLDB = strCLDBToUse;

            this.effectiveDate = effectiveDate ?? DateTime.Today;

			if (Loadinfo(ref lngGroupID))
			{
				this.Show(App.MainForm);
			}
			else
			{
				Close();
			}
		}

		private void ResizeGridBalance()
		{
			int GridWidth = 0;
			GridWidth = GridBalance.WidthOriginal;
			GridBalance.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth));
			GridBalance.ColWidth(1, FCConvert.ToInt32(0.1 * GridWidth));
			GridBalance.ColWidth(2, FCConvert.ToInt32(0.6 * GridWidth));
			GridBalance.ColWidth(3, FCConvert.ToInt32(0.16 * GridWidth));
		}

		private void SetupGridBalance()
		{
			GridBalance.TextMatrix(0, 0, "Module");
			GridBalance.TextMatrix(0, 1, "Account");
			GridBalance.TextMatrix(0, 2, "Name");
			GridBalance.TextMatrix(0, 3, "Balance");
            //FC:FINAL:AM:#3399 - set columns alignment
            GridBalance.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridBalance.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

		private bool Loadinfo(ref int lngGroupID)
		{
			bool Loadinfo = false;
			// groupnumber is the ID not the changeable group number
			// does not check booleans since they may not have the modules anymore, but still have entries in collections
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsModule = new clsDRWrapper();
			clsDRWrapper rsTest = new clsDRWrapper();
			decimal dblTotalBalance;
			decimal dblBalance = 0;
			string strSql = "";
			// Dim boolBasisAndOverpayOK As Boolean
			int intBasisBak;
            int groupNumber;
			double dblOPIntRateBak;
			try
			{
				// On Error GoTo ErrorHandler
				Loadinfo = false;
				clsLoad.OpenRecordset("select * from groupmaster where ID = " + FCConvert.ToString(lngGroupID), "CentralData");
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("Group with id = " + FCConvert.ToString(lngGroupID) + " not found.", "Missing Group", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return Loadinfo;
				}
				framGroup.Text = "Group " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("GroupNumber")));
                groupNumber = clsLoad.Get_Fields_Int32("GroupNumber");

				lblName.Text = "";
				lblAddress1.Text = "";
				lblAddress2.Text = "";
				lblStreetLetter.Text = "";
				lblStreetNumber.Text = "";
				lblStreetName.Text = "";
				lblCity.Text = "";
				lblState.Text = "";
				lblZip.Text = "";
				lblZip4.Text = "";
				GridBalance.Rows = 1;
				// kk03242014 trocl-816  UT and CL could have different Basis and Overpay Interest settings
				// but both CL and UT use the same global variables
				// Also need the Interest method for UT
				intBasisBak = modGlobalVariables.Statics.gintBasis;
				#if TWPP0000
								                dblOPIntRateBak = modPPGN.Statics.dblOverPayRate;
#elif TWUT0000
								                dblOPIntRateBak = modExtraModules.Statics.dblOverPayRate;
#elif TWCR0000
												dblOPIntRateBak = modExtraModules.Statics.dblOverPayRate;
#elif TWCL0000
												dblOPIntRateBak = modExtraModules.Statics.dblOverPayRate;
#elif TWCL0001
												dblOPIntRateBak = modExtraModules.Statics.dblOverPayRate;
#else
				dblOPIntRateBak = modGlobalVariables.Statics.dblOverPayRate;
				#endif
				// boolBasisAndOverpayOK = False
				dblTotalBalance = 0;

				clsLoad.OpenRecordset("select * from MODULEASSOCIATION where groupnumber = " + FCConvert.ToString(lngGroupID) + " order by MODULE,account", "CentralData");
				if (!clsLoad.EndOfFile())
				{
					var accountGroup = StaticSettings.GlobalCommandDispatcher.Send(new CalculateAccountGroup(groupNumber, effectiveDate)).Result;
					while (!clsLoad.EndOfFile())
					{
						if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) == "RE")
						{
                            if (rsTest.DBExists("RealEstate"))
							{
								// trocls-118 fix fso error 1.5.18
								// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
								clsModule.OpenRecordset("select * from master CROSS APPLY " + clsModule.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(OwnerPartyID, NULL, 'RE', rsAccount) as p where rsaccount = " + clsLoad.Get_Fields("account") + " and rscard = 1", strREDB);
								if (clsModule.EndOfFile())
								{
									// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
									MessageBox.Show("Could not find account RE " + clsLoad.Get_Fields("account") + "\r\n" + "Information not complete.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return Loadinfo;
								}
								if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("groupprimary")))
								{
									lblName.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("DeedName1")));
									lblAddress1.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Address1")));
									lblAddress2.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Address2")));
									lblCity.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("City")));
									// TODO: Field [PartyState] not found!! (maybe it is an alias?)
									lblState.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields("PartyState")));
									lblZip.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Zip")));
									lblZip4.Text = "";
									lblStreetNumber.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("rslocnumalph")));
									lblStreetName.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("rslocstreet")));
									lblStreetLetter.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("rslocapt")));
								}
								// look in collections db for balance
								// If Not File.Exists(strCLDB) Then
								if (!rsTest.DBExists("Collections"))
								{
									// trocls-118 fix fso error 1.5.18
									MessageBox.Show("Could not find Collections database.  Balance information not complete.", "Missing Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return Loadinfo;
								}

                                var balanceInfo = accountGroup.AccountSummaries.FirstOrDefault(x =>
                                    x.AccountType == AccountBillingType.RealEstate &&
                                    x.Account == clsLoad.Get_Fields_Int32("account"));

                                if (balanceInfo != null)
                                {
                                    dblBalance = balanceInfo.Balance;
                                }
                                else
                                {
                                    dblBalance = 0;
                                }
								
								GridBalance.AddItem(Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) + "\t" + clsLoad.Get_Fields("account") + "\t" + clsModule.Get_Fields_String("FullNameLF") + "\t" + Strings.Format(dblBalance, "#,###,##0.00"));
								dblTotalBalance += dblBalance;
							}
							else
							{
								MessageBox.Show("Could not find Real Estate database." + "\r\n" + "Information not complete.", "Missing database", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return Loadinfo;
							}
						}
						else if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) == "PP")
						{
							if (rsTest.DBExists("PersonalProperty"))
							{
								// trocls-118 fix fso error 1.5.18
								// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
								clsModule.OpenRecordset("select * from ppmaster CROSS APPLY " + clsModule.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'PP', Account) as p where account = " + clsLoad.Get_Fields("account"), strPPDB);
								if (!clsModule.EndOfFile())
								{
								}
								else
								{
									// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
									MessageBox.Show("Could not find account PP " + clsLoad.Get_Fields("account"), "Missing account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return Loadinfo;
								}
								if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("groupprimary")))
								{
									lblName.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("FullNameLF")));
									lblAddress1.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Address1")));
									lblAddress2.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Address2")));
									lblCity.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("City")));
									// TODO: Field [PartyState] not found!! (maybe it is an alias?)
									lblState.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields("PartyState")));
									lblZip.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Zip")));
									lblZip4.Text = "";
									lblStreetNumber.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_Int32("streetnumber")));
									lblStreetName.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("street")));
									lblStreetLetter.Text = "";
								}
								// look in collections db for balance
								// If Not File.Exists(strCLDB) Then
								if (!rsTest.DBExists("Collections"))
								{
									// trocls-118 fix fso error 1.5.18
									MessageBox.Show("Could not find Collections database.  Balance information not complete.", "Missing Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return Loadinfo;
								}
                                var balanceInfo = accountGroup.AccountSummaries.FirstOrDefault(x =>
                                    x.AccountType == AccountBillingType.PersonalProperty &&
                                    x.Account == clsLoad.Get_Fields_Int32("account"));

                                if (balanceInfo != null)
                                {
                                    dblBalance = balanceInfo.Balance;
                                }
                                else
                                {
                                    dblBalance = 0;
                                }

								GridBalance.AddItem(Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) + "\t" + clsLoad.Get_Fields("account") + "\t" + clsModule.Get_Fields_String("FullNameLF") + "\t" + Strings.Format(dblBalance, "#,###,##0.00"));
								dblTotalBalance += dblBalance;
							}
							else
							{
								MessageBox.Show("Could not find Personal Property database." + "\r\n" + "Information not complete.", "Missing database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return Loadinfo;
							}
						}
						else if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) == "UT")
						{
							// If File.Exists(strUTDB) Then
							if (rsTest.DBExists("UtilityBilling"))
							{
								// trocls-118 fix fso error 1.5.18
								// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
								clsModule.OpenRecordset("select * from master CROSS APPLY " + clsModule.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(OwnerPartyID, NULL, 'UT', AccountNumber) as p where accountnumber = " + clsLoad.Get_Fields("account"), strUTDB);
								if (!clsModule.EndOfFile())
								{
									if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("groupprimary")))
									{
										lblName.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("DeedName1")));
										lblAddress1.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Address1")));
										lblAddress2.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Address2")));
										lblCity.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("City")));
										// TODO: Field [PartyState] not found!! (maybe it is an alias?)
										lblState.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields("PartyState")));
										lblZip.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("Zip")));
										lblZip4.Text = "";
										lblStreetNumber.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_Int32("streetnumber")));
										lblStreetName.Text = Strings.Trim(FCConvert.ToString(clsModule.Get_Fields_String("streetname")));
										lblStreetLetter.Text = "";
									}

									var balanceInfo = accountGroup.AccountSummaries.FirstOrDefault(x =>
                                        x.AccountType == AccountBillingType.Utility &&
                                        x.Account == clsLoad.Get_Fields_Int32("account"));

                                    if (balanceInfo != null)
                                    {
                                        dblBalance = balanceInfo.Balance;
                                    }
                                    else
                                    {
                                        dblBalance = 0;
                                    }

									GridBalance.AddItem(Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) + "\t" + clsLoad.Get_Fields("account") + "\t" + clsModule.Get_Fields_String("FullNameLF") + "\t" + Strings.Format(dblBalance, "#,###,##0.00"));
                                    dblTotalBalance += dblBalance;
								}
								else
								{
									// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
									MessageBox.Show("Could not find account UT " + clsLoad.Get_Fields("account"), "Missing account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return Loadinfo;
								}
							}
							else
							{
								MessageBox.Show("Could not find Utility Billing database." + "\r\n" + "Information not complete.", "Missing database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return Loadinfo;
							}
						}
						clsLoad.MoveNext();
					}
				}
				if (GridBalance.Row > 16)
				{
					GridBalance.HeightOriginal = GridBalance.RowHeight(0) * 16 + 30;
				}
				else
				{
					GridBalance.HeightOriginal = GridBalance.RowHeight(0) * GridBalance.Rows + 30;
				}
				txtTotalBalance.Text = Strings.Format(dblTotalBalance, "#,###,##0.00");
				// Restore the original Basis and OverPay Int Rate
				modGlobalVariables.Statics.gintBasis = intBasisBak;
				#if TWPP0000
								                modPPGN.Statics.dblOverPayRate = dblOPIntRateBak;
#elif TWUT0000
								                modExtraModules.Statics.dblOverPayRate = dblOPIntRateBak;
#elif TWCR0000
												dblOPIntRateBak = modExtraModules.Statics.dblOverPayRate;
#elif TWCL0000
								                modExtraModules.Statics.dblOverPayRate = dblOPIntRateBak;
#elif TWCL0001
								                modExtraModules.Statics.dblOverPayRate = dblOPIntRateBak;
#else
				modGlobalVariables.Statics.dblOverPayRate = dblOPIntRateBak;
				#endif
				Loadinfo = true;
				return Loadinfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Loadinfo;
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM: use the report viewer
			//rptGroupBalance.InstancePtr.ShowDialog();
			frmReportViewer.InstancePtr.Init(rptGroupBalance.InstancePtr);
		}
	}
}
