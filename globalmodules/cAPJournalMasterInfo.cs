﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global
{
    public class cAPJournalMasterInfo
    {
        private String payableDate;
        public String Description { get; set; } = "";
        public int JournalNumber { get; set; } = 0;
        public int JournalMasterID { get; set; } = 0;
        public int BankID { get; set; } = 0;
        public int Period { get; set; } = 0;

        public String PayableDate
        {
            get
            {
                return payableDate; 
            }
            set
            {
                DateTime parsedDateTime;
                if (DateTime.TryParse(value,out parsedDateTime))
                {
                    payableDate = value;
                }
                else
                {
                    payableDate = "";
                }
            }
        }
    }
}
