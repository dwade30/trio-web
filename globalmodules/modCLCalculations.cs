﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Diagnostics;
using SharedApplication.Extensions;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#elif TWPP0000
using TWPP0000;
using modExtraModules = TWPP0000.modPPGN;
using modStatusPayments = TWPP0000.modPPGN;
using modGlobalVariables = TWPP0000.modGlobal;


#elif TWCR0000
using TWCR0000;


#elif TWBL0000
using TWBL0000;
using modExtraModules = TWBL0000.modGlobalVariables;
using modGlobal = TWBL0000.modGlobalVariables;
using modStatusPayments = TWBL0000.modGlobalVariables;


#elif TWRE0000
using TWRE0000;
using modStatusPayments = TWRE0000.modGlobalVariables;
using modExtraModules = TWRE0000.modGlobalVariables;
using modGlobal = TWRE0000.modGlobalVariables;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
using modStatusPayments = TWUT0000.modMain;
#endif
namespace Global
{
    public class modCLCalculations
    {
        // If this is true then the checkbox on the effective date change screen will default to true
        public static decimal CalculateInterest(decimal curAmount, DateTime dtInterestStartDate, DateTime dtDateNow, double dblRateAsDecimal, int intBasis)
        {
            double dblPerDiem = 0;
            return CalculateInterest(curAmount, dtInterestStartDate, dtDateNow, dblRateAsDecimal, intBasis, 0, 0, ref dblPerDiem);
        }

        public static decimal CalculateInterest(decimal curAmount, DateTime dtInterestStartDate, DateTime dtDateNow, double dblRateAsDecimal, int intBasis, int UsingStartDate/* = 0*/, double dblOverPaymentIntRate/*= 0*/, ref double dblPerDiem)
        {
            decimal CalculateInterest = 0;
            // VBto upgrade warning: dblPrincipalDue As double	OnWriteFCConvert.ToDouble(
            double dblPrincipalDue;
            int intDays;
            int lngPerDays;
            clsDRWrapper rsSettings = new clsDRWrapper();
            // MsgBox intDays
            dblPrincipalDue = FCConvert.ToDouble(curAmount * -1);
            intDays = intBasis;
            if (intDays == 0)
            {
                if (modGlobal.Statics.gintBasis == 0)
                {
                    rsSettings.OpenRecordset("SELECT Basis FROM Collections", modExtraModules.strCLDatabase);
                    if (rsSettings.EndOfFile() != true && rsSettings.BeginningOfFile() != true)
                    {
                        if (((rsSettings.Get_Fields_Int32("Basis"))) != 360 && ((rsSettings.Get_Fields_Int32("Basis"))) != 365)
                        {
                            FCMessageBox.Show("Can't determine number of days in a year to charge interest for.  This will default to 360.", MsgBoxStyle.Information, "Invalid Days Per Year");
                            intDays = 360;
                        }
                        else
                        {
                            intDays = FCConvert.ToInt32(rsSettings.Get_Fields_Int32("Basis"));
                        }
                    }
                    else
                    {
                        FCMessageBox.Show("Can't determine number of days in a year to charge interest for.  This will default to 360.", MsgBoxStyle.Information, "Invalid Days Per Year");
                        intDays = 360;
                    }
                }
                else
                {
                    intDays = modGlobal.Statics.gintBasis;
                }
            }
            lngPerDays = DateAndTime.DateDiff("d", dtInterestStartDate, dtDateNow) + UsingStartDate;
            if (lngPerDays < 1)
            {
                CalculateInterest = 0;
                dblPerDiem = Financial.IPmt((dblRateAsDecimal / intDays), 1, 1, dblPrincipalDue, 0);
            }
            else if (curAmount == 0)
            {
                CalculateInterest = 0;
                dblPerDiem = 0;
            }
            else
            {
                if (intDays == 0)
                {
                    if (modGlobal.Statics.gintBasis == 0)
                    {
                        rsSettings.OpenRecordset("SELECT Basis FROM Collections", modExtraModules.strCLDatabase);
                        if (rsSettings.EndOfFile() != true && rsSettings.BeginningOfFile() != true)
                        {
                            if (((rsSettings.Get_Fields_Int32("Basis"))) != 360 && ((rsSettings.Get_Fields_Int32("Basis"))) != 365)
                            {
                                FCMessageBox.Show("Can't determine number of days in a year to charge interest for.  This will default to 360.", MsgBoxStyle.Information, "Invalid Days Per Year");
                                intDays = 360;
                            }
                            else
                            {
                                intDays = FCConvert.ToInt32(rsSettings.Get_Fields_Int32("Basis"));
                            }
                        }
                        else
                        {
                            FCMessageBox.Show("Can't determine number of days in a year to charge interest for.  This will default to 360.", MsgBoxStyle.Information, "Invalid Days Per Year");
                            intDays = 360;
                        }
                    }
                    else
                    {
                        intDays = modGlobal.Statics.gintBasis;
                    }
                }
                if (curAmount > 0)
                {
                    dblPerDiem = Financial.IPmt((dblRateAsDecimal / intDays), 1, lngPerDays, dblPrincipalDue, 0);
                }
                else
                {
                    dblPerDiem = Financial.IPmt((dblOverPaymentIntRate / intDays), 1, lngPerDays, dblPrincipalDue, 0);
                }

                CalculateInterest = Math.Round(FCConvert.ToDecimal(dblPerDiem * lngPerDays), 2);
            }
            // MsgBox CalculateInterest & " - Calculated Interest" & vbCrLf & lngPerDays & " - Days" & vbCrLf & dblPerDiem & " - Per Diem" & vbCrLf & dblPrincipalDue & " - Principal Due" & vbCrLf & intDays & " - Days Per Year"
            return CalculateInterest;
        }

        public static double Rounding(float nValue, short nPositiveNumberOfDigits)
        {
            double Rounding = 0;
            string strFormat;
            strFormat = "#,###." + Strings.StrDup(nPositiveNumberOfDigits, "0");
            //FC:FINAL:AM:#i134 - have to round to 3 decimals because in VB6 CCur(5.684959) returns 5.685
            //Rounding = FCConvert.ToDouble(Strings.Format(FCConvert.ToDouble(nValue), strFormat));
            Rounding = FCConvert.ToDouble(Strings.Format(Math.Round(nValue, 3), strFormat));
            return Rounding;
        }
        // VBto upgrade warning: 'Return' As Variant --> As string
        //public static string MostRecentDate(ref string ApplyDate, ref string InterestStartDate)
        //{
        //    string MostRecentDate = "";
        //    if (Information.IsDate(ApplyDate) == true)
        //    {
        //        if (Strings.CompareString(Strings.Format(InterestStartDate, "MM/dd/yyyy"), "<", Strings.Format(ApplyDate, "MM/dd/yyyy")))
        //        {
        //            MostRecentDate = Strings.Format(ApplyDate, "MM/dd/yyyy");
        //        }
        //        else
        //        {
        //            MostRecentDate = Strings.Format(InterestStartDate, "MM/dd/yyyy");
        //        }
        //    }
        //    else
        //    {
        //        MostRecentDate = Strings.Format(InterestStartDate, "MM/dd/yyyy");
        //    }
        //    return MostRecentDate;
        //}

        //public static object CurrentYearInterest()
        //{
        //    object CurrentYearInterest = null;
        //    double curInterestAmount;
        //    return CurrentYearInterest;
        //}
#if !TWPP0000
        public static void CLSetup()
        {
            int intError = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper WWK = new clsDRWrapper();
                clsDRWrapper WCL = new clsDRWrapper();
                clsDRWrapper Modules = new clsDRWrapper();
                clsDRWrapper rsUpdate = new clsDRWrapper();
                string strTemp = "";
                intError = 1;
                if (WWK.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings"))
                {
                    intError = 2;
                    WCL.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
                    intError = 3;
                    modGlobalConstants.Statics.gstrCityTown = FCConvert.ToString(WWK.Get_Fields_String("CityTown"));
                    intError = 4;
                    modGlobal.Statics.gintBasis = FCConvert.ToInt32(WCL.Get_Fields_Int32("Basis"));
                    StaticSettings.TaxCollectionSettings.DaysInAYear = modGlobal.Statics.gintBasis;
                    // MsgBox gintBasis
                    intError = 5;
                   // modExtraModules.Statics.dblDefaultInterestRate = Conversion.Val(WCL.Get_Fields_Double("TaxRate"));
                    intError = 6;
                    modExtraModules.Statics.dblOverPayRate = Conversion.Val(WCL.Get_Fields_Double("OverPayInterestRate"));
                    StaticSettings.TaxCollectionSettings.OverPaymentInterestRate =
                        modExtraModules.Statics.dblOverPayRate.ToDecimal();
                    StaticSettings.TaxCollectionSettings.DiscountRate =
                        WCL.Get_Fields_Double("DiscountPercent").ToDecimal();
                    intError = 7;
                    Statics.gboolShowMapLotInCLAudit = FCConvert.ToBoolean(WCL.Get_Fields_Boolean("ShowMapLotinCLAudit"));
                    Statics.gboolCLUseTownSeal30DN = FCConvert.ToBoolean(WCL.Get_Fields_Boolean("ShowTownSeal30DN"));
                    Statics.gboolCLUseTownSealLien = FCConvert.ToBoolean(WCL.Get_Fields_Boolean("ShowTownSealLien"));
                    Statics.gboolCLUseTownSealLienMat = FCConvert.ToBoolean(WCL.Get_Fields_Boolean("ShowTownSealLienMat"));
                    Statics.gboolCLUseTownSealLienDischarge = FCConvert.ToBoolean(WCL.Get_Fields_Boolean("ShowTownSealLienDischarge"));
                    Statics.gboolDefaultRTDToAutoChange = FCConvert.ToBoolean(WCL.Get_Fields_Boolean("DefaultRTDToAutoChange"));
                    modGlobalConstants.Statics.gblnShowReceiptNumonDetail = FCConvert.ToBoolean(WCL.Get_Fields_Boolean("ReceiptNumonAcctDetail"));
                   // modStatusPayments.Statics.gboolDefaultPaymentsToAuto = FCConvert.ToBoolean(WCL.Get_Fields_Boolean("DefaultPaymentsToAuto"));
                }
                else
                {
                    FCMessageBox.Show("An error occured while opening TWCL0000.vb1.  Please make sure that you are running from the correct directory.", MsgBoxStyle.OkOnly, "Open Database Error");
                }
                intError = 500;
                return;
            }
            catch (Exception ex)
            {
                
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Collections Setup Error - " + FCConvert.ToString(intError));
                // Close 13
            }
        }

#endif
        public static void ApplyAbatementToPeriods(double TaxDue1, double TaxDue2, double TaxDue3, double TaxDue4, ref double dblPer1, ref double dblPer2, ref double dblPer3, ref double dblPer4, string strCode, double dblAmount, int intPeriods)
        {
            if (dblAmount != 0)
            {
                double dblTemp = 0;
                dblTemp = 0;
                if (intPeriods < 1)
                    intPeriods = 1;
                if (intPeriods > 1)
                {
                    if (strCode == "1")
                    {
                        dblPer1 -= dblAmount;
                        dblAmount = 0;
                    }
                    else if (strCode == "2")
                    {
                        dblPer2 -= dblAmount;
                        dblAmount = 0;
                    }
                    else if (strCode == "3")
                    {
                        dblPer3 -= dblAmount;
                        dblAmount = 0;
                    }
                    else if (strCode == "4")
                    {
                        dblPer4 -= dblAmount;
                        dblAmount = 0;
                    }
                    else
                    {
                        switch (intPeriods)
                        {
                            case 2:
                                {
                                    dblTemp = modGlobal.Round(dblAmount / 2, 2);
                                    if (TaxDue2 >= dblTemp)
                                    {
                                        dblPer2 -= dblTemp;
                                        dblAmount -= dblTemp;
                                    }
                                    else
                                    {
                                        dblPer2 -= TaxDue2;
                                        dblAmount -= TaxDue2;
                                    }
                                    dblPer1 -= dblAmount;
                                    dblAmount = 0;
                                    break;
                                }
                            case 3:
                                {
                                    dblTemp = modGlobal.Round(dblAmount / 3, 2);
                                    if (TaxDue3 >= dblTemp)
                                    {
                                        dblPer3 -= dblTemp;
                                        dblAmount -= dblTemp;
                                    }
                                    else
                                    {
                                        dblPer3 -= TaxDue3;
                                        dblAmount -= TaxDue3;
                                    }
                                    if (TaxDue2 >= dblTemp)
                                    {
                                        dblPer2 -= dblTemp;
                                        dblAmount -= dblTemp;
                                    }
                                    else
                                    {
                                        dblPer2 -= TaxDue2;
                                        dblAmount -= TaxDue2;
                                    }
                                    dblPer1 -= dblAmount;
                                    dblAmount = 0;
                                    break;
                                }
                            case 4:
                                {
                                    dblTemp = modGlobal.Round(dblAmount / 4, 2);
                                    if (TaxDue4 >= dblTemp)
                                    {
                                        dblPer4 -= dblTemp;
                                        dblAmount -= dblTemp;
                                    }
                                    else
                                    {
                                        dblPer4 -= TaxDue4;
                                        dblAmount -= TaxDue4;
                                    }
                                    dblTemp = modGlobal.Round(dblAmount / 3, 2);
                                    if (TaxDue3 >= dblTemp)
                                    {
                                        dblPer3 -= dblTemp;
                                        dblAmount -= dblTemp;
                                    }
                                    else
                                    {
                                        dblPer3 -= TaxDue3;
                                        dblAmount -= TaxDue3;
                                    }
                                    if (TaxDue2 >= dblTemp)
                                    {
                                        dblPer2 -= dblTemp;
                                        dblAmount -= dblTemp;
                                    }
                                    else
                                    {
                                        dblPer2 -= TaxDue2;
                                        dblAmount -= TaxDue2;
                                    }
                                    dblPer1 -= dblAmount;
                                    dblAmount = 0;
                                    break;
                                }
                        }
                        //end switch
                    }
                }
                else
                {
                    // can only put it to per 1 anyway
                    dblPer1 -= dblAmount;
                    dblAmount = 0;
                }
            }
        }
        // VBto upgrade warning: dblPer1 As double	OnWrite(short, string, double)
        public static void ApplyToPeriods(double TaxDue1, double TaxDue2, double TaxDue3, double TaxDue4, ref double dblPer1, ref double dblPer2, ref double dblPer3, ref double dblPer4, string strCode, double dblAmount, int intPeriods)
        {
            double dblTemp = 0;
            if (dblAmount != 0)
            {
                if (intPeriods < 1)
                    intPeriods = 1;
                if (dblAmount > 0)
                {
                    // Select Case strCode
                    // Case "2"
                    // Case "3"
                    // Case "4"
                    // Case Else
                    if (dblAmount > dblPer1)
                    {
                        dblTemp = dblAmount - dblPer1;
                        dblAmount -= dblPer1;
                        dblPer1 = 0;
                    }
                    else
                    {
                        dblPer1 = FCConvert.ToDouble(Strings.Format(dblPer1 - dblAmount, "0.00"));
                        dblAmount = 0;
                    }
                    if (dblAmount != 0)
                    {
                        if (intPeriods > 1)
                        {
                            if (dblAmount > dblPer2)
                            {
                                dblAmount -= dblPer2;
                                dblPer2 = 0;
                            }
                            else
                            {
                                dblPer2 -= dblAmount;
                                dblAmount = 0;
                            }
                        }
                        else
                        {
                            dblPer1 -= dblAmount;
                            dblAmount = 0;
                        }
                    }
                    if (dblAmount != 0)
                    {
                        if (intPeriods > 2)
                        {
                            if (dblAmount > dblPer3)
                            {
                                dblAmount -= dblPer3;
                                dblPer3 = 0;
                            }
                            else
                            {
                                dblPer3 -= dblAmount;
                                dblAmount = 0;
                            }
                        }
                        else
                        {
                            dblPer2 -= dblAmount;
                            dblAmount = 0;
                        }
                    }
                    if (dblAmount != 0)
                    {
                        if (intPeriods > 3)
                        {
                            if (dblAmount > dblPer4)
                            {
                                dblAmount -= dblPer4;
                                dblPer4 = 0;
                            }
                            else
                            {
                                dblPer4 -= dblAmount;
                                dblAmount = 0;
                            }
                        }
                        else
                        {
                            dblPer3 -= dblAmount;
                            dblAmount = 0;
                        }
                    }
                    // End Select
                }
                else
                {
                    if (intPeriods > 3)
                    {
                        if (dblPer4 < TaxDue4)
                        {
                            if (dblPer4 - dblAmount > TaxDue4)
                            {
                                dblTemp = TaxDue4 - dblPer4;
                                dblAmount += dblTemp;
                                dblPer4 = TaxDue4;
                            }
                            else
                            {
                                dblPer4 -= dblAmount;
                                dblAmount = 0;
                            }
                        }
                    }
                    if (intPeriods > 2 && dblAmount < 0)
                    {
                        if (dblPer3 < TaxDue3)
                        {
                            if (dblPer3 - dblAmount > TaxDue3)
                            {
                                dblTemp = TaxDue3 - dblPer3;
                                dblAmount += dblTemp;
                                dblPer3 = TaxDue3;
                            }
                            else
                            {
                                dblPer3 -= dblAmount;
                                dblAmount = 0;
                            }
                        }
                    }
                    if (intPeriods > 1 && dblAmount < 0)
                    {
                        if (dblPer2 < TaxDue2)
                        {
                            if (dblPer2 - dblAmount > TaxDue2)
                            {
                                dblTemp = TaxDue2 - dblPer2;
                                dblAmount += dblTemp;
                                dblPer2 = TaxDue2;
                            }
                            else
                            {
                                dblPer2 -= dblAmount;
                                dblAmount = 0;
                            }
                        }
                    }
                    if (dblAmount < 0)
                    {
                        if (dblPer1 < TaxDue1)
                        {
                            if (dblPer1 - dblAmount > TaxDue1)
                            {
                                dblTemp = TaxDue1 - dblPer1;
                                dblAmount += dblTemp;
                                dblPer1 = TaxDue1;
                            }
                            else
                            {
                                dblPer1 -= dblAmount;
                                dblAmount = 0;
                            }
                        }
                    }
                    if (dblAmount < 0)
                    {
                        switch (intPeriods)
                        {
                            case 2:
                                {
                                    dblPer2 -= dblAmount;
                                    dblAmount = 0;
                                    break;
                                }
                            case 3:
                                {
                                    dblPer3 -= dblAmount;
                                    dblAmount = 0;
                                    break;
                                }
                            case 4:
                                {
                                    dblPer4 -= dblAmount;
                                    dblAmount = 0;
                                    break;
                                }
                            default:
                                {
                                    dblPer1 -= dblAmount;
                                    dblAmount = 0;
                                    break;
                                }
                        }
                        //end switch
                    }
                }
            }
        }

        public static bool CalcBillAsOfFromPayments_540(int BNumber, DateTime dtDate, ref double dblTotalOwed, double dblPrinOwed, ref double dblIOwed, double dblCOwed, ref double dblCurInterest, ref double dblNonInt/* = 0 */)
        {
            return CalcBillAsOfFromPayments(BNumber, dtDate, ref dblTotalOwed, ref dblPrinOwed, ref dblIOwed, ref dblCOwed, ref dblCurInterest, ref dblNonInt);
        }

        public static bool CalcBillAsOfFromPayments_702(int BNumber, DateTime dtDate, ref double dblTotalOwed, double dblPrinOwed, double dblIOwed, double dblCOwed, ref double dblCurInterest, ref double dblNonInt/* = 0 */)
        {
            return CalcBillAsOfFromPayments(BNumber, dtDate, ref dblTotalOwed, ref dblPrinOwed, ref dblIOwed, ref dblCOwed, ref dblCurInterest, ref dblNonInt);
        }

        public static bool CalcBillAsOfFromPayments_2160(int BNumber, DateTime dtDate, ref double dblTotalOwed, double dblPrinOwed, double dblIOwed, double dblCOwed, double dblCurInterest, ref double dblNonInt/* = 0 */)
        {
            return CalcBillAsOfFromPayments(BNumber, dtDate, ref dblTotalOwed, ref dblPrinOwed, ref dblIOwed, ref dblCOwed, ref dblCurInterest, ref dblNonInt);
        }

        public static bool CalcBillAsOfFromPayments(int BNumber, DateTime dtDate, ref double dblTotalOwed, ref double dblPrinOwed, ref double dblIOwed, ref double dblCOwed, ref double dblCurInterest, ref double dblNonInt/* = 0 */)
        {
            bool CalcBillAsOfFromPayments = false;
            try
            {
                // On Error GoTo ErrorHandler
                double dblTotalDue = 0;
                int lngLienRecNumber;
                clsDRWrapper rsBill = new clsDRWrapper();
                clsDRWrapper rsPay = new clsDRWrapper();
                double dblCurInt;
                double dblTaxDue1;
                double dblTaxDue2;
                double dblTaxDue3;
                double dblTaxDue4;
                double dblPrincipal;
                double dblPrincipalPaid;
                double[] dblDue = new double[4 + 1];
                double dblIntCharged;
                double dblIntPaid;
                double dblAbate1/*unused?*/;
                double dblAbate2/*unused?*/;
                double dblAbate3/*unused?*/;
                double dblAbate4/*unused?*/;
                double[] dblPaid = new double[4 + 1];
                double dblCosts;
                double dblCostsPaid;
                int intNumPeriods;
                int lngRateKey = 0;
                int lngLienRateKey = 0;
                DateTime dtLienDate;
                DateTime dtDueDate1;
                DateTime dtDueDate2;
                DateTime dtDueDate3;
                DateTime dtDueDate4;
                DateTime dtIntDate1;
                DateTime dtIntDate2;
                DateTime dtIntDate3;
                DateTime dtIntDate4;
                DateTime[] dtIntDate = new DateTime[4 + 1];
                bool boolUseLien;
                int x;
                bool boolREBill;
                string strSQL = "";
                string strSQLA = "";
                // VBto upgrade warning: dtLatestIntDate As DateTime	OnWriteFCConvert.ToInt16(
                DateTime dtLatestIntDate;
                // VBto upgrade warning: dblPrincipalOwed As double	OnWrite(short, string)
                double dblPrincipalOwed;
                double dblInterestOwed;
                double dblCostOwed;
                int intPeriodToUse/*unused?*/;
                bool boolTC;
                double dblIntRate;
                dblPaid[1] = 0;
                dblPaid[2] = 0;
                dblPaid[3] = 0;
                dblPaid[4] = 0;
                dblDue[1] = 0;
                dblDue[2] = 0;
                dblDue[3] = 0;
                dblDue[4] = 0;
                dblIntRate = 0;
                dblPrincipalOwed = 0;
                dblInterestOwed = 0;
                dblCostOwed = 0;
                dtLatestIntDate = DateTime.FromOADate(0);
                boolREBill = true;
                boolUseLien = false;
                dblPrincipalPaid = 0;
                dblCosts = 0;
                dblCostsPaid = 0;
                dblIntCharged = 0;
                dblIntPaid = 0;
                dblPrincipal = 0;
                lngLienRecNumber = 0;
                dblCurInt = 0;
                dblTaxDue1 = 0;
                dblTaxDue2 = 0;
                dblTaxDue3 = 0;
                dblTaxDue4 = 0;
                dblNonInt = 0;
                intNumPeriods = 1;
                boolTC = false;
                rsBill.OpenRecordset("select lienrecordnumber, ratekey, taxdue1, taxdue2, taxdue3, taxdue4, " + "transferfrombillingDATEFIRST, BillingType from billingmaster where ID = " + FCConvert.ToString(BNumber), modExtraModules.strCLDatabase);
                if (!rsBill.EndOfFile())
                {
                    lngLienRecNumber = FCConvert.ToInt32(rsBill.Get_Fields_Int32("lienrecordnumber"));
                    lngRateKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBill.Get_Fields_Int32("ratekey"))));
                    dblTaxDue1 = Conversion.Val(rsBill.Get_Fields_Decimal("taxdue1"));
                    dblTaxDue2 = Conversion.Val(rsBill.Get_Fields_Decimal("taxdue2"));
                    dblTaxDue3 = Conversion.Val(rsBill.Get_Fields_Decimal("taxdue3"));
                    dblTaxDue4 = Conversion.Val(rsBill.Get_Fields_Decimal("taxdue4"));
                    if (Information.IsDate(rsBill.Get_Fields("transferfrombillingDATEFIRST")))
                    {
                        if (rsBill.Get_Fields_DateTime("transferfrombillingDATEFIRST") is DateTime)
                        {
                            if (DateAndTime.DateDiff("d", dtDate, (DateTime)rsBill.Get_Fields_DateTime("transferfrombillingDATEFIRST")) > 0)
                            {
                                dblTaxDue1 = 0;
                                dblTaxDue2 = 0;
                                dblTaxDue3 = 0;
                                dblTaxDue4 = 0;
                            }
                        }
                    }
                    if (Strings.UCase(FCConvert.ToString(rsBill.Get_Fields_String("BillingType"))) == "PP")
                    {
                        boolREBill = false;
                    }
                    rsBill.OpenRecordset("Select ID from taxclub where billkey = " + FCConvert.ToString(BNumber), modExtraModules.strCLDatabase);
                    if (!rsBill.EndOfFile())
                    {
                        boolTC = true;
                    }
                    if (lngLienRecNumber > 0)
                    {
                        rsBill.OpenRecordset("select principal, interest, costs, ratekey from lienrec where ID =" + FCConvert.ToString(lngLienRecNumber), modExtraModules.strCLDatabase);
                        if (!rsBill.EndOfFile())
                        {
                            // TODO: Check the table for the column [principal] and replace with corresponding Get_Field method
                            dblPrincipal = Conversion.Val(rsBill.Get_Fields("principal"));
                            // TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            dblIntCharged = Conversion.Val(rsBill.Get_Fields("Interest"));
                            // TODO: Check the table for the column [costs] and replace with corresponding Get_Field method
                            dblCosts = Conversion.Val(rsBill.Get_Fields("costs"));
                            lngLienRateKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBill.Get_Fields_Int32("ratekey"))));
                            rsBill.OpenRecordset("select duedate1, interestrate, intereststartdate1 from raterec where ID = " + FCConvert.ToString(lngLienRateKey), modExtraModules.strCLDatabase);
                            if (!rsBill.EndOfFile())
                            {
                                if (Information.IsDate(rsBill.Get_Fields("duedate1")))
                                {
                                    dtLatestIntDate = (DateTime)rsBill.Get_Fields_DateTime("duedate1");
                                    dtLienDate = (DateTime)rsBill.Get_Fields_DateTime("duedate1");
                                    if (DateAndTime.DateDiff("d", dtDate, dtLienDate) <= 0)
                                    {
                                        boolUseLien = true;
                                        dtDueDate1 = dtLienDate;
                                        dblDue[1] = dblPrincipal;
                                        dblIntRate = Conversion.Val(rsBill.Get_Fields_Double("interestrate"));
                                        if (Information.IsDate(rsBill.Get_Fields("InterestStartDate1")))
                                        {
                                            dtIntDate1 = (DateTime)rsBill.Get_Fields_DateTime("Intereststartdate1");
                                        }
                                        else
                                        {
                                            CalcBillAsOfFromPayments = false;
                                        }
                                    }
                                }
                                else
                                {
                                    CalcBillAsOfFromPayments = false;
                                    return CalcBillAsOfFromPayments;
                                }
                            }
                            else
                            {
                                CalcBillAsOfFromPayments = false;
                                return CalcBillAsOfFromPayments;
                            }
                        }
                    }
                    if (!boolUseLien)
                    {
                        // If boolREBill Then
                        strSQL = "SELECT Code, effectiveinterestdate, prelieninterest, currentinterest, principal, liencost, " + "period FROM paymentrec WHERE BillKey = " + FCConvert.ToString(BNumber) + " AND (BillCode = 'R' or billcode = 'P') AND ReceiptNumber > -1 and recordedtransactiondate <= '" + FCConvert.ToString(dtDate) + "' ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
                        // kk03192014  trocl-560  Add check for abatements to get interest calculated correctly
                        strSQLA = "SELECT Period, Principal FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(BNumber) + " AND (BillCode = 'R' or billcode = 'P') AND ReceiptNumber > -1 AND Code = 'A' AND recordedtransactiondate <= '" + FCConvert.ToString(dtDate) + "' ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
                        // Else
                        // strSQL = "SELECT * FROM paymentrec WHERE BillKey = " & BNumber & " AND BillCode = 'P' AND ReceiptNumber <> -1 and recordedtransactiondate <= #" & dtDate & "# ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID "
                        // End If
                        dblPrincipal = 0;
                        dblIntCharged = 0;
                        dblCosts = 0;
                        dblPrincipal = dblTaxDue1 + dblTaxDue2 + dblTaxDue3 + dblTaxDue4;
                        dblDue[1] = dblTaxDue1;
                        dblDue[2] = dblTaxDue2;
                        dblDue[3] = dblTaxDue3;
                        dblDue[4] = dblTaxDue4;
                        if (lngRateKey > 0)
                        {
                            rsBill.OpenRecordset("select NumberOfPeriods, InterestRate, duedate1, " + "InterestStartDate1, InterestStartDate2, InterestStartDate3, InterestStartDate4 from raterec where ID = " + FCConvert.ToString(lngRateKey), modExtraModules.strCLDatabase);
                            intNumPeriods = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBill.Get_Fields_Int16("NumberOfPeriods"))));
                            dblIntRate = Conversion.Val(rsBill.Get_Fields_Double("Interestrate"));
                            if (Information.IsDate(rsBill.Get_Fields("duedate1")))
                            {
                                dtLatestIntDate = (DateTime)rsBill.Get_Fields_DateTime("duedate1");
                            }
                            for (x = 1; x <= intNumPeriods; x++)
                            {
                                if (Information.IsDate(rsBill.Get_Fields("InterestStartDate" + FCConvert.ToString(x))))
                                {
                                    dtIntDate[x] = rsBill.Get_Fields_DateTime("InterestStartDate" + FCConvert.ToString(x));
                                }
                            }
                            // x
                        }
                        else
                        {
                            // prepayment
                            intNumPeriods = 1;
                            dblIntRate = 0;
                        }
                    }
                    else
                    {
                        strSQL = "SELECT Code, effectiveinterestdate, prelieninterest, currentinterest, principal, liencost, " + "period FROM paymentrec WHERE BillKey = " + FCConvert.ToString(lngLienRecNumber) + " AND BillCode = 'L' AND ReceiptNumber > -1 and ISNULL(recordedtransactiondate, '12/30/1899') <= '" + FCConvert.ToString(dtDate) + "' ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
                        strSQLA = "SELECT Period, Principal FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngLienRecNumber) + " AND BillCode = 'L' AND ReceiptNumber > -1 AND Code = 'A' AND recordedtransactiondate <= '" + FCConvert.ToString(dtDate) + "' ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
                    }
                    // kk04112014 trocl-560  Apply Abatements first
                    rsPay.OpenRecordset(strSQLA, modExtraModules.strCLDatabase);
                    while (!rsPay.EndOfFile())
                    {
                        // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                        // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        ApplyAbatementToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, ref dblDue[1], ref dblDue[2], ref dblDue[3], ref dblDue[4], FCConvert.ToString(rsPay.Get_Fields("Period")), Conversion.Val(rsPay.Get_Fields("Principal")), intNumPeriods);
                        rsPay.MoveNext();
                    }
                    rsPay.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                    while (!rsPay.EndOfFile())
                    {
                        string VBtoVar = rsPay.Get_Fields_String("Code");
                        if (VBtoVar == "A")
                        {
                            // abatement
                            // If IsDate(.Get_Fields("effectiveinterestdate")) Then
                            // dtLatestIntDate = .Get_Fields("effectiveinterestdate")
                            // End If
                            dblIntPaid += Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            dblPrincipalPaid += Conversion.Val(rsPay.Get_Fields("Principal"));
                            dblCostsPaid += Conversion.Val(rsPay.Get_Fields_Decimal("liencost"));
                            // Call ApplyToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, dblDue(1), dblDue(2), dblDue(3), dblDue(4), rsPay.Get_Fields("Period"), Val(rsPay.Get_Fields("Principal")), intNumPeriods)
                            // kk                        Call ApplyAbatementToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, dblDue(1), dblDue(2), dblDue(3), dblDue(4), .Get_Fields("Period"), val(.Get_Fields("Principal")), intNumPeriods)
                        }
                        else if (VBtoVar == "I")
                        {
                            // interest
                            if (Information.IsDate(rsPay.Get_Fields("effectiveinterestdate")))
                            {
                                if (DateAndTime.DateDiff("d", dtLatestIntDate, (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate")) > 0)
                                {
                                    dtLatestIntDate = (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate");
                                }
                            }
                            dblIntCharged += (-1 * Conversion.Val(rsPay.Get_Fields_Decimal("currentInterest")));
                        }
                        else if (VBtoVar == "P")
                        {
                            // payment
                            if (Information.IsDate(rsPay.Get_Fields("effectiveinterestdate")))
                            {
                                if (DateAndTime.DateDiff("d", dtLatestIntDate, (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate")) > 0)
                                {
                                    dtLatestIntDate = (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate");
                                }
                            }
                            dblIntPaid += Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            dblPrincipalPaid += Conversion.Val(rsPay.Get_Fields("Principal"));
                            // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            ApplyToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, ref dblDue[1], ref dblDue[2], ref dblDue[3], ref dblDue[4], FCConvert.ToString(rsPay.Get_Fields("Period")), Conversion.Val(rsPay.Get_Fields("Principal")), intNumPeriods);
                            dblCostsPaid += Conversion.Val(rsPay.Get_Fields_Decimal("liencost"));
                        }
                        else if ((VBtoVar == "C") || (VBtoVar == "N") || (VBtoVar == "F"))
                        {
                            // correction
                            if (Information.IsDate(rsPay.Get_Fields("effectiveinterestdate")))
                            {
                                dtLatestIntDate = (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate");
                            }
                            dblIntPaid += Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            dblPrincipalPaid += Conversion.Val(rsPay.Get_Fields("Principal"));
                            // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            ApplyToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, ref dblDue[1], ref dblDue[2], ref dblDue[3], ref dblDue[4], FCConvert.ToString(rsPay.Get_Fields("Period")), Conversion.Val(rsPay.Get_Fields("Principal")), intNumPeriods);
                            dblCostsPaid += Conversion.Val(rsPay.Get_Fields_Decimal("liencost"));
                        }
                        else if ((VBtoVar == "X") || (VBtoVar == "S"))
                        {
                            // dos correction
                            // kgk 02-25-11 TROCL-696  Copied the "correction" code from above to handle DOS correction
                            if (Information.IsDate(rsPay.Get_Fields("effectiveinterestdate")))
                            {
                                dtLatestIntDate = (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate");
                            }
                            dblIntPaid += Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            dblPrincipalPaid += Conversion.Val(rsPay.Get_Fields("Principal"));
                            // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            ApplyToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, ref dblDue[1], ref dblDue[2], ref dblDue[3], ref dblDue[4], FCConvert.ToString(rsPay.Get_Fields("Period")), Conversion.Val(rsPay.Get_Fields("Principal")), intNumPeriods);
                            dblCostsPaid += Conversion.Val(rsPay.Get_Fields_Decimal("liencost"));
                        }
                        else if (VBtoVar == "U")
                        {
                            // tax club
                            dblIntPaid += Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            dblPrincipalPaid += Conversion.Val(rsPay.Get_Fields("Principal"));
                            // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            ApplyToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, ref dblDue[1], ref dblDue[2], ref dblDue[3], ref dblDue[4], FCConvert.ToString(rsPay.Get_Fields("Period")), Conversion.Val(rsPay.Get_Fields("Principal")), intNumPeriods);
                            dblCostsPaid += Conversion.Val(rsPay.Get_Fields_Decimal("liencost"));
                        }
                        else if (VBtoVar == "Y")
                        {
                            // prepayment
                            if (Information.IsDate(rsPay.Get_Fields("effectiveinterestdate")))
                            {
                                dtLatestIntDate = (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate");
                            }
                            dblIntPaid += Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            dblPrincipalPaid += Conversion.Val(rsPay.Get_Fields("Principal"));
                            // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            ApplyToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, ref dblDue[1], ref dblDue[2], ref dblDue[3], ref dblDue[4], FCConvert.ToString(rsPay.Get_Fields("Period")), Conversion.Val(rsPay.Get_Fields("Principal")), intNumPeriods);
                            dblCostsPaid += Conversion.Val(rsPay.Get_Fields_Decimal("liencost"));
                        }
                        else if (VBtoVar == "D")
                        {
                            // discount
                            if (Information.IsDate(rsPay.Get_Fields("effectiveinterestdate")))
                            {
                                if (DateAndTime.DateDiff("d", dtLatestIntDate, (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate")) > 0)
                                {
                                    dtLatestIntDate = (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate");
                                }
                            }
                            dblIntPaid += Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            dblPrincipalPaid += Conversion.Val(rsPay.Get_Fields("Principal"));
                            // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            ApplyToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, ref dblDue[1], ref dblDue[2], ref dblDue[3], ref dblDue[4], FCConvert.ToString(rsPay.Get_Fields("Period")), Conversion.Val(rsPay.Get_Fields("Principal")), intNumPeriods);
                            dblCostsPaid += Conversion.Val(rsPay.Get_Fields_Decimal("liencost"));
                        }
                        else if (VBtoVar == "L")
                        {
                            // maturity fee
                            if (Information.IsDate(rsPay.Get_Fields("effectiveinterestdate")))
                            {
                                if (DateAndTime.DateDiff("d", dtLatestIntDate, (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate")) > 0)
                                {
                                    dtLatestIntDate = (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate");
                                }
                            }
                            dblCosts += (-1 * Conversion.Val(rsPay.Get_Fields_Decimal("liencost")));
                        }
                        else if (VBtoVar == "3")
                        {
                            // 30 day notice
                            if (Information.IsDate(rsPay.Get_Fields("effectiveinterestdate")))
                            {
                                if (DateAndTime.DateDiff("d", dtLatestIntDate, (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate")) > 0)
                                {
                                    dtLatestIntDate = (DateTime)rsPay.Get_Fields_DateTime("effectiveinterestdate");
                                }
                            }
                            dblCosts += (-1 * Conversion.Val(rsPay.Get_Fields_Decimal("liencost")));
                        }
                        else if (VBtoVar == "R")
                        {
                            // RefundED Abatement
                            // If IsDate(rsPay.Get_Fields("effectiveinterestdate")) Then
                            // If DateDiff("d", dtLatestIntDate, rsPay.Get_Fields("effectiveinterestdate")) > 0 Then
                            // dtLatestIntDate = rsPay.Get_Fields("effectiveinterestdate")
                            // End If
                            // End If
                            // dblIntPaid = dblIntPaid + Val(rsPay.Get_Fields("PreLienInterest")) + Val(rsPay.Get_Fields("CurrentInterest"))
                            // dblPrincipalPaid = dblPrincipalPaid + Val(rsPay.Get_Fields("Principal"))
                            // Call ApplyToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, dblDue(1), dblDue(2), dblDue(3), dblDue(4), rsPay.Get_Fields("Period"), Val(rsPay.Get_Fields("Principal")), intNumPeriods)
                            // Call ApplyAbatementToPeriods(dblTaxDue1, dblTaxDue2, dblTaxDue3, dblTaxDue4, dblDue(1), dblDue(2), dblDue(3), dblDue(4), rsPay.Get_Fields("Period"), Val(rsPay.Get_Fields("Principal")), intNumPeriods)
                            // dblCostsPaid = dblCostsPaid + Val(rsPay.Get_Fields("liencost"))
                            // 
                        }
                        rsPay.MoveNext();
                    }
                    dblPrincipalOwed = FCConvert.ToDouble(Strings.Format(dblPrincipal - dblPrincipalPaid, "0.00"));
                    DateTime dtTempDate;
                    // VBto upgrade warning: tInt As double	OnWrite(short, double)
                    double tInt = 0;
                    tInt = 0;
                    // kgk 02-10-11 TROCL-694  Per Corey and John this should match the status and/or the hardcoded report
                    if (dblPrincipalOwed != 0)
                    {
                        if (!boolTC && DateAndTime.DateDiff("d", dtLatestIntDate, dtDate) != 0)
                        {
                            // if not a tax club
                            for (x = 1; x <= intNumPeriods; x++)
                            {
                                if (Information.IsDate(dtIntDate[x]))
                                {
                                    if (DateAndTime.DateDiff("d", dtIntDate[x], dtDate) > 0)
                                    {
                                        if (DateAndTime.DateDiff("d", dtLatestIntDate, dtIntDate[x]) > 0)
                                        {
                                            dtTempDate = DateAndTime.DateAdd("d", -1, dtIntDate[x]);
                                        }
                                        else
                                        {
                                            dtTempDate = dtLatestIntDate;
                                        }
                                        tInt += FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblDue[x]), dtTempDate, dtDate, dblIntRate, modGlobal.Statics.gintBasis));
                                    }
                                }
                            }
                            // x
                            // tInt = CalculateInterest(dblPrincipalOwed, dtLatestIntDate, dtDate, dblIntRate, gintBasis)
                        }
                    }
                    dblCurInterest = tInt;
                    dblInterestOwed = dblIntCharged - dblIntPaid + tInt;
                    dblCostOwed = dblCosts - dblCostsPaid;
                    dblTotalDue = dblPrincipalOwed + dblInterestOwed + dblCostOwed;
                    dblTotalOwed = dblTotalDue;
                    dblPrinOwed = dblPrincipalOwed;
                    dblIOwed = dblInterestOwed;
                    dblCOwed = dblCostOwed;
                    dblNonInt = dblPrincipalOwed + dblCostOwed;
                    CalcBillAsOfFromPayments = true;
                }
                else
                {
                    dblTotalOwed = 0;
                    dblIOwed = 0;
                    dblCOwed = 0;
                    dblPrinOwed = 0;
                    dblCurInterest = 0;
                    dblNonInt = 0;
                    CalcBillAsOfFromPayments = false;
                }
                return CalcBillAsOfFromPayments;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In CalsAccountAsOfFromPayments", MsgBoxStyle.Critical, "Error");
            }
            return CalcBillAsOfFromPayments;
        }

#if !TWPP0000
        public static double CalculateAccountCLAsOf(ref clsDRWrapper rsTemp, ref int lngAccountNumber, ref DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin/* = 0 */, ref double dblCurInt/* = 0 */, ref double dblCurCost/* = 0 */, ref bool boolShowError/*= false*/, ref bool boolUseTableName/*= false*/, ref double dblPerDiem/*= 0*/, ref double dblAbatePer1/*= 0*/, ref double dblAbatePer2/*= 0*/, ref double dblAbatePer3/*= 0*/, ref double dblAbatePer4/*= 0*/, ref bool boolShowAllTaxDue/*= false*/, short intOnlyShowPeriodUpTo = 4, bool boolForceInterestCalculation = false)
        {
            double CalculateAccountCLAsOf = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will calculate the total amount owed on this person's bill at the payment date
                clsDRWrapper rsCalc = new clsDRWrapper();
                clsDRWrapper rsP = new clsDRWrapper();
                clsDRWrapper rsT = new clsDRWrapper();
                bool boolContinue;
                int intNumPeriods = 0;
                DateTime[] dtStartDate = new DateTime[4 + 1];
                DateTime[] dtDueDate = new DateTime[4 + 1];
                double[] dblPrinDuePerPeriod = new double[4 + 1];
                int intCT;
                double dblIntRate = 0;
                // VBto upgrade warning: dtIntPaidDate As DateTime	OnWrite(DateTime, short)
                DateTime dtIntPaidDate;
                double dblPrinPaid;
                double dblCost/*unused?*/;
                double dblPD = 0;
                bool boolNoCalcInt = false;
                string strTableName = "";
                if (boolUseTableName)
                {
                    // If boolRE Then
                    strTableName = "BillingMaster.";
                    // Else
                    // strTableName = "PPMaster."
                    // End If
                }
                else
                {
                    strTableName = "";
                }
                boolContinue = true;
                // get the tax rate from the rate rec
                if (rsCalc.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsTemp.Get_Fields_Int32("RateKey"), modExtraModules.strCLDatabase))
                {
                    if (rsCalc.EndOfFile() != true && rsCalc.BeginningOfFile() != true)
                    {
                        if (FCUtils.IsNull(rsCalc.Get_Fields_Double("InterestRate")) == false)
                        {
                            dblIntRate = Conversion.Val(rsCalc.Get_Fields_Double("InterestRate"));
                            intNumPeriods = FCConvert.ToInt32(rsCalc.Get_Fields_Int16("NumberOfPeriods"));
                        }
                        else
                        {
                            if (boolShowError)
                                FCMessageBox.Show("Error retrieving Tax Rate Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                            boolContinue = false;
                        }
                    }
                    else
                    {
                        if (boolShowError)
                            FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                        boolContinue = false;
                    }
                }
                else
                {
                    if (boolShowError)
                        FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                    boolContinue = false;
                }
                dtIntPaidDate = rsTemp.Get_Fields_DateTime("InterestAppliedThroughDate");
                if (DateAndTime.DateDiff("d", dtDate, dtIntPaidDate) > 0)
                {
                    if (!rsCalc.EndOfFile())
                    {
                        dtIntPaidDate = DateAndTime.DateAdd("d", -1, (DateTime)rsCalc.Get_Fields_DateTime("intereststartdate1"));
                    }
                    else
                    {
                        dtIntPaidDate = DateTime.FromOADate(0);
                    }
                    rsT.OpenRecordset("select max(effectiveinterestdate) as mdate from paymentrec where billkey = " + rsTemp.Get_Fields_Int32("billkey") + " and not billcode = 'L' and effectiveinterestdate <= '" + FCConvert.ToString(dtDate) + "'", modExtraModules.strCLDatabase);
                    // Call rsT.OpenRecordset("select max(effectiveinterestdate) as mdate from paymentrec where billkey = " & rsTemp.Get_Fields("billkey") & " and not billcode = 'L' and RECORDEDTRANSACTIONdate <= #" & dtDate & "#", strCLDatabase)
                    if (!rsT.EndOfFile())
                    {
                        // TODO: Field [mdate] not found!! (maybe it is an alias?)
                        if (Information.IsDate(rsT.Get_Fields("mdate")))
                        {
                            // TODO: Field [mdate] not found!! (maybe it is an alias?)
                            dtIntPaidDate = (DateTime)rsT.Get_Fields("mdate");
                        }
                    }
                }
                if (boolContinue)
                {
                    if (dtIntPaidDate.ToOADate() == 0)
                    {
                        // if the date has not been set then get the bill creation date from the rate record
                        if (rsCalc.Get_Fields_DateTime("InterestStartDate1") is DateTime)
                        {
                            dtIntPaidDate = DateAndTime.DateAdd("d", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate1"));
                        }
                    }
                    else
                    {
                        if (DateAndTime.DateDiff("D", (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate1"), dtIntPaidDate) >= 0)
                        {
                            // keep rockin
                        }
                        else
                        {
                            dtIntPaidDate = DateAndTime.DateAdd("d", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate1"));
                        }
                    }
                }
                dblPrinDuePerPeriod[1] = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue1")) - dblAbatePer1;
                if (intNumPeriods > 1 && intOnlyShowPeriodUpTo > 1)
                {
                    dblPrinDuePerPeriod[2] = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue2")) - dblAbatePer2;
                    if (intNumPeriods > 2 && intOnlyShowPeriodUpTo > 2)
                    {
                        dblPrinDuePerPeriod[3] = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue3")) - dblAbatePer3;
                        if (intNumPeriods > 3 && intOnlyShowPeriodUpTo > 3)
                        {
                            dblPrinDuePerPeriod[4] = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue4")) - dblAbatePer4;
                        }
                    }
                }
                dblPrinPaid = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("PrincipalPaid")) - (dblAbatePer1 + dblAbatePer2 + dblAbatePer3 + dblAbatePer4);
                if (boolShowAllTaxDue)
                {
                    dblCurPrin = dblPrinDuePerPeriod[1] + dblPrinDuePerPeriod[2] + dblPrinDuePerPeriod[3] + dblPrinDuePerPeriod[4];
                }
                else
                {
                    // find out how much principal is due
                    dblCurPrin = dblPrinDuePerPeriod[1];
                    if (intNumPeriods > 1 && intOnlyShowPeriodUpTo > 1)
                    {
                        dblCurPrin += dblPrinDuePerPeriod[2];
                        if (intNumPeriods > 2 && intOnlyShowPeriodUpTo > 2)
                        {
                            dblCurPrin += dblPrinDuePerPeriod[3];
                            if (intNumPeriods > 3 && intOnlyShowPeriodUpTo > 3)
                            {
                                dblCurPrin += dblPrinDuePerPeriod[4];
                            }
                        }
                    }
                }
                // this is the overall total
                dblCurPrin -= dblPrinPaid;
                // this is the breakdown
                if (dblPrinDuePerPeriod[1] > dblPrinPaid)
                {
                    // if period 1 due is more than the paid
                    dblPrinDuePerPeriod[1] -= dblPrinPaid;
                    // then find out how much is left after paying as much as possible to Per 1
                    dblPrinPaid = 0;
                    // and set the paid = 0
                }
                else
                {
                    dblPrinPaid -= dblPrinDuePerPeriod[1];
                    // else subtract how much was due from per1 and find out how much is left over
                    dblPrinDuePerPeriod[1] = 0;
                    // set how much is due from per 1 = 0
                    if (dblPrinDuePerPeriod[2] > dblPrinPaid && intOnlyShowPeriodUpTo > 1)
                    {
                        // now check the next periods
                        dblPrinDuePerPeriod[2] -= dblPrinPaid;
                        dblPrinPaid = 0;
                    }
                    else
                    {
                        dblPrinPaid -= dblPrinDuePerPeriod[2];
                        dblPrinDuePerPeriod[2] = 0;
                        if (dblPrinDuePerPeriod[3] > dblPrinPaid && intOnlyShowPeriodUpTo > 2)
                        {
                            dblPrinDuePerPeriod[3] -= dblPrinPaid;
                            dblPrinPaid = 0;
                        }
                        else
                        {
                            dblPrinPaid -= dblPrinDuePerPeriod[3];
                            dblPrinDuePerPeriod[3] = 0;
                            if (dblPrinDuePerPeriod[4] > dblPrinPaid && intOnlyShowPeriodUpTo > 3)
                            {
                                dblPrinDuePerPeriod[4] -= dblPrinPaid;
                                dblPrinPaid = 0;
                            }
                            else
                            {
                                dblPrinPaid -= dblPrinDuePerPeriod[4];
                                dblPrinDuePerPeriod[4] = 0;
                                dblPrinDuePerPeriod[1] -= dblPrinPaid;
                            }
                        }
                    }
                }
                // check to see if the last payment was a tax club payment, if so then do not calculate new interest
                // kk07172014 trocl-1189  No interest on past due PP account because RE account with same number has Tax Club setup
                // Must make sure the account type is correct - RE vs PP
                // If rsTemp.Get_Fields("BillingType") <> "PP" Then 'strTableName & "BillingType"
                // If boolRE Then
                // rsP.OpenRecordset "SELECT * FROM PaymentRec WHERE Account = " & lngAccountNumber & " AND [YEAR] = " & rsTemp.Get_Fields("BillingYear") & " AND Code <> 'I' AND BillCode <> 'P' and effectiveINTERESTDATE <= '" & Format(dtDate, "MM/dd/yyyy") & "' ORDER BY EffectiveInterestDate desc", strCLDatabase
                // Else
                // rsP.OpenRecordset "SELECT * FROM PaymentRec WHERE Account = " & lngAccountNumber & " AND YEAR = " & rsTemp.Get_Fields("BillingYear") & " AND Code <> 'I' AND BillCode = 'P' ORDER BY EffectiveInterestDate desc", strCLDatabase
                // End If
                // Else
                // rsP.OpenRecordset "SELECT * FROM PaymentRec WHERE Account = " & rsTemp.Get_Fields(strTableName & "Account") & " AND YEAR = " & rsTemp.Get_Fields("BillingYear") & " AND Code <> 'I' AND BillCode <> 'P' ORDER BY EffectiveInterestDate desc", strCLDatabase
                // End If
                rsP.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND [YEAR] = " + rsTemp.Get_Fields_Int32("BillingYear") + " AND Code <> 'I' AND BillCode <> 'L' AND BillKey = " + rsTemp.Get_Fields_Int32("ID") + " ORDER BY EffectiveInterestDate desc", modExtraModules.strCLDatabase);
                // MAL@20081205: Change to check for tax club record not just last payment = "U"
                // Tracker Reference:16457
                rsT.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND [Year] = " + rsTemp.Get_Fields_Int32("BillingYear") + " AND BillKey = " + rsTemp.Get_Fields_Int32("ID"), modExtraModules.strCLDatabase);
                if (rsT.RecordCount() > 0)
                {
                    if (!FCConvert.ToBoolean(rsT.Get_Fields_Boolean("NonActive")))
                    {
                        if (!rsP.EndOfFile())
                        {
                            if (rsP.Get_Fields_String("Code") == "U")
                            {
                                boolNoCalcInt = true;
                            }
                        }
                    }
                    else
                    {
                        boolNoCalcInt = false;
                    }
                }
                else
                {
                    boolNoCalcInt = false;
                }
                // If Not rsP.EndOfFile Then
                // If rsP.Get_Fields("Code") = "U" Then
                // boolNoCalcInt = True
                // End If
                // End If
                if (boolContinue)
                {
                    // fill the interest dates
                    dtStartDate[1] = DateAndTime.DateAdd("d", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate1"));
                    dtStartDate[2] = DateAndTime.DateAdd("d", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate2"));
                    dtStartDate[3] = DateAndTime.DateAdd("d", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate3"));
                    dtStartDate[4] = DateAndTime.DateAdd("d", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate4"));
                }
                for (intCT = 1; intCT <= 4; intCT++)
                {
                    // this will set the dates ahead if interest has already been applied
                    if (dtIntPaidDate.ToOADate() > dtStartDate[intCT].ToOADate())
                    {
                        dtStartDate[intCT] = dtIntPaidDate;
                    }
                }
                if (boolContinue)
                {
                    dtDueDate[1] = (DateTime)rsCalc.Get_Fields_DateTime("DueDate1");
                    dtDueDate[2] = (DateTime)rsCalc.Get_Fields_DateTime("DueDate2");
                    dtDueDate[3] = (DateTime)rsCalc.Get_Fields_DateTime("DueDate3");
                    dtDueDate[4] = (DateTime)rsCalc.Get_Fields_DateTime("DueDate4");
                }
                for (intCT = 1; intCT <= 4; intCT++)
                {
                    // this will set the dates ahead if interest has already been applied
                    if (dtIntPaidDate.ToOADate() > dtDueDate[intCT].ToOADate())
                    {
                        dtDueDate[intCT] = dtIntPaidDate;
                    }
                }
                // find out how much interest is due
                // If boolUseTableName Then
                dblCurInt = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("InterestCharged") * -1);
                // Else
                // dblCurInt = rsTemp.Get_Fields("InterestCharged") * -1
                // End If
                dblCurInt -= FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("InterestPaid"));
                dblXtraInt = 0;
                if (!boolNoCalcInt || boolForceInterestCalculation)
                {
                    // find out current interest
                    if (DateAndTime.DateDiff("d", dtDate, dtDueDate[1]) > 0 && dtDueDate[1].ToOADate() > 0)
                    {
                        // this is the base scenerio...payment is before interest starts
                        dblXtraInt = 0;
                    }
                    else
                    {
                        if (DateAndTime.DateDiff("d", dtDate, dtIntPaidDate) > 0)
                        {
                            // this is if interest has been charged to the period already
                            dblXtraInt = 0;
                        }
                        else
                        {
                            if (dblPrinDuePerPeriod[1] != 0)
                            {
                                dblXtraInt = FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[1]), dtStartDate[1], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPD));
                                dblPerDiem += dblPD;
                            }
                            if (dtDate.ToOADate() > dtDueDate[2].ToOADate() && dtDueDate[2].ToOADate() > 0)
                            {
                                if (dblPrinDuePerPeriod[2] > 0)
                                {
                                    dblXtraInt += FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[2]), dtStartDate[2], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPD));
                                    dblPerDiem += dblPD;
                                }
                                if (dtDate.ToOADate() > dtDueDate[3].ToOADate() && dtDueDate[3].ToOADate() > 0)
                                {
                                    if (dblPrinDuePerPeriod[3] > 0)
                                    {
                                        dblXtraInt += FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[3]), dtStartDate[3], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPD));
                                        dblPerDiem += dblPD;
                                    }
                                    if (dtDate.ToOADate() > dtDueDate[4].ToOADate() && dtDueDate[4].ToOADate() > 0)
                                    {
                                        if (dblPrinDuePerPeriod[4] > 0)
                                        {
                                            dblXtraInt += FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[4]), dtStartDate[4], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPD));
                                            dblPerDiem += dblPD;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (((dblCurCost)) == 0)
                {
                    // if this is empty then calculate it
                    dblCurCost = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("DemandFees") - rsTemp.Get_Fields_Decimal("DemandFeesPaid"));
                }
                dblCurInt += dblXtraInt;
                CalculateAccountCLAsOf = Conversion.Val(dblCurPrin) + Conversion.Val(dblCurInt) + Conversion.Val(dblCurCost);
                return CalculateAccountCLAsOf;
            }
            catch (Exception ex)
            {
                
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Calculating Account");
            }
            return CalculateAccountCLAsOf;
        }

#endif
        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblCurInt, bool boolUseTableName)
        {
            double dblXtraInt = 0;
            double dblCurPrin = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }
        //FC:FINAL:BBE - add overload for TWBL0000
        public static double CalculateAccountCL3(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin)
        {
            double dblCurInt = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }
        //FC:FINAL:BBE - add overload for TWBL0000
        public static double CalculateAccountCL3(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin, ref double dblCurCost)
        {
            double dblCurInt = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurInt, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4)
        {
            double dblCurPrin = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            //10
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, short intOnlyShowPeriodUpTo)
        {
            double dblXtraInt = 0;
            double dblCurPrin = 0;
            double dblCurInt = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurInt, bool boolUseTableName, ref double dblNonInt)
        {
            double dblCurPrin = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            bool boolForceInterestCalculation = false;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurInt, ref double dblCurCost, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4)
        {
            double dblCurPrin = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL1(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, double dblCurCost, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4, bool boolShowAllTaxDue)
        {
            double dblCurInt = 0;
            double dblCurPrin = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL2(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, double dblCurCost)
        {
            double dblCurInt = 0;
            double dblCurPrin = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, false, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL2(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurInt, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4, ref double dblNonInt)
        {
            double dblCurPrin = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin, ref double dblCurInt, ref double dblCurCost)
        {
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin, ref double dblCurInt, ref double dblCurCost, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4)
        {
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }
        //FC:FINAL:DDU:#i147 Interchanged variables
        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin, ref double dblCurInt, ref double dblCurCost, ref double dblPerDiem, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4)
        {
            bool boolShowError = false;
            bool boolUseTableName = false;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4, bool boolForceInterestCalculation)
        {
            double dblCurPrin = 0;
            double dblCurInt = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            short intOnlyShowPeriodUpTo = 4;
            bool boolShowAllTaxDue = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt)
        {
            double dblCurPrin = 0;
            double dblCurInt = 0;
            double dblCurCost = 0;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0, dblAbatePer2 = 0, dblAbatePer3 = 0, dblAbatePer4 = 0;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, false, 0, false, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblCurInt, short intOnlyShowPeriodUpTo)
        {
            double dblXtraInt = 0;
            double dblCurPrin = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin, ref double dblCurInt, ref double dblCurCost, short intOnlyShowPeriodUpTo)
        {
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL2(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin, ref double dblCurInt, ref double dblCurCost, ref double dblPerDiem)
        {
            bool boolShowError = false;
            bool boolUseTableName = false;
            short intOnlyShowPeriodUpTo = 4;
            double dblAbatePer1 = 0;
            double dblAbatePer2 = 0;
            double dblAbatePer3 = 0;
            double dblAbatePer4 = 0;
            bool boolShowAllTaxDue = false;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4)
        {
            double dblCurPrin = 0;
            double dblCurInt = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            bool boolShowAllTaxDue = false;
            short intOnlyShowPeriodUpTo = 4;
            bool boolForceInterestCalculation = false;
            double dblNonInt = 0;
            return CalculateAccountCL(ref rsTemp, lngAccountNumber, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblAbatePer1, ref dblAbatePer2, ref dblAbatePer3, ref dblAbatePer4, boolShowAllTaxDue, intOnlyShowPeriodUpTo, boolForceInterestCalculation, ref dblNonInt);
        }

        public static double CalculateAccountCL(ref clsDRWrapper rsTemp, int lngAccountNumber, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin/* = 0 */, ref double dblCurInt/* = 0 */, ref double dblCurCost/* = 0 */, bool boolShowError/*= false*/, bool boolUseTableName/*= false*/, ref double dblPerDiem, ref double dblAbatePer1, ref double dblAbatePer2, ref double dblAbatePer3, ref double dblAbatePer4, bool boolShowAllTaxDue/*= false*/, short intOnlyShowPeriodUpTo/*= 4*/, bool boolForceInterestCalculation/* = false*/, ref double dblNonInt/* = 0 */)
        {
            double CalculateAccountCL = 0;
            clsDRWrapper rsCalc = new clsDRWrapper();
            clsDRWrapper rsP = new clsDRWrapper();
            clsDRWrapper rsT = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will calculate the total amount owed on this person's bill at the payment date
                bool boolContinue;
                int intNumPeriods = 0;
                DateTime[] dtStartDate = new DateTime[4 + 1];
                DateTime[] dtDueDate = new DateTime[4 + 1];
                // VBto upgrade warning: dblPrinDuePerPeriod As double	OnWrite(double, string, short)
                double[] dblPrinDuePerPeriod = new double[4 + 1];
                int intCT;
                double dblIntRate = 0;
                // VBto upgrade warning: dtIntPaidDate As DateTime	OnWrite(short, DateTime)
                DateTime dtIntPaidDate;
                double dblPrinPaid;
                double dblCost/*unused?*/;
                double dblPD = 0;
                bool boolNoCalcInt;
                string strTableName = "";
                if (intOnlyShowPeriodUpTo == 0)
                {
                    intOnlyShowPeriodUpTo = 4;
                }
                if (boolUseTableName)
                {
                    // If boolRE Then
                    strTableName = "BillingMaster.";
                    // Else
                    // strTableName = "PPMaster."
                    // End If
                }
                else
                {
                    strTableName = "";
                }
                dblNonInt = 0;
                boolContinue = true;
                // get the tax rate from the rate rec
                if (rsCalc.OpenRecordset("SELECT InterestRate,NumberOfPeriods,DueDate1,DueDate2,DueDate3,DueDate4,InterestStartDate1,InterestStartDate2,InterestStartDate3,InterestStartDate4 FROM RateRec WHERE ID = " + rsTemp.Get_Fields_Int32("RateKey"), modExtraModules.strCLDatabase))
                {
                    if (rsCalc.EndOfFile() != true && rsCalc.BeginningOfFile() != true)
                    {
                        if (FCUtils.IsNull(rsCalc.Get_Fields_Double("InterestRate")) == false)
                        {
                            dblIntRate = rsCalc.Get_Fields_Double("InterestRate");
                            intNumPeriods = rsCalc.Get_Fields_Int16("NumberOfPeriods");
                        }
                        else
                        {
                            if (boolShowError)
                                FCMessageBox.Show("Error retrieving Tax Rate Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                            boolContinue = false;
                        }
                    }
                    else
                    {
                        if (boolShowError)
                            FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                        boolContinue = false;
                    }
                }
                else
                {
                    if (boolShowError)
                        FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                    boolContinue = false;
                }
                if (!FCUtils.IsValidDateTime(rsTemp.Get_Fields("InterestAppliedThroughDate")))
                {
                    dtIntPaidDate = DateTime.FromOADate(0);
                }
                else
                {
                    dtIntPaidDate = rsTemp.Get_Fields_DateTime("InterestAppliedThroughDate");
                }
                if (boolContinue)
                {
                    if (dtIntPaidDate.ToOADate() == 0)
                    {
                        // if the date has not been set then get the bill creation date from the rate record
                        if (FCUtils.IsValidDateTime(rsCalc.Get_Fields("InterestStartDate1")))
                        {
                            dtIntPaidDate = DateAndTime.DateAdd("d", -1, rsCalc.Get_Fields_DateTime("InterestStartDate1"));
                        }
                    }
                    else
                    {
                        if (DateAndTime.DateDiff("D", rsCalc.Get_Fields_DateTime("InterestStartDate1"), dtIntPaidDate) >= 0)
                        {
                            // keep rockin
                        }
                        else
                        {
                            dtIntPaidDate = DateAndTime.DateAdd("d", -1, rsCalc.Get_Fields_DateTime("InterestStartDate1"));
                        }
                    }
                }
                dblPrinDuePerPeriod[1] = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue1")) - dblAbatePer1;
                if (intNumPeriods > 1 && intOnlyShowPeriodUpTo > 1)
                {
                    dblPrinDuePerPeriod[2] = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue2")) - dblAbatePer2;
                    if (intNumPeriods > 2 && intOnlyShowPeriodUpTo > 2)
                    {
                        dblPrinDuePerPeriod[3] = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue3")) - dblAbatePer3;
                        if (intNumPeriods > 3 && intOnlyShowPeriodUpTo > 3)
                        {
                            dblPrinDuePerPeriod[4] = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue4")) - dblAbatePer4;
                        }
                    }
                }
                dblPrinPaid = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("PrincipalPaid")) - (dblAbatePer1 + dblAbatePer2 + dblAbatePer3 + dblAbatePer4);
                if (boolShowAllTaxDue)
                {
                    dblCurPrin = dblPrinDuePerPeriod[1] + dblPrinDuePerPeriod[2] + dblPrinDuePerPeriod[3] + dblPrinDuePerPeriod[4];
                }
                else
                {
                    // find out how much principal is due
                    dblCurPrin = dblPrinDuePerPeriod[1];
                    if (intNumPeriods > 1 && intOnlyShowPeriodUpTo > 1)
                    {
                        dblCurPrin += dblPrinDuePerPeriod[2];
                        if (intNumPeriods > 2 && intOnlyShowPeriodUpTo > 2)
                        {
                            dblCurPrin += dblPrinDuePerPeriod[3];
                            if (intNumPeriods > 3 && intOnlyShowPeriodUpTo > 3)
                            {
                                dblCurPrin += dblPrinDuePerPeriod[4];
                            }
                        }
                    }
                }
                // this is the overall total
                dblCurPrin = FCConvert.ToDouble(Strings.Format(dblCurPrin - dblPrinPaid, "0.00"));
                // this is the breakdown
                if (((dblCurPrin)) != 0)
                {
                    if (dblPrinDuePerPeriod[1] > dblPrinPaid)
                    {
                        // if period 1 due is more than the paid
                        dblPrinDuePerPeriod[1] = FCConvert.ToDouble(Strings.Format(dblPrinDuePerPeriod[1] - dblPrinPaid, "0.00"));
                        // then find out how much is left after paying as much as possible to Per 1
                        dblPrinPaid = 0;
                        // and set the paid = 0
                    }
                    else
                    {
                        dblPrinPaid -= dblPrinDuePerPeriod[1];
                        // else subtract how much was due from per1 and find out how much is left over
                        dblPrinDuePerPeriod[1] = 0;
                        // set how much is due from per 1 = 0
                        if (dblPrinDuePerPeriod[2] > dblPrinPaid && intOnlyShowPeriodUpTo > 1)
                        {
                            // now check the next periods
                            dblPrinDuePerPeriod[2] -= dblPrinPaid;
                            dblPrinPaid = 0;
                        }
                        else
                        {
                            dblPrinPaid -= dblPrinDuePerPeriod[2];
                            dblPrinDuePerPeriod[2] = 0;
                            if (dblPrinDuePerPeriod[3] > dblPrinPaid && intOnlyShowPeriodUpTo > 2)
                            {
                                dblPrinDuePerPeriod[3] -= dblPrinPaid;
                                dblPrinPaid = 0;
                            }
                            else
                            {
                                dblPrinPaid -= dblPrinDuePerPeriod[3];
                                dblPrinDuePerPeriod[3] = 0;
                                if (dblPrinDuePerPeriod[4] > dblPrinPaid && intOnlyShowPeriodUpTo > 3)
                                {
                                    dblPrinDuePerPeriod[4] -= dblPrinPaid;
                                    dblPrinPaid = 0;
                                }
                                else
                                {
                                    dblPrinPaid -= dblPrinDuePerPeriod[4];
                                    dblPrinDuePerPeriod[4] = 0;
                                    dblPrinDuePerPeriod[1] -= dblPrinPaid;
                                }
                            }
                        }
                    }
                }
                else
                {
                    dblPrinDuePerPeriod[1] = 0;
                    dblPrinDuePerPeriod[2] = 0;
                    dblPrinDuePerPeriod[3] = 0;
                    dblPrinDuePerPeriod[4] = 0;
                }
                // check to see if the last payment was a tax club payment, if so then do not calculate new interest
                // kk07172014 trocl-1189  No interest on past due PP account because RE account with same number has Tax Club setup
                // Must make sure the account type is correct - RE vs PP
                // If rsTemp.Get_Fields("BillingType") <> "PP" Then 'strTableName & "BillingType"
                // If boolRE Then
                // rsP.OpenRecordset "SELECT Code FROM PaymentRec WHERE Account = " & lngAccountNumber & " AND [YEAR] = " & rsTemp.Get_Fields("BillingYear") & " AND Code <> 'I' AND BillCode <> 'P' ORDER BY EffectiveInterestDate desc", strCLDatabase
                // Else
                // rsP.OpenRecordset "SELECT * FROM PaymentRec WHERE Account = " & lngAccountNumber & " AND YEAR = " & rsTemp.Get_Fields("BillingYear") & " AND Code <> 'I' AND BillCode = 'P' ORDER BY EffectiveInterestDate desc", strCLDatabase
                // End If
                // Else
                // rsP.OpenRecordset "SELECT * FROM PaymentRec WHERE Account = " & rsTemp.Get_Fields(strTableName & "Account") & " AND YEAR = " & rsTemp.Get_Fields("BillingYear") & " AND Code <> 'I' AND BillCode <> 'P' ORDER BY EffectiveInterestDate desc", strCLDatabase
                // End If
                rsP.OpenRecordset("SELECT TOP 1 Code FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND [YEAR] = " + rsTemp.Get_Fields_Int32("BillingYear") + " AND Code <> 'I' AND (BillCode = 'P' OR BillCode = 'R') AND BillKey = " + rsTemp.Get_Fields_Int32("ID") + " ORDER BY EffectiveInterestDate desc", modExtraModules.strCLDatabase);
                // MAL@20081205: Change to check for tax club record not just last payment = "U"
                // Tracker Reference:16457
                boolNoCalcInt = false;
                rsT.OpenRecordset("SELECT NonActive FROM TaxClub WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND [Year] = " + rsTemp.Get_Fields_Int32("BillingYear") + " AND BillKey = " + rsTemp.Get_Fields_Int32("ID"), modExtraModules.strCLDatabase);
                if (rsT.RecordCount() > 0)
                {
                    if (!rsT.Get_Fields_Boolean("NonActive"))
                    {
                        if (!rsP.EndOfFile())
                        {
                            if (rsP.Get_Fields_String("Code") == "U")
                            {
                                boolNoCalcInt = true;
                            }
                        }
                    }
                }

                if (boolContinue)
                {
                    // fill the interest dates
                    dtStartDate[1] = DateAndTime.DateAdd("d", -1, rsCalc.Get_Fields_DateTime("InterestStartDate1"));
                    if (Information.IsDate(rsCalc.Get_Fields("InterestStartDate2")))
                    {
                        dtStartDate[2] = DateAndTime.DateAdd("d", -1, rsCalc.Get_Fields_DateTime("InterestStartDate2"));
                    }
                    if (Information.IsDate(rsCalc.Get_Fields("InterestStartDate3")))
                    {
                        dtStartDate[3] = DateAndTime.DateAdd("d", -1, rsCalc.Get_Fields_DateTime("InterestStartDate3"));
                    }
                    if (Information.IsDate(rsCalc.Get_Fields("InterestStartDate4")))
                    {
                        dtStartDate[4] = DateAndTime.DateAdd("d", -1, rsCalc.Get_Fields_DateTime("InterestStartDate4"));
                    }
                }
                for (intCT = 1; intCT <= 4; intCT++)
                {
                    // this will set the dates ahead if interest has already been applied
                    if (dtIntPaidDate.ToOADate() > dtStartDate[intCT].ToOADate())
                    {
                        dtStartDate[intCT] = dtIntPaidDate;
                    }
                }
                if (boolContinue)
                {
                    dtDueDate[1] = rsCalc.Get_Fields_DateTime("DueDate1");
                    if (Information.IsDate(rsCalc.Get_Fields("DueDate2")))
                    {
                        dtDueDate[2] = rsCalc.Get_Fields_DateTime("DueDate2");
                    }
                    if (Information.IsDate(rsCalc.Get_Fields("DueDate3")))
                    {
                        dtDueDate[3] = rsCalc.Get_Fields_DateTime("DueDate3");
                    }
                    if (Information.IsDate(rsCalc.Get_Fields("DueDate4")))
                    {
                        dtDueDate[4] = rsCalc.Get_Fields_DateTime("DueDate4");
                    }
                }
                for (intCT = 1; intCT <= 4; intCT++)
                {
                    // this will set the dates ahead if interest has already been applied
                    if (dtIntPaidDate.ToOADate() > dtDueDate[intCT].ToOADate())
                    {
                        dtDueDate[intCT] = dtIntPaidDate;
                    }
                }
                // find out how much interest is due
                // If boolUseTableName Then
                dblCurInt = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("InterestCharged")) * -1;
                // Else
                // dblCurInt = rsTemp.Get_Fields("InterestCharged") * -1
                // End If
                dblCurInt -= FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("InterestPaid"));
                dblXtraInt = 0;
                if (!boolNoCalcInt || boolForceInterestCalculation)
                {
                    // find out current interest
                    if (DateAndTime.DateDiff("d", dtDate, dtDueDate[1]) > 0 && dtDueDate[1].ToOADate() > 0)
                    {
                        // this is the base scenerio...payment is before interest starts
                        dblXtraInt = 0;
                    }
                    else
                    {
                        if (DateAndTime.DateDiff("d", dtDate, dtIntPaidDate) > 0)
                        {
                            // this is if interest has been charged to the period already
                            dblXtraInt = 0;
                        }
                        else
                        {
                            if (dblPrinDuePerPeriod[1] != 0)
                            {
                                dblXtraInt = FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[1]), dtStartDate[1], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPD));
                                dblPerDiem += dblPD;
                            }
                            if (dtDate.ToOADate() > dtDueDate[2].ToOADate() && dtDueDate[2].ToOADate() > 0)
                            {
                                if (dblPrinDuePerPeriod[2] > 0)
                                {
                                    dblXtraInt += FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[2]), dtStartDate[2], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPD));
                                    dblPerDiem += dblPD;
                                }
                                if (dtDate.ToOADate() > dtDueDate[3].ToOADate() && dtDueDate[3].ToOADate() > 0)
                                {
                                    if (dblPrinDuePerPeriod[3] > 0)
                                    {
                                        dblXtraInt += FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[3]), dtStartDate[3], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPD));
                                        dblPerDiem += dblPD;
                                    }
                                    if (dtDate.ToOADate() > dtDueDate[4].ToOADate() && dtDueDate[4].ToOADate() > 0)
                                    {
                                        if (dblPrinDuePerPeriod[4] > 0)
                                        {
                                            dblXtraInt += FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[4]), dtStartDate[4], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPD));
                                            dblPerDiem += dblPD;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (((dblCurCost)) == 0)
                {
                    // if this is empty then calculate it
                    dblCurCost = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("DemandFees") - rsTemp.Get_Fields_Decimal("DemandFeesPaid"));
                }
                dblCurInt += dblXtraInt;
                dblNonInt = dblCurPrin + dblCurCost;
                CalculateAccountCL = dblCurPrin + dblCurInt + dblCurCost;
                rsT.DisposeOf();
                rsP.DisposeOf();
                rsCalc.DisposeOf();
                return CalculateAccountCL;
            }
            catch (Exception ex)
            {
                
                rsT.DisposeOf();
                rsP.DisposeOf();
                rsCalc.DisposeOf();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Calculating Account");
            }
            return CalculateAccountCL;
        }

        public static double CalculateAccountCLLien(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXTraInt, ref double dblCurInt, ref double dblNonInt)
        {
            double dblCurPrin = 0;
            double dblCurCost = 0;
            double dblPerDiem = 0;
            double dblPLIDue = 0;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXTraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }

        public static double CalculateAccountCLLien(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXTraInt, bool boolUseTableName)
        {
            double dblCurPrin = 0;
            double dblCurInt = 0;
            double dblCurCost = 0;
            bool boolShowError = false;
            double dblPerDiem = 0;
            double dblPLIDue = 0;
            double dblNonInt = 0;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXTraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }

        public static double CalculateAccountCLLien(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXTraInt, ref double dblCurCost)
        {
            double dblCurInt = 0;
            double dblCurPrin = 0;
            double dblPerDiem = 0;
            double dblPLIDue = 0;
            double dblNonInt = 0;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXTraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }

        public static double CalculateAccountCLLien(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXTraInt)
        {
            double dblCurInt = 0;
            double dblCurPrin = 0;
            double dblCurCost = 0;
            double dblPerDiem = 0;
            double dblPLIDue = 0;
            double dblNonInt = 0;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXTraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }
        //FC:FINAL:BBE - add overload for TWBL0000.
        public static double CalculateAccountCLLien3(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXTraInt, ref double dblCurPrin)
        {
            double dblCurCost = 0;
            double dblCurInt = 0;
            double dblPerDiem = 0;
            double dblPLIDue = 0;
            double dblNonInt = 0;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXTraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }
        //FC:FINAL:BBE - add overload for TWBL0000.
        public static double CalculateAccountCLLien3(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXTraInt, ref double dblCurPrin, ref double dblCurCost)
        {
            double dblCurInt = 0;
            double dblPerDiem = 0;
            double dblPLIDue = 0;
            double dblNonInt = 0;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXTraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }

        public static double CalculateAccountCLLien2(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXTraInt, ref double dblCurInt)
        {
            double dblCurCost = 0;
            double dblCurPrin = 0;
            double dblPerDiem = 0;
            double dblPLIDue = 0;
            double dblNonInt = 0;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXTraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }

        public static double CalculateAccountCLLien(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin, ref double dblCurInt, ref double dblCurCost)
        {
            bool boolShowError = false;
            bool boolUseTableName = false;
            double dblPerDiem = 0;
            double dblPLIDue = 0;
            double dblNonInt = 0;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }

        public static double CalculateAccountCLLien(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin/* = 0 */, ref double dblCurInt/* = 0 */, ref double dblCurCost/* = 0 */, ref double dblPerDiem, ref double dblPLIDue)
        {
            double dblNonInt = 0;
            bool boolShowError = false;
            bool boolUseTableName = false;
            return CalculateAccountCLLien(rsTemp, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, ref dblPerDiem, ref dblPLIDue, ref dblNonInt);
        }

        public static double CalculateAccountCLLien(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin/* = 0 */, ref double dblCurInt/* = 0 */, ref double dblCurCost/* = 0 */, bool boolShowError/*= false*/, bool boolUseTableName/*= false*/, ref double dblPerDiem, ref double dblPLIDue, ref double dblNonInt/* = 0 */)
        {
            double CalculateAccountCLLien = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will calculate the total amount owed on this person's bill at the payment date
                // this is strictly used for Liened accounts, the recordset passed in should
                // be on the Lien Record that is to be calculated
                // rstemp is the lien record
                // dtdate is the payment date
                // dblXtraInt is the extra interest that is calculated and can be retrieved through this variable
                clsDRWrapper rsCalc = new clsDRWrapper();
                bool boolContinue;
                int intNumPeriods = 0;
                DateTime[] dtStartDate = new DateTime[4 + 1];
                DateTime[] dtDueDate = new DateTime[4 + 1];
                double[] dblPrinDuePerPeriod = new double[4 + 1];
                int intCT;
                double dblIntRate = 0;
                // VBto upgrade warning: dtIntPaidDate As DateTime	OnWriteFCConvert.ToInt16(
                DateTime dtIntPaidDate;
                double dblPrinPaid = 0;
                string strTableName = "";
                double dblXCostPLI = 0;
                Debug.Assert(boolUseTableName == false);
                if (boolUseTableName)
                {
                    strTableName = "LR_";
                    // "LienRec."     ' SQL Server doesn't allow multipart column names
                }
                else
                {
                    strTableName = "";
                }
                boolContinue = true;
                // get the tax rate from the rate rec
                if (rsCalc.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsTemp.Get_Fields(strTableName + "RateKey"), modExtraModules.strCLDatabase))
                {
                    if (rsCalc.EndOfFile() != true && rsCalc.BeginningOfFile() != true)
                    {
                        if (FCUtils.IsNull(rsCalc.Get_Fields_Double("InterestRate")) == false)
                        {
                            dblIntRate = Conversion.Val(rsCalc.Get_Fields_Double("InterestRate"));
                            intNumPeriods = FCConvert.ToInt32(rsCalc.Get_Fields_Int16("NumberOfPeriods"));
                        }
                        else
                        {
                            if (boolShowError)
                                FCMessageBox.Show("Error retrieving Tax Rate Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                            boolContinue = false;
                        }
                    }
                    else
                    {
                        if (boolShowError)
                            FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                        boolContinue = false;
                    }
                }
                else
                {
                    if (boolShowError)
                        FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
                    boolContinue = false;
                }
                if (boolContinue)
                {
                    if (rsTemp.Get_Fields(strTableName + "InterestAppliedThroughDate") != null)
                    {
                        dtIntPaidDate = rsTemp.Get_Fields(strTableName + "InterestAppliedThroughDate");
                    }
                    else
                    {
                        dtIntPaidDate = DateTime.FromOADate(0);
                    }
                    if (dtIntPaidDate.ToOADate() == 0)
                    {
                        // if the date has not been set then get the bill creation date from the rate record
                        if (rsCalc.Get_Fields_DateTime("InterestStartDate1") != null)
                        {
                            dtIntPaidDate = (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate1");
                        }
                    }
                    // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                    dblPrinDuePerPeriod[1] = FCConvert.ToDouble(rsTemp.Get_Fields("Principal"));
                    dblPrinDuePerPeriod[2] = 0;
                    dblPrinDuePerPeriod[3] = 0;
                    dblPrinDuePerPeriod[4] = 0;
                    dblPrinPaid = Conversion.Val(rsTemp.Get_Fields(strTableName + "PrincipalPaid"));
                    // find out how much principal is due
                    dblCurPrin = dblPrinDuePerPeriod[1];
                    if (intNumPeriods > 1)
                    {
                        dblCurPrin += dblPrinDuePerPeriod[2];
                        if (intNumPeriods > 2)
                        {
                            dblCurPrin += dblPrinDuePerPeriod[3];
                            if (intNumPeriods > 3)
                            {
                                dblCurPrin += dblPrinDuePerPeriod[4];
                            }
                        }
                    }
                    // this is the overall total
                    dblCurPrin = FCConvert.ToDouble(Strings.Format(dblCurPrin - dblPrinPaid, "0.00"));
                    // this is the breakdown
                    if (dblPrinDuePerPeriod[1] > dblPrinPaid)
                    {
                        // if period 1 due is more than the paid
                        dblPrinDuePerPeriod[1] -= dblPrinPaid;
                        // then find out how much is left after paying as much as possible to Per 1
                        dblPrinPaid = 0;
                        // and set the paid = 0
                    }
                    else
                    {
                        dblPrinPaid -= dblPrinDuePerPeriod[1];
                        // else subtract how much was due from per1 and find out how much is left over
                        dblPrinDuePerPeriod[1] = 0;
                        // set how much is due from per 1 = 0
                    }
                    // fill the interest dates
                    dtStartDate[1] = DateAndTime.DateAdd("D", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate1"));
                    if (Information.IsDate(rsCalc.Get_Fields("Intereststartdate2")))
                    {
                        dtStartDate[2] = DateAndTime.DateAdd("D", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate2"));
                    }
                    if (Information.IsDate(rsCalc.Get_Fields("Intereststartdate3")))
                    {
                        dtStartDate[3] = DateAndTime.DateAdd("D", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate3"));
                    }
                    if (Information.IsDate(rsCalc.Get_Fields("Intereststartdate4")))
                    {
                        dtStartDate[4] = DateAndTime.DateAdd("D", -1, (DateTime)rsCalc.Get_Fields_DateTime("InterestStartDate4"));
                    }
                    for (intCT = 1; intCT <= 4; intCT++)
                    {
                        // this will set the dates ahead if interest has already been applied
                        if (dtIntPaidDate.ToOADate() > dtStartDate[intCT].ToOADate())
                        {
                            dtStartDate[intCT] = dtIntPaidDate;
                            // DateAdd("D", -1, dtIntPaidDate)
                        }
                    }
                    dtDueDate[1] = (DateTime)rsCalc.Get_Fields_DateTime("DueDate1");
                    if (Information.IsDate(rsCalc.Get_Fields("duedate2")))
                    {
                        dtDueDate[2] = (DateTime)rsCalc.Get_Fields_DateTime("DueDate2");
                    }
                    if (Information.IsDate(rsCalc.Get_Fields("duedate3")))
                    {
                        dtDueDate[3] = (DateTime)rsCalc.Get_Fields_DateTime("DueDate3");
                    }
                    if (Information.IsDate(rsCalc.Get_Fields("duedate4")))
                    {
                        dtDueDate[4] = (DateTime)rsCalc.Get_Fields_DateTime("DueDate4");
                    }
                    for (intCT = 1; intCT <= 4; intCT++)
                    {
                        // this will set the dates ahead if interest has already been applied
                        if (dtIntPaidDate.ToOADate() > dtDueDate[intCT].ToOADate())
                        {
                            dtDueDate[intCT] = dtIntPaidDate;
                        }
                    }
                    // find out how much interest is due
                    dblCurInt = (Conversion.Val(rsTemp.Get_Fields(strTableName + "InterestCharged")) * -1);
                    dblCurInt -= (Conversion.Val(rsTemp.Get_Fields(strTableName + "InterestPaid")));
                    // + rsTemp.Get_Fields("PLIPaid"))
                    dblXtraInt = 0;
                    // find out current interest
                    if (dtDate.ToOADate() < dtDueDate[1].ToOADate() && dtDueDate[1].ToOADate() > 0)
                    {
                        // this is the base scenerio...payment is before interest starts
                        dblXtraInt = 0;
                    }
                    else
                    {
                        if (dtDate.ToOADate() < dtIntPaidDate.ToOADate())
                        {
                            // this is if interest has been charged to the period already
                            dblXtraInt = 0;
                        }
                        else
                        {
                            if (dblPrinDuePerPeriod[1] != 0)
                            {
                                dblXtraInt = FCConvert.ToDouble(CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod[1]), dtStartDate[1], dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPerDiem));
                            }
                        }
                    }
                }
                if (boolContinue)
                {
                    // TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
                    // TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                    dblCurCost = Conversion.Val(rsTemp.Get_Fields("Costs") - rsTemp.Get_Fields("MaturityFee") - rsTemp.Get_Fields_Decimal("CostsPaid"));
                    // TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
                    // TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                    dblPLIDue = Conversion.Val(rsTemp.Get_Fields("Interest") - rsTemp.Get_Fields("PLIPaid"));
                    // TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
                    // TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                    dblXCostPLI = Conversion.Val(rsTemp.Get_Fields("Interest") - rsTemp.Get_Fields("PLIPaid")) + dblCurCost;
                    // this will add the Pre Lien Interest and Costs
                    dblCurInt += dblXtraInt;
                    // interest charged AND calculated interest
                    // dblCurInt = (rsTemp.Get_Fields("InterestCharged") * -1) - rsTemp.Get_Fields("InterestPaid") + dblXtraInt
                    dblNonInt = Conversion.Val(dblCurPrin) + dblXCostPLI;
                    CalculateAccountCLLien = Conversion.Val(dblCurPrin) + Conversion.Val(dblCurInt) + dblXCostPLI;
                }
                else
                {
                    // reset values
                    dblCurPrin = 0;
                    dblCurInt = 0;
                    dblCurCost = 0;
                    dblNonInt = 0;
                    CalculateAccountCLLien = 0;
                }
                return CalculateAccountCLLien;
            }
            catch (Exception ex)
            {
                
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Calculating Lien");
            }
            return CalculateAccountCLLien;
        }

        public static void CheckForAbatementPayments(int lngBillKey, int lngRateKey, bool boolRE, ref double dblAbate1, ref double dblAbate2, ref double dblAbate3, ref double dblAbate4, double dblTax1, double dblTax2, double dblTax3, double dblTax4)
        {
            string strSQL = "";
            clsDRWrapper rsLoad = new clsDRWrapper();
            clsDRWrapper rsRK = new clsDRWrapper();
            int i/*unused?*/;
            double dblAdded = 0;
            if (boolRE)
            {
                strSQL = "SELECT Code, Period, Principal FROM paymentrec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND BillCode = 'R' AND code = 'A' and ReceiptNumber <> -1 ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
            }
            else
            {
                strSQL = "SELECT Code, Period, Principal FROM paymentrec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND BillCode = 'P' AND code = 'A' and ReceiptNumber <> -1 ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
            }
            dblAbate1 = 0;
            dblAbate2 = 0;
            dblAbate3 = 0;
            dblAbate4 = 0;

            try
            {
                rsLoad.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                if (rsLoad.RecordCount() > 0)
                {
                    rsRK.OpenRecordset("SELECT NumberOfPeriods FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey), modExtraModules.strCLDatabase);
                    while (!rsLoad.EndOfFile())
                    {
                        dblAdded = 0;
                        if (FCConvert.ToString(rsLoad.Get_Fields("Code")) == "A")
                        {
                            string period = rsLoad.Get_Fields_String("Period");
                            var principal = Conversion.Val(rsLoad.Get_Fields("Principal"));

                            switch (period)
                            {
                                case "1":
                                    dblAbate1 += principal;

                                    break;
                                case "2":
                                    dblAbate2 += principal;

                                    break;
                                case "3":
                                    dblAbate3 += principal;

                                    break;
                                case "4":
                                    dblAbate4 += principal;

                                    break;
                                case "A" when !rsRK.EndOfFile():
                                    {
                                        var numberOfPeriods = rsRK.Get_Fields_Int16("NumberOfPeriods");

                                        switch (numberOfPeriods)
                                        {
                                            case 1:
                                                // only one period, so put it all into per 1
                                                dblAbate1 += principal;

                                                break;

                                            case 2:
                                                {
                                                    // two periods so make sure that not too much has been put into period 2, once it is full then put the rest into period one
                                                    var roundedPrincipal = modGlobal.Round(principal / 2, 2);

                                                    if (dblTax2 >= dblAbate2 + roundedPrincipal)
                                                    {
                                                        dblAbate2 += roundedPrincipal;
                                                        dblAdded += roundedPrincipal;
                                                    }
                                                    else
                                                    {
                                                        dblAdded += (dblTax2 - dblAbate2);
                                                        dblAbate2 = dblTax2;
                                                    }

                                                    dblAbate1 += modGlobal.Round(principal - dblAdded, 2);

                                                    break;
                                                }
                                            case 3:
                                                {
                                                    var roundedPrincipal = modGlobal.Round(principal / 3, 2);

                                                    if (dblTax3 >= dblAbate3 + roundedPrincipal)
                                                    {
                                                        dblAbate3 += roundedPrincipal;
                                                        dblAdded += roundedPrincipal;
                                                    }
                                                    else
                                                    {
                                                        dblAdded += (dblTax3 - dblAbate3);
                                                        dblAbate3 += dblTax3;
                                                    }

                                                    if (dblTax2 >= dblAbate2 + roundedPrincipal)
                                                    {
                                                        dblAbate2 += roundedPrincipal;
                                                        dblAdded += roundedPrincipal;
                                                    }
                                                    else
                                                    {
                                                        dblAdded += (dblTax2 - dblAbate2);
                                                        dblAbate2 = dblTax2;
                                                    }

                                                    dblAbate1 += modGlobal.Round(principal - dblAdded, 2);

                                                    break;
                                                }
                                            case 4:
                                                {
                                                    var roundedPrincipal = modGlobal.Round(principal / 4, 2);

                                                    if (dblTax4 >= dblAbate4 + roundedPrincipal)
                                                    {
                                                        dblAbate4 += roundedPrincipal;
                                                        dblAdded = roundedPrincipal;
                                                    }
                                                    else
                                                    {
                                                        dblAdded += (dblTax4 - dblAbate4);
                                                        dblAbate4 = dblTax4;
                                                    }

                                                    if (dblTax3 >= dblAbate3 + roundedPrincipal)
                                                    {
                                                        dblAbate3 += roundedPrincipal;
                                                        dblAdded += roundedPrincipal;
                                                    }
                                                    else
                                                    {
                                                        dblAdded += (dblTax3 - dblAbate3);
                                                        dblAbate3 += dblTax3;
                                                    }
                                                    if (dblTax2 >= dblAbate2 + roundedPrincipal)
                                                    {
                                                        dblAbate2 += roundedPrincipal;
                                                        dblAdded += roundedPrincipal;
                                                    }
                                                    else
                                                    {
                                                        dblAdded += (dblTax2 - dblAbate2);
                                                        dblAbate2 = dblTax2;
                                                    }
                                                    dblAbate1 += modGlobal.Round(principal - dblAdded, 2);

                                                    break;
                                                }
                                        }
                                        break;


                                    }
                                case "A":
                                    dblAbate1 += principal;

                                    break;
                            }
                        }
                        rsLoad.MoveNext();
                    }
                }
            }
            finally
            {
                rsLoad.DisposeOf();
                rsRK.DisposeOf();
            }

        }

        //Needed for dynamic parameters
        public static double CalculateAccountTotal1(int lngAccountNumber, bool boolREAccount)
        {
            return CalculateAccountTotal(lngAccountNumber, boolREAccount, false, DateTime.Now, 0, 0);
        }
        // VBto upgrade warning: dtDate As DateTime	OnWrite(DateTime, TRIOTEXT.T2KDateBox)
        public static double CalculateAccountTotal(int lngAccountNumber, bool boolREAccount, bool boolShowAllPeriodTax = false, DateTime? dtDate = null/*DateTime.Now*/, int REacct = 0, int lngYear = 0)
        {
            double CalculateAccountTotal = 0;
            if (!dtDate.HasValue)
            {
                dtDate = DateTime.Now;
            }
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will calculate the total remaining balance and
                // return it as a double for the account and type passed in
                clsDRWrapper rsCL = new clsDRWrapper();
                clsDRWrapper rsLien = new clsDRWrapper();
                string strSQL = "";
                double dblTotal = 0;
                double dblXtraInt = 0;
                double AbatePer1 = 0;
                double AbatePer2 = 0;
                double AbatePer3 = 0;
                double AbatePer4 = 0;
                int intTemp/*unused?*/;
                if (dtDate.Value.ToOADate() == 0)
                {
                    dtDate = DateTime.Today;
                }
                if (boolREAccount)
                {
                    strSQL = "SELECT ID,LienRecordNumber,RateKey,TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,BillingYear,InterestCharged,InterestPaid,InterestAppliedThroughDate,DemandFees,DemandFeesPaid " + "FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'RE'";
                }
                else
                {
                    strSQL = "SELECT ID,LienRecordNumber,RateKey,TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,BillingYear,InterestCharged,InterestPaid,InterestAppliedThroughDate,DemandFees,DemandFeesPaid " + "FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'PP'";
                }
                if (lngYear > 0)
                {
                    strSQL += " and billingyear >= " + FCConvert.ToString(lngYear) + "1 and billingyear <= " + FCConvert.ToString(lngYear) + "9 ";
                }
                rsCL.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                if (!rsCL.EndOfFile())
                {
                    while (!rsCL.EndOfFile())
                    {
                        // calculate each year
                        dblXtraInt = 0;
                        if (rsCL.Get_Fields_Int32("LienRecordNumber") == 0)
                        {
                            // non-lien
                            AbatePer1 = 0;
                            AbatePer2 = 0;
                            AbatePer3 = 0;
                            AbatePer4 = 0;
                            CheckForAbatementPayments(rsCL.Get_Fields_Int32("ID"), rsCL.Get_Fields_Int32("ratekey"), boolREAccount, ref AbatePer1, ref AbatePer2, ref AbatePer3, ref AbatePer4, Conversion.Val(rsCL.Get_Fields_Decimal("taxdue1")), Conversion.Val(rsCL.Get_Fields_Decimal("taxdue2")), Conversion.Val(rsCL.Get_Fields_Decimal("taxdue3")), Conversion.Val(rsCL.Get_Fields_Decimal("taxdue4")));
                            dblTotal += CalculateAccountCL1(ref rsCL, lngAccountNumber, dtDate.Value, ref dblXtraInt, Conversion.Val(rsCL.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsCL.Get_Fields_Decimal("DemandFeesPaid")), ref AbatePer1, ref AbatePer2, ref AbatePer3, ref AbatePer4, boolShowAllPeriodTax);
                        }
                        else
                        {
                            // lien
                            // rsLien.FindFirstRecord "ID", rsCL.Get_Fields("LienrecordNumber")
                            // If rsLien.NoMatch Then
                            rsLien.OpenRecordset("SELECT ID,RateKey,InterestAppliedThroughDate,MaturityFee," + "Principal,Interest,InterestCharged,Costs," + "PrincipalPaid,PLIPaid,InterestPaid,CostsPaid " + "FROM LienRec WHERE ID = " + FCConvert.ToString(rsCL.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
                            if (rsLien.RecordCount() == 0)
                            {
                                // no match has been found
                                // do nothing
                            }
                            else
                            {
                                // calculate the lien record
                                dblTotal += CalculateAccountCLLien(rsLien, dtDate.Value, ref dblXtraInt);
                            }
                        }
                        // Dave 08/03/2007 subtract interest if it is interest earned on a pre payment since it doesnt show in collections until the next billing
                        if (dblXtraInt < 0)
                        {
                            dblTotal -= dblXtraInt;
                        }
                        rsCL.MoveNext();
                    }
                }
                else
                {
                    // nothing found
                    dblTotal = 0;
                }
                CalculateAccountTotal = dblTotal;
                return CalculateAccountTotal;
            }
            catch (Exception ex)
            {
                
                CalculateAccountTotal = 0;
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Calculate Account Total Error");
            }
            return CalculateAccountTotal;
        }

        public static bool NewOwner2(string strName, int lngAccount,  DateTime dtBillDate)
        {
            bool temp = false;
            return HasNewOwner( dtBillDate, lngAccount);
        }

        public static bool HasNewOwner(DateTime billDate, int account)
        {
            var dtDate = FCConvert.ToDateTime("04/01/" + FCConvert.ToString(billDate.Year));
            using (var rsLoad = new clsDRWrapper())
            {
                rsLoad.OpenRecordset("SELECT top 1 id FROM SRMaster WHERE RSAccount = " + account + " AND RSCard = 1 AND SaleDate > '" + dtDate.FormatAndPadShortDate() + "'", "RealEstate");
                if (!rsLoad.EndOfFile())
                {
                    return true;
                }
                rsLoad.OpenRecordset("SELECT top 1 id FROM PreviousOwner WHERE Account = " + account + " AND SaleDate > '" + dtDate.FormatAndPadShortDate() + "'", "RealEstate");
                if (!rsLoad.EndOfFile())
                {
                    return true;
                }
            }
            return false;
        }
        public static bool NewOwner(string strName, int lngAccount, DateTime dtBillDate, ref bool boolREMatch, ref bool boolNameMatch, string strAddress1 = "", string strAddress2 = "", string strAddress3 = "", string strCity = "", string strState = "", string strZip = "", string strZip4 = "")
        {
            bool NewOwner = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will find out if there has been a new owner since the billing date
                // strName is the current name
                // lngAccount is the account number
                // dtBillDate is the billing date on this bill
                // boolREMatch will return if the REMaster was found for this account
                clsDRWrapper rsSR = new clsDRWrapper();
               
                clsDRWrapper rsRE = new clsDRWrapper();
                // VBto upgrade warning: dtDate As DateTime	OnWrite(string)
                DateTime dtDate;
               // cParty cCParty = new cParty();
                // take the year from the billing date and set the date to use as the 1st of April
                dtDate = FCConvert.ToDateTime("04/01/" + FCConvert.ToString(dtBillDate.Year));
                NewOwner = false;
                // rsRE.OpenRecordset "SELECT FullNameLF FROM Master CROSS APPLY " & rsRE.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(OwnerPartyID,NULL,'RE',RSAccount) WHERE RSCard = 1 AND RSAccount = " & lngAccount, strREDatabase
                rsRE.OpenRecordset("SELECT ownerpartyid,deedname1 FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngAccount), modExtraModules.strREDatabase);
                if (!rsRE.EndOfFile())
                {
                    cPartyController tPC = new cPartyController();
                    // VBto upgrade warning: tP As cParty	OnWrite(cParty)
                    cParty tP = new cParty();
                    tP = tPC.GetParty(FCConvert.ToInt32(rsRE.Get_Fields_Int32("ownerpartyid")));
                    boolREMatch = true;
                    if (!(tP == null))
                    {
                        // if the names are not the same then check the sales records or previous owner tables
                        if (Strings.Trim(Strings.UCase(rsRE.Get_Fields_String("DeedName1"))) != Strings.Trim(Strings.UCase(strName)))
                        {
                            rsSR.OpenRecordset("SELECT * FROM SRMaster WHERE RSAccount = " + FCConvert.ToString(lngAccount) + " AND RSCard = 1 AND SaleDate > '" + FCConvert.ToString(dtDate) + "'", modExtraModules.strREDatabase);
                            if (!rsSR.EndOfFile())
                            {
                                return true;
                            }
                            rsSR.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + FCConvert.ToString(lngAccount) + " AND SaleDate > '" + FCConvert.ToString(dtDate) + "'", modExtraModules.strREDatabase);
                            if (!rsSR.EndOfFile())
                            {
                                return true;
                            }
                        }
                        else
                        {
                            boolNameMatch = true;
                        }
                    }
                }
                else
                {
                    boolREMatch = false;
                }
                return NewOwner;
            }
            catch (Exception ex)
            {
                
                NewOwner = false;
            }
            return NewOwner;
        }

        public static bool CheckTranCode_78(bool boolRE, int lngAcct, int lngTrancode1, object lngTrancode2)
        {
            return CheckTranCode(ref boolRE, ref lngAcct, ref lngTrancode1, ref lngTrancode2);
        }

        public static bool CheckTranCode(ref bool boolRE, ref int lngAcct, ref int lngTrancode1, ref object lngTrancode2)
        {
            bool CheckTranCode = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsT = new clsDRWrapper();
                if (boolRE)
                {
                    rsT.OpenRecordset("SELECT RITrancode FROM Master WHERE RSAccount = " + FCConvert.ToString(lngAcct) + " AND RSCard = 1", modExtraModules.strREDatabase);
                    if (!rsT.EndOfFile())
                    {
                        if (Conversion.Val(rsT.Get_Fields_Int32("RITranCode")) >= lngTrancode1 && Conversion.Val(rsT.Get_Fields_Int32("RITranCode")) <= Conversion.Val(lngTrancode2))
                        {
                            CheckTranCode = true;
                        }
                    }
                }
                else
                {
                    rsT.OpenRecordset("SELECT Trancode FROM Master WHERE Account = " + FCConvert.ToString(lngAcct), modExtraModules.strPPDatabase);
                    if (!rsT.EndOfFile())
                    {
                        // TODO: Check the table for the column [Trancode] and replace with corresponding Get_Field method
                        // TODO: Check the table for the column [Trancode] and replace with corresponding Get_Field method
                        if (Conversion.Val(rsT.Get_Fields("Trancode")) >= lngTrancode1 && Conversion.Val(rsT.Get_Fields("Trancode")) <= Conversion.Val(lngTrancode2))
                        {
                            CheckTranCode = true;
                        }
                    }
                }
                return CheckTranCode;
            }
            catch (Exception ex)
            {
                
                CheckTranCode = false;
            }
            return CheckTranCode;
        }

        public static void ClearCMFTable_Collections(string strType)
        {
            // MAL@20071022: Function to clear any records in the CMFNumbers table with matching type
            // Call Reference: 114824
            clsDRWrapper rsCMF = new clsDRWrapper();
            rsCMF.Execute("DELETE FROM CMFNumbers WHERE ProcessType = '" + strType + "'", modExtraModules.strCLDatabase);
        }

        public static int CalculateInterestedParties(int lngAccount, clsDRWrapper rsIP = null)
        {
            int CalculateInterestedParties = 0;
            int lngReturn = 0;
            if (rsIP == null)
            {
                return 0;
            }
            rsIP.OpenRecordset("SELECT * FROM Owners WHERE Account = " + FCConvert.ToString(lngAccount) + " AND (ModuleCode = 'RE') AND ReceiveCopies = 1 AND ISNULL(AssocID,0) = 0", modExtraModules.strREDatabase);
            if (rsIP.RecordCount() > 0)
            {
                rsIP.MoveLast();
                rsIP.MoveFirst();
                lngReturn = rsIP.RecordCount();
            }
            else
            {
                lngReturn = 0;
            }
            CalculateInterestedParties = lngReturn;
            return CalculateInterestedParties;
        }

        public static void CLCalcSetup()
        {
            // kk03242014 trocl-816  Add this sub so we can set the basis and overpay rate so we can
            // calculate interest for group balance without running CLSetup
            // The globals are the same in CL and UT, but the values may be different
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper WCL = new clsDRWrapper();
                WCL.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
                if (!WCL.EndOfFile())
                {
                    modGlobal.Statics.gintBasis = FCConvert.ToInt32(WCL.Get_Fields_Int32("Basis"));
                    modExtraModules.Statics.dblOverPayRate = WCL.Get_Fields_Double("OverPayInterestRate");
                    StaticSettings.TaxCollectionSettings.DaysInAYear = modGlobal.Statics.gintBasis;
                    StaticSettings.TaxCollectionSettings.OverPaymentInterestRate =
                        modExtraModules.Statics.dblOverPayRate.ToDecimal();
                    StaticSettings.TaxCollectionSettings.DiscountRate =
                        WCL.Get_Fields_Double("DiscountPercent").ToDecimal();
                }
                else
                {
                    MessageBox.Show("An error occured while opening TWCL0000.vb1.  Please make sure that you are running from the correct directory.", "Open Database Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return;
            }
            catch
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Collections Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static Boolean OwnerLastRecordedAddress(string strName, int lngAccount, DateTime dtBillDate, ref string strSecondOwnerName, ref string strAddress1, ref string strAddress2, ref string strAddress3)
        {
            string strCity = "";
            string strState = "";
            string strZip = "";
            string strZip4 = "";
            return OwnerLastRecordedAddress(strName, lngAccount, dtBillDate, ref strSecondOwnerName, ref strAddress1, ref strAddress2, ref strAddress3, ref strCity, ref strState, ref strZip, ref strZip4);
        }
        public static Boolean OwnerLastRecordedAddress(string strName, int lngAccount, DateTime dtBillDate, ref string strSecondOwnerName, ref string strAddress1, ref string strAddress2, ref string strAddress3, ref string strCity, ref string strState, ref string strZip, ref string strZip4)
        {
            Boolean returnVal = false;
            try
            {
                clsDRWrapper rsSR = new clsDRWrapper();
                clsDRWrapper rsRE = new clsDRWrapper();
                DateTime dtDate = new DateTime(dtBillDate.Year, 4, 1);

                rsRE.OpenRecordset("Select ownerpartyid,Deedname1 from Master where rscard = 1 and rsaccount = " + lngAccount, modExtraModules.strREDatabase);
                if (!rsRE.EndOfFile())
                {

                    if (rsRE.Get_Fields_String("DeedName1").Trim().ToUpper() != strName.Trim().ToUpper())
                    {
                        rsSR.OpenRecordset("Select * from SRMaster where RSAccount = " + lngAccount.ToString() + " and RSCard = 1 and saledate > '" + dtDate.Month + "/" + dtDate.Day + "/" + dtDate.Year + "'", modExtraModules.strREDatabase);
                        if (!rsSR.EndOfFile())
                        {
                            while (!rsSR.EndOfFile())
                            {
                                if (rsSR.Get_Fields_String("RSName").Trim().ToUpper() == strName.Trim().ToUpper())
                                {
                                    strSecondOwnerName = rsSR.Get_Fields_String("RsSecOwner");
                                    strAddress1 = rsSR.Get_Fields_String("RSAddr1");
                                    strAddress2 = rsSR.Get_Fields_String("RsAddr2");
                                    strAddress3 = rsSR.Get_Fields_String("RSAddr3") + ", " + rsSR.Get_Fields_String("RSState") + " " + rsSR.Get_Fields_String("RSZip") + " " + rsSR.Get_Fields_String("RSZip4");
                                    strCity = rsSR.Get_Fields_String("RSAddr3");
                                    strState = rsSR.Get_Fields_String("RSState");
                                    strZip = rsSR.Get_Fields_String("RSZip");
                                    strZip4 = rsSR.Get_Fields_String("RSZip4");
                                    if (!String.IsNullOrWhiteSpace(rsSR.Get_Fields_String("RSAddr1") + rsSR.Get_Fields_String("RSAddr2") + rsSR.Get_Fields_String("RSAddr3")))
                                    {
                                        return true;
                                    }
                                    returnVal = true;
                                }
                                rsSR.MoveNext();
                            }
                        }

                        rsSR.OpenRecordset("Select * from PreviousOwner where Account = " + lngAccount.ToString() + " and SaleDate > '" + dtDate.Month + "/" + dtDate.Day + "/" + dtDate.Year + "'", modExtraModules.strREDatabase);
                        if (!rsSR.EndOfFile())
                        {
                            while (!rsSR.EndOfFile())
                            {
                                if (rsSR.Get_Fields_String("Name").Trim().ToUpper() == strName.Trim().ToUpper())
                                {
                                    strSecondOwnerName = rsSR.Get_Fields_String("SecOwner");
                                    strAddress1 = rsSR.Get_Fields_String("Address1");
                                    strAddress2 = rsSR.Get_Fields_String("Address2");
                                    strAddress3 = rsSR.Get_Fields_String("City") + ", " + rsSR.Get_Fields_String("State") + " " + rsSR.Get_Fields_String("Zip") + " " + rsSR.Get_Fields_String("Zip4");
                                    strCity = rsSR.Get_Fields_String("City");
                                    strState = rsSR.Get_Fields_String("State");
                                    strZip = rsSR.Get_Fields_String("Zip");
                                    strZip4 = rsSR.Get_Fields_String("Zip4");
                                    return true;
                                }
                                rsSR.MoveNext();
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return returnVal;
        }

        public class StaticVariables
        {
            // ********************************************************
            // LAST UPDATED   :               01/26/2007              *
            // MODIFIED BY    :               Jim Bertolino           *
            // *
            // DATE           :               09/13/2002              *
            // WRITTEN BY     :               Jim Bertolino           *
            // *
            // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
            // ********************************************************
            //=========================================================
            public bool gboolShowMapLotInCLAudit;
            public bool gboolCLUseTownSeal30DN;
            // If this is true and a town seal is setup then show it on 30DN, Liens, Lien mat and Discharge Notices
            public bool gboolCLUseTownSealLien;
            // If this is true and a town seal is setup then show it on 30DN, Liens, Lien mat and Discharge Notices
            public bool gboolCLUseTownSealLienMat;
            // If this is true and a town seal is setup then show it on 30DN, Liens, Lien mat and Discharge Notices
            public bool gboolCLUseTownSealLienDischarge;
            // If this is true and a town seal is setup then show it on 30DN, Liens, Lien mat and Discharge Notices
            public bool gboolDefaultRTDToAutoChange;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
