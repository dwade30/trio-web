﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for arARDailyAudit.
	/// </summary>
	public partial class arARDailyAudit : BaseSectionReport
	{
		// nObj = 1
		//   0	arARDailyAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/10/2005              *
		// ********************************************************
		public bool boolWide;
		// True if this report is in Wide Format
		public float lngWidth;
		// This is the Print Width of the report
		bool boolDone;
		public bool boolOrderByReceipt;
		// order by receipt
		bool boolCloseOutAudit;
		// Actually close out the payments
		public bool boolLandscape;
		// is the report to be printed landscape?
		public string strOrder = "";

		public arARDailyAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static arARDailyAudit InstancePtr
		{
			get
			{
				return (arARDailyAudit)Sys.GetInstance(typeof(arARDailyAudit));
			}
		}

		protected arARDailyAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// this should let the detail section fire once
			if (boolDone)
			{
				eArgs.EOF = boolDone;
			}
			else
			{
				eArgs.EOF = boolDone;
				boolDone = true;
			}
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//clsDRWrapper rsCloseOut = new clsDRWrapper();
			string strSQL = "";
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsOrder = new clsDRWrapper();
			StartUp();
			// If rsOrder.OpenRecordset("SELECT * FROM Collections", strCLDatabase, , , , , , False) Then
			// If rsOrder.EndOfFile <> True Then
			// If rsOrder.Get_Fields("AuditSeqReceipt") Then
			// boolOrderByReceipt = True
			// Else
			// boolOrderByReceipt = False
			// End If
			// Else
			// boolOrderByReceipt = True
			// End If
			// Else
			// MsgBox "An error has occured while determining your default Audit sort order.  This will not affect the data.  The order has been set to Receipt Number.", vbInformation, "Collections Table Error"
			// boolOrderByReceipt = True
			// End If
			if (boolOrderByReceipt)
			{
				strOrder = " ORDER BY ReceiptNumber";
			}
			else
			{
				strOrder = "";
			}
			rsOrder.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut), modExtraModules.strARDatabase);
			if (rsOrder.EndOfFile())
			{
				lblOutput.Visible = true;
				lblOutput.Text = "There are no collection records to process at this time.";
			}
			else
			{
				lblOutput.Visible = false;
				sarDADetailOb.Visible = true;
				sarDACashOb.Visible = true;
				sarDANonCashOb.Visible = true;
				sarDATypeSummaryOB.Visible = true;
				// set the subreports
				sarDADetailOb.Report = new sarARDADetail();
				sarDACashOb.Report = new sarARDACash();
				sarDANonCashOb.Report = new sarARDANonCash();
				sarDATypeSummaryOB.Report = new sarARDATypeSummary();
			}
			rsOrder.DisposeOf();
		}

		private void SetupFields()
		{
			// find out which way the user wants to print
			boolWide = boolLandscape;
			if (boolWide)
			{
				// wide format
				lngWidth = 13500 / 1440f;
				Document.Printer.Landscape = true;
			}
			else
			{
				// narrow format
				lngWidth = 10750 / 1440f;
				Document.Printer.Landscape = false;
			}
			this.PrintWidth = lngWidth;
			lblPreview.Width = lngWidth;
			sarDADetailOb.Width = lngWidth;
			sarDACashOb.Width = lngWidth;
			sarDANonCashOb.Width = lngWidth;
			sarDATypeSummaryOB.Width = lngWidth - 1 / 1440F;
			sarDADetailOb.Left = 0;
			sarDACashOb.Left = 0;
			sarDANonCashOb.Left = 0;
			sarDATypeSummaryOB.Left = 0;
			lblOutput.Width = lngWidth;
			lblHeader.Width = lngWidth;
			lblDate.Left = lngWidth - lblDate.Width;
			lblPage.Left = lngWidth - lblPage.Width;
		}

		private void StartUp()
		{
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			// these are passed from rptCLDailyAuditMaster
			boolCloseOutAudit = rptARDailyAuditMaster.InstancePtr.boolCloseOutAudit;
			if (boolCloseOutAudit)
			{
				lblPreview.Visible = false;
			}
			else
			{
				lblPreview.Visible = true;
			}
			boolLandscape = rptARDailyAuditMaster.InstancePtr.boolLandscape;
			lblHeader.Text = "Accounts Receivable Daily Audit Report";
			SetupFields();
			frmWait.InstancePtr.IncrementProgress();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + rptARDailyAuditMaster.InstancePtr.PageNumber;
		}

		private void arARDailyAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arARDailyAudit.Caption	= "Daily Audit Report";
			//arARDailyAudit.Icon	= "arARDailyAudit.dsx":0000";
			//arARDailyAudit.Left	= 0;
			//arARDailyAudit.Top	= 0;
			//arARDailyAudit.Width	= 11880;
			//arARDailyAudit.Height	= 8415;
			//arARDailyAudit.StartUpPosition	= 3;
			//arARDailyAudit.SectionData	= "arARDailyAudit.dsx":058A;
			//End Unmaped Properties
		}
	}
}
