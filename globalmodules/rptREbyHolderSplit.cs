﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;
#elif TWBD0000
using TWBD0000;
#elif TWCR0000
using TWCR0000;
#elif TWBL0000
using TWBL0000;
using modExtraModules = TWBL0000.modGlobalVariables;
#elif TWRE0000
using modExtraModules = TWRE0000.modGlobalVariables;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptREbyHolderSplit : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/15/2005              *
		// ********************************************************
		clsDRWrapper clsre = new clsDRWrapper();
		clsDRWrapper clsMortgage = new clsDRWrapper();
		//clsDRWrapper clsAssoc = new clsDRWrapper();
		bool boolTax;
		// whether we should print tax info or not
		clsDRWrapper clsTaxRE = new clsDRWrapper();
		int lngBYear;
		// billing year
		double dblTaxSum1;
		double dblTaxSum2;
		double dblTaxSum3;
		double dblTaxSum4;
		double dblOrigSum;
		double dblOSSum;
		double dblTotalDueSum;
		double dblTax1;
		double dblTax2;
		double dblTax3;
		double dblTax4;
		cPartyController tPCont = new cPartyController();
		bool boolCL = false;

		public rptREbyHolderSplit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate List By Mortgage Holder (Split Periods)";
		}

		public static rptREbyHolderSplit InstancePtr
		{
			get
			{
				return (rptREbyHolderSplit)Sys.GetInstance(typeof(rptREbyHolderSplit));
			}
		}

		protected rptREbyHolderSplit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre.Dispose();
				clsTaxRE.Dispose();
				clsMortgage.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_DataInitialize(object sender, System.EventArgs e)
		{
			this.Fields.Add("TheBinder");
			this.Fields["TheBinder"].Value = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
			if (!eArgs.EOF)
			{
				// do it here or the groupheader info will be behind the times
				this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("mortgageholderid");
			}
			//Detail_Format();
		}

		public void Init(bool boolNameOrder, bool boolShowTax, int lngBillYear, string strStart = "", string strEnd = "")
		{
			string strSQL = "";
			string strOrder = "";
			string str1;
			string str2;
			string str3;
			string str4;
			string strWhere = "";
			string strDBName;
			try
			{
				boolCL = modGlobalConstants.Statics.gboolCL;
				// On Error GoTo ErrorHandler
				if (boolNameOrder)
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " and name >= '" + strStart + "' ";
						}
					}
					else
					{
						strWhere = " and name between '" + strStart + "' and '" + strEnd + "' ";
					}
				}
				else
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " and mortgageholderid >= " + FCConvert.ToString(Conversion.Val(strStart)) + " ";
						}
					}
					else
					{
						strWhere = " and mortgageholderid between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
					}
				}
				clsre.OpenRecordset("select * from returnaddress", "twbl0000.vb1");
				str1 = "";
				str2 = "";
				str3 = "";
				str4 = "";
				if (!clsre.EndOfFile())
				{
					str1 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("address1")));
					str2 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("address2")));
					str3 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("address3")));
					str4 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("address4")));
					if (Strings.Trim(str3) == string.Empty)
					{
						str3 = str4;
						str4 = "";
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						str2 = str3;
						str3 = str4;
						str4 = "";
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						str1 = str2;
						str2 = str3;
						str3 = str4;
						str4 = "";
					}
				}
				if (str1 != string.Empty)
				{
					txtReturnAddress1.Text = str1;
					txtReturnAddress2.Text = str2;
					txtReturnAddress3.Text = str3;
					txtReturnAddress4.Text = str4;
				}
				else
				{
					lblReturnTo.Visible = false;
				}
				boolTax = boolShowTax;
				lngBYear = lngBillYear;
				if (boolShowTax && boolCL)
				{
					lnTotal.Visible = true;
					// txtTotalOutstanding.Visible = True
					txtTotalOriginal.Visible = true;
					txtTotalTax1.Visible = true;
					txtTotalTax2.Visible = true;
					txtTotalTax3.Visible = true;
					txtTotalTax4.Visible = true;
					// lblOutstanding.Visible = True
					// txtOutstanding.Visible = True
					lblOriginal.Visible = true;
					lblTax1.Visible = true;
					lblTax2.Visible = true;
					lblTax3.Visible = true;
					lblTax4.Visible = true;
					txtOriginal.Visible = true;
					txtTax1.Visible = true;
					txtTax2.Visible = true;
					txtTax3.Visible = true;
					txtTax4.Visible = true;
					lblTaxYear.Text = "Tax Year " + FCConvert.ToString(lngBYear);
					lblTotalDue.Text = "Total Due as of " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
					txtTotalDue.Visible = true;
					txtTotalDueTotal.Visible = true;
				}
				else
				{
					lnTotal.Visible = false;
					// txtTotalOutstanding.Visible = False
					txtTotalOriginal.Visible = false;
					txtTotalTax1.Visible = false;
					txtTotalTax2.Visible = false;
					txtTotalTax3.Visible = false;
					txtTotalTax4.Visible = false;
					// lblOutstanding.Visible = False
					// txtOutstanding.Visible = False
					lblOriginal.Visible = false;
					lblTax1.Visible = false;
					lblTax2.Visible = false;
					lblTax3.Visible = false;
					lblTax4.Visible = false;
					txtOriginal.Visible = false;
					txtTax1.Visible = false;
					txtTax2.Visible = false;
					txtTax3.Visible = false;
					txtTax4.Visible = false;
					lblTotalDue.Visible = false;
					txtTotalDue.Visible = false;
					txtTotalDueTotal.Visible = false;
					GroupFooter1.Height = 0;
				}
				if (boolNameOrder)
				{
					strOrder = " order by name ,DeedName1 ";
				}
				else
				{
					strOrder = " order by mortgageholderid,DeedName1 ";
				}
				strDBName = clsre.Get_GetFullDBName("CentralData") + ".dbo.";
				clsre.OpenRecordset("select * from master inner join (" + strDBName + "mortgageholderS inner join " + strDBName + "mortgageassociation ON (MORtgageassociation.mortgageholderid = mortgageholders.ID)) on (mortgageassociation.account = master.rsaccount) CROSS APPLY " + clsre.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(OwnerPartyID) as p where mortgageassociation.module = 'RE' and not (master.rsdeleted = 1) and master.rscard = 1 " + strWhere + strOrder, "twre0000.vb1");
				if (clsre.EndOfFile())
				{
					FCMessageBox.Show("No records found.", MsgBoxStyle.Information, "No Match");
					Cancel();
					return;
				}
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "", false, false);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Init", MsgBoxStyle.Critical, "Error");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			txtDate.Text = DateTime.Today.ToString();
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strAddress1 = "";
				string strAddress2 = "";
				string strAddress3 = "";
				string strAddress4 = "";
				double dblOS/*unused?*/;
				double dblTotalDue = 0;
				double dblOriginal = 0;
				txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				if (!clsre.EndOfFile())
				{
					// set the group binder
					if (!boolTax)
					{
						this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("mortgageholderid");
						txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
						txtName.Text = clsre.Get_Fields_String("DeedName1");
						txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocnumalph")) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocapt"))) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocstreet")));
						txtTax1.Text = "";
						txtTax2.Text = "";
						txtTax3.Text = "";
						txtTax4.Text = "";
						txtOriginal.Text = "";
						// txtOutstanding.Text = ""
					}
					else
					{
						this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("mortgageholderid");
						txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
						txtName.Text = clsre.Get_Fields_String("DeedName1");
						txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocnumalph")) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocapt"))) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocstreet")));
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						CalcOutstandingPerPeriod_2(FCConvert.ToInt32(clsre.Get_Fields("Account")), lngBYear);
						if (dblTax1 > 0)
						{
							txtTax1.Text = Strings.Format(dblTax1, "#,###,###,##0.00");
						}
						else
						{
							txtTax1.Text = "0.00";
						}
						if (dblTax2 > 0)
						{
							txtTax2.Text = Strings.Format(dblTax2, "#,###,###,##0.00");
						}
						else
						{
							txtTax2.Text = "0.00";
						}
						if (dblTax3 > 0)
						{
							txtTax3.Text = Strings.Format(dblTax3, "#,###,###,##0.00");
						}
						else
						{
							txtTax3.Text = "0.00";
						}
						if (dblTax4 > 0)
						{
							txtTax4.Text = Strings.Format(dblTax4, "#,###,###,##0.00");
						}
						else
						{
							txtTax4.Text = "0.00";
						}
						if (boolCL && boolTax)
						{
							dblTotalDue = modCLCalculations.CalculateAccountTotal1(FCConvert.ToInt32(clsre.Get_Fields_Int32("RSAccount")), true);
							txtTotalDue.Text = Strings.Format(dblTotalDue, "#,###,###,##0.00");
						}
						// Call clsTaxRE.OpenRecordset("select sum(taxdue1 + taxdue2 + taxdue3 + taxdue4) as taxsum from billingmaster where account = " & clsre.Fields("account") & " and billingyear between " & lngBYear * 10 & " and " & (lngBYear * 10 + 9) & " and billingtype = 'RE'", "twcl0000.vb1")
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						clsTaxRE.OpenRecordset("SELECT TaxDue1, TaxDue2 , TaxDue3 , TaxDue4 FROM BillingMaster WHERE Account = " + FCConvert.ToString(clsre.Get_Fields("Account")) + " and BillingYear Between " + FCConvert.ToString(lngBYear * 10) + " AND " + FCConvert.ToString(lngBYear * 10 + 9) + " AND BillingType = 'RE'", "TWCL0000.vb1");
						if (!clsTaxRE.EndOfFile())
						{
							dblOriginal = FCConvert.ToDouble(Conversion.Val(clsTaxRE.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(clsTaxRE.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(clsTaxRE.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(clsTaxRE.Get_Fields_Decimal("TaxDue4")));
							if (dblOriginal > 0)
							{
								txtOriginal.Text = Strings.Format(dblOriginal, "#,###,###,##0.00");
							}
						}
						// If Val(clsTaxRE.Fields("TaxDue1")) > 0 Then
						// dblTax1 = CDbl(clsTaxRE.Fields("TaxDue1"))
						// txtTax1.Text = Format(dblTax1, "#,###,###,##0.00")
						// Else
						// txtTax1.Text = "0.00"
						// End If
						// 
						// If Val(clsTaxRE.Fields("TaxDue2")) > 0 Then
						// dblTax2 = CDbl(clsTaxRE.Fields("TaxDue2"))
						// txtTax2.Text = Format(dblTax2, "#,###,###,##0.00")
						// Else
						// txtTax2.Text = "0.00"
						// End If
						// 
						// If Val(clsTaxRE.Fields("TaxDue3")) > 0 Then
						// dblTax3 = CDbl(clsTaxRE.Fields("TaxDue3"))
						// txtTax3.Text = Format(dblTax3, "#,###,###,##0.00")
						// Else
						// txtTax3.Text = "0.00"
						// End If
						// 
						// If Val(clsTaxRE.Fields("TaxDue4")) > 0 Then
						// dblTax4 = CDbl(clsTaxRE.Fields("TaxDue4"))
						// txtTax4.Text = Format(dblTax4, "#,###,###,##0.00")
						// Else
						// txtTax4.Text = "0.00"
						// End If
						// 
						// If gboolCL And boolTax Then
						// dblOS = CalcOutstandingForYear(clsre.Fields("RSAccount"), lngBYear)
						// txtOutstanding.Text = Format(dblOS, "#,###,###,##0.00")
						// 
						// dblTotalDue = CalculateAccountTotal(clsre.Fields("RSAccount"), True)
						// txtTotalDue.Text = Format(dblTotalDue, "#,###,###,##0.00")
						// 
						// End If
						// Else
						// txtTax1.Text = "0.00"
						// txtTax2.Text = "0.00"
						// txtTax3.Text = "0.00"
						// txtTax4.Text = "0.00"
						// txtOutstanding.Text = "0.00"
						// txtTotalDue.Text = "0.00"
						// End If
					}
					dblTaxSum1 += dblTax1;
					dblTaxSum2 += dblTax2;
					dblTaxSum3 += dblTax3;
					dblTaxSum4 += dblTax4;
					dblOrigSum += dblOriginal;
					// dblOSSum = dblOSSum + dblOS
					dblTotalDueSum += dblTotalDue;
					clsre.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Detail Format");
			}
		}

		private void GroupFooter1_Format(object sender, System.EventArgs e)
		{
			if (boolCL && boolTax)
			{
				// txtTotalOutstanding.Text = Format(dblOSSum, "#,##0.00")
				txtTotalTax1.Text = Strings.Format(dblTaxSum1, "#,##0.00");
				txtTotalTax2.Text = Strings.Format(dblTaxSum2, "#,##0.00");
				txtTotalTax3.Text = Strings.Format(dblTaxSum3, "#,##0.00");
				txtTotalTax4.Text = Strings.Format(dblTaxSum4, "#,##0.00");
				txtTotalDueTotal.Text = Strings.Format(dblTotalDueSum, "#,##0.00");
				txtTotalOriginal.Text = Strings.Format(dblOrigSum, "#,##0.00");
				dblOSSum = 0;
				dblTaxSum1 = 0;
				dblTaxSum2 = 0;
				dblTaxSum3 = 0;
				dblTaxSum4 = 0;
				dblTotalDueSum = 0;
				dblOrigSum = 0;
			}
			else
			{
				// txtTotalOutstanding.Text = ""
				txtTotalTax1.Text = "";
				txtTotalTax2.Text = "";
				txtTotalTax3.Text = "";
				txtTotalTax4.Text = "";
				txtTotalDueTotal.Text = "";
				txtTotalOriginal.Text = "";
			}
		}

		private void GroupHeader1_Format(object sender, System.EventArgs e)
		{
			string strAddress1;
			string strAddress2;
			// VBto upgrade warning: strAddress3 As object	OnWrite(object, string)
			string strAddress3;
			// VBto upgrade warning: strAddress4 As Variant --> As string
			string strAddress4;
			clsMortgage.OpenRecordset("select * from mortgageholders where ID = " + this.Fields["TheBinder"].Value, "CentralData");
			if (clsMortgage.EndOfFile())
				return;
			txtNumber.Text = this.Fields["thebinder"].Value.ToString();
			txtMortgageName.Text = clsMortgage.Get_Fields_String("name");
			strAddress1 = clsMortgage.Get_Fields_String("address1");
			strAddress2 = clsMortgage.Get_Fields_String("address2");
			// straddress3 = Trim(Trim(clsMortgage.Fields("streetnumber") & " " & clsMortgage.Fields("apt")) & " " & clsMortgage.Fields("streetname"))
			strAddress3 = clsMortgage.Get_Fields_String("address3");
			if (FCConvert.ToString(strAddress3) == "0")
				strAddress3 = "";
			strAddress4 = Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("city")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("state")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("zip")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("zip4")));
			// now condense this info
			if (FCConvert.ToString(strAddress3) == string.Empty)
			{
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			if (FCConvert.ToString(strAddress2) == string.Empty)
			{
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			if (FCConvert.ToString(strAddress1) == string.Empty)
			{
				strAddress1 = strAddress2;
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			txtAddress1.Text = strAddress1;
			txtAddress2.Text = strAddress2;
			txtAddress3.Text = strAddress3;
			txtAddress4.Text = strAddress4;
		}
		// VBto upgrade warning: 'Return' As Variant --> As double	OnWrite(short, double)
		private double CalcOutstandingForYear(ref int lngAccountNumber, ref int lngYear)
		{
			double CalcOutstandingForYear = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the total remaining balance and
				// return it as a double for the account and type passed in
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				string strSQL;
				double dblTotal = 0;
				double dblXtraInt = 0;
				CalcOutstandingForYear = 0;
				strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = 'RE'  and billingyear between " + FCConvert.ToString(lngYear * 10) + " and " + FCConvert.ToString(lngYear * 10 + 9);
				rsLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
				rsCL.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				if (!rsCL.EndOfFile())
				{
					while (!rsCL.EndOfFile())
					{
						// calculate each year
						if (((rsCL.Get_Fields_Int32("LienRecordNumber"))) == 0)
						{
							// non-lien
							dblTotal += modCLCalculations.CalculateAccountCL(ref rsCL, lngAccountNumber, DateTime.Today, ref dblXtraInt);
						}
						else
						{
							// lien
							// rsLien.FindFirstRecord "LienRecordNumber", rsCL.Fields("LienrecordNumber")
							rsLien.FindFirst("ID = " + FCConvert.ToString(rsCL.Get_Fields_Int32("lienrecordnumber")));
							if (rsLien.NoMatch)
							{
								// no match has been found
								// do nothing
							}
							else
							{
								// calculate the lien record
								dblTotal += modCLCalculations.CalculateAccountCLLien(rsLien, DateTime.Today, ref dblXtraInt);
							}
						}
						rsCL.MoveNext();
					}
				}
				else
				{
					// nothing found
					dblTotal = 0;
				}
				CalcOutstandingForYear = dblTotal;
				return CalcOutstandingForYear;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Calculate Account Total Error");
			}
			return CalcOutstandingForYear;
		}

		private void CalcOutstandingPerPeriod_2(int lngAccountNumber, int lngYear)
		{
			CalcOutstandingPerPeriod(ref lngAccountNumber, ref lngYear);
		}

		private void CalcOutstandingPerPeriod(ref int lngAccountNumber, ref int lngYear)
		{
			// MAL@20080114: Calculate amount due per period (non-lien records)
			// Call Reference: 117211
			clsDRWrapper rsTax = new clsDRWrapper();
			double dblTotal/*unused?*/;
			double dblPrinPaid = 0;
			double dblDemand = 0;
			double dblInterest = 0;
			double dblCurInt = 0;
			double dblTempTot;
			rsTax.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " and BillingYear Between " + FCConvert.ToString(lngYear * 10) + " AND " + FCConvert.ToString(lngYear * 10 + 9) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
			if (rsTax.RecordCount() > 0)
			{
				dblTempTot = modCLCalculations.CalculateAccountCL2(ref rsTax, lngAccountNumber, DateTime.Today, ref dblCurInt, Conversion.Val(rsTax.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsTax.Get_Fields_Decimal("DemandFeesPaid")));
				dblPrinPaid = Conversion.Val(rsTax.Get_Fields_Decimal("PrincipalPaid"));
				dblDemand = Conversion.Val(rsTax.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsTax.Get_Fields_Decimal("DemandFeesPaid"));
				dblInterest = (dblCurInt - Conversion.Val(rsTax.Get_Fields_Decimal("InterestCharged"))) - Conversion.Val(rsTax.Get_Fields_Decimal("InterestPaid"));
				if ((Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue1")) + dblDemand) > dblPrinPaid)
				{
					dblTax1 = (Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue1")) + dblDemand + dblInterest) - dblPrinPaid;
					dblPrinPaid = 0;
				}
				else
				{
					dblTax1 = 0;
					dblPrinPaid -= (Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue1")) + dblDemand);
				}
				if (Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue2")) > dblPrinPaid)
				{
					dblTax2 = Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue2")) - dblPrinPaid;
					dblPrinPaid = 0;
				}
				else
				{
					dblTax2 = 0;
					dblPrinPaid -= Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue2"));
				}
				if (Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue3")) > dblPrinPaid)
				{
					dblTax3 = Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue3")) - dblPrinPaid;
					dblPrinPaid = 0;
				}
				else
				{
					dblTax3 = 0;
					dblPrinPaid -= Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue3"));
				}
				if (Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue4")) > dblPrinPaid)
				{
					dblTax4 = Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue4")) - dblPrinPaid;
					dblPrinPaid = 0;
				}
				else
				{
					dblTax4 = 0;
					dblPrinPaid -= Conversion.Val(rsTax.Get_Fields_Decimal("TaxDue4"));
				}
			}
			else
			{
				dblTax1 = 0;
				dblTax2 = 0;
				dblTax3 = 0;
				dblTax4 = 0;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void rptREbyHolderSplit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptREbyHolderSplit.Text	= "Real Estate List By Mortgage Holder (Split Periods)";
			//rptREbyHolderSplit.Icon	= "rptREbyHolderSplit.dsx":0000";
			//rptREbyHolderSplit.Left	= 0;
			//rptREbyHolderSplit.Top	= 0;
			//rptREbyHolderSplit.Width	= 11880;
			//rptREbyHolderSplit.Height	= 8490;
			//rptREbyHolderSplit.StartUpPosition	= 3;
			//rptREbyHolderSplit.SectionData	= "rptREbyHolderSplit.dsx":058A;
			//End Unmaped Properties
		}
	}
}
