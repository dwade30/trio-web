﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWXF0000
{
	public class cREFullAccount
	{
		//=========================================================
		private int lngRecordID;
		private int lngAccountNumber;
		private int lngOwnerID;
		private int lngSecOwnerID;
		private bool boolTaxAcquired;
		private bool boolInBankruptcy;
		private bool boolRevocableTrust;
		private cParty pOwner = new cParty();
		private cParty pSecOwner = new cParty();
		private string strMapLot = string.Empty;
		private string strAccountID = string.Empty;
		private bool boolInactive;
		private cGenericCollection collCards = new cGenericCollection();
		private cGenericCollection collExemptions = new cGenericCollection();
		private string strPreviousOwner = string.Empty;
		private cRESaleItem siSaleItem = new cRESaleItem();
		private int lngTranCode;
		private bool boolUpdated;
		private bool boolDeleted;

		public cRESaleItem SaleInformation
		{
			get
			{
				cRESaleItem SaleInformation = null;
				SaleInformation = siSaleItem;
				return SaleInformation;
			}
		}

		public string PreviousOwner
		{
			set
			{
				strPreviousOwner = value;
				IsUpdated = true;
			}
			get
			{
				string PreviousOwner = "";
				PreviousOwner = strPreviousOwner;
				return PreviousOwner;
			}
		}

		public int TranCode
		{
			set
			{
				lngTranCode = value;
				IsUpdated = true;
			}
			get
			{
				int TranCode = 0;
				TranCode = lngTranCode;
				return TranCode;
			}
		}

		public bool IsInactive
		{
			set
			{
				boolInactive = value;
				IsUpdated = true;
			}
			get
			{
				bool IsInactive = false;
				IsInactive = boolInactive;
				return IsInactive;
			}
		}

		public string MapLot
		{
			set
			{
				strMapLot = value;
				IsUpdated = true;
			}
			get
			{
				string MapLot = "";
				MapLot = strMapLot;
				return MapLot;
			}
		}

		public string AccountID
		{
			set
			{
				strAccountID = value;
				IsUpdated = true;
			}
			get
			{
				string AccountID = "";
				AccountID = strAccountID;
				return AccountID;
			}
		}

		public cGenericCollection Exemptions
		{
			get
			{
				cGenericCollection Exemptions = null;
				Exemptions = collExemptions;
				return Exemptions;
			}
		}

		public int AccountNumber
		{
			set
			{
				lngAccountNumber = value;
				IsUpdated = true;
			}
			get
			{
				int AccountNumber = 0;
				AccountNumber = lngAccountNumber;
				return AccountNumber;
			}
		}

		public int OwnerID
		{
			set
			{
				lngOwnerID = value;
				IsUpdated = true;
			}
			get
			{
				int OwnerID = 0;
				OwnerID = lngOwnerID;
				return OwnerID;
			}
		}

		public int SecondOwnerID
		{
			set
			{
				lngSecOwnerID = value;
				IsUpdated = true;
			}
			get
			{
				int SecondOwnerID = 0;
				SecondOwnerID = lngSecOwnerID;
				return SecondOwnerID;
			}
		}

		public bool TaxAcquired
		{
			set
			{
				boolTaxAcquired = value;
				IsUpdated = true;
			}
			get
			{
				bool TaxAcquired = false;
				TaxAcquired = boolTaxAcquired;
				return TaxAcquired;
			}
		}

		public bool InBankruptcy
		{
			set
			{
				boolInBankruptcy = value;
				IsUpdated = true;
			}
			get
			{
				bool InBankruptcy = false;
				InBankruptcy = boolInBankruptcy;
				return InBankruptcy;
			}
		}

		public bool RevocableTrust
		{
			set
			{
				boolRevocableTrust = value;
				IsUpdated = true;
			}
			get
			{
				bool RevocableTrust = false;
				RevocableTrust = boolRevocableTrust;
				return RevocableTrust;
			}
		}

		public cParty Owner
		{
			set
			{
				pOwner = value;
				IsUpdated = true;
			}
			get
			{
				return pOwner;
			}
		}

		public cParty SecondOwner
		{
			set
			{
				pSecOwner = value;
				IsUpdated = true;
			}
			get
			{
				return pSecOwner;
			}
		}

		public cGenericCollection Cards
		{
			get
			{
				cGenericCollection Cards = null;
				Cards = collCards;
				return Cards;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}
	}
}
