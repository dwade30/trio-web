﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;

#if TWCL0000
using TWCL0000;

#elif TWBD0000
using TWBD0000;


#elif TWFA0000
using TWFA0000;


#elif TWAR0000
using TWAR0000;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGlobalRoutines;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalRoutines;


#elif TWCK0000
using modGlobal = TWCK0000.modGNBas;

#elif TWCL0000
using TWCL0000;

#elif TWCL0001
using TWCL0001;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmCalender.
	/// </summary>
	public partial class frmCalender : BaseForm
	{
		public frmCalender()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCalender InstancePtr
		{
			get
			{
				return (frmCalender)Sys.GetInstance(typeof(frmCalender));
			}
		}

		protected frmCalender _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           10/21/2003
		// This form will be used to check out what is happening
		// on a certain month
		// ********************************************************
		int SaturdayCol;
		int SundayCol;
		int MondayCol;
		int TuesdayCol;
		int WednesdayCol;
		int ThursdayCol;
		int FridayCol;
		int WeekOneRow;
		int WeekTwoRow;
		int WeekThreeRow;
		int WeekFourRow;
		int WeekFiveRow;
		int WeekSixRow;
		bool blnEdit;
		int intMonthStartCol;
		int intMonthStartRow;
		int intDaysInMonth;
		// vbPorter upgrade warning: datReturnedDate As DateTime	OnWrite(string, DateTime)
		DateTime datReturnedDate;
		int intSelectedDay;
		DateTime timFirstClick;
		int intClickedCol;
		int intClickedRow;

		private void cboMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblMonthYear.Text = cboMonth.Text + " " + cboYear.Text;
			FormatCalender();
		}

		private void cboYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblMonthYear.Text = cboMonth.Text + " " + cboYear.Text;
			FormatCalender();
		}

		private void frmCalender_Activated(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Maximized && this.WindowState != FormWindowState.Minimized)
			{
				this.Height = this.Height - 15;
			}
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCalender_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCalender.FillStyle	= 0;
			//frmCalender.ScaleWidth	= 5880;
			//frmCalender.ScaleHeight	= 5280;
			//frmCalender.LinkTopic	= "Form2";
			//frmCalender.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			int totalWidth;
			SundayCol = 0;
			MondayCol = 1;
			TuesdayCol = 2;
			WednesdayCol = 3;
			ThursdayCol = 4;
			FridayCol = 5;
			SaturdayCol = 6;
			WeekOneRow = 1;
			WeekTwoRow = 2;
			WeekThreeRow = 3;
			WeekFourRow = 4;
			WeekFiveRow = 5;
			WeekSixRow = 6;
			intSelectedDay = 1;
			vsCalender.TextMatrix(0, SundayCol, "Sun");
			vsCalender.TextMatrix(0, MondayCol, "Mon");
			vsCalender.TextMatrix(0, TuesdayCol, "Tues");
			vsCalender.TextMatrix(0, WednesdayCol, "Wed");
			vsCalender.TextMatrix(0, ThursdayCol, "Thur");
			vsCalender.TextMatrix(0, FridayCol, "Fri");
			vsCalender.TextMatrix(0, SaturdayCol, "Sat");
			vsCalender.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsCalender.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsCalender.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, vsCalender.Rows - 1, vsCalender.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightTop);
			//for (counter = 1; counter <= 6; counter++)
			//{
			//    vsCalender.RowHeight(counter, vsCalender.RowHeight(counter) * 2);
			//}
			for (counter = 0; counter <= vsCalender.Cols - 1; counter++)
			{
				vsCalender.ColWidth(counter, FCConvert.ToInt32(vsCalender.WidthOriginal / 6.6));
				//FC:FINAL:AM: disable sorting
				vsCalender.Columns[counter].SortMode = DataGridViewColumnSortMode.NotSortable;
			}
			blnEdit = true;
			for (counter = DateTime.Now.Year - 100; counter <= DateTime.Now.Year + 2; counter++)
			{
				cboYear.AddItem(counter.ToString());
			}
			cboMonth.SelectedIndex = FCConvert.ToInt32(Strings.Format(DateTime.Today, "MM")) - 1;
			cboYear.SelectedIndex = 1;
			blnEdit = false;
			FormatCalender();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetCustomFormColors();
		}

		private void frmCalender_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (this.ActiveControl.GetName() == "vsCalender")
				{
					mnuProcessSave_Click();
				}
				else
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Label1_Click()
		{
		}

		private void frmCalender_Resize(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= this.vsCalender.Cols - 1; intCounter++)
			{
				vsCalender.ColWidth(intCounter, FCConvert.ToInt32(vsCalender.WidthOriginal * 0.141));
			}
			//for (intCounter = 0; intCounter <= this.vsCalender.Rows - 1; intCounter++)
			//{
			//    vsCalender.RowHeight(intCounter, FCConvert.ToInt32(vsCalender.HeightOriginal * 0.135));
			//}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FormatCalender()
		{
			int counter;
			int counter2;
			// vbPorter upgrade warning: datFirstDay As DateTime	OnWrite(string)
			DateTime datFirstDay;
			// vbPorter upgrade warning: datLastDay As DateTime	OnWrite(string, DateTime)
			DateTime datLastDay;
			string strDay = "";
			int intDayOfWeek = 0;
			if (!blnEdit)
			{
				vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vsCalender.Rows - 1, 6, Color.White);
				for (counter = 1; counter <= WeekSixRow; counter++)
				{
					for (counter2 = 0; counter2 <= vsCalender.Cols - 1; counter2++)
					{
						vsCalender.TextMatrix(counter, counter2, "");
					}
				}
				// show a border between the titles and the data input section of the grid
				//FC:FINAL:AM:#4410 - CellBorder is not supported
                //for (counter = 1; counter <= WeekSixRow; counter++)
				//{
				//	for (counter2 = 0; counter2 <= vsCalender.Cols - 1; counter2++)
				//	{
				//		vsCalender.Select(counter, counter2);
				//		vsCalender.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, 1, 1, -1, -1);
				//	}
				//}
				datFirstDay = FCConvert.ToDateTime(FCConvert.ToString(cboMonth.SelectedIndex + 1) + "/01/" + cboYear.Text);
				strDay = Strings.Format(datFirstDay, "dddd");
				if (Strings.UCase(strDay) == "SUNDAY")
				{
					intDayOfWeek = 0;
				}
				else if (Strings.UCase(strDay) == "MONDAY")
				{
					intDayOfWeek = 1;
				}
				else if (Strings.UCase(strDay) == "TUESDAY")
				{
					intDayOfWeek = 2;
				}
				else if (Strings.UCase(strDay) == "WEDNESDAY")
				{
					intDayOfWeek = 3;
				}
				else if (Strings.UCase(strDay) == "THURSDAY")
				{
					intDayOfWeek = 4;
				}
				else if (Strings.UCase(strDay) == "FRIDAY")
				{
					intDayOfWeek = 5;
				}
				else if (Strings.UCase(strDay) == "SATURDAY")
				{
					intDayOfWeek = 6;
				}
				intMonthStartCol = intDayOfWeek;
				intMonthStartRow = 0;
				if (intDayOfWeek > 0)
				{
					vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, 1, intDayOfWeek - 1, 0xE0E0E0);
				}
				if (datFirstDay.Month == 12)
				{
					datLastDay = FCConvert.ToDateTime("01/01/" + FCConvert.ToString(datFirstDay.Year + 1));
				}
				else
				{
					datLastDay = FCConvert.ToDateTime(FCConvert.ToString(datFirstDay.Month + 1) + "/01/" + FCConvert.ToString(datFirstDay.Year));
				}
				datLastDay = DateAndTime.DateAdd("d", -1, datLastDay);
				intDaysInMonth = datLastDay.Day;
				counter = WeekOneRow;
				counter2 = 1;
				do
				{
					vsCalender.TextMatrix(counter, intDayOfWeek, FCConvert.ToString(counter2));
					counter2 += 1;
					intDayOfWeek += 1;
					if (intDayOfWeek > 6)
					{
						intDayOfWeek = 0;
						counter += 1;
					}
				}
				while (counter2 <= datLastDay.Day);
				if (intDayOfWeek < 7)
				{
					vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, intDayOfWeek, counter, 6, 0xE0E0E0);
				}
				if (counter < WeekSixRow)
				{
					counter = WeekSixRow;
					vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, 6, 0xE0E0E0);
				}
				SelectDay_2(1);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int intCurrentWeek;
			int counter;
			DateTime datStartDate;
			DateTime datEndDate;
			if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row, vsCalender.Col)) == 0xE0E0E0)
			{
				return;
			}
			intCurrentWeek = vsCalender.Row;
			vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCurrentWeek, vsCalender.Col, 0x808080);
			datReturnedDate = FCConvert.ToDateTime(FCConvert.ToString(cboMonth.SelectedIndex + 1) + "/" + vsCalender.TextMatrix(intCurrentWeek, vsCalender.Col) + "/" + cboYear.Text);
			Close();
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private void vsCalender_ClickEvent(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			DateTime timClickTime = DateTime.FromOADate(0);
			if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row, vsCalender.Col)) == 0xE0E0E0)
			{
				return;
			}
			timClickTime += DateTime.Now.TimeOfDay;
			if (Information.IsDate(timFirstClick))
			{
				if (timFirstClick.Minute == timClickTime.Minute && timFirstClick.Second == timClickTime.Second && intClickedCol == vsCalender.Col && intClickedRow == vsCalender.Row)
				{
					vsCalender_DblClick(sender, new DataGridViewCellEventArgs(vsCalender.CurrentCell.ColumnIndex, vsCalender.CurrentCell.RowIndex));
				}
				else
				{
					timFirstClick = timClickTime;
					intClickedCol = vsCalender.Col;
					intClickedRow = vsCalender.Row;
				}
			}
			for (counter = 1; counter <= vsCalender.Rows - 1; counter++)
			{
				for (counter2 = 0; counter2 <= 6; counter2++)
				{
					if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, counter2)) == 0x808080)
					{
						vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, counter2, Color.White);
					}
				}
			}
			vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row, vsCalender.Col, 0x808080);
		}

		public void Init(ref DateTime datReturnDate, DateTime? datStartDateTemp = null)
		{
			//FC:FINAL:AM: call load first
			frmCalender_Load(this, EventArgs.Empty);
			DateTime datStartDate = datStartDateTemp ?? DateTime.Today;
			cboMonth.SelectedIndex = datStartDate.Month - 1;
			SetYearCombo_2(FCConvert.ToString(datStartDate.Year));
			SelectDay_2(datStartDate.Day);
			datReturnedDate = datStartDate;
			frmCalender.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
			datReturnDate = datReturnedDate;
		}
		// vbPorter upgrade warning: intDay As short	OnWriteFCConvert.ToInt32(
		private void SelectDay_2(int intDay)
		{
			SelectDay(ref intDay);
		}

		private void SelectDay(ref int intDay)
		{
			int counter;
			int counter2;
			for (counter = 1; counter <= vsCalender.Rows - 1; counter++)
			{
				for (counter2 = 0; counter2 <= 6; counter2++)
				{
					if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, counter2)) == 0x808080)
					{
						vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, counter2, Color.White);
					}
				}
			}
			for (counter = 1; counter <= vsCalender.Rows - 1; counter++)
			{
				for (counter2 = 0; counter2 <= 6; counter2++)
				{
					if (Conversion.Val(vsCalender.TextMatrix(counter, counter2)) == intDay)
					{
						vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, counter2, 0x808080);
						vsCalender.Select(counter, counter2);
					}
				}
			}
		}

		private void vsCalender_DblClick(object sender, DataGridViewCellEventArgs e)
		{
			mnuProcessSave_Click();
		}

		private void vsCalender_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Left)
			{
				if (vsCalender.Col > 0)
				{
					if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row, vsCalender.Col - 1)) == 0xE0E0E0)
					{
						KeyCode = 0;
						return;
					}
				}
			}
			else if (KeyCode == Keys.Right)
			{
				if (vsCalender.Col < vsCalender.Cols - 1)
				{
					if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row, vsCalender.Col + 1)) == 0xE0E0E0)
					{
						KeyCode = 0;
						return;
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (vsCalender.Row > 1)
				{
					if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row - 1, vsCalender.Col)) == 0xE0E0E0)
					{
						KeyCode = 0;
						return;
					}
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vsCalender.Row < vsCalender.Rows - 1)
				{
					if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row + 1, vsCalender.Col)) == 0xE0E0E0)
					{
						KeyCode = 0;
						return;
					}
				}
			}
		}

        private void vsCalender_RowColChange(object sender, System.EventArgs e)
        {
            FCUtils.StartTask(this, () =>
            {
                System.Threading.Thread.Sleep(200);
                int counter;
                int counter2;
                if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row, vsCalender.Col)) == 0xE0E0E0)
                {
                    return;
                }
                for (counter = 1; counter <= vsCalender.Rows - 1; counter++)
                {
                    for (counter2 = 0; counter2 <= 6; counter2++)
                    {
                        if (FCConvert.ToInt32(vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, counter2)) == 0x808080)
                        {
                            vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, counter2, Color.White);
                        }
                    }
                }
                vsCalender.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsCalender.Row, vsCalender.Col, 0x808080);
            });
        }
		// vbPorter upgrade warning: strSearch As string	OnWriteFCConvert.ToInt32(
		private void SetYearCombo_2(string strSearch)
		{
			SetYearCombo(ref strSearch);
		}

		private void SetYearCombo(ref string strSearch)
		{
			int counter;
			for (counter = 0; counter <= cboYear.Items.Count - 1; counter++)
			{
				if (strSearch == cboYear.Items[counter].ToString())
				{
					cboYear.SelectedIndex = counter;
					break;
				}
			}
		}

		private object SetCustomFormColors()
		{
			object SetCustomFormColors = null;
			//lblMonthYear.ForeColor = ColorTranslator.FromOle(0xC00000);
			return SetCustomFormColors;
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(mnuProcessSave, EventArgs.Empty);
		}
	}
}
