﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

#if TWCL0000
using TWCL0000;


#elif TWFA0000
using TWFA0000;


#elif TWBD0000
using TWBD0000;


#elif TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;


#elif TWBL0000
using TWBL0000;
using modGlobal = TWBL0000.modGlobalRoutines;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGNBas;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;


#elif TWMV0000
using modGlobal = TWMV0000.modGlobalRoutines;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalRoutines;
#endif
namespace Global
{
	public class modBudgetaryAccounting
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               03/04/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/23/2004              *
		// *
		// This module is to be used with modAccountTitle and     *
		// modValidateAccount                                     *
		// ********************************************************
		// Type to hold account information
		// AcctType Types
		// C - Cash Entry
		// A - Normal Account Entry
		// D - Due To/From Entry
		public struct FundType
		{
            // vbPorter upgrade warning: Account As string	OnWrite(string, int)

            public string _account;
            public string _description;
            public string _acctType;
            public string _overrideAP;
            public string _overrideCR;
            public string _overridePY;
            public string _cashFund;
            public string _project;
            public string _rCB;
            // this is the account number
            // vbPorter upgrade warning: Amount As Decimal	OnWrite(Decimal, double, short)
            public Decimal Amount;
			// amount to be passed in to the journal
			public string Description { get { return _description ?? ""; } set { _description = value; } }
			// description of the entry
			public string AcctType { get { return _acctType ?? ""; } set { _acctType = value; } }
			// type of account "FUND" is a fund, "A" is an account, "C" is a cash entry, "D" is a due to/due from entry
			public bool UseDueToFrom;
			public string OverrideAP { get { return _overrideAP ?? ""; } set { _overrideAP = value; } }
            public string OverrideCR { get { return _overrideCR ?? ""; } set { _overrideCR = value; } }
            public string OverridePY { get { return _overridePY ?? ""; } set { _overridePY = value; } }
            public string CashFund { get { return _cashFund ?? ""; } set { _cashFund = value; } }
            public string Project { get { return _project ?? ""; } set { _project = value; } }
            public string RCB { get { return _rCB ?? ""; } set { _rCB = value; } }

            public string Account { get { return _account ?? ""; } set { _account = value; } }

            public void Init()
			{
				_account = "";
				Description = "";
				AcctType = "";
				OverrideAP = "";
				OverrideCR = "";
				OverridePY = "";
				CashFund = "";
				Project = "";
				RCB = "";
			}
		};

		public struct AcctsForCheckRec
		{
			public int lngKey;
			public string strJournalType;
			public string strAccount;
			public int intBank;
			// vbPorter upgrade warning: curAmount As Decimal	OnWrite(short, Decimal)
			public Decimal curAmount;
			public string strCheck;
			public DateTime datCheckDate;
			public string strCheckRecType;
			public string strPayee;
			public int lngJournal;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public AcctsForCheckRec(int unusedParam)
			{
				this.lngKey = 0;
				this.strAccount = String.Empty;
				this.strJournalType = String.Empty;
				this.intBank = 0;
				this.curAmount = 0;
				this.strCheck = String.Empty;
				this.datCheckDate = DateTime.FromOADate(0);
				this.strCheckRecType = String.Empty;
				this.strPayee = String.Empty;
				this.lngJournal = 0;
			}
		};

		public enum AutoPostType
		{
			aptAccountsPayable = 1,
			aptAccountsPayableCorrections = 2,
			aptBudgetTransfer = 3,
			aptCashReceipts = 4,
			aptGeneralJournal = 5,
			aptEncumbrance = 6,
			aptCashDisbursement = 7,
			aptReversingJournal = 8,
			aptCreateOpeningAdjustments = 9,
			aptCreditMemo = 10,
			aptARBills = 11,
			aptCRDailyAudit = 12,
			aptFADepreciation = 13,
			aptMVRapidRenewal = 14,
			aptPYProcesses = 15,
			aptCLLiens = 16,
			aptCLTaxAcquired = 17,
			aptBLCommitment = 18,
			aptBLSupplemental = 19,
			aptUTBills = 20,
			aptUTLiens = 21,
		}

		public static FundType[] CalcDueToFromFundCash(ref FundType[] Funds, ref string strJournalType)
		{
			FundType[] CalcDueToFromFundCash = null;
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			FundType[] ftFunds = new FundType[999 + 1];
			// this will be the array that is returned
			// this function will accept an array of FundTypes with amounts to calculate the amount
			// of each fund's cash entries grouped by the Due To From fund.  This will be used to check for out of balance by fund journals
			// PRE:   Funds will be an array of Fund totals...
			// Each Account is associated with a single fund and these accounts/amounts
			// will be sorted out into Funds
			// POST:  The array that is passed out will only have the Fund totals in it
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsFund = new clsDRWrapper();
				// vbPorter upgrade warning: lngCT As int	OnRead(string)
				int lngCT;
				// total number of accounts in the array
				int lngUB;
				// this is the upper bound of the array passed in
				int lngFund;
				lngUB = Information.UBound(Funds, 1);
				// set all of the fund numbers
				rsFundInfo.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "' ORDER BY Fund", "TWBD0000.vb1");
				for (lngCT = 1; lngCT <= 999; lngCT++)
				{
					ftFunds[lngCT].Account = FCConvert.ToString(lngCT);
					ftFunds[lngCT].Amount = 0;
					ftFunds[lngCT].Description = "";
					ftFunds[lngCT].AcctType = "FUND";
				}
				rsFund.OpenRecordset("SELECT * FROM DeptDivTitles", "TWBD0000.vb1");
				if ((rsFund.EndOfFile() != true && rsFund.BeginningOfFile() != true))
				{
					// check for Fund Information
					for (lngCT = 1; lngCT <= lngUB; lngCT++)
					{
						// check all of the accounts
						//Application.DoEvents();
						// check to see if there is an invalid account number
						if (Conversion.Val(Funds[lngCT].Account) != Conversion.Val(Funds[lngCT].CashFund) && Funds[lngCT].UseDueToFrom && (!(strJournalType == "AP") || Funds[lngCT].OverrideAP == ""))
						{
							ftFunds[FCConvert.ToInt32(Funds[lngCT].CashFund)].Amount = ftFunds[FCConvert.ToInt32(Funds[lngCT].CashFund)].Amount + Funds[lngCT].Amount;
						}
						else
						{
							if (strJournalType == "AP" && Funds[lngCT].OverrideAP != "")
							{
								ftFunds[FCConvert.ToInt32(Funds[lngCT].OverrideAP)].Amount += Funds[lngCT].Amount;
							}
							else
							{
								ftFunds[FCConvert.ToInt32(Funds[lngCT].Account)].Amount += Funds[lngCT].Amount;
							}
						}
					}
					CalcDueToFromFundCash = ftFunds;
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("An error has occured finding your Budgetary Account Information.", "Fund Information Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CalcDueToFromFundCash;
				}
				return CalcDueToFromFundCash;
			}
			catch (Exception ex)
			{
                if (Information.Err(ex).Number != 9)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Calculate Due To / From Fund Cash Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					// this is the subscript out of range from the UBound on an empty array
				}
				CalcDueToFromFundCash = ftFunds;
			}
			return CalcDueToFromFundCash;
		}

		public static FundType[] CalcFundCash(ref FundType[] Accounts)
		{
			FundType[] CalcFundCash = null;
			FundType[] ftFunds = new FundType[999 + 1];
			// this will be the array that is returned
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			string strTempDiv = "";
			// this function will accept an array of FundTypes with amounts to calculate the amount
			// of each fund's cash entries
			// PRE:   Accounts will be an array of Account totals...
			// Each Account is associated with a single fund and these accounts/amounts
			// will be sorted out into Funds
			// POST:  The array that is passed out will only have the Fund totals in it
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsFund = new clsDRWrapper();
				// vbPorter upgrade warning: lngCT As int	OnRead(string)
				int lngCT;
				// total number of accounts in the array
				int lngUB;
				// this is the upper bound of the array passed in
				int lngFund = 0;
				clsDRWrapper rsUpdate = new clsDRWrapper();
				// Dave 4/20/2004
				// update budgetary database with new Due To/From options
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LedgerTitles' AND COLUMN_NAME = 'CashFund'", "Budgetary");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD CashFund nvarchar(20) NULL", "Budgetary");
				}
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LedgerTitles' AND COLUMN_NAME = 'PYOverrideCashFund'", "Budgetary");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD PYOverrideCashFund nvarchar(20) NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD CROverrideCashFund nvarchar(20) NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD APOverrideCashFund nvarchar(20) NULL", "Budgetary");
					InitializeFundDueToFromInfo();
				}
				lngUB = Information.UBound(Accounts, 1);
				// set all of the fund numbers
				rsFundInfo.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "' ORDER BY Fund", "TWBD0000.vb1");
				for (lngCT = 1; lngCT <= 999; lngCT++)
				{
					//FC:FINAL:DSE Initialize string fields with empty value
					ftFunds[lngCT].Init();
					ftFunds[lngCT].Account = FCConvert.ToString(lngCT);
					ftFunds[lngCT].Amount = 0;
					ftFunds[lngCT].Description = "";
					ftFunds[lngCT].AcctType = "FUND";
					if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
					{
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						if (Conversion.Val(rsFundInfo.Get_Fields("Fund")) == lngCT)
						{
							ftFunds[lngCT].UseDueToFrom = FCConvert.ToBoolean(rsFundInfo.Get_Fields_Boolean("UseDueToFrom"));
							ftFunds[lngCT].CashFund = FCConvert.ToString(rsFundInfo.Get_Fields_String("CashFund"));
							ftFunds[lngCT].OverrideAP = FCConvert.ToString(rsFundInfo.Get_Fields_String("APOverrideCashFund"));
							ftFunds[lngCT].OverrideCR = FCConvert.ToString(rsFundInfo.Get_Fields_String("CROverrideCashFund"));
							ftFunds[lngCT].OverridePY = FCConvert.ToString(rsFundInfo.Get_Fields_String("PYOverrideCashFund"));
							rsFundInfo.MoveNext();
						}
						else
						{
							ftFunds[lngCT].UseDueToFrom = false;
							ftFunds[lngCT].CashFund = "";
							ftFunds[lngCT].OverrideAP = "";
							ftFunds[lngCT].OverrideCR = "";
							ftFunds[lngCT].OverridePY = "";
						}
					}
					else
					{
						ftFunds[lngCT].UseDueToFrom = false;
						ftFunds[lngCT].CashFund = "";
						ftFunds[lngCT].OverrideAP = "";
						ftFunds[lngCT].OverrideCR = "";
						ftFunds[lngCT].OverridePY = "";
					}
				}
				rsFund.OpenRecordset("SELECT * FROM DeptDivTitles", "TWBD0000.vb1");
				if ((rsFund.EndOfFile() != true && rsFund.BeginningOfFile() != true) )
				{
					// check for Fund Information
					for (lngCT = 0; lngCT <= lngUB; lngCT++)
					{
                        // check all of the accounts
                        // check to see if there is an invalid account number
                        //Application.DoEvents();
                        //FC:FINAL:SBE - #i2247 check for null
                        //if (Strings.InStr(1, Accounts[lngCT].Account, "_", CompareConstants.vbBinaryCompare) == 0 && Accounts[lngCT].Account.Length > 2 && Strings.Left(Accounts[lngCT].Account, 1) != "M")
                        if (Strings.InStr(1, Accounts[lngCT].Account, "_", CompareConstants.vbBinaryCompare) == 0 && Accounts[lngCT].Account != null && Accounts[lngCT].Account.Length > 2 && Strings.Left(Accounts[lngCT].Account, 1) != "M")
						{
							// check to see if this is a GL Account
							if (Strings.Left(Accounts[lngCT].Account, 1) == "G")
							{
								// extract the fund number and add the amount
								lngFund = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Accounts[lngCT].Account, 3, FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2))))))));
								if (lngFund > 0)
								{
									ftFunds[lngFund].Amount += Accounts[lngCT].Amount;
								}
								else
								{
									frmWait.InstancePtr.Unload();
									MessageBox.Show("Cannot find the fund for account " + Accounts[lngCT].Account + " with an amount of " + Strings.Format(Accounts[lngCT].Amount, "$#,##0.00") + ".", "Fund Cash Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								}
							}
							else
							{
								// If Not blnSchool And (Left$(Accounts(lngCT).Account, 1) <> "P" And Left$(Accounts(lngCT).Account, 1) <> "V" And Left$(Accounts(lngCT).Account, 1) <> "L") Then
								if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) == 0)
								{
									strTempDiv = "0";
								}
								else
								{
									strTempDiv = Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), "0");
								}
								// this will find out which fund this account number uses by checking the BD database
								if (Strings.Left(Accounts[lngCT].Account, 1) == "R")
								{
									// revenue
									rsFund.FindFirstRecord2("Division, Department", strTempDiv + "," + Strings.Mid(Accounts[lngCT].Account, 3, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 1, 2)))), ",");
								}
								else
								{
									// expense
									rsFund.FindFirstRecord2("Division, Department", strTempDiv + "," + Strings.Mid(Accounts[lngCT].Account, 3, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 1, 2)))), ",");
								}
								if (rsFund.NoMatch != true)
								{
									// fill the dollar amounts in
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									ftFunds[FCConvert.ToInt32(rsFund.Get_Fields("Fund"))].Amount += Accounts[lngCT].Amount;
								}
							}
						}
						else
						{
							// invalid account format
						}
					}
					CalcFundCash = ftFunds;
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("An error has occured finding your Budgetary Account Information.", "Fund Information Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CalcFundCash;
				}
				return CalcFundCash;
			}
			catch (Exception ex)
			{
				
				if (Information.Err(ex).Number != 9)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Calculate Fund Cash Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					// this is the subscript out of range from the UBound on an empty array
				}
				CalcFundCash = ftFunds;
			}
			return CalcFundCash;
		}

		public static void InitializeFundDueToFromInfo()
		{
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsDueToFromAccountInfo = new clsDRWrapper();
			string strCashFund = "";
			if (FCConvert.ToString(GetBDVariable("Due")) == "Y")
			{
				rsDueToFromAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CF'", "TWBD0000.vb1");
				if (rsDueToFromAccountInfo.EndOfFile() != true && rsDueToFromAccountInfo.BeginningOfFile() != true)
				{
					if (!rsDueToFromAccountInfo.IsFieldNull("Account"))
					{
						strCashFund = "";
					}
					else
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						strCashFund = FCConvert.ToString(rsDueToFromAccountInfo.Get_Fields("Account"));
					}
				}
				else
				{
					strCashFund = "";
				}
				rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE UseDueToFrom = 1", "TWBD0000.vb1");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					do
					{
						rs.Edit();
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rs.Get_Fields("Fund")) == strCashFund)
						{
							rs.Set_Fields("UseDueToFrom", false);
						}
						else
						{
							if (FCConvert.ToBoolean(GetBDVariable("ExcludeAPDueToFrom")) == true)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rs.Set_Fields("APOverrideCashFund", rs.Get_Fields("Fund"));
							}
							else
							{
								rs.Set_Fields("APOverrideCashFund", "");
							}
							if (FCConvert.ToBoolean(GetBDVariable("ExcludeCRDueToFrom")) == true)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rs.Set_Fields("CROverrideCashFund", rs.Get_Fields("Fund"));
							}
							else
							{
								rs.Set_Fields("CROverrideCashFund", "");
							}
							if (FCConvert.ToBoolean(GetBDVariable("ExcludePYDueToFrom")) == true)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rs.Set_Fields("PYOverrideCashFund", rs.Get_Fields("Fund"));
							}
							else
							{
								rs.Set_Fields("PYOverrideCashFund", "");
							}
							rs.Set_Fields("CashFund", strCashFund);
						}
						rs.Update();
						rs.MoveNext();
					}
					while (rs.EndOfFile() != true);
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public static object GetBDVariable(string strName)
		{
			object GetBDVariable = null;
			// this function will get any variable from the budgetary table in the GN database
			clsDRWrapper rsVariables = new clsDRWrapper();
			// Dim ff As New FCFileSystem
			try
			{
				// On Error GoTo ErrorHandler
				rsVariables.OpenRecordset("SELECT * FROM Budgetary", "Budgetary");
				if (rsVariables.EndOfFile())
				{
					GetBDVariable = string.Empty;
				}
				else
				{
					GetBDVariable = rsVariables.Get_Fields(strName);
				}
				if (fecherFoundation.FCUtils.IsNull(GetBDVariable))
					GetBDVariable = "";
				return GetBDVariable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Get BD Variables Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetBDVariable;
		}

		public static bool UpdateBDVariable(string strVariableName, object NewValue)
		{
			bool UpdateBDVariable = false;
			clsDRWrapper rsVariables = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				UpdateBDVariable = false;
				rsVariables.OpenRecordset("SELECT * FROM Budgetary", "TWBD0000.vb1");
				if (rsVariables.EndOfFile())
				{
					rsVariables.AddNew();
				}
				else
				{
					rsVariables.Edit();
				}
				rsVariables.Set_Fields(strVariableName, NewValue);
				rsVariables.Update();
				UpdateBDVariable = true;
				return UpdateBDVariable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + ex.GetBaseException().Message, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateBDVariable;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public static object GetSystemSettingsVariable(string strName)
		{
			object GetSystemSettingsVariable = null;
			// this function will get any variable from the budgetary table in the GN database
			clsDRWrapper rsVariables = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsVariables.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (rsVariables.EndOfFile())
				{
					GetSystemSettingsVariable = string.Empty;
				}
				else
				{
					GetSystemSettingsVariable = rsVariables.Get_Fields(strName);
				}
				if (fecherFoundation.FCUtils.IsNull(GetSystemSettingsVariable))
					GetSystemSettingsVariable = "";
				return GetSystemSettingsVariable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Get System Settings Variables Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetSystemSettingsVariable;
		}

		public static bool UpdateSystemSettingsVariable(string strVariableName, object NewValue)
		{
			bool UpdateSystemSettingsVariable = false;
			clsDRWrapper rsVariables = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				UpdateSystemSettingsVariable = false;
				rsVariables.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (rsVariables.EndOfFile())
				{
					rsVariables.AddNew();
				}
				else
				{
					rsVariables.Edit();
				}
				rsVariables.Set_Fields(strVariableName, NewValue);
				rsVariables.Update();
				UpdateSystemSettingsVariable = true;
				return UpdateSystemSettingsVariable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + ex.GetBaseException().Message, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateSystemSettingsVariable;
		}
		// vbPorter upgrade warning: boolOverride As object	OnWrite(bool)
		public static bool CalcDueToFrom_342(ref FundType[] Accounts, FundType[] Funds, bool bolNoAccounts = false, string CashDescription = "", string strCashAccountType = "AP", string strAltCashAccount = "", bool blnSchool = false, int lngCashIndex = 0, bool blnPayroll = false)
		{
			return CalcDueToFrom(ref Accounts, ref Funds, bolNoAccounts, CashDescription, false, false, strCashAccountType, strAltCashAccount, blnSchool, lngCashIndex, blnPayroll);
		}

		public static bool CalcDueToFrom(ref FundType[] Accounts, ref FundType[] Funds, bool bolNoAccounts = false, string CashDescription = "", bool boolOverride = false, bool boolWinCR = false, string strCashAccountType = "AP", string strAltCashAccount = "", bool blnSchool = false, int lngCashIndex = 0, bool blnPayroll = false)
		{
			bool CalcDueToFrom = false;
			// PRE:    'Accounts() is an array of 99 funds with amounts and fund numbers in account field and
			// Funds() is a dynamic array of any accounts you want posted to the budgetary system
			// SchoolFunds() is a dynamic array of any school accounts you want posted to the budgetary system
			// POST:   'Accounts will have all of the original accounts and their totals with Fund cash entries,
			// Due To and Due From entries
			string strCashFund = "";
			string strCashAccount = "";
			string strDueToAccount = "";
			string strDueFromAccount = "";
			// Dim strSchoolCashAccount As String
			// Dim strSchoolDueToAccount As String
			// Dim strSchoolDueFromAccount As String
			clsDRWrapper rsTemp = new clsDRWrapper();
			Decimal curCashTotal;
			int counter;
			bool blnUseDueToFrom = false;
			// vbPorter upgrade warning: FundCashTotals As Decimal	OnWrite(short, Decimal)
			Decimal[] FundCashTotals = new Decimal[999 + 1];
			clsDRWrapper rsUpdate = new clsDRWrapper();
			CalcDueToFrom = true;
			try
			{
				// On Error GoTo ErrorTag
				if (blnSchool)
					return CalcDueToFrom;
				if (FCConvert.ToBoolean(boolOverride))
					return CalcDueToFrom;
				for (counter = 1; counter <= 999; counter++)
				{
					FundCashTotals[counter] = 0;
				}
				// Dave 4/20/2004
				// update budgetary database with new Due To/From options
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LedgerTitles' AND COLUMN_NAME = 'CashFund'", "Budgetary");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD CashFund nvarchar(20) NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD PYOverrideCashFund nvarchar(20) NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD CROverrideCashFund nvarchar(20) NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD APOverrideCashFund nvarchar(20) NULL", "Budgetary");
					InitializeFundDueToFromInfo();
				}
				if (FCConvert.ToString(GetBDVariable("Due")) == "Y")
				{
					blnUseDueToFrom = true;
				}
				else
				{
					blnUseDueToFrom = false;
				}
				// Get the Cash Account - this selection will allow the different modules to use
				// the three different cash accounts...Misc, Payroll and AP Cash
				// If gboolTownAccounts Then
				if ((strCashAccountType == "AP") || (strCashAccountType == "GJ"))
				{
					rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CA'", "TWBD0000.vb1");
				}
				else if (strCashAccountType == "CR")
				{
					rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
				}
				else if (strCashAccountType == "PY")
				{
					rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CP'", "TWBD0000.vb1");
				}
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					strCashAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You must set up a Cash Account before you may continue.", "No Accounts Payable Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
					CalcDueToFrom = false;
					return CalcDueToFrom;
				}
				// End If
				// If they use Due To\Due From accounts get the information
				if (blnUseDueToFrom)
				{
				
					rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'DT'", "TWBD0000.vb1");
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						if (rsTemp.IsFieldNull("Account"))
						{
							frmWait.InstancePtr.Unload();
							MessageBox.Show("You must set up a Due To Account before you may continue.", "No Due To Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							CalcDueToFrom = false;
							return CalcDueToFrom;
						}
						else
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strDueToAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
						}
					}
					else
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("You must set up a Due To Account before you may continue.", "No Due To Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						CalcDueToFrom = false;
						return CalcDueToFrom;
					}
					// get the due from account
					rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'DF'", "TWBD0000.vb1");
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Account"))) == "")
						{
							frmWait.InstancePtr.Unload();
							MessageBox.Show("You must set up a Due From Account before you may continue.", "No Due From Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							CalcDueToFrom = false;
							return CalcDueToFrom;
						}
						else
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strDueFromAccount = FCConvert.ToString(rsTemp.Get_Fields("Account"));
						}
					}
					else
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("You must set up a Due From Account before you may continue.", "No Due From Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						CalcDueToFrom = false;
						return CalcDueToFrom;
					}
				
				}
				else
				{
					// do nothing
				}
				if (blnUseDueToFrom)
				{
					for (counter = 1; counter <= 999; counter++)
					{
						//Application.DoEvents();
						if (Funds[counter].Amount != 0)
						{
							if (Conversion.Val(Funds[counter].Account) != Conversion.Val(Funds[counter].CashFund) && Funds[counter].UseDueToFrom && (!blnPayroll || Funds[counter].OverridePY == "") && (!boolWinCR || Funds[counter].OverrideCR == ""))
							{
								// add money to cash total
								// 10/07/2003 - Jim B - This was not accumulating...added "curCashTotal +"
								FundCashTotals[FCConvert.ToInt32(Funds[counter].CashFund)] = FundCashTotals[FCConvert.ToInt32(Funds[counter].CashFund)] + Funds[counter].Amount;
								// create due to entry
								if (bolNoAccounts)
								{
									bolNoAccounts = false;
									if (Funds[counter].Amount < 0 || boolWinCR)
									{
										Accounts[0].Description = "Due To\\From";
										Accounts[0].Account = "G " + Funds[counter].CashFund + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, 2);
										Accounts[0].AcctType = "D";
										Accounts[0].Amount = Funds[counter].Amount;
										// * -1
									}
									else
									{
										Accounts[0].Description = "Due To\\From";
										Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].CashFund, 2);
										Accounts[0].AcctType = "D";
										Accounts[0].Amount = Funds[counter].Amount * -1;
									}
								}
								else
								{
									Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
									if (Funds[counter].Amount < 0 || boolWinCR)
									{
										Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
										Accounts[Information.UBound(Accounts)].Account = "G " + Funds[counter].CashFund + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, 2);
										Accounts[Information.UBound(Accounts)].AcctType = "D";
										Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount;
										// * -1
									}
									else
									{
										Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
										Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].CashFund, 2);
										Accounts[Information.UBound(Accounts)].AcctType = "D";
										Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount * -1;
									}
								}
								// create due from entry
								Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
								if (Funds[counter].Amount < 0 || boolWinCR)
								{
									Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
									Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueFromAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].CashFund, 2);
									Accounts[Information.UBound(Accounts)].AcctType = "D";
									Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount * -1;
								}
								else
								{
									Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
									Accounts[Information.UBound(Accounts)].Account = "G " + Funds[counter].CashFund + "-" + strDueFromAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2))));
									Accounts[Information.UBound(Accounts)].AcctType = "D";
									Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount;
								}
							}
							else if (Conversion.Val(Funds[counter].Account) != Conversion.Val(Funds[counter].OverridePY) && Funds[counter].UseDueToFrom && blnPayroll && Funds[counter].OverridePY != "")
							{
								// add money to cash total
								// 10/07/2003 - Jim B - This was not accumulating...added "curCashTotal +"
								FundCashTotals[FCConvert.ToInt32(Funds[counter].OverridePY)] = FundCashTotals[FCConvert.ToInt32(Funds[counter].OverridePY)] + Funds[counter].Amount;
								// create due to entry
								if (bolNoAccounts)
								{
									bolNoAccounts = false;
									if (Funds[counter].Amount < 0 || boolWinCR)
									{
										Accounts[0].Description = "Due To\\From";
										Accounts[0].Account = "G " + Funds[counter].OverridePY + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, 2);
										Accounts[0].AcctType = "D";
										Accounts[0].Amount = Funds[counter].Amount;
									}
									else
									{
										Accounts[0].Description = "Due To\\From";
										Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].OverridePY, 2);
										Accounts[0].AcctType = "D";
										Accounts[0].Amount = Funds[counter].Amount * -1;
									}
								}
								else
								{
									Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
									if (Funds[counter].Amount < 0 || boolWinCR)
									{
										Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
										Accounts[Information.UBound(Accounts)].Account = "G " + Funds[counter].OverridePY + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, 2);
										Accounts[Information.UBound(Accounts)].AcctType = "D";
										Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount;
									}
									else
									{
										Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
										Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].OverridePY, 2);
										Accounts[Information.UBound(Accounts)].AcctType = "D";
										Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount * -1;
									}
								}
								// create due from entry
								Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
								if (Funds[counter].Amount < 0 || boolWinCR)
								{
									Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
									Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueFromAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].OverridePY, 2);
									Accounts[Information.UBound(Accounts)].AcctType = "D";
									Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount * -1;
								}
								else
								{
									Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
									Accounts[Information.UBound(Accounts)].Account = "G " + Funds[counter].OverridePY + "-" + strDueFromAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2))));
									Accounts[Information.UBound(Accounts)].AcctType = "D";
									Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount;
								}
							}
							else if (Conversion.Val(Funds[counter].Account) != Conversion.Val(Funds[counter].OverrideCR) && Funds[counter].UseDueToFrom && boolWinCR && Funds[counter].OverrideCR != "")
							{
								// add money to cash total
								// 10/07/2003 - Jim B - This was not accumulating...added "curCashTotal +"
								FundCashTotals[FCConvert.ToInt32(Funds[counter].OverrideCR)] = FundCashTotals[FCConvert.ToInt32(Funds[counter].OverrideCR)] + Funds[counter].Amount;
								// create due to entry
								if (bolNoAccounts)
								{
									bolNoAccounts = false;
									if (Funds[counter].Amount < 0 || boolWinCR)
									{
										Accounts[0].Description = "Due To\\From";
										Accounts[0].Account = "G " + Funds[counter].OverrideCR + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, 2);
										Accounts[0].AcctType = "D";
										Accounts[0].Amount = Funds[counter].Amount;
									}
									else
									{
										Accounts[0].Description = "Due To\\From";
										Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].OverrideCR, 2);
										Accounts[0].AcctType = "D";
										Accounts[0].Amount = Funds[counter].Amount * -1;
									}
								}
								else
								{
									Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
									if (Funds[counter].Amount < 0 || boolWinCR)
									{
										Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
										Accounts[Information.UBound(Accounts)].Account = "G " + Funds[counter].OverrideCR + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, 2);
										Accounts[Information.UBound(Accounts)].AcctType = "D";
										Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount;
									}
									else
									{
										Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
										Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueToAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].OverrideCR, 2);
										Accounts[Information.UBound(Accounts)].AcctType = "D";
										Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount * -1;
									}
								}
								// create due from entry
								Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
								if (Funds[counter].Amount < 0 || boolWinCR)
								{
									Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
									Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strDueFromAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].OverrideCR, 2);
									Accounts[Information.UBound(Accounts)].AcctType = "D";
									Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount * -1;
								}
								else
								{
									Accounts[Information.UBound(Accounts)].Description = "Due To\\From";
									Accounts[Information.UBound(Accounts)].Account = "G " + Funds[counter].OverrideCR + "-" + strDueFromAccount + "-" + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2))));
									Accounts[Information.UBound(Accounts)].AcctType = "D";
									Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount;
								}
							}
							else
							{
								if (Funds[counter].UseDueToFrom)
								{
									if (blnPayroll && Funds[counter].OverridePY != "")
									{
										FundCashTotals[FCConvert.ToInt32(Funds[counter].OverridePY)] = FundCashTotals[FCConvert.ToInt32(Funds[counter].OverridePY)] + Funds[counter].Amount;
									}
									else if (boolWinCR && Funds[counter].OverrideCR != "")
									{
										FundCashTotals[FCConvert.ToInt32(Funds[counter].OverrideCR)] = FundCashTotals[FCConvert.ToInt32(Funds[counter].OverrideCR)] + Funds[counter].Amount;
									}
									else
									{
										// add money to cash total
										FundCashTotals[FCConvert.ToInt32(Funds[counter].CashFund)] = FundCashTotals[FCConvert.ToInt32(Funds[counter].CashFund)] + Funds[counter].Amount;
									}
								}
								else
								{
									FundCashTotals[FCConvert.ToInt32(Funds[counter].Account)] = FundCashTotals[FCConvert.ToInt32(Funds[counter].Account)] + Funds[counter].Amount;
								}
							}
						}
					}
					// create cash entry
					if (strCashAccountType != "GJ")
					{
						for (counter = 1; counter <= 999; counter++)
						{
							//Application.DoEvents();
							if (FundCashTotals[counter] != 0)
							{
								if (bolNoAccounts)
								{
									bolNoAccounts = false;
									Accounts[0].Description = CashDescription + " - Cash";
									if (modAccountTitle.Statics.YearFlag)
									{
										Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strCashAccount;
									}
									else
									{
										Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strCashAccount + "-00";
									}
									Accounts[0].AcctType = "C";
									Accounts[0].Amount = FundCashTotals[counter] * -1;
								}
								else
								{
									if (FundCashTotals[counter] != 0)
									{
										Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
										Accounts[Information.UBound(Accounts)].Description = CashDescription + " - Cash";
										if (modAccountTitle.Statics.YearFlag)
										{
											Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strCashAccount;
										}
										else
										{
											Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strCashAccount + "-00";
										}
										Accounts[Information.UBound(Accounts)].Amount = FundCashTotals[counter] * -1;
										Accounts[Information.UBound(Accounts)].AcctType = "C";
									}
								}
								lngCashIndex = Information.UBound(Accounts, 1);
							}
						}
					}
				}
				else
				{
					// if Due To/From is not used then make a cash entry for each fund that has a balance
					if (strCashAccountType != "GJ")
					{
						lngCashIndex = Information.UBound(Accounts, 1) + 1;
						for (counter = 1; counter <= 999; counter++)
						{
							//Application.DoEvents();
							if (Funds[counter].Amount != 0)
							{
								if (bolNoAccounts)
								{
									bolNoAccounts = false;
									Accounts[0].Description = CashDescription + " - Cash";
									// If Not blnSchool Then
									if (modAccountTitle.Statics.YearFlag)
									{
										Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strCashAccount;
									}
									else
									{
										Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strCashAccount + "-00";
									}
									// Else
									// Accounts(0).Account = "L " & GetFormat(Funds(counter).Account, Val(Left(SchoolLedger, 2))) & "-" & strSchoolCashAccount & "-00"
									// End If
									Accounts[0].AcctType = "C";
									Accounts[0].Amount = Funds[counter].Amount * -1;
								}
								else
								{
									Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
									Accounts[Information.UBound(Accounts)].Description = CashDescription + " - Cash";
									// If Not blnSchool Then
									if (modAccountTitle.Statics.YearFlag)
									{
										Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strCashAccount;
									}
									else
									{
										Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strCashAccount + "-00";
									}
									// Else
									// Accounts(UBound(Accounts)).Account = "L " & GetFormat(Funds(counter).Account, Val(Left(SchoolLedger, 2))) & "-" & strSchoolCashAccount & "-00"
									// End If
									Accounts[Information.UBound(Accounts)].AcctType = "C";
									Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount * -1;
								}
							}
						}
					}
				}
				return CalcDueToFrom;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Calculate DueTo DoFrom Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				CalcDueToFrom = false;
			}
			return CalcDueToFrom;
		}
		// vbPorter upgrade warning: lngPeriod As int	OnWrite(string, int)
		public static int AddToJournal_621(FundType[] Accounts, string strJournalType, int lngJournalNumber, DateTime dtDate, int lngPeriod = 0, bool boolOverride = false, int lngCloseoutID = -1, string strDBPath = "")
		{
			return AddToJournal(ref Accounts, strJournalType, lngJournalNumber, dtDate, "", lngPeriod, boolOverride, lngCloseoutID, strDBPath);
		}

		public static int AddToJournal(ref FundType[] Accounts, string strJournalType, int lngJournalNumber, DateTime dtDate, string strJournalDescription = "", int lngPeriod = 0, bool boolOverride = false, int lngCloseoutID = -1, string strDBPath = "")
		{
			int AddToJournal = 0;
			int counter;
			string strModule = "";
			bool boolSetrsTemp;
			int lngError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// PRE:    Accounts() is the array that was created in CalcDueToFrom and it will
				// create all of the entries needed in the BD system.  Also a Journal #, Journal Type and Date
				// of the Journal must be passed in
				// POST:   The correct BD table will be updated with the new entries and the Journal will be ready for posting
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper Master = new clsDRWrapper();
				int TempJournal = 0;
				boolSetrsTemp = true;
				lngError = 1;
				if (strDBPath != string.Empty)
				{
					rsTemp.GroupName = strDBPath;
					Master.GroupName = strDBPath;
				}
				if ((strJournalType == "CR") || (strJournalType == "CD"))
				{
					strModule = "BD";
				}
				else if (strJournalType == "CW")
				{
					strModule = "CR";
				}
				else if (strJournalType == "PY")
				{
					strModule = "PY";
				}
				else if (strJournalType == "FA")
				{
					strJournalType = "GJ";
					strModule = "FA";
				}
				lngError = 2;
				if (boolOverride)
				{
					return AddToJournal;
				}
				if (lngJournalNumber != 0)
				{
					lngError = 3;
					rsTemp.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Status = 'E' AND Type = '" + strJournalType + "'", "TWBD0000.vb1");
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						TempJournal = lngJournalNumber;
					}
					else
					{
						AddToJournal = -1;
						return AddToJournal;
					}
				}
				else
				{
					lngError = 4;
					TryLockAgain:
					;
					// attempt to lock the journal table to get a number
					if (LockJournal(strDBPath) == false)
					{
						lngError = 5;
						// if unable to lock show a message and leave the function
						frmWait.InstancePtr.Unload();
						switch (MessageBox.Show("User " + Statics.strLockedBy + " is currently locking the journal table.  You must wait until they are finished before you save.  Would you like to retry?", "Unable to Save", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
						{
							case DialogResult.Yes:
								{
									goto TryLockAgain;
									break;
								}
							default:
								{
									// give the user an option of saving the records in a BD table or resetting the locks
									switch (MessageBox.Show("If you are sure that no one is currently using the TRIO Budgetary Accounting System then you can reset the Budgetary lock controls.  Would you like to unlock the controls now?" + "\r\n" + "Select No to retry.", "Unlock Controls", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
									{
										case DialogResult.Yes:
											{
												// CYA Entry
												modGlobalFunctions.AddCYAEntry_6(strModule, "Unlocked BD Controls for Journal Entry.");
												// unlock controls
												UnlockJournal(strDBPath);
												goto TryLockAgain;
												break;
											}
										case DialogResult.No:
											{
												switch (MessageBox.Show("If you are sure that no one is currently using the TRIO Budgetary Accounting System then you can save this information in a temporary table.  Would you like to save the Budgetary entries now?" + "\r\n" + "Select No to retry.", "Unlock Controls", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
												{
													case DialogResult.Yes:
														{
															// save the entries into a fake journal
															modGlobalFunctions.AddCYAEntry_6(strModule, "Saved BD entries.");
															boolSetrsTemp = false;
															// TempJournalMaster is the temporary table in BD
															rsTemp.OpenRecordset("SELECT * FROM TempJournalEntries ORDER BY JournalNumber Desc", "TWBD0000.vb1");
															if (rsTemp.EndOfFile() != true)
															{
																// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
																TempJournal = FCConvert.ToInt32(rsTemp.Get_Fields("JournalNumber")) + 1;
															}
															else
															{
																TempJournal = 1;
															}
															break;
														}
													case DialogResult.No:
														{
															goto TryLockAgain;
															break;
														}
													default:
														{
															goto TryLockAgain;
															break;
														}
												}
												//end switch
												break;
											}
										default:
											{
												goto TryLockAgain;
												break;
											}
									}
									//end switch
									break;
								}
						}
						//end switch
					}
					else
					{
						// everything is fine
						// get the next available journal
						lngError = 6;
						Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC", "TWBD0000.vb1");
						if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
						{
							Master.MoveLast();
							Master.MoveFirst();
							// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
						}
						else
						{
							TempJournal = 1;
						}
						lngError = 7;
						// add a new journal to the JournalMaster table
						Master.AddNew();
						lngError = 71;
						Master.Set_Fields("JournalNumber", TempJournal);
						lngError = 72;
						Master.Set_Fields("Status", "E");
						lngError = 73;
						Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						lngError = 74;
						Master.Set_Fields("StatusChangeDate", DateTime.Today);
						lngError = 75;
						Master.Set_Fields("Description", Strings.Left(strJournalDescription, 25));
						lngError = 76;
						Master.Set_Fields("Type", strJournalType);
						if (lngCloseoutID > 0)
						{
							Master.Set_Fields("CRPeriodCloseoutKey", lngCloseoutID);
						}
						lngError = 77;
						if (lngPeriod > 0 && lngPeriod <= 12)
						{
							lngError = 78;
							Master.Set_Fields("Period", lngPeriod);
						}
						else
						{
							lngError = 79;
							Master.Set_Fields("Period", dtDate.Month);
						}
						lngError = 8;
						Master.Update();
						Master.Reset();
						lngError = 9;
						// unlock the journal table so other people can get journal numbers
						UnlockJournal(strDBPath);
					}
				}
				lngError = 10;
				if (boolSetrsTemp)
				{
					// reset this unless it is already set pointing at the TempJournalEntries table
					rsTemp.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0", "TWBD0000.vb1");
				}
				rsTemp.OmitNullsOnInsert = true;
				lngError = 11;
				for (counter = 0; counter <= Information.UBound(Accounts, 1); counter++)
				{
					if (Strings.Left(Accounts[counter].Account, 1) != "M")
					{
						// check for manual entried that should not be sent to BD
						if (Accounts[counter].Amount != 0)
						{
							// added 01/05/2004 - adversely effects BD post journal routine
							rsTemp.OmitNullsOnInsert = true;
							rsTemp.AddNew();
							if (strJournalType == "CW")
							{
								rsTemp.Set_Fields("Type", "W");
							}
							else if (strJournalType == "PY")
							{
								rsTemp.Set_Fields("Type", "P");
							}
							else if (strJournalType == "CR")
							{
								rsTemp.Set_Fields("Type", "C");
							}
							else if (strJournalType == "CD")
							{
								rsTemp.Set_Fields("Type", "D");
							}
							else if (strJournalType == "GJ")
							{
								rsTemp.Set_Fields("Type", "G");
							}
							rsTemp.Set_Fields("JournalEntriesDate", dtDate);
							rsTemp.Set_Fields("Description", Accounts[counter].Description);
							rsTemp.Set_Fields("VendorNumber", 0);
							rsTemp.Set_Fields("JournalNumber", TempJournal);
							rsTemp.Set_Fields("Account", Accounts[counter].Account);
							rsTemp.Set_Fields("Amount", Accounts[counter].Amount);
							rsTemp.Set_Fields("Project", Accounts[counter].Project);
							rsTemp.Set_Fields("WarrantNumber", 0);
							if (lngPeriod > 0 && lngPeriod <= 12)
							{
								rsTemp.Set_Fields("Period", lngPeriod);
							}
							else
							{
								rsTemp.Set_Fields("Period", dtDate.Month);
							}
							rsTemp.Set_Fields("Status", "E");
							if (Accounts[counter].AcctType == "A")
							{
								// do nothing
								if (Strings.Trim(Accounts[counter].RCB) != string.Empty)
								{
									rsTemp.Set_Fields("RCB", Accounts[counter].RCB);
								}
								else
								{
									rsTemp.Set_Fields("RCB", "R");
									// kk12152015 trobds-69  No default value in SQL, default to R
								}
							}
							else
							{
								// correction
								rsTemp.Set_Fields("RCB", "L");
							}
							rsTemp.Update(true);
						}
					}
				}
				lngError = 12;
				AddToJournal = TempJournal;
				// this returns the journal number
				// Dave 6/03/05  Added because of new way we sum budgetary information
				UpdateCalculationNeededTable_5(FCConvert.ToInt16(TempJournal), strDBPath);
				return AddToJournal;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				AddToJournal = -1;
				modGlobalFunctions.AddCYAEntry_240(strModule, "modBudgetaryAccounting", "Error in Add to Journal.", "Error after line " + FCConvert.ToString(lngError) + ".", FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message);
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Add To Journal - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddToJournal;
		}

		public static bool PostJournal(int lngJournalNumber, string strDBPath = "")
		{
			bool PostJournal = false;
			int UpdateRecordNumber;
			int lngErrorCode = 0;
			int counter;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper Master = new clsDRWrapper();
				int TempJournal;
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				clsDRWrapper rsJournalMaster = new clsDRWrapper();
				clsDRWrapper rsJournalDetail = new clsDRWrapper();
				clsDRWrapper rsEncData = new clsDRWrapper();
				clsDRWrapper rsDate = new clsDRWrapper();
				clsDRWrapper rsAutoEntry = new clsDRWrapper();
				clsDRWrapper rsOtherAPJournals = new clsDRWrapper();
				clsDRWrapper rsDetailInfo = new clsDRWrapper();
				clsDRWrapper rsOtherAPDetail = new clsDRWrapper();
				clsDRWrapper rsOldJournalData = new clsDRWrapper();
				clsDRWrapper rsCreditMemo = new clsDRWrapper();
				double TotalEncumbrance = 0;
				clsDRWrapper rsEntries = new clsDRWrapper();
				clsDRWrapper rsJournals = new clsDRWrapper();
				bool blnob = false;
				string ControlDate = "";
				// date to use for control entries
				int lngCloseoutKey = 0;
				bool blnCreditMemo = false;
				string strAPCheckDate;
				clsDRWrapper rsEncAdj = new clsDRWrapper();
				PostJournal = false;
				if (strDBPath != string.Empty)
				{
					rsTemp.DefaultGroup = strDBPath;
					Master.DefaultGroup = strDBPath;
				}
				GetControlAccounts();
				TempJournal = 0;
				strAPCheckDate = "";
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					Statics.strZeroDiv = "0";
				}
				else
				{
					Statics.strZeroDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
				}
				rsJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber), "TWBD0000.vb1");
				if (BadAccountsInJournal(lngJournalNumber))
				{
					MessageBox.Show("There are one or more bad accounts in journal " + FCConvert.ToString(lngJournalNumber) + ".  Please fix the problem with the accounts and try to post again.", "Invalid Accounts In Journal", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return PostJournal;
				}
				else
				{
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP")
					{
						if (!rsJournals.IsFieldNull("CheckDate"))
						{
							// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM APJournal WHERE CheckDate = '" + rsJournals.Get_Fields_DateTime("CheckDate") + "' AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'W' OR Status = 'X') AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
						}
						else
						{
							// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM APJournal WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'W' OR Status = 'X') AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
						}
						strAPCheckDate = FCConvert.ToString(rsJournals.Get_Fields_DateTime("CheckDate"));
					}
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsJournals.Get_Fields("Type") == "AC")
					{
						// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM APJournal WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'E' AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					}
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (rsJournals.Get_Fields("Type") == "EN")
					{
						// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM Encumbrances WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'E' AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					}
							// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
							else if (rsJournals.Get_Fields("Type") == "PY")
					{
						// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM JournalEntries WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND RCB <> 'E' AND Status = 'E' AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					}
					else
					{
						// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM JournalEntries WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'E' AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					}
					if (FCConvert.ToBoolean(rsJournals.Get_Fields_Boolean("CreditMemo")) || FCConvert.ToBoolean(rsJournals.Get_Fields_Boolean("CreditMemoCorrection")))
					{
						blnCreditMemo = true;
					}
					else
					{
						blnCreditMemo = false;
					}
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "PY")
					{
						// rsJournals.Fields("Type") = "CW" Or
						// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						CombineDuplicateControlEntries_2(FCConvert.ToInt32(rsJournals.Get_Fields("JournalNumber")));
					}
					blnob = false;
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "GJ")
					{
						if (Conversion.Val(rsEntries.Get_Fields_Decimal("TotalAmount")) != 0)
						{
							// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsEncAdj.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + "AND Period = " + rsJournals.Get_Fields("Period") + " AND RCB <> 'E'", "TWBD0000.vb1");
							if (rsEncAdj.EndOfFile() != true && rsEncAdj.BeginningOfFile() != true)
							{
								blnob = true;
							}
						}
						else
						{
							if (!CheckOBF_6(lngJournalNumber, "GJ"))
							{
								blnob = true;
							}
						}
					}
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsJournals.Get_Fields("Type") == "PY")
					{
						if (Conversion.Val(rsEntries.Get_Fields_Decimal("TotalAmount")) != 0)
						{
							// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsEncAdj.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + "AND Period = " + rsJournals.Get_Fields("Period") + " AND RCB = 'E'", "TWBD0000.vb1");
							if (rsEncAdj.EndOfFile() != true && rsEncAdj.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								blnob = true;
							}
						}
					}
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (rsJournals.Get_Fields("Type") == "CW")
					{
						if (Conversion.Val(rsEntries.Get_Fields_Decimal("TotalAmount")) != 0)
						{
							blnob = true;
						}
						else
						{
							// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
							if (!CheckOBF_6(lngJournalNumber, rsJournals.Get_Fields("Type")))
							{
								blnob = true;
							}
						}
					}
				}
				if (blnob == true)
				{
					ans = MessageBox.Show("The Journal you are trying to post is out of balance.  Do you still wish to post it?", "Post OOB Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
					if (ans == DialogResult.Yes)
					{
						frmMultiModulePostingOpID.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
						if (Statics.strOpID != "")
						{
							//Application.DoEvents();
						}
						else
						{
							return PostJournal;
						}
					}
					else
					{
						return PostJournal;
					}
				}
				lngErrorCode = 2;
				//! Load frmWait; // show the wait form
				frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
				// center it
				frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Posting Journals";
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.Refresh();
				lngErrorCode = 3;
				Statics.lngNumberOfAccounts = 0;
				Statics.AcctsToPass = new AcctsForCheckRec[0 + 1];
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				Statics.AcctsToPass[0] = new AcctsForCheckRec(0);
				// get the date to use for control entries for journals
				lngErrorCode = 4;
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "EN")
				{
					lngErrorCode = 5;
					rsDate.OpenRecordset("SELECT TOP 1 * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " ORDER BY EncumbrancesDate", "TWBD0000.vb1");
					ControlDate = FCConvert.ToString(rsDate.Get_Fields_DateTime("EncumbrancesDate"));
				}
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "GJ" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "CD" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "CR" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "PY" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "CW")
				{
					lngErrorCode = 6;
					rsDate.OpenRecordset("SELECT TOP 1 * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " ORDER BY JournalEntriesDate", "TWBD0000.vb1");
					ControlDate = FCConvert.ToString(rsDate.Get_Fields_DateTime("JournalEntriesDate"));
				}
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsJournals.Get_Fields("Type") == "AC")
				{
					lngErrorCode = 7;
					rsDate.OpenRecordset("SELECT TOP 1 * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " ORDER BY Payable", "TWBD0000.vb1");
					ControlDate = FCConvert.ToString(rsDate.Get_Fields_DateTime("CheckDate"));
				}
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (rsJournals.Get_Fields("Type") == "AP")
				{
					lngErrorCode = 8;
					if (strAPCheckDate != "")
					{
						rsDate.OpenRecordset("SELECT TOP 1 * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " ORDER BY Payable", "TWBD0000.vb1");
					}
					else
					{
						rsDate.OpenRecordset("SELECT TOP 1 * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " ORDER BY Payable", "TWBD0000.vb1");
					}
					ControlDate = FCConvert.ToString(rsDate.Get_Fields_DateTime("CheckDate"));
				}
				lngErrorCode = 9;
				// create auto entry if needed
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP")
				{
					lngErrorCode = 10;
					if (strAPCheckDate != "")
					{
						rsAutoEntry.OpenRecordset("SELECT * FROM APJournal WHERE Frequency <> '' AND (Until >= " + DateTime.Today.ToShortDateString() + " OR isnull(Until,'') = '') AND (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " ORDER BY Payable", "TWBD0000.vb1");
					}
					else
					{
						rsAutoEntry.OpenRecordset("SELECT * FROM APJournal WHERE Frequency <> '' AND (Until >= " + DateTime.Today.ToShortDateString() + " OR isnull(Until,'') = '') AND (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " ORDER BY Payable", "TWBD0000.vb1");
					}
					if (rsAutoEntry.EndOfFile() != true && rsAutoEntry.BeginningOfFile() != true)
					{
						do
						{
							//Application.DoEvents();
							if (TempJournal == 0)
							{
								if (LockJournal() == false)
								{
									MessageBox.Show("User " + Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									//Application.DoEvents();
									frmWait.InstancePtr.Unload();
									return PostJournal;
								}
								lngErrorCode = 11;
								Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC", "TWBD0000.vb1");
								if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
								{
									lngErrorCode = 12;
									Master.MoveLast();
									Master.MoveFirst();
									// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
									TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
								}
								else
								{
									TempJournal = 1;
								}
								lngErrorCode = 13;
								rsOtherAPJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP'", "TWBD0000.vb1");
								if (rsOtherAPJournals.EndOfFile() != true && rsOtherAPJournals.BeginningOfFile() != true)
								{
									CheckAgain:
									;
									ans = MessageBox.Show("The repeat invoices will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save Repeat Checks", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
									if (ans == DialogResult.No)
									{
										frmSelectPostingJournal.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
										if (Statics.lngSelectedJournal == -1)
										{
											goto CheckAgain;
										}
										else
										{
											TempJournal = Statics.lngSelectedJournal;
										}
										// TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
										rsOtherAPJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE Period = " + FCConvert.ToString(DateAndTime.DateAdd("d", rsAutoEntry.Get_Fields("Frequency"), (DateTime)rsAutoEntry.Get_Fields_DateTime("Payable")).Month) + " AND JournalNumber = " + FCConvert.ToString(TempJournal), "TWBD0000.vb1");
										if (rsOtherAPJournals.EndOfFile() != true && rsOtherAPJournals.BeginningOfFile() != true)
										{
											// do nothing
										}
										else
										{
											Master.AddNew();
											Master.Set_Fields("JournalNumber", TempJournal);
											Master.Set_Fields("Status", "E");
											Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
											Master.Set_Fields("StatusChangeDate", DateTime.Today);
											Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Repeat Invoices");
											Master.Set_Fields("Type", "AP");
											// TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
											Master.Set_Fields("Period", DateAndTime.DateAdd("d", rsAutoEntry.Get_Fields("Frequency"), (DateTime)rsAutoEntry.Get_Fields_DateTime("Payable")).Month);
											Master.Update();
											Master.Reset();
										}
										UnlockJournal();
									}
									else
									{
										Master.AddNew();
										Master.Set_Fields("JournalNumber", TempJournal);
										Master.Set_Fields("Status", "E");
										Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yy") + " Repeat Invoices");
										Master.Set_Fields("Type", "AP");
                                        Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                                        Master.Set_Fields("StatusChangeDate", DateTime.Today);
                                        // TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                                        Master.Set_Fields("Period", DateAndTime.DateAdd("d", rsAutoEntry.Get_Fields("Frequency"), (DateTime)rsAutoEntry.Get_Fields_DateTime("Payable")).Month);
										Master.Update();
										// Master.Reset
										UnlockJournal();
									}
								}
								else
								{
									MessageBox.Show("Journal number " + modValidateAccount.GetFormat_6(FCConvert.ToString(TempJournal), 4) + " has been assigned to the repeat invoices journal.", "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								lngErrorCode = 14;
							}
							else
							{
								// TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
								rsOtherAPJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE Period = " + FCConvert.ToString(DateAndTime.DateAdd("d", rsAutoEntry.Get_Fields("Frequency"), (DateTime)rsAutoEntry.Get_Fields_DateTime("Payable")).Month) + " AND JournalNumber = " + FCConvert.ToString(TempJournal), "TWBD0000.vb1");
								if (rsOtherAPJournals.EndOfFile() != true && rsOtherAPJournals.BeginningOfFile() != true)
								{
									// do nothing
								}
								else
								{
									lngErrorCode = 15;
									Master.AddNew();
									Master.Set_Fields("JournalNumber", TempJournal);
									Master.Set_Fields("Status", "E");
									Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
									Master.Set_Fields("StatusChangeDate", DateTime.Today);
									Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Repeat Invoices");
									Master.Set_Fields("Type", "AP");
									// TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
									Master.Set_Fields("Period", DateAndTime.DateAdd("d", rsAutoEntry.Get_Fields("Frequency"), (DateTime)rsAutoEntry.Get_Fields_DateTime("Payable")).Month);
									Master.Update();
									// Master.Reset
								}
							}
							lngErrorCode = 16;
							rsOtherAPJournals.OpenRecordset("SELECT * FROM APJournal", "TWBD0000.vb1");
							rsOtherAPJournals.AddNew();
							rsOtherAPJournals.Set_Fields("JournalNumber", TempJournal);
							rsOtherAPJournals.Set_Fields("VendorNumber", rsAutoEntry.Get_Fields_Int32("VendorNumber"));
							rsOtherAPJournals.Set_Fields("TempVendorName", rsAutoEntry.Get_Fields_String("TempVendorName"));
							rsOtherAPJournals.Set_Fields("TempVendorAddress1", rsAutoEntry.Get_Fields_String("TempVendorAddress1"));
							rsOtherAPJournals.Set_Fields("TempVendorAddress2", rsAutoEntry.Get_Fields_String("TempVendorAddress2"));
							rsOtherAPJournals.Set_Fields("TempVendorAddress3", rsAutoEntry.Get_Fields_String("TempVendorAddress3"));
							rsOtherAPJournals.Set_Fields("TempVendorCity", rsAutoEntry.Get_Fields_String("TempVendorCity"));
							rsOtherAPJournals.Set_Fields("TempVendorState", rsAutoEntry.Get_Fields_String("TempVendorState"));
							rsOtherAPJournals.Set_Fields("TempVendorZip", rsAutoEntry.Get_Fields_String("TempVendorZip"));
							rsOtherAPJournals.Set_Fields("TempVendorZip4", rsAutoEntry.Get_Fields_String("TempVendorZip4"));
							// TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
							rsOtherAPJournals.Set_Fields("Period", DateAndTime.DateAdd("d", rsAutoEntry.Get_Fields("Frequency"), (DateTime)rsAutoEntry.Get_Fields_DateTime("Payable")).Month);
							// TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
							rsOtherAPJournals.Set_Fields("Payable", DateAndTime.DateAdd("d", rsAutoEntry.Get_Fields("Frequency"), (DateTime)rsAutoEntry.Get_Fields_DateTime("Payable")));
							// TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
							rsOtherAPJournals.Set_Fields("Frequency", rsAutoEntry.Get_Fields("Frequency"));
							rsOtherAPJournals.Set_Fields("Seperate", rsAutoEntry.Get_Fields_Boolean("Seperate"));
							rsOtherAPJournals.Set_Fields("Description", rsAutoEntry.Get_Fields_String("Description"));
							rsOtherAPJournals.Set_Fields("Reference", rsAutoEntry.Get_Fields_String("Reference"));
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							rsOtherAPJournals.Set_Fields("Amount", rsAutoEntry.Get_Fields("Amount"));
							rsOtherAPJournals.Set_Fields("Check", "");
							rsOtherAPJournals.Set_Fields("Status", "E");
							rsOtherAPJournals.Set_Fields("PostedDate", null);
							rsOtherAPJournals.Set_Fields("Warrant", "");
							rsOtherAPJournals.Set_Fields("CheckDate", null);
							rsOtherAPJournals.Set_Fields("Returned", "N");
							rsOtherAPJournals.Set_Fields("TotalEncumbrance", 0);
							rsOtherAPJournals.Set_Fields("EncumbranceRecord", 0);
							lngErrorCode = 17;
							rsOtherAPDetail.OpenRecordset("SELECT * FROM APJournalDetail", "TWBD0000.vb1");
							rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsAutoEntry.Get_Fields_Int32("ID"), "TWBD0000.vb1");
							if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
							{
								lngErrorCode = 18;
								do
								{
									//Application.DoEvents();
									rsOtherAPDetail.AddNew();
									rsOtherAPDetail.Set_Fields("APJournalID", rsOtherAPJournals.Get_Fields_Int32("ID"));
									rsOtherAPDetail.Set_Fields("Description", rsDetailInfo.Get_Fields_String("Description"));
									// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
									rsOtherAPDetail.Set_Fields("Account", rsDetailInfo.Get_Fields("Account"));
									// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
									rsOtherAPDetail.Set_Fields("Amount", rsDetailInfo.Get_Fields("Amount"));
									rsOtherAPDetail.Set_Fields("Project", rsDetailInfo.Get_Fields_String("Project"));
									// TODO: Check the table for the column [Discount] and replace with corresponding Get_Field method
									rsOtherAPDetail.Set_Fields("Discount", rsDetailInfo.Get_Fields("Discount"));
									rsOtherAPDetail.Set_Fields("Encumbrance", 0);
									// TODO: Check the table for the column [1099] and replace with corresponding Get_Field method
									rsOtherAPDetail.Set_Fields("1099", rsDetailInfo.Get_Fields("1099"));
									rsOtherAPDetail.Set_Fields("RCB", "R");
									rsOtherAPDetail.Set_Fields("Corrected", false);
									rsOtherAPDetail.Set_Fields("EncumbranceDetailRecord", 0);
									rsOtherAPDetail.Update(true);
									rsDetailInfo.MoveNext();
								}
								while (rsDetailInfo.EndOfFile() != true);
								lngErrorCode = 19;
								rsOtherAPJournals.Update(true);
							}
							rsAutoEntry.MoveNext();
						}
						while (rsAutoEntry.EndOfFile() != true);
						lngErrorCode = 20;
					}
				}
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (FCConvert.ToString(rsJournals.Get_Fields("Type")) != "EN" && FCConvert.ToString(rsJournals.Get_Fields("Type")) != "AP" && FCConvert.ToString(rsJournals.Get_Fields("Type")) != "AC")
				{
					if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "CALAIS" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "NORWAY")
					{
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						MoveWaterEntries_6(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")));
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsJournalMaster.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
						if (rsJournalMaster.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
						{
							// do nothing
						}
						else
						{
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsJournalMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
							if (rsJournalMaster.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
							{
								rsJournalMaster.Edit();
								rsJournalMaster.Set_Fields("Status", "D");
								rsJournalMaster.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
								rsJournalMaster.Set_Fields("StatusChangeDate", DateTime.Today);
								rsJournalMaster.Set_Fields("StatusChangeOpID", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
								rsJournalMaster.Update();
								frmWait.InstancePtr.Unload();
								MessageBox.Show("There were no other entries in the journal so it has been set to deleted status.", "Journal Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return PostJournal;
							}
						}
					}
				}
				lngErrorCode = 21;
				// if closing out an Encumbrance, AP Journal, or AP Journal Correction create a main control entry
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AC")
				{
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					lngCloseoutKey = CreateAPControl_60(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")), ControlDate, rsJournals.Get_Fields("Type"), strDBPath);
				}
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsJournals.Get_Fields("Type") == "EN")
				{
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					lngCloseoutKey = CreateEncControl_6(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")), ControlDate, strDBPath);
				}
				else
				{
					lngCloseoutKey = 0;
				}
				lngErrorCode = 22;
				// close expense accounts in journal
				// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (!CloseExp_60(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")), ControlDate, rsJournals.Get_Fields("Type"), lngCloseoutKey, strAPCheckDate, strDBPath))
				{
					frmWait.InstancePtr.Unload();
					return PostJournal;
				}
				lngErrorCode = 23;
				// close revenue accounts in journal
				// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (!CloseRev_60(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")), ControlDate, rsJournals.Get_Fields("Type"), lngCloseoutKey, strAPCheckDate, strDBPath))
				{
					frmWait.InstancePtr.Unload();
					return PostJournal;
				}
				lngErrorCode = 24;
				if (FCConvert.ToString(GetBDVariable("Due")) == "Y" && blnCreditMemo == false)
				{
					// close Due To and Due From
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AC")
					{
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						if (!CloseDueToFrom_24(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")), rsJournals.Get_Fields("Type"), lngCloseoutKey, strAPCheckDate, strDBPath))
						{
							frmWait.InstancePtr.Unload();
							return PostJournal;
						}
					}
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "CD" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "CR" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "GJ")
					{
						// use global module code to close Due To From
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						if (!GlobalCloseDueToFrom_24(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")), rsJournals.Get_Fields("Type"), ControlDate, strDBPath))
						{
							frmWait.InstancePtr.Unload();
							return PostJournal;
						}
					}
				}
				else
				{
					// close cash in journal
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (!CloseCash_60(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")), ControlDate, rsJournals.Get_Fields("Type"), lngCloseoutKey, blnCreditMemo, strAPCheckDate, strDBPath))
					{
						frmWait.InstancePtr.Unload();
						return PostJournal;
					}
				}
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "PY" && modGlobalConstants.Statics.gstrArchiveYear != "")
				{
					CheckJournalForAPArchiveEntries(ref lngJournalNumber, strDBPath);
				}
				lngErrorCode = 25;
				// close encumbrances in journal
				// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (!CloseEnc_60(lngJournalNumber, FCConvert.ToInt16(rsJournals.Get_Fields("Period")), ControlDate, rsJournals.Get_Fields("Type"), lngCloseoutKey, strAPCheckDate, strDBPath))
				{
					frmWait.InstancePtr.Unload();
					return PostJournal;
				}
				lngErrorCode = 26;
				// if journal is out of balance save closing operator information
				if (strAPCheckDate != "")
				{
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					rsJournalMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
				}
				else
				{
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					rsJournalMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
				}
				if (FCConvert.ToBoolean(rsJournalMaster.Get_Fields_Boolean("CreditMemo")))
				{
					rsCreditMemo.OpenRecordset("SELECT * FROM CreditMemo WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber), "TWBD0000.vb1");
					if (rsCreditMemo.EndOfFile() != true && rsCreditMemo.BeginningOfFile() != true)
					{
						do
						{
							rsCreditMemo.Edit();
							rsCreditMemo.Set_Fields("Status", "P");
							rsCreditMemo.Set_Fields("PostedDate", DateTime.Today);
							rsCreditMemo.Update();
							rsCreditMemo.MoveNext();
						}
						while (rsCreditMemo.EndOfFile() != true);
					}
				}
				lngErrorCode = 27;
				if (rsJournalMaster.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
				{
					if (blnob)
					{
						rsJournalMaster.Edit();
						rsJournalMaster.Set_Fields("PostedOOB", true);
						rsJournalMaster.Set_Fields("OOBPostingOpID", Statics.strOpID);
						rsJournalMaster.Set_Fields("OOBPostingDate", DateTime.Today);
						rsJournalMaster.Set_Fields("OOBPostingTime", DateAndTime.TimeOfDay);
						rsJournalMaster.Set_Fields("Status", "P");
						rsJournalMaster.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						rsJournalMaster.Set_Fields("StatusChangeDate", DateTime.Today);
						rsJournalMaster.Set_Fields("StatusChangeOpID", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
						rsJournalMaster.Update();
					}
					else
					{
						rsJournalMaster.Edit();
						rsJournalMaster.Set_Fields("PostedOOB", false);
						rsJournalMaster.Set_Fields("Status", "P");
						rsJournalMaster.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						rsJournalMaster.Set_Fields("StatusChangeDate", DateTime.Today);
						rsJournalMaster.Set_Fields("StatusChangeOpID", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
						rsJournalMaster.Update();
					}
				}
				else
				{
					MessageBox.Show("Could not Find Journal " + FCConvert.ToString(lngJournalNumber) + " in the JournalMaster table");
				}
				lngErrorCode = 28;
				// Change status of Journal to Posted
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "EN")
				{
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					rsJournalMaster.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					if (rsJournalMaster.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
					{
						lngErrorCode = 29;
						do
						{
							rsJournalMaster.Edit();
							rsJournalMaster.Set_Fields("Status", "P");
							rsJournalMaster.Set_Fields("PostedDate", DateTime.Today);
							rsJournalMaster.Update();
							rsJournalMaster.MoveNext();
						}
						while (rsJournalMaster.EndOfFile() != true);
					}
				}
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AC")
				{
					lngErrorCode = 30;
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP")
					{
						if (strAPCheckDate != "")
						{
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsJournalMaster.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
						}
						else
						{
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsJournalMaster.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
						}
					}
					else
					{
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsJournalMaster.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					}
					if (rsJournalMaster.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
					{
						do
						{
							rsJournalMaster.Edit();
							rsJournalMaster.Set_Fields("Status", "P");
							rsJournalMaster.Set_Fields("PostedDate", DateTime.Today);
							rsJournalMaster.Update();
							rsJournalMaster.MoveNext();
						}
						while (rsJournalMaster.EndOfFile() != true);
					}
				}
				else
				{
					lngErrorCode = 31;
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					rsJournalMaster.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					if (rsJournalMaster.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
					{
						do
						{
							rsJournalMaster.Edit();
							rsJournalMaster.Set_Fields("Status", "P");
							rsJournalMaster.Set_Fields("PostedDate", DateTime.Today);
							rsJournalMaster.Update();
							rsJournalMaster.MoveNext();
						}
						while (rsJournalMaster.EndOfFile() != true);
					}
				}
				lngErrorCode = 32;
				// Update Encumbrance Information changed by Posted Journals
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "EN")
				{
					// do nothing
				}
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AC")
				{
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP")
					{
						if (strAPCheckDate != "")
						{
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsJournalMaster.OpenRecordset("SELECT * FROM APJournal WHERE EncumbranceRecord <> 0 AND Status = 'P' AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
						}
						else
						{
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsJournalMaster.OpenRecordset("SELECT * FROM APJournal WHERE EncumbranceRecord <> 0 AND Status = 'P' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
						}
					}
					else
					{
						// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsJournalMaster.OpenRecordset("SELECT * FROM APJournal WHERE EncumbranceRecord <> 0 AND Status = 'P' AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					}
					if (rsJournalMaster.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
					{
						do
						{
							rsJournalDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalMaster.Get_Fields_Int32("ID"), "TWBD0000.vb1");
							if (rsJournalDetail.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
							{
								TotalEncumbrance = 0;
								do
								{
									if (FCConvert.ToInt32(rsJournalDetail.Get_Fields_Int32("EncumbranceDetailRecord")) != 0)
									{
										TotalEncumbrance += FCConvert.ToDouble(rsJournalDetail.Get_Fields_Decimal("Encumbrance"));
										rsEncData.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = " + rsJournalDetail.Get_Fields_Int32("EncumbranceDetailRecord"), "TWBD0000.vb1");
										if (rsEncData.EndOfFile() != true && rsEncData.BeginningOfFile() != true)
										{
											rsEncData.Edit();
											rsEncData.Set_Fields("Liquidated", rsEncData.Get_Fields_Decimal("Liquidated") + rsJournalDetail.Get_Fields_Decimal("Encumbrance"));
											rsEncData.Update();
										}
									}
									rsJournalDetail.MoveNext();
								}
								while (rsJournalDetail.EndOfFile() != true);
							}
							rsEncData.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + rsJournalMaster.Get_Fields_Int32("EncumbranceRecord"), "TWBD0000.vb1");
							if (rsEncData.EndOfFile() != true && rsEncData.BeginningOfFile() != true)
							{
								rsEncData.Edit();
								rsEncData.Set_Fields("Liquidated", rsEncData.Get_Fields_Decimal("Liquidated") + FCConvert.ToDecimal(TotalEncumbrance));
								rsEncData.Update();
							}
							rsJournalMaster.MoveNext();
						}
						while (rsJournalMaster.EndOfFile() != true);
					}
				}
				lngErrorCode = 33;
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "CD" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "GJ" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "CR" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "CW")
				{
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					rsJournalMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND Period = " + rsJournals.Get_Fields("Period"), "TWBD0000.vb1");
					if (rsJournalMaster.EndOfFile() != true && rsJournalMaster.BeginningOfFile() != true)
					{
						if (rsJournalMaster.Get_Fields_Boolean("AutomaticJournal") != true)
						{
							// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
							AddCheckRecEntriesToStruct_6(lngJournalNumber, rsJournals.Get_Fields("Type"));
						}
					}
					else
					{
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						AddCheckRecEntriesToStruct_6(lngJournalNumber, rsJournals.Get_Fields("Type"));
					}
				}
				SaveCheckRecEntries();
				lngErrorCode = 34;
				PostJournal = true;
				return PostJournal;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Posting Error - " + FCConvert.ToString(lngErrorCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return PostJournal;
		}

		public static bool LockJournal(string strPath = "")
		{
			bool LockJournal = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (strPath != string.Empty)
			{
				rsTemp.GroupName = strPath;
			}
			rsTemp.OpenRecordset("SELECT * FROM JournalLock", "TWBD0000.vb1");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				if (rsTemp.Get_Fields_Boolean("MasterLock") == false)
				{
					if (rsTemp.Edit())
					{
						rsTemp.Set_Fields("MasterLock", true);
						rsTemp.Set_Fields("OpID", modGlobalConstants.Statics.gstrUserID);
						if (rsTemp.Update(false))
						{
							LockJournal = true;
						}
						else
						{
							LockJournal = false;
						}
					}
				}
				else
				{
					LockJournal = false;
					Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
					rsTemp.Reset();
					return LockJournal;
				}
			}
			else
			{
				rsTemp.AddNew();
				rsTemp.Set_Fields("MasterLock", true);
				rsTemp.Set_Fields("OpID", modGlobalConstants.Statics.gstrUserID);
				if (rsTemp.Update(false))
				{
					LockJournal = true;
				}
				else
				{
					LockJournal = false;
				}
			}
			rsTemp.OpenRecordset("SELECT * FROM JournalLock", "TWBD0000.vb1");
			if (FCConvert.ToString(rsTemp.Get_Fields_String("OpID")) != FCConvert.ToString(modGlobalConstants.Statics.gstrUserID))
			{
				LockJournal = false;
				Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
			}
			return LockJournal;
		}

		public static void UnlockJournal(string strPath = "")
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (strPath != string.Empty)
			{
				rsTemp.GroupName = strPath;
			}
			rsTemp.OpenRecordset("SELECT * FROM JournalLock", "TWBD0000.vb1");
			rsTemp.Edit();
			rsTemp.Set_Fields("MasterLock", false);
			rsTemp.Set_Fields("OpID", "");
			rsTemp.Update(true);
		}

		public static int GetFundFromAccount_2(string strAcct, bool boolShowError = false)
		{
			return GetFundFromAccount(strAcct, boolShowError);
		}

		public static int GetFundFromAccount(string strAcct, bool boolShowError = false)
		{
			int GetFundFromAccount = 0;
			// this function will take the account passed in and find the fund and return it
			// if there is an error then it will return -1
			int intFirst = 0;
			int lngDept = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (strAcct != "" && strAcct.Length > 2 && Strings.InStr(1, "_", strAcct, CompareConstants.vbBinaryCompare) == 0)
				{
					// if it is not empty
					if (Strings.Left(strAcct, 1) == "G")
					{
						intFirst = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))));
						if (strAcct.Length > (intFirst + 2))
						{
							GetFundFromAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strAcct, 3, intFirst))));
						}
						else
						{
							GetFundFromAccount = -1;
						}
					}
					else if (Strings.Left(strAcct, 1) == "E")
					{
						intFirst = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))));
						if (strAcct.Length > (intFirst + 2))
						{
							lngDept = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strAcct, 3, intFirst))));
							GetFundFromAccount = GetFundFromDept(ref lngDept, ref intFirst, ref boolShowError);
						}
						else
						{
							GetFundFromAccount = -1;
						}
					}
					else if (Strings.Left(strAcct, 1) == "R")
					{
						intFirst = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2))));
						if (strAcct.Length > (intFirst + 2))
						{
							lngDept = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strAcct, 3, intFirst))));
							GetFundFromAccount = GetFundFromDept(ref lngDept, ref intFirst, ref boolShowError);
						}
						else
						{
							GetFundFromAccount = -1;
						}
					}
					else
					{
					}
				}
				return GetFundFromAccount;
			}
			catch (Exception ex)
			{
				
				if (boolShowError)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Get Fund Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				GetFundFromAccount = -1;
			}
			return GetFundFromAccount;
		}
		// vbPorter upgrade warning: intLen As short	OnWriteFCConvert.ToInt32(
		private static int GetFundFromDept(ref int lngDept, ref int intLen, ref bool boolShowError)
		{
			int GetFundFromDept = 0;
			// this will return the fund associated withthe department passed in
			// if there is an error, then there will be
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBD = new clsDRWrapper();
				rsBD.OpenRecordset("SELECT * FROM DeptDivTitles WHERE convert(int, IsNull(Fund, 0)) <> 0", "TWBD0000.vb1");
				if (rsBD.EndOfFile() != true && rsBD.BeginningOfFile() != true)
				{
					rsBD.FindFirstRecord("Department", modGlobal.PadToString(lngDept, FCConvert.ToInt16(intLen)));
					if (rsBD.NoMatch)
					{
						// no match was found
						GetFundFromDept = -1;
					}
					else
					{
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						GetFundFromDept = FCConvert.ToInt32(rsBD.Get_Fields("Fund"));
					}
				}
				else
				{
					GetFundFromDept = -1;
				}
				return GetFundFromDept;
			}
			catch (Exception ex)
			{
				
				if (boolShowError)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", "Get Fund From Department Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				GetFundFromDept = -1;
			}
			return GetFundFromDept;
		}
		// vbPorter upgrade warning: lngJournal As short	OnWriteFCConvert.ToInt32(
		public static void UpdateCalculationNeededTable_5(short lngJournal, string strPath = "")
		{
			UpdateCalculationNeededTable(lngJournal, false, strPath);
		}

		public static void UpdateCalculationNeededTable(int lngJournal, bool blnForce = false, string strPath = "")
		{
			clsDRWrapper rsCalcInfo = new clsDRWrapper();
			clsDRWrapper rsMasterInfo = new clsDRWrapper();
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			bool blnLedger;
			bool blnExp;
			bool blnRev;
			// Dim blnSchoolLedger As Boolean
			// Dim blnSchoolExp As Boolean
			// Dim blnSchoolRev As Boolean
			if (strPath != string.Empty)
			{
				rsCalcInfo.GroupName = strPath;
				rsMasterInfo.GroupName = strPath;
				rsJournalInfo.GroupName = strPath;
			}
			blnLedger = false;
			blnExp = false;
			blnRev = false;
			// blnSchoolLedger = False
			// blnSchoolExp = False
			// blnSchoolRev = False
			if (blnForce)
			{
				rsCalcInfo.OpenRecordset("SELECT * FROM CalculationNeeded", "TWBD0000.vb1");
				if (rsCalcInfo.EndOfFile() != true && rsCalcInfo.BeginningOfFile() != true)
				{
					rsCalcInfo.Edit();
				}
				else
				{
					rsCalcInfo.AddNew();
				}
				rsCalcInfo.Set_Fields("ExpPendingSummary", true);
				rsCalcInfo.Set_Fields("RevPendingSummary", true);
				rsCalcInfo.Set_Fields("LedgerPendingSummary", true);
				rsCalcInfo.Set_Fields("ExpPendingDetail", true);
				rsCalcInfo.Set_Fields("RevPendingDetail", true);
				rsCalcInfo.Set_Fields("LedgerPendingDetail", true);
				rsCalcInfo.Set_Fields("SchoolExpPendingSummary", true);
				rsCalcInfo.Set_Fields("SchoolRevPendingSummary", true);
				rsCalcInfo.Set_Fields("SchoolLedgerPendingSummary", true);
				rsCalcInfo.Set_Fields("SchoolExpPendingDetail", true);
				rsCalcInfo.Set_Fields("SchoolRevPendingDetail", true);
				rsCalcInfo.Set_Fields("SchoolLedgerPendingDetail", true);
				rsCalcInfo.Update();
			}
			else
			{
				rsMasterInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
				if (rsMasterInfo.EndOfFile() != true && rsMasterInfo.BeginningOfFile() != true)
				{
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsMasterInfo.Get_Fields("Type")) == "AC" || FCConvert.ToString(rsMasterInfo.Get_Fields("Type")) == "AP")
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID WHERE APJournal.JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
					}
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsMasterInfo.Get_Fields("Type") == "EN")
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID WHERE Encumbrances.JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
					}
					else
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
					}
					if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
					{
						do
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsJournalInfo.Get_Fields("Account")), 1) == "E")
							{
								blnExp = true;
							}
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							else if (Strings.Left(FCConvert.ToString(rsJournalInfo.Get_Fields("Account")), 1) == "R")
							{
								blnRev = true;
							}
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								else if (Strings.Left(FCConvert.ToString(rsJournalInfo.Get_Fields("Account")), 1) == "G")
							{
								blnLedger = true;
								// ElseIf Left(rsJournalInfo.Fields("Account"), 1) = "P" Then
								// blnSchoolExp = True
								// ElseIf Left(rsJournalInfo.Fields("Account"), 1) = "V" Then
								// blnSchoolRev = True
								// ElseIf Left(rsJournalInfo.Fields("Account"), 1) = "L" Then
								// blnSchoolLedger = True
							}
							rsJournalInfo.MoveNext();
						}
						while (rsJournalInfo.EndOfFile() != true);
					}
					rsCalcInfo.OpenRecordset("SELECT * FROM CalculationNeeded", "TWBD0000.vb1");
					if (rsCalcInfo.EndOfFile() != true && rsCalcInfo.BeginningOfFile() != true)
					{
						rsCalcInfo.Edit();
					}
					else
					{
						rsCalcInfo.AddNew();
					}
					if (blnExp)
					{
						rsCalcInfo.Set_Fields("ExpPendingSummary", blnExp);
						rsCalcInfo.Set_Fields("ExpPendingDetail", blnExp);
					}
					if (blnRev)
					{
						rsCalcInfo.Set_Fields("RevPendingSummary", blnRev);
						rsCalcInfo.Set_Fields("RevPendingDetail", blnRev);
					}
					if (blnLedger)
					{
						rsCalcInfo.Set_Fields("LedgerPendingSummary", blnLedger);
						rsCalcInfo.Set_Fields("LedgerPendingDetail", blnLedger);
					}
					rsCalcInfo.Update();
				}
				else
				{
					rsCalcInfo.OpenRecordset("SELECT * FROM CalculationNeeded", "TWBD0000.vb1");
					if (rsCalcInfo.EndOfFile() != true && rsCalcInfo.BeginningOfFile() != true)
					{
						rsCalcInfo.Edit();
					}
					else
					{
						rsCalcInfo.AddNew();
					}
					rsCalcInfo.Set_Fields("ExpPendingSummary", true);
					rsCalcInfo.Set_Fields("RevPendingSummary", true);
					rsCalcInfo.Set_Fields("LedgerPendingSummary", true);
					rsCalcInfo.Set_Fields("ExpPendingDetail", true);
					rsCalcInfo.Set_Fields("RevPendingDetail", true);
					rsCalcInfo.Set_Fields("LedgerPendingDetail", true);
					rsCalcInfo.Set_Fields("SchoolExpPendingSummary", true);
					rsCalcInfo.Set_Fields("SchoolRevPendingSummary", true);
					rsCalcInfo.Set_Fields("SchoolLedgerPendingSummary", true);
					rsCalcInfo.Set_Fields("SchoolExpPendingDetail", true);
					rsCalcInfo.Set_Fields("SchoolRevPendingDetail", true);
					rsCalcInfo.Set_Fields("SchoolLedgerPendingDetail", true);
					rsCalcInfo.Update();
				}
			}
		}

		public static string GetExpense(string x)
		{
			string GetExpense = "";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				GetExpense = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
			}
			else
			{
				GetExpense = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
			}
			return GetExpense;
		}

		public static string GetDepartment_2(string x)
		{
			return GetDepartment(x);
		}

		public static string GetDepartment(string x)
		{
			string GetDepartment = "";
			GetDepartment = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
			return GetDepartment;
		}

		public static string GetExpDivision(string x)
		{
			string GetExpDivision = "";
			GetExpDivision = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			return GetExpDivision;
		}

		public static string GetObject(string x)
		{
			string GetObject = "";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				GetObject = Strings.Mid(x, 6 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			}
			else
			{
				GetObject = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			}
			return GetObject;
		}

		public static string GetRevenue_2(string x)
		{
			return GetRevenue(x);
		}

		public static string GetRevenue(string x)
		{
			string GetRevenue = "";
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				GetRevenue = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
			}
			else
			{
				GetRevenue = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
			}
			return GetRevenue;
		}

		public static string GetRevDivision(string x)
		{
			string GetRevDivision = "";
			GetRevDivision = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))));
			return GetRevDivision;
		}

		public static string GetSuffix(string x)
		{
			string GetSuffix = "";
			GetSuffix = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)))));
			return GetSuffix;
		}

		public static string GetFund_2(string x)
		{
			return GetFund(ref x);
		}

		public static string GetFund(ref string x)
		{
			string GetFund = "";
			GetFund = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))));
			return GetFund;
		}

		public static string GetAccount_2(string x)
		{
			return GetAccount(x);
		}

		public static string GetAccount(string x)
		{
			string GetAccount = "";
			GetAccount = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			return GetAccount;
		}
		// Public Function GetSchoolFund(x As String) As String
		// GetSchoolFund = Mid(x, 3, Val(Left(SchoolExp, 2)))
		// End Function
		//
		// Public Function GetSchoolProgram(x As String) As String
		// GetSchoolProgram = Mid(x, 4 + Val(Left(SchoolExp, 2)), Val(Mid(SchoolExp, 3, 2)))
		// End Function
		//
		// Public Function GetSchoolFunction(x As String) As String
		// GetSchoolFunction = Mid(x, 5 + Val(Left(SchoolExp, 2)) + Val(Mid(SchoolExp, 3, 2)), Val(Mid(SchoolExp, 5, 2)))
		// End Function
		//
		// Public Function GetSchoolObject(x As String) As String
		// GetSchoolObject = Mid(x, 6 + Val(Left(SchoolExp, 2)) + Val(Mid(SchoolExp, 3, 2)) + Val(Mid(SchoolExp, 5, 2)), Val(Mid(SchoolExp, 7, 2)))
		// End Function
		//
		// Public Function GetSchoolCostCenter(x As String) As String
		// GetSchoolCostCenter = Mid(x, 7 + Val(Left(SchoolExp, 2)) + Val(Mid(SchoolExp, 3, 2)) + Val(Mid(SchoolExp, 5, 2)) + Val(Mid(SchoolExp, 7, 2)), Val(Mid(SchoolExp, 9, 2)))
		// End Function
		//
		// Public Function GetSchoolRevenue(x As String) As String
		// GetSchoolRevenue = Mid(x, 4 + Val(Left(SchoolRev, 2)), Val(Mid(SchoolRev, 3, 2)))
		// End Function
		//
		// Public Function GetSchoolBalanceSheetCode(x As String) As String
		// GetSchoolBalanceSheetCode = Mid(x, 4 + Val(Left(SchoolLedger, 2)), Val(Mid(SchoolLedger, 3, 2)))
		// End Function
		//
		// Public Function GetSchoolSuffix(x As String) As String
		// GetSchoolSuffix = Mid(x, 5 + Val(Left(SchoolLedger, 2)) + Val(Mid(SchoolLedger, 3, 2)), Val(Mid(SchoolLedger, 5, 2)))
		// End Function
		public static string GetBankOverride(ref int lngBankNumber, bool boolTown, bool boolShowError = true)
		{
			string GetBankOverride = "";
			// if they have budgetary and the bank has an override cash account, this returns the override portion of the account
			try
			{
				// On Error GoTo ErrorHandler
				GetBankOverride = "";
				if (lngBankNumber > 0)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						clsDRWrapper rsLoad = new clsDRWrapper();
						rsLoad.OpenRecordset("select * from banks where ID = " + FCConvert.ToString(lngBankNumber), "twbd0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							GetBankOverride = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("towncashaccount")));
						}
					}
				}
				return GetBankOverride;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolShowError)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In GetBankOverride", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return GetBankOverride;
		}

		public static bool CalcCashFundsFromOverride(ref string strOverrideAccountPiece, ref FundType[] Accounts, ref FundType[] Funds, bool bolNoAccounts = false, string CashDescription = "", string strCashAccountType = "AP", bool blnSchool = false, int lngCashIndex = 0)
		{
			bool CalcCashFundsFromOverride = false;
			// creates an account for each fund having values.  The appropriate fund followed by the override followed by the suffix is the account format
			CalcCashFundsFromOverride = false;
			try
			{
				// On Error GoTo ErrorHandler
				int counter;
				if (strCashAccountType != "GJ")
				{
					lngCashIndex = Information.UBound(Accounts, 1) + 1;
					for (counter = 1; counter <= 999; counter++)
					{
						if (Funds[counter].Amount != 0)
						{
							if (bolNoAccounts)
							{
								bolNoAccounts = false;
								Accounts[0].Description = CashDescription + " - Cash";
								if (modAccountTitle.Statics.YearFlag)
								{
									Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strOverrideAccountPiece;
								}
								else
								{
									Accounts[0].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strOverrideAccountPiece + "-00";
								}
								Accounts[0].AcctType = "C";
								Accounts[0].Amount = Funds[counter].Amount * -1;
							}
							else
							{
								Array.Resize(ref Accounts, Information.UBound(Accounts, 1) + 1 + 1);
								Accounts[Information.UBound(Accounts)].Description = CashDescription + " - Cash";
								if (modAccountTitle.Statics.YearFlag)
								{
									Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strOverrideAccountPiece;
								}
								else
								{
									Accounts[Information.UBound(Accounts)].Account = "G " + modValidateAccount.GetFormat_6(Funds[counter].Account, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + strOverrideAccountPiece + "-00";
								}
								Accounts[Information.UBound(Accounts)].AcctType = "C";
								Accounts[Information.UBound(Accounts)].Amount = Funds[counter].Amount * -1;
							}
						}
					}
				}
				CalcCashFundsFromOverride = true;
				return CalcCashFundsFromOverride;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In CalcCashFundsFromOverride", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalcCashFundsFromOverride;
		}

		public static bool BadAccountsInJournal(int lngJournal, string strDBPath = "")
		{
			bool BadAccountsInJournal = false;
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			int lngTempValidAcctCheck = 0;
			BadAccountsInJournal = false;
			if (strDBPath != string.Empty)
			{
				rsMaster.DefaultGroup = strDBPath;
				rsJournalInfo.DefaultGroup = strDBPath;
			}
			rsMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
			if (rsMaster.EndOfFile() != true && rsMaster.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsMaster.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsMaster.Get_Fields("Type")) == "AC")
				{
					rsJournalInfo.OpenRecordset("SELECT Account FROM APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID WHERE APJournal.JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
				}
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsMaster.Get_Fields("Type") == "EN")
				{
					rsJournalInfo.OpenRecordset("SELECT Account FROM EncumbranceDetail INNER JOIN Encumbrances ON EncumbranceDetail.EncumbranceID = Encumbrances.ID WHERE Encumbrances.JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
				}
				else
				{
					rsJournalInfo.OpenRecordset("SELECT Account FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
				}
				if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
				{
					lngTempValidAcctCheck = modValidateAccount.Statics.ValidAcctCheck;
					modValidateAccount.Statics.ValidAcctCheck = 3;
					do
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						BadAccountsInJournal = !modValidateAccount.AccountValidate(rsJournalInfo.Get_Fields("Account"));
						if (BadAccountsInJournal == true)
						{
							break;
						}
						rsJournalInfo.MoveNext();
					}
					while (rsJournalInfo.EndOfFile() != true);
					modValidateAccount.Statics.ValidAcctCheck = lngTempValidAcctCheck;
				}
			}
			return BadAccountsInJournal;
		}

		public static bool CheckOBF_6(int lngJournal, string strJournalType, string strDBPath = "")
		{
			return CheckOBF(lngJournal, strJournalType, strDBPath);
		}

		public static bool CheckOBF(int lngJournal, string strJournalType, string strDBPath = "")
		{
			bool CheckOBF = false;
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			FundType[] RegularAccountInfo = null;
			FundType[] ControlAccountInfo = null;
			FundType[] RegularFundInfo = null;
			FundType[] ControlFundInfo = null;
			int counter = 0;
			if (strDBPath != string.Empty)
			{
				rsJournalInfo.DefaultGroup = strDBPath;
			}
			CheckOBF = true;
			if (strJournalType == "GJ" || strJournalType == "CW")
			{
				rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
				if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
				{
					RegularAccountInfo = new FundType[rsJournalInfo.RecordCount() - 1 + 1];
					counter = 0;
					do
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						RegularAccountInfo[counter].Account = FCConvert.ToString(rsJournalInfo.Get_Fields("Account"));
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						RegularAccountInfo[counter].AcctType = FCConvert.ToString(rsJournalInfo.Get_Fields("Type"));
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						RegularAccountInfo[counter].Amount = FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount"));
						RegularAccountInfo[counter].Description = FCConvert.ToString(rsJournalInfo.Get_Fields_String("Description"));
						rsJournalInfo.MoveNext();
						counter += 1;
					}
					while (rsJournalInfo.EndOfFile() != true);
					RegularFundInfo = CalcFundCash(ref RegularAccountInfo);
				}
				else
				{
					RegularAccountInfo = new FundType[0 + 1];
				}
				if (FCConvert.ToString(GetBDVariable("Due")) != "Y" || strJournalType == "CW" || strJournalType == "PY")
				{
					for (counter = 1; counter <= 99; counter++)
					{
						if (RegularFundInfo[counter].Amount != 0)
						{
							CheckOBF = false;
							return CheckOBF;
						}
					}
				}
				else
				{
					FundType[] CashFundFundInfo = null;
					CashFundFundInfo = CalcDueToFromFundCash(ref RegularFundInfo, ref strJournalType);
					for (counter = 1; counter <= 99; counter++)
					{
						if (CashFundFundInfo[counter].Amount != 0)
						{
							CheckOBF = false;
							return CheckOBF;
						}
					}
				}
			}
			return CheckOBF;
		}

		public static void MoveWaterEntries_6(int lngJournal, short intPeriod, string strDBPath = "")
		{
			MoveWaterEntries(ref lngJournal, ref intPeriod, strDBPath);
		}

		public static void MoveWaterEntries(ref int lngJournal, ref short intPeriod, string strDBPath = "")
		{
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsWaterMaster = new clsDRWrapper();
			clsDRWrapper rsJournal = new clsDRWrapper();
			clsDRWrapper rsWaterJournal = new clsDRWrapper();
			string strArchivePath;
			int intWaterJournal;
            //FC:FINAL:AM:#4328 - set the database, not the group
            cSettingsController setCont = new cSettingsController();
            strArchivePath = setCont.GetSettingValue("WATERDBLOCATION", "", "", "", "");
			rsWaterMaster.MegaGroup = strArchivePath;
			rsWaterJournal.MegaGroup = strArchivePath;
			if (strDBPath != string.Empty)
			{
				rsMaster.DefaultGroup = strDBPath;
				rsJournal.DefaultGroup = strDBPath;
			}

			rsMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
			rsJournal.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
			rsWaterJournal.OpenRecordset("SELECT * FROM JournalEntries", "TWBD0000.vb1");
			intWaterJournal = 0;
			if (rsJournal.EndOfFile() != true && rsJournal.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (GetFundFromAccount_2(rsJournal.Get_Fields("Account")) == 9)
					{
						if (intWaterJournal == 0)
						{
							if (LockJournal() == false)
							{
								MessageBox.Show("User " + Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								//Application.DoEvents();
								frmWait.InstancePtr.Unload();
								return;
							}
							rsWaterMaster.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC", "TWBD0000.vb1");
							if (rsWaterMaster.EndOfFile() != true && rsWaterMaster.BeginningOfFile() != true)
							{
								rsWaterMaster.MoveLast();
								rsWaterMaster.MoveFirst();
								// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								intWaterJournal = FCConvert.ToInt32(rsWaterMaster.Get_Fields("JournalNumber")) + 1;
							}
							else
							{
								intWaterJournal = 1;
							}
							rsWaterMaster.AddNew();
							rsWaterMaster.Set_Fields("JournalNumber", intWaterJournal);
							rsWaterMaster.Set_Fields("Status", "E");
							rsWaterMaster.Set_Fields("Description", rsMaster.Get_Fields_String("Description"));
							// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
							rsWaterMaster.Set_Fields("Type", rsMaster.Get_Fields("Type"));
							rsWaterMaster.Set_Fields("Period", intPeriod);
							rsWaterMaster.Update();
							UnlockJournal();
						}
						rsWaterJournal.AddNew();
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						rsWaterJournal.Set_Fields("Type", rsJournal.Get_Fields("Type"));
						rsWaterJournal.Set_Fields("JournalEntriesDate", rsJournal.Get_Fields_DateTime("JournalEntriesDate"));
						rsWaterJournal.Set_Fields("Description", rsJournal.Get_Fields_String("Description"));
						rsWaterJournal.Set_Fields("JournalNumber", intWaterJournal);
						rsWaterJournal.Set_Fields("Period", intPeriod);
						rsWaterJournal.Set_Fields("Project", rsJournal.Get_Fields_String("Project"));
						if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "NORWAY")
						{
							// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsJournal.Get_Fields("account")), 1) == "R")
							{
								// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
								rsWaterJournal.Set_Fields("account", "R " + GetDepartment_2(rsJournal.Get_Fields("account")) + "-4" + GetRevenue_2(rsJournal.Get_Fields("account")));
							}
							else
							{
								// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
								rsWaterJournal.Set_Fields("account", rsJournal.Get_Fields("account"));
							}
						}
						else
						{
							// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
							rsWaterJournal.Set_Fields("account", rsJournal.Get_Fields("account"));
						}
						rsWaterJournal.Set_Fields("RCB", rsJournal.Get_Fields_String("RCB"));
						// TODO: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
						rsWaterJournal.Set_Fields("CarryForward", rsJournal.Get_Fields("CarryForward"));
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						rsWaterJournal.Set_Fields("Amount", rsJournal.Get_Fields("Amount"));
						rsWaterJournal.Set_Fields("Status", rsJournal.Get_Fields_String("Status"));
						rsWaterJournal.Update();
						rsJournal.Delete();
                        rsJournal.Update();
                    }
                    else
                    {
                        rsJournal.MoveNext();
					}
                }
				while (rsJournal.EndOfFile() != true);
				if (intWaterJournal != 0)
				{
					MessageBox.Show("Journal " + Strings.Format(intWaterJournal, "0000") + " has been created in the water database to hold all the water entries found in journal " + Strings.Format(lngJournal, "0000"), "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		public static int CreateAPControl_60(int lngJournal, short intPeriod, string strControlDate, string strType, string strAPCheckDate = "", string strDBPath = "")
		{
			return CreateAPControl(ref lngJournal, ref intPeriod, ref strControlDate, ref strType, strAPCheckDate, strDBPath);
		}

		public static int CreateAPControl(ref int lngJournal, ref short intPeriod, ref string strControlDate, ref string strType, string strAPCheckDate = "", string strDBPath = "")
		{
			int CreateAPControl = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsCheckDate = new clsDRWrapper();
			clsDRWrapper rsWarrantInfo = new clsDRWrapper();
			if (strDBPath != string.Empty)
			{
				rsTemp.DefaultGroup = strDBPath;
				rsCheckDate.DefaultGroup = strDBPath;
				rsWarrantInfo.DefaultGroup = strDBPath;
			}
			if (strType == "AP")
			{
				rsTemp.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Description = 'Control Entries'", "TWBD0000.vb1");
				if (strAPCheckDate != "")
				{
					rsWarrantInfo.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
				}
				else
				{
					rsWarrantInfo.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
				}
			}
			else
			{
				rsTemp.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Description = 'Control Entries'", "TWBD0000.vb1");
				rsWarrantInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
			}
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				CreateAPControl = FCConvert.ToInt32(rsTemp.Get_Fields("Number"));
			}
			else
			{
				if (strType == "AP")
				{
					if (strAPCheckDate != "")
					{
						rsCheckDate.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
					}
					else
					{
						rsCheckDate.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
					}
				}
				else
				{
					rsCheckDate.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
				}
				rsTemp.AddNew();
				rsTemp.Set_Fields("JournalNumber", lngJournal);
				rsTemp.Set_Fields("Period", intPeriod);
				rsTemp.Set_Fields("Description", "Control Entries");
				rsTemp.Set_Fields("Status", "P");
				rsTemp.Set_Fields("CheckDate", strControlDate);
				if (rsWarrantInfo.EndOfFile() != true && rsWarrantInfo.BeginningOfFile() != true)
				{
					// TODO: Check the table for the column [Warrant] and replace with corresponding Get_Field method
					rsTemp.Set_Fields("Warrant", rsWarrantInfo.Get_Fields("Warrant"));
				}
				if (strType == "AP")
				{
					if (!rsCheckDate.IsFieldNull("CheckDate"))
					{
						rsTemp.Set_Fields("Payable", rsCheckDate.Get_Fields_DateTime("CheckDate"));
					}
				}
				else
				{
					rsTemp.Set_Fields("Payable", DateAndTime.DateValue(strControlDate));
				}
				rsTemp.Set_Fields("PostedDate", DateTime.Today);
				rsTemp.Update();
				CreateAPControl = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
			}
			return CreateAPControl;
		}

		public static int CreateEncControl_6(int lngJournal, short intPeriod, string strControlDate, string strDBPath = "")
		{
			return CreateEncControl(ref lngJournal, ref intPeriod, ref strControlDate, strDBPath);
		}

		public static int CreateEncControl(ref int lngJournal, ref short intPeriod, ref string strControlDate, string strDBPath = "")
		{
			int CreateEncControl = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (strDBPath != string.Empty)
			{
				rsTemp.DefaultGroup = strDBPath;
			}
			rsTemp.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND Description = 'Control Entries'", "TWBD0000.vb1");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// TODO: Field [Number] not found!! (maybe it is an alias?)
				CreateEncControl = FCConvert.ToInt32(rsTemp.Get_Fields("Number"));
			}
			else
			{
				rsTemp.AddNew();
				rsTemp.Set_Fields("JournalNumber", lngJournal);
				rsTemp.Set_Fields("Description", "Control Entries");
				rsTemp.Set_Fields("Status", "P");
				rsTemp.Set_Fields("EncumbrancesDate", strControlDate);
				rsTemp.Set_Fields("Period", intPeriod);
				rsTemp.Update(true);
				CreateEncControl = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
			}
			return CreateEncControl;
		}

		public static bool CloseExp_60(int lngJournal, short intPeriod, string ControlDate, string strType, int lngCloseoutID, string strAPCheckDate = "", string strDBPath = "")
		{
			return CloseExp(ref lngJournal, ref intPeriod, ref ControlDate, ref strType, ref lngCloseoutID, strAPCheckDate, strDBPath);
		}

		public static bool CloseExp(ref int lngJournal, ref short intPeriod, ref string ControlDate, ref string strType, ref int lngCloseoutID, string strAPCheckDate = "", string strDBPath = "")
		{
			bool CloseExp = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			clsDRWrapper rsControlEntry = new clsDRWrapper();
			string strSql = "";
			clsDRWrapper rsGeneralInfo = new clsDRWrapper();
			string strJournalSQL = "";
			CloseExp = true;
			if (strDBPath != string.Empty)
			{
				rsTemp.DefaultGroup = strDBPath;
				rsFundInfo.DefaultGroup = strDBPath;
				rsControlEntry.DefaultGroup = strDBPath;
				rsGeneralInfo.DefaultGroup = strDBPath;
			}
			if (Conversion.Val(Statics.strExpCtrAccount) == 0)
			{
				MessageBox.Show("You must set up a Town Expense Control Account before you may continue.", "No Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CloseExp = false;
				return CloseExp;
			}
			rsControlEntry.OmitNullsOnInsert = true;
			if (strType == "EN")
			{
				rsControlEntry.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = 0", "TWBD0000.vb1");
				rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(lngCloseoutID), "TWBD0000.vb1");
			}
			else if (strType == "AP" || strType == "AC")
			{
				rsControlEntry.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0", "TWBD0000.vb1");
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(lngCloseoutID), "TWBD0000.vb1");
			}
			else
			{
				rsControlEntry.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0", "TWBD0000.vb1");
			}
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles", "TWBD0000.vb1");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
					if (strType == "EN")
					{
						strJournalSQL = "SELECT ID FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'E' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("EncumbranceID", lngCloseoutID);
							rsControlEntry.Set_Fields("Description", "Expense CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strExpCtrAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strExpCtrAccount);
							}
							rsControlEntry.Set_Fields("Project", "CTRL");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", rsTemp.Get_Fields("JournalTotal"));
							rsControlEntry.Update();
						}
					}
					else if (strType == "AP" || strType == "AC")
					{
						if (strType == "AP")
						{
							if (strAPCheckDate != "")
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
							else
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
						}
						else
						{
							strJournalSQL = "SELECT ID FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						}
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal, SUM(Encumbrance) as EncumbranceTotal FROM APJournalDetail WHERE left(Account, 1) = 'E' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
						// TODO: Field [EncumbranceTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal")) - Conversion.Val(rsTemp.Get_Fields("EncumbranceTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
							rsControlEntry.Set_Fields("Description", "Expense CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strExpCtrAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strExpCtrAccount);
							}
							rsControlEntry.Set_Fields("Project", "CTRL");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
							// TODO: Field [EncumbranceTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal")) - Conversion.Val(rsTemp.Get_Fields("EncumbranceTotal")));
							rsControlEntry.Set_Fields("RCB", "L");
							rsControlEntry.Update();
						}
					}
					else
					{
						if (strType == "PY")
						{
							rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'E' AND RCB <> 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						}
						else
						{
							rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						}
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("JournalNumber", lngJournal);
							rsControlEntry.Set_Fields("Description", "Expense CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strExpCtrAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strExpCtrAccount);
							}
							rsControlEntry.Set_Fields("RCB", "L");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", rsTemp.Get_Fields("JournalTotal"));
							rsControlEntry.Set_Fields("Status", "P");
							rsControlEntry.Set_Fields("PostedDate", DateTime.Today);
							rsControlEntry.Set_Fields("Period", intPeriod);
							rsControlEntry.Set_Fields("JournalEntriesDate", DateAndTime.DateValue(ControlDate));
							if (strType == "CW")
							{
								rsControlEntry.Set_Fields("Type", "W");
							}
							else if (strType == "CR")
							{
								rsControlEntry.Set_Fields("Type", "C");
							}
							else if (strType == "CD")
							{
								rsControlEntry.Set_Fields("Type", "D");
							}
							else if (strType == "PY")
							{
								rsControlEntry.Set_Fields("Type", "P");
							}
							else
							{
								rsControlEntry.Set_Fields("Type", "G");
							}
							rsControlEntry.Update();
						}
					}
					rsFundInfo.MoveNext();
				}
				while (rsFundInfo.EndOfFile() != true);
			}
			return CloseExp;
		}

		public static bool CloseRev_60(int lngJournal, short intPeriod, string ControlDate, string strType, int lngCloseoutID, string strAPCheckDate = "", string strDBPath = "")
		{
			return CloseRev(ref lngJournal, ref intPeriod, ref ControlDate, ref strType, ref lngCloseoutID, strAPCheckDate, strDBPath);
		}

		public static bool CloseRev(ref int lngJournal, ref short intPeriod, ref string ControlDate, ref string strType, ref int lngCloseoutID, string strAPCheckDate = "", string strDBPath = "")
		{
			bool CloseRev = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			clsDRWrapper rsControlEntry = new clsDRWrapper();
			string strSql = "";
			clsDRWrapper rsGeneralInfo = new clsDRWrapper();
			string strJournalSQL = "";
			CloseRev = true;
			if (strDBPath != string.Empty)
			{
				rsTemp.DefaultGroup = strDBPath;
				rsFundInfo.DefaultGroup = strDBPath;
				rsControlEntry.DefaultGroup = strDBPath;
				rsGeneralInfo.DefaultGroup = strDBPath;
			}
			if (Conversion.Val(Statics.strRevCtrAccount) == 0)
			{
				MessageBox.Show("You must set up a Town Revenue Control Account before you may continue.", "No Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CloseRev = false;
				return CloseRev;
			}
			rsControlEntry.OmitNullsOnInsert = true;
			if (strType == "EN")
			{
				rsControlEntry.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = 0", "TWBD0000.vb1");
				rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(lngCloseoutID), "TWBD0000.vb1");
			}
			else if (strType == "AP" || strType == "AC")
			{
				rsControlEntry.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0", "TWBD0000.vb1");
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(lngCloseoutID), "TWBD0000.vb1");
			}
			else
			{
				rsControlEntry.OpenRecordset("SELECT * FROM JournalEntries", "TWBD0000.vb1");
			}
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles", "TWBD0000.vb1");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
					if (strType == "EN")
					{
						strJournalSQL = "SELECT ID FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'R' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("EncumbranceID", lngCloseoutID);
							rsControlEntry.Set_Fields("Description", "Revenue CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strRevCtrAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strRevCtrAccount);
							}
							rsControlEntry.Set_Fields("Project", "CTRL");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", rsTemp.Get_Fields("JournalTotal"));
							rsControlEntry.Update();
						}
					}
					else if (strType == "AP" || strType == "AC")
					{
						if (strType == "AP")
						{
							if (strAPCheckDate != "")
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
							else
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
						}
						else
						{
							strJournalSQL = "SELECT ID FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						}
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'R' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
							rsControlEntry.Set_Fields("Description", "Revenue CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strRevCtrAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strRevCtrAccount);
							}
							rsControlEntry.Set_Fields("RCB", "L");
							rsControlEntry.Set_Fields("Project", "CTRL");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", FCConvert.ToDecimal(Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))));
							rsControlEntry.Update();
						}
					}
					else
					{
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'R' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("JournalNumber", lngJournal);
							rsControlEntry.Set_Fields("Description", "Revenue CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strRevCtrAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strRevCtrAccount);
							}
							rsControlEntry.Set_Fields("RCB", "L");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", rsTemp.Get_Fields("JournalTotal"));
							rsControlEntry.Set_Fields("Status", "P");
							rsControlEntry.Set_Fields("PostedDate", DateTime.Today);
							rsControlEntry.Set_Fields("Period", intPeriod);
							rsControlEntry.Set_Fields("JournalEntriesDate", DateAndTime.DateValue(ControlDate));
							if (strType == "CW")
							{
								rsControlEntry.Set_Fields("Type", "W");
							}
							else if (strType == "CR")
							{
								rsControlEntry.Set_Fields("Type", "C");
							}
							else if (strType == "CD")
							{
								rsControlEntry.Set_Fields("Type", "D");
							}
							else if (strType == "PY")
							{
								rsControlEntry.Set_Fields("Type", "P");
							}
							else
							{
								rsControlEntry.Set_Fields("Type", "G");
							}
							rsControlEntry.Update();
						}
					}
					rsFundInfo.MoveNext();
				}
				while (rsFundInfo.EndOfFile() != true);
			}
			return CloseRev;
		}

		public static bool CloseCash_60(int lngJournal, short intPeriod, string ControlDate, string strType, int lngCloseoutID, bool blnCreditMemo, string strAPCheckDate = "", string strDBPath = "")
		{
			return CloseCash(ref lngJournal, ref intPeriod, ref ControlDate, ref strType, ref lngCloseoutID, ref blnCreditMemo, strAPCheckDate, strDBPath);
		}

		public static bool CloseCash(ref int lngJournal, ref short intPeriod, ref string ControlDate, ref string strType, ref int lngCloseoutID, ref bool blnCreditMemo, string strAPCheckDate = "", string strDBPath = "")
		{
			bool CloseCash = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsTemp2 = new clsDRWrapper();
			clsDRWrapper rsTemp3 = new clsDRWrapper();
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			string strAccount = "";
			clsDRWrapper rsControlEntry = new clsDRWrapper();
			string strSql = "";
			clsDRWrapper rsGeneralInfo = new clsDRWrapper();
			string strJournalSQL = "";
			clsDRWrapper rsMasterInfo = new clsDRWrapper();
			clsDRWrapper rsJournalMaster = new clsDRWrapper();
			string strCredMemRecAcct = "";
			CloseCash = true;
			rsControlEntry.OmitNullsOnInsert = true;
			if (strDBPath != string.Empty)
			{
				rsTemp.DefaultGroup = strDBPath;
				rsTemp2.DefaultGroup = strDBPath;
				rsTemp3.DefaultGroup = strDBPath;
				rsFundInfo.DefaultGroup = strDBPath;
				rsControlEntry.DefaultGroup = strDBPath;
				rsGeneralInfo.DefaultGroup = strDBPath;
				rsMasterInfo.DefaultGroup = strDBPath;
				rsJournalMaster.DefaultGroup = strDBPath;
			}
			if (blnCreditMemo)
			{
				// Get the Credit Memo Receivable Account
				if (Conversion.Val(Statics.strCredMemRecAccount) == 0)
				{
					MessageBox.Show("You must set up a Town Credit Memo Receivable Account before you may continue.", "No Town Credit Memo Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseCash = false;
					return CloseCash;
				}
			}
			else
			{
				// Get the Credit Memo Receivable Account
				if (Conversion.Val(Statics.strCredMemRecAccount) == 0)
				{
					MessageBox.Show("You must set up a Town Credit Memo Receivable Account before you may continue.", "No Town Credit Memo Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CloseCash = false;
					return CloseCash;
				}
				if (Strings.Trim(strType) == "CR")
				{
					if (Conversion.Val(Statics.strCRCashAccount) == 0)
					{
						MessageBox.Show("You must set up a Town Miscellaneous Cash Account before you may continue.", "No Town Accounts Payable Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CloseCash = false;
						return CloseCash;
					}
				}
				else
				{
					if (Conversion.Val(Statics.strAPCashAccount) == 0)
					{
						MessageBox.Show("You must set up a Town Accounts Payable Cash Account before you may continue.", "No Town Accounts Payable Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CloseCash = false;
						return CloseCash;
					}
				}
			}
			if (strType == "EN")
			{
				return CloseCash;
			}
			else if (strType == "AP" || strType == "AC")
			{
				rsControlEntry.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0", "TWBD0000.vb1");
			}
			else if (Strings.Trim(strType) == "CR" || strType == "CD")
			{
				rsControlEntry.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0", "TWBD0000.vb1");
			}
			else
			{
				return CloseCash;
			}
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles", "TWBD0000.vb1");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
					if (strType == "AP" || strType == "AC")
					{
						if (strType == "AP")
						{
							if (strAPCheckDate != "")
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) = '' AND IsNull(UseAlternateCash, 0) = 0 AND (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
							else
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) = '' AND IsNull(UseAlternateCash, 0) = 0 AND (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
						}
						else
						{
							strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) = '' AND IsNull(UseAlternateCash, 0) = 0 AND Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						}
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'R' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") IN (" + strSql + ") AND IsNull(Project, '') <> 'CTRL'", "TWBD0000.vb1");
						rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'E' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") IN (" + strSql + ") AND IsNull(Project, '') <> 'CTRL'", "TWBD0000.vb1");
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "' AND IsNull(Project, '') <> 'CTRL'", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
						if (((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
							if (strType == "AP")
							{
								rsControlEntry.Set_Fields("Description", "Cash A/P");
							}
							else
							{
								if (blnCreditMemo)
								{
									rsControlEntry.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yy") + " CM");
								}
								else
								{
									rsControlEntry.Set_Fields("Description", "Void A/P");
								}
							}
							if (!modAccountTitle.Statics.YearFlag)
							{
								if (blnCreditMemo)
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strCredMemRecAccount + "-00");
								}
								else
								{
									if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
									{
										// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
										rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount + "-00");
									}
									else
									{
										// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
										rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEOYAPAccount + "-00");
										// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
										// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount + "-00", FCConvert.ToDecimal(-1 * ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal"))))));
									}
								}
							}
							else
							{
								if (blnCreditMemo)
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strCredMemRecAccount);
								}
								else
								{
									if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
									{
										// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
										rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount);
									}
									else
									{
										// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
										rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEOYAPAccount);
										// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
										// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount, FCConvert.ToDecimal(-1 * ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal"))))));
									}
								}
							}
							rsControlEntry.Set_Fields("Project", "CTRL");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", -1 * ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))));
							rsControlEntry.Set_Fields("RCB", "L");
							rsControlEntry.Update();
						}
					}
					else
					{
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'R' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ") AND RCB <> 'L'", "TWBD0000.vb1");
						rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ") AND RCB <> 'L'", "TWBD0000.vb1");
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "' AND RCB <> 'L'", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) != 0)
						{
							rsMasterInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("JournalNumber", lngJournal);
							if (rsMasterInfo.EndOfFile() != true && rsMasterInfo.BeginningOfFile() != true)
							{
								if (blnCreditMemo)
								{
									rsControlEntry.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yy") + " CM");
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description"))).Length > 18)
									{
										rsControlEntry.Set_Fields("Description", Strings.Left(Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description"))), 18) + " - Cash");
									}
									else
									{
										rsControlEntry.Set_Fields("Description", Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description"))) + " - Cash");
									}
								}
							}
							else
							{
								if (blnCreditMemo)
								{
									rsControlEntry.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yy") + " CM");
								}
								else if (Strings.Trim(strType) == "CR")
								{
									rsControlEntry.Set_Fields("Description", "Cash C/R");
								}
								else
								{
									rsControlEntry.Set_Fields("Description", "Cash A/P");
								}
							}
							if (!modAccountTitle.Statics.YearFlag)
							{
								if (blnCreditMemo)
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strCredMemRecAccount + "-00");
								}
								else if (Strings.Trim(strType) == "CR")
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strCRCashAccount + "-00");
								}
								else
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount + "-00");
								}
							}
							else
							{
								if (blnCreditMemo)
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strCredMemRecAccount);
								}
								else if (Strings.Trim(strType) == "CR")
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strCRCashAccount);
								}
								else
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount);
								}
							}
							rsControlEntry.Set_Fields("RCB", "L");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", -1 * (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal"))));
							rsControlEntry.Set_Fields("Status", "P");
							rsControlEntry.Set_Fields("PostedDate", DateTime.Today);
							rsControlEntry.Set_Fields("Period", intPeriod);
							rsControlEntry.Set_Fields("JournalEntriesDate", DateAndTime.DateValue(ControlDate));
							if (strType == "CR")
							{
								rsControlEntry.Set_Fields("Type", "C");
							}
							else if (strType == "CD")
							{
								rsControlEntry.Set_Fields("Type", "D");
							}
							rsControlEntry.Update();
						}
					}
					rsFundInfo.MoveNext();
				}
				while (rsFundInfo.EndOfFile() != true);
			}
			// Town Override Cash entries
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles", "TWBD0000.vb1");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
					if (strType == "AP" || strType == "AC")
					{
						if (strType == "AP")
						{
							if (strAPCheckDate != "")
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) <> '' AND IsNull(UseAlternateCash, 0) = 0 AND (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
							else
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) <> '' AND IsNull(UseAlternateCash, 0) = 0 AND (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
						}
						else
						{
							strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) <> '' AND IsNull(UseAlternateCash, 0) = 0 AND Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						}
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp.OpenRecordset("SELECT COUNT(EntryCount) as TotalEntryCount, SUM(JournalTotal) as TotalJournalTotal, SUM(DiscountTotal) as TotalDiscountTotal, TownOverride FROM (SELECT * FROM (SELECT COUNT(APJournalDetail.Amount) as EntryCount, SUM(APJournalDetail.Amount) as JournalTotal, SUM(Discount) as DiscountTotal, TownOverride FROM APJournalDetail INNER JOIN APJournal ON APJournal.ID = APJournalDetail.APJournalID WHERE left(Account, 1) = 'R' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") IN (" + strSql + ") AND IsNull(Project, '') <> 'CTRL' GROUP BY TownOverride) as TownOverride1 " + "UNION ALL SELECT * FROM (SELECT COUNT(APJournalDetail.Amount) as EntryCount, SUM(APJournalDetail.Amount) as JournalTotal, SUM(Discount) as DiscountTotal, TownOverride FROM APJournalDetail INNER JOIN APJournal ON APJournal.ID = APJournalDetail.APJournalID WHERE left(Account, 1) = 'E' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") IN (" + strSql + ") AND IsNull(Project, '') <> 'CTRL' GROUP BY TownOverride) as TownOverride2 " + "UNION ALL SELECT * FROM (SELECT COUNT(APJournalDetail.Amount) as EntryCount, SUM(APJournalDetail.Amount) as JournalTotal, SUM(Discount) as DiscountTotal, TownOverride FROM APJournalDetail INNER JOIN APJournal ON APJournal.ID = APJournalDetail.APJournalID WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "' AND IsNull(Project, '') <> 'CTRL' GROUP BY TownOverride) as TownOverride3) as temp GROUP BY TownOverride");
						if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
						{
							do
							{
								// TODO: Field [TotalJournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [TotalDiscountTotal] not found!! (maybe it is an alias?)
								if ((Conversion.Val(rsTemp.Get_Fields("TotalJournalTotal")) - Conversion.Val(rsTemp.Get_Fields("TotalDiscountTotal"))) != 0)
								{
									rsControlEntry.AddNew();
									rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
									if (strType == "AP")
									{
										rsControlEntry.Set_Fields("Description", "Override Cash A/P");
									}
									else
									{
										if (blnCreditMemo)
										{
											rsControlEntry.Set_Fields("Description", "Override " + Strings.Format(DateTime.Today, "MM/dd/yy") + " CM");
										}
										else
										{
											rsControlEntry.Set_Fields("Description", "Override Void A/P");
										}
									}
									if (!modAccountTitle.Statics.YearFlag)
									{
										if (blnCreditMemo)
										{
											// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
											rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride") + "-00");
										}
										else
										{
											if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride") + "-00");
											}
											else
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEOYAPAccount + "-00");
												// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												// TODO: Field [TotalJournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [TotalDiscountTotal] not found!! (maybe it is an alias?)
												AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride") + "-00", FCConvert.ToDecimal(-1 * (Conversion.Val(rsTemp.Get_Fields("TotalJournalTotal")) - Conversion.Val(rsTemp.Get_Fields("TotalDiscountTotal")))));
											}
										}
									}
									else
									{
										if (blnCreditMemo)
										{
											// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
											rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride"));
										}
										else
										{
											if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride"));
											}
											else
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEOYAPAccount);
												// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												// TODO: Field [TotalJournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [TotalDiscountTotal] not found!! (maybe it is an alias?)
												AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride"), FCConvert.ToDecimal(-1 * (Conversion.Val(rsTemp.Get_Fields("TotalJournalTotal")) - Conversion.Val(rsTemp.Get_Fields("TotalDiscountTotal")))));
											}
										}
									}
									rsControlEntry.Set_Fields("Project", "CTRL");
									// TODO: Field [TotalJournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [TotalDiscountTotal] not found!! (maybe it is an alias?)
									rsControlEntry.Set_Fields("Amount", -1 * (Conversion.Val(rsTemp.Get_Fields("TotalJournalTotal")) - Conversion.Val(rsTemp.Get_Fields("TotalDiscountTotal"))));
									rsControlEntry.Set_Fields("RCB", "L");
									rsControlEntry.Update();
								}
								rsTemp.MoveNext();
							}
							while (rsTemp.EndOfFile() != true);
						}
					}
					rsFundInfo.MoveNext();
				}
				while (rsFundInfo.EndOfFile() != true);
			}
			// AP A;ternate Cash Entry
			if (strType == "AP")
			{
				if (strAPCheckDate != "")
				{
					rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE IsNull(UseAlternateCash, 0) = 1 AND (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
				}
				else
				{
					rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE IsNull(UseAlternateCash, 0) = 1 AND (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
				}
			}
			else
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE IsNull(UseAlternateCash, 0) = 1 AND Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
			}
			if (rsGeneralInfo.EndOfFile() != true && rsGeneralInfo.BeginningOfFile() != true)
			{
				do
				{
					rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE APJournalID = " + rsGeneralInfo.Get_Fields_Int32("ID") + " AND IsNull(Project, '') <> 'CTRL'", "TWBD0000.vb1");
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal")) != 0)
					{
						rsControlEntry.AddNew();
						rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
						if (strType == "AP")
						{
							rsControlEntry.Set_Fields("Description", "Alt Cash A/P");
						}
						else
						{
							rsControlEntry.Set_Fields("Description", "Void Alt Cash A/P");
						}
						if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
						{
							rsControlEntry.Set_Fields("Account", rsGeneralInfo.Get_Fields_String("AlternateCashAccount"));
						}
						else
						{
							if (Strings.Left(FCConvert.ToString(rsGeneralInfo.Get_Fields_String("AlternateCashAccount")), 1) == "G")
							{
								if (modAccountTitle.Statics.YearFlag)
								{
									rsControlEntry.Set_Fields("Account", "G " + GetFund_2(rsGeneralInfo.Get_Fields_String("AlternateCashAccount")) + "-" + Statics.strEOYAPAccount);
								}
								else
								{
									rsControlEntry.Set_Fields("Account", "G " + GetFund_2(rsGeneralInfo.Get_Fields_String("AlternateCashAccount")) + "-" + Statics.strEOYAPAccount + "-00");
								}
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), rsGeneralInfo.Get_Fields_String("AlternateCashAccount"), FCConvert.ToDecimal(-1 * (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal")))));
							}
							else
							{
							}
						}
						rsControlEntry.Set_Fields("Project", "CTRL");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
						rsControlEntry.Set_Fields("Amount", -1 * (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))));
						rsControlEntry.Set_Fields("RCB", "L");
						rsControlEntry.Update();
					}
					rsGeneralInfo.MoveNext();
				}
				while (rsGeneralInfo.EndOfFile() != true);
			}
			return CloseCash;
		}

		public static bool CloseEnc_60(int lngJournal, short intPeriod, string ControlDate, string strType, int lngCloseoutID, string strAPCheckDate = "", string strDBPath = "")
		{
			return CloseEnc(ref lngJournal, ref intPeriod, ref ControlDate, ref strType, ref lngCloseoutID, strAPCheckDate, strDBPath);
		}

		public static bool CloseEnc(ref int lngJournal, ref short intPeriod, ref string ControlDate, ref string strType, ref int lngCloseoutID, string strAPCheckDate = "", string strDBPath = "")
		{
			bool CloseEnc = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsTemp2 = new clsDRWrapper();
			clsDRWrapper rsTemp3 = new clsDRWrapper();
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			string strAccount = "";
			clsDRWrapper rsControlEntry = new clsDRWrapper();
			string strSql = "";
			clsDRWrapper rsGeneralInfo = new clsDRWrapper();
			string strJournalSQL = "";
			clsDRWrapper rsPrevYearTest = new clsDRWrapper();
			rsControlEntry.OmitNullsOnInsert = true;
			CloseEnc = true;
			if (strDBPath != string.Empty)
			{
				rsTemp.DefaultGroup = strDBPath;
				rsTemp2.DefaultGroup = strDBPath;
				rsTemp3.DefaultGroup = strDBPath;
				rsFundInfo.DefaultGroup = strDBPath;
				rsControlEntry.DefaultGroup = strDBPath;
				rsGeneralInfo.DefaultGroup = strDBPath;
				rsPrevYearTest.DefaultGroup = strDBPath;
			}
			if (strType == "EN")
			{
				rsControlEntry.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = 0", "TWBD0000.vb1");
				rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(lngCloseoutID), "TWBD0000.vb1");
			}
			else if (strType == "AP" || strType == "AC")
			{
				rsControlEntry.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0", "TWBD0000.vb1");
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(lngCloseoutID), "TWBD0000.vb1");
			}
			else
			{
				rsControlEntry.OpenRecordset("SELECT * FROM JournalEntries", "TWBD0000.vb1");
			}
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles", "TWBD0000.vb1");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
					if (strType == "EN")
					{
						strJournalSQL = "SELECT ID FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'R' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'E' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("EncumbranceID", lngCloseoutID);
							rsControlEntry.Set_Fields("Description", "Encumbrance CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEncOffAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEncOffAccount);
							}
							rsControlEntry.Set_Fields("Project", "CTRL");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal"))) * -1);
							rsControlEntry.Update();
						}
					}
					else if (strType == "AP" || strType == "AC")
					{
						if (strType == "AP")
						{
							if (strAPCheckDate != "")
							{
								strJournalSQL = "SELECT APJournal.ID FROM APJournal INNER JOIN Encumbrances ON APJournal.EncumbranceRecord = Encumbrances.ID WHERE ISNULL(PastYearEnc, 0) <> 1 AND (APJournal.Status = 'W' OR APJournal.Status = 'X') AND APJournal.CheckDate = '" + strAPCheckDate + "' AND APJournal.JournalNumber = " + FCConvert.ToString(lngJournal) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod);
							}
							else
							{
								strJournalSQL = "SELECT APJournal.ID FROM APJournal INNER JOIN Encumbrances ON APJournal.EncumbranceRecord = Encumbrances.ID WHERE ISNULL(PastYearEnc, 0) <> 1 AND (APJournal.Status = 'W' OR APJournal.Status = 'X') AND APJournal.JournalNumber = " + FCConvert.ToString(lngJournal) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod);
							}
						}
						else
						{
							strJournalSQL = "SELECT APJournal.ID FROM APJournal INNER JOIN Encumbrances ON APJournal.EncumbranceRecord = Encumbrances.ID WHERE ISNULL(PastYearEnc, 0) <> 1 AND APJournal.Status = 'E' AND APJournal.JournalNumber = " + FCConvert.ToString(lngJournal) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod);
						}
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Encumbrance) as JournalTotal FROM APJournalDetail WHERE left(Account, 1) = 'R' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Encumbrance) as JournalTotal FROM APJournalDetail WHERE left(Account, 1) = 'E' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Encumbrance) as JournalTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
							rsControlEntry.Set_Fields("Description", "Encumbrance CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEncOffAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEncOffAccount);
							}
							rsControlEntry.Set_Fields("Project", "CTRL");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal")));
							rsControlEntry.Set_Fields("RCB", "L");
							rsControlEntry.Update();
						}
						if (strType == "AP")
						{
							if (strAPCheckDate != "")
							{
								strJournalSQL = "SELECT APJournal.ID FROM APJournal INNER JOIN Encumbrances ON APJournal.EncumbranceRecord = Encumbrances.ID WHERE ISNULL(PastYearEnc, 0) = 1 AND (APJournal.Status = 'W' OR APJournal.Status = 'X') AND APJournal.CheckDate = '" + strAPCheckDate + "' AND APJournal.JournalNumber = " + FCConvert.ToString(lngJournal) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod);
							}
							else
							{
								strJournalSQL = "SELECT APJournal.ID FROM APJournal INNER JOIN Encumbrances ON APJournal.EncumbranceRecord = Encumbrances.ID WHERE ISNULL(PastYearEnc, 0) = 1 AND (APJournal.Status = 'W' OR APJournal.Status = 'X') AND APJournal.JournalNumber = " + FCConvert.ToString(lngJournal) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod);
							}
						}
						else
						{
							strJournalSQL = "SELECT APJournal.ID FROM APJournal INNER JOIN Encumbrances ON APJournal.EncumbranceRecord = Encumbrances.ID WHERE ISNULL(PastYearEnc, 0) = 1 AND APJournal.Status = 'E' AND APJournal.JournalNumber = " + FCConvert.ToString(lngJournal) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod);
						}
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Encumbrance) as JournalTotal FROM APJournalDetail WHERE left(Account, 1) = 'R' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Encumbrance) as JournalTotal FROM APJournalDetail WHERE left(Account, 1) = 'E' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Encumbrance) as JournalTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
							rsControlEntry.Set_Fields("Description", "Prior Year Enc CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strUnliqEncCtrAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strUnliqEncCtrAccount);
							}
							rsControlEntry.Set_Fields("Project", "CTRL");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal")));
							rsControlEntry.Set_Fields("RCB", "L");
							rsControlEntry.Update();
						}
					}
					else
					{
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'R' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ") AND RCB = 'E'", "TWBD0000.vb1");
						rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'E' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ") AND RCB = 'E'", "TWBD0000.vb1");
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND Description <> 'Expense CTL' AND Description <> 'Revenue CTL' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "' AND RCB = 'E'", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) != 0)
						{
							rsControlEntry.AddNew();
							rsControlEntry.Set_Fields("JournalNumber", lngJournal);
							rsControlEntry.Set_Fields("Description", "Encumbrance CTL");
							if (!modAccountTitle.Statics.YearFlag)
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEncOffAccount + "-00");
							}
							else
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEncOffAccount);
							}
							rsControlEntry.Set_Fields("RCB", "L");
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
							rsControlEntry.Set_Fields("Amount", (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal"))) * -1);
							rsControlEntry.Set_Fields("Status", "P");
							rsControlEntry.Set_Fields("PostedDate", DateTime.Today);
							rsControlEntry.Set_Fields("Period", intPeriod);
							rsControlEntry.Set_Fields("JournalEntriesDate", DateAndTime.DateValue(ControlDate));
							if (strType == "CW")
							{
								rsControlEntry.Set_Fields("Type", "W");
							}
							else if (strType == "CR")
							{
								rsControlEntry.Set_Fields("Type", "C");
							}
							else if (strType == "CD")
							{
								rsControlEntry.Set_Fields("Type", "D");
							}
							else if (strType == "PY")
							{
								rsControlEntry.Set_Fields("Type", "P");
							}
							else
							{
								rsControlEntry.Set_Fields("Type", "G");
							}
							rsControlEntry.Update();
						}
					}
					rsFundInfo.MoveNext();
				}
				while (rsFundInfo.EndOfFile() != true);
			}
			return CloseEnc;
		}

		public static bool CloseDueToFrom_24(int lngJournal, short intPeriod, string strType, int lngCloseoutID, string strAPCheckDate = "", string strDBPath = "")
		{
			return CloseDueToFrom(ref lngJournal, ref intPeriod, ref strType, ref lngCloseoutID, strAPCheckDate, strDBPath);
		}

		public static bool CloseDueToFrom(ref int lngJournal, ref short intPeriod, ref string strType, ref int lngCloseoutID, string strAPCheckDate = "", string strDBPath = "")
		{
			bool CloseDueToFrom = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsTemp2 = new clsDRWrapper();
			clsDRWrapper rsTemp3 = new clsDRWrapper();
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			clsDRWrapper rsControlEntry = new clsDRWrapper();
			clsDRWrapper rsMasterInfo = new clsDRWrapper();
			string strSql = "";
			clsDRWrapper rsGeneralInfo = new clsDRWrapper();
			string strJournalSQL = "";
			string strCashFund = "";
			Decimal curCashTotal;
			bool blnCreditMemo = false;
			// vbPorter upgrade warning: FundCashTotals As Decimal	OnWrite(short, Decimal)
			Decimal[] FundCashTotals = new Decimal[99 + 1];
			int counter;
			CloseDueToFrom = true;
			rsControlEntry.OmitNullsOnInsert = true;
			if (strDBPath != string.Empty)
			{
				rsTemp.DefaultGroup = strDBPath;
				rsTemp2.DefaultGroup = strDBPath;
				rsTemp3.DefaultGroup = strDBPath;
				rsFundInfo.DefaultGroup = strDBPath;
				rsControlEntry.DefaultGroup = strDBPath;
				rsGeneralInfo.DefaultGroup = strDBPath;
				rsMasterInfo.DefaultGroup = strDBPath;
			}
			// Get the Cash Account
			if (Conversion.Val(Statics.strAPCashAccount) == 0)
			{
				MessageBox.Show("You must set up a Town Accounts Payable Cash Account before you may continue.", "No Town Accounts Payable Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CloseDueToFrom = false;
				return CloseDueToFrom;
			}
			if (Conversion.Val(Statics.strDueToCtrAccount) == 0)
			{
				MessageBox.Show("You must set up your Due To Account before you may proceed", "Setup Due To Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CloseDueToFrom = false;
				return CloseDueToFrom;
			}
			if (Conversion.Val(Statics.strDueFromCtrAccount) == 0)
			{
				MessageBox.Show("You must set up your Due From Account before you may proceed", "Setup Due From Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CloseDueToFrom = false;
				return CloseDueToFrom;
			}
			rsControlEntry.OpenRecordset("SELECT * FROM APJournalDetail", "TWBD0000.vb1");
			for (counter = 1; counter <= 99; counter++)
			{
				FundCashTotals[counter] = 0;
			}
			// Normal Town Entries
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund, UseDueToFrom, CashFund, APOverrideCashFund FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "'", "TWBD0000.vb1");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(GetBDVariable("Due")) == "Y")
				{
					do
					{
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
						if (strType == "AP")
						{
							strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) = '' AND IsNull(UseAlternateCash, 0) = 0 AND (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						}
						else
						{
							strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) = '' AND IsNull(UseAlternateCash, 0) = 0 AND Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						}
						rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE IsNull(Project, '') <> 'CTRL' AND left(Account, 1) = 'R' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE IsNull(Project, '') <> 'CTRL' AND left(Account, 1) = 'E' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE IsNull(Project, '') <> 'CTRL' AND left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'", "TWBD0000.vb1");
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) != 0)
						{
							// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
							if (rsFundInfo.Get_Fields("Fund") != rsFundInfo.Get_Fields_String("CashFund") && FCConvert.ToBoolean(rsFundInfo.Get_Fields_Boolean("UseDueToFrom")) && FCConvert.ToString(rsFundInfo.Get_Fields_String("APOverrideCashFund")) == "")
							{
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("CashFund"))] = FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("CashFund"))] + FCConvert.ToDecimal((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal"))));
								// create due to entry
								rsControlEntry.AddNew();
								rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
								rsControlEntry.Set_Fields("Description", "Due To\\From");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								if (((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) < 0)
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strDueFromCtrAccount + "-" + modValidateAccount.GetFormat_6(rsFundInfo.Get_Fields_String("CashFund"), 2));
								}
								else
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strDueToCtrAccount + "-" + modValidateAccount.GetFormat_6(rsFundInfo.Get_Fields_String("CashFund"), 2));
								}
								rsControlEntry.Set_Fields("Project", "CTRL");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								rsControlEntry.Set_Fields("Amount", ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) * -1);
								rsControlEntry.Set_Fields("RCB", "L");
								rsControlEntry.Update();
								// create due from entry
								rsControlEntry.AddNew();
								rsControlEntry.Set_Fields("APjournalID", lngCloseoutID);
								rsControlEntry.Set_Fields("Description", "Due To\\From");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								if (((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) < 0)
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields_String("CashFund") + "-" + Statics.strDueToCtrAccount + "-" + modValidateAccount.GetFormat_6(rsFundInfo.Get_Fields("Fund"), 2));
								}
								else
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields_String("CashFund") + "-" + Statics.strDueFromCtrAccount + "-" + modValidateAccount.GetFormat_6(rsFundInfo.Get_Fields("Fund"), 2));
								}
								rsControlEntry.Set_Fields("Project", "CTRL");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								rsControlEntry.Set_Fields("Amount", ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))));
								rsControlEntry.Set_Fields("RCB", "L");
								rsControlEntry.Update();
							}
							// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
							else if (rsFundInfo.Get_Fields("Fund") != rsFundInfo.Get_Fields_String("APOverrideCashFund") && FCConvert.ToBoolean(rsFundInfo.Get_Fields_Boolean("UseDueToFrom")) && FCConvert.ToString(rsFundInfo.Get_Fields_String("APOverrideCashFund")) != "")
							{
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("APOverrideCashFund"))] = FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("APOverrideCashFund"))] + FCConvert.ToDecimal((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal"))));
								// create due to entry
								rsControlEntry.AddNew();
								rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
								rsControlEntry.Set_Fields("Description", "Due To\\From");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								if (((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) < 0)
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strDueFromCtrAccount + "-" + modValidateAccount.GetFormat_6(rsFundInfo.Get_Fields_String("APOverrideCashFund"), 2));
								}
								else
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strDueToCtrAccount + "-" + modValidateAccount.GetFormat_6(rsFundInfo.Get_Fields_String("APOverrideCashFund"), 2));
								}
								rsControlEntry.Set_Fields("Project", "CTRL");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								rsControlEntry.Set_Fields("Amount", ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) * -1);
								rsControlEntry.Set_Fields("RCB", "L");
								rsControlEntry.Update();
								// create due from entry
								rsControlEntry.AddNew();
								rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
								rsControlEntry.Set_Fields("Description", "Due To\\From");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								if (((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) < 0)
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields_String("APOverrideCashFund") + "-" + Statics.strDueToCtrAccount + "-" + modValidateAccount.GetFormat_6(rsFundInfo.Get_Fields("Fund"), 2));
								}
								else
								{
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields_String("APOverrideCashFund") + "-" + Statics.strDueFromCtrAccount + "-" + modValidateAccount.GetFormat_6(rsFundInfo.Get_Fields("Fund"), 2));
								}
								rsControlEntry.Set_Fields("Project", "CTRL");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
								rsControlEntry.Set_Fields("Amount", ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))));
								rsControlEntry.Set_Fields("RCB", "L");
								rsControlEntry.Update();
							}
							else
							{
								if (FCConvert.ToBoolean(rsFundInfo.Get_Fields_Boolean("UseDueToFrom")))
								{
									if (FCConvert.ToString(rsFundInfo.Get_Fields_String("APOverrideCashFund")) != "")
									{
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("APOverrideCashFund"))] = FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("APOverrideCashFund"))] + FCConvert.ToDecimal((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal"))));
									}
									else
									{
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("CashFund"))] = FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("CashFund"))] + FCConvert.ToDecimal((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal"))));
									}
								}
									// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
									else if (rsFundInfo.Get_Fields("Fund") == rsFundInfo.Get_Fields_String("CashFund"))
								{
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("CashFund"))] = FundCashTotals[FCConvert.ToInt32(rsFundInfo.Get_Fields_String("CashFund"))] + FCConvert.ToDecimal((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal"))));
								}
								else
								{
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									if (((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) != 0)
									{
										rsControlEntry.AddNew();
										rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
										if (strType == "AP")
										{
											rsControlEntry.Set_Fields("Description", "Cash A/P");
										}
										else
										{
											rsControlEntry.Set_Fields("Description", "Void A/P");
										}
										if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
										{
											if (modAccountTitle.Statics.YearFlag)
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount);
											}
											else
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount + "-00");
											}
										}
										else
										{
											if (modAccountTitle.Statics.YearFlag)
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEOYAPAccount);
												// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
												// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
												// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
												AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount, FCConvert.ToDecimal(((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) * -1));
											}
											else
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEOYAPAccount + "-00");
												// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
												// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
												// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
												AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strAPCashAccount + "-00", FCConvert.ToDecimal(((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) * -1));
											}
										}
										rsControlEntry.Set_Fields("Project", "CTRL");
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										rsControlEntry.Set_Fields("Amount", ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) * -1);
										rsControlEntry.Set_Fields("RCB", "L");
										rsControlEntry.Update();
									}
								}
							}
						}
						rsFundInfo.MoveNext();
					}
					while (rsFundInfo.EndOfFile() != true);
				}
			}
			// Town Override Cash entries
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles", "TWBD0000.vb1");
			if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
					if (strType == "AP" || strType == "AC")
					{
						if (strType == "AP")
						{
							if (strAPCheckDate != "")
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) <> '' AND IsNull(UseAlternateCash, 0) = 0 AND (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
							else
							{
								strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) <> '' AND IsNull(UseAlternateCash, 0) = 0 AND (Status = 'W' OR Status = 'X') AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
							}
						}
						else
						{
							strJournalSQL = "SELECT ID FROM APJournal WHERE rtrim(IsNull(TownOverride, '')) <> '' AND IsNull(UseAlternateCash, 0) = 0 AND Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod);
						}
						// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
						rsTemp.OpenRecordset("SELECT COUNT(EntryCount) as TotalEntryCount, SUM(JournalTotal) as TotalJournalTotal, SUM(DiscountTotal) as TotalDiscountTotal, TownOverride FROM (SELECT * FROM (SELECT COUNT(APJournalDetail.Amount) as EntryCount, SUM(APJournalDetail.Amount) as JournalTotal, SUM(Discount) as DiscountTotal, TownOverride FROM APJournalDetail INNER JOIN APJournal ON APJournal.ID = APJournalDetail.APJournalID WHERE left(Account, 1) = 'R' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") IN (" + strSql + ") AND IsNull(Project, '') <> 'CTRL' GROUP BY TownOverride) as TownOverride1 " + "UNION ALL SELECT * FROM (SELECT COUNT(APJournalDetail.Amount) as EntryCount, SUM(APJournalDetail.Amount) as JournalTotal, SUM(Discount) as DiscountTotal, TownOverride FROM APJournalDetail INNER JOIN APJournal ON APJournal.ID = APJournalDetail.APJournalID WHERE left(Account, 1) = 'E' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") IN (" + strSql + ") AND IsNull(Project, '') <> 'CTRL' GROUP BY TownOverride) as TownOverride2 " + "UNION ALL SELECT * FROM (SELECT COUNT(APJournalDetail.Amount) as EntryCount, SUM(APJournalDetail.Amount) as JournalTotal, SUM(Discount) as DiscountTotal, TownOverride FROM APJournalDetail INNER JOIN APJournal ON APJournal.ID = APJournalDetail.APJournalID WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "' AND IsNull(Project, '') <> 'CTRL' GROUP BY TownOverride) as TownOverride3) as temp GROUP BY TownOverride");
						if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
						{
							do
							{
								// TODO: Field [TotalJournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [TotalDiscountTotal] not found!! (maybe it is an alias?)
								if ((Conversion.Val(rsTemp.Get_Fields("TotalJournalTotal")) - Conversion.Val(rsTemp.Get_Fields("TotalDiscountTotal"))) != 0)
								{
									rsControlEntry.AddNew();
									rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
									if (strType == "AP")
									{
										rsControlEntry.Set_Fields("Description", "Override Cash A/P");
									}
									else
									{
										if (blnCreditMemo)
										{
											rsControlEntry.Set_Fields("Description", "Override " + Strings.Format(DateTime.Today, "MM/dd/yy") + " CM");
										}
										else
										{
											rsControlEntry.Set_Fields("Description", "Override Void A/P");
										}
									}
									if (!modAccountTitle.Statics.YearFlag)
									{
										if (blnCreditMemo)
										{
											// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
											rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride") + "-00");
										}
										else
										{
											if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride") + "-00");
											}
											else
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEOYAPAccount + "-00");
												// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												// TODO: Field [TotalJournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [TotalDiscountTotal] not found!! (maybe it is an alias?)
												AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride") + "-00", FCConvert.ToDecimal(-1 * (Conversion.Val(rsTemp.Get_Fields("TotalJournalTotal")) - Conversion.Val(rsTemp.Get_Fields("TotalDiscountTotal")))));
											}
										}
									}
									else
									{
										if (blnCreditMemo)
										{
											// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
											rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride"));
										}
										else
										{
											if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride"));
											}
											else
											{
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												rsControlEntry.Set_Fields("Account", "G " + rsFundInfo.Get_Fields("Fund") + "-" + Statics.strEOYAPAccount);
												// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
												// TODO: Field [TotalJournalTotal] not found!! (maybe it is an alias?)
												// TODO: Field [TotalDiscountTotal] not found!! (maybe it is an alias?)
												AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + rsFundInfo.Get_Fields("Fund") + "-" + rsTemp.Get_Fields_String("TownOverride"), FCConvert.ToDecimal(-1 * (Conversion.Val(rsTemp.Get_Fields("TotalJournalTotal")) - Conversion.Val(rsTemp.Get_Fields("TotalDiscountTotal")))));
											}
										}
									}
									rsControlEntry.Set_Fields("Project", "CTRL");
									// TODO: Field [TotalJournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [TotalDiscountTotal] not found!! (maybe it is an alias?)
									rsControlEntry.Set_Fields("Amount", -1 * (Conversion.Val(rsTemp.Get_Fields("TotalJournalTotal")) - Conversion.Val(rsTemp.Get_Fields("TotalDiscountTotal"))));
									rsControlEntry.Set_Fields("RCB", "L");
									rsControlEntry.Update();
								}
								rsTemp.MoveNext();
							}
							while (rsTemp.EndOfFile() != true);
						}
					}
					rsFundInfo.MoveNext();
				}
				while (rsFundInfo.EndOfFile() != true);
			}
			// Town Alternate Cash entries
			if (strType == "AP")
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE ISNULL(UseAlternateCash, 0) = 1 AND left(AlternateCashAccount, 1) = 'G' AND (Status = 'W' OR Status = 'X') AND CheckDate = '" + strAPCheckDate + "' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
			}
			else
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE ISNULL(UseAlternateCash, 0) = 1 AND left(AlternateCashAccount, 1) = 'G' AND Status = 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
			}
			if (rsGeneralInfo.EndOfFile() != true && rsGeneralInfo.BeginningOfFile() != true)
			{
				do
				{
					rsFundInfo.OpenRecordset("SELECT DISTINCT Fund, UseDueToFrom FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "'", "TWBD0000.vb1");
					if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(GetBDVariable("Due")) == "Y")
						{
							do
							{
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								strSql = "SELECT Department FROM DeptDivTitles WHERE Division = '" + Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
								rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE IsNull(Project, '') <> 'CTRL' AND left(Account, 1) = 'R' AND APJournalID = " + rsGeneralInfo.Get_Fields_Int32("ID") + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
								rsTemp2.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE IsNull(Project, '') <> 'CTRL' AND left(Account, 1) = 'E' AND APJournalID = " + rsGeneralInfo.Get_Fields_Int32("ID") + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSql + ")", "TWBD0000.vb1");
								// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
								rsTemp3.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE IsNull(Project, '') <> 'CTRL' AND left(Account, 1) = 'G' AND APJournalID = " + rsGeneralInfo.Get_Fields_Int32("ID") + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'", "TWBD0000.vb1");
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
								if ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) + Conversion.Val(rsTemp3.Get_Fields("JournalTotal"))) != 0)
								{
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
									// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
									if (((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) != 0)
									{
										rsControlEntry.AddNew();
										rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
										if (strType == "AP")
										{
											rsControlEntry.Set_Fields("Description", "Alt Cash A/P");
										}
										else
										{
											rsControlEntry.Set_Fields("Description", "Void Alt Cash A/P");
										}
										if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
										{
											rsControlEntry.Set_Fields("Account", rsGeneralInfo.Get_Fields_String("AlternateCashAccount"));
										}
										else
										{
											if (!modAccountTitle.Statics.YearFlag)
											{
												rsControlEntry.Set_Fields("Account", "G " + GetFund_2(rsGeneralInfo.Get_Fields_String("AlternateCashAccount")) + "-" + Statics.strEOYAPAccount + "-00");
											}
											else
											{
												rsControlEntry.Set_Fields("Account", "G " + GetFund_2(rsGeneralInfo.Get_Fields_String("AlternateCashAccount")) + "-" + Statics.strEOYAPAccount);
											}
											// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
											// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
											// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
											// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
											// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
											// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
											// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
											AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), rsGeneralInfo.Get_Fields_String("AlternateCashAccount"), FCConvert.ToDecimal(((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) * -1));
										}
										rsControlEntry.Set_Fields("Project", "CTRL");
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
										// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
										rsControlEntry.Set_Fields("Amount", ((Conversion.Val(rsTemp.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp2.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp2.Get_Fields("DiscountTotal"))) + (Conversion.Val(rsTemp3.Get_Fields("JournalTotal")) - Conversion.Val(rsTemp3.Get_Fields("DiscountTotal")))) * -1);
										rsControlEntry.Set_Fields("RCB", "L");
										rsControlEntry.Update();
									}
								}
								rsFundInfo.MoveNext();
							}
							while (rsFundInfo.EndOfFile() != true);
						}
					}
					rsGeneralInfo.MoveNext();
				}
				while (rsGeneralInfo.EndOfFile() != true);
			}
			if (FCConvert.ToString(GetBDVariable("Due")) == "Y")
			{
				for (counter = 1; counter <= 99; counter++)
				{
					if (FundCashTotals[counter] != 0)
					{
						rsControlEntry.AddNew();
						rsControlEntry.Set_Fields("APJournalID", lngCloseoutID);
						if (strType == "AP")
						{
							rsControlEntry.Set_Fields("Description", "Cash A/P");
						}
						else
						{
							rsControlEntry.Set_Fields("Description", "Void A/P");
						}
						if (modAccountTitle.Statics.YearFlag)
						{
							if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
							{
								rsControlEntry.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + Statics.strAPCashAccount);
							}
							else
							{
								rsControlEntry.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + Statics.strEOYAPAccount);
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + Statics.strAPCashAccount, FundCashTotals[counter] * -1);
							}
						}
						else
						{
							if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "")
							{
								rsControlEntry.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + Statics.strAPCashAccount + "-00");
							}
							else
							{
								rsControlEntry.Set_Fields("Account", "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + Statics.strEOYAPAccount + "-00");
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								AddEntryToArchiveAPAmounts_78(lngJournal, rsControlEntry.Get_Fields("Account"), "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + Statics.strAPCashAccount + "-00", FundCashTotals[counter] * -1);
							}
						}
						rsControlEntry.Set_Fields("Project", "CTRL");
						rsControlEntry.Set_Fields("Amount", FundCashTotals[counter] * -1);
						rsControlEntry.Set_Fields("RCB", "L");
						rsControlEntry.Update();
					}
				}
			}
			return CloseDueToFrom;
		}

		public static void GetControlAccounts()
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'EO'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strEncOffAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'EC'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strExpCtrAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'RC'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strRevCtrAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'PE'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strPastYearEncControlAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'UE'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strUnliqEncCtrAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'FB'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strFundBalCtrAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CR'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strCredMemRecAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CF'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strDueToFromCashFund = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'DT'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strDueToCtrAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'DF'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strDueFromCtrAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CA'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strAPCashAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strCRCashAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
			rsAccountInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'AP'", "Budgetary");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				Statics.strEOYAPAccount = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
			}
		}

		public static bool GlobalCloseDueToFrom_24(int lngJournal, short intPeriod, string strType, string ControlDate, string strDBPath = "")
		{
			return GlobalCloseDueToFrom(ref lngJournal, ref intPeriod, ref strType, ref ControlDate, strDBPath);
		}

		public static bool GlobalCloseDueToFrom(ref int lngJournal, ref short intPeriod, ref string strType, ref string ControlDate, string strDBPath = "")
		{
			bool GlobalCloseDueToFrom = false;
			FundType[] ftFundData = null;
			// Dim ftFundDataSchool() As FundType
			FundType[] ftAccountData = null;
			clsDRWrapper rsJournalData = new clsDRWrapper();
			int counter = 0;
			string strCashDescription = "";
			clsDRWrapper rsMasterInfo = new clsDRWrapper();
			string strCash = "";
			bool blnExistingEntries;
			GlobalCloseDueToFrom = true;
			if (strDBPath != string.Empty)
			{
				rsJournalData.DefaultGroup = strDBPath;
				rsMasterInfo.DefaultGroup = strDBPath;
			}
			rsMasterInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
			if (rsMasterInfo.EndOfFile() != true && rsMasterInfo.BeginningOfFile() != true)
			{
				if (Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description"))).Length > 18)
				{
					strCashDescription = Strings.Left(Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description"))), 18);
				}
				else
				{
					strCashDescription = Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description")));
				}
			}
			else
			{
				strCashDescription = "";
			}
			rsJournalData.OpenRecordset("SELECT * FROM JournalEntries WHERE RCB <> 'L' and RCB <> 'E' AND JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "TWBD0000.vb1");
			if (rsJournalData.EndOfFile() != true && rsJournalData.BeginningOfFile() != true)
			{
				counter = 0;
				do
				{
					Array.Resize(ref ftAccountData, counter + 1);
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					ftAccountData[counter].Account = FCConvert.ToString(rsJournalData.Get_Fields("Account"));
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					ftAccountData[counter].Amount = FCConvert.ToDecimal(rsJournalData.Get_Fields("Amount"));
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					ftAccountData[counter].AcctType = FCConvert.ToString(rsJournalData.Get_Fields("Type"));
					ftAccountData[counter].Description = FCConvert.ToString(rsJournalData.Get_Fields_String("Description"));
					counter += 1;
					rsJournalData.MoveNext();
				}
				while (rsJournalData.EndOfFile() != true);
				ftFundData = CalcFundCash(ref ftAccountData);
				ftAccountData = new FundType[0 + 1];
				if (strType == "CD")
				{
					strCash = "AP";
				}
				else if (strType == "CR")
				{
					strCash = "CR";
				}
				else if (strType == "GJ")
				{
					strCash = "GJ";
				}
				if (CalcDueToFrom_342(ref ftAccountData, ftFundData, true, strCashDescription, strCash))
				{
					counter = AddToJournal_621(ftAccountData, strType, lngJournal, DateAndTime.DateValue(ControlDate), intPeriod);
				}
				else
				{
					MessageBox.Show("No Data Found for Journal " + FCConvert.ToString(lngJournal), "Due To/From Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					GlobalCloseDueToFrom = false;
					return GlobalCloseDueToFrom;
				}
			}
			return GlobalCloseDueToFrom;
		}

		public static void CheckJournalForAPArchiveEntries(ref int lngJournal, string strDBPath = "")
		{
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsArchive = new clsDRWrapper();
			string strAcct = "";
			if (strDBPath != string.Empty)
			{
				rs.DefaultGroup = strDBPath;
				rsArchive.DefaultGroup = strDBPath;
			}
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (GetAccount_2(rs.Get_Fields("Account")) == Statics.strEOYAPAccount)
					{
						if (modAccountTitle.Statics.YearFlag)
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strAcct = "G " + GetFund_2(rs.Get_Fields("Account")) + "-" + Statics.strCRCashAccount;
						}
						else
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strAcct = "G " + GetFund_2(rs.Get_Fields("Account")) + "-" + Statics.strCRCashAccount + "-00";
						}
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						AddEntryToArchiveAPAmounts_60(lngJournal, rs.Get_Fields("Account"), strAcct, rs.Get_Fields("Amount"), strDBPath);
					}
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}
		// vbPorter upgrade warning: curAmount As Decimal	OnWrite(double, Decimal)
		public static void AddEntryToArchiveAPAmounts_60(int lngJournal, string strAccount, string strCashAccount, Decimal curAmount, string strDBPath = "")
		{
			AddEntryToArchiveAPAmounts(ref lngJournal, ref strAccount, ref strCashAccount, ref curAmount, strDBPath);
		}

		public static void AddEntryToArchiveAPAmounts_78(int lngJournal, string strAccount, string strCashAccount, Decimal curAmount, string strDBPath = "")
		{
			AddEntryToArchiveAPAmounts(ref lngJournal, ref strAccount, ref strCashAccount, ref curAmount, strDBPath);
		}

		public static void AddEntryToArchiveAPAmounts(ref int lngJournal, ref string strAccount, ref string strCashAccount, ref Decimal curAmount, string strDBPath = "")
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			if (strDBPath != string.Empty)
			{
				rsInfo.DefaultGroup = strDBPath;
				rsJournalInfo.DefaultGroup = strDBPath;
			}
			rsInfo.OpenRecordset("SELECT * FROM ArchiveAPAmounts WHERE ID = 0", "TWBD0000.vb1");
			rsInfo.AddNew();
			rsInfo.Set_Fields("JournalNumber", lngJournal);
			rsInfo.Set_Fields("Account", strAccount);
			rsInfo.Set_Fields("CashAccount", strCashAccount);
			rsInfo.Set_Fields("Amount", curAmount);
			rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWBD0000.vb1");
			if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsJournalInfo.Get_Fields("Type")) == "AC")
				{
					rsInfo.Set_Fields("Correction", true);
				}
				else
				{
					rsInfo.Set_Fields("Correction", false);
				}
			}
			else
			{
				rsInfo.Set_Fields("Correction", false);
			}
			rsInfo.Set_Fields("Transferred", false);
			rsInfo.Update();
		}

		public static void AddCheckRecEntriesToStruct_6(int intJournal, string strType, string strDBPath = "")
		{
			AddCheckRecEntriesToStruct(ref intJournal, ref strType, strDBPath);
		}

		public static void AddCheckRecEntriesToStruct(ref int intJournal, ref string strType, string strDBPath = "")
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int intBank = 0;
			clsDRWrapper rsBankInfo = new clsDRWrapper();
			bool blnCashAccount = false;
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			bool blnInterest = false;
			if (strDBPath != string.Empty)
			{
				rsInfo.DefaultGroup = strDBPath;
				rsBankInfo.DefaultGroup = strDBPath;
				rsVendorInfo.DefaultGroup = strDBPath;
			}
			if (strType == "CW")
				strType = "CR";
			if (strType == "CD")
			{
				rsInfo.OpenRecordset("SELECT CheckNumber, SUM(Amount) as TotalAmount, JournalEntriesDate, BankNumber, VendorNumber FROM JournalEntries WHERE convert(int, IsNull(CheckNumber, 0)) > 0 AND JournalNumber = " + FCConvert.ToString(intJournal) + " GROUP BY CheckNumber, JournalEntriesDate, BankNumber, VendorNumber", "TWBD0000.vb1");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						if (Statics.lngNumberOfAccounts == 0)
						{
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("TotalAmount"));
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].datCheckDate = (DateTime)rsInfo.Get_Fields_DateTime("JournalEntriesDate");
							// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].intBank = FCConvert.ToInt32(rsInfo.Get_Fields("BankNumber"));
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].lngKey = 0;
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].strAccount = "";
							// TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheck = FCConvert.ToString(rsInfo.Get_Fields("CheckNumber"));
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "1";
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].strJournalType = strType;
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].lngJournal = intJournal;
							rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"), "TWBD0000.vb1");
							if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
							{
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))) + " Jrnl " + Strings.Format(intJournal, "0000");
							}
							else
							{
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Vendor # " + rsInfo.Get_Fields_Int32("VendorNumber") + " Jrnl " + Strings.Format(intJournal, "0000");
							}
						}
						else
						{
							Array.Resize(ref Statics.AcctsToPass, Statics.lngNumberOfAccounts + 1);
							//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
							Statics.AcctsToPass[Statics.lngNumberOfAccounts] = new AcctsForCheckRec(0);
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("TotalAmount"));
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].datCheckDate = (DateTime)rsInfo.Get_Fields_DateTime("JournalEntriesDate");
							// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].intBank = FCConvert.ToInt32(rsInfo.Get_Fields("BankNumber"));
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].lngKey = 0;
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].strAccount = "";
							// TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheck = FCConvert.ToString(rsInfo.Get_Fields("CheckNumber"));
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "1";
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].strJournalType = strType;
							Statics.AcctsToPass[Statics.lngNumberOfAccounts].lngJournal = intJournal;
							rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"), "TWBD0000.vb1");
							if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
							{
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))) + " Jrnl " + Strings.Format(intJournal, "0000");
							}
							else
							{
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Vendor # " + rsInfo.Get_Fields_Int32("VendorNumber") + " Jrnl " + Strings.Format(intJournal, "0000");
							}
						}
						Statics.lngNumberOfAccounts += 1;
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(intJournal), "TWBD0000.vb1");
				// & " AND (left(Account, 1) = 'G' )"
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					intBank = 0;
					rsBankInfo.OpenRecordset("SELECT * FROM Banks WHERE rtrim(Name) <> ''", "TWBD0000.vb1");
					if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
					{
						if (rsBankInfo.RecordCount() == 1)
						{
							intBank = FCConvert.ToInt32(rsBankInfo.Get_Fields_Int32("ID"));
						}
					}
					do
					{
						//Application.DoEvents();
						blnCashAccount = false;
						blnInterest = false;
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBankInfo.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Account = '" + rsInfo.Get_Fields("Account") + "' OR (left(Account, " + FCConvert.ToString(FCConvert.ToString(rsInfo.Get_Fields("Account")).Length - 3) + ") = '" + Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), FCConvert.ToString(rsInfo.Get_Fields("Account")).Length - 3) + "' AND ISNULL(AllSuffix, 0) = 1)", "TWBD0000.vb1");
						if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
						{
							blnCashAccount = true;
							// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							intBank = FCConvert.ToInt32(rsBankInfo.Get_Fields("BankNumber"));
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							blnInterest = FCConvert.ToBoolean(rsBankInfo.Get_Fields("Interest"));
						}
						if (blnCashAccount == false)
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "G" && ((modAccountTitle.Statics.YearFlag == false && Strings.Right(FCConvert.ToString(rsInfo.Get_Fields("Account")), 2) == "00") || modAccountTitle.Statics.YearFlag == true))
							{
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsBankInfo.OpenRecordset("SELECT * FROM Banks WHERE TownCashAccount = '" + GetAccount_2(rsInfo.Get_Fields("Account")) + "'", "TWBD0000.vb1");
								if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
								{
									blnCashAccount = true;
									intBank = FCConvert.ToInt32(rsBankInfo.Get_Fields_Int32("ID"));
								}
							}
						}
						if ((strType == "CR") && blnCashAccount == false)
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "G" && ((modAccountTitle.Statics.YearFlag == false && Strings.Right(FCConvert.ToString(rsInfo.Get_Fields("Account")), 2) == "00") || modAccountTitle.Statics.YearFlag == true))
							{
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsBankInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM' AND Account = '" + GetAccount_2(rsInfo.Get_Fields("Account")) + "'", "TWBD0000.vb1");
								if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
								{
									blnCashAccount = true;
									intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(GetBankVariable("CRBank"))));
								}
							}
						}
						if (strType == "GJ" && blnCashAccount == false)
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("Account")), 1) == "G" && ((modAccountTitle.Statics.YearFlag == false && Strings.Right(FCConvert.ToString(rsInfo.Get_Fields("Account")), 2) == "00") || modAccountTitle.Statics.YearFlag == true))
							{
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsBankInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM' AND Account = '" + GetAccount_2(rsInfo.Get_Fields("Account")) + "'", "TWBD0000.vb1");
								if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
								{
									blnCashAccount = true;
									intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(GetBankVariable("CRBank"))));
								}
								if (blnCashAccount == false)
								{
									// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
									rsBankInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CA' AND Account = '" + GetAccount_2(rsInfo.Get_Fields("Account")) + "'", "TWBD0000.vb1");
									if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
									{
										blnCashAccount = true;
										intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(GetBankVariable("APBank"))));
									}
								}
								if (blnCashAccount == false)
								{
									// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
									rsBankInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CP' AND Account = '" + GetAccount_2(rsInfo.Get_Fields("Account")) + "'", "TWBD0000.vb1");
									if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
									{
										blnCashAccount = true;
										intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(GetBankVariable("PayrollBank"))));
									}
								}
							}
						}
						if (blnCashAccount)
						{
							if (Statics.lngNumberOfAccounts == 0)
							{
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].datCheckDate = (DateTime)rsInfo.Get_Fields_DateTime("JournalEntriesDate");
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].intBank = intBank;
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].lngKey = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID"));
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strAccount = FCConvert.ToString(rsInfo.Get_Fields("Account"));
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheck = "";
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].lngJournal = intJournal;
								if (blnInterest == false)
								{
									if (!Statics.gboolAutoInterest && strType != "CR")
									{
										// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
										if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) < 0)
										{
											// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount")) * -1;
										}
										else
										{
											// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
										}
									}
									else
									{
										// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
										Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
									}
									if (strType == "CR")
									{
										Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "3";
										if (Statics.gboolAutoInterest)
										{
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Deposit" + " Jrnl " + Strings.Format(intJournal, "0000");
										}
										else
										{
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = rsInfo.Get_Fields_String("Description") + " Jrnl " + Strings.Format(intJournal, "0000");
										}
									}
									else
									{
										// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
										if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) > 0)
										{
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "6";
											if (Statics.gboolAutoInterest)
											{
												Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Other Credits" + " Jrnl " + Strings.Format(intJournal, "0000");
											}
											else
											{
												Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = rsInfo.Get_Fields_String("Description") + " Jrnl " + Strings.Format(intJournal, "0000");
											}
										}
										else
										{
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "7";
											if (Statics.gboolAutoInterest)
											{
												Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Other Debits" + " Jrnl " + Strings.Format(intJournal, "0000");
											}
											else
											{
												Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = rsInfo.Get_Fields_String("Description") + " Jrnl " + Strings.Format(intJournal, "0000");
											}
										}
									}
								}
								else
								{
									Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "5";
									Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Interest" + " Jrnl " + Strings.Format(intJournal, "0000");
									// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
									Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount")) * -1;
								}
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strJournalType = strType;
							}
							else
							{
								Array.Resize(ref Statics.AcctsToPass, Statics.lngNumberOfAccounts + 1);
								//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
								Statics.AcctsToPass[Statics.lngNumberOfAccounts] = new AcctsForCheckRec(0);
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].datCheckDate = (DateTime)rsInfo.Get_Fields_DateTime("JournalEntriesDate");
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].intBank = intBank;
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].lngKey = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID"));
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strAccount = FCConvert.ToString(rsInfo.Get_Fields("Account"));
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheck = "";
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].lngJournal = intJournal;
								if (blnInterest == false)
								{
									if (!Statics.gboolAutoInterest && strType != "CR")
									{
										// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
										if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) < 0)
										{
											// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount")) * -1;
										}
										else
										{
											// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
										}
									}
									else
									{
										// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
										Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
									}
									if (strType == "CR")
									{
										Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "3";
										if (Statics.gboolAutoInterest)
										{
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Deposit" + " Jrnl " + Strings.Format(intJournal, "0000");
										}
										else
										{
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = rsInfo.Get_Fields_String("Description") + " Jrnl " + Strings.Format(intJournal, "0000");
										}
									}
									else
									{
										// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
										if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) > 0)
										{
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "6";
											if (Statics.gboolAutoInterest)
											{
												Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Other Credits" + " Jrnl " + Strings.Format(intJournal, "0000");
											}
											else
											{
												Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = rsInfo.Get_Fields_String("Description") + " Jrnl " + Strings.Format(intJournal, "0000");
											}
										}
										else
										{
											Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "7";
											if (Statics.gboolAutoInterest)
											{
												Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Other Debits" + " Jrnl " + Strings.Format(intJournal, "0000");
											}
											else
											{
												Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = rsInfo.Get_Fields_String("Description") + " Jrnl " + Strings.Format(intJournal, "0000");
											}
										}
									}
								}
								else
								{
									Statics.AcctsToPass[Statics.lngNumberOfAccounts].strCheckRecType = "5";
									Statics.AcctsToPass[Statics.lngNumberOfAccounts].strPayee = "Interest" + " Jrnl " + Strings.Format(intJournal, "0000");
									// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
									Statics.AcctsToPass[Statics.lngNumberOfAccounts].curAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount")) * -1;
								}
								Statics.AcctsToPass[Statics.lngNumberOfAccounts].strJournalType = strType;
							}
							if (intBank != 0)
							{
								// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
								if (Conversion.Val(rsInfo.Get_Fields("BankNumber")) == 0)
								{
									rsInfo.Edit();
									rsInfo.Set_Fields("BankNumber", intBank);
									rsInfo.Update();
								}
							}
							Statics.lngNumberOfAccounts += 1;
						}
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
			}
		}

		public static void SaveCheckRecEntries(string strDBPath = "")
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			bool blnShowBankForm;
			clsDRWrapper rsBankInfo = new clsDRWrapper();
			if (strDBPath != string.Empty)
			{
				rsInfo.DefaultGroup = strDBPath;
				rsBankInfo.DefaultGroup = strDBPath;
			}
			blnShowBankForm = false;
			for (counter = 0; counter <= Statics.lngNumberOfAccounts - 1; counter++)
			{
				if (Statics.AcctsToPass[counter].intBank == 0)
				{
					blnShowBankForm = true;
					break;
				}
			}
			if (blnShowBankForm)
			{
				frmSelectBankNumber.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
			}
			CombineJournalEntries();
			rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster", "TWBD0000.vb1");
			for (counter = 0; counter <= Statics.lngNumberOfAccounts - 1; counter++)
			{
				if (Statics.AcctsToPass[counter].intBank != 0)
				{
					rsBankInfo.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(Statics.AcctsToPass[counter].intBank), "TWBD0000.vb1");
					rsInfo.AddNew();
					rsInfo.Set_Fields("CheckNumber", FCConvert.ToString(Conversion.Val(Statics.AcctsToPass[counter].strCheck)));
					rsInfo.Set_Fields("Type", Statics.AcctsToPass[counter].strCheckRecType);
					rsInfo.Set_Fields("CheckDate", Statics.AcctsToPass[counter].datCheckDate);
					rsInfo.Set_Fields("Name", Statics.AcctsToPass[counter].strPayee);
					rsInfo.Set_Fields("Amount", Statics.AcctsToPass[counter].curAmount);
					rsInfo.Set_Fields("Status", "1");
					rsInfo.Set_Fields("StatusDate", DateTime.Today);
					rsInfo.Set_Fields("Automatic", true);
					if (Statics.AcctsToPass[counter].strJournalType == "CD")
					{
						rsInfo.Set_Fields("CDEntry", true);
					}
					else
					{
						rsInfo.Set_Fields("CDEntry", false);
					}
					if (FCConvert.ToString(rsInfo.Get_Fields_String("Status")) == "1" || FCConvert.ToString(rsInfo.Get_Fields_String("Status")) == "2")
					{
						if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
						{
							if (Information.IsDate(rsBankInfo.Get_Fields("StatementDate")))
							{
								if (FCConvert.ToString(rsInfo.Get_Fields_String("Status")) == "1")
								{
									if (rsInfo.Get_Fields_DateTime("CheckDate") <= rsBankInfo.Get_Fields_DateTime("StatementDate"))
									{
										rsInfo.Set_Fields("Status", "2");
										rsInfo.Set_Fields("StatusDate", DateTime.Today);
									}
								}
								else
								{
									if (rsInfo.Get_Fields_DateTime("CheckDate") > rsBankInfo.Get_Fields_DateTime("StatementDate"))
									{
										rsInfo.Set_Fields("Status", "1");
										rsInfo.Set_Fields("StatusDate", DateTime.Today);
									}
								}
							}
						}
					}
					rsInfo.Set_Fields("BankNumber", Statics.AcctsToPass[counter].intBank);
					rsInfo.Update(true);
				}
			}
		}

		private static void CombineJournalEntries()
		{
			int intCurrentIndex;
			int lngCurrentJournal = 0;
			int[] intCashIndex = new int[99 + 1];
			int[] intInterestIndex = new int[99 + 1];
			int intEntriesToDelete = 0;
			int counter;
			int counter2;
			// vbPorter upgrade warning: curCashAmount As Decimal	OnWrite(short, Decimal)
			Decimal[] curCashAmount = new Decimal[99 + 1];
			// vbPorter upgrade warning: curInterestAmount As Decimal	OnWrite(short, Decimal)
			Decimal[] curInterestAmount = new Decimal[99 + 1];
			int counter3 = 0;
			if (Statics.gboolAutoInterest)
			{
				lngCurrentJournal = -1;
				for (counter = 0; counter <= 99; counter++)
				{
					intInterestIndex[counter] = -1;
					intCashIndex[counter] = -1;
					curCashAmount[counter] = 0;
					curInterestAmount[counter] = 0;
				}
				for (intCurrentIndex = 0; intCurrentIndex <= Statics.lngNumberOfAccounts - 1; intCurrentIndex++)
				{
					// if we get to a new journal reset all variables and combine any found amounts from the last journal
					if (lngCurrentJournal != Statics.AcctsToPass[intCurrentIndex].lngJournal)
					{
						lngCurrentJournal = Statics.AcctsToPass[intCurrentIndex].lngJournal;
						for (counter = 0; counter <= 99; counter++)
						{
							// if cash entries were found
							if (intCashIndex[counter] != -1)
							{
								// put the total cash amount minus interest inot the cash index
								Statics.AcctsToPass[intCashIndex[counter]].curAmount = curCashAmount[counter] - curInterestAmount[counter];
								// if the amount minus interest = 0 then erase this cash entry otherwise check to see if the amount has gone from debit to credit and label it correctly
								if (Statics.AcctsToPass[intCashIndex[counter]].curAmount != 0)
								{
									// if the entry was a deposit dont change the description otherwise set the description based on the amount
									if (Statics.AcctsToPass[intCashIndex[counter]].strCheckRecType != "3")
									{
										if (Statics.AcctsToPass[intCashIndex[counter]].curAmount > 0)
										{
											Statics.AcctsToPass[intCashIndex[counter]].strCheckRecType = "6";
											Statics.AcctsToPass[intCashIndex[counter]].strPayee = "Other Credits" + " Jrnl " + Strings.Format(Statics.AcctsToPass[intCashIndex[counter]].lngJournal, "0000");
										}
										else
										{
											Statics.AcctsToPass[intCashIndex[counter]].strCheckRecType = "7";
											Statics.AcctsToPass[intCashIndex[counter]].strPayee = "Other Debits" + " Jrnl " + Strings.Format(Statics.AcctsToPass[intCashIndex[counter]].lngJournal, "0000");
											Statics.AcctsToPass[intCashIndex[counter]].curAmount *= -1;
										}
									}
								}
								else
								{
									Statics.AcctsToPass[intCashIndex[counter]].intBank = -1;
								}
							}
							// put all interest into the found index if there is any
							if (intInterestIndex[counter] != -1)
							{
								Statics.AcctsToPass[intInterestIndex[counter]].curAmount = curInterestAmount[counter];
							}
							// reset variables
							intInterestIndex[counter] = -1;
							intCashIndex[counter] = -1;
							curCashAmount[counter] = 0;
							curInterestAmount[counter] = 0;
						}
					}
					// if journal is not a CD type journal then combine amounts if possible
					if (Statics.AcctsToPass[intCurrentIndex].strJournalType != "CD")
					{
						// if this entry is a cash entry not an interest entry
						if (FCConvert.ToDouble(Statics.AcctsToPass[intCurrentIndex].strCheckRecType) != 5)
						{
							// check to see if we already have a cash index for this bank for this journal
							if (intCashIndex[Statics.AcctsToPass[intCurrentIndex].intBank] == -1)
							{
								// if not set the cash index and save the amount
								intCashIndex[Statics.AcctsToPass[intCurrentIndex].intBank] = intCurrentIndex;
								curCashAmount[Statics.AcctsToPass[intCurrentIndex].intBank] = Statics.AcctsToPass[intCurrentIndex].curAmount;
							}
							else
							{
								// if we do add the amount from this entry into our already existing amount and set the bank to -1 so we know to delete this entry at the end of the procedure
								curCashAmount[Statics.AcctsToPass[intCurrentIndex].intBank] += Statics.AcctsToPass[intCurrentIndex].curAmount;
								Statics.AcctsToPass[intCurrentIndex].intBank = -1;
							}
						}
						else
						{
							// check to see if we have an interest index set up for this bank for this journal
							if (intInterestIndex[Statics.AcctsToPass[intCurrentIndex].intBank] == -1)
							{
								// if nto set the interest index and save the amount
								intInterestIndex[Statics.AcctsToPass[intCurrentIndex].intBank] = intCurrentIndex;
								curInterestAmount[Statics.AcctsToPass[intCurrentIndex].intBank] = Statics.AcctsToPass[intCurrentIndex].curAmount;
							}
							else
							{
								// if we do add the amount from this entry into our already existing amount and set the bank to -1 so we knwo to delete this entry at the end of the procedure
								curInterestAmount[Statics.AcctsToPass[intCurrentIndex].intBank] += Statics.AcctsToPass[intCurrentIndex].curAmount;
								Statics.AcctsToPass[intCurrentIndex].intBank = -1;
							}
						}
					}
				}
				// we have come to the end of our accounts so we need to combine amounts one last time for the leftover entries
				for (counter = 0; counter <= 99; counter++)
				{
					if (intCashIndex[counter] != -1)
					{
						Statics.AcctsToPass[intCashIndex[counter]].curAmount = curCashAmount[counter] - curInterestAmount[counter];
						if (Statics.AcctsToPass[intCashIndex[counter]].curAmount != 0)
						{
							if (Statics.AcctsToPass[intCashIndex[counter]].strCheckRecType != "3")
							{
								if (Statics.AcctsToPass[intCashIndex[counter]].curAmount > 0)
								{
									Statics.AcctsToPass[intCashIndex[counter]].strCheckRecType = "6";
									Statics.AcctsToPass[intCashIndex[counter]].strPayee = "Other Credits" + " Jrnl " + Strings.Format(Statics.AcctsToPass[intCashIndex[counter]].lngJournal, "0000");
								}
								else
								{
									Statics.AcctsToPass[intCashIndex[counter]].strCheckRecType = "7";
									Statics.AcctsToPass[intCashIndex[counter]].strPayee = "Other Debits" + " Jrnl " + Strings.Format(Statics.AcctsToPass[intCashIndex[counter]].lngJournal, "0000");
									Statics.AcctsToPass[intCashIndex[counter]].curAmount *= -1;
								}
							}
						}
						else
						{
							Statics.AcctsToPass[intCashIndex[counter]].intBank = -1;
						}
					}
					if (intInterestIndex[counter] != -1)
					{
						Statics.AcctsToPass[intInterestIndex[counter]].curAmount = curInterestAmount[counter];
					}
					intInterestIndex[counter] = -1;
					intCashIndex[counter] = -1;
					curCashAmount[counter] = 0;
					curInterestAmount[counter] = 0;
				}
				// now we go through the array and check for any entries with a bank number of -1
				intEntriesToDelete = 0;
				counter3 = 0;
				for (counter = 0; counter <= Statics.lngNumberOfAccounts - 1; counter++)
				{
					if (Statics.AcctsToPass[counter3].intBank == -1)
					{
						// if we find one we bring all the information past it back one so we can redim the array at the end and get rid of thebad data
						intEntriesToDelete += 1;
						for (counter2 = counter3 + 1; counter2 <= Statics.lngNumberOfAccounts - 1; counter2++)
						{
							Statics.AcctsToPass[counter2 - 1].curAmount = Statics.AcctsToPass[counter2].curAmount;
							Statics.AcctsToPass[counter2 - 1].datCheckDate = Statics.AcctsToPass[counter2].datCheckDate;
							Statics.AcctsToPass[counter2 - 1].intBank = Statics.AcctsToPass[counter2].intBank;
							Statics.AcctsToPass[counter2 - 1].lngKey = Statics.AcctsToPass[counter2].lngKey;
							Statics.AcctsToPass[counter2 - 1].strAccount = Statics.AcctsToPass[counter2].strAccount;
							Statics.AcctsToPass[counter2 - 1].strCheck = Statics.AcctsToPass[counter2].strCheck;
							Statics.AcctsToPass[counter2 - 1].strCheckRecType = Statics.AcctsToPass[counter2].strCheckRecType;
							Statics.AcctsToPass[counter2 - 1].strJournalType = Statics.AcctsToPass[counter2].strJournalType;
							Statics.AcctsToPass[counter2 - 1].lngJournal = Statics.AcctsToPass[counter2].lngJournal;
							Statics.AcctsToPass[counter2 - 1].strPayee = Statics.AcctsToPass[counter2].strPayee;
						}
					}
					else
					{
						counter3 += 1;
					}
				}
				// set the new number of entries and delete out bad data
				Statics.lngNumberOfAccounts -= intEntriesToDelete;
				if (Statics.lngNumberOfAccounts > 0)
				{
					Array.Resize(ref Statics.AcctsToPass, Statics.lngNumberOfAccounts - 1 + 1);
				}
			}
		}

		public static void CombineDuplicateControlEntries_2(int intJournal, string strDBPath = "")
		{
			CombineDuplicateControlEntries(ref intJournal, strDBPath);
		}

		public static void CombineDuplicateControlEntries(ref int intJournal, string strDBPath = "")
		{
			clsDRWrapper rsGroupJournalInfo = new clsDRWrapper();
			clsDRWrapper rsIndividualJournalInfo = new clsDRWrapper();
			int lngRecordNumber = 0;
			clsDRWrapper rsDeleteJournalInfo = new clsDRWrapper();
			if (strDBPath != string.Empty)
			{
				rsGroupJournalInfo.DefaultGroup = strDBPath;
				rsIndividualJournalInfo.DefaultGroup = strDBPath;
				rsDeleteJournalInfo.DefaultGroup = strDBPath;
			}
			rsGroupJournalInfo.OpenRecordset("SELECT SUM(Amount) as TotalAmount, Account FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(intJournal) + " AND RCB = 'L' GROUP BY Account", "TWBD0000.vb1");
			if (rsGroupJournalInfo.EndOfFile() != true && rsGroupJournalInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsIndividualJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(intJournal) + " AND RCB = 'L' AND Account = '" + rsGroupJournalInfo.Get_Fields("Account") + "'", "TWBD0000.vb1");
					if (rsIndividualJournalInfo.EndOfFile() != true && rsIndividualJournalInfo.BeginningOfFile() != true)
					{
						if (rsIndividualJournalInfo.RecordCount() != 1)
						{
							lngRecordNumber = FCConvert.ToInt32(rsIndividualJournalInfo.Get_Fields_Int32("ID"));
							rsIndividualJournalInfo.Edit();
							rsIndividualJournalInfo.Set_Fields("Amount", rsGroupJournalInfo.Get_Fields_Decimal("TotalAmount"));
							rsIndividualJournalInfo.Update();
							rsIndividualJournalInfo.Reset();
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (!rsDeleteJournalInfo.Execute("DELETE  FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(intJournal) + " AND RCB = 'L' AND Account = '" + rsGroupJournalInfo.Get_Fields("Account") + "' AND ID <> " + FCConvert.ToString(lngRecordNumber), "TWBD0000.vb1"))
							{
								MessageBox.Show("Error Combining Control Entries.  Please Call TRIO before Posting any Journals.", "Error Combining Multiple Control Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
					else
					{
						MessageBox.Show("Error Combining Control Entries.  Please Call TRIO before Posting any Journals.", "Error Combining Multiple Control Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					rsGroupJournalInfo.MoveNext();
				}
				while (rsGroupJournalInfo.EndOfFile() != true);
			}
		}

		public static bool ValidateControlAccountsForPosting(ref int intJournal, ref string strType, string strDBPath = "")
		{
			bool ValidateControlAccountsForPosting = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			ValidateControlAccountsForPosting = false;
			if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) != "")
			{
				if (Conversion.Val(Statics.strEOYAPAccount) == 0)
				{
					MessageBox.Show("You must set up your EOY Accounts Payable Account before you may proceed", "Setup EOY Accounts Payable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateControlAccountsForPosting;
				}
			}
			if (FCConvert.ToString(GetBDVariable("Due")) == "Y")
			{
				if (Conversion.Val(Statics.strDueToCtrAccount) == 0)
				{
					MessageBox.Show("You must set up your Due To Account before you may proceed", "Setup Due To Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateControlAccountsForPosting;
				}
				if (Conversion.Val(Statics.strDueFromCtrAccount) == 0)
				{
					MessageBox.Show("You must set up your Due From Account before you may proceed", "Setup Due From Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateControlAccountsForPosting;
				}
			}
			if (Conversion.Val(Statics.strAPCashAccount) == 0)
			{
				MessageBox.Show("You must set up your Town Accounts Payable Cash Account before you may proceed", "Setup Town Accounts Payable Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return ValidateControlAccountsForPosting;
			}
			if (Conversion.Val(Statics.strExpCtrAccount) == 0)
			{
				MessageBox.Show("You must set up your Town Expense Control Account before you may proceed", "Setup Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return ValidateControlAccountsForPosting;
			}
			if (Conversion.Val(Statics.strRevCtrAccount) == 0)
			{
				MessageBox.Show("You must set up your Town Revenue Control Account before you may proceed", "Setup Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return ValidateControlAccountsForPosting;
			}
			if (Conversion.Val(Statics.strCredMemRecAccount) == 0)
			{
				MessageBox.Show("You must set up your Town Credit Memo Receivable Account before you may proceed", "Setup Town Credit Memo Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return ValidateControlAccountsForPosting;
			}
			if (strType == "EN")
			{
				if (Conversion.Val(Statics.strEncOffAccount) == 0)
				{
					MessageBox.Show("You must set up your Town Encumbrance Control Account before you may proceed", "Setup Town Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateControlAccountsForPosting;
				}
			}
			else if (strType == "AP" || strType == "AC")
			{
				rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Encumbrance) as JournalTotal FROM APJournalDetail WHERE APJournalID IN (SELECT ID FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(intJournal) + ")", "TWBD0000.vb1");
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) != 0)
				{
					if (Conversion.Val(Statics.strEncOffAccount) == 0)
					{
						MessageBox.Show("You must set up your Town Encumbrance Control Account before you may proceed", "Setup Town Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return ValidateControlAccountsForPosting;
					}
				}
			}
			else
			{
				rsTemp.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(intJournal) + " AND RCB = 'E'", "TWBD0000.vb1");
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					if (Conversion.Val(Statics.strEncOffAccount) == 0)
					{
						MessageBox.Show("You must set up your Town Encumbrance Control Account before you may proceed", "Setup Town Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return ValidateControlAccountsForPosting;
					}
				}
			}
			ValidateControlAccountsForPosting = true;
			return ValidateControlAccountsForPosting;
		}
		// Public Sub CalculateAccountInfo(blnSummary As Boolean, blnPosted As Boolean, blnPending As Boolean, strAcctType As String, Optional strPath As String = "", Optional blnForceCalculate As Boolean = False)
		public static void CalculateAccountInfo(string strPath = "", bool boolForceCalculate = false)
		{
			bool boolCalc;
			boolCalc = boolForceCalculate;
			clsDRWrapper rsLoad = new clsDRWrapper();
			if (strPath != "")
			{
				rsLoad.GroupName = strPath;
			}
			if (!boolCalc)
			{
				rsLoad.OpenRecordset("select * from calculationneeded", "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("exppendingsummary")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("exppostedsummary")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("revpendingsummary")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("revpostedsummary")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ledgerpendingsummary")))
					{
						boolCalc = true;
					}
					else if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("LedgerPostedSummary")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ExpPendingDetail")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ExpPostedDetail")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("RevPendingDetail")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("RevPostedDetail")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("LedgerPendingDetail")) || FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("LedgerPostedDetail")))
					{
						boolCalc = true;
					}
				}
				else
				{
					boolCalc = true;
				}
			}
			if (boolCalc)
			{
				cJournalService jServ = new cJournalService();
				jServ.CalculateSummaryDetailInfo(strPath);
				rsLoad.OpenRecordset("select * from calculationneeded", "Budgetary");
				if (rsLoad.EndOfFile())
				{
					rsLoad.AddNew();
				}
				else
				{
					rsLoad.Edit();
				}
				rsLoad.Set_Fields("ExpPendingSummary", false);
				rsLoad.Set_Fields("ExpPostedSummary", false);
				rsLoad.Set_Fields("RevPendingSummary", false);
				rsLoad.Set_Fields("RevPostedSummary", false);
				rsLoad.Set_Fields("LedgerPendingSummary", false);
				rsLoad.Set_Fields("LedgerPostedSummary", false);
				rsLoad.Set_Fields("ExpPendingDetail", false);
				rsLoad.Set_Fields("ExpPostedDetail", false);
				rsLoad.Set_Fields("RevPendingDetail", false);
				rsLoad.Set_Fields("RevPostedDetail", false);
				rsLoad.Set_Fields("LedgerPendingDetail", false);
				rsLoad.Set_Fields("LedgerPostedDetail", false);
				rsLoad.Set_Fields("SchoolExpPendingSummary", false);
				rsLoad.Set_Fields("SchoolExpPostedSummary", false);
				rsLoad.Set_Fields("SchoolRevPendingSummary", false);
				rsLoad.Set_Fields("SchoolRevPostedSummary", false);
				rsLoad.Set_Fields("SchoolLedgerPendingSummary", false);
				rsLoad.Set_Fields("SchoolLedgerPostedSummary", false);
				rsLoad.Set_Fields("SchoolExpPendingDetail", false);
				rsLoad.Set_Fields("SchoolExpPostedDetail", false);
				rsLoad.Set_Fields("SchoolRevPendingDetail", false);
				rsLoad.Set_Fields("SchoolRevPostedDetail", false);
				rsLoad.Set_Fields("SchoolLedgerPendingDetail", false);
				rsLoad.Set_Fields("SchoolLedgerPostedDetail", false);
				rsLoad.Update();
			}
			
		}
		
		public static short LowMonthCalc(string x)
		{
			short LowMonthCalc = 0;
			if (x == "January")
			{
				LowMonthCalc = 1;
			}
			else if (x == "February")
			{
				LowMonthCalc = 2;
			}
			else if (x == "March")
			{
				LowMonthCalc = 3;
			}
			else if (x == "April")
			{
				LowMonthCalc = 4;
			}
			else if (x == "May")
			{
				LowMonthCalc = 5;
			}
			else if (x == "June")
			{
				LowMonthCalc = 6;
			}
			else if (x == "July")
			{
				LowMonthCalc = 7;
			}
			else if (x == "August")
			{
				LowMonthCalc = 8;
			}
			else if (x == "September")
			{
				LowMonthCalc = 9;
			}
			else if (x == "October")
			{
				LowMonthCalc = 10;
			}
			else if (x == "November")
			{
				LowMonthCalc = 11;
			}
			else if (x == "December")
			{
				LowMonthCalc = 12;
			}
			return LowMonthCalc;
		}

		public static short HighMonthCalc(string x)
		{
			short HighMonthCalc = 0;
			if (x == "January")
			{
				HighMonthCalc = 1;
			}
			else if (x == "February")
			{
				HighMonthCalc = 2;
			}
			else if (x == "March")
			{
				HighMonthCalc = 3;
			}
			else if (x == "April")
			{
				HighMonthCalc = 4;
			}
			else if (x == "May")
			{
				HighMonthCalc = 5;
			}
			else if (x == "June")
			{
				HighMonthCalc = 6;
			}
			else if (x == "July")
			{
				HighMonthCalc = 7;
			}
			else if (x == "August")
			{
				HighMonthCalc = 8;
			}
			else if (x == "September")
			{
				HighMonthCalc = 9;
			}
			else if (x == "October")
			{
				HighMonthCalc = 10;
			}
			else if (x == "November")
			{
				HighMonthCalc = 11;
			}
			else if (x == "December")
			{
				HighMonthCalc = 12;
			}
			return HighMonthCalc;
		}

		public static string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		public static string GetDebCredSQL(string strJrnlType, string strCredOrDeb, bool blnCorrect, bool blnPending, bool blnPayrollContract = false)
		{
			string GetDebCredSQL = "";
			if (strJrnlType == "A")
			{
				if (strCredOrDeb == "D")
				{
					if (blnCorrect)
					{
						if (blnPending)
						{
							GetDebCredSQL = "APJournalDetail.RCB = 'C' AND APJournalDetail.Amount < 0 AND APJournal.Status <> 'P'";
						}
						else
						{
							GetDebCredSQL = "APJournalDetail.RCB = 'C' AND APJournalDetail.Amount < 0 AND APJournal.Status = 'P'";
						}
					}
					else
					{
						if (blnPending)
						{
							GetDebCredSQL = "APJournalDetail.RCB <> 'C' AND APJournalDetail.Amount > 0 AND APJournal.Status <> 'P'";
						}
						else
						{
							GetDebCredSQL = "APJournalDetail.RCB <> 'C' AND APJournalDetail.Amount > 0 AND APJournal.Status = 'P'";
						}
					}
				}
				else
				{
					if (blnCorrect)
					{
						if (blnPending)
						{
							GetDebCredSQL = "APJournalDetail.RCB = 'C' AND APJournalDetail.Amount > 0 AND APJournal.Status <> 'P'";
						}
						else
						{
							GetDebCredSQL = "APJournalDetail.RCB = 'C' AND APJournalDetail.Amount > 0 AND APJournal.Status = 'P'";
						}
					}
					else
					{
						if (blnPending)
						{
							GetDebCredSQL = "APJournalDetail.RCB <> 'C' AND APJournalDetail.Amount < 0 AND APJournal.Status <> 'P'";
						}
						else
						{
							GetDebCredSQL = "APJournalDetail.RCB <> 'C' AND APJournalDetail.Amount < 0 AND APJournal.Status = 'P'";
						}
					}
				}
			}
			else
			{
				if (strCredOrDeb == "D")
				{
					if (blnCorrect)
					{
						if (blnPending)
						{
							GetDebCredSQL = "Status = 'E' AND Amount < 0 AND RCB = 'C'";
						}
						else
						{
							GetDebCredSQL = "Status = 'P' AND Amount < 0 AND RCB = 'C'";
						}
					}
					else
					{
						if (blnPending)
						{
							GetDebCredSQL = "Status = 'E' AND Amount > 0 AND RCB <> 'C'";
						}
						else
						{
							if (blnPayrollContract)
							{
								GetDebCredSQL = "Status = 'P' AND Amount < 0 AND RCB = 'E' AND Type = 'P'";
							}
							else
							{
								GetDebCredSQL = "Status = 'P' AND Amount > 0 AND RCB <> 'C' AND RCB <> 'E' AND RCB <> 'B'";
							}
						}
					}
				}
				else
				{
					if (blnCorrect)
					{
						if (blnPending)
						{
							GetDebCredSQL = "Status = 'E' AND Amount > 0 AND RCB = 'C'";
						}
						else
						{
							GetDebCredSQL = "Status = 'P' AND Amount > 0 AND RCB = 'C'";
						}
					}
					else
					{
						if (blnPending)
						{
							GetDebCredSQL = "Status = 'E' AND Amount < 0 AND RCB <> 'C'";
						}
						else
						{
							GetDebCredSQL = "Status = 'P' AND Amount < 0 AND RCB <> 'C' AND RCB <> 'E' AND RCB <> 'B'";
						}
					}
				}
			}
			return GetDebCredSQL;
		}

		private static string GetMonthlyBudgetField(ref short intPeriod)
		{
			string GetMonthlyBudgetField = "";
			switch (intPeriod)
			{
				case 1:
					{
						GetMonthlyBudgetField = "JanPercent";
						break;
					}
				case 2:
					{
						GetMonthlyBudgetField = "FebPercent";
						break;
					}
				case 3:
					{
						GetMonthlyBudgetField = "MarPercent";
						break;
					}
				case 4:
					{
						GetMonthlyBudgetField = "AprPercent";
						break;
					}
				case 5:
					{
						GetMonthlyBudgetField = "MayPercent";
						break;
					}
				case 6:
					{
						GetMonthlyBudgetField = "JunePercent";
						break;
					}
				case 7:
					{
						GetMonthlyBudgetField = "JulyPercent";
						break;
					}
				case 8:
					{
						GetMonthlyBudgetField = "AugPercent";
						break;
					}
				case 9:
					{
						GetMonthlyBudgetField = "SeptPercent";
						break;
					}
				case 10:
					{
						GetMonthlyBudgetField = "OctPercent";
						break;
					}
				case 11:
					{
						GetMonthlyBudgetField = "NovPercent";
						break;
					}
				case 12:
					{
						GetMonthlyBudgetField = "DecPercent";
						break;
					}
			}
			//end switch
			return GetMonthlyBudgetField;
		}

		public static bool AutoPostAllowed(AutoPostType aptType)
		{
			bool AutoPostAllowed = false;
			clsDRWrapper rs = new clsDRWrapper();
			AutoPostAllowed = false;
			try
			{
				// On Error GoTo ErrorHandler
				// kk09232014 trout-1115  Bypass check if fields have not been added
				rs.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Budgetary' AND COLUMN_NAME = 'AutoPostGJ'", "Budgetary");
				if (rs.RecordCount() == 0)
				{
					return AutoPostAllowed;
				}
				rs.OpenRecordset("SELECT * FROM Budgetary", "TWBD0000.vb1");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					switch (aptType)
					{
						case AutoPostType.aptAccountsPayable:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostAP"));
								break;
							}
						case AutoPostType.aptGeneralJournal:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostGJ"));
								break;
							}
						case AutoPostType.aptCashReceipts:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostCR"));
								break;
							}
						case AutoPostType.aptCashDisbursement:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostCD"));
								break;
							}
						case AutoPostType.aptEncumbrance:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostEN"));
								break;
							}
						case AutoPostType.aptAccountsPayableCorrections:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostAPCorr"));
								break;
							}
						case AutoPostType.aptCreditMemo:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostCM"));
								break;
							}
						case AutoPostType.aptReversingJournal:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostReversingJournals"));
								break;
							}
						case AutoPostType.aptBudgetTransfer:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostBudgetTransfer"));
								break;
							}
						case AutoPostType.aptCreateOpeningAdjustments:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostCreateOpeningAdjustments"));
								break;
							}
						case AutoPostType.aptARBills:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostARBills"));
								break;
							}
						case AutoPostType.aptCRDailyAudit:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostCRDailyAudit"));
								break;
							}
						case AutoPostType.aptFADepreciation:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostFADepreciation"));
								break;
							}
						case AutoPostType.aptMVRapidRenewal:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostMARR"));
								break;
							}
						case AutoPostType.aptPYProcesses:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostPYProcesses"));
								break;
							}
						case AutoPostType.aptCLLiens:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostCLLiens"));
								break;
							}
						case AutoPostType.aptCLTaxAcquired:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostCLTA"));
								break;
							}
						case AutoPostType.aptBLCommitment:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostBLCommitments"));
								break;
							}
						case AutoPostType.aptBLSupplemental:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostBLSupplementals"));
								break;
							}
						case AutoPostType.aptUTBills:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostUBBills"));
								break;
							}
						case AutoPostType.aptUTLiens:
							{
								AutoPostAllowed = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AutoPostUBLiens"));
								break;
							}
					}
					//end switch
				}
				return AutoPostAllowed;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				AutoPostAllowed = false;
			}
			return AutoPostAllowed;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public static object GetBankVariable(string strName)
		{
			object GetBankVariable = null;
			clsDRWrapper rsVariables = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsVariables.Reset();
				rsVariables.OpenRecordset("SELECT * FROM BankIndex", "CentralData", FCConvert.ToInt32(RecordsetTypeEnum.dbOpenDynaset), FCConvert.ToBoolean(0), FCConvert.ToInt32(FCRecordset.LockTypeEnum.dbOptimistic));
				if (rsVariables.EndOfFile())
				{
					GetBankVariable = string.Empty;
				}
				else
				{
					GetBankVariable = rsVariables.Get_Fields(strName);
				}

                if (GetBankVariable == null)
                {
                    GetBankVariable = "";
                }
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + ex.GetBaseException().Message, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetBankVariable;
		}

		public class StaticVariables
		{
			public string strLockedBy = "";
			public int lngNumberOfAccounts;
			public AcctsForCheckRec[] AcctsToPass = null;
			public bool gboolAutoInterest;
			public bool gboolPUCChartOfAccounts;
			public int lngSelectedJournal;
			public string User = "";
			public string strZeroDiv = "";
			//FC:FINAL:DSE:#600 auto-initialize members declared with the "As New" declaration
			//public clsDRWrapper SearchResults = new clsDRWrapper();
			public clsDRWrapper SearchResults_AutoInitialized;

			public clsDRWrapper SearchResults
			{
				get
				{
					if (SearchResults_AutoInitialized == null)
					{
						SearchResults_AutoInitialized = new clsDRWrapper();
					}
					return SearchResults_AutoInitialized;
				}
				set
				{
					SearchResults_AutoInitialized = value;
				}
			}

			public string strOpID = "";
			// Control Accounts
			public string strEncOffAccount = "";
			public string strPastYearEncControlAccount = "";
			public string strRevCtrAccount = "";
			public string strExpCtrAccount = "";
			public string strUnliqEncCtrAccount = "";
			public string strFundBalCtrAccount = "";
			public string strCredMemRecAccount = "";
			public string strAPCashAccount = "";
			public string strCRCashAccount = "";
			public string strDueToCtrAccount = "";
			public string strDueFromCtrAccount = "";
			public string strDueToFromCashFund = "";
			public string strEOYAPAccount = "";
			
			//private int intExpLen;
			//private int intLedgerLen;
			//private int intExpDeptLen;
			//private int intRevDeptLen;
			//private int intRevLen;
			//private int intExpDivisionLen;
			//private int intRevDivisionLen;
			//private int intExpObjLen;
			//private int intFundLen;
			//private int intLedgerAcctLen;
			//private int intLedgerSuffixLen;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
