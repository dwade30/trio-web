﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;
using TWSharedLibrary;
#if TWGNENTY
using TWGNENTY;


#elif TWMV0000
using TWMV0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmCentralPartyMerge.
	/// </summary>
	public partial class frmCentralPartyMerge : BaseForm
	{
		public frmCentralPartyMerge()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCentralPartyMerge InstancePtr
		{
			get
			{
				return (frmCentralPartyMerge)Sys.GetInstance(typeof(frmCentralPartyMerge));
			}
		}

		protected frmCentralPartyMerge _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		string strSearchIDs = "";
		clsDRWrapper rsPartyMatch = new clsDRWrapper();
		clsDRWrapper rsMatches = new clsDRWrapper();
		private int[] PartyMatchInfo = null;
		bool blnCustomerExists;
		int IDCol;
		int SelectCol;
		int InfoCol;
		int PartyIDCol;
		int lngTotalParties;
		int lngMergedParties;
		int intMatchCount;
		clsDRWrapper rsPartyIDs = new clsDRWrapper();

		private void cmdMatchSearch_Click(object sender, System.EventArgs e)
		{
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Retrieving Party Information");
			if (strSearchIDs != "")
			{
				rsPartyIDs.OpenRecordset("SELECT ID FROM Parties WHERE ID IN (" + strSearchIDs + ")", "CentralParties");
				lngTotalParties = rsPartyIDs.RecordCount();
				lngMergedParties = 0;
				UpdateProgress();
			}
			else
			{
				if (cmbSpecific.Text == "All")
				{
					rsPartyIDs.OpenRecordset("SELECT ID FROM Parties WHERE ISNULL(Merged, 0) = 0", "CentralParties");
				}
				else
				{
					if (blnCustomerExists)
					{
						rsPartyIDs.OpenRecordset("SELECT * FROM Parties WHERE ID = " + txtCustomerNumber.Text, "CentralParties");
					}
					else
					{
						MessageBox.Show("Customer Not Found", "Invalid Customer", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
			}
			if (rsPartyIDs.BeginningOfFile() != true && rsPartyIDs.EndOfFile() != true)
			{
				FindNextPossibleMatch();
			}
			else
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("No parties found to merge.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void cmdMatchSearch_Click()
		{
			cmdMatchSearch_Click(cmdMatchSearch, new System.EventArgs());
		}

		private void FindNextPossibleMatch()
		{
			float sPercThreshold;
			bool blnMatch = false;
			int intPossibleMerges = 0;
			float sPerc = 0;
			sPercThreshold = 92;
			// if >= this threshhold then probably a match
			if (rsPartyIDs.EndOfFile())
			{
				ShowWizardStep_2(0);
				return;
			}
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Searching for possible matches");
			do
			{
				//Application.DoEvents();
				intPossibleMerges = 0;
				PartyMatchInfo = new int[intPossibleMerges + 1];
				PartyMatchInfo[intPossibleMerges] = FCConvert.ToInt32(rsPartyIDs.Get_Fields_Int32("ID"));
				rsPartyMatch.OpenRecordset("SELECT * FROM Parties WHERE ID = " + rsPartyIDs.Get_Fields_Int32("ID"), "CentralParties");
				rsMatches.OpenRecordset("SELECT * FROM Parties WHERE PartyType = " + rsPartyMatch.Get_Fields_Int32("PartyType") + " AND Left(FirstName, 2) = '" + Strings.Left(FCConvert.ToString(rsPartyMatch.Get_Fields_String("FirstName")), 2) + "'", "CentralParties");
				do
				{
					//Application.DoEvents();
					blnMatch = false;
					// First compare entity type
					if (rsPartyMatch.Get_Fields_Int32("ID") != rsMatches.Get_Fields_Int32("ID"))
					{
						if (rsPartyMatch.Get_Fields_Int32("PartyType") != rsMatches.Get_Fields_Int32("PartyType"))
						{
							goto EndCheck;
						}
						else
						{
							if (FCConvert.ToInt32(rsPartyMatch.Get_Fields_Int32("PartyType")) != 1)
							{
								// check designation if both names have one
								if (Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("Designation"))) != "" && Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("Designation"))) != "")
								{
									if (Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("Designation")))) != Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("Designation")))))
									{
										goto EndCheck;
									}
								}
								if (Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("LastName"))) != "" || Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("LastName"))) != "")
								{
									// check middle names
									if (Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("MiddleName"))) != "" && Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("MiddleName"))) != "")
									{
										if (Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("MiddleName"))).Length != 1 || Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("MiddleName"))).Length != 1)
										{
											sPerc = FCConvert.ToSingle(DegreeOfSimilarity_6(Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("FirstName")))) + " " + Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("MiddleName")))) + " " + Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("LastName")))), Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("FirstName")))) + " " + Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("MiddleName")))) + " " + Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("LastName"))))));
										}
										else
										{
											if (Strings.Mid(Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("MiddleName")))), 1, 1) == Strings.Mid(Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("MiddleName")))), 1, 1))
											{
												sPerc = FCConvert.ToSingle(DegreeOfSimilarity_6(Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("FirstName")))) + " " + Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("LastName")))), Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("FirstName")))) + " " + Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("LastName"))))));
											}
											else
											{
												goto EndCheck;
											}
										}
									}
									else
									{
										sPerc = FCConvert.ToSingle(DegreeOfSimilarity_6(Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("FirstName")))) + " " + Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("LastName")))), Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("FirstName")))) + " " + Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("LastName"))))));
									}
									if (sPerc >= sPercThreshold)
									{
										blnMatch = true;
									}
								}
								else
								{
									// both have no last name
									sPerc = FCConvert.ToSingle(DegreeOfSimilarity_6(Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("FirstName")))), Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("FirstName"))))));
									if (sPerc >= sPercThreshold)
									{
										blnMatch = true;
									}
								}
							}
							else
							{
								// just compare first name
								if (Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("FirstName"))) != "" && Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("FirstName"))) != "")
								{
									sPerc = FCConvert.ToSingle(DegreeOfSimilarity_6(Strings.LCase(Strings.Trim(FCConvert.ToString(rsPartyMatch.Get_Fields_String("FirstName")))), Strings.LCase(Strings.Trim(FCConvert.ToString(rsMatches.Get_Fields_String("FirstName"))))));
									if (sPerc >= sPercThreshold)
									{
										blnMatch = true;
									}
								}
							}
						}
					}
					EndCheck:
					;
					if (blnMatch)
					{
						intPossibleMerges += 1;
						Array.Resize(ref PartyMatchInfo, intPossibleMerges + 1);
						PartyMatchInfo[intPossibleMerges] = FCConvert.ToInt32(rsMatches.Get_Fields_Int32("ID"));
					}
					rsMatches.MoveNext();
				}
				while (rsMatches.EndOfFile() != true);
				if (intPossibleMerges > 0)
				{
					rsPartyIDs.MoveNext();
					break;
				}
				else
				{
					if (rsPartyMatch.EndOfFile() != true && rsPartyMatch.BeginningOfFile() != true)
					{
						rsPartyMatch.Edit();
						rsPartyMatch.Set_Fields("Merged", true);
						rsPartyMatch.Update();
						lngMergedParties += 1;
						UpdateProgress();
					}
				}
				rsPartyIDs.MoveNext();
			}
			while (rsPartyIDs.EndOfFile() != true);
			frmWait.InstancePtr.Unload();
			if (intPossibleMerges > 0)
			{
				FillStep1();
			}
			else
			{
				MessageBox.Show("Merge process completed!", "Merge Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Unload();
			}
		}

		private void cmdMergeComplete_Click(object sender, System.EventArgs e)
		{
			CompleteMergeForParty();
		}

		private void CompleteMergeForParty()
		{
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			for (counter = 0; counter <= vsParties.Rows - 1; counter++)
			{
				modCentralPartyMerge.FlagPartyAsMerged(FCConvert.ToInt32(vsParties.TextMatrix(counter, IDCol)));
			}
			UpdateProgress();
			FindNextPossibleMatch();
		}

		private void cmdStep1ClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsParties.Rows - 1; counter++)
			{
				//Application.DoEvents();
				vsParties.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdStep1Next_Click(object sender, System.EventArgs e)
		{
			int counter;
			intMatchCount = 0;
			for (counter = 0; counter <= vsParties.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (FCConvert.CBool(vsParties.TextMatrix(counter, SelectCol)) == true)
				{
					intMatchCount += 1;
				}
			}
			if (intMatchCount <= 1)
			{
				MessageBox.Show("You must select 2 or more parties to continue the merge process.  If there are no parties to merge just click the Merge Complete button.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			MoveNext_2(1);
		}

		private void cmdStep1SelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsParties.Rows - 1; counter++)
			{
				vsParties.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void cmdStep2Back_Click(object sender, System.EventArgs e)
		{
			MoveBack_2(2);
		}

		private void cmdStep2Next_Click(object sender, System.EventArgs e)
		{
			int counter;
			int intSelected;
			intSelected = 0;
			for (counter = 0; counter <= vsSelectedParties.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (FCConvert.CBool(vsSelectedParties.TextMatrix(counter, SelectCol)) == true)
				{
					intSelected += 1;
				}
			}
			if (intSelected == 1)
			{
				MoveNext_2(2);
			}
			else
			{
				MessageBox.Show("You must select 1 party to be used as the main party in the merge.");
				return;
			}
		}

		private void cmdStep3Back_Click(object sender, System.EventArgs e)
		{
			MoveBack_2(3);
		}

		private void cmdStep3ClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsAddresses.Rows - 1; counter++)
			{
				//Application.DoEvents();
				vsAddresses.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdStep3Next_Click(object sender, System.EventArgs e)
		{
			int counter;
			cmdStep4Next.Text = "Finish";
			vsAddressTypes.Rows = 0;
			for (counter = 0; counter <= vsAddresses.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (FCConvert.CBool(vsAddresses.TextMatrix(counter, SelectCol)) == true)
				{
					vsAddressTypes.Rows += 1;
					vsAddressTypes.TextMatrix(vsAddressTypes.Rows - 1, IDCol, vsAddresses.TextMatrix(counter, IDCol));
					vsAddressTypes.TextMatrix(vsAddressTypes.Rows - 1, InfoCol, vsAddresses.TextMatrix(counter, InfoCol));
					vsAddressTypes.TextMatrix(vsAddressTypes.Rows - 1, SelectCol, FCConvert.ToString(false));
				}
			}
			vsAddressTypes.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
			//FC:FINAL:MSH - issue #1241: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
			//vsAddressTypes.AutoSize(0, vsAddressTypes.Cols - 1);
			for (int i = 0; i < vsAddressTypes.RowCount; i++)
			{
				int rowIndex = vsAddressTypes.GetFlexRowIndex(i);
				int lineMultiplier = vsAddressTypes.TextMatrix(rowIndex, InfoCol).Split('\n').Length;
				if (lineMultiplier > 0)
				{
					vsAddressTypes.RowHeight(rowIndex, lineMultiplier * 300);
				}
			}
			if (vsAddressTypes.Rows > 1)
			{
				cmdStep4Next.Text = "Next >";
			}
			MoveNext_2(3);
		}

		private void cmdStep3SelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsAddresses.Rows - 1; counter++)
			{
				//Application.DoEvents();
				vsAddresses.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void cmdStep4Back_Click(object sender, System.EventArgs e)
		{
			MoveBack_2(4);
		}

		private void cmdStep4ClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsPhones.Rows - 1; counter++)
			{
				//Application.DoEvents();
				vsPhones.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdStep4Next_Click(object sender, System.EventArgs e)
		{
			MoveNext_2(4);
		}

		private void cmdStep4SelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsPhones.Rows - 1; counter++)
			{
				//Application.DoEvents();
				vsPhones.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void cmdStep5Back_Click(object sender, System.EventArgs e)
		{
			MoveBack_2(5);
		}

		private void cmdStep5ClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsContacts.Rows - 1; counter++)
			{
				//Application.DoEvents();
				vsContacts.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdStep5Next_Click(object sender, System.EventArgs e)
		{
			MoveNext_2(5);
		}

		private void SaveMergedPartyInfo()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			int counter;
			// vbPorter upgrade warning: lngMergePartyID As int	OnWrite(string)
			int lngMergePartyID = 0;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Saving Merged Party Info");
			for (counter = 0; counter <= vsSelectedParties.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (FCConvert.CBool(vsSelectedParties.TextMatrix(counter, SelectCol)) == true)
				{
					lngMergePartyID = FCConvert.ToInt32(vsSelectedParties.TextMatrix(counter, IDCol));
					break;
				}
			}
			if (vsAddresses.Rows > 1)
			{
				for (counter = 0; counter <= vsAddresses.Rows - 1; counter++)
				{
					//Application.DoEvents();
					if (FCConvert.CBool(vsAddresses.TextMatrix(counter, SelectCol)) == false)
					{
						rsUpdate.Execute("DELETE Addresses WHERE ID = " + vsAddresses.TextMatrix(counter, IDCol), "CentralParties");
					}
				}
			}
			if (vsPhones.Rows > 1)
			{
				for (counter = 0; counter <= vsPhones.Rows - 1; counter++)
				{
					//Application.DoEvents();
					if (FCConvert.CBool(vsPhones.TextMatrix(counter, SelectCol)) == false)
					{
						rsUpdate.Execute("DELETE CentralPhoneNumbers WHERE ID = " + vsPhones.TextMatrix(counter, IDCol), "CentralParties");
					}
				}
			}
			if (vsContacts.Rows > 1)
			{
				for (counter = 0; counter <= vsContacts.Rows - 1; counter++)
				{
					//Application.DoEvents();
					if (FCConvert.CBool(vsContacts.TextMatrix(counter, SelectCol)) == false)
					{
						modCentralPartyMerge.RemoveContactFromParty_2(FCConvert.ToInt32(FCConvert.ToDouble(vsContacts.TextMatrix(counter, IDCol))));
					}
				}
			}
			if (vsAddressTypes.Rows > 1)
			{
				for (counter = 0; counter <= vsAddressTypes.Rows - 1; counter++)
				{
					//Application.DoEvents();
					rsUpdate.OpenRecordset("SELECT * FROM Addresses WHERE ID = " + vsAddressTypes.TextMatrix(counter, IDCol), "CentralParties");
					rsUpdate.Edit();
					if (FCConvert.CBool(vsAddressTypes.TextMatrix(counter, SelectCol)) == true)
					{
						rsUpdate.Set_Fields("AddressType", "Primary");
						rsUpdate.Set_Fields("ProgModule", "");
						rsUpdate.Set_Fields("Seasonal", false);
					}
					else
					{
						rsUpdate.Set_Fields("AddressType", "Override");
						// Select Case vsAddressTypes.TextMatrix(counter, SelectCol)
						// Case "Accounts Receivable"
						// rsUpdate.Fields("ProgModule") = "AR"
						// Case "Clerk"
						// rsUpdate.Fields("ProgModule") = "CK"
						// Case "Code Enforcement"
						// rsUpdate.Fields("ProgModule") = "CE"
						// Case "Motor Vehicle"
						// rsUpdate.Fields("ProgModule") = "MV"
						// Case "Real Estate"
						// rsUpdate.Fields("ProgModule") = "RE"
						// Case "Perosnal Property"
						// rsUpdate.Fields("ProgModule") = "PP"
						// Case "Utility Billing"
						// rsUpdate.Fields("ProgModule") = "UT"
						// End Select
						rsUpdate.Set_Fields("Seasonal", false);
					}
					rsUpdate.Update();
				}
			}
			else
			{
				if (vsAddresses.Rows == 1)
				{
					rsUpdate.OpenRecordset("SELECT * FROM Addresses WHERE ID = " + vsAddresses.TextMatrix(0, IDCol), "CentralParties");
					rsUpdate.Edit();
					rsUpdate.Set_Fields("AddressType", "Primary");
					rsUpdate.Set_Fields("ProgModule", "");
					rsUpdate.Set_Fields("Seasonal", false);
					rsUpdate.Update();
				}
			}
			for (counter = 0; counter <= vsParties.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (lngMergePartyID != FCConvert.ToInt32(FCConvert.ToDouble(vsParties.TextMatrix(counter, IDCol))) && FCConvert.CBool(vsParties.TextMatrix(counter, SelectCol)) == true)
				{
					modCentralPartyMerge.ChangePartyIDInOtherModules_6(lngMergePartyID, FCConvert.ToInt32(FCConvert.ToDouble(vsParties.TextMatrix(counter, IDCol))));
					modCentralPartyMerge.ChangePartyIDInCentralParties_6(lngMergePartyID, FCConvert.ToInt32(FCConvert.ToDouble(vsParties.TextMatrix(counter, IDCol))));
					modCentralPartyMerge.RemovePartyFromCentralParties_2(FCConvert.ToInt32(FCConvert.ToDouble(vsParties.TextMatrix(counter, IDCol))));
					lngTotalParties -= 1;
				}
			}
			modCentralPartyMerge.RenumberPhoneOrderForParty(lngMergePartyID);
			lngMergedParties += 1;
			modCentralPartyMerge.FlagPartyAsMerged(lngMergePartyID);
			for (counter = vsParties.Rows - 1; counter >= 0; counter--)
			{
				//Application.DoEvents();
				if (FCConvert.CBool(vsParties.TextMatrix(counter, SelectCol)) == true)
				{
					vsParties.RemoveItem(counter);
				}
			}
			frmWait.InstancePtr.Unload();
			UpdateProgress();
		}

		private void cmdStep5SelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsContacts.Rows - 1; counter++)
			{
				//Application.DoEvents();
				vsContacts.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void cmdStep6Back_Click(object sender, System.EventArgs e)
		{
			MoveBack_2(6);
		}

		private void cmdStep6Next_Click(object sender, System.EventArgs e)
		{
			int counter;
			int intPrimaryCount;
			intPrimaryCount = 0;
			for (counter = 0; counter <= vsAddressTypes.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (FCConvert.CBool(vsAddressTypes.TextMatrix(counter, SelectCol)) == true)
				{
					// MsgBox "You must select a type for each address before you may continue.", vbInformation, "Invalid Type"
					// Exit Sub
					// ElseIf vsAddressTypes.TextMatrix(counter, SelectCol) = "Primary" Then
					intPrimaryCount += 1;
				}
			}
			if (intPrimaryCount == 0)
			{
				MessageBox.Show("You must select 1 address to be the primary address for the party.", "No Primary", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (intPrimaryCount > 1)
			{
				MessageBox.Show("You may only select a single address to be primary.  All others must be an alternate.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			MoveNext_2(6);
		}

		private void frmCentralPartyMerge_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalRoutines.FormExist(this))
			{
				return;
			}
			this.Refresh();
			if (strSearchIDs != "")
			{
				cmdMatchSearch_Click();
			}
		}

		private void frmCentralPartyMerge_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCentralPartyMerge properties;
			//frmCentralPartyMerge.FillStyle	= 0;
			//frmCentralPartyMerge.ScaleWidth	= 11670;
			//frmCentralPartyMerge.ScaleHeight	= 9390;
			//frmCentralPartyMerge.LinkTopic	= "Form2";
			//frmCentralPartyMerge.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsParties = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// SetTRIOColors Me
			blnCustomerExists = false;
			IDCol = 2;
			PartyIDCol = 1;
			SelectCol = 0;
			InfoCol = 3;
			// vsParties.ColHidden(IDCol) = True
			vsParties.ColHidden(PartyIDCol, true);
			vsParties.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsParties.ColWidth(SelectCol, FCConvert.ToInt32(vsParties.WidthOriginal * 0.1));
			// vsSelectedParties.ColHidden(IDCol) = True
			vsSelectedParties.ColHidden(PartyIDCol, true);
			vsSelectedParties.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsSelectedParties.ColWidth(SelectCol, FCConvert.ToInt32(vsSelectedParties.WidthOriginal * 0.1));
			vsAddresses.ColHidden(IDCol, true);
			vsAddresses.ColHidden(PartyIDCol, true);
			vsAddresses.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsAddresses.ColWidth(SelectCol, FCConvert.ToInt32(vsAddresses.WidthOriginal * 0.1));
			vsContacts.ColHidden(IDCol, true);
			vsContacts.ColHidden(PartyIDCol, true);
			vsContacts.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsContacts.ColWidth(SelectCol, FCConvert.ToInt32(vsContacts.WidthOriginal * 0.1));
			vsPhones.ColHidden(IDCol, true);
			vsPhones.ColHidden(PartyIDCol, true);
			vsPhones.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsPhones.ColWidth(SelectCol, FCConvert.ToInt32(vsPhones.WidthOriginal * 0.1));
			vsAddressTypes.ColHidden(IDCol, true);
			vsAddressTypes.ColHidden(PartyIDCol, true);
			// vsAddressTypes.ColComboList(SelectCol) = "Primary|Alternate"
			vsAddressTypes.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsAddressTypes.ColWidth(SelectCol, FCConvert.ToInt32(vsAddressTypes.WidthOriginal * 0.1));
			vsParties.Editable = FCGrid.EditableSettings.flexEDNone;
			vsSelectedParties.Editable = FCGrid.EditableSettings.flexEDNone;
			vsAddresses.Editable = FCGrid.EditableSettings.flexEDNone;
			vsContacts.Editable = FCGrid.EditableSettings.flexEDNone;
			vsPhones.Editable = FCGrid.EditableSettings.flexEDNone;
			if (strSearchIDs == "")
			{
				rsParties.OpenRecordset("SELECT COUNT(ID) as RecordTotal FROM Parties", "CentralParties");
				// TODO: Field [RecordTotal] not found!! (maybe it is an alias?)
				lngTotalParties = FCConvert.ToInt32(rsParties.Get_Fields("RecordTotal"));
				rsParties.OpenRecordset("SELECT COUNT(ID) as RecordTotal FROM Parties WHERE ISNULL(Merged, 0) = 1", "CentralParties");
				// TODO: Field [RecordTotal] not found!! (maybe it is an alias?)
				lngMergedParties = FCConvert.ToInt32(rsParties.Get_Fields("RecordTotal"));
				UpdateProgress();
			}
		}

		private void UpdateProgress()
		{
			// Frame1.Visible = True
			if (lngMergedParties > lngTotalParties)
				lngMergedParties = lngTotalParties;
			prgDataMerged.Maximum = lngTotalParties;
			prgDataMerged.Value = lngMergedParties;
			lblPartiesChecked.Text = "Parties Checked: " + FCConvert.ToString(modGNBas.Round_8((FCConvert.ToDouble(lngMergedParties) / lngTotalParties) * 100, 2)) + "%";
		}

		private void frmCentralPartyMerge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCentralPartyMerge_Resize(object sender, System.EventArgs e)
		{
			vsParties.ColWidth(SelectCol, FCConvert.ToInt32(vsParties.WidthOriginal * 0.1));
			vsSelectedParties.ColWidth(SelectCol, FCConvert.ToInt32(vsSelectedParties.WidthOriginal * 0.1));
			vsAddresses.ColWidth(SelectCol, FCConvert.ToInt32(vsAddresses.WidthOriginal * 0.1));
			vsContacts.ColWidth(SelectCol, FCConvert.ToInt32(vsContacts.WidthOriginal * 0.1));
			vsPhones.ColWidth(SelectCol, FCConvert.ToInt32(vsPhones.WidthOriginal * 0.1));
			vsAddressTypes.ColWidth(SelectCol, FCConvert.ToInt32(vsAddressTypes.WidthOriginal * 0.25));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSpecific.Enabled = false;
		}

		private void optSpecific_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSpecific.Enabled = true;
		}

		private void txtCustomerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCustomerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtCustomerNumber.Text) && Conversion.Val(txtCustomerNumber.Text) > 0)
			{
				GetPartyInfo_2(FCConvert.ToInt32(FCConvert.ToDouble(txtCustomerNumber.Text)));
			}
			else
			{
				ClearPartyInfo();
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(ref lngID);
			}
		}

		public void ClearPartyInfo()
		{
			lblCustomerInfo.Text = "";
			blnCustomerExists = false;
		}

		public void GetPartyInfo_2(int intPartyID)
		{
			GetPartyInfo(ref intPartyID);
		}

		public void GetPartyInfo(ref int intPartyID)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			pInfo = pCont.GetParty(intPartyID);
			if (!(pInfo == null))
			{
				blnCustomerExists = true;
				lblCustomerInfo.Text = pInfo.FullName;
				pAdd = pInfo.GetAddress("AR", intPartyID);
				if (!(pAdd == null))
				{
					lblCustomerInfo.Text = lblCustomerInfo.Text + "\r\n" + pAdd.GetFormattedAddress();
				}
			}
		}

		public void Init(string strIDs = "")
		{
			strSearchIDs = strIDs;
			this.Show(App.MainForm);
		}

		private double DegreeOfSimilarity_6(string s, string t)
		{
			return DegreeOfSimilarity(s, ref t);
		}

		private double DegreeOfSimilarity(string s, ref string t)
		{
			double DegreeOfSimilarity = 0;
			double lcs;
			double ms;
			if (s == "" || t == "")
			{
				DegreeOfSimilarity = 0;
				return DegreeOfSimilarity;
			}
			if (s == t)
			{
				DegreeOfSimilarity = 100;
				return DegreeOfSimilarity;
			}
			lcs = LCSLength(s, t);
			ms = (s.Length + t.Length) / 2.0;
			DegreeOfSimilarity = (lcs * 100) / ms;
			return DegreeOfSimilarity;
		}
		// <summary>
		// gets longest common string length
		// </summary>
		// <param name="s1"></param>
		// <param name="s2"></param>
		// <returns></returns>
		// <remarks></remarks>
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short LCSLength(string s1, string s2)
		{
			short LCSLength = 0;
			int i;
			int j;
			try
			{
				// On Error GoTo ErrorHandler
				int m;
				m = s1.Length;
				int n;
				n = s2.Length;
				int[,] matrix = new int[100 + 1, 100 + 1];
				for (i = 0; i <= m - 1; i++)
				{
					matrix[i, 0] = 0;
				}
				// i
				for (j = 0; j <= n - 1; j++)
				{
					matrix[0, j] = 0;
				}
				// j
				if (Strings.Mid(s1, 1, 1) == Strings.Mid(s2, 1, 1))
				{
					matrix[0, 0] = 1;
				}
				for (i = 1; i <= m - 1; i++)
				{
					for (j = 1; j <= n - 1; j++)
					{
						if (Strings.Mid(s1, i + 1, 1) == Strings.Mid(s2, j + 1, 1))
						{
							matrix[i, j] = matrix[i - 1, j - 1] + 1;
						}
						else if (matrix[i - 1, j] >= matrix[i, j - 1])
						{
							matrix[i, j] = matrix[i - 1, j];
						}
						else
						{
							matrix[i, j] = matrix[i, j - 1];
						}
					}
					// j
				}
				// i
				LCSLength = FCConvert.ToInt16(matrix[m - 1, n - 1]);
				return LCSLength;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				LCSLength = 0;
			}
			return LCSLength;
		}

		private void FillStep1()
		{
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strAddr = "";
			ShowWizardStep_2(1);
			cmdMergeComplete.Enabled = false;
			cmdStep1Next.Enabled = false;
			vsParties.Rows = 0;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading possible matches", true, Information.UBound(PartyMatchInfo, 1), true);
			for (counter = 0; counter <= Information.UBound(PartyMatchInfo, 1); counter++)
			{
				//Application.DoEvents();
				rsTemp.OpenRecordset("SELECT * FROM PartyAndAddressView WHERE ID = " + FCConvert.ToString(PartyMatchInfo[counter]), "CentralParties");
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					vsParties.Rows += 1;
					vsParties.TextMatrix(vsParties.Rows - 1, IDCol, FCConvert.ToString(rsTemp.Get_Fields_Int32("ID")));
					vsParties.TextMatrix(vsParties.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsParties.TextMatrix(vsParties.Rows - 1, PartyIDCol, FCConvert.ToString(rsTemp.Get_Fields_Int32("partyid")));
					strAddr = FCConvert.ToString(rsTemp.Get_Fields_String("FullName"));
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1"))) != "")
					{
						strAddr += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
					}
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2"))) != "")
					{
						strAddr += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
					}
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3"))) != "")
					{
						strAddr += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
					}
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("City"))) != "" || Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("State"))) != "" || Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Zip"))) != "")
					{
						strAddr += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Zip")));
					}
					vsParties.TextMatrix(vsParties.Rows - 1, InfoCol, strAddr);
					frmWait.InstancePtr.IncrementProgress();
				}
			}
			vsParties.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
			//FC:FINAL:MSH - issue #1241: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
			//vsParties.AutoSize(0, vsParties.Cols-1);
			for (int i = 0; i < vsParties.RowCount; i++)
			{
				int rowIndex = vsParties.GetFlexRowIndex(i);
				int lineMultiplier = vsParties.TextMatrix(rowIndex, InfoCol).Split('\n').Length;
				if (lineMultiplier > 0)
				{
					vsParties.RowHeight(rowIndex, lineMultiplier * 300);
				}
			}
			frmWait.InstancePtr.Unload();
			cmdMergeComplete.Enabled = true;
			cmdStep1Next.Enabled = true;
			cmdStep1SelectAll.Focus();
		}

		private bool FillStep2()
		{
			bool FillStep2 = false;
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strAddr = "";
			cmdMergeComplete.Enabled = false;
			cmdStep1Next.Enabled = false;
			vsSelectedParties.Rows = 0;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading selected party information", true, intMatchCount, true);
			for (counter = 0; counter <= vsParties.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (FCConvert.CBool(vsParties.TextMatrix(counter, SelectCol)) == true)
				{
					vsSelectedParties.Rows += 1;
					vsSelectedParties.TextMatrix(vsSelectedParties.Rows - 1, IDCol, vsParties.TextMatrix(counter, IDCol));
					vsSelectedParties.TextMatrix(vsSelectedParties.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsSelectedParties.TextMatrix(vsSelectedParties.Rows - 1, InfoCol, vsParties.TextMatrix(counter, InfoCol));
					rsTemp.MoveNext();
					frmWait.InstancePtr.IncrementProgress();
				}
			}
			if (vsSelectedParties.Rows > 1)
			{
				vsSelectedParties.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
				//FC:FINAL:MSH - issue #1241: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
				//vsSelectedParties.AutoSize(0, vsSelectedParties.Cols-1);
				for (int i = 0; i < vsSelectedParties.RowCount; i++)
				{
					int rowIndex = vsSelectedParties.GetFlexRowIndex(i);
					int lineMultiplier = vsSelectedParties.TextMatrix(rowIndex, InfoCol).Split('\n').Length;
					if (lineMultiplier > 0)
					{
						vsSelectedParties.RowHeight(rowIndex, lineMultiplier * 300);
					}
				}
				frmWait.InstancePtr.Unload();
				ShowWizardStep_2(2);
				FillStep2 = true;
			}
			return FillStep2;
		}

		private bool FillStep3()
		{
			bool FillStep3 = false;
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strAddr = "";
			cmdStep2Back.Enabled = false;
			cmdStep2Next.Enabled = false;
			vsAddresses.Rows = 0;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading address information", true, intMatchCount, true);
			for (counter = 0; counter <= vsSelectedParties.Rows - 1; counter++)
			{
				rsTemp.OpenRecordset("SELECT * FROM Addresses WHERE PartyID = " + vsSelectedParties.TextMatrix(counter, IDCol), "CentralParties");
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					do
					{
						//Application.DoEvents();
						vsAddresses.Rows += 1;
						vsAddresses.TextMatrix(vsAddresses.Rows - 1, IDCol, FCConvert.ToString(rsTemp.Get_Fields_Int32("ID")));
						vsAddresses.TextMatrix(vsAddresses.Rows - 1, PartyIDCol, FCConvert.ToString(rsTemp.Get_Fields_Int32("PartyID")));
						vsAddresses.TextMatrix(vsAddresses.Rows - 1, SelectCol, FCConvert.ToString(false));
						strAddr = "";
						if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1"))) != "")
						{
							if (strAddr == "")
							{
								strAddr = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
							}
							else
							{
								strAddr += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
							}
						}
						if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2"))) != "")
						{
							if (strAddr == "")
							{
								strAddr = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
							}
							else
							{
								strAddr += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
							}
						}
						if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3"))) != "")
						{
							if (strAddr == "")
							{
								strAddr = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
							}
							else
							{
								strAddr += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
							}
						}
						if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("City"))) != "" || Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("State"))) != "" || Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Zip"))) != "")
						{
							if (strAddr == "")
							{
								strAddr = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Zip")));
							}
							else
							{
								strAddr += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Zip")));
							}
						}
						vsAddresses.TextMatrix(vsAddresses.Rows - 1, InfoCol, strAddr);
						rsTemp.MoveNext();
					}
					while (rsTemp.EndOfFile() != true);
				}
				frmWait.InstancePtr.IncrementProgress();
			}
			if (vsAddresses.Rows > 1)
			{
				vsAddresses.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
				//FC:FINAL:MSH - issue #1241: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
				//vsAddresses.AutoSize(0, vsAddresses.Cols-1);
				for (int i = 0; i < vsAddresses.RowCount; i++)
				{
					int rowIndex = vsAddresses.GetFlexRowIndex(i);
					int lineMultiplier = vsAddresses.TextMatrix(rowIndex, InfoCol).Split('\n').Length;
					if (lineMultiplier > 0)
					{
						vsAddresses.RowHeight(rowIndex, lineMultiplier * 300);
					}
				}
				frmWait.InstancePtr.Unload();
				ShowWizardStep_2(3);
				FillStep3 = true;
			}
			return FillStep3;
		}

		private void MoveBack_2(short intStep)
		{
			MoveBack(ref intStep);
		}

		private void MoveBack(ref short intStep)
		{
			if (intStep == 2)
			{
				ShowWizardStep_2(1);
			}
			else if (intStep == 3)
			{
				ShowWizardStep_2(2);
			}
			else if (intStep == 4)
			{
				if (vsAddresses.Rows > 1)
				{
					ShowWizardStep_2(3);
				}
				else
				{
					ShowWizardStep_2(2);
				}
			}
			else if (intStep == 5)
			{
				if (vsPhones.Rows > 1)
				{
					ShowWizardStep_2(4);
				}
				else if (vsAddresses.Rows > 1)
				{
					ShowWizardStep_2(3);
				}
				else
				{
					ShowWizardStep_2(2);
				}
			}
			else if (intStep == 6)
			{
				if (vsContacts.Rows > 1)
				{
					ShowWizardStep_2(5);
				}
				else if (vsPhones.Rows > 1)
				{
					ShowWizardStep_2(4);
				}
				else if (vsAddresses.Rows > 1)
				{
					ShowWizardStep_2(3);
				}
				else
				{
					ShowWizardStep_2(2);
				}
			}
		}

		private void MoveNext_2(short intStep)
		{
			MoveNext(ref intStep);
		}

		private void MoveNext(ref short intStep)
		{
			if (intStep <= 1)
			{
				if (FillStep2())
				{
					return;
				}
			}
			if (intStep <= 2)
			{
				if (FillStep3())
				{
					return;
				}
			}
			if (intStep <= 3)
			{
				if (FillStep4())
				{
					return;
				}
			}
			if (intStep <= 4)
			{
				if (FillStep5())
				{
					return;
				}
			}
			if (intStep <= 5)
			{
				if (FillStep6())
				{
					return;
				}
			}
			if (intStep <= 6)
			{
				SaveMergedPartyInfo();
				if (vsParties.Rows == 0)
				{
					FindNextPossibleMatch();
				}
				else
				{
					ShowWizardStep_2(1);
				}
			}
		}

		private bool FillStep4()
		{
			bool FillStep4 = false;
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			cmdStep3Back.Enabled = false;
			cmdStep3Next.Enabled = false;
			vsPhones.Rows = 0;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading phone information", true, intMatchCount, true);
			for (counter = 0; counter <= vsSelectedParties.Rows - 1; counter++)
			{
				//Application.DoEvents();
				rsTemp.OpenRecordset("SELECT * FROM CentralPhoneNumbers WHERE PartyID = " + vsSelectedParties.TextMatrix(counter, IDCol), "CentralParties");
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					do
					{
						//Application.DoEvents();
						vsPhones.Rows += 1;
						vsPhones.TextMatrix(vsPhones.Rows - 1, IDCol, FCConvert.ToString(rsTemp.Get_Fields_Int32("ID")));
						vsPhones.TextMatrix(vsPhones.Rows - 1, SelectCol, FCConvert.ToString(false));
						if (FCConvert.ToString(rsTemp.Get_Fields_String("Extension")) == "")
						{
							vsPhones.TextMatrix(vsPhones.Rows - 1, InfoCol, rsTemp.Get_Fields_String("Description") + "\r\n" + rsTemp.Get_Fields_String("PhoneNumber"));
						}
						else
						{
							vsPhones.TextMatrix(vsPhones.Rows - 1, InfoCol, rsTemp.Get_Fields_String("Description") + "\r\n" + rsTemp.Get_Fields_String("PhoneNumber") + " X" + rsTemp.Get_Fields_String("Extension"));
						}
						rsTemp.MoveNext();
					}
					while (rsTemp.EndOfFile() != true);
				}
				frmWait.InstancePtr.IncrementProgress();
			}
			if (vsPhones.Rows > 1)
			{
				vsPhones.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
				//FC:FINAL:MSH - issue #1241: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
				//vsPhones.AutoSize(0, vsPhones.Cols-1);
				for (int i = 0; i < vsPhones.RowCount; i++)
				{
					int rowIndex = vsPhones.GetFlexRowIndex(i);
					int lineMultiplier = vsPhones.TextMatrix(rowIndex, InfoCol).Split('\n').Length;
					if (lineMultiplier > 0)
					{
						vsPhones.RowHeight(rowIndex, lineMultiplier * 300);
					}
				}
				frmWait.InstancePtr.Unload();
				ShowWizardStep_2(4);
				FillStep4 = true;
			}
			return FillStep4;
		}

		private bool FillStep5()
		{
			bool FillStep5 = false;
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsPhone = new clsDRWrapper();
			string strTemp = "";
			cmdStep4Back.Enabled = false;
			cmdStep4Next.Enabled = false;
			vsContacts.Rows = 0;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading contact information", true, intMatchCount, true);
			for (counter = 0; counter <= vsSelectedParties.Rows - 1; counter++)
			{
				rsTemp.OpenRecordset("SELECT * FROM Contacts WHERE PartyID = " + vsSelectedParties.TextMatrix(counter, IDCol), "CentralParties");
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					do
					{
						//Application.DoEvents();
						vsContacts.Rows += 1;
						vsContacts.TextMatrix(vsContacts.Rows - 1, IDCol, FCConvert.ToString(rsTemp.Get_Fields_Int32("ID")));
						vsContacts.TextMatrix(vsContacts.Rows - 1, SelectCol, FCConvert.ToString(false));
						strTemp = FCConvert.ToString(rsTemp.Get_Fields_String("Description"));
						if (Strings.Trim(strTemp) == "")
						{
							strTemp = FCConvert.ToString(rsTemp.Get_Fields_String("Name"));
						}
						else
						{
							strTemp += "\r\n" + rsTemp.Get_Fields_String("Name");
						}
						rsPhone.OpenRecordset("SELECT * FROM ContactPhoneNumbers WHERE ContactID = " + rsTemp.Get_Fields_Int32("ID") + " ORDER BY PhoneOrder", "CentralParties");
						if (rsPhone.EndOfFile() != true && rsPhone.BeginningOfFile() != true)
						{
							if (Strings.Trim(FCConvert.ToString(rsPhone.Get_Fields_String("Extension"))) == "")
							{
								strTemp += "\r\n" + rsPhone.Get_Fields_String("PhoneNumber");
							}
							else
							{
								strTemp += "\r\n" + rsPhone.Get_Fields_String("PhoneNumber") + " X" + rsPhone.Get_Fields_String("Extension");
							}
						}
						vsContacts.TextMatrix(vsContacts.Rows - 1, InfoCol, strTemp);
						rsTemp.MoveNext();
					}
					while (rsTemp.EndOfFile() != true);
				}
				frmWait.InstancePtr.IncrementProgress();
			}
			if (vsContacts.Rows > 1)
			{
				vsContacts.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
				//FC:FINAL:MSH - issue #1241: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
				//vsContacts.AutoSize(0, vsContacts.Cols-1);
				for (int i = 0; i < vsContacts.RowCount; i++)
				{
					int rowIndex = vsContacts.GetFlexRowIndex(i);
					int lineMultiplier = vsContacts.TextMatrix(rowIndex, InfoCol).Split('\n').Length;
					if (lineMultiplier > 0)
					{
						vsContacts.RowHeight(rowIndex, lineMultiplier * 300);
					}
				}
				frmWait.InstancePtr.Unload();
				ShowWizardStep_2(5);
				FillStep5 = true;
			}
			return FillStep5;
		}

		private bool FillStep6()
		{
			bool FillStep6 = false;
			if (vsAddressTypes.Rows > 1)
			{
				ShowWizardStep_2(6);
				frmWait.InstancePtr.Unload();
				FillStep6 = true;
			}
			return FillStep6;
		}

		private void vsAddresses_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsAddresses.Row >= 0)
			{
				if (FCConvert.CBool(vsAddresses.TextMatrix(vsAddresses.Row, SelectCol)) == false)
				{
					vsAddresses.TextMatrix(vsAddresses.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsAddresses.TextMatrix(vsAddresses.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsAddresses_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
			{
				e.KeyChar = (char)0;
				if (FCConvert.CBool(vsAddresses.TextMatrix(vsAddresses.Row, SelectCol)) == false)
				{
					vsAddresses.TextMatrix(vsAddresses.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsAddresses.TextMatrix(vsAddresses.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsAddressTypes_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsAddressTypes.Col != SelectCol)
			{
				vsAddressTypes.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				vsAddressTypes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			int counter;
			int lngRow;
			lngRow = vsAddressTypes.Row;
			if (vsAddressTypes.Rows >= 0)
			{
				if (FCConvert.CBool(vsAddressTypes.TextMatrix(lngRow, SelectCol)) == false)
				{
					vsAddressTypes.TextMatrix(lngRow, SelectCol, FCConvert.ToString(true));
					for (counter = 0; counter <= vsAddressTypes.Rows - 1; counter++)
					{
						//Application.DoEvents();
						if (counter != lngRow)
						{
							vsAddressTypes.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
						}
					}
				}
				else
				{
					vsAddressTypes.TextMatrix(vsAddressTypes.Rows, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsAddressTypes_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int counter;
			int lngRow;
			lngRow = vsAddressTypes.Row;
			if (e.KeyChar == 13)
			{
				e.KeyChar = (char)0;
				if (FCConvert.CBool(vsAddressTypes.TextMatrix(lngRow, SelectCol)) == false)
				{
					vsAddressTypes.TextMatrix(lngRow, SelectCol, FCConvert.ToString(true));
					for (counter = 0; counter <= vsAddressTypes.Rows - 1; counter++)
					{
						//Application.DoEvents();
						if (counter != lngRow)
						{
							vsAddressTypes.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
						}
					}
				}
				else
				{
					vsAddressTypes.TextMatrix(vsAddressTypes.Rows, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsAddressTypes_RowColChange(object sender, System.EventArgs e)
		{
			if (vsAddressTypes.Col != SelectCol)
			{
				vsAddressTypes.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				vsAddressTypes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
		}

		private void vsContacts_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsContacts.Row >= 0)
			{
				if (FCConvert.CBool(vsContacts.TextMatrix(vsContacts.Row, SelectCol)) == false)
				{
					vsContacts.TextMatrix(vsContacts.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsContacts.TextMatrix(vsContacts.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsContacts_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
			{
				e.KeyChar = (char)0;
				if (FCConvert.CBool(vsContacts.TextMatrix(vsContacts.Row, SelectCol)) == false)
				{
					vsContacts.TextMatrix(vsContacts.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsContacts.TextMatrix(vsContacts.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsParties_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsParties.Row >= 0)
			{
				if (FCConvert.CBool(vsParties.TextMatrix(vsParties.Row, SelectCol)) == false)
				{
					vsParties.TextMatrix(vsParties.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsParties.TextMatrix(vsParties.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsParties_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
			{
				e.KeyChar = (char)0;
				if (FCConvert.CBool(vsParties.TextMatrix(vsParties.Row, SelectCol)) == false)
				{
					vsParties.TextMatrix(vsParties.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsParties.TextMatrix(vsParties.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsSelectedParties_ClickEvent(object sender, System.EventArgs e)
		{
			int counter;
			if (vsSelectedParties.Row >= 0)
			{
				if (FCConvert.CBool(vsSelectedParties.TextMatrix(vsSelectedParties.Row, SelectCol)) == false)
				{
					vsSelectedParties.TextMatrix(vsSelectedParties.Row, SelectCol, FCConvert.ToString(true));
					for (counter = 0; counter <= vsSelectedParties.Rows - 1; counter++)
					{
						//Application.DoEvents();
						if (counter != vsSelectedParties.Row)
						{
							vsSelectedParties.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
						}
					}
				}
				else
				{
					vsSelectedParties.TextMatrix(vsSelectedParties.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsSelectedParties_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int counter;
			if (e.KeyChar == 13)
			{
				e.KeyChar = (char)0;
				if (FCConvert.CBool(vsSelectedParties.TextMatrix(vsSelectedParties.Row, SelectCol)) == false)
				{
					vsSelectedParties.TextMatrix(vsSelectedParties.Row, SelectCol, FCConvert.ToString(true));
					for (counter = 0; counter <= vsSelectedParties.Rows - 1; counter++)
					{
						//Application.DoEvents();
						if (counter != vsSelectedParties.Row)
						{
							vsSelectedParties.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
						}
					}
				}
				else
				{
					vsParties.TextMatrix(vsSelectedParties.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsPhones_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsPhones.Row >= 0)
			{
				if (FCConvert.CBool(vsPhones.TextMatrix(vsPhones.Row, SelectCol)) == false)
				{
					vsPhones.TextMatrix(vsPhones.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsPhones.TextMatrix(vsPhones.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsPhones_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
			{
				e.KeyChar = (char)0;
				if (FCConvert.CBool(vsPhones.TextMatrix(vsPhones.Row, SelectCol)) == false)
				{
					vsPhones.TextMatrix(vsPhones.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsPhones.TextMatrix(vsPhones.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void ShowWizardStep_2(short intStep)
		{
			ShowWizardStep(ref intStep);
		}

		private void ShowWizardStep(ref short intStep)
		{
			fraMatchSearch.Visible = false;
			fraMergeWizardStep1.Visible = false;
			fraMergeWizardStep2.Visible = false;
			fraMergeWizardStep3.Visible = false;
			fraMergeWizardStep4.Visible = false;
			fraMergeWizardStep5.Visible = false;
			fraMergeWizardStep6.Visible = false;
			switch (intStep)
			{
				case 0:
					{
						//shpSteps.Visible = false;
						fraMatchSearch.Visible = true;
						break;
					}
				case 1:
					{
						//shpSteps.Visible = true;
						fraMergeWizardStep1.Visible = true;
						cmdMergeComplete.Enabled = true;
						cmdStep1Next.Enabled = true;
						break;
					}
				case 2:
					{
						//shpSteps.Visible = true;
						fraMergeWizardStep2.Visible = true;
						cmdStep2Next.Enabled = true;
						cmdStep2Back.Enabled = true;
						break;
					}
				case 3:
					{
						//shpSteps.Visible = true;
						fraMergeWizardStep3.Visible = true;
						cmdStep3Next.Enabled = true;
						cmdStep3Back.Enabled = true;
						break;
					}
				case 4:
					{
						//shpSteps.Visible = true;
						fraMergeWizardStep4.Visible = true;
						cmdStep4Next.Enabled = true;
						cmdStep4Back.Enabled = true;
						break;
					}
				case 5:
					{
						//shpSteps.Visible = true;
						fraMergeWizardStep5.Visible = true;
						cmdStep5Next.Enabled = true;
						cmdStep5Back.Enabled = true;
						break;
					}
				case 6:
					{
						//shpSteps.Visible = true;
						fraMergeWizardStep6.Visible = true;
						cmdStep6Next.Enabled = true;
						cmdStep6Back.Enabled = true;
						break;
					}
			}
			//end switch
		}

		private void cmbSpecific_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbSpecific.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbSpecific.Text == "Single")
			{
				optSpecific_CheckedChanged(sender, e);
			}
		}
	}
}
