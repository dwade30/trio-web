﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;


#elif TWRE0000
using modGlobal = TWRE0000.modGlobalVariables;
using modExtraModules = TWRE0000.modGlobalVariables;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmRECLComment.
	/// </summary>
	public partial class frmRECLComment : BaseForm
	{
		public frmRECLComment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRECLComment InstancePtr
		{
			get
			{
				return (frmRECLComment)Sys.GetInstance(typeof(frmRECLComment));
			}
		}

		protected frmRECLComment _InstancePtr = null;

		bool boolChanged;
		int lngAccountNumber;
		clsDRWrapper rsCLComment = new clsDRWrapper();
		clsDRWrapper rsREComment = new clsDRWrapper();
		bool blnCL;
		string strREComment = "";
		string strCLComment = "";

		private void frmRECLComment_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmRECLComment_Load(object sender, System.EventArgs e)
		{
			
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}

		public void Init( int lngAcct,  bool boolModal = false)
		{
			// parameters:
			// lngAcct  the real estate account number
			// intFormSize the size of the window (triowindowsizebiggie etc.)
			lngAccountNumber = lngAcct;
            rtbText.Text = "";
			// MAL@20080702: Add ability to see previously entered comments
			// Tracker Reference: 13674
			rsREComment.OpenRecordset("SELECT * from CommRec  where CRecordNumber = " + FCConvert.ToString(lngAccountNumber) + " AND RSCARD = 1", modExtraModules.strREDatabase);
			if (rsREComment.RecordCount() > 0)
			{
				strREComment = FCConvert.ToString(rsREComment.Get_Fields_String("CComment"));
			}
			else
			{
				strREComment = "";
			}
			rsCLComment.OpenRecordset("SELECT * from CommentRec where RE = 1 and AccountKey = " + FCConvert.ToString(lngAccountNumber), modExtraModules.strCLDatabase);
			if (rsCLComment.RecordCount() > 0)
			{
				strCLComment = FCConvert.ToString(rsCLComment.Get_Fields_String("Comment"));
			}
			else
			{
				strCLComment = "";
			}
			if (FCConvert.ToBoolean(Strings.InStr(1, modGlobal.DEFAULTDATABASE, "CL", CompareConstants.vbTextCompare)))
			{
				// Calling Program is CL
				blnCL = true;
				cmbCommentType.SelectedIndex = 1;
				txtExistingComments.Text = strCLComment;
			}
			else
			{
				// Calling Program is RE
				blnCL = false;
				cmbCommentType.SelectedIndex = 0;
				txtExistingComments.Text = strREComment;
			}
			if (boolModal)
			{
				this.Show(FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
			rtbText.Focus();
		}
		// VBto upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (Strings.Trim(rtbText.Text) != string.Empty && boolChanged)
			{
				if (FCMessageBox.Show("You have entered a comment" + "\r\n" + "Do you want to save it before exiting?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Save?") == DialogResult.Yes)
				{
					if (!SaveComment())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmRECLComment_Resize(object sender, System.EventArgs e)
		{
			
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			rtbText.Text = "";
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				// App.MainForm.CommonDialog1.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				// App.MainForm.CommonDialog1.Flags = (cdlPDReturnDC+cdlPDNoPageNums)	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				App.MainForm.CommonDialog1.CancelError = true;
                // If rtbText.SelLength = 0 Then
                // App.MainForm.CommonDialog1.Flags = 0 /*? App.MainForm.CommonDialog1.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+cdlPDAllPages	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                // App.MainForm.CommonDialog1.Flags = 0 /*? App.MainForm.CommonDialog1.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+cdlPDSelection	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //Information.Err().Clear();
                //App.MainForm.CommonDialog1.ShowPrinter();
                //if (Information.Err().Number == 0)
                //{
                //	//FC:FINAL:AM:#i225
                rtbText.SelPrint(0);
                //	FCGlobal.Printer.Print(rtbText.Text);
                //	FCGlobal.Printer.EndDoc();
                //}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In mnuPrint_click", MsgBoxStyle.Critical, "Error");
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveComment();
		}

		private bool SaveComment()
		{
			bool SaveComment = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				if (Strings.Trim(rtbText.Text) != string.Empty)
				{
					SaveComment = false;
					clsSave.OpenRecordset("select * from commrec  where crecordnumber = " + FCConvert.ToString(lngAccountNumber) + " AND RSCARD = 1", modExtraModules.strREDatabase);
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
						clsSave.Set_Fields("ccomment", FCConvert.ToString(clsSave.Get_Fields_String("ccomment")) + "\r\n" + rtbText.Text);
					}
					else
					{
						clsSave.AddNew();
						clsSave.Set_Fields("crecordnumber", lngAccountNumber);
						clsSave.Set_Fields("ccomment", rtbText.Text);
						clsSave.Set_Fields("rscard", 1);
					}
					clsSave.Update();
					clsSave.OpenRecordset("select * from commentrec where re = 1 and accountkey = " + FCConvert.ToString(lngAccountNumber), modExtraModules.strCLDatabase);
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
						clsSave.Set_Fields("comment", FCConvert.ToString(clsSave.Get_Fields_String("comment")) + "\r\n" + rtbText.Text);
					}
					else
					{
						clsSave.AddNew();
						clsSave.Set_Fields("accountkey", lngAccountNumber);
						clsSave.Set_Fields("comment", rtbText.Text);
						clsSave.Set_Fields("RE", true);
					}
					clsSave.Update();
					FCMessageBox.Show("Message added to Real Estate and Collections", MsgBoxStyle.Information, "Saved");
				}
				boolChanged = false;
				SaveComment = true;
				return SaveComment;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In SaveComment", MsgBoxStyle.Critical, "Error");
			}
			return SaveComment;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveComment())
			{
				mnuExit_Click();
			}
		}

		private void optCommentType_Click(int Index, object sender, System.EventArgs e)
		{
			if (cmbCommentType.SelectedIndex == 0)
			{
				txtExistingComments.Text = strREComment;
			}
			else
			{
				txtExistingComments.Text = strCLComment;
			}
		}

		private void optCommentType_Click(object sender, System.EventArgs e)
		{
			int index = cmbCommentType.SelectedIndex;
			optCommentType_Click(index, sender, e);
		}

		private void rtbText_Change()
		{
			boolChanged = true;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
            if (SaveComment())
            {
                Close();
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}

		private void cmdClear_Click(object sender, EventArgs e)
		{
			this.mnuClear_Click(sender, e);
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			this.mnuPrint_Click(sender, e);
		}
	}
}
