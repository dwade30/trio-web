//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Diagnostics;
using System.Windows.Forms;
using fecherFoundation;
using TWSharedLibrary.Data;

namespace Global
{
	public class cUtilityBillingDB
	{

		//=========================================================

		private string strLastError = "";
		private int lngLastError;
        private const string DatabaseToUpdate = "UtilityBilling";
		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{   // On Error GoTo ErrorHandler
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", "UtilityBilling");
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
					tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
					tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
					tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return GetVersion;
		}


		public void SetVersion(ref cVersionInfo nVersion)
		{
			try
			{   // On Error GoTo ErrorHandler
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", "UtilityBilling");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{   // On Error GoTo ErrorHandler
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;
                clsDRWrapper rsTest = new clsDRWrapper();
                if (!rsTest.DBExists(DatabaseToUpdate))
                {
                    return CheckVersion;
                }

                cVer = GetVersion();
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1; // default to 1.0.0.0

				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (OldCDBS())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}

				}

				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 1;
				if (tVer.IsNewer(cVer))
				{
					if (AddLienProcessExclusion())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 2;
				if (tVer.IsNewer(cVer))
				{
					if (AddCollectorTitleToLienControl())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 3;
				if (tVer.IsNewer(cVer))
				{
					if (AddConveyanceDateToIConnectInfo())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				// kk12182017 trout-1225  Add Reversal and SetEIDate fields to PaymentRec to make Reversals in UT more like CL
				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 4;
				if (tVer.IsNewer(cVer))
				{
					if (AddReversalFields())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				//kk01222018 trouts-260  Add fields for trout-1118
				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 5;
				if (tVer.IsNewer(cVer))
				{
					if (AddPrevChangeOutConsFields())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 2;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddImportExceptionsTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				//kk07152014 trout-1069  Add a configurable default value for Display History on the DE screen
				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 3;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddDefDisplayHistoryField())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}


				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddACHInfoTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 2;
				tVer.Build = 0;
				if (tVer.IsNewer( cVer))
				{
					if (AddtblAutoPayTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 3;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddAccountACHTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				//kk02212018 troges-88  Add table for batch processing  'CODE FREEZE  TROGES-88
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 4;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddBatchRecoverTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 5;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddDeedNamesToMaster())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 6;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddImportedPayments())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 7;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddImportIDToBatchRecover())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 8;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddPaymentIdentifierToBatchRecover())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 9;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddProcessedToBatchRecover())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 10;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddCorrelationIdentifier())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 11;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddCustomerPartyView())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 12;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddPreviousInterestPaidDatesToBill())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 13;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddTransactionIdToPaymentRec())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 14;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddPreviousInterestPaidDatesToLien())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 15;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddBillIdLienIdToPaymentRec())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 16;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddCustomerPartyView())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 19;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (FixBillStatus())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

				tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 20;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (RemoveInvalidPayments())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

				CheckVersion = true;
				return CheckVersion;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return CheckVersion;
		}

        private bool RemoveInvalidPayments()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();

            try
            {
                rsUpdate.Execute("DELETE FROM PaymentRec WHERE ReceiptNumber = -1", "UtilityBilling");

                return true;
            }
            catch
            {
                return false;
            }
		}

        private bool FixBillStatus()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();

            try
            {
                rsUpdate.Execute("UPDATE Bill SET BillStatus = 'B' WHERE BillStatus = 'P' OR BillStatus = 'A'",
                    "UtilityBilling");
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AlterCustomerViewToUseLeftJoins()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();

			try
			{
				rsUpdate.Execute("ALTER VIEW [dbo].[CustomerPartyView] as " +
	                 "SELECT cust.*, party.PartyGuid, party.PartyType, party.FirstName, party.MiddleName, Party.LastName, Party.Designation, party.WebAddress, party.Address1, party.Address2, party.Address3, party.City, party.State, party.Zip, party.Country, party.OverrideName, party.FullName, party.FullNameLF, " +
	                 "billedParty.FullNameLF as BilledNameLF, billed2Party.FullNameLF as Billed2NameLF, owner2Party.FullNameLF as Owner2FullNameLF, " +
	                 "billedParty.Address1 as BilledAddress1, billedParty.Address2 as BilledAddress2, billedParty.Address3 as BilledAddress3, billedParty.City as BilledCity, billedParty.State as BilledState, billedParty.Zip as BilledZip, billedParty.Country as BilledCountry " +
	                 "FROM dbo.Master as cust INNER JOIN " +
	                 rsUpdate.Get_GetFullDBName("CentralParties") +
	                 ".dbo.PartyAndAddressView as party ON cust.OwnerPartyID = party.ID " +
	                 "LEFT JOIN " + rsUpdate.Get_GetFullDBName("CentralParties") +
	                 ".dbo.PartyAndAddressView as owner2Party ON cust.SecondOwnerPartyID = owner2Party.ID " +
	                 "LEFT JOIN " + rsUpdate.Get_GetFullDBName("CentralParties") +
	                 ".dbo.PartyAndAddressView as billedParty ON cust.BillingPartyID = billedParty.ID " +
	                 "LEFT JOIN " + rsUpdate.Get_GetFullDBName("CentralParties") +
	                 ".dbo.PartyAndAddressView as billed2Party ON cust.SecondBillingPartyID = billed2Party.ID", "UtilityBilling");

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddBillIdLienIdToPaymentRec()
		{
			try
			{
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("PaymentRec", "BillId", "UtilityBilling"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[PaymentRec] Add [BillId] int NULL", "UtilityBilling");
					rsUpdate.Execute("Alter Table.[dbo].[PaymentRec] Add [LienId] int NULL", "UtilityBilling");
					rsUpdate.Execute("Update PaymentRec SET LienId = BillKey, BillId = 0 WHERE Lien = 1", "UtilityBilling");
					rsUpdate.Execute("Update PaymentRec SET BillId = BillKey, LienId = 0 WHERE Lien <> 1", "UtilityBilling");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddTransactionIdToPaymentRec()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			clsDRWrapper rsPayment = new clsDRWrapper();
			Guid transId;

			try
			{
				// On Error GoTo ErrorHandler
				rsUpdate = new clsDRWrapper();
				if (!FieldExists("PaymentRec", "TransactionIdentifier", "UtilityBilling"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[PaymentRec] Add [TransactionIdentifier] uniqueidentifier NULL",
						"UtilityBilling");

					rsPayment = new clsDRWrapper();
					rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE CHGINTNumber <> 0", "UtilityBilling");
					if (rsPayment.EndOfFile() != true)
					{
						do
						{
							transId = Guid.NewGuid();
							rsUpdate.Execute("UPDATE PaymentRec SET TransactionIdentifier = '" + transId + "'" +
							                 " WHERE ID = " + rsPayment.Get_Fields("ID") +
							                 " OR ID = " + rsPayment.Get_Fields("CHGINTNumber"), "UtilityBilling");
							rsPayment.MoveNext();
						} while (rsPayment.EndOfFile() != true);
					}
				}

				return true;
			}
			catch
			{
				return false;
			}
			finally
			{
				rsPayment.DisposeOf();
				rsUpdate.DisposeOf();
			}
		}

		private bool AddPreviousInterestPaidDatesToLien()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("Lien", "PreviousInterestPaidDate", "UtilityBilling"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[Lien] Add [PreviousInterestPaidDate] datetime NULL", "UtilityBilling");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddPreviousInterestPaidDatesToBill()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("Bill", "SPreviousInterestPaidDate", "UtilityBilling"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[Bill] Add [SPreviousInterestPaidDate] datetime NULL", "UtilityBilling");
				}

				if (!FieldExists("Bill", "WPreviousInterestPaidDate", "UtilityBilling"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[Bill] Add [WPreviousInterestPaidDate] datetime NULL", "UtilityBilling");
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddCustomerPartyView()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();

			try
			{
				rsUpdate.OpenRecordset("select* FROM sys.views where name = 'CustomerPartyView'", "TWUT0000.vb1");
                if (!rsUpdate.EndOfFile())
                {
                    rsUpdate.Execute("Drop View CustomerPartyView", "UtilityBilling");
                }
					rsUpdate.Execute("CREATE VIEW CustomerPartyView as " +		                 
                        "SELECT cust.*, party.PartyGuid, party.PartyType, party.FirstName, party.MiddleName, Party.LastName, Party.Designation, party.WebAddress, party.Address1, party.Address2, party.Address3, party.City, party.State, party.Zip, party.Country, party.OverrideName, party.FullName, party.FullNameLF, " +
                        "billedParty.FullNameLF as BilledNameLF, billed2Party.FullNameLF as Billed2NameLF, owner2Party.FullNameLF as Owner2FullNameLF, " +
                        "billedParty.Address1 as BilledAddress1, billedParty.Address2 as BilledAddress2, billedParty.Address3 as BilledAddress3, billedParty.City as BilledCity, billedParty.State as BilledState, billedParty.Zip as BilledZip, billedParty.Country as BilledCountry " +
                        "FROM dbo.Master as cust INNER JOIN " +
                        rsUpdate.Get_GetFullDBName("CentralParties") +
                        ".dbo.PartyAndAddressView as party ON cust.OwnerPartyID = party.ID " +
                        "LEFT JOIN " + rsUpdate.Get_GetFullDBName("CentralParties") +
                        ".dbo.PartyAndAddressView as owner2Party ON cust.SecondOwnerPartyID = owner2Party.ID " +
                        "LEFT JOIN " + rsUpdate.Get_GetFullDBName("CentralParties") +
                        ".dbo.PartyAndAddressView as billedParty ON cust.BillingPartyID = billedParty.ID " +
                        "LEFT JOIN " + rsUpdate.Get_GetFullDBName("CentralParties") +
                        ".dbo.PartyAndAddressView as billed2Party ON cust.SecondBillingPartyID = billed2Party.ID", "UtilityBilling");

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddPaymentIdentifierToBatchRecover()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("BatchRecover", "PaymentIdentifier", "UtilityBilling"))
				{
					rsUpdate.Execute("ALTER TABLE BatchRecover ADD PaymentIdentifier nvarchar(64) NULL", "UtilityBilling");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddImportIDToBatchRecover()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("BatchRecover", "ImportID", "UtilityBilling"))
				{
					rsUpdate.Execute("ALTER TABLE BatchRecover ADD ImportID int NULL", "UtilityBilling");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddImportedPayments()
		{
			string strSql;
			clsDRWrapper rsExec = new clsDRWrapper();

			try
			{
				strSql = GetTableCreationHeaderSQL("ImportedPayments");
				strSql = strSql + "[BillId] [int] NOT NULL,";
				strSql = strSql + "[BillType] [varchar] (25) NOT NULL,";
				strSql = strSql + "[BillNumber] [varchar] (25) NOT NULL,";
				strSql = strSql + "[AccountNumber] [int] NOT NULL,";
				strSql = strSql + "[PaymentDate] [DateTime] NOT NULL,";
				strSql = strSql + "[PaymentAmount] [Decimal] (18,2) NOT NULL,";
				strSql = strSql + "[PaidBy] [varchar] (255) NOT NULL,";
				strSql = strSql + "[AppliedDateTime] [DateTime] NULL,";
				strSql = strSql + "[AppliedId] [int] NOT NULL,";
				strSql = strSql + "[PaymentIdentifier] [nvarchar] (64) NULL,";
				strSql = strSql + GetTablePKSql("ImportedPayments");

				rsExec.Execute(strSql, "UtilityBilling");

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		private bool AddProcessedToBatchRecover()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("BatchRecover", "Processed", "UtilityBilling"))
				{
					rsUpdate.Execute("ALTER TABLE BatchRecover ADD Processed bit NULL", "UtilityBilling");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddDeedNamesToMaster()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("Master", "DeedName1", "UtilityBilling"))
				{
					rsUpdate.Execute("ALTER TABLE [Master] ADD [DeedName1] nvarchar(255) NULL", "UtilityBilling");
				}

				if (!FieldExists("Master", "DeedName2", "UtilityBilling"))
				{
					rsUpdate.Execute("ALTER TABLE [Master] ADD [DeedName2] nvarchar(255) NULL", "UtilityBilling");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private string GetTableCreationHeaderSQL(string strTableName)
		{
			string GetTableCreationHeaderSQL = "";
			string strSQL;
			strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
			strSQL += "Create Table [dbo].[" + strTableName + "] (";
			strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
			GetTableCreationHeaderSQL = strSQL;
			return GetTableCreationHeaderSQL;
		}

		private string GetTablePKSql(string strTableName)
		{
			string GetTablePKSql = "";
			string strSQL;
			strSQL = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
			GetTablePKSql = strSQL;
			return GetTablePKSql;
		}

		private bool AddCorrelationIdentifier()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("PaymentRec", "CorrelationIdentifier", "UtilityBilling"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[PaymentRec] Add [CorrelationIdentifier] uniqueidentifier NULL", "UtilityBilling");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddConveyanceDateToIConnectInfo()
		{
			bool AddConveyanceDateToIConnectInfo = false;
			// trouts-212 add missing fields
			clsDRWrapper rsUpdate = new clsDRWrapper();

			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'tblIConnectInfo' AND COLUMN_NAME = 'ConveyedDate'", "UtilityBilling");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("alter table tblIConnectInfo add ConveyedDate datetime NULL", "UtilityBilling");
				rsUpdate.Execute("Update tblIConnectInfo set ConveyedDate = '" + Strings.Format(DateAndTime.DateValue(FCConvert.ToString(0)), "MM/dd/yyyy") + "'", "UtilityBilling");
			}

			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'tblIConnectInfo' AND COLUMN_NAME = 'Uploaded'", "UtilityBilling");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("alter table tblIConnectInfo add Uploaded bit NULL", "UtilityBilling");
				rsUpdate.Execute("Update tblIConnectInfo set Uploaded = 0", "UtilityBilling");
			}

			AddConveyanceDateToIConnectInfo = true;
			return AddConveyanceDateToIConnectInfo;
		}

		private bool AddCollectorTitleToLienControl()
		{
			bool AddCollectorTitleToLienControl = false;
			clsDRWrapper rsUpdate = new clsDRWrapper();

			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'SControl_LienProcess' AND COLUMN_NAME = 'CollectorTitle'", "UtilityBilling");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("alter table SControl_LienProcess add CollectorTitle nvarchar(50) NULL", "UtilityBilling");
				rsUpdate.Execute("Update SControl_LienProcess set CollectorTitle = 'Treasurer'", "UtilityBilling");
			}

			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WControl_LienProcess' AND COLUMN_NAME = 'CollectorTitle'", "UtilityBilling");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("alter table WControl_LienProcess add CollectorTitle nvarchar(50) NULL", "UtilityBilling");
				rsUpdate.Execute("Update WControl_LienProcess set CollectorTitle = 'Treasurer'", "UtilityBilling");
			}

			AddCollectorTitleToLienControl = true;
			return AddCollectorTitleToLienControl;
		}

		private bool AddLienProcessExclusion()
		{
			bool AddLienProcessExclusion = false;
			clsDRWrapper rsUpdate = new clsDRWrapper();
			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Bill' AND COLUMN_NAME = 'LienProcessExclusion'", "UtilityBilling");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("alter table Bill add LienProcessExclusion bit NULL", "UtilityBilling");
				rsUpdate.Execute("Update Bill set LienProcessExclusion = 0", "UtilityBilling");
			}
			AddLienProcessExclusion = true;
			return AddLienProcessExclusion;
		}

		private bool OldCDBS()
		{
			bool OldCDBS = false;
			clsDRWrapper rsDB = new clsDRWrapper();

			// kk05222014 trouts-100  Check SameBillOwner with OwnerPartyID's and BillingPartyID's
			rsDB.Execute("UPDATE Master SET BillingPartyID = OwnerPartyID WHERE ISNULL(SameBillOwner,0) = 1 AND BillingPartyID <> OwnerPartyID", "UtilityBilling");
			rsDB.Execute("UPDATE Master SET SecondBillingPartyID = SecondOwnerPartyID WHERE ISNULL(SameBillOwner,0) = 1 AND SecondBillingPartyID <> SecondOwnerPartyID", "UtilityBilling");

			// Make sure Version is in the UtiliyBilling table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'Version' AND TABLE_NAME = 'UtilityBilling'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[UtilityBilling] ADD [Version] nvarchar(50) NULL", "UtilityBilling");
			}

			// kk 09256013 trouts-27  Lien Discharge Filing Fee Increase
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'LienFeeIncrRunOn' AND TABLE_NAME = 'UtilityBilling'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[UtilityBilling] ADD [LienFeeIncrRunOn] datetime NULL", "UtilityBilling");
				rsDB.Execute("UPDATE UtilityBilling SET LienFeeIncrRunOn = '" + Strings.Format(DateAndTime.DateValue(FCConvert.ToString(0)), "MM/dd/yyyy") + "'", "UtilityBilling");
			}

			// Make sure BillStormwaterFee is in the UtiliyBilling table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'BillStormwaterFee' AND TABLE_NAME = 'UtilityBilling'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[UtilityBilling] ADD [BillStormwaterFee] bit NULL", "UtilityBilling");
				rsDB.Execute("UPDATE UtilityBilling SET BillStormwaterFee = 0", "UtilityBilling");
			}

			// Make sure ImpervSurfArea is in the Master table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ImpervSurfArea' AND TABLE_NAME = 'Master'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[Master] ADD [ImpervSurfArea] decimal(18, 2) NULL", "UtilityBilling");
			}

			// Make sure Bill is indexed on AccountKey
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_Bill_AccountKey'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_Bill_AccountKey] ON [dbo].[Bill] " + "([AccountKey] ASC,[Service] Asc) " + "INCLUDE ([ID],[ActualAccountNumber],[BillNumber],[MeterKey],[Book],[BillingRateKey]) " + "WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", "UtilityBilling");
			}

			// Make sure Bill is indexed on MeterKey
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_Bill_MeterKey'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_Bill_MeterKey] ON [dbo].[Bill] " + "([MeterKey] ASC,[Service] Asc) " + "INCLUDE ([ID],[AccountKey],[ActualAccountNumber],[BillNumber],[Book],[BillingRateKey]) " + "WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", "UtilityBilling");
			}

			// Make sure PaymentRec is indexed on BillKey
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_PaymentRec_BillKey'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_PaymentRec_BillKey] ON [dbo].[PaymentRec] " + "([BillKey] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", "UtilityBilling");
			}

			// Make sure PaymentRec is indexed on AccountKey
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_PaymentRec_AccountKey'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_PaymentRec_AccountKey] ON [dbo].[PaymentRec] " + "([AccountKey] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", "UtilityBilling");
			}

			// Make sure MeterTable is indexed on AccountKey
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_MeterTable_AccountKey'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_MeterTable_AccountKey] ON [dbo].[MeterTable] " + "([AccountKey] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", "UtilityBilling");
			}

			// kk 01286014 trout-1009  Add option to force Electronic Data Entry to use default reading date
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ForceReadingDate' AND TABLE_NAME = 'UtilityBilling'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[UtilityBilling] ADD [ForceReadingDate] bit NULL", "UtilityBilling");
				rsDB.Execute("UPDATE UtilityBilling SET ForceReadingDate = 0", "UtilityBilling");
			}

			// kk01272015 trout-1061/trouts-135  Add BillKey to MeterConsumption table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'BillKey' AND TABLE_NAME = 'MeterConsumption'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[MeterConsumption] ADD [BillKey] int NULL", "UtilityBilling");
				rsDB.Execute("UPDATE MeterConsumption SET BillKey = 0", "UtilityBilling");
			}

			// kk03162015 trocrs-36  Add option to disable auto-fill payment amount on collection screen
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'DisableAutoPmtFill' AND TABLE_NAME = 'UtilityBilling'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[UtilityBilling] ADD [DisableAutoPmtFill] bit NULL", "UtilityBilling");
				rsDB.Execute("UPDATE UtilityBilling SET DisableAutoPmtFill = 0", "UtilityBilling");
			}

			// kk06042015 trout-1165  Add EPmtID field to PaymentRec to track payments imported from online billing/payment systems (mgh)
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'EPmtID' AND TABLE_NAME = 'PaymentRec'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[PaymentRec] ADD [EPmtID] nvarchar(64) NULL", "UtilityBilling");
			}

			// kk06042015 trout-1166  Move collections settings from CollectionControl to UtilityBilling
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ControlsMoved' AND TABLE_NAME = 'CollectionControl'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[CollectionControl] ADD [ControlsMoved] datetime NULL", "UtilityBilling");
				UpdateFieldsFromCollectionControl();
			}

			// kk07082015 trouts-149  Change NULL impervious surface areas to -1 to indicate that the account is not billed
			UpdateNullImpervSurfArea();

			// kk09022015 trouts-146 Add flag to skip LDN prompt in CR
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'SkipLDNPrompt' AND TABLE_NAME = 'UtilityBilling'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[UtilityBilling] ADD [SkipLDNPrompt] bit NULL", "UtilityBilling");
			}

			// kk09242015 trouts-157  Add IMPB Tracking Number to CMFNumbers and Bill tables
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'IMPBTrackingNumber' AND TABLE_NAME = 'CMFNumbers'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[CMFNumbers] ADD [IMPBTrackingNumber] nvarchar(30) NULL", "UtilityBilling");
			}
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'IMPBTrackingNumber' AND TABLE_NAME = 'Bill'", "UtilityBilling");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[Bill] ADD [IMPBTrackingNumber] nvarchar(30) NULL", "UtilityBilling");
			}

			// kk11032015 trouts-170  Increase storage for RateKeyList field in the SControl_ and WControl_30DayNotice tables
			rsDB.OpenRecordset("SELECT CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'RateKeyList' AND TABLE_NAME = 'SControl_30DayNotice'", "UtilityBilling");
			if (!rsDB.EndOfFile())
			{
				// TODO: Field [CHARACTER_MAXIMUM_LENGTH] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsDB.Get_Fields("CHARACTER_MAXIMUM_LENGTH")) != -1)
				{
					Modify_30DN_Control_Tables();
				}
			}

			OldCDBS = true;
			return OldCDBS;
		}

		private void UpdateNullImpervSurfArea()
		{
			// kk07082015 trouts-149  Change NULL impervious surface areas to -1 to indicate that the account is not billed
			clsDRWrapper rsUpd = new clsDRWrapper();

			rsUpd.Execute("UPDATE Master SET ImpervSurfArea = -1 WHERE ImpervSurfArea IS NULL", "UtilityBilling");
		}


		private void Modify_30DN_Control_Tables()
		{
			clsDRWrapper rsDB = new clsDRWrapper();
			string strTableDef;
			bool boolOldRec = false;

			int BillingYear = 0;
			double MinimumAmount = 0;
			DateTime MailDate = default(DateTime);
			string CollectorName = "";
			string CollectorTitle = "";
			string MuniTitle = "";
			string Muni = "";
			string County = "";
			string State = "";
			string Zip = "";
			double Demand = 0;
			double CertMailFee = 0;
			string MapPreparer = "";
			string MapPrepareDate = "";
			bool PayCertMailFee = false;
			bool SendCopyToMortHolder = false;
			bool ChargeForMortHolder;
			string SendCopyToNewOwner = "";
			DateTime DateCreated = default(DateTime);
			string User = "";
			string OldCollector = "";
			DateTime RecommitmentDate = default(DateTime);
			DateTime DateUpdated = default(DateTime);
			double MailTo = 0;
			string RateKeyList = "";
			string DefaultSort = "";
			string LegalDescription = "";

			strTableDef = "[ID] [int] IDENTITY(1,1) NOT NULL, [BillingYear] [int] NULL, [MinimumAmount] [float] NULL," + "[MailDate] [datetime] NULL, [CollectorName] [nvarchar](255) NULL, [CollectorTitle] [nvarchar](50) NULL," + "[MuniTitle] [nvarchar](50) NULL, [Muni] [nvarchar](255) NULL, [County] [nvarchar](255) NULL," + "[State] [nvarchar](255) NULL, [Zip] [nvarchar](255) NULL, [Demand] [float] NULL," + "[CertMailFee] [float] NULL, [MapPreparer] [nvarchar](255) NULL, [MapPrepareDate] [nvarchar](50) NULL," + "[PayCertMailFee] [bit] NULL, [SendCopyToMortHolder] [bit] NULL, [ChargeForMortHolder] [bit] NULL," + "[SendCopyToNewOwner] [nvarchar](150) NULL, [DateCreated] [datetime] NULL, [User] [nvarchar](255) NULL," + "[OldCollector] [nvarchar](255) NULL, [RecommitmentDate] [datetime] NULL, [DateUpdated] [datetime] NULL," + "[MailTo] [float] NULL, [RateKeyList] [varchar](max) NULL, [DefaultSort] [nvarchar](1) NULL, " + "[LegalDescription] [nvarchar](255) NULL, " + "PRIMARY KEY CLUSTERED ([ID] Asc) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, " + "ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]";



			rsDB.OpenRecordset("SELECT * FROM SControl_30DayNotice", "UtilityBilling");
			if (!rsDB.EndOfFile())
			{
				boolOldRec = true;
				BillingYear = FCConvert.ToInt32(rsDB.Get_Fields_Int32("BillingYear"));
				MinimumAmount = rsDB.Get_Fields_Double("MinimumAmount");
				if (!rsDB.IsFieldNull("MailDate"))
				{
					MailDate = (DateTime)rsDB.Get_Fields_DateTime("MailDate");
				}
				CollectorName = FCConvert.ToString(rsDB.Get_Fields_String("CollectorName"));
				CollectorTitle = FCConvert.ToString(rsDB.Get_Fields_String("CollectorTitle"));
				MuniTitle = FCConvert.ToString(rsDB.Get_Fields_String("MuniTitle"));
				Muni = FCConvert.ToString(rsDB.Get_Fields_String("Muni"));
				// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
				County = FCConvert.ToString(rsDB.Get_Fields("County"));
				State = FCConvert.ToString(rsDB.Get_Fields_String("State"));
				Zip = FCConvert.ToString(rsDB.Get_Fields_String("Zip"));
				Demand = rsDB.Get_Fields_Double("Demand");
				CertMailFee = rsDB.Get_Fields_Double("CertMailFee");
				MapPreparer = FCConvert.ToString(rsDB.Get_Fields_String("MapPreparer"));
				MapPrepareDate = FCConvert.ToString(rsDB.Get_Fields_String("MapPrepareDate"));
				PayCertMailFee = FCConvert.ToBoolean(rsDB.Get_Fields_Boolean("PayCertMailFee"));
				SendCopyToMortHolder = FCConvert.ToBoolean(rsDB.Get_Fields_Boolean("SendCopyToMortHolder"));
				if (!rsDB.IsFieldNull("DateCreated"))
				{
					DateCreated = (DateTime)rsDB.Get_Fields_DateTime("DateCreated");
				}
				// TODO: Check the table for the column [User] and replace with corresponding Get_Field method
				User = FCConvert.ToString(rsDB.Get_Fields("User"));
				OldCollector = FCConvert.ToString(rsDB.Get_Fields_String("OldCollector"));
				if (!rsDB.IsFieldNull("RecommitmentDate"))
				{
					RecommitmentDate = (DateTime)rsDB.Get_Fields_DateTime("RecommitmentDate");
				}
				if (!rsDB.IsFieldNull("DateUpdated"))
				{
					DateUpdated = (DateTime)rsDB.Get_Fields_DateTime("DateUpdated");
				}
				MailTo = rsDB.Get_Fields_Double("MailTo");
				RateKeyList = FCConvert.ToString(rsDB.Get_Fields_String("RateKeyList"));
				DefaultSort = FCConvert.ToString(rsDB.Get_Fields_String("DefaultSort"));
				LegalDescription = FCConvert.ToString(rsDB.Get_Fields_String("LegalDescription"));
			}
			else
			{
				boolOldRec = false;
			}


			rsDB.Execute("DROP TABLE [SControl_30DayNotice]", "UtilityBilling");

			rsDB.Execute("CREATE TABLE [dbo].[SControl_30DayNotice](" + strTableDef + ") ON [PRIMARY]", "UtilityBilling");

			if (boolOldRec)
			{

				rsDB.OpenRecordset("SELECT * FROM SControl_30DayNotice", "UtilityBilling");
				rsDB.AddNew();
				rsDB.Set_Fields("BillingYear", BillingYear);
				rsDB.Set_Fields("MinimumAmount", MinimumAmount);
				rsDB.Set_Fields("MailDate", MailDate);
				rsDB.Set_Fields("CollectorName", CollectorName);
				rsDB.Set_Fields("CollectorTitle", CollectorTitle);
				rsDB.Set_Fields("MuniTitle", MuniTitle);
				rsDB.Set_Fields("Muni", Muni);
				rsDB.Set_Fields("County", County);
				rsDB.Set_Fields("State", State);
				rsDB.Set_Fields("Zip", Zip);
				rsDB.Set_Fields("Demand", Demand);
				rsDB.Set_Fields("CertMailFee", CertMailFee);
				rsDB.Set_Fields("MapPreparer", MapPreparer);
				rsDB.Set_Fields("MapPrepareDate", MapPrepareDate);
				rsDB.Set_Fields("PayCertMailFee", PayCertMailFee);
				rsDB.Set_Fields("SendCopyToMortHolder", SendCopyToMortHolder);
				rsDB.Set_Fields("DateCreated", DateCreated = default(DateTime));
				rsDB.Set_Fields("User", User);
				rsDB.Set_Fields("OldCollector", OldCollector);
				rsDB.Set_Fields("RecommitmentDate", RecommitmentDate);
				rsDB.Set_Fields("DateUpdated", DateUpdated);
				rsDB.Set_Fields("MailTo", MailTo);
				rsDB.Set_Fields("RateKeyList", RateKeyList);
				rsDB.Set_Fields("DefaultSort", DefaultSort);
				rsDB.Set_Fields("LegalDescription", LegalDescription);
				rsDB.Update();

			}




			rsDB.OpenRecordset("SELECT * FROM WControl_30DayNotice", "UtilityBilling");
			if (!rsDB.EndOfFile())
			{
				boolOldRec = true;
				BillingYear = FCConvert.ToInt32(rsDB.Get_Fields_Int32("BillingYear"));
				MinimumAmount = rsDB.Get_Fields_Double("MinimumAmount");
				if (!rsDB.IsFieldNull("MailDate"))
				{
					MailDate = (DateTime)rsDB.Get_Fields_DateTime("MailDate");
				}
				CollectorName = FCConvert.ToString(rsDB.Get_Fields_String("CollectorName"));
				CollectorTitle = FCConvert.ToString(rsDB.Get_Fields_String("CollectorTitle"));
				MuniTitle = FCConvert.ToString(rsDB.Get_Fields_String("MuniTitle"));
				Muni = FCConvert.ToString(rsDB.Get_Fields_String("Muni"));
				// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
				County = FCConvert.ToString(rsDB.Get_Fields("County"));
				State = FCConvert.ToString(rsDB.Get_Fields_String("State"));
				Zip = FCConvert.ToString(rsDB.Get_Fields_String("Zip"));
				Demand = rsDB.Get_Fields_Double("Demand");
				CertMailFee = rsDB.Get_Fields_Double("CertMailFee");
				MapPreparer = FCConvert.ToString(rsDB.Get_Fields_String("MapPreparer"));
				MapPrepareDate = FCConvert.ToString(rsDB.Get_Fields_String("MapPrepareDate"));
				PayCertMailFee = FCConvert.ToBoolean(rsDB.Get_Fields_Boolean("PayCertMailFee"));
				SendCopyToMortHolder = FCConvert.ToBoolean(rsDB.Get_Fields_Boolean("SendCopyToMortHolder"));
				if (!rsDB.IsFieldNull("DateCreated"))
				{
					DateCreated = (DateTime)rsDB.Get_Fields_DateTime("DateCreated");
				}
				// TODO: Check the table for the column [User] and replace with corresponding Get_Field method
				User = FCConvert.ToString(rsDB.Get_Fields("User"));
				OldCollector = FCConvert.ToString(rsDB.Get_Fields_String("OldCollector"));
				if (!rsDB.IsFieldNull("RecommitmentDate"))
				{
					RecommitmentDate = (DateTime)rsDB.Get_Fields_DateTime("RecommitmentDate");
				}
				if (!rsDB.IsFieldNull("DateUpdated"))
				{
					DateUpdated = (DateTime)rsDB.Get_Fields_DateTime("DateUpdated");
				}
				MailTo = rsDB.Get_Fields_Double("MailTo");
				RateKeyList = FCConvert.ToString(rsDB.Get_Fields_String("RateKeyList"));
				DefaultSort = FCConvert.ToString(rsDB.Get_Fields_String("DefaultSort"));
				LegalDescription = FCConvert.ToString(rsDB.Get_Fields_String("LegalDescription"));
			}
			else
			{
				boolOldRec = false;
			}



			rsDB.Execute("DROP TABLE [WControl_30DayNotice]", "UtilityBilling");

			rsDB.Execute("CREATE TABLE [dbo].[WControl_30DayNotice](" + strTableDef + ") ON [PRIMARY]", "UtilityBilling");


			if (boolOldRec)
			{

				rsDB.OpenRecordset("SELECT * FROM WControl_30DayNotice", "UtilityBilling");
				rsDB.AddNew();
				rsDB.Set_Fields("BillingYear", BillingYear);
				rsDB.Set_Fields("MinimumAmount", MinimumAmount);
				rsDB.Set_Fields("MailDate", MailDate);
				rsDB.Set_Fields("CollectorName", CollectorName);
				rsDB.Set_Fields("CollectorTitle", CollectorTitle);
				rsDB.Set_Fields("MuniTitle", MuniTitle);
				rsDB.Set_Fields("Muni", Muni);
				rsDB.Set_Fields("County", County);
				rsDB.Set_Fields("State", State);
				rsDB.Set_Fields("Zip", Zip);
				rsDB.Set_Fields("Demand", Demand);
				rsDB.Set_Fields("CertMailFee", CertMailFee);
				rsDB.Set_Fields("MapPreparer", MapPreparer);
				rsDB.Set_Fields("MapPrepareDate", MapPrepareDate);
				rsDB.Set_Fields("PayCertMailFee", PayCertMailFee);
				rsDB.Set_Fields("SendCopyToMortHolder", SendCopyToMortHolder);
				rsDB.Set_Fields("DateCreated", DateCreated);
				rsDB.Set_Fields("User", User);
				rsDB.Set_Fields("OldCollector", OldCollector);
				rsDB.Set_Fields("RecommitmentDate", RecommitmentDate);
				rsDB.Set_Fields("DateUpdated", DateUpdated);
				rsDB.Set_Fields("MailTo", MailTo);
				rsDB.Set_Fields("RateKeyList", RateKeyList);
				rsDB.Set_Fields("DefaultSort", DefaultSort);
				rsDB.Set_Fields("LegalDescription", LegalDescription);
				rsDB.Update();

			}
		}

		private void UpdateFieldsFromCollectionControl()
		{
			// kk05042015 trout-1166  Some of the following fields were being stored in the UtilityBilling
			// table by Customize but being read from the CollectionControl
			// table for use in Calculations
			// The others just need to be scaled correctly

			clsDRWrapper rsUtil = new clsDRWrapper();
			double tmpDiscRate = 0;
			double tmpOverpayIntRate = 0;
			bool tmpAuditSeqRcpt = false;


			rsUtil.OpenRecordset("SELECT * FROM CollectionControl", "UtilityBilling");
			if (!rsUtil.EndOfFile())
			{
				// TODO: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
				tmpDiscRate = FCConvert.ToDouble(rsUtil.Get_Fields("DiscountPercent"));
				tmpAuditSeqRcpt = FCConvert.ToBoolean(rsUtil.Get_Fields_Boolean("AuditSeqReceipt"));

				rsUtil.Edit();
				rsUtil.Set_Fields("ControlsMoved", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
				rsUtil.Update();

				rsUtil.OpenRecordset("SELECT * FROM UtilityBilling", "UtilityBilling");
				if (!rsUtil.EndOfFile())
				{
					tmpOverpayIntRate = FCConvert.ToDouble(rsUtil.Get_Fields_Double("OverpayInterestRate"));

					rsUtil.Edit();
					rsUtil.Set_Fields("DiscountPercent", tmpDiscRate / 10000.0);
					rsUtil.Set_Fields("OverpayInterestRate", tmpOverpayIntRate / 100.0);
					rsUtil.Set_Fields("AuditSeqReceipt", tmpAuditSeqRcpt);
					rsUtil.Update();
				}
			}


		}

		private bool AddReversalFields()
		{
			bool AddReversalFields = false;
			// kk12182017 trout-1225  Add Reversal and SetEIDate fields to PaymentRec table
			// Making reversals in UT work more like CL
			// because we're having issues in UT with Interest Date not being reset correctly
			// and interest being doubled after a payment and reversal

			clsDRWrapper rsUpdate = new clsDRWrapper();

			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'PaymentRec' AND COLUMN_NAME = 'SetEIDate'", "UtilityBilling");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("alter table PaymentRec add SetEIDate DateTime NULL", "UtilityBilling");
				rsUpdate.Execute("Update PaymentRec set SetEIDate = '" + Strings.Format(DateAndTime.DateValue(FCConvert.ToString(0)), "MM/dd/yyyy") + "'", "UtilityBilling");
			}

			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'PaymentRec' AND COLUMN_NAME = 'Reversal'", "UtilityBilling");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("alter table PaymentRec add Reversal bit NULL", "UtilityBilling");
				rsUpdate.Execute("Update PaymentRec set Reversal = 0", "UtilityBilling");
			}

			AddReversalFields = true;

			return AddReversalFields;
		}


		private bool AddPrevChangeOutConsFields()//kk01222018 trouts-260 add missing fields from trout-1118
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			try
			{
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'UtilityBilling' AND COLUMN_NAME = 'OutPrintIncludeChangeOutCons'", "UtilityBilling");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE UtilityBilling ADD OutPrintIncludeChangeOutCons bit NULL", "UtilityBilling");
					rsUpdate.Execute("UPDATE UtilityBilling SET OutPrintIncludeChangeOutCons = 0", "UtilityBilling");
				}
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Bill' AND COLUMN_NAME = 'MeterChangedOutConsumption'", "UtilityBilling");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE Bill ADD MeterChangedOutConsumption int NULL", "UtilityBilling");
					rsUpdate.Execute("UPDATE Bill SET MeterChangedOutConsumption = 0", "UtilityBilling");
				}
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'MeterTable' AND COLUMN_NAME = 'PrevChangeOutConsumption'", "UtilityBilling");

				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE MeterTable ADD PrevChangeOutConsumption int NULL", "UtilityBilling");
					rsUpdate.Execute("UPDATE MeterTable SET PrevChangeOutConsumption = 0", "UtilityBilling");
				}
				return true;
			}
			catch
			{
				return false;
			}
		}
		private bool AddImportExceptionsTable()
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{
				if (!TableExists("tblImportExceptions", "UtilityBilling"))
				{
					strSQL = "if not exists (select * from information_schema.tables where table_name = N'tblImportExceptions') ";

					strSQL = strSQL + "Create Table [dbo].[tblImportExceptions] (";
					strSQL = strSQL + "[ID] [int] IDENTITY (1,1) NOT NULL,";
					strSQL = strSQL + "[AccountNumber] [nvarchar] (50) NOT NULL,";
					strSQL = strSQL + "[Location] [nvarchar] (50) NULL,";
					strSQL = strSQL + "[Name] [nvarchar] (50) NULL,";
					strSQL = strSQL + "[Comment] [nvarchar] (255) NULL,";
					strSQL = strSQL + " Constraint [PK_tblImportExceptions] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
					rsSave.Execute(strSQL, "UtilityBilling");
				}
				return true;
			}
			catch
			{
				return false;
			}

		}

		private bool TableExists(string strTable, string strDB)
		{
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				strSQL = "select * from sys.tables where name = '" + strTable + "' and type = 'U'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (rsLoad.EndOfFile())
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}

		private bool FieldExists(string strTable, string strField, string strDB)
		{
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
				rsLoad.OpenRecordset(strSQL, strDB);

				if (!rsLoad.EndOfFile())
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}

		private bool AddDefDisplayHistoryField()
		{
			//kk07152014 trout-1069  Add a configurable default value for Display History on the DE screen

			clsDRWrapper rsUtil = new clsDRWrapper();
			try
			{
			rsUtil.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'UtilityBilling' AND COLUMN_NAME = 'DefDisplayHistory'", "UtilityBilling");
			if (rsUtil.RecordCount() == 0)
			{
				rsUtil.Execute("ALTER TABLE UtilityBilling ADD DefDisplayHistory bit NULL", "UtilityBilling");
				rsUtil.Execute("UPDATE UtilityBilling SET DefDisplayHistory = 0", "UtilityBilling");
			}
			return true;

				//    With rsUtil
				//       If Not .DoesFieldExist("DefDisplayHistory", "UtilityBilling", strUTDatabase) Then
				//            If .UpdateDatabaseTable("UtilityBilling", strUTDatabase, DATABASEPASSWORD) Then
				//                .CreateTableField "DefDisplayHistory", dbBoolean
				//                .SetFieldAllowZeroLength "DefDisplayHistory", True
				//                .SetFieldDefaultValue "DefDisplayHistory", False
				//
				//                .OpenRecordset "SELECT * FROM UtilityBilling", strUTDatabase
				//                .Edit
				//                .Fields("DefDisplayHistory") = False
				//                .Update
				//
				//                .Reset
				//            End If
				//        End If
				//    End With
			}
			catch
			{
				return false;
			}
		}

		private bool AddACHInfoTable()
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{

			if (TableExists("tblACHInformation", "UtilityBilling"))
			{
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'tblACHInformation') ";
				strSQL = strSQL + "Create Table [dbo].[tblACHInformation] (";
				strSQL = strSQL + "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL = strSQL + "[DestinationName] [nvarchar] (100) NOT NULL,";
				strSQL = strSQL + "[DestinationRT] [nvarchar] (9) NOT NULL,";
				strSQL = strSQL + "[CompanyName] [nvarchar] (100) NULL,";
				strSQL = strSQL + "[CompanyRT] [nvarchar] (9) NOT NULL,";
				strSQL = strSQL + "[CompanyAccount] [nvarchar] (20) NOT NULL,";
				strSQL = strSQL + "[CompanyAccountType] [nvarchar] (2) NULL,";
				strSQL = strSQL + "[CompanyID] [nvarchar] (10) NOT NULL,";
				strSQL = strSQL + "[OriginName] [nvarchar] (100) NULL,";
				strSQL = strSQL + "[OriginRT] [nvarchar] (9) NULL,";
				strSQL = strSQL + "[ODFINum] [nvarchar] (9) NULL,";
				strSQL = strSQL + "[ACHFileName] [nvarchar] (255) NOT NULL,";
				strSQL = strSQL + " Constraint [PK_tblACHInformation] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "UtilityBilling");
			}
			return true;
			}
			catch
			{
				return false;
			}
		}
		private bool AddtblAutoPayTable()
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{
			if (!TableExists("tblAutoPay", "UtilityBilling"))
			{
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'tblAutoPay') ";
				strSQL = strSQL + "Create Table [dbo].[tblAutoPay] (";
				strSQL = strSQL + "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL = strSQL + "[BatchNumber] [nvarchar] (15) NOT NULL,";
				strSQL = strSQL + "[BillKey] [int] NOT NULL,";
				strSQL = strSQL + "[Service] [nvarchar] (1) NOT NULL,";
				strSQL = strSQL + "[AccountNumber] [int] NULL,";
				strSQL = strSQL + "[Name] [nvarchar] (50) NULL,";
				strSQL = strSQL + "[AcctACHID] [int] NOT NULL,";
				strSQL = strSQL + "[ACHAmount] [float] NULL,";
				strSQL = strSQL + "[Prin] [float] NULL,";
				strSQL = strSQL + "[Int] [float] NULL,";
				strSQL = strSQL + "[Cost] [float] NULL,";
				strSQL = strSQL + "[Tax] [float] NULL,";
				strSQL = strSQL + "[BillDate] [DateTime] NULL,";
				strSQL = strSQL + "[ACHDueDate] [DateTime] NULL,";
				strSQL = strSQL + "[ACHDate] [DateTime] NULL,";
				strSQL = strSQL + "[ACHDone] [bit] NULL,";
				strSQL = strSQL + "[EffPmtDate] [DateTime] NULL,";
				strSQL = strSQL + "[PostedDate] [DateTime] NULL,";
				strSQL = strSQL + "[Posted] [bit] NULL,";
				strSQL = strSQL + "[LineNumber] [int] NULL,";
				strSQL = strSQL + "[PaymentID] [int] NULL,";
				strSQL = strSQL + "[ACHFlag] [bit] NULL,";
				strSQL = strSQL + " Constraint [PK_tblAutoPay] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "UtilityBilling");
			}
			return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddAccountACHTable()
		{
			string strSQL;
			clsDRWrapper rsSave = new clsDRWrapper();
			try
			{
			if (!TableExists("tblAcctACH", "UtilityBilling"))
			{
				strSQL = "if not exists (select * from information_schema.tables where table_name = N'tblAcctACH') ";
				strSQL = strSQL + "Create Table [dbo].[tblAcctACH] (";
					strSQL = strSQL + "[ID] [int] IDENTITY (1,1) NOT NULL,";
				strSQL = strSQL + "[AccountKey] [int] NULL,";
				strSQL = strSQL + "[Service] [nvarchar] (1) NOT NULL,";
				strSQL = strSQL + "[ACHBankRT] [nvarchar] (9) NOT NULL,";
				strSQL = strSQL + "[ACHAcctNumber] [nvarchar] (50) NOT NULL,";
				strSQL = strSQL + "[ACHAcctType] [nvarchar] (2) NOT NULL,";
				strSQL = strSQL + "[ACHLimit] [float] NULL,";
				strSQL = strSQL + "[ACHActive] [bit] NULL,";
				strSQL = strSQL + "[ACHPrenote] [bit] NULL,";
				strSQL = strSQL + " Constraint [PK_tblacctACH] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
				rsSave.Execute(strSQL, "UtilityBilling");
			}
			return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddBatchRecoverTable()
		{
			//kk02212018 troges-88  Add table for UT Batch Payment processing

			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{ 
				if (!TableExists("BatchRecover", "UtilityBilling"))
				{
					strSQL = "if not exists (select * from information_schema.tables where table_name = N'BatchRecover') " +
					 "Create Table [dbo].[BatchRecover] (" +
					 "[ID] [int] IDENTITY (1,1) NOT NULL," +
					 "[TellerID] [nvarchar] (255) NOT NULL," +
					 "[PaidBy] [nvarchar] (255) NULL," +
					 "[Ref] [nvarchar] (255) NULL," +
					 "[Name] [nvarchar] (255) NULL," +
					 "[BillNumber] [int] NULL," +
					 "[AccountNumber] [int] NULL," +
					 "[Prin] [float] NULL," +
					 "[Tax] [float] NULL," +
					 "[Int] [float] NULL," +
					 "[Cost] [float] NULL," +
					 "[PrintReceipt] [bit] NULL," +
					 "[Service] [nvarchar] (255) NULL," +
					 "[BatchRecoverDate] [datetime] NULL," +
					 "[ETDate] [datetime] NULL," +
					 "[PaymentKey] [int] NULL," +
					 "[PayCorrect] [bit] NULL," +
					 " Constraint [PK_BatchRecover] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
					rsSave.Execute(strSQL, "UtilityBilling");
				}
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
