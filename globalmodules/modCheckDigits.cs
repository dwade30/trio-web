﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	public class modCheckDigits
	{
		// vbPorter upgrade warning: 'Return' As Variant --> As int
		public static int CalculateCheckDigits03(bool boolFromSM = false, bool boolUpdateDisk = false, string strPath = "")
		{
			int CalculateCheckDigits03 = 0;
			// this is the new check digit function, the others are archived functions
			// vbPorter upgrade warning: DateSum As int	OnWrite(int, double)
			int DateSum;
			int counter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rs = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				// todo check path
				if (strPath != "")
				{
					// todo check path
					// rs.Path = strPath
					// rsData.Path = strPath
				}
				if (boolFromSM)
				{
					if (boolUpdateDisk)
					{
						// FROM GE AND THE FIX PROCESS FUNCTION
						// rs.MegaGroup = ""
						// rs.GroupName = ""
						// Call rs.OpenRecordset("SELECT * FROM Modules", "TWUPDATE.VB1")
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM Modules", "systemsettings");
					}
				}
				else
				{
					rs.OpenRecordset("SELECT * FROM Modules", "systemsettings");
				}
				DateSum = 0;
				// Module Expiration Dates
				// Check RE
				// TODO: Check the table for the column [RE] and replace with corresponding Get_Field method
				if (FCConvert.ToBoolean(rs.Get_Fields("RE")))
				{
					if (Information.IsDate(rs.Get_Fields("redate")))
					{
						DateSum += (rs.Get_Fields_DateTime("REDate")).Year + (rs.Get_Fields_DateTime("REDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("RELevel") + ""));
					// commercial RE
					if (Information.IsDate(rs.Get_Fields("CMDate" + "")))
						DateSum += ((DateTime)rs.Get_Fields_DateTime("CMDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CMDate")).Month;
					// RE Sketches
					if (Information.IsDate(rs.Get_Fields("skdate")))
					{
						if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("SK")))
							DateSum += ((DateTime)rs.Get_Fields_DateTime("SKDate")).Year + ((DateTime)rs.Get_Fields_DateTime("SKDate")).Month;
					}
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("SK")))
						DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("SKLevel") + ""));
					// RE hand held device
					if (Information.IsDate(rs.Get_Fields("rhdate")))
					{
						if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("RH")))
							DateSum += ((DateTime)rs.Get_Fields_DateTime("RHDate")).Year + ((DateTime)rs.Get_Fields_DateTime("RHDate")).Month;
					}
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("RH")))
						DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("RHLevel") + ""));
					// Real Estate Max Accounts
					if (!rsData.EndOfFile())
					{
						if (Conversion.Val(rsData.Get_Fields_Int32("RE_MaxAccounts") + "") > 0)
						{
							DateSum += ((FCUtils.iDiv(rsData.Get_Fields_Int32("RE_MaxAccounts"), 100)) * 3);
						}
					}
				}
				// Check MV
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("MV")))
				{
					if (Information.IsDate(rs.Get_Fields("mvdate")))
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("MVDate")).Year + ((DateTime)rs.Get_Fields_DateTime("MVDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("MVLevel") + ""));
					// Rapid Renewal
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("RR")))
					{
						if (Information.IsDate(rs.Get_Fields("rrdate")))
						{
							DateSum += ((DateTime)rs.Get_Fields_DateTime("RRDate")).Year + ((DateTime)rs.Get_Fields_DateTime("RRDate")).Month;
						}
						DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("RRLevel") + ""));
					}
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AR")))
				{
					if (!rs.IsFieldNull("ARDate") || Conversion.Val(rs.Get_Fields_DateTime("ARDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("ARDate")).Year + ((DateTime)rs.Get_Fields_DateTime("ARDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("ARLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BD")))
				{
					if (!rs.IsFieldNull("BDDate") || Conversion.Val(rs.Get_Fields_DateTime("BDDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("BDDate")).Year + ((DateTime)rs.Get_Fields_DateTime("BDDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("BDLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BL")))
				{
					if (!rs.IsFieldNull("BLDate") || Conversion.Val(rs.Get_Fields_DateTime("BLDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("BLDate")).Year + ((DateTime)rs.Get_Fields_DateTime("BLDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("BLLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CE")))
				{
					if (!rs.IsFieldNull("CEDate") || Conversion.Val(rs.Get_Fields_DateTime("CEDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("CEDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CEDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("CELevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CK")))
				{
					if (!rs.IsFieldNull("CKDate") || Conversion.Val(rs.Get_Fields_DateTime("CKDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("CKDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CKDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("CKLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CL")))
				{
					if (!rs.IsFieldNull("CLDate") || Conversion.Val(rs.Get_Fields_DateTime("CLDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("CLDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CLDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("CLLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CR")))
				{
					if (!rs.IsFieldNull("CRDate") || Conversion.Val(rs.Get_Fields_DateTime("CRDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("CRDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CRDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("CRLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("E9")))
				{
					if (!rs.IsFieldNull("E9Date") || Conversion.Val(rs.Get_Fields_DateTime("E9Date")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("E9Date")).Year + ((DateTime)rs.Get_Fields_DateTime("E9Date")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("E9Level") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("FA")))
				{
					if (!rs.IsFieldNull("FADate") || Conversion.Val(rs.Get_Fields_DateTime("FADate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("FADate")).Year + ((DateTime)rs.Get_Fields_DateTime("FADate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("FALevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("HR")))
				{
					if (!rs.IsFieldNull("HRDate") || Conversion.Val(rs.Get_Fields_DateTime("HRDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("HRDate")).Year + ((DateTime)rs.Get_Fields_DateTime("HRDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("HRLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("IV")))
				{
					if (!rs.IsFieldNull("IVDate") || Conversion.Val(rs.Get_Fields_DateTime("IVDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("IVDate")).Year + ((DateTime)rs.Get_Fields_DateTime("IVDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("IVLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("MS")))
				{
					if (!rs.IsFieldNull("MSDate") || Conversion.Val(rs.Get_Fields_DateTime("MSDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("MSDate")).Year + ((DateTime)rs.Get_Fields_DateTime("MSDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("MSLevel") + ""));
				}
				// TODO: Check the table for the column [PP] and replace with corresponding Get_Field method
				if (FCConvert.ToBoolean(rs.Get_Fields("PP")))
				{
					if (!rs.IsFieldNull("PPDate") || Conversion.Val(rs.Get_Fields_DateTime("PPDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("PPDate")).Year + ((DateTime)rs.Get_Fields_DateTime("PPDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("PPLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("PY")))
				{
					if (!rs.IsFieldNull("PYDate") || Conversion.Val(rs.Get_Fields_DateTime("PYDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("PYDate")).Year + ((DateTime)rs.Get_Fields_DateTime("PYDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("PYLevel") + ""));
				}
				// TODO: Check the table for the column [RB] and replace with corresponding Get_Field method
				if (FCConvert.ToBoolean(rs.Get_Fields("RB")))
				{
					if (!rs.IsFieldNull("RBDate") || Conversion.Val(rs.Get_Fields_DateTime("RBDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("RBDate")).Year + ((DateTime)rs.Get_Fields_DateTime("RBDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("RBLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("TS")))
				{
					if (!rs.IsFieldNull("TSDate") || Conversion.Val(rs.Get_Fields_DateTime("TSDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("TSDate")).Year + ((DateTime)rs.Get_Fields_DateTime("TSDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("TSLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("UT")))
				{
					if (!rs.IsFieldNull("UTDate") || Conversion.Val(rs.Get_Fields_DateTime("UTDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("UTDate")).Year + ((DateTime)rs.Get_Fields_DateTime("UTDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("UTLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("VR")))
				{
					if (!rs.IsFieldNull("VRDate") || Conversion.Val(rs.Get_Fields_DateTime("VRDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("VRDate")).Year + ((DateTime)rs.Get_Fields_DateTime("VRDate")).Month;
					}
					DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("VRLevel") + ""));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("XF")))
				{
					if (!rs.IsFieldNull("XFDate") || Conversion.Val(rs.Get_Fields_DateTime("XFDate")) != 0)
					{
						DateSum += ((DateTime)rs.Get_Fields_DateTime("xfdate")).Year + ((DateTime)rs.Get_Fields_DateTime("xfdate")).Month;
					}
				}
				// Networking flag
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("NT")))
					DateSum += 1;
				if (DateSum > 0)
				{
					CalculateCheckDigits03 = DateSum;
				}
				return CalculateCheckDigits03;
			}
			catch
			{
				// ErrorHandler:
				if (Information.Err().Number == 340 || Information.Err().Number == 3265)
				{
					{
						/*? Resume Next; */}
				}
				else
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Calculating CD - 03", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return CalculateCheckDigits03;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As int
		//public static int CalculateCheckDigits(bool boolFromSM = false, bool boolUpdateDisk = false, string strPath = "")
		//{
		//	int CalculateCheckDigits = 0;
		//	// this is the revamped old function with some of the newer mods in it
		//	// vbPorter upgrade warning: DateSum As int	OnWrite(int, double)
		//	int DateSum;
		//	int counter;
		//	clsDRWrapper rsData = new clsDRWrapper();
		//	clsDRWrapper rs = new clsDRWrapper();
		//	int lngErrorCode = 0;
		//	try
		//	{
		//		// On Error GoTo ErrorHandler
		//		lngErrorCode = 1;
		//		// get the network flag
		//		modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "NetworkFlag", ref modReplaceWorkFiles.Statics.gstrNetworkFlag);
		//		modReplaceWorkFiles.Statics.gstrReturn = modReplaceWorkFiles.Statics.gstrNetworkFlag;
		//		lngErrorCode = 2;
		//		if (boolFromSM)
		//		{
		//			if (boolUpdateDisk)
		//			{
		//				// If strPath = vbNullString Then
		//				// lngErrorCode = 3
		//				// Call rs.OpenRecordset("SELECT * FROM Modules", "TWUPDATE.VB1")
		//				// Else
		//				// FROM GE AND THE FIX PROCESS FUNCTION
		//				// lngErrorCode = 4
		//				// todo check path
		//				// rs.Path = strPath
		//				// Call rs.OpenRecordset("SELECT * FROM Modules", "TWUPDATE.VB1")
		//				// End If
		//			}
		//			else
		//			{
		//				lngErrorCode = 5;
		//				rs.OpenRecordset("SELECT * FROM Modules", "systemsettings");
		//			}
		//		}
		//		else
		//		{
		//			lngErrorCode = 6;
		//			rs.OpenRecordset("SELECT * FROM Modules", "systemsettings");
		//		}
		//		DateSum = 0;
		//		// Module Expiration Dates
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("MV")))
		//			DateSum += ((DateTime)rs.Get_Fields_DateTime("MVDate")).Year + ((DateTime)rs.Get_Fields_DateTime("MVDate")).Month;
		//		lngErrorCode = 7;
		//		// TODO: Check the table for the column [RE] and replace with corresponding Get_Field method
		//		if (FCConvert.ToBoolean(rs.Get_Fields("RE")))
		//		{
		//			DateSum += ((DateTime)rs.Get_Fields_DateTime("REDate")).Year + ((DateTime)rs.Get_Fields_DateTime("REDate")).Month;
		//			if (Information.IsDate(rs.Get_Fields("WSDate" + "")))
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("WSDate")).Year + ((DateTime)rs.Get_Fields_DateTime("WSDate")).Month;
		//			if (Information.IsDate(rs.Get_Fields("RPDate" + "")))
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("RPDate")).Year + ((DateTime)rs.Get_Fields_DateTime("RPDate")).Month;
		//			if (Information.IsDate(rs.Get_Fields("CMDate" + "")))
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("CMDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CMDate")).Month;
		//			lngErrorCode = 8;
		//			// Real Estate Max Accounts
		//			if (boolFromSM)
		//			{
		//				if (boolUpdateDisk)
		//				{
		//					if (strPath == string.Empty)
		//					{
		//						rsData.OpenRecordset("SELECT * FROM Modules", "TWUPDATE.vb1");
		//					}
		//					else
		//					{
		//						// FROM GE AND THE FIX PROCESS FUNCTION
		//						// todo check path
		//						// rsData.Path = strPath
		//						rsData.OpenRecordset("SELECT * FROM Modules", "TWUPDATE.vb1");
		//					}
		//				}
		//				else
		//				{
		//					if (strPath == string.Empty)
		//					{
		//						rsData.OpenRecordset("SELECT * FROM Modules", "systemsettings");
		//					}
		//					else
		//					{
		//						// FROM GE AND THE FIX PROCESS FUNCTION
		//						// rsData.Path = strPath
		//						rsData.OpenRecordset("SELECT * FROM Modules", "systemsettings");
		//					}
		//				}
		//			}
		//			else
		//			{
		//				rsData.OpenRecordset("SELECT * FROM Modules", "systemsettings");
		//			}
		//			lngErrorCode = 9;
		//			if (!rsData.EndOfFile())
		//			{
		//				// TODO: Field [WMaxAcct] not found!! (maybe it is an alias?)
		//				if (Conversion.Val(rsData.Get_Fields("WMaxAcct") + "") > 0)
		//				{
		//					// TODO: Field [WMaxAcct] not found!! (maybe it is an alias?)
		//					DateSum += ((FCUtils.iDiv(rsData.Get_Fields("WMaxAcct"), 100)) * 3);
		//				}
		//			}
		//		}
		//		lngErrorCode = 10;
		//		// TODO: Check the table for the column [PP] and replace with corresponding Get_Field method
		//		if (FCConvert.ToBoolean(rs.Get_Fields("PP")))
		//		{
		//			if (!rs.IsFieldNull("PPDate") || Conversion.Val(rs.Get_Fields_DateTime("PPDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("PPDate")).Year + ((DateTime)rs.Get_Fields_DateTime("PPDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("PPLevel") + ""));
		//		}
		//		lngErrorCode = 11;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BL")))
		//		{
		//			if (!rs.IsFieldNull("BLDate") || Conversion.Val(rs.Get_Fields_DateTime("BLDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("BLDate")).Year + ((DateTime)rs.Get_Fields_DateTime("BLDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("BLLevel") + ""));
		//		}
		//		lngErrorCode = 12;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CL")))
		//		{
		//			if (!rs.IsFieldNull("CLDate") || Conversion.Val(rs.Get_Fields_DateTime("CLDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("CLDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CLDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("CLLevel") + ""));
		//		}
		//		lngErrorCode = 13;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CK")))
		//		{
		//			if (!rs.IsFieldNull("CKDate") || Conversion.Val(rs.Get_Fields_DateTime("CKDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("CKDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CKDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("CKLevel") + ""));
		//		}
		//		lngErrorCode = 14;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("VR")))
		//		{
		//			if (!rs.IsFieldNull("VRDate") || Conversion.Val(rs.Get_Fields_DateTime("VRDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("VRDate")).Year + ((DateTime)rs.Get_Fields_DateTime("VRDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("VRLevel") + ""));
		//		}
		//		lngErrorCode = 15;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CE")))
		//		{
		//			if (!rs.IsFieldNull("CEDate") || Conversion.Val(rs.Get_Fields_DateTime("CEDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("CEDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CEDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("CELevel") + ""));
		//		}
		//		lngErrorCode = 16;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BD")))
		//		{
		//			if (!rs.IsFieldNull("BDDate") || Conversion.Val(rs.Get_Fields_DateTime("BDDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("BDDate")).Year + ((DateTime)rs.Get_Fields_DateTime("BDDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("BDLevel") + ""));
		//		}
		//		lngErrorCode = 17;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CR")))
		//		{
		//			if (!rs.IsFieldNull("CRDate") || Conversion.Val(rs.Get_Fields_DateTime("CRDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("CRDate")).Year + ((DateTime)rs.Get_Fields_DateTime("CRDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("CRLevel") + ""));
		//		}
		//		lngErrorCode = 18;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("PY")))
		//		{
		//			if (!rs.IsFieldNull("PYDate") || Conversion.Val(rs.Get_Fields_DateTime("PYDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("PYDate")).Year + ((DateTime)rs.Get_Fields_DateTime("PYDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("PYLevel") + ""));
		//		}
		//		lngErrorCode = 19;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("UT")))
		//		{
		//			if (!rs.IsFieldNull("UTDate") || Conversion.Val(rs.Get_Fields_DateTime("UTDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("UTDate")).Year + ((DateTime)rs.Get_Fields_DateTime("UTDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("UTLevel") + ""));
		//		}
		//		lngErrorCode = 20;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("TC")))
		//		{
		//			if (!rs.IsFieldNull("TCDate") || Conversion.Val(rs.Get_Fields_DateTime("TCDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("TCDate")).Year + ((DateTime)rs.Get_Fields_DateTime("TCDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("TCLevel") + ""));
		//		}
		//		lngErrorCode = 21;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("E9")))
		//		{
		//			if (!rs.IsFieldNull("E9Date") || Conversion.Val(rs.Get_Fields_DateTime("E9Date")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("E9Date")).Year + ((DateTime)rs.Get_Fields_DateTime("E9Date")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("E9Level") + ""));
		//		}
		//		lngErrorCode = 22;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("FA")))
		//		{
		//			if (!rs.IsFieldNull("FADate") || Conversion.Val(rs.Get_Fields_DateTime("FADate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("FADate")).Year + ((DateTime)rs.Get_Fields_DateTime("FADate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("FALevel") + ""));
		//		}
		//		lngErrorCode = 23;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("TS")))
		//		{
		//			if (!rs.IsFieldNull("TSDate") || Conversion.Val(rs.Get_Fields_DateTime("TSDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("TSDate")).Year + ((DateTime)rs.Get_Fields_DateTime("TSDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("TSLevel") + ""));
		//		}
		//		lngErrorCode = 24;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AR")))
		//		{
		//			if (!rs.IsFieldNull("ARDate") || Conversion.Val(rs.Get_Fields_DateTime("ARDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("ARDate")).Year + ((DateTime)rs.Get_Fields_DateTime("ARDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("ARLevel") + ""));
		//		}
		//		// If .Fields("NT") Then
		//		// If .Fields("NTDate") <> "" Or Val(.Fields("NTDate")) <> 0 Then
		//		// DateSum = DateSum + Year(.Fields("NTDate")) + Month(.Fields("NTDate"))
		//		// End If
		//		// DateSum = DateSum + Val(.Fields("NTLevel") & "")
		//		// End If
		//		lngErrorCode = 25;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("IV")))
		//		{
		//			if (!rs.IsFieldNull("IVDate") || Conversion.Val(rs.Get_Fields_DateTime("IVDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("IVDate")).Year + ((DateTime)rs.Get_Fields_DateTime("IVDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("IVLevel") + ""));
		//		}
		//		lngErrorCode = 26;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("RR")))
		//		{
		//			if (!rs.IsFieldNull("RRDate") || Conversion.Val(rs.Get_Fields_DateTime("RRDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("RRDate")).Year + ((DateTime)rs.Get_Fields_DateTime("RRDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("RRLevel") + ""));
		//		}
		//		lngErrorCode = 27;
		//		if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("MS")))
		//		{
		//			if (!rs.IsFieldNull("MSDate") || Conversion.Val(rs.Get_Fields_DateTime("MSDate")) != 0)
		//			{
		//				DateSum += ((DateTime)rs.Get_Fields_DateTime("MSDate")).Year + ((DateTime)rs.Get_Fields_DateTime("MSDate")).Month;
		//			}
		//			DateSum += FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int16("MSLevel") + ""));
		//		}
		//		lngErrorCode = 28;
		//		// Networking flag
		//		if (modReplaceWorkFiles.Statics.gstrNetworkFlag == "Y")
		//		{
		//			DateSum += 1;
		//		}
		//		lngErrorCode = 29;
		//		if (DateSum > 0)
		//		{
		//			CalculateCheckDigits = DateSum;
		//		}
		//		lngErrorCode = 30;
		//		return CalculateCheckDigits;
		//	}
		//	catch
		//	{
		//		// ErrorHandler:
		//		if (Information.Err().Number == 340 || Information.Err().Number == 3265)
		//		{
		//			{
		//				/*? Resume Next; */}
		//		}
		//		else
		//		{
		//			MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Calculating CD - 00" + FCConvert.ToString(lngErrorCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		}
		//	}
		//	return CalculateCheckDigits;
		//}

		public static void UpdateCheckDigits(bool boolFromSM = false, bool boolUpdateDisk = false, string strDatabasePath = "")
		{
			clsDRWrapper rs = new clsDRWrapper();
			int lngTempNew = 0;
			// todo check path
			// If strDatabasePath <> "" Then
			// rs.Path = strDatabasePath
			// End If
			// UPDATE THE CHECK DIGIT FIELD
			if (boolFromSM)
			{
				if (boolUpdateDisk)
				{
					rs.OpenRecordset("SELECT * FROM Modules", "TWUPDATE.VB1");
				}
				else
				{
					rs.OpenRecordset("SELECT * FROM Modules", "systemsettings");
				}
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM Modules", "systemsettings");
			}
			rs.Edit();
			lngTempNew = CalculateCheckDigits03(boolFromSM, boolUpdateDisk, strDatabasePath);
			// lngTemp = CalculateCheckDigits(boolFromSM, boolUpdateDisk)
			// lngTempOld = CalculateCheckDigitsOld(boolFromSM, boolUpdateDisk)
			// 
			rs.Set_Fields("CheckDigits", lngTempNew);
			rs.Update();
		}

		public static bool CheckTownData(bool boolFromSM = false, bool boolUpdate = false, string strMuniName = "")
		{
			bool CheckTownData = false;
			string LetterSum;
			string strTemp = "";
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM Modules", "systemsettings");
			if (strMuniName == string.Empty)
			{
				if (boolFromSM)
				{
					// this is an error if this occurs
					MessageBox.Show("There has been an error calculating the check digits.  Please enter the municipality's name and try again.", "Missing Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return CheckTownData;
				}
				else
				{
					rs.OpenRecordset("select muniname from globalvariables", "systemsettings");
					strTemp = FCConvert.ToString(rs.Get_Fields_String("muniname"));
					rs.OpenRecordset("select * from modules", "systemsettings");
				}
			}
			else
			{
				strTemp = strMuniName;
			}
			LetterSum = "";
			modGlobalConstants.Statics.MuniName = strTemp;
			if (Strings.InStr(1, modGlobalConstants.Statics.MuniName, "-", CompareConstants.vbBinaryCompare) != 0)
			{
				if (Strings.InStr(1, modGlobalConstants.Statics.MuniName, "-", CompareConstants.vbBinaryCompare) < 4)
				{
					MessageBox.Show("Bad entity name", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					CheckTownData = false;
					return CheckTownData;
				}
				else if (Strings.Trim(Strings.Left(modGlobalConstants.Statics.MuniName, Strings.InStr(1, modGlobalConstants.Statics.MuniName, "-", CompareConstants.vbBinaryCompare) - 1)).Length < 3)
				{
					MessageBox.Show("Bad entity name", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					CheckTownData = false;
					return CheckTownData;
				}
				LetterSum = ReturnTownEncryption_2(Strings.Trim(Strings.Left(modGlobalConstants.Statics.MuniName, Strings.InStr(1, modGlobalConstants.Statics.MuniName, "-", CompareConstants.vbBinaryCompare) - 1)));
				// systemmaintenance doesn't ignore the dash when creating the code
				if (LetterSum != FCConvert.ToString(rs.Get_Fields_String("TownCheck")))
				{
					LetterSum = ReturnTownEncryption(ref modGlobalConstants.Statics.MuniName);
				}
			}
			else
			{
				LetterSum = ReturnTownEncryption(ref modGlobalConstants.Statics.MuniName);
			}
			if (LetterSum != FCConvert.ToString(rs.Get_Fields_String("TownCheck")) && Strings.Trim(LetterSum) != Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("towncheck"))))
			{
				CheckTownData = false;
			}
			else
			{
				CheckTownData = true;
			}
			return CheckTownData;
		}

		public static string ReturnTownEncryption_2(string strMuniName)
		{
			return ReturnTownEncryption(ref strMuniName);
		}

		public static string ReturnTownEncryption(ref string strMuniName)
		{
			string ReturnTownEncryption = "";
			// this fucntion will return the encryption of whatever has been passed in
			string strEncrypt = "";
			string strTemp;
			strTemp = strMuniName;
			if (Strings.Trim(strTemp).Length == 3)
			{
				strEncrypt += Strings.UCase(Strings.Left(Strings.Trim(strTemp), 1));
				strEncrypt += Strings.UCase(Strings.Right(Strings.Trim(strTemp), 1));
				strEncrypt += Strings.UCase(Strings.Mid(Strings.Trim(strTemp), 2, 1));
				strEncrypt += "ZB";
			}
			else if (Strings.Trim(strTemp).Length == 4)
			{
				strEncrypt += Strings.UCase(Strings.Left(Strings.Trim(strTemp), 1));
				strEncrypt += Strings.UCase(Strings.Right(Strings.Trim(strTemp), 1));
				strEncrypt += Strings.UCase(Strings.Mid(Strings.Trim(strTemp), 2, 1));
				strEncrypt += "Z";
				strEncrypt += Strings.UCase(Strings.Mid(Strings.Trim(strTemp), 3, 1));
			}
			else
			{
				strEncrypt += Strings.UCase(Strings.Left(Strings.Trim(strTemp), 1));
				strEncrypt += Strings.UCase(Strings.Right(Strings.Trim(strTemp), 1));
				strEncrypt += Strings.UCase(Strings.Mid(Strings.Trim(strTemp), 2, 1));
				strEncrypt += Strings.UCase(Strings.Mid(Strings.Trim(strTemp), Strings.Trim(strTemp).Length - 4, 1));
				strEncrypt += Strings.UCase(Strings.Mid(Strings.Trim(strTemp), Strings.Trim(strTemp).Length - 2, 1));
			}
			ReturnTownEncryption = strEncrypt;
			return ReturnTownEncryption;
		}


	}
}
