//Fecher vbPorter - Version 1.0.0.32
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Commands;
using TWSharedLibrary;
using Wisej.Web;


namespace TWPP0000
{
    /// <summary>
    /// Summary description for frmArchive.
    /// </summary>
    public partial class frmArchive : BaseForm
    {
       private System.ComponentModel.IContainer components;

        public frmArchive()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmArchive InstancePtr
        {
            get
            {
                return (frmArchive)Sys.GetInstance(typeof(frmArchive));
            }
        }
        protected static frmArchive _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        


        //=========================================================

        string strThisModule;
        string strLastDrive = "";
        bool boolOkToCreate;
        int lngRowToEdit;

        const int CNSTGRIDCOLYEAR = 0;
        const int CNSTGRIDCOLDESC = 1;
        const int CNSTGRIDCOLDIRECTORY = 2;
        const int CNSTGRIDCOLAUTOID = 3;

        const string CNSTREPPARCHIVETYPE = "CommitmentArchive";

        public void Init(string strDB, bool boolCreate)
        {
            boolOkToCreate = boolCreate;
            //FC:FINAL:MSH - i.issue #1279: incorrect btn name
            //mnuCreateArchive.Enabled = false;
            cmdCreateArchive.Visible = false;
            cmdNew.Visible = false;
            strThisModule = strDB;
            this.Show(App.MainForm);
        }

        private void cmdNew_Click(object sender, System.EventArgs e)
        {
            AddArchive();
        }

        private void AddArchive()
        {
            if (modPPGN.Statics.boolInArchiveMode)
            {
                MessageBox.Show("Please switch to CURRENT YEAR to add new archive", "Add archive", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int intMin;
            int intMax;
            int intYear;
            int x;
            intMin = 2000;
            for (x = 1; x <= Grid.Rows - 1; x++)
            {
                if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLYEAR)) > intMin)
                {
                    intMin = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLYEAR))));
                }
            } // x
            intMin += 1;

            intYear = DateTime.Now.Year;
            if (intYear < intMin)
            {
                intYear = intMin;
            }
            intMax = DateTime.Now.Year + 1;
            if (intMax < intMin)
            {
                MessageBox.Show("No valid archive year can be created." + "\r\n" + "If you wish to overwrite an archive you must remove it first.", "No Valid Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            frmNewArchive.InstancePtr.Init(intMin, intMax, intYear);
            //FC:FINAL:MSH - i.issue #1332: assign handler for reloading archives table after creating new archive
            frmNewArchive.InstancePtr.ArchiveCreated -= new frmNewArchive.ArchiveCreatedEventHandler(this.newArchiveForm_ArchiveCreated);
            frmNewArchive.InstancePtr.ArchiveCreated += new frmNewArchive.ArchiveCreatedEventHandler(this.newArchiveForm_ArchiveCreated);
            //SettingsInfo S = new SettingsInfo();
            //S.ReloadArchives();
            //FillGrid();
        }

        private void frmArchive_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmArchive properties;
            //frmArchive.ScaleWidth	= 5880;
            //frmArchive.ScaleHeight	= 3810;
            //frmArchive.LinkTopic	= "Form1";
            //End Unmaped Properties
            //FC:FINAL:MSH - i.issue #1279: btns location won't be recalculated before showing and if btns are visible, so 
            // before showing we will hide btns for recalculating location and after change visibility
            if (!boolOkToCreate)
            {
                //FC:FINAL:MSH - i.issue #1279: incorrect btn name
                //mnuCreateArchive.Enabled = false;
                cmdCreateArchive.Visible = false;
                cmdNew.Visible = false;
            }
            else
            {
                cmdCreateArchive.Visible = true;
                cmdNew.Visible = true;
            }

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
            SetupGrid();
            FillGrid();
        }

        private void frmArchive_Resize(object sender, System.EventArgs e)
        {
            ResizeGrid();
        }



        private void Grid_DblClick(object sender, System.EventArgs e)
        {
            if (Grid.Row > 0)
            {
                RunArchive(Grid.Row);
            }
        }

        private void RunArchive(int intRow)
        {
            //FC:FINAL:MSH - issue #1727: add message that Archive mode can't be executed if any other forms are opened
            int twppOpenForms = Application.OpenForms.OfType<Form>().Where(form => form.GetType().Assembly.GetName().Name.ToUpper() == "TWPP0000").Count();
            if (twppOpenForms > 1)
            {
                MessageBox.Show("All Personal Property tabs must be closed before changing the working year", "Run Archive", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

			if (intRow > 0 && intRow <= Grid.Rows - 1)
            {
                clsDRWrapper rsLoad = new clsDRWrapper();
                int intYear = 0;
                intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(intRow, CNSTGRIDCOLYEAR))));
                if (intYear > 2000)
                {
                    rsLoad.DefaultGroup = CNSTREPPARCHIVETYPE + "_" + FCConvert.ToString(intYear);

                    // load menu color and archive year screen
                    //FC:FINAL:BBE - In other projects also commented
                    //modGlobalFunctions.SetMenuBackColor();
                    modGlobalConstants.Statics.gstrArchiveYear = Grid.TextMatrix(intRow, 0);

                    //FC:FINAL:BBE - In other projects also commented
                    //FC:FINAL:CHN - issue #1516: Add showing archive image after running archive.
					//FC:FINAL:DDU:#1826 - remove archive text from app
                    //modArchiveImage.ShowArchiveImage(ref modGlobalConstants.Statics.gstrArchiveYear);
                    modPPGN.Statics.boolInArchiveMode = true;
                    //MDIParent.InstancePtr.StatusBar1.Panels[0].Text = modGlobalConstants.MuniName + " " + modGlobalConstants.Statics.gstrArchiveYear;
                    App.MainForm.StatusBarText1 = TWSharedLibrary.Variables.Statics.DataGroup + " " + modGlobalConstants.Statics.gstrArchiveYear;
                    // do a forceupdate
                    modGlobalConstants.Statics.MuniName = TWSharedLibrary.Variables.Statics.DataGroup + " " + modGlobalConstants.Statics.gstrArchiveYear;

                    modPPGN.ModSpecificReset();

                    StaticSettings.GlobalCommandDispatcher.Send(new SetArchiveMode
                    {
	                    ArchiveMode = modGlobalConstants.Statics.gstrArchiveYear != "",
	                    ArchiveTag = modGlobalConstants.Statics.gstrArchiveYear == "" ? "Live" : rsLoad.DefaultGroup
					});
					Close();
                }
                else if (intYear == 0)
                {
                    rsLoad.DefaultGroup = "Live";
                    modGlobalConstants.Statics.gstrArchiveYear = "";
                    modPPGN.Statics.boolInArchiveMode = false;
                    App.MainForm.StatusBarText1 = TWSharedLibrary.Variables.Statics.DataGroup;
                    modGlobalConstants.Statics.MuniName = TWSharedLibrary.Variables.Statics.DataGroup;
					//modPPGN.ModSpecificReset();
					StaticSettings.GlobalCommandDispatcher.Send(new SetArchiveMode
					{
						ArchiveMode = modGlobalConstants.Statics.gstrArchiveYear != "",
						ArchiveTag = modGlobalConstants.Statics.gstrArchiveYear == "" ? "Live" : rsLoad.DefaultGroup
					});
					Close();
                }
                else
                {
                    MessageBox.Show("The archive year is not valid.", "Invalid Archive Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void mnuCreateArchive_Click(object sender, System.EventArgs e)
        {
            AddArchive();
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void FillGrid()
        {
            cArchiveUtility tArch = new cArchiveUtility();
            FCCollection tColl = new FCCollection();
            tColl = tArch.GetArchiveConnectionGroups(CNSTREPPARCHIVETYPE, 0);
            Grid.Rows = 1;
            int intRow;
            if (modPPGN.Statics.boolInArchiveMode == true)
            {
                Grid.Rows += 1;
                intRow = Grid.Rows - 1;
                Grid.TextMatrix(intRow, CNSTGRIDCOLDESC, "CURRENT YEAR DATA");
                Grid.TextMatrix(intRow, CNSTGRIDCOLYEAR, "CURRENT");
            }
            if (!(tColl == null))
            {
                foreach (ConnectionGroup tcGroup in tColl)
                {
                    Grid.Rows += 1;
                    intRow = Grid.Rows - 1;
                    Grid.TextMatrix(intRow, CNSTGRIDCOLDESC, tcGroup.Description);
                    Grid.TextMatrix(intRow, CNSTGRIDCOLYEAR, FCConvert.ToString(tcGroup.ID));
                } // tcGroup
            }

        }


        private void SetupGrid()
        {

            Grid.TextMatrix(0, CNSTGRIDCOLYEAR, "Year");
            Grid.TextMatrix(0, CNSTGRIDCOLDESC, "Description");
            Grid.ColHidden(CNSTGRIDCOLDIRECTORY, true);
            Grid.ColHidden(CNSTGRIDCOLAUTOID, true);
			//FC:FINAL:DDU:#1826 - aligned columns
			Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);

		}

        private void ResizeGrid()
        {
            int lngGridWidth = 0;


            lngGridWidth = Grid.WidthOriginal;
            Grid.ColWidth(0, FCConvert.ToInt32(0.2 * lngGridWidth));
            //FC:FINAL:CHN - issue #1512: Missing auto resize (different behavior with VB6).
            Grid.ColWidth(1, FCConvert.ToInt32(0.78 * lngGridWidth));

        }

        private void mnuRunArchive_Click(object sender, System.EventArgs e)
        {
			if (Grid.Row > 0)
            {
                RunArchive(Grid.Row);
            }
            else
            {
                MessageBox.Show("You haven't selected an archive.", "No Archive Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        //FC:FINAL:MSH - i.issue #1332: move reloading archives table to method, which we can assign to ArchiveCreated event
        private void newArchiveForm_ArchiveCreated()
        {
            SettingsInfo S = new SettingsInfo();
            S.ReloadArchives();
            FillGrid();
        }
    }
}