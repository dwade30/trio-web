﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMultiModuleEncJournals : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsGeneralInfo = new clsDRWrapper();
		// VBto upgrade warning: curDebitTotal As double	OnWrite(short, double)
		decimal curDebitTotal;
		// VBto upgrade warning: curCreditTotal As double	OnWrite(short, double)
		decimal curCreditTotal;
		bool blnFirstControl;

		public rptMultiModuleEncJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptMultiModuleEncJournals_ReportEnd;
		}

        private void RptMultiModuleEncJournals_ReportEnd(object sender, EventArgs e)
        {
			rsInfo.DisposeOf();
            rsGeneralInfo.DisposeOf();

		}

        public static rptMultiModuleEncJournals InstancePtr
		{
			get
			{
				return (rptMultiModuleEncJournals)Sys.GetInstance(typeof(rptMultiModuleEncJournals));
			}
		}

		protected rptMultiModuleEncJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsGeneralInfo.Dispose();
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsInfo.EndOfFile())
			{
				TryAgain:
				;
				rsGeneralInfo.MoveNext();
				if (rsGeneralInfo.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
					rsInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + FCConvert.ToString(rsGeneralInfo.Get_Fields_Int32("ID")), "TWBD0000.vb1");
					if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
					{
						rsInfo.MoveLast();
						rsInfo.MoveFirst();
					}
					else
					{
						goto TryAgain;
					}
				}
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsDesc = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			rsDesc.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber), "TWBD0000.vb1");
			if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
			{
				lblDescription.Text = rsDesc.Get_Fields_String("Description");
			}
			else
			{
				lblDescription.Text = "UNKNOWN";
			}
			Label6.Text = "Journal No. " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + "       Post Date: " + DateTime.Today.ToShortDateString() + "       Type: EN";
			rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period + " ORDER BY ID", "TWBD0000.vb1");
			rsInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + FCConvert.ToString(rsGeneralInfo.Get_Fields_Int32("ID")), "TWBD0000.vb1");
			if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
			{
				rsInfo.MoveLast();
				rsInfo.MoveFirst();
			}
			if (rsGeneralInfo.EndOfFile() != true && rsGeneralInfo.BeginningOfFile() != true)
			{
				rsGeneralInfo.MoveLast();
				rsGeneralInfo.MoveFirst();
			}
			curDebitTotal = 0;
			curCreditTotal = 0;
			blnFirstControl = false;
			Line2.Visible = false;
			rsDesc.DisposeOf();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (blnFirstControl == false)
			{
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Project")) == "CTRL")
				{
					Line2.Visible = true;
					blnFirstControl = true;
				}
			}
			else
			{
				if (Line2.Visible == true)
				{
					Line2.Visible = false;
				}
			}
			// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
			Field1.Text = Strings.Format(rsGeneralInfo.Get_Fields("Period"), "00");
			Field3.Text = rsInfo.Get_Fields_String("Description");
			// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
			Field6.Text = rsInfo.Get_Fields_String("Account");
			Field2.Text = Strings.Format(rsGeneralInfo.Get_Fields_DateTime("EncumbrancesDate"), "MM/dd/yyyy");
			// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) > 0)
			{
				// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
				Field14.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
				Field8.Text = "";
			}
			else
			{
				// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
				Field8.Text = Strings.Format(rsInfo.Get_Fields("Amount") * -1, "#,##0.00");
				Field14.Text = "";
			}
			if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
			{
				// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) > 0)
				{
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					curDebitTotal += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
				}
				else
				{
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					curCreditTotal += (FCConvert.ToDecimal(rsInfo.Get_Fields("Amount")) * -1);
				}
			}
			// TODO: Check the table for the column [PO] and replace with corresponding Get_Field method
			Field13.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields("PO"));
			Field9.Text = rsInfo.Get_Fields_String("Project");
			if (FCConvert.ToString(rsInfo.Get_Fields_String("Project")) == "CTRL")
			{
				Field11.Text = "";
			}
			else
			{
				Field11.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(rsGeneralInfo.Get_Fields_Int32("VendorNumber")), 5);
			}
			rsInfo.MoveNext();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldDebitTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
			fldCreditTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
			rptSubTotals.Report = rptMultiModuleJournalTotals.InstancePtr;
			rptSubTotals.Report.UserData = this.UserData;
		}

		private void rptMultiModuleEncJournals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMultiModuleEncJournals.Icon	= "rptMultiModuleEncJournals.dsx":0000";
			//rptMultiModuleEncJournals.Left	= 0;
			//rptMultiModuleEncJournals.Top	= 0;
			//rptMultiModuleEncJournals.Width	= 11880;
			//rptMultiModuleEncJournals.Height	= 8595;
			//rptMultiModuleEncJournals.StartUpPosition	= 3;
			//rptMultiModuleEncJournals.SectionData	= "rptMultiModuleEncJournals.dsx":058A;
			//End Unmaped Properties
		}
	}
}
