﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailGroups.
	/// </summary>
	partial class frmEmailGroups : BaseForm
	{
		public fecherFoundation.FCGrid GridDelete;
		public fecherFoundation.FCGrid GridContacts;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCTextBox txtQuickName;
		public fecherFoundation.FCToolBar Toolbar1;
		private fecherFoundation.FCToolBarButton btnSaveGroup;
		private fecherFoundation.FCToolBarButton btnAddGroup;
		private fecherFoundation.FCToolBarButton btnDeleteGroup;
		private fecherFoundation.FCToolBarButton btnSearchGroup;
		private fecherFoundation.FCToolBarButton btnAddToGroup;
		private fecherFoundation.FCToolBarButton btnDeleteContact;
		private fecherFoundation.FCToolBarButton btnEditContacts;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddContact;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteContact;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuPrintContacts;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAllGroups;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmailGroups));
			this.GridDelete = new fecherFoundation.FCGrid();
			this.GridContacts = new fecherFoundation.FCGrid();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.txtQuickName = new fecherFoundation.FCTextBox();
			this.Toolbar1 = new fecherFoundation.FCToolBar();
			this.btnSaveGroup = new fecherFoundation.FCToolBarButton();
			this.btnAddGroup = new fecherFoundation.FCToolBarButton();
			this.btnDeleteGroup = new fecherFoundation.FCToolBarButton();
			this.btnSearchGroup = new fecherFoundation.FCToolBarButton();
			this.btnAddToGroup = new fecherFoundation.FCToolBarButton();
			this.btnDeleteContact = new fecherFoundation.FCToolBarButton();
			this.btnEditContacts = new fecherFoundation.FCToolBarButton();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddContact = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteContact = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintContacts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintAllGroups = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdContacts = new fecherFoundation.FCButton();
			this.cmdAddGroup = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridContacts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContacts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 411);
			this.BottomPanel.Size = new System.Drawing.Size(606, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridDelete);
			this.ClientArea.Controls.Add(this.GridContacts);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.txtQuickName);
			//FC:FINAL:MSH - i.issue #1597: remove toolbar
			//this.ClientArea.Controls.Add(this.Toolbar1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Location = new System.Drawing.Point(0, 67);
			this.ClientArea.Size = new System.Drawing.Size(606, 344);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSearch);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdAddGroup);
			this.TopPanel.Controls.Add(this.cmdContacts);
			this.TopPanel.Size = new System.Drawing.Size(606, 67);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdContacts, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddGroup, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(26, 26);
			this.HeaderText.Size = new System.Drawing.Size(93, 30);
			this.HeaderText.Text = "Groups";
			// 
			// GridDelete
			// 
			this.GridDelete.AllowSelection = false;
			this.GridDelete.AllowUserToResizeColumns = false;
			this.GridDelete.AllowUserToResizeRows = false;
			this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
			this.GridDelete.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridDelete.ColumnHeadersHeight = 30;
			this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDelete.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDelete.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDelete.FixedCols = 0;
			this.GridDelete.FixedRows = 0;
			this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.FrozenCols = 0;
			this.GridDelete.GridColor = System.Drawing.Color.Empty;
			this.GridDelete.Location = new System.Drawing.Point(504, 3);
			this.GridDelete.Name = "GridDelete";
			this.GridDelete.ReadOnly = true;
			this.GridDelete.RowHeadersVisible = false;
			this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDelete.RowHeightMin = 0;
			this.GridDelete.Rows = 0;
			this.GridDelete.ShowColumnVisibilityMenu = false;
			this.GridDelete.Size = new System.Drawing.Size(63, 21);
			this.GridDelete.StandardTab = true;
			this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDelete.TabIndex = 5;
			this.GridDelete.Visible = false;
			// 
			// GridContacts
			// 
			this.GridContacts.AllowSelection = false;
			this.GridContacts.AllowUserToResizeColumns = false;
			this.GridContacts.AllowUserToResizeRows = false;
			this.GridContacts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridContacts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridContacts.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridContacts.BackColorBkg = System.Drawing.Color.Empty;
			this.GridContacts.BackColorFixed = System.Drawing.Color.Empty;
			this.GridContacts.BackColorSel = System.Drawing.Color.Empty;
			this.GridContacts.Cols = 5;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridContacts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridContacts.ColumnHeadersHeight = 30;
			this.GridContacts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridContacts.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridContacts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridContacts.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.GridContacts.FixedCols = 0;
			this.GridContacts.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridContacts.FrozenCols = 0;
			this.GridContacts.GridColor = System.Drawing.Color.Empty;
			this.GridContacts.Location = new System.Drawing.Point(30, 144);
			this.GridContacts.Name = "GridContacts";
			this.GridContacts.ReadOnly = true;
			this.GridContacts.RowHeadersVisible = false;
			this.GridContacts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridContacts.RowHeightMin = 0;
			this.GridContacts.Rows = 1;
			this.GridContacts.ShowColumnVisibilityMenu = false;
			this.GridContacts.Size = new System.Drawing.Size(558, 181);
			this.GridContacts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridContacts.TabIndex = 6;
			this.GridContacts.KeyDown += new Wisej.Web.KeyEventHandler(this.GridContacts_KeyDown);
			// 
			// txtDescription
			// 
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Info;
			this.txtDescription.Location = new System.Drawing.Point(138, 25);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(429, 40);
			this.txtDescription.TabIndex = 2;
			this.txtDescription.Validating += new System.ComponentModel.CancelEventHandler(this.txtDescription_Validating);
			// 
			// txtQuickName
			// 
			this.txtQuickName.AutoSize = false;
			this.txtQuickName.BackColor = System.Drawing.SystemColors.Info;
			this.txtQuickName.Location = new System.Drawing.Point(138, 85);
			this.txtQuickName.Name = "txtQuickName";
			this.txtQuickName.Size = new System.Drawing.Size(429, 40);
			this.txtQuickName.TabIndex = 4;
			this.txtQuickName.TextChanged += new System.EventHandler(this.txtQuickName_TextChanged);
			// 
			// Toolbar1
			// 
			//////this.Toolbar1.BackColor = System.Drawing.SystemColors.Control;
			this.Toolbar1.ButtonHeight = 0;
			this.Toolbar1.Buttons.Add(this.btnSaveGroup);
			this.Toolbar1.Buttons.Add(this.btnAddGroup);
			this.Toolbar1.Buttons.Add(this.btnDeleteGroup);
			this.Toolbar1.Buttons.Add(this.btnSearchGroup);
			this.Toolbar1.Buttons.Add(this.btnAddToGroup);
			this.Toolbar1.Buttons.Add(this.btnDeleteContact);
			this.Toolbar1.Buttons.Add(this.btnEditContacts);
			this.Toolbar1.Buttons.Add(this.btnSaveGroup);
			this.Toolbar1.Buttons.Add(this.btnAddGroup);
			this.Toolbar1.Buttons.Add(this.btnDeleteGroup);
			this.Toolbar1.Buttons.Add(this.btnSearchGroup);
			this.Toolbar1.Buttons.Add(this.btnAddToGroup);
			this.Toolbar1.Buttons.Add(this.btnDeleteContact);
			this.Toolbar1.Buttons.Add(this.btnEditContacts);
			this.Toolbar1.ButtonSize = new System.Drawing.Size(24, 24);
			this.Toolbar1.ButtonWidth = 0;
			this.Toolbar1.Dock = Wisej.Web.DockStyle.None;
			this.Toolbar1.ImageList = this.ImageList1;
			this.Toolbar1.Location = new System.Drawing.Point(0, 0);
			this.Toolbar1.Name = "Toolbar1";
			this.Toolbar1.Size = new System.Drawing.Size(100, 26);
			this.Toolbar1.TabIndex = 0;
			this.Toolbar1.TabStop = false;
			this.Toolbar1.ButtonClick += new System.EventHandler<fecherFoundation.FCToolBarButtonClickEventArgs>(this.Toolbar1_ButtonClick);
			// 
			// btnSaveGroup
			// 
			this.btnSaveGroup.ImageIndex = 0;
			this.btnSaveGroup.Key = "btnSaveGroup";
			this.btnSaveGroup.Name = "btnSaveGroup";
			this.btnSaveGroup.ToolTipText = "Save";
			this.btnSaveGroup.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnAddGroup
			// 
			this.btnAddGroup.ImageIndex = 1;
			this.btnAddGroup.Key = "btnAddGroup";
			this.btnAddGroup.Name = "btnAddGroup";
			this.btnAddGroup.ToolTipText = "Add New Group";
			this.btnAddGroup.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnDeleteGroup
			// 
			this.btnDeleteGroup.ImageIndex = 2;
			this.btnDeleteGroup.Key = "btnDeleteGroup";
			this.btnDeleteGroup.Name = "btnDeleteGroup";
			this.btnDeleteGroup.ToolTipText = "Delete Group";
			this.btnDeleteGroup.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnSearchGroup
			// 
			this.btnSearchGroup.ImageIndex = 3;
			this.btnSearchGroup.Key = "btnSearchGroup";
			this.btnSearchGroup.Name = "btnSearchGroup";
			this.btnSearchGroup.ToolTipText = "Search Groups";
			this.btnSearchGroup.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnAddToGroup
			// 
			this.btnAddToGroup.ImageIndex = 4;
			this.btnAddToGroup.Key = "btnAddToGroup";
			this.btnAddToGroup.Name = "btnAddToGroup";
			this.btnAddToGroup.ToolTipText = "Add a contact to the group";
			this.btnAddToGroup.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnDeleteContact
			// 
			this.btnDeleteContact.ImageIndex = 5;
			this.btnDeleteContact.Key = "btnDeleteContact";
			this.btnDeleteContact.Name = "btnDeleteContact";
			this.btnDeleteContact.ToolTipText = "Remove contact from group";
			this.btnDeleteContact.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditContacts
			// 
			this.btnEditContacts.ImageIndex = 6;
			this.btnEditContacts.Key = "btnEditContacts";
			this.btnEditContacts.Name = "btnEditContacts";
			this.btnEditContacts.ToolTipText = "Contacts";
			this.btnEditContacts.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// ImageList1
			// 
			this.ImageList1.ImageSize = new System.Drawing.Size(24, 24);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 99);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(82, 15);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "QUICKNAME";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 39);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(92, 15);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "DESCRIPTION";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcess
			});
			this.MainMenu1.Name = null;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAddContact,
				this.mnuDeleteContact,
				this.mnuSepar2,
				this.mnuPrintContacts,
				this.mnuPrintAllGroups
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuAddContact
			// 
			this.mnuAddContact.Index = 0;
			this.mnuAddContact.Name = "mnuAddContact";
			this.mnuAddContact.Text = "Add Contact";
			this.mnuAddContact.Click += new System.EventHandler(this.mnuAddContact_Click);
			// 
			// mnuDeleteContact
			// 
			this.mnuDeleteContact.Index = 1;
			this.mnuDeleteContact.Name = "mnuDeleteContact";
			this.mnuDeleteContact.Text = "Remove Contact";
			this.mnuDeleteContact.Click += new System.EventHandler(this.mnuDeleteContact_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 2;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuPrintContacts
			// 
			this.mnuPrintContacts.Index = 3;
			this.mnuPrintContacts.Name = "mnuPrintContacts";
			this.mnuPrintContacts.Text = "Print Group";
			this.mnuPrintContacts.Click += new System.EventHandler(this.mnuPrintContacts_Click);
			// 
			// mnuPrintAllGroups
			// 
			this.mnuPrintAllGroups.Index = 4;
			this.mnuPrintAllGroups.Name = "mnuPrintAllGroups";
			this.mnuPrintAllGroups.Text = "Print All Groups";
			this.mnuPrintAllGroups.Click += new System.EventHandler(this.mnuPrintAllGroups_Click);
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(276, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(98, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdContacts
			// 
			this.cmdContacts.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdContacts.AppearanceKey = "toolbarButton";
			this.cmdContacts.Location = new System.Drawing.Point(501, 28);
			this.cmdContacts.Name = "cmdContacts";
			this.cmdContacts.Size = new System.Drawing.Size(69, 24);
			this.cmdContacts.TabIndex = 53;
			this.cmdContacts.Text = "Contacts";
			this.cmdContacts.Click += new System.EventHandler(this.cmdContacts_Click);
			// 
			// cmdAddGroup
			// 
			this.cmdAddGroup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddGroup.AppearanceKey = "toolbarButton";
			this.cmdAddGroup.Location = new System.Drawing.Point(158, 28);
			this.cmdAddGroup.Name = "cmdAddGroup";
			this.cmdAddGroup.Size = new System.Drawing.Size(116, 24);
			this.cmdAddGroup.TabIndex = 54;
			this.cmdAddGroup.Text = "Add New Group";
			this.cmdAddGroup.Click += new System.EventHandler(this.cmdAddContact_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(277, 28);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(95, 24);
			this.cmdDelete.TabIndex = 55;
			this.cmdDelete.Text = "Delete Group";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDeleteContact_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSearch.AppearanceKey = "toolbarButton";
			this.cmdSearch.ImageSource = "icon_search";
			this.cmdSearch.Location = new System.Drawing.Point(375, 28);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(123, 24);
			this.cmdSearch.TabIndex = 56;
			this.cmdSearch.Text = "Search Groups";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// frmEmailGroups
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(606, 519);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmEmailGroups";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Groups";
			this.Load += new System.EventHandler(this.frmEmailGroups_Load);
			this.Activated += new System.EventHandler(this.frmEmailGroups_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmailGroups_KeyDown);
			this.Resize += new System.EventHandler(this.frmEmailGroups_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridContacts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdContacts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			this.ResumeLayout(false);
		}

		private void GridContacts_KeyDown1(object sender, KeyEventArgs e)
		{
			throw new NotImplementedException();
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdContacts;
		public FCButton cmdAddGroup;
		public FCButton cmdDelete;
		public FCButton cmdSearch;
	}
}
