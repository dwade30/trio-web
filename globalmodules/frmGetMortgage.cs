﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;
using modGlobalRoutines = TWCL0000.modGlobal;


#elif TWBL0000
using TWBL0000;
using modExtraModules = TWBL0000.modGlobalVariables;
using modGlobal = TWBL0000.modCollectionsRelated;


#elif TWRE0000
using TWRE0000;
using modGlobal = TWRE0000.modGlobalVariables;
using modExtraModules = TWRE0000.modGlobalVariables;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
using modGlobalRoutines = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmGetMortgage.
	/// </summary>
	public partial class frmGetMortgage : BaseForm
	{
		public frmGetMortgage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetMortgage InstancePtr
		{
			get
			{
				return (frmGetMortgage)Sys.GetInstance(typeof(frmGetMortgage));
			}
		}

		protected frmGetMortgage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/18/2005              *
		// ********************************************************
		public string auxCmbAccountItem1 = "Real Estate Account";
		public string auxCmbAccountItem2 = "Utility Account";
		public string auxCmbSearchByItem0 = "Name";
		public string auxCmbSearchByItem1 = "Map / Lot";
		public string auxCmbSearchByItem2 = "Location";
		bool boolLoaded;
		int lngLastMH;
		int lngLastRE;
		int lngLastUT;

		private void cmdClear_Click()
		{
			txtSearch.Text = "";
			txtAccount.Text = "";
		}

		private void cmdSearch_Click()
		{
			StartSearch();
		}

		private void frmGetMortgage_Activated(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (!boolLoaded)
			{
				if (txtAccount.Enabled && txtAccount.Visible)
				{
					txtAccount.Focus();
				}
				boolLoaded = true;
			}
			// get the last account numbers used to default values each time the user hits this form (just to make sure they are current)
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastMHNumber", ref strTemp);
			lngLastMH = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastRENumber", ref strTemp);
			lngLastRE = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastUTNumber", ref strTemp);
			lngLastUT = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			//FC:FINAL:MSH - i.issue #1104: in original app this items are hidden for 1 and 3 modules
			#if !TWBL0000 && !TWRE0000
			this.mnuFileReportsMHwithUT.Visible = true;
			this.mnuFileReportsMHwithUTAmounts.Visible = true;
			this.mnuFileReportsUTwithMH.Visible = true;
			#else
						            this.mnuFileReportsMHwithUT.Visible = false;
            this.mnuFileReportsMHwithUTAmounts.Visible = false;
            this.mnuFileReportsUTwithMH.Visible = false;
#endif
		}

		private void frmGetMortgage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmGetMortgage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			clsDRWrapper clsTemp = new clsDRWrapper();
			if (KeyAscii == 13)
			{
				// return key
				KeyAscii = 0;
				if (Strings.Trim(txtSearch.Text).Length > 0)
				{
					StartSearch(true);
				}
				else
				{
					StartSearch();
				}
				// If UCase(Screen.ActiveControl.Name) = "TXTACCOUNT" Then
				// KeyAscii = 0
				// StartSearch
				// ElseIf UCase(Screen.ActiveControl.Name) = "TXTSEARCH" Then
				// KeyAscii = 0
				// StartSearch True
				// End If
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmGetMortgage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetMortgage.Icon	= "frmGetMortgage.frx":0000";
			//frmGetMortgage.ScaleWidth	= 9045;
			//frmGetMortgage.ScaleHeight	= 6930;
			//frmGetMortgage.LinkTopic	= "Form1";
			//frmGetMortgage.LockControls	= -1  'True;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmGetMortgage.frx":058A";
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				// If Not ValidPermissions(Me, CNSTEDITMORTGAGE, False) Then
				// mnuNewMortgageHolder.Enabled = False
				// End If
				if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
				{
                    if (!cmbAccount.Items.Contains(auxCmbAccountItem1))
                    {
                        if (cmbAccount.Items.Count > 1)
                        {
                            cmbAccount.Items.Insert(1, auxCmbAccountItem1);
                        }
                        else
                        {
                            cmbAccount.Items.Add(auxCmbAccountItem1);
                        }
                    }
				}
				else
				{
                    if (cmbAccount.Items.Contains(auxCmbAccountItem1))
                    {
                        cmbAccount.Items.Remove(auxCmbAccountItem1);
                    }
                }
				if (modGlobalConstants.Statics.gboolUT)
				{
                    if (!cmbAccount.Items.Contains(auxCmbAccountItem2))
                    {
                        cmbAccount.Items.Add(auxCmbAccountItem2);
                    }
                }
				else
				{
                    if (cmbAccount.Items.Contains(auxCmbAccountItem1))
                    {
                        cmbAccount.Items.Remove(auxCmbAccountItem1);
                    }
                }
				//#if ModuleID==1 || ModuleID==3	// DeadCode
				#if ModuleID
																mnuFileReportsMHwithUT.Visible = false;
				mnuFileReportsMHwithUTAmounts.Visible = false;
				mnuFileReportsUTwithMH.Visible = false;

#endif
				cmbAccount.SelectedIndex = 0;
				lblAccountNumber.Text = "Enter the Mortgage Holder and press 'Enter' to continue";
				// get the last account numbers used to default values
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastMHNumber", ref strTemp);
				lngLastMH = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastRENumber", ref strTemp);
				lngLastRE = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastUTNumber", ref strTemp);
				lngLastUT = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				txtAccount.Text = FCConvert.ToString(lngLastMH);
				// initialize the MH Number
				return;
			}
			catch (Exception ex)
			{
				
				txtAccount.Text = "";
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Form");
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			App.MainForm.Show();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuFileLabels_Click(object sender, System.EventArgs e)
		{
			frmCustomLabels.InstancePtr.Unload();
			App.DoEvents();
			frmCustomLabels.InstancePtr.Show(App.MainForm);
			Close();
		}

		private void mnuFileMortgageHolderListing_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:RPU:#i1167 - Show the question before report because otherwise it's not shown and select the first option in the comboBox as in Original
			frmQuestion.InstancePtr.Init(20, "By Mortgage Holder ID", "By Mortgage Holder Name", "Order Report By", 0);
			// rptMortgageHolderMasterList.Show , MDIParent
			frmReportViewer.InstancePtr.Init(rptMortgageHolderMasterList.InstancePtr, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "MortgageHolder", false, false);
		}

		private void mnuFileReportsMHwithRE_Click(object sender, System.EventArgs e)
		{
			// this will show the report that has MH with the associated RE Accounts
			rptMHWithREAccounts.InstancePtr.Init(true);
		}

		private void mnuFileReportsMHwithREAmounts_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngYear;
				clsDRWrapper rsLYB = new clsDRWrapper();
				if (modGlobal.Statics.gstrLastYearBilled == "")
				{
					rsLYB.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster WHERE TaxDue1 > 0 ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
					if (!rsLYB.EndOfFile())
					{
						modGlobal.Statics.gstrLastYearBilled = FCConvert.ToString(rsLYB.Get_Fields_Int32("BillingYear"));
					}
					else
					{
						modGlobal.Statics.gstrLastYearBilled = FCConvert.ToString(DateTime.Today.Year) + "1";
					}
				}
				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modGlobal.Statics.gstrLastYearBilled, 4))));
				rptREbyHolder.InstancePtr.Init(false, true, lngYear);
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Last Year Billed");
			}
		}

		private void mnuFileReportsMHwithREAmountsSplit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngYear;
				clsDRWrapper rsLYB = new clsDRWrapper();
				if (modGlobal.Statics.gstrLastYearBilled == "")
				{
					rsLYB.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster WHERE (TaxDue1 > 0 Or TaxDue2 > 0 Or TaxDue3 > 0 Or TaxDue4 > 0) ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
					if (!rsLYB.EndOfFile())
					{
						modGlobal.Statics.gstrLastYearBilled = FCConvert.ToString(rsLYB.Get_Fields_Int32("BillingYear"));
					}
					else
					{
						modGlobal.Statics.gstrLastYearBilled = FCConvert.ToString(DateTime.Today.Year) + "1";
					}
				}
				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modGlobal.Statics.gstrLastYearBilled, 4))));
				rptREbyHolderSplit.InstancePtr.Init(false, true, lngYear);
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Generating Report");
			}
		}

		private void mnuFileReportsMHwithUT_Click(object sender, System.EventArgs e)
		{
			// this will show the report that has MH with the associated UT Accounts
			//#if !(ModuleID==1) && !(ModuleID==3)	// ActiveBranch
			#if !TWBL0000 && !TWRE0000
			rptMHWithUTAccounts.InstancePtr.Init(true);
			#endif
		}

		private void mnuFileReportsMHwithUTAmounts_Click(object sender, System.EventArgs e)
		{
			//#if !(ModuleID==1) && !(ModuleID==3)	// ActiveBranch
			#if !TWBL0000 && !TWRE0000
			rptUTbyHolder.InstancePtr.Init(false);
			#endif
		}

		private void mnuFileReportsREwithMH_Click(object sender, System.EventArgs e)
		{
			// this will show the report that has RE Accounts with the associated MH
			rptREMortgage.InstancePtr.Init(true);
		}

		private void mnuFileReportsUTwithMH_Click(object sender, System.EventArgs e)
		{
			//#if !(ModuleID==1) && !(ModuleID==3)	// ActiveBranch
			#if !TWBL0000 && !TWRE0000
			rptUTMortgage.InstancePtr.Init(true);
			#endif
		}

		private void mnuGetAccount_Click(object sender, System.EventArgs e)
		{
			if (Strings.Trim(txtAccount.Text).Length > 0 && Strings.Trim(txtSearch.Text) == "")
			{
				StartSearch();
			}
			else
			{
				StartSearch(true);
			}
		}

		private void mnuNewMortgageHolder_Click(object sender, System.EventArgs e)
		{
			//! Load frmMortgageHolder;
			// send a 0 to indicate creating a new one
			frmMortgageHolder.InstancePtr.Init(0, 1);
			frmMortgageHolder.InstancePtr.Show(App.MainForm);
		}

		private void optAccount_Click(int Index, object sender, System.EventArgs e)
		{
            switch (cmbAccount.Text)
			{
				case "Mortgage Holder Number":
					{
						lblAccountNumber.Text = "Enter the Mortgage Holder and press 'Enter' to continue";
						cmbSearchBy.Clear();
						cmbSearchBy.AddItem(auxCmbSearchByItem0);
						cmbSearchBy.SelectedIndex = 0;
						lblSearchBy.Text = "Mortgage Holder";
						txtAccount.Text = FCConvert.ToString(lngLastMH);
						break;
					}
				case "Real Estate Account":
					{
						lblAccountNumber.Text = "Enter the Account Number and press 'Enter' to continue";
						cmbSearchBy.Clear();
						cmbSearchBy.AddItem(auxCmbSearchByItem0);
						cmbSearchBy.AddItem(auxCmbSearchByItem1);
						cmbSearchBy.AddItem(auxCmbSearchByItem2);
						cmbSearchBy.SelectedIndex = 0;
						lblSearchBy.Text = "Real Estate";
						txtAccount.Text = FCConvert.ToString(lngLastRE);
						break;
					}
				case "Utility Account":
					{
						lblAccountNumber.Text = "Enter the Account Number and press 'Enter' to continue";
						cmbSearchBy.Clear();
						cmbSearchBy.AddItem(auxCmbSearchByItem0);
						cmbSearchBy.AddItem(auxCmbSearchByItem1);
						cmbSearchBy.AddItem(auxCmbSearchByItem2);
						cmbSearchBy.SelectedIndex = 0;
						lblSearchBy.Text = "Utility";
						txtAccount.Text = FCConvert.ToString(lngLastUT);
						break;
					}
			}
			//end switch
			if (txtAccount.Enabled && txtAccount.Visible)
			{
				txtAccount.Focus();
			}
		}

		private void optAccount_Click(object sender, System.EventArgs e)
		{
			int index = cmbAccount.SelectedIndex;
			optAccount_Click(index, sender, e);
		}

		private void StartSearch(bool boolSearch = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper clsTemp = new clsDRWrapper();
				string strSQL = "";
				string strWhere = "";
				string strBeginLike = "";
				string strEndLike = "";
				string strOrderBy = "";
				string strInnerJoin = "";
				bool boolJoined = false;
				bool boolMatches = false;
				int lngTemp = 0;
				string strSearchText;
				string strDBName;
				strDBName = "";
				strSearchText = modGlobalFunctions.EscapeQuotes(Strings.Trim(txtSearch.Text));
				// check to see if the account box has a number first, if it does not then
				if (Strings.Trim(txtAccount.Text) == "" && Strings.Trim(strSearchText) == "")
				{
					// nothing entered in either
					FCMessageBox.Show("Please enter a value in either the Account Field or the Search Field.", MsgBoxStyle.Information, "Data Entry");
					return;
				}
				else if (((Strings.Trim(txtAccount.Text) != "" && Strings.Trim(strSearchText) == "") || (Strings.Trim(txtAccount.Text) != "" && Strings.Trim(strSearchText) != "")) && boolSearch == false)
				{
					// just the account number or both full
					strSearchText = "";
					if (Conversion.Val(txtAccount.Text) <= 0)
					{
						string title = string.Empty;
						#if TWBL0000
														                        title = "TWBL0000";
#elif TWCL0000
														                        title = "TWCL0000";
#elif TWCL0001
														                        title = "TWCL0001";
#elif TWRE0000
														                        title = "TWRE0000";
#elif TWUT0000
														                        title = "TWUT0000";
#endif
						FCMessageBox.Show("Invalid account number", MsgBoxStyle.Information, title);
						return;
					}
					// check if its a valid account
					if (cmbAccount.Text == "Mortgage Holder Number")
					{
						// mortgage holder
						clsTemp.OpenRecordset("SELECT ID FROM MortgageHolders WHERE ID = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), "CentralData");
						if (!clsTemp.EndOfFile())
						{
							//! Load frmMortgageHolder;
							// send mortgage number and set to 1 to indicate it is a mortgage holder
							frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), 1);
							frmMortgageHolder.InstancePtr.Show(App.MainForm);
						}
						else
						{
							FCMessageBox.Show("The mortgage holder number doesn't exist.", MsgBoxStyle.Information, "No Mortgage Holder Match");
							return;
						}
					}
					else if (cmbAccount.Text == "Real Estate Account")
					{
						// real estate account number
						clsTemp.OpenRecordset("SELECT ID FROM Master WHERE ISNULL(RSDeleted,0) = 0 AND RSCard = 1 AND RSAccount = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), modExtraModules.strREDatabase);
						if (!clsTemp.EndOfFile())
						{
							//! Load frmMortgageHolder;
							// send re account and set to 2 to indicate it is a re number
							frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), 2);
							frmMortgageHolder.InstancePtr.Show(App.MainForm);
						}
						else
						{
							FCMessageBox.Show("The Real Estate Account either doesn't exist or is deleted", MsgBoxStyle.Information, "No Real Estate Match");
							return;
						}
					}
					else
					{
						// Utility Account Number
						clsTemp.OpenRecordset("SELECT REAccount, UseMortgageHolder FROM Master WHERE ISNULL(Deleted,0) = 0 AND AccountNumber = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), modExtraModules.strUTDatabase);
						if (!clsTemp.EndOfFile())
						{
							// MAL@20071106: Correct the way that it selects mortgage holders
							// Tracker Reference: 11257
							if (FCConvert.ToInt32(clsTemp.Get_Fields_Int32("REAccount")) > 0 && modGlobalConstants.Statics.gboolRE && clsTemp.Get_Fields_Boolean("UseMortgageHolder") == true)
							{
								// If clsTemp.Get_Fields("REAccount") > 0 And gboolRE Then
								lngTemp = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("REAccount"));
								clsTemp.OpenRecordset("SELECT RSAccount FROM Master WHERE NOT (RSDeleted = 1) AND RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngTemp), modExtraModules.strREDatabase);
								if (!clsTemp.EndOfFile())
								{
									FCMessageBox.Show("This Utility account is linked to Real Estate account #" + FCConvert.ToString(clsTemp.Get_Fields_Int32("RSAccount")) + ".", MsgBoxStyle.Information, "Linked Account");
									//! Load frmMortgageHolder;
									// send RE Account associated with the UT Account and set to 2 to indicate it is a RE Account Number
									frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(clsTemp.Get_Fields_Int32("RSAccount")), 2, true);
									frmMortgageHolder.InstancePtr.Show(App.MainForm);
									modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastUTNumber", modGlobalRoutines.PadToString(FCConvert.ToInt32(txtAccount.Text), 6));
								}
								else
								{
									FCMessageBox.Show("This Utility account was linked to Real Estate account #" + FCConvert.ToString(lngTemp) + ", but the link is invalid and will be removed.", MsgBoxStyle.Information, "Linked Account");
									clsTemp.OpenRecordset("SELECT * FROM Master WHERE ISNULL(Deleted,0) = 0 AND AccountNumber = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), modExtraModules.strUTDatabase);
									clsTemp.Edit();
									clsTemp.Set_Fields("REAccount", 0);
									clsTemp.Update();
									// load the UT account by itself
									//! Load frmMortgageHolder;
									// send UT Account and set to 3 to indicate it is a UT Account Number
									frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), 3, false);
									frmMortgageHolder.InstancePtr.Show(App.MainForm);
								}
							}
							else
							{
								//! Load frmMortgageHolder;
								// send UT Account and set to 3 to indicate it is a UT Account Number
								frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), 3, false);
								frmMortgageHolder.InstancePtr.Show(App.MainForm);
							}
						}
						else
						{
							FCMessageBox.Show("The Utility account either doesn't exist or is deleted.", MsgBoxStyle.Information, "No Utility Match");
							return;
						}
					}
				}
				else
				{
					// just the search string
					// dynamically build sql string
					if (Strings.Trim(strSearchText) == string.Empty)
					{
						FCMessageBox.Show("You must enter criteria to search for.", MsgBoxStyle.Information, "No Search Criteria");
						return;
					}
					if (cmbAccount.Text == "Mortgage Holder Number")
					{
						strSQL = "SELECT * FROM MortgageHolders ";
						strDBName = "CentralData";
					}
					else if (cmbAccount.Text == "Real Estate Account")
					{
						strSQL = "SELECT * FROM Master as c CROSS APPLY " + clsTemp.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.OwnerPartyID) as p ";
						strDBName = "RealEstate";
					}
					else
					{
						strSQL = "SELECT c.*, p.FullNameLF as OwnerFullNameLF, q.FullNameLF as BillingFullNameLF FROM Master as c CROSS APPLY " + clsTemp.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.OwnerPartyID) as p CROSS APPLY " + clsTemp.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.BillingPartyID) as q ";
						strDBName = "UtilityBilling";
					}
					if (cmbContain.SelectedIndex == 0)
					{
						// contains
						strBeginLike = "'%";
						strEndLike = "%'";
					}
					else if (cmbContain.SelectedIndex == 1)
					{
						// starts with
						strBeginLike = "'";
						strEndLike = "%'";
					}
					else
					{
						// ends with
						strBeginLike = "'%";
						strEndLike = "'";
					}
					// build the where clause
					if (cmbSearchBy.SelectedIndex == 0)
					{
						// mortgage holder name
						if (cmbAccount.Text == "Mortgage Holder Number")
						{
							strWhere = " Name LIKE " + strBeginLike + Strings.Trim(strSearchText) + strEndLike;
						}
						else if (cmbAccount.Text == "Real Estate Account")
						{
							strWhere = " DeedName1 LIKE " + strBeginLike + Strings.Trim(strSearchText) + strEndLike + " AND RSCard = 1";
						}
						else
						{
							// UT
							strWhere = " p.FullNameLF LIKE " + strBeginLike + Strings.Trim(strSearchText) + strEndLike + " OR q.FullNameLF LIKE " + strBeginLike + Strings.Trim(strSearchText) + strEndLike;
						}
					}
					else if (cmbSearchBy.SelectedIndex == 1)
					{
						// re maplot
						if (cmbAccount.Text == "Real Estate Account")
						{
							strWhere = " RSMaplot LIKE " + strBeginLike + Strings.Trim(strSearchText) + strEndLike + " AND RSCard = 1";
						}
						else if (cmbAccount.Text == "Utility Account")
						{
							// UT
							strWhere = " MapLot Like " + strBeginLike + Strings.Trim(strSearchText) + strEndLike;
						}
					}
					else if (cmbSearchBy.SelectedIndex == 2)
					{
						// re location
						if (cmbAccount.Text == "Real Estate Account")
						{
							strWhere = " RSLocstreet LIKE " + strBeginLike + Strings.Trim(strSearchText) + strEndLike + " AND RSCard = 1";
						}
						else if (cmbAccount.Text == "Utility Account")
						{
							// UT
							strWhere = " StreetName LIKE " + strBeginLike + Strings.Trim(strSearchText) + strEndLike;
						}
					}
					if (cmbSearchBy.SelectedIndex == 0 && cmbAccount.Text == "Mortgage Holder Number")
					{
						strSQL += " WHERE " + strWhere;
					}
					else if (cmbAccount.Text == "Real Estate Account")
					{
						strSQL += " WHERE " + strWhere + " ORDER BY RSAccount";
					}
					else if (cmbAccount.Text == "Utility Account")
					{
						// UT
						strSQL += " WHERE " + strWhere + " ORDER BY AccountNumber";
					}
					// The string is built now send it to the search form and show it
					//! Load frmMortgageSearch;
					if (cmbAccount.Text == "Mortgage Holder Number")
					{
						frmMortgageSearch.InstancePtr.Init(ref strSQL, 1, boolJoined, boolMatches, strDBName);
					}
					else if (cmbAccount.Text == "Real Estate Account")
					{
						frmMortgageSearch.InstancePtr.Init(ref strSQL, 2, boolJoined, boolMatches, strDBName);
					}
					else
					{
						// UT
						frmMortgageSearch.InstancePtr.Init(ref strSQL, 3, boolJoined, boolMatches, strDBName);
					}
					if (boolMatches)
					{
						// do nothing
					}
					else
					{
						App.DoEvents();
						if (txtSearch.Enabled && txtSearch.Visible)
						{
							txtSearch.Focus();
						}
						else
						{
							frmGetMortgage.InstancePtr.Show(App.MainForm);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Search");
			}
		}

		private void txtAccount_Enter(object sender, System.EventArgs e)
		{
			if (txtAccount.Text.Length > 0)
			{
				txtAccount.SelectionStart = 0;
				txtAccount.SelectionLength = txtAccount.Text.Length;
			}
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 91 || KeyAscii == 93)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuGetAccount_Click(sender, e);
		}

		private void cmdNewMortgageHolder_Click(object sender, EventArgs e)
		{
			this.mnuNewMortgageHolder_Click(sender, e);
		}
	}
}
