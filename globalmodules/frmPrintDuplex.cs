﻿using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	/// <summary>
	/// Summary description for frmPrintDuplex.
	/// </summary>
	public partial class frmPrintDuplex : BaseForm
	{
		public frmPrintDuplex()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintDuplex InstancePtr
		{
			get
			{
				return (frmPrintDuplex)Sys.GetInstance(typeof(frmPrintDuplex));
			}
		}

		protected frmPrintDuplex _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strDuplex;

		private void frmPrintDuplex_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintDuplex.Icon	= "frmPrintDuplex.frx":0000";
			//frmPrintDuplex.ScaleWidth	= 5880;
			//frmPrintDuplex.ScaleHeight	= 4065;
			//frmPrintDuplex.LinkTopic	= "Form1";
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmPrintDuplex.frx":058A";
			//End Unmaped Properties
			string strTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			strTemp = modRegistry.GetRegistryKey("PrintDuplex");
			if (Strings.UCase(Strings.Trim(strTemp)) == "NONE")
			{
				cmbDuplex.SelectedIndex = 0;
			}
			else if (Strings.UCase(Strings.Trim(strTemp)) == "FULL")
			{
				cmbDuplex.SelectedIndex = 1;
			}
			else if (Strings.UCase(Strings.Trim(strTemp)) == "MANUALTOPISLAST")
			{
				cmbDuplex.SelectedIndex = 2;
			}
			else if (Strings.UCase(Strings.Trim(strTemp)) == "MANUALTOPISFIRST")
			{
				cmbDuplex.SelectedIndex = 3;
			}
			else
			{
				cmbDuplex.SelectedIndex = 0;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			strDuplex = "";
			Close();
		}

		public string Init()
		{
			string Init = "";
			Init = "";
			this.Show(FormShowEnum.Modal);
			Init = strDuplex;
			return Init;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (cmbDuplex.SelectedIndex == 0)
			{
				strDuplex = "NONE";
			}
			else if (cmbDuplex.SelectedIndex == 1)
			{
				strDuplex = "FULL";
			}
			else if (cmbDuplex.SelectedIndex == 2)
			{
				strDuplex = "MANUALTOPISLAST";
			}
			else if (cmbDuplex.SelectedIndex == 3)
			{
				strDuplex = "MANUALTOPISFIRST";
			}
			// save choice as registry setting
			modRegistry.SaveRegistryKey("PrintDuplex", strDuplex);
			Close();
		}

		private void fcLabel1_Click(object sender, System.EventArgs e)
		{
		}

		private void cmbDuplex_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		}

		private void btnProcess_Click(object sender, System.EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}
	}
}
