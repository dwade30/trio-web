﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arLienPartialPayment : BaseSectionReport
	{
		string strMainText;
		DateTime dtPaymentDate;
		string strOwnerName = "";
		string strMuniName = "";
		string strState = "";
		int lngLineLen;
		string strSignerName = "";
		string strTitle = "";
		string strBillingYear = "";
		double dblPaymentAmount;

		public arLienPartialPayment()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Discharge Notice";
		}

		public static arLienPartialPayment InstancePtr
		{
			get
			{
				return (arLienPartialPayment)Sys.GetInstance(typeof(arLienPartialPayment));
			}
		}

		protected arLienPartialPayment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			lngLineLen = 35;
			// this is how many underscores are in the printed lines
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				lblTownHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
			}
			else
			{
				lblTownHeader.Text = strMuniName;
			}
			lblTitleBar.Text = "WAIVER FORM FOR USE WITH PARTIAL PAYMENTS";
			lblLegalDescription.Text = "30-DAY NOTICES & PROPERTY TAX LIENS";
			strMainText = "    I, " + strOwnerName + ", do hereby acknowledge that I have voluntarily made a partial payment upon my tax due the ";
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + ", " + strState + " for the year ";
			}
			else
			{
				strMainText += strMuniName + ", " + strState + " for the year ";
			}
			strMainText += strBillingYear + " in the sum of " + Strings.Format(dblPaymentAmount, "$#,##0.00");
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += " and I agree that sum is accepted by said " + modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
			}
			else
			{
				strMainText += " and I agree that sum is accepted by said " + strMuniName;
			}
			strMainText += " as partial payment as aforesaid without, in any way, waiving the lien of said ";
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + " for said tax.";
			}
			else
			{
				strMainText += strMuniName + " for said tax.";
			}
			fldMainText.Text = strMainText;
			fldDateLine.Text = "Dated at " + strMuniName + ", " + strState;
			fldDateLine.Text = fldDateLine.Text + "                 " + Strings.Format(dtPaymentDate, "MM/dd/yyyy");
			fldWitnessLine.Text = Strings.StrDup(lngLineLen, "_");
			fldTaxPayerLine.Text = Strings.StrDup(lngLineLen, "_");
			fldWitnessName.Text = strSignerName;
			fldTaxPayerName.Text = strOwnerName;
		}
		// VBto upgrade warning: dtPassPayDate As DateTime	OnWrite(DateTime, string)
		public void Init(string strPassMuni, string strPassState, double dblPassPaymentAmount, string strPassName1, string strPassBillingYear, DateTime dtPassPayDate, string strPassSignerName, string strPassMapLot = "", int lngPassAccount = 0)
		{
			// this routine will set all of the variables needed
			strMuniName = strPassMuni;
			strState = strPassState;
			strOwnerName = strPassName1;
			strBillingYear = strPassBillingYear;
			dblPaymentAmount = dblPassPaymentAmount;
			dtPaymentDate = dtPassPayDate;
			strSignerName = strPassSignerName;
			// map lot
			lblMapLot.Text = strPassMapLot;
			// account number
			if (lngPassAccount > 0)
			{
				fldAccount.Text = modGlobal.PadToString(FCConvert.ToInt16(lngPassAccount), 5);
			}
			else
			{
				fldAccount.Text = "";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SetStrings();
		}
	}
}
