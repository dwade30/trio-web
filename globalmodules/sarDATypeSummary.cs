﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarDATypeSummary : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/10/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/10/2003              *
		// ********************************************************
		float lngWidth;
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL = "";
		bool boolEndReport;
		double dblPrinSum;
		double dblIntSum;
		double dblPLISum;
		double dblCostSum;
		bool boolREType;
		bool boolWide;

		public sarDATypeSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarDATypeSummary_ReportEnd;
		}

        private void SarDATypeSummary_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static sarDATypeSummary InstancePtr
		{
			get
			{
				return (sarDATypeSummary)Sys.GetInstance(typeof(sarDATypeSummary));
			}
		}

		protected sarDATypeSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				eArgs.EOF = false;
				BindFields();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQLAddition = "";
			if (FCConvert.ToString(this.UserData) == "RE")
			{
				boolREType = true;
			}
			else
			{
				boolREType = false;
			}
			if (boolREType)
			{
				strSQLAddition = "AND BillCode <> 'P' ";
			}
			else
			{
				strSQLAddition = "AND BillCode = 'P' ";
			}
			// default the totals
			fldTotalPrin.Text = " 0.00";
			fldTotalInt.Text = " 0.00";
			fldTotalPLI.Text = " 0.00";
			fldTotalCosts.Text = " 0.00";
			fldTotalTotal.Text = " 0.00";
			// this will put the SQL string together and set the order
			if (modGlobalConstants.Statics.gboolCR)
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(CurrentInterest) as Interest, SUM(PreLienInterest) AS PLI, SUM(LienCost) AS Cost, Code FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND ReceiptNumber >= 0 GROUP BY Code ORDER BY Code";
			}
			else
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(CurrentInterest) as Interest, SUM(PreLienInterest) AS PLI, SUM(LienCost) AS Cost, Code FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' GROUP BY Code ORDER BY Code";
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			SetupFields();
			frmWait.InstancePtr.IncrementProgress();
			if (rsData.EndOfFile())
			{
				EndRoutine();
			}
		}

		private void SetupFields()
		{
			// this will adjust the fields to their correct
			boolWide = rptCLDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptCLDailyAuditMaster.InstancePtr.lngWidth;
			// set the full page objects
			this.PrintWidth = lngWidth;
			// Me.Detail.Width = lngWidth
			lblHeader.Width = lngWidth - 1 / 1440F;
			lnHeader.X2 = lngWidth - 1 / 1440F;
			// header labels/lines
			if (!boolWide)
			{
				lblType.Left = 0;
				lblDesc.Left = 1350 / 1440F;
				lblPrincipal.Left = 3230 / 1440F;
				lblPLI.Left = 4670 / 1440F;
				lblInterest.Left = 6110 / 1440F;
				lblCosts.Left = 7550 / 1440F;
				lblTotal.Left = 8990 / 1440F;
			}
			else
			{
				lblType.Left = 0;
				lblDesc.Left = 1350 / 1440F;
				lblPrincipal.Left = 5120 / 1440F;
				lblPLI.Left = 7100 / 1440F;
				lblInterest.Left = 9080 / 1440F;
				lblCosts.Left = 11060 / 1440F;
				lblTotal.Left = 13040 / 1440F;
			}
			// detail section
			fldType.Left = lblType.Left;
			fldDesc.Left = lblDesc.Left;
			fldPrincipal.Left = lblPrincipal.Left;
			fldPLI.Left = lblPLI.Left;
			fldInterest.Left = lblInterest.Left;
			fldCosts.Left = lblCosts.Left;
			fldTotal.Left = lblTotal.Left;
			// footer labels/fields
			lblTotals.Left = lblDesc.Left;
			fldTotalPrin.Left = lblPrincipal.Left;
			fldTotalPLI.Left = lblPLI.Left;
			fldTotalInt.Left = lblInterest.Left;
			fldTotalCosts.Left = lblCosts.Left;
			fldTotalTotal.Left = lblTotal.Left;
			lnTotals.X1 = lblPrincipal.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void BindFields()
		{
			// this will fill the fields with the correct data
			frmWait.InstancePtr.IncrementProgress();
			fldType.Text = rsData.Get_Fields_String("Code");
			fldDesc.Text = GetDesc_2(rsData.Get_Fields_String("Code"));
			fldPrincipal.Text = Strings.Format(rsData.Get_Fields_Double("Prin"), "#,##0.00");
			dblPrinSum += Conversion.Val(rsData.Get_Fields_Double("Prin"));
			// TODO: Field [PLI] not found!! (maybe it is an alias?)
			fldPLI.Text = Strings.Format(rsData.Get_Fields("PLI"), "#,##0.00");
			// TODO: Field [PLI] not found!! (maybe it is an alias?)
			dblPLISum += Conversion.Val(rsData.Get_Fields("PLI"));
			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
			fldInterest.Text = Strings.Format(rsData.Get_Fields("Interest"), "#,##0.00");
			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
			dblIntSum += Conversion.Val(rsData.Get_Fields("Interest"));
			// TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
			fldCosts.Text = Strings.Format(rsData.Get_Fields("Cost"), "#,##0.00");
			// TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
			dblCostSum += Conversion.Val(rsData.Get_Fields("Cost"));
			// MAL@20080527: Corrected to include PLI in the total
			// fldTotal.Text = Format(.Fields("Prin") + .Fields("Interest") + .Fields("Cost"), "#,##0.00")
			// TODO: Field [PLI] not found!! (maybe it is an alias?)
			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
			// TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
			fldTotal.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("Prin")) + Conversion.Val(rsData.Get_Fields("PLI")) + Conversion.Val(rsData.Get_Fields("Interest")) + Conversion.Val(rsData.Get_Fields("Cost")), "#,##0.00");
			// move to the next record
			rsData.MoveNext();
		}

		private void EndRoutine()
		{
			// this will show a message that there are no records to process and hide the rest of the report
			lnHeader.Visible = false;
			lblHeader.Visible = false;
			lnTotals.Visible = false;
			lblPrincipal.Visible = false;
			lblPLI.Visible = false;
			lblInterest.Visible = false;
			lblCosts.Visible = false;
			lblTotal.Visible = false;
			fldTotalPrin.Visible = false;
			fldTotalInt.Visible = false;
			fldTotalCosts.Visible = false;
			fldTotalTotal.Visible = false;
			lblTotals.Visible = false;
			// lblTotals.Width = lngWidth - lblTotals.Left
			// lblTotals.Text = "There are no records to process."
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalPrin.Text = Strings.Format(dblPrinSum, "#,##0.00");
			fldTotalPLI.Text = Strings.Format(dblPLISum, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblIntSum, "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblCostSum, "#,##0.00");
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPrin.Text) + FCConvert.ToDouble(fldTotalPLI.Text) + FCConvert.ToDouble(fldTotalInt.Text) + FCConvert.ToDouble(fldTotalCosts.Text), "#,##0.00");
		}

		private string GetDesc_2(string strCode)
		{
			return GetDesc(ref strCode);
		}

		private string GetDesc(ref string strCode)
		{
			string GetDesc = "";
			if (Strings.UCase(strCode) == "3")
			{
				GetDesc = "Demand Fees";
			}
			else if (Strings.UCase(strCode) == "A")
			{
				GetDesc = "Abatement";
			}
			else if (Strings.UCase(strCode) == "C")
			{
				GetDesc = "Correction";
			}
			else if (Strings.UCase(strCode) == "D")
			{
				GetDesc = "Discount";
			}
			else if (Strings.UCase(strCode) == "L")
			{
				GetDesc = "Lien Costs";
			}
			else if (Strings.UCase(strCode) == "P")
			{
				GetDesc = "Payment";
			}
			else if (Strings.UCase(strCode) == "R")
			{
				GetDesc = "Refunded Abatement";
			}
			else if (Strings.UCase(strCode) == "S")
			{
				GetDesc = "Supplemental";
			}
			else if (Strings.UCase(strCode) == "U")
			{
				GetDesc = "Tax Club";
			}
			else if (Strings.UCase(strCode) == "Y")
			{
				GetDesc = "Pre Payment";
			}
			else
			{
				GetDesc = "";
			}
			return GetDesc;
		}

		private void sarDATypeSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarDATypeSummary.Caption	= "Daily Audit Detail";
			//sarDATypeSummary.Icon	= "sarDATypeSummary.dsx":0000";
			//sarDATypeSummary.Left	= 0;
			//sarDATypeSummary.Top	= 0;
			//sarDATypeSummary.Width	= 11880;
			//sarDATypeSummary.Height	= 8175;
			//sarDATypeSummary.StartUpPosition	= 3;
			//sarDATypeSummary.SectionData	= "sarDATypeSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
