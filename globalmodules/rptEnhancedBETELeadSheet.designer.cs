﻿namespace Global
{
	/// <summary>
	/// Summary description for rptEnhancedBETELeadSheet.
	/// </summary>
	partial class rptEnhancedBETELeadSheet
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEnhancedBETELeadSheet));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field162 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field164 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field165 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field166 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field167 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field168 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field169 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field170 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field171 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field172 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field173 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field174 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field175 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field176 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field177 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field178 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field179 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field180 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field181 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field182 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field183 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field184 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field185 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field186 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field187 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field188 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field189 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field190 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field191 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field192 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field193 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field194 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field195 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field196 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field197 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field198 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field199 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field200 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field201 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field202 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field203 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt1a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt1b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt1c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt1d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2e = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2f = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2g = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt3a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt3b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt3c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt4a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRetentionPercent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field161 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field204 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field162)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field163)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field164)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field165)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field166)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field167)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field168)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field169)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field170)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field171)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field172)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field173)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field174)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field178)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field179)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field180)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field181)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field182)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field183)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field184)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field185)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field186)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field187)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field188)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field189)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field190)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field191)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field192)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field193)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field194)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field195)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field196)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field197)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field198)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field199)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field200)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field201)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field202)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field203)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2e)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2f)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2g)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRetentionPercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field161)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field204)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field6,
				this.Field162,
				this.txtLine1,
				this.lblYear1,
				this.Field163,
				this.Field164,
				this.Field165,
				this.Field166,
				this.Field167,
				this.Field168,
				this.Field169,
				this.Field170,
				this.Line3,
				this.Field171,
				this.Field172,
				this.Field173,
				this.Field174,
				this.Field175,
				this.Field176,
				this.Field177,
				this.Field178,
				this.Field179,
				this.Field180,
				this.Field181,
				this.Field182,
				this.Field183,
				this.Field184,
				this.Field185,
				this.Field186,
				this.Field187,
				this.Field188,
				this.Field189,
				this.Line4,
				this.Field190,
				this.Field191,
				this.Field192,
				this.Field193,
				this.Field194,
				this.Field195,
				this.Field196,
				this.Field197,
				this.Field198,
				this.Field199,
				this.Line5,
				this.Field200,
				this.Field201,
				this.Field202,
				this.Field203,
				this.txt1a,
				this.txt1b,
				this.txt1c,
				this.txt1d,
				this.txt2a,
				this.txt2b,
				this.txt2c,
				this.txt2d,
				this.txt2e,
				this.txt2f,
				this.txt2g,
				this.txt3a,
				this.txt3b,
				this.txt3c,
				this.txt4a,
				this.txtRetentionPercent
			});
			this.Detail.Height = 7.447917F;
			this.Detail.Name = "Detail";
			// 
			// Field6
			// 
			this.Field6.Height = 0.25F;
			this.Field6.Left = 0.75F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Field6.Text = "Standard BETE reimbursement computation";
			this.Field6.Top = 0F;
			this.Field6.Width = 3.75F;
			// 
			// Field162
			// 
			this.Field162.Height = 0.25F;
			this.Field162.Left = 0F;
			this.Field162.Name = "Field162";
			this.Field162.Style = "font-size: 10pt; font-weight: bold";
			this.Field162.Text = "1.";
			this.Field162.Top = 0F;
			this.Field162.Width = 0.25F;
			// 
			// txtLine1
			// 
			this.txtLine1.Height = 0.1875F;
			this.txtLine1.Left = 0.4375F;
			this.txtLine1.Name = "txtLine1";
			this.txtLine1.Style = "font-size: 9pt; vertical-align: middle";
			this.txtLine1.Text = "Total valuation of ALL BETE qualified exempt property as of April 1,";
			this.txtLine1.Top = 0.4375F;
			this.txtLine1.Width = 4.3125F;
			// 
			// lblYear1
			// 
			this.lblYear1.Height = 0.1875F;
			this.lblYear1.HyperLink = null;
			this.lblYear1.Left = 4.3125F;
			this.lblYear1.Name = "lblYear1";
			this.lblYear1.Style = "font-family: \'Arial\'; font-size: 9pt; vertical-align: middle";
			this.lblYear1.Text = "2015";
			this.lblYear1.Top = 0.4375F;
			this.lblYear1.Width = 0.4375F;
			// 
			// Field163
			// 
			this.Field163.Height = 0.1875F;
			this.Field163.Left = 0.4375F;
			this.Field163.Name = "Field163";
			this.Field163.Style = "font-size: 9pt; vertical-align: middle";
			this.Field163.Text = "Total valuation of all BETE qualified exempt property not located in a Municipal";
			this.Field163.Top = 0.6875F;
			this.Field163.Width = 5.625F;
			// 
			// Field164
			// 
			this.Field164.Height = 0.1875F;
			this.Field164.Left = 0.4375F;
			this.Field164.Name = "Field164";
			this.Field164.Style = "font-size: 9pt; vertical-align: middle";
			this.Field164.Text = "Retention Tax Increment Financing District          (Line 1a. minus line 3b.)";
			this.Field164.Top = 0.875F;
			this.Field164.Width = 5.125F;
			// 
			// Field165
			// 
			this.Field165.Height = 0.1875F;
			this.Field165.Left = 0.1875F;
			this.Field165.Name = "Field165";
			this.Field165.Style = "vertical-align: middle";
			this.Field165.Text = "(a)";
			this.Field165.Top = 0.4375F;
			this.Field165.Width = 0.3125F;
			// 
			// Field166
			// 
			this.Field166.Height = 0.1875F;
			this.Field166.Left = 0.1875F;
			this.Field166.Name = "Field166";
			this.Field166.Style = "vertical-align: middle";
			this.Field166.Text = "(b)";
			this.Field166.Top = 0.6875F;
			this.Field166.Width = 0.3125F;
			// 
			// Field167
			// 
			this.Field167.Height = 0.1875F;
			this.Field167.Left = 0.4375F;
			this.Field167.Name = "Field167";
			this.Field167.Style = "font-size: 9pt; vertical-align: middle";
			this.Field167.Text = "Percent of reimbursement for BETE exempt property (2016 statutory standard 50% re" + "imbursement)";
			this.Field167.Top = 1.125F;
			this.Field167.Width = 5.625F;
			// 
			// Field168
			// 
			this.Field168.Height = 0.1875F;
			this.Field168.Left = 0.1875F;
			this.Field168.Name = "Field168";
			this.Field168.Style = "vertical-align: middle";
			this.Field168.Text = "(c)";
			this.Field168.Top = 1.125F;
			this.Field168.Width = 0.3125F;
			// 
			// Field169
			// 
			this.Field169.Height = 0.1875F;
			this.Field169.Left = 0.5F;
			this.Field169.Name = "Field169";
			this.Field169.Style = "font-size: 9pt; vertical-align: middle";
			this.Field169.Text = "Valuation of all BETE qualified exempt property subject to standard reimbursement" + "";
			this.Field169.Top = 1.375F;
			this.Field169.Width = 5.625F;
			// 
			// Field170
			// 
			this.Field170.Height = 0.1875F;
			this.Field170.Left = 0.1875F;
			this.Field170.Name = "Field170";
			this.Field170.Style = "vertical-align: middle";
			this.Field170.Text = "(d)";
			this.Field170.Top = 1.375F;
			this.Field170.Width = 0.3125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.75F;
			this.Line3.Width = 7.25F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.25F;
			this.Line3.Y1 = 1.75F;
			this.Line3.Y2 = 1.75F;
			// 
			// Field171
			// 
			this.Field171.Height = 0.25F;
			this.Field171.Left = 0.4375F;
			this.Field171.Name = "Field171";
			this.Field171.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Field171.Text = "Enhanced Reimbursement if personal property factor exceeds 5% of total taxable va" + "lue";
			this.Field171.Top = 2F;
			this.Field171.Width = 6.3125F;
			// 
			// Field172
			// 
			this.Field172.Height = 0.25F;
			this.Field172.Left = 0F;
			this.Field172.Name = "Field172";
			this.Field172.Style = "font-size: 10pt; font-weight: bold";
			this.Field172.Text = "2.";
			this.Field172.Top = 2F;
			this.Field172.Width = 0.25F;
			// 
			// Field173
			// 
			this.Field173.Height = 0.1875F;
			this.Field173.Left = 0.625F;
			this.Field173.Name = "Field173";
			this.Field173.Style = "font-size: 9pt; vertical-align: middle";
			this.Field173.Text = "Total value of all business personal property";
			this.Field173.Top = 2.375F;
			this.Field173.Width = 5.625F;
			// 
			// Field174
			// 
			this.Field174.Height = 0.1875F;
			this.Field174.Left = 0.3125F;
			this.Field174.Name = "Field174";
			this.Field174.Style = "vertical-align: middle";
			this.Field174.Text = "(a)";
			this.Field174.Top = 2.375F;
			this.Field174.Width = 0.3125F;
			// 
			// Field175
			// 
			this.Field175.Height = 0.1875F;
			this.Field175.Left = 0.625F;
			this.Field175.Name = "Field175";
			this.Field175.Style = "font-size: 9pt; vertical-align: middle";
			this.Field175.Text = "(include all taxable and all exempt BETE qualified business personal property)";
			this.Field175.Top = 2.625F;
			this.Field175.Width = 5.625F;
			// 
			// Field176
			// 
			this.Field176.Height = 0.1875F;
			this.Field176.Left = 0.625F;
			this.Field176.Name = "Field176";
			this.Field176.Style = "font-size: 9pt; vertical-align: middle";
			this.Field176.Text = "Total value of all taxable real and personal property";
			this.Field176.Top = 2.875F;
			this.Field176.Width = 5.625F;
			// 
			// Field177
			// 
			this.Field177.Height = 0.1875F;
			this.Field177.Left = 0.3125F;
			this.Field177.Name = "Field177";
			this.Field177.Style = "vertical-align: middle";
			this.Field177.Text = "(b)";
			this.Field177.Top = 2.875F;
			this.Field177.Width = 0.3125F;
			// 
			// Field178
			// 
			this.Field178.Height = 0.1875F;
			this.Field178.Left = 0.625F;
			this.Field178.Name = "Field178";
			this.Field178.Style = "font-size: 9pt; vertical-align: middle";
			this.Field178.Text = "Total valuation of all BETE qualified exempt property subject to Enhanced reimbur" + "sement if not";
			this.Field178.Top = 3.125F;
			this.Field178.Width = 5.625F;
			// 
			// Field179
			// 
			this.Field179.Height = 0.1875F;
			this.Field179.Left = 0.3125F;
			this.Field179.Name = "Field179";
			this.Field179.Style = "vertical-align: middle";
			this.Field179.Text = "(c)";
			this.Field179.Top = 3.125F;
			this.Field179.Width = 0.3125F;
			// 
			// Field180
			// 
			this.Field180.Height = 0.1875F;
			this.Field180.Left = 0.625F;
			this.Field180.Name = "Field180";
			this.Field180.Style = "font-size: 9pt; vertical-align: middle";
			this.Field180.Text = "located in a Municipal Retention TIF District subject ot a > % of line 2.(f)";
			this.Field180.Top = 3.375F;
			this.Field180.Width = 5.625F;
			// 
			// Field181
			// 
			this.Field181.Height = 0.1875F;
			this.Field181.Left = 5.1875F;
			this.Field181.Name = "Field181";
			this.Field181.Style = "font-size: 8.5pt; vertical-align: middle";
			this.Field181.Text = "Line 1.(a) minus line 3.(c) if 2.(f) > 3.(a)";
			this.Field181.Top = 3.375F;
			this.Field181.Width = 2.25F;
			// 
			// Field182
			// 
			this.Field182.Height = 0.1875F;
			this.Field182.Left = 0.625F;
			this.Field182.Name = "Field182";
			this.Field182.Style = "font-size: 9pt; vertical-align: middle";
			this.Field182.Text = "Personal Property Factor [2a. / (2b. + 1a.)]";
			this.Field182.Top = 3.625F;
			this.Field182.Width = 5.625F;
			// 
			// Field183
			// 
			this.Field183.Height = 0.1875F;
			this.Field183.Left = 0.3125F;
			this.Field183.Name = "Field183";
			this.Field183.Style = "vertical-align: middle";
			this.Field183.Text = "(d)";
			this.Field183.Top = 3.625F;
			this.Field183.Width = 0.3125F;
			// 
			// Field184
			// 
			this.Field184.Height = 0.1875F;
			this.Field184.Left = 0.625F;
			this.Field184.Name = "Field184";
			this.Field184.Style = "font-size: 9pt; vertical-align: middle";
			this.Field184.Text = "Line 2d. / 2";
			this.Field184.Top = 3.8125F;
			this.Field184.Width = 5.625F;
			// 
			// Field185
			// 
			this.Field185.Height = 0.1875F;
			this.Field185.Left = 0.3125F;
			this.Field185.Name = "Field185";
			this.Field185.Style = "vertical-align: middle";
			this.Field185.Text = "(e)";
			this.Field185.Top = 3.8125F;
			this.Field185.Width = 0.3125F;
			// 
			// Field186
			// 
			this.Field186.Height = 0.1875F;
			this.Field186.Left = 0.625F;
			this.Field186.Name = "Field186";
			this.Field186.Style = "font-size: 9pt; vertical-align: middle";
			this.Field186.Text = "Line 2(e). plus 50%  (If line 2(d) is greater than 5%)";
			this.Field186.Top = 4F;
			this.Field186.Width = 5.625F;
			// 
			// Field187
			// 
			this.Field187.Height = 0.1875F;
			this.Field187.Left = 0.3125F;
			this.Field187.Name = "Field187";
			this.Field187.Style = "vertical-align: middle";
			this.Field187.Text = "(f)";
			this.Field187.Top = 4F;
			this.Field187.Width = 0.3125F;
			// 
			// Field188
			// 
			this.Field188.Height = 0.1875F;
			this.Field188.Left = 0.625F;
			this.Field188.Name = "Field188";
			this.Field188.Style = "font-size: 9pt; vertical-align: middle";
			this.Field188.Text = "Valuation of all BETE qualified exempt property subject to Enhanced reimbursement" + "";
			this.Field188.Top = 4.1875F;
			this.Field188.Width = 5.625F;
			// 
			// Field189
			// 
			this.Field189.Height = 0.1875F;
			this.Field189.Left = 0.3125F;
			this.Field189.Name = "Field189";
			this.Field189.Style = "vertical-align: middle";
			this.Field189.Text = "(g)";
			this.Field189.Top = 4.1875F;
			this.Field189.Width = 0.3125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 4.5625F;
			this.Line4.Width = 7.25F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.25F;
			this.Line4.Y1 = 4.5625F;
			this.Line4.Y2 = 4.5625F;
			// 
			// Field190
			// 
			this.Field190.Height = 0.25F;
			this.Field190.Left = 0.4375F;
			this.Field190.Name = "Field190";
			this.Field190.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Field190.Text = "Municipal Retention Tax Increment Percentage";
			this.Field190.Top = 4.8125F;
			this.Field190.Width = 6.3125F;
			// 
			// Field191
			// 
			this.Field191.Height = 0.25F;
			this.Field191.Left = 0F;
			this.Field191.Name = "Field191";
			this.Field191.Style = "font-size: 10pt; font-weight: bold";
			this.Field191.Text = "3.";
			this.Field191.Top = 4.8125F;
			this.Field191.Width = 0.25F;
			// 
			// Field192
			// 
			this.Field192.Height = 0.1875F;
			this.Field192.Left = 0.625F;
			this.Field192.Name = "Field192";
			this.Field192.Style = "font-size: 9pt; vertical-align: middle";
			this.Field192.Text = "Percentage of captured assessed value retained and allocated to the municipality";
			this.Field192.Top = 5.125F;
			this.Field192.Width = 4.6875F;
			// 
			// Field193
			// 
			this.Field193.Height = 0.1875F;
			this.Field193.Left = 0.3125F;
			this.Field193.Name = "Field193";
			this.Field193.Style = "vertical-align: middle";
			this.Field193.Text = "(a)";
			this.Field193.Top = 5.125F;
			this.Field193.Width = 0.3125F;
			// 
			// Field194
			// 
			this.Field194.Height = 0.1875F;
			this.Field194.Left = 0.625F;
			this.Field194.Name = "Field194";
			this.Field194.Style = "font-size: 9pt; vertical-align: middle";
			this.Field194.Text = "for the municipality\'s own authorized TIF project costs approved as of 4/1/2008.";
			this.Field194.Top = 5.375F;
			this.Field194.Width = 5.625F;
			// 
			// Field195
			// 
			this.Field195.Height = 0.1875F;
			this.Field195.Left = 0.625F;
			this.Field195.Name = "Field195";
			this.Field195.Style = "font-size: 8.5pt; vertical-align: middle";
			this.Field195.Text = "(Defaults to Statutory Standard unless Municipal Retention % is greater than stan" + "dard reimbursement)";
			this.Field195.Top = 5.625F;
			this.Field195.Width = 5.625F;
			// 
			// Field196
			// 
			this.Field196.Height = 0.1875F;
			this.Field196.Left = 0.625F;
			this.Field196.Name = "Field196";
			this.Field196.Style = "font-size: 9pt; vertical-align: middle";
			this.Field196.Text = "Captured Assessed Value of BETE qualified property located within a Municipal Ret" + "ention TIF district";
			this.Field196.Top = 5.875F;
			this.Field196.Width = 5.6875F;
			// 
			// Field197
			// 
			this.Field197.Height = 0.1875F;
			this.Field197.Left = 0.3125F;
			this.Field197.Name = "Field197";
			this.Field197.Style = "vertical-align: middle";
			this.Field197.Text = "(b)";
			this.Field197.Top = 5.875F;
			this.Field197.Width = 0.3125F;
			// 
			// Field198
			// 
			this.Field198.Height = 0.1875F;
			this.Field198.Left = 0.625F;
			this.Field198.Name = "Field198";
			this.Field198.Style = "font-size: 9pt; vertical-align: middle";
			this.Field198.Text = "Valuation of all TIF BETE qualified exempt property subject to reimbursement";
			this.Field198.Top = 6.125F;
			this.Field198.Width = 5.625F;
			// 
			// Field199
			// 
			this.Field199.Height = 0.1875F;
			this.Field199.Left = 0.3125F;
			this.Field199.Name = "Field199";
			this.Field199.Style = "vertical-align: middle";
			this.Field199.Text = "(c)";
			this.Field199.Top = 6.125F;
			this.Field199.Width = 0.3125F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 3F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 6.5F;
			this.Line5.Width = 7.25F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.25F;
			this.Line5.Y1 = 6.5F;
			this.Line5.Y2 = 6.5F;
			// 
			// Field200
			// 
			this.Field200.Height = 0.25F;
			this.Field200.Left = 0.4375F;
			this.Field200.Name = "Field200";
			this.Field200.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Field200.Text = "Total Reimbursable BETE Exempt Valuation";
			this.Field200.Top = 6.75F;
			this.Field200.Width = 6.3125F;
			// 
			// Field201
			// 
			this.Field201.Height = 0.25F;
			this.Field201.Left = 0F;
			this.Field201.Name = "Field201";
			this.Field201.Style = "font-size: 10pt; font-weight: bold";
			this.Field201.Text = "4.";
			this.Field201.Top = 6.75F;
			this.Field201.Width = 0.25F;
			// 
			// Field202
			// 
			this.Field202.Height = 0.1875F;
			this.Field202.Left = 0.625F;
			this.Field202.Name = "Field202";
			this.Field202.Style = "font-size: 9pt; vertical-align: middle";
			this.Field202.Text = "Total of all reimbursable BETE Exempt Valuation        1.(d) or 2.(g) + 3.(c)";
			this.Field202.Top = 7.0625F;
			this.Field202.Width = 5.625F;
			// 
			// Field203
			// 
			this.Field203.Height = 0.1875F;
			this.Field203.Left = 0.3125F;
			this.Field203.Name = "Field203";
			this.Field203.Style = "vertical-align: middle";
			this.Field203.Text = "(a)";
			this.Field203.Top = 7.0625F;
			this.Field203.Width = 0.3125F;
			// 
			// txt1a
			// 
			this.txt1a.Height = 0.1875F;
			this.txt1a.Left = 6.25F;
			this.txt1a.Name = "txt1a";
			this.txt1a.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt1a.Text = null;
			this.txt1a.Top = 0.4375F;
			this.txt1a.Width = 1.1875F;
			// 
			// txt1b
			// 
			this.txt1b.Height = 0.1875F;
			this.txt1b.Left = 6.25F;
			this.txt1b.Name = "txt1b";
			this.txt1b.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt1b.Text = null;
			this.txt1b.Top = 0.6875F;
			this.txt1b.Width = 1.1875F;
			// 
			// txt1c
			// 
			this.txt1c.Height = 0.1875F;
			this.txt1c.Left = 6.25F;
			this.txt1c.Name = "txt1c";
			this.txt1c.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt1c.Text = null;
			this.txt1c.Top = 1.125F;
			this.txt1c.Width = 1.1875F;
			// 
			// txt1d
			// 
			this.txt1d.Height = 0.1875F;
			this.txt1d.Left = 6.25F;
			this.txt1d.Name = "txt1d";
			this.txt1d.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt1d.Text = null;
			this.txt1d.Top = 1.375F;
			this.txt1d.Width = 1.1875F;
			// 
			// txt2a
			// 
			this.txt2a.Height = 0.1875F;
			this.txt2a.Left = 6.25F;
			this.txt2a.Name = "txt2a";
			this.txt2a.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt2a.Text = null;
			this.txt2a.Top = 2.375F;
			this.txt2a.Width = 1.1875F;
			// 
			// txt2b
			// 
			this.txt2b.Height = 0.1875F;
			this.txt2b.Left = 6.25F;
			this.txt2b.Name = "txt2b";
			this.txt2b.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt2b.Text = null;
			this.txt2b.Top = 2.875F;
			this.txt2b.Width = 1.1875F;
			// 
			// txt2c
			// 
			this.txt2c.Height = 0.1875F;
			this.txt2c.Left = 6.25F;
			this.txt2c.Name = "txt2c";
			this.txt2c.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt2c.Text = null;
			this.txt2c.Top = 3.125F;
			this.txt2c.Width = 1.1875F;
			// 
			// txt2d
			// 
			this.txt2d.Height = 0.1875F;
			this.txt2d.Left = 6.25F;
			this.txt2d.Name = "txt2d";
			this.txt2d.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt2d.Text = null;
			this.txt2d.Top = 3.625F;
			this.txt2d.Width = 1.1875F;
			// 
			// txt2e
			// 
			this.txt2e.Height = 0.1875F;
			this.txt2e.Left = 6.25F;
			this.txt2e.Name = "txt2e";
			this.txt2e.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt2e.Text = null;
			this.txt2e.Top = 3.8125F;
			this.txt2e.Width = 1.1875F;
			// 
			// txt2f
			// 
			this.txt2f.Height = 0.1875F;
			this.txt2f.Left = 6.25F;
			this.txt2f.Name = "txt2f";
			this.txt2f.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt2f.Text = null;
			this.txt2f.Top = 4F;
			this.txt2f.Width = 1.1875F;
			// 
			// txt2g
			// 
			this.txt2g.Height = 0.1875F;
			this.txt2g.Left = 6.25F;
			this.txt2g.Name = "txt2g";
			this.txt2g.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt2g.Text = null;
			this.txt2g.Top = 4.1875F;
			this.txt2g.Width = 1.1875F;
			// 
			// txt3a
			// 
			this.txt3a.Height = 0.1875F;
			this.txt3a.Left = 6.25F;
			this.txt3a.Name = "txt3a";
			this.txt3a.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt3a.Text = null;
			this.txt3a.Top = 5.125F;
			this.txt3a.Width = 1.1875F;
			// 
			// txt3b
			// 
			this.txt3b.Height = 0.1875F;
			this.txt3b.Left = 6.25F;
			this.txt3b.Name = "txt3b";
			this.txt3b.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt3b.Text = null;
			this.txt3b.Top = 5.875F;
			this.txt3b.Width = 1.1875F;
			// 
			// txt3c
			// 
			this.txt3c.Height = 0.1875F;
			this.txt3c.Left = 6.25F;
			this.txt3c.Name = "txt3c";
			this.txt3c.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt3c.Text = null;
			this.txt3c.Top = 6.125F;
			this.txt3c.Width = 1.1875F;
			// 
			// txt4a
			// 
			this.txt4a.Height = 0.1875F;
			this.txt4a.Left = 6.25F;
			this.txt4a.Name = "txt4a";
			this.txt4a.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txt4a.Text = null;
			this.txt4a.Top = 7.0625F;
			this.txt4a.Width = 1.1875F;
			// 
			// txtRetentionPercent
			// 
			this.txtRetentionPercent.Height = 0.1875F;
			this.txtRetentionPercent.Left = 5.5F;
			this.txtRetentionPercent.Name = "txtRetentionPercent";
			this.txtRetentionPercent.Style = "font-family: \'Arial\'; font-size: 11.5pt; text-align: right; vertical-align: middl" + "e";
			this.txtRetentionPercent.Text = null;
			this.txtRetentionPercent.Top = 5.125F;
			this.txtRetentionPercent.Width = 0.625F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.Field126,
				this.txtMunicipality,
				this.Field161,
				this.Field204
			});
			this.ReportHeader.Height = 0.9270833F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1875F;
			this.txtTitle.Left = 1F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Arial\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "ENHANCED BETE MUNICIPAL TAX RATE CALCULATION FORM";
			this.txtTitle.Top = 0.03125F;
			this.txtTitle.Width = 5.4375F;
			// 
			// Field126
			// 
			this.Field126.Height = 0.1875F;
			this.Field126.Left = 0.0625F;
			this.Field126.Name = "Field126";
			this.Field126.Style = "font-size: 9pt; font-weight: bold; text-align: center; text-decoration: underline" + "";
			this.Field126.Text = "BE SURE TO COMPLETE & FILE THIS FORM IN CONJUCTION WITH ENHANCED BETE TAX RATE CA" + "LCULATION FORM";
			this.Field126.Top = 0.6875F;
			this.Field126.Width = 7.375F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 2.125F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.5F;
			this.txtMunicipality.Width = 3.625F;
			// 
			// Field161
			// 
			this.Field161.Height = 0.1875F;
			this.Field161.Left = 0.75F;
			this.Field161.Name = "Field161";
			this.Field161.Style = "font-style: italic; text-align: right";
			this.Field161.Text = "Municipality:";
			this.Field161.Top = 0.5F;
			this.Field161.Width = 1.3125F;
			// 
			// Field204
			// 
			this.Field204.Height = 0.1875F;
			this.Field204.Left = 1F;
			this.Field204.Name = "Field204";
			this.Field204.Style = "font-family: \'Arial\'; font-size: 11.5pt; font-weight: bold; text-align: center";
			this.Field204.Text = "ENHANCED BUSINESS EQUIPMENT TAX EXEMPTION CALC SHEET";
			this.Field204.Top = 0.25F;
			this.Field204.Width = 5.4375F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// rptEnhancedBETELeadSheet
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field162)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field163)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field164)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field165)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field166)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field167)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field168)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field169)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field170)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field171)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field172)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field173)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field174)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field178)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field179)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field180)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field181)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field182)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field183)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field184)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field185)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field186)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field187)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field188)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field189)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field190)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field191)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field192)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field193)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field194)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field195)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field196)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field197)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field198)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field199)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field200)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field201)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field202)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field203)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2e)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2f)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2g)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt3c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt4a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRetentionPercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field161)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field204)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field162;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field163;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field164;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field165;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field166;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field167;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field168;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field169;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field170;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field171;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field172;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field173;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field174;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field175;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field176;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field177;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field178;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field179;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field180;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field181;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field182;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field183;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field184;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field185;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field186;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field187;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field188;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field189;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field190;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field191;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field192;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field193;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field194;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field195;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field196;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field197;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field198;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field199;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field200;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field201;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field202;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field203;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt1a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt1b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt1c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt1d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2e;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2f;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2g;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt3c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt4a;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRetentionPercent;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field126;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field161;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field204;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
