﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cALFRange
	{
		//=========================================================
		private string strAssetLowRange = string.Empty;
		private string strAssetHighRange = string.Empty;
		private string strLiabilityLowRange = string.Empty;
		private string strLiabilityHighRange = string.Empty;
		private string strFundBalanceLowRange = string.Empty;
		private string strFundBalanceHighRange = string.Empty;

		public string AssetLowRange
		{
			set
			{
				strAssetLowRange = value;
			}
			get
			{
				string AssetLowRange = "";
				AssetLowRange = strAssetLowRange;
				return AssetLowRange;
			}
		}

		public string AssetHighRange
		{
			set
			{
				strAssetHighRange = value;
			}
			get
			{
				string AssetHighRange = "";
				AssetHighRange = strAssetHighRange;
				return AssetHighRange;
			}
		}

		public string LiabilityLowRange
		{
			set
			{
				strLiabilityLowRange = value;
			}
			get
			{
				string LiabilityLowRange = "";
				LiabilityLowRange = strLiabilityLowRange;
				return LiabilityLowRange;
			}
		}

		public string LiabilityHighRange
		{
			set
			{
				strLiabilityHighRange = value;
			}
			get
			{
				string LiabilityHighRange = "";
				LiabilityHighRange = strLiabilityHighRange;
				return LiabilityHighRange;
			}
		}

		public string FundBalanceLowRange
		{
			set
			{
				strFundBalanceLowRange = value;
			}
			get
			{
				string FundBalanceLowRange = "";
				FundBalanceLowRange = strFundBalanceLowRange;
				return FundBalanceLowRange;
			}
		}

		public string FundBalanceHighRange
		{
			set
			{
				strFundBalanceHighRange = value;
			}
			get
			{
				string FundBalanceHighRange = "";
				FundBalanceHighRange = strFundBalanceHighRange;
				return FundBalanceHighRange;
			}
		}

		public string GetALFByAccountSegment(string strAccount)
		{
			string GetALFByAccountSegment = "";
			string strReturn;
			strReturn = "1";
			if (fecherFoundation.Strings.CompareString(strAccount, FundBalanceLowRange, true) >= 0 && fecherFoundation.Strings.CompareString(strAccount, FundBalanceHighRange, true) <= 0)
			{
				strReturn = "3";
			}
			else if (fecherFoundation.Strings.CompareString(strAccount, LiabilityLowRange, true) >= 0 && fecherFoundation.Strings.CompareString(strAccount, LiabilityHighRange, true) <= 0)
			{
				strReturn = "2";
			}
			GetALFByAccountSegment = strReturn;
			return GetALFByAccountSegment;
		}
	}
}
