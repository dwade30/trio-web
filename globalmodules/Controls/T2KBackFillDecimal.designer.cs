﻿namespace Global
{
	partial class T2KBackFillDecimal
	{
		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent2 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent3 = new Wisej.Web.JavaScript.ClientEvent();
			this.javaScript = new Wisej.Web.JavaScript(this.components);
			clientEvent1.Event = "keypress";
			clientEvent1.JavaScript = "T2KBackFillDecimal_KeyDown(this, e);";
			clientEvent2.Event = "tap";
			clientEvent2.JavaScript = "T2KBackFillDecimal_Tap(this, e);";
			clientEvent3.Event = "keyup";
            clientEvent3.JavaScript = "T2KBackFillDecimal_KeyUp(this, e);";
			this.javaScript.GetJavaScriptEvents(this).Add(clientEvent1);
			this.javaScript.GetJavaScriptEvents(this).Add(clientEvent2);
			this.javaScript.GetJavaScriptEvents(this).Add(clientEvent3);
		}
		#endregion

		private Wisej.Web.JavaScript javaScript;
	}
}
