﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace Global
{
	public partial class T2KPhoneNumberBox : MaskedTextBox, ISupportInitialize
	{
		public T2KPhoneNumberBox()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			Mask = "(999)000-0000";
		}

        public bool Locked
        {
            get
            {
                return base.ReadOnly;
            }
            set
            {
                base.ReadOnly = value;
            }
        }

        public void BeginInit()
		{
		}

		public void EndInit()
		{
		}
	}
}
