﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace Global
{
	public partial class T2KBackFillDecimal : TextBox, ISupportInitialize
	{
		public T2KBackFillDecimal()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.TextAlign = HorizontalAlignment.Right;
			//FC:FINAL:DSE Fire Validate event on Enter
			this.KeyDown += T2KBackFillDecimal_KeyDown;
            //this.Validating += T2KBackFillDecimal_Validating;
            this.Eval(@"this.getContentElement().addListener('paste', TextBox_OnPasteWithDot)");
        }
		//FC:FINAL:AM:#i576 - format the number
		//private void T2KBackFillDecimal_Validating(object sender, CancelEventArgs e)
		//{
		//	decimal value = 0;
		//	if (this.Text.Trim() == string.Empty || Decimal.TryParse(this.Text, out value))
		//	{
		//		this.Text = value.ToString("N2");
		//	}
		//	else
		//	{
		//		MessageBox.Show("Invalid Number");
		//		e.Cancel = true;
		//	}
		//}

		private void T2KBackFillDecimal_KeyDown(object sender, KeyEventArgs e)
		{
			//FC:FINAL:DSE Fire Validating events on ENTER key
			if (e.KeyCode == Keys.Enter)
			{
				CancelEventArgs cancelEvent = new CancelEventArgs();
				this.OnValidating(cancelEvent);
				if (cancelEvent.Cancel)
				{
					return;
				}
				Form frm = this.FindForm();
				if (frm != null)
				{
					Control ctrl = frm.GetNextControl(this, true);
					if (ctrl != null)
					{
						ctrl.Focus();
					}
				}
			}
		}

        //FC:FINAL:AM:#2202 - set default text on enter
        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            if(String.IsNullOrEmpty(this.Text))
            {
                this.Text = "0.00";
            }
        }

        public bool Locked
        {
            get
            {
                return base.ReadOnly;
            }
            set
            {
                base.ReadOnly = value;
            }
        }

        public void BeginInit()
		{
		}

		public void EndInit()
		{
		}
    }
}
