﻿namespace Global
{
	partial class T2KBackFillWhole
	{
		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
			this.javaScript = new Wisej.Web.JavaScript(this.components);
			clientEvent1.Event = "keypress";
			clientEvent1.JavaScript = @"
if (e._keyCode == 8 || //BackSpace
    e._keyCode == 9 || //Tab
    e._keyCode == 13 || //Enter
    (e._keyCode >= 37 && e._keyCode <= 40 && e._identifier != ""'"" && e._identifier != ""&"" && e._identifier != ""("" && e._identifier != ""%"") || //arrow keys
    e._identifier == 'Delete' ||
    e._identifier == 'Insert' ||
    e._identifier == 'Home' ||
    e._identifier == 'End' ||
    (e._keyCode >= 48 && e._keyCode <= 57)) { //0 -> 9
    //allowkey
}
else {
    //block non numeric input
    e._preventDefault = true; 
}";
			this.javaScript.GetJavaScriptEvents(this).Add(clientEvent1);
		}
		#endregion

		private Wisej.Web.JavaScript javaScript;
	}
}
