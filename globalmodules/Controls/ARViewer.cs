﻿using fecherFoundation;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.SectionReportModel;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SharedApplication.Enums;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;
using Wisej.Web.Ext.AspNetControl;

namespace Global
{
    public partial class ARViewer : AspNetWrapper<GrapeCity.ActiveReports.Web.WebViewer>
    {
        private FCSectionReport reportSource = null;
        private string reportName = null;

        public ARViewer()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            Load += ARViewer_Load;
        }

        private void ARViewer_Load(object sender, EventArgs e)
        {
            //Need to run first the report otherwise the session is null during the callbacks
            //FC:FINAL:RPU:#102 Do this only if the ReportSource!=null
            if (ReportSource != null)
            {
                //FC:FINAL:AM:#i193 - check if the report has been already generated
                if (ReportSource.State != GrapeCity.ActiveReports.SectionReport.ReportState.Completed && 
                    ReportSource.State != SectionReport.ReportState.InProgress)
                {
                    ShowLoader = true;
                    //FC:FINAL:DSE:#1392 Set report visibility because there is a viewer displaying it
                    FCSectionReport rpt = ReportSource as FCSectionReport;
                    if (rpt != null)
                    {
                        rpt.Visible = true;
                    }
                    ReportSource.RunReport();
                    ShowLoader = false;
                }
                //this.WrappedControl.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.FlashViewer;
                //FC:FINAL:AM:#i307 - don't show the report if there are no pages
                if (ReportSource != null && reportSource.Document.Pages.Count > 0)
                {
                    WrappedControl.Report = ReportSource;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ReportName))
                {
                    WrappedControl.ReportName = ReportName;
                }
                else
                {
                    //do nothing
                }
            }
        }

        public GrapeCity.ActiveReports.Extensibility.Printing.Printer Printer
        {
            get
            {
                return ReportSource.Document.Printer;
            }
        }

        public FCSectionReport ReportSource
        {
            get
            {
                return reportSource;
            }
            set
            {
                reportSource = value;
            }
            //get
            //{
            //	return (GrapeCity.ActiveReports.SectionReport)this.WrappedControl.Report;
            //}
            //set
            //{
            //	this.WrappedControl.Report = value;
            //}
        }

        public string ReportName
        {
            get
            {
                return reportName;
            }
            set
            {
                reportName = value;
            }
        }



        //public void PrintReport(bool dummy = false)
        //{
        //	if (this.ReportSource != null && this.ReportSource.Document != null)
        //	{
        //		//FC:FINAL:JEI:IT629
        //		int intStartPage = this.Printer.PrinterSettings.FromPage;
        //		int intEndPage = this.Printer.PrinterSettings.ToPage;
        //		if (intEndPage != ReportSource.Document.Pages.Count || intStartPage != 1)
        //		{
        //			Printer.PrinterSettings.PrintRange = System.Drawing.Printing.PrintRange.SomePages;
        //		}
        //		else
        //		{
        //			Printer.PrinterSettings.PrintRange = System.Drawing.Printing.PrintRange.AllPages;
        //		}
        //		this.ReportSource.Document.Print(false);
        //	}
        //}
    }

    public static class Extensions
    {
        public static void PrintReportOnDotMatrix(this SectionReport report, string settingsPrinterName, bool PrintAsRTF = false, bool PrintAsTIFF = false, bool PrintAsRDF = true, DirectPrintingParams printerSettings = null)
        {
            //if (ClientPrintingSetup.InstancePtr.CheckConfiguration())
            //{
            if (report?.Document != null)
            {
                report.Run();

                if (PrintAsRTF)
                {
                    //EXPORT to RTF and send to TRIO Assistant
                    var rtfExport = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();

                    string text = string.Empty;


                    using (var stream = new MemoryStream())
                    {
                        rtfExport.Export(report.Document, stream);
                        text = StreamToText(stream);
                    }

                    var desiredPrinter = GetPrinterToUse(settingsPrinterName);
                    clsPrinterFunctions.PrintRTFTextToPrinter(desiredPrinter, text, report.PageSettings);

                }
                else
                {
                    if (PrintAsTIFF)
                    {
                        //EXPORT to TIFF and send to TRIO Assistant
                        var tiffExport = new GrapeCity.ActiveReports.Export.Image.Tiff.Section.TiffExport();
                        tiffExport.DpiX = 96;
                        tiffExport.DpiY = 96;
                        string tempFilePath = GetTempFileName(".tiff");

                        using (var stream = new MemoryStream())
                        {

                            tiffExport.Export(report.Document, stream);
                            StreamToFile(stream, tempFilePath);

                        }

                        var reportObject = new
                        {
                            file = tempFilePath,
                            name = "reportExport.tiff",
                            target = "_blank"
                        };
                        var downloadLink = CreateDownloadLink(reportObject);

                        var desiredPrinter = GetPrinterToUse(settingsPrinterName);
                        clsPrinterFunctions.PrintTIFFToPrinter(desiredPrinter, downloadLink, report.PageSettings);
                    }
                    else
                    {
                        if (PrintAsRDF)
                        {
                            string tempFilePath = GetTempFileName(".rdf");

                            using (var stream = new MemoryStream())
                            {

                                report.Document.Save(stream
                                                   , GrapeCity.ActiveReports.Document.Section.RdfFormat.AR20
                                                   , GrapeCity.ActiveReports.Document.Section.SaveOptions.Compressed);
                                StreamToFile(stream, tempFilePath);

                            }

                            var reportObject = new
                            {
                                file = tempFilePath,
                                name = "reportExport.rdf",
                                target = "_blank"
                            };

                            var downloadLink = CreateDownloadLink(reportObject);

                            var desiredPrinter = GetPrinterToUse(settingsPrinterName);

                            //report.Document.Print(false, false, true);

                            clsPrinterFunctions.PrintRDFToPrinter(desiredPrinter, downloadLink, report.PageSettings, printerSettings);
                        }
                    }
                }
            }

            // }
        }

        private static string StreamToText(MemoryStream stream)
        {
            string text;
            stream.Position = 0;

            using (StreamReader reader = new StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }

            return text;
        }

        private static void StreamToFile(MemoryStream stream, string tempFilePath)
        {
            stream.Position = 0;

            using (FileStream fileStream = new FileStream(tempFilePath, FileMode.Create))
            {
                stream.CopyTo(fileStream, 1024);
            }
        }

        private static string CreateDownloadLink(object reportObject)
        {
            string s = WisejSerializer.Serialize(reportObject, WisejSerializerOptions.CamelCase);
            s = Convert.ToBase64String(System.Text.Encoding.Unicode.GetBytes(s));
            string rdfDownloadLink = Application.StartupUrl;
            rdfDownloadLink = rdfDownloadLink.EndsWith("/") ? rdfDownloadLink : rdfDownloadLink + "/";
            rdfDownloadLink = rdfDownloadLink + "download.wx?x=" + s;

            return rdfDownloadLink;
        }

        private static string GetPrinterToUse(string printerCategory)
        {
	        var desiredPrinter = GetPrinterNameFromSettings(printerCategory);
            var clientPrinters = clsPrinterFunctions.GetClientPrinters();

            var selectedPrinter = GetUsablePrinter(clientPrinters, desiredPrinter);
            
            // at this point you either have a selectedPrinter, or you don't got no printers in your list, son.
            //while (selectedPrinter == null && clientPrinters != null)
            //{
            //    using (var selectPrinter = new FCSelectClientPrinter())
            //    {
            //        foreach (var clientPrinter in clientPrinters)
            //        {
            //            selectPrinter.AddPrinter(clientPrinter.Name);
            //        }

            //        selectPrinter.ShowDialog();
            //        desiredPrinter = selectPrinter.SelectedPrinter;
            //        selectedPrinter = clientPrinters.FirstOrDefault(p => desiredPrinter != null && p.Name == desiredPrinter);
            //    }
            //}

            // if there is no desired printer that matches anything, then  

            return selectedPrinter?.Name;
        }

        private static Printer GetUsablePrinter(List<Printer> clientPrinters, string desiredPrinter)
        {
            // if we don't have a printer that matches, then grab the first printer in the list
            return clientPrinters?.FirstOrDefault(p => desiredPrinter != null && p.Name == desiredPrinter) 
                   ?? clientPrinters?.FirstOrDefault();
        }

        private static string GetPrinterNameFromSettings(string settingsPrinterName)
        {
            return StaticSettings.GlobalSettingService.GetSettingValue(SettingOwnerType.Machine, settingsPrinterName)?.SettingValue ?? "";
        }

        public static void PrintReport(this SectionReport report, bool dummy = false, List<string> batchReports = null)
        {
            //FC:FINAL:AM:#2444 - run again the report
            //if (report != null && report.Document != null && report.Document.Pages.Count == 0)
            if (report != null && report.Document != null)
            {
                report.Run();
            }
            //FC:FINAL:SBE - #1047 - Print report on client side - export to PDF and insert print script
            report.ExportToPDFAndPrint(batchReports: batchReports);
        }

        public static void ExportToPDFAndPrint(this SectionReport report, List<string> batchReports = null)
        {
            //FC:FINAL:JTA - if report does not have any data, exported pdf will have the size 0, and the PdfReader will throw an exception 
            if (report.Document.Pages.Count == 0)
            {
                return;
            }
            string fileName, tempFilePath;
            ExportReportToPDF(report, out fileName, out tempFilePath);
            if (batchReports == null)
            {
                //if this is not part of batch report printing, print it directly
                AddPrintScriptToPDF(fileName, tempFilePath);
            }
            else
            {
                //if this is part of batch report printing, add the filename to list of batch reports to be merged
                batchReports.Add(tempFilePath);
            }
        }

        private static void ExportReportToPDF(SectionReport report, out string fileName, out string tempFilePath)
        {
            GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
            fileName = string.Empty;
            fileName = FCUtils.FixFileName(report.Name) + ".pdf";

            tempFilePath = GetTempFileName(".pdf");
            if (report.Document != null)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    using (FileStream fileStream = new FileStream(tempFilePath, FileMode.Create))
                    {
                        stream.CopyTo(fileStream, 1024);
                    }                    
                }
            }
            report.Dispose();
        }

        private static void AddPrintScriptToPDF(string fileName, string tempFilePath)
        {
            string tempFilePath1 = GetTempFileName(".pdf");
            PdfReader reader = new PdfReader(tempFilePath);
            FileStream pdfPrintStream = new FileStream(tempFilePath1, FileMode.Create);
            PdfStamper stamper = new PdfStamper(reader, pdfPrintStream);
            string jsText = "var res = app.setTimeOut('var pp = this.getPrintParams();pp.interactive = pp.constants.interactionLevel.full;this.print(pp);', 200);";
            stamper.JavaScript = jsText;
            stamper.Close();
            pdfPrintStream.Close();
            if (Application.Browser.Type == "Chrome")
            {
                //FC:FINAL:SBE - #2340 - when report is printed in Chrome, and there is also a file download, we can have the download skiped, because of processing multiple responses on the client side
                Application.StartTask(() =>
                {
                    var reportObject = new
                    {
                        file = tempFilePath1,
                        name = fileName,
                        target = "_blank"
                    };
                    string s = WisejSerializer.Serialize(reportObject, WisejSerializerOptions.CamelCase);
                    s = Convert.ToBase64String(System.Text.Encoding.Unicode.GetBytes(s));
                    string downloadURL = Application.StartupUrl;
                    downloadURL = downloadURL.EndsWith("/") ? downloadURL : downloadURL + "/";
                    downloadURL = downloadURL + "download.wx?x=" + s;
                    Application.EvalAsync("PrintGoogleChrome('" + downloadURL + "')");
                    FCUtils.ApplicationUpdate(App.MainForm);
                });
                //FC:FINAL:SBE - #2340 - when report is printed in Chrome, and there is also a file download, we can have the download skiped, because of processing multiple responses on the client side
                System.Threading.Thread.Sleep(3000);
            }
            else
            {
                FCUtils.DownloadAndOpen("_blank", tempFilePath1, fileName);
            }
            File.Delete(tempFilePath);
        }

        public static void ExportToPDF(this SectionReport report, string optionalPDFName = "")
        {
            if (report.Document.Pages.Count == 0)
            {
                return;
            }

            string fileName = optionalPDFName;
            if (string.IsNullOrWhiteSpace(fileName))
            {
                fileName = FCUtils.FixFileName(report.Name) + ".pdf";
            }
            GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
            string tempFilePath = GetTempFileName(".pdf");
            using (MemoryStream stream = new MemoryStream())
            {
                a.Export(report.Document, stream);
                stream.Position = 0;
                FCUtils.Download(stream, fileName);
                report.Dispose();
                report = null;

            }
        }

        public static void CombineAndPrintPDFs(List<string> batchReports)
        {
            if (batchReports != null && batchReports.Count > 0)
            {
                string tempFilePath = GetTempFileName(".pdf");
                CombineMultiplePDFs(batchReports, tempFilePath);
                AddPrintScriptToPDF("combined.pdf", tempFilePath);
            }
        }

        private static string GetTempFileName(string extension)
        {
            string applicationName = Path.GetFileName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
            string tempFolder = Path.Combine(Application.StartupPath, "Temp", "ReportViewer");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }
            Guid guid = Guid.NewGuid();
            string tempFileName = guid.ToString() + extension;
            string tempFilePath = Path.Combine(tempFolder, tempFileName);
            while (File.Exists(tempFilePath))
            {
                guid = Guid.NewGuid();
                tempFileName = guid.ToString() + extension;
                tempFilePath = Path.Combine(tempFolder, tempFileName);
            }
            return tempFilePath;
        }

        private static void CombineMultiplePDFs(List<string> inputPDFFilepaths, string outPDFFilePath)
        {
            iTextSharp.text.Document document = new iTextSharp.text.Document();
            using (FileStream outPDFFileStream = new FileStream(outPDFFilePath, FileMode.Create))
            {
                PdfCopy pdfWriter = new PdfCopy(document, outPDFFileStream);
                //exit method if file cannot be created
                if (pdfWriter == null)
                {
                    MessageBox.Show("Unable to create combined PDF file");
                    return;
                }

                document.Open();
                //read input pdf files
                foreach (string fileName in inputPDFFilepaths)
                {
                    PdfReader pdfReader = new PdfReader(fileName);
                    pdfReader.ConsolidateNamedDestinations();
                    //read pdf pages
                    for (int i = 1; i <= pdfReader.NumberOfPages; i++)
                    {
                        PdfImportedPage page = pdfWriter.GetImportedPage(pdfReader, i);
                        pdfWriter.AddPage(page);
                    }
                    //read pdf form if it exists
                    PRAcroForm form = pdfReader.AcroForm;
                    if (form != null)
                    {
                        pdfWriter.CopyAcroForm(pdfReader);
                    }

                    pdfReader.Close();
                }

                pdfWriter.Close();
                document.Close();
            }
        }

        public static void Close(this SectionReport report)
        {
            report.Stop();
        }

        public static void Hide(this SectionReport report)
        {
        }

        public static void Unload(this SectionReport report)
        {
            report.Dispose();
        }

        public static List<ARControl> GetAllControls(this SectionReport report)
        {
            List<ARControl> controls = new List<ARControl>();
            foreach (Section section in report.Sections)
            {
                foreach (ARControl control in section.Controls)
                {
                    controls.Add(control);
                }
            }
            return controls;
        }
    }

    public class DirectPrintingParams
    {
        public int FromPage { get; set; }
        public int ToPage { get; set; }
    }
}
