﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	public partial class T2KOverTypeBox : FCTextBox, ISupportInitialize
	{
		public T2KOverTypeBox()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            var setSelectionStartEvent = new JavaScript.ClientEvent();
            setSelectionStartEvent.Event = "focusin";
            setSelectionStartEvent.JavaScript = "this.setTextSelection(0,1);";
            this.ClientEvents.Add(setSelectionStartEvent);
            //FC:FINAL:DSE #i540 Fire Validating events on ENTER key
            this.KeyDown += T2KOverTypeBox_KeyDown;
		}

		private void T2KOverTypeBox_KeyDown(object sender, KeyEventArgs e)
		{
			//FC:FINAL:DSE #i540 Fire Validating events on ENTER key
			if (e.KeyCode == Keys.Enter)
			{
				CancelEventArgs cancelEvent = new CancelEventArgs();
				this.OnValidating(cancelEvent);
				if (cancelEvent.Cancel)
				{
					return;
				}
                //FC:FINAL:SBE - #3727 - this is not needed for #i540, and introduces additional side effect
				//Form frm = this.FindForm();
				//if (frm != null)
				//{
				//	Control ctrl = frm.GetNextControl(this, true);
				//	if (ctrl != null)
				//	{
				//		ctrl.Focus();
				//	}
				//}
			}
		}

        public bool Locked
        {
            get
            {
                return base.ReadOnly;
            }
            set
            {
                base.ReadOnly = value;
            }
        }

        public void BeginInit()
		{
		}

		public void EndInit()
		{
		}
	}
}
