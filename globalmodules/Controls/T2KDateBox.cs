﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	public partial class T2KDateBox : MaskedTextBox, ISupportInitialize
	{
		#region Members
		private ToolTip toolTip = new ToolTip();
		#endregion

		#region Constructor
		public T2KDateBox()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

        private void InitializeComponentEx()
        {   
            var setSelectionStartEvent = new JavaScript.ClientEvent();
            setSelectionStartEvent.Event = "focusin";
            setSelectionStartEvent.JavaScript = "this.setTextSelection(0,0);";
            this.ClientEvents.Add(setSelectionStartEvent);

            this.Validating += T2KDateBox_Validating;
            this.Enter += T2KDateBox_Enter;
            this.KeyUp += T2KDateBox_KeyUp;

            this.Eval(@"this.getContentElement().addListener('paste', TextBox_OnPaste)");
        }

        private void T2KDateBox_KeyUp(object sender, KeyEventArgs e)
        {
            //FC:FINAL:SBE - Harris #3718 - added just to make the event critical
        }

        //HARRIS:DDU:#2204 - added code from check text to T2KDateBox_Enter, when user enters on control
        private void T2KDateBox_Enter(object sender, EventArgs e)
		{
			if (this.UnmaskText(base.Text) == "")
			{
				base.Text = "00/00/0000";
			}
        }

		private void T2KDateBox_Validating(object sender, CancelEventArgs e)
		{
			DateTime value;
			if (Information.IsDate(this.Text) || this.Text == "00/00/0000" || this.UnmaskText(this.Text) == "")
			{
				return;
			}
			if (!DateTime.TryParse(this.Text, out value) && this.UnmaskText(this.Text) != "")
			{
				MessageBox.Show("This is not a valid date.", "Invalid Date");
				if (!this.Modified)
				{
					return;
				}
				else
				{
					this.Focus();
					this.SelStart = 0;
				}
				e.Cancel = true;
				this.Modified = false;
			}
		}
		#endregion

		#region Properties

		//DDU:#i2330 - fixed globally problem with getting all controls throu a foreach iteration & added DataChanged in T2KDateBox, FCCheckBox & FCRadioButton in order to discover if changes was made to the controls data
		/// <summary>
		/// Returns/sets a value indicating that data in a control has changed by some process other than by retrieving data from the current record.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool DataChanged
		{
			get
			{
				return this.Modified;
			}
			set
			{
				this.Modified = value;
			}
		}

		//DDU get the text as data even it is nothing - to work as in original
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}

        public bool IsEmpty
        {
            get
            {
                return this.Text == "00/00/0000" || this.UnmaskText(this.Text) == "";
            }
        }

		public bool Locked
		{
			get
            {
                return base.ReadOnly;
            }
            set
            {
                base.ReadOnly = value;
            }
		}

		public int SelLength
		{
			get
            {
                return base.SelectionLength;
            }
			set
            {
                base.SelectionLength = value;
            }
		}

		public int SelStart
		{
			get
            {
                return base.SelectionStart;
            }
			set
            {
                if (value > -1)
                {
                    base.SelectionStart = value;
                }
            }
		}
		/// <summary>
		/// Returns or sets a ToolTip.
		/// Syntax
		/// object.ToolTipText [= string]
		/// The ToolTipText property syntax has these parts:
		/// Part	Description
		/// object	An object expression that evaluates to an object in the Applies To list.
		/// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
		/// Remarks
		/// If you use only an image to label an object, you can use this property to explain each object with a few words.
		/// At design time you can set the ToolTipText property string in the control's properties dialog box.
		/// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
		/// </summary>
		[DefaultValue("")]
		public string ToolTipText
		{
			get
			{
				return toolTip.GetToolTip(this);
			}
			set
			{
				toolTip.SetToolTip(this, value);
			}
		}

		public void BeginInit()
		{
		}

		public void EndInit()
		{
		}
		#endregion

		protected override void OnValidating(CancelEventArgs e)
		{
			//FC:FINAL:DSE:#i1963 Validating event should not come if form is closed from the close button
			if (this.FindForm()?.CloseReason != CloseReason.UserClosing)
			{
				base.OnValidating(e);
				if (!e.Cancel)
				{
					this.Modified = false;
                }
			}
		}
	}
}
