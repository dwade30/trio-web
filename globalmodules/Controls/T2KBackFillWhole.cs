﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace Global
{
	public partial class T2KBackFillWhole : TextBox, ISupportInitialize
	{
		public T2KBackFillWhole()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            this.Eval(@"this.getContentElement().addListener('paste', TextBox_OnPaste)");
        }

        public bool Locked
        {
            get
            {
                return base.ReadOnly;
            }
            set
            {
                base.ReadOnly = value;
            }
        }

        public void BeginInit()
		{
		}

		public void EndInit()
		{
		}
	}
}
