﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmBookPageReport.
	/// </summary>
	partial class frmBookPageReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbBPType;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtAccount;
		public fecherFoundation.FCTextBox txtAccount_1;
		public fecherFoundation.FCTextBox txtAccount_0;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblAccount;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBookPageReport));
			this.cmbBPType = new fecherFoundation.FCComboBox();
			this.txtAccount_1 = new fecherFoundation.FCTextBox();
			this.txtAccount_0 = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.lblBPType = new fecherFoundation.FCLabel();
			this.groupBox1 = new Wisej.Web.GroupBox();
			this.btnProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 386);
			this.BottomPanel.Size = new System.Drawing.Size(359, 13);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnProcess);
			this.ClientArea.Controls.Add(this.groupBox1);
			this.ClientArea.Size = new System.Drawing.Size(359, 326);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(359, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(213, 30);
			this.HeaderText.Text = "Book Page Report";
			// 
			// cmbBPType
			// 
			this.cmbBPType.AutoSize = false;
			this.cmbBPType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBPType.FormattingEnabled = true;
			this.cmbBPType.Items.AddRange(new object[] {
				"All Accounts",
				"Eligible for Lien"
			});
			this.cmbBPType.Location = new System.Drawing.Point(125, 152);
			this.cmbBPType.Name = "cmbBPType";
			this.cmbBPType.Size = new System.Drawing.Size(155, 40);
			this.cmbBPType.TabIndex = 5;
			this.cmbBPType.Text = "All Accounts";
			// 
			// txtAccount_1
			// 
			this.txtAccount_1.AutoSize = false;
			this.txtAccount_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount_1.Location = new System.Drawing.Point(125, 93);
			this.txtAccount_1.Name = "txtAccount_1";
			this.txtAccount_1.Size = new System.Drawing.Size(155, 40);
			this.txtAccount_1.TabIndex = 3;
			this.txtAccount_1.Enter += new System.EventHandler(this.txtAccount_Enter);
			// 
			// txtAccount_0
			// 
			this.txtAccount_0.AutoSize = false;
			this.txtAccount_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount_0.Location = new System.Drawing.Point(125, 32);
			this.txtAccount_0.Name = "txtAccount_0";
			this.txtAccount_0.Size = new System.Drawing.Size(155, 40);
			this.txtAccount_0.TabIndex = 1;
			this.txtAccount_0.Enter += new System.EventHandler(this.txtAccount_Enter);
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(20, 45);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(48, 16);
			this.Label2.TabIndex = 0;
			this.Label2.Text = "START";
			// 
			// lblAccount
			// 
			this.lblAccount.AutoSize = true;
			this.lblAccount.Location = new System.Drawing.Point(20, 105);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(34, 16);
			this.lblAccount.TabIndex = 2;
			this.lblAccount.Text = "END";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// lblBPType
			// 
			this.lblBPType.AutoSize = true;
			this.lblBPType.Location = new System.Drawing.Point(20, 165);
			this.lblBPType.Name = "lblBPType";
			this.lblBPType.Size = new System.Drawing.Size(40, 16);
			this.lblBPType.TabIndex = 4;
			this.lblBPType.Text = "TYPE";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.Label2);
			this.groupBox1.Controls.Add(this.cmbBPType);
			this.groupBox1.Controls.Add(this.lblBPType);
			this.groupBox1.Controls.Add(this.lblAccount);
			this.groupBox1.Controls.Add(this.txtAccount_0);
			this.groupBox1.Controls.Add(this.txtAccount_1);
			this.groupBox1.Location = new System.Drawing.Point(30, 25);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(299, 210);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.Text = "Enter the account range";
			this.groupBox1.UseMnemonic = false;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 261);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(84, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmBookPageReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(359, 399);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmBookPageReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Book Page Report";
			this.Load += new System.EventHandler(this.frmBookPageReport_Load);
			this.Activated += new System.EventHandler(this.frmBookPageReport_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBookPageReport_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBookPageReport_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCLabel lblBPType;
		private GroupBox groupBox1;
		private FCButton btnProcess;
	}
}
