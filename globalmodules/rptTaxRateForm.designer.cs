﻿namespace Global
{
	/// <summary>
	/// Summary description for rptTaxRateForm.
	/// </summary>
	partial class rptTaxRateForm
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxRateForm));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCountyTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMunicipalAppropriation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTIF = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSchool = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAppropriations = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRealEstate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPersonalProperty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalBase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRevenueSharing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherRevenue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalDeductions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNetToBeRaised = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt19Plus21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNet1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBase2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMaxTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field66 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field69 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBase1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field81 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNet2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field84 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMaxTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field86 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field88 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNet3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field90 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHomesteadVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxRate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNet4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field95 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field96 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field97 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field98 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field99 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMinRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field104 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxCommitment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMaxOverlay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field108 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverlay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field123 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field128 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHalfHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field130 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field131 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field132 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field133 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBETEValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field135 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl5btext = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field137 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field138 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field139 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field140 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBETEVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxRate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field143 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field144 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field146 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field147 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field148 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field149 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field150 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field151 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field152 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field153 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field154 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field155 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field156 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field157 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field158 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field159 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field160 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field162 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field163 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field164 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBETEReimburseValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field166 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field167 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field168 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field169 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field170 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field171 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field172 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field173 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field161 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountyTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalAppropriation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTIF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSchool)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAppropriations)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRealEstate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPersonalProperty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRevenueSharing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherRevenue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetToBeRaised)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt19Plus21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomesteadVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMinRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxCommitment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxOverlay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverlay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl5btext)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field138)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field139)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field140)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field143)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field144)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field146)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field147)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field148)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field149)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field150)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field151)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field152)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field153)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field154)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field155)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field156)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field157)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field158)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field159)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field160)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field162)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field163)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field164)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEReimburseValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field166)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field167)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field168)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field169)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field170)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field171)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field172)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field173)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field161)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtLine1,
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field6,
            this.Field7,
            this.Field8,
            this.Field9,
            this.Field10,
            this.Field11,
            this.Field12,
            this.Field13,
            this.Field14,
            this.Field15,
            this.Field16,
            this.Field17,
            this.Field18,
            this.Field19,
            this.Field20,
            this.Field21,
            this.Field22,
            this.Field23,
            this.Field24,
            this.Field25,
            this.Field26,
            this.Field27,
            this.Field28,
            this.Field29,
            this.Field30,
            this.Field31,
            this.Field32,
            this.Field33,
            this.txtCountyTax,
            this.txtMunicipalAppropriation,
            this.txtTIF,
            this.txtSchool,
            this.txtTotalAppropriations,
            this.txtRealEstate,
            this.txtPersonalProperty,
            this.txtTotalHomestead,
            this.txtTotalTaxable,
            this.txtTotalBase,
            this.txtRevenueSharing,
            this.Field45,
            this.Field46,
            this.txtOtherRevenue,
            this.Field48,
            this.txtTotalDeductions,
            this.Field50,
            this.txtNetToBeRaised,
            this.Field52,
            this.Field53,
            this.Field54,
            this.txt19Plus21,
            this.Field56,
            this.Field57,
            this.txtNet1,
            this.Field59,
            this.Field60,
            this.txtBase2,
            this.Field62,
            this.Field63,
            this.txtMaxTax,
            this.Field66,
            this.Field67,
            this.Field68,
            this.Field69,
            this.Field70,
            this.Field71,
            this.Field72,
            this.Field73,
            this.txtBase1,
            this.Field75,
            this.Field76,
            this.Field77,
            this.Field78,
            this.Field79,
            this.Field80,
            this.Field81,
            this.Field82,
            this.txtNet2,
            this.Field84,
            this.txtMaxTax2,
            this.Field86,
            this.txtTotTaxable,
            this.Field88,
            this.txtNet3,
            this.Field90,
            this.txtHomesteadVal,
            this.txtTaxRate,
            this.txtTaxRate2,
            this.txtNet4,
            this.Field95,
            this.Field96,
            this.Field97,
            this.Field98,
            this.Field99,
            this.Field100,
            this.Field101,
            this.txtMinRate,
            this.txtMaxRate,
            this.Field104,
            this.txtTaxCommitment,
            this.Field106,
            this.txtMaxOverlay,
            this.Field108,
            this.txtHomestead,
            this.Field110,
            this.txtOverlay,
            this.Field111,
            this.Field112,
            this.Field114,
            this.Field117,
            this.Label1,
            this.Field118,
            this.Field119,
            this.Field120,
            this.Field121,
            this.Field123,
            this.Field124,
            this.Field127,
            this.Field128,
            this.txtHalfHomestead,
            this.Field130,
            this.Field131,
            this.Field132,
            this.Field133,
            this.txtBETEValuation,
            this.Field135,
            this.lbl5btext,
            this.Label2,
            this.Label3,
            this.Field137,
            this.Field138,
            this.Field139,
            this.Field140,
            this.txtBETEVal,
            this.txtTaxRate3,
            this.Field143,
            this.Field144,
            this.txtBETE,
            this.Field146,
            this.Field147,
            this.Field148,
            this.Field149,
            this.Field150,
            this.Field151,
            this.Field152,
            this.Field153,
            this.Field154,
            this.Field155,
            this.Field156,
            this.Field157,
            this.Field158,
            this.Field159,
            this.Field160,
            this.Field162,
            this.Field163,
            this.Field164,
            this.txtBETEReimburseValue,
            this.Field166,
            this.Field167,
            this.Field168,
            this.Field169,
            this.Field170,
            this.Field171,
            this.Field172,
            this.Field173,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11});
			this.Detail.Height = 9.1275F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtLine1
			// 
			this.txtLine1.Height = 0.1875F;
			this.txtLine1.Left = 0.0625F;
			this.txtLine1.Name = "txtLine1";
			this.txtLine1.Style = "font-size: 9pt";
			this.txtLine1.Text = "1.  Total taxable valuation of real estate";
			this.txtLine1.Top = 0.03125F;
			this.txtLine1.Width = 3.9375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 9pt";
			this.Field1.Text = "2.  Total taxable valuation of personal property";
			this.Field1.Top = 0.3125F;
			this.Field1.Width = 3.9375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 0.0625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-size: 9pt";
			this.Field2.Text = "3.  Total taxable valuation of real estate and personal property (Line 1 plus lin" +
    "e 2)";
			this.Field2.Top = 0.59375F;
			this.Field2.Width = 5.625F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.0625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-size: 9pt";
			this.Field3.Text = "4.  (a) Total exempt value for all homestead exemptions granted";
			this.Field3.Top = 0.875F;
			this.Field3.Width = 3.6875F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 0.07291666F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-size: 9pt";
			this.Field4.Text = "6.  Total valuation base (Line 3 plus line 4(b) plus line 5(b))";
			this.Field4.Top = 2.270833F;
			this.Field4.Width = 5.6875F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.25F;
			this.Field6.Left = 0.25F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Field6.Text = "Assessments";
			this.Field6.Top = 2.520833F;
			this.Field6.Width = 1.8125F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 0.0625F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-size: 9pt";
			this.Field7.Text = "7.  County tax";
			this.Field7.Top = 2.71875F;
			this.Field7.Width = 4.0625F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 0.0625F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-size: 9pt";
			this.Field8.Text = "8.  Municipal appropriation";
			this.Field8.Top = 2.9375F;
			this.Field8.Width = 4.0625F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 0.0625F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-size: 9pt";
			this.Field9.Text = "9.  TIF Financing plan amount";
			this.Field9.Top = 3.15625F;
			this.Field9.Width = 4.0625F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 0F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-size: 9pt";
			this.Field10.Text = "10.  Local education appropriation";
			this.Field10.Top = 3.375F;
			this.Field10.Width = 2.322917F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 0F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-size: 9pt";
			this.Field11.Text = "11.  Total assessments (Add lines 7 through 10)";
			this.Field11.Top = 3.71875F;
			this.Field11.Width = 5.875F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.25F;
			this.Field12.Left = 0.3125F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Field12.Text = "ALLOWABLE DEDUCTIONS";
			this.Field12.Top = 3.96875F;
			this.Field12.Width = 2.75F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 0F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-size: 9pt";
			this.Field13.Text = "12.  Anticipated state municipal revenue sharing";
			this.Field13.Top = 4.1875F;
			this.Field13.Width = 4.125F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 0F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-size: 9pt";
			this.Field14.Text = "13.  Other revenues:";
			this.Field14.Top = 4.40625F;
			this.Field14.Width = 1.375F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 0F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-size: 9pt";
			this.Field15.Text = "14.  Total deductions (Line 12 plus line 13)";
			this.Field15.Top = 4.875F;
			this.Field15.Width = 5.875F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 0F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-size: 9pt";
			this.Field16.Text = "15. Net to be raised by local property tax rate (Line 11 minus line 14)";
			this.Field16.Top = 5.1875F;
			this.Field16.Width = 5.875F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.1875F;
			this.Field17.Left = 0F;
			this.Field17.Name = "Field17";
			this.Field17.Text = "16.";
			this.Field17.Top = 5.5F;
			this.Field17.Width = 0.25F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.1875F;
			this.Field18.Left = 0F;
			this.Field18.Name = "Field18";
			this.Field18.Text = "17.";
			this.Field18.Top = 5.8125F;
			this.Field18.Width = 0.25F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1875F;
			this.Field19.Left = 0F;
			this.Field19.Name = "Field19";
			this.Field19.Text = "18.";
			this.Field19.Top = 6.125F;
			this.Field19.Width = 0.25F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 0F;
			this.Field20.Name = "Field20";
			this.Field20.Text = "19.";
			this.Field20.Top = 6.4375F;
			this.Field20.Width = 0.25F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.1875F;
			this.Field21.Left = 0F;
			this.Field21.Name = "Field21";
			this.Field21.Text = "20.";
			this.Field21.Top = 6.75F;
			this.Field21.Width = 0.25F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.1875F;
			this.Field22.Left = 0F;
			this.Field22.Name = "Field22";
			this.Field22.Text = "21.";
			this.Field22.Top = 7.0625F;
			this.Field22.Width = 0.25F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1875F;
			this.Field23.Left = 0F;
			this.Field23.Name = "Field23";
			this.Field23.Text = "23.";
			this.Field23.Top = 7.75F;
			this.Field23.Width = 0.25F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1875F;
			this.Field24.Left = 4.25F;
			this.Field24.Name = "Field24";
			this.Field24.Text = "$";
			this.Field24.Top = 0.03125F;
			this.Field24.Visible = false;
			this.Field24.Width = 0.1875F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1875F;
			this.Field25.Left = 4.25F;
			this.Field25.Name = "Field25";
			this.Field25.Text = "$";
			this.Field25.Top = 0.3125F;
			this.Field25.Visible = false;
			this.Field25.Width = 0.1875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 6F;
			this.Field26.Name = "Field26";
			this.Field26.Text = "$";
			this.Field26.Top = 0.59375F;
			this.Field26.Visible = false;
			this.Field26.Width = 0.1875F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.1875F;
			this.Field27.Left = 4.25F;
			this.Field27.Name = "Field27";
			this.Field27.Text = "$";
			this.Field27.Top = 0.875F;
			this.Field27.Visible = false;
			this.Field27.Width = 0.1875F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.1875F;
			this.Field28.Left = 6.010417F;
			this.Field28.Name = "Field28";
			this.Field28.Text = "$";
			this.Field28.Top = 2.270833F;
			this.Field28.Visible = false;
			this.Field28.Width = 0.1875F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 4.25F;
			this.Field29.Name = "Field29";
			this.Field29.Text = "$";
			this.Field29.Top = 2.71875F;
			this.Field29.Visible = false;
			this.Field29.Width = 0.1875F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.1875F;
			this.Field30.Left = 4.25F;
			this.Field30.Name = "Field30";
			this.Field30.Text = "$";
			this.Field30.Top = 2.9375F;
			this.Field30.Visible = false;
			this.Field30.Width = 0.1875F;
			// 
			// Field31
			// 
			this.Field31.Height = 0.1875F;
			this.Field31.Left = 4.25F;
			this.Field31.Name = "Field31";
			this.Field31.Text = "$";
			this.Field31.Top = 3.15625F;
			this.Field31.Visible = false;
			this.Field31.Width = 0.1875F;
			// 
			// Field32
			// 
			this.Field32.Height = 0.1875F;
			this.Field32.Left = 4.25F;
			this.Field32.Name = "Field32";
			this.Field32.Text = "$";
			this.Field32.Top = 3.375F;
			this.Field32.Visible = false;
			this.Field32.Width = 0.1875F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.1875F;
			this.Field33.Left = 6F;
			this.Field33.Name = "Field33";
			this.Field33.Text = "$";
			this.Field33.Top = 3.71875F;
			this.Field33.Visible = false;
			this.Field33.Width = 0.1875F;
			// 
			// txtCountyTax
			// 
			this.txtCountyTax.Height = 0.1875F;
			this.txtCountyTax.Left = 4.4375F;
			this.txtCountyTax.Name = "txtCountyTax";
			this.txtCountyTax.Style = "text-align: right";
			this.txtCountyTax.Text = "Field34";
			this.txtCountyTax.Top = 2.71875F;
			this.txtCountyTax.Width = 1.375F;
			// 
			// txtMunicipalAppropriation
			// 
			this.txtMunicipalAppropriation.Height = 0.1875F;
			this.txtMunicipalAppropriation.Left = 4.4375F;
			this.txtMunicipalAppropriation.Name = "txtMunicipalAppropriation";
			this.txtMunicipalAppropriation.Style = "text-align: right";
			this.txtMunicipalAppropriation.Text = "Field35";
			this.txtMunicipalAppropriation.Top = 2.9375F;
			this.txtMunicipalAppropriation.Width = 1.375F;
			// 
			// txtTIF
			// 
			this.txtTIF.Height = 0.1875F;
			this.txtTIF.Left = 4.4375F;
			this.txtTIF.Name = "txtTIF";
			this.txtTIF.Style = "text-align: right";
			this.txtTIF.Text = "Field36";
			this.txtTIF.Top = 3.15625F;
			this.txtTIF.Width = 1.375F;
			// 
			// txtSchool
			// 
			this.txtSchool.Height = 0.1875F;
			this.txtSchool.Left = 4.4375F;
			this.txtSchool.Name = "txtSchool";
			this.txtSchool.Style = "text-align: right";
			this.txtSchool.Text = "Field37";
			this.txtSchool.Top = 3.375F;
			this.txtSchool.Width = 1.375F;
			// 
			// txtTotalAppropriations
			// 
			this.txtTotalAppropriations.Height = 0.1875F;
			this.txtTotalAppropriations.Left = 6.1875F;
			this.txtTotalAppropriations.Name = "txtTotalAppropriations";
			this.txtTotalAppropriations.Style = "text-align: right";
			this.txtTotalAppropriations.Text = "Field38";
			this.txtTotalAppropriations.Top = 3.71875F;
			this.txtTotalAppropriations.Width = 1.25F;
			// 
			// txtRealEstate
			// 
			this.txtRealEstate.Height = 0.1875F;
			this.txtRealEstate.Left = 4.4375F;
			this.txtRealEstate.Name = "txtRealEstate";
			this.txtRealEstate.Style = "text-align: right";
			this.txtRealEstate.Text = "Field39";
			this.txtRealEstate.Top = 0.03125F;
			this.txtRealEstate.Width = 1.375F;
			// 
			// txtPersonalProperty
			// 
			this.txtPersonalProperty.Height = 0.1875F;
			this.txtPersonalProperty.Left = 4.4375F;
			this.txtPersonalProperty.Name = "txtPersonalProperty";
			this.txtPersonalProperty.Style = "text-align: right";
			this.txtPersonalProperty.Text = "Field40";
			this.txtPersonalProperty.Top = 0.3125F;
			this.txtPersonalProperty.Width = 1.375F;
			// 
			// txtTotalHomestead
			// 
			this.txtTotalHomestead.Height = 0.1875F;
			this.txtTotalHomestead.Left = 4.4375F;
			this.txtTotalHomestead.Name = "txtTotalHomestead";
			this.txtTotalHomestead.Style = "text-align: right";
			this.txtTotalHomestead.Text = "Field41";
			this.txtTotalHomestead.Top = 0.875F;
			this.txtTotalHomestead.Width = 1.375F;
			// 
			// txtTotalTaxable
			// 
			this.txtTotalTaxable.Height = 0.1875F;
			this.txtTotalTaxable.Left = 6.1875F;
			this.txtTotalTaxable.Name = "txtTotalTaxable";
			this.txtTotalTaxable.Style = "text-align: right";
			this.txtTotalTaxable.Text = "Field42";
			this.txtTotalTaxable.Top = 0.59375F;
			this.txtTotalTaxable.Width = 1.25F;
			// 
			// txtTotalBase
			// 
			this.txtTotalBase.Height = 0.1875F;
			this.txtTotalBase.Left = 6.197917F;
			this.txtTotalBase.Name = "txtTotalBase";
			this.txtTotalBase.Style = "text-align: right";
			this.txtTotalBase.Text = "Field43";
			this.txtTotalBase.Top = 2.270833F;
			this.txtTotalBase.Width = 1.25F;
			// 
			// txtRevenueSharing
			// 
			this.txtRevenueSharing.Height = 0.1875F;
			this.txtRevenueSharing.Left = 4.4375F;
			this.txtRevenueSharing.Name = "txtRevenueSharing";
			this.txtRevenueSharing.Style = "text-align: right";
			this.txtRevenueSharing.Text = "Field44";
			this.txtRevenueSharing.Top = 4.1875F;
			this.txtRevenueSharing.Width = 1.375F;
			// 
			// Field45
			// 
			this.Field45.Height = 0.1875F;
			this.Field45.Left = 4.25F;
			this.Field45.Name = "Field45";
			this.Field45.Text = "$";
			this.Field45.Top = 4.1875F;
			this.Field45.Visible = false;
			this.Field45.Width = 0.1875F;
			// 
			// Field46
			// 
			this.Field46.Height = 0.1875F;
			this.Field46.Left = 4.25F;
			this.Field46.Name = "Field46";
			this.Field46.Text = "$";
			this.Field46.Top = 4.40625F;
			this.Field46.Visible = false;
			this.Field46.Width = 0.1875F;
			// 
			// txtOtherRevenue
			// 
			this.txtOtherRevenue.Height = 0.1875F;
			this.txtOtherRevenue.Left = 4.4375F;
			this.txtOtherRevenue.Name = "txtOtherRevenue";
			this.txtOtherRevenue.Style = "text-align: right";
			this.txtOtherRevenue.Text = "Field47";
			this.txtOtherRevenue.Top = 4.40625F;
			this.txtOtherRevenue.Width = 1.375F;
			// 
			// Field48
			// 
			this.Field48.Height = 0.1875F;
			this.Field48.Left = 6F;
			this.Field48.Name = "Field48";
			this.Field48.Text = "$";
			this.Field48.Top = 4.875F;
			this.Field48.Visible = false;
			this.Field48.Width = 0.1875F;
			// 
			// txtTotalDeductions
			// 
			this.txtTotalDeductions.Height = 0.1875F;
			this.txtTotalDeductions.Left = 6.1875F;
			this.txtTotalDeductions.Name = "txtTotalDeductions";
			this.txtTotalDeductions.Style = "text-align: right";
			this.txtTotalDeductions.Text = "Field49";
			this.txtTotalDeductions.Top = 4.875F;
			this.txtTotalDeductions.Width = 1.25F;
			// 
			// Field50
			// 
			this.Field50.Height = 0.1875F;
			this.Field50.Left = 6F;
			this.Field50.Name = "Field50";
			this.Field50.Text = "$";
			this.Field50.Top = 5.1875F;
			this.Field50.Visible = false;
			this.Field50.Width = 0.1875F;
			// 
			// txtNetToBeRaised
			// 
			this.txtNetToBeRaised.Height = 0.1875F;
			this.txtNetToBeRaised.Left = 6.1875F;
			this.txtNetToBeRaised.Name = "txtNetToBeRaised";
			this.txtNetToBeRaised.Style = "text-align: right";
			this.txtNetToBeRaised.Text = "Field51";
			this.txtNetToBeRaised.Top = 5.1875F;
			this.txtNetToBeRaised.Width = 1.25F;
			// 
			// Field52
			// 
			this.Field52.Height = 0.21875F;
			this.Field52.Left = 0.4375F;
			this.Field52.Name = "Field52";
			this.Field52.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field52.Text = "THIS PAGE MUST BE COMPLETED AND RETURNED WITH THE MVR";
			this.Field52.Top = 8.375F;
			this.Field52.Visible = false;
			this.Field52.Width = 6.5625F;
			// 
			// Field53
			// 
			this.Field53.Height = 0.28125F;
			this.Field53.Left = 1F;
			this.Field53.Name = "Field53";
			this.Field53.Style = "font-size: 8.5pt; text-align: center";
			this.Field53.Text = "Results from this completed form should be used to prepare the Municipal Tax Asse" +
    "ssment Warrant, Certificate of Assessment to Municipal Treasurer and Municipal V" +
    "aluation Return.";
			this.Field53.Top = 8.625F;
			this.Field53.Width = 5.4375F;
			// 
			// Field54
			// 
			this.Field54.Height = 0.1875F;
			this.Field54.Left = 0.375F;
			this.Field54.Name = "Field54";
			this.Field54.Text = "$";
			this.Field54.Top = 7.75F;
			this.Field54.Visible = false;
			this.Field54.Width = 0.1875F;
			// 
			// txt19Plus21
			// 
			this.txt19Plus21.Height = 0.1875F;
			this.txt19Plus21.Left = 0.5625F;
			this.txt19Plus21.Name = "txt19Plus21";
			this.txt19Plus21.Style = "text-align: right";
			this.txt19Plus21.Text = "Field55";
			this.txt19Plus21.Top = 7.75F;
			this.txt19Plus21.Width = 1.25F;
			// 
			// Field56
			// 
			this.Field56.Height = 0.19F;
			this.Field56.Left = 4.25F;
			this.Field56.Name = "Field56";
			this.Field56.Style = "font-size: 7pt; text-align: left";
			this.Field56.Text = "(Enter on line 5, Assessment Warrant)";
			this.Field56.Top = 7.9375F;
			this.Field56.Width = 1.75F;
			// 
			// Field57
			// 
			this.Field57.Height = 0.1875F;
			this.Field57.Left = 0.375F;
			this.Field57.Name = "Field57";
			this.Field57.Text = "$";
			this.Field57.Top = 5.5F;
			this.Field57.Visible = false;
			this.Field57.Width = 0.1875F;
			// 
			// txtNet1
			// 
			this.txtNet1.Height = 0.1875F;
			this.txtNet1.Left = 0.5625F;
			this.txtNet1.Name = "txtNet1";
			this.txtNet1.Style = "text-align: right";
			this.txtNet1.Text = "Field58";
			this.txtNet1.Top = 5.5F;
			this.txtNet1.Width = 1.25F;
			// 
			// Field59
			// 
			this.Field59.Height = 0.1875F;
			this.Field59.Left = 2F;
			this.Field59.Name = "Field59";
			this.Field59.Style = "font-weight: bold; text-align: center";
			this.Field59.Text = "X";
			this.Field59.Top = 5.5F;
			this.Field59.Width = 0.1875F;
			// 
			// Field60
			// 
			this.Field60.Height = 0.1875F;
			this.Field60.Left = 2.3125F;
			this.Field60.Name = "Field60";
			this.Field60.Text = "$";
			this.Field60.Top = 6.125F;
			this.Field60.Visible = false;
			this.Field60.Width = 0.1875F;
			// 
			// txtBase2
			// 
			this.txtBase2.Height = 0.1875F;
			this.txtBase2.Left = 2.5F;
			this.txtBase2.Name = "txtBase2";
			this.txtBase2.Style = "text-align: right";
			this.txtBase2.Text = "Field61";
			this.txtBase2.Top = 6.125F;
			this.txtBase2.Width = 1.1875F;
			// 
			// Field62
			// 
			this.Field62.Height = 0.1875F;
			this.Field62.Left = 3.875F;
			this.Field62.Name = "Field62";
			this.Field62.Style = "font-weight: bold";
			this.Field62.Text = "=";
			this.Field62.Top = 5.5F;
			this.Field62.Width = 0.1875F;
			// 
			// Field63
			// 
			this.Field63.Height = 0.1875F;
			this.Field63.Left = 4.25F;
			this.Field63.Name = "Field63";
			this.Field63.Text = "$";
			this.Field63.Top = 5.5F;
			this.Field63.Visible = false;
			this.Field63.Width = 0.1875F;
			// 
			// txtMaxTax
			// 
			this.txtMaxTax.Height = 0.1875F;
			this.txtMaxTax.Left = 4.4375F;
			this.txtMaxTax.Name = "txtMaxTax";
			this.txtMaxTax.Style = "text-align: right";
			this.txtMaxTax.Text = "Field65";
			this.txtMaxTax.Top = 5.5F;
			this.txtMaxTax.Width = 1.1875F;
			// 
			// Field66
			// 
			this.Field66.Height = 0.1875F;
			this.Field66.Left = 5.6875F;
			this.Field66.Name = "Field66";
			this.Field66.Text = "Maximum Allowable Tax";
			this.Field66.Top = 5.5F;
			this.Field66.Width = 1.5625F;
			// 
			// Field67
			// 
			this.Field67.Height = 0.1875F;
			this.Field67.Left = 5.6875F;
			this.Field67.Name = "Field67";
			this.Field67.Text = "Minimum Tax Rate";
			this.Field67.Top = 5.8125F;
			this.Field67.Width = 1.5625F;
			// 
			// Field68
			// 
			this.Field68.Height = 0.1875F;
			this.Field68.Left = 5.6875F;
			this.Field68.Name = "Field68";
			this.Field68.Text = "Maximum Tax Rate";
			this.Field68.Top = 6.125F;
			this.Field68.Width = 1.5625F;
			// 
			// Field69
			// 
			this.Field69.Height = 0.1875F;
			this.Field69.Left = 5.6875F;
			this.Field69.Name = "Field69";
			this.Field69.Text = "Tax for Commitment";
			this.Field69.Top = 6.4375F;
			this.Field69.Width = 1.5625F;
			// 
			// Field70
			// 
			this.Field70.Height = 0.1875F;
			this.Field70.Left = 5.6875F;
			this.Field70.Name = "Field70";
			this.Field70.Text = "Maximum Overlay";
			this.Field70.Top = 6.75F;
			this.Field70.Width = 1.5625F;
			// 
			// Field71
			// 
			this.Field71.Height = 0.1875F;
			this.Field71.Left = 5.6875F;
			this.Field71.Name = "Field71";
			this.Field71.Text = "Homestead Reimbursement";
			this.Field71.Top = 7.0625F;
			this.Field71.Width = 1.75F;
			// 
			// Field72
			// 
			this.Field72.Height = 0.1875F;
			this.Field72.Left = 5.6875F;
			this.Field72.Name = "Field72";
			this.Field72.Text = "Overlay";
			this.Field72.Top = 7.75F;
			this.Field72.Width = 1.5625F;
			// 
			// Field73
			// 
			this.Field73.Height = 0.1875F;
			this.Field73.Left = 2.3125F;
			this.Field73.Name = "Field73";
			this.Field73.Text = "$";
			this.Field73.Top = 5.8125F;
			this.Field73.Visible = false;
			this.Field73.Width = 0.1875F;
			// 
			// txtBase1
			// 
			this.txtBase1.Height = 0.1875F;
			this.txtBase1.Left = 2.5F;
			this.txtBase1.Name = "txtBase1";
			this.txtBase1.Style = "text-align: right";
			this.txtBase1.Text = "Field74";
			this.txtBase1.Top = 5.8125F;
			this.txtBase1.Width = 1.1875F;
			// 
			// Field75
			// 
			this.Field75.Height = 0.1875F;
			this.Field75.Left = 2.5F;
			this.Field75.Name = "Field75";
			this.Field75.Style = "text-align: right";
			this.Field75.Text = "1.05";
			this.Field75.Top = 5.5F;
			this.Field75.Width = 0.875F;
			// 
			// Field76
			// 
			this.Field76.Height = 0.1875F;
			this.Field76.Left = 2F;
			this.Field76.Name = "Field76";
			this.Field76.Style = "font-weight: bold; text-align: center";
			this.Field76.Text = "/";
			this.Field76.Top = 5.8125F;
			this.Field76.Width = 0.1875F;
			// 
			// Field77
			// 
			this.Field77.Height = 0.1875F;
			this.Field77.Left = 2F;
			this.Field77.Name = "Field77";
			this.Field77.Style = "font-weight: bold; text-align: center";
			this.Field77.Text = "/";
			this.Field77.Top = 6.125F;
			this.Field77.Width = 0.1875F;
			// 
			// Field78
			// 
			this.Field78.Height = 0.1875F;
			this.Field78.Left = 2F;
			this.Field78.Name = "Field78";
			this.Field78.Style = "font-weight: bold; text-align: center";
			this.Field78.Text = "X";
			this.Field78.Top = 6.4375F;
			this.Field78.Width = 0.1875F;
			// 
			// Field79
			// 
			this.Field79.Height = 0.1875F;
			this.Field79.Left = 2F;
			this.Field79.Name = "Field79";
			this.Field79.Style = "font-weight: bold; text-align: center";
			this.Field79.Text = "X";
			this.Field79.Top = 6.75F;
			this.Field79.Width = 0.1875F;
			// 
			// Field80
			// 
			this.Field80.Height = 0.1875F;
			this.Field80.Left = 2F;
			this.Field80.Name = "Field80";
			this.Field80.Style = "font-weight: bold; text-align: center";
			this.Field80.Text = "X";
			this.Field80.Top = 7.072917F;
			this.Field80.Width = 0.1875F;
			// 
			// Field81
			// 
			this.Field81.Height = 0.1875F;
			this.Field81.Left = 2F;
			this.Field81.Name = "Field81";
			this.Field81.Style = "font-weight: bold; text-align: center";
			this.Field81.Text = "-";
			this.Field81.Top = 7.75F;
			this.Field81.Width = 0.1875F;
			// 
			// Field82
			// 
			this.Field82.Height = 0.1875F;
			this.Field82.Left = 0.375F;
			this.Field82.Name = "Field82";
			this.Field82.Text = "$";
			this.Field82.Top = 5.8125F;
			this.Field82.Visible = false;
			this.Field82.Width = 0.1875F;
			// 
			// txtNet2
			// 
			this.txtNet2.Height = 0.1875F;
			this.txtNet2.Left = 0.5625F;
			this.txtNet2.Name = "txtNet2";
			this.txtNet2.Style = "text-align: right";
			this.txtNet2.Text = "Field83";
			this.txtNet2.Top = 5.8125F;
			this.txtNet2.Width = 1.25F;
			// 
			// Field84
			// 
			this.Field84.Height = 0.1875F;
			this.Field84.Left = 0.375F;
			this.Field84.Name = "Field84";
			this.Field84.Text = "$";
			this.Field84.Top = 6.125F;
			this.Field84.Visible = false;
			this.Field84.Width = 0.1875F;
			// 
			// txtMaxTax2
			// 
			this.txtMaxTax2.Height = 0.1875F;
			this.txtMaxTax2.Left = 0.5625F;
			this.txtMaxTax2.Name = "txtMaxTax2";
			this.txtMaxTax2.Style = "text-align: right";
			this.txtMaxTax2.Text = "Field85";
			this.txtMaxTax2.Top = 6.125F;
			this.txtMaxTax2.Width = 1.25F;
			// 
			// Field86
			// 
			this.Field86.Height = 0.1875F;
			this.Field86.Left = 0.375F;
			this.Field86.Name = "Field86";
			this.Field86.Text = "$";
			this.Field86.Top = 6.4375F;
			this.Field86.Visible = false;
			this.Field86.Width = 0.1875F;
			// 
			// txtTotTaxable
			// 
			this.txtTotTaxable.Height = 0.1875F;
			this.txtTotTaxable.Left = 0.375F;
			this.txtTotTaxable.Name = "txtTotTaxable";
			this.txtTotTaxable.Style = "text-align: right";
			this.txtTotTaxable.Text = "Field87";
			this.txtTotTaxable.Top = 6.4375F;
			this.txtTotTaxable.Width = 1.4375F;
			// 
			// Field88
			// 
			this.Field88.Height = 0.1875F;
			this.Field88.Left = 0.375F;
			this.Field88.Name = "Field88";
			this.Field88.Text = "$";
			this.Field88.Top = 6.75F;
			this.Field88.Visible = false;
			this.Field88.Width = 0.1875F;
			// 
			// txtNet3
			// 
			this.txtNet3.Height = 0.1875F;
			this.txtNet3.Left = 0.5625F;
			this.txtNet3.Name = "txtNet3";
			this.txtNet3.Style = "text-align: right";
			this.txtNet3.Text = "Field89";
			this.txtNet3.Top = 6.75F;
			this.txtNet3.Width = 1.25F;
			// 
			// Field90
			// 
			this.Field90.Height = 0.1875F;
			this.Field90.Left = 0.375F;
			this.Field90.Name = "Field90";
			this.Field90.Text = "$";
			this.Field90.Top = 7.0625F;
			this.Field90.Visible = false;
			this.Field90.Width = 0.1875F;
			// 
			// txtHomesteadVal
			// 
			this.txtHomesteadVal.Height = 0.1875F;
			this.txtHomesteadVal.Left = 0.5625F;
			this.txtHomesteadVal.Name = "txtHomesteadVal";
			this.txtHomesteadVal.Style = "text-align: right";
			this.txtHomesteadVal.Text = "Field91";
			this.txtHomesteadVal.Top = 7.0625F;
			this.txtHomesteadVal.Width = 1.25F;
			// 
			// txtTaxRate
			// 
			this.txtTaxRate.Height = 0.1875F;
			this.txtTaxRate.Left = 2.5F;
			this.txtTaxRate.Name = "txtTaxRate";
			this.txtTaxRate.Style = "text-align: right";
			this.txtTaxRate.Text = "Field92";
			this.txtTaxRate.Top = 6.4375F;
			this.txtTaxRate.Width = 1.1875F;
			// 
			// txtTaxRate2
			// 
			this.txtTaxRate2.Height = 0.1875F;
			this.txtTaxRate2.Left = 2.541667F;
			this.txtTaxRate2.Name = "txtTaxRate2";
			this.txtTaxRate2.Style = "text-align: right";
			this.txtTaxRate2.Text = "Field93";
			this.txtTaxRate2.Top = 7.0625F;
			this.txtTaxRate2.Width = 1.145833F;
			// 
			// txtNet4
			// 
			this.txtNet4.Height = 0.1875F;
			this.txtNet4.Left = 2.5F;
			this.txtNet4.Name = "txtNet4";
			this.txtNet4.Style = "text-align: right";
			this.txtNet4.Text = "Field94";
			this.txtNet4.Top = 7.75F;
			this.txtNet4.Width = 1.1875F;
			// 
			// Field95
			// 
			this.Field95.Height = 0.1875F;
			this.Field95.Left = 2.5625F;
			this.Field95.Name = "Field95";
			this.Field95.Style = "text-align: right";
			this.Field95.Text = "0.05";
			this.Field95.Top = 6.75F;
			this.Field95.Width = 0.8125F;
			// 
			// Field96
			// 
			this.Field96.Height = 0.1875F;
			this.Field96.Left = 3.875F;
			this.Field96.Name = "Field96";
			this.Field96.Style = "font-weight: bold";
			this.Field96.Text = "=";
			this.Field96.Top = 5.8125F;
			this.Field96.Width = 0.1875F;
			// 
			// Field97
			// 
			this.Field97.Height = 0.1875F;
			this.Field97.Left = 3.875F;
			this.Field97.Name = "Field97";
			this.Field97.Style = "font-weight: bold";
			this.Field97.Text = "=";
			this.Field97.Top = 6.125F;
			this.Field97.Width = 0.1875F;
			// 
			// Field98
			// 
			this.Field98.Height = 0.1875F;
			this.Field98.Left = 3.875F;
			this.Field98.Name = "Field98";
			this.Field98.Style = "font-weight: bold";
			this.Field98.Text = "=";
			this.Field98.Top = 6.4375F;
			this.Field98.Width = 0.1875F;
			// 
			// Field99
			// 
			this.Field99.Height = 0.1875F;
			this.Field99.Left = 3.875F;
			this.Field99.Name = "Field99";
			this.Field99.Style = "font-weight: bold";
			this.Field99.Text = "=";
			this.Field99.Top = 6.75F;
			this.Field99.Width = 0.1875F;
			// 
			// Field100
			// 
			this.Field100.Height = 0.1875F;
			this.Field100.Left = 3.875F;
			this.Field100.Name = "Field100";
			this.Field100.Style = "font-weight: bold";
			this.Field100.Text = "=";
			this.Field100.Top = 7.0625F;
			this.Field100.Width = 0.1875F;
			// 
			// Field101
			// 
			this.Field101.Height = 0.1875F;
			this.Field101.Left = 3.875F;
			this.Field101.Name = "Field101";
			this.Field101.Style = "font-weight: bold";
			this.Field101.Text = "=";
			this.Field101.Top = 7.75F;
			this.Field101.Width = 0.1875F;
			// 
			// txtMinRate
			// 
			this.txtMinRate.Height = 0.1875F;
			this.txtMinRate.Left = 4.4375F;
			this.txtMinRate.Name = "txtMinRate";
			this.txtMinRate.Style = "text-align: right";
			this.txtMinRate.Text = "Field102";
			this.txtMinRate.Top = 5.8125F;
			this.txtMinRate.Width = 1.1875F;
			// 
			// txtMaxRate
			// 
			this.txtMaxRate.Height = 0.1875F;
			this.txtMaxRate.Left = 4.4375F;
			this.txtMaxRate.Name = "txtMaxRate";
			this.txtMaxRate.Style = "text-align: right";
			this.txtMaxRate.Text = "Field103";
			this.txtMaxRate.Top = 6.125F;
			this.txtMaxRate.Width = 1.1875F;
			// 
			// Field104
			// 
			this.Field104.Height = 0.1875F;
			this.Field104.Left = 4.25F;
			this.Field104.Name = "Field104";
			this.Field104.Text = "$";
			this.Field104.Top = 6.4375F;
			this.Field104.Visible = false;
			this.Field104.Width = 0.1875F;
			// 
			// txtTaxCommitment
			// 
			this.txtTaxCommitment.Height = 0.1875F;
			this.txtTaxCommitment.Left = 4.4375F;
			this.txtTaxCommitment.Name = "txtTaxCommitment";
			this.txtTaxCommitment.Style = "text-align: right";
			this.txtTaxCommitment.Text = "Field105";
			this.txtTaxCommitment.Top = 6.4375F;
			this.txtTaxCommitment.Width = 1.1875F;
			// 
			// Field106
			// 
			this.Field106.Height = 0.1875F;
			this.Field106.Left = 4.25F;
			this.Field106.Name = "Field106";
			this.Field106.Text = "$";
			this.Field106.Top = 6.75F;
			this.Field106.Visible = false;
			this.Field106.Width = 0.1875F;
			// 
			// txtMaxOverlay
			// 
			this.txtMaxOverlay.Height = 0.1875F;
			this.txtMaxOverlay.Left = 4.4375F;
			this.txtMaxOverlay.Name = "txtMaxOverlay";
			this.txtMaxOverlay.Style = "text-align: right";
			this.txtMaxOverlay.Text = "Field107";
			this.txtMaxOverlay.Top = 6.75F;
			this.txtMaxOverlay.Width = 1.1875F;
			// 
			// Field108
			// 
			this.Field108.Height = 0.1875F;
			this.Field108.Left = 4.25F;
			this.Field108.Name = "Field108";
			this.Field108.Text = "$";
			this.Field108.Top = 7.0625F;
			this.Field108.Visible = false;
			this.Field108.Width = 0.1875F;
			// 
			// txtHomestead
			// 
			this.txtHomestead.Height = 0.1875F;
			this.txtHomestead.Left = 4.4375F;
			this.txtHomestead.Name = "txtHomestead";
			this.txtHomestead.Style = "text-align: right";
			this.txtHomestead.Text = "Field109";
			this.txtHomestead.Top = 7.0625F;
			this.txtHomestead.Width = 1.1875F;
			// 
			// Field110
			// 
			this.Field110.Height = 0.1875F;
			this.Field110.Left = 4.25F;
			this.Field110.Name = "Field110";
			this.Field110.Text = "$";
			this.Field110.Top = 7.75F;
			this.Field110.Visible = false;
			this.Field110.Width = 0.1875F;
			// 
			// txtOverlay
			// 
			this.txtOverlay.Height = 0.1875F;
			this.txtOverlay.Left = 4.4375F;
			this.txtOverlay.Name = "txtOverlay";
			this.txtOverlay.Style = "text-align: right";
			this.txtOverlay.Text = "Field111";
			this.txtOverlay.Top = 7.75F;
			this.txtOverlay.Width = 1.1875F;
			// 
			// Field111
			// 
			this.Field111.Height = 0.19F;
			this.Field111.Left = 4.25F;
			this.Field111.Name = "Field111";
			this.Field111.Style = "font-size: 7pt; text-align: left";
			this.Field111.Text = "(Enter on MVR Page 1, line 13)";
			this.Field111.Top = 6.625F;
			this.Field111.Width = 1.4375F;
			// 
			// Field112
			// 
			this.Field112.Height = 0.19F;
			this.Field112.Left = 4.25F;
			this.Field112.Name = "Field112";
			this.Field112.Style = "font-size: 7pt; text-align: left";
			this.Field112.Text = "(Enter on line 8, Assessment Warrant)";
			this.Field112.Top = 7.25F;
			this.Field112.Width = 1.9375F;
			// 
			// Field114
			// 
			this.Field114.Height = 0.125F;
			this.Field114.Left = 0.375F;
			this.Field114.Name = "Field114";
			this.Field114.Style = "font-size: 7pt; text-align: left";
			this.Field114.Text = "(Adjusted to Municipal Fiscal Year)";
			this.Field114.Top = 3.5625F;
			this.Field114.Width = 2.4375F;
			// 
			// Field117
			// 
			this.Field117.Height = 0.125F;
			this.Field117.Left = 4.5625F;
			this.Field117.Name = "Field117";
			this.Field117.Style = "font-size: 7pt; text-align: left";
			this.Field117.Text = "(Line 4(a) multiplied by .7)";
			this.Field117.Top = 1.3125F;
			this.Field117.Width = 1.6875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.28125F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.28125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 7pt";
			this.Label1.Text = "appropriated to be used to reduce the commitment such as excise tax revenue, tree" +
    " growth reimbursement, trust fund or bank interest income, appropriated surplus " +
    "revenue, etc.";
			this.Label1.Top = 4.572917F;
			this.Label1.Width = 5.770833F;
			// 
			// Field118
			// 
			this.Field118.Height = 0.19F;
			this.Field118.Left = 0.375F;
			this.Field118.Name = "Field118";
			this.Field118.Style = "font-size: 7pt; text-align: right";
			this.Field118.Text = "(Line 19 plus lines 21 and 22)";
			this.Field118.Top = 7.96875F;
			this.Field118.Width = 1.4375F;
			// 
			// Field119
			// 
			this.Field119.Height = 0.19F;
			this.Field119.Left = 2.5625F;
			this.Field119.Name = "Field119";
			this.Field119.Style = "font-size: 7pt; text-align: right";
			this.Field119.Text = "(Selected Rate)";
			this.Field119.Top = 7.25F;
			this.Field119.Width = 1.125F;
			// 
			// Field120
			// 
			this.Field120.Height = 0.125F;
			this.Field120.Left = 2.5625F;
			this.Field120.Name = "Field120";
			this.Field120.Style = "font-size: 7pt; text-align: right";
			this.Field120.Text = "(Selected Rate)";
			this.Field120.Top = 6.625F;
			this.Field120.Width = 1.125F;
			// 
			// Field121
			// 
			this.Field121.Height = 0.19F;
			this.Field121.Left = 0.375F;
			this.Field121.Name = "Field121";
			this.Field121.Style = "font-size: 8.5pt; text-align: left";
			this.Field121.Text = "(If Line 23 exceeds Line 20 select a lower tax rate.)";
			this.Field121.Top = 8.15625F;
			this.Field121.Width = 4.25F;
			// 
			// Field123
			// 
			this.Field123.Height = 0.19F;
			this.Field123.Left = 3.5F;
			this.Field123.Name = "Field123";
			this.Field123.Style = "font-size: 8.5pt; text-align: center";
			this.Field123.Text = "-10 -";
			this.Field123.Top = 8.9375F;
			this.Field123.Width = 0.375F;
			// 
			// Field124
			// 
			this.Field124.Height = 0.25F;
			this.Field124.Left = 0.5F;
			this.Field124.Name = "Field124";
			this.Field124.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field124.Text = "Do NOT return with the MVR. This page is for information only.";
			this.Field124.Top = 8.375F;
			this.Field124.Visible = false;
			this.Field124.Width = 6.4375F;
			// 
			// Field127
			// 
			this.Field127.Height = 0.1875F;
			this.Field127.Left = 0.2604167F;
			this.Field127.Name = "Field127";
			this.Field127.Style = "font-size: 9pt";
			this.Field127.Text = "(b) Homestead exemption reimbursement value";
			this.Field127.Top = 1.125F;
			this.Field127.Width = 3.5F;
			// 
			// Field128
			// 
			this.Field128.Height = 0.1875F;
			this.Field128.Left = 4.25F;
			this.Field128.Name = "Field128";
			this.Field128.Text = "$";
			this.Field128.Top = 1.125F;
			this.Field128.Visible = false;
			this.Field128.Width = 0.1875F;
			// 
			// txtHalfHomestead
			// 
			this.txtHalfHomestead.Height = 0.1875F;
			this.txtHalfHomestead.Left = 4.4375F;
			this.txtHalfHomestead.Name = "txtHalfHomestead";
			this.txtHalfHomestead.Style = "text-align: right";
			this.txtHalfHomestead.Text = "Field41";
			this.txtHalfHomestead.Top = 1.125F;
			this.txtHalfHomestead.Width = 1.375F;
			// 
			// Field130
			// 
			this.Field130.Height = 0.1875F;
			this.Field130.Left = 3.875F;
			this.Field130.Name = "Field130";
			this.Field130.Style = "text-align: right";
			this.Field130.Text = "4(a)";
			this.Field130.Top = 0.875F;
			this.Field130.Width = 0.3125F;
			// 
			// Field131
			// 
			this.Field131.Height = 0.1875F;
			this.Field131.Left = 3.875F;
			this.Field131.Name = "Field131";
			this.Field131.Style = "text-align: right";
			this.Field131.Text = "4(b)";
			this.Field131.Top = 1.125F;
			this.Field131.Width = 0.3125F;
			// 
			// Field132
			// 
			this.Field132.Height = 0.1875F;
			this.Field132.Left = 0.0625F;
			this.Field132.Name = "Field132";
			this.Field132.Style = "font-size: 9pt";
			this.Field132.Text = "5.  Total exempt value of all BETE qualified property";
			this.Field132.Top = 1.4375F;
			this.Field132.Width = 3.25F;
			// 
			// Field133
			// 
			this.Field133.Height = 0.1875F;
			this.Field133.Left = 4.25F;
			this.Field133.Name = "Field133";
			this.Field133.Text = "$";
			this.Field133.Top = 1.4375F;
			this.Field133.Visible = false;
			this.Field133.Width = 0.1875F;
			// 
			// txtBETEValuation
			// 
			this.txtBETEValuation.Height = 0.1875F;
			this.txtBETEValuation.Left = 4.4375F;
			this.txtBETEValuation.Name = "txtBETEValuation";
			this.txtBETEValuation.Style = "text-align: right";
			this.txtBETEValuation.Text = "Field41";
			this.txtBETEValuation.Top = 1.4375F;
			this.txtBETEValuation.Width = 1.375F;
			// 
			// Field135
			// 
			this.Field135.Height = 0.1875F;
			this.Field135.Left = 3.875F;
			this.Field135.Name = "Field135";
			this.Field135.Style = "text-align: right";
			this.Field135.Text = "5(a)";
			this.Field135.Top = 1.4375F;
			this.Field135.Width = 0.3125F;
			// 
			// lbl5btext
			// 
			this.lbl5btext.Height = 0.125F;
			this.lbl5btext.Left = 4.562F;
			this.lbl5btext.Name = "lbl5btext";
			this.lbl5btext.Style = "font-size: 7pt; text-align: left";
			this.lbl5btext.Text = "(line 5(a) multiplied by 0.5)";
			this.lbl5btext.Top = 1.875F;
			this.lbl5btext.Visible = false;
			this.lbl5btext.Width = 1.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.125F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.4375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 7pt";
			this.Label2.Text = "(All other revenues that have been formally";
			this.Label2.Top = 4.4375F;
			this.Label2.Width = 2.1875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.125F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.59375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 7pt; font-weight: bold";
			this.Label3.Text = "Do Not Include any Homestead or BETE Reimbursement";
			this.Label3.Top = 4.697917F;
			this.Label3.Width = 2.9375F;
			// 
			// Field137
			// 
			this.Field137.Height = 0.1875F;
			this.Field137.Left = 0F;
			this.Field137.Name = "Field137";
			this.Field137.Text = "22.";
			this.Field137.Top = 7.395833F;
			this.Field137.Width = 0.25F;
			// 
			// Field138
			// 
			this.Field138.Height = 0.1875F;
			this.Field138.Left = 5.6875F;
			this.Field138.Name = "Field138";
			this.Field138.Text = "BETE Reimbursement";
			this.Field138.Top = 7.395833F;
			this.Field138.Width = 1.75F;
			// 
			// Field139
			// 
			this.Field139.Height = 0.1875F;
			this.Field139.Left = 2F;
			this.Field139.Name = "Field139";
			this.Field139.Style = "font-weight: bold; text-align: center";
			this.Field139.Text = "X";
			this.Field139.Top = 7.40625F;
			this.Field139.Width = 0.1875F;
			// 
			// Field140
			// 
			this.Field140.Height = 0.1875F;
			this.Field140.Left = 0.375F;
			this.Field140.Name = "Field140";
			this.Field140.Text = "$";
			this.Field140.Top = 7.395833F;
			this.Field140.Visible = false;
			this.Field140.Width = 0.1875F;
			// 
			// txtBETEVal
			// 
			this.txtBETEVal.Height = 0.1875F;
			this.txtBETEVal.Left = 0.5625F;
			this.txtBETEVal.Name = "txtBETEVal";
			this.txtBETEVal.Style = "text-align: right";
			this.txtBETEVal.Text = "Field91";
			this.txtBETEVal.Top = 7.395833F;
			this.txtBETEVal.Width = 1.25F;
			// 
			// txtTaxRate3
			// 
			this.txtTaxRate3.Height = 0.1875F;
			this.txtTaxRate3.Left = 2.541667F;
			this.txtTaxRate3.Name = "txtTaxRate3";
			this.txtTaxRate3.Style = "text-align: right";
			this.txtTaxRate3.Text = "Field93";
			this.txtTaxRate3.Top = 7.395833F;
			this.txtTaxRate3.Width = 1.145833F;
			// 
			// Field143
			// 
			this.Field143.Height = 0.1875F;
			this.Field143.Left = 3.875F;
			this.Field143.Name = "Field143";
			this.Field143.Style = "font-weight: bold";
			this.Field143.Text = "=";
			this.Field143.Top = 7.395833F;
			this.Field143.Width = 0.1875F;
			// 
			// Field144
			// 
			this.Field144.Height = 0.1875F;
			this.Field144.Left = 4.25F;
			this.Field144.Name = "Field144";
			this.Field144.Text = "$";
			this.Field144.Top = 7.395833F;
			this.Field144.Visible = false;
			this.Field144.Width = 0.1875F;
			// 
			// txtBETE
			// 
			this.txtBETE.Height = 0.1875F;
			this.txtBETE.Left = 4.4375F;
			this.txtBETE.Name = "txtBETE";
			this.txtBETE.Style = "text-align: right";
			this.txtBETE.Text = "Field109";
			this.txtBETE.Top = 7.395833F;
			this.txtBETE.Width = 1.1875F;
			// 
			// Field146
			// 
			this.Field146.Height = 0.19F;
			this.Field146.Left = 4.25F;
			this.Field146.Name = "Field146";
			this.Field146.Style = "font-size: 7pt; text-align: left";
			this.Field146.Text = "(Enter on line 9, Assessment Warrant)";
			this.Field146.Top = 7.583333F;
			this.Field146.Width = 1.9375F;
			// 
			// Field147
			// 
			this.Field147.Height = 0.19F;
			this.Field147.Left = 2.5625F;
			this.Field147.Name = "Field147";
			this.Field147.Style = "font-size: 7pt; text-align: right";
			this.Field147.Text = "(Selected Rate)";
			this.Field147.Top = 7.583333F;
			this.Field147.Width = 1.125F;
			// 
			// Field148
			// 
			this.Field148.Height = 0.1875F;
			this.Field148.Left = 3.875F;
			this.Field148.Name = "Field148";
			this.Field148.Style = "text-align: right";
			this.Field148.Text = "1";
			this.Field148.Top = 0.03125F;
			this.Field148.Width = 0.3125F;
			// 
			// Field149
			// 
			this.Field149.Height = 0.1875F;
			this.Field149.Left = 3.875F;
			this.Field149.Name = "Field149";
			this.Field149.Style = "text-align: right";
			this.Field149.Text = "2";
			this.Field149.Top = 0.3125F;
			this.Field149.Width = 0.3125F;
			// 
			// Field150
			// 
			this.Field150.Height = 0.1875F;
			this.Field150.Left = 5.6875F;
			this.Field150.Name = "Field150";
			this.Field150.Style = "text-align: right";
			this.Field150.Text = "3";
			this.Field150.Top = 0.59375F;
			this.Field150.Width = 0.3125F;
			// 
			// Field151
			// 
			this.Field151.Height = 0.1875F;
			this.Field151.Left = 5.697917F;
			this.Field151.Name = "Field151";
			this.Field151.Style = "text-align: right";
			this.Field151.Text = "6";
			this.Field151.Top = 2.270833F;
			this.Field151.Width = 0.3125F;
			// 
			// Field152
			// 
			this.Field152.Height = 0.1875F;
			this.Field152.Left = 3.875F;
			this.Field152.Name = "Field152";
			this.Field152.Style = "font-size: 10pt; text-align: right";
			this.Field152.Text = "7";
			this.Field152.Top = 2.71875F;
			this.Field152.Width = 0.3125F;
			// 
			// Field153
			// 
			this.Field153.Height = 0.1875F;
			this.Field153.Left = 3.875F;
			this.Field153.Name = "Field153";
			this.Field153.Style = "font-size: 10pt; text-align: right";
			this.Field153.Text = "8";
			this.Field153.Top = 2.9375F;
			this.Field153.Width = 0.3125F;
			// 
			// Field154
			// 
			this.Field154.Height = 0.1875F;
			this.Field154.Left = 3.875F;
			this.Field154.Name = "Field154";
			this.Field154.Style = "font-size: 10pt; text-align: right";
			this.Field154.Text = "9";
			this.Field154.Top = 3.15625F;
			this.Field154.Width = 0.3125F;
			// 
			// Field155
			// 
			this.Field155.Height = 0.1875F;
			this.Field155.Left = 3.875F;
			this.Field155.Name = "Field155";
			this.Field155.Style = "font-size: 10pt; text-align: right";
			this.Field155.Text = "10";
			this.Field155.Top = 3.375F;
			this.Field155.Width = 0.3125F;
			// 
			// Field156
			// 
			this.Field156.Height = 0.1875F;
			this.Field156.Left = 5.6875F;
			this.Field156.Name = "Field156";
			this.Field156.Style = "text-align: right";
			this.Field156.Text = "11";
			this.Field156.Top = 3.71875F;
			this.Field156.Width = 0.3125F;
			// 
			// Field157
			// 
			this.Field157.Height = 0.1875F;
			this.Field157.Left = 3.875F;
			this.Field157.Name = "Field157";
			this.Field157.Style = "text-align: right";
			this.Field157.Text = "12";
			this.Field157.Top = 4.1875F;
			this.Field157.Width = 0.3125F;
			// 
			// Field158
			// 
			this.Field158.Height = 0.1875F;
			this.Field158.Left = 3.875F;
			this.Field158.Name = "Field158";
			this.Field158.Style = "text-align: right";
			this.Field158.Text = "13";
			this.Field158.Top = 4.40625F;
			this.Field158.Width = 0.3125F;
			// 
			// Field159
			// 
			this.Field159.Height = 0.1875F;
			this.Field159.Left = 5.6875F;
			this.Field159.Name = "Field159";
			this.Field159.Style = "text-align: right";
			this.Field159.Text = "14";
			this.Field159.Top = 4.875F;
			this.Field159.Width = 0.3125F;
			// 
			// Field160
			// 
			this.Field160.Height = 0.1875F;
			this.Field160.Left = 5.6875F;
			this.Field160.Name = "Field160";
			this.Field160.Style = "text-align: right";
			this.Field160.Text = "15";
			this.Field160.Top = 5.1875F;
			this.Field160.Width = 0.3125F;
			// 
			// Field162
			// 
			this.Field162.Height = 0.1875F;
			this.Field162.Left = 2.3125F;
			this.Field162.Name = "Field162";
			this.Field162.Style = "font-size: 8.5pt; font-weight: bold; vertical-align: middle";
			this.Field162.Text = "(local share/contribution)";
			this.Field162.Top = 3.375F;
			this.Field162.Width = 1.71875F;
			// 
			// Field163
			// 
			this.Field163.Height = 0.1875F;
			this.Field163.Left = 0.2604167F;
			this.Field163.Name = "Field163";
			this.Field163.Style = "font-size: 9pt";
			this.Field163.Text = "BETE exemption reimbursement value";
			this.Field163.Top = 1.6875F;
			this.Field163.Width = 3.645833F;
			// 
			// Field164
			// 
			this.Field164.Height = 0.1875F;
			this.Field164.Left = 4.25F;
			this.Field164.Name = "Field164";
			this.Field164.Text = "$";
			this.Field164.Top = 1.6875F;
			this.Field164.Visible = false;
			this.Field164.Width = 0.1875F;
			// 
			// txtBETEReimburseValue
			// 
			this.txtBETEReimburseValue.Height = 0.1875F;
			this.txtBETEReimburseValue.Left = 4.4375F;
			this.txtBETEReimburseValue.Name = "txtBETEReimburseValue";
			this.txtBETEReimburseValue.Style = "text-align: right";
			this.txtBETEReimburseValue.Text = "Field41";
			this.txtBETEReimburseValue.Top = 1.6875F;
			this.txtBETEReimburseValue.Width = 1.375F;
			// 
			// Field166
			// 
			this.Field166.Height = 0.1875F;
			this.Field166.Left = 3.875F;
			this.Field166.Name = "Field166";
			this.Field166.Style = "text-align: right";
			this.Field166.Text = "5(b)";
			this.Field166.Top = 1.6875F;
			this.Field166.Width = 0.3125F;
			// 
			// Field167
			// 
			this.Field167.Height = 0.1875F;
			this.Field167.Left = 0.4583333F;
			this.Field167.Name = "Field167";
			this.Field167.Style = "font-size: 8.5pt";
			this.Field167.Text = "Municipalities with significant personal property & equipment";
			this.Field167.Top = 1.875F;
			this.Field167.Width = 3.3125F;
			// 
			// Field168
			// 
			this.Field168.Height = 0.1875F;
			this.Field168.Left = 0.4583333F;
			this.Field168.Name = "Field168";
			this.Field168.Style = "font-size: 8.5pt";
			this.Field168.Text = "may qualify for more than 50% reimbursement. Contact MRS for the Enhanced Tax Rat" +
    "e Calculator form.";
			this.Field168.Top = 2.0625F;
			this.Field168.Width = 6.3125F;
			// 
			// Field169
			// 
			this.Field169.Height = 0.125F;
			this.Field169.Left = 4.5625F;
			this.Field169.Name = "Field169";
			this.Field169.Style = "font-size: 7pt; text-align: left";
			this.Field169.Text = "(must match MVR Page 1, line 6)";
			this.Field169.Top = 0.2083333F;
			this.Field169.Width = 1.875F;
			// 
			// Field170
			// 
			this.Field170.Height = 0.125F;
			this.Field170.Left = 4.5625F;
			this.Field170.Name = "Field170";
			this.Field170.Style = "font-size: 7pt; text-align: left";
			this.Field170.Text = "(must match MVR Page 1, line10)";
			this.Field170.Top = 0.4722222F;
			this.Field170.Width = 2.1875F;
			// 
			// Field171
			// 
			this.Field171.Height = 0.125F;
			this.Field171.Left = 5.5625F;
			this.Field171.Name = "Field171";
			this.Field171.Style = "font-size: 7pt; text-align: right";
			this.Field171.Text = "(must match MVR Page 1, line 11)";
			this.Field171.Top = 0.7708333F;
			this.Field171.Width = 1.875F;
			// 
			// Field172
			// 
			this.Field172.Height = 0.125F;
			this.Field172.Left = 4.5625F;
			this.Field172.Name = "Field172";
			this.Field172.Style = "font-size: 7pt; text-align: left";
			this.Field172.Text = "(must match MVR Page 1, line 14f)";
			this.Field172.Top = 1.020833F;
			this.Field172.Width = 2.125F;
			// 
			// Field173
			// 
			this.Field173.Height = 0.125F;
			this.Field173.Left = 4.5625F;
			this.Field173.Name = "Field173";
			this.Field173.Style = "font-size: 7pt; text-align: left";
			this.Field173.Text = "(must match MVR Page 2, line 15c)";
			this.Field173.Top = 1.59375F;
			this.Field173.Width = 1.9375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle,
            this.Field126,
            this.txtMunicipality,
            this.Field161});
			this.ReportHeader.Height = 0.5625F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1875F;
			this.txtTitle.Left = 0.125F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "MUNICIPAL TAX RATE CALCULATION FORM";
			this.txtTitle.Top = 0.03125F;
			this.txtTitle.Width = 7.1875F;
			// 
			// Field126
			// 
			this.Field126.Height = 0.1875F;
			this.Field126.Left = 0.0625F;
			this.Field126.Name = "Field126";
			this.Field126.Style = "font-size: 10pt; font-weight: bold; text-align: center; text-decoration: underlin" +
    "e";
			this.Field126.Text = "BE SURE TO COMPLETE THIS FORM BEFORE FILLING IN THE TAX ASSESSMENT WARRANT";
			this.Field126.Top = 0.375F;
			this.Field126.Width = 7.375F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 2.125F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.1875F;
			this.txtMunicipality.Width = 3.625F;
			// 
			// Field161
			// 
			this.Field161.Height = 0.1875F;
			this.Field161.Left = 0.7604167F;
			this.Field161.Name = "Field161";
			this.Field161.Style = "font-style: italic; text-align: right";
			this.Field161.Text = "Municipality:";
			this.Field161.Top = 0.1875F;
			this.Field161.Width = 1.302083F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// textBox1
			// 
			this.textBox1.Height = 0.125F;
			this.textBox1.Left = 4.563F;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "font-size: 7pt; text-align: left";
			this.textBox1.Text = "(must match MVR Page 2, line 16c +16d)";
			this.textBox1.Top = 3.29F;
			this.textBox1.Width = 1.9375F;
			// 
			// textBox2
			// 
			this.textBox2.Height = 0.19F;
			this.textBox2.Left = 0.281F;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "font-size: 7pt; text-align: right";
			this.textBox2.Text = "(Amount from line 15)";
			this.textBox2.Top = 5.687F;
			this.textBox2.Width = 1.4375F;
			// 
			// textBox3
			// 
			this.textBox3.Height = 0.19F;
			this.textBox3.Left = 0.281F;
			this.textBox3.Name = "textBox3";
			this.textBox3.Style = "font-size: 7pt; text-align: right";
			this.textBox3.Text = "(Amount from line 15)";
			this.textBox3.Top = 6F;
			this.textBox3.Width = 1.4375F;
			// 
			// textBox4
			// 
			this.textBox4.Height = 0.19F;
			this.textBox4.Left = 0.281F;
			this.textBox4.Name = "textBox4";
			this.textBox4.Style = "font-size: 7pt; text-align: right";
			this.textBox4.Text = "(Amount from line 16)";
			this.textBox4.Top = 6.315F;
			this.textBox4.Width = 1.4375F;
			// 
			// textBox5
			// 
			this.textBox5.Height = 0.19F;
			this.textBox5.Left = 0.2810001F;
			this.textBox5.Name = "textBox5";
			this.textBox5.Style = "font-size: 7pt; text-align: right";
			this.textBox5.Text = "(Amount from line 3)";
			this.textBox5.Top = 6.625F;
			this.textBox5.Width = 1.4375F;
			// 
			// textBox6
			// 
			this.textBox6.Height = 0.19F;
			this.textBox6.Left = 0.2810001F;
			this.textBox6.Name = "textBox6";
			this.textBox6.Style = "font-size: 7pt; text-align: right";
			this.textBox6.Text = "(Amount from line 15)";
			this.textBox6.Top = 6.937F;
			this.textBox6.Width = 1.4375F;
			// 
			// textBox7
			// 
			this.textBox7.Height = 0.19F;
			this.textBox7.Left = 0.2810001F;
			this.textBox7.Name = "textBox7";
			this.textBox7.Style = "font-size: 7pt; text-align: right";
			this.textBox7.Text = "(Amount from line 4b)";
			this.textBox7.Top = 7.25F;
			this.textBox7.Width = 1.4375F;
			// 
			// textBox8
			// 
			this.textBox8.Height = 0.19F;
			this.textBox8.Left = 0.281F;
			this.textBox8.Name = "textBox8";
			this.textBox8.Style = "font-size: 7pt; text-align: right";
			this.textBox8.Text = "(Amount from line 5b)";
			this.textBox8.Top = 7.583F;
			this.textBox8.Width = 1.438F;
			// 
			// textBox9
			// 
			this.textBox9.Height = 0.19F;
			this.textBox9.Left = 2.383F;
			this.textBox9.Name = "textBox9";
			this.textBox9.Style = "font-size: 7pt; text-align: right";
			this.textBox9.Text = "(Amount from line 6)";
			this.textBox9.Top = 6F;
			this.textBox9.Width = 1.4375F;
			// 
			// textBox10
			// 
			this.textBox10.Height = 0.19F;
			this.textBox10.Left = 2.383F;
			this.textBox10.Name = "textBox10";
			this.textBox10.Style = "font-size: 7pt; text-align: right";
			this.textBox10.Text = "(Amount from line 6)";
			this.textBox10.Top = 6.315F;
			this.textBox10.Width = 1.4375F;
			// 
			// textBox11
			// 
			this.textBox11.Height = 0.19F;
			this.textBox11.Left = 2.383F;
			this.textBox11.Name = "textBox11";
			this.textBox11.Style = "font-size: 7pt; text-align: right";
			this.textBox11.Text = "(Amount from line 15)";
			this.textBox11.Top = 7.969F;
			this.textBox11.Width = 1.4375F;
			// 
			// rptTaxRateForm
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountyTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalAppropriation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTIF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSchool)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAppropriations)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRealEstate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPersonalProperty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRevenueSharing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherRevenue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetToBeRaised)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt19Plus21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomesteadVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMinRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxCommitment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaxOverlay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverlay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl5btext)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field138)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field139)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field140)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field143)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field144)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field146)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field147)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field148)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field149)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field150)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field151)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field152)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field153)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field154)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field155)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field156)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field157)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field158)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field159)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field160)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field162)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field163)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field164)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEReimburseValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field166)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field167)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field168)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field169)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field170)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field171)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field172)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field173)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field161)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountyTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipalAppropriation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSchool;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAppropriations;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRealEstate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPersonalProperty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBase;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRevenueSharing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherRevenue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field48;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetToBeRaised;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field54;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt19Plus21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field56;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNet1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field60;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBase2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field62;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field63;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMaxTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field66;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field67;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field68;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field70;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field71;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field72;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field73;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBase1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field75;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field76;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field77;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field78;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field79;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field80;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field81;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field82;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNet2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field84;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMaxTax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field86;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field88;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNet3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field90;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHomesteadVal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxRate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNet4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field95;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field96;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field97;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field98;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field99;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field100;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field101;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field104;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxCommitment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field106;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMaxOverlay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field108;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field110;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverlay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field111;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field112;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field114;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field118;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field119;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field120;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field121;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field123;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field124;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field127;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field128;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHalfHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field130;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field131;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field132;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field133;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field135;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lbl5btext;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field137;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field138;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field139;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field140;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEVal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxRate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field143;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field144;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field146;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field147;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field148;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field149;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field150;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field151;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field152;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field153;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field154;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field155;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field156;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field157;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field158;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field159;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field160;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field162;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field163;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field164;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEReimburseValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field166;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field167;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field168;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field169;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field170;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field171;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field172;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field173;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field126;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field161;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
	}
}
