﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for srptMeterDetail.
	/// </summary>
	public partial class srptUTMeterDetail : FCSectionReport
	{
		// nObj = 1
		//   0	srptUTMeterDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/11/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/06/2006              *
		// ********************************************************
		int lngMeterKey;
		clsDRWrapper rsData = new clsDRWrapper();
		int lngBillCount;
		double[] dblTotalSumAmount = new double[7 + 1];

		public srptUTMeterDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Billing History";
            this.ReportEnd += SrptUTMeterDetail_ReportEnd;
		}

        private void SrptUTMeterDetail_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static srptUTMeterDetail InstancePtr
		{
			get
			{
				return (srptUTMeterDetail)Sys.GetInstance(typeof(srptUTMeterDetail));
			}
		}

		protected srptUTMeterDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				lngMeterKey = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
				// force this report to be landscape
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				// this loads the data
				if (LoadAccounts())
				{
					// rock on
				}
				else
				{
					Cancel();
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Sub Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			FillTotals();
		}

		private bool LoadAccounts()
		{
			bool LoadAccounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				rsData.OpenRecordset("SELECT * FROM Bill WHERE MeterKey = " + FCConvert.ToString(lngMeterKey) + " ORDER BY ID desc", modExtraModules.strUTDatabase);
				if (!rsData.EndOfFile())
				{
					LoadAccounts = true;
				}
				else
				{
					LoadAccounts = false;
				}
				return LoadAccounts;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadAccounts;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRate = new clsDRWrapper();
				double dblAmount = 0;
				double dblWCurInt;
				double dblWCHGInt;
				double dblSCurInt;
				double dblSCHGInt;
				double dblTotal;
				fldRegularS.Text = "";
				fldMiscS.Text = "";
				fldTaxS.Text = "";
				fldAdjS.Text = "";
				fldServiceS.Text = "";
				fldRegularS.Visible = false;
				fldMiscS.Visible = false;
				fldTaxS.Visible = false;
				fldAdjS.Visible = false;
				fldServiceS.Visible = false;
				fldAmountS.Visible = false;
				if (!rsData.EndOfFile())
				{
					fldReading.Text = FCConvert.ToString(rsData.Get_Fields_Int32("CurReading"));
					rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsData.Get_Fields_Int32("BillingRateKey"), modExtraModules.strUTDatabase);
					if (!rsRate.EndOfFile())
					{
						fldBillDate.Text = Strings.Format(rsRate.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
					}
					else
					{
						fldBillDate.Text = "No RK";
					}
					if (Conversion.Val(rsData.Get_Fields_Int32("CurReading") - rsData.Get_Fields_Int32("PrevReading")) >= 0)
					{
						fldActualCons.Text = FCConvert.ToString(rsData.Get_Fields_Int32("CurReading") - rsData.Get_Fields_Int32("PrevReading"));
					}
					else
					{
						fldActualCons.Text = "Lower Current";
					}
					fldCons.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Consumption"));
					fldStatus.Text = Strings.Format(rsData.Get_Fields_String("BillStatus"), "#,##0.00");
					string vbPorterVar = FCConvert.ToString(rsData.Get_Fields_String("Service"));
					if (vbPorterVar == "B")
					{
						fldRegularW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WUnitsAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WConsumptionAmount")), "#,##0.00");
						fldMiscW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WMiscAmount")), "#,##0.00");
						// TODO: Check the table for the column [WTax] and replace with corresponding Get_Field method
						fldTaxW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields("WTax")), "#,##0.00");
						fldAdjW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WAdjustAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WDEAdjustAmount")), "#,##0.00");
						// remove the totals from this bill and the interest amounts
						fldServiceW.Text = "W";
						fldRegularS.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("SFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SUnitsAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SConsumptionAmount")), "#,##0.00");
						fldMiscS.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("SMiscAmount")), "#,##0.00");
						// TODO: Check the table for the column [STax] and replace with corresponding Get_Field method
						fldTaxS.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields("STax")), "#,##0.00");
						fldAdjS.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("SAdjustAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SDEAdjustAmount")), "#,##0.00");
						// remove the totals from this bill and the interest amounts
						fldServiceS.Text = "S";
						fldAmountW.Text = Strings.Format(FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text), "#,##0.00");
						fldAmountS.Text = Strings.Format(FCConvert.ToDouble(fldRegularS.Text) + FCConvert.ToDouble(fldTaxS.Text) + FCConvert.ToDouble(fldMiscS.Text) + FCConvert.ToDouble(fldAdjS.Text), "#,##0.00");
						dblAmount = FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text) + FCConvert.ToDouble(fldRegularS.Text) + FCConvert.ToDouble(fldTaxS.Text) + FCConvert.ToDouble(fldMiscS.Text) + FCConvert.ToDouble(fldAdjS.Text);
						fldRegularS.Visible = true;
						fldMiscS.Visible = true;
						fldTaxS.Visible = true;
						fldAdjS.Visible = true;
						fldServiceS.Visible = true;
						fldAmountS.Visible = true;
						this.Detail.Height = 540 / 1440f;
					}
					else if (vbPorterVar == "W")
					{
						fldRegularW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WUnitsAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WConsumptionAmount")), "#,##0.00");
						fldMiscW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WMiscAmount")), "#,##0.00");
						// TODO: Check the table for the column [WTax] and replace with corresponding Get_Field method
						fldTaxW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields("WTax")), "#,##0.00");
						// MAL@20080212: Tracker Reference: 12296
						// fldAdjW.Text = Format(CDbl(rsData.Fields("WAdjustAmount")), "#,##0.00")   'remove the totals from this bill and the interest amounts
						fldAdjW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WAdjustAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WDEAdjustAmount")), "#,##0.00");
						// remove the totals from this bill and the interest amounts
						fldAmountW.Text = Strings.Format(FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text), "#,##0.00");
						dblAmount = FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text);
						fldServiceW.Text = "W";
						this.Detail.Height = 270 / 1440f;
					}
					else if (vbPorterVar == "S")
					{
						fldRegularW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("SFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SUnitsAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SConsumptionAmount")), "#,##0.00");
						fldMiscW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("SMiscAmount")), "#,##0.00");
						// TODO: Check the table for the column [STax] and replace with corresponding Get_Field method
						fldTaxW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields("STax")), "#,##0.00");
						// MAL@20080212: Added check for DE adjustments
						// Tracker Reference: 12296
						// fldAdjW.Text = Format(CDbl(rsData.Fields("SAdjustAmount")), "#,##0.00")   'remove the totals from this bill and the interest amounts
						fldAdjW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("SAdjustAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SDEAdjustAmount")), "#,##0.00");
						// remove the totals from this bill and the interest amounts
						fldAmountW.Text = Strings.Format(FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text), "#,##0.00");
						dblAmount = FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text);
						fldServiceW.Text = "S";
						this.Detail.Height = 270 / 1440f;
					}
					if (dblAmount == 0)
					{
						if (rsData.Get_Fields_Double("WPrinOwed") + rsData.Get_Fields_Double("SPrinOwed") != 0)
						{
							// this is a loadback
							if (FCConvert.ToString(rsData.Get_Fields_String("Service")) == "B")
							{
								fldRegularW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Double("WPrinOwed")), "#,##0.00");
								fldTaxW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Double("WTaxOwed")), "#,##0.00");
								fldRegularS.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Double("SPrinOwed")), "#,##0.00");
								fldTaxS.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Double("STaxOwed")), "#,##0.00");
								fldAmountW.Text = Strings.Format(FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text), "#,##0.00");
								fldAmountS.Text = Strings.Format(FCConvert.ToDouble(fldRegularS.Text) + FCConvert.ToDouble(fldTaxS.Text) + FCConvert.ToDouble(fldMiscS.Text) + FCConvert.ToDouble(fldAdjS.Text), "#,##0.00");
							}
							else if (rsData.Get_Fields_String("Service") == "W")
							{
								fldRegularW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Double("WPrinOwed")), "#,##0.00");
								fldTaxW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Double("WTaxOwed")), "#,##0.00");
								fldAmountW.Text = Strings.Format(FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text), "#,##0.00");
							}
							else
							{
								fldRegularW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("SFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Double("SPrinOwed")), "#,##0.00");
								fldTaxW.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Double("STaxOwed")), "#,##0.00");
								fldAmountW.Text = Strings.Format(FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldAdjW.Text), "#,##0.00");
							}
						}
					}
					if (FCConvert.ToString(rsData.Get_Fields_String("Service")) == "B")
					{
						// fill the local (group) totals
						if (fldActualCons.Text != "Lower Current")
						{
							dblTotalSumAmount[7] += Conversion.Val(fldActualCons.Text);
						}
						dblTotalSumAmount[0] += Conversion.Val(fldCons.Text);
						dblTotalSumAmount[1] += FCConvert.ToDouble(fldRegularW.Text) + FCConvert.ToDouble(fldRegularS.Text);
						dblTotalSumAmount[2] += FCConvert.ToDouble(fldMiscW.Text) + FCConvert.ToDouble(fldMiscS.Text);
						dblTotalSumAmount[3] += FCConvert.ToDouble(fldTaxW.Text) + FCConvert.ToDouble(fldTaxS.Text);
						dblTotalSumAmount[4] += FCConvert.ToDouble(fldAdjW.Text) + FCConvert.ToDouble(fldAdjS.Text);
						dblTotalSumAmount[6] += FCConvert.ToDouble(fldAmountW.Text) + FCConvert.ToDouble(fldAmountS.Text);
					}
					else
					{
						if (fldActualCons.Text != "Lower Current")
						{
							dblTotalSumAmount[7] += Conversion.Val(fldActualCons.Text);
						}
						dblTotalSumAmount[0] += Conversion.Val(fldCons.Text);
						dblTotalSumAmount[1] += FCConvert.ToDouble(fldRegularW.Text);
						dblTotalSumAmount[2] += FCConvert.ToDouble(fldMiscW.Text);
						dblTotalSumAmount[3] += FCConvert.ToDouble(fldTaxW.Text);
						dblTotalSumAmount[4] += FCConvert.ToDouble(fldAdjW.Text);
						dblTotalSumAmount[6] += FCConvert.ToDouble(fldAmountW.Text);
					}
					lngBillCount += 1;
					rsData.MoveNext();
				}
				rsRate.DisposeOf();
                return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fldTotalActualCons.Text = Strings.Format(dblTotalSumAmount[7], "#,##0");
				fldTotalCons.Text = Strings.Format(dblTotalSumAmount[0], "#,##0");
				fldTotalRegular.Text = Strings.Format(dblTotalSumAmount[1], "#,##0.00");
				fldTotalMisc.Text = Strings.Format(dblTotalSumAmount[2], "#,##0.00");
				fldTotalTax.Text = Strings.Format(dblTotalSumAmount[3], "#,##0.00");
				fldTotalAdj.Text = Strings.Format(dblTotalSumAmount[4], "#,##0.00");
				fldTotalAmount.Text = Strings.Format(dblTotalSumAmount[6], "#,##0.00");
				if (lngBillCount == 1)
				{
					fldFooter.Text = "\r\n" + "1 bill";
				}
				else
				{
					fldFooter.Text = FCConvert.ToString(lngBillCount) + " bills";
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Totals", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void srptUTMeterDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptUTMeterDetail.Caption	= "Billing History";
			//srptUTMeterDetail.Icon	= "srptMeterDetail.dsx":0000";
			//srptUTMeterDetail.Left	= 0;
			//srptUTMeterDetail.Top	= 0;
			//srptUTMeterDetail.Width	= 11880;
			//srptUTMeterDetail.Height	= 8010;
			//srptUTMeterDetail.WindowState	= 2;
			//srptUTMeterDetail.SectionData	= "srptMeterDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
