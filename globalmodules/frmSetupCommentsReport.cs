﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmSetupCommentsReport.
	/// </summary>
	public partial class frmSetupCommentsReport : BaseForm
	{
		public frmSetupCommentsReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            //FC:FINAL:BSE:#4095 allow only numeric input in textbox 
            txtStartAcct.AllowOnlyNumericInput(true);
            txtEndAcct.AllowOnlyNumericInput(true);
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupCommentsReport InstancePtr
		{
			get
			{
				return (frmSetupCommentsReport)Sys.GetInstance(typeof(frmSetupCommentsReport));
			}
		}

		protected frmSetupCommentsReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation                  *
		// *
		// WRITTEN BY     :   Kevin Kelly                         *
		// MODIFIED BY    :                                       *
		// DATE MODIFIED  :   07/29/2016                          *
		// *
		// Allows the user to pick date and account ranges        *
		// and order for the Comments report                      *
		// ********************************************************
		DateTime dtDate1;
		DateTime dtDate2;
		// vbPorter upgrade warning: lngAcct1 As int	OnWrite(short, string)
		int lngAcct1;
		// vbPorter upgrade warning: lngAcct2 As int	OnWrite(short, string)
		int lngAcct2;
		bool boolRunRpt;
		// kk06192017 trout-1252  Change so report is canceled if form is just exited
		public bool Init(ref DateTime dtStartDate, ref DateTime dtEndDate, ref int lngStartAcct, ref int lngEndAcct)
		{
			bool Init = false;
			this.Show(FormShowEnum.Modal, App.MainForm);
			dtStartDate = dtDate1;
			dtEndDate = dtDate2;
			lngStartAcct = lngAcct1;
			lngEndAcct = lngAcct2;
			Init = boolRunRpt;
			return Init;
		}

		private void frmSetupCommentsReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmSetupCommentsReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupCommentsReport.FillStyle	= 0;
			//frmSetupCommentsReport.ScaleWidth	= 6690;
			//frmSetupCommentsReport.ScaleHeight	= 3390;
			//frmSetupCommentsReport.LinkTopic	= "Form2";
			//frmSetupCommentsReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strTemp = "";
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			boolRunRpt = false;
			dtDate1 = DateAndTime.DateValue(FCConvert.ToString(0));
			dtDate2 = DateAndTime.DateValue(FCConvert.ToString(0));
			lngAcct1 = 0;
			lngAcct2 = 0;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (ValidateChoices())
			{
				if (txtStartDate.Text != "")
				{
					dtDate1 = DateAndTime.DateValue(txtStartDate.Text);
					dtDate2 = DateAndTime.DateValue(txtEndDate.Text);
				}
				if (txtStartAcct.Text != "")
				{
					lngAcct1 = FCConvert.ToInt32(txtStartAcct.Text);
					lngAcct2 = FCConvert.ToInt32(txtEndAcct.Text);
				}
				boolRunRpt = true;
				Close();
			}
		}

		private bool ValidateChoices()
		{
			bool ValidateChoices = false;
			ValidateChoices = true;
            //FC:FINAL:AM:#4096 - check if the dates are empty
            //if (txtStartDate.Text != "" || txtEndDate.Text != "" || txtStartAcct.Text != "" || txtEndAcct.Text != "")
            if (!txtStartDate.IsEmpty || !txtEndDate.IsEmpty || txtStartAcct.Text != "" || txtEndAcct.Text != "")
            {
				// Make sure the date range is ok
				if (Strings.Trim(txtStartDate.Text) != "")
				{
					if (!Information.IsDate(txtStartDate.Text))
					{
						ValidateChoices = false;
						MessageBox.Show("Start date is not a valid date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtStartDate.Focus();
					}
					else
					{
						if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateTime.Today.ToOADate())
						{
							ValidateChoices = false;
							MessageBox.Show("Start date cannot be in the future.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtStartDate.Focus();
						}
						else
						{
							if (Strings.Trim(txtEndDate.Text) != "")
							{
								if (!Information.IsDate(txtEndDate.Text))
								{
									ValidateChoices = false;
									MessageBox.Show("Start date is not a valid date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									txtEndDate.Focus();
								}
								else
								{
									if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
									{
										ValidateChoices = false;
										MessageBox.Show("Start date cannot be later then end date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										txtEndDate.Focus();
									}
								}
							}
							else
							{
								txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
							}
						}
					}
				}
				else
				{
					if (Strings.Trim(txtEndDate.Text) != "")
					{
						ValidateChoices = false;
						MessageBox.Show("You must provide a valid start date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtStartDate.Focus();
					}
				}
				if (ValidateChoices)
				{
					// Make sure the account range is ok
					if (Strings.Trim(txtStartAcct.Text) != "")
					{
						if (Strings.Trim(txtEndAcct.Text) != "")
						{
							if (Conversion.Val(txtStartAcct.Text) > Conversion.Val(txtEndAcct.Text))
							{
								ValidateChoices = false;
								MessageBox.Show("Start account number cannot be greater than end account number.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								txtEndAcct.Focus();
							}
							else
							{
								if (Conversion.Val(txtStartAcct.Text) > 2147483647)
								{
									txtStartAcct.Text = FCConvert.ToString(2147483647);
								}
								if (Conversion.Val(txtEndAcct.Text) > 2147483647)
								{
									txtEndAcct.Text = "2147483647";
								}
							}
						}
						else
						{
							txtEndAcct.Text = "2147483647";
						}
					}
					else
					{
						if (Strings.Trim(txtEndAcct.Text) != "")
						{
							ValidateChoices = false;
							MessageBox.Show("You must provide a valid start account number.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtStartAcct.Focus();
						}
					}
				}
			}
			return ValidateChoices;
		}

		private void txtStartAcct_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// account so only numbers allowed
			if (KeyAscii == Keys.Back)
			{
				// backspace
			}
			else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
				// 0 - 9
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtEndAcct_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// account so only numbers allowed
			if (KeyAscii == Keys.Back)
			{
				// backspace
			}
			else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
				// 0 - 9
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// Private Sub txtStartDate_KeyPress(KeyAscii As Integer)
		// date so only numbers, / or - allowed
		// Select Case KeyAscii
		// Case 8              ' backspace
		// Case 45, 47         ' / and -
		// Case 48 To 57       ' 0 - 9
		// Case Else
		// KeyAscii = 0
		// End Select
		// End Sub
		//
		// Private Sub txtEndDate_KeyPress(KeyAscii As Integer)
		// date so only numbers, / or - allowed
		// Select Case KeyAscii
		// Case 8              ' backspace
		// Case 45, 47         ' / and -
		// Case 48 To 57       ' 0 - 9
		// Case Else
		// KeyAscii = 0
		// End Select
		// End Sub
	}
}
