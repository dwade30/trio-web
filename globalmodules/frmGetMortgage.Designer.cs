﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGetMortgage.
	/// </summary>
	partial class frmGetMortgage : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAccount;
		public fecherFoundation.FCComboBox cmbSearchBy;
		public fecherFoundation.FCComboBox cmbContain;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblAccountNumber;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileMortgageHolderListing;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsMHwithRE;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsMHwithREAmounts;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsMHwithREAmountsSplit;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsREwithMH;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsMHwithUT;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsMHwithUTAmounts;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsUTwithMH;
		public fecherFoundation.FCToolStripMenuItem mnuFileLabels;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetMortgage));
			this.cmbAccount = new fecherFoundation.FCComboBox();
			this.cmbSearchBy = new fecherFoundation.FCComboBox();
			this.cmbContain = new fecherFoundation.FCComboBox();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.lblContain = new fecherFoundation.FCLabel();
			this.lblSearchBy = new fecherFoundation.FCLabel();
			this.lblAccountNumber = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFileMortgageHolderListing = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsMHwithRE = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsMHwithREAmounts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsMHwithREAmountsSplit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsREwithMH = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsMHwithUT = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsMHwithUTAmounts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsUTwithMH = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileLabels = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.lblSelect = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdNewMortgageHolder = new fecherFoundation.FCButton();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGetAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNewMortgageHolder = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewMortgageHolder)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 556);
			this.BottomPanel.Size = new System.Drawing.Size(576, 10);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnProcess);
			this.ClientArea.Controls.Add(this.lblSelect);
			this.ClientArea.Controls.Add(this.cmbAccount);
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.lblAccountNumber);
			this.ClientArea.Size = new System.Drawing.Size(576, 496);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNewMortgageHolder);
			this.TopPanel.Size = new System.Drawing.Size(576, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNewMortgageHolder, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(326, 30);
			this.HeaderText.Text = "Mortgage Holder Information";
			// 
			// cmbAccount
			// 
			this.cmbAccount.AutoSize = false;
			this.cmbAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAccount.FormattingEnabled = true;
			this.cmbAccount.Items.AddRange(new object[] {
				"Mortgage Holder Number",
				"Real Estate Account",
				"Utility Account"
			});
			this.cmbAccount.Location = new System.Drawing.Point(106, 30);
			this.cmbAccount.Name = "cmbAccount";
			this.cmbAccount.Size = new System.Drawing.Size(230, 40);
			this.cmbAccount.TabIndex = 1;
			this.cmbAccount.Text = "Mortgage Holder Number";
			this.cmbAccount.SelectedIndexChanged += new System.EventHandler(this.optAccount_Click);
			// 
			// cmbSearchBy
			// 
			this.cmbSearchBy.AutoSize = false;
			this.cmbSearchBy.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSearchBy.FormattingEnabled = true;
			this.cmbSearchBy.Items.AddRange(new object[] {
				"Name"
			});
			this.cmbSearchBy.Location = new System.Drawing.Point(232, 30);
			this.cmbSearchBy.Name = "cmbSearchBy";
			this.cmbSearchBy.Size = new System.Drawing.Size(207, 40);
			this.cmbSearchBy.TabIndex = 1;
			this.cmbSearchBy.Text = "Name";
			// 
			// cmbContain
			// 
			this.cmbContain.AutoSize = false;
			this.cmbContain.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbContain.FormattingEnabled = true;
			this.cmbContain.Items.AddRange(new object[] {
				"Contain Search",
				"Start With Search",
				"Ends With Search"
			});
			this.cmbContain.Location = new System.Drawing.Point(232, 86);
			this.cmbContain.Name = "cmbContain";
			this.cmbContain.Size = new System.Drawing.Size(207, 40);
			this.cmbContain.TabIndex = 3;
			this.cmbContain.Text = "Start With Search";
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.LinkItem = null;
			this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAccount.LinkTopic = null;
			this.txtAccount.Location = new System.Drawing.Point(106, 115);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(230, 40);
			this.txtAccount.TabIndex = 3;
			this.txtAccount.Enter += new System.EventHandler(this.txtAccount_Enter);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.fcLabel1);
			this.Frame2.Controls.Add(this.txtSearch);
			this.Frame2.Controls.Add(this.lblContain);
			this.Frame2.Controls.Add(this.lblSearchBy);
			this.Frame2.Controls.Add(this.cmbContain);
			this.Frame2.Controls.Add(this.cmbSearchBy);
			this.Frame2.Location = new System.Drawing.Point(30, 190);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(459, 197);
			this.Frame2.TabIndex = 4;
			this.Frame2.Text = "Search By";
			this.Frame2.UseMnemonic = false;
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(20, 147);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(160, 15);
			this.fcLabel1.TabIndex = 4;
			this.fcLabel1.Text = "CRITERIA TO SEARCH BY";
			this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSearch
			// 
			this.txtSearch.AutoSize = false;
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.LinkItem = null;
			this.txtSearch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSearch.LinkTopic = null;
			this.txtSearch.Location = new System.Drawing.Point(232, 141);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(207, 40);
			this.txtSearch.TabIndex = 5;
			this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
			// 
			// lblContain
			// 
			this.lblContain.AutoSize = true;
			this.lblContain.Location = new System.Drawing.Point(20, 92);
			this.lblContain.Name = "lblContain";
			this.lblContain.Size = new System.Drawing.Size(162, 15);
			this.lblContain.TabIndex = 2;
			this.lblContain.Text = "FIND ALL RECORDS THAT";
			this.lblContain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSearchBy
			// 
			this.lblSearchBy.AutoSize = true;
			this.lblSearchBy.Location = new System.Drawing.Point(20, 44);
			this.lblSearchBy.Name = "lblSearchBy";
			this.lblSearchBy.Size = new System.Drawing.Size(134, 15);
			this.lblSearchBy.TabIndex = 0;
			this.lblSearchBy.Text = "MORTGAGE HOLDER";
			this.lblSearchBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.AutoSize = true;
			this.lblAccountNumber.Location = new System.Drawing.Point(30, 89);
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Size = new System.Drawing.Size(411, 15);
			this.lblAccountNumber.TabIndex = 2;
			this.lblAccountNumber.Text = "ENTER THE ACCOUNT NUMBER AND PRESS \'ENTER\' TO CONTINUE";
			this.lblAccountNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
                this.mnuFileMortgageHolderListing,
                this.mnuFileReportsMHwithRE,
                this.mnuFileReportsMHwithREAmounts,
                this.mnuFileReportsMHwithREAmountsSplit,
                this.mnuFileReportsREwithMH,
                this.mnuFileReportsMHwithUT,
                this.mnuFileReportsMHwithUTAmounts,
                this.mnuFileReportsUTwithMH,
                this.mnuFileLabels
            });
			this.MainMenu1.Name = null;
			// 
			// mnuFileMortgageHolderListing
			// 
			this.mnuFileMortgageHolderListing.Index = 0;
			this.mnuFileMortgageHolderListing.Name = "mnuFileMortgageHolderListing";
			this.mnuFileMortgageHolderListing.Text = "Mortgage Holder Master Listing";
			this.mnuFileMortgageHolderListing.Click += new System.EventHandler(this.mnuFileMortgageHolderListing_Click);
			// 
			// mnuFileReportsMHwithRE
			// 
			this.mnuFileReportsMHwithRE.Index = 1;
			this.mnuFileReportsMHwithRE.Name = "mnuFileReportsMHwithRE";
			this.mnuFileReportsMHwithRE.Text = "Mortgage Holders w/RE Accounts";
			this.mnuFileReportsMHwithRE.Click += new System.EventHandler(this.mnuFileReportsMHwithRE_Click);
			// 
			// mnuFileReportsMHwithREAmounts
			// 
			this.mnuFileReportsMHwithREAmounts.Index = 2;
			this.mnuFileReportsMHwithREAmounts.Name = "mnuFileReportsMHwithREAmounts";
			this.mnuFileReportsMHwithREAmounts.Text = "Mortgage Holders w/RE Amounts";
			this.mnuFileReportsMHwithREAmounts.Click += new System.EventHandler(this.mnuFileReportsMHwithREAmounts_Click);
			// 
			// mnuFileReportsMHwithREAmountsSplit
			// 
			this.mnuFileReportsMHwithREAmountsSplit.Index = 3;
			this.mnuFileReportsMHwithREAmountsSplit.Name = "mnuFileReportsMHwithREAmountsSplit";
			this.mnuFileReportsMHwithREAmountsSplit.Text = "Mortgage Holders w/RE Amounts (Split)";
			this.mnuFileReportsMHwithREAmountsSplit.Click += new System.EventHandler(this.mnuFileReportsMHwithREAmountsSplit_Click);
			// 
			// mnuFileReportsREwithMH
			// 
			this.mnuFileReportsREwithMH.Index = 4;
			this.mnuFileReportsREwithMH.Name = "mnuFileReportsREwithMH";
			this.mnuFileReportsREwithMH.Text = "RE Accounts w/Mortgage Holders";
			this.mnuFileReportsREwithMH.Click += new System.EventHandler(this.mnuFileReportsREwithMH_Click);
			// 
			// mnuFileReportsMHwithUT
			// 
			this.mnuFileReportsMHwithUT.Index = 5;
			this.mnuFileReportsMHwithUT.Name = "mnuFileReportsMHwithUT";
			this.mnuFileReportsMHwithUT.Text = "Mortgage Holders w/UT Accounts";
			this.mnuFileReportsMHwithUT.Click += new System.EventHandler(this.mnuFileReportsMHwithUT_Click);
			// 
			// mnuFileReportsMHwithUTAmounts
			// 
			this.mnuFileReportsMHwithUTAmounts.Index = 6;
			this.mnuFileReportsMHwithUTAmounts.Name = "mnuFileReportsMHwithUTAmounts";
			this.mnuFileReportsMHwithUTAmounts.Text = "Mortgage Holders w/UT Amounts";
			this.mnuFileReportsMHwithUTAmounts.Click += new System.EventHandler(this.mnuFileReportsMHwithUTAmounts_Click);
			// 
			// mnuFileReportsUTwithMH
			// 
			this.mnuFileReportsUTwithMH.Index = 7;
			this.mnuFileReportsUTwithMH.Name = "mnuFileReportsUTwithMH";
			this.mnuFileReportsUTwithMH.Text = "UT Accounts w/Mortgage Holders";
			this.mnuFileReportsUTwithMH.Click += new System.EventHandler(this.mnuFileReportsUTwithMH_Click);
			// 
			// mnuFileLabels
			// 
			this.mnuFileLabels.Index = 8;
			this.mnuFileLabels.Name = "mnuFileLabels";
			this.mnuFileLabels.Text = "Mortgage Holder Labels";
			this.mnuFileLabels.Click += new System.EventHandler(this.mnuFileLabels_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// lblSelect
			// 
			this.lblSelect.AutoSize = true;
			this.lblSelect.Location = new System.Drawing.Point(30, 44);
			this.lblSelect.Name = "lblSelect";
			this.lblSelect.Size = new System.Drawing.Size(56, 15);
			this.lblSelect.TabIndex = 0;
			this.lblSelect.Text = "SELECT";
			this.lblSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 425);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdNewMortgageHolder
			// 
			this.cmdNewMortgageHolder.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNewMortgageHolder.AppearanceKey = "toolbarButton";
			this.cmdNewMortgageHolder.Location = new System.Drawing.Point(403, 29);
			this.cmdNewMortgageHolder.Name = "cmdNewMortgageHolder";
			this.cmdNewMortgageHolder.Shortcut = Wisej.Web.Shortcut.CtrlShiftN;
			this.cmdNewMortgageHolder.Size = new System.Drawing.Size(155, 24);
			this.cmdNewMortgageHolder.TabIndex = 52;
			this.cmdNewMortgageHolder.Text = "New Mortgage Holder";
			this.cmdNewMortgageHolder.Click += new System.EventHandler(this.cmdNewMortgageHolder_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = -1;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = -1;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuGetAccount
			// 
			this.mnuGetAccount.Index = -1;
			this.mnuGetAccount.Name = "mnuGetAccount";
			this.mnuGetAccount.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuGetAccount.Text = "Process";
			this.mnuGetAccount.Click += new System.EventHandler(this.mnuGetAccount_Click);
			// 
			// mnuSeperator2
			// 
			this.mnuSeperator2.Index = -1;
			this.mnuSeperator2.Name = "mnuSeperator2";
			this.mnuSeperator2.Text = "-";
			// 
			// mnuNewMortgageHolder
			// 
			this.mnuNewMortgageHolder.Index = -1;
			this.mnuNewMortgageHolder.Name = "mnuNewMortgageHolder";
			this.mnuNewMortgageHolder.Shortcut = Wisej.Web.Shortcut.CtrlShiftN;
			this.mnuNewMortgageHolder.Text = "New Mortgage Holder";
			this.mnuNewMortgageHolder.Click += new System.EventHandler(this.mnuNewMortgageHolder_Click);
			// 
			// frmGetMortgage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(576, 566);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmGetMortgage";
			this.ShowInTaskbar = false;
			this.Text = "Mortgage Holder Information";
			this.Load += new System.EventHandler(this.frmGetMortgage_Load);
			this.Activated += new System.EventHandler(this.frmGetMortgage_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetMortgage_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetMortgage_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewMortgageHolder)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel lblContain;
		private FCLabel lblSearchBy;
		private FCLabel lblSelect;
		private FCLabel fcLabel1;
		private FCButton btnProcess;
		public FCButton cmdNewMortgageHolder;
		public FCToolStripMenuItem mnuFileSeperator2;
		public FCToolStripMenuItem mnuExit;
		public FCToolStripMenuItem mnuSeperator;
		public FCToolStripMenuItem mnuGetAccount;
		public FCToolStripMenuItem mnuSeperator2;
		public FCToolStripMenuItem mnuNewMortgageHolder;
	}
}
