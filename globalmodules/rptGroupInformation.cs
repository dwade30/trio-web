﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#else
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptGroupInformation : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/19/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/19/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsMaster = new clsDRWrapper();
		//clsDRWrapper rsCL = new clsDRWrapper();
		int lngGroupNumber;
		int lngGroupID;
		string strPrimaryType = "";
		int lngPrimaryAccount;
		double dblTotalBalance;

		public rptGroupInformation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Group Balance";
            this.ReportEnd += RptGroupInformation_ReportEnd;
		}

        private void RptGroupInformation_ReportEnd(object sender, EventArgs e)
        {
			rsData.DisposeOf();
            rsMaster.DisposeOf();

		}

        public static rptGroupInformation InstancePtr
		{
			get
			{
				return (rptGroupInformation)Sys.GetInstance(typeof(rptGroupInformation));
			}
		}

		protected rptGroupInformation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			rsData.Dispose();
			rsMaster.Dispose();
			base.Dispose(disposing);
		}

		public void Init(int lngPassGroupNumber)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			lngGroupNumber = lngPassGroupNumber;
			// kgk trocr302 10-24-2011  Straighten out some confusion over GroupMaster.GroupNumber vs. ModuleAssociation.GroupNumber
			rsTemp.OpenRecordset("SELECT * FROM GroupMaster WHERE GroupMaster.GroupNumber = " + FCConvert.ToString(lngGroupNumber), "CentralData");
			if (rsTemp.RecordCount() > 0)
			{
				lngGroupID = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
			}
			else
			{
				lngGroupID = 0;
			}
			fldHeader.Text = "Group #" + FCConvert.ToString(lngGroupNumber) + " Information";
			frmReportViewer.InstancePtr.Init(this);
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANLDER
				double dblXInt/*unused?*/;
				int lngAccount = 0;
				double dblTotal = 0;
				txtModule.Text = "";
				txtAccount.Text = "";
				txtName.Text = "";
				txtBalance.Text = "";
				if (!rsData.EndOfFile())
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					lngAccount = FCConvert.ToInt32(rsData.Get_Fields("Account"));
					string VBtoVar = rsData.Get_Fields_String("Module");
					if (VBtoVar == "RE")
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsMaster.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(rsData.Get_Fields("Account")), modExtraModules.strREDatabase);
						if (!rsMaster.EndOfFile())
						{
							txtName.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSName")));
						}
						txtModule.Text = "RE";
						txtAccount.Text = lngAccount.ToString();
						dblTotal = modCLCalculations.CalculateAccountTotal(lngAccount, true);
					}
					else if (VBtoVar == "PP")
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsMaster.OpenRecordset("SELECT * FROM PPMaster WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")), modExtraModules.strPPDatabase);
						if (!rsMaster.EndOfFile())
						{
							txtName.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name")));
						}
						txtModule.Text = "PP";
						txtAccount.Text = lngAccount.ToString();
						dblTotal = modCLCalculations.CalculateAccountTotal(lngAccount, false);
					}
					else if (VBtoVar == "UT")
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsMaster.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(rsData.Get_Fields("Account")), modExtraModules.strUTDatabase);
						if (!rsMaster.EndOfFile())
						{
							// TODO: Field [OwnerName] not found!! (maybe it is an alias?)
							txtName.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName")));
						}
						txtModule.Text = "UT";
						txtAccount.Text = lngAccount.ToString();
						dblTotal = modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT(ref lngAccount), true) + modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT(ref lngAccount), false);
					}
					txtBalance.Text = Strings.Format(dblTotal, "$#,##0.00");
					dblTotalBalance += dblTotal;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANLDER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Binding Fields");
			}
		}

		private void GetHeaderInformation()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This will load the header information
				if (strPrimaryType == "RE")
				{
					rsMaster.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngPrimaryAccount), modExtraModules.strREDatabase);
					if (!rsMaster.EndOfFile())
					{
						txtGroupName.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSName")));
						txtAddress1.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSAddr1")));
						txtAddress2.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSAddr2")));
						txtCity.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSAddr3")));
						txtState.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSState")));
						if (FCConvert.ToString(rsMaster.Get_Fields_String("RSZip4")) != "")
						{
							txtZip.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSZip4")));
						}
						else
						{
							txtZip.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("RSZip")));
						}
					}
				}
				else if (strPrimaryType == "PP")
				{
					rsMaster.OpenRecordset("SELECT * FROM PPMaster WHERE Account = " + FCConvert.ToString(lngPrimaryAccount), modExtraModules.strPPDatabase);
					if (!rsMaster.EndOfFile())
					{
						txtGroupName.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name")));
						txtAddress1.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Address1")));
						txtAddress2.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Address2")));
						txtCity.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("City")));
						txtState.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("State")));
						if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Zip4"))) != "")
						{
							txtZip.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Zip"))) + "-" + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Zip4")));
						}
						else
						{
							txtZip.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Zip")));
						}
					}
				}
				else if (strPrimaryType == "UT")
				{
					rsMaster.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngPrimaryAccount), modExtraModules.strUTDatabase);
					if (!rsMaster.EndOfFile())
					{
						// TODO: Field [OwnerName] not found!! (maybe it is an alias?)
						txtGroupName.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName")));
						txtAddress1.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress1")));
						txtAddress2.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress2")));
						txtCity.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OCity")));
						txtState.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OState")));
						if (FCConvert.ToString(rsMaster.Get_Fields_String("OZip4")) != "")
						{
							txtZip.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip4")));
						}
						else
						{
							txtZip.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip")));
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Getting header Information");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			// kgk trocr-302 10-24-2011
			rsData.OpenRecordset("SELECT * FROM ModuleAssociation WHERE GroupNumber = " + FCConvert.ToString(lngGroupID), "CentralData");
			if (rsData.EndOfFile())
			{
				FCMessageBox.Show("No accounts are associated with group #" + FCConvert.ToString(lngGroupNumber) + ".", MsgBoxStyle.Exclamation, "No Associated Accounts");
				Cancel();
			}
			else
			{
				rsData.FindFirstRecord("GroupPrimary", 1);
				if (!rsData.NoMatch)
				{
				}
				else
				{
					rsData.MoveFirst();
				}
				strPrimaryType = FCConvert.ToString(rsData.Get_Fields_String("Module"));
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				lngPrimaryAccount = FCConvert.ToInt32(rsData.Get_Fields("Account"));
				rsData.MoveFirst();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
			rsData.MoveNext();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			GetHeaderInformation();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtGroupBalance.Text = Strings.Format(dblTotalBalance, "$#,##0.00");
		}

		private void rptGroupInformation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptGroupInformation.Caption	= "Group Balance";
			//rptGroupInformation.Icon	= "rptGroupInformation.dsx":0000";
			//rptGroupInformation.Left	= 0;
			//rptGroupInformation.Top	= 0;
			//rptGroupInformation.Width	= 11880;
			//rptGroupInformation.Height	= 8595;
			//rptGroupInformation.StartUpPosition	= 3;
			//rptGroupInformation.SectionData	= "rptGroupInformation.dsx":058A;
			//End Unmaped Properties
		}
	}
}
