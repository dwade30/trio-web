﻿//Fecher vbPorter - Version 1.0.0.40
namespace Global
{
	public class cCryptoUtility
	{
		//=========================================================
		Chilkat.Crypt2 tCrypt = new Chilkat.Crypt2();

		public string HashString(string strString)
		{
			string HashString = "";
			tCrypt.HashAlgorithm = "sha512";
			string strReturn;
			strReturn = tCrypt.HashStringENC(strString);
			HashString = strReturn;
			return HashString;
		}

		public string KeyFromPassword(string strPass, string strSalt, int intIterations, int intBits)
		{
			string KeyFromPassword = "";
			// Dim tCrypt As New ChilkatCrypt2
			//tCrypt.UnlockComponent("HARRISCrypt_vEoVPOQtXRFY");
            tCrypt.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
			string strReturn;
			strReturn = tCrypt.Pbkdf2(strPass, "ansi", "sha1", strSalt, intIterations, intBits, "base64");
			KeyFromPassword = strReturn;
			return KeyFromPassword;
		}

		public string DecryptByPassword(string strEncrypted, string strPassword, string strSalt)
		{
			string DecryptByPassword = "";
			DecryptByPassword = DecryptFromPassword(strEncrypted, strPassword, strSalt, "", 2, 256);
			return DecryptByPassword;
		}

		private string DecryptFromPassword(string strEncrypted, string strPassword, string strSalt, string strIV, int intIterations, int intBitSize)
		{
			string DecryptFromPassword = "";
			string strReturn;
			string strPass;
			string strIVector = "";
			strPass = KeyFromPassword(strPassword, strSalt, 2, 256);
			if (strIV == "")
			{
				strIVector = KeyFromPassword(strPassword, strSalt, 2, 128);
			}
			else
			{
				strIVector = strIV;
			}
			strReturn = Decrypt(strEncrypted, strPass, strIVector);
			DecryptFromPassword = strReturn;
			return DecryptFromPassword;
		}

		public string Decrypt(string strEncrypted, string strKey, string strIV)
		{
			string Decrypt = "";
			string strReturn = "";
			if (strEncrypted != "")
			{
				// Dim tCrypt As New ChilkatCrypt2
				//tCrypt.UnlockComponent("HARRISCrypt_vEoVPOQtXRFY");
                tCrypt.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
				tCrypt.CryptAlgorithm = "aes";
				tCrypt.KeyLength = 256;
				tCrypt.CipherMode = "cbc";
				tCrypt.PaddingScheme = 0;
				tCrypt.SetEncodedKey(strKey, "base64");
				tCrypt.SetEncodedIV(strIV, "base64");
				strReturn = tCrypt.DecryptStringENC(strEncrypted);
			}
			Decrypt = strReturn;
			return Decrypt;
		}

		public cCryptoUtility() : base()
		{
			//tCrypt.UnlockComponent("HARRISCrypt_vEoVPOQtXRFY");
            tCrypt.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
        }
	}
}
