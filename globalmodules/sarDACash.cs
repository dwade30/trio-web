﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

#if TWCL0000
using TWCL0000;


#else
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarDACash : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/22/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/26/2003              *
		// ********************************************************
		bool boolWide;
		float lngWidth;
		clsDRWrapper rsPayment = new clsDRWrapper();
		clsDRWrapper rsCorrection = new clsDRWrapper();
		clsDRWrapper rsYear = new clsDRWrapper();
		string strSQL;
		int lngYear;
		bool boolRECash;

		public sarDACash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarDACash_ReportEnd;
		}

        private void SarDACash_ReportEnd(object sender, EventArgs e)
        {
			rsPayment.DisposeOf();
            rsCorrection.DisposeOf();
            rsYear.DisposeOf();

		}

        public static sarDACash InstancePtr
		{
			get
			{
				return (sarDACash)Sys.GetInstance(typeof(sarDACash));
			}
		}

		protected sarDACash _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsYear.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (FCConvert.ToString(this.UserData) == "RE")
			{
				boolRECash = true;
			}
			else
			{
				boolRECash = false;
			}
			SetupFields();
			SetupValues();
			lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(FCConvert.ToString(DateTime.Now.Year) + "1")));
			if (rsPayment.EndOfFile() != true)
			{
				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsPayment.Get_Fields("Year")) < lngYear)
				{
					// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
					lngYear = FCConvert.ToInt32(rsPayment.Get_Fields("Year"));
				}
			}
			if (rsCorrection.EndOfFile() != true)
			{
				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsCorrection.Get_Fields("Year")) < lngYear)
				{
					// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
					lngYear = FCConvert.ToInt32(rsCorrection.Get_Fields("Year"));
				}
			}
			frmWait.InstancePtr.IncrementProgress();
		}

		private void SetupFields()
		{
			boolWide = rptCLDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptCLDailyAuditMaster.InstancePtr.lngWidth;
			this.PrintWidth = lngWidth;
			// Header Section
			if (!boolWide)
			{
				// Narrow
				lblYear.Left = 0;
				lblPrincipal.Left = 900 / 1440f;
				lblPaymentPrin.Left = 900 / 1440f;
				lblCorrectionPrin.Left = 2160 / 1440f;
				lblInterest.Left = 3420 / 1440f;
				lblPaymentInt.Left = 3420 / 1440f;
				lblCorrectionInt.Left = 4680 / 1440f;
				lblCosts.Left = 5940 / 1440f;
				lblPaymentCosts.Left = 5940 / 1440f;
				lblCorrectionCosts.Left = 7200 / 1440f;
				lblTotal.Left = 8460 / 1440f;
			}
			else
			{
				// Wide
				lblYear.Left = 0;
				lblPrincipal.Left = 2250 / 1440f;
				lblPaymentPrin.Left = 2250 / 1440f;
				lblCorrectionPrin.Left = 3510 / 1440f;
				lblInterest.Left = 5400 / 1440f;
				lblPaymentInt.Left = 5400 / 1440f;
				lblCorrectionInt.Left = 6660 / 1440f;
				lblCosts.Left = 8550 / 1440f;
				lblPaymentCosts.Left = 8550 / 1440f;
				lblCorrectionCosts.Left = 9810 / 1440f;
				lblTotal.Left = 11790 / 1440f;
			}
			lnHeader.X2 = lngWidth;
			lblHeader.Width = lngWidth;
			// Detail Section
			fldYear.Left = lblYear.Left;
			fldPaymentPrin.Left = lblPaymentPrin.Left;
			fldCorrectionPrin.Left = lblCorrectionPrin.Left;
			fldPaymentInterest.Left = lblPaymentInt.Left;
			fldCorrectionInt.Left = lblCorrectionInt.Left;
			fldPaymentCosts.Left = lblPaymentCosts.Left;
			fldCorrectionCosts.Left = lblCorrectionCosts.Left;
			fldTotal.Left = lblTotal.Left;
			// Footer Section
			fldTotalPaymentPrin.Left = lblPaymentPrin.Left;
			fldTotalCorrectionPrin.Left = lblCorrectionPrin.Left;
			fldTotalPaymentInt.Left = lblPaymentInt.Left;
			fldTotalCorrectionInt.Left = lblCorrectionInt.Left;
			fldTotalPaymentCost.Left = lblPaymentCosts.Left;
			fldTotalCorrectionCosts.Left = lblCorrectionCosts.Left;
			fldTotalTotal.Left = lblTotal.Left;
			lnTotals.X1 = fldTotalPaymentPrin.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void SetupValues()
		{
			// this will get all of the values ready for reporting
			clsDRWrapper rsTotals = new clsDRWrapper();
			string strSQLAddition = "";
			if (boolRECash)
			{
				strSQLAddition = "AND BillCode <> 'P' ";
			}
			else
			{
				strSQLAddition = "AND BillCode = 'P' ";
			}
			strSQL = "SELECT [Year] FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' GROUP BY [Year]";
			rsYear.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsYear.EndOfFile() != true && rsYear.BeginningOfFile() != true)
			{
				// set the payments recordset
				strSQL = "SELECT [Year], SUM(Principal) AS Prin, SUM(LienCost) AS Costs, SUM(PreLienInterest + CurrentInterest) AS Interest FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' OR GeneralLedger = 'Y') AND (Principal + LienCost + PreLienInterest + CurrentInterest) > 0 GROUP BY [Year] ORDER BY [Year]";
				rsPayment.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				// set the correction recordset
				strSQL = "SELECT [Year], SUM(Principal) AS Prin, SUM(LienCost) AS Costs, SUM(PreLienInterest + CurrentInterest) AS Interest FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' OR GeneralLedger = 'Y') AND (Principal + LienCost + PreLienInterest + CurrentInterest) <= 0 GROUP BY [Year] ORDER BY [Year]";
				rsCorrection.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				// find the totals and show them in the footer
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(LienCost) AS Costs, SUM(PreLienInterest + CurrentInterest) AS Interest FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' OR GeneralLedger = 'Y') AND (Principal + LienCost + PreLienInterest + CurrentInterest) > 0";
				rsTotals.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				if (rsTotals.EndOfFile() != true)
				{
					fldTotalPaymentPrin.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					fldTotalPaymentInt.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("Interest")), "#,##0.00");
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					fldTotalPaymentCost.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("Costs")), "#,##0.00");
				}
				else
				{
					fldTotalPaymentPrin.Text = "0.00";
					fldTotalPaymentInt.Text = "0.00";
					fldTotalPaymentCost.Text = "0.00";
				}
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(LienCost) AS Costs, SUM(PreLienInterest + CurrentInterest) AS Interest FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' OR GeneralLedger = 'Y') AND (Principal + LienCost + PreLienInterest + CurrentInterest) <= 0";
				rsTotals.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				if (rsTotals.EndOfFile() != true)
				{
					fldTotalCorrectionPrin.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					fldTotalCorrectionInt.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("Interest")), "#,##0.00");
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					fldTotalCorrectionCosts.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("Costs")), "#,##0.00");
				}
				else
				{
					fldTotalCorrectionPrin.Text = "0.00";
					fldTotalCorrectionInt.Text = "0.00";
					fldTotalCorrectionCosts.Text = "0.00";
				}
				// this will show the end total
				fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPaymentPrin.Text) + FCConvert.ToDouble(fldTotalPaymentInt.Text) + FCConvert.ToDouble(fldTotalPaymentCost.Text) + FCConvert.ToDouble(fldTotalCorrectionPrin.Text) + FCConvert.ToDouble(fldTotalCorrectionInt.Text) + FCConvert.ToDouble(fldTotalCorrectionCosts.Text), "#,##0.00");
			}
			else
			{
				Cancel();
			}
			rsTotals.DisposeOf();
		}

		private void BindFields_2(int lngYR)
		{
			BindFields(ref lngYR);
		}

		private void BindFields(ref int lngYR)
		{
			// this will put the current record year in the fields
			double dblSum;
			fldYear.Text = modExtraModules.FormatYear(lngYR.ToString());
			frmWait.InstancePtr.IncrementProgress();
			dblSum = 0;
			if (rsPayment.EndOfFile() != true)
			{
				rsPayment.FindFirstRecord("Year", lngYR);
				if (rsPayment.NoMatch)
				{
					fldPaymentPrin.Text = "0.00";
					fldPaymentInterest.Text = "0.00";
					fldPaymentCosts.Text = "0.00";
				}
				else
				{
					fldPaymentPrin.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					fldPaymentInterest.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields("Interest")), "#,##0.00");
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					fldPaymentCosts.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields("Costs")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					dblSum = Conversion.Val(rsPayment.Get_Fields_Double("Prin")) + Conversion.Val(rsPayment.Get_Fields("Interest")) + Conversion.Val(rsPayment.Get_Fields("Costs"));
				}
			}
			else
			{
				fldPaymentPrin.Text = "0.00";
				fldPaymentInterest.Text = "0.00";
				fldPaymentCosts.Text = "0.00";
			}
			if (rsCorrection.EndOfFile() != true)
			{
				rsCorrection.FindFirstRecord("Year", lngYR);
				if (rsCorrection.NoMatch)
				{
					fldCorrectionPrin.Text = "0.00";
					fldCorrectionInt.Text = "0.00";
					fldCorrectionCosts.Text = "0.00";
				}
				else
				{
					fldCorrectionPrin.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					fldCorrectionInt.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields("Interest")), "#,##0.00");
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					fldCorrectionCosts.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields("Costs")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					dblSum += Conversion.Val(rsCorrection.Get_Fields_Double("Prin")) + Conversion.Val(rsCorrection.Get_Fields("Interest")) + Conversion.Val(rsCorrection.Get_Fields("Costs"));
				}
			}
			else
			{
				fldCorrectionPrin.Text = "0.00";
				fldCorrectionInt.Text = "0.00";
				fldCorrectionCosts.Text = "0.00";
			}
			fldTotal.Text = Strings.Format(dblSum, "#,##0.00");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsYear.EndOfFile() != true)
			{
				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
				BindFields_2(FCConvert.ToInt32(rsYear.Get_Fields("Year")));
				rsYear.MoveNext();
				// move to the next year and check to see if there are any years that need to be shown
			}
		}

		private void sarDACash_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarDACash.Caption	= "Daily Audit Cash Summary";
			//sarDACash.Icon	= "sarDACash.dsx":0000";
			//sarDACash.Left	= 0;
			//sarDACash.Top	= 0;
			//sarDACash.Width	= 11880;
			//sarDACash.Height	= 8415;
			//sarDACash.StartUpPosition	= 3;
			//sarDACash.SectionData	= "sarDACash.dsx":058A;
			//End Unmaped Properties
		}
	}
}
