﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Runtime.InteropServices;

namespace Global
{
	/// <summary>
	/// Summary description for frmAbout.
	/// </summary>
	partial class frmAbout : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLine> Line1;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCPictureBox imgIcon;
		public fecherFoundation.FCLabel lblVersion;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblExpires;
		public fecherFoundation.FCLabel lblDisclaimer;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAbout));
            this.cmdOK = new fecherFoundation.FCButton();
            this.imgIcon = new fecherFoundation.FCPictureBox();
            this.lblVersion = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblExpires = new fecherFoundation.FCLabel();
            this.lblDisclaimer = new fecherFoundation.FCLabel();
            this.lblVersionNumber = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdOK);
            this.BottomPanel.Location = new System.Drawing.Point(0, 192);
            this.BottomPanel.Size = new System.Drawing.Size(787, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblVersionNumber);
            this.ClientArea.Controls.Add(this.imgIcon);
            this.ClientArea.Controls.Add(this.lblVersion);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblExpires);
            this.ClientArea.Controls.Add(this.lblDisclaimer);
            this.ClientArea.Size = new System.Drawing.Size(807, 310);
            this.ClientArea.Controls.SetChildIndex(this.lblDisclaimer, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblExpires, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblVersion, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgIcon, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblVersionNumber, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(807, 60);
            this.TopPanel.Text = "About";
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(67, 28);
            this.HeaderText.Tag = "";
            this.HeaderText.Text = "About";
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "acceptButton";
            this.cmdOK.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdOK.Location = new System.Drawing.Point(334, 29);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdOK.Size = new System.Drawing.Size(98, 48);
            this.cmdOK.TabIndex = 7;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // imgIcon
            // 
            this.imgIcon.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgIcon.FillColor = 14540253;
            this.imgIcon.Image = ((System.Drawing.Image)(resources.GetObject("imgIcon.Image")));
            this.imgIcon.Location = new System.Drawing.Point(30, 30);
            this.imgIcon.Name = "imgIcon";
            this.imgIcon.Size = new System.Drawing.Size(340, 105);
            this.imgIcon.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Location = new System.Drawing.Point(406, 60);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(63, 15);
            this.lblVersion.TabIndex = 1;
            this.lblVersion.Text = "VERSION";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Location = new System.Drawing.Point(406, 95);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(121, 15);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "EXPIRATION DATE";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExpires
            // 
            this.lblExpires.BackColor = System.Drawing.Color.Transparent;
            this.lblExpires.Location = new System.Drawing.Point(554, 95);
            this.lblExpires.Name = "lblExpires";
            this.lblExpires.Size = new System.Drawing.Size(154, 15);
            this.lblExpires.TabIndex = 3;
            this.lblExpires.Text = "123456";
            this.lblExpires.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDisclaimer
            // 
            this.lblDisclaimer.BackColor = System.Drawing.Color.Transparent;
            this.lblDisclaimer.Location = new System.Drawing.Point(30, 141);
            this.lblDisclaimer.Name = "lblDisclaimer";
            this.lblDisclaimer.Size = new System.Drawing.Size(766, 51);
            this.lblDisclaimer.TabIndex = 6;
            this.lblDisclaimer.Text = "WARNING:  THIS PROGRAM IS PROTECTED BY US AND INTERNATIONAL COPYRIGHT LAWS.  \r\nC " +
    "2018 HARRIS COMPUTER SYSTEMS";
            // 
            // lblVersionNumber
            // 
            this.lblVersionNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblVersionNumber.Location = new System.Drawing.Point(554, 60);
            this.lblVersionNumber.Name = "lblVersionNumber";
            this.lblVersionNumber.Size = new System.Drawing.Size(154, 15);
            this.lblVersionNumber.TabIndex = 1003;
            this.lblVersionNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmAbout
            // 
            this.AcceptButton = this.cmdOK;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(807, 370);
            this.FillColor = 14540253;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAbout";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "About";
            this.Load += new System.EventHandler(this.frmAbout_Load);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcon)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        public FCLabel lblVersionNumber;
    }
}
