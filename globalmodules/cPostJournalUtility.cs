﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using Global;
using fecherFoundation;

namespace Global
{
	public class cPostJournalUtility
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDAccountController acctCont = new cBDAccountController();
		private cBDAccountController acctCont_AutoInitialized;

		private cBDAccountController acctCont
		{
			get
			{
				if (acctCont_AutoInitialized == null)
				{
					acctCont_AutoInitialized = new cBDAccountController();
				}
				return acctCont_AutoInitialized;
			}
			set
			{
				acctCont_AutoInitialized = value;
			}
		}
		// Private bdSetCont As New cBDSettingsController
		// Private bdSet As cBDSettings
		private cStandardAccounts sAccounts;
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}

		private void LoadStandardAccounts()
		{
			sAccounts = acctCont.GetStandardAccounts();
		}

		private void CloseGJExp(ref cJournal journ)
		{
			Dictionary<object, object> fundAmounts = new Dictionary<object, object>();
			// vbPorter upgrade warning: tEntry As cJournalEntry	OnWrite(object, cJournalEntry)
			cJournalEntry tEntry;
			cJournalEntry detEntry;
			string strFund = "";
			journ.JournalEntries.MoveFirst();
			if (sAccounts == null)
			{
				LoadStandardAccounts();
			}
			while (journ.JournalEntries.IsCurrent())
			{
				//Application.DoEvents();
				detEntry = (cJournalEntry)journ.JournalEntries.GetCurrentItem();
				if (Strings.LCase(Strings.Left(detEntry.Account + " ", 1)) == "e" && detEntry.Amount != 0)
				{
					if (Strings.LCase(detEntry.RCB) != "e" || Strings.LCase(journ.JournalType) != "py")
					{
						// find fund for account
						strFund = acctCont.GetFundForAccount(detEntry.Account);
						// get entry for fund
						if (fundAmounts.ContainsKey(strFund))
						{
							tEntry = (cJournalEntry)fundAmounts[strFund];
						}
						else
						{
							tEntry = new cJournalEntry();
							tEntry.Description = "Expense CTL";
							tEntry.RCB = "L";
							tEntry.Status = "P";
							tEntry.PostedDate = FCConvert.ToString(DateTime.Today);
							tEntry.Period = journ.Period;
							tEntry.JournalEntriesDate = detEntry.JournalEntriesDate;
							tEntry.JournalType = "G";
							tEntry.Account = "G " + strFund + "-" + sAccounts.ExpenseControl;
							if (!modAccountTitle.Statics.YearFlag)
							{
								tEntry.Account = tEntry.Account + "-00";
							}
							fundAmounts.Add(strFund, tEntry);
						}
						// update amount
						tEntry.Amount += detEntry.Amount;
					}
				}
				journ.JournalEntries.MoveNext();
			}
			// vbPorter upgrade warning: AR As object	OnRead(cJournalEntry)
			object[] AR = new object[fundAmounts.Count];
			fundAmounts.Values.CopyTo(AR, 0);
			int x;
			for (x = 0; x <= Information.UBound(AR, 1); x++)
			{
				//Application.DoEvents();
				tEntry = (cJournalEntry)AR[x];
				if (tEntry.Amount != 0)
				{
					journ.JournalEntries.AddItem(tEntry);
				}
			}
			fundAmounts.Clear();
		}

		private void CloseAPExp(ref cGenericCollection apJourns, ref cAPJournal apCtrlJournal)
		{
			Dictionary<object, object> fundAmounts = new Dictionary<object, object>();
			// vbPorter upgrade warning: tEntry As cAPJournalDetail	OnWrite(object, cAPJournalDetail)
			cAPJournalDetail tEntry;
			string strFund = "";
			cAPJournal aJourn;
			cAPJournalDetail apDet;
			apJourns.MoveFirst();
			while (apJourns.IsCurrent())
			{
				aJourn = (cAPJournal)apJourns.GetCurrentItem();
				if (Strings.LCase(aJourn.Description) != "control entries")
				{
					aJourn.JournalEntries.MoveFirst();
					while (apJourns.IsCurrent())
					{
						//Application.DoEvents();
						apDet = (cAPJournalDetail)apJourns.GetCurrentItem();
						if (Strings.LCase(Strings.Left(apDet.Account, FCConvert.ToInt32(" "))) == "e")
						{
							if (apDet.NetAmount() != 0)
							{
								strFund = acctCont.GetFundForAccount(apDet.Account);
								if (fundAmounts.ContainsKey(strFund))
								{
									tEntry = (cAPJournalDetail)fundAmounts[strFund];
								}
								else
								{
									tEntry = new cAPJournalDetail();
									tEntry.Description = "Expense CTL";
									tEntry.Project = "CTRL";
									tEntry.RCB = "L";
									if (!modAccountTitle.Statics.YearFlag)
									{
										tEntry.Account = "G " + strFund + "-" + sAccounts.ExpenseControl + "-00";
									}
									else
									{
										tEntry.Account = "G " + strFund + "-" + sAccounts.ExpenseControl;
									}
									fundAmounts.Add(strFund, tEntry);
								}
								tEntry.Amount += apDet.NetAmount();
							}
						}
						aJourn.JournalEntries.MoveNext();
					}
				}
				apJourns.MoveNext();
			}
			// vbPorter upgrade warning: AR As object	OnRead(cAPJournalDetail)
			object[] AR = new object[fundAmounts.Count];
			fundAmounts.Values.CopyTo(AR, 0);
			int x;
			for (x = 0; x <= Information.UBound(AR, 1); x++)
			{
				//Application.DoEvents();
				tEntry = (cAPJournalDetail)AR[x];
				if (tEntry.Amount != 0)
				{
					// Call journ.JournalEntries.AddItem(tEntry)
					apCtrlJournal.JournalEntries.AddItem(tEntry);
				}
			}
			fundAmounts.Clear();
		}
	}
}
