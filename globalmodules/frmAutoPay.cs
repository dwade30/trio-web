﻿using System;
using System.Windows.Forms.VisualStyles;
using System.Xml;
using fecherFoundation;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary.Data;
using Wisej.Web;

#if  TWCR0000
using TWCR0000;

#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
using modUseCR = TWUT0000.modUTUseCR;
#endif

namespace Global
{
    public partial class frmAutoPay : BaseForm
    {
        private string strPaidBy = "";
        private string strRef = "";
        private string strTLRID = "";
        private double dblCurPrin = 0;
        private double dblCurInt = 0;
        private double dblPLInt = 0;
        private double dblCurCost = 0;
        private double dblCurTax = 0;
        private DateTime dtIntPaidDate;
        private int lngBatchCHGINTNumber;
        private double dblXtraCurInt = 0;

        private bool boolPreNote = false;
        private bool boolPosting = false;
        private bool boolLoaded = false;
        private string strWS = "";
        private clsDRWrapper rsData = new clsDRWrapper();

        private const int vsColFlag = 0;
        private const int vsColAcct = 1;
        private const int vsColName    = 2;
        private const int vsColBook = 3;
        private const int vsColLoc = 4;
        private const int vsColAmt = 5;
        private const int vsColBal = 6;
        private const int vsColBillDate = 7;
        private const int vsColEffDate = 8;
        private const int vsColPostDate = 9;
        private const int vsColHid1 = 10;
        private const int vsColHid2    = 11;

        public double BatchTotal { get; set; }
        public DateTime EffectivePmtDate { get; set; }
        public string BatchNumber
        {
            get
            {
                return cmbBatch.Text;
            }
        }
        public frmAutoPay()
        {
            InitializeComponent();
            InitializeComponentEx();
            SetupHandlers();
        }
        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        public static frmAutoPay InstancePtr
        {
            get
            {
                return (frmAutoPay)Sys.GetInstance(typeof(frmAutoPay));
            }
        }

        protected frmAutoPay _InstancePtr = null;

        private void SetupHandlers()
        {
            this.FCFormActivate += FrmAutoPay_FCFormActivate;
            this.cmbBatch.SelectedIndexChanged += CmbBatch_SelectedIndexChanged;
            this.KeyPress += FrmAutoPay_KeyPress;
            this.FormUnload += FrmAutoPay_FormUnload;
            this.vsAutoPay.DoubleClick += VsAutoPay_DoubleClick;
            this.vsAutoPay.CurrentCellChanged += VsAutoPay_CurrentCellChanged;
            this.vsAutoPay.CellValidating += VsAutoPay_CellValidating;
            this.vsAutoPay.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsAutoPay_MouseMoveEvent);
            this.vsAutoPay.CellBeginEdit += VsAutoPay_CellBeginEdit;
        }

        private void VsAutoPay_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (RowIsDisabled(e.RowIndex))
            {
                e.Cancel = true;
            }
        }

        private void vsAutoPay_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsAutoPay[e.ColumnIndex, e.RowIndex];

            int lngMR = vsAutoPay.GetFlexRowIndex(e.RowIndex);
            int lngMC = vsAutoPay.GetFlexColIndex(e.ColumnIndex);

            if (lngMR > 0 && lngMC >= 0)
            {
                //if (vsAutoPay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngMR,  lngMC) ==
                //    Information.RGB(230, 230, 230))
                if (RowIsDisabled(lngMR))
                {
                    if (!boolPosting)
                    {
                        if (boolPreNote)
                        {
                            //toolTip1.SetToolTip(vsAutoPay,"This account is not configured for prenote processing. It will not be processed.");
                            cell.ToolTipText =  "This account is not configured for prenote processing. It will not be processed.";
                        }
                        else
                        {
                            if (Convert.ToInt32(vsAutoPay.TextMatrix(lngMR, vsColBal)) <= 0)
                            {
                                //toolTip1.SetToolTip(vsAutoPay,"Nothing due on this account. It will not be processed.");
                                cell.ToolTipText = "Nothing due on this account. It will not be processed.";
                            }
                            else
                            {
                                //toolTip1.SetToolTip(vsAutoPay,"ACH is inactie for ths account. It will not be processed.");
                                cell.ToolTipText = "ACH is inactie for ths account. It will not be processed.";
                            }
                        }
                    }
                    else
                    {
                        //toolTip1.SetToolTip(vsAutoPay,"Account not included in ACH file. It will not be processed");
                        cell.ToolTipText = "Account not included in ACH file. It will not be processed";
                    }
                }
                else
                {
                    //toolTip1.SetToolTip(vsAutoPay,"");
                    cell.ToolTipText = "";
                }
            }
            
        }

        private bool RowIsDisabled(int row)
        {
            if (row >= vsAutoPay.Rows)
            {
                return true;
            }

            if (vsAutoPay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row, vsColName) ==
                Information.RGB(230, 230, 230))
            {
                return true;
            }

            return false;
        }
        private void VsAutoPay_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            var invalidPayment = false;
            switch (e.ColumnIndex)
            {
                case vsColAmt:
                    if (String.IsNullOrWhiteSpace(vsAutoPay.EditText))
                    {
                        invalidPayment = true;
                    }
                    else if (Convert.ToDouble(vsAutoPay.EditText) <= 0)
                    {
                        invalidPayment = true;
                    }

                    if (invalidPayment)
                    {
                        MessageBox.Show("Invalid Payment Amount", "Invalid Amount", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        e.Cancel = true;
                    }
                    break;

            }
        }

        private void VsAutoPay_CurrentCellChanged(object sender, EventArgs e)
        {
            switch (vsAutoPay.Col)
            {
                case vsColFlag:
                    vsAutoPay.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                    break;
                case vsColAmt:
                    if (!boolPreNote)
                    {
                        vsAutoPay.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                        vsAutoPay.EditCell();
                        vsAutoPay.EditSelStart = 0;
                        vsAutoPay.EditSelLength = vsAutoPay.EditText.Length;
                    }
                    break;
                default:
                    vsAutoPay.Editable = FCGrid.EditableSettings.flexEDNone;
                    break;
            }
        }


        private void VsAutoPay_DoubleClick(object sender, EventArgs e)
        {
            var lngMR = vsAutoPay.MouseRow;
            var lngMC = vsAutoPay.MouseCol;
            if (lngMR == 0 && lngMC == 0)
            {
                SelectAll();
            }
        }

        private void FrmAutoPay_FormUnload(object sender, FCFormClosingEventArgs e)
        {
            boolLoaded = false;
        }

        private void FrmAutoPay_KeyPress(object sender, KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                Close();
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void CmbBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            var strBatchNumber = cmbBatch.Text;
            boolPreNote = strBatchNumber.ToLower().EndsWith("pn");
            FillAutoPayGrid();
        }

        private void FrmAutoPay_FCFormActivate(object sender, EventArgs e)
        {
            if (!boolLoaded)
            {
                boolLoaded = true;

                ResetMenu();
                strWS = "S";
                FillBatchCombo();
            }
        }

        private void FillBatchCombo()
        {
            var rsTemp = new clsDRWrapper();
           cmbBatch.Clear();
           if (boolPosting)
           {
               rsTemp.OpenRecordset(
                   "Select distinct batchnumber from tblautopay where posted = 0 and achdone = 1 and service = '" +
                   strWS + "' order by batchnumber", "UtilityBilling");
               while (!rsTemp.EndOfFile())
               {
                   if (!rsTemp.Get_Fields_String("BatchNumber").ToUpper().EndsWith("PN"))
                   {
                        cmbBatch.AddItem("BatchNumber");
                   }

                   rsTemp.MoveNext();
               }
           }
           else
           {
               rsTemp.OpenRecordset(
                   "Select distinct batchnumber from tblautopay where achdone = 0 and service = '" + strWS +
                   "' order by batchnumber", "UtilityBilling");
               while (!rsTemp.EndOfFile())
               {
                   cmbBatch.AddItem(rsTemp.Get_Fields_String("BatchNumber"));
                   rsTemp.MoveNext();
               }
           }
        }

        public void Init(bool boolPassPost)
        {
            boolPosting = boolPassPost;
            this.Show(App.MainForm);
        }

        private void ResetMenu()
        {
            if (boolPosting)
            {
                mnuFileForcePrenote.Enabled = false;
                mnuFileProcPrenotes.Enabled = false;
                mnuFileForceClear.Enabled = false;
                cmdSelectAll.Enabled = false;
                cmdClearSelection.Enabled = false;
                cmdPrintPreview.Enabled = false;
                cmdPostPayments.Enabled = false;
            }
            else
            {
                mnuFileForcePrenote.Enabled = false;
                mnuFileProcPrenotes.Enabled = true;
                mnuFileForceClear.Enabled = false;
                cmdSelectAll.Enabled = false;
                cmdClearSelection.Enabled = false;
                cmdPrintPreview.Enabled = false;
                cmdPostPayments.Enabled = false;
            }
        }

        private void FormatGrid()
        {
            vsAutoPay.Cols = 12;

            var wid = vsAutoPay.WidthOriginal;
            vsAutoPay.ColWidth(vsColFlag, Convert.ToInt32(wid * .02));
            vsAutoPay.ColWidth(vsColAcct,Convert.ToInt32(wid * .02));
            vsAutoPay.ColWidth(vsColName, Convert.ToInt32(wid * .2));
            vsAutoPay.ColWidth(vsColBook, Convert.ToInt32(wid * .05));
            vsAutoPay.ColWidth(vsColLoc, Convert.ToInt32(wid * .18));
            vsAutoPay.ColWidth(vsColAmt, Convert.ToInt32(wid * .07));
            vsAutoPay.ColWidth(vsColBal, Convert.ToInt32(wid * .07));
            vsAutoPay.ColWidth(vsColBillDate, Convert.ToInt32(wid * .09));
            vsAutoPay.ColWidth(vsColEffDate, Convert.ToInt32(wid * .09));
            vsAutoPay.ColWidth(vsColPostDate, Convert.ToInt32(wid * .09));
            vsAutoPay.ColWidth(vsColHid1, 0);
            vsAutoPay.ColWidth(vsColHid2, 0);

            vsAutoPay.ColDataType(vsColFlag, FCGrid.DataTypeSettings.flexDTBoolean);
            vsAutoPay.ColDataType(vsColBillDate, FCGrid.DataTypeSettings.flexDTDate);
            vsAutoPay.ColDataType(vsColEffDate, FCGrid.DataTypeSettings.flexDTDate);
            vsAutoPay.ColDataType(vsColPostDate, FCGrid.DataTypeSettings.flexDTDate);

            vsAutoPay.ColAlignment(vsColAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAutoPay.ColAlignment(vsColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAutoPay.ColAlignment(vsColBook, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAutoPay.ColAlignment(vsColLoc, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAutoPay.ColAlignment(vsColAmt, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsAutoPay.ColAlignment(vsColBal, FCGrid.AlignmentSettings.flexAlignRightCenter);

            vsAutoPay.ColFormat(vsColAmt, "#,##0.00");
            vsAutoPay.ColFormat(vsColBal, "#,##0.00");

            vsAutoPay.TextMatrix(0, vsColFlag, "");
            vsAutoPay.TextMatrix(0, vsColAcct, "Account");
            vsAutoPay.TextMatrix(0, vsColName, "Name");
            vsAutoPay.TextMatrix(0, vsColBook, "Book");
            vsAutoPay.TextMatrix(0, vsColLoc, "Location");
            vsAutoPay.TextMatrix(0, vsColAmt, "DW Amount");
            vsAutoPay.TextMatrix(0, vsColBal, "Balance");
            vsAutoPay.TextMatrix(0, vsColBillDate, "Bill Date");
            if (boolPosting)
            {
                vsAutoPay.TextMatrix(0, vsColEffDate, "Eff Date");
                vsAutoPay.TextMatrix(0, vsColPostDate, "Posted");
            }
            else
            {
                vsAutoPay.TextMatrix(0, vsColEffDate, "Process By");
                vsAutoPay.TextMatrix(0, vsColPostDate, "Processed");
            }
        }

        private void FillAutoPayGrid()
        {
            string strSQL = "";
            var rsTemp = new clsDRWrapper();
            double dblTotalDue = 0;
            double dblPrinDue = 0;
            double dblXtraInt = 0;
            double dblIntDue = 0;
            double dblCostDue = 0;
            double dblPLIDue = 0;
            string strNewOwner = "";
            string strLoc = "";

            try
            {
                vsAutoPay.Rows = 1;
                strSQL = "Select * from tblAutoPay where batchnumber = '" + BatchNumber + "' order by accountnumber";
                rsData.OpenRecordset(strSQL, "UtilityBilling");
                if (rsData.EndOfFile())
                {
                    MessageBox.Show("There are no accounts eligible for Auto-Pay", "No Eligible Accounts",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Unload();
                }

                while (!rsData.EndOfFile())
                {
                    dblTotalDue = 0;
                    dblPrinDue = 0;
                    dblXtraInt = 0;
                    dblIntDue = 0;
                    dblPLIDue = 0;

                    if (boolPreNote)
                    {
                        rsTemp.OpenRecordset(
                            "Select id as accountkey, utbook as book, streetnumber, streetname from master where AccountNumber = " +
                            rsData.Get_Fields_Int32("AccountNumber"), "UtilityBilling");
                        if (!rsTemp.EndOfFile())
                        {
                            if (rsTemp.Get_Fields_String("StreetNumber").Trim() != "")
                            {
                                strLoc = rsTemp.Get_Fields_String("StreetNumber").Trim() + " " +
                                         rsTemp.Get_Fields_String("StreetName");
                            }
                            else
                            {
                                strLoc = rsTemp.Get_Fields_String("StreetName").Trim();
                            }
                        }
                    }
                    else
                    {
                        rsTemp.OpenRecordset("Select * from Bill where id = " + rsData.Get_Fields_Int32("BillKey"),
                            "UtilityBilling");
                        var pmtDate = rsData.Get_Fields_DateTime("EffPmtDate");
                        dblTotalDue = modUTCalculations.CalculateAccountUTTotal(rsTemp.Get_Fields_Int32("AccountKey"),
                            rsTemp.Get_Fields_String("Service") == "W", true, ref dblXtraInt, ref dblIntDue,
                            ref pmtDate, false, 0, false, ref dblCostDue, ref dblPLIDue);
                        dblPrinDue = dblTotalDue - (dblCostDue + dblXtraInt + dblIntDue + dblPLIDue);
                    }

                    vsAutoPay.Rows += 1;
                    var lastRow = vsAutoPay.Rows - 1;
                    var lastCol = vsAutoPay.Cols - 1;
                    vsAutoPay.RowData(lastRow, rsTemp.Get_Fields_Int32("AccountKey"));
                    vsAutoPay.TextMatrix(lastRow, vsColFlag, rsData.Get_Fields_Boolean("ACHFlag"));
                    vsAutoPay.TextMatrix(lastRow, vsColAcct, rsData.Get_Fields_Int32("AccountNumber"));
                    vsAutoPay.TextMatrix(lastRow, vsColName, rsData.Get_Fields_String("Name"));
                    vsAutoPay.TextMatrix(lastRow, vsColBook, rsData.Get_Fields_Int32("Book"));
                    if (boolPreNote)
                    {
                        vsAutoPay.TextMatrix(lastRow, vsColLoc, strLoc);
                        vsAutoPay.TextMatrix(lastRow, vsColBillDate, "");
                    }
                    else
                    {
                        vsAutoPay.TextMatrix(lastRow, vsColLoc, rsTemp.Get_Fields_String("Location"));
                        vsAutoPay.TextMatrix(lastRow, vsColBillDate,
                            rsTemp.Get_Fields_DateTime("BillDate").FormatAndPadShortDate());
                    }

                    vsAutoPay.TextMatrix(lastRow, vsColAmt, rsData.Get_Fields_Double("ACHAmount"));
                    vsAutoPay.TextMatrix(lastRow, vsColBal, dblTotalDue);
                    if (boolPosting)
                    {
                        vsAutoPay.TextMatrix(lastRow, vsColEffDate,
                            rsData.Get_Fields_DateTime("EffPmtDate").FormatAndPadShortDate());
                        if (rsData.Get_Fields_DateTime("PostedDate") == DateTime.MinValue)
                        {
                            vsAutoPay.TextMatrix(lastRow, vsColPostDate, "");
                        }
                        else
                        {
                            vsAutoPay.TextMatrix(lastRow, vsColPostDate,
                                rsData.Get_Fields_DateTime("PostedDate").FormatAndPadShortDate());
                        }
                    }
                    else
                    {
                        vsAutoPay.TextMatrix(lastRow, vsColEffDate,
                            rsData.Get_Fields_DateTime("ACHDueDate").FormatAndPadShortDate());
                        vsAutoPay.TextMatrix(lastRow, vsColPostDate, "");
                    }

                    vsAutoPay.TextMatrix(lastRow, vsColHid1, rsData.Get_Fields_Int32("BillKey"));
                    vsAutoPay.TextMatrix(lastRow, vsColHid2, rsData.Get_Fields_Int32("ID"));

                    if (!boolPosting && !boolPreNote)
                    {
                        if (dblTotalDue <= 0)
                        {
                            vsAutoPay.TextMatrix(lastRow, vsColFlag, false);
                            vsAutoPay.TextMatrix(lastRow, vsColAmt, 0);
                            vsAutoPay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lastRow, 0, lastRow, lastCol, Information.RGB(230, 230, 230));
                        }
                        else
                        {
                            rsTemp.OpenRecordset(
                                "select ACHActive, ACHPreNote from tblAcctACH where id = " +
                                rsData.Get_Fields_Int32("AcctACHID"), "UtilityBilling");
                            if (!rsTemp.EndOfFile())
                            {
                                if (!rsTemp.Get_Fields_Boolean("ACHActive") || rsTemp.Get_Fields_Boolean("ACHPreNote"))
                                {
                                    vsAutoPay.TextMatrix(lastRow, vsColFlag, false);
                                    vsAutoPay.TextMatrix(lastRow, vsColAmt, 0);
                                    vsAutoPay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lastRow, 0, lastRow, lastCol, Information.RGB(230, 230, 230));
                                }
                            }
                        }
                    }
                    else if (boolPreNote && !boolPosting)
                    {
                        rsTemp.OpenRecordset(
                            "select achactive, achprenote from tblacctach where id = " +
                            rsData.Get_Fields_Int32("AcctACHID"), "UtilityBilling");
                        if (!rsTemp.EndOfFile())
                        {
                            if (!rsTemp.Get_Fields_Boolean("Achactive") || !rsTemp.Get_Fields_Boolean("ACHPreNote"))
                            {
                                vsAutoPay.TextMatrix(lastRow, vsColFlag, false);
                                vsAutoPay.TextMatrix(lastRow, vsColAmt, 0);
                                vsAutoPay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lastRow, 0, lastRow, lastCol, Information.RGB(230, 230, 230));
                            }
                        }
                    }
                    else if (boolPosting)
                    {
                        if (rsData.Get_Fields_Int32("LineNumber") == 0)
                        {
                            vsAutoPay.TextMatrix(lastRow, vsColFlag, false);
                            vsAutoPay.TextMatrix(lastRow, vsColAmt, 0);
                            vsAutoPay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lastRow, 0, lastRow, lastCol, Information.RGB(230, 230, 230));
                        }
                    }

                    rsData.MoveNext();
                }

                if (vsAutoPay.Rows > 1)
                {
                    vsAutoPay.Select(1,2);
                    vsAutoPay.Sort = FCGrid.SortSettings.flexSortGenericAscending;
                }

                this.cmdClearSelection.Enabled = true;
                this.cmdSelectAll.Enabled = true;
                if (!boolPosting && !boolPreNote)
                {
                    mnuFileForcePrenote.Enabled = true;
                }

                mnuFileProcPrenotes.Enabled = false;
                mnuFileForceClear.Enabled = true;
                cmdPrintPreview.Enabled = true;
                cmdPostPayments.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mnuProcessExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ClearCheckBoxes()
        {
            for (int x = 1; x < vsAutoPay.Rows; x++)
            {
                vsAutoPay.TextMatrix(x, vsColFlag, false);
            }
        }

        private void MnuFileForcePrenote_Click(object sender, System.EventArgs e)
        {
            CreateAutoPayFile(true);
        }

        private void CreateAutoPayFile(bool boolForcePrenote = false)
        {
#if TWUT0000
            throw new NotImplementedException();
            var tFileCon = new clsACHFileControllerUT();
            clsACHFile tFile;
#endif
        }

        private void MnuFileProcPrenotes_Click(object sender, System.EventArgs e)
        {
            CreatePrenoteBatch();
        }

        private void CreatePrenoteBatch()
        {
            var rsAcctACH = new clsDRWrapper();
            var rsAutoPay = new clsDRWrapper();
            string strSQL = "";
            DateTime dtProcDate =  DateTime.FromOADate(0); 
            DateTime dtBatchDate = DateTime.Now;
            int intCount = 0;
            var strBatchNumber = "";

            strSQL =
                "Select Master.AcountNumber, Master.Name, Master.OwnerName, Master.SameBillOwner, Master.WBillToOwner,";
            strSQL +=
                " Master.SBillToOwner, tblacctach.id as AcctACHID,tblAcctACH.* from master inner join tblAcctACH on Master.id ";
            strSQL += "= tblAcctACH.AccountKey where ACHActive = 1 and ACHPreNote = 1";
            rsAcctACH.OpenRecordset(strSQL, "UtilityBilling");
            while (!rsAcctACH.EndOfFile())
            {
                if (dtProcDate == DateTime.FromOADate(0))
                {
                    dtProcDate = DateTime.Now;
                    object temp = dtProcDate;
                    frmInput.InstancePtr.Init(ref temp, "Auto-Pay Process Date",
                        "Enter the PreNote Processing Date.", 1500, false, modGlobalConstants.InputDTypes.idtDate);
                    if (Convert.ToDateTime(temp) != DateTime.FromOADate(0))
                    {
                        dtProcDate = Convert.ToDateTime(temp);
                    }

                    dtBatchDate = DateTime.Now;
                    strBatchNumber = fecherFoundation.Strings.Format(dtBatchDate, "YYMMDDHhNn") + "-PN";
                }

                rsAutoPay.OpenRecordset("Select * from tblAutoPay where id = -1", "UtilityBilling");
                    rsAutoPay.AddNew();
                    rsAutoPay.Set_Fields("BatchNumber",strBatchNumber);
                    rsAutoPay.Set_Fields("BillKey",0);
                    rsAutoPay.Set_Fields("Service",rsAcctACH.Get_Fields_String("Service"));
                    rsAutoPay.Set_Fields("AccountNumber",rsAcctACH.Get_Fields_Int32("AccountNumber"));
                    if (rsAcctACH.Get_Fields_Boolean("SameBillOwner"))
                    {
                        rsAutoPay.Set_Fields("Name",rsAcctACH.Get_Fields_String("OwnerName"));
                    }
                    else
                    {
                        if (rsAcctACH.Get_Fields_Boolean(rsAcctACH.Get_Fields_String("Service") + "BillToOwner"))
                        {
                            rsAutoPay.Set_Fields("Name",rsAcctACH.Get_Fields_String("OwnerName"));
                        }
                        else
                        {
                            rsAutoPay.Set_Fields("Name",rsAcctACH.Get_Fields_String("Name"));
                        }
                    }

                    rsAutoPay.Set_Fields("AcctACHID",rsAcctACH.Get_Fields_Int32("AcctACHID"));
                    rsAutoPay.Set_Fields("ACHAmount",0);
                    rsAutoPay.Set_Fields("Prin",0);
                    rsAutoPay.Set_Fields("Int",0);
                    rsAutoPay.Set_Fields("Cost",0);
                    rsAutoPay.Set_Fields("Tax",0);
                    rsAutoPay.Set_Fields("BillDate",dtBatchDate.FormatAndPadShortDate());
                    rsAutoPay.Set_Fields("ACHDueDate",dtProcDate.FormatAndPadShortDate());
                    rsAutoPay.Set_Fields("ACHDate", FCConvert.ToDateTime(0));
                    rsAutoPay.Set_Fields("ACHDone",false);
                    rsAutoPay.Set_Fields("EffPmtDate",dtProcDate.FormatAndPadShortDate());
                    rsAutoPay.Set_Fields("PostedDate",FCConvert.ToDateTime(0));
                    rsAutoPay.Set_Fields("Posted",false);
                    rsAutoPay.Set_Fields("LineNumber",0);
                    rsAutoPay.Set_Fields("PaymentID",0);
                    rsAutoPay.Set_Fields("ACHFlag", rsAcctACH.Get_Fields_Boolean("ACHActive"));
                    rsAutoPay.Update();
                    intCount += 1;              

                rsAcctACH.MoveNext();
            }

            if (intCount > 0)
            {
                FillBatchCombo();
                cmbBatch.Text = strBatchNumber;
                boolPreNote = true;
                FillAutoPayGrid();
            }
            else
            {
                MessageBox.Show("No active Auto-Pay accounts marked for PreNote processing", "Auto-Pay PreNote Error",
                    MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }

        private void MnuFileForceClear_Click(object sender, System.EventArgs e)
        {
            ForceClear();
        }

        private void ForceClear()
        {
            if (boolPosting)
            {
                if (MessageBox.Show(
                        "This process will force this batch to Posted status without creating any payment records. \nThe batch will no longer appear in the list. Are you sure?",
                        "Confirm Force Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    ForceClearBatch(false);
                }
            }
            else
            {
                if (MessageBox.Show(
                        "This process will delete this batch without creating an ACH file or payment records.\nThe batch will no longer appear in the list. Are you sure?", 
                        "Confirm Force Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    ForceClearBatch(true);
                }
            }
        }

        private void ForceClearBatch(bool boolDelete)
        {
            var rsClr = new clsDRWrapper();

            if (!boolDelete)
            {
                rsClr.Execute("update tblautopay set posted = 1 where batchnumber = '" + BatchNumber + "'",
                    "UtilityBilling");
            }
            else
            {
                rsClr.Execute("delete from tblAutoPay where batchnumber = '" + BatchNumber + "'", "UtilityBilling");
            }

            FillBatchCombo();
            vsAutoPay.Rows = 1;
            ResetMenu();
        }

        private void cmdClearSelection_Click(object sender, EventArgs e)
        {
            ClearCheckBoxes();
        }

        private void cmdSelectAll_Click(object sender, EventArgs e)
        {
            SelectAll();
        }

        private void SelectAll()
        {
            for (int x = 1; x < vsAutoPay.Rows; x++)
            {
                if (vsAutoPay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, vsColName) !=
                    Information.RGB(230, 230, 230))
                {
                    vsAutoPay.TextMatrix(x, vsColFlag, true);
                }
                    
            }
        }

        private void cmdPrintPreview_Click(object sender, EventArgs e)
        {
            if (boolPosting)
            {
                SaveAutoPayGrid();
                arUTAutoPayEditReport.InstancePtr.Init(autoPayRptType.autoPayRptTypePost,true,BatchNumber);
            }
            else
            {
                SaveAutoPayGrid();
                if (boolPreNote)
                {
                    arUTAutoPayEditReport.InstancePtr.Init( autoPayRptType.autoPayRptTypePreNote,true,BatchNumber);
                }
                else
                {
                    arUTAutoPayEditReport.InstancePtr.Init( autoPayRptType.autoPayRptTypeACH,true,BatchNumber);
                }
            }
        }

        private void SaveAutoPayGrid()
        {
            for (int intCT = 1; intCT < vsAutoPay.Rows; intCT++)
            {
                rsData.FindFirst("ID - " + vsAutoPay.TextMatrix(intCT, vsColHid2));
                if (!rsData.NoMatch)
                {
                    rsData.Edit();
                    rsData.Set_Fields("ACHFlag", vsAutoPay.TextMatrix(intCT,vsColFlag));
                    if (modGlobal.Round(rsData.Get_Fields_Double("ACHAmount"), 2) !=
                        modGlobal.Round(Convert.ToDouble(vsAutoPay.TextMatrix(intCT, vsColAmt)), 2))
                    {
                        rsData.Set_Fields("ACHAmount",Convert.ToDouble(vsAutoPay.TextMatrix(intCT,vsColAmt)));
                    }
                    rsData.Update();
                }
            }
        }

        private void cmdPostPayments_Click(object sender, EventArgs e)
        {
            if (boolPosting)
            {
                PostPayments();
                Unload();
            }
            else
            {
                CreateAutoPayFile();
            }
        }

        private void PostPayments()
        {
            int lastRow = vsAutoPay.Rows - 1;
            if (lastRow > 0)
            {
                SaveAutoPayGrid();
                if (CreateBatchUTRecords())
                {
                    //if (modExtraModules.IsThisCR())
                    //{
                    //   modUseCR.CreateUTBatchReceiptEntry(false, true);
                    //}

                    MessageBox.Show("Auto-Pay Batch update complete.", "Payments Posted", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    if (!modExtraModules.IsThisCR())
                    {
                        arUTAutoPayEditReport.InstancePtr.Init(autoPayRptType.autoPayRptTypePost,false,BatchNumber);
                    }
                }
            }

            vsAutoPay.Rows = 1;
            Unload();
        }

        private bool CreateBatchUTRecords()
        {
            var rsPmtRec = new clsDRWrapper();
            var rsBillRec = new clsDRWrapper();
            var rsLien = new clsDRWrapper();
            string strSQL = "";
            double dblTotalDue = 0;
            string strSrvc = "";

            BatchTotal = 0;

            strSQL =
                "Select * from tblAutoPay where achflag = 1 and achdone = 1 and linenumber > 0 and not posted = 1 and Batchnumber = '" +
                BatchNumber + "'";

            rsData.OpenRecordset(strSQL, "UtilityBilling");
            if (!rsData.EndOfFile())
            {
                while (!rsData.EndOfFile())
                {
                    strSQL = "Select * from bill where id = " + rsData.Get_Fields_Int32("BillKey");
                    if (rsBillRec.OpenRecordset(strSQL, "UtilityBilling"))
                    {
                        strSrvc = rsData.Get_Fields_String("Service");

                        dblCurInt = 0;
                        dblCurTax = 0;
                        dblXtraCurInt = 0;
                        var tempDate = DateTime.Now;
                        dblTotalDue = modUTCalculations.CalculateAccountUT(rsBillRec,
                            rsData.Get_Fields_DateTime("EffPmtDate"), ref dblXtraCurInt, strSrvc == "W", ref dblCurPrin,
                            ref dblCurInt, ref dblCurCost, false, false, ref tempDate, ref dblCurTax);
                        dblPLInt = modGlobal.Round(dblTotalDue - (dblCurPrin + dblCurInt + dblCurCost + dblCurTax), 2);
                        strSQL = "Select * from paymentrec where id = 0";
                        rsPmtRec.OpenRecordset(strSQL, "UtilityBilling");
                        rsPmtRec.AddNew();

                        AdjustUTPaymentAmount(rsData.Get_Fields_Double("ACHAmount"), true);

                        rsData.Edit();
                        rsData.Set_Fields("Prin",dblCurPrin);
                        rsData.Set_Fields("Int",dblCurInt);
                        rsData.Set_Fields("Cost",dblCurCost);
                        rsData.Set_Fields("Tax",dblCurTax);
                        rsData.Set_Fields("PaymentID", rsPmtRec.Get_Fields_Int32("ID"));
                        rsData.Update();

                        rsPmtRec.Set_Fields("AccountKey",rsBillRec.Get_Fields_Int32("AccountKey"));
                        rsPmtRec.Set_Fields("BillNumber",rsBillRec.Get_Fields_Int32("BillNumber"));
                        rsPmtRec.Set_Fields("Service", strSrvc);
                        if (rsBillRec.Get_Fields_Int32(strSrvc + "LienRecordNumber") != 0)
                        {
                            rsPmtRec.Set_Fields("Lien",true);
                            rsPmtRec.Set_Fields("BillKey", rsBillRec.Get_Fields_Int32(strSrvc + "LienRecordNumber"));
                        }
                        else
                        {
                            rsPmtRec.Set_Fields("Lien",false);
                            rsPmtRec.Set_Fields("BillKey", rsBillRec.Get_Fields_Int32("Bill"));
                        }

                        rsPmtRec.Set_Fields("CHGIntNumber",0);
                        rsPmtRec.Set_Fields("CHGIntDate",FCConvert.ToDateTime(0));
                        rsPmtRec.Set_Fields("ActualSystemDate",DateTime.Now);
                        rsPmtRec.Set_Fields("EffectiveInterestDate",rsData.Get_Fields_DateTime("EffPmtDate"));
                        rsPmtRec.Set_Fields("RecordedTransactionDate",rsData.Get_Fields_DateTime("EffPmtDate"));
                        rsPmtRec.Set_Fields("Teller","");
                        rsPmtRec.Set_Fields("Reference","");
                        rsPmtRec.Set_Fields("Period","A");
                        rsPmtRec.Set_Fields("Code","P");
                        if (modExtraModules.IsThisCR())
                        {
                            rsPmtRec.Set_Fields("ReceiptNumber",-1);
                        }
                        else
                        {
                            rsPmtRec.Set_Fields("ReceiptNumber",0);
                        }
                        rsPmtRec.Set_Fields("Principal",modGlobal.Round(dblCurPrin,2));
                        rsPmtRec.Set_Fields("PreLienInterest",modGlobal.Round(dblPLInt,2));
                        rsPmtRec.Set_Fields("CurrentInterest",modGlobal.Round(dblCurInt,2));
                        rsPmtRec.Set_Fields("Tax",modGlobal.Round(dblCurTax,2));
                        rsPmtRec.Set_Fields("LienCost",modGlobal.Round(dblCurCost,2));
                        rsPmtRec.Set_Fields("TransNumber",0);
                        rsPmtRec.Set_Fields("PaidBy", strPaidBy);
                        rsPmtRec.Set_Fields("Comments","This was a batch transaction");
                        rsPmtRec.Set_Fields("CashDrawer","Y");
                        rsPmtRec.Set_Fields("GeneralLedger","Y");
                        rsPmtRec.Set_Fields("BudgetaryAccountNumber","");

                        if (!rsPmtRec.Update())
                        {
                            MessageBox.Show(
                                "Error " + rsPmtRec.ErrorDescription + " for account number " +
                                rsData.Get_Fields_Int32("AccountNumber").ToString(), "Update Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);

                        }
                        else
                        {
                            if (dblXtraCurInt != 0)
                            {
                                rsPmtRec.FindFirst("BillKey = " + rsData.Get_Fields_Int32("BillKey").ToString());
                                if (CreateBatchUTCHGINTRecord(ref rsPmtRec, dblXtraCurInt))
                                {
                                    rsPmtRec.Edit();
                                    rsPmtRec.Set_Fields("CHGIntNumber",lngBatchCHGINTNumber);
                                    rsPmtRec.Update();
                                }
                                else
                                {
                                    MessageBox.Show(
                                        "Error creating charged interest record for account number " +
                                        rsData.Get_Fields_Int32("AccountNumber"), "Batch CHGINT Error",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }

                            if (!modExtraModules.IsThisCR())
                            {
                                if (rsBillRec.Get_Fields_Int32(strWS + "LienRecordNumber") != 0)
                                {
                                    rsLien.OpenRecordset(
                                        "Select * from lienrec where lienrecordnumber = " +
                                        rsBillRec.Get_Fields_Int32(strWS + "LienRecordNumber"), "UtilityBilling");
                                    if (!rsLien.EndOfFile())
                                    {
                                        rsLien.Edit();
                                        rsLien.Set_Fields("PrincipalPaid", rsLien.Get_Fields_Double("PrincipalPaid") + modGlobal.Round(dblCurPrin,2));
                                        rsLien.Set_Fields("InterestPaid",rsLien.Get_Fields_Double("InterestPaid") + modGlobal.Round(dblCurInt,2));
                                        rsLien.Set_Fields("CostsPaid",rsLien.Get_Fields_Double("CostsPaid") + modGlobal.Round(dblCurCost,2));
                                        rsLien.Set_Fields("PLIPaid",rsLien.Get_Fields_Double("PLIPaid") + modGlobal.Round(dblPLInt,2));
                                        rsLien.Update();
                                    }
                                }
                                else
                                {
                                    rsBillRec.Edit();
                                    rsBillRec.Set_Fields(strWS + "PrinPaid", rsBillRec.Get_Fields_Double(strWS + "PrinPaid") + modGlobal.Round(dblCurPrin,2));
                                    rsBillRec.Set_Fields(strWS + "IntPaid", rsBillRec.Get_Fields_Double(strWS + "IntPaid") + modGlobal.Round(dblCurInt,2));
                                    rsBillRec.Set_Fields(strWS + "CostPaid", rsBillRec.Get_Fields_Double(strWS + "CostPaid") + modGlobal.Round(dblCurCost,2));
                                    rsBillRec.Set_Fields(strWS + "TaxPaid",rsBillRec.Get_Fields_Double(strWS + "TaxPaid") + modGlobal.Round(dblCurTax,2));
                                    rsBillRec.Update();
                                }

                                rsData.Edit();
                                rsData.Set_Fields("Prin",dblCurPrin);
                                rsData.Set_Fields("Int",dblCurInt);
                                rsData.Set_Fields("Cost",dblCurCost);
                                rsData.Set_Fields("Tax", dblCurTax);
                                rsData.Set_Fields("Posted",true);
                                rsData.Set_Fields("PostedDate", DateTime.Now);
                                rsData.Update();
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show(
                            "Error processing account number " + rsData.Get_Fields_Int32("AccountNumber").ToString() +
                            ", no bill found", "Bill Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                    BatchTotal += rsData.Get_Fields_Double("ACHAmount");
                    rsData.MoveNext();
                }

                if (!modExtraModules.IsThisCR())
                {
                    strSQL = "Select * from tblAutoPay where ACHDone = 1 and not posted = 1 and BatchNumber = '" +
                             BatchNumber + "'";
                    rsPmtRec.OpenRecordset(strSQL, "UtilityBilling");
                    if (!rsPmtRec.EndOfFile())
                    {
                        if (MessageBox.Show(
                                "Some batch entries were not processed.\nDo you want to mark this batch as completed?",
                                "Unprocessed Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                            DialogResult.Yes)
                        {
                            while (!rsPmtRec.EndOfFile())
                            {
                                rsPmtRec.Edit();
                                rsPmtRec.Set_Fields("Posted",true);
                                rsPmtRec.Set_Fields("PostedDate",DateTime.Now);
                                rsPmtRec.Update();
                                rsPmtRec.MoveNext();
                            }
                        }
                    }
                }

                return true;
            }
            else
            {
                if (!modExtraModules.IsThisCR())
                {
                    strSQL = "Select * from tblAutoPay where ACHDone = 1 and not posted = 1 and batchnumber = '" +
                             BatchNumber + "'";
                    rsPmtRec.OpenRecordset(strSQL, "UtilityBilling");
                    if (!rsPmtRec.EndOfFile())
                    {
                        if (MessageBox.Show(
                                "There were no payments recorded.\nDo you want to mark this batch as completed?",
                                "Unprocessed Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                            DialogResult.Yes)
                        {
                            while (!rsPmtRec.EndOfFile())
                            {
                                rsPmtRec.Edit();
                                rsPmtRec.Set_Fields("Posted",true);
                                rsPmtRec.Set_Fields("PostedDate",DateTime.Now);
                                rsPmtRec.Update();
                                rsPmtRec.MoveNext();
                            }
                        }
                    }
                }

                return false;
            }
        }

        private bool CreateBatchUTCHGINTRecord(ref clsDRWrapper rsTemp, double dblInt)
        {
            DateTime dtStartingDate;
            DateTime dtEffectiveInterestDate;
            var rsChgInt = new clsDRWrapper();
            string strSQL = "";

            lngBatchCHGINTNumber = 0;
            strSQL = "Select * from paymentrec where account = 0";
            if (rsChgInt.OpenRecordset(strSQL, "UtilityBilling"))
            {
                rsChgInt.AddNew();
                rsChgInt.Set_Fields("AccountKey",rsTemp.Get_Fields_Int32("AccountKey"));
                rsChgInt.Set_Fields("BillNumber",rsTemp.Get_Fields_Int32("BillNumber"));
                rsChgInt.Set_Fields("Service",rsTemp.Get_Fields_String("Service"));
                rsChgInt.Set_Fields("BillKey",rsTemp.Get_Fields_Int32("BillKey"));
                rsChgInt.Set_Fields("Lien",rsTemp.Get_Fields_Boolean("Lien"));
                rsChgInt.Set_Fields("CHGIntNumber",0);
                rsChgInt.Set_Fields("CHGIntDate",dtIntPaidDate);
                rsChgInt.Set_Fields("ActualSystemDate",DateTime.Now);
                rsChgInt.Set_Fields("EffectiveInterestDate",rsTemp.Get_Fields_DateTime("EffectiveInterestDate"));
                rsChgInt.Set_Fields("RecordedTransactionDate",DateTime.Now);
                rsChgInt.Set_Fields("Teller",strTLRID);
                rsChgInt.Set_Fields("Reference","CHGINT");
                rsChgInt.Set_Fields("Period","A");
                rsChgInt.Set_Fields("Code","I");
                if (modExtraModules.IsThisCR())
                {
                    rsChgInt.Set_Fields("ReceiptNumber",-1);
                }
                else
                {
                    rsChgInt.Set_Fields("ReceiptNumber",0);
                }
                rsChgInt.Set_Fields("Principal",0);
                rsChgInt.Set_Fields("PreLienInterest",0);
                rsChgInt.Set_Fields("CurrentInterest",modGlobal.Round(dblXtraCurInt * -1,2));
                rsChgInt.Set_Fields("LienCost",0);
                rsChgInt.Set_Fields("Tax",0);
                rsChgInt.Set_Fields("TransNumber",0);
                rsChgInt.Set_Fields("PaidBy",strPaidBy);
                rsChgInt.Set_Fields("Comments","This was a batch transaction.");
                rsChgInt.Set_Fields("CashDrawer","Y");
                rsChgInt.Set_Fields("GeneralLedger","Y");
                rsChgInt.Set_Fields("BudgetaryAccountNumber","");
                if (rsChgInt.Update())
                {
                    lngBatchCHGINTNumber = rsChgInt.Get_Fields_Int32("ID");
                    return true;
                }
                else
                {
                    MessageBox.Show("Error " + rsChgInt.ErrorDescription, "Batch CHGINT Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                MessageBox.Show("Error " + rsChgInt.ErrorDescription, "Batch CHGINT Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
        }

        private void AdjustUTPaymentAmount(double dblNewAmount, bool boolDoNotQueryOverPay)
        {
            try
            {
                if (dblNewAmount >= dblCurInt + dblPLInt)
                {
                    dblNewAmount = dblNewAmount - (dblCurInt + dblPLInt);
                    if (dblNewAmount >= dblCurCost)
                    {
                        dblNewAmount = dblNewAmount - dblCurCost;
                        if (dblNewAmount >= dblCurTax)
                        {
                            dblNewAmount = dblNewAmount - dblCurTax;
                            if (dblNewAmount >= dblCurPrin)
                            {
                                dblNewAmount = modGlobal.Round(dblNewAmount - dblCurPrin, 2);
                            }
                            else
                            {
                                dblCurPrin = dblNewAmount;
                                dblNewAmount = 0;
                            }
                        }
                        else
                        {
                            dblCurPrin = 0;
                            dblCurTax = dblNewAmount;
                            dblNewAmount = 0;
                        }
                    }
                    else
                    {
                        dblCurPrin = 0;
                        dblCurTax = 0;
                        dblCurCost = dblNewAmount;
                        dblNewAmount = 0;
                    }
                }
                else if (dblNewAmount >= dblCurInt)
                {
                    dblNewAmount = dblNewAmount - dblCurInt;
                    dblPLInt = dblNewAmount;
                    dblNewAmount = 0;
                }
                else
                {
                    dblCurInt = dblNewAmount;
                    dblPLInt = 0;
                    dblCurPrin = 0;
                    dblCurCost = 0;
                    dblCurTax = 0;
                    dblNewAmount = 0;
                }

                if (modGlobal.Round(dblNewAmount, 2) > 0)
                {
                    dblCurPrin = dblCurPrin + dblNewAmount;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message, "Error Adjusting Payment Amount", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
