﻿using fecherFoundation;
using SharedApplication.Extensions;
using TWCL0000;

namespace Global
{
	public class clsPmtTypeSummary
	{
		//=========================================================
		private string mstrDesc = string.Empty;
		private string mstrCode = string.Empty;
		private double mdblPrin;
		private double mdblPrinHold;
		private double mdblInt;
		private double mdblIntHold;
		private double mdblPLI;
		private double mdblPLIHold;
		private double mdblCost;
		private double mdblCostHold;
		private double mdblTax;
		private double mdblTaxHold;

		public string Description
		{
			set
			{
				// Allow empty?
				mstrDesc = value;
			}
			get
			{
				string Description = "";
				Description = mstrDesc;
				return Description;
			}
		}

		public string Code
		{
			set
			{
				// Check for valid codes?
				mstrCode = value;
			}
			get
			{
				string Code = "";
				Code = mstrCode;
				return Code;
			}
		}

		public double Principal
		{
			get
			{
				double Principal = 0;
				Principal = mdblPrin;
				return Principal;
			}
		}

		public double Interest
		{
			get
			{
				double Interest = 0;
				Interest = mdblInt;
				return Interest;
			}
		}

		public double PreLienInterst
		{
			get
			{
				double PreLienInterst = 0;
				PreLienInterst = mdblPLI;
				return PreLienInterst;
			}
		}

		public double LienCost
		{
			get
			{
				double LienCost = 0;
				LienCost = mdblCost;
				return LienCost;
			}
		}

		public double Tax
		{
			get
			{
				double Tax = 0;
				Tax = mdblTax;
				return Tax;
			}
		}

		public double Total
		{
			get
			{
				double Total = 0;
				Total = modGlobal.Round(mdblPrin + mdblInt + mdblPLI + mdblCost + mdblTax, 2);
				return Total;
			}
		}

		public double TotalInt
		{
			get
			{
				double TotalInt = 0;
				TotalInt = modGlobal.Round(mdblInt, 2);
				return TotalInt;
			}
		}

		public double TotalNonInt
		{
			get
			{
				double TotalNonInt = 0;
				TotalNonInt = modGlobal.Round(mdblPrin + mdblPLI + mdblCost + mdblTax, 2);
				return TotalNonInt;
			}
		}

		public bool IsEmpty
		{
			get
			{
				bool IsEmpty = false;
				IsEmpty = FCConvert.CBool(mdblPrin == 0.0 && mdblPLI == 0.0 && mdblInt == 0.0 && mdblCost == 0.0 && mdblTax == 0.0);
				return IsEmpty;
			}
		}

		public void AddPayment(double dblPrin, double dblInt, double dblPLI = 0.0, double dblCost = 0.0, double dblTax = 0.0)
		{
			mdblPrinHold += dblPrin;
			mdblPLIHold += dblPLI;
			mdblIntHold += dblInt;
			mdblCostHold += dblCost;
			mdblTaxHold += dblTax;
		}

        public void AddPayment(decimal principal, decimal interest, decimal prelieninterest, decimal cost,
            decimal tax = 0M)
        {
            mdblPrinHold += principal.ToDouble();
            mdblPLIHold += prelieninterest.ToDouble();
            mdblIntHold += interest.ToDouble();
            mdblCostHold += cost.ToDouble();
            mdblTaxHold += tax.ToDouble();
        }

		public void SavePayments()
		{
			mdblPrin += mdblPrinHold;
			mdblPLI += mdblPLIHold;
			mdblInt += mdblIntHold;
			mdblCost += mdblCostHold;
			mdblTax += mdblTaxHold;
			mdblPrinHold = 0.0;
			mdblPLIHold = 0.0;
			mdblIntHold = 0.0;
			mdblCostHold = 0.0;
			mdblTaxHold = 0.0;
		}

		public void DiscardPayments()
		{
			mdblPrinHold = 0.0;
			mdblPLIHold = 0.0;
			mdblIntHold = 0.0;
			mdblCostHold = 0.0;
			mdblTaxHold = 0.0;
		}
	}
}
