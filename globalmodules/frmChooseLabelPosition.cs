﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmChooseLabelPosition.
	/// </summary>
	public partial class frmChooseLabelPosition : BaseForm
	{
		private System.ComponentModel.IContainer components;

		public frmChooseLabelPosition()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseLabelPosition InstancePtr
		{
			get
			{
				return (frmChooseLabelPosition)Sys.GetInstance(typeof(frmChooseLabelPosition));
			}
		}

		protected frmChooseLabelPosition _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ************************
		/// </summary>
		/// <summary>
		/// Created by Corey Gray 5/7/2004
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// ************************
		/// </summary>
		/// <summary>
		/// This form accepts the number of labels down and across a page
		/// </summary>
		/// <summary>
		/// The user then clicks on the label they want to start printing from
		/// </summary>
		/// <summary>
		/// f12 and double clicking do the same thing
		/// </summary>
		/// <summary>
		/// F12 will only exit if a cell is chosen
		/// </summary>
		/// <summary>
		/// If the user hits escape, chooses exit from the menu, or closes the window, the form will
		/// </summary>
		/// <summary>
		/// return a label number of -1 meaning cancel
		/// </summary>
		/// <summary>
		/// otherwise it will return the label number which is NOT zero based
		/// </summary>
		/// <summary>
		/// the report should then print Label Position Returned - 1 blank labels before printing data.
		/// </summary>
		int intLabelToUse;
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Init(int intRows, int intCols)
		{
			short Init = 0;
			//FC:FINAL:CHN - issue #1619: Remove Header text.
			this.HeaderText.Text = "";
			// Accepts the number of labels down and across as parameters
			intLabelToUse = -1;
			GridLabels.Rows = intRows;
			GridLabels.Cols = intCols;
			//FC:FINAL:RPU: #i1522 - Set ShowFocusCell property to true
			GridLabels.ShowFocusCell = true;
			SetupGridLabels();
			Init = 0;
			this.Show(FormShowEnum.Modal);
			Init = FCConvert.ToInt16(intLabelToUse);
			return Init;
		}

		private void ResizeGridLabels()
		{
			int GridWidth = 0;
			int GridHeight = 0;
			int x;
			//FC:FINAL:CHN - issue #1657: Fix scrollbar of Grid.
			// GridHeight = GridLabels.HeightOriginal;
			GridHeight = GridLabels.HeightOriginal - GridLabels.Rows * 8;
			GridWidth = GridLabels.WidthOriginal;
			for (x = 0; x <= GridLabels.Rows - 1; x++)
			{
				GridLabels.RowHeight(x, FCConvert.ToInt32(FCConvert.ToDouble(GridHeight) / GridLabels.Rows));
			}
			// x
			for (x = 0; x <= GridLabels.Cols - 1; x++)
			{
				GridLabels.ColWidth(x, FCConvert.ToInt32(FCConvert.ToDouble(GridWidth) / GridLabels.Cols));
			}
			// x
		}

		private void frmChooseLabelPosition_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmChooseLabelPosition_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseLabelPosition properties;
			//frmChooseLabelPosition.ScaleWidth	= 9045;
			//frmChooseLabelPosition.ScaleHeight	= 7740;
			//frmChooseLabelPosition.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void frmChooseLabelPosition_Resize(object sender, System.EventArgs e)
		{
			ResizeGridLabels();
		}

		private void SetupGridLabels()
		{
			// Put text "Label " and the label number in each cell
			int x;
			int intC;
			int intR;
			x = 0;
			for (intR = 0; intR <= GridLabels.Rows - 1; intR++)
			{
				for (intC = 0; intC <= GridLabels.Cols - 1; intC++)
				{
					x += 1;
					GridLabels.TextMatrix(intR, intC, "Label " + FCConvert.ToString(x));
					GridLabels.Cell(FCGrid.CellPropertySettings.flexcpData, intR, intC, x);
				}
				// intC
			}
			// intR
		}

		private void GridLabels_DblClick(object sender, System.EventArgs e)
		{
			// return the label chosen
			intLabelToUse = FCConvert.ToInt32(GridLabels.Cell(FCGrid.CellPropertySettings.flexcpData, GridLabels.Row, GridLabels.Col));
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			intLabelToUse = -1;
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// return the label chosen if one was
			if (GridLabels.Col >= 0 && GridLabels.Row >= 0)
			{
				intLabelToUse = FCConvert.ToInt32(GridLabels.Cell(FCGrid.CellPropertySettings.flexcpData, GridLabels.Row, GridLabels.Col));
				Close();
			}
			else
			{
				MessageBox.Show("You must choose a label to continue", "No Label Chosen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		private void GridLabels_Click(object sender, EventArgs e)
		{
		}
	}
}
