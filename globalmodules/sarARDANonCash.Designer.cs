﻿namespace Global
{
	/// <summary>
	/// Summary description for sarARDANonCash.
	/// </summary>
	partial class sarARDANonCash
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarARDANonCash));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAbatement = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDiscount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOther = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldBillType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAbatement1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOther1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAbatement2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAbatement3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOther2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOther3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalAbatement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDiscount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAbatement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAbatement1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOther1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAbatement2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAbatement3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOther2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOther3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAbatement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBillType,
				this.fldType1,
				this.fldAbatement1,
				this.fldDiscount1,
				this.fldOther1,
				this.fldTotal1,
				this.fldType2,
				this.fldType3,
				this.fldAbatement2,
				this.fldAbatement3,
				this.fldDiscount2,
				this.fldDiscount3,
				this.fldOther2,
				this.fldOther3,
				this.fldTotal2,
				this.fldTotal3
			});
			this.Detail.Height = 0.4583333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader
			});
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblBillType,
				this.lblType,
				this.lblAbatement,
				this.lblDiscount,
				this.lblOther,
				this.lblTotal,
				this.lnHeader
			});
			this.GroupHeader1.Height = 0.2604167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lnTotal,
				this.fldTotalAbatement,
				this.fldTotalDiscount,
				this.fldTotalOther,
				this.fldTotalTotal,
				this.lblTotals
			});
			this.GroupFooter1.Height = 0.2083333F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "- - - - - Non Cash - - - - -";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 9.375F;
			// 
			// lblBillType
			// 
			this.lblBillType.Height = 0.1875F;
			this.lblBillType.HyperLink = null;
			this.lblBillType.Left = 0F;
			this.lblBillType.Name = "lblBillType";
			this.lblBillType.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblBillType.Text = "Year";
			this.lblBillType.Top = 0.0625F;
			this.lblBillType.Width = 0.625F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 1F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblType.Text = "Type";
			this.lblType.Top = 0.0625F;
			this.lblType.Width = 0.5625F;
			// 
			// lblAbatement
			// 
			this.lblAbatement.Height = 0.1875F;
			this.lblAbatement.HyperLink = null;
			this.lblAbatement.Left = 3.125F;
			this.lblAbatement.Name = "lblAbatement";
			this.lblAbatement.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAbatement.Text = "Abatement";
			this.lblAbatement.Top = 0.0625F;
			this.lblAbatement.Width = 1.0625F;
			// 
			// lblDiscount
			// 
			this.lblDiscount.Height = 0.1875F;
			this.lblDiscount.HyperLink = null;
			this.lblDiscount.Left = 4.8125F;
			this.lblDiscount.Name = "lblDiscount";
			this.lblDiscount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDiscount.Text = "Discount";
			this.lblDiscount.Top = 0.0625F;
			this.lblDiscount.Width = 1.0625F;
			// 
			// lblOther
			// 
			this.lblOther.Height = 0.1875F;
			this.lblOther.HyperLink = null;
			this.lblOther.Left = 6.5F;
			this.lblOther.Name = "lblOther";
			this.lblOther.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblOther.Text = "Other";
			this.lblOther.Top = 0.0625F;
			this.lblOther.Width = 1.0625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 8.1875F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.0625F;
			this.lblTotal.Width = 1.1875F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.25F;
			this.lnHeader.Width = 6.9375F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 6.9375F;
			this.lnHeader.Y1 = 0.25F;
			this.lnHeader.Y2 = 0.25F;
			// 
			// fldBillType
			// 
			this.fldBillType.Height = 0.1875F;
			this.fldBillType.Left = 0F;
			this.fldBillType.Name = "fldBillType";
			this.fldBillType.Style = "font-family: \'Tahoma\'";
			this.fldBillType.Text = null;
			this.fldBillType.Top = 0F;
			this.fldBillType.Width = 0.625F;
			// 
			// fldType1
			// 
			this.fldType1.Height = 0.1875F;
			this.fldType1.Left = 1F;
			this.fldType1.Name = "fldType1";
			this.fldType1.Style = "font-family: \'Tahoma\'";
			this.fldType1.Text = "Prin";
			this.fldType1.Top = 0F;
			this.fldType1.Width = 0.5625F;
			// 
			// fldAbatement1
			// 
			this.fldAbatement1.Height = 0.1875F;
			this.fldAbatement1.Left = 3.125F;
			this.fldAbatement1.Name = "fldAbatement1";
			this.fldAbatement1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAbatement1.Text = "0.00";
			this.fldAbatement1.Top = 0F;
			this.fldAbatement1.Width = 1.0625F;
			// 
			// fldDiscount1
			// 
			this.fldDiscount1.Height = 0.1875F;
			this.fldDiscount1.Left = 4.8125F;
			this.fldDiscount1.Name = "fldDiscount1";
			this.fldDiscount1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldDiscount1.Text = "0.00";
			this.fldDiscount1.Top = 0F;
			this.fldDiscount1.Width = 1.0625F;
			// 
			// fldOther1
			// 
			this.fldOther1.Height = 0.1875F;
			this.fldOther1.Left = 6.5F;
			this.fldOther1.Name = "fldOther1";
			this.fldOther1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOther1.Text = "0.00";
			this.fldOther1.Top = 0F;
			this.fldOther1.Width = 1.0625F;
			// 
			// fldTotal1
			// 
			this.fldTotal1.Height = 0.1875F;
			this.fldTotal1.Left = 8.1875F;
			this.fldTotal1.Name = "fldTotal1";
			this.fldTotal1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal1.Text = "0.00";
			this.fldTotal1.Top = 0F;
			this.fldTotal1.Width = 1.1875F;
			// 
			// fldType2
			// 
			this.fldType2.Height = 0.1875F;
			this.fldType2.Left = 1F;
			this.fldType2.Name = "fldType2";
			this.fldType2.Style = "font-family: \'Tahoma\'";
			this.fldType2.Text = "Int";
			this.fldType2.Top = 0.125F;
			this.fldType2.Width = 0.5625F;
			// 
			// fldType3
			// 
			this.fldType3.Height = 0.1875F;
			this.fldType3.Left = 1F;
			this.fldType3.Name = "fldType3";
			this.fldType3.Style = "font-family: \'Tahoma\'";
			this.fldType3.Text = "Tax";
			this.fldType3.Top = 0.25F;
			this.fldType3.Width = 0.5625F;
			// 
			// fldAbatement2
			// 
			this.fldAbatement2.Height = 0.1875F;
			this.fldAbatement2.Left = 3.125F;
			this.fldAbatement2.Name = "fldAbatement2";
			this.fldAbatement2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAbatement2.Text = "0.00";
			this.fldAbatement2.Top = 0.125F;
			this.fldAbatement2.Width = 1.0625F;
			// 
			// fldAbatement3
			// 
			this.fldAbatement3.Height = 0.1875F;
			this.fldAbatement3.Left = 3.125F;
			this.fldAbatement3.Name = "fldAbatement3";
			this.fldAbatement3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAbatement3.Text = "0.00";
			this.fldAbatement3.Top = 0.25F;
			this.fldAbatement3.Width = 1.0625F;
			// 
			// fldDiscount2
			// 
			this.fldDiscount2.Height = 0.1875F;
			this.fldDiscount2.Left = 4.8125F;
			this.fldDiscount2.Name = "fldDiscount2";
			this.fldDiscount2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldDiscount2.Text = "0.00";
			this.fldDiscount2.Top = 0.125F;
			this.fldDiscount2.Width = 1.0625F;
			// 
			// fldDiscount3
			// 
			this.fldDiscount3.Height = 0.1875F;
			this.fldDiscount3.Left = 4.8125F;
			this.fldDiscount3.Name = "fldDiscount3";
			this.fldDiscount3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldDiscount3.Text = "0.00";
			this.fldDiscount3.Top = 0.25F;
			this.fldDiscount3.Width = 1.0625F;
			// 
			// fldOther2
			// 
			this.fldOther2.Height = 0.1875F;
			this.fldOther2.Left = 6.5F;
			this.fldOther2.Name = "fldOther2";
			this.fldOther2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOther2.Text = "0.00";
			this.fldOther2.Top = 0.125F;
			this.fldOther2.Width = 1.0625F;
			// 
			// fldOther3
			// 
			this.fldOther3.Height = 0.1875F;
			this.fldOther3.Left = 6.5F;
			this.fldOther3.Name = "fldOther3";
			this.fldOther3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOther3.Text = "0.00";
			this.fldOther3.Top = 0.25F;
			this.fldOther3.Width = 1.0625F;
			// 
			// fldTotal2
			// 
			this.fldTotal2.Height = 0.1875F;
			this.fldTotal2.Left = 8.1875F;
			this.fldTotal2.Name = "fldTotal2";
			this.fldTotal2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal2.Text = "0.00";
			this.fldTotal2.Top = 0.125F;
			this.fldTotal2.Width = 1.1875F;
			// 
			// fldTotal3
			// 
			this.fldTotal3.Height = 0.1875F;
			this.fldTotal3.Left = 8.1875F;
			this.fldTotal3.Name = "fldTotal3";
			this.fldTotal3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal3.Text = "0.00";
			this.fldTotal3.Top = 0.25F;
			this.fldTotal3.Width = 1.1875F;
			// 
			// lnTotal
			// 
			this.lnTotal.Height = 0F;
			this.lnTotal.Left = 1.5F;
			this.lnTotal.LineWeight = 1F;
			this.lnTotal.Name = "lnTotal";
			this.lnTotal.Top = 0F;
			this.lnTotal.Width = 5.4375F;
			this.lnTotal.X1 = 1.5F;
			this.lnTotal.X2 = 6.9375F;
			this.lnTotal.Y1 = 0F;
			this.lnTotal.Y2 = 0F;
			// 
			// fldTotalAbatement
			// 
			this.fldTotalAbatement.Height = 0.1875F;
			this.fldTotalAbatement.Left = 2.375F;
			this.fldTotalAbatement.Name = "fldTotalAbatement";
			this.fldTotalAbatement.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalAbatement.Text = "0.00";
			this.fldTotalAbatement.Top = 0F;
			this.fldTotalAbatement.Width = 1.0625F;
			// 
			// fldTotalDiscount
			// 
			this.fldTotalDiscount.Height = 0.1875F;
			this.fldTotalDiscount.Left = 3.5F;
			this.fldTotalDiscount.Name = "fldTotalDiscount";
			this.fldTotalDiscount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalDiscount.Text = "0.00";
			this.fldTotalDiscount.Top = 0F;
			this.fldTotalDiscount.Width = 1.0625F;
			// 
			// fldTotalOther
			// 
			this.fldTotalOther.Height = 0.1875F;
			this.fldTotalOther.Left = 4.625F;
			this.fldTotalOther.Name = "fldTotalOther";
			this.fldTotalOther.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalOther.Text = "0.00";
			this.fldTotalOther.Top = 0F;
			this.fldTotalOther.Width = 1.0625F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 5.75F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0F;
			this.fldTotalTotal.Width = 1.1875F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 1F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotals.Text = "Totals:";
			this.lblTotals.Top = 0F;
			this.lblTotals.Width = 0.5625F;
			// 
			// sarARDANonCash
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAbatement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAbatement1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOther1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAbatement2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAbatement3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOther2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOther3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAbatement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAbatement1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOther1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAbatement2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAbatement3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOther2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOther3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAbatement;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOther;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAbatement;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
	}
}
