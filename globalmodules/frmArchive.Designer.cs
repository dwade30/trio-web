//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPP0000
{
    /// <summary>
    /// Summary description for frmArchive.
    /// </summary>
    partial class frmArchive : BaseForm
    {
        public fecherFoundation.FCButton cmdNew;
        public FCGrid Grid;
        public fecherFoundation.FCLabel Label1;
        public fecherFoundation.FCToolStripMenuItem mnuFile;
        public fecherFoundation.FCToolStripMenuItem mnuRunArchive;
        public fecherFoundation.FCToolStripMenuItem mnuSepar2;
        public fecherFoundation.FCToolStripMenuItem mnuCreateArchive;
        public fecherFoundation.FCToolStripMenuItem mnuSepar;
        public fecherFoundation.FCToolStripMenuItem mnuExit;
        private FCButton cmdRunArchive;
        private FCButton cmdCreateArchive;

	 protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmArchive));
            this.cmdNew = new fecherFoundation.FCButton();
            this.Grid = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRunArchive = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCreateArchive = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdRunArchive = new fecherFoundation.FCButton();
            this.cmdCreateArchive = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRunArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateArchive)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdNew);
            this.BottomPanel.Location = new System.Drawing.Point(0, 316);
            this.BottomPanel.Size = new System.Drawing.Size(743, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(743, 256);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdRunArchive);
            this.TopPanel.Controls.Add(this.cmdCreateArchive);
            this.TopPanel.Size = new System.Drawing.Size(743, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCreateArchive, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRunArchive, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(106, 30);
            this.HeaderText.Text = "Archives";
            // 
            // cmdNew
            // 
            this.cmdNew.AppearanceKey = "acceptButton";
            this.cmdNew.Location = new System.Drawing.Point(299, 30);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdNew.Size = new System.Drawing.Size(128, 48);
            this.cmdNew.TabIndex = 2;
            this.cmdNew.Text = "New Archive";
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeColumnsMode = Wisej.Web.DataGridViewAutoSizeColumnsMode.AllCells;
            this.Grid.Cols = 4;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(30, 65);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(685, 171);
            this.Grid.TabIndex = 1;
            this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(190, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "CHOOSE AN ARCHIVE TO RUN";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRunArchive,
            this.mnuSepar2,
            this.mnuCreateArchive,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuRunArchive
            // 
            this.mnuRunArchive.Index = 0;
            this.mnuRunArchive.Name = "mnuRunArchive";
            this.mnuRunArchive.Text = "Run Archive";
            this.mnuRunArchive.Click += new System.EventHandler(this.mnuRunArchive_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuCreateArchive
            // 
            this.mnuCreateArchive.Index = 2;
            this.mnuCreateArchive.Name = "mnuCreateArchive";
            this.mnuCreateArchive.Text = "Create Archive";
            this.mnuCreateArchive.Click += new System.EventHandler(this.mnuCreateArchive_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 3;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdRunArchive
            // 
            this.cmdRunArchive.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRunArchive.Location = new System.Drawing.Point(509, 29);
            this.cmdRunArchive.Name = "cmdRunArchive";
            this.cmdRunArchive.Size = new System.Drawing.Size(92, 24);
            this.cmdRunArchive.TabIndex = 1;
            this.cmdRunArchive.Text = "Run Archive";
            this.cmdRunArchive.Click += new System.EventHandler(this.mnuRunArchive_Click);
            // 
            // cmdCreateArchive
            // 
            this.cmdCreateArchive.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCreateArchive.Location = new System.Drawing.Point(607, 29);
            this.cmdCreateArchive.Name = "cmdCreateArchive";
            this.cmdCreateArchive.Size = new System.Drawing.Size(108, 24);
            this.cmdCreateArchive.TabIndex = 1;
            this.cmdCreateArchive.Text = "Create Archive";
            this.cmdCreateArchive.Click += new System.EventHandler(this.mnuCreateArchive_Click);
            // 
            // frmArchive
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(743, 424);
            this.FillColor = 0;
            this.Name = "frmArchive";
            this.Text = "Archives";
            this.Load += new System.EventHandler(this.frmArchive_Load);
            this.Click += new System.EventHandler(this.mnuRunArchive_Click);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRunArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateArchive)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
	}
}