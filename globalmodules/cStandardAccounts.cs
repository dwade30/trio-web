﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cStandardAccounts
	{
		//=========================================================
		private string strUnliquidatedEncumbrance = string.Empty;
		private string strExpenseControl = string.Empty;
		private string strRevenueControl = string.Empty;
		private string strEncumbranceOffset = string.Empty;
		private string strFundBalance = string.Empty;
		private string strMiscCash = string.Empty;

		public string MiscCash
		{
			get
			{
				return strMiscCash;
			}
			set
			{
				strMiscCash = value;
			}
		}


		public string UnliquidatedEncumbrance
		{
			set
			{
				strUnliquidatedEncumbrance = value;
			}
			get
			{
				string UnliquidatedEncumbrance = "";
				UnliquidatedEncumbrance = strUnliquidatedEncumbrance;
				return UnliquidatedEncumbrance;
			}
		}

		public string ExpenseControl
		{
			set
			{
				strExpenseControl = value;
			}
			get
			{
				string ExpenseControl = "";
				ExpenseControl = strExpenseControl;
				return ExpenseControl;
			}
		}

		public string RevenueControl
		{
			set
			{
				strRevenueControl = value;
			}
			get
			{
				string RevenueControl = "";
				RevenueControl = strRevenueControl;
				return RevenueControl;
			}
		}

		public string EncumbranceOffset
		{
			set
			{
				strEncumbranceOffset = value;
			}
			get
			{
				string EncumbranceOffset = "";
				EncumbranceOffset = strEncumbranceOffset;
				return EncumbranceOffset;
			}
		}

		public string FundBalance
		{
			set
			{
				strFundBalance = value;
			}
			get
			{
				string FundBalance = "";
				FundBalance = strFundBalance;
				return FundBalance;
			}
		}
	}
}
