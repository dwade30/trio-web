﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for arUTLienPartialPayment.
	/// </summary>
	public partial class arUTLienPartialPayment : BaseSectionReport
	{
		// nObj = 1
		//   0	arUTLienPartialPayment	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/11/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/11/2004              *
		// ********************************************************
		string strMainText;
		DateTime dtPaymentDate;
		string strOwnerName = "";
		string strMuniName = "";
		string strState = "";
		int lngLineLen;
		string strSignerName = "";
        string strBillingYear = "";
		double dblPaymentAmount;
		string strService = "";

		public arUTLienPartialPayment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static arUTLienPartialPayment InstancePtr
		{
			get
			{
				return (arUTLienPartialPayment)Sys.GetInstance(typeof(arUTLienPartialPayment));
			}
		}

		protected arUTLienPartialPayment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			lngLineLen = 35;
			// this is how many underscores are in the printed lines
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				lblTownHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
			}
			else
			{
				lblTownHeader.Text = strMuniName;
			}
			lblTitleBar.Text = "WAIVER FORM FOR USE WITH PARTIAL PAYMENTS";
			lblLegalDescription.Text = "30-DAY NOTICES & UTILITY LIENS";
			strMainText = "    I, " + strOwnerName + ", do hereby acknowledge that I have voluntarily made a partial payment upon my " + strService + " charges due the ";
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + ", " + strState + " for bill number ";
			}
			else
			{
				strMainText += strMuniName + ", " + strState + " for bill number ";
			}
			strMainText += strBillingYear + " in the sum of " + Strings.Format(dblPaymentAmount, "$#,##0.00");
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += " and I agree that sum is accepted by said " + modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
			}
			else
			{
				strMainText += " and I agree that sum is accepted by said " + strMuniName;
			}
			strMainText += " as partial payment as aforesaid without, in any way, waiving the lien of said ";
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + " for said " + strService + " charges.";
			}
			else
			{
				strMainText += strMuniName + " for said " + strService + " charges.";
			}
			fldMainText.Text = strMainText;
			fldDateLine.Text = "Dated at " + strMuniName + ", " + strState;
			fldDateLine.Text = fldDateLine.Text + "                 " + Strings.Format(dtPaymentDate, "MM/dd/yyyy");
			fldWitnessLine.Text = Strings.StrDup(lngLineLen, "_");
			fldTaxPayerLine.Text = Strings.StrDup(lngLineLen, "_");
			fldWitnessName.Text = strSignerName;
			fldTaxPayerName.Text = strOwnerName;
		}

		public void Init(string strPassMuni, string strPassState, double dblPassPaymentAmount, string strPassName1, string strPassBillingYear, DateTime dtPassPayDate, string strPassSignerName, string strPassService, string strPassMapLot = "", int lngPassAccount = 0)
		{
			// this routine will set all of the variables needed
			strMuniName = strPassMuni;
			strState = strPassState;
			strOwnerName = strPassName1;
			strBillingYear = strPassBillingYear;
			dblPaymentAmount = dblPassPaymentAmount;
			dtPaymentDate = dtPassPayDate;
			strSignerName = strPassSignerName;
			if (strPassService == "W")
			{
				strService = "water";
			}
			else
			{
				strService = "sewer";
			}
			// map lot
			lblMapLot.Text = strPassMapLot;
			// account number
			if (lngPassAccount > 0)
			{
				// MAL@20081022: Removed conversion to integer because long account numbers cause overflow error
				// Tracker Reference: 15796
				// fldAccount.Text = PadToString(CInt(lngPassAccount), 5)
				fldAccount.Text = modGlobal.PadToString_8(lngPassAccount, 5);
			}
			else
			{
				fldAccount.Text = "";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SetStrings();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void arUTLienPartialPayment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arUTLienPartialPayment.Caption	= "Partial Payment Waiver";
			//arUTLienPartialPayment.Icon	= "arUTLienPartialPayment.dsx":0000";
			//arUTLienPartialPayment.Left	= 0;
			//arUTLienPartialPayment.Top	= 0;
			//arUTLienPartialPayment.Width	= 11880;
			//arUTLienPartialPayment.Height	= 8595;
			//arUTLienPartialPayment.StartUpPosition	= 3;
			//arUTLienPartialPayment.SectionData	= "arUTLienPartialPayment.dsx":058A;
			//End Unmaped Properties
		}
	}
}
