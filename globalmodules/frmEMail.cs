﻿using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
    /// <summary>
    /// Summary description for frmEMail.
    /// </summary>
    public partial class frmEMail : BaseForm
    {
        public frmEMail()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.ImageSend = new System.Collections.Generic.List<fecherFoundation.FCPictureBox>();
            this.ImageSend.AddControlArrayElement(ImageSend_0, 0);
            this.ImageSend.AddControlArrayElement(ImageSend_1, 1);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmEMail InstancePtr
        {
            get
            {
                return (frmEMail)Sys.GetInstance(typeof(frmEMail));
            }
        }

        protected frmEMail _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By
        // Date
        // ********************************************************
        //FC:FINAL:RPU:#1255 - Add a flag to retain if the form was opened from general entry
        public bool fromGeneralEntry = false;

        private struct EMailAttachment
        {
            public string strFullName;
            public string strName;
            // VBto upgrade warning: strFileSize As string	OnWrite(string, short)
            public string strFileSize;
            public string strFileNewName;
            public bool boolDelete;
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public EMailAttachment(int unusedParam)
            {
                this.strFileNewName = String.Empty;
                this.strFullName = String.Empty;
                this.strFileSize = String.Empty;
                this.boolDelete = false;
                this.strName = String.Empty;
            }
        };
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private SmtpClient smtpClient = new SmtpClient();
        //private MailMessage mailMessage = new MailMessage();
        private SmtpClient smtpClient_AutoInitialized;

        private SmtpClient smtpClient
        {
            get
            {
                if (smtpClient_AutoInitialized == null)
                {
                    smtpClient_AutoInitialized = new SmtpClient();
                }
                return smtpClient_AutoInitialized;
            }
            set
            {
                smtpClient_AutoInitialized = value;
            }
        }

        private MailMessage mailMessage_AutoInitialized;

        private MailMessage mailMessage
        {
            get
            {
                if (mailMessage_AutoInitialized == null)
                {
                    mailMessage_AutoInitialized = new MailMessage();
                }
                return mailMessage_AutoInitialized;
            }
            set
            {
                mailMessage_AutoInitialized = value;
            }
        }

        private bool boolFailure;
        private bool boolSentAtLeastOne;
        private EMailAttachment[] strAryAttach = null;
        private bool boolShowModal;
        private int lngCurrEmail;
        private bool boolNoConfigurationWarning;
        private bool boolSendAsHTML;
        const string CNSTDefaultMailPassword = "Tr!0FuseMail1234";
        const int CNSTGRIDNAMESCOLMISC = 3;
        //private cEMailService emailservice = new cEMailService();
        private cEmailConfig curConfig;

        public bool Init(string strAttachments = "", string strRecipientList = "", string strSubject = "", string strBody = "", bool boolDisclaimer = false, bool boolModal = false, bool boolDeleteOriginalAttachments = false, bool boolDontShowConfigurationWarning = false, bool boolSendDontShow = false, bool blnShowSuccess = true, bool blnPersonalSignature = false, bool blnCompanySignature = false, bool blnHTML = false, bool showAsModalForm = false, bool generalEntry = false)
        {
            bool Init = false;
            // accepts a comma delimited list of files to attach
            // Attachements are ; delimited if you want to give it a different name. So Filename1;NewName,Filename2 etc.
            // accepts a comma delimited list of addresses/quicknames.  First one is the recipient.  the rest go in cc
            // boolDeleteOriginalAttachments if set to true will cuase the files passed in as arguments only, to be deleted after a successful send
            // returns false if form could not load or something wrong with attachments etc
            string[] strAryRecip = null;
            string[] strAryTemp = null;
            int x;
            string strCC;
            string strNewName = "";
            strAryAttach = new EMailAttachment[1 + 1];
            //FC:FINAL:RPU: #1255 - Add a flag to retain if the form was opened from general entry
            fromGeneralEntry = generalEntry;
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            strAryAttach[1] = new EMailAttachment(0);
            boolNoConfigurationWarning = boolDontShowConfigurationWarning;
            boolSendAsHTML = blnHTML;
            lngCurrEmail = 0;
            strAryAttach[1].strFullName = "";
            strAryAttach[1].strFileSize = "";
            strAryAttach[1].strName = "";
            strAryAttach[1].boolDelete = false;
            strAryAttach[1].strFileNewName = "";
            boolFailure = false;
            boolSentAtLeastOne = false;
            Init = true;
            //smtpMail = new AOSMTPLib.Mail();
            //smtpMail.Reset();
            //smtpMail.RegisterKey = "AM-XS1088813180-00543-85974BE0A264AB51DDD95341712D1CB8";
            strCC = "";
            if (strRecipientList != string.Empty)
            {
                strAryRecip = Strings.Split(strRecipientList, ",", -1, CompareConstants.vbTextCompare);
                for (x = 0; x <= Information.UBound(strAryRecip, 1); x++)
                {
                    if (x == 0)
                    {
                        txtMailTo.Text = strAryRecip[x];
                    }
                    else
                    {
                        strCC += strAryRecip[x] + ", ";
                    }
                }
                // x
                strCC = Strings.Trim(strCC);
                if (strCC != string.Empty)
                {
                    strCC = Strings.Mid(strCC, 1, strCC.Length - 1);
                }
                txtCarbonCopy.Text = strCC;
            }
            if (Strings.Trim(strAttachments) != string.Empty)
            {
                string strFileName = "";
                int intIndex;
                double dblSize = 0;
                FileInfo fl = null;
                strAryRecip = Strings.Split(strAttachments, ",", -1, CompareConstants.vbTextCompare);
                for (x = 0; x <= Information.UBound(strAryRecip, 1); x++)
                {
                    strAryTemp = Strings.Split(strAryRecip[x], ";", -1, CompareConstants.vbTextCompare);
                    strFileName = strAryTemp[0];
                    strNewName = Information.UBound(strAryTemp, 1) > 0 ? Strings.Trim(strAryTemp[1]) : "";
                    strFileName = Path.GetFullPath(strFileName);
                    if (!File.Exists(strFileName))
                    {
                        FCMessageBox.Show("File " + strFileName + " not found", MsgBoxStyle.Critical, "File Not Found");
                        Init = false;
                        return Init;
                    }
                    fl = new FileInfo(strFileName);
                    if (Information.UBound(strAryAttach, 1) == 1)
                    {
                        if (strAryAttach[1].strFullName != string.Empty)
                        {
                            Array.Resize(ref strAryAttach, 2 + 1);
                        }
                    }
                    else
                    {
                        Array.Resize(ref strAryAttach, Information.UBound(strAryAttach, 1) + 1 + 1);
                    }
                    intIndex = Information.UBound(strAryAttach, 1);
                    dblSize = FCConvert.ToInt32(fl.Length) / 1024;
                    if (dblSize >= 1000)
                    {
                        dblSize /= 1024;
                        if (dblSize < 1000)
                        {
                            strAryAttach[intIndex].strFileSize = Strings.Format(dblSize, "0.00") + "M";
                        }
                        else
                        {
                            dblSize /= 1024;
                            strAryAttach[intIndex].strFileSize = Strings.Format(dblSize, "0.00") + "G";
                        }
                    }
                    else
                    {
                        strAryAttach[intIndex].strFileSize = Strings.Format(dblSize, "0.00") + "k";
                    }
                    strAryAttach[intIndex].strFileNewName = strNewName;
                    strAryAttach[intIndex].strFullName = strFileName;
                    strAryAttach[intIndex].strName = strNewName == string.Empty ? Path.GetFileNameWithoutExtension(strFileName) : strNewName;
                    strAryAttach[intIndex].strFileNewName = strAryAttach[intIndex].strName;
                    strAryAttach[intIndex].boolDelete = boolDeleteOriginalAttachments;
                }
                // x
                fl = null;
            }
            ReShowAttachments();
            if (strSubject != string.Empty)
            {
                txtSubject.Text = strSubject;
            }
            if (strBody != string.Empty)
            {
                rtbMessage.Text = strBody;
            }
            chkDisclaimer.CheckState = boolDisclaimer ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked;
            chkSignature.CheckState = blnPersonalSignature ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked;
            chkCompanySignature.CheckState = blnCompanySignature ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked;
            boolShowModal = boolModal;
            if (LoadConfiguration())
            {
                if (!boolSendDontShow)
                {
                    //FC:FINAL:SBE - add new flag for modal. Show Email form as modal, when it is opened from the main screen (Gear menu)
                    //if (!boolModal)
                    if (!showAsModalForm)
                    {
                        this.Show(App.MainForm);
                    }
                    else
                    {
                        ////FC:FINAL:AM: always show in tab
                        ////this.Show(FormShowEnum.Modal);
                        //this.Show(App.MainForm);
                        this.Show(FormShowEnum.Modal);
                    }
                }
                else
                {
                    SendMail(blnShowSuccess);
                }
            }
            else
            {
                Init = false;
            }
            Init = !boolFailure || boolSentAtLeastOne;
            return Init;
        }

        private void frmEMail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Escape)
            {
                KeyCode = 0;
                mnuExit_Click();
            }
        }

        private void frmEMail_Load(object sender, System.EventArgs e)
        {

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            lblCarbonCopy.FontSize = 8;
            ImageSend[0].ImageSource = ImageList1.Images[11 - 1].ImageSource;
            ImageSend[1].ImageSource = ImageList1.Images[11 - 1].ImageSource;
            ImageSend[0].Visible = true;
            ImageSend[1].Visible = false;
            SetupGridNames();
            LoadGridNames();
        }

        private bool LoadConfiguration()
        {
            bool LoadConfiguration = false;
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cEmailConfig eConfig;
                cEMailService emailservice = new cEMailService();
                LoadConfiguration = false;
                eConfig = emailservice.GetEffectiveEMailConfigByUserID(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                if (eConfig.SMTPPort == 0)
                {
                    eConfig.SMTPPort = 25;
                }
                smtpClient.Host = eConfig.SMTPHost;
                smtpClient.Credentials = new NetworkCredential(eConfig.UserName, eConfig.Password);
                if (!String.IsNullOrEmpty(eConfig.DefaultFromAddress))
                {
                    try
                    {
                        //FC:FINAL:AM:#3770 - remove spaces from the email address
                        //mailMessage.From = new MailAddress(eConfig.DefaultFromAddress, eConfig.DefaultFromName);
                        mailMessage.From = new MailAddress(eConfig.DefaultFromAddress.Replace(" ", ""), eConfig.DefaultFromName);
                    }
                    catch (Exception ex)
                    {
                        StaticSettings.GlobalTelemetryService.TrackException(ex);
                        FCMessageBox.Show("There is an issue with the email address entered in Email Configuration. Please correct before sending any email messages through TRIO", MsgBoxStyle.Exclamation);
                    }
                }
                smtpClient.Port = eConfig.SMTPPort > 0 ? eConfig.SMTPPort : 25;
                curConfig = eConfig;
                emailservice.Initialize(ref eConfig);

                LoadConfiguration = true;

                if (String.IsNullOrEmpty(eConfig.DefaultFromAddress))
                {
                    FCMessageBox.Show("A user address has not been provided" + "\r\n" + "Without an address E-mail cannot be sent", MsgBoxStyle.Exclamation, "No Address");
                }
                else if (Strings.Trim(smtpClient.Host) == string.Empty)
                {
                    if (!boolNoConfigurationWarning)
                    {
                        FCMessageBox.Show("E-mail is not configured on this system" + "\r\n" + "Spam and junk mail filters may prevent E-mail from being received", MsgBoxStyle.Exclamation, "Not Configured");
                    }
                }
                return LoadConfiguration;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In Load Configuration", MsgBoxStyle.Critical, "Error");
            }
            return LoadConfiguration;
        }

        private void GetRecipient()
        {
            try
            {
                // On Error GoTo ErrorHandler
                string strReturn;
                int lngID;
                bool boolContact;
                clsDRWrapper rsLoad = new clsDRWrapper();
                txtMailTo.Focus();
                lngID = 0;
                strReturn = "";
                boolContact = frmEmailChooseContactGroup.InstancePtr.Init(true, true, ref lngID, ref strReturn, false);
                if (lngID > 0)
                {
                    if (strReturn == string.Empty)
                    {
                        if (boolContact)
                        {
                            // look up email address
                            rsLoad.OpenRecordset("select * from emailcontacts where contactid = " + FCConvert.ToString(lngID), "SystemSettings");
                            if (!rsLoad.EndOfFile())
                            {
                                strReturn = FCConvert.ToString(rsLoad.Get_Fields_String("email"));
                            }
                        }
                        else
                        {
                            // group
                            rsLoad.OpenRecordset("select * from emailcontacts inner join emailassociations on (emailcontacts.contactid = emailassociations.contactid) where groupid = " + FCConvert.ToString(lngID), "SystemSettings");
                            while (!rsLoad.EndOfFile())
                            {
                                strReturn += FCConvert.ToString(rsLoad.Get_Fields_String("email")) + ", ";
                                rsLoad.MoveNext();
                            }
                            strReturn = Strings.Trim(strReturn);
                            if (strReturn != string.Empty)
                            {
                                strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
                            }
                        }
                    }
                    if (Strings.Trim(txtMailTo.Text) == string.Empty)
                    {
                        txtMailTo.Text = strReturn;
                    }
                    else
                    {
                        txtMailTo.Text = txtMailTo.Text + ", " + strReturn;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In GetRecipient", MsgBoxStyle.Critical, "Error");
            }
        }

        private void frmEMail_Resize(object sender, System.EventArgs e)
        {
            ResizeGridNames();
        }

        private void GridNames_Click(object sender, EventArgs e)
        {
            int lngRow;
            lngRow = GridNames.MouseRow;
            if (lngRow < 2)
                return;
            lngRow = GridNames.Row;
            if (Conversion.Val(GridNames.RowData(lngRow)) > 0)
            {
                // is a nickname not a category
                if (ImageSend[1].Visible)
                {
                    if (Strings.Trim(txtCarbonCopy.Text) != string.Empty)
                    {
                        if (NotAlreadyIn_2(txtCarbonCopy.Text, GridNames.TextMatrix(lngRow, 1)))
                        {
                            txtCarbonCopy.Text = txtCarbonCopy.Text + ", " + GridNames.TextMatrix(lngRow, 1);
                        }
                    }
                    else
                    {
                        txtCarbonCopy.Text = GridNames.TextMatrix(lngRow, 1);
                    }
                }
                else
                {
                    if (Strings.Trim(txtMailTo.Text) != string.Empty)
                    {
                        if (NotAlreadyIn_2(txtMailTo.Text, GridNames.TextMatrix(lngRow, 1)))
                        {
                            txtMailTo.Text = txtMailTo.Text + ", " + GridNames.TextMatrix(lngRow, 1);
                        }
                    }
                    else
                    {
                        txtMailTo.Text = GridNames.TextMatrix(lngRow, 1);
                    }
                }
            }
        }

        private bool NotAlreadyIn_2(string strDest, string strSrc)
        {
            return NotAlreadyIn(ref strDest, ref strSrc);
        }

        private bool NotAlreadyIn(ref string strDest, ref string strSrc)
        {
            bool NotAlreadyIn = false;
            NotAlreadyIn = true;
            string[] strAry = null;
            int x;
            strAry = Strings.Split(strDest, ",", -1, CompareConstants.vbTextCompare);
            for (x = 0; x <= Information.UBound(strAry, 1); x++)
            {
                if (Strings.Trim(Strings.UCase(strAry[x])) == Strings.Trim(Strings.UCase(strSrc)))
                {
                    NotAlreadyIn = false;
                    return NotAlreadyIn;
                }
            }
            // x
            return NotAlreadyIn;
        }

        //private void GridNames_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	int lngRow;
        //	lngRow = GridNames.MouseRow;
        //	if (lngRow <= 0)
        //	{
        //		ToolTip1.SetToolTip(GridNames, "");
        //		return;
        //	}
        //	if (Conversion.Val(GridNames.RowData(lngRow)) == 1)
        //	{
        //		// contact
        //		ToolTip1.SetToolTip(GridNames, GridNames.TextMatrix(lngRow, 2));
        //		// email
        //	}
        //	else if (Conversion.Val(GridNames.RowData(lngRow)) == 2)
        //	{
        //		// groups
        //		ToolTip1.SetToolTip(GridNames, GridNames.TextMatrix(lngRow, CNSTGRIDNAMESCOLMISC));
        //	}
        //	else
        //	{
        //		ToolTip1.SetToolTip(GridNames, "");
        //	}
        //}
        private void GridNames_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngRow;
            lngRow = GridNames.GetFlexRowIndex(e.RowIndex);

            if (GridNames.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = GridNames[e.ColumnIndex, e.RowIndex];
                if (lngRow <= 0)
                {
                    cell.ToolTipText = "";
                    return;
                }
                if (Conversion.Val(GridNames.RowData(lngRow)) == 1)
                {
                    // contact
                    cell.ToolTipText = GridNames.TextMatrix(lngRow, 2);
                    // email
                }
                else if (Conversion.Val(GridNames.RowData(lngRow)) == 2)
                {
                    // groups
                    cell.ToolTipText = GridNames.TextMatrix(lngRow, CNSTGRIDNAMESCOLMISC);
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
        }
        private void lblCarbonCopy_Click(object sender, System.EventArgs e)
        {
            GetCarbonCopy();
        }

        private void GetCarbonCopy()
        {
            try
            {
                // On Error GoTo ErrorHandler
                string strReturn;
                int lngID;
                bool boolContact;
                clsDRWrapper rsLoad = new clsDRWrapper();
                txtCarbonCopy.Focus();
                lngID = 0;
                strReturn = "";
                boolContact = frmEmailChooseContactGroup.InstancePtr.Init(true, true, ref lngID, ref strReturn, false);
                if (lngID > 0)
                {
                    if (Strings.Trim(strReturn) == string.Empty)
                    {
                        if (boolContact)
                        {
                            // contact
                            rsLoad.OpenRecordset("select * from emailcontacts where contactid = " + FCConvert.ToString(lngID), "SystemSettings");
                            if (!rsLoad.EndOfFile())
                            {
                                strReturn = FCConvert.ToString(rsLoad.Get_Fields_String("email"));
                            }
                        }
                        else
                        {
                            // group
                            rsLoad.OpenRecordset("select * from emailcontacts inner join emailassociations on (emailcontacts.contactid = emailassociations.contactid) where groupid = " + FCConvert.ToString(lngID), "SystemSettings");
                            while (!rsLoad.EndOfFile())
                            {
                                strReturn += FCConvert.ToString(rsLoad.Get_Fields_String("email")) + ", ";
                                rsLoad.MoveNext();
                            }
                            strReturn = Strings.Trim(strReturn);
                            if (strReturn != string.Empty)
                            {
                                strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
                            }
                        }
                    }
                    if (Strings.Trim(txtCarbonCopy.Text) == string.Empty)
                    {
                        txtCarbonCopy.Text = strReturn;
                    }
                    else
                    {
                        txtCarbonCopy.Text = txtCarbonCopy.Text + ", " + strReturn;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In GetCarboncopy", MsgBoxStyle.Critical, "Error");
            }
        }

        private void NewMessage()
        {
            try
            {
                // On Error GoTo ErrorHandler
                int x;
                if (lngCurrEmail == 0)
                {
                    // might have to delete some temp files
                    for (x = 1; x <= Information.UBound(strAryAttach, 1); x++)
                    {
                        if (strAryAttach[x].boolDelete)
                        {
                            if (File.Exists(strAryAttach[x].strFullName))
                            {
                                File.Delete(strAryAttach[x].strFullName);
                                strAryAttach[x].boolDelete = false;
                                strAryAttach[x].strFileSize = "";
                                strAryAttach[x].strName = "";
                                strAryAttach[x].strFullName = "";
                                strAryAttach[x].strFileNewName = "";
                            }
                        }
                    }
                    // x
                }
                strAryAttach = new EMailAttachment[1 + 1];
                //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                strAryAttach[1] = new EMailAttachment(0);
                strAryAttach[1].boolDelete = false;
                strAryAttach[1].strFileNewName = "";
                strAryAttach[1].strFileSize = FCConvert.ToString(0);
                strAryAttach[1].strFullName = "";
                strAryAttach[1].strName = "";
                ReShowAttachments();
                lngCurrEmail = 0;
                txtSubject.Text = "";
                txtCarbonCopy.Text = "";
                txtMailTo.Text = "";
                rtbMessage.Text = "";
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In NewMessage", MsgBoxStyle.Critical, "Error");
            }
        }

        private void lblSendTo_Click(object sender, System.EventArgs e)
        {
            GetRecipient();
        }

        private void mnuAttach_Click(object sender, System.EventArgs e)
        {
            AddAttachment();
        }

        private void mnuClear_Click(object sender, System.EventArgs e)
        {
            NewMessage();
        }

        private void mnuDeleteDraft_Click(object sender, System.EventArgs e)
        {
            int lngID;
            lngID = frmEmailGetEmailID.InstancePtr.Init(modEMail.CNSTEMAILDRAFTBOX, modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
            if (lngID > 0)
            {
                clsDRWrapper rsSave = new clsDRWrapper();
                rsSave.Execute("delete from emailboxes where id = " + FCConvert.ToString(lngID), "SystemSettings");
                FCMessageBox.Show("Draft Deleted", MsgBoxStyle.Information, "Deleted");
            }
        }

        private void mnuDetachFiles_Click(object sender, System.EventArgs e)
        {
            DetachAttachment();
        }

        private void mnuEditConfiguration_Click(object sender, System.EventArgs e)
        {
            frmEmailConfiguration.InstancePtr.Init(true);
            LoadConfiguration();
        }

        private void mnuEditContacts_Click(object sender, System.EventArgs e)
        {
            //FC:FINAL:RPU:#1255 - Send also information if was opened from general entry
            //frmEmailContacts.InstancePtr.Init(true, false);
            frmEmailContacts.InstancePtr.Init(true, generalEntry: fromGeneralEntry);
            LoadGridNames();
        }

        private void mnuEditDisclaimer_Click(object sender, System.EventArgs e)
        {
            frmEmailSignatures.InstancePtr.Init(modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER, true);
        }

        private void mnuEditGroups_Click(object sender, System.EventArgs e)
        {
            frmEmailGroups.InstancePtr.Init(0, true, false);
            LoadGridNames();
        }

        private void mnuEditSignatures_Click(object sender, System.EventArgs e)
        {
            frmEmailSignatures.InstancePtr.Init(modEMail.CNSTEMAILSIGNATURETYPEPERSONAL, true);
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void mnuExit_Click()
        {
            //mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void mnuLoadDraft_Click(object sender, System.EventArgs e)
        {
            LoadDraft();
        }

        private void LoadDraft()
        {
            try
            {
                // On Error GoTo ErrorHandler
                int lngID;
                lngID = frmEmailGetEmailID.InstancePtr.Init(modEMail.CNSTEMAILDRAFTBOX, modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                if (lngID > 0)
                {
                    LoadMail(lngID);
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadDraft", MsgBoxStyle.Critical, "Error");
            }
        }

        private void LoadSent()
        {
            try
            {
                // On Error GoTo ErrorHandler
                int lngID;
                lngID = frmEmailGetEmailID.InstancePtr.Init(modEMail.CNSTEMAILSENTBOX, modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                if (lngID > 0)
                {
                    LoadMail(lngID);
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadSent", MsgBoxStyle.Critical, "Error");
            }
        }

        private void mnuLoadSent_Click(object sender, System.EventArgs e)
        {
            LoadSent();
        }

        private void mnuSaveDraft_Click(object sender, System.EventArgs e)
        {
            if (SaveMail_2(modEMail.CNSTEMAILDRAFTBOX))
            {
                FCMessageBox.Show("Message Saved", MsgBoxStyle.Information, "Saved");
            }
        }

        private void mnuSaveExit_Click(object sender, System.EventArgs e)
        {
            SendMail();
        }

        private void LoadMail(int lngID)
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                rsLoad.OpenRecordset("select * from emailboxes where id = " + FCConvert.ToString(lngID), "SystemSettings");
                if (!rsLoad.EndOfFile())
                {
                    if (((rsLoad.Get_Fields_Int32("boxtype"))) == modEMail.CNSTEMAILDRAFTBOX)
                    {
                        lngCurrEmail = lngID;
                        if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("boolPersonalSIG")))
                        {
                            chkSignature.CheckState = Wisej.Web.CheckState.Checked;
                        }
                        else
                        {
                            chkSignature.CheckState = Wisej.Web.CheckState.Unchecked;
                        }
                        if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("boolCompanySIG")))
                        {
                            chkCompanySignature.CheckState = Wisej.Web.CheckState.Checked;
                        }
                        else
                        {
                            chkCompanySignature.CheckState = Wisej.Web.CheckState.Unchecked;
                        }
                        if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("boolDisclaimer")))
                        {
                            chkDisclaimer.CheckState = Wisej.Web.CheckState.Checked;
                        }
                        else
                        {
                            chkDisclaimer.CheckState = Wisej.Web.CheckState.Unchecked;
                        }
                    }
                    else
                    {
                        lngCurrEmail = 0;
                        chkCompanySignature.CheckState = Wisej.Web.CheckState.Unchecked;
                        chkDisclaimer.CheckState = Wisej.Web.CheckState.Unchecked;
                        chkSignature.CheckState = Wisej.Web.CheckState.Unchecked;
                    }
                    txtMailTo.Text = FCConvert.ToString(rsLoad.Get_Fields_String("recipient"));
                    txtCarbonCopy.Text = FCConvert.ToString(rsLoad.Get_Fields_String("CC"));
                    rtbMessage.Text = FCConvert.ToString(rsLoad.Get_Fields_String("body"));
                    txtSubject.Text = FCConvert.ToString(rsLoad.Get_Fields_String("subject"));
                    int x;
                    string strAttach = "";
                    string[] strTempARY = null;
                    string[] strATArray = null;
                    int intIndex = 0;
                    strAttach = "";
                    FCUtils.EraseSafe(strAryAttach);
                    strAryAttach = new EMailAttachment[1 + 1];
                    //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                    strAryAttach[1] = new EMailAttachment(0);
                    strAttach = FCConvert.ToString(rsLoad.Get_Fields_String("attachments"));
                    if (Strings.Trim(strAttach) != string.Empty)
                    {
                        strTempARY = Strings.Split(strAttach, ";", -1, CompareConstants.vbTextCompare);
                        for (x = 0; x <= Information.UBound(strTempARY, 1); x++)
                        {
                            strATArray = Strings.Split(strTempARY[x], ",", -1, CompareConstants.vbTextCompare);
                            intIndex = x + 1;
                            if (intIndex > 1)
                            {
                                Array.Resize(ref strAryAttach, intIndex + 1);
                            }
                            strAryAttach[intIndex].strFullName = strATArray[0];
                            strAryAttach[intIndex].strFileNewName = strATArray[1];
                            strAryAttach[intIndex].strName = strATArray[2];
                            strAryAttach[intIndex].strFileSize = strATArray[3];
                            strAryAttach[intIndex].boolDelete = FCConvert.CBool(strATArray[4]);
                        }
                        // x
                    }
                    ReShowAttachments();
                }
                else
                {
                    NewMessage();
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadMail", MsgBoxStyle.Critical, "Error");
            }
        }
        // VBto upgrade warning: 'Return' As bool	OnWrite(bool, short)
        private bool SaveMail_2(int lngBox)
        {
            return SaveMail(ref lngBox);
        }

        private bool SaveMail(ref int lngBox)
        {
            bool SaveMail = false;
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsSave = new clsDRWrapper();
                string strBody;
                string strTemp = "";
                SaveMail = false;
                rsSave.OpenRecordset("select * from emailboxes where id = -1", "SystemSettings");
                rsSave.AddNew();
                rsSave.Set_Fields("boxtype", lngBox);
                rsSave.Set_Fields("recipient", txtMailTo.Text);
                rsSave.Set_Fields("CC", txtCarbonCopy.Text);
                rsSave.Set_Fields("subject", txtSubject.Text);
                strBody = rtbMessage.Text;
                rsSave.Set_Fields("userid", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                if (lngBox == modEMail.CNSTEMAILDRAFTBOX)
                {
                    if (chkCompanySignature.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        rsSave.Set_Fields("boolCompanySig", true);
                    }
                    else
                    {
                        rsSave.Set_Fields("boolCompanySig", false);
                    }
                    if (chkDisclaimer.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        rsSave.Set_Fields("boolDisclaimer", true);
                    }
                    else
                    {
                        rsSave.Set_Fields("boolDisclaimer", false);
                    }
                    if (chkSignature.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        rsSave.Set_Fields("boolPersonalSig", true);
                    }
                    else
                    {
                        rsSave.Set_Fields("boolPersonalSig", false);
                    }
                    rsSave.Set_Fields("senddate", 0);
                }
                else
                {
                    rsSave.Set_Fields("boolcompanysig", false);
                    rsSave.Set_Fields("booldisclaimer", false);
                    rsSave.Set_Fields("boolpersonalsig", false);
                    if (chkSignature.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        strTemp = GetEMailSignature(modEMail.CNSTEMAILSIGNATURETYPEPERSONAL, modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
                        if (Strings.Trim(strTemp) != string.Empty)
                        {
                            strBody += "\r\n" + "\r\n" + strTemp;
                        }
                    }
                    if (chkCompanySignature.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        strTemp = GetEMailSignature(modEMail.CNSTEMAILSIGNATURETYPEGLOBAL);
                        if (Strings.Trim(strTemp) != string.Empty)
                        {
                            strBody += "\r\n" + "\r\n" + strTemp;
                        }
                    }
                    if (chkDisclaimer.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        strTemp = GetEMailSignature(modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER);
                        if (Strings.Trim(strTemp) != string.Empty)
                        {
                            strBody += "\r\n" + "\r\n" + strTemp;
                        }
                    }
                    rsSave.Set_Fields("senddate", DateTime.Now);
                }
                if (Strings.Trim(strBody) == string.Empty)
                {
                    strBody = " ";
                }
                rsSave.Set_Fields("body", strBody);
                // save attachments
                int x;
                string strAttach;
                strAttach = "";
                for (x = 1; x <= Information.UBound(strAryAttach, 1); x++)
                {
                    if (Strings.Trim(strAryAttach[x].strFullName) != string.Empty)
                    {
                        strAttach += strAryAttach[x].strFullName + ",";
                        strAttach += strAryAttach[x].strFileNewName + ",";
                        strAttach += strAryAttach[x].strName + ",";
                        strAttach += strAryAttach[x].strFileSize + ",";
                        if (strAryAttach[x].boolDelete)
                        {
                            strAttach += "TRUE";
                        }
                        else
                        {
                            strAttach += "FALSE";
                        }
                        strAttach += ";";
                    }
                }
                // x
                if (strAttach != string.Empty)
                {
                    strAttach = Strings.Mid(strAttach, 1, strAttach.Length - 1);
                }
                rsSave.Set_Fields("attachments", strAttach);
                rsSave.Update();
                lngCurrEmail = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
                if (lngCurrEmail > 0 && lngBox == modEMail.CNSTEMAILSENTBOX)
                {
                    // moving to the sent box
                    //FC:FINAL:IPI - #1254 - this code is commented out in original code
                    //rsSave.Execute("delete from emailboxes where id = " + FCConvert.ToString(lngCurrEmail), "SystemSettings");
                    lngCurrEmail = 0;
                }
                SaveMail = FCConvert.ToBoolean(1);
                return SaveMail;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In SaveMail", MsgBoxStyle.Critical, "Error");
            }
            return SaveMail;
        }

        private void Toolbar1_ButtonClick(object sender, fecherFoundation.FCToolBarButtonClickEventArgs e)
        {
            ToolBarButton Button = e.Button;
            if (Strings.UCase(Button.Name) == "BTNSAVEDRAFT")
            {
                SaveMail_2(modEMail.CNSTEMAILDRAFTBOX);
            }
            else if (Strings.UCase(Button.Name) == "BTNLOADDRAFT")
            {
                LoadDraft();
            }
            else if (Strings.UCase(Button.Name) == "BTNATTACHFILE")
            {
                AddAttachment();
            }
            else if (Strings.UCase(Button.Name) == "BTNDETACHFILE")
            {
                DetachAttachment();
            }
            else if (Strings.UCase(Button.Name) == "BTNHIGHPRIORITY")
            {
            }
            else if (Strings.UCase(Button.Name) == "BTNRECEIPT")
            {
            }
            else if (Strings.UCase(Button.Name) == "BTNSEND")
            {
                SendMail();
            }
            else if (Strings.UCase(Button.Name) == "BTNEDITCONTACTS")
            {
                //FC:FINAL:RPU:#1255 - Send also information if was opened from general entry
                //frmEmailContacts.InstancePtr.Init(true);
                frmEmailContacts.InstancePtr.Init(true, generalEntry: fromGeneralEntry);
                LoadGridNames();
            }
            else if (Strings.UCase(Button.Name) == "BTNEDITGROUPS")
            {
                frmEmailGroups.InstancePtr.Init(0, false, false);
                LoadGridNames();
            }
            else if (Strings.UCase(Button.Name) == "BTNEDITSIGNATURES")
            {
                frmEmailSignatures.InstancePtr.Init(modEMail.CNSTEMAILSIGNATURETYPEPERSONAL);
            }
            else if (Strings.UCase(Button.Name) == "BTNEDITDISCLAIMER")
            {
                frmEmailSignatures.InstancePtr.Init(modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER);
            }
            else if (Strings.UCase(Button.Name) == "BTNEDITCONFIGURATION")
            {
                frmEmailConfiguration.InstancePtr.Init(true);
                LoadConfiguration();
            }
        }

        private void AddAttachment()
        {
            try
            {
                // On Error GoTo ErrorHandler
                string strFileName = "";
                string strName = "";
                int intIndex;
                double dblSize = 0;
                FileInfo fl = null;
                App.MainForm.CommonDialog1.DialogTitle = "Choose File to Attach";
                // App.MainForm.CommonDialog1.Flags = cdlOFNExplorer+cdlOFNFileMustExist+cdlOFNLongNames+cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                App.MainForm.CommonDialog1.CancelError = true;
                App.MainForm.CommonDialog1.InitDir = FCFileSystem.Statics.UserDataFolder;
                App.MainForm.CommonDialog1.ShowOpen();
                strFileName = App.MainForm.CommonDialog1.FileName;
                strFileName = Strings.Trim(strFileName);
                if (strFileName != string.Empty)
                {
                    fl = new FileInfo(strFileName);
                    if (Information.UBound(strAryAttach, 1) == 1)
                    {
                        if (strAryAttach[1].strFullName != string.Empty)
                        {
                            Array.Resize(ref strAryAttach, 2 + 1);
                        }
                    }
                    else
                    {
                        Array.Resize(ref strAryAttach, Information.UBound(strAryAttach, 1) + 1 + 1);
                    }
                    intIndex = Information.UBound(strAryAttach, 1);
                    dblSize = FCConvert.ToInt32(fl.Length) / 1024;
                    if (dblSize >= 1000)
                    {
                        dblSize /= 1024;
                        if (dblSize < 1000)
                        {
                            strAryAttach[intIndex].strFileSize = Strings.Format(dblSize, "0.00") + "M";
                        }
                        else
                        {
                            dblSize /= 1024;
                            strAryAttach[intIndex].strFileSize = Strings.Format(dblSize, "0.00") + "G";
                        }
                    }
                    else
                    {
                        strAryAttach[intIndex].strFileSize = Strings.Format(dblSize, "0.00") + "k";
                    }
                    strAryAttach[intIndex].strFileNewName = "";
                    strAryAttach[intIndex].strFullName = strFileName;
                    strAryAttach[intIndex].strName = Path.GetFileName(strFileName);
                    strAryAttach[intIndex].boolDelete = false;
                    fl = null;
                }
                ReShowAttachments();
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                if (Information.Err(ex).Number != 32755)
                {
                    FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In AddAttachment", MsgBoxStyle.Critical, "Error");
                }
            }
        }

        private void DetachAttachment()
        {
            try
            {
                // On Error GoTo ErrorHandler
                string[] strAryChoices = null;
                string strList;
                string strReturn = "";
                int x;
                strList = "";
                for (x = 1; x <= Information.UBound(strAryAttach, 1); x++)
                {
                    if (Strings.Trim(strAryAttach[x].strFullName) != string.Empty)
                    {
                        strList += FCConvert.ToString(x) + ";" + strAryAttach[x].strName + "|";
                    }
                }
                // x
                if (Strings.Trim(strList) != string.Empty)
                {
                    strList = Strings.Mid(strList, 1, strList.Length - 1);
                    strReturn = frmListChoiceWithIDs.InstancePtr.Init(ref strList, "Detach", "Choose Files to Detach", true);
                    if (Strings.Trim(strReturn) != string.Empty)
                    {
                        strAryChoices = Strings.Split(strReturn, ",", -1, CompareConstants.vbTextCompare);
                        for (x = 0; x <= Information.UBound(strAryChoices, 1); x++)
                        {
                            if (strAryAttach[FCConvert.ToInt32(strAryChoices[x])].boolDelete)
                            {
                                if (File.Exists(strAryAttach[FCConvert.ToInt32(strAryChoices[x])].strFullName))
                                {
                                    File.Delete(strAryAttach[FCConvert.ToInt32(strAryChoices[x])].strFullName);
                                }
                            }
                            strAryAttach[FCConvert.ToInt32(strAryChoices[x])].boolDelete = false;
                            strAryAttach[FCConvert.ToInt32(strAryChoices[x])].strFileSize = "";
                            strAryAttach[FCConvert.ToInt32(strAryChoices[x])].strFullName = "";
                            strAryAttach[FCConvert.ToInt32(strAryChoices[x])].strName = "";
                            strAryAttach[FCConvert.ToInt32(strAryChoices[x])].strFileNewName = "";
                        }
                        // x
                    }
                }
                else
                {
                    FCMessageBox.Show("There are no files to detach", MsgBoxStyle.Exclamation, "No Attachments");
                }
                ReShowAttachments();
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In DetachAttachment", MsgBoxStyle.Critical, "Error");
            }
        }

        private void ReShowAttachments()
        {
            int x;
            string strReturn;
            strReturn = "";
            for (x = 1; x <= Information.UBound(strAryAttach, 1); x++)
            {
                if (Strings.Trim(strAryAttach[x].strFullName) != string.Empty)
                {
                    strReturn += strAryAttach[x].strName + " (" + strAryAttach[x].strFileSize + "), ";
                }
            }
            // x
            strReturn = Strings.Trim(strReturn);
            if (strReturn != string.Empty)
            {
                strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
            }
            lblAttachments.Text = strReturn;
        }
        //
        // smtpMail.ServerAddr = eConfig.SMTPHost
        // smtpMail.PASSWORD = eConfig.PASSWORD
        // smtpMail.UserName = eConfig.UserName
        // smtpMail.FromAddr = eConfig.DefaultFromAddress
        // smtpMail.AuthType = eConfig.AuthCode
        // smtpMail.From = eConfig.DefaultFromName
        private void SendMail(bool blnShowSuccess = true)
        {
            const string newLine = "\r\n";

            try
            {
                // On Error GoTo ErrorHandler
                string strAddress = "";
                string strBody;
                string strTemp = "";
                string[] strAry = null;
                string[] strGroup = null;
                string strName = "";
                string[] strNameAddress = null;

                //cEmail curMail = new cEmail();
                int x;
                int intloop;

                // If Trim(smtpMail.FromAddr) = vbNullString Then
                if (Strings.Trim(curConfig.DefaultFromAddress) == string.Empty)
                {
                    FCMessageBox.Show("You must specify the E-mail address that you are mailing from", MsgBoxStyle.Exclamation, "No Sender Address");

                    return;
                }

                mailMessage = new MailMessage();

                //FC:FINAL:AM:#3770 - remove spaces from the email address
                //mailMessage.From = new MailAddress(curConfig.DefaultFromAddress, curConfig.DefaultFromName);
                try
                {
                    mailMessage.From = new MailAddress(curConfig.DefaultFromAddress.Replace(" ", ""), curConfig.DefaultFromName);
                }
                catch (Exception ex)
                {
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
                    FCMessageBox.Show("There is an issue with the email address entered in Email Configuration. Please correct before sending any email messages through TRIO", MsgBoxStyle.Exclamation);
                    return;
                }

                //smtpMail.ClearAttachment();
                //smtpMail.ClearHeader();
                //smtpMail.ClearRecipient();
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;

                if (Strings.Trim(txtMailTo.Text) == string.Empty)
                {
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                    FCMessageBox.Show("You must specify a recipient", MsgBoxStyle.Exclamation, "No Recipient");

                    return;
                }

                mailMessage.Priority = chkHighPriority.Checked ? MailPriority.High : MailPriority.Normal;

                if (chkRequestReceipt.Checked)
                {
                    mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                }

                // 
                if (Strings.Trim(txtMailTo.Text) != string.Empty)
                {
                    strAry = Strings.Split(txtMailTo.Text, ",", -1, CompareConstants.vbTextCompare);

                    for (x = 0; x <= Information.UBound(strAry, 1); x++)
                    {
                        strTemp = Strings.Trim(strAry[x]);

                        if (strTemp == string.Empty) continue;

                        strAddress = ParseEmailAddress_6(strTemp, true);

                        if (Strings.Trim(strAddress) == string.Empty)
                        {
                            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                            FCMessageBox.Show("Invalid quickname: " + strTemp, MsgBoxStyle.Exclamation, "Invalid Quickname");

                            return;
                        }

                        strGroup = Strings.Split(strAddress, ",", -1, CompareConstants.vbTextCompare);

                        for (intloop = 0; intloop <= Information.UBound(strGroup, 1); intloop++)
                        {
                            strNameAddress = Strings.Split(strGroup[intloop], ";", -1, CompareConstants.vbTextCompare);
                            mailMessage.To.Add(strNameAddress[1]);

                        }

                        // intloop
                    }

                    // x
                }

                // now carbon copies
                if (Strings.Trim(txtCarbonCopy.Text) != string.Empty)
                {
                    strAry = Strings.Split(txtCarbonCopy.Text, ",", -1, CompareConstants.vbTextCompare);

                    for (x = 0; x <= Information.UBound(strAry, 1); x++)
                    {
                        strTemp = Strings.Trim(strAry[x]);

                        if (strTemp == string.Empty) continue;

                        strAddress = ParseEmailAddress_6(strTemp, true);

                        if (Strings.Trim(strAddress) == string.Empty)
                        {
                            FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                            FCMessageBox.Show("Invalid quickname: " + strTemp, MsgBoxStyle.Exclamation, "Invalid Quickname");

                            return;
                        }

                        strGroup = Strings.Split(strAddress, ",", -1, CompareConstants.vbTextCompare);

                        for (intloop = 0; intloop <= Information.UBound(strGroup, 1); intloop++)
                        {
                            strNameAddress = Strings.Split(strGroup[intloop], ";", -1, CompareConstants.vbTextCompare);
                            mailMessage.CC.Add(strNameAddress[1]);

                        }

                        // intloop
                    }

                    // x
                }

                mailMessage.Subject = txtSubject.Text;

                //smtpMail.Subject = txtSubject.Text;
                strBody = rtbMessage.Text;

                if (chkSignature.CheckState == Wisej.Web.CheckState.Checked)
                {
                    strTemp = GetEMailSignature(modEMail.CNSTEMAILSIGNATURETYPEPERSONAL, modGlobalConstants.Statics.clsSecurityClass.Get_UserID());

                    if (Strings.Trim(strTemp) != string.Empty)
                    {
                        strBody += newLine + newLine + strTemp;
                    }
                }

                if (chkCompanySignature.CheckState == Wisej.Web.CheckState.Checked)
                {
                    strTemp = GetEMailSignature(modEMail.CNSTEMAILSIGNATURETYPEGLOBAL);

                    if (Strings.Trim(strTemp) != string.Empty)
                    {
                        strBody += newLine + newLine + strTemp;
                    }
                }

                if (chkDisclaimer.CheckState == Wisej.Web.CheckState.Checked)
                {
                    strTemp = GetEMailSignature(modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER);

                    if (Strings.Trim(strTemp) != string.Empty)
                    {
                        strBody += newLine + newLine + strTemp;
                    }
                }

                if (boolSendAsHTML)
                {
                    strBody = strBody.Replace(newLine, "<br />");
                }

                mailMessage.Body = strBody;

                if (boolSendAsHTML)
                {
                    //smtpMail.BodyFormat = 1;
                    mailMessage.IsBodyHtml = true;
                }

                // now attachments
                for (x = 1; x <= Information.UBound(strAryAttach, 1); x++)
                {
                    var eMailAttachment = strAryAttach[x];

                    var attachmentName = eMailAttachment.strFullName;

                    if (string.IsNullOrEmpty(attachmentName)) continue;

                    var attachment = new Attachment(attachmentName, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = attachment.ContentDisposition;
                    disposition.CreationDate = System.IO.File.GetCreationTime(attachmentName);
                    disposition.ModificationDate = System.IO.File.GetLastWriteTime(attachmentName);
                    disposition.ReadDate = System.IO.File.GetLastAccessTime(attachmentName);
                    mailMessage.Attachments.Add(attachment);
                }

                // x
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;

                if (curConfig.UseTransportLayerSecurity)
                { }

                try
                {
                    smtpClient.Send(mailMessage);

                    // If smtpMail.SendMail = 0 Then
                    if (blnShowSuccess)
                    {
                        FCMessageBox.Show("Mail sent", MsgBoxStyle.Information, "Successful");
                    }

                    SaveMail_2(modEMail.CNSTEMAILSENTBOX);

                    //FC:FINAL:SBE - #381 - dispose email message and attachments, to have the possibility to delete the attachment
                    this.DisposeMessage(mailMessage);

                    for (x = 1; x <= strAryAttach.UBound(); x++)
                    {
                        if (!strAryAttach[x].boolDelete)
                            continue;

                        if (!File.Exists(strAryAttach[x].strFullName))
                            continue;

                        File.Delete(strAryAttach[x].strFullName);
                        strAryAttach[x].boolDelete = false;
                        strAryAttach[x].strFileSize = "";
                        strAryAttach[x].strName = "";
                        strAryAttach[x].strFullName = "";
                        strAryAttach[x].strFileNewName = "";
                    }

                    // x
                    ReShowAttachments();
                    NewMessage();
                }
                catch (Exception ex)
                {
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
                    FCMessageBox.Show(ex.Message, MsgBoxStyle.Critical, "Error");
                }

                //if (!smtpClient.HadError)
                //{
                //    // If smtpMail.SendMail = 0 Then
                //    if (blnShowSuccess)
                //    {
                //        FCMessageBox.Show("Mail sent", MsgBoxStyle.Information, "Successful");
                //    }
                //    SaveMail_2(modEMail.CNSTEMAILSENTBOX);
                //    for (x = 1; x <= Information.UBound(strAryAttach, 1); x++)
                //    {
                //        if (strAryAttach[x].boolDelete)
                //        {
                //            if (File.Exists(strAryAttach[x].strFullName))
                //            {
                //                File.Delete(strAryAttach[x].strFullName);
                //                strAryAttach[x].boolDelete = false;
                //                strAryAttach[x].strFileSize = "";
                //                strAryAttach[x].strName = "";
                //                strAryAttach[x].strFullName = "";
                //                strAryAttach[x].strFileNewName = "";
                //            }
                //        }
                //    }
                //    // x
                //    ReShowAttachments();
                //    NewMessage();
                //}
                //else
                //{
                //    // MsgBox smtpMail.GetLastErrDescription
                //    FCMessageBox.Show(FCConvert.ToString(emailservice.LastErrorNumber) + " " + emailservice.LastErrorMessage, MsgBoxStyle.Critical, "Error");
                //}
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                FCMessageBox.Show("Error Number "
                                  + FCConvert.ToString(Information.Err(ex)
                                                                  .Number)
                                  + " "
                                  + ex.GetBaseException()
                                      .Message
                                  + newLine
                                  + "In SendMail");
            }
        }

        private void DisposeMessage(MailMessage message)
        {
            if (message.Attachments != null)
            {
                for (int i = message.Attachments.Count - 1; i >= 0; i--)
                {
                    message.Attachments[i].Dispose();
                }
                message.Attachments.Clear();
                message.Attachments.Dispose();
            }
            message.Dispose();
            message = null;
        }

        private string ParseEmailAddress_6(string strAddress, bool boolIncludeName = false)
        {
            return ParseEmailAddress(ref strAddress, boolIncludeName);
        }

        private string ParseEmailAddress(ref string strAddress, bool boolIncludeName = false)
        {
            string ParseEmailAddress = "";
            // takes an address or nickname and sends back a comma delimited list of addresses
            // if boolincludename is true each address will be a ; delimited list of name or nickname and then address
            try
            {
                // On Error GoTo ErrorHandler
                string strReturn;
                string strTemp;
                strTemp = "";
                strReturn = "";
                ParseEmailAddress = "";
                if (Strings.Trim(strAddress) == string.Empty)
                    return ParseEmailAddress;
                if (Strings.InStr(1, strAddress, "@", CompareConstants.vbTextCompare) > 0)
                {
                    strReturn = strAddress;
                    if (boolIncludeName)
                    {
                        strReturn = strAddress + ";" + strReturn;
                    }
                }
                else
                {
                    clsDRWrapper rsLoad = new clsDRWrapper();

                    try
                    {
                        rsLoad.OpenRecordset("select * from emailcontacts where nickname = '" + strAddress + "'", "SystemSettings");
                        if (!rsLoad.EndOfFile())
                        {
                            strReturn = FCConvert.ToString(rsLoad.Get_Fields_String("email"));
                            if (boolIncludeName)
                            {
                                if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("firstname"))) != string.Empty)
                                {
                                    strReturn = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("firstname")) + " " + FCConvert.ToString(rsLoad.Get_Fields_String("Lastname"))) + ";" + strReturn;
                                }
                                else
                                {
                                    strReturn = strAddress + ";" + strReturn;
                                }
                            }
                        }
                        else
                        {
                            rsLoad.OpenRecordset("select * from emailgroups where nickname = '" + strAddress + "'", "SystemSettings");
                            if (!rsLoad.EndOfFile())
                            {
                                int lngGroupID = 0;
                                lngGroupID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("groupid"));
                                rsLoad.OpenRecordset("select * from emailcontacts inner join emailassociations on (emailcontacts.contactid = emailassociations.contactid) where groupid = " + FCConvert.ToString(lngGroupID), "SystemSettings");
                                strTemp = "";
                                while (!rsLoad.EndOfFile())
                                {
                                    if (boolIncludeName)
                                    {
                                        if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("firstname"))) != string.Empty)
                                        {
                                            strTemp = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("firstname")) + " " + FCConvert.ToString(rsLoad.Get_Fields_String("lastname"))) + ";";
                                        }
                                        else if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("nickname"))) != string.Empty)
                                        {
                                            strTemp = FCConvert.ToString(rsLoad.Get_Fields_String("nickname")) + ";";
                                        }
                                        else
                                        {
                                            strTemp = FCConvert.ToString(rsLoad.Get_Fields_String("email")) + ";";
                                        }
                                    }
                                    strReturn += strTemp + FCConvert.ToString(rsLoad.Get_Fields_String("email")) + ",";
                                    rsLoad.MoveNext();
                                }
                                if (strReturn != string.Empty)
                                {
                                    strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
                                }
                            }
                        }
                    }
                    finally
                    {
                        rsLoad.DisposeOf();
                    }
                }
                ParseEmailAddress = strReturn;
                return ParseEmailAddress;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In ParseEmailAddress", MsgBoxStyle.Critical, "Error");
            }
            return ParseEmailAddress;
        }

        private string GetEMailSignature(int lngSigType, int lngUserID = 0)
        {
            string GetEMailSignature = "";
            try
            {
                // On Error GoTo ErrorHandler
                string strReturn;
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strSQL;
                strReturn = "";
                GetEMailSignature = "";

                try
                {
                    strSQL = "select * from emailsignatures where sigtype = " + FCConvert.ToString(lngSigType);
                    switch (lngSigType)
                    {
                        case modEMail.CNSTEMAILSIGNATURETYPEPERSONAL:
                            {
                                strSQL += " and userid = " + FCConvert.ToString(lngUserID);
                                break;
                            }
                    }
                    //end switch
                    rsLoad.OpenRecordset(strSQL, "SystemSettings");
                    if (!rsLoad.EndOfFile())
                    {
                        strReturn = FCConvert.ToString(rsLoad.Get_Fields_String("message"));
                    }
                }
                finally
                {
                    rsLoad.DisposeOf();
                }
                GetEMailSignature = strReturn;
                return GetEMailSignature;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "in GetEmailSignature", MsgBoxStyle.Critical, "Error");
            }
            return GetEMailSignature;
        }

        private void txtCarbonCopy_Enter(object sender, System.EventArgs e)
        {
            ImageSend[1].Visible = true;
            ImageSend[0].Visible = false;
        }

        private void txtMailTo_Enter(object sender, System.EventArgs e)
        {
            ImageSend[0].Visible = true;
            ImageSend[1].Visible = false;
        }

        private void SetupGridNames()
        {
            GridNames.TextMatrix(0, 1, "Quickname");
            GridNames.ColHidden(2, true);
            GridNames.ColHidden(CNSTGRIDNAMESCOLMISC, true);
        }

        private void ResizeGridNames()
        {
            int GridWidth = 0;
            GridWidth = GridNames.WidthOriginal;
            GridNames.ColWidth(0, FCConvert.ToInt32(0.3 * GridWidth));
        }

        private void LoadGridNames()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                int lngRow;
                clsDRWrapper rsGroup = new clsDRWrapper();
                int lngGroupRow;
                // do contacts first
                GridNames.Rows = 2;
                lngRow = GridNames.Rows - 1;
                GridNames.TextMatrix(lngRow, 1, "Contacts");
                GridNames.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, 0, ImageList1.Images[4 - 1]);
                GridNames.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, 0, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
                GridNames.IsSubtotal(lngRow, true);
                GridNames.RowOutlineLevel(lngRow, 1);
                //FC:FINAL:AM: don't set the height; used anchoring instead
                //GridNames.RowHeight(lngRow, FCConvert.ToInt32(2.5 * GridNames.RowHeight(0)));
                rsLoad.OpenRecordset("select * from emailcontacts order by nickname", "SystemSettings");
                while (!rsLoad.EndOfFile())
                {
                    GridNames.Rows += 1;
                    lngRow = GridNames.Rows - 1;
                    GridNames.TextMatrix(lngRow, 1, FCConvert.ToString(rsLoad.Get_Fields_String("nickname")));
                    GridNames.RowData(lngRow, 1);
                    // 0 means ignore, 1 means it is a quickname not the contacts or group line
                    GridNames.TextMatrix(lngRow, 2, FCConvert.ToString(rsLoad.Get_Fields_String("email")));
                    GridNames.RowOutlineLevel(lngRow, 2);
                    GridNames.IsSubtotal(lngRow, true);
                    GridNames.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, 0, ImageList1.Images[12 - 1]);
                    GridNames.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, 0, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
                    rsLoad.MoveNext();
                }
                GridNames.Rows += 1;
                lngRow = GridNames.Rows - 1;
                GridNames.RowOutlineLevel(lngRow, 1);
                GridNames.IsSubtotal(lngRow, true);
                GridNames.Rows += 1;
                lngRow = GridNames.Rows - 1;
                GridNames.TextMatrix(lngRow, 1, "Groups");
                GridNames.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, 0, ImageList1.Images[9 - 1]);
                GridNames.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, 0, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
                GridNames.IsSubtotal(lngRow, true);
                GridNames.RowOutlineLevel(lngRow, 1);
                GridNames.RowHeight(lngRow, FCConvert.ToInt32(2.5 * GridNames.RowHeight(lngRow)));
                rsLoad.OpenRecordset("select * from emailgroups order by nickname", "SystemSettings");
                while (!rsLoad.EndOfFile())
                {
                    GridNames.Rows += 1;
                    lngRow = GridNames.Rows - 1;
                    GridNames.TextMatrix(lngRow, 1, FCConvert.ToString(rsLoad.Get_Fields_String("nickname")));
                    GridNames.TextMatrix(lngRow, CNSTGRIDNAMESCOLMISC, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
                    GridNames.RowData(lngRow, 2);
                    GridNames.RowOutlineLevel(lngRow, 2);
                    GridNames.IsSubtotal(lngRow, true);
                    GridNames.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, 0, ImageList1.Images[9 - 1]);
                    GridNames.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, 0, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
                    lngGroupRow = lngRow;
                    rsGroup.OpenRecordset("select * from emailcontacts inner join emailassociations on (emailcontacts.id = emailassociations.contactid) where groupid = " + FCConvert.ToString(rsLoad.Get_Fields_Int32("id")) + " order by nickname,email", "SystemSettings");
                    while (!rsGroup.EndOfFile())
                    {
                        GridNames.Rows += 1;
                        lngRow = GridNames.Rows - 1;
                        GridNames.RowOutlineLevel(lngRow, 3);
                        GridNames.IsSubtotal(lngRow, true);
                        GridNames.RowData(lngRow, 1);
                        GridNames.TextMatrix(lngRow, 1, FCConvert.ToString(rsGroup.Get_Fields_String("nickname")));
                        GridNames.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, 0, ImageList1.Images[12 - 1]);
                        GridNames.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, 0, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
                        GridNames.TextMatrix(lngRow, 2, FCConvert.ToString(rsGroup.Get_Fields_String("email")));
                        GridNames.IsCollapsed(lngGroupRow, FCGrid.CollapsedSettings.flexOutlineCollapsed);
                        rsGroup.MoveNext();
                    }
                    rsLoad.MoveNext();
                }
                //FC:FINAL:SBE - redesign grid level tree task
                modColorScheme.ColorGrid(GridNames);
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "in LoadGridNames", MsgBoxStyle.Critical, "Error");
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            this.mnuSaveExit_Click(sender, e);
        }

        private void cmdSaveDraft_Click(object sender, EventArgs e)
        {
            this.mnuSaveDraft_Click(sender, e);
        }

        private void cmdDeleteDraft_Click(object sender, EventArgs e)
        {
            this.mnuDeleteDraft_Click(sender, e);
        }

        private void cmdLoadSent_Click(object sender, EventArgs e)
        {
            this.mnuLoadSent_Click(sender, e);
        }

        private void cmdLoadDraft_Click(object sender, EventArgs e)
        {
            this.mnuLoadDraft_Click(sender, e);
        }

        private void cmdDetachFiles_Click(object sender, EventArgs e)
        {
            this.mnuDetachFiles_Click(sender, e);
        }

        private void cmdAttach_Click(object sender, EventArgs e)
        {
            this.mnuAttach_Click(sender, e);
        }
    }
}
