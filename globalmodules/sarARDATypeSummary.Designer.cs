﻿namespace Global
{
	/// <summary>
	/// Summary description for sarARDATypeSummary.
	/// </summary>
	partial class sarARDATypeSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarARDATypeSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPrincipal,
				this.fldInterest,
				this.fldTax,
				this.fldTotal,
				this.fldType,
				this.fldDesc
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblPrincipal,
				this.lblInterest,
				this.lblTax,
				this.lblTotal,
				this.lnHeader,
				this.lblType,
				this.lblDesc,
				this.lblHeader
			});
			this.GroupHeader1.Height = 0.5F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotals,
				this.fldTotalPrin,
				this.fldTotalInt,
				this.fldTotalTax,
				this.fldTotalTotal,
				this.lnTotals
			});
			this.GroupFooter1.Height = 0.4270833F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 4.25F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0.3125F;
			this.lblPrincipal.Width = 1F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 5.625F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0.3125F;
			this.lblInterest.Width = 1F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 7F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.3125F;
			this.lblTax.Width = 1F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 8.375F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.3125F;
			this.lblTotal.Width = 1F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.5F;
			this.lnHeader.Width = 6.9375F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 6.9375F;
			this.lnHeader.Y1 = 0.5F;
			this.lnHeader.Y2 = 0.5F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 0F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblType.Text = "Type";
			this.lblType.Top = 0.3125F;
			this.lblType.Width = 0.5F;
			// 
			// lblDesc
			// 
			this.lblDesc.Height = 0.1875F;
			this.lblDesc.HyperLink = null;
			this.lblDesc.Left = 0.9375F;
			this.lblDesc.Name = "lblDesc";
			this.lblDesc.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDesc.Text = "Description";
			this.lblDesc.Top = 0.3125F;
			this.lblDesc.Width = 2F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3125F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "- - - Type Summary - - -";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 2.9375F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 1F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 3.9375F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 1F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 4.9375F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 1F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 5.9375F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 0F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldType.Text = null;
			this.fldType.Top = 0F;
			this.fldType.Width = 0.5F;
			// 
			// fldDesc
			// 
			this.fldDesc.Height = 0.1875F;
			this.fldDesc.Left = 0.9375F;
			this.fldDesc.Name = "fldDesc";
			this.fldDesc.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldDesc.Text = null;
			this.fldDesc.Top = 0F;
			this.fldDesc.Width = 2F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.9375F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0.0625F;
			this.lblTotals.Width = 2F;
			// 
			// fldTotalPrin
			// 
			this.fldTotalPrin.Height = 0.1875F;
			this.fldTotalPrin.Left = 3F;
			this.fldTotalPrin.Name = "fldTotalPrin";
			this.fldTotalPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrin.Text = "0.00";
			this.fldTotalPrin.Top = 0.0625F;
			this.fldTotalPrin.Width = 1F;
			// 
			// fldTotalInt
			// 
			this.fldTotalInt.Height = 0.1875F;
			this.fldTotalInt.Left = 4F;
			this.fldTotalInt.Name = "fldTotalInt";
			this.fldTotalInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalInt.Text = "0.00";
			this.fldTotalInt.Top = 0.0625F;
			this.fldTotalInt.Width = 1F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 5F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTax.Text = "0.00";
			this.fldTotalTax.Top = 0.0625F;
			this.fldTotalTax.Width = 1F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 6F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0.0625F;
			this.fldTotalTotal.Width = 1F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 3F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Width = 4F;
			this.lnTotals.X1 = 3F;
			this.lnTotals.X2 = 7F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// sarARDATypeSummary
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDesc;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
	}
}
