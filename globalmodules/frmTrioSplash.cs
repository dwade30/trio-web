﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Text;

#if TWCR0000
using TWCR0000;


#elif TWCE0000
using modGlobal = TWCE0000.modMain;


#elif TWMV0000
using modGlobal = TWCE0000.modGlobalVariables;


#elif TWPY0000
using modGlobal = TWCE0000.modGlobalVariables;


#elif TWCK0000
using TWCK0000;
using modGlobal = TWCK0000.modGNBas;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmTrioSplash.
	/// </summary>
	public partial class frmTrioSplash : BaseForm
	{
		public frmTrioSplash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTrioSplash InstancePtr
		{
			get
			{
				return (frmTrioSplash)Sys.GetInstance(typeof(frmTrioSplash));
			}
		}

		protected frmTrioSplash _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// Date           :                                       *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :               10/30/2002              *
		// ********************************************************
		// *******This code needs to be put into the Menu Option Click event
		// Private Sub mnuOptionsChangeUser_Click()
		// frmTrioSplash.Show vbModal
		// End Sub
		// *******This sub needs to be put into your MDIParent form as a public sub
		// *******frmTRIOSplash will call it when it has changed the security options
		// *******You have to use your own menu calls in the select case
		// Public Sub ChangeUser()
		// this sub use the security class and refresh the menu options reflecting the new user's security level
		// Set secUser = New clsTrioSecurity
		// secUser.Init "CR"
		//
		// refresh the grid
		// Select Case GRID.Tag           'this should be all of your headings and get menu calls, a good example would be to look in your Menu1 Subs
		// Case "Main"
		// GetCRMenu
		// Case "File"
		// GetFileMenu
		// Case Else
		// GetCRMenu
		// End Select
		// End Sub
		int BadPassword;
		string strCityTown;
		clsDRWrapper rsSec = new clsDRWrapper();

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show("Are you sure that you want to exit TRIO?", "Exit TRIO", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				modReplaceWorkFiles.EntryFlagFile(true, "");
				frmTrioSplash.InstancePtr.Close();
				Application.Exit();
			}
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			int intXX = 0;
			if (cboUserID.Text != "")
			{
				rsSec.FindFirstRecord("UserID", cboUserID.Text);
				if (FCConvert.ToString(rsSec.Get_Fields_String("PASSWORD")) == string.Empty)
				{
					MessageBox.Show("No password is defined for this user.  Please check your account setup.", "Password Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				if (Strings.UCase(modEncrypt.ToggleEncryptCode(rsSec.Get_Fields_String("PASSWORD"))) == Strings.UCase(txtPassword.Text))
				{
					intXX = FCConvert.ToInt32(rsSec.Get_Fields_Int32("ID"));
					modReplaceWorkFiles.EntryFlagFile(true, "YY");
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "SecurityID", modGlobal.PadToString(intXX, 3));
					Close();
					MDIParent.InstancePtr.ChangeUser();
				}
				else
				{
					BadPassword += 1;
					if (BadPassword == 3)
					{
						MessageBox.Show("Password Violation!   Program will be Terminated.", "INVALID PASSWORD", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						frmTrioSplash.InstancePtr.Close();
						return;
					}
					MessageBox.Show("Please Re-Enter Password.", "Incorrect Password", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtPassword.Text = "";
					txtPassword.Focus();
				}
			}
		}

		private void frmTrioSplash_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (MessageBox.Show("Are you sure that you want to exit TRIO?", "Exit TRIO", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					frmTrioSplash.InstancePtr.Close();
					Application.Exit();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTrioSplash_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTrioSplash.ScaleWidth	= 8190;
			//frmTrioSplash.ScaleHeight	= 6855;
			//frmTrioSplash.ClipControls	= 0   'False;
			//frmTrioSplash.LinkTopic	= "Form2";
			//End Unmaped Properties
			strCityTown = "Municipality";
			if (rsSec.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings"))
			{
				if (rsSec.EndOfFile() != true && rsSec.BeginningOfFile() != true)
				{
					strCityTown = FCConvert.ToString(rsSec.Get_Fields_String("CityTown"));
				}
			}
			lblWarning.Text = "Warning:  This Program is protected by US and International Copyright Laws as discribed in Help About.  Copyright @" + FCConvert.ToString(DateTime.Now.Year) + " TRIO Software Corporation";
			//FC:FINAL:MSH - incorrect converting from byte array to string (same with i.issue #1071)
			//lblCompanyProduct.Text = FCConvert.ToString(Encoding.Default.GetBytes(strCityTown)) + " of " + modGlobalConstants.Statics.MuniName;
			lblCompanyProduct.Text = Encoding.Default.GetString(Encoding.Default.GetBytes(strCityTown)) + " of " + modGlobalConstants.Statics.MuniName;
			lblCompanyProduct.Left = FCConvert.ToInt32((frmTrioSplash.InstancePtr.Width - lblCompanyProduct.Width) / 2.0);
			lblVersion.Text = "Version " + FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision);
			lblProductName.Text = "TRIO";
			if (rsSec.OpenRecordset("SELECT * FROM Security", "SystemSettings"))
			{
				if (rsSec.EndOfFile() != true && rsSec.BeginningOfFile() != true)
				{
					do
					{
						// TODO: Check the table for the column [UserID] and replace with corresponding Get_Field method
						cboUserID.AddItem(FCConvert.ToString(rsSec.Get_Fields("UserID")));
						rsSec.MoveNext();
					}
					while (!rsSec.EndOfFile());
				}
				else
				{
					MessageBox.Show("User Information could not be loaded.  Please Quit this menu and try again.", "OpID Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				MessageBox.Show("User Information could not be loaded.  Please Quit this menu and try again.", "OpID Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			frmTrioSplash.InstancePtr.ZOrder(ZOrderConstants.SendToBack);
			this.Refresh();
		}

		private void StartTRIO()
		{
			//MDIParent.InstancePtr.Show();
		}
	}
}
