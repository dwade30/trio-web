﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMultiModuleEncumbranceLiquidation : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		clsDRWrapper rsGeneralInfo = new clsDRWrapper();
		clsDRWrapper rsGeneralEncInfo = new clsDRWrapper();
		clsDRWrapper rsDetailEncInfo = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		bool blnFirstRecord;

		public rptMultiModuleEncumbranceLiquidation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Encumbrance Liquidation Report";
            this.ReportEnd += RptMultiModuleEncumbranceLiquidation_ReportEnd;
		}

        private void RptMultiModuleEncumbranceLiquidation_ReportEnd(object sender, EventArgs e)
        {
			rsGeneralInfo.DisposeOf();
            rsGeneralEncInfo.DisposeOf();
            rsDetailEncInfo.DisposeOf();
            rsVendorInfo.DisposeOf();

		}

        public static rptMultiModuleEncumbranceLiquidation InstancePtr
		{
			get
			{
				return (rptMultiModuleEncumbranceLiquidation)Sys.GetInstance(typeof(rptMultiModuleEncumbranceLiquidation));
			}
		}

		protected rptMultiModuleEncumbranceLiquidation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsVendorInfo.Dispose();
				rsGeneralInfo.Dispose();
				rsDetailEncInfo.Dispose();
				rsGeneralEncInfo.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rsGeneralInfo.EndOfFile();
			}
			else
			{
				rsGeneralInfo.MoveNext();
				eArgs.EOF = rsGeneralInfo.EndOfFile();
			}
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			rsGeneralInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE EncumbranceDetailRecord <> 0 AND APJournalID IN (SELECT ID FROM APJournal WHERE EncumbranceRecord <> 0 AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + ")", "TWBD0000.vb1");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rsDetailEncInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = " + FCConvert.ToString(rsGeneralInfo.Get_Fields_Int32("EncumbranceDetailRecord")), "TWBD0000.vb1");
			rsGeneralEncInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(rsDetailEncInfo.Get_Fields_Int32("EncumbranceID")), "TWBD0000.vb1");
			if (((rsGeneralEncInfo.Get_Fields_Int32("VendorNumber"))) == 0)
			{
				fldVendor.Text = "0000 " + FCConvert.ToString(rsGeneralEncInfo.Get_Fields_String("TempVendorName"));
			}
			else
			{
				rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(rsGeneralEncInfo.Get_Fields_Int32("VendorNumber")), "TWBD0000.vb1");
				fldVendor.Text = Strings.Format(rsGeneralEncInfo.Get_Fields_Int32("VendorNumber"), "00000") + " " + FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"));
			}
			// TODO: Check the table for the column [PO] and replace with corresponding Get_Field method
			fldReference.Text = FCConvert.ToString(rsGeneralEncInfo.Get_Fields("PO"));
			fldLiquidated.Text = Strings.Format(rsGeneralInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
			// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
			fldRemaining.Text = Strings.Format(Conversion.Val(rsDetailEncInfo.Get_Fields("Amount")) + Conversion.Val(rsDetailEncInfo.Get_Fields_Decimal("Adjustments")) - Conversion.Val(rsDetailEncInfo.Get_Fields_Decimal("Liquidated")), "#,##0.00");
			// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsDetailEncInfo.Get_Fields_String("Account");
		}

		private void rptMultiModuleEncumbranceLiquidation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMultiModuleEncumbranceLiquidation.Caption	= "Encumbrance Liquidation Report";
			//rptMultiModuleEncumbranceLiquidation.Icon	= "rptMultiModuleEncumbranceLiquidation.dsx":0000";
			//rptMultiModuleEncumbranceLiquidation.Left	= 0;
			//rptMultiModuleEncumbranceLiquidation.Top	= 0;
			//rptMultiModuleEncumbranceLiquidation.Width	= 11880;
			//rptMultiModuleEncumbranceLiquidation.Height	= 8595;
			//rptMultiModuleEncumbranceLiquidation.StartUpPosition	= 3;
			//rptMultiModuleEncumbranceLiquidation.SectionData	= "rptMultiModuleEncumbranceLiquidation.dsx":058A;
			//End Unmaped Properties
		}
	}
}
