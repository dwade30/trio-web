﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using TWSharedLibrary;

#if TWBL0000
using TWBL0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for rptEnhancedBETELeadSheet.
	/// </summary>
	public partial class rptEnhancedBETELeadSheet : BaseSectionReport
	{
		public rptEnhancedBETELeadSheet()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "BETE";
		}

		public static rptEnhancedBETELeadSheet InstancePtr
		{
			get
			{
				return (rptEnhancedBETELeadSheet)Sys.GetInstance(typeof(rptEnhancedBETELeadSheet));
			}
		}

		protected rptEnhancedBETELeadSheet _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEnhancedBETELeadSheet	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cEnhancedBETE theModel;
		private int intCount;

		public void Init(ref cEnhancedBETE reportModel)
		{
			theModel = reportModel;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs e)
		{
			if (intCount < 1)
			{
				e.EOF = false;
				txtMunicipality.Text = theModel.Municipality;
				lblYear1.Text = theModel.ReportYear.ToString();
				txtTitle.Text = FCConvert.ToString(theModel.ReportYear) + " ENHANCED BETE MUNICIPAL TAX RATE CALCULATION FORM";
				txt1a.Text = "$" + Strings.Format(theModel.TotalValuationBETEQualifiedExemptProperty, "#,###,###,##0");
				txt1b.Text = "$" + Strings.Format(theModel.TotalValuationBETENotInTIF, "#,###,###,##0");
				txt1c.Text = Strings.Format(theModel.BETEPercent * 100, "0.00") + "%";
				txt1d.Text = "$" + Strings.Format(theModel.TotalValuationBETEStandardReim, "#,###,###,##0");
				txt2a.Text = "$" + Strings.Format(theModel.TotalValueBusinessPersonalProperty, "#,###,###,##0");
				txt2b.Text = "$" + Strings.Format(theModel.TotalValueTaxableProperty, "#,###,###,##0");
				txt2c.Text = "$" + Strings.Format(theModel.TotalValuationBETEEnhancedReimNotInTif, "#,###,###,##0");
				txt2d.Text = Strings.Format(theModel.PersonalPropertyFactor * 100, "0.00") + "%";
				txt2e.Text = Strings.Format(theModel.HalfFactor * 100, "0.00") + "%";
				txt2f.Text = Strings.Format(theModel.HalfPlus50 * 100, "0.00") + "%";
				txt2g.Text = Strings.Format(theModel.TotalValuationBeteSubjectEnhancedReim, "#,###,###,##0");
				txtRetentionPercent.Text = Strings.Format(theModel.RetentionPercentage * 100, "0.00") + "%";
				txt3a.Text = Strings.Format(theModel.EffectivePercentage * 100, "0.00") + "%";
				txt3b.Text = "$" + Strings.Format(theModel.BETEValuationInTIF, "#,###,###,##0");
				txt3c.Text = "$" + Strings.Format(theModel.TIFBETEQualifiedValuationReimbursable, "#,###,###,##0");
				txt4a.Text = "$" + Strings.Format(theModel.TotalReimbursableBETE, "#,###,###,##0");
			}
			intCount += 1;
		}

		
	}
}
