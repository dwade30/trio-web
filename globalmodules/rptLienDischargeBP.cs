﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptLienDischargeBP : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/30/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/06/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsMaster = new clsDRWrapper();
		int lngAccount;

		public rptLienDischargeBP()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "30 Day Notice";
            this.ReportEnd += RptLienDischargeBP_ReportEnd;
		}

        private void RptLienDischargeBP_ReportEnd(object sender, EventArgs e)
        {
			rsData.DisposeOf();
            rsMaster.DisposeOf();

		}

        public static rptLienDischargeBP InstancePtr
		{
			get
			{
				return (rptLienDischargeBP)Sys.GetInstance(typeof(rptLienDischargeBP));
			}
		}

		protected rptLienDischargeBP _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
            rsData.DisposeOf();
            rsMaster.DisposeOf();

		}

		public void Init(int lngPassAccount)
		{
			string strFields;
			lngAccount = lngPassAccount;
			strFields = "Name1,LienRec.RateKey as LienRateKey,LienRec.ID,BillingYear,MapLot,Book,Page";
			rsData.OpenRecordset($"SELECT {strFields} FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE Account = {FCConvert.ToString(lngAccount)}", modExtraModules.strCLDatabase);
			rsMaster.OpenRecordset($"SELECT * FROM Master WHERE RSAccount = {FCConvert.ToString(lngAccount)}", modExtraModules.strREDatabase);
			if (rsData.EndOfFile() || rsMaster.EndOfFile())
			{
				// need to find both a RE master and a Liened Bill
				Cancel();
				return;
			}
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fldDate.Text = frmLienDischargeNoticeBP.InstancePtr.txtDate.Text;
				fldHeading.Text = "Dear " + FCConvert.ToString(rsData.Get_Fields_String("Name1")) + ",";
				fldTopText.Text = frmLienDischargeNoticeBP.InstancePtr.txtTop.Text;
				lblYear.Text = "Year";
				lblMapLot.Text = "Map Lot";
				lblLienDate.Text = "Lien Date";
				lblLienBook.Text = "Book";
				lblLienPage.Text = "Page";
				lblDischargeDate.Text = "Discharge Date";
				lblLDNBook.Text = "Book";
				lblLDNPage.Text = "Page";
				fldBottomText.Text = frmLienDischargeNoticeBP.InstancePtr.txtBottom.Text;
				fldEnding.Text = "Sincerely," + "\r\n" + "\r\n";
				fldEnding.Text = fldEnding.Text + frmLienDischargeNoticeBP.InstancePtr.txtName1.Text + "\r\n";
				fldEnding.Text = fldEnding.Text + frmLienDischargeNoticeBP.InstancePtr.txtName2.Text + "\r\n";
				fldEnding.Text = fldEnding.Text + frmLienDischargeNoticeBP.InstancePtr.txtName3.Text + "\r\n";
				fldEnding.Text = fldEnding.Text + frmLienDischargeNoticeBP.InstancePtr.txtName4.Text + "\r\n";
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Report");
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLDN = new clsDRWrapper();
				clsDRWrapper rsRK = new clsDRWrapper();
				fldYear.Text = modExtraModules.FormatYear(FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")));
				fldMapLot.Text = rsData.Get_Fields_String("MapLot");
				// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
				fldLienBook.Text = FCConvert.ToString(rsData.Get_Fields("Book"));
				// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
				fldLienPage.Text = FCConvert.ToString(rsData.Get_Fields("Page"));
				// TODO: Field [LienRateKey] not found!! (maybe it is an alias?)
				rsRK.OpenRecordset("SELECT BillingDate FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields("LienRateKey")), modExtraModules.strCLDatabase);
				if (!rsRK.EndOfFile())
				{
					fldLienDate.Text = Strings.Format(rsRK.Get_Fields_DateTime("BillingDate"), "MM/dd/yyyy");
				}
				else
				{
					fldLienDate.Text = "";
				}
				rsLDN.OpenRecordset("SELECT DatePaid, Book, Page FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")), modExtraModules.strCLDatabase);
				if (!rsLDN.EndOfFile())
				{
					fldDischargeDate.Text = Strings.Format(rsLDN.Get_Fields_DateTime("DatePaid"), "MM/dd/yyyy");
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					fldLDNBook.Text = FCConvert.ToString(rsLDN.Get_Fields("Book"));
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					fldLDNPage.Text = FCConvert.ToString(rsLDN.Get_Fields("Page"));
				}
				else
				{
					fldDischargeDate.Text = "";
					fldLDNBook.Text = "";
					fldLDNPage.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Binding Fields");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsData.EndOfFile())
			{
				BindFields();
				rsData.MoveNext();
			}
		}

		private void rptLienDischargeBP_Load(object sender, System.EventArgs e)
		{

		}
	}
}
