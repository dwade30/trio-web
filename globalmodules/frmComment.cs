﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;

#if TWAR0000
using TWAR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;


#elif TWCE0000
using modGlobal = TWCE0000.modMDIParent;


#elif TWMV0000
using modGlobal = TWMV0000.modGlobalRoutines;


#elif TWCK0000
using modGlobal = TWCK0000.modGNBas;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmComment.
	/// </summary>
	public partial class frmComment : BaseForm
	{
		public frmComment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmComment InstancePtr
		{
			get
			{
				return (frmComment)Sys.GetInstance(typeof(frmComment));
			}
		}

		protected frmComment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intFrmSize;
		string strMod;
		int lngIDNumber;
		int UserCol;
		int ModuleCol;
		int CommentCol;

		private void frmComment_Activated(object sender, System.EventArgs e)
		{
			if (!modGlobal.FormExist(this))
			{
				fgrdComments.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
				fgrdComments.AutoSize(0, fgrdComments.Cols - 1);
			}
		}

		private void frmComment_KeyDown(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmComment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmComment.ScaleWidth	= 9045;
			//frmComment.ScaleHeight	= 7410;
			//frmComment.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, intFrmSize);
			modGlobalFunctions.SetTRIOColors(this);
			UserCol = 1;
			ModuleCol = 2;
			CommentCol = 3;
			fgrdComments.TextMatrix(0, UserCol, "Entered By");
			fgrdComments.TextMatrix(0, ModuleCol, "Program");
			fgrdComments.TextMatrix(0, CommentCol, "Comment");
			fgrdComments.ColWidth(0, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.03));
			fgrdComments.ColWidth(UserCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.18));
			fgrdComments.ColWidth(ModuleCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.12));
			fgrdComments.ColWidth(CommentCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.03));
		}

		public void Init(string strModule, int lngID, string strFormCaption = "Comment", string strControlCaption = "", short intFormSize = modGlobalConstants.TRIOWINDOWSIZEBIGGIE, bool boolModal = false)
		{
			// parameters:
			// strmodule   the two letter abbrev. for your module
			// lngid       the Party ID number
			// strformcaption   the caption of the form.  it is "Comment" by default
			// strcontrolcaption an optional explanation of the data in the control
			// intFormSize the size of the window (triowindowsizebiggie etc.)
			// Returns:
			// whether the comment is blank or not
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			FCCollection pComments = new FCCollection();
			intFrmSize = intFormSize;
			this.Text = strFormCaption;
			lblTitle.Text = strControlCaption;
			strMod = strModule;
			lngIDNumber = lngID;
			intFrmSize = intFormSize;
			//if (intFrmSize==modGlobalConstants.TRIOWINDOWSIZEBIGGIE) {
			//	vsElasticLight1.Enabled = true;
			//}
			fgrdComments.Rows = 1;
			pInfo = pCont.GetParty(lngID);
			if (!(pInfo == null))
			{
				pComments = pInfo.GetModuleSpecificComments(strMod);
				// kk01062015 trouts-119  Use the module passed in   '  .GetModuleSpecificComments("AR")
				if (pComments.Count > 0)
				{
					foreach (cPartyComment pComm in pComments)
					{
						fgrdComments.Rows += 1;
						fgrdComments.TextMatrix(fgrdComments.Rows - 1, UserCol, pComm.EnteredBy);
						fgrdComments.TextMatrix(fgrdComments.Rows - 1, ModuleCol, pComm.ProgModule);
						fgrdComments.TextMatrix(fgrdComments.Rows - 1, CommentCol, pComm.Comment);
					}
				}
			}
			// fgrdComments.Cell(flexcpFontSize, 0, 0, fgrdComments.rows - 1, fgrdComments.Cols - 1) = 9
			// 
			// fgrdComments.AutoSizeMode = flexAutoSizeRowHeight
			// fgrdComments.AutoSize 0, fgrdComments.Cols - 1
			if (boolModal)
			{
				this.Show(FormShowEnum.Modal, App.MainForm);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void frmComment_Resize(object sender, System.EventArgs e)
		{
			if (intFrmSize == modGlobalConstants.TRIOWINDOWSIZEBIGGIE)
				return;
			//fgrdComments.Width = frmComment.InstancePtr.Width - (fgrdComments.Left * 2) - 100;
			lblTitle.Width = fgrdComments.Width;
			fgrdComments.Height = ClientArea.Height - fgrdComments.Top - BottomPanel.Height;
			fgrdComments.ColWidth(0, FCConvert.ToInt32(fgrdComments.Width * 0.03));
			fgrdComments.ColWidth(UserCol, FCConvert.ToInt32(fgrdComments.Width * 0.18));
			fgrdComments.ColWidth(ModuleCol, FCConvert.ToInt32(fgrdComments.Width * 0.12));
			fgrdComments.ColWidth(CommentCol, FCConvert.ToInt32(fgrdComments.Width * 0.03));
			// fgrdComments.Cell(flexcpFontSize, 0, 0, fgrdComments.rows - 1, fgrdComments.Cols - 1) = 9
			// fgrdComments.AutoSizeMode = flexAutoSizeRowHeight
			// fgrdComments.AutoSize 0, fgrdComments.Cols - 1
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
