﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cSecuritySetup
	{
		//=========================================================
		private FCCollection permtypes = new FCCollection();

		public FCCollection GetPermissionSetup(string strModuleCode)
		{
			FCCollection GetPermissionSetup = null;
			if (Strings.LCase(strModuleCode) == "re")
			{
				GetPermissionSetup = CreateREPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "pp")
			{
				GetPermissionSetup = CreatePPPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "bl")
			{
				GetPermissionSetup = CreateBLPermissions();
			}
			else if ((Strings.LCase(strModuleCode) == "ge") || (Strings.LCase(strModuleCode) == "gn"))
			{
				GetPermissionSetup = CreateGNPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "fa")
			{
				GetPermissionSetup = CreateFAPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "ck")
			{
				GetPermissionSetup = CreateCKPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "ar")
			{
				GetPermissionSetup = CreateARPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "bd")
			{
				GetPermissionSetup = CreateBDPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "cl")
			{
				GetPermissionSetup = CreateCLPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "cr")
			{
				GetPermissionSetup = CreateCRPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "ce")
			{
				GetPermissionSetup = CreateCEPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "e9")
			{
				GetPermissionSetup = CreateE9Permissions();
			}
			else if (Strings.LCase(strModuleCode) == "mv")
			{
				GetPermissionSetup = CreateMVPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "rb")
			{
				GetPermissionSetup = CreateRBPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "py")
			{
				GetPermissionSetup = CreatePYPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "cp")
			{
				GetPermissionSetup = CreateCPPermissions();
			}
			else if (Strings.LCase(strModuleCode) == "ut")
			{
				GetPermissionSetup = CreateUTPermissions();
			}
			else
			{
				GetPermissionSetup = new FCCollection();
			}
			return GetPermissionSetup;
		}

		private FCCollection CreateUTPermissions()
		{
			FCCollection CreateUTPermissions = null;
			cUTSecuritySetup tSetup = new cUTSecuritySetup();
			CreateUTPermissions = tSetup.Setup;
			return CreateUTPermissions;
		}

		private FCCollection CreateCPPermissions()
		{
			FCCollection CreateCPPermissions = null;
			cCPSecuritySetup tSetup = new cCPSecuritySetup();
			CreateCPPermissions = tSetup.Setup;
			return CreateCPPermissions;
		}

		private FCCollection CreateFAPermissions()
		{
			FCCollection CreateFAPermissions = null;
			cFASecuritySetup tSetup = new cFASecuritySetup();
			CreateFAPermissions = tSetup.Setup;
			return CreateFAPermissions;
		}

		private FCCollection CreateCKPermissions()
		{
			FCCollection CreateCKPermissions = null;
			cCKSecuritySetup tSetup = new cCKSecuritySetup();
			CreateCKPermissions = tSetup.Setup;
			return CreateCKPermissions;
		}

		private FCCollection CreateARPermissions()
		{
			FCCollection CreateARPermissions = null;
			cARSecuritySetup tSetup = new cARSecuritySetup();
			CreateARPermissions = tSetup.Setup;
			return CreateARPermissions;
		}

		private FCCollection CreateBDPermissions()
		{
			FCCollection CreateBDPermissions = null;
			cBDSecuritySetup tSetup = new cBDSecuritySetup();
			CreateBDPermissions = tSetup.Setup;
			return CreateBDPermissions;
		}

		private FCCollection CreateREPermissions()
		{
			FCCollection CreateREPermissions = null;
			cRESecuritySetup tSetup = new cRESecuritySetup();
			CreateREPermissions = tSetup.Setup;
			return CreateREPermissions;
		}

		private FCCollection CreatePPPermissions()
		{
			FCCollection CreatePPPermissions = null;
			cPPSecuritySetup tSetup = new cPPSecuritySetup();
			CreatePPPermissions = tSetup.Setup;
			return CreatePPPermissions;
		}

		private FCCollection CreateBLPermissions()
		{
			FCCollection CreateBLPermissions = null;
			cBLSecuritySetup tSetup = new cBLSecuritySetup();
			CreateBLPermissions = tSetup.Setup;
			return CreateBLPermissions;
		}

		private FCCollection CreateCLPermissions()
		{
			FCCollection CreateCLPermissions = null;
			cCLSecuritySetup tSetup = new cCLSecuritySetup();
			CreateCLPermissions = tSetup.Setup;
			return CreateCLPermissions;
		}

		private FCCollection CreateCRPermissions()
		{
			FCCollection CreateCRPermissions = null;
			cCRSecuritySetup tSetup = new cCRSecuritySetup();
			CreateCRPermissions = tSetup.Setup;
			return CreateCRPermissions;
		}

		private FCCollection CreateCEPermissions()
		{
			FCCollection CreateCEPermissions = null;
			cCESecuritySetup tSetup = new cCESecuritySetup();
			CreateCEPermissions = tSetup.Setup;
			return CreateCEPermissions;
		}

		private FCCollection CreateE9Permissions()
		{
			FCCollection CreateE9Permissions = null;
			cE9SecuritySetup tSetup = new cE9SecuritySetup();
			CreateE9Permissions = tSetup.Setup;
			return CreateE9Permissions;
		}

		private FCCollection CreateGNPermissions()
		{
			FCCollection CreateGNPermissions = null;
			cGESecuritySetup tSetup = new cGESecuritySetup();
			CreateGNPermissions = tSetup.Setup;
			return CreateGNPermissions;
		}

		private FCCollection CreateMVPermissions()
		{
			FCCollection CreateMVPermissions = null;
			cMVSecuritySetup tSetup = new cMVSecuritySetup();
			CreateMVPermissions = tSetup.Setup;
			return CreateMVPermissions;
		}

		private FCCollection CreateRBPermissions()
		{
			FCCollection CreateRBPermissions = null;
			FCCollection tColl = new FCCollection();
			CreateRBPermissions = tColl;
			return CreateRBPermissions;
		}

		private FCCollection CreatePYPermissions()
		{
			FCCollection CreatePYPermissions = null;
			cPYSecuritySetup tSetup = new cPYSecuritySetup();
			CreatePYPermissions = tSetup.Setup;
			return CreatePYPermissions;
		}
	}
}
