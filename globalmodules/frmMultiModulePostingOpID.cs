﻿using fecherFoundation;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmMultiModulePostingOpID.
	/// </summary>
	public partial class frmMultiModulePostingOpID : BaseForm
	{
		public frmMultiModulePostingOpID()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMultiModulePostingOpID InstancePtr
		{
			get
			{
				return (frmMultiModulePostingOpID)Sys.GetInstance(typeof(frmMultiModulePostingOpID));
			}
		}

		protected frmMultiModulePostingOpID _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmMultiModulePostingOpID_Activated(object sender, System.EventArgs e)
		{
			txtOpId.Focus();
		}

		private void frmMultiModulePostingOpID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				modBudgetaryAccounting.Statics.strOpID = Strings.Trim(txtOpId.Text);
				if (modBudgetaryAccounting.Statics.strOpID.Length != 3)
				{
					FCMessageBox.Show("Operator ID must be 3 characters in length", MsgBoxStyle.Exclamation, "Error");
					modBudgetaryAccounting.Statics.strOpID = "";
					txtOpId.Text = "";
					txtOpId.Focus();
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmMultiModulePostingOpID_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMultiModulePostingOpID.ScaleWidth	= 2895;
			//frmMultiModulePostingOpID.ScaleHeight	= 1635;
			//frmMultiModulePostingOpID.LinkTopic	= "Form1";
			//txtOpId.IMEMode	= 3  'DISABLE;
			//txtOpId.MaxLength	= 3;
			//txtOpId.PasswordChar	= "l";
			//vsElasticLight1.OleObjectBlob	= "frmMultiModulePostingOpID.frx":0000";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
			modGlobalFunctions.SetTRIOColors(this, false);
		}
	}
}
