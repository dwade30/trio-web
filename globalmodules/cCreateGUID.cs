﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Text;
using System.Runtime.InteropServices;

namespace Global
{
	public class cCreateGUID
	{
		//=========================================================
		//DefLng(A-Z);
		[StructLayout(LayoutKind.Sequential)]
		private struct GUID
		{
			public int data1;
			public short Data2;
			public short Data3;
			public char[] Data4/* = new char[8]*/;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public GUID(int unusedParam)
			{
				this.data1 = 0;
				this.Data2 = 0;
				this.Data3 = 0;
				this.Data4 = new char[8];
			}
		};

		[DllImport("ole32.dll")]
		private static extern int CoCreateGuid(ref GUID tGUIDStructure);

		[DllImport("ole32.dll")]
		private static extern int StringFromGUID2([MarshalAs(UnmanagedType.LPStruct)] GUID rguid, [Out] StringBuilder lpstrClsId, int cbMax);

		private bool mbNoBraces;
		//
		public bool bNoBraces
		{
			get
			{
				bool bNoBraces = false;
				bNoBraces = mbNoBraces;
				return bNoBraces;
			}
			set
			{
				mbNoBraces = value;
			}
		}

		public string CreateGUID(bool bNoBraces = false)
		{
			//FC:FINAL:AM:#i458 - use .NET Guid
			//string CreateGUID = "";
			//string sGUID = "";
			//// store result here
			//GUID tGUID = new GUID();
			//// get into this structure
			//StringBuilder bGuid;
			//// get formatted string here
			//int lRtn = 0;
			//const int clLen = 50;
			//this.bNoBraces = bNoBraces;
			//if (CoCreateGuid(ref tGUID) == 0)
			//{
			//    // use API to get the GUID
			//    bGuid = new StringBuilder(50);
			//    lRtn = StringFromGUID2(tGUID, bGuid, clLen);
			//    // use API to format it
			//    if (lRtn > 0)
			//    {
			//        // truncate nulls
			//        //sGUID = Strings.Mid(Encoding.UTF8.GetString(bGuid), 1, lRtn-1);
			//        sGUID = bGuid.ToString(1, lRtn - 1);
			//    }
			//    if (FCConvert.CBool(this.bNoBraces))
			//    {
			//        CreateGUID = Strings.Mid(sGUID, 2, sGUID.Length - 2);
			//        // 2005/12/18 Remove braces {}
			//    }
			//    else
			//    {
			//        CreateGUID = sGUID;
			//    }
			//}
			//return CreateGUID;
			return Guid.NewGuid().ToString();
		}

		public bool IsValidGUID(ref object GUID)
		{
			bool IsValidGUID = false;
			IsValidGUID = IsGUIDValid(ref GUID);
			return IsValidGUID;
		}

		public bool IsGUIDValid(ref object GUID)
		{
			bool IsGUIDValid = false;
			const string sSample = "{0547C3D5-FA24-11D0-B3F9-004445535400}";
			string[] ary = null;
			string sTemp;
			int iPos = 0;
			if (FCUtils.IsNull(GUID))
				return IsGUIDValid;
			// 2004/12/31 Added
			if (FCUtils.IsEmpty(GUID))
				return IsGUIDValid;
			// 2004/12/31 Added
			sTemp = GUID.ToString();
			// convert to string
			sTemp = Strings.Trim(sTemp);
			// 2004/12/31 Make sure no extra spaces
			if (sTemp.Length < (sSample.Length - 2))
				return IsGUIDValid;
			// can't be less than min with out braces
			// 2003/03/21 Strip off prefix, if any
			if (sTemp.Length > 0)
			{
				if (Strings.Right(sTemp, 1) == "}")
				{
					// 2004/12/31 Prefix in this form supported
					iPos = Strings.InStr(sTemp, "{", CompareConstants.vbTextCompare/*?*/);
					// in first position?
					if (iPos > 1)
					{
						sTemp = Strings.Mid(sTemp, iPos);
					}
				}
			}
			// 2003/03/21 Add braces if none
			if (sTemp.Length == sSample.Length - 2)
			{
				// maybe no braces
				if (Strings.Left(sTemp, 1) != "{")
				{
					sTemp = "{" + sTemp;
				}
				if (Strings.Right(sTemp, 1) != "}")
				{
					sTemp += "}";
				}
			}
			if (sTemp.Length == sSample.Length)
			{
				// correct length
				if (Strings.Left(sTemp, 1) == "{" && Strings.Right(sTemp, 1) == "}")
				{
					// has braces
					if (FCConvert.ToBoolean(Strings.InStr(sTemp, "-", CompareConstants.vbTextCompare/*?*/)))
					{
						// 2004/12/31 Must have at least one dash
						ary = Strings.Split(sTemp, "-", -1, CompareConstants.vbBinaryCompare);
						// right number of dashes
						if (Information.UBound(ary, 1) == 4)
						{
							// must be this
							if (ary[0].Length == "{0547C3D5".Length)
							{
								// correct lengths
								if (ary[1].Length == 4)
								{
									if (ary[2].Length == 4)
									{
										if (ary[3].Length == 4)
										{
											if (ary[4].Length == "004445535400}".Length)
											{
												IsGUIDValid = true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return IsGUIDValid;
		}

		public string CreateGUIDWithPrefix(ref string sPrefix)
		{
			string CreateGUIDWithPrefix = "";
			string GUID;
			GUID = CreateGUID();
			GUID = sPrefix + GUID;
			CreateGUIDWithPrefix = GUID;
			return CreateGUIDWithPrefix;
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short LenGUID(object vntGUID = null)
		{
			short LenGUID = 0;
			if (Information.IsNothing(vntGUID))
			{
				LenGUID = FCConvert.ToInt16(CreateGUID().Length);
			}
			else
			{
				LenGUID = FCConvert.ToInt16(FCConvert.ToString(vntGUID).Length);
				// 2005/12/18 Added
			}
			return LenGUID;
		}
	}
}
