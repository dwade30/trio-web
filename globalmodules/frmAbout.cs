﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Runtime.InteropServices;
using SharedApplication.Enums;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for frmAbout.
	/// </summary>
	public partial class frmAbout : BaseForm
	{
		public frmAbout()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAbout InstancePtr
		{
			get
			{
				return (frmAbout)Sys.GetInstance(typeof(frmAbout));
			}
		}

		protected frmAbout _InstancePtr = null;

		private void frmAbout_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper rsGeneralInfo = new clsDRWrapper();

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			this.Text = "About ";
			
            lblVersionNumber.Text = Application.ProductVersion;
            //lblTitle.Text = App.MainForm.Caption;
			lblDisclaimer.Text = "Warning:  This program is protected by US and International Copyright Laws.  " + Environment.NewLine + FCConvert.ToString(Convert.ToChar(169)) + FCConvert.ToString(DateTime.Now.Year) + " Harris Computer Systems";
			rsGeneralInfo.OpenRecordset("SELECT * FROM Modules", "SystemSettings");
            //var exeName = Strings.UCase(Strings.Mid(App.EXEName, 3, 2));
            var moduleCaption = App.MainForm.Caption;
            string exeName = "GN";
            if (StaticSettings.gGlobalActiveModuleSettings.AccountsReceivableIsActive)
            {
                exeName = "AR";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.BudgetaryIsActive)
            {
                exeName = "BD";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.CashReceiptingIsActive)
            {
                exeName = "CR";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.ClerkIsActive)
            {
                exeName = "CK";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.CodeEnforcementIsActive)
            {
                exeName = "CE";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.FixedAssetsIsActive)
            {
                exeName = "FA";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.MotorVehicleIsActive)
            {
                exeName = "MV";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.PayrollIsActive)
            {
                exeName = "PY";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.PersonalPropertyIsActive)
            {
                exeName = "PP";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.RealEstateIsActive)
            {
                exeName = "RE";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.UtilityBillingIsActive)
            {
                exeName = "UT";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.TaxBillingIsActive)
            {
                exeName = "BL";
            }
            else if (StaticSettings.gGlobalActiveModuleSettings.RedbookIsActive)
            {
                exeName = "RB";
            }
            
            //if (moduleCaption.ToLower().Contains("collections"))
            //{
            //    exeName = "CL";
            //}
            //else if (moduleCaption.ToLower().Contains("clerk"))
            //{
            //    exeName = "CK";
            //}
            //else if (moduleCaption.ToLower().Contains("motor"))
            //{
            //    exeName = "MV";
            //}
            //else if (moduleCaption.ToLower().Contains("real estate"))
            //{
            //    exeName = "RE";
            //}
            //else if (moduleCaption.ToLower().Contains("personal"))
            //{
            //    exeName = "PP";
            //}
            //else if (moduleCaption.ToLower().Contains("payroll"))
            //{
            //    exeName = "PY";
            //}
            //else if (moduleCaption.ToLower().Contains("billing"))
            //{
            //    exeName = "BL";
            //}
            //else if (moduleCaption.ToLower().Contains("utility"))
            //{
            //    exeName = "UT";
            //}
            //else if (moduleCaption.ToLower().Contains("fixed"))
            //{
            //    exeName = "FA";
            //}
            //else if (moduleCaption.ToLower().Contains("budgetary"))
            //{
            //    exeName = "BD";
            //}
            //else if (moduleCaption.ToLower().Contains("cash"))
            //{
            //    exeName = "CR";
            //}
            //else if (moduleCaption.ToLower().Contains("code enforcement"))
            //{
            //    exeName = "CE";
            //}
            //else if (moduleCaption.ToLower().Contains("accounts"))
            //{
            //    exeName = "AR";
            //}
            //else if (moduleCaption.ToLower().Contains("blue book"))
            //{
            //    exeName = "RB";
            //}
            Label2.Visible = true;
            switch (exeName)
            {
                case "CL":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("CLDate"));
                    break;
                case "RE":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("REDate"));

                    break;
                case "PP":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("PPDate"));

                    break;
                case "BL":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("BLDate"));

                    break;
                case "CK":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("CKDate"));

                    break;
                case "VR":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("VRDate"));

                    break;
                case "CE":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("CEDate"));

                    break;
                case "BD":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("BDDate"));

                    break;
                case "MV":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("MVDate"));

                    break;
                case "CR":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("CRDate"));

                    break;
                case "PY":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("PYDate"));

                    break;
                case "UT":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("UTDate"));

                    break;
                case "E9":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("E9Date"));

                    break;
                case "FA":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("FADate"));

                    break;
                case "GN":                    
                    lblExpires.Text = "";
                    Label2.Visible = false;
                    break;
                case "AR":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("ARDate"));
                    break;
                case "RB":
                    lblExpires.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields_DateTime("RBDate"));
                    break;
            }
		}
		private void cmdOK_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
