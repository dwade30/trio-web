﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmLienDischargeNoticeBP.
	/// </summary>
	public partial class frmLienDischargeNoticeBP : BaseForm
	{
		public frmLienDischargeNoticeBP()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienDischargeNoticeBP InstancePtr
		{
			get
			{
				return (frmLienDischargeNoticeBP)Sys.GetInstance(typeof(frmLienDischargeNoticeBP));
			}
		}

		protected frmLienDischargeNoticeBP _InstancePtr = null;

		clsDRWrapper rsLien = new clsDRWrapper();
		clsDRWrapper rsSettings = new clsDRWrapper();
		int lngAccount;

		public void Init(DateTime dtPassPayDate, int lngPassAccount)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTest = new clsDRWrapper();
				bool boolRecords = false;
				lngAccount = lngPassAccount;
				// if all saved are to be printed, then check to see if there are any
				rsLien.OpenRecordset("SELECT * FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber WHERE Account = " + FCConvert.ToString(lngAccount), modExtraModules.strCLDatabase);
				if (!rsLien.EndOfFile())
				{
					// keep going
				}
				else
				{
					FCMessageBox.Show("Lien records for account number " + FCConvert.ToString(lngAccount) + " could not be found.", MsgBoxStyle.Exclamation, "Missing Lien Records");
					Close();
					return;
				}
				while (!(rsLien.EndOfFile() || boolRecords))
				{
					rsTest.OpenRecordset("SELECT top 1 * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(rsLien.Get_Fields_Int32("ID")), modExtraModules.strCLDatabase);
					boolRecords = !rsTest.EndOfFile();
					rsLien.MoveNext();
				}
				rsLien.MoveFirst();
				LoadSettings();
				if (boolRecords)
				{
					this.Show(App.MainForm);
				}
				else
				{
					FCMessageBox.Show("Cannot find a lien discharge created for account " + FCConvert.ToString(lngAccount) + ".", MsgBoxStyle.Exclamation, "Cannot Find Lien Discharge");
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing");
			}
		}

		private void frmLienDischargeNoticeBP_Load(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmLienDischargeNoticeBP_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SaveSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save all of the settings for the lien discharge notice
				clsDRWrapper rsTemp = new clsDRWrapper();
				DateTime dtComExp;
				bool boolTemp/*unused?*/;
				DateTime dtAppear;
				if (ValidateAnswers())
				{
					rsSettings.OpenRecordset("SELECT * FROM Control_DischargeNotice", modExtraModules.strCLDatabase);
					if (rsSettings.EndOfFile())
					{
						rsSettings.AddNew();
					}
					else
					{
						rsSettings.Edit();
					}
					rsSettings.Set_Fields("AltTopText", txtTop.Text);
					rsSettings.Set_Fields("AltBottomText", txtBottom.Text);
					rsSettings.Set_Fields("AltName1", txtName1.Text);
					rsSettings.Set_Fields("AltName2", txtName2.Text);
					rsSettings.Set_Fields("AltName3", txtName3.Text);
					rsSettings.Set_Fields("AltName4", txtName4.Text);
					rsSettings.Update();
					// show the report for this account
					rptLienDischargeBP.InstancePtr.Init(lngAccount);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Settings");
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
				lblStart.Text = "Dear, Taxpayer" + "\r\n" + "     ";
				lblBKList.Text = "Year      MapLot    Date           Book Page    Discharge Date   Book Page" + "\r\n";
				lblBKList.Text = lblBKList.Text + FCConvert.ToString(DateTime.Today.Year - 1) + "-1   000-000   00/00/0000   0000 0000    00/00/0000         0000 0000";
				txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				rsSettings.OpenRecordset("SELECT * FROM Control_DischargeNotice", modExtraModules.strCLDatabase);
				if (!rsSettings.EndOfFile())
				{
					if (FCConvert.ToString(rsSettings.Get_Fields_String("AltTopText")) != "" || FCConvert.ToString(rsSettings.Get_Fields_String("AltTopText")) != "")
					{
						txtTop.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AltTopText"));
						txtBottom.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AltBottomText"));
						txtName1.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AltName1"));
						txtName2.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AltName2"));
						txtName3.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AltName3"));
						txtName4.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AltName4"));
					}
					else
					{
						// start from scratch
						txtTop.Text = "This letter is to confirm the tax liens on the above referenced property through the municipality of " + modGlobalConstants.Statics.MuniName + ", have been satisfied.  Below is a list of said liens along with the lien discharge information.";
						txtBottom.Text = "This should be sufficient information to provide to bank institutions etc.  Should you have any questions, please to contact me.";
						txtName1.Text = "";
						txtName2.Text = "";
						txtName3.Text = "";
						txtName4.Text = "";
					}
				}
				else
				{
					// start from scratch
					txtTop.Text = "This letter is to confirm the tax liens on the above referenced property through the municipality of " + modGlobalConstants.Statics.MuniName + ", have been satisfied.  Below is a list of said liens along with the lien discharge information.";
					txtBottom.Text = "This should be sufficient information to provide to bank institutions etc.  Should you have any questions, please to contact me.";
					txtName1.Text = "";
					txtName2.Text = "";
					txtName3.Text = "";
					txtName4.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Settings");
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			// this will keep the data and print the lien discharge notice
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// right now I do not need to validate anything
				ValidateAnswers = true;
				return ValidateAnswers;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Validating Answers");
			}
			return ValidateAnswers;
		}

		private void CheckSavedLDN()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This routine will go through the saved LDN file and remove the ones that are not paid anymore.
				// It will also show a message with a list of the accounts that have been changed
				clsDRWrapper rsLDN = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				double dblXtraInt = 0;
				string strUnpaidLiens = "";
				int lngFound = 0;
				rsLDN.OpenRecordset("SELECT * FROM DischargeNeeded WHERE NOT (Printed = 1)", modExtraModules.strCLDatabase);
				// frmWait.Init "Please Wait..." & vbCrLf & "Calculating Eligibility", True, rsLDN.RecordCount, True
				while (!rsLDN.EndOfFile())
				{
					// frmWait.IncrementProgress
					App.DoEvents();
					rsLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsLDN.Get_Fields_Int32("LienKey")), modExtraModules.strCLDatabase);
					if (modCLCalculations.CalculateAccountCLLien(rsLien, DateTime.Today, ref dblXtraInt) > 0)
					{
						// check to see if the lien still has a 0 or lower balance
						lngFound += 1;
						rsLien.OpenRecordset("SELECT Account, BillingYear FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(rsLDN.Get_Fields_Int32("LienKey")), modExtraModules.strCLDatabase);
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						strUnpaidLiens += "Account : " + modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsLien.Get_Fields("Account")), 5) + " BillingYear : " + modExtraModules.FormatYear(FCConvert.ToString(rsLien.Get_Fields_Int32("BillingYear"))) + "\r\n";
						rsLDN.Delete();
						// if not then delete it
					}
					rsLDN.MoveNext();
				}
				switch (lngFound)
				{
					case 0:
						{
							// do nothing
							break;
						}
					case 1:
						{
							FCMessageBox.Show("There has been 1 lien found that does not have a zero or negative balance.  It will not be printed." + "\r\n" + strUnpaidLiens, MsgBoxStyle.Exclamation, "Unpaid Liens Found");
							break;
						}
					default:
						{
							FCMessageBox.Show("There have been " + FCConvert.ToString(lngFound) + " liens found that do not have a zero or negative balance.  They will not be printed." + "\r\n" + strUnpaidLiens, MsgBoxStyle.Exclamation, "Unpaid Liens Found");
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Checking Saved LDN");
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}
	}
}
