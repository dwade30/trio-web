﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using GrapeCity.ActiveReports;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for rptARDailyAuditMaster.
	/// </summary>
	public partial class rptARDailyAuditMaster : BaseSectionReport
	{
		// nObj = 1
		//   0	rptARDailyAuditMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/10/2005              *
		// ********************************************************
		public bool boolWide;
		// True if this report is in Wide Format
		public float lngWidth;
		// This is the Print Width of the report
		bool boolDone;
		public bool boolOrderByReceipt;
		// order by receipt
		public bool boolCloseOutAudit;
		// Actually close out the payments
		public bool boolLandscape;
		// is the report to be printed landscape?
		public rptARDailyAuditMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Daily Audit Report";
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static rptARDailyAuditMaster InstancePtr
		{
			get
			{
				return (rptARDailyAuditMaster)Sys.GetInstance(typeof(rptARDailyAuditMaster));
			}
		}

		protected rptARDailyAuditMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs e)
		{
			// this should let the detail section fire once
			if (boolDone)
			{
				e.EOF = boolDone;
			}
			else
			{
				e.EOF = boolDone;
				boolDone = true;
			}
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
            ActiveReport_Terminate(sender, e);

        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading", true);
		}

		private void SetupFields()
		{
			// find out which way the user wants to print
			boolWide = boolLandscape;
			if (boolWide)
			{
				// wide format
				lngWidth = 13500 / 1440f;
				Document.Printer.Landscape = true;
			}
			else
			{
				// narrow format
				lngWidth = 10750 / 1440f;
				Document.Printer.Landscape = false;
			}
			this.PrintWidth = lngWidth;
			sarARDailyAudit.Width = lngWidth;
		}

		public void StartUp(bool boolCloseOut, bool boolPassLandscape)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				boolCloseOutAudit = boolCloseOut;
				// if the boolAppend is on then the first report will ALWAYS be the RE audit
				// at the end of the first report the PP audit will be printed
				boolLandscape = boolPassLandscape;
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				SetupFields();
				frmWait.InstancePtr.IncrementProgress();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//clsDRWrapper rsCloseOut = new clsDRWrapper();
				string strSQL = "";
				// This will move all of the reports back 1 number until 9 and then save this as the first
				if (boolCloseOutAudit)
				{
					modGlobalFunctions.IncrementSavedReports("LastARDailyAudit");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastARDailyAudit1.RDF"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			sarARDailyAudit.Report = new arARDailyAudit();
		}

		private void rptARDailyAuditMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptARDailyAuditMaster.Caption	= "Daily Audit Report";
			//rptARDailyAuditMaster.Icon	= "rptARDailtAuditMaster.dsx":0000";
			//rptARDailyAuditMaster.Left	= 0;
			//rptARDailyAuditMaster.Top	= 0;
			//rptARDailyAuditMaster.Width	= 11880;
			//rptARDailyAuditMaster.Height	= 8595;
			//rptARDailyAuditMaster.StartUpPosition	= 3;
			//rptARDailyAuditMaster.SectionData	= "rptARDailtAuditMaster.dsx":058A;
			//End Unmaped Properties
		}
	}
}
