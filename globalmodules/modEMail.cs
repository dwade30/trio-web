﻿namespace Global
{
	public class modEMail
	{
		//=========================================================
		public const int CNSTEMAILSIGNATURETYPEPERSONAL = 1;
		// log-in specific signature
		public const int CNSTEMAILSIGNATURETYPEGLOBAL = 2;
		// town/city/company signature
		public const int CNSTEMAILSIGNATURETYPEDISCLAIMER = 3;
		// disclaimer signature
		public const int CNSTEMAILDRAFTBOX = 1;
		public const int CNSTEMAILSENTBOX = 2;
	}
}
