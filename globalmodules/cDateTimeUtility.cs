﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using fecherFoundation;

namespace TWPY0000
{
	public class cDateTimeUtility
	{
		//=========================================================
		[StructLayout(LayoutKind.Sequential)]
		private struct FILETIME
		{
			public int dwLowDateTime;
			public int dwHighDateTime;
		};

		[StructLayout(LayoutKind.Sequential)]
		private struct SYSTEMTIME
		{
			// SystemTime structure for the Win32 API
			// vbPorter upgrade warning: wYear As short	OnWriteFCConvert.ToInt32(
			public int wYear;
			// vbPorter upgrade warning: wMonth As short	OnWriteFCConvert.ToInt32(
			public int wMonth;
			public int wDayOfWeek;
			// vbPorter upgrade warning: wDay As short	OnWriteFCConvert.ToInt32(
			public int wDay;
			// vbPorter upgrade warning: wHour As short	OnWriteFCConvert.ToInt32(
			public int wHour;
			// vbPorter upgrade warning: wMinute As short	OnWriteFCConvert.ToInt32(
			public int wMinute;
			// vbPorter upgrade warning: wSecond As short	OnWriteFCConvert.ToInt32(
			public int wSecond;
			public int wMilliseconds;
		};

		[StructLayout(LayoutKind.Sequential)]
		private struct TIME_ZONE_INFORMATION
		{
			public int Bias;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 63 + 1)]
			public byte[] StandardName;
			// unicode (0-based)
			public SYSTEMTIME StandardDate;
			public int StandardBias;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 63 + 1)]
			public byte[] DaylightName;
			// unicode (0-based)
			public SYSTEMTIME DaylightDate;
			public int DaylightBias;
		};
		// Private Declare Sub SetSystemTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)
		[DllImport("kernel32")]
		private static extern void GetSystemTime(ref SYSTEMTIME lpSystemTime);

		[DllImport("kernel32")]
		private static extern int GetTimeZoneInformation(ref TIME_ZONE_INFORMATION lpTimeZoneInformation);

		[DllImport("kernel32")]
		private static extern int FileTimeToSystemTime(ref FILETIME lpFileTime, ref SYSTEMTIME lpSystemTime);

		[DllImport("kernel32")]
		private static extern int SystemTimeToFileTime(ref SYSTEMTIME lpSystemTime, ref FILETIME lpFileTime);

		[DllImport("kernel32")]
		private static extern int LocalFileTimeToFileTime(ref FILETIME lpLocalFileTime, ref FILETIME lpFileTime);

		[DllImport("kernel32")]
		private static extern int FileTimeToLocalFileTime(ref FILETIME lpFileTime, ref FILETIME lpLocalFileTime);

		public string GetUDTDateTime()
		{
			string GetUDTDateTime = "";
			const int TIME_ZONE_ID_DAYLIGHT = 2;
			TIME_ZONE_INFORMATION tzi = new TIME_ZONE_INFORMATION();
			int dwBias = 0;
			string sZone = "";
			string tmp;
			switch (GetTimeZoneInformation(ref tzi))
			{
				case TIME_ZONE_ID_DAYLIGHT:
					{
						dwBias = tzi.Bias + tzi.DaylightBias;
						sZone = " (" + Strings.Left(Encoding.UTF8.GetString(tzi.DaylightName), 1) + "DT)";
						break;
					}
				default:
					{
						dwBias = tzi.Bias + tzi.StandardBias;
						sZone = " (" + Strings.Left(Encoding.UTF8.GetString(tzi.StandardName), 1) + "ST)";
						break;
					}
			}
			//end switch
			tmp = "  " + Strings.Right("00" + (dwBias / 60).ToString(), 2) + Strings.Right("00" + (dwBias % 60).ToString(), 2) + sZone;
			if (dwBias > 0)
			{
				Strings.MidSet(ref tmp, 2, 1, "-");
			}
			else
			{
				Strings.MidSet(ref tmp, 2, 2, "+0");
			}
			GetUDTDateTime = Strings.Format(DateTime.Now, "ddd, dd MMM yyyy hh:mm:ss") + tmp;
			return GetUDTDateTime;
		}

		private void DateToSystemTime(DateTime the_date, ref SYSTEMTIME system_time)
		{
			system_time.wYear = the_date.Year;
			system_time.wMonth = the_date.Month;
			system_time.wDay = the_date.Day;
			system_time.wHour = the_date.Hour;
			system_time.wMinute = the_date.Minute;
			system_time.wSecond = the_date.Second;
		}
		// vbPorter upgrade warning: the_date As DateTime	OnWrite(double, DateTime)
		private void SystemTimeToDate(ref SYSTEMTIME system_time, ref DateTime the_date)
		{
			the_date = DateTime.FromOADate(fecherFoundation.DateAndTime.DateSerial(system_time.wYear, system_time.wMonth, system_time.wDay).ToOADate() + fecherFoundation.DateAndTime.TimeSerial(system_time.wHour, system_time.wMinute, system_time.wSecond).ToOADate());
		}
		// Convert a local time to UTC.
		public DateTime LocalTimeToUTC(DateTime the_date)
		{
            //FC:FINAL:SBE - #2767 - use .NET framework for converting local DateTime from/to utc DateTime
            return TimeZoneInfo.ConvertTimeToUtc(the_date, TimeZoneInfo.Local);
            //DateTime LocalTimeToUTC = System.DateTime.Now;
            //SYSTEMTIME system_time = new SYSTEMTIME();
            //FILETIME local_file_time = new FILETIME();
            //FILETIME utc_file_time = new FILETIME();
            //// Convert it into a SYSTEMTIME.
            //DateToSystemTime(the_date, ref system_time);
            //// Convert it to a FILETIME.
            //SystemTimeToFileTime(ref system_time, ref local_file_time);
            //// Convert it to a UTC time.
            //LocalFileTimeToFileTime(ref local_file_time, ref utc_file_time);
            //// Convert it to a SYSTEMTIME.
            //FileTimeToSystemTime(ref utc_file_time, ref system_time);
            //// Convert it to a Date.
            //SystemTimeToDate(ref system_time, ref the_date);
            //LocalTimeToUTC = the_date;
            //return LocalTimeToUTC;
        }
		// Convert a UTC time into local time.
		public DateTime UTCToLocalTime(DateTime the_date)
		{
            //FC:FINAL:SBE - #2767 - use .NET framework for converting local DateTime from/to utc DateTime
            the_date = DateTime.SpecifyKind(the_date, DateTimeKind.Utc);
            return the_date.ToLocalTime();
            //DateTime UTCToLocalTime = System.DateTime.Now;
			//SYSTEMTIME system_time = new SYSTEMTIME();
			//FILETIME local_file_time = new FILETIME();
			//FILETIME utc_file_time = new FILETIME();
			//// Convert it into a SYSTEMTIME.
			//DateToSystemTime(the_date, ref system_time);
			//// Convert it to a UTC time.
			//SystemTimeToFileTime(ref system_time, ref utc_file_time);
			//// Convert it to a FILETIME.
			//FileTimeToLocalFileTime(ref utc_file_time, ref local_file_time);
			//// Convert it to a SYSTEMTIME.
			//FileTimeToSystemTime(ref local_file_time, ref system_time);
			//// Convert it to a Date.
			//SystemTimeToDate(ref system_time, ref the_date);
			//UTCToLocalTime = the_date;
			//return UTCToLocalTime;
		}

		public class StaticVariables
		{
			public DateTime ZERO_DATE = DateTime.FromOADate(0);
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
