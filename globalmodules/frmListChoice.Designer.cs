using System;
using Wisej.Web;
using fecherFoundation;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for frmListChoice.
	/// </summary>
	partial class frmListChoice : BaseForm
	{
		public FCGrid Grid;
		public FCLabel lblMessage;
		public Wisej.Web.MenuItem mnuProcess;
		public Wisej.Web.MenuItem mnuSaveContinue;
		public Wisej.Web.MenuItem Seperator;
		public Wisej.Web.MenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Grid = new fecherFoundation.FCGrid();
            this.lblMessage = new fecherFoundation.FCLabel();
            this.mnuProcess = new Wisej.Web.MenuItem();
            this.mnuSaveContinue = new Wisej.Web.MenuItem();
            this.Seperator = new Wisej.Web.MenuItem();
            this.mnuExit = new Wisej.Web.MenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 360);
            this.BottomPanel.Size = new System.Drawing.Size(603, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.lblMessage);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(603, 360);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(603, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(189, 30);
            this.HeaderText.Text = "Load Data From";
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(30, 59);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Size = new System.Drawing.Size(547, 282);
            this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(30, 30);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(381, 16);
            this.lblMessage.TabIndex = 1;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(216, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(170, 48);
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmListChoice
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(603, 468);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.Name = "frmListChoice";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.Load += new System.EventHandler(this.frmListChoice_Load);
            this.Resize += new System.EventHandler(this.frmListChoice_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmListChoice_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}