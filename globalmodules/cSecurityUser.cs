﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	public class cSecurityUser
	{
		//=========================================================
		private int lngID;
		private string strUserID = "";
		private string strOPID = "";
		private string strUserName = "";
		private int intFrequency;
		// VBto upgrade warning: dtChanged As DateTime	OnWrite(DateTime, short)
		private DateTime dtChanged;
		private bool boolUseSecurity;
		// VBto upgrade warning: dtUpdate As DateTime	OnWrite(DateTime, short)
		private DateTime dtUpdate;
		private string strPassHash = "";
		private bool boolDefaultAdvancedSearch;

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public string User
		{
			get
			{
				string User = "";
				User = strUserID;
				return User;
			}
			set
			{
				strUserID = value;
			}
		}

		public string OpID
		{
			get
			{
				string OpID = "";
				OpID = strOPID;
				return OpID;
			}
			set
			{
				strOPID = value;
			}
		}

		public string UserName
		{
			get
			{
				string UserName = "";
				UserName = strUserName;
				return UserName;
			}
			set
			{
				strUserName = value;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Frequency
		{
			get
			{
				short Frequency = 0;
				Frequency = FCConvert.ToInt16(intFrequency);
				return Frequency;
			}
			set
			{
				intFrequency = value;
			}
		}

		public DateTime DateChanged
		{
			get
			{
				DateTime DateChanged = System.DateTime.Now;
				DateChanged = dtChanged;
				return DateChanged;
			}
			set
			{
				dtChanged = value;
			}
		}

		public bool UseSecurity
		{
			get
			{
				bool UseSecurity = false;
				UseSecurity = boolUseSecurity;
				return UseSecurity;
			}
			set
			{
				boolUseSecurity = value;
			}
		}

		public DateTime UpdateDate
		{
			get
			{
				DateTime UpdateDate = System.DateTime.Now;
				UpdateDate = dtUpdate;
				return UpdateDate;
			}
			set
			{
				dtUpdate = value;
			}
		}

		public string PasswordHash
		{
			get
			{
				string PasswordHash = "";
				PasswordHash = strPassHash;
				return PasswordHash;
			}
			set
			{
				strPassHash = value;
			}
		}

		public bool DefaultAdvancedSearch
		{
			get
			{
				bool DefaultAdvancedSearch = false;
				DefaultAdvancedSearch = boolDefaultAdvancedSearch;
				return DefaultAdvancedSearch;
			}
			set
			{
				boolDefaultAdvancedSearch = value;
			}
		}

		public cSecurityUser() : base()
		{
			lngID = 0;
			strUserID = "";
			strOPID = "";
			strUserName = "";
			intFrequency = 0;
			dtChanged = DateTime.FromOADate(0);
			boolUseSecurity = true;
			dtUpdate = DateTime.FromOADate(0);
			strPassHash = "";
			boolDefaultAdvancedSearch = false;
		}
	}
}
