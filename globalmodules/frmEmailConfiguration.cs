﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailConfiguration.
	/// </summary>
	public partial class frmEmailConfiguration : BaseForm
	{
		public frmEmailConfiguration()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEmailConfiguration InstancePtr
		{
			get
			{
				return (frmEmailConfiguration)Sys.GetInstance(typeof(frmEmailConfiguration));
			}
		}

		protected frmEmailConfiguration _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cEMailConfigController eConfigCont = new cEMailConfigController();
		private cEMailConfigController eConfigCont_AutoInitialized;

		private cEMailConfigController eConfigCont
		{
			get
			{
				if (eConfigCont_AutoInitialized == null)
				{
					eConfigCont_AutoInitialized = new cEMailConfigController();
				}
				return eConfigCont_AutoInitialized;
			}
			set
			{
				eConfigCont_AutoInitialized = value;
			}
		}

		public void Init(bool boolModal = false)
		{
			if (boolModal)
			{
				this.Show(FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void frmEmailConfiguration_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmEmailConfiguration_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmailConfiguration.Icon	= "frmEmailConfiguration.frx":0000";
			//frmEmailConfiguration.FillStyle	= 0;
			//frmEmailConfiguration.ScaleWidth	= 9300;
			//frmEmailConfiguration.ScaleHeight	= 7890;
			//frmEmailConfiguration.LinkTopic	= "Form2";
			//frmEmailConfiguration.LockControls	= true;
			//frmEmailConfiguration.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtPassword.IMEMode	= 3  'DISABLE;
			txtPassword.PasswordChar = '*';
            //txtUserPassword.IMEMode	= 3  'DISABLE;
            txtUserPassword.PasswordChar = '*';
            //vsElasticLight1.OleObjectBlob	= "frmEmailConfiguration.frx":058A";
            //End Unmaped Properties
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FillcmbAuthType();
			LoadSettings();
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strTemp;
				int x;
				cEmailConfig eConfig;
				cEMailService emailservice = new cEMailService();
				strTemp = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
				framUser.Text = "Settings for user " + strTemp + " if different than above";
				txtEmail.Text = "";
				txtPassword.Text = "";
				txtSMTPServer.Text = "";
				txtUser.Text = "";
				txtFrom.Text = "";
				cmbAuthType.SelectedIndex = 1;
				chkUseSSL.CheckState = Wisej.Web.CheckState.Unchecked;
				chkUseTLS.CheckState = Wisej.Web.CheckState.Unchecked;
				eConfig = eConfigCont.GetConfigurationByUserID(0);
				// default
				if (!(eConfig == null))
				{
					if (eConfig.ID > 0)
					{
						txtEmail.Text = eConfig.DefaultFromAddress;
						txtPassword.Text = eConfig.Password;
						txtSMTPServer.Text = eConfig.SMTPHost;
						txtUser.Text = eConfig.UserName;
						txtFrom.Text = eConfig.DefaultFromName;
						if (eConfig.UseSSL)
						{
							chkUseSSL.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (eConfig.UseTransportLayerSecurity)
						{
							chkUseTLS.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (eConfig.SMTPPort > 0)
						{
							txtPort.Text = FCConvert.ToString(eConfig.SMTPPort);
						}
						else
						{
							txtPort.Text = "";
						}
						for (x = 0; x <= cmbAuthType.Items.Count - 1; x++)
						{
							if (cmbAuthType.ItemData(x) == eConfig.AuthCode)
							{
								cmbAuthType.SelectedIndex = x;
								break;
							}
						}
						// x
					}
				}
				txtUserEmail.Text = "";
				txtUserPassword.Text = "";
				txtUserServer.Text = "";
				txtUserUser.Text = "";
				txtUserFrom.Text = "";
				cmbUserAuthType.SelectedIndex = 1;
				chkUserUseSSL.CheckState = Wisej.Web.CheckState.Unchecked;
				chkUserUseTLS.CheckState = Wisej.Web.CheckState.Unchecked;
				eConfig = eConfigCont.GetConfigurationByUserID(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				if (!(eConfig == null))
				{
					if (eConfig.ID > 0)
					{
						txtUserEmail.Text = eConfig.DefaultFromAddress;
						txtUserPassword.Text = eConfig.Password;
						txtUserServer.Text = eConfig.SMTPHost;
						txtUserUser.Text = eConfig.UserName;
						txtUserFrom.Text = eConfig.DefaultFromName;
						if (eConfig.UseSSL)
						{
							chkUserUseSSL.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (eConfig.UseTransportLayerSecurity)
						{
							chkUserUseTLS.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (eConfig.SMTPPort > 0)
						{
							txtUserPort.Text = FCConvert.ToString(eConfig.SMTPPort);
						}
						else
						{
							txtUserPort.Text = "";
						}
						for (x = 0; x <= cmbUserAuthType.Items.Count - 1; x++)
						{
							if (cmbUserAuthType.ItemData(x) == eConfig.AuthCode)
							{
								cmbUserAuthType.SelectedIndex = x;
								break;
							}
						}
						// x
					}
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadSettings", MsgBoxStyle.Critical, "Error");
			}
		}

		private bool SaveConfiguration()
		{
			bool SaveConfiguration = false;
			try
			{
				// On Error GoTo ErrorHandler
				cEmailConfig eConfig;
				SaveConfiguration = false;
				eConfig = eConfigCont.GetConfigurationByUserID(0);
				if (eConfig == null)
				{
					eConfig = new cEmailConfig();
				}
				eConfig.DefaultFromAddress = Strings.Trim(txtEmail.Text);
				eConfig.AuthCode = FCConvert.ToInt16(cmbAuthType.ItemData(cmbAuthType.SelectedIndex));
				eConfig.DefaultFromName = Strings.Trim(txtFrom.Text);
				eConfig.Password = Strings.Trim(txtPassword.Text);
				eConfig.SMTPHost = Strings.Trim(txtSMTPServer.Text);
				eConfig.UserName = Strings.Trim(txtUser.Text);
				eConfig.UserID = 0;
				eConfig.SMTPPort = FCConvert.ToInt32(Math.Round(Conversion.Val(txtPort.Text)));
				eConfig.UseSSL = chkUseSSL.CheckState == Wisej.Web.CheckState.Checked;
				eConfig.UseTransportLayerSecurity = chkUseTLS.CheckState == Wisej.Web.CheckState.Checked;
				eConfigCont.SaveEmailConfiguration(ref eConfig);
				eConfig = eConfigCont.GetConfigurationByUserID(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				if (eConfig == null)
				{
					eConfig = new cEmailConfig();
					eConfig.UserID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
				}
				eConfig.AuthCode = FCConvert.ToInt16(cmbUserAuthType.ItemData(cmbUserAuthType.SelectedIndex));
				eConfig.DefaultFromAddress = Strings.Trim(txtUserEmail.Text);
				eConfig.DefaultFromName = Strings.Trim(txtUserFrom.Text);
				eConfig.Password = Strings.Trim(txtUserPassword.Text);
				eConfig.SMTPHost = Strings.Trim(txtUserServer.Text);
				eConfig.SMTPPort = FCConvert.ToInt32(Math.Round(Conversion.Val(txtUserPort.Text)));
				eConfig.UserName = Strings.Trim(txtUserUser.Text);
				eConfig.UseSSL = chkUserUseSSL.CheckState == Wisej.Web.CheckState.Checked;
				eConfig.UseTransportLayerSecurity = chkUserUseTLS.CheckState == Wisej.Web.CheckState.Checked;
				eConfigCont.SaveEmailConfiguration(ref eConfig);
				SaveConfiguration = true;
				FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");
				return SaveConfiguration;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In SaveConfiguration", MsgBoxStyle.Critical, "Error");
			}
			return SaveConfiguration;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveConfiguration();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveConfiguration())
			{
				mnuExit_Click();
			}
		}

		private void FillcmbAuthType()
		{
			cmbAuthType.Clear();
			cmbAuthType.AddItem("Automatically");
			cmbAuthType.ItemData(cmbAuthType.NewIndex, -1);
			cmbAuthType.AddItem("Login");
			cmbAuthType.ItemData(cmbAuthType.NewIndex, 0);
			cmbAuthType.AddItem("NTLM");
			cmbAuthType.ItemData(cmbAuthType.NewIndex, 1);
			cmbAuthType.AddItem("Cram-MD5");
			cmbAuthType.ItemData(cmbAuthType.NewIndex, 2);
			cmbAuthType.AddItem("Plain");
			cmbAuthType.ItemData(cmbAuthType.NewIndex, 3);
			cmbAuthType.AddItem("None");
			cmbAuthType.ItemData(cmbAuthType.NewIndex, 5);
			cmbUserAuthType.Clear();
			cmbUserAuthType.AddItem("Automatically");
			cmbUserAuthType.ItemData(cmbUserAuthType.NewIndex, -1);
			cmbUserAuthType.AddItem("Login");
			cmbUserAuthType.ItemData(cmbUserAuthType.NewIndex, 0);
			cmbUserAuthType.AddItem("NTLM");
			cmbUserAuthType.ItemData(cmbUserAuthType.NewIndex, 1);
			cmbUserAuthType.AddItem("Cram-MD5");
			cmbUserAuthType.ItemData(cmbUserAuthType.NewIndex, 2);
			cmbUserAuthType.AddItem("Plain");
			cmbUserAuthType.ItemData(cmbUserAuthType.NewIndex, 3);
			cmbUserAuthType.AddItem("None");
			cmbUserAuthType.ItemData(cmbUserAuthType.NewIndex, 5);
		}

		private void txtPort_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int x;
			string strTemp;
			strTemp = txtPort.Text;
			for (x = 0; x <= 25; x++)
			{
				strTemp = strTemp.Replace(FCConvert.ToString(Convert.ToChar(Convert.ToByte("a"[0]) + x)), "");
				strTemp = strTemp.Replace(FCConvert.ToString(Convert.ToChar(Convert.ToByte("A"[0]) + x)), "");
			}
			// x
			txtPort.Text = strTemp;
		}

		private void txtUserPort_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int x;
			string strTemp;
			strTemp = txtUserPort.Text;
			for (x = 0; x <= 25; x++)
			{
				strTemp = strTemp.Replace(FCConvert.ToString(Convert.ToChar(Convert.ToByte("a"[0]) + x)), "");
				strTemp = strTemp.Replace(FCConvert.ToString(Convert.ToChar(Convert.ToByte("A"[0]) + x)), "");
			}
			// x
			txtUserPort.Text = strTemp;
		}
		// VBto upgrade warning: 'Return' As Variant --> As string
		private string AuthStringFromCode(int intCode)
		{
			string AuthStringFromCode = "";
			string strReturn;
			strReturn = "";
			switch (intCode)
			{
				case -1:
					{
						strReturn = "";
						break;
					}
				case 0:
					{
						strReturn = "LOGIN";
						break;
					}
				case 1:
					{
						strReturn = "NTLM";
						break;
					}
				case 2:
					{
						strReturn = "CRAM-MD5";
						break;
					}
				case 3:
					{
						strReturn = "PLAIN";
						break;
					}
				case 5:
					{
						strReturn = "NONE";
						break;
					}
			}
			//end switch
			AuthStringFromCode = strReturn;
			return AuthStringFromCode;
		}
		// VBto upgrade warning: 'Return' As Variant --> As short	OnWriteFCConvert.ToInt32(
		private short AuthCodeFromString(string strCode)
		{
			short AuthCodeFromString = 0;
			int intReturn;
			intReturn = -1;
			if (Strings.UCase(strCode) == "")
			{
				intReturn = -1;
			}
			else if (Strings.UCase(strCode) == "LOGIN")
			{
				intReturn = 0;
			}
			else if (Strings.UCase(strCode) == "NTLM")
			{
				intReturn = 1;
			}
			else if (Strings.UCase(strCode) == "CRAM-MD5")
			{
				intReturn = 2;
			}
			else if (Strings.UCase(strCode) == "PLAIN")
			{
				intReturn = 3;
			}
			else if (Strings.UCase(strCode) == "NONE")
			{
				intReturn = 5;
			}
			AuthCodeFromString = FCConvert.ToInt16(intReturn);
			return AuthCodeFromString;
		}

		public string Get_AuthMethodDescription(string strAuthMethod)
		{
			string AuthMethodDescription = "";
			string strReturn = "";
			if (Strings.UCase(strAuthMethod) == "LOGIN")
			{
				strReturn = "Login";
			}
			else if (Strings.UCase(strAuthMethod) == "NTLM")
			{
				strReturn = "NTLM";
			}
			else if (Strings.UCase(strAuthMethod) == "CRAM-MD5")
			{
				strReturn = "CRAM-MD5";
			}
			else if (Strings.UCase(strAuthMethod) == "PLAIN")
			{
				strReturn = "Plain";
			}
			else if (Strings.UCase(strAuthMethod) == "NONE")
			{
				strReturn = "None";
			}
			else
			{
				strReturn = "Automatic";
			}
			AuthMethodDescription = strReturn;
			return AuthMethodDescription;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}
	}
}
