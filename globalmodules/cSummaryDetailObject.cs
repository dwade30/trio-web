﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cSummaryDetailObject
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collExpenseSummaries = new cGenericCollection();
		//private cGenericCollection collExpenseDetails = new cGenericCollection();
		//private cGenericCollection collRevenueSummaries = new cGenericCollection();
		//private cGenericCollection collRevenueDetails = new cGenericCollection();
		//private cGenericCollection collLedgerSummaries = new cGenericCollection();
		//private cGenericCollection collLedgerDetails = new cGenericCollection();
		private cGenericCollection collExpenseSummaries_AutoInitialized;

		private cGenericCollection collExpenseSummaries
		{
			get
			{
				if (collExpenseSummaries_AutoInitialized == null)
				{
					collExpenseSummaries_AutoInitialized = new cGenericCollection();
				}
				return collExpenseSummaries_AutoInitialized;
			}
			set
			{
				collExpenseSummaries_AutoInitialized = value;
			}
		}

		private cGenericCollection collExpenseDetails_AutoInitialized;

		private cGenericCollection collExpenseDetails
		{
			get
			{
				if (collExpenseDetails_AutoInitialized == null)
				{
					collExpenseDetails_AutoInitialized = new cGenericCollection();
				}
				return collExpenseDetails_AutoInitialized;
			}
			set
			{
				collExpenseDetails_AutoInitialized = value;
			}
		}

		private cGenericCollection collRevenueSummaries_AutoInitialized;

		private cGenericCollection collRevenueSummaries
		{
			get
			{
				if (collRevenueSummaries_AutoInitialized == null)
				{
					collRevenueSummaries_AutoInitialized = new cGenericCollection();
				}
				return collRevenueSummaries_AutoInitialized;
			}
			set
			{
				collRevenueSummaries_AutoInitialized = value;
			}
		}

		private cGenericCollection collRevenueDetails_AutoInitialized;

		private cGenericCollection collRevenueDetails
		{
			get
			{
				if (collRevenueDetails_AutoInitialized == null)
				{
					collRevenueDetails_AutoInitialized = new cGenericCollection();
				}
				return collRevenueDetails_AutoInitialized;
			}
			set
			{
				collRevenueDetails_AutoInitialized = value;
			}
		}

		private cGenericCollection collLedgerSummaries_AutoInitialized;

		private cGenericCollection collLedgerSummaries
		{
			get
			{
				if (collLedgerSummaries_AutoInitialized == null)
				{
					collLedgerSummaries_AutoInitialized = new cGenericCollection();
				}
				return collLedgerSummaries_AutoInitialized;
			}
			set
			{
				collLedgerSummaries_AutoInitialized = value;
			}
		}

		private cGenericCollection collLedgerDetails_AutoInitialized;

		private cGenericCollection collLedgerDetails
		{
			get
			{
				if (collLedgerDetails_AutoInitialized == null)
				{
					collLedgerDetails_AutoInitialized = new cGenericCollection();
				}
				return collLedgerDetails_AutoInitialized;
			}
			set
			{
				collLedgerDetails_AutoInitialized = value;
			}
		}

		public cGenericCollection ExpenseSummaries
		{
			get
			{
				cGenericCollection ExpenseSummaries = null;
				ExpenseSummaries = collExpenseSummaries;
				return ExpenseSummaries;
			}
		}

		public cGenericCollection ExpenseDetails
		{
			get
			{
				cGenericCollection ExpenseDetails = null;
				ExpenseDetails = collExpenseDetails;
				return ExpenseDetails;
			}
		}

		public cGenericCollection RevenueSummaries
		{
			get
			{
				cGenericCollection RevenueSummaries = null;
				RevenueSummaries = collRevenueSummaries;
				return RevenueSummaries;
			}
		}

		public cGenericCollection RevenueDetails
		{
			get
			{
				cGenericCollection RevenueDetails = null;
				RevenueDetails = collRevenueDetails;
				return RevenueDetails;
			}
		}

		public cGenericCollection LedgerSummaries
		{
			get
			{
				cGenericCollection LedgerSummaries = null;
				LedgerSummaries = collLedgerSummaries;
				return LedgerSummaries;
			}
		}

		public cGenericCollection LedgerDetails
		{
			get
			{
				cGenericCollection LedgerDetails = null;
				LedgerDetails = collLedgerDetails;
				return LedgerDetails;
			}
		}
	}
}
