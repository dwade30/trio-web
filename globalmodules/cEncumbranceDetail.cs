﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cEncumbranceDetail
	{
		//=========================================================
		private bool boolUpdated;
		private bool boolDeleted;
		private int lngRecordID;
		private int lngEncumbranceID;
		private string strDetailDescription = string.Empty;
		private string strAccount = string.Empty;
		private string strProject = string.Empty;
		private double dblDetailAmount;
		private double dblAdjustments;
		private double dblLiquidated;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int EncumbranceID
		{
			set
			{
				lngEncumbranceID = value;
				IsUpdated = true;
			}
			get
			{
				int EncumbranceID = 0;
				EncumbranceID = lngEncumbranceID;
				return EncumbranceID;
			}
		}

		public string Description
		{
			set
			{
				strDetailDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strDetailDescription;
				return Description;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
				IsUpdated = true;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string Project
		{
			set
			{
				strProject = value;
				IsUpdated = true;
			}
			get
			{
				string Project = "";
				Project = strProject;
				return Project;
			}
		}

		public double Amount
		{
			set
			{
				dblDetailAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblDetailAmount;
				return Amount;
			}
		}

		public double Adjustments
		{
			set
			{
				dblAdjustments = value;
				IsUpdated = true;
			}
			get
			{
				double Adjustments = 0;
				Adjustments = dblAdjustments;
				return Adjustments;
			}
		}

		public double LiquidatedAmount
		{
			set
			{
				dblLiquidated = value;
				IsUpdated = true;
			}
			get
			{
				double LiquidatedAmount = 0;
				LiquidatedAmount = dblLiquidated;
				return LiquidatedAmount;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public bool IsDebit()
		{
			bool IsDebit = false;
			if (Amount > 0)
			{
				IsDebit = true;
			}
			return IsDebit;
		}

		public bool IsCredit()
		{
			bool IsCredit = false;
			if (Amount < 0)
			{
				IsCredit = true;
			}
			return IsCredit;
		}
	}
}
