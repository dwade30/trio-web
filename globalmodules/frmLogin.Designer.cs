﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for frmLogin.
	/// </summary>
	partial class frmLogin : BaseForm
	{
		public fecherFoundation.FCComboBox cmbUsers;
		public fecherFoundation.FCComboBox cboDataGroup;
		public fecherFoundation.FCButton btnCancel;
		public fecherFoundation.FCButton btnLogin;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCPictureBox Image2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblGroup;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
			this.cmbUsers = new fecherFoundation.FCComboBox();
			this.cboDataGroup = new fecherFoundation.FCComboBox();
			this.btnCancel = new fecherFoundation.FCButton();
			this.btnLogin = new fecherFoundation.FCButton();
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.Image2 = new fecherFoundation.FCPictureBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblGroup = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnLogin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnLogin);
			this.BottomPanel.Controls.Add(this.btnCancel);
			this.BottomPanel.Location = new System.Drawing.Point(0, 392);
			this.BottomPanel.Size = new System.Drawing.Size(398, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbUsers);
			this.ClientArea.Controls.Add(this.cboDataGroup);
			this.ClientArea.Controls.Add(this.txtPassword);
			this.ClientArea.Controls.Add(this.Image2);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.lblGroup);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(398, 332);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(398, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(72, 30);
			this.HeaderText.Text = "Login";
			// 
			// cmbUsers
			// 
			this.cmbUsers.AutoSize = false;
			this.cmbUsers.BackColor = System.Drawing.SystemColors.Window;
			this.cmbUsers.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbUsers.FormattingEnabled = true;
			this.cmbUsers.Location = new System.Drawing.Point(138, 210);
			this.cmbUsers.Name = "cmbUsers";
			this.cmbUsers.Size = new System.Drawing.Size(171, 40);
			this.cmbUsers.TabIndex = 8;
			this.cmbUsers.Text = "Combo1";
			// 
			// cboDataGroup
			// 
			this.cboDataGroup.AutoSize = false;
			this.cboDataGroup.BackColor = System.Drawing.SystemColors.Window;
			this.cboDataGroup.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboDataGroup.FormattingEnabled = true;
			this.cboDataGroup.Location = new System.Drawing.Point(138, 150);
			this.cboDataGroup.Name = "cboDataGroup";
			this.cboDataGroup.Size = new System.Drawing.Size(171, 40);
			this.cboDataGroup.TabIndex = 7;
			this.cboDataGroup.Text = "Combo1";
			this.cboDataGroup.SelectedIndexChanged += new System.EventHandler(this.cboDataGroup_SelectedIndexChanged);
			// 
			// btnCancel
			// 
			this.btnCancel.AppearanceKey = "acceptButton";
			this.btnCancel.Location = new System.Drawing.Point(191, 33);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 40);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnLogin
			// 
			this.btnLogin.AppearanceKey = "acceptButton";
			this.btnLogin.Location = new System.Drawing.Point(114, 33);
			this.btnLogin.Name = "btnLogin";
			this.btnLogin.Size = new System.Drawing.Size(71, 40);
			this.btnLogin.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnLogin.TabIndex = 3;
			this.btnLogin.Text = "Log In";
			this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
			// 
			// txtPassword
			// 
			this.txtPassword.AutoSize = false;
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword.Location = new System.Drawing.Point(138, 270);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Size = new System.Drawing.Size(171, 40);
			this.txtPassword.TabIndex = 0;
			// 
			// Image2
			// 
			this.Image2.AllowDrop = true;
			this.Image2.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image2.DrawStyle = ((short)(0));
			this.Image2.DrawWidth = ((short)(1));
			this.Image2.FillColor = 16777215;
			this.Image2.FillStyle = ((short)(1));
			this.Image2.FontTransparent = true;
			this.Image2.Image = ((System.Drawing.Image)(resources.GetObject("Image2.Image")));
			this.Image2.Location = new System.Drawing.Point(0, 0);
			this.Image2.Name = "Image2";
			this.Image2.Picture = ((System.Drawing.Image)(resources.GetObject("Image2.Picture")));
			this.Image2.Size = new System.Drawing.Size(398, 88);
			this.Image2.TabIndex = 9;
			// 
			// Label3
			// 
			this.Label3.BackColor = System.Drawing.Color.Transparent;
			this.Label3.Location = new System.Drawing.Point(30, 164);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(90, 22);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "DATA GROUP";
			// 
			// lblGroup
			// 
			this.lblGroup.BackColor = System.Drawing.Color.Transparent;
			this.lblGroup.Location = new System.Drawing.Point(30, 103);
			this.lblGroup.Name = "lblGroup";
			this.lblGroup.Size = new System.Drawing.Size(333, 22);
			this.lblGroup.TabIndex = 4;
			this.lblGroup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label2
			// 
			this.Label2.BackColor = System.Drawing.Color.Transparent;
			this.Label2.Location = new System.Drawing.Point(30, 284);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(75, 22);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "PASSWORD";
			// 
			// Label1
			// 
			this.Label1.BackColor = System.Drawing.Color.Transparent;
			this.Label1.Location = new System.Drawing.Point(30, 224);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(75, 22);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "USER NAME";
			// 
			// frmLogin
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(398, 500);
			this.ControlBox = false;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmLogin";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "";
			this.Load += new System.EventHandler(this.frmLogin_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLogin_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnLogin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
