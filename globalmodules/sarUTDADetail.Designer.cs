﻿namespace Global
{
	/// <summary>
	/// Summary description for sarUTDADetail.
	/// </summary>
	partial class sarUTDADetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarUTDADetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPer = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTeller = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTeller = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceipt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldYear,
				this.fldPrincipal,
				this.fldInterest,
				this.fldCosts,
				this.fldTotal,
				this.fldPer,
				this.fldCode,
				this.fldRef,
				this.fldDate,
				this.fldTeller,
				this.fldReceipt,
				this.fldCash,
				this.fldTax,
				this.fldMapLot,
				this.fldName
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccount,
				this.lblYear,
				this.lblPrincipal,
				this.lblInterest,
				this.lblCosts,
				this.lblTotal,
				this.lblPer,
				this.lblCode,
				this.lblRef,
				this.lblDate,
				this.lblTeller,
				this.lblRNum,
				this.lnHeader,
				this.lblCash,
				this.lblTax
			});
			this.GroupHeader1.Height = 0.3229167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotals,
				this.fldTotalPrin,
				this.fldTotalInt,
				this.fldTotalCosts,
				this.fldTotalTotal,
				this.lnTotals,
				this.fldTotalTax
			});
			this.GroupFooter1.Height = 0.5F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.125F;
			this.lblAccount.Width = 0.5F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 0.5F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblYear.Text = "Bill";
			this.lblYear.Top = 0.125F;
			this.lblYear.Width = 0.4375F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 0.9375F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0.125F;
			this.lblPrincipal.Width = 0.8125F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 2.375F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0.125F;
			this.lblInterest.Width = 0.625F;
			// 
			// lblCosts
			// 
			this.lblCosts.Height = 0.1875F;
			this.lblCosts.HyperLink = null;
			this.lblCosts.Left = 3F;
			this.lblCosts.Name = "lblCosts";
			this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCosts.Text = "Costs";
			this.lblCosts.Top = 0.125F;
			this.lblCosts.Width = 0.625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 3.625F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.125F;
			this.lblTotal.Width = 0.8125F;
			// 
			// lblPer
			// 
			this.lblPer.Height = 0.1875F;
			this.lblPer.HyperLink = null;
			this.lblPer.Left = 4.4375F;
			this.lblPer.Name = "lblPer";
			this.lblPer.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPer.Text = "P";
			this.lblPer.Top = 0.125F;
			this.lblPer.Width = 0.1875F;
			// 
			// lblCode
			// 
			this.lblCode.Height = 0.1875F;
			this.lblCode.HyperLink = null;
			this.lblCode.Left = 4.625F;
			this.lblCode.Name = "lblCode";
			this.lblCode.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCode.Text = "C";
			this.lblCode.Top = 0.125F;
			this.lblCode.Width = 0.1875F;
			// 
			// lblRef
			// 
			this.lblRef.Height = 0.1875F;
			this.lblRef.HyperLink = null;
			this.lblRef.Left = 5.1875F;
			this.lblRef.Name = "lblRef";
			this.lblRef.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblRef.Text = "Ref";
			this.lblRef.Top = 0.125F;
			this.lblRef.Width = 0.625F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.8125F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.125F;
			this.lblDate.Width = 0.6875F;
			// 
			// lblTeller
			// 
			this.lblTeller.Height = 0.1875F;
			this.lblTeller.HyperLink = null;
			this.lblTeller.Left = 6.5F;
			this.lblTeller.Name = "lblTeller";
			this.lblTeller.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTeller.Text = "TLR";
			this.lblTeller.Top = 0.125F;
			this.lblTeller.Width = 0.375F;
			// 
			// lblRNum
			// 
			this.lblRNum.Height = 0.1875F;
			this.lblRNum.HyperLink = null;
			this.lblRNum.Left = 6.875F;
			this.lblRNum.Name = "lblRNum";
			this.lblRNum.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblRNum.Text = "Receipt";
			this.lblRNum.Top = 0.125F;
			this.lblRNum.Width = 0.625F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.3125F;
			this.lnHeader.Width = 7.5F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 7.5F;
			this.lnHeader.Y1 = 0.3125F;
			this.lnHeader.Y2 = 0.3125F;
			// 
			// lblCash
			// 
			this.lblCash.Height = 0.1875F;
			this.lblCash.HyperLink = null;
			this.lblCash.Left = 4.8125F;
			this.lblCash.Name = "lblCash";
			this.lblCash.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblCash.Text = "Csh";
			this.lblCash.Top = 0.125F;
			this.lblCash.Width = 0.375F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 1.75F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.125F;
			this.lblTax.Width = 0.625F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAccount.Text = "0000000";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.5625F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 0.75F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldYear.Text = "0000";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.5F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 1.375F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 0.8125F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 3F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 0.8125F;
			// 
			// fldCosts
			// 
			this.fldCosts.Height = 0.1875F;
			this.fldCosts.Left = 3.8125F;
			this.fldCosts.Name = "fldCosts";
			this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCosts.Text = null;
			this.fldCosts.Top = 0F;
			this.fldCosts.Width = 0.8125F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 4.625F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 0.875F;
			// 
			// fldPer
			// 
			this.fldPer.Height = 0.1875F;
			this.fldPer.Left = 5.5F;
			this.fldPer.Name = "fldPer";
			this.fldPer.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPer.Text = null;
			this.fldPer.Top = 0F;
			this.fldPer.Width = 0.1875F;
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 5.6875F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCode.Text = null;
			this.fldCode.Top = 0F;
			this.fldCode.Width = 0.1875F;
			// 
			// fldRef
			// 
			this.fldRef.Height = 0.1875F;
			this.fldRef.Left = 6.4375F;
			this.fldRef.MultiLine = false;
			this.fldRef.Name = "fldRef";
			this.fldRef.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.fldRef.Text = null;
			this.fldRef.Top = 0F;
			this.fldRef.Width = 0.875F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 7.375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.8125F;
			// 
			// fldTeller
			// 
			this.fldTeller.Height = 0.1875F;
			this.fldTeller.Left = 8.25F;
			this.fldTeller.MultiLine = false;
			this.fldTeller.Name = "fldTeller";
			this.fldTeller.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.fldTeller.Text = null;
			this.fldTeller.Top = 0F;
			this.fldTeller.Width = 0.375F;
			// 
			// fldReceipt
			// 
			this.fldReceipt.Height = 0.1875F;
			this.fldReceipt.Left = 8.75F;
			this.fldReceipt.Name = "fldReceipt";
			this.fldReceipt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldReceipt.Text = null;
			this.fldReceipt.Top = 0F;
			this.fldReceipt.Width = 0.5625F;
			// 
			// fldCash
			// 
			this.fldCash.Height = 0.1875F;
			this.fldCash.Left = 6F;
			this.fldCash.Name = "fldCash";
			this.fldCash.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldCash.Text = null;
			this.fldCash.Top = 0F;
			this.fldCash.Width = 0.375F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 2.1875F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 0.8125F;
			// 
			// fldMapLot
			// 
			this.fldMapLot.Height = 0.1875F;
			this.fldMapLot.Left = 3.5F;
			this.fldMapLot.Name = "fldMapLot";
			this.fldMapLot.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldMapLot.Text = null;
			this.fldMapLot.Top = 0F;
			this.fldMapLot.Visible = false;
			this.fldMapLot.Width = 3F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.5F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Visible = false;
			this.fldName.Width = 3F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.75F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0.0625F;
			this.lblTotals.Width = 0.625F;
			// 
			// fldTotalPrin
			// 
			this.fldTotalPrin.Height = 0.1875F;
			this.fldTotalPrin.Left = 1.375F;
			this.fldTotalPrin.Name = "fldTotalPrin";
			this.fldTotalPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrin.Text = "0.00";
			this.fldTotalPrin.Top = 0.0625F;
			this.fldTotalPrin.Width = 0.8125F;
			// 
			// fldTotalInt
			// 
			this.fldTotalInt.Height = 0.1875F;
			this.fldTotalInt.Left = 3F;
			this.fldTotalInt.Name = "fldTotalInt";
			this.fldTotalInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalInt.Text = "0.00";
			this.fldTotalInt.Top = 0.0625F;
			this.fldTotalInt.Width = 0.8125F;
			// 
			// fldTotalCosts
			// 
			this.fldTotalCosts.Height = 0.1875F;
			this.fldTotalCosts.Left = 3.8125F;
			this.fldTotalCosts.Name = "fldTotalCosts";
			this.fldTotalCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCosts.Text = "0.00";
			this.fldTotalCosts.Top = 0.0625F;
			this.fldTotalCosts.Width = 0.8125F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 4.625F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0.0625F;
			this.fldTotalTotal.Width = 0.8125F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 0.6875F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Width = 4.75F;
			this.lnTotals.X1 = 0.6875F;
			this.lnTotals.X2 = 5.4375F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 2.1875F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTax.Text = "0.00";
			this.fldTotalTax.Top = 0.0625F;
			this.fldTotalTax.Width = 0.8125F;
			// 
			// sarUTDADetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTeller;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPer;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRef;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTeller;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRNum;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCash;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
	}
}
