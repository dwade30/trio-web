﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmCertificateOfAssessment.
	/// </summary>
	partial class frmCertificateOfAssessment : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCTextBox txtPage2;
		public fecherFoundation.FCTextBox txtPage1;
		public fecherFoundation.FCTextBox txtPercent;
		public fecherFoundation.FCTextBox txtTreasurer;
		public fecherFoundation.FCTextBox txtTaxCollector;
		public fecherFoundation.FCTextBox txtCounty;
		public Global.T2KDateBox t2kDueDate;
		public Global.T2KDateBox t2kInterestDate;
		public Global.T2KDateBox t2kCollectionDate;
		public Global.T2KDateBox T2KCommitmentDate;
		public Global.T2KDateBox T2KDueDate2;
		public Global.T2KDateBox T2KInterestDate2;
		public Global.T2KDateBox T2KDueDate3;
		public Global.T2KDateBox T2KInterestDate3;
		public Global.T2KDateBox T2KDueDate4;
		public Global.T2KDateBox T2KInterestDate4;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;
		private System.ComponentModel.IContainer components;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCertificateOfAssessment));
			this.txtPage2 = new fecherFoundation.FCTextBox();
			this.txtPage1 = new fecherFoundation.FCTextBox();
			this.txtPercent = new fecherFoundation.FCTextBox();
			this.txtTreasurer = new fecherFoundation.FCTextBox();
			this.txtTaxCollector = new fecherFoundation.FCTextBox();
			this.txtCounty = new fecherFoundation.FCTextBox();
			this.t2kDueDate = new Global.T2KDateBox();
			this.t2kInterestDate = new Global.T2KDateBox();
			this.t2kCollectionDate = new Global.T2KDateBox();
			this.T2KCommitmentDate = new Global.T2KDateBox();
			this.T2KDueDate2 = new Global.T2KDateBox();
			this.T2KInterestDate2 = new Global.T2KDateBox();
			this.T2KDueDate3 = new Global.T2KDateBox();
			this.T2KInterestDate3 = new Global.T2KDateBox();
			this.T2KDueDate4 = new Global.T2KDateBox();
			this.T2KInterestDate4 = new Global.T2KDateBox();
			this.Label1_10 = new fecherFoundation.FCLabel();
			this.Label1_9 = new fecherFoundation.FCLabel();
			this.Label1_8 = new fecherFoundation.FCLabel();
			this.Label1_7 = new fecherFoundation.FCLabel();
			this.Label1_3 = new fecherFoundation.FCLabel();
			this.Label1_6 = new fecherFoundation.FCLabel();
			this.Label1_5 = new fecherFoundation.FCLabel();
			this.Label1_4 = new fecherFoundation.FCLabel();
			this.Label1_2 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSaveAndContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kCollectionDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KCommitmentDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDueDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KInterestDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDueDate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KInterestDate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDueDate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KInterestDate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAndContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 619);
			this.BottomPanel.Size = new System.Drawing.Size(923, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtPage2);
			this.ClientArea.Controls.Add(this.txtPage1);
			this.ClientArea.Controls.Add(this.txtPercent);
			this.ClientArea.Controls.Add(this.txtTreasurer);
			this.ClientArea.Controls.Add(this.txtTaxCollector);
			this.ClientArea.Controls.Add(this.txtCounty);
			this.ClientArea.Controls.Add(this.t2kDueDate);
			this.ClientArea.Controls.Add(this.t2kInterestDate);
			this.ClientArea.Controls.Add(this.t2kCollectionDate);
			this.ClientArea.Controls.Add(this.T2KCommitmentDate);
			this.ClientArea.Controls.Add(this.T2KDueDate2);
			this.ClientArea.Controls.Add(this.T2KInterestDate2);
			this.ClientArea.Controls.Add(this.T2KDueDate3);
			this.ClientArea.Controls.Add(this.T2KInterestDate3);
			this.ClientArea.Controls.Add(this.T2KDueDate4);
			this.ClientArea.Controls.Add(this.T2KInterestDate4);
			this.ClientArea.Controls.Add(this.Label1_10);
			this.ClientArea.Controls.Add(this.Label1_9);
			this.ClientArea.Controls.Add(this.Label1_8);
			this.ClientArea.Controls.Add(this.Label1_7);
			this.ClientArea.Controls.Add(this.Label1_3);
			this.ClientArea.Controls.Add(this.Label1_6);
			this.ClientArea.Controls.Add(this.Label1_5);
			this.ClientArea.Controls.Add(this.Label1_4);
			this.ClientArea.Controls.Add(this.Label1_2);
			this.ClientArea.Controls.Add(this.Label1_1);
			this.ClientArea.Controls.Add(this.Label1_0);
			this.ClientArea.Size = new System.Drawing.Size(923, 559);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(923, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(311, 30);
			this.HeaderText.Text = "Certification of Assessment";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// txtPage2
			// 
			this.txtPage2.AutoSize = false;
			this.txtPage2.BackColor = System.Drawing.SystemColors.Window;
			this.txtPage2.LinkItem = null;
			this.txtPage2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPage2.LinkTopic = null;
			this.txtPage2.Location = new System.Drawing.Point(425, 510);
			this.txtPage2.Name = "txtPage2";
			this.txtPage2.Size = new System.Drawing.Size(60, 40);
			this.txtPage2.TabIndex = 26;
			this.txtPage2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtPage2, "The page numbers of the commitment book. Leave blank to fill in later");
			// 
			// txtPage1
			// 
			this.txtPage1.AutoSize = false;
			this.txtPage1.BackColor = System.Drawing.SystemColors.Window;
			this.txtPage1.LinkItem = null;
			this.txtPage1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPage1.LinkTopic = null;
			this.txtPage1.Location = new System.Drawing.Point(214, 510);
			this.txtPage1.Name = "txtPage1";
			this.txtPage1.Size = new System.Drawing.Size(60, 40);
			this.txtPage1.TabIndex = 24;
			this.txtPage1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtPage1, "The page numbers of the commitment book. Leave blank to fill in later");
			// 
			// txtPercent
			// 
			this.txtPercent.AutoSize = false;
			this.txtPercent.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent.LinkItem = null;
			this.txtPercent.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent.LinkTopic = null;
			this.txtPercent.Location = new System.Drawing.Point(214, 450);
			this.txtPercent.Name = "txtPercent";
			this.txtPercent.Size = new System.Drawing.Size(60, 40);
			this.txtPercent.TabIndex = 21;
			this.txtPercent.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtPercent, null);
			// 
			// txtTreasurer
			// 
			this.txtTreasurer.AutoSize = false;
			this.txtTreasurer.BackColor = System.Drawing.SystemColors.Window;
			this.txtTreasurer.LinkItem = null;
			this.txtTreasurer.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTreasurer.LinkTopic = null;
			this.txtTreasurer.Location = new System.Drawing.Point(214, 150);
			this.txtTreasurer.Name = "txtTreasurer";
			this.txtTreasurer.Size = new System.Drawing.Size(199, 40);
			this.txtTreasurer.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtTreasurer, null);
			// 
			// txtTaxCollector
			// 
			this.txtTaxCollector.AutoSize = false;
			this.txtTaxCollector.BackColor = System.Drawing.SystemColors.Window;
			this.txtTaxCollector.LinkItem = null;
			this.txtTaxCollector.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTaxCollector.LinkTopic = null;
			this.txtTaxCollector.Location = new System.Drawing.Point(214, 90);
			this.txtTaxCollector.Name = "txtTaxCollector";
			this.txtTaxCollector.Size = new System.Drawing.Size(199, 40);
			this.txtTaxCollector.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtTaxCollector, null);
			// 
			// txtCounty
			// 
			this.txtCounty.AutoSize = false;
			this.txtCounty.BackColor = System.Drawing.SystemColors.Window;
			this.txtCounty.LinkItem = null;
			this.txtCounty.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCounty.LinkTopic = null;
			this.txtCounty.Location = new System.Drawing.Point(214, 30);
			this.txtCounty.Name = "txtCounty";
			this.txtCounty.Size = new System.Drawing.Size(199, 40);
			this.txtCounty.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtCounty, null);
			// 
			// t2kDueDate
			// 
			this.t2kDueDate.Location = new System.Drawing.Point(214, 330);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.t2kDueDate.Mask = "##/##/####";
			this.t2kDueDate.Mask = "00/00/0000";
			this.t2kDueDate.Name = "t2kDueDate";
			this.t2kDueDate.Size = new System.Drawing.Size(115, 40);
			this.t2kDueDate.TabIndex = 11;
			this.t2kDueDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.t2kDueDate, null);
			// 
			// t2kInterestDate
			// 
			this.t2kInterestDate.Location = new System.Drawing.Point(214, 390);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.t2kInterestDate.Mask = "##/##/####";
			this.t2kInterestDate.Mask = "00/00/0000";
			this.t2kInterestDate.Name = "t2kInterestDate";
			this.t2kInterestDate.Size = new System.Drawing.Size(115, 40);
			this.t2kInterestDate.TabIndex = 16;
			this.t2kInterestDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.t2kInterestDate, null);
			// 
			// t2kCollectionDate
			// 
			this.t2kCollectionDate.Location = new System.Drawing.Point(214, 270);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.t2kCollectionDate.Mask = "##/##/####";
			this.t2kCollectionDate.Mask = "00/00/0000";
			this.t2kCollectionDate.Name = "t2kCollectionDate";
			this.t2kCollectionDate.Size = new System.Drawing.Size(115, 40);
			this.t2kCollectionDate.TabIndex = 9;
			this.t2kCollectionDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.t2kCollectionDate, "Date tax collector must have all taxes completed and accounted");
			// 
			// T2KCommitmentDate
			// 
			this.T2KCommitmentDate.Location = new System.Drawing.Point(214, 210);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.T2KCommitmentDate.Mask = "##/##/####";
			this.T2KCommitmentDate.Mask = "00/00/0000";
			this.T2KCommitmentDate.Name = "T2KCommitmentDate";
			this.T2KCommitmentDate.Size = new System.Drawing.Size(115, 40);
			this.T2KCommitmentDate.TabIndex = 7;
			this.T2KCommitmentDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.T2KCommitmentDate, "Date of certificate. (Date signed)");
			// 
			// T2KDueDate2
			// 
			this.T2KDueDate2.Location = new System.Drawing.Point(393, 330);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.T2KDueDate2.Mask = "##/##/####";
			this.T2KDueDate2.Mask = "00/00/0000";
			this.T2KDueDate2.Name = "T2KDueDate2";
			this.T2KDueDate2.Size = new System.Drawing.Size(115, 40);
			this.T2KDueDate2.TabIndex = 12;
			this.T2KDueDate2.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.T2KDueDate2, null);
			// 
			// T2KInterestDate2
			// 
			this.T2KInterestDate2.Location = new System.Drawing.Point(393, 390);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.T2KInterestDate2.Mask = "##/##/####";
			this.T2KInterestDate2.Mask = "00/00/0000";
			this.T2KInterestDate2.Name = "T2KInterestDate2";
			this.T2KInterestDate2.Size = new System.Drawing.Size(115, 40);
			this.T2KInterestDate2.TabIndex = 17;
			this.T2KInterestDate2.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.T2KInterestDate2, null);
			// 
			// T2KDueDate3
			// 
			this.T2KDueDate3.Location = new System.Drawing.Point(574, 330);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.T2KDueDate3.Mask = "##/##/####";
			this.T2KDueDate3.Mask = "00/00/0000";
			this.T2KDueDate3.Name = "T2KDueDate3";
			this.T2KDueDate3.Size = new System.Drawing.Size(115, 40);
			this.T2KDueDate3.TabIndex = 13;
			this.T2KDueDate3.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.T2KDueDate3, null);
			// 
			// T2KInterestDate3
			// 
			this.T2KInterestDate3.Location = new System.Drawing.Point(574, 390);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.T2KInterestDate3.Mask = "##/##/####";
			this.T2KInterestDate3.Mask = "00/00/0000";
			this.T2KInterestDate3.Name = "T2KInterestDate3";
			this.T2KInterestDate3.Size = new System.Drawing.Size(115, 40);
			this.T2KInterestDate3.TabIndex = 18;
			this.T2KInterestDate3.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.T2KInterestDate3, null);
			// 
			// T2KDueDate4
			// 
			this.T2KDueDate4.Location = new System.Drawing.Point(756, 330);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.T2KDueDate4.Mask = "##/##/####";
			this.T2KDueDate4.Mask = "00/00/0000";
			this.T2KDueDate4.Name = "T2KDueDate4";
			this.T2KDueDate4.Size = new System.Drawing.Size(115, 40);
			this.T2KDueDate4.TabIndex = 14;
			this.T2KDueDate4.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.T2KDueDate4, null);
			// 
			// T2KInterestDate4
			// 
			this.T2KInterestDate4.Location = new System.Drawing.Point(756, 390);
			//FC:FINAL:CHN - issue #1394: Allow only numbers at date field.
			// this.T2KInterestDate4.Mask = "##/##/####";
			this.T2KInterestDate4.Mask = "00/00/0000";
			this.T2KInterestDate4.Name = "T2KInterestDate4";
			this.T2KInterestDate4.Size = new System.Drawing.Size(115, 40);
			this.T2KInterestDate4.TabIndex = 19;
			this.T2KInterestDate4.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.T2KInterestDate4, null);
			// 
			// Label1_10
			// 
			this.Label1_10.Location = new System.Drawing.Point(30, 224);
			this.Label1_10.Name = "Label1_10";
			this.Label1_10.Size = new System.Drawing.Size(114, 17);
			this.Label1_10.TabIndex = 6;
			this.Label1_10.Text = "COMMITMENT DATE";
			this.ToolTip1.SetToolTip(this.Label1_10, "Date of certificate. (Date signed)");
			// 
			// Label1_9
			// 
			this.Label1_9.Location = new System.Drawing.Point(30, 284);
			this.Label1_9.Name = "Label1_9";
			this.Label1_9.Size = new System.Drawing.Size(141, 17);
			this.Label1_9.TabIndex = 8;
			this.Label1_9.Text = "SETTLEMENT DATE";
			this.ToolTip1.SetToolTip(this.Label1_9, "Date tax collector must have all taxes completed and accounted");
			// 
			// Label1_8
			// 
			this.Label1_8.Location = new System.Drawing.Point(336, 464);
			this.Label1_8.Name = "Label1_8";
			this.Label1_8.Size = new System.Drawing.Size(24, 17);
			this.Label1_8.TabIndex = 22;
			this.Label1_8.Text = "%";
			this.ToolTip1.SetToolTip(this.Label1_8, null);
			// 
			// Label1_7
			// 
			this.Label1_7.Location = new System.Drawing.Point(337, 524);
			this.Label1_7.Name = "Label1_7";
			this.Label1_7.Size = new System.Drawing.Size(24, 17);
			this.Label1_7.TabIndex = 25;
			this.Label1_7.Text = "TO";
			this.ToolTip1.SetToolTip(this.Label1_7, null);
			// 
			// Label1_3
			// 
			this.Label1_3.Location = new System.Drawing.Point(30, 524);
			this.Label1_3.Name = "Label1_3";
			this.Label1_3.Size = new System.Drawing.Size(114, 17);
			this.Label1_3.TabIndex = 23;
			this.Label1_3.Text = "PAGES";
			this.ToolTip1.SetToolTip(this.Label1_3, "The page numbers of the commitment book. Leave blank to fill in later");
			// 
			// Label1_6
			// 
			this.Label1_6.Location = new System.Drawing.Point(30, 464);
			this.Label1_6.Name = "Label1_6";
			this.Label1_6.Size = new System.Drawing.Size(114, 17);
			this.Label1_6.TabIndex = 20;
			this.Label1_6.Text = "INTEREST RATE";
			this.ToolTip1.SetToolTip(this.Label1_6, null);
			// 
			// Label1_5
			// 
			this.Label1_5.Location = new System.Drawing.Point(30, 404);
			this.Label1_5.Name = "Label1_5";
			this.Label1_5.Size = new System.Drawing.Size(114, 17);
			this.Label1_5.TabIndex = 15;
			this.Label1_5.Text = "INTEREST DATE(S)";
			this.ToolTip1.SetToolTip(this.Label1_5, null);
			// 
			// Label1_4
			// 
			this.Label1_4.Location = new System.Drawing.Point(30, 344);
			this.Label1_4.Name = "Label1_4";
			this.Label1_4.Size = new System.Drawing.Size(114, 17);
			this.Label1_4.TabIndex = 10;
			this.Label1_4.Text = "DUE DATE(S)";
			this.ToolTip1.SetToolTip(this.Label1_4, null);
			// 
			// Label1_2
			// 
			this.Label1_2.Location = new System.Drawing.Point(30, 164);
			this.Label1_2.Name = "Label1_2";
			this.Label1_2.Size = new System.Drawing.Size(114, 17);
			this.Label1_2.TabIndex = 4;
			this.Label1_2.Text = "TREASURER";
			this.ToolTip1.SetToolTip(this.Label1_2, null);
			// 
			// Label1_1
			// 
			this.Label1_1.Location = new System.Drawing.Point(30, 104);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(114, 17);
			this.Label1_1.TabIndex = 2;
			this.Label1_1.Text = "TAX COLLECTOR";
			this.ToolTip1.SetToolTip(this.Label1_1, null);
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(30, 44);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(114, 17);
			this.Label1_0.TabIndex = 0;
			this.Label1_0.Text = "COUNTY";
			this.ToolTip1.SetToolTip(this.Label1_0, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveAndContinue
			// 
			this.cmdSaveAndContinue.AppearanceKey = "acceptButton";
			this.cmdSaveAndContinue.Location = new System.Drawing.Point(371, 30);
			this.cmdSaveAndContinue.Name = "cmdSaveAndContinue";
			this.cmdSaveAndContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAndContinue.Size = new System.Drawing.Size(181, 48);
			this.cmdSaveAndContinue.TabIndex = 0;
			this.cmdSaveAndContinue.Text = "Save & Continue";
			this.ToolTip1.SetToolTip(this.cmdSaveAndContinue, null);
			this.cmdSaveAndContinue.Click += new System.EventHandler(this.cmdSaveAndContinue_Click);
			// 
			// frmCertificateOfAssessment
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(923, 727);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCertificateOfAssessment";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Certification of Assessment";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmCertificateOfAssessment_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCertificateOfAssessment_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kCollectionDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KCommitmentDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDueDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KInterestDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDueDate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KInterestDate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDueDate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KInterestDate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAndContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdSaveAndContinue;
	}
}
