﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWCL0000;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptInterestedPartyMasterList : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               12/31/2007              *
		// ********************************************************
		string strSQL = "";
		clsDRWrapper rsData = new clsDRWrapper();
		int lngTotalIntParties;
		string strCurrentModule;

		public rptInterestedPartyMasterList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Interested Party Master List";
		}

		public static rptInterestedPartyMasterList InstancePtr
		{
			get
			{
				return (rptInterestedPartyMasterList)Sys.GetInstance(typeof(rptInterestedPartyMasterList));
			}
		}

		protected rptInterestedPartyMasterList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			strCurrentModule = Strings.Mid(modGlobal.DEFAULTDATABASE, 3, 2);
			if (strCurrentModule == "CL")
				strCurrentModule = "RE";
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			frmQuestion.InstancePtr.Init(20, "By Interested Party ID", "By Interested Party Name", "Order Report By");
			if (modGlobal.Statics.gintPassQuestion == 0)
			{
				strSQL = "SELECT * FROM Owners WHERE ModuleCode = '" + strCurrentModule + "' AND ASSOCID = 0  ORDER BY AutoID";
			}
			else
			{
				strSQL = "SELECT * FROM Owners WHERE ModuleCode = '" + strCurrentModule + "' AND ASSOCID = 0 ORDER BY Name";
			}
			if (strCurrentModule != "PP")
			{
				rsData.OpenRecordset(strSQL, modExtraModules.strREDatabase);
			}
			else
			{
				rsData.OpenRecordset(strSQL, modExtraModules.strPPDatabase);
			}
			lngTotalIntParties = rsData.RecordCount();
			if (lngTotalIntParties == 0)
			{
				// hide the headers if there are no records
				lblIPNumber.Visible = false;
				lblAccount.Visible = false;
				lblName.Visible = false;
				lblAddress.Visible = false;
			}
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			float lngHt;
			intRow = 0;
			lngHt = 270 / 1440F;
			if (Strings.Trim(fldAddress1.Text) != "")
			{
				fldAddress1.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress1.Top = 0;
			}
			if (Strings.Trim(fldAddress2.Text) != "")
			{
				fldAddress2.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress2.Top = 0;
			}
			if (Strings.Trim(fldAddress3.Text) != "")
			{
				fldAddress3.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress3.Top = 0;
			}
			this.Detail.Height = intRow * lngHt + 200 / 1440F;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			if (!rsData.EndOfFile())
			{
				// TODO: Field [AutoID] not found!! (maybe it is an alias?)
				fldIPNumber.Text = FCConvert.ToString(rsData.Get_Fields("AutoID"));
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
				fldName.Text = rsData.Get_Fields_String("Name");
				fldAddress1.Text = rsData.Get_Fields_String("Address1");
				fldAddress2.Text = rsData.Get_Fields_String("Address2");
				fldAddress3.Text = FCConvert.ToString(rsData.Get_Fields_String("City")) + ", " + FCConvert.ToString(rsData.Get_Fields_String("State")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Zip"));
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4"))) != "")
				{
					fldAddress3.Text = fldAddress3.Text + "-" + FCConvert.ToString(rsData.Get_Fields_String("Zip4"));
				}
				SetupFields();
				// move to the next record in the query
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			if (lngTotalIntParties == 1)
			{
				lblFooter.Text = "There was " + FCConvert.ToString(lngTotalIntParties) + " interested parties processed.";
			}
			else
			{
				lblFooter.Text = "There were " + FCConvert.ToString(lngTotalIntParties) + " interested parties processed.";
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		
	}
}
