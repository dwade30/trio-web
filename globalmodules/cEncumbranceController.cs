﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	public class cEncumbranceController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cEncumbrance GetEncumbrance(int lngID)
		{
			cEncumbrance GetEncumbrance = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cEncumbrance enc = null;
				rsLoad.OpenRecordset("select * from Encumbrances where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					enc = new cEncumbrance();
					FillEncumbrance(ref enc, ref rsLoad);
				}
				GetEncumbrance = enc;
				return GetEncumbrance;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetEncumbrance;
		}

		public cGenericCollection GetEncumbrancesByJournalNumber(int lngJournalNumber)
		{
			cGenericCollection GetEncumbrancesByJournalNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cEncumbrance enc;
				rsLoad.OpenRecordset("select * from Encumbrances where journalnumber = " + FCConvert.ToString(lngJournalNumber) + " order by Description", "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					enc = new cEncumbrance();
					FillEncumbrance(ref enc, ref rsLoad);
					gColl.AddItem(enc);
					rsLoad.MoveNext();
				}
				GetEncumbrancesByJournalNumber = gColl;
				return GetEncumbrancesByJournalNumber;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetEncumbrancesByJournalNumber;
		}

		public cGenericCollection GetFullEncumbrancesByJournalNumber(int lngJournalNumber, string strArchive = "")
		{
			cGenericCollection GetFullEncumbrancesByJournalNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cEncumbrance enc;
				if (strArchive != "")
				{
					rsLoad.GroupName = strArchive;
				}
				rsLoad.OpenRecordset("select * from Encumbrances where journalnumber = " + FCConvert.ToString(lngJournalNumber) + " order by Description", "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					enc = new cEncumbrance();
					FillEncumbrance(ref enc, ref rsLoad);
					FillEncumbranceDetails_6(enc.ID, enc.EncumbranceDetails, strArchive);
					gColl.AddItem(enc);
					rsLoad.MoveNext();
				}
				GetFullEncumbrancesByJournalNumber = gColl;
				return GetFullEncumbrancesByJournalNumber;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetFullEncumbrancesByJournalNumber;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As int
		public int GetEncumbranceDetailCount(int lngEncumbranceID)
		{
			int GetEncumbranceDetailCount = 0;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngReturn;
				rsLoad.OpenRecordset("Select count(ID) as thecount from EncumbranceDetail where EncumbranceID = " + FCConvert.ToString(lngEncumbranceID), "Budgetary");
				// TODO: Field [thecount] not found!! (maybe it is an alias?)
				lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("thecount"))));
				GetEncumbranceDetailCount = lngReturn;
				return GetEncumbranceDetailCount;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetEncumbranceDetailCount;
		}

		public cGenericCollection GetEncumbranceDetails(int lngEncumbranceID)
		{
			cGenericCollection GetEncumbranceDetails = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cGenericCollection collDetails = new cGenericCollection();
				cEncumbranceDetail eDetail;
				rsLoad.OpenRecordset("Select * from EncumbranceDetail where EncumbranceID = " + FCConvert.ToString(lngEncumbranceID), "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					////////Application.DoEvents();
					eDetail = new cEncumbranceDetail();
					FillEncumbranceDetail(ref eDetail, ref rsLoad);
					collDetails.AddItem(eDetail);
					rsLoad.MoveNext();
				}
				GetEncumbranceDetails = collDetails;
				return GetEncumbranceDetails;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetEncumbranceDetails;
		}

		private void FillEncumbranceDetail(ref cEncumbranceDetail eDetail, ref clsDRWrapper rsLoad)
		{
			if (!(eDetail == null))
			{
				if (rsLoad != null)
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					eDetail.Account = rsLoad.Get_Fields_String("Account");
					eDetail.Adjustments = Conversion.Val(rsLoad.Get_Fields_Decimal("Adjustments"));
					// TODO: Check the table for the column [amount] and replace with corresponding Get_Field method
					eDetail.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
					eDetail.Project = rsLoad.Get_Fields_String("Project");
					eDetail.Description = rsLoad.Get_Fields_String("Description");
					eDetail.EncumbranceID = rsLoad.Get_Fields_Int32("EncumbranceID");
					eDetail.ID = rsLoad.Get_Fields_Int32("ID");
					eDetail.LiquidatedAmount = Conversion.Val(rsLoad.Get_Fields_Decimal("Liquidated"));
				}
			}
		}

		public void DeleteEncumbranceDetailsByEncumbranceID(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (lngID > 0)
				{
					clsDRWrapper rsEx = new clsDRWrapper();
					rsEx.Execute("Delete from encumbrancedetail where EncumbranceID = " + FCConvert.ToString(lngID), "Budgetary");
				}
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void DeleteEncumbrance(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from Encumbrances where id = " + FCConvert.ToString(lngID), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void DeleteEncumbranceDetail(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from EncumbranceDetail where id = " + FCConvert.ToString(lngID), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public cEncumbrance GetFullEncumbrance(int lngID)
		{
			cEncumbrance GetFullEncumbrance = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				cEncumbrance enc;
				rs.OpenRecordset("select * from encumbrances where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rs.EndOfFile())
				{
					enc = new cEncumbrance();
					FillEncumbrance(ref enc, ref rs);
					FillEncumbranceDetails_6(enc.ID, enc.EncumbranceDetails);
					GetFullEncumbrance = enc;
				}
				return GetFullEncumbrance;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetFullEncumbrance;
		}

		private void FillEncumbrance(ref cEncumbrance enc, ref clsDRWrapper rsLoad)
		{
			if (enc == null)
				return;
			if (!rsLoad.EndOfFile())
			{
				// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				enc.JournalNumber = rsLoad.Get_Fields_Int32("JournalNumber");
				enc.Adjustments = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("Adjustments"));
				enc.Description = rsLoad.Get_Fields_String("Description");
				// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
				enc.EncumbranceAmount = FCConvert.ToDouble(rsLoad.Get_Fields("Amount"));
				enc.IsPastYearEncumbrance = rsLoad.Get_Fields_Boolean("PastYearEnc");
				enc.LiquidatedAmount = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("Liquidated"));
				// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
				enc.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));
				// TODO: Check the table for the column [PO] and replace with corresponding Get_Field method
				enc.PO = rsLoad.Get_Fields_String("PO");
				if (Information.IsDate(rsLoad.Get_Fields("PostedDate")))
				{
					enc.PostedDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PostedDate"));
				}
				else
				{
					enc.PostedDate = "";
				}
				enc.Status = rsLoad.Get_Fields_String("Status");
				enc.TempVendorAddress1 = rsLoad.Get_Fields_String("TempVendorAddress1");
				enc.TempVendorAddress2 = rsLoad.Get_Fields_String("TempVendorAddress2");
				enc.TempVendorAddress3 = rsLoad.Get_Fields_String("TempVendorAddress3");
				enc.TempVendorAddress4 = rsLoad.Get_Fields_String("TempVendorAddress4");
				enc.TempVendorCity = rsLoad.Get_Fields_String("TempVendorCity");
				enc.TempVendorName = rsLoad.Get_Fields_String("TempVendorName");
				enc.TempVendorState = rsLoad.Get_Fields_String("TempVendorState");
				enc.TempVendorZip = rsLoad.Get_Fields_String("TempVendorZip");
				enc.TempVendorZip4 = rsLoad.Get_Fields_String("TempVendorZip4");
				enc.VendorNumber = rsLoad.Get_Fields_Int32("VendorNumber");
				if (Information.IsDate(rsLoad.Get_Fields("EncumbrancesDate")))
				{
					enc.EncumbrancesDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("EncumbrancesDate"));
				}
				else
				{
					enc.EncumbrancesDate = "";
				}
				enc.ID = rsLoad.Get_Fields_Int32("ID");
				enc.IsUpdated = false;
			}
		}

		private void FillEncumbranceDetails_6(int lngEncID, cGenericCollection encDetails, string strArchive = "")
		{
			FillEncumbranceDetails(lngEncID, ref encDetails, strArchive);
		}

		private void FillEncumbranceDetails(int lngEncID, ref cGenericCollection encDetails, string strArchive = "")
		{
			if (!(encDetails == null) && lngEncID > 0)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				cEncumbranceDetail eDetail;
				if (strArchive != "")
				{
					rsLoad.GroupName = strArchive;
				}
				rsLoad.OpenRecordset("Select * from EncumbranceDetail where EncumbranceID = " + FCConvert.ToString(lngEncID), "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					////////Application.DoEvents();
					eDetail = new cEncumbranceDetail();
					FillEncumbranceDetail(ref eDetail, ref rsLoad);
					encDetails.AddItem(eDetail);
					rsLoad.MoveNext();
				}
			}
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}

		public void SaveEncumbrance(ref cEncumbrance enc)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				return;
			}
			catch (Exception ex)
			{
				SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public cGenericCollection GetFullEncumbrancesForEOY()
		{
			cGenericCollection GetFullEncumbrancesForEOY = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cEncumbrance enc;
				rsLoad.OpenRecordset("SELECT * FROM Encumbrances WHERE Status = 'P' AND Description <> 'Control Entries' AND (Amount + Adjustments - Liquidated) <> 0", "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					enc = new cEncumbrance();
					FillEncumbrance(ref enc, ref rsLoad);
					FillEncumbranceDetails_6(enc.ID, enc.EncumbranceDetails);
					gColl.AddItem(enc);
					rsLoad.MoveNext();
				}
				GetFullEncumbrancesForEOY = gColl;
				return GetFullEncumbrancesForEOY;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetFullEncumbrancesForEOY;
		}
	}
}
