﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

#if TWCL0000
using TWCL0000;


#else
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarDADetail : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/04/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/08/2005              *
		// ********************************************************
		float lngWidth;
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL;
		clsDRWrapper rsReceiptNumber = new clsDRWrapper();
		bool boolWide;
		clsDRWrapper rsBill = new clsDRWrapper();
		bool boolOrderByReceipt;

		public sarDADetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarDADetail_ReportEnd;
		}

        private void SarDADetail_ReportEnd(object sender, EventArgs e)
        {
			rsData.DisposeOf();
            rsReceiptNumber.DisposeOf();
            rsBill.DisposeOf();
		}

        public static sarDADetail InstancePtr
		{
			get
			{
				return (sarDADetail)Sys.GetInstance(typeof(sarDADetail));
			}
		}

		protected sarDADetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsBill?.Dispose();
				rsData?.Dispose();
				rsReceiptNumber?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				eArgs.EOF = false;
				BindFields();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsOrder = new clsDRWrapper();
			if (modCLCalculations.Statics.gboolShowMapLotInCLAudit)
			{
				rsBill.OpenRecordset("SELECT ID, LienRecordNumber, MapLot, Name1, Name2 FROM BillingMaster", modExtraModules.strCLDatabase);
			}
			if (rsOrder.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase, 0, false, 0, false, "", false, "", true))
			{
				if (rsOrder.EndOfFile() != true)
				{
					if (FCConvert.ToBoolean(rsOrder.Get_Fields_Boolean("AuditSeqReceipt")))
					{
						boolOrderByReceipt = true;
					}
					else
					{
						boolOrderByReceipt = false;
					}
				}
				else
				{
					boolOrderByReceipt = true;
				}
			}
			else
			{
				FCMessageBox.Show("An error has occured while determining your default Audit sort order.  This will not affect the data.  The order has been set to Receipt Number.", MsgBoxStyle.Information, "Collections Table Error");
				boolOrderByReceipt = true;
			}
			// Setup Totals
			strSQL = "SELECT SUM(Principal) AS Prin, SUM(PreLienInterest) as PLI, SUM(CurrentInterest) AS Interest, SUM(LienCost) AS Costs FROM (SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + rptCLDailyAuditMaster.InstancePtr.strType + "AND Code <> 'I') AS QTmp";
			// "AND (Code = 'C' OR Code = 'P' OR Code = 'A' OR Code = 'D' OR Code = 'S' OR Code = 'R'))" ', SUM(Principal + PreLienInterest + LienCost) AS Tot
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsData.EndOfFile() != true)
			{
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Report", true);
				// fill the totals in
				fldTotalPrin.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("Prin")), "#,##0.00");
				// TODO: Field [PLI] not found!! (maybe it is an alias?)
				fldTotalPLI.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("PLI")), "#,##0.00");
				if (fldTotalPLI.Text == "0.00")
					fldTotalPLI.Text = " 0.00";
				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
				fldTotalInt.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("Interest")), "#,##0.00");
				if (fldTotalInt.Text == "0.00")
					fldTotalInt.Text = " 0.00";
				// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
				fldTotalCosts.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("Costs")), "#,##0.00");
				if (modGlobalConstants.Statics.gboolCR)
				{
					rsReceiptNumber.OpenRecordset("SELECT ReceiptKey, ReceiptNumber FROM Receipt", modExtraModules.strCRDatabase);
				}
				// fldTotalTotal.Text = Format(Val(rsData.Fields("Tot")), "#,##0.00")
			}
			else
			{
				fldTotalPrin.Text = " 0.00";
				fldTotalInt.Text = " 0.00";
				fldTotalPLI.Text = " 0.00";
				fldTotalCosts.Text = " 0.00";
				fldTotalTotal.Text = " 0.00";
			}
			// calculate the total line
			// MAL@20080527: Corrected to include PLI in the overall total
			// Tracker Reference: 13738
			// fldTotalTotal.Text = Format(CDbl(fldTotalPrin.Text) + CDbl(fldTotalInt.Text) + CDbl(fldTotalCosts.Text), "#,##0.00")
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPrin.Text) + FCConvert.ToDouble(fldTotalPLI.Text) + FCConvert.ToDouble(fldTotalInt.Text) + FCConvert.ToDouble(fldTotalCosts.Text), "#,##0.00");
			// this will put the SQL string together and set the order
			if (modGlobalConstants.Statics.gboolCR)
			{
				if (boolOrderByReceipt)
				{
					strSQL = "SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + rptCLDailyAuditMaster.InstancePtr.strType + "AND Code <> 'I' AND ReceiptNumber > 0 ORDER BY ReceiptNumber, [Year], Account, RecordedTransactionDate";
				}
				else
				{
					strSQL = "SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + rptCLDailyAuditMaster.InstancePtr.strType + "AND Code <> 'I' AND ReceiptNumber > 0 ORDER BY Account, [Year], RecordedTransactionDate";
				}
			}
			else
			{
				if (boolOrderByReceipt)
				{
					strSQL = "SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + rptCLDailyAuditMaster.InstancePtr.strType + "AND Code <> 'I' ORDER BY ReceiptNumber, [Year], Account, RecordedTransactionDate";
				}
				else
				{
					strSQL = "SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + rptCLDailyAuditMaster.InstancePtr.strType + "AND Code <> 'I' ORDER BY Account, [Year], RecordedTransactionDate";
				}
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			SetupFields();
			rsOrder.Dispose();
            frmWait.InstancePtr.IncrementProgress();
			if (rsData.EndOfFile())
			{
				EndRoutine();
			}
			
		}

		private void SetupFields()
		{
			// this will adjust the fields to their correct
			lngWidth = rptCLDailyAuditMaster.InstancePtr.lngWidth;
			boolWide = rptCLDailyAuditMaster.InstancePtr.boolLandscape;
			// header labels/lines
			if (!boolWide)
			{
				// portrait
				lblAccount.Left = 0;
				lblYear.Left = 720 / 1440F;
				lblPrincipal.Left = 1F;
				lblPLI.Left = 2610 / 1440F;
				lblInterest.Left = 3510 / 1440F;
				lblCosts.Left = 4410 / 1440F;
				lblTotal.Left = 5400 / 1440F;
				lblPer.Left = 6570 / 1440F;
				lblCode.Left = 6840 / 1440F;
				lblCash.Left = 7110 / 1440F;
				lblRef.Left = 7650 / 1440F;
				lblDate.Left = 8640 / 1440F;
				lblTeller.Left = 9540 / 1440F;
				lblRNum.Left = 10080 / 1440F;
			}
			else
			{
				// landscape
				lblAccount.Left = 0;
				lblYear.Left = 1080 / 1440F;
				lblPrincipal.Left = 2340 / 1440F;
				lblPLI.Left = 3780 / 1440F;
				lblInterest.Left = 5220 / 1440F;
				lblCosts.Left = 6660 / 1440F;
				lblTotal.Left = 8010 / 1440F;
				lblPer.Left = 9360 / 1440F;
				lblCode.Left = 9630 / 1440F;
				lblCash.Left = 10080 / 1440F;
				lblRef.Left = 10800 / 1440F;
				lblDate.Left = 11970 / 1440F;
				lblTeller.Left = 13230 / 1440F;
				lblRNum.Left = 14040 / 1440F;
			}
			lnHeader.X2 = lngWidth;
			this.PrintWidth = lngWidth;
			// detail section
			fldAccount.Left = lblAccount.Left;
			fldYear.Left = lblYear.Left;
			fldPrincipal.Left = lblPrincipal.Left;
			fldPLI.Left = lblPLI.Left;
			fldInterest.Left = lblInterest.Left;
			fldCosts.Left = lblCosts.Left;
			fldTotal.Left = lblTotal.Left;
			fldPer.Left = lblPer.Left;
			fldCode.Left = lblCode.Left;
			fldCash.Left = lblCash.Left;
			fldRef.Left = lblRef.Left;
			fldDate.Left = lblDate.Left;
			fldTeller.Left = lblTeller.Left;
			fldReceipt.Left = lblRNum.Left;
			// footer labels/fields
			lblTotals.Left = lblYear.Left;
			fldTotalPrin.Left = lblPrincipal.Left + lblPrincipal.Width - fldTotalPrin.Width;
			fldTotalPLI.Left = lblPLI.Left + lblPLI.Width - fldTotalPLI.Width;
			fldTotalInt.Left = lblInterest.Left + lblInterest.Width - fldTotalInt.Width;
			fldTotalCosts.Left = lblCosts.Left + lblCosts.Width - fldTotalCosts.Width;
			fldTotalTotal.Left = lblTotal.Left;
			lnTotals.X1 = lblTotals.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void BindFields()
		{
			// this will fill the fields with the correct data
			string strMapLot = "";
			string strName = "";
			frmWait.InstancePtr.IncrementProgress();
			if (modCLCalculations.Statics.gboolShowMapLotInCLAudit)
			{
				fldMapLot.Top = 270 / 1440F;
				fldMapLot.Left = fldRef.Left;
				if (FCConvert.ToString(rsData.Get_Fields_String("BillCode")) == "L")
				{
					rsBill.FindFirstRecord("ID", GetBillKeyFromLRN_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillKey"))));
					if (!rsBill.NoMatch)
					{
						// fill the map lot field
						strMapLot = FCConvert.ToString(rsBill.Get_Fields_String("MapLot"));
						if (Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("Name2"))) != "")
						{
							strName = Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("Name1"))) + " " + Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("Name2")));
						}
						else
						{
							strName = Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("Name1")));
						}
					}
					else
					{
						strMapLot = "";
					}
				}
				else
				{
					rsBill.FindFirstRecord("ID", rsData.Get_Fields_Int32("BillKey"));
					if (!rsBill.NoMatch)
					{
						// fill the map lot field
						strMapLot = FCConvert.ToString(rsBill.Get_Fields_String("MapLot"));
						if (Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("Name2"))) != "")
						{
							strName = Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("Name1"))) + " " + Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("Name2")));
						}
						else
						{
							strName = Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("Name1")));
						}
					}
					else
					{
						strMapLot = "";
					}
				}
				fldMapLot.Text = "Map Lot : " + strMapLot;
				fldName.Text = strName;
			}
			else
			{
				fldMapLot.Text = "";
				fldName.Text = "";
			}
			//FC:FINAL:DDU #i120 added conversion to sql result
			// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = Conversion.Str(rsData.Get_Fields("Account"));
			// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
			fldYear.Text = modExtraModules.FormatYear(FCConvert.ToString(rsData.Get_Fields("Year")));
			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
			fldPrincipal.Text = Strings.Format(rsData.Get_Fields("Principal"), "#,##0.00");
			fldPLI.Text = Strings.Format(rsData.Get_Fields_Decimal("PreLienInterest"), " #,##0.00");
			fldInterest.Text = Strings.Format(rsData.Get_Fields_Decimal("CurrentInterest"), " #,##0.00");
			fldCosts.Text = Strings.Format(rsData.Get_Fields_Decimal("LienCost"), "#,##0.00");
			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
			fldTotal.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("Principal")) + Conversion.Val(rsData.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsData.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsData.Get_Fields_Decimal("LienCost")), "#,##0.00");
			// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPer.Text = FCConvert.ToString(rsData.Get_Fields("Period"));
			fldCode.Text = rsData.Get_Fields_String("Code");
			if (FCConvert.ToString(rsData.Get_Fields_String("CashDrawer")) == "Y")
			{
				fldCash.Text = "Y";
			}
			else
			{
				if (FCConvert.ToString(rsData.Get_Fields_String("GeneralLedger")) == "Y")
				{
					fldCash.Text = "N Y";
				}
				else
				{
					fldCash.Text = "N N";
				}
			}
			fldRef.Text = rsData.Get_Fields_String("Reference");
			fldTeller.Text = Strings.Left(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Teller"))) + "   ", 3);
			fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yy");
			// fldDate.Text = Format(.Fields("EffectiveInterestDate"), "MM/dd/yy")
			if (modGlobalConstants.Statics.gboolCR)
			{
				// this will find the real receipt number
				//FC:FINAL:DDU #i120 added conversion to sql result
				rsReceiptNumber.FindFirstRecord("ReceiptKey", Conversion.Str(rsData.Get_Fields_Int32("ReceiptNumber")));
				if (!rsReceiptNumber.NoMatch)
				{
					fldReceipt.Text = Conversion.Str(rsReceiptNumber.Get_Fields_Int32("ReceiptNumber"));
				}
				else
				{
					fldReceipt.Text = Conversion.Str(rsData.Get_Fields_Int32("ReceiptNumber"));
				}
			}
			else
			{
				fldReceipt.Text = Conversion.Str(rsData.Get_Fields_Int32("ReceiptNumber"));
			}
			// move to the next record
			rsData.MoveNext();
		}

		private void EndRoutine()
		{
			// this will show a message that there are no records to process and hide the rest of the report
			lnHeader.Visible = false;
			lnTotals.Visible = false;
			lblAccount.Visible = false;
			lblYear.Visible = false;
			lblPrincipal.Visible = false;
			lblInterest.Visible = false;
			lblPLI.Visible = false;
			lblCosts.Visible = false;
			lblTotal.Visible = false;
			lblPer.Visible = false;
			lblCode.Visible = false;
			lblRef.Visible = false;
			lblDate.Visible = false;
			lblTeller.Visible = false;
			lblRNum.Visible = false;
			fldTotalPrin.Visible = false;
			fldTotalInt.Visible = false;
			fldTotalCosts.Visible = false;
			fldTotalTotal.Visible = false;
			lblTotals.Visible = false;
			// lblTotals.Width = lngWidth - lblTotals.Left
			// lblTotals.Text = "There are no records to process."
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (modCLCalculations.Statics.gboolShowMapLotInCLAudit)
			{
				Detail.Height = 540 / 1440F;
				fldMapLot.Visible = true;
				fldName.Visible = true;
				fldMapLot.Top = 270 / 1440F;
				fldName.Top = 270 / 1440F;
				fldName.Left = fldPrincipal.Left;
				fldMapLot.Left = fldRef.Left;
			}
			else
			{
				Detail.Height = 270 / 1440F;
				fldMapLot.Visible = false;
				fldName.Visible = false;
			}
		}

		private int GetBillKeyFromLRN_2(int lngLRN)
		{
			return GetBillKeyFromLRN(ref lngLRN);
		}

		private int GetBillKeyFromLRN(ref int lngLRN)
		{
			int GetBillKeyFromLRN = 0;
			var rsBK = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER

                rsBK.OpenRecordset("SELECT * FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(lngLRN),
                    modExtraModules.strCLDatabase);
                if (!rsBK.EndOfFile())
                {
                    GetBillKeyFromLRN = FCConvert.ToInt32(rsBK.Get_Fields_Int32("ID"));
                }

                return GetBillKeyFromLRN;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting BK From LRN");
            }
            finally
            {
				rsBK.Dispose();
            }
			return GetBillKeyFromLRN;
		}

		
	}
}
