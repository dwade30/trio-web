﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;

namespace Global
{
	public class cCentralDocumentDB
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		const string CNSTDocumentDBName = "CentralDocuments";

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngError, string strError)
		{
			strLastError = strError;
			lngLastError = lngError;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", CNSTDocumentDBName);
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
					tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
					tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
					tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
				lngLastError = Information.Err(ex).Number;
			}
			return GetVersion;
		}

		public void SetVersion(cVersionInfo nVersion)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", CNSTDocumentDBName);
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
				lngLastError = Information.Err(ex).Number;
			}
		}

		private bool FieldExists(string strTable, string strField, string strDB)
		{
			bool FieldExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (!rsLoad.EndOfFile())
				{
					FieldExists = true;
				}
				return FieldExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return FieldExists;
		}

		private bool TableExists(string strTable, string strDB)
		{
			bool TableExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				strSQL = "select * from sys.tables where name = '" + strTable + "' and type = 'U'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (!rsLoad.EndOfFile())
				{
					TableExists = true;
				}
				return TableExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return TableExists;
		}

		private string GetTableCreationHeaderSQL(string strTableName)
		{
			string GetTableCreationHeaderSQL = "";
			string strSQL;
			strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " ;
			strSQL += "Create Table [dbo].[" + strTableName + "] (";
			strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
			GetTableCreationHeaderSQL = strSQL;
			return GetTableCreationHeaderSQL;
		}

		private string GetTablePKSql(string strTableName)
		{
			string GetTablePKSql = "";
			string strSQL;
			strSQL = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
			GetTablePKSql = strSQL;
			return GetTablePKSql;
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;
				clsDRWrapper rsTest = new clsDRWrapper();
				if (!rsTest.DBExists(CNSTDocumentDBName))
				{
					CreateDB();
					cVer = new cVersionInfo();
				}
				else
				{
					if (TableExists("DBVersion", CNSTDocumentDBName))
					{
						cVer = GetVersion();
					}
					else
					{
						cVer = new cVersionInfo();
					}
				}
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1;
				// default to 1.0.0.0
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 0;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddDBVersion())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return false;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddDocumentsTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				
                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 4;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddDocumentIdentifier())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                
                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 6;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddDateCreatedToDocuments())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                
                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 9;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddHashField())
                    {
						nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
							SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 10;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddDocumentsStoredProcedures())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

				tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 11;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddSketchesTable())
                    {
						nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
							SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
				
				CheckVersion = true;
				return CheckVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return CheckVersion;
		}

        private void CreateDB()
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				clsDRWrapper rsSave = new clsDRWrapper();
				strSQL = "Create Database [" + rsSave.Get_GetFullDBName("CentralDocuments") + "] ";
				rsSave.Execute(strSQL, "SystemSettings");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		private bool AddDBVersion()
		{
			bool AddDBVersion = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				if (!TableExists("DBVersion", CNSTDocumentDBName))
				{
					strSQL = GetTableCreationHeaderSQL("DBVersion");
					strSQL += "[Build] [int]  NULL,";
					strSQL += "[Major] [int] NULL,";
					strSQL += "[Minor] [int] NULL,";
					strSQL += "[Revision] [int] NULL ";
					strSQL += GetTablePKSql("DBVersion");
					rsSave.Execute(strSQL, CNSTDocumentDBName, false);
				}
				AddDBVersion = true;
				return AddDBVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return AddDBVersion;
		}

		private bool AddDocumentsTable()
		{
			bool AddDocumentsTable = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				if (!TableExists("Documents", CNSTDocumentDBName))
				{
					strSQL = GetTableCreationHeaderSQL("Documents");
					strSQL += "[MediaType] [nvarchar] (50) NULL,";
					strSQL += "[ItemData] [varbinary] (Max) NULL,";
					strSQL += "[ItemName] [nvarchar] (255) NULL,";
					strSQL += "[ItemDescription] [nvarchar] (255) NULL,";
					strSQL += "[ReferenceGroup] [nvarchar] (50) NULL, ";
					strSQL += "[ReferenceType] [nvarchar] (255) NULL,";
					strSQL += "[ReferenceId] [int] NULL,";
					strSQL += "[AltReference] [nvarchar] (50) NULL,";
                    strSQL += "[DocumentIdentifier] [UniqueIdentifier] Not Null ";
					strSQL += GetTablePKSql("Documents");
					rsSave.Execute(strSQL, CNSTDocumentDBName, false);
				}
				AddDocumentsTable = true;
				return AddDocumentsTable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return AddDocumentsTable;
		}

		private bool AddDocumentsStoredProcedures()
		{
			bool AddDocumentsStoredProcedures = false;
			if (AddInsertDocumentsProcedure())
			{
				if (AddUpdateDocumentsProcedure())
				{
					if (AddUpdateDocumentInfoProcedure())
					{
						AddDocumentsStoredProcedures = true;
					}
				}
			}
			return AddDocumentsStoredProcedures;
		}

		private bool AddInsertDocumentsProcedure()
		{
			bool AddInsertDocumentsProcedure = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("Select * from sys.objects where object_id = OBJECT_ID(N'Insert_Document') and type in (N'P', N'PC')", CNSTDocumentDBName);
                if (!rsSave.EndOfFile())
                {
                    rsSave.Execute("Drop procedure [dbo].[Insert_Document]", CNSTDocumentDBName);
                }

					strSQL = "Create Procedure [dbo].[Insert_Document] " ;
					strSQL += " @MediaType nvarchar(50) = '', ";
					strSQL +=  "@ItemData varbinary(Max) , ";
					strSQL +=  "@ItemName nvarchar(50) = '', ";
					strSQL +=  "@ItemDescription nvarchar(255) = '', ";
					strSQL +=  "@ReferenceType nvarchar(50) = '', ";
					strSQL +=  "@ReferenceId int = 0, ";
					strSQL +=  "@AltReference nvarchar(50) = '', ";
					strSQL +=  "@DataGroup nvarchar(50) = '', ";
                    strSQL +=  "@DocumentIdentifier UniqueIdentifier  ,";
                    strSQL += "@DateCreated DateTime ,";
					strSQL +=  "@ID int OUTPUT ";
					strSQL +=  "AS BEGIN SET NOCOUNT ON; ";
					strSQL +=  "Insert into [Documents] ([MediaType],[ItemData],[ItemName],[ItemDescription],[ReferenceType],[ReferenceId],[AltReference],[ReferenceGroup],[DocumentIdentifier],[DateCreated],[HashedValue]) ";
					strSQL +=  "Values ";
					strSQL +=  "(@MediaType,@ItemData,@ItemName,@ItemDescription,@ReferenceType,@ReferenceId,@AltReference,@DataGroup,@DocumentIdentifier,@DateCreated,HashBytes('Sha2_256',@ItemData)) ";
					strSQL +=  "set @ID = SCOPE_IDENTITY() ";
					strSQL +=  "END";
					if (!rsSave.Execute(strSQL, CNSTDocumentDBName, false))
					{
						return false;
					}
				
				AddInsertDocumentsProcedure = true;
				return AddInsertDocumentsProcedure;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return AddInsertDocumentsProcedure;
		}

		private bool AddUpdateDocumentsProcedure()
		{
			bool AddUpdateDocumentsProcedure = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("Select * from sys.objects where object_id = OBJECT_ID(N'Update_Document') and type in (N'P', N'PC')", CNSTDocumentDBName);
                if (!rsSave.EndOfFile())
                {
                    rsSave.Execute("Drop procedure [dbo].[Update_Document]", CNSTDocumentDBName);
                }

					strSQL = "Create Procedure [dbo].[Update_Document] " ;
					strSQL +=  "@ID int ,";
					strSQL +=  "@MediaType nvarchar(50) = '', ";
					strSQL +=  "@ItemData varbinary(Max) , ";
					strSQL +=  "@ItemName nvarchar(50) = '', ";
					strSQL +=  "@ItemDescription nvarchar(255) = '', ";
					strSQL +=  "@ReferenceType nvarchar(50) = '', ";
					strSQL +=  "@ReferenceId int = 0, ";
					strSQL +=  "@AltReference nvarchar(50) = '', ";
					strSQL +=  "@DataGroup nvarchar(50) = '' ";
                    strSQL +=  "as BEGIN SET NOCOUNT ON; ";
					strSQL +=  "update [Documents] set ";
					strSQL +=  "AltReference = @AltReference, ";
					strSQL +=  "MediaType = @MediaType, ";
					strSQL +=  "ItemData = @ItemData, ";
					strSQL += "ItemName = @ItemName, ";
					strSQL +=  "ItemDescription = @ItemDescription, ";
					strSQL +=  "ReferenceType = @ReferenceType, ";
					strSQL +=  "ReferenceId = @ReferenceId, ";
					strSQL +=  "ReferenceGroup = @DataGroup, ";
                    strSQL +=  "HashedValue = HashBytes('SHA2_256',@ItemData) ";
					strSQL +=  "where ";
					strSQL +=  "ID = @ID ";
					strSQL +=  "END";
					if (!rsSave.Execute(strSQL, CNSTDocumentDBName, false))
					{
						return false;
					}

                return true;
            }
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return false;
		}

		private bool AddUpdateDocumentInfoProcedure()
		{
			bool AddUpdateDocumentInfoProcedure = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("Select * from sys.objects where object_id = OBJECT_ID(N'Update_DocumentInfo') and type in (N'P', N'PC')", CNSTDocumentDBName);
                if (!rsSave.EndOfFile())
                {
                    rsSave.Execute("Drop procedure [dbo].[Update_DocumentInfo]", CNSTDocumentDBName);
                }

					strSQL = "Create Procedure [dbo].[Update_DocumentInfo] " ;
					strSQL += "@ID int ,";
					strSQL +=  "@MediaType nvarchar(50) = '', ";
					strSQL +=  "@ItemName nvarchar(50) = '', ";
					strSQL +=  "@ItemDescription nvarchar(255) = '', ";
					strSQL +=  "@ReferenceType nvarchar(50) = '', ";
					strSQL +=  "@ReferenceId int = 0, ";
					strSQL +=  "@AltReference nvarchar(50) = '', ";
					strSQL +=  "@DataGroup nvarchar(50) = '' ";
					strSQL +=  "as BEGIN SET NOCOUNT ON; ";
					strSQL += "update [Documents] set ";
					strSQL += "AltReference = @AltReference, ";
					strSQL +=  "MediaType = @MediaType, ";
					strSQL +=  "ItemName = @ItemName, ";
					strSQL +=  "ItemDescription = @ItemDescription, ";
					strSQL +=  "ReferenceType = @ReferenceType, ";
					strSQL +=  "ReferenceId = @ReferenceId, ";
					strSQL +=  "ReferenceGroup = @DataGroup ";
					strSQL +=  "where ";
					strSQL +=  "ID = @ID ";
					strSQL +=  "END";
					if (!rsSave.Execute(strSQL, CNSTDocumentDBName, false))
					{
						return false;
					}
				return true;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return false;
		}

        private bool AddDocumentIdentifier()
        {
            try
            {
                if (!FieldExists("Documents", "DocumentIdentifier", "CentralDocuments"))
                {
                    var rsSave = new clsDRWrapper();
                    rsSave.Execute("Alter Table Documents Add DocumentIdentifier UniqueIdentifier Not Null Default newid()",
                        "CentralDocuments");
                    rsSave.Execute("Update Documents set DocumentIdentifier = NEWID() where DocumentIdentifier is null",
                        "CentralDocuments");
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddDateCreatedToDocuments()
        {
            try
            {
                if (!FieldExists("Documents", "DateCreated", "CentralDocuments"))
                {
                    var rsSave = new clsDRWrapper();
                    rsSave.Execute("Alter Table Documents Add DateCreated DateTime not null Default GetDate()",
                        "CentralDocuments");
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddSketchesTable()
        {
            try
            {
                if (!TableExists("Sketches", "CentralDocuments"))
                {
                    var rsSave =new clsDRWrapper();
                    string strSQL = "";
                    strSQL = GetTableCreationHeaderSQL("Sketches");
                    strSQL += "[MediaType] [nvarchar] (50) NULL,";
                    strSQL += "[SketchData] [varbinary] (Max) NULL,";
                    //strSQL += "[Account] [int] NULL,";
                    //strSQL += "[Card] [int] NULL,";
                    strSQL += "[CardIdentifier] [UniqueIdentifier] Not Null,";
                    strSQL += "[SketchIdentifier] [UniqueIdentifier] Not Null,";
                    strSQL += "[SketchImage] [varbinary] (Max) NULL,";
                    strSQL += "[SequenceNumber] [int] NULL,";
                    strSQL += "[Calculations] [nvarchar] (1024) NULL,";
                    strSQL += "[DateUpdated] [DateTime] Not Null Default GetDate()";
                    strSQL += GetTablePKSql("Sketches");
                    rsSave.Execute(strSQL, CNSTDocumentDBName, false);


                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddHashField()
        {
            try
            {
				var rsSave = new clsDRWrapper();
                var sql =
                    "if not exists (select * from sys.columns where OBJECT_ID = object_id(N'[dbo].[Documents]') and name = 'HashedValue') Begin ";
                sql += "Alter Table Documents Add HashedValue VarBinary(64) NULL ";
                sql += "EXEC sp_executesql N'";
                sql += "Update Documents set HashedValue = HashBytes(''SHA2_256'',ItemData)'";
                       sql += " END";
                       if (rsSave.Execute(sql, "CentralDocuments"))
                       {
                           
                           return true;
                       }
            }
            catch
            {
                return false;
            }

            return false;
        }
    }
}
