﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarBookPageMH : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/27/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/27/2005              *
		// ********************************************************
		int lngAcct;
		clsDRWrapper rsData = new clsDRWrapper();

		public sarBookPageMH()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarBookPageMH_ReportEnd;
		}

        private void SarBookPageMH_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static sarBookPageMH InstancePtr
		{
			get
			{
				return (sarBookPageMH)Sys.GetInstance(typeof(sarBookPageMH));
			}
		}

		protected sarBookPageMH _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lngAcct = FCConvert.ToInt32(this.UserData);
			strSQL = "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Module = 'RE' AND Account = " + FCConvert.ToString(lngAcct);
			rsData.OpenRecordset(strSQL, "CentralData");
			if (rsData.EndOfFile())
			{
				fldBookPage.Text = "";
				// No Matches"
				lblHeader.Visible = false;
				GroupHeader1.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					fldBookPage.Text = rsData.Get_Fields_String("BookPage");
					fldMHName.Text = rsData.Get_Fields_String("Name");
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Book Page Report - MH");
			}
		}

		private void sarBookPageMH_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarBookPageMH.Caption	= "Account Detail";
			//sarBookPageMH.Icon	= "sarBookPageMH.dsx":0000";
			//sarBookPageMH.Left	= 0;
			//sarBookPageMH.Top	= 0;
			//sarBookPageMH.Width	= 11880;
			//sarBookPageMH.Height	= 8595;
			//sarBookPageMH.StartUpPosition	= 3;
			//sarBookPageMH.SectionData	= "sarBookPageMH.dsx":058A;
			//End Unmaped Properties
		}
	}
}
