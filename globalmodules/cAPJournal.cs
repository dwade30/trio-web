﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cAPJournal
	{
		//=========================================================
		private int lngRecordID;
		private int lngJournalNumber;
		private int lngVendorNumber;
		private string strTempVendorName = string.Empty;
		private string strTempVendorAddress1 = string.Empty;
		private string strTempVendorAddress2 = string.Empty;
		private string strTempVendorAddress3 = string.Empty;
		private string strTempVendorAddress4 = string.Empty;
		private int intJournalPeriod;
		private string strPayableDate = "";
		private string strFrequency = string.Empty;
		private string strUntilTime = "";
		private bool boolSeparate;
		private string strJournalDescription = string.Empty;
		private string strReference = string.Empty;
		private double dblTotalAmount;
		private string strCheckNumber = string.Empty;
		private string strJournalStatus = string.Empty;
		private string strPostedDate = "";
		private string strWarrant = string.Empty;
		private string strCheckDate = "";
		private string strReturned = string.Empty;
		private double dblTotalEncumbrance;
		private int lngEncumbranceRecord;
		private string strTempVendorCity = string.Empty;
		private string strTempVendorState = string.Empty;
		private string strTempVendorZip = string.Empty;
		private string strTempVendorZip4 = string.Empty;
		private int lngSeparateCheckRecord;
		private bool boolPrepaidCheck;
		private bool boolUseAlternateCash;
		private bool boolCreditMemoCorrection;
		private int lngCreditMemoRecord;
		private string strTownOverride = string.Empty;
		private bool boolPrintedIndividual;
		private int lngPurchaseOrderID;
		private bool boolEFTCheck;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collJournalEntries = new cGenericCollection();
		private cGenericCollection collJournalEntries_AutoInitialized;

		private cGenericCollection collJournalEntries
		{
			get
			{
				if (collJournalEntries_AutoInitialized == null)
				{
					collJournalEntries_AutoInitialized = new cGenericCollection();
				}
				return collJournalEntries_AutoInitialized;
			}
			set
			{
				collJournalEntries_AutoInitialized = value;
			}
		}

		private cCreditMemo credMemoRecord;
		private cVendor vend;
		private bool boolUpdated;
		private bool boolDeleted;
		private string strAltCashAccount = string.Empty;
		private int intPeriod;

		public cCreditMemo CreditMemo
		{
			set
			{
				credMemoRecord = value;
				IsUpdated = true;
			}
			get
			{
				cCreditMemo CreditMemo = null;
				CreditMemo = credMemoRecord;
				return CreditMemo;
			}
		}

		public string AlternateCashAccount
		{
			set
			{
				strAltCashAccount = value;
				IsUpdated = true;
			}
			get
			{
				string AlternateCashAccount = "";
				AlternateCashAccount = strAltCashAccount;
				return AlternateCashAccount;
			}
		}

		public cVendor Vendor
		{
			set
			{
				vend = value;
				IsUpdated = true;
			}
			get
			{
				cVendor Vendor = null;
				Vendor = vend;
				return Vendor;
			}
		}

		public cGenericCollection JournalEntries
		{
			get
			{
				cGenericCollection JournalEntries = null;
				JournalEntries = collJournalEntries;
				return JournalEntries;
			}
		}

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
				IsUpdated = true;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public string TempVendorName
		{
			set
			{
				strTempVendorName = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorName = "";
				TempVendorName = strTempVendorName;
				return TempVendorName;
			}
		}

		public string TempVendorAddress1
		{
			set
			{
				strTempVendorAddress1 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorAddress1 = "";
				TempVendorAddress1 = strTempVendorAddress1;
				return TempVendorAddress1;
			}
		}

		public string TempVendorAddress2
		{
			set
			{
				strTempVendorAddress2 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorAddress2 = "";
				TempVendorAddress2 = strTempVendorAddress2;
				return TempVendorAddress2;
			}
		}

		public string TempVendorAddress3
		{
			set
			{
				strTempVendorAddress3 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorAddress3 = "";
				TempVendorAddress3 = strTempVendorAddress3;
				return TempVendorAddress3;
			}
		}

		public string TempVendorAddress4
		{
			set
			{
				strTempVendorAddress4 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorAddress4 = "";
				TempVendorAddress4 = strTempVendorAddress4;
				return TempVendorAddress4;
			}
		}

		public int Period
		{
			set
			{
				intJournalPeriod = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				return intJournalPeriod;
			}
		}

		public string PayableDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strPayableDate = value;
				}
				else
				{
					strPayableDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string PayableDate = "";
				PayableDate = strPayableDate;
				return PayableDate;
			}
		}

		public string Frequency
		{
			set
			{
				strFrequency = value;
				IsUpdated = true;
			}
			get
			{
				string Frequency = "";
				Frequency = strFrequency;
				return Frequency;
			}
		}

		public string UntilDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strUntilTime = value;
				}
				else
				{
					strUntilTime = "";
				}
				IsUpdated = true;
			}
			get
			{
				string UntilDate = "";
				UntilDate = strUntilTime;
				return UntilDate;
			}
		}

		public bool Separate
		{
			set
			{
				boolSeparate = value;
				IsUpdated = true;
			}
			get
			{
				bool Separate = false;
				Separate = boolSeparate;
				return Separate;
			}
		}

		public string Description
		{
			set
			{
				strJournalDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strJournalDescription;
				return Description;
			}
		}

		public string Reference
		{
			set
			{
				strReference = value;
				IsUpdated = true;
			}
			get
			{
				string Reference = "";
				Reference = strReference;
				return Reference;
			}
		}

		public double Amount
		{
			set
			{
				dblTotalAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblTotalAmount;
				return Amount;
			}
		}

		public string CheckNumber
		{
			set
			{
				strCheckNumber = value;
				IsUpdated = true;
			}
			get
			{
				string CheckNumber = "";
				CheckNumber = strCheckNumber;
				return CheckNumber;
			}
		}

		public string Status
		{
			set
			{
				strJournalStatus = value;
				IsUpdated = true;
			}
			get
			{
				string Status = "";
				Status = strJournalStatus;
				return Status;
			}
		}

		public string PostedDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strPostedDate = value;
				}
				else
				{
					strPostedDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string PostedDate = "";
				PostedDate = strPostedDate;
				return PostedDate;
			}
		}

		public string Warrant
		{
			set
			{
				strWarrant = value;
				IsUpdated = true;
			}
			get
			{
				string Warrant = "";
				Warrant = strWarrant;
				return Warrant;
			}
		}

		public string CheckDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strCheckDate = value;
				}
				else
				{
					strCheckDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string CheckDate = "";
				CheckDate = strCheckDate;
				return CheckDate;
			}
		}

		public string Returned
		{
			set
			{
				strReturned = value;
				IsUpdated = true;
			}
			get
			{
				string Returned = "";
				Returned = strReturned;
				return Returned;
			}
		}

		public double TotalEncumbrance
		{
			set
			{
				dblTotalEncumbrance = value;
				IsUpdated = true;
			}
			get
			{
				double TotalEncumbrance = 0;
				TotalEncumbrance = dblTotalEncumbrance;
				return TotalEncumbrance;
			}
		}

		public int EncumbranceRecord
		{
			set
			{
				lngEncumbranceRecord = value;
				IsUpdated = true;
			}
			get
			{
				int EncumbranceRecord = 0;
				EncumbranceRecord = lngEncumbranceRecord;
				return EncumbranceRecord;
			}
		}

		public string TempVendorCity
		{
			set
			{
				strTempVendorCity = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorCity = "";
				TempVendorCity = strTempVendorCity;
				return TempVendorCity;
			}
		}

		public string TempVendorState
		{
			set
			{
				strTempVendorState = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorState = "";
				TempVendorState = strTempVendorState;
				return TempVendorState;
			}
		}

		public string TempVendorZip
		{
			set
			{
				strTempVendorZip = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorZip = "";
				TempVendorZip = strTempVendorZip;
				return TempVendorZip;
			}
		}

		public string TempVendorZip4
		{
			set
			{
				strTempVendorZip4 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorZip4 = "";
				TempVendorZip4 = strTempVendorZip4;
				return TempVendorZip4;
			}
		}

		public int SeparateCheckRecord
		{
			set
			{
				lngSeparateCheckRecord = value;
				IsUpdated = true;
			}
			get
			{
				int SeparateCheckRecord = 0;
				SeparateCheckRecord = lngSeparateCheckRecord;
				return SeparateCheckRecord;
			}
		}

		public bool PrepaidCheck
		{
			set
			{
				boolPrepaidCheck = value;
				IsUpdated = true;
			}
			get
			{
				bool PrepaidCheck = false;
				PrepaidCheck = boolPrepaidCheck;
				return PrepaidCheck;
			}
		}

		public bool UseAlternateCash
		{
			set
			{
				boolUseAlternateCash = value;
				IsUpdated = true;
			}
			get
			{
				bool UseAlternateCash = false;
				UseAlternateCash = boolUseAlternateCash;
				return UseAlternateCash;
			}
		}

		public bool CREDITMEMOCORRECTION
		{
			set
			{
				boolCreditMemoCorrection = value;
				IsUpdated = true;
			}
			get
			{
				bool CREDITMEMOCORRECTION = false;
				CREDITMEMOCORRECTION = boolCreditMemoCorrection;
				return CREDITMEMOCORRECTION;
			}
		}

		public int creditMemoRecord
		{
			set
			{
				lngCreditMemoRecord = value;
				IsUpdated = true;
			}
			get
			{
				int creditMemoRecord = 0;
				creditMemoRecord = lngCreditMemoRecord;
				return creditMemoRecord;
			}
		}

		public string TownOverride
		{
			set
			{
				strTownOverride = value;
				IsUpdated = true;
			}
			get
			{
				string TownOverride = "";
				TownOverride = strTownOverride;
				return TownOverride;
			}
		}

		public bool PrintedIndividual
		{
			set
			{
				boolPrintedIndividual = value;
				IsUpdated = true;
			}
			get
			{
				bool PrintedIndividual = false;
				PrintedIndividual = boolPrintedIndividual;
				return PrintedIndividual;
			}
		}

		public int PurchaseOrderID
		{
			set
			{
				lngPurchaseOrderID = value;
				IsUpdated = true;
			}
			get
			{
				int PurchaseOrderID = 0;
				PurchaseOrderID = lngPurchaseOrderID;
				return PurchaseOrderID;
			}
		}

		public bool EFTCheck
		{
			set
			{
				boolEFTCheck = value;
				IsUpdated = true;
			}
			get
			{
				bool EFTCheck = false;
				EFTCheck = boolEFTCheck;
				return EFTCheck;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
