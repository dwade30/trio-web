﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cDriveWriteInfo
	{
		//=========================================================
		private bool boolCDWrite;
		private bool boolDVDWrite;
		private bool boolBDWrite;
		private bool boolCDRW;
		private bool boolDVDRW;
		private string strVolume = "";
		private int intDriveIndex;
		private string strUniqueID = string.Empty;
		private FCCollection colWriteSpeeds = new FCCollection();

		public FCCollection WriteSpeeds
		{
			get
			{
				FCCollection WriteSpeeds = null;
				WriteSpeeds = colWriteSpeeds;
				return WriteSpeeds;
			}
		}
		// vbPorter upgrade warning: lngSpeed As int	OnWriteFCConvert.ToDouble(
		public void AddSpeed(int lngSpeed, string strDescription)
		{
			cOpticalWriteSpeed tSpeed = new cOpticalWriteSpeed();
			tSpeed.WriteSpeed = lngSpeed;
			tSpeed.Description = strDescription;
			colWriteSpeeds.Add(tSpeed);
		}

		public string Description
		{
			get
			{
				string Description = "";
				string strReturn;
				string strComma;
				strComma = "";
				strReturn = strVolume;
				if (boolCDWrite || boolCDRW)
				{
					strReturn += strComma + " ";
					strComma = ",";
					if (boolCDRW)
					{
						strReturn += "CD-RW";
					}
					else
					{
						strReturn += "CD";
					}
				}
				if (boolDVDWrite || boolDVDRW)
				{
					strReturn += strComma + " ";
					strComma = ",";
					if (boolDVDRW)
					{
						strReturn += "DVD-RW";
					}
					else
					{
						strReturn += "DVD";
					}
				}
				if (boolBDWrite)
				{
					strReturn += strComma + " ";
					strComma = ",";
					strReturn += "BD";
				}
				Description = strReturn;
				return Description;
			}
		}

		public bool CDWrite
		{
			set
			{
				boolCDWrite = value;
			}
			get
			{
				bool CDWrite = false;
				CDWrite = boolCDWrite;
				return CDWrite;
			}
		}

		public bool DVDWrite
		{
			set
			{
				boolDVDWrite = value;
			}
			get
			{
				bool DVDWrite = false;
				DVDWrite = boolDVDWrite;
				return DVDWrite;
			}
		}

		public bool BDWrite
		{
			set
			{
				boolBDWrite = value;
			}
			get
			{
				bool BDWrite = false;
				BDWrite = boolBDWrite;
				return BDWrite;
			}
		}

		public bool CDRW
		{
			set
			{
				boolCDRW = value;
			}
			get
			{
				bool CDRW = false;
				CDRW = boolCDRW;
				return CDRW;
			}
		}

		public bool DVDRW
		{
			set
			{
				boolDVDRW = value;
			}
			get
			{
				bool DVDRW = false;
				DVDRW = boolDVDRW;
				return DVDRW;
			}
		}

		public string Volume
		{
			set
			{
				strVolume = value;
			}
			get
			{
				string Volume = "";
				Volume = strVolume;
				return Volume;
			}
		}

		public short DriveIndex
		{
			set
			{
				intDriveIndex = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short DriveIndex = 0;
				DriveIndex = FCConvert.ToInt16(intDriveIndex);
				return DriveIndex;
			}
		}

		public string ID
		{
			set
			{
				strUniqueID = value;
			}
			get
			{
				string ID = "";
				ID = strUniqueID;
				return ID;
			}
		}

		public cDriveWriteInfo() : base()
		{
			boolCDWrite = false;
			boolDVDWrite = false;
			boolBDWrite = false;
			boolCDRW = false;
			strVolume = "";
			intDriveIndex = -1;
			strUniqueID = "";
		}
	}
}
