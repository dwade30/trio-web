//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.DataBaseLayer;
using fecherFoundation.Extensions;

#if TWAR0000
using TWAR0000;


#elif TWBL0000
using TWBL0000;
#endif
using System.IO;

namespace Global
{
	/// <summary>
	/// Summary description for frmCustomBill.
	/// </summary>
	partial class frmCustomBill : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> CustomLabel;
		public fecherFoundation.FCGrid GridCopy;
		public fecherFoundation.FCGrid GridHighlighted;
		public fecherFoundation.FCFrame framControlInfo;
		public fecherFoundation.FCButton cmdOKControlInfoNew;
		public fecherFoundation.FCGrid GridControlInfo;
		public fecherFoundation.FCButton cmdOKControlInfo;
		public fecherFoundation.FCGrid GridControlTypes;
		public fecherFoundation.FCPictureBox imgControlImage;
		public fecherFoundation.FCGrid GridFields;
		public fecherFoundation.FCGrid GridBillSize;
		public fecherFoundation.FCGrid GridDeleted;
		public fecherFoundation.FCPanel framPage;
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCLabel ctlGroupControl;
		public fecherFoundation.FCPictureBox BillImage;
		public fecherFoundation.FCLabel CustomLabel_0;
		public fecherFoundation.FCFrame framUserText;
		public fecherFoundation.FCButton cmdUserDefinedTextOK;
		public fecherFoundation.FCRichTextBox rtbUserText;
		public fecherFoundation.FCFrame framLoadDelete;
		public fecherFoundation.FCButton cmdLoadDeleteCancel;
		public fecherFoundation.FCGrid GridLoadDelete;
		public fecherFoundation.FCLine RatioLine;
		//public AxvsElasticLightLib.AxvsElasticLight vsElasticLight1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddField;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteField;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuZoomIn;
		public fecherFoundation.FCToolStripMenuItem mnuZoomOut;
		public fecherFoundation.FCToolStripMenuItem mnuSepar7;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSample;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuImportFormat;
		public fecherFoundation.FCToolStripMenuItem mnuExportFormat;
		public fecherFoundation.FCToolStripMenuItem mnuSepar6;
		public fecherFoundation.FCToolStripMenuItem mnuPreferences;
		public fecherFoundation.FCToolStripMenuItem mnuFullCharFullLine;
		public fecherFoundation.FCToolStripMenuItem mnuMoveHalfCharHalfLine;
		public fecherFoundation.FCToolStripMenuItem mnuMoveQuarterCharQuarterLine;
		public fecherFoundation.FCToolStripMenuItem mnusepar5;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteFormat;
		public fecherFoundation.FCToolStripMenuItem mnuLoadFormat;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveAs;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuPop;
		public fecherFoundation.FCToolStripMenuItem mnuAlignLefts;
		public fecherFoundation.FCToolStripMenuItem mnuAlignRights;
		public fecherFoundation.FCToolStripMenuItem mnuAlignTops;
		public fecherFoundation.FCToolStripMenuItem mnuAlignBottoms;
		public fecherFoundation.FCToolStripMenuItem mnuPopSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuBringToFront;
		public fecherFoundation.FCToolStripMenuItem mnuSendToBack;
		public fecherFoundation.FCToolStripMenuItem mnuPopSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuCopyControls;
		public fecherFoundation.FCToolStripMenuItem mnuPasteControls;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomBill));
            this.GridCopy = new fecherFoundation.FCGrid();
            this.GridHighlighted = new fecherFoundation.FCGrid();
            this.framControlInfo = new fecherFoundation.FCFrame();
            this.GridControlTypes = new fecherFoundation.FCGrid();
            this.GridControlInfo = new fecherFoundation.FCGrid();
            this.cmdOKControlInfo = new fecherFoundation.FCButton();
            this.cmdOKControlInfoNew = new fecherFoundation.FCButton();
            this.imgControlImage = new fecherFoundation.FCPictureBox();
            this.GridFields = new fecherFoundation.FCGrid();
            this.GridBillSize = new fecherFoundation.FCGrid();
            this.GridDeleted = new fecherFoundation.FCGrid();
            this.framPage = new fecherFoundation.FCPanel();
            this.Picture1 = new fecherFoundation.FCPictureBox();
            this.ctlGroupControl = new fecherFoundation.FCLabel();
            this.BillImage = new fecherFoundation.FCPictureBox();
            this.CustomLabel_0 = new fecherFoundation.FCLabel();
            this.framUserText = new fecherFoundation.FCFrame();
            this.cmdUserDefinedTextOK = new fecherFoundation.FCButton();
            this.rtbUserText = new fecherFoundation.FCRichTextBox();
            this.framLoadDelete = new fecherFoundation.FCFrame();
            this.cmdLoadDeleteCancel = new fecherFoundation.FCButton();
            this.GridLoadDelete = new fecherFoundation.FCGrid();
            this.RatioLine = new fecherFoundation.FCLine();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuZoomIn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuZoomOut = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintSample = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImportFormat = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExportFormat = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPreferences = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFullCharFullLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMoveHalfCharHalfLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMoveQuarterCharQuarterLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteFormat = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLoadFormat = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddField = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteField = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar7 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar6 = new fecherFoundation.FCToolStripMenuItem();
            this.mnusepar5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveAs = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPop = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAlignLefts = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAlignRights = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAlignTops = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAlignBottoms = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPopSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBringToFront = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSendToBack = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPopSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCopyControls = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPasteControls = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdSaveAs = new fecherFoundation.FCButton();
            this.cmdAddField = new fecherFoundation.FCButton();
            this.cmdDeleteField = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCopy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridHighlighted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framControlInfo)).BeginInit();
            this.framControlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridControlTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControlInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKControlInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKControlInfoNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgControlImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridBillSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPage)).BeginInit();
            this.framPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            this.Picture1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BillImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framUserText)).BeginInit();
            this.framUserText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUserDefinedTextOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbUserText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framLoadDelete)).BeginInit();
            this.framLoadDelete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadDeleteCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLoadDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteField)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 602);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framLoadDelete);
            this.ClientArea.Controls.Add(this.framControlInfo);
            this.ClientArea.Controls.Add(this.GridBillSize);
            this.ClientArea.Controls.Add(this.framPage);
            this.ClientArea.Controls.Add(this.GridCopy);
            this.ClientArea.Controls.Add(this.GridHighlighted);
            this.ClientArea.Controls.Add(this.GridDeleted);
            this.ClientArea.Controls.Add(this.GridFields);
            this.ClientArea.Controls.Add(this.RatioLine);
            this.ClientArea.Controls.Add(this.framUserText);
            this.ClientArea.Size = new System.Drawing.Size(1078, 606);
            this.ClientArea.Controls.SetChildIndex(this.framUserText, 0);
            this.ClientArea.Controls.SetChildIndex(this.RatioLine, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridFields, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridDeleted, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridHighlighted, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridCopy, 0);
            this.ClientArea.Controls.SetChildIndex(this.framPage, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridBillSize, 0);
            this.ClientArea.Controls.SetChildIndex(this.framControlInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.framLoadDelete, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDeleteField);
            this.TopPanel.Controls.Add(this.cmdAddField);
            this.TopPanel.Controls.Add(this.cmdSaveAs);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSaveAs, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddField, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteField, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(125, 28);
            this.HeaderText.Text = "Custom Bill";
            // 
            // GridCopy
            // 
            this.GridCopy.Cols = 16;
            this.GridCopy.ColumnHeadersVisible = false;
            this.GridCopy.FixedCols = 0;
            this.GridCopy.FixedRows = 0;
            this.GridCopy.Location = new System.Drawing.Point(0, 37);
            this.GridCopy.Name = "GridCopy";
            this.GridCopy.RowHeadersVisible = false;
            this.GridCopy.Rows = 0;
            this.GridCopy.ShowFocusCell = false;
            this.GridCopy.Size = new System.Drawing.Size(9, 18);
            this.GridCopy.TabIndex = 20;
            this.GridCopy.Visible = false;
            // 
            // GridHighlighted
            // 
            this.GridHighlighted.Cols = 3;
            this.GridHighlighted.ColumnHeadersVisible = false;
            this.GridHighlighted.FixedCols = 0;
            this.GridHighlighted.FixedRows = 0;
            this.GridHighlighted.Location = new System.Drawing.Point(0, 20);
            this.GridHighlighted.Name = "GridHighlighted";
            this.GridHighlighted.RowHeadersVisible = false;
            this.GridHighlighted.Rows = 0;
            this.GridHighlighted.ShowFocusCell = false;
            this.GridHighlighted.Size = new System.Drawing.Size(2, 10);
            this.GridHighlighted.TabIndex = 19;
            this.GridHighlighted.Visible = false;
            // 
            // framControlInfo
            // 
            this.framControlInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framControlInfo.BackColor = System.Drawing.Color.White;
            this.framControlInfo.Controls.Add(this.GridControlTypes);
            this.framControlInfo.Controls.Add(this.GridControlInfo);
            this.framControlInfo.Controls.Add(this.cmdOKControlInfo);
            this.framControlInfo.Controls.Add(this.cmdOKControlInfoNew);
            this.framControlInfo.Controls.Add(this.imgControlImage);
            this.framControlInfo.Location = new System.Drawing.Point(30, 30);
            this.framControlInfo.Name = "framControlInfo";
            this.framControlInfo.Size = new System.Drawing.Size(1024, 569);
            this.framControlInfo.TabIndex = 8;
            this.framControlInfo.Visible = false;
            // 
            // GridControlTypes
            // 
            this.GridControlTypes.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridControlTypes.Cols = 10;
            this.GridControlTypes.ExtendLastCol = true;
            this.GridControlTypes.Location = new System.Drawing.Point(20, 30);
            this.GridControlTypes.Name = "GridControlTypes";
            this.GridControlTypes.Rows = 1;
            this.GridControlTypes.ShowFocusCell = false;
            this.GridControlTypes.Size = new System.Drawing.Size(988, 462);
            this.GridControlTypes.TabIndex = 13;
            this.GridControlTypes.Visible = false;
            this.GridControlTypes.Click += new System.EventHandler(this.GridControlTypes_ClickEvent);
            this.GridControlTypes.MouseMove += new Wisej.Web.MouseEventHandler(this.GridControlTypes_MouseMoveEvent);
            // 
            // GridControlInfo
            // 
            this.GridControlInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridControlInfo.ColumnHeadersVisible = false;
            this.GridControlInfo.ExtendLastCol = true;
            this.GridControlInfo.FixedRows = 0;
            this.GridControlInfo.Location = new System.Drawing.Point(20, 30);
            this.GridControlInfo.Name = "GridControlInfo";
            this.GridControlInfo.Rows = 6;
            this.GridControlInfo.ShowFocusCell = false;
            this.GridControlInfo.Size = new System.Drawing.Size(988, 462);
            this.GridControlInfo.TabIndex = 12;
            this.GridControlInfo.CellButtonClick += new System.EventHandler(this.GridControlInfo_CellButtonClick);
            this.GridControlInfo.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridControlInfo_BeforeEdit);
            this.GridControlInfo.CurrentCellChanged += new System.EventHandler(this.GridControlInfo_RowColChange);
            this.GridControlInfo.MouseMove += new Wisej.Web.MouseEventHandler(this.GridControlInfo_MouseMoveEvent);
            // 
            // cmdOKControlInfo
            // 
            this.cmdOKControlInfo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdOKControlInfo.AppearanceKey = "actionButton";
            this.cmdOKControlInfo.Location = new System.Drawing.Point(20, 510);
            this.cmdOKControlInfo.Name = "cmdOKControlInfo";
            this.cmdOKControlInfo.Size = new System.Drawing.Size(76, 40);
            this.cmdOKControlInfo.TabIndex = 9;
            this.cmdOKControlInfo.Text = "OK";
            this.cmdOKControlInfo.Click += new System.EventHandler(this.cmdOKControlInfo_Click);
            // 
            // cmdOKControlInfoNew
            // 
            this.cmdOKControlInfoNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdOKControlInfoNew.AppearanceKey = "actionButton";
            this.cmdOKControlInfoNew.Location = new System.Drawing.Point(20, 512);
            this.cmdOKControlInfoNew.Name = "cmdOKControlInfoNew";
            this.cmdOKControlInfoNew.Size = new System.Drawing.Size(76, 40);
            this.cmdOKControlInfoNew.TabIndex = 18;
            this.cmdOKControlInfoNew.Text = "OK";
            this.cmdOKControlInfoNew.Visible = false;
            this.cmdOKControlInfoNew.Click += new System.EventHandler(this.cmdOKControlInfoNew_Click);
            // 
            // imgControlImage
            // 
            this.imgControlImage.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgControlImage.FillColor = 16777215;
            this.imgControlImage.Image = ((System.Drawing.Image)(resources.GetObject("imgControlImage.Image")));
            this.imgControlImage.Location = new System.Drawing.Point(700, 12);
            this.imgControlImage.Name = "imgControlImage";
            this.imgControlImage.Size = new System.Drawing.Size(260, 130);
            this.imgControlImage.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgControlImage.Visible = false;
            // 
            // GridFields
            // 
            this.GridFields.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridFields.Cols = 10;
            this.GridFields.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridFields.FixedCols = 2;
            this.GridFields.FrozenCols = 1;
            this.GridFields.Location = new System.Drawing.Point(500, 30);
            this.GridFields.Name = "GridFields";
            this.GridFields.ReadOnly = false;
            this.GridFields.Rows = 1;
            this.GridFields.ShowFocusCell = false;
            this.GridFields.Size = new System.Drawing.Size(557, 303);
            this.GridFields.TabIndex = 1001;
            this.GridFields.CellButtonClick += new System.EventHandler(this.GridFields_CellButtonClick);
            this.GridFields.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridFields_ValidateEdit);
            this.GridFields.CurrentCellChanged += new System.EventHandler(this.GridFields_RowColChange);
            this.GridFields.Click += new System.EventHandler(this.GridFields_ClickEvent);
            this.GridFields.KeyDown += new Wisej.Web.KeyEventHandler(this.GridFields_KeyDownEvent);
            this.GridFields.KeyUp += new Wisej.Web.KeyEventHandler(this.GridFields_KeyUpEvent);
            // 
            // GridBillSize
            // 
            this.GridBillSize.ColumnHeadersVisible = false;
            this.GridBillSize.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridBillSize.FixedRows = 0;
            this.GridBillSize.Location = new System.Drawing.Point(30, 30);
            this.GridBillSize.Name = "GridBillSize";
            this.GridBillSize.ReadOnly = false;
            this.GridBillSize.Rows = 9;
            this.GridBillSize.ShowFocusCell = false;
            this.GridBillSize.Size = new System.Drawing.Size(450, 303);
            this.GridBillSize.TabIndex = 11;
            this.GridBillSize.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridBillSize_ChangeEdit);
            this.GridBillSize.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridBillSize_BeforeEdit);
            this.GridBillSize.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridBillSize_ValidateEdit);
            // 
            // GridDeleted
            // 
            this.GridDeleted.Cols = 1;
            this.GridDeleted.ColumnHeadersVisible = false;
            this.GridDeleted.FixedCols = 0;
            this.GridDeleted.FixedRows = 0;
            this.GridDeleted.Location = new System.Drawing.Point(0, 62);
            this.GridDeleted.Name = "GridDeleted";
            this.GridDeleted.RowHeadersVisible = false;
            this.GridDeleted.Rows = 0;
            this.GridDeleted.ShowFocusCell = false;
            this.GridDeleted.Size = new System.Drawing.Size(17, 10);
            this.GridDeleted.TabIndex = 10;
            this.GridDeleted.Visible = false;
            // 
            // framPage
            // 
            this.framPage.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framPage.AutoScroll = true;
            this.framPage.Controls.Add(this.Picture1);
            this.framPage.Location = new System.Drawing.Point(30, 353);
            this.framPage.Name = "framPage";
            this.framPage.Size = new System.Drawing.Size(1027, 249);
            this.framPage.TabIndex = 1;
            // 
            // Picture1
            // 
            this.Picture1.BackColor = System.Drawing.SystemColors.Window;
            this.Picture1.Controls.Add(this.ctlGroupControl);
            this.Picture1.Controls.Add(this.BillImage);
            this.Picture1.Controls.Add(this.CustomLabel_0);
            this.Picture1.FillColor = -2147483643;
            this.Picture1.Image = ((System.Drawing.Image)(resources.GetObject("Picture1.Image")));
            this.Picture1.Location = new System.Drawing.Point(0, 3);
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new System.Drawing.Size(994, 138);
            // 
            // ctlGroupControl
            // 
            this.ctlGroupControl.BorderStyle = 1;
            this.ctlGroupControl.Name = "ctlGroupControl";
            this.ctlGroupControl.Size = new System.Drawing.Size(1, 10);
            this.ctlGroupControl.TabIndex = 0;
            this.ctlGroupControl.Visible = false;
            // 
            // BillImage
            // 
            this.BillImage.BorderStyle = Wisej.Web.BorderStyle.None;
            this.BillImage.FillColor = -2147483643;
            this.BillImage.Image = ((System.Drawing.Image)(resources.GetObject("BillImage.Image")));
            this.BillImage.Name = "BillImage";
            this.BillImage.Size = new System.Drawing.Size(33, 43);
            this.BillImage.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.BillImage.MouseDown += new Wisej.Web.MouseEventHandler(this.BillImage_MouseDown);
            this.BillImage.MouseMove += new Wisej.Web.MouseEventHandler(this.BillImage_MouseMove);
            this.BillImage.MouseUp += new Wisej.Web.MouseEventHandler(this.BillImage_MouseUp);
            // 
            // CustomLabel_0
            // 
            this.CustomLabel_0.AllowDrag = true;
            this.CustomLabel_0.AllowDrop = true;
            this.CustomLabel_0.BackColor = System.Drawing.Color.Transparent;
            this.CustomLabel_0.BorderStyle = 1;
            this.CustomLabel_0.Movable = true;
            this.CustomLabel_0.Name = "CustomLabel_0";
            this.CustomLabel_0.Size = new System.Drawing.Size(58, 18);
            this.CustomLabel_0.TabIndex = 17;
            this.CustomLabel_0.Visible = false;
            this.CustomLabel_0.MouseDown += new Wisej.Web.MouseEventHandler(this.CustomLabel_MouseDown);
            this.CustomLabel_0.MouseUp += new Wisej.Web.MouseEventHandler(this.CustomLabel_0_MouseUp);
            // 
            // framUserText
            // 
            this.framUserText.BackColor = System.Drawing.Color.White;
            this.framUserText.Controls.Add(this.cmdUserDefinedTextOK);
            this.framUserText.Controls.Add(this.rtbUserText);
            this.framUserText.Location = new System.Drawing.Point(30, 30);
            this.framUserText.Name = "framUserText";
            this.framUserText.Size = new System.Drawing.Size(583, 468);
            this.framUserText.TabIndex = 5;
            this.framUserText.Text = "User Defined Text";
            this.framUserText.Visible = false;
            // 
            // cmdUserDefinedTextOK
            // 
            this.cmdUserDefinedTextOK.AppearanceKey = "actionButton";
            this.cmdUserDefinedTextOK.Location = new System.Drawing.Point(254, 411);
            this.cmdUserDefinedTextOK.Name = "cmdUserDefinedTextOK";
            this.cmdUserDefinedTextOK.Size = new System.Drawing.Size(75, 40);
            this.cmdUserDefinedTextOK.TabIndex = 7;
            this.cmdUserDefinedTextOK.Text = "OK";
            this.cmdUserDefinedTextOK.Click += new System.EventHandler(this.cmdUserDefinedTextOK_Click);
            // 
            // rtbUserText
            // 
            this.rtbUserText.Location = new System.Drawing.Point(20, 30);
            this.rtbUserText.Name = "rtbUserText";
            this.rtbUserText.Size = new System.Drawing.Size(551, 368);
            this.rtbUserText.TabIndex = 6;
            // 
            // framLoadDelete
            // 
            this.framLoadDelete.BackColor = System.Drawing.Color.White;
            this.framLoadDelete.Controls.Add(this.cmdLoadDeleteCancel);
            this.framLoadDelete.Controls.Add(this.GridLoadDelete);
            this.framLoadDelete.Location = new System.Drawing.Point(34, 30);
            this.framLoadDelete.Name = "framLoadDelete";
            this.framLoadDelete.Size = new System.Drawing.Size(1023, 317);
            this.framLoadDelete.TabIndex = 14;
            this.framLoadDelete.Visible = false;
            // 
            // cmdLoadDeleteCancel
            // 
            this.cmdLoadDeleteCancel.AppearanceKey = "actionButton";
            this.cmdLoadDeleteCancel.Location = new System.Drawing.Point(480, 257);
            this.cmdLoadDeleteCancel.Name = "cmdLoadDeleteCancel";
            this.cmdLoadDeleteCancel.Size = new System.Drawing.Size(63, 40);
            this.cmdLoadDeleteCancel.TabIndex = 16;
            this.cmdLoadDeleteCancel.Text = "Cancel";
            this.cmdLoadDeleteCancel.Click += new System.EventHandler(this.cmdLoadDeleteCancel_Click);
            // 
            // GridLoadDelete
            // 
            this.GridLoadDelete.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridLoadDelete.Location = new System.Drawing.Point(20, 20);
            this.GridLoadDelete.Name = "GridLoadDelete";
            this.GridLoadDelete.ShowFocusCell = false;
            this.GridLoadDelete.Size = new System.Drawing.Size(985, 220);
            this.GridLoadDelete.TabIndex = 15;
            this.GridLoadDelete.CurrentCellChanged += new System.EventHandler(this.GridLoadDelete_RowColChange);
            this.GridLoadDelete.DoubleClick += new System.EventHandler(this.GridLoadDelete_DblClick);
            // 
            // RatioLine
            // 
            this.RatioLine.Location = new System.Drawing.Point(0, -29);
            this.RatioLine.Name = "RatioLine";
            this.RatioLine.Size = new System.Drawing.Size(1018, 1);
            this.RatioLine.Visible = false;
            this.RatioLine.X2 = 1440F;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuZoomIn,
            this.mnuZoomOut,
            this.mnuPrintSample,
            this.mnuImportFormat,
            this.mnuExportFormat,
            this.mnuPreferences,
            this.mnuDeleteFormat,
            this.mnuLoadFormat,
            this.mnuNew});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuZoomIn
            // 
            this.mnuZoomIn.Index = 0;
            this.mnuZoomIn.Name = "mnuZoomIn";
            this.mnuZoomIn.Shortcut = Wisej.Web.Shortcut.CtrlI;
            this.mnuZoomIn.Text = "Zoom In";
            this.mnuZoomIn.Click += new System.EventHandler(this.mnuZoomIn_Click);
            // 
            // mnuZoomOut
            // 
            this.mnuZoomOut.Index = 1;
            this.mnuZoomOut.Name = "mnuZoomOut";
            this.mnuZoomOut.Shortcut = Wisej.Web.Shortcut.CtrlO;
            this.mnuZoomOut.Text = "Zoom Out";
            this.mnuZoomOut.Click += new System.EventHandler(this.mnuZoomOut_Click);
            // 
            // mnuPrintSample
            // 
            this.mnuPrintSample.Index = 2;
            this.mnuPrintSample.Name = "mnuPrintSample";
            this.mnuPrintSample.Text = "Print Sample";
            this.mnuPrintSample.Click += new System.EventHandler(this.mnuPrintSample_Click);
            // 
            // mnuImportFormat
            // 
            this.mnuImportFormat.Index = 3;
            this.mnuImportFormat.Name = "mnuImportFormat";
            this.mnuImportFormat.Text = "Import Format";
            this.mnuImportFormat.Click += new System.EventHandler(this.mnuImportFormat_Click);
            // 
            // mnuExportFormat
            // 
            this.mnuExportFormat.Index = 4;
            this.mnuExportFormat.Name = "mnuExportFormat";
            this.mnuExportFormat.Text = "Export Format";
            this.mnuExportFormat.Click += new System.EventHandler(this.mnuExportFormat_Click);
            // 
            // mnuPreferences
            // 
            this.mnuPreferences.Index = 5;
            this.mnuPreferences.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFullCharFullLine,
            this.mnuMoveHalfCharHalfLine,
            this.mnuMoveQuarterCharQuarterLine});
            this.mnuPreferences.Name = "mnuPreferences";
            this.mnuPreferences.Text = "Preferences";
            // 
            // mnuFullCharFullLine
            // 
            this.mnuFullCharFullLine.Checked = true;
            this.mnuFullCharFullLine.Index = 0;
            this.mnuFullCharFullLine.Name = "mnuFullCharFullLine";
            this.mnuFullCharFullLine.Text = "Move Full Character & Full Line";
            this.mnuFullCharFullLine.Click += new System.EventHandler(this.mnuFullCharFullLine_Click);
            // 
            // mnuMoveHalfCharHalfLine
            // 
            this.mnuMoveHalfCharHalfLine.Index = 1;
            this.mnuMoveHalfCharHalfLine.Name = "mnuMoveHalfCharHalfLine";
            this.mnuMoveHalfCharHalfLine.Text = "Move Half Character & Half Line";
            this.mnuMoveHalfCharHalfLine.Click += new System.EventHandler(this.mnuMoveHalfCharHalfLine_Click);
            // 
            // mnuMoveQuarterCharQuarterLine
            // 
            this.mnuMoveQuarterCharQuarterLine.Index = 2;
            this.mnuMoveQuarterCharQuarterLine.Name = "mnuMoveQuarterCharQuarterLine";
            this.mnuMoveQuarterCharQuarterLine.Text = "Move Quarter Character & Quarter Line";
            this.mnuMoveQuarterCharQuarterLine.Click += new System.EventHandler(this.mnuMoveQuarterCharQuarterLine_Click);
            // 
            // mnuDeleteFormat
            // 
            this.mnuDeleteFormat.Index = 6;
            this.mnuDeleteFormat.Name = "mnuDeleteFormat";
            this.mnuDeleteFormat.Text = "Delete Format";
            this.mnuDeleteFormat.Click += new System.EventHandler(this.mnuDeleteFormat_Click);
            // 
            // mnuLoadFormat
            // 
            this.mnuLoadFormat.Index = 7;
            this.mnuLoadFormat.Name = "mnuLoadFormat";
            this.mnuLoadFormat.Text = "Load Format";
            this.mnuLoadFormat.Click += new System.EventHandler(this.mnuLoadFormat_Click);
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 8;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New Format";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuAddField
            // 
            this.mnuAddField.Index = -1;
            this.mnuAddField.Name = "mnuAddField";
            this.mnuAddField.Shortcut = Wisej.Web.Shortcut.ShiftInsert;
            this.mnuAddField.Text = "Add Field";
            this.mnuAddField.Click += new System.EventHandler(this.mnuAddField_Click);
            // 
            // mnuDeleteField
            // 
            this.mnuDeleteField.Index = -1;
            this.mnuDeleteField.Name = "mnuDeleteField";
            this.mnuDeleteField.Shortcut = Wisej.Web.Shortcut.ShiftDelete;
            this.mnuDeleteField.Text = "Delete Field";
            this.mnuDeleteField.Click += new System.EventHandler(this.mnuDeleteField_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = -1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuSepar7
            // 
            this.mnuSepar7.Index = -1;
            this.mnuSepar7.Name = "mnuSepar7";
            this.mnuSepar7.Text = "-";
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = -1;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuSepar6
            // 
            this.mnuSepar6.Index = -1;
            this.mnuSepar6.Name = "mnuSepar6";
            this.mnuSepar6.Text = "-";
            // 
            // mnusepar5
            // 
            this.mnusepar5.Index = -1;
            this.mnusepar5.Name = "mnusepar5";
            this.mnusepar5.Text = "-";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveAs
            // 
            this.mnuSaveAs.Index = -1;
            this.mnuSaveAs.Name = "mnuSaveAs";
            this.mnuSaveAs.Text = "Save As";
            this.mnuSaveAs.Click += new System.EventHandler(this.mnuSaveAs_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuPop
            // 
            this.mnuPop.Index = -1;
            this.mnuPop.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAlignLefts,
            this.mnuAlignRights,
            this.mnuAlignTops,
            this.mnuAlignBottoms,
            this.mnuPopSepar1,
            this.mnuBringToFront,
            this.mnuSendToBack,
            this.mnuPopSepar2,
            this.mnuCopyControls,
            this.mnuPasteControls});
            this.mnuPop.Name = "mnuPop";
            this.mnuPop.Text = "mnuPop";
            this.mnuPop.Visible = false;
            // 
            // mnuAlignLefts
            // 
            this.mnuAlignLefts.Index = 0;
            this.mnuAlignLefts.Name = "mnuAlignLefts";
            this.mnuAlignLefts.Text = "Align Lefts";
            this.mnuAlignLefts.Click += new System.EventHandler(this.mnuAlignLefts_Click);
            // 
            // mnuAlignRights
            // 
            this.mnuAlignRights.Index = 1;
            this.mnuAlignRights.Name = "mnuAlignRights";
            this.mnuAlignRights.Text = "Align Rights";
            this.mnuAlignRights.Click += new System.EventHandler(this.mnuAlignRights_Click);
            // 
            // mnuAlignTops
            // 
            this.mnuAlignTops.Index = 2;
            this.mnuAlignTops.Name = "mnuAlignTops";
            this.mnuAlignTops.Text = "Align Tops";
            this.mnuAlignTops.Click += new System.EventHandler(this.mnuAlignTops_Click);
            // 
            // mnuAlignBottoms
            // 
            this.mnuAlignBottoms.Index = 3;
            this.mnuAlignBottoms.Name = "mnuAlignBottoms";
            this.mnuAlignBottoms.Text = "Align Bottoms";
            this.mnuAlignBottoms.Click += new System.EventHandler(this.mnuAlignBottoms_Click);
            // 
            // mnuPopSepar1
            // 
            this.mnuPopSepar1.Index = 4;
            this.mnuPopSepar1.Name = "mnuPopSepar1";
            this.mnuPopSepar1.Text = "-";
            // 
            // mnuBringToFront
            // 
            this.mnuBringToFront.Index = 5;
            this.mnuBringToFront.Name = "mnuBringToFront";
            this.mnuBringToFront.Text = "Bring to Front";
            this.mnuBringToFront.Click += new System.EventHandler(this.mnuBringToFront_Click);
            // 
            // mnuSendToBack
            // 
            this.mnuSendToBack.Index = 6;
            this.mnuSendToBack.Name = "mnuSendToBack";
            this.mnuSendToBack.Text = "Send to Back";
            this.mnuSendToBack.Click += new System.EventHandler(this.mnuSendToBack_Click);
            // 
            // mnuPopSepar2
            // 
            this.mnuPopSepar2.Index = 7;
            this.mnuPopSepar2.Name = "mnuPopSepar2";
            this.mnuPopSepar2.Text = "-";
            // 
            // mnuCopyControls
            // 
            this.mnuCopyControls.Index = 8;
            this.mnuCopyControls.Name = "mnuCopyControls";
            this.mnuCopyControls.Text = "Copy";
            this.mnuCopyControls.Click += new System.EventHandler(this.mnuCopyControls_Click);
            // 
            // mnuPasteControls
            // 
            this.mnuPasteControls.Enabled = false;
            this.mnuPasteControls.Index = 9;
            this.mnuPasteControls.Name = "mnuPasteControls";
            this.mnuPasteControls.Text = "Paste";
            this.mnuPasteControls.Click += new System.EventHandler(this.mnuPasteControls_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(458, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(82, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdSaveAs
            // 
            this.cmdSaveAs.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSaveAs.Location = new System.Drawing.Point(1030, 29);
            this.cmdSaveAs.Name = "cmdSaveAs";
            this.cmdSaveAs.Size = new System.Drawing.Size(64, 24);
            this.cmdSaveAs.TabIndex = 1;
            this.cmdSaveAs.Text = "Save As";
            this.cmdSaveAs.Click += new System.EventHandler(this.cmdSaveAs_Click);
            // 
            // cmdAddField
            // 
            this.cmdAddField.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddField.Location = new System.Drawing.Point(855, 29);
            this.cmdAddField.Name = "cmdAddField";
            this.cmdAddField.Shortcut = Wisej.Web.Shortcut.ShiftInsert;
            this.cmdAddField.Size = new System.Drawing.Size(74, 24);
            this.cmdAddField.TabIndex = 2;
            this.cmdAddField.Text = "Add Field";
            this.cmdAddField.Click += new System.EventHandler(this.cmdAddField_Click);
            // 
            // cmdDeleteField
            // 
            this.cmdDeleteField.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteField.Location = new System.Drawing.Point(935, 29);
            this.cmdDeleteField.Name = "cmdDeleteField";
            this.cmdDeleteField.Shortcut = Wisej.Web.Shortcut.ShiftDelete;
            this.cmdDeleteField.Size = new System.Drawing.Size(89, 24);
            this.cmdDeleteField.TabIndex = 3;
            this.cmdDeleteField.Text = "Delete Field";
            this.cmdDeleteField.Click += new System.EventHandler(this.cmdDeleteField_Click);
            // 
            // frmCustomBill
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmCustomBill";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.Load += new System.EventHandler(this.frmCustomBill_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomBill_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCopy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridHighlighted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framControlInfo)).EndInit();
            this.framControlInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridControlTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControlInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKControlInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKControlInfoNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgControlImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridBillSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPage)).EndInit();
            this.framPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            this.Picture1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BillImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framUserText)).EndInit();
            this.framUserText.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdUserDefinedTextOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbUserText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framLoadDelete)).EndInit();
            this.framLoadDelete.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadDeleteCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLoadDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteField)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdSaveAs;
		private FCButton cmdAddField;
		private FCButton cmdDeleteField;
	}
}