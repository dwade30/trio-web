//Fecher vbPorter - Version 1.0.0.32

namespace Global
{
    public class modInterestedParties
    {

        //=========================================================

        /// <summary>
        /// Public Function AddMultiOwnerTable(strDatabase As String) As Boolean
        /// </summary>
        /// <summary>
        /// Dim rsSave As New clsdrwrapper
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// On Error GoTo ErrorHandler
        /// </summary>
        /// <summary>
        /// AddMultiOwnerTable = False
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// If Not rsSave.UpdateDatabaseTable("Owners", strDatabase) Then
        /// </summary>
        /// <summary>
        /// If rsSave.CreateNewDatabaseTable("Owners", strDatabase) Then
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("id", dbLong)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAttribute("id", dbAutoIncrField)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("Module", dbText, 25)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAllowZeroLength("Module", True)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("Account", dbLong)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("Name", dbText, 255)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAllowZeroLength("Name", True)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("Address1", dbText, 255)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAllowZeroLength("Address1", True)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("Address2", dbText, 255)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAllowZeroLength("Address2", True)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("City", dbText, 255)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAllowZeroLength("City", True)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("State", dbText, 255)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAllowZeroLength("State", True)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("Zip", dbText, 255)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAllowZeroLength("Zip", True)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("Zip4", dbText, 255)
        /// </summary>
        /// <summary>
        /// Call rsSave.SetFieldAllowZeroLength("Zip4", True)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("BillYear", dbLong)
        /// </summary>
        /// <summary>
        /// Call rsSave.AddTableField("AssocID", dbLong) 'in RE is a previous owner record, in CL could point to a bill record, in PP means nothing
        /// </summary>
        /// <summary>
        /// in re billyear is unused, associd is 0 if it is current.  An associd > 0 belongs to a previous owner record not a current record
        /// </summary>
        /// <summary>
        /// rsSave.UpdateTableCreation
        /// </summary>
        /// <summary>
        /// Else
        /// </summary>
        /// <summary>
        /// Exit Function
        /// </summary>
        /// <summary>
        /// End If
        /// </summary>
        /// <summary>
        /// End If
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// AddMultiOwnerTable = True
        /// </summary>
        /// <summary>
        /// Exit Function
        /// </summary>
        /// <summary>
        /// ErrorHandler:
        /// </summary>
        /// <summary>
        /// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In AddMultiOwnerTable", vbCritical, "Error"
        /// </summary>
        /// <summary>
        /// End Function
        /// </summary>

    }
}