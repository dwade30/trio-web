﻿namespace Global
{
	/// <summary>
	/// Summary description for srptUTPerDiemList.
	/// </summary>
	partial class srptUTPerDiemList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptUTPerDiemList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblPerDiemHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldPerDiem1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPerDiem1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldPerDiemTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPerDiemTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiemHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPerDiem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPerDiemTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiemTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPerDiem1,
				this.lblPerDiem1
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblPerDiemHeader,
				this.Line3
			});
			this.GroupHeader1.Height = 0.2083333F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line4,
				this.fldPerDiemTotal,
				this.lblPerDiemTotal
			});
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblPerDiemHeader
			// 
			this.lblPerDiemHeader.Height = 0.1875F;
			this.lblPerDiemHeader.HyperLink = null;
			this.lblPerDiemHeader.Left = 0.375F;
			this.lblPerDiemHeader.Name = "lblPerDiemHeader";
			this.lblPerDiemHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblPerDiemHeader.Text = "Per Diem";
			this.lblPerDiemHeader.Top = 0F;
			this.lblPerDiemHeader.Width = 2.625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.1875F;
			this.Line3.Width = 2.625F;
			this.Line3.X1 = 0.375F;
			this.Line3.X2 = 3F;
			this.Line3.Y1 = 0.1875F;
			this.Line3.Y2 = 0.1875F;
			// 
			// fldPerDiem1
			// 
			this.fldPerDiem1.Height = 0.1875F;
			this.fldPerDiem1.Left = 2F;
			this.fldPerDiem1.Name = "fldPerDiem1";
			this.fldPerDiem1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldPerDiem1.Text = null;
			this.fldPerDiem1.Top = 0F;
			this.fldPerDiem1.Width = 1F;
			// 
			// lblPerDiem1
			// 
			this.lblPerDiem1.Height = 0.1875F;
			this.lblPerDiem1.HyperLink = null;
			this.lblPerDiem1.Left = 0.375F;
			this.lblPerDiem1.Name = "lblPerDiem1";
			this.lblPerDiem1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblPerDiem1.Text = null;
			this.lblPerDiem1.Top = 0F;
			this.lblPerDiem1.Width = 1.625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.375F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0F;
			this.Line4.Width = 2.625F;
			this.Line4.X1 = 0.375F;
			this.Line4.X2 = 3F;
			this.Line4.Y1 = 0F;
			this.Line4.Y2 = 0F;
			// 
			// fldPerDiemTotal
			// 
			this.fldPerDiemTotal.Height = 0.1875F;
			this.fldPerDiemTotal.Left = 2F;
			this.fldPerDiemTotal.Name = "fldPerDiemTotal";
			this.fldPerDiemTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.fldPerDiemTotal.Text = null;
			this.fldPerDiemTotal.Top = 0.0625F;
			this.fldPerDiemTotal.Width = 1F;
			// 
			// lblPerDiemTotal
			// 
			this.lblPerDiemTotal.Height = 0.1875F;
			this.lblPerDiemTotal.HyperLink = null;
			this.lblPerDiemTotal.Left = 0.375F;
			this.lblPerDiemTotal.Name = "lblPerDiemTotal";
			this.lblPerDiemTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblPerDiemTotal.Text = "Total";
			this.lblPerDiemTotal.Top = 0.0625F;
			this.lblPerDiemTotal.Width = 1.625F;
			// 
			// srptUTPerDiemList
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiemHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPerDiem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPerDiemTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiemTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPerDiem1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiem1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiemHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPerDiemTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiemTotal;
	}
}
