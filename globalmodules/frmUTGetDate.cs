//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using Global;
using fecherFoundation;

namespace TWUT0000
{
    /// <summary>
    /// Summary description for frmUTGetDate.
    /// </summary>
    public partial class frmUTGetDate : BaseForm
    {

        public frmUTGetDate()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null )
                _InstancePtr = this;
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmUTGetDate InstancePtr
        {
            get
            {
                if (_InstancePtr == null) // || _InstancePtr.IsDisposed
                    _InstancePtr = new frmUTGetDate();
                return _InstancePtr;
            }
        }
        protected static frmUTGetDate _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>



        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               05/11/2005              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               05/11/2005              *
        // ********************************************************
        DateTime dtNew;

        public DateTime Init(string strText, DateTime dtEffectiveDate)
        {
            DateTime Init = System.DateTime.Now;
            // strText is the actual sting that will be shown above the date box
            // intType will tell me which place in my code called this form

            lblInstructions.Text = strText;
            dtNew = dtEffectiveDate;
            txtDate.Text = Strings.Format(dtEffectiveDate, "MM/dd/yyyy");
            this.Show(FormShowEnum.Modal);
            Init = dtNew;
            return Init;
        }

        private void cmdOk_Click(object sender, System.EventArgs e)
        {
            if (Information.IsDate(txtDate.Text))
            {
                dtNew = DateAndTime.DateValue(txtDate.Text);
            }
            this.Unload();
        }
        public void cmdOk_Click()
        {
            cmdOk_Click(cmdOk, new System.EventArgs());
        }


        private void cmdQuit_Click(object sender, System.EventArgs e)
        {
            this.Unload();
        }

        private void frmUTGetDate_Activated(object sender, System.EventArgs e)
        {
            if (modMain.FormExist(this)) return;
        }

        private void frmUTGetDate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

            switch (KeyCode)
            {

                case Keys.Return:
                    {
                        //not supported
                        //Support.SendKeys("{tab}", false);
                        break;
                    }
            } //end switch
        }

        private void frmUTGetDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

            if (KeyAscii == Keys.Escape) this.Unload();

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmUTGetDate_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmUTGetDate properties;
            //frmUTGetDate.ScaleWidth	= 3015;
            //frmUTGetDate.ScaleHeight	= 2310;
            //frmUTGetDate.LinkTopic	= "Form1";
            //frmUTGetDate.LockControls	= -1  'True;
            //End Unmaped Properties

            //redesigned
            //modGlobalFunctions.SetFixedSize_6(ref this, modGlobalConstants.TRIOWINDOWSIZESMALL);
            //modGlobalFunctions.SetTRIOColors(ref this);
        }

        private void txtDate_KeyDownEvent(object sender, Wisej.Web.KeyEventArgs e)
        {
            switch (e.KeyCode)
            {

                case Keys.Return:
                    {
                        // if the user hits the enter key on the textbox when they are done editing the date
                        cmdOk_Click();
                        break;
                    }
            } //end switch
        }

        private void txtDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(Strings.Left(txtDate.Text, 2)) > 12)
            {
                e.Cancel = true;
                MessageBox.Show("Please use the date format MM/dd/yyyy", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }
}