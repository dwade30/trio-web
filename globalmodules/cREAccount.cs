﻿//Fecher vbPorter - Version 1.0.0.59
using System;

namespace Global
{
	public class cREAccount
	{
		//=========================================================
		private int lngID;
		private int lngAccount;
		private int lngOwnerID;
		private int lngSecOwnerID;
		private bool boolTaxAcquired;
		private bool boolInBankruptcy;
		private bool boolRevocableTrust;
		private cParty pOwner = new cParty();
		private cParty pSecOwner = new cParty();
		private short[] intExemptCode = new short[3 + 1];
		private double[] dblExemptPct = new double[3 + 1];
		private double[] dblExemptVal = new double[3 + 1];
		private string strMapLot = "";
		private int lngTranCode;
		// vbPorter upgrade warning: dtSaleDate As DateTime	OnWrite(DateTime, short)
		private DateTime dtSaleDate;
		private short intSaleFinancing;
		private short intSaleValidity;
		private short intSaleVerified;
		private short intSaleType;
		private string strAccountID = "";
		private double dblSalePrice;
		private string strPrevOwner = "";

		public string PreviousOwner
		{
			get
			{
				string PreviousOwner = "";
				PreviousOwner = strPrevOwner;
				return PreviousOwner;
			}
			set
			{
				strPrevOwner = value;
			}
		}

		public double SalePrice
		{
			get
			{
				double SalePrice = 0;
				SalePrice = dblSalePrice;
				return SalePrice;
			}
			set
			{
				dblSalePrice = value;
			}
		}

		public string AccountID
		{
			get
			{
				string AccountID = "";
				AccountID = strAccountID;
				return AccountID;
			}
			set
			{
				strAccountID = value;
			}
		}

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public int Account
		{
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
			set
			{
				lngAccount = value;
			}
		}

		public int OwnerID
		{
			get
			{
				int OwnerID = 0;
				OwnerID = lngOwnerID;
				return OwnerID;
			}
			set
			{
				lngOwnerID = value;
			}
		}

		public int SecOwnerID
		{
			get
			{
				int SecOwnerID = 0;
				SecOwnerID = lngSecOwnerID;
				return SecOwnerID;
			}
			set
			{
				lngSecOwnerID = value;
			}
		}

		public bool TaxAcquired
		{
			get
			{
				bool TaxAcquired = false;
				TaxAcquired = boolTaxAcquired;
				return TaxAcquired;
			}
			set
			{
				boolTaxAcquired = value;
			}
		}

		public bool InBankruptcy
		{
			get
			{
				bool InBankruptcy = false;
				InBankruptcy = boolInBankruptcy;
				return InBankruptcy;
			}
			set
			{
				boolInBankruptcy = value;
			}
		}

		public bool RevocableTrust
		{
			get
			{
				bool RevocableTrust = false;
				RevocableTrust = boolRevocableTrust;
				return RevocableTrust;
			}
			set
			{
				boolRevocableTrust = value;
			}
		}

		public cParty OwnerParty
		{
			get
			{
				return pOwner;
			}
		}

		public cParty SecOwnerParty
		{
			get
			{
				return pSecOwner;
			}
		}

		public double Get_ExemptPct(short intIndex)
		{
			double ExemptPct = 0;
			ExemptPct = dblExemptPct[intIndex];
			return ExemptPct;
		}

		public void Set_ExemptPct(short intIndex, double dblPct)
		{
			dblExemptPct[intIndex] = dblPct;
		}

		public void Set_ExemptCode(short intIndex, short intCode)
		{
			intExemptCode[intIndex] = intCode;
		}

		public short Get_ExemptCode(short intIndex)
		{
			short ExemptCode = 0;
			ExemptCode = intExemptCode[intIndex];
			return ExemptCode;
		}

		public double Get_ExemptVal(short intIndex)
		{
			double ExemptVal = 0;
			ExemptVal = dblExemptVal[intIndex];
			return ExemptVal;
		}

		public void Set_ExemptVal(short intIndex, double dblValue)
		{
			dblExemptVal[intIndex] = dblValue;
		}

		public string MapLot
		{
			get
			{
				string MapLot = "";
				MapLot = strMapLot;
				return MapLot;
			}
			set
			{
				strMapLot = value;
			}
		}

		public int TranCode
		{
			get
			{
				int TranCode = 0;
				TranCode = lngTranCode;
				return TranCode;
			}
			set
			{
				lngTranCode = value;
			}
		}

		public DateTime SaleDate
		{
			get
			{
				DateTime SaleDate = System.DateTime.Now;
				SaleDate = dtSaleDate;
				return SaleDate;
			}
			set
			{
				dtSaleDate = value;
			}
		}

		public short SaleFinancing
		{
			get
			{
				short SaleFinancing = 0;
				SaleFinancing = intSaleFinancing;
				return SaleFinancing;
			}
			set
			{
				intSaleFinancing = value;
			}
		}

		public short SaleValidity
		{
			get
			{
				short SaleValidity = 0;
				SaleValidity = intSaleValidity;
				return SaleValidity;
			}
			set
			{
				intSaleValidity = value;
			}
		}

		public short SaleVerified
		{
			get
			{
				short SaleVerified = 0;
				SaleVerified = intSaleVerified;
				return SaleVerified;
			}
			set
			{
				intSaleVerified = value;
			}
		}

		public short SaleType
		{
			get
			{
				short SaleType = 0;
				SaleType = intSaleType;
				return SaleType;
			}
			set
			{
				intSaleType = value;
			}
		}

		public void Clear()
		{
			lngID = 0;
			intSaleType = 0;
			intSaleFinancing = 0;
			intSaleValidity = 0;
			intSaleVerified = 0;
			short x;
			for (x = 1; x <= 3; x++)
			{
				intExemptCode[x] = 0;
				dblExemptPct[x] = 100;
				dblExemptVal[x] = 0;
			}
			// x
			strMapLot = "";
			lngAccount = 0;
			lngOwnerID = 0;
			lngSecOwnerID = 0;
			lngTranCode = 0;
			boolInBankruptcy = false;
			boolTaxAcquired = false;
			boolRevocableTrust = false;
			dtSaleDate = DateTime.FromOADate(0);
			pOwner = new cParty();
			pSecOwner = new cParty();
			strAccountID = "";
			dblSalePrice = 0;
		}

		public cREAccount() : base()
		{
			Clear();
		}
	}
}
