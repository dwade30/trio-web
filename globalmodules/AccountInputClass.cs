﻿using fecherFoundation;
using fecherFoundation.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;
using Wisej.Web.Ext.CustomProperties;

namespace Global
{
    public class AccountInputClass
    {
        private string strAcctType = string.Empty;
        private bool blnValidate;
        private bool blnOnlyAllowDefaultType;
        private bool blnAllowSplits;
        // allow multitowns to split accounts
        private string strToolTipText = string.Empty;
        private AccountInputEditor accountInputEditor;
        private bool AccountInputValidating = false;

        public AccountInputEditor AccountInputEditor
        {
            get
            {
                return accountInputEditor;
            }
            set
            {
                if (object.Equals(value, null))
                {
                    RemoveEventhandlers();
                }
                accountInputEditor = value;
                AttachEventhandlers();
            }
        }

        public bool AllowSplits
        {
            set
            {
                blnAllowSplits = value;
            }
            get
            {
                bool AllowSplits = false;
                AllowSplits = blnAllowSplits;
                return AllowSplits;
            }
        }

        public string ToolTipText
        {
            set
            {
                strToolTipText = value;
                if (!object.Equals(AccountInputEditor, null))
                {
                    AccountInputEditor.ToolTipText = value;
                }
            }
            get
            {
                string ToolTipText = "";
                ToolTipText = strToolTipText;
                return ToolTipText;
            }
        }

        public bool Validation
        {
            set
            {
                blnValidate = value;
            }
            get
            {
                bool Validation = false;
                Validation = blnValidate;
                return Validation;
            }
        }

        public bool OnlyAllowDefaultType
        {
            set
            {
                blnOnlyAllowDefaultType = value;
            }
            get
            {
                bool OnlyAllowDefaultType = false;
                Validation = blnOnlyAllowDefaultType;
                return OnlyAllowDefaultType;
            }
        }

        public string DefaultAccountType
        {
            set
            {
                strAcctType = value;
            }
            get
            {
                string DefaultAccountType = "";
                DefaultAccountType = strAcctType;
                return DefaultAccountType;
            }
        }

        public AccountInputClass()
        {
            blnValidate = true;
        }

        private void AttachEventhandlers()
        {
            if (!object.Equals(AccountInputEditor, null))
            {
                if (modNewAccountBox.Statics.GSegmentSize == "")
                {
                    modNewAccountBox.GetFormats();
                }
                RemoveEventhandlers();
                InitializeInput();
                AccountInputEditor.KeyDown += new KeyEventHandler(AccountInputEditor_KeyDown);
                AccountInputEditor.Validating += new CancelEventHandler(this.AccountInputEditor_ValidateEdit);
                AccountInputEditor.GotFocus += new EventHandler(AccountInputEditor_GotFocus);
                AccountInputEditor.KeyUp += new KeyEventHandler(AccountInputEditor_KeyUpEdit);

            }
        }

        private void RemoveEventhandlers()
        {
            if (!object.Equals(AccountInputEditor, null))
            {
                AccountInputEditor.KeyDown -= new KeyEventHandler(AccountInputEditor_KeyDown);
                AccountInputEditor.Validating -= new CancelEventHandler(this.AccountInputEditor_ValidateEdit);
                AccountInputEditor.GotFocus -= new EventHandler(AccountInputEditor_GotFocus);
                AccountInputEditor.KeyUp -= new KeyEventHandler(AccountInputEditor_KeyUpEdit);
            }
        }

        private void InitializeInput()
        {
            AccountInputEditor.MaxLength = 23;
            AccountInputEditor.CharacterCasing = CharacterCasing.Upper;
            if (string.IsNullOrEmpty(ToolTipText))
            {
                AccountInputEditor.ToolTipText = "Hit F2 to get a valid accounts list.";
            }

            CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
            DynamicObject customClientData = new DynamicObject();
            customClientData["GRID7Light_Col"] = 1;
            customClientData["intAcctCol"] = -1;
            customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
            customClientData["gboolTownAccounts"] = true;
            customClientData["gboolSchoolAccounts"] = false;
            customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
            customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
            customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
            customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
            customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
            customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
            customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
            customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
            customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
            customProperties.SetCustomPropertiesValue(AccountInputEditor, customClientData);

            AccountInputEditor.ClientEvents.Clear();
            JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
            keyDownEvent.Event = "keydown";
            keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
            JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
            keyUpEvent.Event = "keyup";
            keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
            AccountInputEditor.ClientEvents.Add(keyDownEvent);
            AccountInputEditor.ClientEvents.Add(keyUpEvent);

            Application.Update(AccountInputEditor);
        }

        private void AccountInputEditor_KeyDown(object sender, KeyEventArgs e)
        {
            //attach to have the event marked as critical
        }

        private void AccountInputEditor_KeyUpEdit(object sender, KeyEventArgs e)
        {
            string strTemp;
            try
            {
                strTemp = AccountInputEditor.Text;
                
                modNewAccountBox.CheckKeyDownEditF2(AccountInputEditor, e.KeyCode, e.Shift ? 1 : 0, AccountInputEditor.SelectionStart, ref strTemp, AccountInputEditor.SelectionLength, blnOnlyAllowDefaultType);
                if (e.KeyCode == Keys.F2)
                {
                    Application.Update(AccountInputEditor);
                    AccountInputEditor.Text = strTemp;
                    AccountInputEditor.SelectionStart = 0;
                    AccountInputEditor.SelectionLength = 1;
                    AccountInputEditor.Call("focus");
                    AccountInputEditor.Focus();
                    Application.Update(AccountInputEditor);
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in AccountInputEditor_KeyUpEdit on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void AccountInputEditor_GotFocus(object sender, EventArgs e)
        {
            InitializeInput();
            string strTemp;
            try
            {
                strTemp = "";
                modNewAccountBox.SetAccountInputEditorFormat(AccountInputEditor, ref strTemp, strAcctType);
                if (strTemp != "" && AccountInputEditor.Text == "")
                {
                    AccountInputEditor.Text = strTemp;
                }
                AccountInputEditor.SelectionStart = 0;
                AccountInputEditor.SelectionLength = 1;
                if (modNewAccountBox.Statics.intMaxLength == 0)
                {
                    modNewAccountBox.Statics.intMaxLength = AccountInputEditor.Text.Length;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in AccountInputClass.AccountInputEditor_GotFocus on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void AccountInputEditor_ValidateEdit(object sender, CancelEventArgs e)
        {
            //FC:FINAL:SBE - #4292 - do not execute Validating event during previous Validating event
            if (AccountInputValidating)
            {
                return;
            }
            try
            {
                AccountInputValidating = true;
                e.Cancel = modNewAccountBox.CheckAccountValidate(AccountInputEditor, e.Cancel, blnAllowSplits);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in AccountInputEditor_ValidateEdit on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                AccountInputValidating = false;
            }
        }
    }
}
