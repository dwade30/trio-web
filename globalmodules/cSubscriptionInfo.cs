﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cSubscriptionInfo
	{
		//=========================================================
		// vbPorter upgrade warning: colSubscriptions As Collection	OnRead(cSubscriptionItem)
		private FCCollection colSubscriptions = new FCCollection();
		private string strName = "";
		// <summary>
		// Group or Town Name subscription is for
		// </summary>
		// <value></value>
		// <returns></returns>
		// <remarks></remarks>
		public string Name
		{
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
			set
			{
				strName = value;
			}
		}

		public string GetSubscriptionItemValue(string strName, string strGroup)
		{
			string GetSubscriptionItemValue = "";
			GetSubscriptionItemValue = "";
			if (!(colSubscriptions == null))
			{
				foreach (cSubscriptionItem tItem in colSubscriptions)
				{
					if (Strings.LCase(tItem.Name) == Strings.LCase(strName))
					{
						if (strGroup != "")
						{
							if (Strings.LCase(strGroup) == Strings.LCase(tItem.Group))
							{
								GetSubscriptionItemValue = tItem.ItemValue;
								return GetSubscriptionItemValue;
							}
						}
						else
						{
							GetSubscriptionItemValue = tItem.ItemValue;
							return GetSubscriptionItemValue;
						}
					}
				}
				// tItem
				// Return ""
			}
			else
			{
				// Return ""
			}
			return GetSubscriptionItemValue;
		}

		public cSubscriptionItem GetSubscriptionItem(string strName, string strGroup)
		{
			cSubscriptionItem GetSubscriptionItem = null;
			GetSubscriptionItem = null;
			if (!(colSubscriptions == null))
			{
				foreach (cSubscriptionItem tItem in colSubscriptions)
				{
					if (Strings.LCase(tItem.Name) == Strings.LCase(strName))
					{
						if (strGroup != "")
						{
							if (Strings.LCase(strGroup) == Strings.LCase(tItem.Group))
							{
								GetSubscriptionItem = tItem;
								return GetSubscriptionItem;
							}
						}
						else
						{
							GetSubscriptionItem = tItem;
							return GetSubscriptionItem;
						}
					}
				}
				// tItem
			}
			else
			{
			}
			return GetSubscriptionItem;
		}

		public cSubscriptionItem AddItem(string strName, string strValue, string strGroup, string strParentGroup)
		{
			cSubscriptionItem AddItem = null;
			cSubscriptionItem tItem = null;
			if (strName != "")
			{
				tItem = GetSubscriptionItem(strName, strGroup);
				bool boolNew = false;
				if (tItem == null)
				{
					tItem = new cSubscriptionItem();
					tItem.Name = strName;
					boolNew = true;
				}
				tItem.ItemValue = strValue;
				if (strGroup != "")
				{
					tItem.Group = strGroup;
					tItem.ParentGroup = strParentGroup;
				}
				if (boolNew)
				{
					colSubscriptions.Add(tItem);
				}
			}
			AddItem = tItem;
			return AddItem;
		}

		public void RemoveItem(string strName, string strGroup)
		{
			if (!(colSubscriptions == null))
			{
				// vbPorter upgrade warning: tItem As cSubscriptionItem	OnWrite(Collection)
				cSubscriptionItem tItem;
				int x;
				for (x = 1; x <= colSubscriptions.Count; x++)
				{
					tItem = colSubscriptions[x];
					if (Strings.LCase(tItem.Name) == Strings.LCase(strName))
					{
						if (strGroup != "")
						{
							if (Strings.LCase(strGroup) == Strings.LCase(tItem.Group))
							{
								colSubscriptions.Remove(x);
							}
						}
						else
						{
							colSubscriptions.Remove(x);
						}
					}
				}
				// x
			}
		}

		public FCCollection Items
		{
			get
			{
				return colSubscriptions;
			}
		}

		public cSubscriptionInfo() : base()
		{
			strName = "";
		}
	}
}
