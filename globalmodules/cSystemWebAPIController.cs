﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using System.Xml;
using TWGNENTY;
using fecherFoundation;
using TWSharedLibrary;

namespace Global
{
	public class cSystemWebAPIController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngError, string strError)
		{
			strLastError = strError;
			lngLastError = lngError;
		}

		public bool UpdateSubscriptionsFromAPI(string strMuni)
		{
			bool UpdateSubscriptionsFromAPI = false;
			bool boolReturn = false;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				cTRIOWebAPI wAPI = new cTRIOWebAPI();
				//if (StaticSettings.gGlobalSettings.IsHarrisStaffComputer)
				//{
				//	wAPI.BaseUrl = "ban-dev-p01";
				//}
				string strSub;
				string strXML = "";
				strSub = wAPI.GetSubscription(strMuni);
                lngLastError = wAPI.LastErrorNumber;
                strLastError = wAPI.LastErrorMessage;
				//Application.DoEvents();
				if (!(strSub == ""))
				{
					boolReturn = UpdateSubscriptionsFromXML(strSub);
					// , strMuni)
				}
				
				return boolReturn;
			}
			catch
			{
				// ErrorHandler:
				lngLastError = Information.Err().Number;
				strLastError = Information.Err().Description;
			}
			return UpdateSubscriptionsFromAPI;
		}
		// Public Function UpdateSubscriptionsFromXML(ByVal strXML As String, ByVal strMuni) As Boolean
		// Dim boolReturn As Boolean
		// On Error GoTo ErrorHandler
		// ClearErrors
		// Dim rsLoad As New clsDRWrapper
		// If Not strMuni = "" Then
		// Dim tSub As cSubscriptionInfo
		// Dim si As New SubscriptionInfo
		// Dim sfc As New cSubscriptionFileController
		// Dim tDoc As New MSXML2.DOMDocument
		// If tDoc.LoadXML(strXML) Then
		// Set tSub = sfc.LoadSubscriptionFromXML(tDoc, strMuni)
		// If Not tSub Is Nothing Then
		// tSub.Name = strMuni
		// Dim ssc As New SubscriptionSQLController
		// Dim tempItem As cSubscriptionItem
		// For Each tempItem In tSub.Items
		// Call si.AddItem(tempItem.Name, tempItem.ItemValue, tempItem.Group, tempItem.ParentGroup)
		// Next
		// boolReturn = ssc.SaveSubscriptions(si, "", rsLoad.MegaGroup)
		// If boolReturn Then
		// now update the check digits
		// Set si = New SubscriptionInfo
		// si.Name = strMuni
		// Set si = ssc.LoadSubscriptions("", rsLoad.MegaGroup)
		// Dim lngCheckDigits As Long
		// lngCheckDigits = ssc.CalculateCheckDigits(si)
		// lngCheckDigits = ssc.CalculateCheckDigits(si)
		// If lngCheckDigits > 0 Then
		// Call rsLoad.OpenRecordset("select * from modules", "SystemSettings")
		// rsLoad.Edit
		// rsLoad.Fields("CheckDigits") = lngCheckDigits
		// rsLoad.Update
		// End If
		// Else
		// strLastError = "Unable to save expiration information"
		// lngLastError = -1
		// End If
		// End If
		// End If
		// End If
		// UpdateSubscriptionsFromXML = boolReturn
		// Exit Function
		// ErrorHandler:
		// strLastError = Err.Description
		// lngLastError = Err.Number
		// End Function
		public bool UpdateSubscriptionsFromXML(string strXML)
		{
			bool UpdateSubscriptionsFromXML = false;
			bool boolReturn = false;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strMuni = "";
				rsLoad.OpenRecordset("select * from globalvariables", "SystemSettings");
				if (!rsLoad.EndOfFile())
				{
					strMuni = FCConvert.ToString(rsLoad.Get_Fields_String("MuniName"));
				}
				if (!(strMuni == ""))
				{
					cSubscriptionInfo tSub = new cSubscriptionInfo();
					cSubscriptionFileController sfc = new cSubscriptionFileController();
					XmlDocument tDoc = new XmlDocument();
					// tDoc = XDocument.Parse(strDecrypted)
					tDoc.LoadXml(strXML);
					{
						tSub = sfc.LoadSubscriptionFromXML(ref tDoc, strMuni);
						//Application.DoEvents();
						if (!(tSub == null))
						{
							tSub.Name = strMuni;
							cSubscriptionSQLController ssc = new cSubscriptionSQLController();
							boolReturn = ssc.SaveSubscriptions(ref tSub, "", strMuni);
							//Application.DoEvents();
							if (boolReturn)
							{
								// now update the check digits
								tSub = new cSubscriptionInfo();
								tSub.Name = strMuni;
								tSub = ssc.LoadSubscriptions("", strMuni);
								int lngCheckDigits = 0;
								// lngCheckDigits = ssc.CalculateCheckDigits(si)
								// lngCheckDigits = CalculateCheckDigits03(False, False)
								lngCheckDigits = ssc.CalculateCheckDigits(ref tSub);
								if (lngCheckDigits > 0)
								{
									rsLoad.OpenRecordset("select * from modules", "SystemSettings");
									rsLoad.Edit();
									rsLoad.Set_Fields("CheckDigits", lngCheckDigits);
									rsLoad.Update();
								}
							}
							else
							{
								strLastError = "Unable to save expiration information";
								lngLastError = -1;
							}
						}
						// End If
						// End If
					}
				}
				UpdateSubscriptionsFromXML = boolReturn;
				return UpdateSubscriptionsFromXML;
			}
			catch
			{
				// ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return UpdateSubscriptionsFromXML;
		}
	}
}
