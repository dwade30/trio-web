﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	public class modAccountTitle
	{
		public static void DetermineAccountTitle(string x, ref FCLabel Y)
		{
			Y.Text = ReturnAccountDescription(x);
		}

		public static string GetDepartmentName(string Department, bool blnLongDescription = false)
		{
			string GetDepartmentName = "";
			clsDRWrapper rsName = new clsDRWrapper();
			if (Statics.ExpDivFlag)
			{
				rsName.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Department + "'", "TWBD0000.vb1");
			}
			else
			{
				rsName.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Department + "' AND Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(Statics.Exp, 3, 2))) + "'", "TWBD0000.vb1");
			}
			if (rsName.EndOfFile() != true && rsName.BeginningOfFile() != true)
			{
				if (!blnLongDescription)
				{
					GetDepartmentName = FCConvert.ToString(rsName.Get_Fields_String("ShortDescription"));
				}
				else
				{
					GetDepartmentName = FCConvert.ToString(rsName.Get_Fields_String("LongDescription"));
				}
			}
			else
			{
				GetDepartmentName = "";
			}
			return GetDepartmentName;
		}

		public static string GetExpDivisionName(string Department, string Division, bool blnLongDescription = false)
		{
			string GetExpDivisionName = "";
			clsDRWrapper rsName = new clsDRWrapper();
			if (Statics.ExpDivFlag)
			{
				GetExpDivisionName = "";
				return GetExpDivisionName;
			}
			rsName.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Department + "' AND Division = '" + Division + "'", "TWBD0000.vb1");
			if (rsName.EndOfFile() != true && rsName.BeginningOfFile() != true)
			{
				if (!blnLongDescription)
				{
					GetExpDivisionName = FCConvert.ToString(rsName.Get_Fields_String("ShortDescription"));
				}
				else
				{
					GetExpDivisionName = FCConvert.ToString(rsName.Get_Fields_String("LongDescription"));
				}
			}
			else
			{
				GetExpDivisionName = "";
			}
			return GetExpDivisionName;
		}

		public static string GetRevDivisionName_8(string Department, string Division, bool blnLongDescription = false)
		{
			return GetRevDivisionName(ref Department, ref Division, blnLongDescription);
		}

		public static string GetRevDivisionName(ref string Department, ref string Division, bool blnLongDescription = false)
		{
			string GetRevDivisionName = "";
			clsDRWrapper rsName = new clsDRWrapper();
			if (Statics.RevDivFlag)
			{
				GetRevDivisionName = "";
				return GetRevDivisionName;
			}
			rsName.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Department + "' AND Division = '" + Division + "'", "TWBD0000.vb1");
			if (rsName.EndOfFile() != true && rsName.BeginningOfFile() != true)
			{
				if (!blnLongDescription)
				{
					GetRevDivisionName = FCConvert.ToString(rsName.Get_Fields_String("ShortDescription"));
				}
				else
				{
					GetRevDivisionName = FCConvert.ToString(rsName.Get_Fields_String("LongDescription"));
				}
			}
			else
			{
				GetRevDivisionName = "";
			}
			return GetRevDivisionName;
		}

		public static string GetExpenseName_2(string Expense, bool blnLongDescription = false)
		{
			return GetExpenseName(ref Expense, blnLongDescription);
		}

		public static string GetExpenseName(ref string Expense, bool blnLongDescription = false)
		{
			string GetExpenseName = "";
			clsDRWrapper rsName = new clsDRWrapper();
			rsName.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense + "' AND Object = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(Statics.Exp, 7, 2)))) + "'", "TWBD0000.vb1");
			if (rsName.EndOfFile() != true && rsName.BeginningOfFile() != true)
			{
				if (!blnLongDescription)
				{
					GetExpenseName = FCConvert.ToString(rsName.Get_Fields_String("ShortDescription"));
				}
				else
				{
					GetExpenseName = FCConvert.ToString(rsName.Get_Fields_String("LongDescription"));
				}
			}
			else
			{
				GetExpenseName = "";
			}
			return GetExpenseName;
		}

		public static string GetObjectName_8(string Expense, string Object, bool blnLongDescription = false)
		{
			return GetObjectName(ref Expense, ref Object, blnLongDescription);
		}

		public static string GetObjectName(ref string Expense, ref string Object, bool blnLongDescription = false)
		{
			string GetObjectName = "";
			clsDRWrapper rsName = new clsDRWrapper();
			if (Statics.ObjFlag)
			{
				GetObjectName = "";
				return GetObjectName;
			}
			rsName.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense + "' AND Object = '" + Object + "'", "TWBD0000.vb1");
			if (rsName.EndOfFile() != true && rsName.BeginningOfFile() != true)
			{
				if (!blnLongDescription)
				{
					GetObjectName = FCConvert.ToString(rsName.Get_Fields_String("ShortDescription"));
				}
				else
				{
					GetObjectName = FCConvert.ToString(rsName.Get_Fields_String("LongDescription"));
				}
			}
			else
			{
				GetObjectName = "";
			}
			return GetObjectName;
		}

		public static string GetRevenueName_26(string Department, string Division, string Revenue, bool blnLongDescription = false)
		{
			return GetRevenueName(ref Department, ref Division, ref Revenue, blnLongDescription);
		}

		public static string GetRevenueName(ref string Department, ref string Division, ref string Revenue, bool blnLongDescription = false)
		{
			string GetRevenueName = "";
			clsDRWrapper rsName = new clsDRWrapper();
			if (!Statics.RevDivFlag)
			{
				rsName.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + Department + "' AND Division = '" + Division + "' AND Revenue = '" + Revenue + "'", "TWBD0000.vb1");
			}
			else
			{
				rsName.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + Department + "' AND Revenue = '" + Revenue + "'", "TWBD0000.vb1");
			}
			if (rsName.EndOfFile() != true && rsName.BeginningOfFile() != true)
			{
				if (!blnLongDescription)
				{
					GetRevenueName = FCConvert.ToString(rsName.Get_Fields_String("ShortDescription"));
				}
				else
				{
					GetRevenueName = FCConvert.ToString(rsName.Get_Fields_String("LongDescription"));
				}
			}
			else
			{
				GetRevenueName = "";
			}
			return GetRevenueName;
		}

		public static string GetFundName_2(string Fund, bool blnLongDescription = false)
		{
			return GetFundName(ref Fund, blnLongDescription);
		}

		public static string GetFundName(ref string Fund, bool blnLongDescription = false)
		{
			string GetFundName = "";
			clsDRWrapper rsName = new clsDRWrapper();
			rsName.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + Fund + "' AND Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(Statics.Ledger, 3, 2)))) + "'", "TWBD0000.vb1");
			if (rsName.EndOfFile() != true && rsName.BeginningOfFile() != true)
			{
				if (!blnLongDescription)
				{
					GetFundName = FCConvert.ToString(rsName.Get_Fields_String("ShortDescription"));
				}
				else
				{
					GetFundName = FCConvert.ToString(rsName.Get_Fields_String("LongDescription"));
				}
			}
			else
			{
				GetFundName = "";
			}
			return GetFundName;
		}

		public static string GetAccountName_26(string Fund, string Account, string Suffix, bool blnLongDescription = false)
		{
			return GetAccountName(ref Fund, ref Account, ref Suffix, blnLongDescription);
		}

		public static string GetAccountName(ref string Fund, ref string Account, ref string Suffix, bool blnLongDescription = false)
		{
			string GetAccountName = "";
			clsDRWrapper rsName = new clsDRWrapper();
			if (Statics.YearFlag)
			{
				rsName.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + Fund + "' AND Account = '" + Account + "'", "TWBD0000.vb1");
			}
			else
			{
				rsName.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + Fund + "' AND Account = '" + Account + "' AND [Year] = '" + Suffix + "'", "TWBD0000.vb1");
			}
			if (rsName.EndOfFile() != true && rsName.BeginningOfFile() != true)
			{
				if (!blnLongDescription)
				{
					GetAccountName = FCConvert.ToString(rsName.Get_Fields_String("ShortDescription"));
				}
				else
				{
					GetAccountName = FCConvert.ToString(rsName.Get_Fields_String("LongDescription"));
				}
			}
			else
			{
				GetAccountName = "";
			}
			return GetAccountName;
		}

		public static string ReturnAccountDescription(string x, bool blnLongDescription = false)
		{
			string ReturnAccountDescription = "";
			string TempFirst = "";
			string TempSecond = "";
			string TempThird = "";
			string TempFourth = "";
			string TempFifth = "";
			string strLabel = "";
			int counter;
			int DeptLength;
			int ExpDivLength;
			int ExpLength;
			int ObjLength;
			int RevDivLength;
			int RevLength;
			int FundLength;
			int AcctLength;
			int SuffixLength;
			
			if (x.Length > 0)
			{
				if (Strings.UCase(Strings.Left(x, 1)) == "M")
				{
					ReturnAccountDescription = "";
					return ReturnAccountDescription;
				}
			}
			DeptLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Exp, 1, 2))));
			ExpDivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Exp, 3, 2))));
			ExpLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Exp, 5, 2))));
			// Initialize variables
			ObjLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Exp, 7, 2))));
			RevDivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Rev, 3, 2))));
			RevLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Rev, 5, 2))));
			FundLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Ledger, 1, 2))));
			AcctLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Ledger, 3, 2))));
			SuffixLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.Ledger, 5, 2))));

			if (Strings.Left(x, 1) == "E")
			{
				TempFirst = Strings.Mid(x, 3, DeptLength);
				if (Strings.Trim(TempFirst) != "")
				{
					if (Strings.InStr(1, TempFirst, "_") != 0)
					{
						for (counter = 1; counter <= TempFirst.Length; counter++)
						{
							if (Strings.Mid(TempFirst, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				TempSecond = Strings.Mid(x, (4 + DeptLength), ExpDivLength);
				if (Strings.Trim(TempSecond) != "")
				{
					if (Strings.InStr(1, TempSecond, "_") != 0)
					{
						for (counter = 1; counter <= TempSecond.Length; counter++)
						{
							if (Strings.Mid(TempSecond, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				if (TempSecond == "")
				{
					TempThird = Strings.Mid(x, (4 + DeptLength + ExpDivLength), ExpLength);
				}
				else
				{
					TempThird = Strings.Mid(x, (5 + DeptLength + ExpDivLength), ExpLength);
				}
				if (Strings.Trim(TempThird) != "")
				{
					if (Strings.InStr(1, TempThird, "_") != 0)
					{
						for (counter = 1; counter <= TempThird.Length; counter++)
						{
							if (Strings.Mid(TempThird, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				if (TempSecond == "")
				{
					TempFourth = Strings.Mid(x, (5 + DeptLength + ExpDivLength + ExpLength), ObjLength);
				}
				else
				{
					TempFourth = Strings.Mid(x, (6 + DeptLength + ExpDivLength + ExpLength), ObjLength);
				}
				if (Strings.Trim(TempFourth) != "")
				{
					if (Strings.InStr(1, TempFourth, "_") != 0)
					{
						for (counter = 1; counter <= TempFourth.Length; counter++)
						{
							if (Strings.Mid(TempFourth, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				TempFirst = GetDepartmentName(Strings.Mid(x, 3, DeptLength), blnLongDescription);
				TempSecond = GetExpDivisionName(Strings.Mid(x, 3, DeptLength), Strings.Mid(x, (4 + DeptLength), ExpDivLength), blnLongDescription);
				if (Statics.ExpDivFlag)
				{
					TempThird = GetExpenseName_2(Strings.Mid(x, (4 + DeptLength + ExpDivLength), ExpLength), blnLongDescription);
					TempFourth = GetObjectName_8(Strings.Mid(x, (4 + DeptLength + ExpDivLength), ExpLength), Strings.Mid(x, (5 + DeptLength + ExpDivLength + ExpLength), ObjLength), blnLongDescription);
				}
				else
				{
					TempThird = GetExpenseName_2(Strings.Mid(x, (5 + DeptLength + ExpDivLength), ExpLength), blnLongDescription);
					TempFourth = GetObjectName_8(Strings.Mid(x, (5 + DeptLength + ExpDivLength), ExpLength), Strings.Mid(x, (6 + DeptLength + ExpDivLength + ExpLength), ObjLength), blnLongDescription);
				}
			}
			else if (Strings.Left(x, 1) == "R")
			{
				TempFirst = Strings.Mid(x, 3, DeptLength);
				if (Strings.Trim(TempFirst) != "")
				{
					if (Strings.InStr(1, TempFirst, "_") != 0)
					{
						for (counter = 1; counter <= TempFirst.Length; counter++)
						{
							if (Strings.Mid(TempFirst, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				TempSecond = Strings.Mid(x, (4 + DeptLength), RevDivLength);
				if (Strings.Trim(TempSecond) != "")
				{
					if (Strings.InStr(1, TempSecond, "_") != 0)
					{
						for (counter = 1; counter <= TempSecond.Length; counter++)
						{
							if (Strings.Mid(TempSecond, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				if (Statics.RevDivFlag)
				{
					TempThird = Strings.Mid(x, (4 + DeptLength + RevDivLength), RevLength);
				}
				else
				{
					TempThird = Strings.Mid(x, (5 + DeptLength + RevDivLength), RevLength);
				}
				if (Strings.Trim(TempThird) != "")
				{
					if (Strings.InStr(1, TempThird, "_") != 0)
					{
						for (counter = 1; counter <= TempThird.Length; counter++)
						{
							if (Strings.Mid(TempThird, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				TempFirst = GetDepartmentName(Strings.Mid(x, 3, DeptLength), blnLongDescription);
				TempSecond = GetRevDivisionName_8(Strings.Mid(x, 3, DeptLength), Strings.Mid(x, (4 + DeptLength), RevDivLength), blnLongDescription);
				if (Statics.RevDivFlag)
				{
					TempThird = GetRevenueName_26(Strings.Mid(x, 3, DeptLength), Strings.Mid(x, (4 + DeptLength), RevDivLength), Strings.Mid(x, (4 + DeptLength + RevDivLength), RevLength), blnLongDescription);
				}
				else
				{
					TempThird = GetRevenueName_26(Strings.Mid(x, 3, DeptLength), Strings.Mid(x, (4 + DeptLength), RevDivLength), Strings.Mid(x, (5 + DeptLength + RevDivLength), RevLength), blnLongDescription);
				}
				TempFourth = "";
			}
			else if (Strings.Left(x, 1) == "G")
			{
				TempFirst = Strings.Mid(x, 3, FundLength);
				if (Strings.Trim(TempFirst) != "")
				{
					if (Strings.InStr(1, TempFirst, "_") != 0)
					{
						for (counter = 1; counter <= TempFirst.Length; counter++)
						{
							if (Strings.Mid(TempFirst, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				TempSecond = Strings.Mid(x, (4 + FundLength), AcctLength);
				if (Strings.Trim(TempSecond) != "")
				{
					if (Strings.InStr(1, TempSecond, "_") != 0)
					{
						for (counter = 1; counter <= TempSecond.Length; counter++)
						{
							if (Strings.Mid(TempSecond, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				TempThird = Strings.Mid(x, (5 + FundLength + AcctLength), SuffixLength);
				if (Strings.Trim(TempThird) != "")
				{
					if (Strings.InStr(1, TempThird, "_") != 0)
					{
						for (counter = 1; counter <= TempThird.Length; counter++)
						{
							if (Strings.Mid(TempThird, counter, 1) != "_")
							{
								return ReturnAccountDescription;
							}
						}
					}
				}
				TempFirst = GetFundName_2(Strings.Mid(x, 3, FundLength), blnLongDescription);
				TempSecond = GetAccountName_26(Strings.Mid(x, 3, FundLength), Strings.Mid(x, (4 + FundLength), AcctLength), Strings.Mid(x, (5 + FundLength + AcctLength), SuffixLength), blnLongDescription);
				TempThird = "";
				TempFourth = "";
			}

		
			if (TempFirst != "" && TempSecond != "")
			{
				strLabel = TempFirst + " / " + TempSecond;
			}
			else
			{
				strLabel = TempFirst;
			}
			if (TempThird == "" && TempFourth == "")
			{
				// do nothing
			}
			else if (TempFourth == "")
			{
				strLabel += " - " + TempThird;
			}
			else if (TempThird == "")
			{
				strLabel += " - " + TempFourth;
			}
			else
			{
				strLabel += " - " + TempThird + " / " + TempFourth;
			}

			ReturnAccountDescription = strLabel;
			// bring the account over to the grid
			return ReturnAccountDescription;
		}

		public static void SetAccountFormats()
		{
			Statics.Exp = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Expenditure"));
			StaticSettings.gGlobalBudgetaryAccountSettings.Exp = Statics.Exp;
			Statics.Rev = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Revenue"));
			StaticSettings.gGlobalBudgetaryAccountSettings.Rev = Statics.Rev;
			Statics.Ledger = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Ledger"));
			StaticSettings.gGlobalBudgetaryAccountSettings.Ledger = Statics.Ledger;
			if (Statics.Exp != "")
			{
				if (Conversion.Val(Strings.Mid(Statics.Exp, 3, 2)) == 0)
				{
					Statics.ExpDivFlag = true;
				}
				else
				{
					Statics.ExpDivFlag = false;
				}
				if (Conversion.Val(Strings.Mid(Statics.Exp, 7, 2)) == 0)
				{
					Statics.ObjFlag = true;
				}
				else
				{
					Statics.ObjFlag = false;
				}
			}
			if (Statics.Rev != "")
			{
				if (Conversion.Val(Strings.Mid(Statics.Rev, 3, 2)) == 0)
				{
					Statics.RevDivFlag = true;
				}
				else
				{
					Statics.RevDivFlag = false;
				}
			}
			if (Statics.Ledger != "")
			{
				if (Conversion.Val(Strings.Mid(Statics.Ledger, 5, 2)) == 0)
				{
					Statics.YearFlag = true;
				}
				else
				{
					Statics.YearFlag = false;
				}
			}
			StaticSettings.gGlobalBudgetaryAccountSettings.ExpDivFlag = Statics.ExpDivFlag;
			StaticSettings.gGlobalBudgetaryAccountSettings.ObjFlag = Statics.ObjFlag;
			StaticSettings.gGlobalBudgetaryAccountSettings.RevDivFlag = Statics.RevDivFlag;
			StaticSettings.gGlobalBudgetaryAccountSettings.YearFlag = Statics.YearFlag;
            StaticSettings.gGlobalBudgetaryAccountSettings.UsePUCChartOfAccounts =
                FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("UsePUCChartOfAccounts"));
        }

		public class StaticVariables
		{
			// ********************************************************
			// and modValidateAccount                                 *
			// This module is to be used with modBudgetaryAccounting  *
			// Last Updated   :               12/03/2002              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// Date           :               06/17/2002              *
			// WRITTEN BY     :               Jim Bertolino           *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			public bool ExpDivFlag;
			public bool RevDivFlag;
			public bool ObjFlag;
			public bool YearFlag;
			public string Exp = "";
			public string Rev = "";
			public string Ledger = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
