//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
namespace Global
{
    /// <summary>
    /// Summary description for frmScanDocument.
    /// </summary>
    partial class frmScanDocument : BaseForm
    {
        //public AxSCANNERLib.AxScanner Scanner1;
        public dynamic Scanner1;
        public fecherFoundation.FCFrame Frame5;
        public fecherFoundation.FCTextBox txtCapHeight;
        public fecherFoundation.FCTextBox txtCapWidth;
        public fecherFoundation.FCTextBox txtCapTop;
        public fecherFoundation.FCTextBox txtCapLeft;
        public fecherFoundation.FCLabel Label38;
        public fecherFoundation.FCLabel Label37;
        public fecherFoundation.FCLabel Label36;
        public fecherFoundation.FCLabel Label35;
        public fecherFoundation.FCLabel Label1;
        public fecherFoundation.FCFrame Frame1;
        public fecherFoundation.FCCheckBox chkEnableDuplex;
        public fecherFoundation.FCCheckBox chkEnableFeeder;
        public fecherFoundation.FCCheckBox chkClearBuffer;
        public fecherFoundation.FCCheckBox chkShowTWAIN;
        public fecherFoundation.FCComboBox cboDPI;
        public fecherFoundation.FCComboBox cboImageSource;
        public fecherFoundation.FCButton cmdScan;
        public fecherFoundation.FCLabel Label10;
        public fecherFoundation.FCLabel Label2;
        public fecherFoundation.FCFrame Frame2;
        public fecherFoundation.FCTabControl SSTab1;
        public fecherFoundation.FCTabPage SSTab1_Page1;
        public fecherFoundation.FCFrame Frame3;
        public fecherFoundation.FCButton cmdBrightness;
        public fecherFoundation.FCTextBox txtBrightness;
        public fecherFoundation.FCButton cmdSaturation;
        public fecherFoundation.FCButton cmdContrast;
        public fecherFoundation.FCTextBox txtContrast;
        public fecherFoundation.FCTextBox txtHue;
        public fecherFoundation.FCTextBox txtGamma;
        public fecherFoundation.FCButton cmdHue;
        public fecherFoundation.FCButton cmdGamma;
        public fecherFoundation.FCTextBox txtSaturation;
        public fecherFoundation.FCLabel Label27;
        public fecherFoundation.FCLabel Label30;
        public fecherFoundation.FCLabel Label29;
        public fecherFoundation.FCLabel Label28;
        public fecherFoundation.FCLabel Label26;
        public fecherFoundation.FCTabPage SSTab1_Page2;
        public fecherFoundation.FCFrame Frame4;
        public fecherFoundation.FCButton cmdClearText;
        public fecherFoundation.FCTextBox txtText2;
        public fecherFoundation.FCTextBox txtText1;
        public fecherFoundation.FCButton cmdDrawText;
        public fecherFoundation.FCTextBox txtTextTop;
        public fecherFoundation.FCTextBox txtTextLeft;
        public fecherFoundation.FCLabel Label17;
        public fecherFoundation.FCLabel Label16;
        public fecherFoundation.FCLabel Label15;
        public fecherFoundation.FCLabel Label14;
        public fecherFoundation.FCButton cmdUpdatePage;
        public fecherFoundation.FCButton cmdDeletePage;
        public fecherFoundation.FCLabel lblCurrentPage;
        public fecherFoundation.FCLabel Label8;
        public fecherFoundation.FCLabel lblTotalPage;
        public fecherFoundation.FCLabel Label3;
        private Wisej.Web.MainMenu MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        public fecherFoundation.FCToolStripMenuItem mnuFileResetCapture;
        public fecherFoundation.FCToolStripMenuItem mnuFileZoom;
        public fecherFoundation.FCToolStripMenuItem mnuFileZoomZoomIn;
        public fecherFoundation.FCToolStripMenuItem mnuFileZoomZoomOut;
        public fecherFoundation.FCToolStripMenuItem mnuZoomFitToWindow;
        public fecherFoundation.FCToolStripMenuItem mnuFileZoomAspectRatio;
        public fecherFoundation.FCToolStripMenuItem mnuFileRotate;
        public fecherFoundation.FCToolStripMenuItem mnuFilePreviousPage;
        public fecherFoundation.FCToolStripMenuItem mnuFileNextPage;
        public fecherFoundation.FCToolStripMenuItem mnuSeperator;
        public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
        public fecherFoundation.FCToolStripMenuItem Seperator;
        public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
        private Wisej.Web.ToolTip ToolTip1;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmScanDocument));
            this.components = new System.ComponentModel.Container();
            //this.Scanner1 = new AxSCANNERLib.AxScanner();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtCapHeight = new fecherFoundation.FCTextBox();
            this.txtCapWidth = new fecherFoundation.FCTextBox();
            this.txtCapTop = new fecherFoundation.FCTextBox();
            this.txtCapLeft = new fecherFoundation.FCTextBox();
            this.Label38 = new fecherFoundation.FCLabel();
            this.Label37 = new fecherFoundation.FCLabel();
            this.Label36 = new fecherFoundation.FCLabel();
            this.Label35 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkEnableDuplex = new fecherFoundation.FCCheckBox();
            this.chkEnableFeeder = new fecherFoundation.FCCheckBox();
            this.chkClearBuffer = new fecherFoundation.FCCheckBox();
            this.chkShowTWAIN = new fecherFoundation.FCCheckBox();
            this.cboDPI = new fecherFoundation.FCComboBox();
            this.cboImageSource = new fecherFoundation.FCComboBox();
            this.cmdScan = new fecherFoundation.FCButton();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.cmdBrightness = new fecherFoundation.FCButton();
            this.txtBrightness = new fecherFoundation.FCTextBox();
            this.cmdSaturation = new fecherFoundation.FCButton();
            this.cmdContrast = new fecherFoundation.FCButton();
            this.txtContrast = new fecherFoundation.FCTextBox();
            this.txtHue = new fecherFoundation.FCTextBox();
            this.txtGamma = new fecherFoundation.FCTextBox();
            this.cmdHue = new fecherFoundation.FCButton();
            this.cmdGamma = new fecherFoundation.FCButton();
            this.txtSaturation = new fecherFoundation.FCTextBox();
            this.Label27 = new fecherFoundation.FCLabel();
            this.Label30 = new fecherFoundation.FCLabel();
            this.Label29 = new fecherFoundation.FCLabel();
            this.Label28 = new fecherFoundation.FCLabel();
            this.Label26 = new fecherFoundation.FCLabel();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.cmdClearText = new fecherFoundation.FCButton();
            this.txtText2 = new fecherFoundation.FCTextBox();
            this.txtText1 = new fecherFoundation.FCTextBox();
            this.cmdDrawText = new fecherFoundation.FCButton();
            this.txtTextTop = new fecherFoundation.FCTextBox();
            this.txtTextLeft = new fecherFoundation.FCTextBox();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.cmdUpdatePage = new fecherFoundation.FCButton();
            this.cmdDeletePage = new fecherFoundation.FCButton();
            this.lblCurrentPage = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.lblTotalPage = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileResetCapture = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoom = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoomZoomIn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoomZoomOut = new fecherFoundation.FCToolStripMenuItem();
            this.mnuZoomFitToWindow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoomAspectRatio = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileRotate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreviousPage = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileNextPage = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.SuspendLayout();
            //
            // Scanner1
            //
            this.Scanner1.Name = "Scanner1";
            this.Scanner1.TabIndex = 22;
            this.Scanner1.Location = new System.Drawing.Point(245, 28);
            this.Scanner1.Size = new System.Drawing.Size(341, 385);
            //this.Scanner1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("Scanner1.OcxState")));
            this.Scanner1.EndScan += new System.EventHandler(this.Scanner1_EndScan);
            //
            // Frame5
            //
            this.Frame5.Controls.Add(this.txtCapHeight);
            this.Frame5.Controls.Add(this.txtCapWidth);
            this.Frame5.Controls.Add(this.txtCapTop);
            this.Frame5.Controls.Add(this.txtCapLeft);
            this.Frame5.Controls.Add(this.Label38);
            this.Frame5.Controls.Add(this.Label37);
            this.Frame5.Controls.Add(this.Label36);
            this.Frame5.Controls.Add(this.Label35);
            this.Frame5.Controls.Add(this.Label1);
            this.Frame5.Name = "Frame5";
            this.Frame5.TabIndex = 34;
            this.ToolTip1.SetToolTip(this.Frame5, "Use these fields to manually override the settings for what part of the scanned image to capture");
            this.Frame5.Location = new System.Drawing.Point(231, 444);
            this.Frame5.Size = new System.Drawing.Size(373, 43);
            this.Frame5.Text = "Capture Area";
            ////this.Frame5.BackColor = System.Drawing.SystemColors.Control;
            //
            // txtCapHeight
            //
            this.txtCapHeight.Name = "txtCapHeight";
            this.txtCapHeight.TabIndex = 29;
            this.txtCapHeight.Location = new System.Drawing.Point(277, 18);
            this.txtCapHeight.Size = new System.Drawing.Size(33, 40);
            this.txtCapHeight.Text = "11.5";
            this.txtCapHeight.BackColor = System.Drawing.SystemColors.Window;
            //
            // txtCapWidth
            //
            this.txtCapWidth.Name = "txtCapWidth";
            this.txtCapWidth.TabIndex = 28;
            this.txtCapWidth.Location = new System.Drawing.Point(190, 18);
            this.txtCapWidth.Size = new System.Drawing.Size(33, 40);
            this.txtCapWidth.Text = "8.23";
            this.txtCapWidth.BackColor = System.Drawing.SystemColors.Window;
            //
            // txtCapTop
            //
            this.txtCapTop.Name = "txtCapTop";
            this.txtCapTop.TabIndex = 27;
            this.txtCapTop.Location = new System.Drawing.Point(107, 18);
            this.txtCapTop.Size = new System.Drawing.Size(33, 40);
            this.txtCapTop.Text = "0";
            this.txtCapTop.BackColor = System.Drawing.SystemColors.Window;
            //
            // txtCapLeft
            //
            this.txtCapLeft.Name = "txtCapLeft";
            this.txtCapLeft.TabIndex = 26;
            this.txtCapLeft.Location = new System.Drawing.Point(36, 18);
            this.txtCapLeft.Size = new System.Drawing.Size(33, 40);
            this.txtCapLeft.Text = "0";
            this.txtCapLeft.BackColor = System.Drawing.SystemColors.Window;
            //
            // Label38
            //
            this.Label38.Name = "Label38";
            this.Label38.TabIndex = 39;
            this.Label38.Location = new System.Drawing.Point(322, 20);
            this.Label38.Size = new System.Drawing.Size(41, 17);
            this.Label38.Text = "Inches";
            ////this.Label38.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label37
            //
            this.Label37.Name = "Label37";
            this.Label37.TabIndex = 38;
            this.Label37.Location = new System.Drawing.Point(235, 20);
            this.Label37.Size = new System.Drawing.Size(39, 17);
            this.Label37.Text = "Height";
            ////this.Label37.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label36
            //
            this.Label36.Name = "Label36";
            this.Label36.TabIndex = 37;
            this.Label36.Location = new System.Drawing.Point(152, 20);
            this.Label36.Size = new System.Drawing.Size(33, 17);
            this.Label36.Text = "Width";
            ////this.Label36.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label35
            //
            this.Label35.Name = "Label35";
            this.Label35.TabIndex = 36;
            this.Label35.Location = new System.Drawing.Point(81, 20);
            this.Label35.Size = new System.Drawing.Size(33, 17);
            this.Label35.Text = "Top";
            ////this.Label35.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label1
            //
            this.Label1.Name = "Label1";
            this.Label1.TabIndex = 35;
            this.Label1.Location = new System.Drawing.Point(8, 20);
            this.Label1.Size = new System.Drawing.Size(25, 17);
            this.Label1.Text = "Left";
            ////this.Label1.BackColor = System.Drawing.SystemColors.Control;
            //
            // Frame1
            //
            this.Frame1.Controls.Add(this.chkEnableDuplex);
            this.Frame1.Controls.Add(this.chkEnableFeeder);
            this.Frame1.Controls.Add(this.chkClearBuffer);
            this.Frame1.Controls.Add(this.chkShowTWAIN);
            this.Frame1.Controls.Add(this.cboDPI);
            this.Frame1.Controls.Add(this.cboImageSource);
            this.Frame1.Controls.Add(this.cmdScan);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Name = "Frame1";
            this.Frame1.TabIndex = 31;
            this.Frame1.Location = new System.Drawing.Point(4, 28);
            this.Frame1.Size = new System.Drawing.Size(199, 201);
            this.Frame1.Text = "Scan Options";
            ////this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            //
            // chkEnableDuplex
            //
            this.chkEnableDuplex.Name = "chkEnableDuplex";
            this.chkEnableDuplex.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.chkEnableDuplex, "Allows double-sided documents to be scanned if the selected scanner supports it.");
            this.chkEnableDuplex.Location = new System.Drawing.Point(6, 81);
            this.chkEnableDuplex.Size = new System.Drawing.Size(155, 17);
            this.chkEnableDuplex.Text = "Enable Duplex";
            ////this.chkEnableDuplex.BackColor = System.Drawing.SystemColors.Control;
            //
            // chkEnableFeeder
            //
            this.chkEnableFeeder.Name = "chkEnableFeeder";
            this.chkEnableFeeder.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.chkEnableFeeder, "Allows an optional feeder on the selected scanner to scan multiple pages at once.");
            this.chkEnableFeeder.Location = new System.Drawing.Point(6, 61);
            this.chkEnableFeeder.Size = new System.Drawing.Size(147, 17);
            this.chkEnableFeeder.Text = "Enable Feeder";
            ////this.chkEnableFeeder.BackColor = System.Drawing.SystemColors.Control;
            //
            // chkClearBuffer
            //
            this.chkClearBuffer.Name = "chkClearBuffer";
            this.chkClearBuffer.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.chkClearBuffer, "If this option is selected, scanned images will replace the existing page of the document, instead of adding a new page.");
            this.chkClearBuffer.Location = new System.Drawing.Point(6, 36);
            this.chkClearBuffer.Size = new System.Drawing.Size(185, 25);
            this.chkClearBuffer.Text = "Clear Image Buffer After Scan";
            ////this.chkClearBuffer.BackColor = System.Drawing.SystemColors.Control;
            this.chkClearBuffer.CheckedChanged += new System.EventHandler(this.chkClearBuffer_CheckedChanged);
            //
            // chkShowTWAIN
            //
            this.chkShowTWAIN.Name = "chkShowTWAIN";
            this.chkShowTWAIN.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.chkShowTWAIN, "Selecting this option will scan the image using the software interface for the selected scanner. This can improve the quality of the images that are scanned.");
            this.chkShowTWAIN.Location = new System.Drawing.Point(6, 20);
            this.chkShowTWAIN.Size = new System.Drawing.Size(187, 17);
            this.chkShowTWAIN.Text = "Show TWAIN User Interface";
            ////this.chkShowTWAIN.BackColor = System.Drawing.SystemColors.Control;
            this.chkShowTWAIN.CheckedChanged += new System.EventHandler(this.chkShowTWAIN_CheckedChanged);
            //
            // cboDPI
            //
            this.cboDPI.Name = "cboDPI";
            this.cboDPI.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cboDPI, "Select the image quality for scanning");
            this.cboDPI.Location = new System.Drawing.Point(34, 103);
            this.cboDPI.Size = new System.Drawing.Size(159, 40);
            this.cboDPI.Text = "";
            this.cboDPI.BackColor = System.Drawing.SystemColors.Window;
            this.cboDPI.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            //
            // cboImageSource
            //
            this.cboImageSource.Name = "cboImageSource";
            this.cboImageSource.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cboImageSource, "Select the scanner to use for capturing images");
            this.cboImageSource.Location = new System.Drawing.Point(4, 148);
            this.cboImageSource.Size = new System.Drawing.Size(189, 40);
            this.cboImageSource.Text = "";
            this.cboImageSource.BackColor = System.Drawing.SystemColors.Window;
            this.cboImageSource.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            //
            // cmdScan
            //
            this.cmdScan.Name = "cmdScan";
            this.cmdScan.TabIndex = 6;
            this.cmdScan.Location = new System.Drawing.Point(24, 176);
            this.cmdScan.Size = new System.Drawing.Size(153, 23);
            this.cmdScan.Text = "Scan Image";
            ////this.cmdScan.BackColor = System.Drawing.SystemColors.Control;
            this.cmdScan.Click += new System.EventHandler(this.cmdScan_Click);
            //
            // Label10
            //
            this.Label10.Name = "Label10";
            this.Label10.TabIndex = 33;
            this.Label10.Location = new System.Drawing.Point(8, 105);
            this.Label10.Size = new System.Drawing.Size(25, 17);
            this.Label10.Text = "DPI";
            ////this.Label10.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label2
            //
            this.Label2.Name = "Label2";
            this.Label2.TabIndex = 32;
            this.Label2.Location = new System.Drawing.Point(6, 129);
            this.Label2.Size = new System.Drawing.Size(78, 25);
            this.Label2.Text = "Image Source";
            ////this.Label2.BackColor = System.Drawing.SystemColors.Control;
            //
            // Frame2
            //
            this.Frame2.Controls.Add(this.SSTab1);
            this.Frame2.Name = "Frame2";
            this.Frame2.TabIndex = 24;
            this.Frame2.Location = new System.Drawing.Point(4, 234);
            this.Frame2.Size = new System.Drawing.Size(211, 211);
            this.Frame2.Text = "Current Image options";
            ////this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            //
            // SSTab1
            //
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.TabIndex = 30;
            this.SSTab1.Location = new System.Drawing.Point(6, 30);
            this.SSTab1.Size = new System.Drawing.Size(201, 173);
            //
            // SSTab1_Page1
            //
            this.SSTab1_Page1.Controls.Add(this.Frame3);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Size = new System.Drawing.Size(201, 153);
            this.SSTab1_Page1.Text = "Image Options";
            //
            // Frame3
            //
            this.Frame3.Controls.Add(this.cmdBrightness);
            this.Frame3.Controls.Add(this.txtBrightness);
            this.Frame3.Controls.Add(this.cmdSaturation);
            this.Frame3.Controls.Add(this.cmdContrast);
            this.Frame3.Controls.Add(this.txtContrast);
            this.Frame3.Controls.Add(this.txtHue);
            this.Frame3.Controls.Add(this.txtGamma);
            this.Frame3.Controls.Add(this.cmdHue);
            this.Frame3.Controls.Add(this.cmdGamma);
            this.Frame3.Controls.Add(this.txtSaturation);
            this.Frame3.Controls.Add(this.Label27);
            this.Frame3.Controls.Add(this.Label30);
            this.Frame3.Controls.Add(this.Label29);
            this.Frame3.Controls.Add(this.Label28);
            this.Frame3.Controls.Add(this.Label26);
            this.Frame3.Name = "Frame3";
            this.Frame3.TabIndex = 44;
            this.Frame3.Location = new System.Drawing.Point(4, 2);
            this.Frame3.Size = new System.Drawing.Size(195, 149);
            this.Frame3.Text = "";
            ////this.Frame3.BackColor = System.Drawing.SystemColors.Control;
            //
            // cmdBrightness
            //
            this.cmdBrightness.Name = "cmdBrightness";
            this.cmdBrightness.TabIndex = 7;
            this.cmdBrightness.Location = new System.Drawing.Point(131, 22);
            this.cmdBrightness.Size = new System.Drawing.Size(33, 23);
            this.cmdBrightness.Text = "Set";
            ////this.cmdBrightness.BackColor = System.Drawing.SystemColors.Control;
            this.cmdBrightness.Click += new System.EventHandler(this.cmdBrightness_Click);
            //
            // txtBrightness
            //
            this.txtBrightness.Name = "txtBrightness";
            this.txtBrightness.TabIndex = 45;
            this.ToolTip1.SetToolTip(this.txtBrightness, "From -1.0 - 1.0");
            this.txtBrightness.Location = new System.Drawing.Point(91, 22);
            this.txtBrightness.Size = new System.Drawing.Size(41, 40);
            this.txtBrightness.Text = "0";
            this.txtBrightness.BackColor = System.Drawing.SystemColors.Window;
            //
            // cmdSaturation
            //
            this.cmdSaturation.Name = "cmdSaturation";
            this.cmdSaturation.TabIndex = 11;
            this.cmdSaturation.Location = new System.Drawing.Point(131, 67);
            this.cmdSaturation.Size = new System.Drawing.Size(33, 23);
            this.cmdSaturation.Text = "Set";
            ////this.cmdSaturation.BackColor = System.Drawing.SystemColors.Control;
            this.cmdSaturation.Click += new System.EventHandler(this.cmdSaturation_Click);
            //
            // cmdContrast
            //
            this.cmdContrast.Name = "cmdContrast";
            this.cmdContrast.TabIndex = 9;
            this.cmdContrast.Location = new System.Drawing.Point(131, 44);
            this.cmdContrast.Size = new System.Drawing.Size(33, 23);
            this.cmdContrast.Text = "Set";
            ////this.cmdContrast.BackColor = System.Drawing.SystemColors.Control;
            this.cmdContrast.Click += new System.EventHandler(this.cmdContrast_Click);
            //
            // txtContrast
            //
            this.txtContrast.Name = "txtContrast";
            this.txtContrast.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtContrast, "Value From 0.1 - 4.0");
            this.txtContrast.Location = new System.Drawing.Point(91, 44);
            this.txtContrast.Size = new System.Drawing.Size(41, 40);
            this.txtContrast.Text = "1";
            this.txtContrast.BackColor = System.Drawing.SystemColors.Window;
            //
            // txtHue
            //
            this.txtHue.Name = "txtHue";
            this.txtHue.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtHue, "Value From -180 - 180");
            this.txtHue.Location = new System.Drawing.Point(91, 89);
            this.txtHue.Size = new System.Drawing.Size(41, 40);
            this.txtHue.Text = "0";
            this.txtHue.BackColor = System.Drawing.SystemColors.Window;
            //
            // txtGamma
            //
            this.txtGamma.Name = "txtGamma";
            this.txtGamma.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.txtGamma, "Value From 0.1 - 4.0");
            this.txtGamma.Location = new System.Drawing.Point(91, 111);
            this.txtGamma.Size = new System.Drawing.Size(41, 40);
            this.txtGamma.Text = "1";
            this.txtGamma.BackColor = System.Drawing.SystemColors.Window;
            //
            // cmdHue
            //
            this.cmdHue.Name = "cmdHue";
            this.cmdHue.TabIndex = 13;
            this.cmdHue.Location = new System.Drawing.Point(131, 89);
            this.cmdHue.Size = new System.Drawing.Size(33, 23);
            this.cmdHue.Text = "Set";
            ////this.cmdHue.BackColor = System.Drawing.SystemColors.Control;
            this.cmdHue.Click += new System.EventHandler(this.cmdHue_Click);
            //
            // cmdGamma
            //
            this.cmdGamma.Name = "cmdGamma";
            this.cmdGamma.TabIndex = 15;
            this.cmdGamma.Location = new System.Drawing.Point(131, 111);
            this.cmdGamma.Size = new System.Drawing.Size(33, 23);
            this.cmdGamma.Text = "Set";
            ////this.cmdGamma.BackColor = System.Drawing.SystemColors.Control;
            this.cmdGamma.Click += new System.EventHandler(this.cmdGamma_Click);
            //
            // txtSaturation
            //
            this.txtSaturation.Name = "txtSaturation";
            this.txtSaturation.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtSaturation, "Value From 0.0 - 3.0");
            this.txtSaturation.Location = new System.Drawing.Point(91, 67);
            this.txtSaturation.Size = new System.Drawing.Size(41, 40);
            this.txtSaturation.Text = "1";
            this.txtSaturation.BackColor = System.Drawing.SystemColors.Window;
            //
            // Label27
            //
            this.Label27.Name = "Label27";
            this.Label27.TabIndex = 50;
            this.ToolTip1.SetToolTip(this.Label27, "Value From -1.0 - 1.0");
            this.Label27.Location = new System.Drawing.Point(24, 22);
            this.Label27.Size = new System.Drawing.Size(72, 15);
            this.Label27.Text = "Brightness";
            ////this.Label27.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label30
            //
            this.Label30.Name = "Label30";
            this.Label30.TabIndex = 49;
            this.ToolTip1.SetToolTip(this.Label30, "Value From 0.1 - 4.0");
            this.Label30.Location = new System.Drawing.Point(24, 111);
            this.Label30.Size = new System.Drawing.Size(72, 15);
            this.Label30.Text = "Gamma";
            ////this.Label30.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label29
            //
            this.Label29.Name = "Label29";
            this.Label29.TabIndex = 48;
            this.ToolTip1.SetToolTip(this.Label29, "Value From -180 - 180");
            this.Label29.Location = new System.Drawing.Point(24, 89);
            this.Label29.Size = new System.Drawing.Size(72, 15);
            this.Label29.Text = "Hue";
            ////this.Label29.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label28
            //
            this.Label28.Name = "Label28";
            this.Label28.TabIndex = 47;
            this.ToolTip1.SetToolTip(this.Label28, "Value From 0.0 - 3.0");
            this.Label28.Location = new System.Drawing.Point(24, 67);
            this.Label28.Size = new System.Drawing.Size(72, 15);
            this.Label28.Text = "Saturation";
            ////this.Label28.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label26
            //
            this.Label26.Name = "Label26";
            this.Label26.TabIndex = 46;
            this.ToolTip1.SetToolTip(this.Label26, "Value From 0.1 - 4.0");
            this.Label26.Location = new System.Drawing.Point(24, 44);
            this.Label26.Size = new System.Drawing.Size(72, 15);
            this.Label26.Text = "Contrast";
            ////this.Label26.BackColor = System.Drawing.SystemColors.Control;
            //
            // SSTab1_Page2
            //
            this.SSTab1_Page2.Controls.Add(this.Frame4);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Size = new System.Drawing.Size(201, 153);
            this.SSTab1_Page2.Text = "Text Options";
            //
            // Frame4
            //
            this.Frame4.Controls.Add(this.cmdClearText);
            this.Frame4.Controls.Add(this.txtText2);
            this.Frame4.Controls.Add(this.txtText1);
            this.Frame4.Controls.Add(this.cmdDrawText);
            this.Frame4.Controls.Add(this.txtTextTop);
            this.Frame4.Controls.Add(this.txtTextLeft);
            this.Frame4.Controls.Add(this.Label17);
            this.Frame4.Controls.Add(this.Label16);
            this.Frame4.Controls.Add(this.Label15);
            this.Frame4.Controls.Add(this.Label14);
            this.Frame4.Name = "Frame4";
            this.Frame4.TabIndex = 51;
            this.Frame4.Location = new System.Drawing.Point(4, 4);
            this.Frame4.Size = new System.Drawing.Size(193, 145);
            this.Frame4.Text = "";
            ////this.Frame4.BackColor = System.Drawing.SystemColors.Control;
            //
            // cmdClearText
            //
            this.cmdClearText.Name = "cmdClearText";
            this.cmdClearText.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.cmdClearText, "Removed entered text from the scanned image");
            this.cmdClearText.Location = new System.Drawing.Point(101, 113);
            this.cmdClearText.Size = new System.Drawing.Size(82, 25);
            this.cmdClearText.Text = "Clear Text";
            ////this.cmdClearText.BackColor = System.Drawing.SystemColors.Control;
            this.cmdClearText.Click += new System.EventHandler(this.cmdClearText_Click);
            //
            // txtText2
            //
            this.txtText2.Name = "txtText2";
            this.txtText2.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.txtText2, "Enter text to display on scanned images");
            this.txtText2.Location = new System.Drawing.Point(44, 79);
            this.txtText2.Size = new System.Drawing.Size(139, 40);
            this.txtText2.Text = "";
            this.txtText2.BackColor = System.Drawing.SystemColors.Window;
            //
            // txtText1
            //
            this.txtText1.Name = "txtText1";
            this.txtText1.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.txtText1, "Enter text to display on scanned images");
            this.txtText1.Location = new System.Drawing.Point(44, 57);
            this.txtText1.Size = new System.Drawing.Size(139, 40);
            this.txtText1.Text = "";
            this.txtText1.BackColor = System.Drawing.SystemColors.Window;
            //
            // cmdDrawText
            //
            this.cmdDrawText.Name = "cmdDrawText";
            this.cmdDrawText.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.cmdDrawText, "Adds entered text to the scanned image");
            this.cmdDrawText.Location = new System.Drawing.Point(8, 113);
            this.cmdDrawText.Size = new System.Drawing.Size(82, 25);
            this.cmdDrawText.Text = "Draw Text";
            ////this.cmdDrawText.BackColor = System.Drawing.SystemColors.Control;
            this.cmdDrawText.Click += new System.EventHandler(this.cmdDrawText_Click);
            //
            // txtTextTop
            //
            this.txtTextTop.Name = "txtTextTop";
            this.txtTextTop.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.txtTextTop, "Enter positive values to change the location of the entered text on the image");
            this.txtTextTop.Location = new System.Drawing.Point(138, 18);
            this.txtTextTop.Size = new System.Drawing.Size(33, 40);
            this.txtTextTop.Text = "0";
            this.txtTextTop.BackColor = System.Drawing.SystemColors.Window;
            //
            // txtTextLeft
            //
            this.txtTextLeft.Name = "txtTextLeft";
            this.txtTextLeft.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.txtTextLeft, "Enter positive values to change the location of the entered text on the image");
            this.txtTextLeft.Location = new System.Drawing.Point(44, 18);
            this.txtTextLeft.Size = new System.Drawing.Size(33, 40);
            this.txtTextLeft.Text = "0";
            this.txtTextLeft.BackColor = System.Drawing.SystemColors.Window;
            //
            // Label17
            //
            this.Label17.Name = "Label17";
            this.Label17.TabIndex = 55;
            this.Label17.Location = new System.Drawing.Point(2, 81);
            this.Label17.Size = new System.Drawing.Size(41, 17);
            this.Label17.Text = "Text 2";
            ////this.Label17.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label16
            //
            this.Label16.Name = "Label16";
            this.Label16.TabIndex = 54;
            this.Label16.Location = new System.Drawing.Point(2, 59);
            this.Label16.Size = new System.Drawing.Size(41, 17);
            this.Label16.Text = "Text 1";
            ////this.Label16.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label15
            //
            this.Label15.Name = "Label15";
            this.Label15.TabIndex = 53;
            this.Label15.Location = new System.Drawing.Point(113, 20);
            this.Label15.Size = new System.Drawing.Size(25, 25);
            this.Label15.Text = "Top";
            ////this.Label15.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label14
            //
            this.Label14.Name = "Label14";
            this.Label14.TabIndex = 52;
            this.Label14.Location = new System.Drawing.Point(16, 20);
            this.Label14.Size = new System.Drawing.Size(41, 17);
            this.Label14.Text = "Left";
            ////this.Label14.BackColor = System.Drawing.SystemColors.Control;
            //
            // cmdUpdatePage
            //
            this.cmdUpdatePage.Name = "cmdUpdatePage";
            this.cmdUpdatePage.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.cmdUpdatePage, "Saves Image and Text Option changes to the existing image");
            this.cmdUpdatePage.Location = new System.Drawing.Point(431, 416);
            this.cmdUpdatePage.Size = new System.Drawing.Size(86, 25);
            this.cmdUpdatePage.Text = "Update Page";
            ////this.cmdUpdatePage.BackColor = System.Drawing.SystemColors.Control;
            this.cmdUpdatePage.Click += new System.EventHandler(this.cmdUpdatePage_Click);
            //
            // cmdDeletePage
            //
            this.cmdDeletePage.Name = "cmdDeletePage";
            this.cmdDeletePage.TabIndex = 25;
            this.ToolTip1.SetToolTip(this.cmdDeletePage, "Removes the current page from the scanned document");
            this.cmdDeletePage.Location = new System.Drawing.Point(522, 416);
            this.cmdDeletePage.Size = new System.Drawing.Size(84, 25);
            this.cmdDeletePage.Text = "Delete Page";
            ////this.cmdDeletePage.BackColor = System.Drawing.SystemColors.Control;
            this.cmdDeletePage.Click += new System.EventHandler(this.cmdDeletePage_Click);
            //
            // lblCurrentPage
            //
            this.lblCurrentPage.Name = "lblCurrentPage";
            this.lblCurrentPage.TabIndex = 43;
            this.lblCurrentPage.Location = new System.Drawing.Point(307, 420);
            this.lblCurrentPage.Size = new System.Drawing.Size(29, 17);
            this.lblCurrentPage.Text = "0";
            ////this.lblCurrentPage.BackColor = System.Drawing.SystemColors.Control;
            this.lblCurrentPage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            // Label8
            //
            this.Label8.Name = "Label8";
            this.Label8.TabIndex = 42;
            this.Label8.Location = new System.Drawing.Point(342, 420);
            this.Label8.Size = new System.Drawing.Size(9, 17);
            this.Label8.Text = "/";
            ////this.Label8.BackColor = System.Drawing.SystemColors.Control;
            //
            // lblTotalPage
            //
            this.lblTotalPage.Name = "lblTotalPage";
            this.lblTotalPage.TabIndex = 41;
            this.lblTotalPage.Location = new System.Drawing.Point(354, 420);
            this.lblTotalPage.Size = new System.Drawing.Size(37, 17);
            this.lblTotalPage.Text = "0";
            ////this.lblTotalPage.BackColor = System.Drawing.SystemColors.Control;
            //
            // Label3
            //
            this.Label3.Name = "Label3";
            this.Label3.TabIndex = 40;
            this.Label3.Location = new System.Drawing.Point(231, 420);
            this.Label3.Size = new System.Drawing.Size(74, 17);
            this.Label3.Text = "Total Pages";
            ////this.Label3.BackColor = System.Drawing.SystemColors.Control;
            //
            // mnuProcess
            //
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] { this.mnuFileResetCapture, this.mnuFileZoom, this.mnuFileRotate, this.mnuFilePreviousPage, this.mnuFileNextPage, this.mnuSeperator, this.mnuProcessSave, this.Seperator, this.mnuProcessQuit });
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            //
            // mnuFileResetCapture
            //
            this.mnuFileResetCapture.Name = "mnuFileResetCapture";
            this.mnuFileResetCapture.Text = "Reset Capture Area to Default";
            this.mnuFileResetCapture.Click += new System.EventHandler(this.mnuFileResetCapture_Click);
            //
            // mnuFileZoom
            //
            this.mnuFileZoom.MenuItems.AddRange(new Wisej.Web.MenuItem[] { this.mnuFileZoomZoomIn, this.mnuFileZoomZoomOut, this.mnuZoomFitToWindow, this.mnuFileZoomAspectRatio });
            this.mnuFileZoom.Name = "mnuFileZoom";
            this.mnuFileZoom.Text = "Zoom";
            //
            // mnuFileZoomZoomIn
            //
            this.mnuFileZoomZoomIn.Name = "mnuFileZoomZoomIn";
            this.mnuFileZoomZoomIn.Text = "Zoom In";
            this.mnuFileZoomZoomIn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuFileZoomZoomIn.Click += new System.EventHandler(this.mnuFileZoomZoomIn_Click);
            //
            // mnuFileZoomZoomOut
            //
            this.mnuFileZoomZoomOut.Name = "mnuFileZoomZoomOut";
            this.mnuFileZoomZoomOut.Text = "Zoom Out";
            this.mnuFileZoomZoomOut.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuFileZoomZoomOut.Click += new System.EventHandler(this.mnuFileZoomZoomOut_Click);
            //
            // mnuZoomFitToWindow
            //
            this.mnuZoomFitToWindow.Name = "mnuZoomFitToWindow";
            this.mnuZoomFitToWindow.Text = "Fit To Window";
            this.mnuZoomFitToWindow.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuZoomFitToWindow.Click += new System.EventHandler(this.mnuZoomFitToWindow_Click);
            //
            // mnuFileZoomAspectRatio
            //
            this.mnuFileZoomAspectRatio.Name = "mnuFileZoomAspectRatio";
            this.mnuFileZoomAspectRatio.Text = "Aspect Ratio";
            this.mnuFileZoomAspectRatio.Click += new System.EventHandler(this.mnuFileZoomAspectRatio_Click);
            //
            // mnuFileRotate
            //
            this.mnuFileRotate.Name = "mnuFileRotate";
            this.mnuFileRotate.Text = "Rotate";
            this.mnuFileRotate.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuFileRotate.Click += new System.EventHandler(this.mnuFileRotate_Click);
            //
            // mnuFilePreviousPage
            //
            this.mnuFilePreviousPage.Name = "mnuFilePreviousPage";
            this.mnuFilePreviousPage.Text = "Previous Page";
            this.mnuFilePreviousPage.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuFilePreviousPage.Click += new System.EventHandler(this.mnuFilePreviousPage_Click);
            //
            // mnuFileNextPage
            //
            this.mnuFileNextPage.Name = "mnuFileNextPage";
            this.mnuFileNextPage.Text = "Next Page";
            this.mnuFileNextPage.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuFileNextPage.Click += new System.EventHandler(this.mnuFileNextPage_Click);
            //
            // mnuSeperator
            //
            this.mnuSeperator.Name = "mnuSeperator";
            this.mnuSeperator.Text = "-";
            //
            // mnuProcessSave
            //
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            //
            // Seperator
            //
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            //
            // mnuProcessQuit
            //
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            //
            // MainMenu1
            //
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] { this.mnuProcess });
            //
            // frmScanDocument
            //
            this.ClientSize = new System.Drawing.Size(610, 467);
            this.ClientArea.Controls.Add(this.Scanner1);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmdUpdatePage);
            this.ClientArea.Controls.Add(this.cmdDeletePage);
            this.ClientArea.Controls.Add(this.lblCurrentPage);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.lblTotalPage);
            this.ClientArea.Controls.Add(this.Label3);
            this.Menu = this.MainMenu1;
            this.Name = "frmScanDocument";
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.MinimizeBox = true;
            this.MaximizeBox = true;
            this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
            //this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmScanDocument.Icon")));
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Activated += new System.EventHandler(this.frmScanDocument_Activated);
            this.Load += new System.EventHandler(this.frmScanDocument_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmScanDocument_KeyPress);
            this.Text = "Scan Image";
            ((System.ComponentModel.ISupportInitialize)(this.Scanner1)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.Frame3.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            this.Frame4.ResumeLayout(false);
            this.SSTab1_Page2.ResumeLayout(false);
            this.SSTab1.ResumeLayout(false);
            this.Frame2.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        #endregion
    }
}