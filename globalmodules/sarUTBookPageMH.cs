//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
    /// <summary>
    /// Summary description for sarUTBookPageMH.
    /// </summary>
    public partial class sarUTBookPageMH : FCSectionReport
    {

        public sarUTBookPageMH()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "sarUTBookPageAccount";
        }

        public static sarUTBookPageMH InstancePtr
        {
            get
            {
                return (sarUTBookPageMH)Sys.GetInstance(typeof(sarUTBookPageMH));
            }
        }

        protected sarUTBookPageMH _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsData.Dispose();
            }
            base.Dispose(disposing);
        }
        // nObj = 1
        //   0	sarUTBookPageMH	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               12/04/2006              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               12/04/2006              *
        // ********************************************************
        int lngAcct;
        clsDRWrapper rsData = new clsDRWrapper();
        string strWS;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
			eArgs.EOF = rsData.EndOfFile();
        }

        private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
        {
            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        this.Close();
                        break;
                    }
            } //end switch
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            string strSQL;
            //modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
            strWS = sarUTBookPageAccount.InstancePtr.strWS;

            lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
            strSQL = "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Module = 'UT' AND Account = " + FCConvert.ToString(lngAcct);
            rsData.OpenRecordset(strSQL, "CentralData");
            if (rsData.EndOfFile())
            {
                fldBookPage.Text = ""; // No Matches"
                lblHeader.Visible = false;
                GroupHeader1.Height = 0;
            }
        }

		private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void BindFields()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                if (!rsData.EndOfFile())
                {
                    fldBookPage.Text = FCConvert.ToString(rsData.Get_Fields_String("BookPage"));
                    fldMHName.Text = FCConvert.ToString(rsData.Get_Fields_String("Name"));
                    rsData.MoveNext();
                }
                return;
            }
            catch
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error In Book Page Report - MH", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void sarUTBookPageMH_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //sarUTBookPageMH properties;
            //sarUTBookPageMH.Caption	= "Account Detail";
            //sarUTBookPageMH.Icon	= "sarUTBookPageMH.dsx":0000";
            //sarUTBookPageMH.Left	= 0;
            //sarUTBookPageMH.Top	= 0;
            //sarUTBookPageMH.Width	= 11880;
            //sarUTBookPageMH.Height	= 8595;
            //sarUTBookPageMH.StartUpPosition	= 3;
            //sarUTBookPageMH.SectionData	= "sarUTBookPageMH.dsx":058A;
            //End Unmaped Properties
        }
    }
}
