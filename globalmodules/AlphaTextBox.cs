﻿using fecherFoundation;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWFA0000
using TWFA0000;


#elif TWAR0000
using TWAR0000;


#elif TWPP0000
using TWPP0000;


#elif TWCK0000
using modGlobal = TWCK0000.modGNBas;


#elif TWMV0000
using modGlobal = TWMV0000.MotorVehicle;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalVariables;
#endif
namespace Global
{
	public class clsAlphaTextBox
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// Date           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               11/17/2002              *
		// ********************************************************
		// All of a forms textboxes should be of this type. That
		// way the code for any textbox event can be
		// located in one central place (versus the same
		// code in every GotFocus, ... event).
		//
		// By using the "WithEvents" keyword, all events that occur
		// within the textboxes are passed to this class where
		// they can be handled or not.
		public FCTextBox TextBox;
		//public AxMSMask.AxMaskEdBox MaskBox = new AxMSMask.AxMaskEdBox();
		public FCComboBox cboComboBox;
		public FCCheckBox chkCheckBox;
		public FCFrame Frame;
		public FCLabel Label;
		public FCButton CommandButton;

		public delegate void ControlHasChangedEventHandler();

		public event ControlHasChangedEventHandler ControlHasChanged;
		// VBto upgrade warning: pFormName As Form	OnWrite(object)
		private Form pFormName = null;
		private bool pboolUseRoutine;

		public void Visible(ref bool State)
		{
			Label.Visible = State;
		}

		private void cboComboBox_Click()
		{
			TextBox_Change();
		}

		private void cboComboBox_KeyPress(ref short KeyAscii)
		{

		}

		private void chkCheckBox_Click()
		{
			TextBox_Change();
		}

		public clsAlphaTextBox() : base()
		{
			//pFormName = modGlobal.Statics.gobjFormName;
			pboolUseRoutine = true;
		}

		~clsAlphaTextBox()
		{
			TextBox = null;
			//MaskBox = null;
			cboComboBox = null;
			chkCheckBox = null;
			Frame = null;
			Label = null;
			CommandButton = null;
			pFormName = null;
		}

		//private void Label_MouseMove(ref short Button, ref short Shift, ref float x, ref float Y)
		//{
		//	/*? On Error Resume Next  */
		//	modGlobal.Statics.gobjLabel.BorderStyle = 0;
		//	modGlobal.Statics.gobjLabel.Appearance = FCLabel.AppearanceConstants.cc3D;
		//	if (Label.BorderStyle == 0)
		//	{
		//		Label.BorderStyle = 1;
		//		Label.Appearance = 0;
		//		modGlobal.Statics.gobjLabel = Label;
		//	}
		//}

		private void MaskBox_Change()
		{
			TextBox_Change();
		}

		private void TextBox_Change()
		{
			// Echo back what the user entered. Since the textbox is an
			// object, we must reference the appropriate properties.
			// this line will allow me to use the code in the form and not this class
			if (!pboolUseRoutine)
				return;
			/*? On Error Resume Next  */
			FCUtils.CallByName(App.MainForm, "ControlChanged", CallType.Method);
		}

		private void TextBox_GotFocus()
		{
			// Highlight the text.
			TextBox.SelectionStart = 0;
			TextBox.SelectionLength = TextBox.Text.Length;
		}

		private void TextBox_LostFocus()
		{
			// TextBox.Text = Trim$(TextBox.Text)
		}
		// VBto upgrade warning: 'Return' As Variant --> As Form
		public Form ActiveForm
		{
			get
			{
				Form ActiveForm = null;
				ActiveForm = pFormName;
				return ActiveForm;
			}
			// VBto upgrade warning: NewValue As object	OnRead(Form)
			set
			{
				pFormName = value;
			}
		}
		// VBto upgrade warning: 'Return' As Variant --> As bool
		public bool UseRoutine
		{
			get
			{
				bool UseRoutine = false;
				UseRoutine = pboolUseRoutine;
				return UseRoutine;
			}
			// VBto upgrade warning: NewValue As Variant --> As bool
			set
			{
				pboolUseRoutine = value;
			}
		}
	}
}
