﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailContacts.
	/// </summary>
	partial class frmEmailContacts : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtEMail;
		public fecherFoundation.FCTextBox txtQuickname;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCTextBox txtCompany;
		public fecherFoundation.FCTextBox txtPosition;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolBar Toolbar1;
		private fecherFoundation.FCToolBarButton btnSave;
		private fecherFoundation.FCToolBarButton btnNew;
		private fecherFoundation.FCToolBarButton btnDelete;
		private fecherFoundation.FCToolBarButton btnSearch;
		private fecherFoundation.FCToolBarButton btnAddToGroup;
		private fecherFoundation.FCToolBarButton btnEditGroups;
		public Wisej.Web.ImageList ImageList1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddToGroup;
		public fecherFoundation.FCToolStripMenuItem mnuGroups;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmailContacts));
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtEMail = new fecherFoundation.FCTextBox();
			this.txtQuickname = new fecherFoundation.FCTextBox();
			this.txtFirstName = new fecherFoundation.FCTextBox();
			this.txtLastName = new fecherFoundation.FCTextBox();
			this.txtCompany = new fecherFoundation.FCTextBox();
			this.txtPosition = new fecherFoundation.FCTextBox();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Toolbar1 = new fecherFoundation.FCToolBar();
			this.btnSave = new fecherFoundation.FCToolBarButton();
			this.btnNew = new fecherFoundation.FCToolBarButton();
			this.btnDelete = new fecherFoundation.FCToolBarButton();
			this.btnSearch = new fecherFoundation.FCToolBarButton();
			this.btnAddToGroup = new fecherFoundation.FCToolBarButton();
			this.btnEditGroups = new fecherFoundation.FCToolBarButton();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddToGroup = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGroups = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdNewContact = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewContact)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 522);
			this.BottomPanel.Size = new System.Drawing.Size(582, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			//FC:FINAL:MSH - i.issue #1597: remove toolbar
			//this.ClientArea.Controls.Add(this.Toolbar1);
			this.ClientArea.Location = new System.Drawing.Point(0, 61);
			this.ClientArea.Size = new System.Drawing.Size(582, 461);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSearch);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdNewContact);
			this.TopPanel.Size = new System.Drawing.Size(582, 61);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNewContact, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(26, 26);
			this.HeaderText.Size = new System.Drawing.Size(109, 30);
			this.HeaderText.Text = "Contacts";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.txtEMail);
			this.Frame1.Controls.Add(this.txtQuickname);
			this.Frame1.Controls.Add(this.txtFirstName);
			this.Frame1.Controls.Add(this.txtLastName);
			this.Frame1.Controls.Add(this.txtCompany);
			this.Frame1.Controls.Add(this.txtPosition);
			this.Frame1.Controls.Add(this.txtDescription);
			this.Frame1.Controls.Add(this.Label7);
			this.Frame1.Controls.Add(this.Label6);
			this.Frame1.Controls.Add(this.Label5);
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(0, 0);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(570, 450);
			this.Frame1.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// txtEMail
			// 
			this.txtEMail.AutoSize = false;
			this.txtEMail.BackColor = System.Drawing.SystemColors.Info;
			this.txtEMail.Location = new System.Drawing.Point(190, 30);
			this.txtEMail.Name = "txtEMail";
			this.txtEMail.Size = new System.Drawing.Size(360, 40);
			this.txtEMail.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtEMail, null);
			this.txtEMail.Validating += new System.ComponentModel.CancelEventHandler(this.txtEMail_Validating);
			// 
			// txtQuickname
			// 
			this.txtQuickname.AutoSize = false;
			this.txtQuickname.BackColor = System.Drawing.SystemColors.Info;
			this.txtQuickname.Location = new System.Drawing.Point(190, 150);
			this.txtQuickname.Name = "txtQuickname";
			this.txtQuickname.Size = new System.Drawing.Size(360, 40);
			this.txtQuickname.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtQuickname, "Enter a quickname that can be used as an alias instead of typing the e-mail addre" + "ss");
			this.txtQuickname.Validating += new System.ComponentModel.CancelEventHandler(this.txtQuickname_Validating);
			// 
			// txtFirstName
			// 
			this.txtFirstName.AutoSize = false;
			this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFirstName.Location = new System.Drawing.Point(190, 210);
			this.txtFirstName.Name = "txtFirstName";
			this.txtFirstName.Size = new System.Drawing.Size(360, 40);
			this.txtFirstName.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtFirstName, null);
			this.txtFirstName.Validating += new System.ComponentModel.CancelEventHandler(this.txtFirstName_Validating);
			// 
			// txtLastName
			// 
			this.txtLastName.AutoSize = false;
			this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
			this.txtLastName.Location = new System.Drawing.Point(190, 270);
			this.txtLastName.Name = "txtLastName";
			this.txtLastName.Size = new System.Drawing.Size(360, 40);
			this.txtLastName.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtLastName, null);
			this.txtLastName.Validating += new System.ComponentModel.CancelEventHandler(this.txtLastName_Validating);
			// 
			// txtCompany
			// 
			this.txtCompany.AutoSize = false;
			this.txtCompany.BackColor = System.Drawing.SystemColors.Window;
			this.txtCompany.Location = new System.Drawing.Point(190, 330);
			this.txtCompany.Name = "txtCompany";
			this.txtCompany.Size = new System.Drawing.Size(360, 40);
			this.txtCompany.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtCompany, null);
			this.txtCompany.Validating += new System.ComponentModel.CancelEventHandler(this.txtCompany_Validating);
			// 
			// txtPosition
			// 
			this.txtPosition.AutoSize = false;
			this.txtPosition.BackColor = System.Drawing.SystemColors.Window;
			this.txtPosition.Location = new System.Drawing.Point(190, 390);
			this.txtPosition.Name = "txtPosition";
			this.txtPosition.Size = new System.Drawing.Size(360, 40);
			this.txtPosition.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtPosition, null);
			this.txtPosition.Validating += new System.ComponentModel.CancelEventHandler(this.txtPosition_Validating);
			// 
			// txtDescription
			// 
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Location = new System.Drawing.Point(190, 90);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(360, 40);
			this.txtDescription.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtDescription, "Enter a description to describe the address such as: Personal or Work Address");
			this.txtDescription.Validating += new System.ComponentModel.CancelEventHandler(this.txtDescription_Validating);
			// 
			// Label7
			// 
			this.Label7.AutoSize = true;
			this.Label7.Location = new System.Drawing.Point(30, 404);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(67, 15);
			this.Label7.TabIndex = 12;
			this.Label7.Text = "POSITION";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label7, null);
			// 
			// Label6
			// 
			this.Label6.AutoSize = true;
			this.Label6.Location = new System.Drawing.Point(30, 344);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(70, 15);
			this.Label6.TabIndex = 10;
			this.Label6.Text = "COMPANY";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label6, null);
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.Location = new System.Drawing.Point(30, 284);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(77, 15);
			this.Label5.TabIndex = 8;
			this.Label5.Text = "LAST NAME";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label4
			// 
			this.Label4.AutoSize = true;
			this.Label4.Location = new System.Drawing.Point(30, 224);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(82, 15);
			this.Label4.TabIndex = 6;
			this.Label4.Text = "FIRST NAME";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.Location = new System.Drawing.Point(30, 164);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(82, 15);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "QUICKNAME";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(92, 15);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "DESCRIPTION";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(112, 15);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "E-MAIL ADDRESS";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// Toolbar1
			// 
			//////this.Toolbar1.BackColor = System.Drawing.SystemColors.Control;
			this.Toolbar1.ButtonHeight = 0;
			this.Toolbar1.Buttons.Add(this.btnSave);
			this.Toolbar1.Buttons.Add(this.btnNew);
			this.Toolbar1.Buttons.Add(this.btnDelete);
			this.Toolbar1.Buttons.Add(this.btnSearch);
			this.Toolbar1.Buttons.Add(this.btnAddToGroup);
			this.Toolbar1.Buttons.Add(this.btnEditGroups);
			this.Toolbar1.Buttons.Add(this.btnSave);
			this.Toolbar1.Buttons.Add(this.btnNew);
			this.Toolbar1.Buttons.Add(this.btnDelete);
			this.Toolbar1.Buttons.Add(this.btnSearch);
			this.Toolbar1.Buttons.Add(this.btnAddToGroup);
			this.Toolbar1.Buttons.Add(this.btnEditGroups);
			this.Toolbar1.ButtonSize = new System.Drawing.Size(24, 24);
			this.Toolbar1.ButtonWidth = 0;
			this.Toolbar1.Dock = Wisej.Web.DockStyle.None;
			this.Toolbar1.ImageList = this.ImageList1;
			this.Toolbar1.Location = new System.Drawing.Point(0, 0);
			this.Toolbar1.Name = "Toolbar1";
			this.Toolbar1.Size = new System.Drawing.Size(100, 26);
			this.Toolbar1.TabIndex = 0;
			this.Toolbar1.TabStop = false;
			this.ToolTip1.SetToolTip(this.Toolbar1, null);
			this.Toolbar1.ButtonClick += new System.EventHandler<fecherFoundation.FCToolBarButtonClickEventArgs>(this.Toolbar1_ButtonClick);
			// 
			// btnSave
			// 
			this.btnSave.ImageIndex = 1;
			this.btnSave.Key = "btnSave";
			this.btnSave.Name = "btnSave";
			this.btnSave.ToolTipText = "Save Contact";
			this.btnSave.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnNew
			// 
			this.btnNew.ImageIndex = 0;
			this.btnNew.Key = "btnNew";
			this.btnNew.Name = "btnNew";
			this.btnNew.ToolTipText = "Add New Contact";
			this.btnNew.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnDelete
			// 
			this.btnDelete.ImageIndex = 2;
			this.btnDelete.Key = "btnDelete";
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.ToolTipText = "Delete Contact";
			this.btnDelete.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnSearch
			// 
			this.btnSearch.ImageIndex = 3;
			this.btnSearch.Key = "btnSearch";
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.ToolTipText = "Search Contacts";
			this.btnSearch.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnAddToGroup
			// 
			this.btnAddToGroup.ImageIndex = 4;
			this.btnAddToGroup.Key = "btnAddToGroup";
			this.btnAddToGroup.Name = "btnAddToGroup";
			this.btnAddToGroup.ToolTipText = "Add Contact to a Group";
			this.btnAddToGroup.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditGroups
			// 
			this.btnEditGroups.ImageIndex = 5;
			this.btnEditGroups.Key = "btnEditGroups";
			this.btnEditGroups.Name = "btnEditGroups";
			this.btnEditGroups.ToolTipText = "Groups";
			this.btnEditGroups.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// ImageList1
			// 
			this.ImageList1.ImageSize = new System.Drawing.Size(24, 24);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcess
			});
			this.MainMenu1.Name = null;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAddToGroup,
				this.mnuGroups
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuAddToGroup
			// 
			this.mnuAddToGroup.Index = 0;
			this.mnuAddToGroup.Name = "mnuAddToGroup";
			this.mnuAddToGroup.Text = "Add To Group";
			this.mnuAddToGroup.Click += new System.EventHandler(this.mnuAddToGroup_Click);
			// 
			// mnuGroups
			// 
			this.mnuGroups.Index = 1;
			this.mnuGroups.Name = "mnuGroups";
			this.mnuGroups.Text = "Groups";
			this.mnuGroups.Click += new System.EventHandler(this.mnuGroups_Click);
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(251, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(98, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save";
			this.ToolTip1.SetToolTip(this.btnProcess, null);
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdNewContact
			// 
			this.cmdNewContact.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNewContact.AppearanceKey = "toolbarButton";
			this.cmdNewContact.Location = new System.Drawing.Point(229, 29);
			this.cmdNewContact.Name = "cmdNewContact";
			this.cmdNewContact.Size = new System.Drawing.Size(94, 24);
			this.cmdNewContact.TabIndex = 52;
			this.cmdNewContact.Text = "Add Contact";
			this.ToolTip1.SetToolTip(this.cmdNewContact, null);
			this.cmdNewContact.Click += new System.EventHandler(this.cmdNewContact_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(327, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(105, 24);
			this.cmdDelete.TabIndex = 53;
			this.cmdDelete.Text = "Delete Contact";
			this.ToolTip1.SetToolTip(this.cmdDelete, null);
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSearch.AppearanceKey = "toolbarButton";
			this.cmdSearch.ImageSource = "icon_search";
			this.cmdSearch.Location = new System.Drawing.Point(435, 29);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(110, 24);
			this.cmdSearch.TabIndex = 54;
			this.cmdSearch.Text = "Find Contact";
			this.ToolTip1.SetToolTip(this.cmdSearch, null);
			//FC:FINAL:MSH - i.issue #1597: assign missing handler
			this.cmdSearch.Click += new EventHandler(this.mnuSearch_Click);
			// 
			// frmEmailContacts
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(582, 630);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmEmailContacts";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Contacts";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmEmailContacts_Load);
			this.Activated += new System.EventHandler(this.frmEmailContacts_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmailContacts_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewContact)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdDelete;
		public FCButton cmdNewContact;
		public FCButton cmdSearch;
	}
}
