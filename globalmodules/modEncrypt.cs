﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	public class modEncrypt
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// Date           :                                       *
		// *
		// MODIFIED BY    :               Dave Wade               *
		// Last Updated   :               12/14/2001              *
		// ********************************************************
		public static string ToggleEncryptCode(string strText = "")
		{
			string ToggleEncryptCode = "";
			int intCounter;
			string strNewString = "";
			strText = Strings.UCase(strText);
			if (strText != "")
			{
				for (intCounter = strText.Length; intCounter >= 1; intCounter--)
				{
					strNewString += FCConvert.ToString(Convert.ToChar(Convert.ToByte(Strings.Mid(strText, intCounter, 1)[0]) ^ 240));
				}
			}
			ToggleEncryptCode = strNewString;
			return ToggleEncryptCode;
		}
	}
}
