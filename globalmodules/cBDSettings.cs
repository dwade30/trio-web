﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cBDSettings
	{
		//=========================================================
		private string strDue = string.Empty;
		private string strDueTo = string.Empty;
		private string strDueFrom = string.Empty;
		private string strCashDue = string.Empty;
		private string strUseProjectNumber = string.Empty;
		private string strFiscalStart = string.Empty;
		private string strSummaryReportOption = string.Empty;
		private string strWarrantPrevOutput = string.Empty;
		private string strWarrantPrevSigLine = "";
		private string strCheckRef = string.Empty;
		private string strPreviewPrint = "";
		private string strWarrantPrintOption = string.Empty;
		private string strAPSeq = string.Empty;
		private string strEOYStatus = string.Empty;
		private string strExpenditure = string.Empty;
		private string strRevenue = string.Empty;
		private string strLedger = string.Empty;
		private string strValidAccountUse = string.Empty;
		private string strCheckRec = "";
		private bool boolValidAccountBudgetProcess;
		private string strPayDate = "";
		private string strStatementDate = "";
		private int lngCheckFormat;
		private bool bool1099Laser;
		private double dblLaserAdjustment;
		private double dbl1099LaserAdjustment;
		private double dblCheckLaserAdjustment;
		private double dblReportAdjustment;
		private string strShadeReports = string.Empty;
		private bool boolShowIcon;
		private string strCheckIcon = string.Empty;
		private bool boolWarrantPrevDeptSummary;
		private bool boolLandscapeWarrant;
		private bool boolPrintSignature;
		private double dblLabelLaserAdjustment;
		private bool boolAllowManualWarrantNumbers;
		private bool boolConvertedCityStateZip;
		private bool boolRecapOrderByAccounts;
		private bool boolSameVendorOnly;
		private bool boolFixedPastYearBudget;
		private int intBalanceValidation;
		private bool boolUseTownSeal;
		private bool boolEncByDept;
		private bool boolAutoNumberPO;
		private double dbl1099LaserAdjustmentHorizontal;
		private bool boolExcludeAPDueToFrom;
		private bool boolExcludeCRDueToFrom;
		private bool boolExcludePYDueToFrom;
		private bool boolAutomaticInterest;
		private bool boolPUCChartOfAccounts;
		private string strImmediateOriginODFINumber = string.Empty;
		private string strImmediateOriginName = string.Empty;
		private string strImmediateOriginRoutingNumber = string.Empty;
		private string strImmediateDestinationName = string.Empty;
		private string strImmediateDestinationRoutingNumber = string.Empty;
		private string strACHTownName = string.Empty;
		private string strACHFederalID = string.Empty;
		private bool boolBalancedACHFile;
		private int intACHFederalIDPrefix;
		private int intEncCarryForward;
		private string strReturnAddress1 = string.Empty;
		private string strReturnAddress2 = string.Empty;
		private string strReturnAddress3 = string.Empty;
		private string strReturnAddress4 = string.Empty;
		private bool boolAutoPostAP;
		private bool boolAutoPostGJ;
		private bool boolAutoPostEN;
		private bool boolAutoPostCR;
		private bool boolAutoPostCD;
		private bool boolAutoPostCM;
		private bool boolAutoPostAPCorr;
		private bool boolAutoPostReversingJournals;
		private bool boolAutoPostBudgetTransfer;
		private bool boolAutoPostCreateOpeningAdjustments;
		private bool boolAutoPostARBills;
		private bool boolAutoPostCRDailyAudit;
		private bool boolAutoPostFADepreciation;
		private bool boolAutoPostMARR;
		private bool boolAutoPostPYProcesses;
		private bool boolAutoPostCLLiens;
		private bool boolAutoPostCLTA;
		private bool boolAutoPostBLCommitments;
		private bool boolAutoPostBLSupplementals;
		private bool boolAutoPostUBBills;
		private bool boolAutoPostUBLiens;
		private string strVersion = string.Empty;
		private int lngRecordID;
		private bool boolUpdated;
		private bool boolDeleted;

		public string Due
		{
			set
			{
				strDue = value;
				IsUpdated = true;
			}
			get
			{
				string Due = "";
				Due = strDue;
				return Due;
			}
		}

		public string DueTo
		{
			set
			{
				strDueTo = value;
				IsUpdated = true;
			}
			get
			{
				string DueTo = "";
				DueTo = strDueTo;
				return DueTo;
			}
		}

		public string DueFrom
		{
			set
			{
				strDueFrom = value;
				IsUpdated = true;
			}
			get
			{
				string DueFrom = "";
				DueFrom = strDueFrom;
				return DueFrom;
			}
		}

		public string CashDue
		{
			set
			{
				strCashDue = value;
				IsUpdated = true;
			}
			get
			{
				string CashDue = "";
				CashDue = strCashDue;
				return CashDue;
			}
		}

		public string UseProjectNumber
		{
			set
			{
				strUseProjectNumber = value;
				IsUpdated = true;
			}
			get
			{
				string UseProjectNumber = "";
				UseProjectNumber = strUseProjectNumber;
				return UseProjectNumber;
			}
		}

		public string FiscalStart
		{
			set
			{
				strFiscalStart = value;
				IsUpdated = true;
			}
			get
			{
				string FiscalStart = "";
				FiscalStart = strFiscalStart;
				return FiscalStart;
			}
		}

		public string SummaryReportOption
		{
			set
			{
				strSummaryReportOption = value;
				IsUpdated = true;
			}
			get
			{
				string SummaryReportOption = "";
				SummaryReportOption = strSummaryReportOption;
				return SummaryReportOption;
			}
		}

		public string WarrantPrevOutput
		{
			set
			{
				strWarrantPrevOutput = value;
				IsUpdated = true;
			}
			get
			{
				string WarrantPrevOutput = "";
				WarrantPrevOutput = strWarrantPrevOutput;
				return WarrantPrevOutput;
			}
		}

		public string WarrantPrintOption
		{
			set
			{
				strWarrantPrintOption = value;
				IsUpdated = true;
			}
			get
			{
				string WarrantPrintOption = "";
				WarrantPrintOption = strWarrantPrintOption;
				return WarrantPrintOption;
			}
		}

		public string APSeq
		{
			set
			{
				strAPSeq = value;
				IsUpdated = true;
			}
			get
			{
				string APSeq = "";
				APSeq = strAPSeq;
				return APSeq;
			}
		}

		public string EOYStatus
		{
			set
			{
				strEOYStatus = value;
				IsUpdated = true;
			}
			get
			{
				string EOYStatus = "";
				EOYStatus = strEOYStatus;
				return EOYStatus;
			}
		}

		public string Expenditure
		{
			set
			{
				strExpenditure = value;
				IsUpdated = true;
			}
			get
			{
				string Expenditure = "";
				Expenditure = strExpenditure;
				return Expenditure;
			}
		}

		public string Revenue
		{
			set
			{
				strRevenue = value;
				IsUpdated = true;
			}
			get
			{
				string Revenue = "";
				Revenue = strRevenue;
				return Revenue;
			}
		}

		public string Ledger
		{
			set
			{
				strLedger = value;
				IsUpdated = true;
			}
			get
			{
				string Ledger = "";
				Ledger = strLedger;
				return Ledger;
			}
		}

		public string ValidAccountUse
		{
			set
			{
				strValidAccountUse = value;
				IsUpdated = true;
			}
			get
			{
				string ValidAccountUse = "";
				ValidAccountUse = strValidAccountUse;
				return ValidAccountUse;
			}
		}

		public string CheckRef
		{
			set
			{
				strCheckRef = value;
				IsUpdated = true;
			}
			get
			{
				string CheckRef = "";
				CheckRef = strCheckRef;
				return CheckRef;
			}
		}

		public bool ValidAccountBudgetProcess
		{
			set
			{
				boolValidAccountBudgetProcess = value;
				IsUpdated = true;
			}
			get
			{
				bool ValidAccountBudgetProcess = false;
				ValidAccountBudgetProcess = boolValidAccountBudgetProcess;
				return ValidAccountBudgetProcess;
			}
		}

		public string PayDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strPayDate = value;
				}
				else
				{
					strPayDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string PayDate = "";
				PayDate = strPayDate;
				return PayDate;
			}
		}

		public string StatementDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strStatementDate = value;
				}
				else
				{
					strStatementDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string StatementDate = "";
				StatementDate = strStatementDate;
				return StatementDate;
			}
		}

		public int CheckFormat
		{
			set
			{
				lngCheckFormat = value;
				IsUpdated = true;
			}
			get
			{
				int CheckFormat = 0;
				CheckFormat = lngCheckFormat;
				return CheckFormat;
			}
		}

		public bool Use1099Laser
		{
			set
			{
				bool1099Laser = value;
				IsUpdated = true;
			}
			get
			{
				bool Use1099Laser = false;
				Use1099Laser = bool1099Laser;
				return Use1099Laser;
			}
		}

		public double LaserAdjustment
		{
			set
			{
				dblLaserAdjustment = value;
				IsUpdated = true;
			}
			get
			{
				double LaserAdjustment = 0;
				LaserAdjustment = dblLaserAdjustment;
				return LaserAdjustment;
			}
		}

		public double LaserAdjustment1099
		{
			set
			{
				dbl1099LaserAdjustment = value;
				IsUpdated = true;
			}
			get
			{
				double LaserAdjustment1099 = 0;
				LaserAdjustment1099 = dbl1099LaserAdjustment;
				return LaserAdjustment1099;
			}
		}

		public double CheckLaserAdjustment
		{
			set
			{
				dblCheckLaserAdjustment = value;
				IsUpdated = true;
			}
			get
			{
				double CheckLaserAdjustment = 0;
				CheckLaserAdjustment = dblCheckLaserAdjustment;
				return CheckLaserAdjustment;
			}
		}

		public double ReportAdjustment
		{
			set
			{
				dblReportAdjustment = value;
				IsUpdated = true;
			}
			get
			{
				double ReportAdjustment = 0;
				return ReportAdjustment;
			}
		}

		public string ShadeReports
		{
			set
			{
				strShadeReports = value;
				IsUpdated = true;
			}
			get
			{
				string ShadeReports = "";
				ShadeReports = strShadeReports;
				return ShadeReports;
			}
		}

		public bool ShowIcon
		{
			set
			{
				boolShowIcon = value;
				IsUpdated = true;
			}
			get
			{
				bool ShowIcon = false;
				ShowIcon = boolShowIcon;
				return ShowIcon;
			}
		}

		public string CheckIcon
		{
			set
			{
				strCheckIcon = value;
				IsUpdated = true;
			}
			get
			{
				string CheckIcon = "";
				CheckIcon = strCheckIcon;
				return CheckIcon;
			}
		}

		public bool WarrantPrevDeptSummary
		{
			set
			{
				boolWarrantPrevDeptSummary = value;
				IsUpdated = true;
			}
			get
			{
				bool WarrantPrevDeptSummary = false;
				WarrantPrevDeptSummary = boolWarrantPrevDeptSummary;
				return WarrantPrevDeptSummary;
			}
		}

		public bool LandscapeWarrant
		{
			set
			{
				boolLandscapeWarrant = value;
				IsUpdated = true;
			}
			get
			{
				bool LandscapeWarrant = false;
				LandscapeWarrant = boolLandscapeWarrant;
				return LandscapeWarrant;
			}
		}

		public bool PrintSignature
		{
			set
			{
				boolPrintSignature = value;
				IsUpdated = true;
			}
			get
			{
				bool PrintSignature = false;
				PrintSignature = boolPrintSignature;
				return PrintSignature;
			}
		}

		public double LabelLaserAdjustment
		{
			set
			{
				dblLabelLaserAdjustment = value;
				IsUpdated = true;
			}
			get
			{
				double LabelLaserAdjustment = 0;
				LabelLaserAdjustment = dblLabelLaserAdjustment;
				return LabelLaserAdjustment;
			}
		}

		public bool AllowManualWarrantNumbers
		{
			set
			{
				boolAllowManualWarrantNumbers = value;
				IsUpdated = true;
			}
			get
			{
				bool AllowManualWarrantNumbers = false;
				AllowManualWarrantNumbers = boolAllowManualWarrantNumbers;
				return AllowManualWarrantNumbers;
			}
		}

		public bool ConvertedCityStateZip
		{
			set
			{
				boolConvertedCityStateZip = value;
				IsUpdated = true;
			}
			get
			{
				bool ConvertedCityStateZip = false;
				ConvertedCityStateZip = boolConvertedCityStateZip;
				return ConvertedCityStateZip;
			}
		}

		public bool RecapOrderByAccounts
		{
			set
			{
				boolRecapOrderByAccounts = value;
				IsUpdated = true;
			}
			get
			{
				bool RecapOrderByAccounts = false;
				RecapOrderByAccounts = boolRecapOrderByAccounts;
				return RecapOrderByAccounts;
			}
		}

		public bool SameVendorOnly
		{
			set
			{
				boolSameVendorOnly = value;
				IsUpdated = true;
			}
			get
			{
				bool SameVendorOnly = false;
				SameVendorOnly = boolSameVendorOnly;
				return SameVendorOnly;
			}
		}

		public bool FixedPastYearBudget
		{
			set
			{
				boolFixedPastYearBudget = value;
				IsUpdated = true;
			}
			get
			{
				bool FixedPastYearBudget = false;
				FixedPastYearBudget = boolFixedPastYearBudget;
				return FixedPastYearBudget;
			}
		}

		public short BalanceValidation
		{
			set
			{
				intBalanceValidation = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short BalanceValidation = 0;
				BalanceValidation = FCConvert.ToInt16(intBalanceValidation);
				return BalanceValidation;
			}
		}

		public bool UseTownSeal
		{
			set
			{
				boolUseTownSeal = value;
				IsUpdated = true;
			}
			get
			{
				bool UseTownSeal = false;
				UseTownSeal = boolUseTownSeal;
				return UseTownSeal;
			}
		}

		public bool EncByDept
		{
			set
			{
				boolEncByDept = value;
				IsUpdated = true;
			}
			get
			{
				bool EncByDept = false;
				EncByDept = boolEncByDept;
				return EncByDept;
			}
		}

		public bool AutoNumberPO
		{
			set
			{
				boolAutoNumberPO = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoNumberPO = false;
				AutoNumberPO = boolAutoNumberPO;
				return AutoNumberPO;
			}
		}

		public double LaserAdjustment1099Horizontal
		{
			set
			{
				dbl1099LaserAdjustmentHorizontal = value;
				IsUpdated = true;
			}
			get
			{
				double LaserAdjustment1099Horizontal = 0;
				LaserAdjustment1099Horizontal = dbl1099LaserAdjustmentHorizontal;
				return LaserAdjustment1099Horizontal;
			}
		}

		public bool ExcludeAPDueToFrom
		{
			set
			{
				boolExcludeAPDueToFrom = value;
				IsUpdated = true;
			}
			get
			{
				bool ExcludeAPDueToFrom = false;
				ExcludeAPDueToFrom = boolExcludeAPDueToFrom;
				return ExcludeAPDueToFrom;
			}
		}

		public bool ExcludeCRDueToFrom
		{
			set
			{
				boolExcludeCRDueToFrom = value;
				IsUpdated = true;
			}
			get
			{
				bool ExcludeCRDueToFrom = false;
				ExcludeCRDueToFrom = boolExcludeCRDueToFrom;
				return ExcludeCRDueToFrom;
			}
		}

		public bool ExcludePYDueToFrom
		{
			set
			{
				boolExcludePYDueToFrom = value;
				IsUpdated = true;
			}
			get
			{
				bool ExcludePYDueToFrom = false;
				ExcludePYDueToFrom = boolExcludePYDueToFrom;
				return ExcludePYDueToFrom;
			}
		}

		public bool AutomaticInterest
		{
			set
			{
				boolAutomaticInterest = value;
				IsUpdated = true;
			}
			get
			{
				bool AutomaticInterest = false;
				AutomaticInterest = boolAutomaticInterest;
				return AutomaticInterest;
			}
		}

		public bool UsePUCChartOfAccounts
		{
			set
			{
				boolPUCChartOfAccounts = value;
				IsUpdated = true;
			}
			get
			{
				bool UsePUCChartOfAccounts = false;
				UsePUCChartOfAccounts = boolPUCChartOfAccounts;
				return UsePUCChartOfAccounts;
			}
		}

		public string ImmediateOriginODFINumber
		{
			set
			{
				strImmediateOriginODFINumber = value;
				IsUpdated = true;
			}
			get
			{
				string ImmediateOriginODFINumber = "";
				ImmediateOriginODFINumber = strImmediateOriginODFINumber;
				return ImmediateOriginODFINumber;
			}
		}

		public string ImmediateOriginName
		{
			set
			{
				strImmediateOriginName = value;
				IsUpdated = true;
			}
			get
			{
				string ImmediateOriginName = "";
				ImmediateOriginName = strImmediateOriginName;
				return ImmediateOriginName;
			}
		}

		public string ImmediateOriginRoutingNumber
		{
			set
			{
				strImmediateOriginRoutingNumber = value;
				IsUpdated = true;
			}
			get
			{
				string ImmediateOriginRoutingNumber = "";
				ImmediateOriginRoutingNumber = strImmediateOriginRoutingNumber;
				return ImmediateOriginRoutingNumber;
			}
		}

		public string ImmediateDestinationName
		{
			set
			{
				strImmediateDestinationName = value;
				IsUpdated = true;
			}
			get
			{
				string ImmediateDestinationName = "";
				ImmediateDestinationName = strImmediateDestinationName;
				return ImmediateDestinationName;
			}
		}

		public string ImmediateDestinationRoutingNumber
		{
			set
			{
				strImmediateDestinationRoutingNumber = value;
				IsUpdated = true;
			}
			get
			{
				string ImmediateDestinationRoutingNumber = "";
				ImmediateDestinationRoutingNumber = strImmediateDestinationRoutingNumber;
				return ImmediateDestinationRoutingNumber;
			}
		}

		public string ACHTownName
		{
			set
			{
				strACHTownName = value;
				IsUpdated = true;
			}
			get
			{
				string ACHTownName = "";
				ACHTownName = strACHTownName;
				return ACHTownName;
			}
		}

		public string ACHFederalID
		{
			set
			{
				strACHFederalID = value;
				IsUpdated = true;
			}
			get
			{
				string ACHFederalID = "";
				ACHFederalID = strACHFederalID;
				return ACHFederalID;
			}
		}

		public bool BalancedACHFile
		{
			set
			{
				boolBalancedACHFile = value;
				IsUpdated = true;
			}
			get
			{
				bool BalancedACHFile = false;
				BalancedACHFile = boolBalancedACHFile;
				return BalancedACHFile;
			}
		}

		public short ACHFederalIDPrefix
		{
			set
			{
				intACHFederalIDPrefix = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short ACHFederalIDPrefix = 0;
				ACHFederalIDPrefix = FCConvert.ToInt16(intACHFederalIDPrefix);
				return ACHFederalIDPrefix;
			}
		}

		public short EncCarryForward
		{
			set
			{
				intEncCarryForward = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short EncCarryForward = 0;
				EncCarryForward = FCConvert.ToInt16(intEncCarryForward);
				return EncCarryForward;
			}
		}

		public string ReturnAddress1
		{
			set
			{
				strReturnAddress1 = value;
				IsUpdated = true;
			}
			get
			{
				string ReturnAddress1 = "";
				ReturnAddress1 = strReturnAddress1;
				return ReturnAddress1;
			}
		}

		public string ReturnAddress2
		{
			set
			{
				strReturnAddress2 = value;
				IsUpdated = true;
			}
			get
			{
				string ReturnAddress2 = "";
				ReturnAddress2 = strReturnAddress2;
				return ReturnAddress2;
			}
		}

		public string ReturnAddress3
		{
			set
			{
				strReturnAddress3 = value;
				IsUpdated = true;
			}
			get
			{
				string ReturnAddress3 = "";
				ReturnAddress3 = strReturnAddress3;
				return ReturnAddress3;
			}
		}

		public string ReturnAddress4
		{
			set
			{
				strReturnAddress4 = value;
				IsUpdated = true;
			}
			get
			{
				string ReturnAddress4 = "";
				ReturnAddress4 = strReturnAddress4;
				return ReturnAddress4;
			}
		}

		public bool AutoPostAP
		{
			set
			{
				boolAutoPostAP = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostAP = false;
				AutoPostAP = boolAutoPostAP;
				return AutoPostAP;
			}
		}

		public bool AutoPostGJ
		{
			set
			{
				boolAutoPostGJ = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostGJ = false;
				AutoPostGJ = boolAutoPostGJ;
				return AutoPostGJ;
			}
		}

		public bool AutoPostEN
		{
			set
			{
				boolAutoPostEN = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostEN = false;
				AutoPostEN = boolAutoPostEN;
				return AutoPostEN;
			}
		}

		public bool AutoPostCR
		{
			set
			{
				boolAutoPostCR = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostCR = false;
				AutoPostCR = boolAutoPostCR;
				return AutoPostCR;
			}
		}

		public bool AutoPostCD
		{
			set
			{
				boolAutoPostCD = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostCD = false;
				AutoPostCD = boolAutoPostCD;
				return AutoPostCD;
			}
		}

		public bool AutoPostCM
		{
			set
			{
				boolAutoPostCM = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostCM = false;
				AutoPostCM = boolAutoPostCM;
				return AutoPostCM;
			}
		}

		public bool AutoPostAPCorr
		{
			set
			{
				boolAutoPostAPCorr = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostAPCorr = false;
				AutoPostAPCorr = boolAutoPostAPCorr;
				return AutoPostAPCorr;
			}
		}

		public bool AutoPostReversingJournals
		{
			set
			{
				boolAutoPostReversingJournals = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostReversingJournals = false;
				AutoPostReversingJournals = boolAutoPostReversingJournals;
				return AutoPostReversingJournals;
			}
		}

		public bool AutoPostBudgetTransfer
		{
			set
			{
				boolAutoPostBudgetTransfer = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostBudgetTransfer = false;
				AutoPostBudgetTransfer = boolAutoPostBudgetTransfer;
				return AutoPostBudgetTransfer;
			}
		}

		public bool AutoPostCreateOpeningAdjustments
		{
			set
			{
				boolAutoPostCreateOpeningAdjustments = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostCreateOpeningAdjustments = false;
				AutoPostCreateOpeningAdjustments = boolAutoPostCreateOpeningAdjustments;
				return AutoPostCreateOpeningAdjustments;
			}
		}

		public bool AutoPostARBills
		{
			set
			{
				boolAutoPostARBills = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostARBills = false;
				AutoPostARBills = boolAutoPostARBills;
				return AutoPostARBills;
			}
		}

		public bool AutoPostCRDailyAudit
		{
			set
			{
				boolAutoPostCRDailyAudit = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostCRDailyAudit = false;
				AutoPostCRDailyAudit = boolAutoPostCRDailyAudit;
				return AutoPostCRDailyAudit;
			}
		}

		public bool AutoPostFADepreciation
		{
			set
			{
				boolAutoPostFADepreciation = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostFADepreciation = false;
				AutoPostFADepreciation = boolAutoPostFADepreciation;
				return AutoPostFADepreciation;
			}
		}

		public bool AutoPostMARR
		{
			set
			{
				boolAutoPostMARR = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostMARR = false;
				AutoPostMARR = boolAutoPostMARR;
				return AutoPostMARR;
			}
		}

		public bool AutoPostPYProcesses
		{
			set
			{
				boolAutoPostPYProcesses = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostPYProcesses = false;
				AutoPostPYProcesses = boolAutoPostPYProcesses;
				return AutoPostPYProcesses;
			}
		}

		public bool AutoPostCLLiens
		{
			set
			{
				boolAutoPostCLLiens = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostCLLiens = false;
				AutoPostCLLiens = boolAutoPostCLLiens;
				return AutoPostCLLiens;
			}
		}

		public bool AutoPostCLTA
		{
			set
			{
				boolAutoPostCLTA = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostCLTA = false;
				AutoPostCLTA = boolAutoPostCLTA;
				return AutoPostCLTA;
			}
		}

		public bool AutoPostBLCommitments
		{
			set
			{
				boolAutoPostBLCommitments = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostBLCommitments = false;
				AutoPostBLCommitments = boolAutoPostBLCommitments;
				return AutoPostBLCommitments;
			}
		}

		public bool AutoPostBLSupplementals
		{
			set
			{
				boolAutoPostBLSupplementals = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostBLSupplementals = false;
				AutoPostBLSupplementals = boolAutoPostBLSupplementals;
				return AutoPostBLSupplementals;
			}
		}

		public bool AutoPostUBBills
		{
			set
			{
				boolAutoPostUBBills = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostUBBills = false;
				AutoPostUBBills = boolAutoPostUBBills;
				return AutoPostUBBills;
			}
		}

		public bool AutoPostUBLiens
		{
			set
			{
				boolAutoPostUBLiens = value;
				IsUpdated = true;
			}
			get
			{
				bool AutoPostUBLiens = false;
				AutoPostUBLiens = boolAutoPostUBLiens;
				return AutoPostUBLiens;
			}
		}

		public string Version
		{
			set
			{
				strVersion = value;
				IsUpdated = true;
			}
			get
			{
				string Version = "";
				Version = strVersion;
				return Version;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public bool ExpensesUseDivision
		{
			get
			{
				bool ExpensesUseDivision = false;
				if (Conversion.Val(Strings.Mid(Expenditure, 3, 2)) > 0)
				{
					ExpensesUseDivision = true;
				}
				return ExpensesUseDivision;
			}
		}

		public bool ExpensesUseObject
		{
			get
			{
				bool ExpensesUseObject = false;
				if (Expenditure != "")
				{
					if (Conversion.Val(Strings.Mid(Expenditure, 7, 2)) > 0)
					{
						ExpensesUseObject = true;
					}
					else
					{
						ExpensesUseObject = false;
					}
				}
				return ExpensesUseObject;
			}
		}

		public bool RevenuesUseDivision
		{
			get
			{
				bool RevenuesUseDivision = false;
				if (Conversion.Val(Strings.Mid(Revenue, 3, 2)) > 0)
				{
					RevenuesUseDivision = true;
				}
				return RevenuesUseDivision;
			}
		}

		public bool LedgersUseSuffix
		{
			get
			{
				bool LedgersUseSuffix = false;
				if (Conversion.Val(Strings.Mid(Ledger, 5, 2)) > 0)
				{
					LedgersUseSuffix = true;
				}
				else
				{
					LedgersUseSuffix = false;
				}
				return LedgersUseSuffix;
			}
		}
	}
}
