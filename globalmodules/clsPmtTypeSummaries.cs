﻿using fecherFoundation;
using System.Collections;

namespace Global
{
	public class clsPmtTypeSummaries : IEnumerable
	{
		//=========================================================
		private FCCollection colPmtTypeSums;

		public clsPmtTypeSummaries() : base()
		{
			clsPmtTypeSummary tSum;
			colPmtTypeSums = new FCCollection();
			// Was 0, Now 1
			tSum = new clsPmtTypeSummary();
			tSum.Code = "3";
			tSum.Description = "30 DN Costs";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 1, Now 2
			tSum = new clsPmtTypeSummary();
			tSum.Code = "A";
			tSum.Description = "Abatement";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 2, Now 3
			tSum = new clsPmtTypeSummary();
			tSum.Code = "C";
			tSum.Description = "Correction";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 3, Now 4
			tSum = new clsPmtTypeSummary();
			tSum.Code = "D";
			tSum.Description = "Discount";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 4, Now 5
			tSum = new clsPmtTypeSummary();
			tSum.Code = "I";
			tSum.Description = "Interest Charged";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 5, Now 6
			tSum = new clsPmtTypeSummary();
			tSum.Code = "L";
			tSum.Description = "Lien Costs";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 6, Now 7
			tSum = new clsPmtTypeSummary();
			tSum.Code = "P";
			tSum.Description = "Payment";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 7, Now 8
			tSum = new clsPmtTypeSummary();
			tSum.Code = "R";
			tSum.Description = "Refunded Abatement";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 8, Now 9
			tSum = new clsPmtTypeSummary();
			tSum.Code = "U";
			tSum.Description = "Tax Club";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 9, Now 10
			tSum = new clsPmtTypeSummary();
			tSum.Code = "X";
			tSum.Description = "DOS Correction";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 10, Now 11
			tSum = new clsPmtTypeSummary();
			tSum.Code = "Y";
			tSum.Description = "Prepayment";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 12, Now 12
			tSum = new clsPmtTypeSummary();
			tSum.Code = "N";
			tSum.Description = "Non-Budgetary";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 13, Now 13
			tSum = new clsPmtTypeSummary();
			tSum.Code = "F";
			tSum.Description = "Refund";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was ??, Now 14
			tSum = new clsPmtTypeSummary();
			tSum.Code = "S";
			tSum.Description = "Supplement";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// Was 11, Now 15
			tSum = new clsPmtTypeSummary();
			tSum.Code = "Z";
			tSum.Description = "Current Interest";
			colPmtTypeSums.Add(tSum, tSum.Code);
			// kk08142014 trocl-1195  Handle unexpected Codes
			tSum = new clsPmtTypeSummary();
			tSum.Code = "ZZ";
			tSum.Description = "Unknown";
			colPmtTypeSums.Add(tSum, tSum.Code);
		}

		private bool IsValidCode( string strCode)
		{
			bool IsValidCode = false;
			// kk08142014 trocl-1195  Add check for "good" Code
			// Single char that is in the list
			if (strCode.Length != 1 || Strings.Trim(strCode) == "")
			{
				IsValidCode = false;
			}
			else if (Strings.InStr("3ACDFILNPRSUXYZ", strCode, CompareConstants.vbTextCompare/*?*/) == 0)
			{
				IsValidCode = false;
			}
			else
			{
				IsValidCode = true;
			}
			return IsValidCode;
		}

		public clsPmtTypeSummary Item(string strCode)
		{
			clsPmtTypeSummary Item = null;
			/*? Attribute *///Item.VB_UserMemId = 0;
			// kk08142014 trocl-1195  Add check for "good" Code
			if (IsValidCode( strCode))
			{
				Item = (clsPmtTypeSummary)colPmtTypeSums[strCode];
			}
			else
			{
				Item = (clsPmtTypeSummary)colPmtTypeSums["ZZ"];
			}
			return Item;
		}
		// NewEnum must return the IUnknown interface of a collection's enumerator.
		//	public IUnknown NewEnum()
		//	{
		//		IUnknown NewEnum = 0;
		///*? Attribute */			NewEnum.VB_UserMemId = -4;
		///*? Attribute */			NewEnum.VB_MemberFlags = "40";
		//		NewEnum = colPmtTypeSums; /*? [_NewEnum] */
		//		return NewEnum;
		//	}
		public clsPmtTypeSummary SummaryTotal()
		{
			clsPmtTypeSummary SummaryTotal = null;
			clsPmtTypeSummary tSumTotal = new clsPmtTypeSummary();
			tSumTotal.Description = "Total";
			foreach (clsPmtTypeSummary tSum in colPmtTypeSums)
			{
				tSumTotal.AddPayment(tSum.Principal, tSum.Interest, tSum.PreLienInterst, tSum.LienCost, tSum.Tax);
			}
			tSumTotal.SavePayments();
			SummaryTotal = tSumTotal;
			return SummaryTotal;
		}

		public void AddPaymentByType(string strCode, double dblPrin, double dblInt, double dblPLI = 0.0, double dblCost = 0.0, double dblTax = 0.0)
		{
			clsPmtTypeSummary tSum;
			// kk08142014 trocl-1195  Add check for "good" Code
			if (IsValidCode(strCode))
			{
				tSum = (clsPmtTypeSummary)colPmtTypeSums[strCode];
			}
			else
			{
				tSum = (clsPmtTypeSummary)colPmtTypeSums["ZZ"];
			}
			tSum.AddPayment(dblPrin, dblInt, dblPLI, dblCost, dblTax);
		}

        public void AddPaymentByType(string strCode, decimal principal, decimal interest, decimal prelieninterest,
            decimal cost, decimal tax = 0M)
        {
            clsPmtTypeSummary tSum;
            if (IsValidCode(strCode))
            {
                tSum = (clsPmtTypeSummary) colPmtTypeSums[strCode];
            }
            else
            {
                tSum = (clsPmtTypeSummary) colPmtTypeSums["ZZ"];
            }
			tSum.AddPayment(principal,interest,prelieninterest,cost,tax);
        }
		public void SaveAllPayments()
		{
			foreach (clsPmtTypeSummary tSum in colPmtTypeSums)
			{
				tSum.SavePayments();
			}
		}

		public void DiscardAllPayments()
		{
			foreach (clsPmtTypeSummary tSum in colPmtTypeSums)
			{
				tSum.DiscardPayments();
			}
		}

		public IEnumerator GetEnumerator()
		{
			return colPmtTypeSums.GetEnumerator();
		}
	}
}
