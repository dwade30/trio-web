﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Budgetary.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Messaging;
using SharedDataAccess.CentralData;
using Xunit;

namespace SharedApplicationTests
{
	public class AccountsReceivableCustomerSearchViewModelTests
	{
		[Fact]
		public void ReturnCorrectLastAccountAccessedSetting()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var settingQueryHandler = Substitute.For<IQueryHandler<SettingSearchCriteria, IEnumerable<Setting>>>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerQueryHandler =
				Substitute.For<IQueryHandler<CustomerSearchCriteria, IEnumerable<CustomerMaster>>>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();

			var service = new AccountsReceivableCustomerSearchViewModel(eventPublisher, settingQueryHandler,
				activeModuleSettings, globalAccountsReceivableSettings, commandDispatcher, customerQueryHandler,
				customerPartyQueryHandler);

		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
