﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fecherFoundation;
using SharedApplication;
using Xunit;

namespace SharedApplicationTests
{
	public class InterestCalculationServiceTests
	{
		[Fact]
		public void ReturnCorrectPerDiemAmount()
		{
			var service = new InterestCalculationService();

			double testRate = .2;
			int testDaysPerYear = 12;
			int testPaymentPeriod = 1;
			int testNumberOfPayments = 1;
			double testAmount = -32.57;

			var result = service.IPmt(testRate / testDaysPerYear, testPaymentPeriod, testNumberOfPayments, testAmount);

			var perDiem = Financial.IPmt((testRate / testDaysPerYear), testPaymentPeriod, testPaymentPeriod, testAmount, 0);

			Assert.True(result == perDiem);
		}
	}
}
