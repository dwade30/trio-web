﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Messaging;
using Xunit;

namespace SharedApplicationTests
{
	public class MiscTransactionViewModelTests
	{
		[Fact]
		public void ReturnCorrectReceiptTypeInformation()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var regionQueryHandler = Substitute.For< IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
			var globalCashReceiptSettings = Substitute.For < GlobalCashReceiptSettings>();
			var activeModuleSettings = Substitute.For < IGlobalActiveModuleSettings>();
			var accountValidationService = Substitute.For < IAccountValidationService>();
			var accountTitleService = Substitute.For < IAccountTitleService>();
			var regionalTownService = Substitute.For < IRegionalTownService>();

			var testReceiptType = new ReceiptType
			{
				TypeCode = 10,
				TypeTitle = "Test Type",
				Account1 = "R 01-01-001",
				Account2 = "R 10-10-200",
				Account3 = "G 1-100-00",
				BMV = false,
				Control1 = "Name",
				Control1Mandatory = true,
				Reference = "Permit Number",
				ReferenceMandatory = false,
				Title1 = "Line 1",
				Title2 = "Line 2",
				Title3 = "Line 3",
				PercentageFee1 = false,
				PercentageFee2 = true,
				PercentageFee3 = true,
				DefaultAmount1 = 10,
				DefaultAmount2 = 0,
				DefaultAmount3 = 0,
				Year1 = false,
				Year2 = false,
				Year3 = true
			};

			receiptTypeQueryHandler.ExecuteQuery(Arg.Is<ReceiptTypeSearchCriteria>(x => x.TownKey == 1 && x.ReceiptType == 10)).Returns(new List<ReceiptType>()
			{
				testReceiptType	
			});

			var service = new MiscTransactionViewModel(receiptTypeQueryHandler, eventPublisher, activeModuleSettings, accountValidationService, accountTitleService, regionalTownService, globalCashReceiptSettings);
			service.SetReceiptTypeInfo(10, 1);

			Assert.True(service.ReceiptTypeInfo().TypeCode == testReceiptType.TypeCode);
			Assert.True(service.ReceiptTypeInfo().TypeTitle == testReceiptType.TypeTitle);
			Assert.True(service.ReceiptTypeInfo().Account2 == testReceiptType.Account2);
			Assert.True(service.ReceiptTypeInfo().Title1 == testReceiptType.Title1);
			Assert.True(service.ReceiptTypeInfo().Control1 == testReceiptType.Control1);
		}

		[Fact]
		public void ReturnCorrectExtendedLineDataForMiscReceipt()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var regionQueryHandler = Substitute.For<IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
			var globalCashReceiptSettings = Substitute.For<GlobalCashReceiptSettings>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var accountTitleService = Substitute.For<IAccountTitleService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();

			var lineAccount1 = "R 01-01-001";
			var lineAccount2 = "R 10-10-200";
			var lineAccount3 = "G 1-100-00";

			var testReceiptType = new ReceiptType
			{
				TypeCode = 10,
				TypeTitle = "Test Type",
				Account1 = lineAccount1,
				Account2 = lineAccount2,
				Account3 = lineAccount3,
				BMV = false,
				Control1 = "Name",
				Control1Mandatory = true,
				Reference = "Permit Number",
				ReferenceMandatory = false,
				Title1 = "Line 1",
				Title2 = "Line 2",
				Title3 = "Line 3",
				PercentageFee1 = false,
				PercentageFee2 = true,
				PercentageFee3 = true,
				DefaultAmount1 = 10,
				DefaultAmount2 = 0,
				DefaultAmount3 = 0,
				Year1 = false,
				Year2 = false,
				Year3 = true
			};

			receiptTypeQueryHandler.ExecuteQuery(Arg.Is<ReceiptTypeSearchCriteria>(x => x.TownKey == 1 && x.ReceiptType == 10)).Returns(new List<ReceiptType>()
			{
				testReceiptType
			});

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(lineAccount1).Returns(true);
			accountValidationService.AccountValidate(lineAccount2).Returns(true);
			accountValidationService.AccountValidate(lineAccount3).Returns(false);

			var service = new MiscTransactionViewModel(receiptTypeQueryHandler, eventPublisher, activeModuleSettings, accountValidationService, accountTitleService, regionalTownService, globalCashReceiptSettings);
			service.SetReceiptTypeInfo(10, 1);

			Assert.True(service.ShowLine(1) == true);
			Assert.True(service.ShowLine(2) == true);
			Assert.True(service.ShowLine(3) == false);
			Assert.True(service.ShowLine(4) == false);
			Assert.True(service.ShowLine(5) == false);
			Assert.True(service.ShowLine(6) == false);

			Assert.True(service.LineTitle(1) == "Line 1");
			Assert.True(service.LineTitle(2) == "Line 2");
			Assert.True(service.LineTitle(3) == "Line 3");
			Assert.True(service.LineTitle(4) == "");
			Assert.True(service.LineTitle(5) == "");
			Assert.True(service.LineTitle(6) == "");

			Assert.True(service.LineDefaultAmount(1) == 10);
			Assert.True(service.LineDefaultAmount(2) == 0);
			Assert.True(service.LineDefaultAmount(3) == 0);
			Assert.True(service.LineDefaultAmount(4) == 0);
			Assert.True(service.LineDefaultAmount(5) == 0);
			Assert.True(service.LineDefaultAmount(6) == 0);

			Assert.True(service.LinePercentage(1) == false);
			Assert.True(service.LinePercentage(2) == true);
			Assert.True(service.LinePercentage(3) == true);
			Assert.True(service.LinePercentage(4) == false);
			Assert.True(service.LinePercentage(5) == false);
			Assert.True(service.LinePercentage(6) == false);

			Assert.True(service.LineYear(1) == false);
			Assert.True(service.LineYear(2) == false);
			Assert.True(service.LineYear(3) == true);
			Assert.True(service.LineYear(4) == false);
			Assert.True(service.LineYear(5) == false);
			Assert.True(service.LineYear(6) == false);
		}

		[Fact]
		public void ReturnCorrectExtendedGeneralDataForMiscReceipt()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var regionQueryHandler = Substitute.For<IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
			var globalCashReceiptSettings = Substitute.For<GlobalCashReceiptSettings>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var accountTitleService = Substitute.For<IAccountTitleService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();

			var testReceiptType = new ReceiptType
			{
				TypeCode = 10,
				TypeTitle = "Test Type",
				Account1 = "R 01-01-001",
				Account2 = "R 10-10-200",
				Account3 = "G 1-100-00",
				BMV = false,
				Control1 = "Name",
				Control1Mandatory = true,
				Reference = "Permit Number",
				ReferenceMandatory = false,
				Title1 = "Line 1",
				Title2 = "Line 2",
				Title3 = "Line 3",
				PercentageFee1 = false,
				PercentageFee2 = true,
				PercentageFee3 = true,
				DefaultAmount1 = 10,
				DefaultAmount2 = 0,
				DefaultAmount3 = 0,
				Year1 = false,
				Year2 = false,
				Year3 = true
			};

			receiptTypeQueryHandler.ExecuteQuery(Arg.Is<ReceiptTypeSearchCriteria>(x => x.TownKey == 1 && x.ReceiptType == 10)).Returns(new List<ReceiptType>()
			{
				testReceiptType
			});

			activeModuleSettings.BudgetaryIsActive.Returns(true);


			var service = new MiscTransactionViewModel(receiptTypeQueryHandler, eventPublisher, activeModuleSettings, accountValidationService, accountTitleService, regionalTownService, globalCashReceiptSettings);
			service.SetReceiptTypeInfo(10, 1);

			Assert.True(service.ShowPercentage() == true);
			Assert.True(service.ShowAccountEntry() == false);
			Assert.True(service.ReferenceVisible() == true);
			Assert.True(service.ControlVisible(1) == true);
			Assert.True(service.ControlVisible(2) == false);
			Assert.True(service.ControlVisible(3) == false);
			Assert.True(service.GetGLAccountDescription("") == "");
		}

		[Fact]
		public void CorrectlyUpdatesControlItems()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var regionQueryHandler = Substitute.For<IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
			var globalCashReceiptSettings = Substitute.For<GlobalCashReceiptSettings>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var accountTitleService = Substitute.For<IAccountTitleService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();

			var service = new MiscTransactionViewModel(receiptTypeQueryHandler, eventPublisher, activeModuleSettings, accountValidationService, accountTitleService, regionalTownService, globalCashReceiptSettings);
			service.Transaction = new MiscReceiptTransaction();
			service.UpdateControlItem("Test 1", "Description 1", "Value 1");
			service.UpdateControlItem("Test 1", "Description 2", "Value 2");

			Assert.True(service.Transaction.Controls.Count() == 1);
			Assert.True(service.Transaction.Controls.FirstOrDefault()?.Name == "Test 1");
			Assert.True(service.Transaction.Controls.FirstOrDefault()?.Description == "Description 2");
			Assert.True(service.Transaction.Controls.FirstOrDefault()?.Value == "Value 2");

			service.UpdateControlItem("Test 2", "Description 1", "Value 1");

			Assert.True(service.Transaction.Controls.Count() == 2);
		}

		[Fact]
		public void CorrectlyUpdatesAmountItems()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var regionQueryHandler = Substitute.For<IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
			var globalCashReceiptSettings = Substitute.For<GlobalCashReceiptSettings>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var accountTitleService = Substitute.For<IAccountTitleService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();

			var service = new MiscTransactionViewModel(receiptTypeQueryHandler, eventPublisher, activeModuleSettings, accountValidationService, accountTitleService, regionalTownService, globalCashReceiptSettings);
			service.Transaction = new MiscReceiptTransaction();
			service.Transaction.PercentageAmount = 200m;
			service.UpdateAmountItem("Amount 1", "State Fee", 0, 42);
			
			Assert.True(service.Transaction.TransactionAmounts.Count() == 1);
			Assert.True(service.Transaction.TransactionAmounts.FirstOrDefault()?.Name == "Amount 1");
			Assert.True(service.Transaction.TransactionAmounts.FirstOrDefault()?.Description == "State Fee");
			Assert.True(service.Transaction.TransactionAmounts.FirstOrDefault()?.Total == 84m);
			Assert.True(service.Transaction.TransactionAmounts.FirstOrDefault()?.PercentageAmount == 42m);


			service.UpdateAmountItem("Amount 1", "City Fee", 20);

			Assert.True(service.Transaction.TransactionAmounts.Count() == 1);
			Assert.True(service.Transaction.TransactionAmounts.FirstOrDefault()?.Name == "Amount 1");
			Assert.True(service.Transaction.TransactionAmounts.FirstOrDefault()?.Description == "City Fee");
			Assert.True(service.Transaction.TransactionAmounts.FirstOrDefault()?.Total == 20m);
			Assert.True(service.Transaction.TransactionAmounts.FirstOrDefault()?.PercentageAmount == 0m);

			service.UpdateAmountItem("Amount 2", "City Fee", 20);

			Assert.True(service.Transaction.TransactionAmounts.Count() == 2);
		}

		[Fact]
		public void CorrectlyCheckPercentageAmount()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var regionQueryHandler = Substitute.For<IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
			var globalCashReceiptSettings = Substitute.For<GlobalCashReceiptSettings>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var accountTitleService = Substitute.For<IAccountTitleService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();

			var testReceiptType = new ReceiptType
			{
				TypeCode = 10,
				TypeTitle = "Test Type",
				Account1 = "R 01-01-001",
				Account2 = "R 10-10-200",
				Account3 = "G 1-100-00",
				BMV = false,
				Control1 = "Name",
				Control1Mandatory = true,
				Reference = "Permit Number",
				ReferenceMandatory = false,
				Title1 = "Line 1",
				Title2 = "Line 2",
				Title3 = "Line 3",
				PercentageFee1 = true,
				PercentageFee2 = true,
				PercentageFee3 = true,
				DefaultAmount1 = 10,
				DefaultAmount2 = 0,
				DefaultAmount3 = 0,
				Year1 = false,
				Year2 = false,
				Year3 = true
			};

			receiptTypeQueryHandler.ExecuteQuery(Arg.Is<ReceiptTypeSearchCriteria>(x => x.TownKey == 1 && x.ReceiptType == 10)).Returns(new List<ReceiptType>()
			{
				testReceiptType
			});

			activeModuleSettings.BudgetaryIsActive.Returns(true);

			var service = new MiscTransactionViewModel(receiptTypeQueryHandler, eventPublisher, activeModuleSettings, accountValidationService, accountTitleService, regionalTownService, globalCashReceiptSettings);
			service.Transaction = new MiscReceiptTransaction();
			service.Transaction.PercentageAmount = 200m;
			service.SetReceiptTypeInfo(10, 1);

			service.UpdateAmountItem("Amount 1", "State Fee", 0, 42);
			service.UpdateAmountItem("Amount 2", "City Fee", 0, 57);
			Assert.True(service.IsValidPercentageTotal() == false);

			service.UpdateAmountItem("Amount 3", "City Fee", 0, 1);
			Assert.True(service.IsValidPercentageTotal() == true);
		}

		[Fact]
		public void IsZeroAmountReceiptReturnsCorrectValue()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var regionQueryHandler = Substitute.For<IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
			var globalCashReceiptSettings = Substitute.For<GlobalCashReceiptSettings>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var accountTitleService = Substitute.For<IAccountTitleService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();

			var testReceiptType = new ReceiptType
			{
				TypeCode = 10,
				TypeTitle = "Test Type",
				Account1 = "R 01-01-001",
				Account2 = "R 10-10-200",
				Account3 = "G 1-100-00",
				BMV = false,
				Control1 = "Name",
				Control1Mandatory = true,
				Reference = "Permit Number",
				ReferenceMandatory = false,
				Title1 = "Line 1",
				Title2 = "Line 2",
				Title3 = "Line 3",
				PercentageFee1 = true,
				PercentageFee2 = true,
				PercentageFee3 = true,
				DefaultAmount1 = 10,
				DefaultAmount2 = 0,
				DefaultAmount3 = 0,
				Year1 = false,
				Year2 = false,
				Year3 = true
			};

			receiptTypeQueryHandler.ExecuteQuery(Arg.Is<ReceiptTypeSearchCriteria>(x => x.TownKey == 1 && x.ReceiptType == 10)).Returns(new List<ReceiptType>()
			{
				testReceiptType
			});

			activeModuleSettings.BudgetaryIsActive.Returns(true);

			var service = new MiscTransactionViewModel(receiptTypeQueryHandler, eventPublisher, activeModuleSettings, accountValidationService, accountTitleService, regionalTownService, globalCashReceiptSettings);
			service.Transaction = new MiscReceiptTransaction();
			service.Transaction.PercentageAmount = 200m;
			service.SetReceiptTypeInfo(10, 1);

			service.UpdateAmountItem("Amount 1", "State Fee", 0, 42);
			service.UpdateAmountItem("Amount 2", "City Fee", 0, 57);
			service.UpdateAmountItem("Amount 3", "City Fee", 0, 1);
			Assert.True(service.IsZeroAmountReceipt() == false);


		}

		[Fact]
		public void IsZeroAmountReceiptReturnsCorrectValueIfTrue()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var regionQueryHandler = Substitute.For<IQueryHandler<RegionSearchCriteria, IEnumerable<Region>>>();
			var globalCashReceiptSettings = Substitute.For<GlobalCashReceiptSettings>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var accountTitleService = Substitute.For<IAccountTitleService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();

			var testReceiptType = new ReceiptType
			{
				TypeCode = 10,
				TypeTitle = "Test Type",
				Account1 = "R 01-01-001",
				Account2 = "R 10-10-200",
				Account3 = "G 1-100-00",
				BMV = false,
				Control1 = "Name",
				Control1Mandatory = true,
				Reference = "Permit Number",
				ReferenceMandatory = false,
				Title1 = "Line 1",
				Title2 = "Line 2",
				Title3 = "Line 3",
				PercentageFee1 = true,
				PercentageFee2 = true,
				PercentageFee3 = true,
				DefaultAmount1 = 10,
				DefaultAmount2 = 0,
				DefaultAmount3 = 0,
				Year1 = false,
				Year2 = false,
				Year3 = true
			};

			receiptTypeQueryHandler.ExecuteQuery(Arg.Is<ReceiptTypeSearchCriteria>(x => x.TownKey == 1 && x.ReceiptType == 10)).Returns(new List<ReceiptType>()
			{
				testReceiptType
			});

			activeModuleSettings.BudgetaryIsActive.Returns(true);

			var service = new MiscTransactionViewModel(receiptTypeQueryHandler, eventPublisher, activeModuleSettings, accountValidationService, accountTitleService, regionalTownService, globalCashReceiptSettings);
			service.Transaction = new MiscReceiptTransaction();
			service.Transaction.PercentageAmount = 0m;
			service.SetReceiptTypeInfo(10, 1);

			Assert.True(service.IsZeroAmountReceipt() == true);

			service.UpdateAmountItem("Amount 1", "State Fee", 0, 42);
			Assert.True(service.IsZeroAmountReceipt() == true);
		}
	}
}
