﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Messaging;
using SharedApplication.Models;
using Xunit;
using Xunit.Sdk;

namespace SharedApplicationTests
{
	public class AccountsReceivablePaymentViewModelTests
	{
		[Fact]
		public void ReturnFalseOnInvalidAccount()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testCustomerId = 1;
			var testInvoiceNumber = "101010";

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(false);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
					}
				});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			var result = service.ValidManualAccount(testAccount, "2222");

			Assert.True(result == false);
		}

		[Fact]
		public void ReturnFalseOnInvalidMAccountNoBudgetary()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "M";
			var testCustomerId = 1;
			var testInvoiceNumber = "101010";

			activeModuleSettings.BudgetaryIsActive.Returns(false);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
					}
				});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			var result = service.ValidManualAccount(testAccount, "2222");

			Assert.True(result == false);
		}

		[Fact]
		public void ReturnTrueOnInvalidMAccountNoBudgetary()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "M TEST";
			var testCustomerId = 1;
			var testInvoiceNumber = "101010";

			activeModuleSettings.BudgetaryIsActive.Returns(false);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
					}
				});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			var result = service.ValidManualAccount(testAccount, "2222");

			Assert.True(result);
		}

		[Fact]
		public void ReturnTrueForValidManualAccountNoBillFound()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testCustomerId = 1;
			var testInvoiceNumber = "101010";

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(true);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
					}
				});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			var result = service.ValidManualAccount(testAccount, "2222");

			Assert.True(result);
		}

		[Fact]
		public void ReturnTrueForValidManualAccountNoDefaultBillFound()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(true);
			budgetaryAccountingService.GetFundFromAccount(testAccount).Returns(10);
			budgetaryAccountingService.GetFundFromAccount(testDefaultBillAccount).Returns(10);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
						BillType = testTypeCode
					}
				});

			defaultBillTypeQueryHandler.ExecuteQuery(Arg.Is<DefaultBillTypeSearchCriteria>(x => x.TypeCode == testTypeCode)).Returns(new List<DefaultBillType>());

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(new ARBill(), DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};
			var result = service.ValidManualAccount(testAccount, testInvoiceNumber);


			Assert.True(result);
		}

		[Fact]
		public void ReturnTrueForValidManualAccountWithAllInfoNeeded()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(true);
			budgetaryAccountingService.GetFundFromAccount(testAccount).Returns(10);
			budgetaryAccountingService.GetFundFromAccount(testDefaultBillAccount).Returns(10);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
						BillType = testTypeCode
					}
				});

			defaultBillTypeQueryHandler.ExecuteQuery(Arg.Is<DefaultBillTypeSearchCriteria>(x => x.TypeCode == testTypeCode)).Returns(new List<DefaultBillType>
			{
				new DefaultBillType
				{
					Account1 = testDefaultBillAccount,
					TypeTitle = "TESTING",
					TypeCode = testTypeCode
				}
			});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(new ARBill(), DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};
			var result = service.ValidManualAccount(testAccount, testInvoiceNumber);


			Assert.True(result);
		}

		[Fact]
		public void ReturnFalseForValidManualAccountWithAllInfoNeededIfFundsDontMatch()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(true);
			budgetaryAccountingService.GetFundFromAccount(testAccount).Returns(10);
			budgetaryAccountingService.GetFundFromAccount(testDefaultBillAccount).Returns(20);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
						BillType = testTypeCode
					}
				});

			defaultBillTypeQueryHandler.ExecuteQuery(Arg.Is<DefaultBillTypeSearchCriteria>(x => x.TypeCode == testTypeCode)).Returns(new List<DefaultBillType>
			{
				new DefaultBillType
				{
					Account1 = testDefaultBillAccount,
					TypeTitle = "TESTING",
					TypeCode = testTypeCode
				}
			});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(new ARBill(), DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};
			var result = service.ValidManualAccount(testAccount, testInvoiceNumber);


			Assert.True(result == false);
		}

		[Fact]
		public void CalculateBillWorksCorrectly()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;
			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 10,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = (decimal)50.20,
				TaxOwed = 90,
				TaxPaid = 50,
				IntAdded = -15,
				IntPaid = 10,
			};

			var balanceRemaining = testBill.PrinOwed - testBill.PrinPaid + testBill.TaxOwed - testBill.TaxPaid -
			                       testBill.IntAdded - testBill.IntPaid;
			testBill.BalanceRemaining = balanceRemaining ?? 0;

			var testBillCalcInfo = new CalculateBillResult();
			var testInterestInfo = new CalculateInterestResult
			{
				Interest = -10,
				PerDiem = .01234
			};
			testBillCalcInfo.InterestCalculated = true;
			testBillCalcInfo.CalculatedInterestResult = testInterestInfo;
			testBillCalcInfo.CalculatedInterestDate = DateTime.Today;
			testBillCalcInfo.LastInterestDate = DateTime.Today.AddMonths(-3);

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(true);
			budgetaryAccountingService.GetFundFromAccount(testAccount).Returns(10);
			budgetaryAccountingService.GetFundFromAccount(testDefaultBillAccount).Returns(20);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill
				});

			defaultBillTypeQueryHandler.ExecuteQuery(Arg.Is<DefaultBillTypeSearchCriteria>(x => x.TypeCode == testTypeCode)).Returns(new List<DefaultBillType>
			{
				new DefaultBillType
				{
					Account1 = testDefaultBillAccount,
					TypeTitle = "TESTING",
					TypeCode = testTypeCode
				}
			});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(testBillCalcInfo);

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};
			var result = service.CalculateBill(testBill);

			var bill = service.Bills.FirstOrDefault();

			Assert.True(result.CalculatedInterestDate == bill.CurrentInterestDate);
			Assert.True(result.LastInterestDate == bill.LastInterestDate);
			Assert.True(result.CalculatedInterestResult.Interest  * -1 == bill.CurrentInterest);
			Assert.True((decimal)result.CalculatedInterestResult.PerDiem == bill.PerDiem);
			Assert.True(result.CalculatedInterestResult.Interest + balanceRemaining == bill.BalanceRemaining);
		}

		[Fact]
		public void CalculatePaymentsAmountForNegativeBillWorksCorrectly()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testInvoiceNumber2 = "201010";
			var testInvoiceNumber3 = "301010";
			var testTypeCode = 5;
			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 100,
				TaxOwed = 40,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = -10
			};

			var testBill2 = new ARBill
			{
				InvoiceNumber = testInvoiceNumber2,
				BillNumber = 10,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 120,
				TaxOwed = 40,
				TaxPaid = 40,
				IntAdded = -5,
				IntPaid = 5,
				Id = 19,
				BalanceRemaining = -20
			};

			var testBill3 = new ARBill
			{
				InvoiceNumber = testInvoiceNumber3,
				BillNumber = 21,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 100,
				TaxOwed = 40,
				TaxPaid = 40,
				IntAdded = -5,
				IntPaid = 10,
				Id = 22,
				BalanceRemaining = -5
			};

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
					testBill2,
					testBill3
				});

			defaultBillTypeQueryHandler.ExecuteQuery(Arg.Is<DefaultBillTypeSearchCriteria>(x => x.TypeCode == testTypeCode)).Returns(new List<DefaultBillType>
			{
				new DefaultBillType
				{
					Account1 = testDefaultBillAccount,
					TypeTitle = "TESTING",
					TypeCode = testTypeCode
				}
			});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}
			service.AdjustNegativeBillsToAddPayment();

			Assert.True(service.Bills.Count == 4);
			Assert.True(service.Bills.Where(x => x.Pending).ToList().Count == 1);
			Assert.True(service.Bills.Where(x => x.Payments.Any(y => y.Pending)).ToList().Count == 4);
		}

		[Fact]
		public void RemovePendingPaymentWorksCorrectlyWithNoChargedInterest()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testInvoiceNumber2 = "201010";
			var testInvoiceNumber3 = "301010";
			var testTypeCode = 5;
			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 0,
				Tax = 0,
				Pending = true
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				Payments = new List<ARPayment>
				{
					testPayment
				}
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			service.RemovePendingPayment(testPayment);

			var resultBill = service.Bills.FirstOrDefault();

			Assert.True(resultBill != null);
			Assert.True(resultBill.Payments.Count == 0);
			Assert.True(resultBill.BalanceRemaining == 50);
		}

		[Fact]
		public void RemovePendingPaymentWorksCorrectlyWithChargedInterest()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testInvoiceNumber2 = "201010";
			var testInvoiceNumber3 = "301010";
			var testTypeCode = 5;
			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 10,
				Tax = 0,
				Pending = true,
				ChargedInterestRecord = new ARPayment
				{
					Interest = -10,
					ChargedInterestDate = DateTime.Today,
					Pending = true,
					Principal = 0,
					Tax = 0
				}
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = 0,
				IntPaid = 0,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				PendingInterestAmount = 10,
				PendingChargedInterest = -10,
				CurrentInterest = -10,
				LastInterestDate = DateTime.Today,
				PreviousInterestDate = DateTime.Today.AddMonths(-3),
				SavedPreviousInterestDate = DateTime.Today.AddMonths(-6),
				Payments = new List<ARPayment>
				{
					testPayment
				}
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			service.RemovePendingPayment(testPayment);

			var resultBill = service.Bills.FirstOrDefault();

			Assert.True(resultBill != null);
			Assert.True(resultBill.Payments.Count == 0);
			Assert.True(resultBill.BalanceRemaining == 60);
			Assert.True(resultBill.PendingInterestAmount == 0);
			Assert.True(resultBill.PendingPrincipalAmount == 0);
			Assert.True(resultBill.PendingChargedInterest == 0);
			Assert.True(resultBill.LastInterestDate == DateTime.Today.AddMonths(-3));
			Assert.True(resultBill.PreviousInterestDate == DateTime.Today.AddMonths(-6));
		}

		[Fact]
		public void RemoveAllPendingPaymentsRemovesPendingBill()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testInvoiceNumber2 = "201010";
			var testInvoiceNumber3 = "301010";
			var testTypeCode = 5;
			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 10,
				Tax = 0,
				Pending = true,
				ChargedInterestRecord = new ARPayment
				{
					Interest = -10,
					ChargedInterestDate = DateTime.Today,
					Pending = true,
					Principal = 0,
					Tax = 0
				}
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = 0,
				IntPaid = 0,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				PendingInterestAmount = 10,
				PendingChargedInterest = -10,
				CurrentInterest = -10,
				LastInterestDate = DateTime.Today,
				PreviousInterestDate = DateTime.Today.AddMonths(-3),
				SavedPreviousInterestDate = DateTime.Today.AddMonths(-6),
				Pending = true,
				Payments = new List<ARPayment>
				{
					testPayment
				}
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			service.RemoveAllPendingPayments();

			Assert.True(service.Bills.Count == 0);
		}

		[Fact]
		public void GetPendingPaymentReturnsCorrectInfo()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testInvoiceNumber2 = "201010";
			var testInvoiceNumber3 = "301010";
			var testTypeCode = 5;
			var testPendingPaymentId = new Guid();

			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 0,
				Tax = 0,
				Pending = true,
				PendingId = testPendingPaymentId
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				Payments = new List<ARPayment>
				{
					testPayment
				}
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			var result = service.GetPendingPayment(testPendingPaymentId);

			Assert.True(result != null);
			Assert.True(result.Principal == 50);
		}

		[Fact]
		public void ChargedInterestExistsOnBillWorksCorrectlyWithChargedInterest()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testInvoiceNumber2 = "201010";
			var testInvoiceNumber3 = "301010";
			var testTypeCode = 5;
			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 10,
				Tax = 0,
				Pending = true,
				ChargedInterestRecord = new ARPayment
				{
					Interest = -10,
					ChargedInterestDate = DateTime.Today,
					Pending = true,
					Principal = 0,
					Reference = "CHGINT",
					Tax = 0
				}
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = 0,
				IntPaid = 0,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				PendingInterestAmount = 10,
				PendingChargedInterest = -10,
				CurrentInterest = -10,
				LastInterestDate = DateTime.Today,
				PreviousInterestDate = DateTime.Today.AddMonths(-3),
				SavedPreviousInterestDate = DateTime.Today.AddMonths(-6),
				Payments = new List<ARPayment>
				{
					testPayment
				}
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			var result = service.ChargedInterestExistsOnBill(testBill.Id);

			Assert.True(result);
		}

		[Fact]
		public void GetBillReturnsCorrectInfoWithAsteriskInInvoiceNumber()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;
			var testPendingPaymentId = new Guid();

			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 0,
				Tax = 0,
				Pending = true,
				PendingId = testPendingPaymentId
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				Payments = new List<ARPayment>
				{
					testPayment
				}
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			var result = service.GetBill(testInvoiceNumber + "*");

			Assert.True(result != null);
			Assert.True(result.InvoiceNumber == testInvoiceNumber);
		}

		[Fact]
		public void LatestPaymentOnBillReturnsFalseIfLaterBill()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;
			var testPendingPaymentId = new Guid();

			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 0,
				Tax = 0,
				Pending = true,
				PendingId = testPendingPaymentId,
				EffectiveInterestDate = DateTime.Today.AddDays(-1)
			};

			var testLaterPayment = new ARPayment
			{
				Principal = 20,
				Interest = 0,
				Tax = 0,
				Pending = true,
				PendingId = new Guid(),
				EffectiveInterestDate = DateTime.Today
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				Payments = new List<ARPayment>
				{
					testPayment,
					testLaterPayment
				}
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			var result = service.LatestPaymentOnBill(testPayment);

			Assert.True(result == false);
		}

		[Fact]
		public void TestCommentForCorrectDataCommentExists()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;
			var testComment = "This is a test comment.";
			var testCommentPriority = 0;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(true);
			budgetaryAccountingService.GetFundFromAccount(testAccount).Returns(10);
			budgetaryAccountingService.GetFundFromAccount(testDefaultBillAccount).Returns(20);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
						BillType = testTypeCode
					}
				});

			defaultBillTypeQueryHandler.ExecuteQuery(Arg.Is<DefaultBillTypeSearchCriteria>(x => x.TypeCode == testTypeCode)).Returns(new List<DefaultBillType>
			{
				new DefaultBillType
				{
					Account1 = testDefaultBillAccount,
					TypeTitle = "TESTING",
					TypeCode = testTypeCode
				}
			});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			commentQueryHandler.ExecuteQuery(Arg.Is<CommentSearchCriteria>(x => x.CustomerId == testCustomerAccountNumber))
				.Returns(
					new List<Comment>
					{
						new Comment
						{
							Text = testComment,
							Priority = testCommentPriority,
							Account = testCustomerId,
							Type = "AR"
						}
					});

			paymentService.CalculateBillTotals(new ARBill(), DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};
			var result = service.AccountComment;


			Assert.True(result!= null);
			Assert.True(result.Text == testComment);
			Assert.True(result.Priority == testCommentPriority);
			Assert.True(result.Account == testCustomerId);

		}

		[Fact]
		public void TestCommentForCorrectDataNoCommentExists()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;
			var testComment = "This is a test comment.";
			var testCommentPriority = 0;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(true);
			budgetaryAccountingService.GetFundFromAccount(testAccount).Returns(10);
			budgetaryAccountingService.GetFundFromAccount(testDefaultBillAccount).Returns(20);
			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					new ARBill
					{
						InvoiceNumber = testInvoiceNumber,
						BillNumber = 10,
						BillType = testTypeCode
					}
				});

			defaultBillTypeQueryHandler.ExecuteQuery(Arg.Is<DefaultBillTypeSearchCriteria>(x => x.TypeCode == testTypeCode)).Returns(new List<DefaultBillType>
			{
				new DefaultBillType
				{
					Account1 = testDefaultBillAccount,
					TypeTitle = "TESTING",
					TypeCode = testTypeCode
				}
			});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			commentQueryHandler.ExecuteQuery(Arg.Is<CommentSearchCriteria>(x => x.CustomerId == testCustomerAccountNumber)).Returns(new List<Comment>());

			paymentService.CalculateBillTotals(new ARBill(), DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};
			var result = service.AccountComment;

			Assert.True(result == null);
		}

		[Fact]
		public void TestTotalPerDiemReturnsCorrectValue()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testTypeCode = 5;
			var testPendingPaymentId = new Guid();
			var testPerDiem1 = .198762;
			var testPerDiem2 = .2093874;

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				PerDiem = (decimal)testPerDiem1
			};

			var testBill2 = new ARBill
			{
				InvoiceNumber = "298398",
				BillNumber = 50,
				Id = 60,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 50,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 50,
				PendingPrincipalAmount = 50,
				PerDiem = (decimal)testPerDiem2
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
					testBill2
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			var result = service.TotalPerDiem;

			Assert.True(result == (decimal)(testPerDiem1 + testPerDiem2));
		}

		[Fact]
		public void PaymentGreaterThanOwedReturnsCorrectResult()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testInvoiceNumber2 = "201010";
			var testInvoiceNumber3 = "301010";
			var testTypeCode = 5;
			var testPendingPaymentId = new Guid();

			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 0,
				Tax = 0,
				Pending = true,
				PendingId = testPendingPaymentId
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 80,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 20,
				PendingPrincipalAmount = 0,
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			var result = service.PaymentGreaterThanOwed(testInvoiceNumber, 50);

			Assert.True(result);
		}

		[Fact]
		public void PaymentGreaterThanOwedReturnsCorrectResultForAutoPayment()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalAccountsReceivableSettings = Substitute.For<GlobalAccountsReceivableSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			var arBillQueryHandler = Substitute.For<IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var paymentService = Substitute.For<IPaymentService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var defaultBillTypeQueryHandler =
				Substitute.For<IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testDefaultBillAccount = "E 20-20-20-20";
			var testCustomerId = 1;
			var testCustomerAccountNumber = 11;
			var testInvoiceNumber = "101010";
			var testInvoiceNumber2 = "201010";
			var testInvoiceNumber3 = "301010";
			var testTypeCode = 5;
			var testPendingPaymentId = new Guid();

			var testPayment = new ARPayment
			{
				Principal = 50,
				Interest = 0,
				Tax = 0,
				Pending = true,
				PendingId = testPendingPaymentId
			};

			var testBill = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 80,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 20,
				PendingPrincipalAmount = 0,
			};

			var testBill2 = new ARBill
			{
				InvoiceNumber = testInvoiceNumber,
				BillNumber = 5,
				Id = 6,
				BillType = testTypeCode,
				PrinOwed = 100,
				PrinPaid = 60,
				TaxOwed = 50,
				TaxPaid = 50,
				IntAdded = -5,
				IntPaid = 5,
				BalanceRemaining = 40,
				PendingPrincipalAmount = 0,
			};

			arBillQueryHandler.ExecuteQuery(Arg.Is<ARBillSearchCriteria>(x => x.CustomerId == testCustomerId && x.VoidedSelection == VoidedStatusSelection.NotVoided)).Returns(
				new List<ARBill>
				{
					testBill,
					testBill2
				});

			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testCustomerId)).Returns(
					new List<CustomerPaymentStatusResult>
					{
						new CustomerPaymentStatusResult
						{
							CustomerId = testCustomerAccountNumber,
							FullName = "TEST TESTER",
							Id = testCustomerId
						}
					});

			paymentService.CalculateBillTotals(testBill, DateTime.Today).ReturnsForAnyArgs(new CalculateBillResult
			{
				InterestCalculated = false,
			});

			var service = new AccountsReceivablePaymentViewModel(eventPublisher, globalAccountsReceivableSettings, commandDispatcher, customerPartyQueryHandler, arBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, paymentService, commentQueryHandler, defaultBillTypeQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, crTransactionGroupRepository);
			service.Transaction = new AccountsReceivableTransaction
			{
				AccountNumber = testCustomerId,
				CorrelationId = new Guid()
			};

			foreach (var bill in service.Bills)
			{
				service.UpdateBalanceRemainingOnBill(bill);
			}

			var result = service.PaymentGreaterThanOwed("Auto", 50);

			Assert.True(result == false);
		}


		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
