﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;
using Xunit;
using Comment = SharedApplication.UtilityBilling.Models.Comment;
using CommentSearchCriteria = SharedApplication.UtilityBilling.CommentSearchCriteria;
using CustomerPartySearchCriteria = SharedApplication.UtilityBilling.CustomerPartySearchCriteria;

namespace SharedApplicationTests
{
	public class UtilityBillingPaymentViewModelTests
	{
		[Fact]
		public void ReturnFalseOnInvalidAccount()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalUtilityBillingSettings = Substitute.For<GlobalUtilityBillingSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>>();
			var ubBillQueryHandler = Substitute.For<IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var billCalculationService = Substitute.For<IBillCalculationService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var masterQueryHandler = Substitute.For<IQueryHandler<MasterSearchCriteria, IEnumerable<Master>>>();
			var moduleAssociationQueryHandler = Substitute.For<IQueryHandler <ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>>();
			var commentRecQueryHandler = Substitute.For<IQueryHandler <CommentSearchCriteria, IEnumerable<CommentRec>>>();
			var reMasterQueryHandler = Substitute.For<IQueryHandler <REMasterSearchCriteria, IEnumerable<REMaster>>>();
			var tblAcctACHQueryHandler = Substitute.For<IQueryHandler <tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>>>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler <ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var dischargeNeededQueryHandler = Substitute.For<IQueryHandler <DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>>>();
			var environmentSettings = Substitute.For<DataEnvironmentSettings>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testCustomerId = 1;
			var testBillNumber = 101010;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(false);
			ubBillQueryHandler.ExecuteQuery(Arg.Is<UBBillSearchCriteria>(x => x.AccountId == testCustomerId)).Returns(
				new List<UBBill>
				{
					new UBBill
					{
						BillNumber = testBillNumber,

					}
				});

			var service = new UtilityBillingPaymentViewModel(eventPublisher, globalUtilityBillingSettings, commandDispatcher, customerPartyQueryHandler, ubBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, billCalculationService, commentQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, masterQueryHandler, environmentSettings, reMasterQueryHandler, tblAcctACHQueryHandler, dischargeNeededQueryHandler, receiptTypeQueryHandler, commentRecQueryHandler, moduleAssociationQueryHandler, crTransactionGroupRepository);
			var result = service.ValidManualAccount(testAccount, UtilityType.Sewer);

			Assert.True(result == false);
		}

		[Fact]
		public void RemoveAllPendingPaymentsWorksCorrectly()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalUtilityBillingSettings = Substitute.For<GlobalUtilityBillingSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>>();
			var ubBillQueryHandler = Substitute.For<IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var billCalculationService = Substitute.For<IBillCalculationService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var masterQueryHandler = Substitute.For<IQueryHandler<MasterSearchCriteria, IEnumerable<Master>>>();
			var moduleAssociationQueryHandler = Substitute.For<IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>>();
			var commentRecQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<CommentRec>>>();
			var reMasterQueryHandler = Substitute.For<IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>>>();
			var tblAcctACHQueryHandler = Substitute.For<IQueryHandler<tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>>>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var dischargeNeededQueryHandler = Substitute.For<IQueryHandler<DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>>>();
			var environmentSettings = Substitute.For<DataEnvironmentSettings>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testCustomerId = 1;
			var testAccountNumber = 10;
			var testBillNumber = 101010;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(false);
			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(
					new List<UBCustomerPaymentStatusResult>
					{
						new UBCustomerPaymentStatusResult
						{
							Id = testCustomerId,
							AccountNumber = testAccountNumber
						}
					});

			masterQueryHandler.ExecuteQuery(Arg.Is<MasterSearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(
				new List<Master>
				{
					new Master
					{
						ID = testCustomerId,
					}
				});

			ubBillQueryHandler.ExecuteQuery(Arg.Is<UBBillSearchCriteria>(x => x.AccountId == testCustomerId)).Returns(
				new List<UBBill>
				{
					new UBBill
					{
						BillNumber = testBillNumber,
						UtilityDetails = new List<UBBillUtilityDetails>
						{
							new UBBillUtilityDetails
							{
								Service = UtilityType.Water,
								Payments = new List<UBPayment>
								{
									new UBPayment
									{
										Pending = true,
										Code = "P",
										Principal = 10,
									}
								}
							}
						}
					}
				});

			var service = new UtilityBillingPaymentViewModel(eventPublisher, globalUtilityBillingSettings, commandDispatcher, customerPartyQueryHandler, ubBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, billCalculationService, commentQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, masterQueryHandler, environmentSettings, reMasterQueryHandler, tblAcctACHQueryHandler, dischargeNeededQueryHandler, receiptTypeQueryHandler, commentRecQueryHandler, moduleAssociationQueryHandler, crTransactionGroupRepository);
			service.Transaction = new UtilityBillingTransaction
			{
				AccountNumber = testAccountNumber
			};

			Assert.True(service.PendingPaymentsExist());
			service.RemoveAllPendingPayments();
			Assert.True(service.PendingPaymentsExist() == false);
		}

		[Fact]
		public void RemovePendingPaymentWorksCorrectly()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalUtilityBillingSettings = Substitute.For<GlobalUtilityBillingSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>>();
			var ubBillQueryHandler = Substitute.For<IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var billCalculationService = Substitute.For<IBillCalculationService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var masterQueryHandler = Substitute.For<IQueryHandler<MasterSearchCriteria, IEnumerable<Master>>>();
			var moduleAssociationQueryHandler = Substitute.For<IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>>();
			var commentRecQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<CommentRec>>>();
			var reMasterQueryHandler = Substitute.For<IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>>>();
			var tblAcctACHQueryHandler = Substitute.For<IQueryHandler<tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>>>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var dischargeNeededQueryHandler = Substitute.For<IQueryHandler<DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>>>();
			var environmentSettings = Substitute.For<DataEnvironmentSettings>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testCustomerId = 1;
			var testAccountNumber = 10;
			var testBillNumber = 101010;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(false);
			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(
					new List<UBCustomerPaymentStatusResult>
					{
						new UBCustomerPaymentStatusResult
						{
							Id = testCustomerId,
							AccountNumber = testAccountNumber
						}
					});

			masterQueryHandler.ExecuteQuery(Arg.Is<MasterSearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(
				new List<Master>
				{
					new Master
					{
						ID = testCustomerId,
					}
				});

			ubBillQueryHandler.ExecuteQuery(Arg.Is<UBBillSearchCriteria>(x => x.AccountId == testCustomerId)).Returns(
				new List<UBBill>
				{
					new UBBill
					{
						BillNumber = testBillNumber,
						UtilityDetails = new List<UBBillUtilityDetails>
						{
							new UBBillUtilityDetails
							{
								Service = UtilityType.Water,
								Payments = new List<UBPayment>
								{
									new UBPayment
									{
										Pending = true,
										Code = "P",
										Principal = 10,
										Id = 100
									}
								}
							}
						}
					}
				});

			var service = new UtilityBillingPaymentViewModel(eventPublisher, globalUtilityBillingSettings, commandDispatcher, customerPartyQueryHandler, ubBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, billCalculationService, commentQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, masterQueryHandler, environmentSettings, reMasterQueryHandler, tblAcctACHQueryHandler, dischargeNeededQueryHandler, receiptTypeQueryHandler, commentRecQueryHandler, moduleAssociationQueryHandler, crTransactionGroupRepository);
			service.Transaction = new UtilityBillingTransaction
			{
				AccountNumber = testAccountNumber
			};

			Assert.True(service.PendingPaymentsExist());
			var payment = service.GetPayment(100);
			service.RemovePendingPayment(payment);
			Assert.True(service.PendingPaymentsExist() == false);
		}

		[Fact]
		public void GetBillWithPaymentWorksCorrectly()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalUtilityBillingSettings = Substitute.For<GlobalUtilityBillingSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>>();
			var ubBillQueryHandler = Substitute.For<IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var billCalculationService = Substitute.For<IBillCalculationService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var masterQueryHandler = Substitute.For<IQueryHandler<MasterSearchCriteria, IEnumerable<Master>>>();
			var moduleAssociationQueryHandler = Substitute.For<IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>>();
			var commentRecQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<CommentRec>>>();
			var reMasterQueryHandler = Substitute.For<IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>>>();
			var tblAcctACHQueryHandler = Substitute.For<IQueryHandler<tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>>>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var dischargeNeededQueryHandler = Substitute.For<IQueryHandler<DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>>>();
			var environmentSettings = Substitute.For<DataEnvironmentSettings>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testCustomerId = 1;
			var testAccountNumber = 10;
			var testBillNumber = 101010;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(false);
			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(
					new List<UBCustomerPaymentStatusResult>
					{
						new UBCustomerPaymentStatusResult
						{
							Id = testCustomerId,
							AccountNumber = testAccountNumber
						}
					});

			masterQueryHandler.ExecuteQuery(Arg.Is<MasterSearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(
				new List<Master>
				{
					new Master
					{
						ID = testCustomerId,
					}
				});

			ubBillQueryHandler.ExecuteQuery(Arg.Is<UBBillSearchCriteria>(x => x.AccountId == testCustomerId)).Returns(
				new List<UBBill>
				{
					new UBBill
					{
						BillNumber = testBillNumber,
						UtilityDetails = new List<UBBillUtilityDetails>
						{
							new UBBillUtilityDetails
							{
								Service = UtilityType.Water,
								Payments = new List<UBPayment>
								{
									new UBPayment
									{
										Pending = true,
										Code = "P",
										Principal = 10,
										Id = 100
									}
								}
							}
						}
					}
				});

			var service = new UtilityBillingPaymentViewModel(eventPublisher, globalUtilityBillingSettings, commandDispatcher, customerPartyQueryHandler, ubBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, billCalculationService, commentQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, masterQueryHandler, environmentSettings, reMasterQueryHandler, tblAcctACHQueryHandler, dischargeNeededQueryHandler, receiptTypeQueryHandler, commentRecQueryHandler, moduleAssociationQueryHandler, crTransactionGroupRepository);
			service.Transaction = new UtilityBillingTransaction
			{
				AccountNumber = testAccountNumber
			};

			var payment = service.GetPayment(100);
			var bill = service.GetBill(payment);
			Assert.True(bill != null);
			Assert.True(bill.BillNumber == testBillNumber);
		}

		[Fact]
		public void GetBillWithBillNumberWorksCorrectly()
		{
			var eventPublisher = Substitute.For<EventPublisher>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();
			var activeModuleSettings = Substitute.For<IGlobalActiveModuleSettings>();
			var globalUtilityBillingSettings = Substitute.For<GlobalUtilityBillingSettings>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>>();
			var ubBillQueryHandler = Substitute.For<IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>>>();
			var groupMasterQueryHandler =
				Substitute.For<IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>>>();
			var billCalculationService = Substitute.For<IBillCalculationService>();
			var commentQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>>>();
			var receiptQueryHandler = Substitute.For<IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>>>();
			var lastYearReceiptQueryHandler =
				Substitute.For<IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>>>();
			var accountValidationService = Substitute.For<IAccountValidationService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var masterQueryHandler = Substitute.For<IQueryHandler<MasterSearchCriteria, IEnumerable<Master>>>();
			var moduleAssociationQueryHandler = Substitute.For<IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>>();
			var commentRecQueryHandler = Substitute.For<IQueryHandler<CommentSearchCriteria, IEnumerable<CommentRec>>>();
			var reMasterQueryHandler = Substitute.For<IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>>>();
			var tblAcctACHQueryHandler = Substitute.For<IQueryHandler<tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>>>();
			var receiptTypeQueryHandler = Substitute.For<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>();
			var dischargeNeededQueryHandler = Substitute.For<IQueryHandler<DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>>>();
			var environmentSettings = Substitute.For<DataEnvironmentSettings>();
			var crTransactionGroupRepository = Substitute.For<CRTransactionGroupRepository>();

			var testAccount = "E 10-10-10-10";
			var testCustomerId = 1;
			var testAccountNumber = 10;
			var testBillNumber = 101010;

			activeModuleSettings.BudgetaryIsActive.Returns(true);
			accountValidationService.AccountValidate(testAccount).Returns(false);
			customerPartyQueryHandler
				.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(
					new List<UBCustomerPaymentStatusResult>
					{
						new UBCustomerPaymentStatusResult
						{
							Id = testCustomerId,
							AccountNumber = testAccountNumber
						}
					});

			masterQueryHandler.ExecuteQuery(Arg.Is<MasterSearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(
				new List<Master>
				{
					new Master
					{
						ID = testCustomerId,
					}
				});

			ubBillQueryHandler.ExecuteQuery(Arg.Is<UBBillSearchCriteria>(x => x.AccountId == testCustomerId)).Returns(
				new List<UBBill>
				{
					new UBBill
					{
						BillNumber = testBillNumber,
						UtilityDetails = new List<UBBillUtilityDetails>
						{
							new UBBillUtilityDetails
							{
								Service = UtilityType.Water,
								Payments = new List<UBPayment>
								{
									new UBPayment
									{
										Pending = true,
										Code = "P",
										Principal = 10,
										Id = 100
									}
								}
							}
						}
					}
				});

			var service = new UtilityBillingPaymentViewModel(eventPublisher, globalUtilityBillingSettings, commandDispatcher, customerPartyQueryHandler, ubBillQueryHandler, activeModuleSettings, groupMasterQueryHandler, billCalculationService, commentQueryHandler, accountValidationService, budgetaryAccountingService, receiptQueryHandler, lastYearReceiptQueryHandler, masterQueryHandler, environmentSettings, reMasterQueryHandler, tblAcctACHQueryHandler, dischargeNeededQueryHandler, receiptTypeQueryHandler, commentRecQueryHandler, moduleAssociationQueryHandler, crTransactionGroupRepository);
			service.Transaction = new UtilityBillingTransaction
			{
				AccountNumber = testAccountNumber
			};

			var payment = service.GetPayment(100);
			var bill = service.GetBill(testBillNumber.ToString());
			Assert.True(bill != null);
			Assert.True(bill.BillNumber == testBillNumber);
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
