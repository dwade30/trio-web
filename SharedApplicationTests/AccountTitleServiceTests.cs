﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.CentralData;
using Xunit;

namespace SharedApplicationTests
{
    public class AccountTitleServiceTests
    {
		[Fact]
		public void ReturnCorrectExpenseAccountTitle()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
		

			var testAccount = "E 10-100-29-10";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToExpenseAccountBreakdown(testBreakdown).Returns(
				new ExpenseAccountBreakdown
				{
					Department = "10",
					Division = "100",
					Expense = "29",
					Object = "10"
				});

			//deptDivTitleQueryHandler.ExecuteQuery(new DeptDivTitleSearchCriteria()).ReturnsForAnyArgs(x => ReturnValuesForDeptDivQuery(x.Arg<DeptDivTitleSearchCriteria>()));
			//expObjTitleQueryHandler.ExecuteQuery(new ExpObjTitleSearchCriteria()).ReturnsForAnyArgs(x => ReturnValuesForExpObjQuery(x.Arg<ExpObjTitleSearchCriteria>()));

			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "000")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "000",
					ShortDescription = "Dept Short",
					LongDescription = "Dept Long"
				}
			});
			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "100")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "100",
					ShortDescription = "Div Short",
					LongDescription = "Div Long"
				}
			});
			expObjTitleQueryHandler.ExecuteQuery(Arg.Is<ExpObjTitleSearchCriteria>(x => x.Expense == "29" && x.Object == "00")).Returns(new List<ExpObjTitle>()
			{
				new ExpObjTitle()
				{
					Expense = "29",
					Object = "00",
					ShortDescription = "Exp Short",
					LongDescription = "Exp Long"
				}
			});
			expObjTitleQueryHandler.ExecuteQuery(Arg.Is<ExpObjTitleSearchCriteria>(x => x.Expense == "29" && x.Object == "10")).Returns(new List<ExpObjTitle>()
			{
				new ExpObjTitle()
				{
					Expense = "29",
					Object = "10",
					ShortDescription = "Obj Short",
					LongDescription = "Obj Long"
				}
			});

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, false);
			Assert.True(result == "Dept Short / Div Short - Exp Short / Obj Short");
		}

		[Fact]
		public void ReturnCorrectExpenseAccountTitleLongDescription()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "E 10-100-29-10";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToExpenseAccountBreakdown(testBreakdown).Returns(
				new ExpenseAccountBreakdown
				{
					Department = "10",
					Division = "100",
					Expense = "29",
					Object = "10"
				});

			//deptDivTitleQueryHandler.ExecuteQuery(new DeptDivTitleSearchCriteria()).ReturnsForAnyArgs(x => ReturnValuesForDeptDivQuery(x.Arg<DeptDivTitleSearchCriteria>()));
			//expObjTitleQueryHandler.ExecuteQuery(new ExpObjTitleSearchCriteria()).ReturnsForAnyArgs(x => ReturnValuesForExpObjQuery(x.Arg<ExpObjTitleSearchCriteria>()));

			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "000")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "000",
					ShortDescription = "Dept Short",
					LongDescription = "Dept Long"
				}
			});
			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "100")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "100",
					ShortDescription = "Div Short",
					LongDescription = "Div Long"
				}
			});
			expObjTitleQueryHandler.ExecuteQuery(Arg.Is<ExpObjTitleSearchCriteria>(x => x.Expense == "29" && x.Object == "00")).Returns(new List<ExpObjTitle>()
			{
				new ExpObjTitle()
				{
					Expense = "29",
					Object = "00",
					ShortDescription = "Exp Short",
					LongDescription = "Exp Long"
				}
			});
			expObjTitleQueryHandler.ExecuteQuery(Arg.Is<ExpObjTitleSearchCriteria>(x => x.Expense == "29" && x.Object == "10")).Returns(new List<ExpObjTitle>()
			{
				new ExpObjTitle()
				{
					Expense = "29",
					Object = "10",
					ShortDescription = "Obj Short",
					LongDescription = "Obj Long"
				}
			});

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, true);
			Assert.True(result == "Dept Long / Div Long - Exp Long / Obj Long");
		}

		[Fact]
		public void ReturnCorrectExpenseAccountTitleNoDiv()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "E 10-29-10";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "29",
				ThirdAccountField = "10",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("0");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToExpenseAccountBreakdown(testBreakdown).Returns(
				new ExpenseAccountBreakdown
				{
					Department = "10",
					Division = "",
					Expense = "29",
					Object = "10"
				});

			//deptDivTitleQueryHandler.ExecuteQuery(new DeptDivTitleSearchCriteria()).ReturnsForAnyArgs(x => ReturnValuesForDeptDivQuery(x.Arg<DeptDivTitleSearchCriteria>()));
			//expObjTitleQueryHandler.ExecuteQuery(new ExpObjTitleSearchCriteria()).ReturnsForAnyArgs(x => ReturnValuesForExpObjQuery(x.Arg<ExpObjTitleSearchCriteria>()));

			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "0",
					ShortDescription = "Dept Short",
					LongDescription = "Dept Long"
				}
			});
			expObjTitleQueryHandler.ExecuteQuery(Arg.Is<ExpObjTitleSearchCriteria>(x => x.Expense == "29" && x.Object == "00")).Returns(new List<ExpObjTitle>()
			{
				new ExpObjTitle()
				{
					Expense = "29",
					Object = "00",
					ShortDescription = "Exp Short",
					LongDescription = "Exp Long"
				}
			});
			expObjTitleQueryHandler.ExecuteQuery(Arg.Is<ExpObjTitleSearchCriteria>(x => x.Expense == "29" && x.Object == "10")).Returns(new List<ExpObjTitle>()
			{
				new ExpObjTitle()
				{
					Expense = "29",
					Object = "10",
					ShortDescription = "Obj Short",
					LongDescription = "Obj Long"
				}
			});

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, false);
			Assert.True(result == "Dept Short - Exp Short / Obj Short");
		}

		[Fact]
		public void ReturnCorrectExpenseAccountTitleNoDivObj()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "E 10-29";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "29",
				ThirdAccountField = "",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("0");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("0");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToExpenseAccountBreakdown(testBreakdown).Returns(
				new ExpenseAccountBreakdown
				{
					Department = "10",
					Division = "",
					Expense = "29",
					Object = ""
				});

			//deptDivTitleQueryHandler.ExecuteQuery(new DeptDivTitleSearchCriteria()).ReturnsForAnyArgs(x => ReturnValuesForDeptDivQuery(x.Arg<DeptDivTitleSearchCriteria>()));
			//expObjTitleQueryHandler.ExecuteQuery(new ExpObjTitleSearchCriteria()).ReturnsForAnyArgs(x => ReturnValuesForExpObjQuery(x.Arg<ExpObjTitleSearchCriteria>()));

			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "0",
					ShortDescription = "Dept Short",
					LongDescription = "Dept Long"
				}
			});
			expObjTitleQueryHandler.ExecuteQuery(Arg.Is<ExpObjTitleSearchCriteria>(x => x.Expense == "29" && x.Object == "")).Returns(new List<ExpObjTitle>()
			{
				new ExpObjTitle()
				{
					Expense = "29",
					Object = "0",
					ShortDescription = "Exp Short",
					LongDescription = "Exp Long"
				}
			});
			

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, false);
			Assert.True(result == "Dept Short - Exp Short");
		}

		[Fact]
		public void ReturnCorrectRevenueAccountTitle()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "R 10-100-29";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "R",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(true);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToRevenueAccountBreakdown(testBreakdown).Returns(
				new RevenueAccountBreakdown()
				{
					Department = "10",
					Division = "100",
					Revenue = "29"

				});

			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "000")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "000",
					ShortDescription = "Dept Short",
					LongDescription = "Dept Long"
				}
			});
			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "100")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "100",
					ShortDescription = "Div Short",
					LongDescription = "Div Long"
				}
			});
			revTitleQueryHandler.ExecuteQuery(Arg.Is<RevTitleSearchCriteria>(x => x.Department == "10" && x.Division == "100" && x.Revenue == "29")).Returns(new List<RevTitle>()
			{
				new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "29",
					ShortDescription = "Rev Short",
					LongDescription = "Rev Long"
				}
			});
			
			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, false);
			Assert.True(result == "Dept Short / Div Short - Rev Short");
		}

		[Fact]
		public void ReturnCorrectRevenueAccountTitleLongDescription()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "R 10-100-29";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "R",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(true);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToRevenueAccountBreakdown(testBreakdown).Returns(
				new RevenueAccountBreakdown()
				{
					Department = "10",
					Division = "100",
					Revenue = "29"

				});

			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "000")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "000",
					ShortDescription = "Dept Short",
					LongDescription = "Dept Long"
				}
			});
			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "100")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "100",
					ShortDescription = "Div Short",
					LongDescription = "Div Long"
				}
			});
			revTitleQueryHandler.ExecuteQuery(Arg.Is<RevTitleSearchCriteria>(x => x.Department == "10" && x.Division == "100" && x.Revenue == "29")).Returns(new List<RevTitle>()
			{
				new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "29",
					ShortDescription = "Rev Short",
					LongDescription = "Rev Long"
				}
			});

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, true);
			Assert.True(result == "Dept Long / Div Long - Rev Long");
		}

		[Fact]
		public void ReturnCorrectRevenueAccountTitleNoDiv()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "R 10-29";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "R",
				FirstAccountField = "10",
				SecondAccountField = "29",
				ThirdAccountField = "",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(false);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("0");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToRevenueAccountBreakdown(testBreakdown).Returns(
				new RevenueAccountBreakdown()
				{
					Department = "10",
					Division = "",
					Revenue = "29"

				});

			deptDivTitleQueryHandler.ExecuteQuery(Arg.Is<DeptDivTitleSearchCriteria>(x => x.Department == "10" && x.Division == "")).Returns(new List<DeptDivTitle>()
			{
				new DeptDivTitle()
				{
					Department = "10",
					Division = "0",
					ShortDescription = "Dept Short",
					LongDescription = "Dept Long"
				}
			});
			revTitleQueryHandler.ExecuteQuery(Arg.Is<RevTitleSearchCriteria>(x => x.Department == "10" && x.Division == "" && x.Revenue == "29")).Returns(new List<RevTitle>()
			{
				new RevTitle
				{
					Department = "10",
					Division = "",
					Revenue = "29",
					ShortDescription = "Rev Short",
					LongDescription = "Rev Long"
				}
			});

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, false);
			Assert.True(result == "Dept Short - Rev Short");
		}

		[Fact]
		public void ReturnCorrectLedgerAccountTitle()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "G 10-100-29";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "G",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.SuffixExistsInLedger().Returns(true);
			globalBudgetaryAccountSettings.AccountZeroString().Returns("000");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToLedgerAccountBreakdown(testBreakdown).Returns(
				new LedgerAccountBreakdown()
				{
					Fund = "10",
					Account = "100",
					Suffix = "29"
				});

			ledgerTitleQueryHandler.ExecuteQuery(Arg.Is<LedgerTitleSearchCriteria>(x => x.Fund == "10" && x.Account == "000" && x.Suffix == "")).Returns(new List<LedgerTitle>()
			{
				new LedgerTitle
				{
					Fund = "10",
					Account = "000",
					Year = "",
					ShortDescription = "Fund Short",
					LongDescription = "Fund Long"
				}
			});

			ledgerTitleQueryHandler.ExecuteQuery(Arg.Is<LedgerTitleSearchCriteria>(x => x.Fund == "10" && x.Account == "100" && x.Suffix == "29")).Returns(new List<LedgerTitle>()
			{
				new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "29",
					ShortDescription = "Ledger Short",
					LongDescription = "Ledger Long"
				}
			});

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, false);
			Assert.True(result == "Fund Short / Ledger Short");
		}

		[Fact]
		public void ReturnCorrectLedgerAccountTitleNoSuffix()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "G 10-100";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "G",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.SuffixExistsInLedger().Returns(false);
			globalBudgetaryAccountSettings.AccountZeroString().Returns("000");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToLedgerAccountBreakdown(testBreakdown).Returns(
				new LedgerAccountBreakdown()
				{
					Fund = "10",
					Account = "100",
					Suffix = ""
				});

			ledgerTitleQueryHandler.ExecuteQuery(Arg.Is<LedgerTitleSearchCriteria>(x => x.Fund == "10" && x.Account == "000" && x.Suffix == "")).Returns(new List<LedgerTitle>()
			{
				new LedgerTitle
				{
					Fund = "10",
					Account = "000",
					Year = "",
					ShortDescription = "Fund Short",
					LongDescription = "Fund Long"
				}
			});

			ledgerTitleQueryHandler.ExecuteQuery(Arg.Is<LedgerTitleSearchCriteria>(x => x.Fund == "10" && x.Account == "100" && x.Suffix == "")).Returns(new List<LedgerTitle>()
			{
				new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "",
					ShortDescription = "Ledger Short",
					LongDescription = "Ledger Long"
				}
			});

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, false);
			Assert.True(result == "Fund Short / Ledger Short");
		}

		[Fact]
		public void ReturnCorrectLedgerAccountTitleLongDescription()
		{
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var expObjTitleQueryHandler = Substitute.For<IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>>>();
			var revTitleQueryHandler = Substitute.For<IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();


			var testAccount = "G 10-100-29";
			AccountBreakdownInformation testBreakdown = new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "G",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			globalBudgetaryAccountSettings.SuffixExistsInLedger().Returns(true);
			globalBudgetaryAccountSettings.AccountZeroString().Returns("000");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(testBreakdown);
			budgetaryAccountingService.ConvertAccountBreakdownToLedgerAccountBreakdown(testBreakdown).Returns(
				new LedgerAccountBreakdown()
				{
					Fund = "10",
					Account = "100",
					Suffix = "29"
				});

			ledgerTitleQueryHandler.ExecuteQuery(Arg.Is<LedgerTitleSearchCriteria>(x => x.Fund == "10" && x.Account == "000" && x.Suffix == "")).Returns(new List<LedgerTitle>()
			{
				new LedgerTitle
				{
					Fund = "10",
					Account = "000",
					Year = "",
					ShortDescription = "Fund Short",
					LongDescription = "Fund Long"
				}
			});

			ledgerTitleQueryHandler.ExecuteQuery(Arg.Is<LedgerTitleSearchCriteria>(x => x.Fund == "10" && x.Account == "100" && x.Suffix == "29")).Returns(new List<LedgerTitle>()
			{
				new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "29",
					ShortDescription = "Ledger Short",
					LongDescription = "Ledger Long"
				}
			});

			var service = new AccountTitleService(deptDivTitleQueryHandler, expObjTitleQueryHandler, revTitleQueryHandler, ledgerTitleQueryHandler, globalBudgetaryAccountSettings, budgetaryAccountingService);
			var result = service.ReturnAccountDescription(testAccount, true);
			Assert.True(result == "Fund Long / Ledger Long");
		}

		private IEnumerable<ExpObjTitle> ReturnValuesForExpObjQuery(ExpObjTitleSearchCriteria searchCriteria)
		{
			if (searchCriteria.Expense == "29" && searchCriteria.Object == "00")
			{
				return new List<ExpObjTitle>()
				{
					new ExpObjTitle()
					{
						Expense = "29",
						Object = "00",
						ShortDescription = "Exp Short",
						LongDescription = "Exp Long"
					}
				};
			}
			else if (searchCriteria.Expense == "29" && searchCriteria.Object == "10")
			{
				return new List<ExpObjTitle>()
				{
					new ExpObjTitle()
					{
						Expense = "29",
						Object = "10",
						ShortDescription = "Obj Short",
						LongDescription = "Obj Long"
					}
				};
			}
			else
			{
				return null;
			}
		}

		private IEnumerable<DeptDivTitle> ReturnValuesForDeptDivQuery(DeptDivTitleSearchCriteria searchCriteria)
		{
			if (searchCriteria.Department == "10" && searchCriteria.Division == "000")
			{
				return new List<DeptDivTitle>()
				{
					new DeptDivTitle()
					{
						Department = "10",
						Division = "000",
						ShortDescription = "Dept Short",
						LongDescription = "Dept Long"
					}
				};
			}
			else if (searchCriteria.Department == "10" && searchCriteria.Division == "100")
			{
				return new List<DeptDivTitle>()
				{
					new DeptDivTitle()
					{
						Department = "10",
						Division = "100",
						ShortDescription = "Div Short",
						LongDescription = "Div Long"
					}
				};
			}
			else
			{
				return null;
			}
		}
	}
}
