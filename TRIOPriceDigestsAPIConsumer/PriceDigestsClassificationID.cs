﻿using SharedApplication.BlueBook.Models;

namespace TRIOPriceDigestsAPIConsumer
{
    public enum PriceDigestsClassificationID
    {
        None = 0,
        CommercialTrucks = 2,
        PassengerVehicles = 4,
        Powersport = 5,
        RecreationalVehicles = 6,
        CommercialTrailers = 7,
        TruckBodies = 8
    }

    public enum PriceDigestsCategoryID
    {  
        None = 0,
        Automobiles = 21,
        LightTrucks = 25,
        Motorcycle = 29,
        HeavyTrucks = 74
    }



    public static class PriceDigestsExtensions
    {

        private static PriceDigestsCategoryID CategoryFromInteger(int value)
        {
            switch (value)
            {
                case 21:
                    return PriceDigestsCategoryID.Automobiles;
                case 25:
                    return PriceDigestsCategoryID.LightTrucks;
                case 29:
                    return PriceDigestsCategoryID.Motorcycle;
                default:
                    return PriceDigestsCategoryID.None;
            }
        }
        private static PriceDigestsClassificationID ClassificationFromInteger(int value)
        {
            switch (value)
            {
                case 2:
                    return PriceDigestsClassificationID.CommercialTrucks;
                case 4:
                    return PriceDigestsClassificationID.PassengerVehicles;
                case 5:
                    return PriceDigestsClassificationID.Powersport;
                case 6:
                    return PriceDigestsClassificationID.RecreationalVehicles;
                case 7:
                    return PriceDigestsClassificationID.CommercialTrailers;
                case 8:
                    return PriceDigestsClassificationID.TruckBodies;
                default:
                    return PriceDigestsClassificationID.None;
            }
        }

        public static int ToInteger(this PriceDigestsClassificationID value)
        {
            return (int) value;
        }

        public static int ToInteger(this PriceDigestsCategoryID value)
        {
            return (int) value;
        }
        public static PriceDigestsClassificationID ClassificationEnum(this VehicleInformation vehicleInformation)
        {
            return ClassificationFromInteger(vehicleInformation.ClassificationId);
        }

        public static PriceDigestsCategoryID CategoryEnum(this VehicleInformation vehicleInformation)
        {
            return CategoryFromInteger(vehicleInformation.CategoryId);
        }
    }
}