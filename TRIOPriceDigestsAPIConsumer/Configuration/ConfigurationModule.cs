﻿using Autofac;
using SharedApplication.BlueBook;

namespace TRIOPriceDigestsAPIConsumer.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TrioPriceDigestsAPIBlueBookRepository>().AsSelf().As<IBlueBookRepository>();
            base.Load(builder);
        }
    }
}