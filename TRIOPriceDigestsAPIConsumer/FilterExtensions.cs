﻿using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.HeavyTrucks;
using SharedApplication.BlueBook.Manufacturer;
using SharedApplication.BlueBook.Rbcars;
using SharedApplication.BlueBook.Rvs;
using SharedApplication.BlueBook.Services;
using SharedApplication.BlueBook.TruckBodies;
using SharedApplication.Extensions;

namespace TRIOPriceDigestsAPIConsumer
{
    public static class FilterExtensions
    {
        public static IEnumerable<(string Name, string value)> ToParameters(this ManufacturerFilterCriteria filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();

            if (filterCriteria != null)
            {
                if (filterCriteria.VehicleTypes.Any())
                {
                    var code = filterCriteria.VehicleTypes.FirstOrDefault();
                    switch (code)
                    {
                        case "C":
                        case "T":
                            parameters.Add((Name: "classificationId",Value: PriceDigestsClassificationID.PassengerVehicles.ToInteger().ToString()));
                            break;
                        case "M":
                            parameters.Add((Name: "categoryId",Value: PriceDigestsCategoryID.Motorcycle.ToInteger().ToString()));
                            break;
                        case "B":
                            parameters.Add((Name:"classificationId", Value: PriceDigestsClassificationID.TruckBodies.ToInteger().ToString()));
                            break;
                        case "R":
                            parameters.Add((Name: "classificationId",Value: PriceDigestsClassificationID.RecreationalVehicles.ToInteger().ToString()));
                            break;
                        case "H":
                            parameters.Add((Name:"classificationId",Value: PriceDigestsClassificationID.CommercialTrucks.ToInteger().ToString()));
                            break;
                        case "X":
                            parameters.Add((Name:"classificationId",Value: PriceDigestsClassificationID.CommercialTrailers.ToInteger().ToString()));
                            break;
                    }
                }
            }

            return parameters;
        }

        public static ( PriceDigestsClassificationID classification, PriceDigestsCategoryID category) VehicleCategoryCodeToClassification(string code)
        {
            PriceDigestsClassificationID classification = PriceDigestsClassificationID.None;
            var categoryId = PriceDigestsCategoryID.Automobiles;
            switch (code.ToUpper())
            {
                case "C":
                    classification = PriceDigestsClassificationID.PassengerVehicles;
                    categoryId = PriceDigestsCategoryID.Automobiles;
                    break;
                case "T":
                    classification = PriceDigestsClassificationID.PassengerVehicles;
                    categoryId = PriceDigestsCategoryID.LightTrucks;
                    break;
                case "M":
                    classification = PriceDigestsClassificationID.Powersport;
                    categoryId = PriceDigestsCategoryID.Motorcycle;
                    break;
                case "H":
                    classification = PriceDigestsClassificationID.CommercialTrucks;
                    categoryId = PriceDigestsCategoryID.None;
                    break;
                case "R":
                    classification = PriceDigestsClassificationID.RecreationalVehicles;
                    categoryId = PriceDigestsCategoryID.None;
                    break;
                case "X":
                    classification = PriceDigestsClassificationID.CommercialTrailers;
                    categoryId = PriceDigestsCategoryID.None;
                    break;

            }

            return (classification: classification, category: categoryId);
        }

        public static IEnumerable<(string Name, string value)> ToParameters(
            this VehicleConfigurationFilter filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();
            if (filterCriteria != null)
            {
                if (filterCriteria.ManufacturerName.HasText())
                {
                    parameters.Add((Name:"manufacturer",Value:filterCriteria.ManufacturerName));
                }
                if (filterCriteria.ModelId > 0)
                {
                    parameters.Add((Name:"modelId",Value: filterCriteria.ModelId.ToString()));
                }

                if (filterCriteria.ModelName.HasText())
                {
                    parameters.Add((Name: "model", Value: filterCriteria.ModelName));
                }
                if (filterCriteria.Year > 0)
                {
                    parameters.Add((Name: "year",Value: filterCriteria.Year.ToString()));
                }

                if (filterCriteria.ClassificationId > 0)
                {
                    parameters.Add((Name: "classificationId",Value: filterCriteria.ClassificationId.ToString()));
                }

                if (filterCriteria.CategoryId > 0)
                {
                    parameters.Add((Name: "categoryId",Value: filterCriteria.CategoryId.ToString()));
                }
            }

            return parameters;
        }

        public static IEnumerable<(string Name, string value)> ToParameters(this CycleFilterCriteria filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();
            parameters.Add((Name: "categoryId", Value: PriceDigestsCategoryID.Motorcycle.ToInteger().ToString()));
            if (filterCriteria != null)
            {
                
                if (filterCriteria.Year > 0)
                {
                    parameters.Add((Name:"year",Value: filterCriteria.Year.ToString()));
                }

                if (filterCriteria.Manufacturer.HasText())
                {
                    parameters.Add((Name:"manufacturer",Value: filterCriteria.Manufacturer));
                }

                if (filterCriteria.ModelNumber.HasText())
                {
                    parameters.Add((Name:"model",Value: filterCriteria.ModelNumber));
                }
            }

            return parameters;
        }

        public static IEnumerable<(string Name, string value)> ToParameters(this RbCarFilterCriteria filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();
            parameters.Add((Name: "classificationId", Value: PriceDigestsClassificationID.PassengerVehicles.ToInteger().ToString()));
            if (filterCriteria != null)
            {
                if (filterCriteria.Year > 0)
                {
                    parameters.Add((Name:"year", Value: filterCriteria.Year.ToString()));
                }

                if (filterCriteria.Manufacturer.HasText())
                {
                    parameters.Add((Name:"manufacturer",Value: filterCriteria.Manufacturer));
                }

                if (filterCriteria.ModelNumber.HasText())
                {
                    parameters.Add((Name:"model",Value: filterCriteria.ModelNumber));
                }
            }

            return parameters;
        }

        public static IEnumerable<(string Name, string value)> ToParameters(
            this HeavyTruckFilterCriteria filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();
            parameters.Add((Name: "categoryId",Value: PriceDigestsCategoryID.HeavyTrucks.ToInteger().ToString()));
            if (filterCriteria != null)
            {
                if (filterCriteria.Year > 0)
                {
                    parameters.Add((Name:"year",Value: filterCriteria.Year.ToString()));
                }

                if (filterCriteria.Manufacturer.HasText())
                {
                    parameters.Add((Name: "manufacturer", Value: filterCriteria.Manufacturer));
                }

                if (filterCriteria.ModelNumber.HasText())
                {
                    parameters.Add((Name:"model",Value: filterCriteria.ModelNumber));
                }
            }

            return parameters;
        }

        public static IEnumerable<(string Name, string value)> ToParameters(this OptionFilterCriteria filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();
            if (filterCriteria != null)
            {
                parameters.Add((Name: "sizeClassId",Value: filterCriteria.SizeClassId.ToString()));
                if (filterCriteria.Year > 0)
                {
                    parameters.Add((Name: "year", Value: filterCriteria.Year.ToString()));
                }
            }
            else
            {
                parameters.Add((Name: "sizeClassId", Value: "0"));
            }
            return parameters;
        }

        public static IEnumerable<(string Name, string value)> ToParameters(this RvFilterCriteria filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();
            parameters.Add((Name: "classificationId", Value: PriceDigestsClassificationID.RecreationalVehicles.ToInteger().ToString()));
            if (filterCriteria != null)
            {
                if (filterCriteria.Year > 0)
                {
                    parameters.Add((Name: "year", Value: filterCriteria.Year.ToString()));
                }
                if (filterCriteria.Manufacturer.HasText())
                {
                    parameters.Add((Name: "manufacturer", Value: filterCriteria.Manufacturer));
                }

                if (filterCriteria.ModelNumber.HasText())
                {
                    parameters.Add((Name: "model", Value: filterCriteria.ModelNumber));
                }
            }

            return parameters;
        }

        public static IEnumerable<(string Name, string value)> ToParameters(this TruckBodyDataFilterCriteria filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();
            parameters.Add((Name: "classificationId",Value: PriceDigestsClassificationID.TruckBodies.ToInteger().ToString()));
            if (filterCriteria != null)
            {
                if (filterCriteria.Year > 0)
                {
                    parameters.Add((Name: "year", Value: filterCriteria.Year.ToString()));
                }
                if (filterCriteria.Manufacturer.HasText())
                {
                    parameters.Add((Name: "manufacturer", Value: filterCriteria.Manufacturer));
                }

                if (filterCriteria.ModelNumber.HasText())
                {
                    parameters.Add((Name: "model", Value: filterCriteria.ModelNumber));
                }
            }
            return parameters;
        }

        public static IEnumerable<(string Name, string value)> ToParameters(
            this TrailerDataFilterCriteria filterCriteria)
        {
            var parameters = new List<(string Name, string Value)>();
            parameters.Add((Name: "classificationId", Value: PriceDigestsClassificationID.CommercialTrailers.ToInteger().ToString()));
            if (filterCriteria != null)
            {
                if (filterCriteria.Year > 0)
                {
                    parameters.Add((Name: "year", Value: filterCriteria.Year.ToString()));
                }
                if (filterCriteria.Manufacturer.HasText())
                {
                    parameters.Add((Name: "manufacturer", Value: filterCriteria.Manufacturer));
                }

                if (filterCriteria.Model.HasText())
                {
                    parameters.Add((Name: "model", Value: filterCriteria.Model));
                }
            }

            return parameters;
        }
    }
}

