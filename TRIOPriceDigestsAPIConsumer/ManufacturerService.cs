﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Manufacturer;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer
{
    public class ManufacturerService : BlueBookAPIBase, IManufacturerService
    {
        private string relativePath = "manufacturers";
        public ManufacturerService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<Mfg> GetManufacturers(ManufacturerFilterCriteria filterCriteria)
        {
            //return GetList<Mfg, ManufacturerFilterCriteria>("manufacturer", filterCriteria);
            var returnedData = GetListWithParameters<Mfg,PriceDigestManufacturer>(relativePath, filterCriteria.ToParameters());
            return new List<Mfg>();
        }
    }
}