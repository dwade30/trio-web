﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.OptionPrices;
using SharedApplication.Extensions;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class VehicleOptionService : BlueBookAPIBase, IOptionsService
    {
        public VehicleOptionService(string apiUri) : base(apiUri)
        {
        }

        public VehicleConfigurationOptions GetOptions(OptionFilterCriteria filterCriteria)
        {
            return GetOptionsWithParameters(@"values/options",
                filterCriteria.ToParameters());
        }

        protected VehicleConfigurationOptions GetOptionsWithParameters(string relativePath,
            IEnumerable<(string Name, string value)> parameters) 
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                int offset = 0;
                var fullUri = CreateFullPath(relativePath);
                var uriBuilder = new UriBuilder(fullUri);
                try
                {
                    var firstRequest = true;
                    VehicleConfigurationOptions options = null;
                    HttpResponseMessage response;
                    bool emptyCollection = false;
                    do
                    {
                        var kvps = parameters.ToDictionary(p => p.Name, p => p.value);
                        kvps.Add("limit", PageSize.ToString());
                        kvps.Add("offset", offset.ToString());
                        string query;
                        using (var content = new FormUrlEncodedContent(kvps))
                        {
                            query = content.ReadAsStringAsync().Result;
                        }
                        uriBuilder.Query = query;
                        response = client.GetAsync(uriBuilder.Uri).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var jsonResult = response.Content.ReadAsStringAsync().Result;
                            var returnedData = JsonSerializer.Deserialize<PriceDigestConfigurationOptions>(jsonResult,
                                new JsonSerializerOptions()
                                {
                                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                                    PropertyNameCaseInsensitive = true,
                                    AllowTrailingCommas = true,
                                    IgnoreNullValues = true
                                });
                            if (firstRequest)
                            {
                                options = returnedData.ToVehicleConfigurationOptions();
                                options.Options.RemoveAll(o =>
                                    o.MSRP == 0 || o.FamilyName.ToLower() == "transmission" ||
                                    o.FamilyName.ToLower() == "engine" || o.FamilyName.ToLower() == "rear axle" ||
                                    o.FamilyName.ToLower() == "front axle");
                                firstRequest = false;
                            }
                            else
                            {
                                if (returnedData.options.Any())
                                {
                                    var optionsWithValues =
                                        returnedData.options.Where(o => o.optionMSRP.ToDecimalValue() > 0 && o.optionFamilyName.ToLower() != "transmission" && o.optionFamilyName.ToLower() != "engine" && o.optionFamilyName.ToLower() != "rear axle" && o.optionFamilyName.ToLower() != "front axle");
                                    if (optionsWithValues.Any())
                                    {
                                        options.Options.AddRange(optionsWithValues.ToVehicleOptions());
                                    }
                                }
                                else
                                {
                                    emptyCollection = true;
                                }
                            }
                        }
                        offset += PageSize;
                    } while (response.IsSuccessStatusCode && !emptyCollection);
                    return options;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        protected IEnumerable<TRetrieveType> GetListWithParameters<TRetrieveType>(string relativePath,
            IEnumerable<(string Name, string value)> parameters)
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                int offset = 0;
                var fullUri = CreateFullPath(relativePath);
                var uriBuilder = new UriBuilder(fullUri);


                try
                {

                    var returnList = new List<TRetrieveType>();
                    HttpResponseMessage response;
                    do
                    {
                        var kvps = parameters.ToDictionary(p => p.Name, p => p.value);
                        kvps.Add("limit", PageSize.ToString());
                        kvps.Add("offset", offset.ToString());
                        string query;
                        using (var content = new FormUrlEncodedContent(kvps))
                        {
                            query = content.ReadAsStringAsync().Result;
                        }

                        uriBuilder.Query = query;
                        response = client.GetAsync(uriBuilder.Uri).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var jsonResult = response.Content.ReadAsStringAsync().Result;
                            var returnedData = JsonSerializer.Deserialize<List<TRetrieveType>>(jsonResult,
                                new JsonSerializerOptions()
                                {
                                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                                    PropertyNameCaseInsensitive = true,
                                    AllowTrailingCommas = true,
                                    IgnoreNullValues = true
                                });
                            returnList.AddRange(returnedData);
                        }

                        offset += PageSize;
                    } while (response.IsSuccessStatusCode);

                    return returnList.AsEnumerable();

                }
                catch (Exception ex)
                {
                    return new List<TRetrieveType>();
                }
            }
        }
    }
}
