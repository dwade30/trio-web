﻿using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.Rvs;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class RvService : BlueBookAPIBase, IRvService
    {
        public RvService(string apiUri) : base(apiUri)
        {
        }

        public Rv GetRv(int id)
        {
            var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                                   new[] { ("configurationId", id.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
            return vehicleSpecs?.ToRv();
        }

        public IEnumerable<Rv> GetRvs(RvFilterCriteria filterCriteria)
        {
            var configurations =
                GetListWithParameters<VehicleConfiguration, PriceDigestVehicleConfiguration>(@"taxonomy/configurations",
                    filterCriteria.ToParameters());
            var rvs = new List<Rv>();
            foreach (var configuration in configurations)
            {
                var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                    new[] { ("configurationId", configuration.ConfigurationId.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
                if (vehicleSpecs != null)
                {
                    rvs.Add(vehicleSpecs.ToRv());
                }
            }

            return rvs;
        }
    }
}