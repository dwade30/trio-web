﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.Services;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class VehicleSpecService : BlueBookAPIBase, IVehicleSpecService
    {
        public VehicleConfigurationSpecs GetVehicleSpecs(int configurationId)
        {
            return GetWithParameters< PriceDigestConfigurationSpecs>(@"specs/basic/",
                new[] {("configurationId", configurationId.ToString())})?.ToVehicleConfigurationSpecs() ?? null;
        }

        public IEnumerable<VehicleConfigurationSpecs> GetVehicleListSpecs(IEnumerable<int> configurations)
        {
            var parameters = configurations.Select(o => new[] {("configurationId", o.ToString())});
            return GetWithParametersFromList<PriceDigestConfigurationSpecs>(@"specs/basic", parameters).ToVehicleConfigurationSpecs();
        }

        //public IEnumerable<VehicleConfigurationSpecs> GetVehicleListSpecs(IEnumerable<ConfigurationVinIdPair> configurations)
        //{
        //    var parameters = configurations.Select(o => new[] { ("configurationId", o.ToString()) });
        //    return GetWithParametersFromList<PriceDigestConfigurationSpecs>(@"specs/basic", parameters).ToVehicleConfigurationSpecs();
        //}

        public VehicleSpecService(string apiUri) : base(apiUri)
        {
        }
    }

    //public class ConfigurationPairs
    //{
    //    public IEnumerable<(string Name, string value)> Parameters { get; set; }
    //    public string Vin { get; set; }

    //    public ConfigurationPairs(string vin,IEnumerable< (string Name, string value)> parameters)
    //    {
    //        Vin = vin;
    //        Parameters = parameters;
    //    }
    //}
}