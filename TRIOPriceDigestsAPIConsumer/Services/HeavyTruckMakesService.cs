﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using TRIOPriceDigestsAPIConsumer;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class HeavyTruckMakesService : BlueBookAPIBase, IHeavyTruckMakeService
    {
        public HeavyTruckMakesService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<HeavyTruckMakeList> GetHeavyTruckMakes()
        {
            var parameters = new List<(string Name, string Value)>()
            {
                (Name:"categoryId",Value: PriceDigestsCategoryID.HeavyTrucks.ToInteger().ToString())
            };
            return GetListWithParameters<VehicleManufacturer, PriceDigestManufacturer>(@"taxonomy/manufacturers", parameters)
                .ToHeavyTruckMakeLists();
        }
    }
}