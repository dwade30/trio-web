﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.Services;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class CycleService : BlueBookAPIBase, ICycleService
    {
        public CycleService(string apiUri) : base(apiUri)
        {
        }

        public Cycle GetCycle(int id)
        {
            var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                                   new[] { ("configurationId", id.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
            return vehicleSpecs?.ToCycle();
        }


        public IEnumerable<Cycle> GetCycles(CycleFilterCriteria filterCriteria)
        {
            var configurations =
                GetListWithParameters<VehicleConfiguration, PriceDigestVehicleConfiguration>(@"taxonomy/configurations",
                    filterCriteria.ToParameters());
            var cycles = new List<Cycle>();
            foreach (var configuration in configurations)
            {
                var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                    new[] { ("configurationId", configuration.ConfigurationId.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
                if (vehicleSpecs != null)
                {
                    cycles.Add(vehicleSpecs.ToCycle());
                }
            }

            return cycles;
        }

    }

    public static class CycleExtensions
    {
       
    }
}