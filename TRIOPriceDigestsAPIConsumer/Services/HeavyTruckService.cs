﻿using System.Collections.Generic;
using System.Linq;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.HeavyTrucks;
using SharedApplication.BlueBook.Models;
using SharedApplication.Extensions;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class HeavyTruckService : BlueBookAPIBase, IHeavyTruckService
    {
        public HeavyTruckService(string apiUri) : base(apiUri)
        {
        }

        public HeavyTruck GetHeavyTruck(int id)
        {
            var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                                   new[] { ("configurationId", id.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
            return vehicleSpecs?.ToHeavyTruck();
        }

        public IEnumerable<HeavyTruck> GetHeavyTrucks(HeavyTruckFilterCriteria filterCriteria)
        {
            var configurations =
                GetListWithParameters<VehicleConfiguration, PriceDigestVehicleConfiguration>(@"taxonomy/configurations",
                    filterCriteria.ToParameters());
            var trucks = new List<HeavyTruck>();
            foreach (var configuration in configurations)
            {
                var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                    new[] { ("configurationId", configuration.ConfigurationId.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
                if (vehicleSpecs != null)
                {
                    trucks.Add(vehicleSpecs.ToHeavyTruck());
                }
            }

            return trucks.OrderBy(x => x.Make).ThenBy(x => x.ModelNumber).ToList();
        }
    }

   
}