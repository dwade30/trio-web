﻿using System.Collections.Generic;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.Services;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class VehicleConfigurationsService : BlueBookAPIBase, IVehicleConfigurationsService //, IVehicleConfigurationsService
    {

        public IEnumerable<VehicleConfiguration> GetConfigurations(VehicleConfigurationFilter filter)
        {

            return GetListWithParameters<VehicleConfiguration, PriceDigestVehicleConfiguration>(@"taxonomy/configurations",
                filter.ToParameters());
            
        }

        public VehicleConfiguration GetConfiguration(int id)
        {
            return GetById<VehicleConfiguration>(@"taxonomy/configurations",id);
        }
        public VehicleConfigurationsService(string apiUri) : base(apiUri)
        {

        }
    }
}