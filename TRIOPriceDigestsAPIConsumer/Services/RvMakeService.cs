﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using TRIOPriceDigestsAPIConsumer.Models;
using TRIOPriceDigestsAPIConsumer;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class RvMakeService : BlueBookAPIBase, IRvMakeListService
    {
        public RvMakeService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<RvmakeList> GetRvMakes()
        {

            var parameters = new List<(string Name, string Value)>()
            {
                (Name:"classificationId",Value: PriceDigestsClassificationID.RecreationalVehicles.ToInteger().ToString())
            };
            return GetListWithParameters<VehicleManufacturer, PriceDigestManufacturer>(@"taxonomy/manufacturers", parameters)
                .ToRvMakeLists();
        }
    }
}