﻿using System.Collections.Generic;
using System.Linq;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.Rbcars;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class RbCarService : BlueBookAPIBase, IRbCarService
    {
        public RbCarService(string apiUri) : base(apiUri)
        {
        }

        public Rbcar GetRbCar(int id)
        {
            //var configuration =  GetById<PriceDigestVehicleConfiguration>(@"taxonomy/configurations", id).ToVehicleConfiguration();
            var configuration =
                GetListByValue<VehicleConfiguration, PriceDigestVehicleConfiguration>(@"taxonomy/configurations",
                    id.ToString()).FirstOrDefault();
            var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                new[] { ("configurationId", id.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
            var car = vehicleSpecs?.ToRbcar();
            car.ModelNumber = configuration.VinModelNumber;
            return car;
        }

        public IEnumerable<Rbcar> GetRbCars(RbCarFilterCriteria filterCriteria)
        {
            //return GetList<Rbcar, RbCarFilterCriteria>("rbcar", filterCriteria);
            var configurations =
                GetListWithParameters<VehicleConfiguration, PriceDigestVehicleConfiguration>(@"taxonomy/configurations",
                    filterCriteria.ToParameters());
            var cars = new List<Rbcar>();
            foreach (var configuration in configurations)
            {
                var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                    new[] { ("configurationId", configuration.ConfigurationId.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
                if (vehicleSpecs != null)
                {
                    cars.Add(vehicleSpecs.ToRbcar());
                }
            }

            return cars;
        }
    }

    //public static class RbcarExtensions
    //{
    //    public static Rbcar ToRbcar(this VehicleConfigurationSpecs configurationSpecs)
    //    {
    //        var specs = configurationSpecs.Specs.ToDictionary(s => s.Name);
    //        return new Rbcar()
    //        {
    //            Id = configurationSpecs.ConfigurationId,
    //            Manufacturer = configurationSpecs.ManufacturerName,
    //            Description = specs.TryGetValue("description", out var description)? description?.Value: "",
    //            Model = configurationSpecs.ModelName,
    //            Year = configurationSpecs.ModelYear.ToString(),
    //            GrossVehicleWeight = specs.TryGetValue("gvw", out var gvw)? gvw?.Value : "",
    //            EquipmentPackage = specs.TryGetValue("equipmentCode", out var equipment)? gvw?.Value : "",
    //            Engine = specs.TryGetValue("engineSize", out var engine)? engine?.Value : "",
    //            Transmission = specs.TryGetValue("transmissionType", out var transmission)? transmission?.Value:"",
    //            NumberOfPassengers = specs.TryGetValue("passengers", out var passengers)? passengers?.Value: "",
    //            WheelBase = specs.TryGetValue("wheelbase", out var wheelbase)? wheelbase?.Value: "",
    //            ModelNumber = specs.TryGetValue("vinModelNumber", out var modelNumber)? modelNumber?.Value: ""
    //        };
    //    }

    //}
}