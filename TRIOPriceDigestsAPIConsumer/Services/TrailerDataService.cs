﻿using System.Collections.Generic;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class TrailerDataService : BlueBookAPIBase, ITrailerDataService
    {
        public TrailerDataService(string apiUri) : base(apiUri)
        {
        }

        public TrailerData GetTrailerData(int id)
        {
            var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                                   new[] { ("configurationId", id.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
            return vehicleSpecs?.ToTrailerData();
        }

        public IEnumerable<TrailerData> GetTrailerDatas(TrailerDataFilterCriteria filterCriteria)
        {
            var configurations =
                GetListWithParameters<VehicleConfiguration, PriceDigestVehicleConfiguration>(@"taxonomy/configurations",
                    filterCriteria.ToParameters());
            var trailers = new List<TrailerData>();
            foreach (var configuration in configurations)
            {
                var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                    new[] { ("configurationId", configuration.ConfigurationId.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
                if (vehicleSpecs != null)
                {
                    trailers.Add(vehicleSpecs.ToTrailerData());
                }
            }

            return trailers;
        }

        public IEnumerable<VehicleManufacturer> GetTrailerManufacturersForYear(int year)
        {
            var parameters = new List<(string Name, string Value)>()
            {
                (Name:"classificationId",Value: PriceDigestsClassificationID.CommercialTrailers.ToInteger().ToString()),
                (Name:"year",Value: year.ToString())
            };

            return GetListWithParameters<VehicleManufacturer, PriceDigestManufacturer>(@"taxonomy/manufacturers", parameters);
        }


    }
}