﻿using System.Collections.Generic;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class VinService : BlueBookAPIBase,IVinService
    {
        public IEnumerable<VehicleInformation> GetVehicleInformation(string vin)
        {
            return GetListByValue<VehicleInformation,PriceDigestVehicleInformation>(@"verification/vin", vin);
        }

        public VinService(string apiUri) : base(apiUri)
        {
        }
    }
}