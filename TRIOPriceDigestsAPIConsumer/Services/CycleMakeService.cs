﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Text.Json;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class CycleMakeService: BlueBookAPIBase, ICycleMakeListService
    {
        public CycleMakeService(string apiUri) : base(apiUri)
        {
        }
        public IEnumerable<CycleMakeList> GetCycleMakes()
        {
            var parameters = new List<(string Name, string Value)>()
            {
                (Name:"categoryId",Value: PriceDigestsCategoryID.Motorcycle.ToInteger().ToString())
            };
            return GetListWithParameters<VehicleManufacturer, PriceDigestManufacturer>(@"taxonomy/manufacturers", parameters)
                .ToCycleMakeLists();
        }


    }
}