﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using BlueBook;
using SharedApplication.BlueBook;
using TRIOPriceDigestsAPIConsumer;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class MakeService : BlueBookAPIBase, IMakeService
    {
        public MakeService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<string> GetCarManufacturers()
        {


            return GetList<string>("CarManufacturer");
        }

        public IEnumerable<string> GetCarAndLightTruckManufacturers()
        {
            //return GetList<string>("CarAndLightTruckManufacturer");
            var manufacturers = GetListWithParameters<Mfg, PriceDigestManufacturer>(@"taxonomy/manufacturers",
                new List<(string Name, string value)>()
                {
                    (Name: "classificationId",
                        value: PriceDigestsClassificationID.PassengerVehicles.ToInteger().ToString())
                });
            return manufacturers.Select(m => m.Manufacturer);
        }

        public IEnumerable<string> GetLightTruckManufacturers()
        {
            return GetList<string>("LightTruckManufacturer");
            var manufacturers = GetListWithParameters<Mfg, PriceDigestManufacturer>(@"taxonomy/manufacturers",
                new List<(string Name, string value)>()
                {
                    (Name: "classificationId",
                        value: PriceDigestsClassificationID.PassengerVehicles.ToInteger().ToString())
                });
            return manufacturers.Select(m => m.Manufacturer);
        }

        public IEnumerable<string> GetCarAndLightTruckManufacturerShortDescriptions(string manufacturer)
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath(@"CarAndLightTruckManufacturerShortDescription/" + manufacturer)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<IEnumerable<string>>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true });
                }
                else
                {
                    return new List<string>();
                }
            }
        }

        public IEnumerable<(string manufacturer, int year)> GetTrailerMakeForYear(int year)
        {
            return new List<(string manufacturer, int year)>();
        }
    }
}