﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.TruckBodies;
using TRIOPriceDigestsAPIConsumer.Models;

namespace TRIOPriceDigestsAPIConsumer.Services
{
    public class TruckBodyService : BlueBookAPIBase, ITruckBodyDataService
    {
        public TruckBodyService(string apiUri) : base(apiUri)
        {
        }

        public TruckBodyData GetTruckBodyData(int id)
        {
            var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                                   new[] { ("configurationId", id.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
            return vehicleSpecs?.ToTruckBodyData();
        }

        public IEnumerable<TruckBodyData> GetTruckBodyDatas(TruckBodyDataFilterCriteria filterCriteria)
        {
            var configurations =
                GetListWithParameters<VehicleConfiguration, PriceDigestVehicleConfiguration>(@"taxonomy/configurations",
                    filterCriteria.ToParameters());
            var truckBodies = new List<TruckBodyData>();
            foreach (var configuration in configurations)
            {
                var vehicleSpecs = GetWithParameters<PriceDigestConfigurationSpecs>(@"specs/basic/",
                    new[] { ("configurationId", configuration.ConfigurationId.ToString()) })?.ToVehicleConfigurationSpecs() ?? null;
                if (vehicleSpecs != null)
                {
                    truckBodies.Add(vehicleSpecs.ToTruckBodyData());
                }
            }

            return truckBodies;
        }

        public IEnumerable<(string manufacturer, int year)> GetTrailerMakeForYear(int year)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<VehicleManufacturer> GetTruckBodyManufacturers(int year)
        {
            var parameters = new List<(string Name, string Value)>()
            {
                (Name:"classificationId",Value: PriceDigestsClassificationID.TruckBodies.ToInteger().ToString()),
                (Name:"year",Value: year.ToString())
            };
            
            return GetListWithParameters<VehicleManufacturer, PriceDigestManufacturer>(@"taxonomy/manufacturers", parameters);
        }
    }
}