﻿using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Services;
using TRIOPriceDigestsAPIConsumer.Services;

namespace TRIOPriceDigestsAPIConsumer
{
    public class TrioPriceDigestsAPIBlueBookRepository : IBlueBookRepository
    {
        private string priceDigestsUrl = @"https://pricedigestsapi.com/v1";

        public TrioPriceDigestsAPIBlueBookRepository()
        {
            VehicleInformation = new VinService(priceDigestsUrl);
            VehicleConfigurations = new VehicleConfigurationsService(priceDigestsUrl);
            VehicleSpecs = new VehicleSpecService(priceDigestsUrl);
            Makes = new MakeService(priceDigestsUrl);
            Cycles = new CycleService(priceDigestsUrl);
            CycleMakes = new CycleMakeService(priceDigestsUrl);
            HeavyTruckMakes = new HeavyTruckMakesService(priceDigestsUrl);
            HeavyTrucks = new HeavyTruckService(priceDigestsUrl);
            Options = new VehicleOptionService(priceDigestsUrl);
            RbCars = new RbCarService(priceDigestsUrl);
            RvMakes = new RvMakeService(priceDigestsUrl);
            Rvs = new RvService(priceDigestsUrl);
            TrailerData = new TrailerDataService(priceDigestsUrl);
            TruckBodyDatas = new TruckBodyService(priceDigestsUrl);
        }

        public ICycleService Cycles { get; }
        public IHeavyTruckService HeavyTrucks { get; }
        public IManufacturerService Manufacturers { get; }
        public IRbCarService RbCars { get; }
        public ITrailerDataService TrailerData { get; }
        public IRvService Rvs { get; }
        public IHeavyTruckMakeService HeavyTruckMakes { get; }
        public IRvMakeListService RvMakes { get; }
        public ICycleMakeListService CycleMakes { get; }
        public IOptionsService Options { get; }
        public IOptionPricesService OptionPrices { get; }
        public IRVXrefService RvXrefs { get; }
        public ITruckBodyOptionService TruckBodyOptions { get; }
        public IEddieService Eddies { get; }
        public IMakeService Makes { get; }
        public ITruckBodyDataService TruckBodyDatas { get; }
        public IVehicleConfigurationsService VehicleConfigurations { get; }
        public IVinService VehicleInformation { get; }
        public IVehicleSpecService VehicleSpecs { get; }
    }
}