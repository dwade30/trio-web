﻿namespace TRIOPriceDigestsAPIConsumer.Models
{
    public class PriceDigestVehicleInformation
    {
        public int modelId { get; set; } = 0;
        public string modelName { get; set; } = "";
        public int configurationId { get; set; } = 0;
        public int categoryId { get; set; } = 0;
        public string categoryName { get; set; } = "";
        public int manufacturerId { get; set; } = 0;
        public string[] manufacturerAliases { get; set; }// = new string[] {};
        public int classificationId { get; set; } = 0;
        public string classificationName { get; set; } = "";
        public string vinModelNumber { get; set; } = "";
        public string modelYear { get; set; } = "";
        public string manufacturerName { get; set; } = "";
    }
}