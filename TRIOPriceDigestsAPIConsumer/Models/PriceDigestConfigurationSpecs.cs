﻿using System.Linq;
using SharedApplication.BlueBook.Models;
using SharedApplication.Extensions;

namespace TRIOPriceDigestsAPIConsumer.Models
{
    public class PriceDigestConfigurationSpecs
    {
        public int modelId { get; set; } = 0;
        public string modelName { get; set; } = "";
        public int configurationId { get; set; } = 0;
        public string modelYear { get; set; } = "";
        public string manufacturerName { get; set; } = "";
        public int classificationId { get; set; } = 0;
        public int sizeClassId { get; set; } = 0;
        public string sizeClassName { get; set; } = "";
        public string subTypeName { get; set; } = "";
        public PriceDigestVehicleSpec[] specs { get; set; }
    }

    public class PriceDigestVehicleSpec
    {
        public string specName { get; set; } = "";
        public string specNameFriendly { get; set; } = "";
        public string specUom { get; set; } = "";
        public string specDescription { get; set; } = "";
        public string specFamily { get; set; } = "";
        public string specValue { get; set; } = "";
    }
}