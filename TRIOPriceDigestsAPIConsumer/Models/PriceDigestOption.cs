﻿namespace TRIOPriceDigestsAPIConsumer.Models
{
    public class PriceDigestOption
    {
        public int optionFamilyId { get; set; } = 0;
        public string optionFamilyName { get; set; } = "";
        public string optionName { get; set; } = "";
        public string optionValue { get; set; } = "";
        public string optionMSRP { get; set; } = "";
    }
}