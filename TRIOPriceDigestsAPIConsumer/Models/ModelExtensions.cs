﻿using System.Collections.Generic;
using System.Linq;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.Extensions;

namespace TRIOPriceDigestsAPIConsumer.Models
{
    public static class ModelExtensions
    {
        public static VehicleManufacturer ToVehicleManufacturer(this PriceDigestManufacturer manufacturer)
        {
            return new VehicleManufacturer(manufacturer.manufacturerId, manufacturer.manufacturerName,
                manufacturer.manufacturerAliases);
        }

        public static IEnumerable<VehicleManufacturer> ToVehicleManufacturers(
            this IEnumerable<PriceDigestManufacturer> manufacturers)
        {
            return manufacturers.Select(m => m.ToVehicleManufacturer());
        }


    }
}