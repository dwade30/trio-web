﻿namespace TRIOPriceDigestsAPIConsumer.Models
{
    public class PriceDigestManufacturer
    {
        public int manufacturerId { get; set; }
        public string manufacturerName { get; set; }
        public string[] manufacturerAliases { get; set; }
    }
}