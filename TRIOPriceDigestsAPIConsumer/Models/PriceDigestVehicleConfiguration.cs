﻿namespace TRIOPriceDigestsAPIConsumer.Models
{
    public class PriceDigestVehicleConfiguration
    {
        public int modelId { get; set; } = 0;
        public string modelName { get; set; } = "";
        public int manufacturerId { get; set; } = 0;
        public int classificationId { get; set; } = 0;
        public int categoryId { get; set; } = 0;
        public string categoryName { get; set; } = "";
        public int subtypeId { get; set; } = 0;
        public string subtypeName { get; set; } = "";
        public int sizeClassId { get; set; } = 0;
        public string sizeClassName { get; set; } = "";
        public int sizeClassMin { get; set; } = 0;
        public int sizeClassMax { get; set; } = 0;
        public string sizeClassUom { get; set; } = "";
        public int configurationId { get; set; } = 0;
        public string modelYear { get; set; } = "";
        public string vinModelNumber { get; set; } = "";
    }
}