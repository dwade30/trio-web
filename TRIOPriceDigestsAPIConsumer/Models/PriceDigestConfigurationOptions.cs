﻿namespace TRIOPriceDigestsAPIConsumer.Models
{
    public class PriceDigestConfigurationOptions
    {
        public int classificationId { get; set; } = 0;
        public string classificationName { get; set; } = "";
        public int categoryId { get; set; } = 0;
        public int subtypeId { get; set; } = 0;
        public string subtypeName { get; set; } = "";
        public int sizeClassId { get; set; } = 0;
        public string sizeClassUom { get; set; } = "";
        public string modelYear { get; set; } = "";

        public PriceDigestOption[] options { get; set; }
    }
}