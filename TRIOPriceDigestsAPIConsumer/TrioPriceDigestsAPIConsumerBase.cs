﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.Extensions;
using TRIOPriceDigestsAPIConsumer.Models;
using TRIOPriceDigestsAPIConsumer.Services;

namespace TRIOPriceDigestsAPIConsumer
{
    public abstract class BlueBookAPIBase
    {
        protected string apiUri;
        protected const string apiKey = "bed534da16041f19ff95e527a38cd6";
        protected const string apiKeyName = "X-Api-Key";
        protected int PageSize { get; set; } = 50;
        protected HttpClient sharedClient = new HttpClient();
        public BlueBookAPIBase(string apiUri)
        {
            this.apiUri = apiUri;
            AddHeaders(sharedClient);
        }

        protected string CreateFullPath(string relativePath)
        {
            return apiUri + @"/" + relativePath;
        }

        protected void AddHeaders(HttpClient client)
        {
            client.DefaultRequestHeaders.Add(apiKeyName, apiKey);
        }

        protected IEnumerable<T> GetList<T>(string relativePath)
        {
            return GetListWithParameters<T>(relativePath, new List<(string Name, string value)>());
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath(relativePath)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<List<T>>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true });
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        protected T GetById<T>(string relativePath, int id) where T : class
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath(relativePath + @"/" + id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<T>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true });
                }
                else
                {
                    return null;
                }
            }
        }

        protected IEnumerable<T> GetListByValue<T,TRetrieveType>(string relativePath, string value) where T : class
        {
            var returnedData = GetListWithParameters<TRetrieveType>(relativePath + @"/" + value,
                new List<(string Name, string value)>());
            return TranslateModels<T, TRetrieveType>(returnedData);
            //using (var client = new HttpClient())
            //{
            //    AddHeaders(client);
            //    var response = client.GetAsync(CreateFullPath(relativePath + @"/" + value)).Result;
            //    if (response.IsSuccessStatusCode)
            //    {
            //        var jsonResult = response.Content.ReadAsStringAsync().Result;
            //        var returnedData = JsonSerializer.Deserialize<List<TRetrieveType>>(jsonResult, new JsonSerializerOptions()
            //        {
            //            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            //            PropertyNameCaseInsensitive = true,
            //            AllowTrailingCommas = true,
            //            IgnoreNullValues = true
            //        });
            //        return TranslateModels<T, TRetrieveType>(returnedData.AsEnumerable());
            //    }
            //    else
            //    {
            //        return new List<T>();
            //    }
            //}
        }

        protected IEnumerable<T> TranslateModels<T, TSourceType>(IEnumerable<TSourceType> sourceData) where T: class
        {
            switch (sourceData)
            {
                case IEnumerable<PriceDigestVehicleInformation> vehicles when typeof(T).ToString()  == typeof(VehicleInformation).ToString():
                    return (IEnumerable<T>) vehicles.ToVehicleInformations();
                    break;
                case IEnumerable<PriceDigestManufacturer> manufacturers when typeof(T).ToString() ==  typeof(Mfg).ToString():
                    return (IEnumerable<T>)manufacturers.Select(m => new Mfg() { Manufacturer = m.manufacturerName, Id = m.manufacturerId });
                    break;
                case IEnumerable<PriceDigestVehicleConfiguration> configurations when typeof(T).ToString() == typeof(VehicleConfiguration).ToString():
                    return (IEnumerable<T>)configurations.ToVehicleConfigurations();
                case IEnumerable<PriceDigestManufacturer> manufacturers when typeof(T).ToString() == typeof(VehicleManufacturer).ToString():
                    return (IEnumerable<T>) manufacturers.ToVehicleManufacturers();
                    break;
                case IEnumerable<PriceDigestOption> options when typeof(T).ToString() == typeof(VehicleOption).ToString():
                    return (IEnumerable<T>) options.ToVehicleOptions();
                default:
                    return new List<T>();
            }
        }

        

        protected IEnumerable<T> GetListWithParameters<T,TRetrieveType>(string relativePath,
            IEnumerable<(string Name, string value)> parameters) where T: class
        {
            var returnedData = GetListWithParameters<TRetrieveType>(relativePath, parameters);
            return TranslateModels<T, TRetrieveType>(returnedData);
        }

        protected IEnumerable<TRetrieveType> GetListWithParameters<TRetrieveType>(string relativePath,
            IEnumerable<(string Name, string value)> parameters)
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                int offset = 0;
                var fullUri = CreateFullPath(relativePath);
                var uriBuilder = new UriBuilder(fullUri);
                try
                {
                    var returnList = new List<TRetrieveType>();
                    HttpResponseMessage response;
                    var stillCheck = false;
                    do
                    {
                        var kvps = parameters.ToDictionary(p => p.Name, p => p.value);
                        kvps.Add("limit",PageSize.ToString());
                        kvps.Add("offset", offset.ToString());
                        string query;
                        using (var content = new FormUrlEncodedContent(kvps))
                        {
                            query = content.ReadAsStringAsync().Result;
                        }
                        uriBuilder.Query = query;
                        response = client.GetAsync(uriBuilder.Uri).Result;
                        var woogy  = client.GetAsync(uriBuilder.Uri);
                        if (response.IsSuccessStatusCode)
                        {
                            var jsonResult = response.Content.ReadAsStringAsync().Result;
                            var returnedData = JsonSerializer.Deserialize<List<TRetrieveType>>(jsonResult, 
                                new JsonSerializerOptions()
                                {
                                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                                    PropertyNameCaseInsensitive = true,
                                    AllowTrailingCommas = true,
                                    IgnoreNullValues = true
                                });
                            returnList.AddRange(returnedData);
                            stillCheck = returnedData.Count >= PageSize;
                        }
                        offset += PageSize;
                    } while (response.IsSuccessStatusCode && stillCheck);
                    return returnList.AsEnumerable();
                }
                catch (Exception ex)
                {
                    return new List<TRetrieveType>();
                }
            }
        }

        protected TRetrieveType GetWithParameters<TRetrieveType>(string relativePath,
            IEnumerable<(string Name, string value)> parameters) where TRetrieveType : class
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var fullUri = CreateFullPath(relativePath);
                var uriBuilder = new UriBuilder(fullUri);
                var kvps = parameters.ToDictionary(p => p.Name, p => p.value);
                //var parameters = new Dictionary<string,string>(p);
                string query;
                using (var content = new FormUrlEncodedContent(kvps))
                {
                    query = content.ReadAsStringAsync().Result;
                }

                uriBuilder.Query = query;
                try
                {
                    var response = client.GetAsync(uriBuilder.Uri).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonResult = response.Content.ReadAsStringAsync().Result;
                        return JsonSerializer.Deserialize<TRetrieveType>(jsonResult, new JsonSerializerOptions()
                        {
                            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                            PropertyNameCaseInsensitive = true,
                            AllowTrailingCommas = true,
                            IgnoreNullValues = true
                        });
                       
                    }

                   
                    return null;// new List<T>();

                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        protected async Task<TRetrieveType> RetrieveAsync<TRetrieveType>(string relativePath,IEnumerable<(string Name, string value)> parameters) where TRetrieveType:class
        {
            var returnList = new List<TRetrieveType>();
            var fullUri = CreateFullPath(relativePath);
            var uriBuilder = new UriBuilder(fullUri);
            var kvps = parameters.ToDictionary(p => p.Name, p => p.value);
            //var parameters = new Dictionary<string,string>(p);
            string query;
            using (var content = new FormUrlEncodedContent(kvps))
            {
                query = content.ReadAsStringAsync().Result;
            }

            uriBuilder.Query = query;
            try
            {
                var response = await sharedClient.GetAsync(uriBuilder.Uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<TRetrieveType>(jsonResult, new JsonSerializerOptions()
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                        PropertyNameCaseInsensitive = true,
                        AllowTrailingCommas = true,
                        IgnoreNullValues = true
                    });

                }
                return null;// new List<T>();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        protected IEnumerable<TRetrieveType> GetWithParametersFromList<TRetrieveType>(string relativePath,
            IEnumerable<IEnumerable<(string Name, string value)>> items) where TRetrieveType : class
        {
            ServicePointManager.DefaultConnectionLimit = 4;
            var returnList = new List<TRetrieveType>();
            var fullUri = CreateFullPath(relativePath);
            var uriBuilder = new UriBuilder(fullUri);

            var tasks = items.Select(i => RetrieveAsync<TRetrieveType>(relativePath, i));
            return Task.WhenAll(tasks).Result.ToList();
        }
    }

    public static class PriceDigestModelExtensions
    {
        public static VehicleConfiguration ToVehicleConfiguration(this PriceDigestVehicleConfiguration configuration)
        {
            return new VehicleConfiguration()
            {
                ModelName = configuration.modelName,
                ClassificationId = configuration.classificationId,
                ModelYear = configuration.modelYear.ToIntegerValue(),
                CategoryId = configuration.categoryId,
                ManufacturerId = configuration.manufacturerId,
                CategoryName = configuration.categoryName,
                ConfigurationId = configuration.configurationId,
                ModelId = configuration.modelId,
                SizeClassId = configuration.sizeClassId,
                SizeClassMax = configuration.sizeClassMax,
                SizeClassMin = configuration.sizeClassMin,
                SizeClassName = configuration.sizeClassName,
                SizeClassUom = configuration.sizeClassUom,
                SubtypeId = configuration.subtypeId,
                SubtypeName = configuration.subtypeName,
                VinModelNumber = configuration.vinModelNumber
            };
        }
        public static IEnumerable<VehicleConfiguration> ToVehicleConfigurations(
            this IEnumerable<PriceDigestVehicleConfiguration> data)
        {
            return data.Select(d => d.ToVehicleConfiguration());
        }


        public static IEnumerable<VehicleInformation> ToVehicleInformations(
            this IEnumerable<PriceDigestVehicleInformation> data)
        {
            return data.Select(v => new VehicleInformation()
            {
                ClassificationId = v.classificationId,
                ClassificationName = v.classificationName,
                ConfigurationId = v.configurationId,
               ManufacturerAliases = v.manufacturerAliases,
                ManufacturerId = v.manufacturerId,
                ModelId = v.modelId,
                ModelName = v.modelName,
                ManufacturerName = v.manufacturerName,
                VinModelNumber = v.vinModelNumber,
                ModelYear = v.modelYear.ToIntegerValue(),
                CategoryId = v.categoryId,
                CategoryName = v.categoryName
            });
        }


        public static VehicleConfigurationSpecs ToVehicleConfigurationSpecs(this PriceDigestConfigurationSpecs data)
        {
            if (data == null)
            {
                return null;
            }
            return new VehicleConfigurationSpecs()
            { 
                ClassificationId = (VehicleClassificationNumber)data.classificationId,
                ManufacturerName = data.manufacturerName,
                ModelName = data.modelName,
                ModelYear = data.modelYear.ToIntegerValue(),
                ConfigurationId = data.configurationId,
                ModelId = data.modelId,
                SizeClassId = data.sizeClassId,
                SizeClassName = data.sizeClassName,
                Specs =  data.specs.ToVehicleSpecs().ToList(),
                 SubTypeName = data.subTypeName
                
            };
        }

        public static IEnumerable<VehicleConfigurationSpecs> ToVehicleConfigurationSpecs(
            this IEnumerable<PriceDigestConfigurationSpecs> data)
        {
            return data.Select(o => o.ToVehicleConfigurationSpecs());
        }

        
        public static VehicleSpec ToVehicleSpec(this PriceDigestVehicleSpec spec)
        {
            return new VehicleSpec()
            {
                Description = spec.specDescription,
                Family = spec.specFamily,
                Name = spec.specName,
                FriendlyName = spec.specNameFriendly,
                Uom = spec.specUom,
                Value =  spec.specValue
            };
        }

        public static IEnumerable<VehicleSpec> ToVehicleSpecs(this IEnumerable<PriceDigestVehicleSpec> specs)
        {
            return specs.Select(s => s.ToVehicleSpec());
        }

        public static VehicleOption ToVehicleOption(this PriceDigestOption option)
        {
            return new VehicleOption(option.optionFamilyId, option.optionFamilyName, option.optionName, option.optionValue.Replace("-", ""), option.optionMSRP.ToDecimalValue().AbsoluteValue());
        }

        public static IEnumerable<VehicleOption> ToVehicleOptions(this IEnumerable<PriceDigestOption> options)
        {
            return options.Select(o => o.ToVehicleOption());
        }

        public static VehicleConfigurationOptions ToVehicleConfigurationOptions(
            this PriceDigestConfigurationOptions options)
        {
            return new VehicleConfigurationOptions()
            {
                ClassificationId = (VehicleClassificationNumber)options.classificationId,
                CategoryId = options.categoryId,
                ClassificationName = options.classificationName,
                SizeClassId = options.sizeClassId,
                SubtypeName = options.subtypeName,
                SizeClassUom = options.sizeClassUom,
                SubtypeId = options.subtypeId,
                Year = options.modelYear.ToIntegerValue(),
                Options = options.options.ToVehicleOptions().ToList()
            };
        }
    }
}