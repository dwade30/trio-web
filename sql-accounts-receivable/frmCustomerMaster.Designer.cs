﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomerMaster.
	/// </summary>
	partial class frmCustomerMaster : BaseForm
	{
		public fecherFoundation.FCButton cmdEdit;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCRichTextBox txtMessage;
		public fecherFoundation.FCCheckBox chkSalesTax;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCRichTextBox txtDataEntryMessage;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkOneTimeBillMessage;
		public fecherFoundation.FCRichTextBox txtBillMessage;
		public fecherFoundation.FCCommonDialog dlg1;
		public fecherFoundation.FCTextBox txtCustomerNumber;
		public fecherFoundation.FCComboBox cboStatus;
		public fecherFoundation.FCLabel lblCustomerInfo;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCHeader lblVoterNumber;
		public fecherFoundation.FCLabel lblCustomerNumber;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCButton mnuFileBillScreen;
		public fecherFoundation.FCButton mnuFileComments;
		public fecherFoundation.FCButton mnuProcessPreviousEntry;
		public fecherFoundation.FCButton mnuProcessNextEntry;
		public fecherFoundation.FCButton cmdSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomerMaster));
			this.cmdEdit = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtMessage = new fecherFoundation.FCRichTextBox();
			this.chkSalesTax = new fecherFoundation.FCCheckBox();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.txtDataEntryMessage = new fecherFoundation.FCRichTextBox();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.chkOneTimeBillMessage = new fecherFoundation.FCCheckBox();
			this.txtBillMessage = new fecherFoundation.FCRichTextBox();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.txtCustomerNumber = new fecherFoundation.FCTextBox();
			this.cboStatus = new fecherFoundation.FCComboBox();
			this.lblCustomerInfo = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblVoterNumber = new fecherFoundation.FCHeader();
			this.lblCustomerNumber = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.mnuFileBillScreen = new fecherFoundation.FCButton();
			this.mnuFileComments = new fecherFoundation.FCButton();
			this.mnuProcessPreviousEntry = new fecherFoundation.FCButton();
			this.mnuProcessNextEntry = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSalesTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDataEntryMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOneTimeBillMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mnuFileBillScreen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mnuFileComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mnuProcessPreviousEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mnuProcessNextEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 570);
			this.BottomPanel.Size = new System.Drawing.Size(1088, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdEdit);
			this.ClientArea.Controls.Add(this.cmdSearch);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.chkSalesTax);
			this.ClientArea.Controls.Add(this.Frame5);
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.txtCustomerNumber);
			this.ClientArea.Controls.Add(this.cboStatus);
			this.ClientArea.Controls.Add(this.lblCustomerInfo);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblCustomerNumber);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Image1);
			this.ClientArea.Size = new System.Drawing.Size(1088, 510);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.mnuProcessPreviousEntry);
			this.TopPanel.Controls.Add(this.mnuProcessNextEntry);
			this.TopPanel.Controls.Add(this.mnuFileComments);
			this.TopPanel.Controls.Add(this.mnuFileBillScreen);
			this.TopPanel.Controls.Add(this.lblVoterNumber);
			this.TopPanel.Size = new System.Drawing.Size(1088, 60);
			this.TopPanel.TabIndex = 0;
			this.TopPanel.Controls.SetChildIndex(this.lblVoterNumber, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.mnuFileBillScreen, 0);
			this.TopPanel.Controls.SetChildIndex(this.mnuFileComments, 0);
			this.TopPanel.Controls.SetChildIndex(this.mnuProcessNextEntry, 0);
			this.TopPanel.Controls.SetChildIndex(this.mnuProcessPreviousEntry, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(154, 30);
			this.HeaderText.Text = "ACCOUNT #";
			// 
			// cmdEdit
			// 
			this.cmdEdit.AppearanceKey = "actionButton";
			this.cmdEdit.Enabled = false;
			this.cmdEdit.ImageSource = "icon - edit";
			this.cmdEdit.Location = new System.Drawing.Point(391, 92);
			this.cmdEdit.Name = "cmdEdit";
			this.cmdEdit.Size = new System.Drawing.Size(40, 40);
			this.cmdEdit.TabIndex = 5;
			this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.ImageSource = "icon - search";
			this.cmdSearch.Location = new System.Drawing.Point(339, 92);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(40, 40);
			this.cmdSearch.TabIndex = 4;
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtMessage);
			this.Frame3.Location = new System.Drawing.Point(421, 247);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(360, 175);
			this.Frame3.TabIndex = 9;
			this.Frame3.Text = "Message";
			// 
			// txtMessage
			// 
			this.txtMessage.Location = new System.Drawing.Point(20, 30);
			this.txtMessage.MaxLength = 255;
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(320, 125);
			// 
			// chkSalesTax
			// 
			this.chkSalesTax.Location = new System.Drawing.Point(490, 106);
			this.chkSalesTax.Name = "chkSalesTax";
			this.chkSalesTax.Size = new System.Drawing.Size(158, 27);
			this.chkSalesTax.TabIndex = 11;
			this.chkSalesTax.Text = "Charge Sales Tax";
			// 
			// Frame5
			// 
			this.Frame5.Controls.Add(this.txtDataEntryMessage);
			this.Frame5.Location = new System.Drawing.Point(30, 247);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(360, 175);
			this.Frame5.TabIndex = 8;
			this.Frame5.Text = "Data Entry Message";
			// 
			// txtDataEntryMessage
			// 
			this.txtDataEntryMessage.Location = new System.Drawing.Point(20, 30);
			this.txtDataEntryMessage.MaxLength = 255;
			this.txtDataEntryMessage.Name = "txtDataEntryMessage";
			this.txtDataEntryMessage.Size = new System.Drawing.Size(320, 125);
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.chkOneTimeBillMessage);
			this.Frame4.Controls.Add(this.txtBillMessage);
			this.Frame4.Location = new System.Drawing.Point(30, 442);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(360, 225);
			this.Frame4.TabIndex = 10;
			this.Frame4.Text = "Bill Message";
			// 
			// chkOneTimeBillMessage
			// 
			this.chkOneTimeBillMessage.Location = new System.Drawing.Point(20, 30);
			this.chkOneTimeBillMessage.Name = "chkOneTimeBillMessage";
			this.chkOneTimeBillMessage.Size = new System.Drawing.Size(137, 27);
			this.chkOneTimeBillMessage.TabIndex = 1;
			this.chkOneTimeBillMessage.Text = "One Time Only";
			// 
			// txtBillMessage
			// 
			this.txtBillMessage.Location = new System.Drawing.Point(20, 80);
			this.txtBillMessage.MaxLength = 255;
			this.txtBillMessage.Name = "txtBillMessage";
			this.txtBillMessage.Size = new System.Drawing.Size(320, 125);
			// 
			// dlg1
			// 
			this.dlg1.Name = "dlg1";
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			// 
			// txtCustomerNumber
			// 
			this.txtCustomerNumber.Location = new System.Drawing.Point(175, 92);
			this.txtCustomerNumber.Name = "txtCustomerNumber";
			this.txtCustomerNumber.Size = new System.Drawing.Size(141, 40);
			this.txtCustomerNumber.TabIndex = 3;
			this.txtCustomerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustomerNumber_Validating);
			this.txtCustomerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCustomerNumber_KeyPress);
			// 
			// cboStatus
			// 
			this.cboStatus.Items.AddRange(new object[] {
            "A  -  Active",
            "D  -  Deleted"});
			this.cboStatus.Location = new System.Drawing.Point(175, 30);
			this.cboStatus.Name = "cboStatus";
			this.cboStatus.Size = new System.Drawing.Size(141, 40);
			this.cboStatus.TabIndex = 1;
			this.cboStatus.DropDown += new System.EventHandler(this.cboStatus_DropDown);
			// 
			// lblCustomerInfo
			// 
			this.lblCustomerInfo.Location = new System.Drawing.Point(177, 152);
			this.lblCustomerInfo.Name = "lblCustomerInfo";
			this.lblCustomerInfo.Size = new System.Drawing.Size(248, 75);
			this.lblCustomerInfo.TabIndex = 7;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 152);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(86, 18);
			this.Label2.TabIndex = 6;
			this.Label2.Text = "CUSTOMER";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblVoterNumber
			// 
			this.lblVoterNumber.Location = new System.Drawing.Point(190, 26);
			this.lblVoterNumber.Name = "lblVoterNumber";
			this.lblVoterNumber.Size = new System.Drawing.Size(66, 30);
			this.lblVoterNumber.TabIndex = 1;
			// 
			// lblCustomerNumber
			// 
			this.lblCustomerNumber.Location = new System.Drawing.Point(30, 106);
			this.lblCustomerNumber.Name = "lblCustomerNumber";
			this.lblCustomerNumber.Size = new System.Drawing.Size(86, 18);
			this.lblCustomerNumber.TabIndex = 2;
			this.lblCustomerNumber.Text = "CUSTOMER #";
			this.lblCustomerNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(30, 44);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(86, 18);
			this.Label6.Text = "STATUS";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Image1
			// 
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Location = new System.Drawing.Point(439, 92);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("icon - notes")));
			this.Image1.Size = new System.Drawing.Size(31, 40);
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image1.TabIndex = 18;
			this.Image1.Visible = false;
			this.Image1.Click += new System.EventHandler(this.Image1_Click);
			// 
			// mnuFileBillScreen
			// 
			this.mnuFileBillScreen.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.mnuFileBillScreen.Location = new System.Drawing.Point(730, 29);
			this.mnuFileBillScreen.Name = "mnuFileBillScreen";
			this.mnuFileBillScreen.Shortcut = Wisej.Web.Shortcut.F9;
			this.mnuFileBillScreen.Size = new System.Drawing.Size(79, 24);
			this.mnuFileBillScreen.TabIndex = 3;
			this.mnuFileBillScreen.Text = "Bill Screen";
			this.mnuFileBillScreen.Click += new System.EventHandler(this.mnuFileBillScreen_Click);
			// 
			// mnuFileComments
			// 
			this.mnuFileComments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.mnuFileComments.Enabled = false;
			this.mnuFileComments.Location = new System.Drawing.Point(642, 29);
			this.mnuFileComments.Name = "mnuFileComments";
			this.mnuFileComments.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuFileComments.Size = new System.Drawing.Size(82, 24);
			this.mnuFileComments.TabIndex = 2;
			this.mnuFileComments.Text = "Comments";
			this.mnuFileComments.Click += new System.EventHandler(this.mnuFileComments_Click);
			// 
			// mnuProcessPreviousEntry
			// 
			this.mnuProcessPreviousEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.mnuProcessPreviousEntry.Location = new System.Drawing.Point(815, 29);
			this.mnuProcessPreviousEntry.Name = "mnuProcessPreviousEntry";
			this.mnuProcessPreviousEntry.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuProcessPreviousEntry.Size = new System.Drawing.Size(132, 24);
			this.mnuProcessPreviousEntry.TabIndex = 4;
			this.mnuProcessPreviousEntry.Text = "Previous Customer";
			this.mnuProcessPreviousEntry.Click += new System.EventHandler(this.mnuProcessPreviousEntry_Click);
			// 
			// mnuProcessNextEntry
			// 
			this.mnuProcessNextEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.mnuProcessNextEntry.Location = new System.Drawing.Point(953, 29);
			this.mnuProcessNextEntry.Name = "mnuProcessNextEntry";
			this.mnuProcessNextEntry.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuProcessNextEntry.Size = new System.Drawing.Size(108, 25);
			this.mnuProcessNextEntry.TabIndex = 5;
			this.mnuProcessNextEntry.Text = "Next Customer";
			this.mnuProcessNextEntry.Click += new System.EventHandler(this.mnuProcessNextEntry_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(274, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(120, 48);
			this.cmdSave.TabIndex = 1;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmCustomerMaster
			// 
			this.ClientSize = new System.Drawing.Size(1088, 678);
			this.KeyPreview = true;
			this.Name = "frmCustomerMaster";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Add / Update Account";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmCustomerMaster_Load);
			this.Activated += new System.EventHandler(this.frmCustomerMaster_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomerMaster_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSalesTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			this.Frame5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDataEntryMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOneTimeBillMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mnuFileBillScreen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mnuFileComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mnuProcessPreviousEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mnuProcessNextEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
