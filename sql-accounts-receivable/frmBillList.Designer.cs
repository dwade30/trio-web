﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillList.
	/// </summary>
	partial class frmBillList : BaseForm
	{
		public fecherFoundation.FCComboBox cmbStatusSelected;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraStatusSelected;
		public fecherFoundation.FCCheckBox chkActive;
		public fecherFoundation.FCCheckBox chkPaid;
		public fecherFoundation.FCCheckBox chkVoid;
		public fecherFoundation.FCFrame fraBillType;
		public fecherFoundation.FCComboBox cboBillType;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCButton cmdRequestDate;
		public fecherFoundation.FCButton Command1;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboCustomers;
		public fecherFoundation.FCCheckBox chkPrintAddress;
		public fecherFoundation.FCCheckBox chkShowDetail;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillList));
			this.cmbStatusSelected = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraStatusSelected = new fecherFoundation.FCFrame();
			this.chkActive = new fecherFoundation.FCCheckBox();
			this.chkPaid = new fecherFoundation.FCCheckBox();
			this.chkVoid = new fecherFoundation.FCCheckBox();
			this.fraBillType = new fecherFoundation.FCFrame();
			this.cboBillType = new fecherFoundation.FCComboBox();
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.cmdRequestDate = new fecherFoundation.FCButton();
			this.Command1 = new fecherFoundation.FCButton();
			this.txtStartDate = new Global.T2KDateBox();
			this.txtEndDate = new Global.T2KDateBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboCustomers = new fecherFoundation.FCComboBox();
			this.chkPrintAddress = new fecherFoundation.FCCheckBox();
			this.chkShowDetail = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraStatusSelected)).BeginInit();
			this.fraStatusSelected.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVoid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBillType)).BeginInit();
			this.fraBillType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRequestDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrintPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 570);
			this.BottomPanel.Size = new System.Drawing.Size(578, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.fraBillType);
			this.ClientArea.Controls.Add(this.fraDateRange);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.chkPrintAddress);
			this.ClientArea.Controls.Add(this.chkShowDetail);
			this.ClientArea.Size = new System.Drawing.Size(578, 510);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(578, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(90, 30);
			this.HeaderText.Text = "Bill List";
			// 
			// cmbStatusSelected
			// 
			this.cmbStatusSelected.Items.AddRange(new object[] {
            "Selected",
            "All"});
			this.cmbStatusSelected.Location = new System.Drawing.Point(20, 30);
			this.cmbStatusSelected.Name = "cmbStatusSelected";
			this.cmbStatusSelected.Size = new System.Drawing.Size(143, 40);
			this.cmbStatusSelected.TabIndex = 1;
			this.cmbStatusSelected.SelectedIndexChanged += new System.EventHandler(this.cmbStatusSelected_SelectedIndexChanged);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.fraStatusSelected);
			this.Frame1.Controls.Add(this.cmbStatusSelected);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(487, 86);
			this.Frame1.Text = "Bill Status";
			// 
			// fraStatusSelected
			// 
			this.fraStatusSelected.AppearanceKey = " groupBoxNoBorder";
			this.fraStatusSelected.Controls.Add(this.chkActive);
			this.fraStatusSelected.Controls.Add(this.chkPaid);
			this.fraStatusSelected.Controls.Add(this.chkVoid);
			this.fraStatusSelected.Location = new System.Drawing.Point(179, 17);
			this.fraStatusSelected.Name = "fraStatusSelected";
			this.fraStatusSelected.Size = new System.Drawing.Size(298, 64);
			this.fraStatusSelected.TabIndex = 2;
			// 
			// chkActive
			// 
			this.chkActive.Location = new System.Drawing.Point(20, 21);
			this.chkActive.Name = "chkActive";
			this.chkActive.Size = new System.Drawing.Size(62, 23);
			this.chkActive.Text = "Active";
			// 
			// chkPaid
			// 
			this.chkPaid.Location = new System.Drawing.Point(119, 21);
			this.chkPaid.Name = "chkPaid";
			this.chkPaid.Size = new System.Drawing.Size(53, 23);
			this.chkPaid.TabIndex = 1;
			this.chkPaid.Text = "Paid";
			// 
			// chkVoid
			// 
			this.chkVoid.Location = new System.Drawing.Point(206, 21);
			this.chkVoid.Name = "chkVoid";
			this.chkVoid.Size = new System.Drawing.Size(68, 23);
			this.chkVoid.TabIndex = 2;
			this.chkVoid.Text = "Voided";
			// 
			// fraBillType
			// 
			this.fraBillType.Controls.Add(this.cboBillType);
			this.fraBillType.Location = new System.Drawing.Point(30, 374);
			this.fraBillType.Name = "fraBillType";
			this.fraBillType.Size = new System.Drawing.Size(391, 90);
			this.fraBillType.TabIndex = 3;
			this.fraBillType.Text = "Bill Types";
			// 
			// cboBillType
			// 
			this.cboBillType.BackColor = System.Drawing.SystemColors.Window;
			this.cboBillType.Location = new System.Drawing.Point(20, 30);
			this.cboBillType.Name = "cboBillType";
			this.cboBillType.Size = new System.Drawing.Size(340, 40);
			// 
			// fraDateRange
			// 
			this.fraDateRange.Controls.Add(this.cmdRequestDate);
			this.fraDateRange.Controls.Add(this.Command1);
			this.fraDateRange.Controls.Add(this.txtStartDate);
			this.fraDateRange.Controls.Add(this.txtEndDate);
			this.fraDateRange.Controls.Add(this.lblTo);
			this.fraDateRange.Location = new System.Drawing.Point(30, 140);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(487, 90);
			this.fraDateRange.TabIndex = 1;
			this.fraDateRange.Text = "Date Range";
            // 
            // cmdRequestDate
            // 
            this.cmdRequestDate.AppearanceKey = "imageButton";
			this.cmdRequestDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdRequestDate.Location = new System.Drawing.Point(140, 30);
			this.cmdRequestDate.Name = "cmdRequestDate";
			this.cmdRequestDate.Size = new System.Drawing.Size(40, 40);
			this.cmdRequestDate.TabIndex = 1;
			this.cmdRequestDate.Click += new System.EventHandler(this.cmdRequestDate_Click);
			// 
			// Command1
			// 
            this.Command1.AppearanceKey = "imageButton";
            this.Command1.ImageSource = "icon - calendar?color=#707884";
            this.Command1.Location = new System.Drawing.Point(369, 30);
			this.Command1.Name = "Command1";
			this.Command1.Size = new System.Drawing.Size(40, 40);
			this.Command1.TabIndex = 4;
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			// 
			// txtStartDate
			// 
			this.txtStartDate.Location = new System.Drawing.Point(20, 30);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(115, 22);
			// 
			// txtEndDate
			// 
			this.txtEndDate.Location = new System.Drawing.Point(251, 30);
			this.txtEndDate.Mask = "##/##/####";
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.Size = new System.Drawing.Size(115, 22);
			this.txtEndDate.TabIndex = 3;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(207, 44);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(23, 20);
			this.lblTo.TabIndex = 2;
			this.lblTo.Text = "TO";
			this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboCustomers);
			this.Frame2.Location = new System.Drawing.Point(30, 255);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(391, 90);
			this.Frame2.TabIndex = 2;
			this.Frame2.Text = "Customers";
			// 
			// cboCustomers
			// 
			this.cboCustomers.BackColor = System.Drawing.SystemColors.Window;
			this.cboCustomers.Location = new System.Drawing.Point(20, 30);
			this.cboCustomers.Name = "cboCustomers";
			this.cboCustomers.Size = new System.Drawing.Size(340, 40);
			// 
			// chkPrintAddress
			// 
			this.chkPrintAddress.Location = new System.Drawing.Point(30, 482);
			this.chkPrintAddress.Name = "chkPrintAddress";
			this.chkPrintAddress.Size = new System.Drawing.Size(112, 23);
			this.chkPrintAddress.TabIndex = 4;
			this.chkPrintAddress.Text = "Show Address";
			// 
			// chkShowDetail
			// 
			this.chkShowDetail.Location = new System.Drawing.Point(215, 482);
			this.chkShowDetail.Name = "chkShowDetail";
			this.chkShowDetail.Size = new System.Drawing.Size(143, 23);
			this.chkShowDetail.TabIndex = 5;
			this.chkShowDetail.Text = "Show Invoice Detail";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 0;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdPrintPreview
			// 
			this.cmdPrintPreview.AppearanceKey = "acceptButton";
			this.cmdPrintPreview.Location = new System.Drawing.Point(229, 31);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrintPreview.Size = new System.Drawing.Size(130, 40);
			this.cmdPrintPreview.Text = "Print Preview";
			this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
			// 
			// frmBillList
			// 
			this.ClientSize = new System.Drawing.Size(578, 666);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBillList";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Bill List";
			this.Load += new System.EventHandler(this.frmBillList_Load);
			this.Activated += new System.EventHandler(this.frmBillList_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBillList_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraStatusSelected)).EndInit();
			this.fraStatusSelected.ResumeLayout(false);
			this.fraStatusSelected.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVoid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBillType)).EndInit();
			this.fraBillType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			this.fraDateRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRequestDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkPrintAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPrintPreview;
	}
}
