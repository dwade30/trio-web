﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using GrapeCity.ActiveReports;
using TWAR0000.Receipting;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for arARAccountDetail.
	/// </summary>
	public partial class arARPaymentStatusDetail : BaseSectionReport
	{
		// nObj = 1
		//   0	arARAccountDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/22/2005              *
		// ********************************************************
		int lngRow;
		int lngPDNumber;
		int lngPDTop;
		double dblPDTotal;
		bool boolPerDiem;
		string strBillText = "";
		private frmARPaymentStatus paymentForm = null; 
		public arARPaymentStatusDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static arARPaymentStatusDetail InstancePtr
		{
			get
			{
				return (arARPaymentStatusDetail)Sys.GetInstance(typeof(arARPaymentStatusDetail));
			}
		}

		protected arARPaymentStatusDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void SetPaymentForm(frmARPaymentStatus callingForm)
		{
			paymentForm = callingForm;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngRow < 0)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}


		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SetHeaderString();
			boolPerDiem = false;
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			CreatePerDiemTable();
			lngRow = 0;

			if (boolPerDiem)
			{
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew1;
				GrapeCity.ActiveReports.SectionReportModel.Label obNew2;
				GrapeCity.ActiveReports.SectionReportModel.Line obNew3;

                obNew1 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("fldPerDiemTotal");
                obNew2 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblPerDiemTotal");
                obNew3 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Line>("lnPerDiemTotal");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngRow = FindNextVisibleRow(ref lngRow);
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			lnTotals.Visible = false;
			lnTotals.LineWeight = 0;
			if (lngRow >= 0)
			{
				if (paymentForm.GRID.RowOutlineLevel(lngRow) == 1)
				{
					// is it a header line
					HideFields_2(false);
					// Bill Number
					fldBN.Text = paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColInvoiceNumber);
					// Date
					fldDate.Text = paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColDate);
					// Ref
					fldRef.Text = "";
					// Code
					fldCode.Text = "";
					// Principal
					fldPrincipal.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColPrincipal), "#,##0.00");
					// Tax
					fldTax.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColTax), "#,##0.00");
					// Interest
					fldInterest.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColInterest), "#,##0.00");
					// Total
					fldTotal.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColTotal), "#,##0.00");
				}
				else if (Strings.Left(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColRef), 10) == "Billed To:")
				{
					// is it a name line
					HideFields_2(true);
					// Name
					fldName.Text = paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColRef) + " " + paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColPaymentCode);
				}
				else
				{
					if (Strings.Left(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColInvoiceNumber), 14) == "Account Totals")
					{
						// is it the last line?
						HideFields_2(false);
						fldName.Visible = true;
						fldBN.Visible = false;
						fldDate.Visible = false;
						fldRef.Visible = false;
						fldCode.Visible = false;
						fldName.Left = 0;
						fldName.Width = fldPrincipal.Left;
						// show the line above this row and make it darker
						lnTotals.Visible = true;
						lnTotals.LineWeight = 2;
						// total line
						fldName.Text = paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColInvoiceNumber);
						// Principal
						fldPrincipal.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColPrincipal), "#,##0.00");
						// Tax
						fldTax.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColTax), "#,##0.00");
						// Interest
						fldInterest.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColInterest), "#,##0.00");
						// Total
						fldTotal.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColTotal), "#,##0.00");
					}
					else if (paymentForm.GRID.TextMatrix(lngRow - 1, paymentForm.lngGRIDColLineCode) == "=")
					{
						// is this line a hidden totals line?
						HideFields_2(true);
						fldName.Text = "";
					}
					else
					{
						HideFields_2(false);
						fldBN.Text = "";
						// Date
						fldDate.Text = paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColDate);
						// Ref
						fldRef.Text = paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColRef);
						if (Strings.Left(fldRef.Text, 5) == "Total")
						{
							lnTotals.Visible = true;
							lnTotals.LineWeight = 1;
						}
						else
						{
							lnTotals.Visible = false;
						}
						// Code
						fldCode.Text = paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColPaymentCode);
						// Principal
						fldPrincipal.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColPrincipal), "#,##0.00");
						// Tax
						fldTax.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColTax), "#,##0.00");
						// Interest
						fldInterest.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColInterest), "#,##0.00");
						// Total
						fldTotal.Text = Strings.Format(paymentForm.GRID.TextMatrix(lngRow, paymentForm.lngGRIDColTotal), "#,##0.00");
					}
				}
			}
			else
			{
				fldDate.Text = "";
				fldRef.Text = "";
				fldCode.Text = "";
				fldPrincipal.Text = "";
				fldTax.Text = "";
				fldInterest.Text = "";
				fldTotal.Text = "";
				fldName.Text = "";
			}
		}

		private int FindNextVisibleRow(ref int lngR)
		{
			int FindNextVisibleRow = 0;
			// this function will start at the row number passed in and return the next row to print
			// this function will return -1 if there are no more rows to show
			int lngNext;
			int intLvl;
			bool boolFound;
			int lngMax = 0;
			boolFound = false;
			lngNext = lngR + 1;
			lngMax = paymentForm.GRID.Rows;
			if (lngNext < lngMax)
			{
				boolFound = false;
				while (!(boolFound || paymentForm.GRID.RowOutlineLevel(lngNext) < 2))
				{
					if (paymentForm.GRID.IsCollapsed(LastParentRow(lngNext)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						lngNext += 1;
						boolFound = false;
						if (lngNext >= lngMax)
						{
							return FindNextVisibleRow;
						}
					}
					else
					{
						boolFound = true;
					}
				}
				FindNextVisibleRow = lngNext;
			}
			if (FindNextVisibleRow == 0)
				FindNextVisibleRow = -1;
			return FindNextVisibleRow;
		}
		// vbPorter upgrade warning: CurRow As Variant --> As int
		public int LastParentRow(int CurRow)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			curLvl = paymentForm.GRID.RowOutlineLevel(l);
			l -= 1;
			while (paymentForm.GRID.RowOutlineLevel(l) > 1)
			{
				l -= 1;
			}
			LastParentRow = l;
			return LastParentRow;
		}

		private void HideFields_2(bool boolHide)
		{
			HideFields(ref boolHide);
		}

		private void HideFields(ref bool boolHide)
		{
			// this sub will either hide or show the fields depending on which variable is shown
			fldDate.Visible = !boolHide;
			fldRef.Visible = !boolHide;
			fldCode.Visible = !boolHide;
			fldPrincipal.Visible = !boolHide;
			fldTax.Visible = !boolHide;
			fldInterest.Visible = !boolHide;
			fldTotal.Visible = !boolHide;
			fldName.Visible = boolHide;
			if (boolHide)
			{
				// makes sure that the name field is in the proper position
				fldName.Left = 1;
				fldName.Width = this.PrintWidth - 2;
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			clsDRWrapper rsMaster = new clsDRWrapper();
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			string strAddr4 = "";
			string strTemp = "";
			// rsMaster.OpenRecordset "SELECT * FROM CustomerMaster WHERE CustomerID = " & lngCurrentCustomerIDAR, strARDatabase
			rsMaster.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsMaster.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE ID = " + paymentForm.ViewModel.Customer.CustomerId.ToString());
			if (!rsMaster.EndOfFile())
			{
				strAddr1 = FCConvert.ToString(rsMaster.Get_Fields_String("Address1"));
				strAddr2 = FCConvert.ToString(rsMaster.Get_Fields_String("Address2"));
				strAddr3 = FCConvert.ToString(rsMaster.Get_Fields_String("Address3"));
				// TODO: Field [PartyState] not found!! (maybe it is an alias?)
				strAddr4 = rsMaster.Get_Fields_String("City") + ", " + rsMaster.Get_Fields("PartyState") + " " + rsMaster.Get_Fields_String("Zip");
				if (Strings.Trim(strAddr3) == "")
				{
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				if (Strings.Trim(strAddr2) == "")
				{
					strAddr2 = strAddr3;
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				if (Strings.Trim(strAddr1) == "")
				{
					strAddr1 = strAddr2;
					strAddr2 = strAddr3;
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
			}
			else
			{
				strAddr1 = "";
				strAddr2 = "";
				strAddr3 = "";
				strAddr4 = "";
			}
			lblAddress1.Text = strAddr1;
			lblAddress2.Text = strAddr2;
			lblAddress3.Text = strAddr3;
			lblAddress4.Text = strAddr4;
			strTemp += " Account " + FCConvert.ToString(paymentForm.ViewModel.Customer.CustomerId) + " Detail";
			strTemp += "\r\n" + "as of " + Strings.Format(paymentForm.ViewModel.EffectiveDate, "MM/dd/yyyy");
			lblHeader.Text = strTemp;
			lblName.Text = "Name: " + paymentForm.lblOwnersName.Text;
		}

		private void CreatePerDiemTable()
		{
			int lngRW;
			for (lngRW = 1; lngRW <= paymentForm.GRID.Rows - 1; lngRW++)
			{
				if (Strings.Trim(paymentForm.GRID.TextMatrix(lngRW, paymentForm.lngGRIDColInvoiceNumber)) != "")
				{
					strBillText = Strings.Trim(paymentForm.GRID.TextMatrix(lngRW, paymentForm.lngGRIDColInvoiceNumber));
				}
				if (Conversion.Val(paymentForm.GRID.TextMatrix(lngRW, paymentForm.lngGRIDColPerDiem)) != 0)
				{
					AddPerDiem(ref lngRW);
				}
			}
		}

		private void AddPerDiem(ref int lngRNum)
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew1;
			GrapeCity.ActiveReports.SectionReportModel.Label obNew2;
			string strTemp = "";
			// this will add another per diem line in the report footer
			if (lngPDNumber == 0)
			{
				lngPDNumber = 1;
				fldPerDiem1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fldPerDiem1.Text = Strings.Format(Conversion.Val(paymentForm.GRID.TextMatrix(lngRNum, paymentForm.lngGRIDColPerDiem)), "0.0000");
				if (Strings.Trim(paymentForm.GRID.TextMatrix(lngRNum, paymentForm.lngGRIDColInvoiceNumber)) == "")
				{
					lblPerDiem1.Text = paymentForm.GRID.TextMatrix(paymentForm.LastParentRow(lngRNum), paymentForm.lngGRIDColInvoiceNumber);
				}
				else
				{
					lblPerDiem1.Text = paymentForm.GRID.TextMatrix(lngRNum, paymentForm.lngGRIDColInvoiceNumber);
				}
				dblPDTotal += Conversion.Val(paymentForm.GRID.TextMatrix(lngRNum, paymentForm.lngGRIDColPerDiem));
			}
			else
			{
				// increment the number of fields
				lngPDNumber += 1;
                // add a field
                obNew1 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("fldPerDiem" + FCConvert.ToString(lngPDNumber));
				//obNew1.Name = "fldPerDiem" + FCConvert.ToString(lngPDNumber);
				obNew1.Top = fldPerDiem1.Top + ((lngPDNumber - 1) * fldPerDiem1.Height);
				obNew1.Left = fldPerDiem1.Left;
				obNew1.Width = fldPerDiem1.Width;
				obNew1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				//strTemp = obNew1.Font;
				obNew1.Font = fldPerDiem1.Font;
				// this sets the font to the same as the field that is already created
				obNew1.Text = Strings.Format(Conversion.Val(paymentForm.GRID.TextMatrix(lngRNum, paymentForm.lngGRIDColPerDiem)), "0.0000");
				dblPDTotal += Conversion.Val(paymentForm.GRID.TextMatrix(lngRNum, paymentForm.lngGRIDColPerDiem));
                // add a label
                obNew2 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblPerDiem" + FCConvert.ToString(lngPDNumber));
				//obNew2.Name = "lblPerDiem" + FCConvert.ToString(lngPDNumber);
				obNew2.Top = lblPerDiem1.Top + ((lngPDNumber - 1) * lblPerDiem1.Height);
				obNew2.Left = lblPerDiem1.Left;
				//strTemp = obNew2.Font;
				obNew2.Font = fldPerDiem1.Font;
				// this sets the font to the same as the field that is already created
				if (Strings.Trim(paymentForm.GRID.TextMatrix(lngRNum, paymentForm.lngGRIDColInvoiceNumber)) == "")
				{
					obNew2.Text = paymentForm.GRID.TextMatrix(paymentForm.LastParentRow(lngRNum), paymentForm.lngGRIDColInvoiceNumber);
				}
				else
				{
					obNew2.Text = paymentForm.GRID.TextMatrix(lngRNum, paymentForm.lngGRIDColInvoiceNumber);
				}
			}
			boolPerDiem = true;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew1;
			GrapeCity.ActiveReports.SectionReportModel.Label obNew2;
			GrapeCity.ActiveReports.SectionReportModel.Line obNew3;
			if (boolPerDiem)
			{
				obNew1 = ReportFooter.Controls["fldPerDiemTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				obNew1.Top = fldPerDiem1.Top + (lngPDNumber * fldPerDiem1.Height);
				obNew1.Left = fldPerDiem1.Left;
				obNew1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew1.Width = fldPerDiem1.Width;
				obNew1.Text = Strings.Format(dblPDTotal, "0.0000");
				obNew2 = ReportFooter.Controls["lblPerDiemTotal"] as GrapeCity.ActiveReports.SectionReportModel.Label;
				obNew2.Top = lblPerDiem1.Top + (lngPDNumber * lblPerDiem1.Height);
				obNew2.Left = lblPerDiem1.Left;
				obNew2.Text = "Total";
				obNew3 = ReportFooter.Controls["lnPerDiemTotal"] as GrapeCity.ActiveReports.SectionReportModel.Line;
				obNew3.X1 = fldPerDiem1.Left;
				obNew3.X2 = fldPerDiem1.Left + fldPerDiem1.Width;
				obNew3.Y1 = lblPerDiem1.Top + (lngPDNumber * lblPerDiem1.Height);
				obNew3.Y2 = obNew3.Y1;
			}
			else
			{
				lblPerDiem1.Visible = false;
				lblPerDiemHeader.Visible = false;
				fldPerDiem1.Visible = false;
				Line3.Visible = false;
			}
		}
	}
}
