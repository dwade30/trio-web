﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmGetCustomerMaster.
	/// </summary>
	public partial class frmGetCustomerMaster : BaseForm
	{
		public frmGetCustomerMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.cmbSearchType.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetCustomerMaster InstancePtr
		{
			get
			{
				return (frmGetCustomerMaster)Sys.GetInstance(typeof(frmGetCustomerMaster));
			}
		}

		protected frmGetCustomerMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();
		bool EditFlag;
		bool SearchFlag;
		int[] recordNum = null;
		bool OKFlag;

		public void StartProgram(int Record)
		{
			int counter;
			string[] strFiles = null;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Customer Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			modGlobal.Statics.lngCurrentCustomer = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
			// initialize the current account number
			modGlobal.Statics.blnFromBillPrep = false;
			rs.OpenRecordset("SELECT * FROM CustomerMaster WHERE CustomerID = " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				frmCustomerMaster.InstancePtr.RecordNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("CustomerID"));
				frmCustomerMaster.InstancePtr.FirstRecordShown = FCConvert.ToInt32(rs.Get_Fields_Int32("CustomerID"));
				frmCustomerMaster.InstancePtr.blnEdit = true;
				frmCustomerMaster.InstancePtr.mnuFileComments.Enabled = true;
				frmCustomerMaster.InstancePtr.mnuFileBillScreen.Enabled = true;
				frmCustomerMaster.InstancePtr.SetStatusCombo(rs.Get_Fields_String("Status"));
				frmCustomerMaster.InstancePtr.txtCustomerNumber.Text = FCConvert.ToString(rs.Get_Fields_Int32("PartyID"));
				frmCustomerMaster.InstancePtr.GetPartyInfo(FCConvert.ToInt32(rs.Get_Fields_Int32("PartyID")));
				frmCustomerMaster.InstancePtr.txtDataEntryMessage.Text = FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage"));
				frmCustomerMaster.InstancePtr.txtBillMessage.Text = FCConvert.ToString(rs.Get_Fields_String("BillMessage"));
				frmCustomerMaster.InstancePtr.txtMessage.Text = FCConvert.ToString(rs.Get_Fields_String("Message"));
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("OneTimeBillMessage")))
				{
					frmCustomerMaster.InstancePtr.chkOneTimeBillMessage.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					frmCustomerMaster.InstancePtr.chkOneTimeBillMessage.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("SalesTax")))
				{
					frmCustomerMaster.InstancePtr.chkSalesTax.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					frmCustomerMaster.InstancePtr.chkSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				frmCustomerMaster.InstancePtr.lblVoterNumber.Text = FCConvert.ToString(rs.Get_Fields_Int32("CustomerID"));
				modGlobal.Statics.CustomerSearchResults.MoveFirst();
				frmCustomerMaster.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
				while (FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID")) != Record)
				{
					//Application.DoEvents();
					modGlobal.Statics.CustomerSearchResults.MoveNext();
					frmCustomerMaster.InstancePtr.mnuProcessPreviousEntry.Enabled = true;
				}
				if (modGlobal.Statics.CustomerSearchResults.RecordCount() > 1)
				{
					modGlobal.Statics.CustomerSearchResults.MoveNext();
					if (modGlobal.Statics.CustomerSearchResults.EndOfFile() != true)
					{
						frmCustomerMaster.InstancePtr.mnuProcessNextEntry.Enabled = true;
					}
					else
					{
						frmCustomerMaster.InstancePtr.mnuProcessNextEntry.Enabled = false;
					}
					modGlobal.Statics.CustomerSearchResults.MovePrevious();
				}
				else
				{
					frmCustomerMaster.InstancePtr.mnuProcessNextEntry.Enabled = false;
				}
				frmCustomerMaster.InstancePtr.Show(App.MainForm);
				// show the form
				// frmWait.Refresh
				//Application.DoEvents();
				modRegistry.SaveRegistryKey("CURRCUST", FCConvert.ToString(modGlobal.Statics.lngCurrentCustomer));
				frmWait.InstancePtr.Unload();
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				MessageBox.Show("No Customer Found", "Non-Existent Customer", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			Close();
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			cmbSearchType.SelectedIndex = 0;
			//optHidden.Checked = true; // clear the search info
			txtSearch.Text = "";
			//vs1.Clear();
			vs1.RowCount = 0;
			vs1.TextMatrix(0, 0, "Cust#");
			vs1.TextMatrix(0, 1, "Name");
			vs1.TextMatrix(0, 2, "Status");
			vs1.TextMatrix(0, 3, "Address");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, 4);
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtGetAccountNumber.Text) != 0)
			{
				// if there is a valid account number
				modGlobal.Statics.lngCurrentCustomer = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
				// set the account number to the one last used
				modGlobal.Statics.CustomerSearchResults.OpenRecordset("SELECT * FROM CustomerMaster WHERE CustomerID = " + FCConvert.ToString(modGlobal.Statics.lngCurrentCustomer));
				StartProgram(modGlobal.Statics.lngCurrentCustomer);
				// call the procedure to retrieve the info
			}
			else
			{
				modGlobal.Statics.blnFromBillPrep = false;
				frmCustomerMaster.InstancePtr.blnEdit = false;
				frmCustomerMaster.InstancePtr.mnuProcessNextEntry.Enabled = false;
				frmCustomerMaster.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
				frmCustomerMaster.InstancePtr.mnuFileBillScreen.Enabled = false;
				frmCustomerMaster.InstancePtr.RecordNumber = 0;
				if (modGlobal.Statics.CustomerSearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					modGlobal.Statics.CustomerSearchResults.Reset();
				}
				//! Load frmWait; // shwo the wait form
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading New Entry";
				// frmWait.Refresh
				frmWait.InstancePtr.Show();
				frmCustomerMaster.InstancePtr.Show(App.MainForm);
				// show the blankform
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
				Close();
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			int intAccount;
			intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
			// get the account number of that record
			if (intAccount != 0)
			{
				// if there is a valid account number
				modGlobal.Statics.lngCurrentCustomer = intAccount;
				// gets the Account Number for the item that is double clicked
				txtGetAccountNumber.Text = FCConvert.ToString(intAccount);
				StartProgram(recordNum[vs1.Row - 1]);
			}
			cmdClear_Click();
			// clear the search
			Frame3.Visible = false;
			// make the list of records invisible
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
			// unload this form
		}

		public void cmdQuit_Click()
		{
			//cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM: only clear the rows
			//vs1.Clear();
			vs1.RowCount = 0;
			Frame3.Visible = false;
			// make the listbox invisible
			cmdClear_Click();
			// clear the search
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string SearchType = "";
			string SearchCriteria = "";
			bool BadSearchFlag = false;
			string strStreetSearch = "";
			int lngStreetNumber;
			string strPrefix = "";
			//! Load frmWait; // shwo the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
			if (cmbSearchType.SelectedIndex == 1)//if (optSearchType[0].Checked != true)
			{
				// make sure a search type is clicked
				//if (optSearchType[1].Checked != true)
				//{
				//    frmWait.InstancePtr.Unload();
				//    //Application.DoEvents();
				//    MessageBox.Show("You must choose a Search Type (Name or Customer Number).", "Search Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//    return;
				//}
				//else
				{
					SearchType = "Address";
				}
			}
			else
			{
				SearchType = "Name";
			}
			if (SearchType == "Name")
			{
				if (txtSearch.Text == "")
				{
					// make sure there is some criteria to search for
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					SearchCriteria = txtSearch.Text;
					// remember the search criteria
				}
			}
			else
			{
				if (txtSearch.Text == "")
				{
					// make sure there is some criteria to search for
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					SearchCriteria = txtSearch.Text;
					// remember the search criteria
				}
			}
			if (SearchType == "Name")
			{
				// CustomerSearchResults.OpenRecordset "SELECT CustomerMaster.*, rtrim(rtrim(FirstName + ' ' + isnull(middlename, '')) + ' ' + rtrim(isnull(LastName, '') + ' ' + isnull(Designation, ''))) as Name FROM CustomerMaster INNER JOIN TRIO_Test_Live_CentralParties.dbo.Parties ON CustomerMaster.PartyID = TRIO_Test_Live_CentralParties.dbo.Parties.ID WHERE rtrim(rtrim(FirstName + ' ' + isnull(middlename, '')) + ' ' + rtrim(isnull(LastName, '') + ' ' + isnull(Designation, ''))) > '" & Trim(txtSearch.Text) & "    ' AND rtrim(rtrim(FirstName + ' ' + isnull(middlename, '')) + ' ' + rtrim(isnull(LastName, '') + ' ' + isnull(Designation, ''))) < '" & Trim(txtSearch.Text) & "zzzz' ORDER BY Name"
				// rs.OpenRecordset "SELECT CustomerMaster.*, rtrim(rtrim(FirstName + ' ' + isnull(middlename, '')) + ' ' + rtrim(isnull(LastName, '') + ' ' + isnull(Designation, ''))) as Name FROM CustomerMaster INNER JOIN TRIO_Test_Live_CentralParties.dbo.Parties ON CustomerMaster.PartyID = TRIO_Test_Live_CentralParties.dbo.Parties.ID WHERE rtrim(rtrim(FirstName + ' ' + isnull(middlename, '')) + ' ' + rtrim(isnull(LastName, '') + ' ' + isnull(Designation, ''))) > '" & Trim(txtSearch.Text) & "    ' AND rtrim(rtrim(FirstName + ' ' + isnull(middlename, '')) + ' ' + rtrim(isnull(LastName, '') + ' ' + isnull(Designation, ''))) < '" & Trim(txtSearch.Text) & "zzzz' ORDER BY Name"
				modGlobal.Statics.CustomerSearchResults.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + modGlobal.Statics.CustomerSearchResults.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.FullName >= '" + Strings.Trim(txtSearch.Text) + "    ' AND p.FullName <= '" + Strings.Trim(txtSearch.Text) + "zzzz' ORDER BY p.FullName");
				rs.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rs.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.FullName >= '" + Strings.Trim(txtSearch.Text) + "    ' AND p.FullName <= '" + Strings.Trim(txtSearch.Text) + "zzzz' ORDER BY p.FullName");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else
			{
				// CustomerSearchResults.OpenRecordset "SELECT CustomerMaster.*, rtrim(rtrim(FirstName + ' ' + isnull(middlename, '')) + ' ' + rtrim(isnull(LastName, '') + ' ' + isnull(Designation, ''))) as Name FROM CustomerMaster INNER JOIN TRIO_Test_Live_CentralParties.dbo.Parties ON CustomerMaster.PartyID = TRIO_Test_Live_CentralParties.dbo.Parties.ID INNER JOIN TRIO_Test_Live_CentralParties.dbo.Addresses ON TRIO_Test_Live_CentralParties.dbo.Addresses.PartyID = TRIO_Test_Live_CentralParties.dbo.Parties.ID WHERE Address1 > '" & Trim(txtSearch.Text) & "    ' AND Address1 < '" & Trim(txtSearch.Text) & "zzzz'"
				// rs.OpenRecordset "SELECT CustomerMaster.*, rtrim(rtrim(FirstName + ' ' + isnull(middlename, '')) + ' ' + rtrim(isnull(LastName, '') + ' ' + isnull(Designation, ''))) as Name FROM CustomerMaster INNER JOIN TRIO_Test_Live_CentralParties.dbo.Parties ON CustomerMaster.PartyID = TRIO_Test_Live_CentralParties.dbo.Parties.ID INNER JOIN TRIO_Test_Live_CentralParties.dbo.Addresses ON TRIO_Test_Live_CentralParties.dbo.Addresses.PartyID = TRIO_Test_Live_CentralParties.dbo.Parties.ID WHERE Address1 > '" & Trim(txtSearch.Text) & "    ' AND Address1 < '" & Trim(txtSearch.Text) & "zzzz'"
				modGlobal.Statics.CustomerSearchResults.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + modGlobal.Statics.CustomerSearchResults.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.Address1 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND p.Address1 <= '" + Strings.Trim(txtSearch.Text) + "zzzz'");
				rs.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rs.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.Address1 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND p.Address1 <= '" + Strings.Trim(txtSearch.Text) + "zzzz'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			if (!BadSearchFlag)
			{
				// if there is a record
				rs.MoveLast();
				rs.MoveFirst();
				modGlobal.Statics.CustomerSearchResults.MoveLast();
				modGlobal.Statics.CustomerSearchResults.MoveFirst();
				frmWait.InstancePtr.Unload();
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					frmCustomerMaster.InstancePtr.mnuProcessNextEntry.Enabled = false;
					frmCustomerMaster.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
					recordNum = new int[1 + 1];
					recordNum[0] = FCConvert.ToInt32(rs.Get_Fields_Int32("CustomerID"));
					cmdClear_Click();
					// clear the search
					modGlobal.Statics.lngCurrentCustomer = FCConvert.ToInt32(rs.Get_Fields_Int32("CustomerID"));
					// save the account number
					txtGetAccountNumber.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentCustomer);
					StartProgram(recordNum[0]);
					// call the procedure to retrieve the info
				}
				else
				{
					frmCustomerMaster.InstancePtr.mnuProcessNextEntry.Enabled = true;
					frmCustomerMaster.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
					Fill_List(ref SearchType);
					// else show the user all possible records
					frmWait.InstancePtr.Unload();
					Frame3.Visible = true;
					// make the list of records visible
					//FC:FINAL:BBE:#462 - don't set the focus for the grid, otherwise the ActiveControl will not correctly updated
					//vs1.Focus();
				}
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				BadSearchFlag = false;
				MessageBox.Show("No Records Found That Match The Criteria", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmGetCustomerMaster_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtGetAccountNumber.Focus();
			txtGetAccountNumber.SelectionStart = 0;
			txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
		}

		private void frmGetCustomerMaster_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Up)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{UP}", false);
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{DOWN}", false);
				}
			}
		}

		private void frmGetCustomerMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				if (this.ActiveControl.GetName() == "vs1")
				{
					// do nothing
				}
				else
				{
					KeyAscii = (Keys)0;
					if (this.ActiveControl.GetName() == "txtGetAccountNumber")
					{
						cmdGetAccountNumber_Click();
						return;
					}
					if (this.ActiveControl.GetName() == "txtSearch")
						cmdSearch_Click();
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				if (Frame3.Visible == true)
				{
					cmdClear_Click();
					Frame3.Visible = false;
				}
				else
				{
					Close();
					//MDIParent.InstancePtr.Focus();
				}
			}
			else if (KeyAscii == Keys.Right || KeyAscii == Keys.F13)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Fill_List(ref string x)
		{
			int I;
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			recordNum = new int[rs.RecordCount() + 1];
			vs1.Rows = 1;
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				recordNum[I - 1] = FCConvert.ToInt32(rs.Get_Fields_Int32("CustomerID"));
				vs1.AddItem("");
				vs1.TextMatrix(I, 0, FCConvert.ToString(rs.Get_Fields_Int32("CustomerID")));
				vs1.TextMatrix(I, 1, FCConvert.ToString(rs.Get_Fields_String("FullName")));
				vs1.TextMatrix(I, 2, FCConvert.ToString(rs.Get_Fields_String("Status")));
				pInfo = pCont.GetParty(FCConvert.ToInt32(rs.Get_Fields_Int32("PartyID")));
				if (!(pInfo == null))
				{
					pAdd = pInfo.GetAddress("AR", FCConvert.ToInt32(rs.Get_Fields_Int32("CustomerID")));
					if (!(pAdd == null))
					{
						vs1.TextMatrix(I, 3, pAdd.GetFormattedAddress());
					}
					else
					{
						vs1.TextMatrix(I, 3, "");
					}
				}
				else
				{
					vs1.TextMatrix(I, 3, "");
				}
				// If Trim(rs.Get_Fields("Zip4")) <> "" Then
				// vs1.TextMatrix(i, 3) = Trim(rs.Get_Fields("Address1") & " " & rs.Get_Fields("Address2") & " " & rs.Get_Fields("Address3")) & " " & rs.Get_Fields("City") & ", " & rs.Get_Fields("State") & " " & rs.Get_Fields("Zip") & "-" & rs.Get_Fields("Zip4")
				// Else
				// vs1.TextMatrix(i, 3) = Trim(rs.Get_Fields("Address1") & " " & rs.Get_Fields("Address2") & " " & rs.Get_Fields("Address3")) & " " & rs.Get_Fields("City") & ", " & rs.Get_Fields("State") & " " & rs.Get_Fields("Zip")
				// End If
				vs1.TextMatrix(I, 3, Strings.Trim(vs1.TextMatrix(I, 3)));
				if (Strings.Right(vs1.TextMatrix(I, 3), 1) == ",")
				{
					vs1.TextMatrix(I, 3, Strings.Left(vs1.TextMatrix(I, 3), vs1.TextMatrix(I, 3).Length - 1));
				}
				if (I < rs.RecordCount())
					rs.MoveNext();
			}
			vs1.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
			vs1.AutoSize(0, vs1.Cols - 1);
		}

		private void frmGetCustomerMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetCustomerMaster.ScaleWidth	= 9045;
			//frmGetCustomerMaster.ScaleHeight	= 7290;
			//frmGetCustomerMaster.LinkTopic	= "Form1";
			//End Unmaped Properties
			int counter;
			modGlobal.Statics.lngCurrentCustomer = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRCUST"))));
			txtGetAccountNumber.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentCustomer);
			lblLastAccount.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentCustomer);
			vs1.TextMatrix(0, 0, "Cust#");
			vs1.TextMatrix(0, 1, "Name");
			vs1.TextMatrix(0, 2, "Stat");
			vs1.TextMatrix(0, 3, "Address");
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.080043));
			vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.3016666));
			vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.0749593));
			vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.0791686));
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, 4);
			vs1.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmGetCustomerMaster_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.080043));
			vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.3016666));
			vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.0749593));
			vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.0791686));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdGetAccountNumber_Click();
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			int intAccount = 0;
			// if there is a record selected
			if (vs1.MouseRow > 0)
			{
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					modGlobal.Statics.lngCurrentCustomer = intAccount;
					// gets the Account Number for the item that is double clicked
					txtGetAccountNumber.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentCustomer);
					StartProgram(recordNum[vs1.Row - 1]);
				}
				cmdClear_Click();
				// clear the search
				Frame3.Visible = false;
				// make the listbox invisible
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Up)
			{
				if (vs1.Row == 1)
				{
					//FC:TODO:AM
					//e.KeyCode = 0;
				}
			}
			else if (e.KeyCode == Keys.Down)
			{
				if (vs1.Row == vs1.Rows - 1)
				{
					//e.KeyCode = 0;
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int intAccount = 0;
			// if there is a record selected
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					modGlobal.Statics.lngCurrentCustomer = intAccount;
					// gets the Account Number for the item that is double clicked
					StartProgram(recordNum[vs1.Row - 1]);
				}
				cmdClear_Click();
				// clear the search
				Frame3.Visible = false;
				// make the listbox invisible
			}
		}

		private void txtSearch_Enter(object sender, System.EventArgs e)
		{
			//if (optSearchType[0].Checked != true && optSearchType[1].Checked != true)
			//{
			//    optSearchType[0].Focus();
			//}
		}

		private void optSearchType_MouseDown(int Index, object sender, System.EventArgs e)
		{
			//FC:FINAL:DDU:TODO
			//MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			//int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			//float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
			//float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
			//if (Button == MouseButtonConstants.LeftButton && Shift == 0)
			//{
			//    OKFlag = true;
			//}
		}

		private void optSearchType_MouseDown(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.Items.IndexOf(sender);
			optSearchType_MouseDown(index, sender, e);
		}
	}
}
