using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptCustomBill.
	/// </summary>
	public partial class rptCustomBill : FCSectionReport
	{

		public static rptCustomBill InstancePtr
		{
			get
			{
				return (rptCustomBill)Sys.GetInstance(typeof(rptCustomBill));
			}
		}

		protected rptCustomBill _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptCustomBill()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
	}
}
