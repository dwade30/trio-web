﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmAddRateRecord.
	/// </summary>
	public partial class frmAddRateRecord : BaseForm
	{
		public frmAddRateRecord()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:AM: moved code from load
			FillComboType();
			LoadYears();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAddRateRecord InstancePtr
		{
			get
			{
				return (frmAddRateRecord)Sys.GetInstance(typeof(frmAddRateRecord));
			}
		}

		protected frmAddRateRecord _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/19/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/24/2004              *
		// ********************************************************
		public int lngRKey;
		bool boolLoaded;
		int intType;
		DateTime dtBillDate;
		DateTime dtStartDate;
		bool boolNew;
		int lngRateKey;
		bool boolDirty;
		int intInterestDays;
		bool blnDemandBill;
		// vbPorter upgrade warning: intPassBillMonth As short	OnWriteFCConvert.ToInt32(
		public void Init(short intIncomingType, DateTime? dtPassBillDate = null, DateTime? dtPassStartDate = null, int lngPassRateKey = 0, int lngPassBillYear = 0, short intPassBillMonth = 0, int lngPassType = 0)
		{
			dtPassBillDate = dtPassBillDate ?? DateTime.Now;
			dtPassStartDate = dtPassStartDate ?? DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTitleInfo = new clsDRWrapper();
				intType = intIncomingType;
				// 1 is a load back
				boolNew = true;
				intInterestDays = -1;
				blnDemandBill = false;
				switch (intType)
				{
					case 1:
						{
							// Load Back
							break;
						}
					case 2:
						{
							// Create Bills
							SetTypeCombo(ref lngPassType);
							SetMonthCombo(ref intPassBillMonth);
							SetYearCombo_2(Strings.Trim(lngPassBillYear.ToString()));
							cboBillType.Enabled = false;
							cboMonth.Enabled = false;
							cboYear.Enabled = false;
							t2kBilldate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
							rsTitleInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(lngPassType));
							if (rsTitleInfo.EndOfFile() != true && rsTitleInfo.BeginningOfFile() != true)
							{
								txtDescription.Text = rsTitleInfo.Get_Fields_String("TypeTitle") + " " + Strings.Format(intPassBillMonth, "00") + "/" + FCConvert.ToString(lngPassBillYear);
								intInterestDays = FCConvert.ToInt32(rsTitleInfo.Get_Fields_Int32("InterestStartDate"));
								if (FCConvert.ToString(rsTitleInfo.Get_Fields_String("FrequencyCode")) == "D")
								{
									blnDemandBill = true;
									txtStartDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
									txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
								}
							}
							txtIntDate.Text = Strings.Format(DateAndTime.DateAdd("d", intInterestDays, DateAndTime.DateValue(t2kBilldate.Text)), "MM/dd/yyyy");
							txtIntDate.ToolTipText = FCConvert.ToString(DateAndTime.DateDiff("d", DateAndTime.DateValue(t2kBilldate.Text), DateAndTime.DateValue(txtIntDate.Text))) + " days after billing";
							break;
						}
					case 200:
						{
							// Edit Key
							lngRateKey = lngPassRateKey;
							LoadRateInformation();
							this.Text = "Update Rate Record";
							boolNew = false;
							// this will allow the user to edit an existing Rate Record
							break;
						}
				}
				//end switch
				if (boolNew)
				{
					dtBillDate = dtPassBillDate.Value;
					dtStartDate = dtPassStartDate.Value;
				}
				this.Visible = false;
				this.Show(FormShowEnum.Modal, App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cboBillType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsType = new clsDRWrapper();
			if (cboBillType.ItemData(cboBillType.SelectedIndex) == 2 || cboBillType.ItemData(cboBillType.SelectedIndex) == 4)
			{
				lblIntRateOrFlatAmount.Text = "Flat Amount:";
			}
			else
			{
				lblIntRateOrFlatAmount.Text = "Interest Rate:";
			}
			rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(Conversion.Val(Strings.Trim(Strings.Left(cboBillType.Text, 3)))));
			if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
			{
				LoadMonths_8(rsType.Get_Fields_String("FrequencyCode"), FCConvert.ToInt16(rsType.Get_Fields_Int32("FirstMonth")));
				if (cboBillType.ItemData(cboBillType.SelectedIndex) == 2 || cboBillType.ItemData(cboBillType.SelectedIndex) == 4)
				{
					txtRateOrFlatAmount.Text = Strings.Format(rsType.Get_Fields_Decimal("FlatAmount"), "#,##0.00");
				}
				else
				{
					txtRateOrFlatAmount.Text = Strings.Format(Conversion.Val(rsType.Get_Fields_Double("PerDiemRate")) * 100, "0.00");
				}
				intInterestDays = FCConvert.ToInt32(rsType.Get_Fields_Int32("InterestStartDate"));
				if (Information.IsDate(t2kBilldate.Text))
				{
					if (intInterestDays != -1)
					{
						txtIntDate.Text = Strings.Format(DateAndTime.DateAdd("d", intInterestDays, DateAndTime.DateValue(t2kBilldate.Text)), "MM/dd/yyyy");
						txtIntDate.ToolTipText = FCConvert.ToString(DateAndTime.DateDiff("d", DateAndTime.DateValue(t2kBilldate.Text), DateAndTime.DateValue(txtIntDate.Text))) + " days after billing";
					}
				}
			}
			cboMonth.SelectedIndex = 0;
			SetYearCombo_2(FCConvert.ToString(DateTime.Today.Year));
		}

		private void frmAddRateRecord_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuFileExit_Click();
			}
		}

		private void frmAddRateRecord_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl is FCTextBox || this.ActiveControl is FCComboBox || this.ActiveControl is Global.T2KDateBox)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			else if (KeyAscii == Keys.F16 || KeyAscii == Keys.Back)
			{
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmAddRateRecord_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAddRateRecord.ScaleWidth	= 9300;
			//frmAddRateRecord.ScaleHeight	= 7620;
			//frmAddRateRecord.LinkTopic	= "Form1";
			//End Unmaped Properties
			//FillComboType();
			//LoadYears();
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuFileExit_Click()
		{
			mnuFileExit_Click(mnuFileExit, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (CheckEntries())
			{
				SaveRateRec();
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (CheckEntries())
			{
				SaveRateRec();
			}
		}

		private void SaveRateRec()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save the new Rate Record
				clsDRWrapper rsRate = new clsDRWrapper();
				string strNumofBills = "";
				clsDRWrapper rsCheck = new clsDRWrapper();
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				clsDRWrapper rsBillUpdate = new clsDRWrapper();
				if (intType == 200)
				{
					rsRate.OpenRecordset("SELECT Count(ID) AS Num FROM Bill WHERE BillNumber = " + FCConvert.ToString(lngRateKey));
					if (!rsRate.EndOfFile())
					{
						// TODO: Field [Num] not found!! (maybe it is an alias?)
						if (FCConvert.ToInt32(rsRate.Get_Fields("Num")) > 0)
						{
							// TODO: Field [Num] not found!! (maybe it is an alias?)
							strNumofBills = rsRate.Get_Fields("Num") + " bills.";
						}
						else
						{
							strNumofBills = "one bill.";
						}
					}
					else
					{
						strNumofBills = "at least one bill.";
					}
					switch (MessageBox.Show("This will affect " + strNumofBills + "  Are you sure that you would like to change this rate record?", "Change Rate Information", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								// keep rocking
								modGlobalFunctions.AddCYAEntry_26("AR", "Changing Rate Record", "Rate Record = " + FCConvert.ToString(lngRateKey));
								break;
							}
						case DialogResult.No:
						case DialogResult.Cancel:
							{
								MessageBox.Show("Save cancelled.", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
					}
					//end switch
					rsBillUpdate.Execute("UPDATE Bill SET BillDate = '" + FCConvert.ToString(DateAndTime.DateValue(t2kBilldate.Text)) + "' WHERE BillNumber = " + FCConvert.ToString(lngRateKey), modCLCalculations.strARDatabase);
					rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), "TWAR0000.vb1");
					if (rsRate.EndOfFile())
					{
						MessageBox.Show("Cannot find Rate Record #" + FCConvert.ToString(lngRateKey) + ".", "Missing Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					rsRate.Edit();
				}
				else
				{
					rsCheck.OpenRecordset("SELECT * FROM RateKeys WHERE BillType = " + FCConvert.ToString(Conversion.Val(Strings.Trim(Strings.Left(cboBillType.Text, 3)))) + " AND BillMonth = " + FCConvert.ToString(FCConvert.ToDateTime(cboMonth.Text + "/1/2000").Month) + " AND BillYear = " + cboYear.Text);
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						ans = MessageBox.Show("There is already a rate record for this type of bill for this billing period.  Are you sure you wish to add another rate record for the same billing period?", "Add Rate Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.No)
						{
							return;
						}
					}
					rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = 0");
					rsRate.AddNew();
				}
				rsRate.Set_Fields("BillType", FCConvert.ToString(Conversion.Val(Strings.Left(cboBillType.Text, 3))));
				rsRate.Set_Fields("BillDate", DateAndTime.DateValue(t2kBilldate.Text));
				rsRate.Set_Fields("DateCreated", DateTime.Now);
				rsRate.Set_Fields("Start", DateAndTime.DateValue(txtStartDate.Text));
				rsRate.Set_Fields("End", DateAndTime.DateValue(txtEndDate.Text));
				rsRate.Set_Fields("IntStart", DateAndTime.DateValue(txtIntDate.Text));
				if (cboBillType.ItemData(cboBillType.SelectedIndex) == 1 || cboBillType.ItemData(cboBillType.SelectedIndex) == 3)
				{
					if (Conversion.Val(txtRateOrFlatAmount.Text) == 0)
					{
						rsRate.Set_Fields("IntRate", 0);
					}
					else
					{
						rsRate.Set_Fields("IntRate", FCConvert.ToDouble(txtRateOrFlatAmount.Text) / 100);
					}
				}
				else
				{
					if (Conversion.Val(txtRateOrFlatAmount.Text) == 0)
					{
						rsRate.Set_Fields("FlatRate", 0);
					}
					else
					{
						rsRate.Set_Fields("FlatRate", FCConvert.ToDecimal(txtRateOrFlatAmount.Text));
					}
				}
				if (cboBillType.ItemData(cboBillType.SelectedIndex) == 1)
				{
					rsRate.Set_Fields("InterestMethod", "AP");
				}
				else if (cboBillType.ItemData(cboBillType.SelectedIndex) == 2)
				{
					rsRate.Set_Fields("InterestMethod", "AF");
				}
				else if (cboBillType.ItemData(cboBillType.SelectedIndex) == 3)
				{
					rsRate.Set_Fields("InterestMethod", "DP");
				}
				else
				{
					rsRate.Set_Fields("InterestMethod", "DF");
				}
				rsRate.Set_Fields("Description", Strings.Trim(txtDescription.Text));
				rsRate.Set_Fields("BillMonth", FCConvert.ToDateTime(cboMonth.Text + "/1/2000").Month);
				rsRate.Set_Fields("BillYear", cboYear.Text);
                rsRate.Update();
				if (intType == 1)
				{
					frmLoadBack.InstancePtr.lngReturnedRateKey = FCConvert.ToInt32(rsRate.Get_Fields_Int32("ID"));
				}
				
				boolDirty = false;
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Save Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckEntries()
		{
			bool CheckEntries = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				int x;
				string strIDate = "";
				string strDDate = "";
				DateTime dtTemp1;
				DateTime dtTemp2;
				CheckEntries = false;
				if (cboBillType.ItemData(cboBillType.SelectedIndex) != 3)
				{
					strTemp = txtRateOrFlatAmount.Text;
					if (Conversion.Val(strTemp) < 0 || Strings.Trim(strTemp) == "" || Conversion.Val(strTemp) > 100)
					{
						MessageBox.Show("The Interest Rate is not valid. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtRateOrFlatAmount.Focus();
						return CheckEntries;
					}
				}
				else
				{
					strTemp = txtRateOrFlatAmount.Text;
					if (Conversion.Val(strTemp) < 0 || Strings.Trim(strTemp) == "")
					{
						MessageBox.Show("The Flat Amount is not valid. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtRateOrFlatAmount.Focus();
						return CheckEntries;
					}
				}
				strTemp = t2kBilldate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The Billing Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					t2kBilldate.Focus();
					return CheckEntries;
				}
				strTemp = txtStartDate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The Start Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtStartDate.Focus();
					return CheckEntries;
				}
				strTemp = txtEndDate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The End Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEndDate.Focus();
					return CheckEntries;
				}
				strTemp = txtIntDate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The Interest Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtIntDate.Focus();
					return CheckEntries;
				}
				if (cboYear.SelectedIndex == -1 || cboMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must select a billing period before you may proceed.  Save Failed.", "Invalid Billing Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					cboMonth.Focus();
					return CheckEntries;
				}
				CheckEntries = true;
				return CheckEntries;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CheckEntries = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "." + "\r\n" + "Occured in Check Entries.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckEntries;
		}

		private bool LoadRateInformation()
		{
			bool LoadRateInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLoad = new clsDRWrapper();
				int intCT;
				rsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey));
				if (!rsLoad.EndOfFile())
				{
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					SetTypeCombo_2(FCConvert.ToInt32(rsLoad.Get_Fields("BillType")));
					if (cboBillType.ItemData(cboBillType.SelectedIndex) == 2 || cboBillType.ItemData(cboBillType.SelectedIndex) == 4)
					{
						lblIntRateOrFlatAmount.Text = "Flat Amount:";
						txtRateOrFlatAmount.Text = Strings.Format(rsLoad.Get_Fields_Decimal("FlatRate"), "#,##0.00");
					}
					else
					{
						lblIntRateOrFlatAmount.Text = "Interest Rate:";
						txtRateOrFlatAmount.Text = FCConvert.ToString(rsLoad.Get_Fields_Double("IntRate") * 100);
					}
					t2kBilldate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
					txtIntDate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy");
					txtStartDate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("Start"), "MM/dd/yyyy");
					// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
					txtEndDate.Text = Strings.Format(rsLoad.Get_Fields("End"), "MM/dd/yyyy");
					// TODO: Check the table for the column [BillYear] and replace with corresponding Get_Field method
					SetYearCombo_2(FCConvert.ToString(rsLoad.Get_Fields("BillYear")));
					SetMonthCombo_2(FCConvert.ToInt16(rsLoad.Get_Fields_Int32("BillMonth")));
					txtDescription.Text = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					LoadRateInformation = true;
				}
				else
				{
					MessageBox.Show("Cannot Load Rate Record #" + FCConvert.ToString(lngRateKey) + ".", "Missing Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					LoadRateInformation = false;
					return LoadRateInformation;
				}
				return LoadRateInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				LoadRateInformation = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "." + "\r\n" + "Occured in Check Entries.", "Load Record Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadRateInformation;
		}

		private void t2kBilldate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void t2kBilldate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsDate(t2kBilldate.Text))
			{
				t2kBilldate.Text = Strings.Format(t2kBilldate.Text, "MM/dd/yyyy");
				if (intInterestDays != -1)
				{
					txtIntDate.Text = Strings.Format(DateAndTime.DateAdd("d", intInterestDays, DateAndTime.DateValue(t2kBilldate.Text)), "MM/dd/yyyy");
					txtIntDate.ToolTipText = FCConvert.ToString(DateAndTime.DateDiff("d", DateAndTime.DateValue(t2kBilldate.Text), DateAndTime.DateValue(txtIntDate.Text))) + " days after billing";
				}
				if (blnDemandBill)
				{
					txtStartDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
					txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				}
			}
            else
            {
                MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                t2kBilldate.Text = Strings.Format("", "MM/dd/yyyy");
                e.Cancel = true;
            }
        }

		private void txtDescription_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtEndDate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtIntDate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtIntDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsDate(txtIntDate.Text))
			{
				txtIntDate.Text = Strings.Format(txtIntDate.Text, "MM/dd/yyyy");
				txtIntDate.ToolTipText = FCConvert.ToString(DateAndTime.DateDiff("d", DateAndTime.DateValue(t2kBilldate.Text), DateAndTime.DateValue(txtIntDate.Text))) + " days after billing";
			}
            else
            {
                MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtIntDate.Text = Strings.Format("", "MM/dd/yyyy");
                e.Cancel = true;
            }
        }

        private void txtRateOrFlatAmount_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtRateOrFlatAmount_Enter(object sender, System.EventArgs e)
		{
			txtRateOrFlatAmount.SelectionStart = 0;
			txtRateOrFlatAmount.SelectionLength = txtRateOrFlatAmount.Text.Length;
		}

		private void FillComboType()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill all of the types except for the ones w/o titles into cmbType
				int Number;
				string Name = "";
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				int intCT;
				// 1 = Demand
				// 2 = Per Diem
				// 3 = Flat
				rsTemp.DefaultDB = "TWAR0000.vb1";
				cboBillType.Clear();
				// type
				rsTemp.OpenRecordset("SELECT * FROM DefaultBillTypes ORDER BY TypeCode");
				if (rsTemp.BeginningOfFile() != true && rsTemp.EndOfFile() != true)
				{
					rsTemp.MoveFirst();
					do
					{
						cboBillType.AddItem(Strings.Format(rsTemp.Get_Fields_Int32("TypeCode"), "00") + "  -  " + rsTemp.Get_Fields_String("TypeTitle"));
						if (FCConvert.ToString(rsTemp.Get_Fields_String("InterestMethod")) == "AP")
						{
							cboBillType.ItemData(cboBillType.ListCount - 1, 1);
						}
						else if (rsTemp.Get_Fields_String("InterestMethod") == "AF")
						{
							cboBillType.ItemData(cboBillType.ListCount - 1, 2);
						}
						else if (rsTemp.Get_Fields_String("InterestMethod") == "DP")
						{
							cboBillType.ItemData(cboBillType.ListCount - 1, 3);
						}
						else
						{
							cboBillType.ItemData(cboBillType.ListCount - 1, 4);
						}
						rsTemp.MoveNext();
					}
					while (!rsTemp.EndOfFile());
				}
				else
				{
					MessageBox.Show("No types were loaded into the combobox, please go to 'Type Setup' and create one.", "No Bill Types Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetTypeCombo_2(int lngSearch)
		{
			SetTypeCombo(ref lngSearch);
		}

		private void SetTypeCombo(ref int lngSearch)
		{
			int counter;
			clsDRWrapper rsType = new clsDRWrapper();
			for (counter = 0; counter <= cboBillType.Items.Count - 1; counter++)
			{
				if (Conversion.Val(Strings.Left(cboBillType.Items[counter].ToString(), 3)) == lngSearch)
				{
					cboBillType.SelectedIndex = counter;
					break;
				}
			}
			rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(lngSearch));
			if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
			{
				LoadMonths_8(rsType.Get_Fields_String("FrequencyCode"), FCConvert.ToInt16(rsType.Get_Fields_Int32("FirstMonth")));
			}
		}

		private void LoadYears()
		{
			int counter;
			cboYear.Clear();
			for (counter = DateTime.Today.Year - 10; counter <= DateTime.Today.Year + 3; counter++)
			{
				cboYear.AddItem(counter.ToString());
			}
		}
		// vbPorter upgrade warning: strCriteria As string	OnWrite(string, int)
		private void SetYearCombo_2(string strCriteria)
		{
			SetYearCombo(ref strCriteria);
		}

		private void SetYearCombo(ref string strCriteria)
		{
			int counter;
			cboYear.SelectedIndex = -1;
			if (strCriteria == "")
			{
				return;
			}
			for (counter = 0; counter <= cboYear.Items.Count - 1; counter++)
			{
				if (Strings.Left(cboYear.Items[counter].ToString(), strCriteria.Length) == strCriteria)
				{
					cboYear.SelectedIndex = counter;
					break;
				}
			}
		}

		private void SetMonthCombo_2(short intCriteria)
		{
			SetMonthCombo(ref intCriteria);
		}

		private void SetMonthCombo(ref short intCriteria)
		{
			int counter;
			string strCriteria;
			strCriteria = Strings.Format(FCConvert.ToString(intCriteria) + "/1/2000", "MMMM");
			cboMonth.SelectedIndex = -1;
			if (strCriteria == "")
			{
				return;
			}
			for (counter = 0; counter <= cboMonth.Items.Count - 1; counter++)
			{
				if (Strings.Left(cboMonth.Items[counter].ToString(), strCriteria.Length) == strCriteria)
				{
					cboMonth.SelectedIndex = counter;
					break;
				}
			}
		}

		private void LoadMonths_8(string strFreq, short intFirst)
		{
			LoadMonths(ref strFreq, ref intFirst);
		}

		private void LoadMonths(ref string strFreq, ref short intFirst)
		{
			int intCounter;
			int intStep = 0;
			if ((strFreq == "D") || (strFreq == "M"))
			{
				intStep = 1;
				intFirst = 1;
			}
			else if (strFreq == "Y")
			{
				intStep = 12;
			}
			else if (strFreq == "Q")
			{
				intStep = 3;
			}
			else if (strFreq == "B")
			{
				intStep = 2;
			}
			else if (strFreq == "S")
			{
				intStep = 6;
			}
			cboMonth.Clear();
			for (intCounter = intFirst; intCounter <= /*?*/12; intCounter += intStep)
			{
				cboMonth.AddItem(Strings.Format(FCConvert.ToString(intCounter) + "/1/2000", "MMMM"));
			}
		}

		private void txtStartDate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSaveExit_Click(sender, e);
		}
	}
}
