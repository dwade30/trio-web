﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	partial class frmCustomize : BaseForm
	{
		public fecherFoundation.FCComboBox cmbInterestType;
		public fecherFoundation.FCLabel lblInterestType;
		public fecherFoundation.FCComboBox cmbSalesTaxReceipt;
		public fecherFoundation.FCLabel lblSalesTaxReceipt;
		public fecherFoundation.FCComboBox cmbBasis;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTaxRate;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboDefaultPayee;
		public fecherFoundation.FCFrame fraPrePayReceivable;
		public FCGrid txtPrePayReceivable;
		public fecherFoundation.FCLabel lblPrePayReceivable;
		public fecherFoundation.FCCheckBox chkAutoCreateJournal;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtBillAdjustment;
		public fecherFoundation.FCTextBox txtLabelAdjustment;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblInstruct;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraInterest;
		public fecherFoundation.FCComboBox cboInterestStartDate;
		public fecherFoundation.FCTextBox txtFlatRate;
		public fecherFoundation.FCTextBox txtPerDiemRate;
		public fecherFoundation.FCLabel lblTaxRate_5;
		public fecherFoundation.FCLabel lblTaxRate_4;
		public fecherFoundation.FCLabel lblTaxRate_3;
		public fecherFoundation.FCLabel lblTaxRate_2;
		public fecherFoundation.FCFrame fraLocationInformation;
		public fecherFoundation.FCComboBox cboState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtTaxRate;
		public FCGrid txtSalesTaxReceivable;
		public FCGrid txtSalesTaxPayable;
		public fecherFoundation.FCLabel lblTaxRate_0;
		public fecherFoundation.FCLabel lblTaxRate_1;
		public fecherFoundation.FCLabel lblSalesTaxReceivable;
		public fecherFoundation.FCLabel lblSalesTaxPayable;
		public fecherFoundation.FCFrame fraPaymentOptions;
		public fecherFoundation.FCCheckBox chkDefaultAccount;
		public fecherFoundation.FCLabel lblBasis;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbInterestType = new fecherFoundation.FCComboBox();
			this.lblInterestType = new fecherFoundation.FCLabel();
			this.cmbSalesTaxReceipt = new fecherFoundation.FCComboBox();
			this.lblSalesTaxReceipt = new fecherFoundation.FCLabel();
			this.cmbBasis = new fecherFoundation.FCComboBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboDefaultPayee = new fecherFoundation.FCComboBox();
			this.fraPrePayReceivable = new fecherFoundation.FCFrame();
			this.txtPrePayReceivable = new fecherFoundation.FCGrid();
			this.lblPrePayReceivable = new fecherFoundation.FCLabel();
			this.chkAutoCreateJournal = new fecherFoundation.FCCheckBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtBillAdjustment = new fecherFoundation.FCTextBox();
			this.txtLabelAdjustment = new fecherFoundation.FCTextBox();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblInstruct = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.fraInterest = new fecherFoundation.FCFrame();
			this.cboInterestStartDate = new fecherFoundation.FCComboBox();
			this.txtFlatRate = new fecherFoundation.FCTextBox();
			this.txtPerDiemRate = new fecherFoundation.FCTextBox();
			this.lblTaxRate_5 = new fecherFoundation.FCLabel();
			this.lblTaxRate_4 = new fecherFoundation.FCLabel();
			this.lblTaxRate_3 = new fecherFoundation.FCLabel();
			this.lblTaxRate_2 = new fecherFoundation.FCLabel();
			this.fraLocationInformation = new fecherFoundation.FCFrame();
			this.cboState = new fecherFoundation.FCComboBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtTaxRate = new fecherFoundation.FCTextBox();
			this.txtSalesTaxReceivable = new fecherFoundation.FCGrid();
			this.txtSalesTaxPayable = new fecherFoundation.FCGrid();
			this.lblTaxRate_0 = new fecherFoundation.FCLabel();
			this.lblTaxRate_1 = new fecherFoundation.FCLabel();
			this.lblSalesTaxReceivable = new fecherFoundation.FCLabel();
			this.lblSalesTaxPayable = new fecherFoundation.FCLabel();
			this.fraPaymentOptions = new fecherFoundation.FCFrame();
			this.chkDefaultAccount = new fecherFoundation.FCCheckBox();
			this.lblBasis = new fecherFoundation.FCLabel();
			this.btnSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPrePayReceivable)).BeginInit();
			this.fraPrePayReceivable.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPrePayReceivable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoCreateJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraInterest)).BeginInit();
			this.fraInterest.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraLocationInformation)).BeginInit();
			this.fraLocationInformation.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtSalesTaxReceivable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalesTaxPayable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPaymentOptions)).BeginInit();
			this.fraPaymentOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDefaultAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 742);
			this.BottomPanel.Size = new System.Drawing.Size(939, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.fraPrePayReceivable);
			this.ClientArea.Controls.Add(this.chkAutoCreateJournal);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.fraInterest);
			this.ClientArea.Controls.Add(this.fraLocationInformation);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.fraPaymentOptions);
			this.ClientArea.Size = new System.Drawing.Size(939, 548);
			this.ClientArea.Controls.SetChildIndex(this.fraPaymentOptions, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraLocationInformation, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraInterest, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkAutoCreateJournal, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraPrePayReceivable, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(939, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(128, 30);
			this.HeaderText.Text = "Customize";
			// 
			// cmbInterestType
			// 
			this.cmbInterestType.Items.AddRange(new object[] {
            "Auto Per Diem",
            "Auto Flat Rate",
            "Demand Per Diem",
            "Demand Flat Rate"});
			this.cmbInterestType.Location = new System.Drawing.Point(178, 180);
			this.cmbInterestType.Name = "cmbInterestType";
			this.cmbInterestType.Size = new System.Drawing.Size(232, 40);
			this.cmbInterestType.TabIndex = 8;
			// 
			// lblInterestType
			// 
			this.lblInterestType.AutoSize = true;
			this.lblInterestType.Location = new System.Drawing.Point(20, 194);
			this.lblInterestType.Name = "lblInterestType";
			this.lblInterestType.Size = new System.Drawing.Size(113, 16);
			this.lblInterestType.TabIndex = 7;
			this.lblInterestType.Text = "INTEREST TYPE";
			// 
			// cmbSalesTaxReceipt
			// 
			this.cmbSalesTaxReceipt.Items.AddRange(new object[] {
            "Record Upon Receipt",
            "Record At Billing"});
			this.cmbSalesTaxReceipt.Location = new System.Drawing.Point(165, 80);
			this.cmbSalesTaxReceipt.Name = "cmbSalesTaxReceipt";
			this.cmbSalesTaxReceipt.Size = new System.Drawing.Size(245, 40);
			this.cmbSalesTaxReceipt.TabIndex = 4;
			this.cmbSalesTaxReceipt.SelectedIndexChanged += new System.EventHandler(this.cmbSalesTaxReceipt_SelectedIndexChanged);
			// 
			// lblSalesTaxReceipt
			// 
			this.lblSalesTaxReceipt.Location = new System.Drawing.Point(20, 94);
			this.lblSalesTaxReceipt.Name = "lblSalesTaxReceipt";
			this.lblSalesTaxReceipt.Size = new System.Drawing.Size(115, 16);
			this.lblSalesTaxReceipt.TabIndex = 3;
			this.lblSalesTaxReceipt.Text = "RECORD SALES TAX";
			// 
			// cmbBasis
			// 
			this.cmbBasis.Items.AddRange(new object[] {
            "365 Days",
            "360 Days"});
			this.cmbBasis.Location = new System.Drawing.Point(94, 30);
			this.cmbBasis.Name = "cmbBasis";
			this.cmbBasis.Size = new System.Drawing.Size(121, 40);
			this.cmbBasis.TabIndex = 1;
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboDefaultPayee);
			this.Frame2.Location = new System.Drawing.Point(30, 652);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(399, 90);
			this.Frame2.TabIndex = 5;
			this.Frame2.Text = "Default Payee";
			// 
			// cboDefaultPayee
			// 
			this.cboDefaultPayee.BackColor = System.Drawing.SystemColors.Window;
			this.cboDefaultPayee.Location = new System.Drawing.Point(20, 30);
			this.cboDefaultPayee.Name = "cboDefaultPayee";
			this.cboDefaultPayee.Size = new System.Drawing.Size(359, 40);
			this.cboDefaultPayee.Sorted = true;
			// 
			// fraPrePayReceivable
			// 
			this.fraPrePayReceivable.Controls.Add(this.txtPrePayReceivable);
			this.fraPrePayReceivable.Controls.Add(this.lblPrePayReceivable);
			this.fraPrePayReceivable.Location = new System.Drawing.Point(30, 542);
			this.fraPrePayReceivable.Name = "fraPrePayReceivable";
			this.fraPrePayReceivable.Size = new System.Drawing.Size(399, 90);
			this.fraPrePayReceivable.TabIndex = 6;
			this.fraPrePayReceivable.Text = "Pre Payment Options";
			// 
			// txtPrePayReceivable
			// 
			this.txtPrePayReceivable.Cols = 1;
			this.txtPrePayReceivable.ColumnHeadersVisible = false;
			this.txtPrePayReceivable.FixedCols = 0;
			this.txtPrePayReceivable.FixedRows = 0;
			this.txtPrePayReceivable.Location = new System.Drawing.Point(203, 30);
			this.txtPrePayReceivable.Name = "txtPrePayReceivable";
			this.txtPrePayReceivable.RowHeadersVisible = false;
			this.txtPrePayReceivable.Rows = 1;
			this.txtPrePayReceivable.Size = new System.Drawing.Size(176, 42);
			this.txtPrePayReceivable.TabIndex = 1;
			// 
			// lblPrePayReceivable
			// 
			this.lblPrePayReceivable.Location = new System.Drawing.Point(20, 44);
			this.lblPrePayReceivable.Name = "lblPrePayReceivable";
			this.lblPrePayReceivable.Size = new System.Drawing.Size(167, 16);
			this.lblPrePayReceivable.Text = "PRE PAY RECEIVABLE ACCT";
			// 
			// chkAutoCreateJournal
			// 
			this.chkAutoCreateJournal.AutoSize = false;
			this.chkAutoCreateJournal.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.chkAutoCreateJournal.Location = new System.Drawing.Point(30, 476);
			this.chkAutoCreateJournal.Name = "chkAutoCreateJournal";
			this.chkAutoCreateJournal.Size = new System.Drawing.Size(399, 46);
			this.chkAutoCreateJournal.TabIndex = 4;
			this.chkAutoCreateJournal.Text = "Create Demand Bill Journal Automatically when Daily Closeout is run";
			this.chkAutoCreateJournal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtBillAdjustment);
			this.Frame3.Controls.Add(this.txtLabelAdjustment);
			this.Frame3.Controls.Add(this.Label4);
			this.Frame3.Controls.Add(this.lblInstruct);
			this.Frame3.Controls.Add(this.Label5);
			this.Frame3.Controls.Add(this.Label3);
			this.Frame3.Location = new System.Drawing.Point(30, 240);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(399, 216);
			this.Frame3.TabIndex = 2;
			this.Frame3.Text = "Laser Printer Line Adjustment";
			// 
			// txtBillAdjustment
			// 
			this.txtBillAdjustment.BackColor = System.Drawing.SystemColors.Window;
			this.txtBillAdjustment.Location = new System.Drawing.Point(160, 30);
			this.txtBillAdjustment.Name = "txtBillAdjustment";
			this.txtBillAdjustment.Size = new System.Drawing.Size(80, 40);
			this.txtBillAdjustment.TabIndex = 1;
			// 
			// txtLabelAdjustment
			// 
			this.txtLabelAdjustment.BackColor = System.Drawing.SystemColors.Window;
			this.txtLabelAdjustment.Location = new System.Drawing.Point(160, 80);
			this.txtLabelAdjustment.Name = "txtLabelAdjustment";
			this.txtLabelAdjustment.Size = new System.Drawing.Size(80, 40);
			this.txtLabelAdjustment.TabIndex = 3;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 44);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(70, 16);
			this.Label4.Text = "LASER BILLS";
			// 
			// lblInstruct
			// 
			this.lblInstruct.Location = new System.Drawing.Point(20, 130);
			this.lblInstruct.Name = "lblInstruct";
			this.lblInstruct.Size = new System.Drawing.Size(329, 45);
			this.lblInstruct.TabIndex = 4;
			this.lblInstruct.Text = "ENTER THE NUMBER OF LINES THAT THE DATA ON YOUR REPORT NEEDS TO BE MOVED.  FRACTI" +
    "ONAL NUMBERS CAN BE ENTERED";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 185);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(110, 16);
			this.Label5.TabIndex = 5;
			this.Label5.Text = "EXAMPLES (1, 1.5, 2)";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 94);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(83, 16);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "LASER LABELS";
			// 
			// fraInterest
			// 
			this.fraInterest.Controls.Add(this.cboInterestStartDate);
			this.fraInterest.Controls.Add(this.cmbInterestType);
			this.fraInterest.Controls.Add(this.lblInterestType);
			this.fraInterest.Controls.Add(this.txtFlatRate);
			this.fraInterest.Controls.Add(this.txtPerDiemRate);
			this.fraInterest.Controls.Add(this.lblTaxRate_5);
			this.fraInterest.Controls.Add(this.lblTaxRate_4);
			this.fraInterest.Controls.Add(this.lblTaxRate_3);
			this.fraInterest.Controls.Add(this.lblTaxRate_2);
			this.fraInterest.Location = new System.Drawing.Point(449, 470);
			this.fraInterest.Name = "fraInterest";
			this.fraInterest.Size = new System.Drawing.Size(430, 240);
			this.fraInterest.TabIndex = 5;
			this.fraInterest.Text = "Default Interest Options";
			// 
			// cboInterestStartDate
			// 
			this.cboInterestStartDate.BackColor = System.Drawing.SystemColors.Window;
			this.cboInterestStartDate.Location = new System.Drawing.Point(178, 130);
			this.cboInterestStartDate.Name = "cboInterestStartDate";
			this.cboInterestStartDate.Size = new System.Drawing.Size(232, 40);
			this.cboInterestStartDate.TabIndex = 6;
			// 
			// txtFlatRate
			// 
			this.txtFlatRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtFlatRate.Location = new System.Drawing.Point(178, 80);
			this.txtFlatRate.Name = "txtFlatRate";
			this.txtFlatRate.Size = new System.Drawing.Size(71, 40);
			this.txtFlatRate.TabIndex = 4;
			this.txtFlatRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtFlatRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtFlatRate_Validating);
			// 
			// txtPerDiemRate
			// 
			this.txtPerDiemRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtPerDiemRate.Location = new System.Drawing.Point(178, 30);
			this.txtPerDiemRate.Name = "txtPerDiemRate";
			this.txtPerDiemRate.Size = new System.Drawing.Size(71, 40);
			this.txtPerDiemRate.TabIndex = 1;
			this.txtPerDiemRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtPerDiemRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtPerDiemRate_Validating);
			// 
			// lblTaxRate_5
			// 
			this.lblTaxRate_5.Location = new System.Drawing.Point(20, 144);
			this.lblTaxRate_5.Name = "lblTaxRate_5";
			this.lblTaxRate_5.Size = new System.Drawing.Size(143, 16);
			this.lblTaxRate_5.TabIndex = 5;
			this.lblTaxRate_5.Text = "INTEREST START DATE";
			// 
			// lblTaxRate_4
			// 
			this.lblTaxRate_4.Location = new System.Drawing.Point(20, 94);
			this.lblTaxRate_4.Name = "lblTaxRate_4";
			this.lblTaxRate_4.Size = new System.Drawing.Size(86, 16);
			this.lblTaxRate_4.TabIndex = 3;
			this.lblTaxRate_4.Text = "FLAT RATE";
			// 
			// lblTaxRate_3
			// 
			this.lblTaxRate_3.Location = new System.Drawing.Point(269, 44);
			this.lblTaxRate_3.Name = "lblTaxRate_3";
			this.lblTaxRate_3.Size = new System.Drawing.Size(13, 16);
			this.lblTaxRate_3.TabIndex = 2;
			this.lblTaxRate_3.Text = "%";
			// 
			// lblTaxRate_2
			// 
			this.lblTaxRate_2.Location = new System.Drawing.Point(20, 44);
			this.lblTaxRate_2.Name = "lblTaxRate_2";
			this.lblTaxRate_2.Size = new System.Drawing.Size(88, 16);
			this.lblTaxRate_2.Text = "PER DIEM RATE";
			// 
			// fraLocationInformation
			// 
			this.fraLocationInformation.Controls.Add(this.cboState);
			this.fraLocationInformation.Controls.Add(this.txtZip);
			this.fraLocationInformation.Controls.Add(this.txtCity);
			this.fraLocationInformation.Controls.Add(this.Label11);
			this.fraLocationInformation.Controls.Add(this.Label10);
			this.fraLocationInformation.Controls.Add(this.Label9);
			this.fraLocationInformation.Location = new System.Drawing.Point(30, 30);
			this.fraLocationInformation.Name = "fraLocationInformation";
			this.fraLocationInformation.Size = new System.Drawing.Size(399, 190);
			this.fraLocationInformation.Text = "Default Location Information";
			// 
			// cboState
			// 
			this.cboState.BackColor = System.Drawing.SystemColors.Window;
			this.cboState.Location = new System.Drawing.Point(145, 80);
			this.cboState.Name = "cboState";
			this.cboState.Size = new System.Drawing.Size(234, 40);
			this.cboState.Sorted = true;
			this.cboState.TabIndex = 3;
			// 
			// txtZip
			// 
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.Location = new System.Drawing.Point(145, 130);
			this.txtZip.MaxLength = 5;
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(102, 40);
			this.txtZip.TabIndex = 5;
			// 
			// txtCity
			// 
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.Location = new System.Drawing.Point(145, 30);
			this.txtCity.MaxLength = 25;
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(234, 40);
			this.txtCity.TabIndex = 1;
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(20, 144);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(75, 16);
			this.Label11.TabIndex = 4;
			this.Label11.Text = "DEFAULT ZIP";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(20, 94);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(92, 16);
			this.Label10.TabIndex = 2;
			this.Label10.Text = "DEFAULT STATE";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 44);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(83, 16);
			this.Label9.Text = "DEFAULT CITY";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtTaxRate);
			this.Frame1.Controls.Add(this.cmbSalesTaxReceipt);
			this.Frame1.Controls.Add(this.lblSalesTaxReceipt);
			this.Frame1.Controls.Add(this.txtSalesTaxReceivable);
			this.Frame1.Controls.Add(this.txtSalesTaxPayable);
			this.Frame1.Controls.Add(this.lblTaxRate_0);
			this.Frame1.Controls.Add(this.lblTaxRate_1);
			this.Frame1.Controls.Add(this.lblSalesTaxReceivable);
			this.Frame1.Controls.Add(this.lblSalesTaxPayable);
			this.Frame1.Location = new System.Drawing.Point(449, 196);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(430, 254);
			this.Frame1.TabIndex = 3;
			this.Frame1.Text = "Sales Tax Options";
			// 
			// txtTaxRate
			// 
			this.txtTaxRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtTaxRate.Location = new System.Drawing.Point(165, 30);
			this.txtTaxRate.Name = "txtTaxRate";
			this.txtTaxRate.Size = new System.Drawing.Size(72, 40);
			this.txtTaxRate.TabIndex = 1;
			this.txtTaxRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtTaxRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaxRate_Validating);
			// 
			// txtSalesTaxReceivable
			// 
			this.txtSalesTaxReceivable.Cols = 1;
			this.txtSalesTaxReceivable.ColumnHeadersVisible = false;
			this.txtSalesTaxReceivable.Enabled = false;
			this.txtSalesTaxReceivable.FixedCols = 0;
			this.txtSalesTaxReceivable.FixedRows = 0;
			this.txtSalesTaxReceivable.Location = new System.Drawing.Point(245, 137);
			this.txtSalesTaxReceivable.Name = "txtSalesTaxReceivable";
			this.txtSalesTaxReceivable.RowHeadersVisible = false;
			this.txtSalesTaxReceivable.Rows = 1;
			this.txtSalesTaxReceivable.Size = new System.Drawing.Size(165, 42);
			this.txtSalesTaxReceivable.TabIndex = 6;
			// 
			// txtSalesTaxPayable
			// 
			this.txtSalesTaxPayable.Cols = 1;
			this.txtSalesTaxPayable.ColumnHeadersVisible = false;
			this.txtSalesTaxPayable.Enabled = false;
			this.txtSalesTaxPayable.FixedCols = 0;
			this.txtSalesTaxPayable.FixedRows = 0;
			this.txtSalesTaxPayable.Location = new System.Drawing.Point(245, 189);
			this.txtSalesTaxPayable.Name = "txtSalesTaxPayable";
			this.txtSalesTaxPayable.RowHeadersVisible = false;
			this.txtSalesTaxPayable.Rows = 1;
			this.txtSalesTaxPayable.Size = new System.Drawing.Size(165, 42);
			this.txtSalesTaxPayable.TabIndex = 8;
			// 
			// lblTaxRate_0
			// 
			this.lblTaxRate_0.Location = new System.Drawing.Point(20, 44);
			this.lblTaxRate_0.Name = "lblTaxRate_0";
			this.lblTaxRate_0.Size = new System.Drawing.Size(60, 16);
			this.lblTaxRate_0.Text = "TAX RATE";
			// 
			// lblTaxRate_1
			// 
			this.lblTaxRate_1.Location = new System.Drawing.Point(257, 44);
			this.lblTaxRate_1.Name = "lblTaxRate_1";
			this.lblTaxRate_1.Size = new System.Drawing.Size(13, 16);
			this.lblTaxRate_1.TabIndex = 2;
			this.lblTaxRate_1.Text = "%";
			// 
			// lblSalesTaxReceivable
			// 
			this.lblSalesTaxReceivable.Enabled = false;
			this.lblSalesTaxReceivable.Location = new System.Drawing.Point(20, 151);
			this.lblSalesTaxReceivable.Name = "lblSalesTaxReceivable";
			this.lblSalesTaxReceivable.Size = new System.Drawing.Size(195, 16);
			this.lblSalesTaxReceivable.TabIndex = 5;
			this.lblSalesTaxReceivable.Text = "SALES TAX RECEIVABLE ACCOUNT";
			// 
			// lblSalesTaxPayable
			// 
			this.lblSalesTaxPayable.Enabled = false;
			this.lblSalesTaxPayable.Location = new System.Drawing.Point(20, 203);
			this.lblSalesTaxPayable.Name = "lblSalesTaxPayable";
			this.lblSalesTaxPayable.Size = new System.Drawing.Size(178, 16);
			this.lblSalesTaxPayable.TabIndex = 7;
			this.lblSalesTaxPayable.Text = "SALES TAX PAYABLE ACCOUNT";
			// 
			// fraPaymentOptions
			// 
			this.fraPaymentOptions.Controls.Add(this.chkDefaultAccount);
			this.fraPaymentOptions.Controls.Add(this.cmbBasis);
			this.fraPaymentOptions.Controls.Add(this.lblBasis);
			this.fraPaymentOptions.Location = new System.Drawing.Point(449, 30);
			this.fraPaymentOptions.Name = "fraPaymentOptions";
			this.fraPaymentOptions.Size = new System.Drawing.Size(430, 146);
			this.fraPaymentOptions.TabIndex = 1;
			this.fraPaymentOptions.Text = "Payment Options";
			// 
			// chkDefaultAccount
			// 
			this.chkDefaultAccount.AutoSize = false;
			this.chkDefaultAccount.ForeColor = System.Drawing.Color.FromName("@windowtext");
			this.chkDefaultAccount.Location = new System.Drawing.Point(20, 80);
			this.chkDefaultAccount.Name = "chkDefaultAccount";
			this.chkDefaultAccount.Size = new System.Drawing.Size(360, 46);
			this.chkDefaultAccount.TabIndex = 2;
			this.chkDefaultAccount.Text = "Show last account accessed when entering from Cash Receipting";
			this.chkDefaultAccount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblBasis
			// 
			this.lblBasis.Location = new System.Drawing.Point(20, 44);
			this.lblBasis.Name = "lblBasis";
			this.lblBasis.Size = new System.Drawing.Size(39, 16);
			this.lblBasis.Text = "BASIS";
			// 
			// btnSave
			// 
			this.btnSave.AppearanceKey = "acceptButton";
			this.btnSave.Location = new System.Drawing.Point(354, 30);
			this.btnSave.Name = "btnSave";
			this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnSave.Size = new System.Drawing.Size(124, 48);
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// frmCustomize
			// 
			this.ClientSize = new System.Drawing.Size(939, 608);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCustomize";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Customize";
			this.Load += new System.EventHandler(this.frmCustomize_Load);
			this.Activated += new System.EventHandler(this.frmCustomize_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomize_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomize_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraPrePayReceivable)).EndInit();
			this.fraPrePayReceivable.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtPrePayReceivable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoCreateJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraInterest)).EndInit();
			this.fraInterest.ResumeLayout(false);
			this.fraInterest.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraLocationInformation)).EndInit();
			this.fraLocationInformation.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtSalesTaxReceivable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalesTaxPayable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPaymentOptions)).EndInit();
			this.fraPaymentOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkDefaultAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnSave;
	}
}
