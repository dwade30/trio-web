﻿namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptBillList.
	/// </summary>
	partial class rptBillList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDescription1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReferenceDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl1Description = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl2Description = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl3Description = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOptions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCustomer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCustomerNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCustomerTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldInvoiceNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBillDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.InvoiceBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoiceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReferenceDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl1Description)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl2Description)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl3Description)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.InvoiceBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDescription1,
				this.fldDescription2,
				this.fldDescription3,
				this.fldDescription4,
				this.fldDescription5,
				this.fldDescription6,
				this.fldDescription7,
				this.fldAmount1,
				this.fldAmount2,
				this.fldAmount3,
				this.fldAmount4,
				this.fldAmount5,
				this.fldAmount6,
				this.fldAmount7,
				this.fldReferenceDescription,
				this.fldControl1Description,
				this.fldControl2Description,
				this.fldControl3Description,
				this.fldReference,
				this.fldControl1,
				this.fldControl2,
				this.fldControl3
			});
			this.Detail.Height = 1.104167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDescription1
			// 
			this.fldDescription1.Height = 0.19F;
			this.fldDescription1.Left = 2.34375F;
			this.fldDescription1.Name = "fldDescription1";
			this.fldDescription1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDescription1.Text = "Field1";
			this.fldDescription1.Top = 0F;
			this.fldDescription1.Width = 1.34375F;
			// 
			// fldDescription2
			// 
			this.fldDescription2.Height = 0.19F;
			this.fldDescription2.Left = 2.34375F;
			this.fldDescription2.Name = "fldDescription2";
			this.fldDescription2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDescription2.Text = "Field1";
			this.fldDescription2.Top = 0.15625F;
			this.fldDescription2.Width = 1.34375F;
			// 
			// fldDescription3
			// 
			this.fldDescription3.Height = 0.19F;
			this.fldDescription3.Left = 2.34375F;
			this.fldDescription3.Name = "fldDescription3";
			this.fldDescription3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDescription3.Text = "Field1";
			this.fldDescription3.Top = 0.3125F;
			this.fldDescription3.Width = 1.34375F;
			// 
			// fldDescription4
			// 
			this.fldDescription4.Height = 0.19F;
			this.fldDescription4.Left = 2.34375F;
			this.fldDescription4.Name = "fldDescription4";
			this.fldDescription4.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDescription4.Text = "Field1";
			this.fldDescription4.Top = 0.46875F;
			this.fldDescription4.Width = 1.34375F;
			// 
			// fldDescription5
			// 
			this.fldDescription5.Height = 0.19F;
			this.fldDescription5.Left = 2.34375F;
			this.fldDescription5.Name = "fldDescription5";
			this.fldDescription5.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDescription5.Text = "Field1";
			this.fldDescription5.Top = 0.625F;
			this.fldDescription5.Width = 1.34375F;
			// 
			// fldDescription6
			// 
			this.fldDescription6.Height = 0.19F;
			this.fldDescription6.Left = 2.34375F;
			this.fldDescription6.Name = "fldDescription6";
			this.fldDescription6.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDescription6.Text = "Field1";
			this.fldDescription6.Top = 0.78125F;
			this.fldDescription6.Width = 1.34375F;
			// 
			// fldDescription7
			// 
			this.fldDescription7.Height = 0.19F;
			this.fldDescription7.Left = 2.34375F;
			this.fldDescription7.Name = "fldDescription7";
			this.fldDescription7.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDescription7.Text = "Field1";
			this.fldDescription7.Top = 0.9375F;
			this.fldDescription7.Width = 1.34375F;
			// 
			// fldAmount1
			// 
			this.fldAmount1.Height = 0.19F;
			this.fldAmount1.Left = 3.71875F;
			this.fldAmount1.Name = "fldAmount1";
			this.fldAmount1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount1.Text = "Field1";
			this.fldAmount1.Top = 0F;
			this.fldAmount1.Width = 0.875F;
			// 
			// fldAmount2
			// 
			this.fldAmount2.Height = 0.19F;
			this.fldAmount2.Left = 3.71875F;
			this.fldAmount2.Name = "fldAmount2";
			this.fldAmount2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount2.Text = "Field1";
			this.fldAmount2.Top = 0.15625F;
			this.fldAmount2.Width = 0.875F;
			// 
			// fldAmount3
			// 
			this.fldAmount3.Height = 0.19F;
			this.fldAmount3.Left = 3.71875F;
			this.fldAmount3.Name = "fldAmount3";
			this.fldAmount3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount3.Text = "Field1";
			this.fldAmount3.Top = 0.3125F;
			this.fldAmount3.Width = 0.875F;
			// 
			// fldAmount4
			// 
			this.fldAmount4.Height = 0.19F;
			this.fldAmount4.Left = 3.71875F;
			this.fldAmount4.Name = "fldAmount4";
			this.fldAmount4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount4.Text = "Field1";
			this.fldAmount4.Top = 0.46875F;
			this.fldAmount4.Width = 0.875F;
			// 
			// fldAmount5
			// 
			this.fldAmount5.Height = 0.19F;
			this.fldAmount5.Left = 3.71875F;
			this.fldAmount5.Name = "fldAmount5";
			this.fldAmount5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount5.Text = "Field1";
			this.fldAmount5.Top = 0.625F;
			this.fldAmount5.Width = 0.875F;
			// 
			// fldAmount6
			// 
			this.fldAmount6.Height = 0.19F;
			this.fldAmount6.Left = 3.71875F;
			this.fldAmount6.Name = "fldAmount6";
			this.fldAmount6.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount6.Text = "Field1";
			this.fldAmount6.Top = 0.78125F;
			this.fldAmount6.Width = 0.875F;
			// 
			// fldAmount7
			// 
			this.fldAmount7.Height = 0.19F;
			this.fldAmount7.Left = 3.71875F;
			this.fldAmount7.Name = "fldAmount7";
			this.fldAmount7.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount7.Text = "Field1";
			this.fldAmount7.Top = 0.9375F;
			this.fldAmount7.Width = 0.875F;
			// 
			// fldReferenceDescription
			// 
			this.fldReferenceDescription.Height = 0.19F;
			this.fldReferenceDescription.Left = 5.21875F;
			this.fldReferenceDescription.Name = "fldReferenceDescription";
			this.fldReferenceDescription.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldReferenceDescription.Text = "Field1";
			this.fldReferenceDescription.Top = 0F;
			this.fldReferenceDescription.Width = 1.25F;
			// 
			// fldControl1Description
			// 
			this.fldControl1Description.Height = 0.19F;
			this.fldControl1Description.Left = 5.21875F;
			this.fldControl1Description.Name = "fldControl1Description";
			this.fldControl1Description.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldControl1Description.Text = "Field1";
			this.fldControl1Description.Top = 0.15625F;
			this.fldControl1Description.Width = 1.25F;
			// 
			// fldControl2Description
			// 
			this.fldControl2Description.Height = 0.19F;
			this.fldControl2Description.Left = 5.21875F;
			this.fldControl2Description.Name = "fldControl2Description";
			this.fldControl2Description.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldControl2Description.Text = "Field1";
			this.fldControl2Description.Top = 0.3125F;
			this.fldControl2Description.Width = 1.25F;
			// 
			// fldControl3Description
			// 
			this.fldControl3Description.Height = 0.19F;
			this.fldControl3Description.Left = 5.21875F;
			this.fldControl3Description.Name = "fldControl3Description";
			this.fldControl3Description.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldControl3Description.Text = "Field1";
			this.fldControl3Description.Top = 0.46875F;
			this.fldControl3Description.Width = 1.25F;
			// 
			// fldReference
			// 
			this.fldReference.Height = 0.19F;
			this.fldReference.Left = 6.5F;
			this.fldReference.Name = "fldReference";
			this.fldReference.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldReference.Text = "Field1";
			this.fldReference.Top = 0F;
			this.fldReference.Width = 1F;
			// 
			// fldControl1
			// 
			this.fldControl1.Height = 0.19F;
			this.fldControl1.Left = 6.5F;
			this.fldControl1.Name = "fldControl1";
			this.fldControl1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldControl1.Text = "Field1";
			this.fldControl1.Top = 0.15625F;
			this.fldControl1.Width = 1F;
			// 
			// fldControl2
			// 
			this.fldControl2.Height = 0.19F;
			this.fldControl2.Left = 6.5F;
			this.fldControl2.Name = "fldControl2";
			this.fldControl2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldControl2.Text = "Field1";
			this.fldControl2.Top = 0.3125F;
			this.fldControl2.Width = 1F;
			// 
			// fldControl3
			// 
			this.fldControl3.Height = 0.19F;
			this.fldControl3.Left = 6.5F;
			this.fldControl3.Name = "fldControl3";
			this.fldControl3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldControl3.Text = "Field1";
			this.fldControl3.Top = 0.46875F;
			this.fldControl3.Width = 1F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblOptions,
				this.lblDateRange
			});
			this.PageHeader.Height = 0.7395833F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Bill List";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.1875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblOptions
			// 
			this.lblOptions.Height = 0.1875F;
			this.lblOptions.HyperLink = null;
			this.lblOptions.Left = 0.375F;
			this.lblOptions.Name = "lblOptions";
			this.lblOptions.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblOptions.Text = "Bill List";
			this.lblOptions.Top = 0.40625F;
			this.lblOptions.Width = 7.625F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 1.5F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Bill List";
			this.lblDateRange.Top = 0.21875F;
			this.lblDateRange.Width = 5.1875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Binder,
				this.fldCustomer,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.fldCustomerNumber
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.9791667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 4.21875F;
			this.Binder.Name = "Binder";
			this.Binder.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.125F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.5625F;
			// 
			// fldCustomer
			// 
			this.fldCustomer.Height = 0.1875F;
			this.fldCustomer.Left = 0.46875F;
			this.fldCustomer.Name = "fldCustomer";
			this.fldCustomer.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldCustomer.Text = "Field1";
			this.fldCustomer.Top = 0.03125F;
			this.fldCustomer.Width = 3.59375F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 0.46875F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldAddress1.Text = "Field1";
			this.fldAddress1.Top = 0.21875F;
			this.fldAddress1.Width = 3.59375F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 0.46875F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldAddress2.Text = "Field1";
			this.fldAddress2.Top = 0.40625F;
			this.fldAddress2.Width = 3.59375F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 0.46875F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldAddress3.Text = "Field1";
			this.fldAddress3.Top = 0.59375F;
			this.fldAddress3.Width = 3.59375F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 0.46875F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldAddress4.Text = "Field1";
			this.fldAddress4.Top = 0.78125F;
			this.fldAddress4.Width = 3.59375F;
			// 
			// fldCustomerNumber
			// 
			this.fldCustomerNumber.Height = 0.1875F;
			this.fldCustomerNumber.Left = 0F;
			this.fldCustomerNumber.Name = "fldCustomerNumber";
			this.fldCustomerNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.fldCustomerNumber.Text = "0000";
			this.fldCustomerNumber.Top = 0.03125F;
			this.fldCustomerNumber.Width = 0.4375F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line3,
				this.Field3,
				this.fldCustomerTotal
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 2.09375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 2.53125F;
			this.Line3.X1 = 2.09375F;
			this.Line3.X2 = 4.625F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.19F;
			this.Field3.Left = 2.09375F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Field3.Text = "Customer Total:";
			this.Field3.Top = 0.03125F;
			this.Field3.Width = 1.5F;
			// 
			// fldCustomerTotal
			// 
			this.fldCustomerTotal.Height = 0.19F;
			this.fldCustomerTotal.Left = 3.625F;
			this.fldCustomerTotal.Name = "fldCustomerTotal";
			this.fldCustomerTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldCustomerTotal.Text = "Field1";
			this.fldCustomerTotal.Top = 0.03125F;
			this.fldCustomerTotal.Width = 1F;
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldInvoiceNumber,
				this.Label9,
				this.fldBillDate,
				this.InvoiceBinder
			});
			this.GroupHeader2.DataField = "InvoiceBinder";
			this.GroupHeader2.Height = 0.21875F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			// 
			// fldInvoiceNumber
			// 
			this.fldInvoiceNumber.Height = 0.1875F;
			this.fldInvoiceNumber.Left = 1.75F;
			this.fldInvoiceNumber.Name = "fldInvoiceNumber";
			this.fldInvoiceNumber.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldInvoiceNumber.Text = "Field1";
			this.fldInvoiceNumber.Top = 0.03125F;
			this.fldInvoiceNumber.Width = 0.875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.96875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label9.Text = "Invoice";
			this.Label9.Top = 0.03125F;
			this.Label9.Width = 0.71875F;
			// 
			// fldBillDate
			// 
			this.fldBillDate.Height = 0.1875F;
			this.fldBillDate.Left = 2.6875F;
			this.fldBillDate.Name = "fldBillDate";
			this.fldBillDate.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldBillDate.Text = "Field1";
			this.fldBillDate.Top = 0.03125F;
			this.fldBillDate.Width = 0.875F;
			// 
			// InvoiceBinder
			// 
			this.InvoiceBinder.DataField = "InvoiceBinder";
			this.InvoiceBinder.Height = 0.125F;
			this.InvoiceBinder.Left = 5.875F;
			this.InvoiceBinder.Name = "InvoiceBinder";
			this.InvoiceBinder.Text = "Field4";
			this.InvoiceBinder.Top = 0.03125F;
			this.InvoiceBinder.Visible = false;
			this.InvoiceBinder.Width = 0.90625F;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line2,
				this.Field1,
				this.fldInvoiceTotal
			});
			this.GroupFooter2.Height = 0.2291667F;
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.09375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 2.53125F;
			this.Line2.X1 = 2.09375F;
			this.Line2.X2 = 4.625F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.19F;
			this.Field1.Left = 2.09375F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Field1.Text = "Total:";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 1.5F;
			// 
			// fldInvoiceTotal
			// 
			this.fldInvoiceTotal.Height = 0.19F;
			this.fldInvoiceTotal.Left = 3.625F;
			this.fldInvoiceTotal.Name = "fldInvoiceTotal";
			this.fldInvoiceTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldInvoiceTotal.Text = "Field1";
			this.fldInvoiceTotal.Top = 0.03125F;
			this.fldInvoiceTotal.Width = 1F;
			// 
			// rptBillList
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptBillList_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            this.ReportEnd += new System.EventHandler(rptBillList_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.fldDescription1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReferenceDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl1Description)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl2Description)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl3Description)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.InvoiceBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReferenceDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl1Description;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl2Description;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl3Description;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl3;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOptions;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomerNumber;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomerTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox InvoiceBinder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceTotal;
	}
}
