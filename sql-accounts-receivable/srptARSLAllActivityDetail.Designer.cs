﻿namespace TWAR0000
{
	/// <summary>
	/// Summary description for srptARSLAllActivityDetail.
	/// </summary>
	partial class srptARSLAllActivityDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptARSLAllActivityDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldOriginalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOriginalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOriginalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnHeadTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldOriginalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnFooterTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDate,
				this.fldCode,
				this.fldRef,
				this.fldPrincipal,
				this.fldTotal,
				this.fldInterest,
				this.fldTax
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldOriginalTotal,
				this.fldOriginalPrincipal,
				this.fldOriginalInterest,
				this.lnHeadTotals,
				this.fldOriginalTax,
				this.fldHeaderTotal,
				this.fldHeaderPrin,
				this.fldHeaderInt,
				this.fldHeaderTax
			});
			this.ReportHeader.Height = 0.3854167F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalTotal,
				this.fldTotalPrincipal,
				this.lblFooter,
				this.lnFooterTotal,
				this.fldTotalInterest,
				this.fldTotalTax
			});
			this.ReportFooter.Height = 0.28125F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// fldOriginalTotal
			// 
			this.fldOriginalTotal.Height = 0.1875F;
			this.fldOriginalTotal.Left = 6F;
			this.fldOriginalTotal.Name = "fldOriginalTotal";
			this.fldOriginalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalTotal.Text = null;
			this.fldOriginalTotal.Top = 0.1875F;
			this.fldOriginalTotal.Width = 1.1875F;
			// 
			// fldOriginalPrincipal
			// 
			this.fldOriginalPrincipal.Height = 0.1875F;
			this.fldOriginalPrincipal.Left = 2.375F;
			this.fldOriginalPrincipal.Name = "fldOriginalPrincipal";
			this.fldOriginalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalPrincipal.Text = null;
			this.fldOriginalPrincipal.Top = 0.1875F;
			this.fldOriginalPrincipal.Width = 1.25F;
			// 
			// fldOriginalInterest
			// 
			this.fldOriginalInterest.Height = 0.1875F;
			this.fldOriginalInterest.Left = 4.8125F;
			this.fldOriginalInterest.Name = "fldOriginalInterest";
			this.fldOriginalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalInterest.Text = null;
			this.fldOriginalInterest.Top = 0.1875F;
			this.fldOriginalInterest.Width = 1.1875F;
			// 
			// lnHeadTotals
			// 
			this.lnHeadTotals.Height = 0F;
			this.lnHeadTotals.Left = 2.5F;
			this.lnHeadTotals.LineWeight = 1F;
			this.lnHeadTotals.Name = "lnHeadTotals";
			this.lnHeadTotals.Top = 0.375F;
			this.lnHeadTotals.Width = 4.6875F;
			this.lnHeadTotals.X1 = 2.5F;
			this.lnHeadTotals.X2 = 7.1875F;
			this.lnHeadTotals.Y1 = 0.375F;
			this.lnHeadTotals.Y2 = 0.375F;
			// 
			// fldOriginalTax
			// 
			this.fldOriginalTax.Height = 0.1875F;
			this.fldOriginalTax.Left = 3.625F;
			this.fldOriginalTax.Name = "fldOriginalTax";
			this.fldOriginalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalTax.Text = null;
			this.fldOriginalTax.Top = 0.1875F;
			this.fldOriginalTax.Width = 1.1875F;
			// 
			// fldHeaderTotal
			// 
			this.fldHeaderTotal.Height = 0.1875F;
			this.fldHeaderTotal.Left = 6F;
			this.fldHeaderTotal.Name = "fldHeaderTotal";
			this.fldHeaderTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderTotal.Text = "Total";
			this.fldHeaderTotal.Top = 0F;
			this.fldHeaderTotal.Width = 1.1875F;
			// 
			// fldHeaderPrin
			// 
			this.fldHeaderPrin.Height = 0.1875F;
			this.fldHeaderPrin.Left = 2.375F;
			this.fldHeaderPrin.Name = "fldHeaderPrin";
			this.fldHeaderPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderPrin.Text = "Principal";
			this.fldHeaderPrin.Top = 0F;
			this.fldHeaderPrin.Width = 1.25F;
			// 
			// fldHeaderInt
			// 
			this.fldHeaderInt.Height = 0.1875F;
			this.fldHeaderInt.Left = 4.8125F;
			this.fldHeaderInt.Name = "fldHeaderInt";
			this.fldHeaderInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderInt.Text = "Interest";
			this.fldHeaderInt.Top = 0F;
			this.fldHeaderInt.Width = 1.1875F;
			// 
			// fldHeaderTax
			// 
			this.fldHeaderTax.Height = 0.1875F;
			this.fldHeaderTax.Left = 3.625F;
			this.fldHeaderTax.Name = "fldHeaderTax";
			this.fldHeaderTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderTax.Text = "Tax";
			this.fldHeaderTax.Top = 0F;
			this.fldHeaderTax.Width = 1.1875F;
			// 
			// fldDate
			// 
			this.fldDate.CanGrow = false;
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0.0625F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 1F;
			// 
			// fldCode
			// 
			this.fldCode.CanGrow = false;
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 1.9375F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'";
			this.fldCode.Text = null;
			this.fldCode.Top = 0F;
			this.fldCode.Width = 0.375F;
			// 
			// fldRef
			// 
			this.fldRef.CanGrow = false;
			this.fldRef.Height = 0.1875F;
			this.fldRef.Left = 1.0625F;
			this.fldRef.Name = "fldRef";
			this.fldRef.Style = "font-family: \'Tahoma\'";
			this.fldRef.Text = null;
			this.fldRef.Top = 0F;
			this.fldRef.Width = 0.875F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.CanGrow = false;
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 2.375F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 1.25F;
			// 
			// fldTotal
			// 
			this.fldTotal.CanGrow = false;
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.1875F;
			// 
			// fldInterest
			// 
			this.fldInterest.CanGrow = false;
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 4.8125F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 1.1875F;
			// 
			// fldTax
			// 
			this.fldTax.CanGrow = false;
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 3.625F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 1.1875F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 6F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = null;
			this.fldTotalTotal.Top = 0F;
			this.fldTotalTotal.Width = 1.1875F;
			// 
			// fldTotalPrincipal
			// 
			this.fldTotalPrincipal.Height = 0.1875F;
			this.fldTotalPrincipal.Left = 2.375F;
			this.fldTotalPrincipal.Name = "fldTotalPrincipal";
			this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrincipal.Text = null;
			this.fldTotalPrincipal.Top = 0F;
			this.fldTotalPrincipal.Width = 1.25F;
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.1875F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 0.875F;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblFooter.Text = "Total";
			this.lblFooter.Top = 0F;
			this.lblFooter.Width = 1.4375F;
			// 
			// lnFooterTotal
			// 
			this.lnFooterTotal.Height = 0F;
			this.lnFooterTotal.Left = 2.5F;
			this.lnFooterTotal.LineWeight = 1F;
			this.lnFooterTotal.Name = "lnFooterTotal";
			this.lnFooterTotal.Top = 0F;
			this.lnFooterTotal.Width = 4.6875F;
			this.lnFooterTotal.X1 = 2.5F;
			this.lnFooterTotal.X2 = 7.1875F;
			this.lnFooterTotal.Y1 = 0F;
			this.lnFooterTotal.Y2 = 0F;
			// 
			// fldTotalInterest
			// 
			this.fldTotalInterest.Height = 0.1875F;
			this.fldTotalInterest.Left = 4.8125F;
			this.fldTotalInterest.Name = "fldTotalInterest";
			this.fldTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalInterest.Text = null;
			this.fldTotalInterest.Top = 0F;
			this.fldTotalInterest.Width = 1.1875F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 3.625F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTax.Text = null;
			this.fldTotalTax.Top = 0F;
			this.fldTotalTax.Width = 1.1875F;
			// 
			// srptARSLAllActivityDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeadTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderTax;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnFooterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
	}
}
