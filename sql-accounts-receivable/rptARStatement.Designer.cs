﻿namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptARStatement.
	/// </summary>
	partial class rptARStatement
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptARStatement));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransaction = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.imgIcon = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldCustomerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCustomerNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStatementDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReference = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.linReference = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver90 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinanceCharge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldRemittanceMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransaction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgIcon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatementDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinanceCharge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemittanceMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDate,
				this.fldInvoice,
				this.fldAmount,
				this.fldBalance,
				this.fldTransaction,
				this.fldReference
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "text-align: center";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.84375F;
			// 
			// fldInvoice
			// 
			this.fldInvoice.Height = 0.1875F;
			this.fldInvoice.Left = 0.9375F;
			this.fldInvoice.Name = "fldInvoice";
			this.fldInvoice.Style = "text-align: center";
			this.fldInvoice.Text = "Field1";
			this.fldInvoice.Top = 0F;
			this.fldInvoice.Width = 0.90625F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 5F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "text-align: right";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1F;
			// 
			// fldBalance
			// 
			this.fldBalance.Height = 0.1875F;
			this.fldBalance.Left = 6.15625F;
			this.fldBalance.Name = "fldBalance";
			this.fldBalance.Style = "text-align: right";
			this.fldBalance.Text = "Field1";
			this.fldBalance.Top = 0F;
			this.fldBalance.Width = 1F;
			// 
			// fldTransaction
			// 
			this.fldTransaction.Height = 0.1875F;
			this.fldTransaction.Left = 2.03125F;
			this.fldTransaction.Name = "fldTransaction";
			this.fldTransaction.Style = "text-align: left";
			this.fldTransaction.Text = "Field1";
			this.fldTransaction.Top = 0F;
			this.fldTransaction.Width = 1.71875F;
			// 
			// fldReference
			// 
			this.fldReference.Height = 0.1875F;
			this.fldReference.Left = 3.8125F;
			this.fldReference.Name = "fldReference";
			this.fldReference.Style = "text-align: left";
			this.fldReference.Text = "Field1";
			this.fldReference.Top = 0F;
			this.fldReference.Width = 1.125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.imgIcon,
				this.Label1
			});
			this.PageHeader.Height = 1.385417F;
			this.PageHeader.Name = "PageHeader";
			// 
			// imgIcon
			// 
			this.imgIcon.Height = 1.385417F;
			this.imgIcon.HyperLink = null;
			this.imgIcon.ImageData = null;
			this.imgIcon.Left = 3.291667F;
			this.imgIcon.LineWeight = 1F;
			this.imgIcon.Name = "imgIcon";
			this.imgIcon.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.imgIcon.Top = 0F;
			this.imgIcon.Width = 1.385417F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 5.5625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 16pt; font-weight: bold; text-align: center";
			this.Label1.Text = "STATEMENT";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 1.625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCustomerName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.Label2,
				this.Label3,
				this.fldCustomerNumber,
				this.fldStatementDate,
				this.Label4,
				this.Label5,
				this.Label7,
				this.Label8,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4,
				this.Line5,
				this.Line6,
				this.Line7,
				this.Line8,
				this.fldAddress4,
				this.Binder,
				this.Label6,
				this.lblReference,
				this.linReference
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 1.541667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// fldCustomerName
			// 
			this.fldCustomerName.Height = 0.19F;
			this.fldCustomerName.Left = 0.1875F;
			this.fldCustomerName.Name = "fldCustomerName";
			this.fldCustomerName.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldCustomerName.Tag = "Large";
			this.fldCustomerName.Text = "COUNTY OF HANCOCK";
			this.fldCustomerName.Top = 0.21875F;
			this.fldCustomerName.Width = 3.125F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.19F;
			this.fldAddress1.Left = 0.1875F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldAddress1.Tag = "Large";
			this.fldAddress1.Text = "50 STATE STREET, SUITE #8";
			this.fldAddress1.Top = 0.40625F;
			this.fldAddress1.Width = 3.125F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.19F;
			this.fldAddress2.Left = 0.1875F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldAddress2.Tag = "Large";
			this.fldAddress2.Text = "ELLSWORTH, ME 04605";
			this.fldAddress2.Top = 0.59375F;
			this.fldAddress2.Width = 3.125F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.19F;
			this.fldAddress3.Left = 0.1875F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldAddress3.Tag = "Large";
			this.fldAddress3.Text = "(207) 667-8272";
			this.fldAddress3.Top = 0.78125F;
			this.fldAddress3.Width = 3.125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 5.09375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 10pt; text-align: right";
			this.Label2.Text = "Customer #:";
			this.Label2.Top = 0.21875F;
			this.Label2.Width = 0.84375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 4.78125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 10pt; text-align: right";
			this.Label3.Text = "Statement Date:";
			this.Label3.Top = 0.59375F;
			this.Label3.Width = 1.15625F;
			// 
			// fldCustomerNumber
			// 
			this.fldCustomerNumber.Height = 0.1875F;
			this.fldCustomerNumber.Left = 6.0625F;
			this.fldCustomerNumber.Name = "fldCustomerNumber";
			this.fldCustomerNumber.Style = "text-align: left";
			this.fldCustomerNumber.Text = "Field1";
			this.fldCustomerNumber.Top = 0.21875F;
			this.fldCustomerNumber.Width = 0.84375F;
			// 
			// fldStatementDate
			// 
			this.fldStatementDate.Height = 0.1875F;
			this.fldStatementDate.Left = 6.0625F;
			this.fldStatementDate.Name = "fldStatementDate";
			this.fldStatementDate.Style = "text-align: left";
			this.fldStatementDate.Text = "Field1";
			this.fldStatementDate.Top = 0.59375F;
			this.fldStatementDate.Width = 0.84375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label4.Text = "Date";
			this.Label4.Top = 1.34375F;
			this.Label4.Width = 0.84375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.9375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label5.Text = "Invoice No.";
			this.Label5.Top = 1.34375F;
			this.Label5.Width = 0.90625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label7.Text = "Amount";
			this.Label7.Top = 1.34375F;
			this.Label7.Width = 1F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6.15625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.Label8.Text = "Balance";
			this.Label8.Top = 1.34375F;
			this.Label8.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.3125F;
			this.Line1.Width = 7.1875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.1875F;
			this.Line1.Y1 = 1.3125F;
			this.Line1.Y2 = 1.3125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.53125F;
			this.Line2.Width = 7.1875F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.1875F;
			this.Line2.Y1 = 1.53125F;
			this.Line2.Y2 = 1.53125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0.21875F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.3125F;
			this.Line3.Width = 0F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 0F;
			this.Line3.Y1 = 1.3125F;
			this.Line3.Y2 = 1.53125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0.21875F;
			this.Line4.Left = 0.90625F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.3125F;
			this.Line4.Width = 0F;
			this.Line4.X1 = 0.90625F;
			this.Line4.X2 = 0.90625F;
			this.Line4.Y1 = 1.3125F;
			this.Line4.Y2 = 1.53125F;
			// 
			// Line5
			// 
			this.Line5.Height = 0.21875F;
			this.Line5.Left = 1.9375F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.3125F;
			this.Line5.Width = 0F;
			this.Line5.X1 = 1.9375F;
			this.Line5.X2 = 1.9375F;
			this.Line5.Y1 = 1.3125F;
			this.Line5.Y2 = 1.53125F;
			// 
			// Line6
			// 
			this.Line6.Height = 0.21875F;
			this.Line6.Left = 4.90625F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1.3125F;
			this.Line6.Width = 0F;
			this.Line6.X1 = 4.90625F;
			this.Line6.X2 = 4.90625F;
			this.Line6.Y1 = 1.3125F;
			this.Line6.Y2 = 1.53125F;
			// 
			// Line7
			// 
			this.Line7.Height = 0.21875F;
			this.Line7.Left = 6.125F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 1.3125F;
			this.Line7.Width = 0F;
			this.Line7.X1 = 6.125F;
			this.Line7.X2 = 6.125F;
			this.Line7.Y1 = 1.3125F;
			this.Line7.Y2 = 1.53125F;
			// 
			// Line8
			// 
			this.Line8.Height = 0.21875F;
			this.Line8.Left = 7.1875F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 1.3125F;
			this.Line8.Width = 0F;
			this.Line8.X1 = 7.1875F;
			this.Line8.X2 = 7.1875F;
			this.Line8.Y1 = 1.3125F;
			this.Line8.Y2 = 1.53125F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.19F;
			this.fldAddress4.Left = 0.1875F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldAddress4.Tag = "Large";
			this.fldAddress4.Text = null;
			this.fldAddress4.Top = 0.96875F;
			this.fldAddress4.Width = 3.125F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.1875F;
			this.Binder.Left = 0F;
			this.Binder.Name = "Binder";
			this.Binder.Text = null;
			this.Binder.Top = 0F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.5F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 2.03125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 10pt; font-weight: bold; text-align: left";
			this.Label6.Text = "Transaction";
			this.Label6.Top = 1.34375F;
			this.Label6.Width = 1.71875F;
			// 
			// lblReference
			// 
			this.lblReference.Height = 0.1875F;
			this.lblReference.HyperLink = null;
			this.lblReference.Left = 3.8125F;
			this.lblReference.Name = "lblReference";
			this.lblReference.Style = "font-size: 10pt; font-weight: bold; text-align: left";
			this.lblReference.Text = "Ticket #";
			this.lblReference.Top = 1.34375F;
			this.lblReference.Width = 1.125F;
			// 
			// linReference
			// 
			this.linReference.Height = 0.21875F;
			this.linReference.Left = 3.78125F;
			this.linReference.LineWeight = 1F;
			this.linReference.Name = "linReference";
			this.linReference.Top = 1.3125F;
			this.linReference.Width = 0F;
			this.linReference.X1 = 3.78125F;
			this.linReference.X2 = 3.78125F;
			this.linReference.Y1 = 1.3125F;
			this.linReference.Y2 = 1.53125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape1,
				this.Line9,
				this.Line10,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.fldCurrent,
				this.fldOver30,
				this.fldOver60,
				this.fldOver90,
				this.fldOver120,
				this.fldFinanceCharge,
				this.Label16,
				this.fldTotal,
				this.Line11,
				this.Line12,
				this.Line13,
				this.Line14,
				this.Line15,
				this.Line16,
				this.Line17,
				this.Line18,
				this.fldRemittanceMessage,
				this.Line19
			});
			this.GroupFooter1.Height = 1.46875F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Shape1.Height = 0.21875F;
			this.Shape1.Left = 0F;
			this.Shape1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0F;
			this.Shape1.Width = 5.34375F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 0F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 0F;
			this.Line9.Width = 7.1875F;
			this.Line9.X1 = 0F;
			this.Line9.X2 = 7.1875F;
			this.Line9.Y1 = 0F;
			this.Line9.Y2 = 0F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 0F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 0.4375F;
			this.Line10.Width = 7.1875F;
			this.Line10.X1 = 0F;
			this.Line10.X2 = 7.1875F;
			this.Line10.Y1 = 0.4375F;
			this.Line10.Y2 = 0.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 8pt; font-weight: bold; text-align: center";
			this.Label9.Text = "Current";
			this.Label9.Top = 0.03125F;
			this.Label9.Width = 0.84375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.90625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 8pt; font-weight: bold; text-align: center";
			this.Label10.Text = "Over 30 Days";
			this.Label10.Top = 0.03125F;
			this.Label10.Width = 0.84375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.8125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 8pt; font-weight: bold; text-align: center";
			this.Label11.Text = "Over 60 Days";
			this.Label11.Top = 0.03125F;
			this.Label11.Width = 0.84375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 8pt; font-weight: bold; text-align: center";
			this.Label12.Text = "Over 90 Days";
			this.Label12.Top = 0.03125F;
			this.Label12.Width = 0.84375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.5625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8pt; font-weight: bold; text-align: center";
			this.Label13.Text = "Over 120 Days";
			this.Label13.Top = 0.03125F;
			this.Label13.Width = 0.84375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.5F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8pt; font-weight: bold; text-align: center";
			this.Label14.Text = "Finance Chgs.";
			this.Label14.Top = 0.03125F;
			this.Label14.Width = 0.84375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.40625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 8pt; font-weight: bold; text-align: center";
			this.Label15.Text = "Total";
			this.Label15.Top = 0.03125F;
			this.Label15.Width = 0.75F;
			// 
			// fldCurrent
			// 
			this.fldCurrent.Height = 0.1875F;
			this.fldCurrent.Left = 0F;
			this.fldCurrent.Name = "fldCurrent";
			this.fldCurrent.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.fldCurrent.Text = "1,000,000.00";
			this.fldCurrent.Top = 0.25F;
			this.fldCurrent.Width = 0.84375F;
			// 
			// fldOver30
			// 
			this.fldOver30.Height = 0.1875F;
			this.fldOver30.Left = 0.90625F;
			this.fldOver30.Name = "fldOver30";
			this.fldOver30.Style = "font-weight: bold; text-align: right";
			this.fldOver30.Text = "Field1";
			this.fldOver30.Top = 0.25F;
			this.fldOver30.Width = 0.84375F;
			// 
			// fldOver60
			// 
			this.fldOver60.Height = 0.1875F;
			this.fldOver60.Left = 1.8125F;
			this.fldOver60.Name = "fldOver60";
			this.fldOver60.Style = "font-weight: bold; text-align: right";
			this.fldOver60.Text = "Field3";
			this.fldOver60.Top = 0.25F;
			this.fldOver60.Width = 0.84375F;
			// 
			// fldOver90
			// 
			this.fldOver90.Height = 0.1875F;
			this.fldOver90.Left = 2.6875F;
			this.fldOver90.Name = "fldOver90";
			this.fldOver90.Style = "font-weight: bold; text-align: right";
			this.fldOver90.Text = "Field1";
			this.fldOver90.Top = 0.25F;
			this.fldOver90.Width = 0.84375F;
			// 
			// fldOver120
			// 
			this.fldOver120.Height = 0.1875F;
			this.fldOver120.Left = 3.59375F;
			this.fldOver120.Name = "fldOver120";
			this.fldOver120.Style = "font-weight: bold; text-align: right";
			this.fldOver120.Text = "Field5";
			this.fldOver120.Top = 0.25F;
			this.fldOver120.Width = 0.84375F;
			// 
			// fldFinanceCharge
			// 
			this.fldFinanceCharge.Height = 0.1875F;
			this.fldFinanceCharge.Left = 4.5F;
			this.fldFinanceCharge.Name = "fldFinanceCharge";
			this.fldFinanceCharge.Style = "font-weight: bold; text-align: right";
			this.fldFinanceCharge.Text = "Field1";
			this.fldFinanceCharge.Top = 0.25F;
			this.fldFinanceCharge.Width = 0.84375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 5.40625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 8pt; font-weight: bold; text-align: center";
			this.Label16.Text = "Balance Due:";
			this.Label16.Top = 0.25F;
			this.Label16.Width = 0.75F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.1875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-weight: bold; text-align: right";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0.25F;
			this.fldTotal.Width = 1F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 0F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 0.21875F;
			this.Line11.Width = 5.34375F;
			this.Line11.X1 = 0F;
			this.Line11.X2 = 5.34375F;
			this.Line11.Y1 = 0.21875F;
			this.Line11.Y2 = 0.21875F;
			// 
			// Line12
			// 
			this.Line12.Height = 0.4375F;
			this.Line12.Left = 0.875F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 0F;
			this.Line12.Width = 0F;
			this.Line12.X1 = 0.875F;
			this.Line12.X2 = 0.875F;
			this.Line12.Y1 = 0F;
			this.Line12.Y2 = 0.4375F;
			// 
			// Line13
			// 
			this.Line13.Height = 0.4375F;
			this.Line13.Left = 1.78125F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 0F;
			this.Line13.Width = 0F;
			this.Line13.X1 = 1.78125F;
			this.Line13.X2 = 1.78125F;
			this.Line13.Y1 = 0F;
			this.Line13.Y2 = 0.4375F;
			// 
			// Line14
			// 
			this.Line14.Height = 0.4375F;
			this.Line14.Left = 2.65625F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 0F;
			this.Line14.Width = 0F;
			this.Line14.X1 = 2.65625F;
			this.Line14.X2 = 2.65625F;
			this.Line14.Y1 = 0F;
			this.Line14.Y2 = 0.4375F;
			// 
			// Line15
			// 
			this.Line15.Height = 0.4375F;
			this.Line15.Left = 3.53125F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 0F;
			this.Line15.Width = 0F;
			this.Line15.X1 = 3.53125F;
			this.Line15.X2 = 3.53125F;
			this.Line15.Y1 = 0F;
			this.Line15.Y2 = 0.4375F;
			// 
			// Line16
			// 
			this.Line16.Height = 0.4375F;
			this.Line16.Left = 4.4375F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 0F;
			this.Line16.Width = 0F;
			this.Line16.X1 = 4.4375F;
			this.Line16.X2 = 4.4375F;
			this.Line16.Y1 = 0F;
			this.Line16.Y2 = 0.4375F;
			// 
			// Line17
			// 
			this.Line17.Height = 0.4375F;
			this.Line17.Left = 5.34375F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 0F;
			this.Line17.Width = 0F;
			this.Line17.X1 = 5.34375F;
			this.Line17.X2 = 5.34375F;
			this.Line17.Y1 = 0F;
			this.Line17.Y2 = 0.4375F;
			// 
			// Line18
			// 
			this.Line18.Height = 0.4375F;
			this.Line18.Left = 7.1875F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 0F;
			this.Line18.Width = 0F;
			this.Line18.X1 = 7.1875F;
			this.Line18.X2 = 7.1875F;
			this.Line18.Y1 = 0F;
			this.Line18.Y2 = 0.4375F;
			// 
			// fldRemittanceMessage
			// 
			this.fldRemittanceMessage.Height = 0.875F;
			this.fldRemittanceMessage.Left = 0F;
			this.fldRemittanceMessage.Name = "fldRemittanceMessage";
			this.fldRemittanceMessage.Text = "Field8";
			this.fldRemittanceMessage.Top = 0.59375F;
			this.fldRemittanceMessage.Width = 7.1875F;
			// 
			// Line19
			// 
			this.Line19.Height = 0.4375F;
			this.Line19.Left = 0F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 0F;
			this.Line19.Width = 0F;
			this.Line19.X1 = 0F;
			this.Line19.X2 = 0F;
			this.Line19.Y1 = 0F;
			this.Line19.Y2 = 0.4375F;
			// 
			// rptARStatement
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptARStatement_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransaction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgIcon)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatementDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinanceCharge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemittanceMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransaction;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgIcon;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomerNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatementDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReference;
		private GrapeCity.ActiveReports.SectionReportModel.Line linReference;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver60;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver90;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver120;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinanceCharge;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRemittanceMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
	}
}
