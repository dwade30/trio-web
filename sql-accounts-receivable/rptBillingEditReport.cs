﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptBillingEditReport.
	/// </summary>
	public partial class rptBillingEditReport : BaseSectionReport
	{
        public int BillMonth { get; set; }
        public int BillYear { get; set; }
		public static rptBillingEditReport InstancePtr
		{
			get
			{
				return (rptBillingEditReport)Sys.GetInstance(typeof(rptBillingEditReport));
			}
		}

		protected rptBillingEditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		bool blnFirstRecord;
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsTypeInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curTotals As Decimal	OnWriteFCConvert.ToInt16(
		Decimal[] curTotals = new Decimal[7 + 1];
		// vbPorter upgrade warning: curGrandTotals As Decimal	OnWrite(short, Decimal)
		Decimal[] curGrandTotals = new Decimal[7 + 1];
		int intCurrentField;
		int intControlFields;
		int intFields;
		int intTotalFields;
		bool blnFee1;
		bool blnFee2;
		bool blnFee3;
		bool blnFee4;
		bool blnFee5;
		bool blnFee6;
		bool blnReference;
		bool blnControl1;
		bool blnControl2;
		bool blnControl3;

		public rptBillingEditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Billing Edit Report";
            this.ReportEnd += RptBillingEditReport_ReportEnd;
		}

        private void RptBillingEditReport_ReportEnd(object sender, EventArgs e)
        {
            rsInfo.DisposeOf();
			rsTypeInfo.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			for (counter = 0; counter <= 7; counter++)
			{
				curTotals[counter] = 0;
				curGrandTotals[counter] = 0;
			}
			rsTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(frmBillPrep.InstancePtr.intBillType));
			//FC:FINAL:SBE - #418 - in VB6 form is loaded when you access any property from it. Force form load
			//frmBillSelect.InstancePtr.LoadForm();                      

            lblBillInfo.Text = "Bill Period: " + Strings.Format(BillMonth + @"/" + BillYear, "MMMM") + " " + BillYear + "          " + "Bill Type: " + rsTypeInfo.Get_Fields_String("TypeTitle");
			FormatFields();
			rsInfo.OpenRecordset("SELECT b.*, c.CustomerID as CustomerID, c.SalesTax as SalesTax, p.* FROM CustomerBills as b INNER JOIN CustomerMaster as c ON b.CustomerID = c.CustomerID CROSS APPLY " + rsInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.PartyID) as p WHERE Exclude = 0 AND Status = 'A' AND Type = " + FCConvert.ToString(frmBillPrep.InstancePtr.intBillType) + " ORDER BY p.FullName");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: curLineTotal As Decimal	OnWrite(short, Decimal)
			Decimal curLineTotal;
			int intCurrentTotal;
			intCurrentTotal = 0;
			intCurrentField = 1;
			curLineTotal = 0;
			fldCustomerID.Text = FCConvert.ToString(rsInfo.Get_Fields_Int32("CustomerID"));
			fldName.Text = rsInfo.Get_Fields_String("FullName");
			if (blnReference)
			{
				SetDetailField_6(intCurrentField, rsInfo.Get_Fields_String("Reference"));
				intCurrentField += 1;
			}
			if (blnControl1)
			{
				SetDetailField_6(intCurrentField, rsInfo.Get_Fields_String("Control1"));
				intCurrentField += 1;
			}
			if (blnControl2)
			{
				SetDetailField_6(intCurrentField, rsInfo.Get_Fields_String("Control2"));
				intCurrentField += 1;
			}
			if (blnControl3)
			{
				SetDetailField_6(intCurrentField, rsInfo.Get_Fields_String("Control3"));
				intCurrentField += 1;
			}
			// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount1")) >= 0)
			{
				// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				fldUnitCost1.Text = Strings.Format(rsInfo.Get_Fields("Amount1"), "#,##0.00");
				fldQuantity1.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity1"), "#,##0.00");
				// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				fldAmount1.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount1") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity1")), 2)), "#,##0.00");
				// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				curGrandTotals[0] += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount1") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity1")), 2)));
				// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount1") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity1")), 2)));
			}
			else
			{
				fldUnitCost1.Text = Strings.Format(rsTypeInfo.Get_Fields_Double("DefaultAmount1"), "#,##0.00");
				fldQuantity1.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity1"), "#,##0.00");
				fldAmount1.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount1") * rsInfo.Get_Fields_Double("Quantity1"), 2)), "#,##0.00");
				curGrandTotals[0] += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount1") * rsInfo.Get_Fields_Double("Quantity1"), 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount1") * rsInfo.Get_Fields_Double("Quantity1"), 2)));
			}
			// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount2")) >= 0)
			{
				// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
				fldUnitCost2.Text = Strings.Format(rsInfo.Get_Fields("Amount2"), "#,##0.00");
				fldQuantity2.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity2"), "#,##0.00");
				// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
				fldAmount2.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount2") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity2")), 2)), "#,##0.00");
				// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
				curGrandTotals[1] += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount2") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity2")), 2)));
				// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount2") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity2")), 2)));
			}
			else
			{
				fldUnitCost2.Text = Strings.Format(rsTypeInfo.Get_Fields_Double("DefaultAmount2"), "#,##0.00");
				fldQuantity2.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity2"), "#,##0.00");
				fldAmount2.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount2") * rsInfo.Get_Fields_Double("Quantity2"), 2)), "#,##0.00");
				curGrandTotals[1] += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount2") * rsInfo.Get_Fields_Double("Quantity2"), 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount2") * rsInfo.Get_Fields_Double("Quantity2"), 2)));
			}
			// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount3")) >= 0)
			{
				// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
				fldUnitCost3.Text = Strings.Format(rsInfo.Get_Fields("Amount3"), "#,##0.00");
				fldQuantity3.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity3"), "#,##0.00");
				// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
				fldAmount3.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount3") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity3")), 2)), "#,##0.00");
				// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
				curGrandTotals[2] += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount3") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity3")), 2)));
				// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount3") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity3")), 2)));
			}
			else
			{
				fldUnitCost3.Text = Strings.Format(rsTypeInfo.Get_Fields_Double("DefaultAmount3"), "#,##0.00");
				fldQuantity3.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity3"), "#,##0.00");
				fldAmount3.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount3") * rsInfo.Get_Fields_Double("Quantity3"), 2)), "#,##0.00");
				curGrandTotals[2] += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount3") * rsInfo.Get_Fields_Double("Quantity3"), 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount3") * rsInfo.Get_Fields_Double("Quantity3"), 2)));
			}
			// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount4")) >= 0)
			{
				// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
				fldUnitCost4.Text = Strings.Format(rsInfo.Get_Fields("Amount4"), "#,##0.00");
				fldQuantity4.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity4"), "#,##0.00");
				// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
				fldAmount4.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount4") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity4")), 2)), "#,##0.00");
				// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
				curGrandTotals[3] += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount4") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity4")), 2)));
				// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount4") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity4")), 2)));
			}
			else
			{
				fldUnitCost4.Text = Strings.Format(rsTypeInfo.Get_Fields_Double("DefaultAmount4"), "#,##0.00");
				fldQuantity4.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity4"), "#,##0.00");
				fldAmount4.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount4") * rsInfo.Get_Fields_Double("Quantity4"), 2)), "#,##0.00");
				curGrandTotals[3] += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount4") * rsInfo.Get_Fields_Double("Quantity4"), 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount4") * rsInfo.Get_Fields_Double("Quantity4"), 2)));
			}
			// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount5")) >= 0)
			{
				fldUnitCost5.Text = Strings.Format(rsInfo.Get_Fields("Amount5"), "#,##0.00");
				fldQuantity5.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity5"), "#,##0.00");
				fldAmount5.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount5") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity5")), 2)), "#,##0.00");
				curGrandTotals[4] += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount5") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity5")), 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount5") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity5")), 2)));
			}
			else
			{
				fldUnitCost5.Text = Strings.Format(rsTypeInfo.Get_Fields_Double("DefaultAmount5"), "#,##0.00");
				fldQuantity5.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity5"), "#,##0.00");
				fldAmount5.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount5") * rsInfo.Get_Fields_Double("Quantity5"), 2)), "#,##0.00");
				curGrandTotals[4] += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount5") * rsInfo.Get_Fields_Double("Quantity5"), 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount5") * rsInfo.Get_Fields_Double("Quantity5"), 2)));
			}
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount6")) >= 0)
			{
				fldUnitCost6.Text = Strings.Format(rsInfo.Get_Fields("Amount6"), "#,##0.00");
				fldQuantity6.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity6"), "#,##0.00");
				fldAmount6.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount6") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity6")), 2)), "#,##0.00");
				curGrandTotals[5] += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount6") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity6")), 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsInfo.Get_Fields("Amount6") * FCConvert.ToDecimal(rsInfo.Get_Fields_Double("Quantity6")), 2)));
			}
			else
			{
				fldUnitCost6.Text = Strings.Format(rsTypeInfo.Get_Fields_Double("DefaultAmount6"), "#,##0.00");
				fldQuantity6.Text = Strings.Format(rsInfo.Get_Fields_Double("Quantity6"), "#,##0.00");
				fldAmount6.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount6") * rsInfo.Get_Fields_Double("Quantity6"), 2)), "#,##0.00");
				curGrandTotals[5] += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount6") * rsInfo.Get_Fields_Double("Quantity6"), 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(rsTypeInfo.Get_Fields_Double("DefaultAmount6") * rsInfo.Get_Fields_Double("Quantity6"), 2)));
			}
			if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("SalesTax")) && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("SalesTax")))
			{
				fldSalesTax.Text = Strings.Format(FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(curLineTotal) * modGlobal.Statics.dblSalesTax, 2)), "#,##0.00");
				curGrandTotals[6] += (FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(curLineTotal) * modGlobal.Statics.dblSalesTax, 2)));
				curLineTotal += (FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(curLineTotal) * modGlobal.Statics.dblSalesTax, 2)));
			}
			else
			{
				fldSalesTax.Text = Strings.Format(0, "#,##0.00");
			}
			fldInvoiceTotal.Text = Strings.Format(curLineTotal, "#,##0.00");
			curGrandTotals[7] += curLineTotal;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void FormatFields()
		{
			int counter;
			float lngTop;
			float lngTotalTop;
			// the 2 is for sales tax and total fields
			intControlFields = 0;
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Reference"))) != "" && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("ReferenceChanges")))
			{
				blnReference = true;
				intControlFields += 1;
				SetRefControlLabelField_24(intControlFields, Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Reference"))), "L");
			}
			else
			{
				blnReference = false;
			}
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control1"))) != "" && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control1Changes")))
			{
				blnControl1 = true;
				intControlFields += 1;
				SetRefControlLabelField_24(intControlFields, Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control1"))), "L");
			}
			else
			{
				blnControl1 = false;
			}
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control2"))) != "" && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control2Changes")))
			{
				blnControl2 = true;
				intControlFields += 1;
				SetRefControlLabelField_24(intControlFields, Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control2"))), "L");
			}
			else
			{
				blnControl2 = false;
			}
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control3"))) != "" && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control3Changes")))
			{
				blnControl3 = true;
				intControlFields += 1;
				SetRefControlLabelField_24(intControlFields, Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control3"))), "L");
			}
			else
			{
				blnControl3 = false;
			}
			switch (intControlFields)
			{
				case 0:
					{
						lblTitle1.Visible = false;
						fldDetail1.Visible = false;
						lblTitle2.Visible = false;
						fldDetail2.Visible = false;
						lblTitle3.Visible = false;
						fldDetail3.Visible = false;
						lblTitle4.Visible = false;
						fldDetail4.Visible = false;
						break;
					}
				case 1:
					{
						lblTitle2.Visible = false;
						fldDetail2.Visible = false;
						lblTitle3.Visible = false;
						fldDetail3.Visible = false;
						lblTitle4.Visible = false;
						fldDetail4.Visible = false;
						break;
					}
				case 2:
					{
						lblTitle3.Visible = false;
						fldDetail3.Visible = false;
						lblTitle4.Visible = false;
						fldDetail4.Visible = false;
						break;
					}
				case 3:
					{
						lblTitle4.Visible = false;
						fldDetail4.Visible = false;
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
			lngTop = fldAmount1.Top;
			lngTotalTop = fldTotalAmount1.Top;
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title1"))) != "")
			{
				lblFeeDescription1.Top = lngTop;
				fldAmount1.Top = lngTop;
				fldQuantity1.Top = lngTop;
				fldUnitCost1.Top = lngTop;
				lblFeeDescription1.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title1")));
				lngTop += lblFeeDescription1.Height;
				lblTotalFeeDescription1.Top = lngTotalTop;
				fldTotalAmount1.Top = lngTotalTop;
				lblTotalFeeDescription1.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title1")));
				lngTotalTop += lblTotalFeeDescription1.Height;
			}
			else
			{
				lblFeeDescription1.Visible = false;
				fldAmount1.Visible = false;
				fldQuantity1.Visible = false;
				fldUnitCost1.Visible = false;
				lblTotalFeeDescription1.Visible = false;
				fldTotalAmount1.Visible = false;
			}
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title2"))) != "")
			{
				lblFeeDescription2.Top = lngTop;
				fldAmount2.Top = lngTop;
				fldQuantity2.Top = lngTop;
				fldUnitCost2.Top = lngTop;
				lblFeeDescription2.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title2")));
				lngTop += lblFeeDescription2.Height;
				lblTotalFeeDescription2.Top = lngTotalTop;
				fldTotalAmount2.Top = lngTotalTop;
				lblTotalFeeDescription2.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title2")));
				lngTotalTop += lblTotalFeeDescription2.Height;
			}
			else
			{
				lblFeeDescription2.Visible = false;
				fldAmount2.Visible = false;
				fldQuantity2.Visible = false;
				fldUnitCost2.Visible = false;
				lblTotalFeeDescription2.Visible = false;
				fldTotalAmount2.Visible = false;
			}
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title3"))) != "")
			{
				lblFeeDescription3.Top = lngTop;
				fldAmount3.Top = lngTop;
				fldQuantity3.Top = lngTop;
				fldUnitCost3.Top = lngTop;
				lblFeeDescription3.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title3")));
				lngTop += lblFeeDescription3.Height;
				lblTotalFeeDescription3.Top = lngTotalTop;
				fldTotalAmount3.Top = lngTotalTop;
				lblTotalFeeDescription3.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title3")));
				lngTotalTop += lblTotalFeeDescription3.Height;
			}
			else
			{
				lblFeeDescription3.Visible = false;
				fldAmount3.Visible = false;
				fldQuantity3.Visible = false;
				fldUnitCost3.Visible = false;
				lblTotalFeeDescription3.Visible = false;
				fldTotalAmount3.Visible = false;
			}
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title4"))) != "")
			{
				lblFeeDescription4.Top = lngTop;
				fldAmount4.Top = lngTop;
				fldQuantity4.Top = lngTop;
				fldUnitCost4.Top = lngTop;
				lblFeeDescription4.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title4")));
				lngTop += lblFeeDescription4.Height;
				lblTotalFeeDescription4.Top = lngTotalTop;
				fldTotalAmount4.Top = lngTotalTop;
				lblTotalFeeDescription4.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title4")));
				lngTotalTop += lblTotalFeeDescription4.Height;
			}
			else
			{
				lblFeeDescription4.Visible = false;
				fldAmount4.Visible = false;
				fldQuantity4.Visible = false;
				fldUnitCost4.Visible = false;
				lblTotalFeeDescription4.Visible = false;
				fldTotalAmount4.Visible = false;
			}
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title5"))) != "")
			{
				lblFeeDescription5.Top = lngTop;
				fldAmount5.Top = lngTop;
				fldQuantity5.Top = lngTop;
				fldUnitCost5.Top = lngTop;
				lblFeeDescription5.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title5")));
				lngTop += lblFeeDescription5.Height;
				lblTotalFeeDescription5.Top = lngTotalTop;
				fldTotalAmount5.Top = lngTotalTop;
				lblTotalFeeDescription5.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title5")));
				lngTotalTop += lblTotalFeeDescription5.Height;
			}
			else
			{
				lblFeeDescription5.Visible = false;
				fldAmount5.Visible = false;
				fldQuantity5.Visible = false;
				fldUnitCost5.Visible = false;
				lblTotalFeeDescription5.Visible = false;
				fldTotalAmount5.Visible = false;
			}
			if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title6"))) != "")
			{
				lblFeeDescription6.Top = lngTop;
				fldAmount6.Top = lngTop;
				fldQuantity6.Top = lngTop;
				fldUnitCost6.Top = lngTop;
				lblFeeDescription6.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title6")));
				lngTop += lblFeeDescription6.Height;
				lblTotalFeeDescription6.Top = lngTotalTop;
				fldTotalAmount6.Top = lngTotalTop;
				lblTotalFeeDescription6.Text = Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title6")));
				lngTotalTop += lblTotalFeeDescription6.Height;
			}
			else
			{
				lblFeeDescription6.Visible = false;
				fldAmount6.Visible = false;
				fldQuantity6.Visible = false;
				fldUnitCost6.Visible = false;
				lblTotalFeeDescription6.Visible = false;
				fldTotalAmount6.Visible = false;
			}
			lblSalesTax.Top = lngTop;
			fldSalesTax.Top = lngTop;
			lngTop += fldSalesTax.Height;
			linInvoiceTotal.Y1 = lngTop;
			linInvoiceTotal.Y2 = lngTop;
			lblInvoiceTotal.Top = lngTop + 90 / 1440f;
			fldInvoiceTotal.Top = lngTop + 90 / 1440f;
			lblTotalSalesTax.Top = lngTotalTop;
			fldTotalSalesTax.Top = lngTotalTop;
			lngTotalTop += fldTotalSalesTax.Height;
			linGrandTotal.Y1 = lngTotalTop;
			linGrandTotal.Y2 = lngTotalTop;
			lblGrandTotal.Top = lngTotalTop + 90 / 1440f;
			fldGrandTotal.Top = lngTotalTop + 90 / 1440f;
		}
		// vbPorter upgrade warning: intField As short	OnWriteFCConvert.ToInt32(
		private void SetRefControlLabelField_24(int intField, string strText, string strAlign)
		{
			SetRefControlLabelField(ref intField, ref strText, ref strAlign);
		}

		private void SetRefControlLabelField(ref int intField, ref string strText, ref string strAlign)
		{
			switch (intField)
			{
				case 1:
					{
						lblTitle1.Text = strText;
						if (strAlign == "L")
						{
							lblTitle1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
							fldDetail1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
						}
						else
						{
							lblTitle1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
							fldDetail1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
						}
						break;
					}
				case 2:
					{
						lblTitle2.Text = strText;
						if (strAlign == "L")
						{
							lblTitle2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
							fldDetail2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
						}
						else
						{
							lblTitle2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
							fldDetail2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
						}
						break;
					}
				case 3:
					{
						lblTitle3.Text = strText;
						if (strAlign == "L")
						{
							lblTitle3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
							fldDetail3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
						}
						else
						{
							lblTitle3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
							fldDetail3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
						}
						break;
					}
				case 4:
					{
						lblTitle4.Text = strText;
						if (strAlign == "L")
						{
							lblTitle4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
							fldDetail4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
						}
						else
						{
							lblTitle4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
							fldDetail4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
						}
						break;
					}
			}
			//end switch
		}
		// vbPorter upgrade warning: intField As short	OnWriteFCConvert.ToInt32(
		private void SetDetailField_6(int intField, string strText)
		{
			SetDetailField(ref intField, ref strText);
		}

		private void SetDetailField(ref int intField, ref string strText)
		{
			switch (intField)
			{
				case 1:
					{
						fldDetail1.Text = strText;
						break;
					}
				case 2:
					{
						fldDetail2.Text = strText;
						break;
					}
				case 3:
					{
						fldDetail3.Text = strText;
						break;
					}
				case 4:
					{
						fldDetail4.Text = strText;
						break;
					}
			}
			//end switch
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalAmount1.Text = Strings.Format(curGrandTotals[0], "#,##0.00");
			fldTotalAmount2.Text = Strings.Format(curGrandTotals[1], "#,##0.00");
			fldTotalAmount3.Text = Strings.Format(curGrandTotals[2], "#,##0.00");
			fldTotalAmount4.Text = Strings.Format(curGrandTotals[3], "#,##0.00");
			fldTotalAmount5.Text = Strings.Format(curGrandTotals[4], "#,##0.00");
			fldTotalAmount6.Text = Strings.Format(curGrandTotals[5], "#,##0.00");
			fldTotalSalesTax.Text = Strings.Format(curGrandTotals[6], "#,##0.00");
			fldGrandTotal.Text = Strings.Format(curGrandTotals[7], "#,##0.00");
		}

		private void rptBillingEditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBillingEditReport.Caption	= "Billing Edit Report";
			//rptBillingEditReport.Icon	= "rptBillingEditReport.dsx":0000";
			//rptBillingEditReport.Left	= 0;
			//rptBillingEditReport.Top	= 0;
			//rptBillingEditReport.Width	= 20280;
			//rptBillingEditReport.Height	= 11115;
			//rptBillingEditReport.StartUpPosition	= 3;
			//rptBillingEditReport.SectionData	= "rptBillingEditReport.dsx":508A;
			//End Unmaped Properties
		}

		private void rptBillingEditReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
