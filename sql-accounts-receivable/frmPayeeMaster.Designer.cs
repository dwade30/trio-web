﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmPayeeMaster.
	/// </summary>
	partial class frmPayeeMaster : BaseForm
	{
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCRichTextBox txtRemittanceMessage;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCFrame fraTownSeal;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCPictureBox imgSignature;
		public fecherFoundation.FCLabel lblVoterNumber;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPayeeMaster));
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtRemittanceMessage = new fecherFoundation.FCRichTextBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.fraTownSeal = new fecherFoundation.FCFrame();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdBrowse = new fecherFoundation.FCButton();
            this.Shape1 = new fecherFoundation.FCLabel();
            this.imgSignature = new fecherFoundation.FCPictureBox();
            this.lblVoterNumber = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.btnProcessNextEntry = new fecherFoundation.FCButton();
            this.btnProcessPreviousEntry = new fecherFoundation.FCButton();
            this.btnFileDelete = new fecherFoundation.FCButton();
            this.btnProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemittanceMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownSeal)).BeginInit();
            this.fraTownSeal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessNextEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessPreviousEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 500);
            this.BottomPanel.Size = new System.Drawing.Size(838, 96);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.fraTownSeal);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(838, 440);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnFileDelete);
            this.TopPanel.Controls.Add(this.btnProcessPreviousEntry);
            this.TopPanel.Controls.Add(this.btnProcessNextEntry);
            this.TopPanel.Controls.Add(this.lblVoterNumber);
            this.TopPanel.Size = new System.Drawing.Size(838, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblVoterNumber, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessNextEntry, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessPreviousEntry, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(102, 30);
            this.HeaderText.Text = "Payee #";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtRemittanceMessage);
            this.Frame3.Location = new System.Drawing.Point(30, 126);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(400, 246);
            this.Frame3.TabIndex = 4;
            this.Frame3.Text = "Remittance Message";
            // 
            // txtRemittanceMessage
            // 
            this.txtRemittanceMessage.Location = new System.Drawing.Point(20, 30);
            this.txtRemittanceMessage.MaxLength = 255;
            this.txtRemittanceMessage.Name = "txtRemittanceMessage";
            this.txtRemittanceMessage.Size = new System.Drawing.Size(356, 197);
            this.txtRemittanceMessage.TabIndex = 5;
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(180, 30);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(250, 40);
            this.txtDescription.TabIndex = 3;
            // 
            // fraTownSeal
            // 
            this.fraTownSeal.Controls.Add(this.cmdClear);
            this.fraTownSeal.Controls.Add(this.cmdBrowse);
            this.fraTownSeal.Controls.Add(this.Shape1);
            this.fraTownSeal.Controls.Add(this.imgSignature);
            this.fraTownSeal.Location = new System.Drawing.Point(504, 30);
            this.fraTownSeal.Name = "fraTownSeal";
            this.fraTownSeal.Size = new System.Drawing.Size(303, 342);
            this.fraTownSeal.TabIndex = 6;
            this.fraTownSeal.Text = "Payee Logo";
            // 
            // cmdClear
            // 
            this.cmdClear.AppearanceKey = "actionButton";
            this.cmdClear.Location = new System.Drawing.Point(158, 270);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(79, 49);
            this.cmdClear.TabIndex = 7;
            this.cmdClear.Text = "Clear";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.AppearanceKey = "actionButton";
            this.cmdBrowse.Location = new System.Drawing.Point(66, 270);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(79, 49);
            this.cmdBrowse.TabIndex = 8;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
            // 
            // Shape1
            // 
            this.Shape1.BorderStyle = 1;
            this.Shape1.Location = new System.Drawing.Point(28, 30);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(248, 234);
            this.Shape1.TabIndex = 9;
            this.Shape1.Visible = false;
            // 
            // imgSignature
            // 
            this.imgSignature.Image = ((System.Drawing.Image)(resources.GetObject("imgSignature.Image")));
            this.imgSignature.Location = new System.Drawing.Point(28, 30);
            this.imgSignature.Name = "imgSignature";
            this.imgSignature.Size = new System.Drawing.Size(147, 123);
            this.imgSignature.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // lblVoterNumber
            // 
            this.lblVoterNumber.AppearanceKey = "Header";
            this.lblVoterNumber.Font = new System.Drawing.Font("Proxima Nova Regular", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblVoterNumber.Location = new System.Drawing.Point(138, 26);
            this.lblVoterNumber.Name = "lblVoterNumber";
            this.lblVoterNumber.Size = new System.Drawing.Size(102, 30);
            this.lblVoterNumber.TabIndex = 7;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(80, 16);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "DESCRIPTION";
            // 
            // btnProcessNextEntry
            // 
            this.btnProcessNextEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessNextEntry.Location = new System.Drawing.Point(742, 29);
            this.btnProcessNextEntry.Name = "btnProcessNextEntry";
            this.btnProcessNextEntry.Shortcut = Wisej.Web.Shortcut.F8;
            this.btnProcessNextEntry.Size = new System.Drawing.Size(83, 25);
            this.btnProcessNextEntry.TabIndex = 2;
            this.btnProcessNextEntry.Text = "Next Payee";
            this.btnProcessNextEntry.Click += new System.EventHandler(this.mnuProcessNextEntry_Click);
            // 
            // btnProcessPreviousEntry
            // 
            this.btnProcessPreviousEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessPreviousEntry.Location = new System.Drawing.Point(626, 29);
            this.btnProcessPreviousEntry.Name = "btnProcessPreviousEntry";
            this.btnProcessPreviousEntry.Shortcut = Wisej.Web.Shortcut.F7;
            this.btnProcessPreviousEntry.Size = new System.Drawing.Size(111, 25);
            this.btnProcessPreviousEntry.TabIndex = 3;
            this.btnProcessPreviousEntry.Text = "Previous Payee";
            this.btnProcessPreviousEntry.Click += new System.EventHandler(this.mnuProcessPreviousEntry_Click);
            // 
            // btnFileDelete
            // 
            this.btnFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileDelete.Enabled = false;
            this.btnFileDelete.Location = new System.Drawing.Point(565, 29);
            this.btnFileDelete.Name = "btnFileDelete";
            this.btnFileDelete.Shortcut = Wisej.Web.Shortcut.F4;
            this.btnFileDelete.Size = new System.Drawing.Size(56, 24);
            this.btnFileDelete.TabIndex = 4;
            this.btnFileDelete.Text = "Delete";
            this.btnFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.AppearanceKey = "acceptButton";
            this.btnProcessSave.Location = new System.Drawing.Point(325, 30);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcessSave.Size = new System.Drawing.Size(100, 48);
            this.btnProcessSave.Text = "Save";
            this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmPayeeMaster
            // 
            this.ClientSize = new System.Drawing.Size(838, 596);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPayeeMaster";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Payees";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmPayeeMaster_Load);
            this.Activated += new System.EventHandler(this.frmPayeeMaster_Activated);
            this.Resize += new System.EventHandler(this.frmPayeeMaster_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPayeeMaster_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemittanceMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownSeal)).EndInit();
            this.fraTownSeal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessNextEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessPreviousEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
		internal FCButton btnProcessNextEntry;
		internal FCButton btnProcessPreviousEntry;
		internal FCButton btnFileDelete;
		internal FCButton btnProcessSave;
	}
}
