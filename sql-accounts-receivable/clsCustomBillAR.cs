﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	public class clsCustomBillAR
	{
		//=========================================================
		// This is set up so that you can have as many classes
		// as you want per module.  You do not have to handle
		// all custom bill/reports the same way
		// You can have multiple classes, or just one class
		// that alters its behaviour based on a variable
		// that determines which type you are doing.
		// The properties and functions are set up to be used
		// like recordsets from the report.  They do not have
		// to actually work this way.  EndOfFile could return
		// clsreport.endoffile, or it could return whether an
		// index is pointing past the end of an array or grid
		// Any functions or variables can be changed or added
		// as long as the interface that the report expects
		// is unchanged. I.E. add as much as you want or change an existing function that
		// the report calls, but don't rename or delete it.
		private string strSQLReport = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper clsReport = new clsDRWrapper();
		private clsDRWrapper clsReport_AutoInitialized;

		private clsDRWrapper clsReport
		{
			get
			{
				if (clsReport_AutoInitialized == null)
				{
					clsReport_AutoInitialized = new clsDRWrapper();
				}
				return clsReport_AutoInitialized;
			}
			set
			{
				clsReport_AutoInitialized = value;
			}
		}

		private int lngCurrentFormatID;
		private string strDisplayTitle = string.Empty;
		private string strPrinterName = string.Empty;
		private string strThisModule = string.Empty;
		private string strThisDB = string.Empty;
		private string strDataDB = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper clsCustomCodes = new clsDRWrapper();
		private clsDRWrapper clsCustomCodes_AutoInitialized;

		private clsDRWrapper clsCustomCodes
		{
			get
			{
				if (clsCustomCodes_AutoInitialized == null)
				{
					clsCustomCodes_AutoInitialized = new clsDRWrapper();
				}
				return clsCustomCodes_AutoInitialized;
			}
			set
			{
				clsCustomCodes_AutoInitialized = value;
			}
		}

		private int lngVerticalAlignment;
		private bool boolUsePrinterFonts;
		private bool boolDotMatrix;

		public bool DotMatrixFormat
		{
			set
			{
				boolDotMatrix = value;
			}
			get
			{
				bool DotMatrixFormat = false;
				DotMatrixFormat = boolDotMatrix;
				return DotMatrixFormat;
			}
		}

		public bool UsePrinterFonts
		{
			set
			{
				boolUsePrinterFonts = value;
			}
			get
			{
				bool UsePrinterFonts = false;
				UsePrinterFonts = boolUsePrinterFonts;
				return UsePrinterFonts;
			}
		}

		public int VerticalAlignment
		{
			set
			{
				// adjust up or down by lngtwips
				lngVerticalAlignment = value;
			}
			get
			{
				int VerticalAlignment = 0;
				VerticalAlignment = lngVerticalAlignment;
				return VerticalAlignment;
			}
		}

		public string Module
		{
			set
			{
				// I.E. BL or UT etc.
				strThisModule = value;
			}
			get
			{
				string Module = "";
				Module = strThisModule;
				return Module;
			}
		}

		public string DBFile
		{
			set
			{
				// The filename.  Usually TW[Mod abbreviation]0000.vb1
				strThisDB = value;
			}
			get
			{
				string DBFile = "";
				DBFile = strThisDB;
				return DBFile;
			}
		}

		public string DataDBFile
		{
			set
			{
				// The filename of the db that has the data
				strDataDB = value;
			}
			get
			{
				string DataDBFile = "";
				if (strDataDB != string.Empty)
				{
					DataDBFile = strDataDB;
				}
				else
				{
					DataDBFile = strThisDB;
				}
				return DataDBFile;
			}
		}

		public string EmailTag()
		{
			string EmailTag = "";
			EmailTag = modARStatusPayments.GetAccountEmail(FCConvert.ToInt32(clsReport.Get_Fields_Int32("AccountKey")));
			return EmailTag;
		}

		public string SQL
		{
			set
			{
				// The sql statement.  This is not accessed directly
				// by the report since the programmer might use an array of types or a grid
				// instead of a recordset
				strSQLReport = value;
			}
			get
			{
				string SQL = "";
				SQL = strSQLReport;
				return SQL;
			}
		}

		public int FormatID
		{
			set
			{
				// The ID of the report
				// Should be set before the report is run
				lngCurrentFormatID = value;
			}
			get
			{
				int FormatID = 0;
				FormatID = lngCurrentFormatID;
				return FormatID;
			}
		}

		public string ReportTitle
		{
			set
			{
				// What you want displayed if the report is previewed
				strDisplayTitle = value;
			}
			get
			{
				string ReportTitle = "";
				ReportTitle = strDisplayTitle;
				return ReportTitle;
			}
		}

		public string PrinterName
		{
			set
			{
				// If you need a specific printer and don't want the user to
				// be able to choose one, fill this in before calling the report
				strPrinterName = value;
			}
			get
			{
				string PrinterName = "";
				PrinterName = strPrinterName;
				return PrinterName;
			}
		}

		public bool EndOfFile
		{
			get
			{
				bool EndOfFile = false;
				// end of file, or more generally, end of the data
				EndOfFile = clsReport.EndOfFile();
				return EndOfFile;
			}
		}

		public bool BeginningOfFile
		{
			get
			{
				bool BeginningOfFile = false;
				BeginningOfFile = clsReport.BeginningOfFile();
				return BeginningOfFile;
			}
		}

		public void MoveNext()
		{
			clsReport.MoveNext();
		}

		public void MoveLast()
		{
			clsReport.MoveLast();
		}

		public void MovePrevious()
		{
			// most likely not used
			clsReport.MovePrevious();
		}

		public void MoveFirst()
		{
			clsReport.MoveFirst();
		}

		public bool LoadData()
		{
			bool LoadData = false;
			// this function returns false if it fails
			// It loads the data.  In the example class, this loads a recordset
			// the function calls all assume a recordset, but the programmer can have movenext
			// add 1 to an index to an array.  The class controls how the data is loaded and read
			// The report will use it like a recordset, but the implementation is irrelevant
			try
			{
				// On Error GoTo ErrorHandler
				LoadData = false;
				// load the data
				clsReport.OpenRecordset(strSQLReport, DataDBFile);
				// load the codes
				clsCustomCodes.OpenRecordset("select * from custombillcodes order by fieldid", strThisDB);
				LoadData = true;
				return LoadData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In clsCustomBill.LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadData;
		}

		public string GetDataByCode(int lngCode, string strExtraParameters = "")
		{
			string GetDataByCode = "";
			// the code corresponds to the data in the custombillcodes table
			bool boolSpecialCase = false;
			GetDataByCode = "";
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			modCustomBill.CustomBillCodeType CurCode = new modCustomBill.CustomBillCodeType(0);
			if (clsCustomCodes.FindFirstRecord("FieldID", lngCode))
			{
				boolSpecialCase = FCConvert.ToBoolean(clsCustomCodes.Get_Fields_Boolean("SpecialCase"));
				// TODO: Check the table for the column [Category] and replace with corresponding Get_Field method
				CurCode.Category = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCustomCodes.Get_Fields("Category"))));
				CurCode.Datatype = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCustomCodes.Get_Fields_Int16("datatype"))));
				CurCode.DBName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("dbname"));
				CurCode.FieldID = lngCode;
				CurCode.FieldName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("FieldName"));
				CurCode.FormatString = FCConvert.ToString(clsCustomCodes.Get_Fields_String("FormatString"));
				CurCode.SpecialCase = boolSpecialCase;
				CurCode.TableName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("tablename"));
				//FC:FINAL:DSE Properties cannot be used as a ref parameter
				clsDRWrapper temp = clsReport;
				if (boolSpecialCase == true)
				{
					// it is a special case and can't be simply loaded straight from the database
					GetDataByCode = modGlobal.HandleSpecialCase(ref temp, ref CurCode, ref strExtraParameters);
				}
				else
				{
					// is a simple code that just takes the data
					GetDataByCode = modGlobal.HandleRegularCase(ref temp, ref CurCode);
				}
				//FC:FINAL:DSE Assign back the values
				clsReport = temp;
			}
			else if (lngCode < 0)
			{
				CurCode.Category = 0;
				CurCode.Datatype = 0;
				CurCode.DBName = "";
				CurCode.FieldID = lngCode;
				CurCode.FieldName = "";
				CurCode.FormatString = "";
				CurCode.SpecialCase = true;
				CurCode.TableName = "";
				//FC:FINAL:DSE Properties cannot be used as a ref parameter
				clsDRWrapper temp = clsReport;
				GetDataByCode = modGlobal.HandleSpecialCase(ref temp, ref CurCode, ref strExtraParameters);
				//FC:FINAL:DSE Assign back values
				clsReport = temp;
			}
			return GetDataByCode;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		public int CentimetersToTwips(ref double dblCentimeters)
		{
			int CentimetersToTwips = 0;
			// returns twips
			CentimetersToTwips = FCConvert.ToInt32(dblCentimeters * 567);
			return CentimetersToTwips;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		public int InchesToTwips(ref double dblInches)
		{
			int InchesToTwips = 0;
			// returns twips
			InchesToTwips = FCConvert.ToInt32(dblInches * 1440);
			return InchesToTwips;
		}

		public double TwipsToInches(ref int lngTwips)
		{
			double TwipsToInches = 0;
			TwipsToInches = lngTwips / 1440.0;
			return TwipsToInches;
		}

		public double TwipsToCentimeters(ref int lngTwips)
		{
			double TwipsToCentimeters = 0;
			TwipsToCentimeters = lngTwips / 567.0;
			return TwipsToCentimeters;
		}
	}
}
