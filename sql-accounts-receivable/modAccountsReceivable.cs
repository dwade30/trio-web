﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.DataBaseLayer;
using System.Globalization;
using System.Text;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using clsDRWrapper = Global.clsDRWrapper;

namespace TWAR0000
{
	//FC:FINAL:AM: changed modAccountsReceivable to modGlobal
	public class modGlobal
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/11/2005              *
		// ********************************************************
		public const int ARSECENTRYINTOAR = 1;
		public const int ARSECBILLINGPROCESS = 2;
		public const int ARSECCOLLECTIONPROCESS = 3;
		public const int ARSECCHARGEINTEREST = 14;
		public const int ARSECDAILYAUDITREPORT = 10;
		public const int ARSECLOADBACK = 11;
		public const int ARSECPAYMENTSADJUSTMENTS = 9;
		public const int ARSECVIEWSTATUS = 8;
		public const int ARSECCUSTOMERBILLUPDATE = 21;
		public const int ARSECFILEMAINTENANCE = 6;
		public const int ARSECCUSTOMIZE = 15;
		public const int ARSECEDITCUSTOMBILLTYPES = 20;
		public const int ARSECRATERECUPDATE = 23;
		public const int ARSECEDITBILLINFO = 22;
		public const int ARSECPRINTING = 7;
		public const int ARSECTYPESETUP = 4;
		public const int AUTOPOSTBILLINGJOURNAL = 24;

		[DllImport("user32")]
		public static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

		[DllImport("advapi32.dll", EntryPoint = "GetUserNameA", SetLastError = true)]
		public static extern bool GetUserName(System.Text.StringBuilder sb, ref Int32 length);

		[DllImport("user32")]
		public static extern int SetParent(int hWndChild, int hWndNewParent);
		//[DllImport("user32", EntryPoint = "SendMessageA")]
		//public static extern int SendMessageAny(int hwnd, int Msg, int wParam, ref void* lParam);
		[DllImport("gdi32")]
		public static extern int SetTextColor(int hdc, int crColor);

		public const string DEFAULTDATABASE = "TWAR0000.vb1";

		public class StaticVariables
		{
            public bool moduleInitialized = false;
            public int glngARCurrentCloseOut;
			public int gintBasis;
			public double dblSalesTax;
			public double dblDefaultPerDiem;
			public Decimal curDefaultFlatRate;
			public int intDefaultInterestStartDate;
			// vbPorter upgrade warning: gobjFormName As object	OnRead(Form)
			public Form gobjFormName;
			// vbPorter upgrade warning: gobjLabel As object --> As FCLabel
			public FCLabel gobjLabel;
			// Public User                         As String
			public string CityTown = "";
			public int lngCurrentCustomer;
			public int lngCurrentPayee;
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			//public clsDRWrapper CustomerSearchResults = new clsDRWrapper();
			//public clsDRWrapper PayeeSearchResults = new clsDRWrapper();
			public clsDRWrapper CustomerSearchResults_AutoInitialized;
			public clsDRWrapper PayeeSearchResults_AutoInitialized;

			public clsDRWrapper CustomerSearchResults
			{
				get
				{
					if (CustomerSearchResults_AutoInitialized == null)
					{
						CustomerSearchResults_AutoInitialized = new clsDRWrapper();
					}
					return CustomerSearchResults_AutoInitialized;
				}
				set
				{
					CustomerSearchResults_AutoInitialized = value;
				}
			}

			public clsDRWrapper PayeeSearchResults
			{
				get
				{
					if (PayeeSearchResults_AutoInitialized == null)
					{
						PayeeSearchResults_AutoInitialized = new clsDRWrapper();
					}
					return PayeeSearchResults_AutoInitialized;
				}
				set
				{
					PayeeSearchResults_AutoInitialized = value;
				}
			}

			public DateTime datSelectedDate;
			public int RecordNumber;
			public int FirstRecordShown;
			public string[] strTypeDescriptions = new string[999 + 1];
			public DateTime gdtARStatusListAsOfDate;
			// variables for Status Lists
			public bool gboolARUseAsOfDate;
			public bool gboolARSLDateRange;
			public DateTime gdtARSLPaymentDate1;
			public DateTime gdtARSLPaymentDate2;
			public bool boolSubReport;
			public bool blnFromBillPrep;
		}

		public static void InitializeAccountsReceivableStatics()
		{
			try
			{
				string temp = "";
				if (!modGlobalFunctions.LoadSQLConfig("TWAR0000.VB1"))
				{
					MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				clsDRWrapper rs = new clsDRWrapper();

				modValidateAccount.SetBDFormats();
				modNewAccountBox.GetFormats();
				modGlobalFunctions.LoadTRIOColors();
				modGlobalFunctions.UpdateUsedModules();
				if (!modGlobalConstants.Statics.gboolAR)
				{
					MessageBox.Show("Accounts Receivable has expired.  Please call TRIO!", "Module Expired", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
				}
				rs.OpenRecordset("SELECT * FROM Customize", "TWAR0000.vb1");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Version")) || FCConvert.ToString(rs.Get_Fields_String("Version")) != FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision))
					{
						goto UpdateTag;
					}
					else
					{
						goto NonUpdateTag;
					}
				}
				else
				{
					goto UpdateTag;
				}
				UpdateTag:
				;
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Updating System";
				frmWait.InstancePtr.Refresh();

				cAccountsReceivableDB arDB = new cAccountsReceivableDB();
				if (!arDB.CheckVersion())
				{
					cVersionInfo tVer;
					tVer = arDB.GetVersion();
					frmWait.InstancePtr.Hide();
					MessageBox.Show("Error checking database." + "\r\n" + tVer.VersionString);
				}

				frmWait.InstancePtr.Unload();
				NonUpdateTag:
				;
				
				modARCalculations.ARSetup();
				modGlobal.Statics.dblSalesTax = FCConvert.ToDouble(modARCalculations.GetARVariable("TaxRate"));
				modGlobalFunctions.UpdateUsedModules();
				modAccountTitle.SetAccountFormats();
				modGlobalFunctions.GetGlobalRegistrySetting();
				
				UserName();
				modGlobalConstants.Statics.clsSecurityClass.Init("AR");
				modReplaceWorkFiles.GetGlobalVariables();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public static void Main(CommandDispatcher commandDispatcher)
		{
			InitializeAccountsReceivableStatics();
			MDIParent.InstancePtr.Init(commandDispatcher);
		}

		

		public static void UserName()
		{
			string strS;
			int lngCnt;
			int lngD;
			//FC:FINAL:PJ: Use external Method correctly
			//lngCnt = 199;
			//strS = Strings.StrDup(200, Strings.Chr(0));
			//lngD = GetUserName(ref strS, ref lngCnt);
			StringBuilder Buffer = new StringBuilder(200);
			int nSize = 199;
			GetUserName(Buffer, ref nSize);
			strS = Buffer.ToString();
			lngCnt = nSize;
			modBudgetaryAccounting.Statics.User = Strings.Trim(Strings.Replace(Strings.Left(strS, lngCnt - 1), FCConvert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare));
		}

		public static void WriteAuditRecord(ref string strOverride, ref string strScreen)
		{
			modGlobalFunctions.AddCYAEntry_240("VR", strOverride, strScreen);
		}

		public static void SetReportFormTest(ref GrapeCity.ActiveReports.SectionReport rpt, ref short intReturn)
		{
			int i;
			// need to make sure activereports is using the 'MVR3' form
			for (i = 0; i <= rpt.Document.Printer.PaperSizes.Count - 1; i++)
			{
				//FC:TODO:AM
				//if (rpt.Document.Printer.PaperSizes[i]==intReturn) {
				//	rpt.Document.Printer.PaperSize = rpt.Document.Printer.PaperSizes[i];
				//}
			}
		}
		// vbPorter upgrade warning: intValue As object	OnWrite(int, string, object)
		// vbPorter upgrade warning: intLength As short	OnWrite(string, short)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string PadToString_6(object intValue, short intLength)
		{
			return PadToString(intValue, intLength);
		}

		public static string PadToString_8(object intValue, short intLength)
		{
			return PadToString(intValue, intLength);
		}

		public static string PadToString(object intValue, short intLength)
		{
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			if ((intLength - FCConvert.ToString(intValue).Length) > 0)
			{
				PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			}
			else
			{
				PadToString = intValue.ToString();
			}
			return PadToString;
		}

		public static short PeriodCalc(string x)
		{
			short PeriodCalc = 0;
			if (x == "January")
			{
				PeriodCalc = 1;
			}
			else if (x == "February")
			{
				PeriodCalc = 2;
			}
			else if (x == "March")
			{
				PeriodCalc = 3;
			}
			else if (x == "April")
			{
				PeriodCalc = 4;
			}
			else if (x == "May")
			{
				PeriodCalc = 5;
			}
			else if (x == "June")
			{
				PeriodCalc = 6;
			}
			else if (x == "July")
			{
				PeriodCalc = 7;
			}
			else if (x == "August")
			{
				PeriodCalc = 8;
			}
			else if (x == "September")
			{
				PeriodCalc = 9;
			}
			else if (x == "October")
			{
				PeriodCalc = 10;
			}
			else if (x == "November")
			{
				PeriodCalc = 11;
			}
			else if (x == "December")
			{
				PeriodCalc = 12;
			}
			return PeriodCalc;
		}
		// vbPorter upgrade warning: clsRep As object	OnWrite(clsDRWrapper)
		public static string HandleRegularCase(ref clsDRWrapper clsRep, ref modCustomBill.CustomBillCodeType clsCCode)
		{
			string HandleRegularCase = "";
			string strTemp;
			// Takes a code number and looks up the data based on the field name in the database
			try
			{
				// On Error GoTo ErrorHandler
				HandleRegularCase = "";
				strTemp = FCConvert.ToString(clsRep.Get_Fields(clsCCode.FieldName));
				switch ((DataTypeEnum)clsCCode.Datatype)
				{
					case DataTypeEnum.dbDate:
						{
							if (clsRep.Get_Fields(clsCCode.FieldName) == 0)
							{
								strTemp = "";
							}
							else
							{
								if (clsCCode.FormatString != string.Empty)
								{
									strTemp = Strings.Format(strTemp, clsCCode.FormatString);
								}
								else
								{
									strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
								}
							}
							break;
						}
					default:
						{
							if (Strings.Trim(clsCCode.FormatString) != string.Empty)
							{
								strTemp = Strings.Format(strTemp, clsCCode.FormatString);
							}
							break;
						}
				}
				//end switch
				HandleRegularCase = strTemp;
				return HandleRegularCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleRegularCase with code " + FCConvert.ToString(clsCCode.FieldID), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleRegularCase;
		}
		// vbPorter upgrade warning: clsRep As object	OnWrite(clsDRWrapper)
		public static string HandleSpecialCase(ref clsDRWrapper clsRep, ref modCustomBill.CustomBillCodeType clsCCode, ref string strExtraParameters)
		{
			string HandleSpecialCase = "";
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsDefault = new clsDRWrapper();
				HandleSpecialCase = "";
				switch (clsCCode.FieldID)
				{
					case -10:
						{
							// Bar Code
							HandleSpecialCase = "A" + Strings.Format(clsRep.Get_Fields_Int32("ActualAccountNumber"), "0000000");
							break;
						}
					case 3:
						{
							// Customer Address
							if (Strings.Trim(clsRep.Get_Fields_String("BAddress1")) != "")
							{
								HandleSpecialCase = HandleSpecialCase + Strings.Trim(clsRep.Get_Fields_String("BAddress1")) + "\r\n";
							}
							if (Strings.Trim(clsRep.Get_Fields_String("BAddress2")) != "")
							{
								HandleSpecialCase = HandleSpecialCase + Strings.Trim(clsRep.Get_Fields_String("BAddress2")) + "\r\n";
							}
							if (Strings.Trim(clsRep.Get_Fields_String("BAddress3")) != "")
							{
								HandleSpecialCase = HandleSpecialCase + Strings.Trim(clsRep.Get_Fields_String("BAddress3")) + "\r\n";
							}
							if (Strings.Trim(clsRep.Get_Fields_String("BZip4")) != "")
							{
								HandleSpecialCase = HandleSpecialCase + Strings.Trim(Strings.Trim(clsRep.Get_Fields_String("BCity")) + ", " + Strings.Trim(clsRep.Get_Fields_String("BState")) + " " + Strings.Trim(clsRep.Get_Fields_String("BZip")) + "-" + Strings.Trim(clsRep.Get_Fields_String("BZip4")));
							}
							else
							{
								HandleSpecialCase = HandleSpecialCase + Strings.Trim(Strings.Trim(clsRep.Get_Fields_String("BCity")) + ", " + Strings.Trim(clsRep.Get_Fields_String("BState")) + " " + Strings.Trim(clsRep.Get_Fields_String("BZip")));
							}
							break;
						}
					case 5:
						{
							// Description 1
							// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount1")) > 0)
							{
								HandleSpecialCase = clsRep.Get_Fields_String("Description1");
							}
							break;
						}
					case 6:
						{
							// Unit Cost 1
							// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount1")) > 0)
							{
								// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields("Amount1"), "#,##0.00##");
							}
							break;
						}
					case 7:
						{
							// Description 2
							// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount2")) > 0)
							{
								HandleSpecialCase = clsRep.Get_Fields_String("Description2");
							}
							break;
						}
					case 8:
						{
							// Unit Cost 2
							// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount2")) > 0)
							{
								// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields("Amount2"), "#,##0.00##");
							}
							break;
						}
					case 9:
						{
							// Description 3
							// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount3")) > 0)
							{
								HandleSpecialCase = clsRep.Get_Fields_String("Description3");
							}
							break;
						}
					case 10:
						{
							// Unit Cost 3
							// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount3")) > 0)
							{
								// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields("Amount3"), "#,##0.00##");
							}
							break;
						}
					case 11:
						{
							// Description 4
							// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount4")) > 0)
							{
								HandleSpecialCase = clsRep.Get_Fields_String("Description4");
							}
							break;
						}
					case 12:
						{
							// Unit Cost 4
							// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount4")) > 0)
							{
								// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields("Amount4"), "#,##0.00##");
							}
							break;
						}
					case 13:
						{
							// Description 5
							// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount5")) > 0)
							{
								HandleSpecialCase = clsRep.Get_Fields_String("Description5");
							}
							break;
						}
					case 14:
						{
							// Unit Cost 5
							// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount5")) > 0)
							{
								// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields("Amount5"), "#,##0.00##");
							}
							break;
						}
					case 15:
						{
							// Description 6
							// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount6")) > 0)
							{
								HandleSpecialCase = clsRep.Get_Fields_String("Description6");
							}
							break;
						}
					case 16:
						{
							// Unit Cost 6
							// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount6")) > 0)
							{
								// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields("Amount6"), "#,##0.00##");
							}
							break;
						}
					case 20:
						{
							// Bill Total
							HandleSpecialCase = Strings.Format(clsRep.Get_Fields_Decimal("PrinOwed") + clsRep.Get_Fields_Decimal("TaxOwed"), "#,##0.00");
							break;
						}
					case 25:
						{
							// Reference Description
							// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
							rsDefault.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + clsRep.Get_Fields("BillType"));
							if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
							{
								HandleSpecialCase = Strings.Trim(FCConvert.ToString(rsDefault.Get_Fields_String("Reference")));
							}
							break;
						}
					case 26:
						{
							// Control1 Description
							// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
							rsDefault.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + clsRep.Get_Fields("BillType"));
							if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
							{
								HandleSpecialCase = Strings.Trim(FCConvert.ToString(rsDefault.Get_Fields_String("Control1")));
							}
							break;
						}
					case 27:
						{
							// Control2 Description
							// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
							rsDefault.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + clsRep.Get_Fields("BillType"));
							if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
							{
								HandleSpecialCase = Strings.Trim(FCConvert.ToString(rsDefault.Get_Fields_String("Control2")));
							}
							break;
						}
					case 28:
						{
							// Control3 Description
							// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
							rsDefault.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + clsRep.Get_Fields("BillType"));
							if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
							{
								HandleSpecialCase = Strings.Trim(FCConvert.ToString(rsDefault.Get_Fields_String("Control3")));
							}
							break;
						}
					case 29:
						{
							// Bill Type Description
							// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
							rsDefault.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + clsRep.Get_Fields("BillType"));
							if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
							{
								HandleSpecialCase = Strings.Trim(FCConvert.ToString(rsDefault.Get_Fields_String("TypeTitle")));
							}
							break;
						}
					case 31:
						{
							// Bill Date
							if (strExtraParameters == "Fee 1")
							{
								// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
								if (Conversion.Val(clsRep.Get_Fields("Amount1")) > 0)
								{
									rsDefault.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + clsRep.Get_Fields_Int32("BillNumber"));
									if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
									{
										HandleSpecialCase = Strings.Format(rsDefault.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
									}
								}
							}
							else if (strExtraParameters == "Fee 2")
							{
								// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
								if (Conversion.Val(clsRep.Get_Fields("Amount2")) > 0)
								{
									rsDefault.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + clsRep.Get_Fields_Int32("BillNumber"));
									if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
									{
										HandleSpecialCase = Strings.Format(rsDefault.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
									}
								}
							}
							else if (strExtraParameters == "Fee 3")
							{
								// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
								if (Conversion.Val(clsRep.Get_Fields("Amount3")) > 0)
								{
									rsDefault.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + clsRep.Get_Fields_Int32("BillNumber"));
									if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
									{
										HandleSpecialCase = Strings.Format(rsDefault.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
									}
								}
							}
							else if (strExtraParameters == "Fee 4")
							{
								// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
								if (Conversion.Val(clsRep.Get_Fields("Amount4")) > 0)
								{
									rsDefault.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + clsRep.Get_Fields_Int32("BillNumber"));
									if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
									{
										HandleSpecialCase = Strings.Format(rsDefault.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
									}
								}
							}
							else if (strExtraParameters == "Fee 5")
							{
								// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
								if (Conversion.Val(clsRep.Get_Fields("Amount5")) > 0)
								{
									rsDefault.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + clsRep.Get_Fields_Int32("BillNumber"));
									if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
									{
										HandleSpecialCase = Strings.Format(rsDefault.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
									}
								}
							}
							else if (strExtraParameters == "Fee 6")
							{
								// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
								if (Conversion.Val(clsRep.Get_Fields("Amount6")) > 0)
								{
									rsDefault.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + clsRep.Get_Fields_Int32("BillNumber"));
									if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
									{
										HandleSpecialCase = Strings.Format(rsDefault.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
									}
								}
							}
							else
							{
								rsDefault.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + clsRep.Get_Fields_Int32("BillNumber"));
								if (rsDefault.EndOfFile() != true && rsDefault.BeginningOfFile() != true)
								{
									HandleSpecialCase = Strings.Format(rsDefault.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
								}
							}
							break;
						}
					case 32:
						{
							// bill message
							rsDefault.OpenRecordset("select billmessage from customermaster where id = " + FCConvert.ToString(Conversion.Val(clsRep.Get_Fields_Int32("accountkey"))));
							if (!rsDefault.EndOfFile())
							{
								HandleSpecialCase = FCConvert.ToString(rsDefault.Get_Fields_String("billmessage"));
							}
							break;
						}
					case 33:
						{
							// Quantity 1
							// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount1")) > 0)
							{
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields_Double("Quantity1"), "#,##0.00##");
							}
							break;
						}
					case 34:
						{
							// Quantity 2
							// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount2")) > 0)
							{
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields_Double("Quantity2"), "#,##0.00##");
							}
							break;
						}
					case 35:
						{
							// Quantity 3
							// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount3")) > 0)
							{
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields_Double("Quantity3"), "#,##0.00##");
							}
							break;
						}
					case 36:
						{
							// Quantity 4
							// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount4")) > 0)
							{
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields_Double("Quantity4"), "#,##0.00##");
							}
							break;
						}
					case 37:
						{
							// Quantity 5
							// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount5")) > 0)
							{
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields_Double("Quantity5"), "#,##0.00##");
							}
							break;
						}
					case 38:
						{
							// Quantity 6
							// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount6")) > 0)
							{
								HandleSpecialCase = Strings.Format(clsRep.Get_Fields_Double("Quantity6"), "#,##0.00##");
							}
							break;
						}
					case 39:
						{
							// Amount 1
							// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount1")) > 0)
							{
								// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(FCUtils.Round(Convert.ToDouble(clsRep.Get_Fields("Amount1")) * clsRep.Get_Fields_Double("Quantity1"), 2), "#,##0.00");
							}
							break;
						}
					case 40:
						{
							// Amount 2
							// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount2")) > 0)
							{
								// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(FCUtils.Round(Convert.ToDouble(clsRep.Get_Fields("Amount2")) * clsRep.Get_Fields_Double("Quantity2"), 2), "#,##0.00");
							}
							break;
						}
					case 41:
						{
							// Amount 3
							// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount3")) > 0)
							{
								// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(FCUtils.Round(Convert.ToDouble(clsRep.Get_Fields("Amount3")) * clsRep.Get_Fields_Double("Quantity3"), 2), "#,##0.00");
							}
							break;
						}
					case 42:
						{
							// Amount 4
							// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount4")) > 0)
							{
								// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(FCUtils.Round(Convert.ToDouble(clsRep.Get_Fields("Amount4")) * clsRep.Get_Fields_Double("Quantity4"), 2), "#,##0.00");
							}
							break;
						}
					case 43:
						{
							// Amount 5
							// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount5")) > 0)
							{
								// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(FCUtils.Round(Convert.ToDouble(clsRep.Get_Fields("Amount5")) * clsRep.Get_Fields_Double("Quantity5"), 2), "#,##0.00");
							}
							break;
						}
					case 44:
						{
							// Amount 6
							// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							if (Conversion.Val(clsRep.Get_Fields("Amount6")) > 0)
							{
								// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
								HandleSpecialCase = Strings.Format(FCUtils.Round(Convert.ToDouble(clsRep.Get_Fields("Amount6")) * clsRep.Get_Fields_Double("Quantity6"), 2), "#,##0.00");
							}
							break;
						}
				}
				//end switch
				return HandleSpecialCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleSpecialCase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleSpecialCase;
		}
		// vbPorter upgrade warning: NewValue As Variant --> As double
		public static bool UpdateARVariable(string strVariableName, double NewValue)
		{
			bool UpdateARVariable = false;
			clsDRWrapper rsVariables = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				UpdateARVariable = false;
				rsVariables.Reset();
				rsVariables.OpenRecordset("SELECT * FROM Customize");
				if (rsVariables.EndOfFile())
				{
					rsVariables.AddNew();
				}
				else
				{
					rsVariables.Edit();
				}
				rsVariables.Set_Fields(strVariableName, NewValue);
				rsVariables.Update();
				UpdateARVariable = true;
				return UpdateARVariable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateARVariable;
		}

		public static int ARCloseOut()
		{
			int ARCloseOut = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will close out all of the payments
				string strSQL;
				clsDRWrapper rsUpdate = new clsDRWrapper();
				// this will find the next close out number
				rsUpdate.OpenRecordset("SELECT * FROM CloseOut WHERE ID = 0", modCLCalculations.strARDatabase);
				rsUpdate.AddNew();
				rsUpdate.Set_Fields("Type", "AR");
				rsUpdate.Update();
				ARCloseOut = FCConvert.ToInt32(rsUpdate.Get_Fields_Int32("ID"));
				// update all of the payments with this number
				strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(ARCloseOut) + " WHERE isnull(DailyCloseOut, 0) = 0";
				if (rsUpdate.Execute(strSQL, modCLCalculations.strARDatabase))
				{
					MessageBox.Show("Records Updated.", "Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Records not Updated.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				return ARCloseOut;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In AR Close Out", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ARCloseOut;
		}

		public static bool ValidateBillType_2(int intBillTypeKey)
		{
			return ValidateBillType(ref intBillTypeKey);
		}

		public static bool ValidateBillType(ref int intBillTypeKey)
		{
			bool ValidateBillType = false;
			clsDRWrapper rs = new clsDRWrapper();
			ValidateBillType = true;
			rs.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillTypeKey));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account1"))) != "")
				{
					ValidateBillType = modValidateAccount.AccountValidate(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account1"))));
				}
				if (ValidateBillType == false)
					return ValidateBillType;
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account2"))) != "")
				{
					ValidateBillType = modValidateAccount.AccountValidate(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account2"))));
				}
				if (ValidateBillType == false)
					return ValidateBillType;
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account3"))) != "")
				{
					ValidateBillType = modValidateAccount.AccountValidate(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account3"))));
				}
				if (ValidateBillType == false)
					return ValidateBillType;
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account4"))) != "")
				{
					ValidateBillType = modValidateAccount.AccountValidate(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account4"))));
				}
				if (ValidateBillType == false)
					return ValidateBillType;
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account5"))) != "")
				{
					ValidateBillType = modValidateAccount.AccountValidate(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account5"))));
				}
				if (ValidateBillType == false)
					return ValidateBillType;
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account6"))) != "")
				{
					ValidateBillType = modValidateAccount.AccountValidate(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Account6"))));
				}
				if (ValidateBillType == false)
					return ValidateBillType;
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("DefaultAccount"))) != "")
				{
					ValidateBillType = modValidateAccount.AccountValidate(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("DefaultAccount"))));
				}
				if (ValidateBillType == false)
					return ValidateBillType;
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ReceivableAccount"))) != "")
				{
					ValidateBillType = modValidateAccount.AccountValidate(Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ReceivableAccount"))));
				}
				if (ValidateBillType == false)
					return ValidateBillType;
			}
			else
			{
				ValidateBillType = false;
			}
			return ValidateBillType;
		}

		public static string GetPeriodEndFromRate(ref int lngRateKey)
		{
			string GetPeriodEndFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			// loads the rate with lngratekey and returns the end date
			try
			{
				// On Error GoTo ErrorHandler
				GetPeriodEndFromRate = "";
				clsLoad.OpenRecordset("select * from ratekeys where ID = " + FCConvert.ToString(lngRateKey), "twar0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					// TODO: Check the table for the column [end] and replace with corresponding Get_Field method
					if (Information.IsDate(clsLoad.Get_Fields("end")))
					{
						// TODO: Check the table for the column [end] and replace with corresponding Get_Field method
						if (Conversion.Val(clsLoad.Get_Fields("end")) != 0)
						{
							// TODO: Check the table for the column [end] and replace with corresponding Get_Field method
							strTemp = Strings.Format(clsLoad.Get_Fields("end"), "MM/dd/yyyy");
						}
					}
				}
				GetPeriodEndFromRate = strTemp;
				return GetPeriodEndFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetPeriodEndFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetPeriodEndFromRate;
		}

		public static string GetPeriodStartFromRate(ref int lngRateKey)
		{
			string GetPeriodStartFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			// loads the rate with lngratekey and returns the startdate
			try
			{
				// On Error GoTo ErrorHandler
				GetPeriodStartFromRate = "";
				clsLoad.OpenRecordset("Select * from ratekeys where ID = " + FCConvert.ToString(lngRateKey), "twar0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("start")))
					{
						if (clsLoad.Get_Fields_DateTime("start").ToOADate() != 0)
						{
							strTemp = Strings.Format(clsLoad.Get_Fields_DateTime("start"), "MM/dd/yyyy");
						}
					}
				}
				GetPeriodStartFromRate = strTemp;
				return GetPeriodStartFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetPeriodStartFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetPeriodStartFromRate;
		}

		public static string GetBillingDateFromRate_2(int lngRateKey)
		{
			return GetBillingDateFromRate(ref lngRateKey);
		}

		public static string GetBillingDateFromRate(ref int lngRateKey)
		{
			string GetBillingDateFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				GetBillingDateFromRate = "";
				clsLoad.OpenRecordset("select * from ratekeys where ID = " + FCConvert.ToString(lngRateKey), "twar0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("BillDate")))
					{
						if (clsLoad.Get_Fields_DateTime("BillDate").ToOADate() != 0)
						{
							strTemp = Strings.Format(clsLoad.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
						}
					}
				}
				GetBillingDateFromRate = strTemp;
				return GetBillingDateFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number" + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetBillingDateFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetBillingDateFromRate;
		}

		public static string GetInterestDateFromRate(ref int lngRateKey)
		{
			string GetInterestDateFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				GetInterestDateFromRate = "";
				clsLoad.OpenRecordset("select * from ratekeys where ID = " + FCConvert.ToString(lngRateKey), "twar0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("Intstart")))
					{
						if (clsLoad.Get_Fields_DateTime("intstart").ToOADate() != 0)
						{
							strTemp = Strings.Format(clsLoad.Get_Fields_DateTime("Intstart"), "MM/dd/yyyy");
							GetInterestDateFromRate = strTemp;
						}
					}
				}
				return GetInterestDateFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetInterestDateFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetInterestDateFromRate;
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, double)
		public static string GetInterestRateFromRate(ref int lngRateKey, ref bool boolWater)
		{
			string GetInterestRateFromRate = "";
			// vbPorter upgrade warning: dblTemp As double	OnWrite(double, string)
			double dblTemp = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				GetInterestRateFromRate = "";
				clsLoad.OpenRecordset("Select * from ratekeys where ID = " + FCConvert.ToString(lngRateKey), "twar0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					dblTemp = Conversion.Val(clsLoad.Get_Fields_Double("intRate"));
					dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp, "0.0000"));
					GetInterestRateFromRate = FCConvert.ToString(dblTemp);
				}
				return GetInterestRateFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetInterestRateFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetInterestRateFromRate;
		}

		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "FormExist";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				foreach (Form frm in FCGlobal.Statics.Forms)
				{
					if (frm.Name == FormName.Name)
					{
						if (FCConvert.ToString(frm.Tag) == "Open")
						{
							FormExist = true;
							return FormExist;
						}
					}
				}
				FormName.Tag = "Open";
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return FormExist;
			}
			return FormExist;
		}
		//FC:FINAL:AM: in TWAR0000 project the Round method is not defined so Round from Math will be called
		public static double Round(double dValue, short iDigits)
		{
			return FCUtils.Round(dValue, iDigits);
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
