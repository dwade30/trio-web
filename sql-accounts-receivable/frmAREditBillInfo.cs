﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmAREditBillInfo.
	/// </summary>
	public partial class frmAREditBillInfo : BaseForm
	{
		public frmAREditBillInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.txtGetAccountNumber.AllowOnlyNumericInput();

		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAREditBillInfo InstancePtr
		{
			get
			{
				return (frmAREditBillInfo)Sys.GetInstance(typeof(frmAREditBillInfo));
			}
		}

		protected frmAREditBillInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/27/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/14/2006              *
		// ********************************************************
		int lngYear;
		bool boolResultsScreen;
		// this is true when the result grid is being shown
		bool boolSearch;
		string strSearchString;
		bool boolDirty;
		int lngLastRow;
		string strCopyName1;
		string strCopyName2;
		string strCopyAddr1;
		string strCopyAddr2;
		string strCopyAddr3;
		string strCopyReference;
		string strCopyControl1;
		string strCopyControl2;
		string strCopyControl3;
		string strCopyCity;
		string strCopyState;
		string strCopyZip;
		string strCopyZip4;
		// these constants are for the grid columns
		// edit grid
		int lngColEDInvoice;
		int lngColEDName1;
		int lngColEDName2;
		int lngColEDBillkey;
		// Dim lngColMapLot                As Long
		// search grid
		int lngColAcct;
		int lngColName;
		int lngColLocationNumber;
		int lngColLocation;
		int lngColMapLot;
		int lngColBookPage;
		int lngColStreetNumber;
		int lngColStreetName;
		int lngRowOwner;
		int lngRowSecondOwner;
		int lngRowAddress1;
		int lngRowAddress2;
		int lngRowAddress3;
		int lngRowCity;
		int lngRowState;
		int lngRowZip;
		int lngRowZip4;
		int lngRowReference;
		int lngRowControl1;
		int lngRowControl2;
		int lngRowControl3;

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			if (boolResultsScreen)
			{
				vsSearch.Visible = false;
				lblSearchListInstruction.Visible = false;
				fraSearchCriteria.Visible = true;
				fraSearch.Visible = true;
				fraGetAccount.Visible = true;
				boolResultsScreen = false;
				btnFileSave.Enabled = true;
				btnProcessGetAccount.Enabled = true;
				btnProcessSearch.Enabled = true;
			}
			else
			{
				//optSearchType[0].Checked = true;
				cmbSearchType.SelectedIndex = 0;
				txtSearch.Text = "";
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int holdaccount = 0;
				clsDRWrapper rsCL = new clsDRWrapper();
				string strSQL = "";
				int intAns;
				lblSearchListInstruction.Visible = false;
				TryAgain:
				;
				if (Conversion.Val(txtGetAccountNumber.Text) != 0)
				{
					holdaccount = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
					intError = 1;
					strSQL = "SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(holdaccount);
					intError = 2;
					rsCL.OpenRecordset(strSQL, modCLCalculations.strARDatabase);
					intError = 3;
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						// Create Queries on the database for the next screen to use
						ShowEditAccount_2(FCConvert.ToInt32(rsCL.Get_Fields_Int32("ActualAccountNumber")));
					}
					else
					{
						MessageBox.Show("The account selected does not have any billing records.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					intError = 50;
					// set the last account field in the registry to the current account
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "ARLastAccountNumber", modGlobal.PadToString_6(holdaccount, 6));
					intError = 53;
					btnFileSave.Enabled = true;
					btnProcessGetAccount.Enabled = true;
					btnProcessSearch.Enabled = true;
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a valid account number.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get Account Error - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Close();
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngSelection = 0;
				string strAcctList = "";
				string strLeftOverAccts = "";
				string SQL = "";
				clsDRWrapper rs = new clsDRWrapper();
				clsDRWrapper rsBook = new clsDRWrapper();
				clsDRWrapper rsCLAccounts = new clsDRWrapper();
				string strList = "";
				string strBook = "";
				string strLastName = "";
				string strDoubleSearch = "";
				// find out which search option has been checked
				if (cmbSearchType.SelectedIndex == 0)
				{
					lngSelection = 0;
				}
				else if (cmbSearchType.SelectedIndex == 1)
				{
					lngSelection = 1;
				}
				else if (cmbSearchType.SelectedIndex == 2)
				{
					lngSelection = 2;
				}
				else if (cmbSearchType.SelectedIndex == 3)
				{
					lngSelection = 3;
				}
				else
				{
					MessageBox.Show("You must select a field to search by from the 'Search By' box.", "Search By", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				// make sure that the user typed search criteria in
				if (Strings.Trim(txtSearch.Text) == "")
				{
					MessageBox.Show("You must type a search criteria in the white box.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
				//Application.DoEvents();
				switch (lngSelection)
				{
					case 0:
						{
							// Name
							// check for the type of account to look for Real Estate or Personal Property
							SQL = "SELECT * FROM Bill INNER JOIN (SELECT ID AS BK FROM Bill WHERE ((BName >= '" + txtSearch.Text + "  ' AND BName < '" + txtSearch.Text + "ZZZ') OR (BName2 >= '" + txtSearch.Text + "  ' AND BName2 < '" + txtSearch.Text + "ZZZ'))) AS List ON Bill.ID = List.BK ORDER BY BName";
							// this will create a string of accounts that will be added in the grid as well as the RE matches (for old collection files with that name)
							rsCLAccounts.OpenRecordset(SQL, modCLCalculations.strARDatabase);
							strAcctList = ",";
							while (!rsCLAccounts.EndOfFile())
							{
								//Application.DoEvents();
								strAcctList += rsCLAccounts.Get_Fields_Int32("ActualAccountNumber") + ",";
								rsCLAccounts.MoveNext();
							}
							SQL = "SELECT * FROM CustomerMaster WHERE (Name >= '" + txtSearch.Text + "' AND Name < '" + txtSearch.Text + "ZZZ') ORDER BY Name, CustomerID";
							strDoubleSearch = "(SELECT CustomerID, Name FROM CustomerMaster WHERE (Name >= '" + txtSearch.Text + "' AND Name < '" + txtSearch.Text + "ZZZ') ";
							strDoubleSearch += "UNION ALL (SELECT ActualAccountNumber as CustomerID, BName AS Name FROM Bill WHERE BName >= '" + txtSearch.Text + "' AND BName < '" + txtSearch.Text + "ZZZ')";
							strDoubleSearch = "SELECT DISTINCT Account, Name FROM (" + strDoubleSearch + ")) ORDER BY Name";
							strSearchString = strDoubleSearch;
							// Case 1                                  'Street Name
							// 
							// rsCLAccounts.OpenRecordset "SELECT * FROM BillingMaster WHERE BillKey = 0", strCLDatabase
							// If boolRE Then
							// SQL = "SELECT *, RSAccount AS Account, RSName AS Name FROM Master WHERE (RSLOCSTREET >= '" & txtSearch.Text & "' AND RSLOCSTREET < '" & txtSearch.Text & "ZZZ') ORDER BY RSLOCSTREET, VAL(RSLOCNUMALPH), RSAccount"
							// Else
							// SQL = "SELECT * FROM PPMaster WHERE (STREET >= '" & txtSearch.Text & "' AND STREET < '" & txtSearch.Text & "ZZZ') ORDER BY STREET, VAL(STREETNUMBER), Account"
							// End If
							// strSearchString = SQL
							// Case 2                                  'Map Lot
							// 
							// rsCLAccounts.OpenRecordset "SELECT * FROM BillingMaster WHERE BillKey = 0", strCLDatabase
							// If boolRE Then
							// SQL = "SELECT *, RSAccount AS Account, RSName AS Name FROM Master WHERE (RSMAPLOT >= '" & txtSearch.Text & "' AND RSMAPLOT < '" & txtSearch.Text & "ZZZ') ORDER BY RSMAPLOT, RSAccount"
							// Else
							// SQL = "SELECT * FROM Master WHERE (RSMAPLOT >= '" & txtSearch.Text & "' AND RSMAPLOT < '" & txtSearch.Text & "ZZZ') ORDER BY RSMAPLOT, Account"
							// Exit Sub
							// End If
							// strSearchString = SQL
							break;
						}
				}
				//end switch
				rs.OpenRecordset(SQL, modCLCalculations.strARDatabase);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
					// , True, .RecordCount, True
					this.Refresh();
					rs.MoveLast();
					rs.MoveFirst();
					// clear the listbox
					vsSearch.Rows = 1;
					FormatSearchGrid();
					// fill the listbox with all the records returned
					while (!rs.EndOfFile())
					{
						// frmWait.IncrementProgress
						//Application.DoEvents();
						vsSearch.AddItem("");
						vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields_Int32("CustomerID")));
						// If (UCase(.Get_Fields("Name")) >= UCase(txtSearch.Text) & "   " And UCase(.Get_Fields("Name")) <= UCase(txtSearch.Text & "ZZZ")) Or lngSelection <> 0 Then
						vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields_String("Name")));
						// & " & " & .Get_Fields_String("BName2")
						// Else
						// vsSearch.TextMatrix(vsSearch.rows - 1, lngColName) = .Get_Fields("BName2") & ", " & .Get_Fields("Name")
						// End If
						rs.MoveNext();
					}
					// now check all of the billing records to see if any did not match the master record,
					// if they did not, then add a row for that bill as well
					rsCLAccounts.MoveFirst();
					while (!rsCLAccounts.EndOfFile())
					{
						rs.FindFirstRecord("CustomerID", rsCLAccounts.Get_Fields_Int32("ActualAccountNumber"));
						if (!rs.NoMatch)
						{
							if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("BNAme")))) == Strings.Trim(Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("Name")))) || Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("BName")))) == Strings.Trim(Strings.UCase(strLastName)))
							{
								// if this bill has the same name as its master account or has the
								// same name as the last bill then do nothing
							}
							else
							{
								// add the row
								vsSearch.AddItem("");
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
								if ((fecherFoundation.Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), Strings.UCase(txtSearch.Text) + "   ", true) >= 0 && fecherFoundation.Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), Strings.UCase(txtSearch.Text + "ZZZ"), true) <= 0) || lngSelection != 0)
								{
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsCLAccounts.Get_Fields_String("BName") + " & " + rsCLAccounts.Get_Fields_String("BName2"));
								}
								else
								{
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsCLAccounts.Get_Fields_String("BName2") + ", " + rsCLAccounts.Get_Fields_String("BName"));
								}
								// highlight the added rows that are only past owners
								vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								strLastName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("BName"));
							}
						}
						else if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("BName")))) != Strings.Trim(Strings.UCase(strLastName)))
						{
							// add the row
							vsSearch.AddItem("");
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
							if ((fecherFoundation.Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), Strings.UCase(txtSearch.Text) + "   ", true) >= 0 && fecherFoundation.Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), Strings.UCase(txtSearch.Text + "ZZZ"), true) <= 0) || lngSelection != 0)
							{
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsCLAccounts.Get_Fields_String("BName") + " & " + rsCLAccounts.Get_Fields_String("BName2"));
							}
							else
							{
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsCLAccounts.Get_Fields_String("BName2") + ", " + rsCLAccounts.Get_Fields_String("BName"));
							}
							// highlight the added rows that are only past owners
							vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							strLastName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("BName"));
						}
						rsCLAccounts.MoveNext();
					}
					// set the height of the grid
					//FC:FINAL:DSE:#480 Form redesign
					/*
                    if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                    {
                        vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
                        vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
                    }
                    else
                    {
                        vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
                        vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                    }
                    */
					vsSearch.Visible = true;
					fraSearch.Visible = true;
					fraGetAccount.Visible = false;
					fraSearchCriteria.Visible = false;
					boolResultsScreen = true;
					btnProcessGetAccount.Enabled = false;
					btnProcessSearch.Enabled = false;
					if (lngSelection == 0)
					{
						// force the name order
						vsSearch.Col = lngColName;
						vsSearch.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					}
					frmWait.InstancePtr.Unload();
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No results were matched to the criteria.  Please try again.", "No RE Results", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Open Recordset ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Close();
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmAREditBillInfo_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorTag
				// If FormExist(Me) Then Exit Sub
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//FC:FINAL:DSE:#180 Window design should match the other search window designs
				//lblInstructions1.Text = "Please enter the account number or hit Enter to view the account shown.";
				if (modCLCalculations.IsThisCR())
				{
					if (modARStatusPayments.Statics.gboolShowLastARAccountInCR)
					{
						txtGetAccountNumber.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentCustomer);
					}
				}
				else
				{
					txtGetAccountNumber.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentCustomer);
				}
				ShowSearch();
				txtGetAccountNumber.SelectionStart = 0;
				txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
				txtGetAccountNumber.Focus();
				FCGlobal.Screen.MousePointer = 0;
				//Application.DoEvents();
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				FCGlobal.Screen.MousePointer = 0;
			}
		}

		private void frmAREditBillInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (fraEditInfo.Visible)
			{
				switch (KeyCode)
				{
					case Keys.C:
						{
							if (Shift == 2)
							{
								CopyInformation();
							}
							break;
						}
					case Keys.V:
						{
							if (Shift == 2)
							{
								PasteInformation();
							}
							break;
						}
				}
				//end switch
			}
		}

		private void frmAREditBillInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				if (vsSearch.Visible == true)
				{
					vsSearch.Visible = false;
					fraSearchCriteria.Visible = true;
					fraSearch.Visible = true;
					fraGetAccount.Visible = true;
					lblSearchListInstruction.Visible = false;
					boolSearch = false;
				}
				else if (fraEditInfo.Visible)
				{
					fraEditInfo.Visible = false;
					fraSearch.Visible = true;
					fraSearchCriteria.Visible = true;
					fraGetAccount.Visible = true;
					btnFileSave.Enabled = false;
					btnProcessSearch.Enabled = true;
					btnProcessGetAccount.Text = "Process";
					lblSearchListInstruction.Visible = false;
					boolSearch = false;
				}
				else
				{
					KeyAscii = (Keys)0;
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmAREditBillInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAREditBillInfo.ScaleWidth	= 9045;
			//frmAREditBillInfo.ScaleHeight	= 7545;
			//frmAREditBillInfo.LinkTopic	= "Form1";
			//frmAREditBillInfo.LockControls	= -1  'True;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				// this just sets the columns constant values
				// edit grid
				lngColEDBillkey = 0;
				lngColEDInvoice = 1;
				lngColEDName1 = 2;
				lngColEDName2 = 3;
				// search grid
				lngColAcct = 0;
				lngColName = 1;
				lngColLocationNumber = 2;
				lngColLocation = 3;
				lngColBookPage = 4;
				lngColMapLot = 5;
				lngRowOwner = 0;
				lngRowSecondOwner = 1;
				lngRowAddress1 = 2;
				lngRowAddress2 = 3;
				lngRowAddress3 = 4;
				lngRowCity = 5;
				lngRowState = 6;
				lngRowZip = 7;
				lngRowZip4 = 8;
				lngRowReference = 9;
				lngRowControl1 = 10;
				lngRowControl2 = 11;
				lngRowControl3 = 12;
				strSearchString = "";
				cmbSearchType.SelectedIndex = 0;
				//optSearchType[2].Enabled = false;
				cmbSearchType.Items.RemoveAt(2);
				modGlobalFunctions.SetTRIOColors(this);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Form Load Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmAREditBillInfo_Resize(object sender, System.EventArgs e)
		{
			if (fraSearch.Visible == true && vsSearch.Visible == false)
			{
				ShowSearch();
			}
			else if (vsSearch.Visible == true)
			{
				FormatSearchGrid();
				// set the height of the grid
				//FC:FINAL:DSE:#480 Form redesign
				/*
                 if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                {
                    vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
                    vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
                }
                else
                {
                    vsSearch.Height = Math.Abs((this.Height - vsSearch.Top) - 1000);
                    vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                }
                */
			}
			else
			{
				AdjustGridHeight();
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInformation();
		}

		private void vsEditInfo_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolDirty = true;
		}

		private void vsEditInfo_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsEditInfo.IsCurrentCellInEditMode)
			{
				boolDirty = true;
			}
		}

		private void vsEditInfo_Enter(object sender, System.EventArgs e)
		{
			// With vsEditInfo
			// .Row = 1
			// .TabBehavior = flexTabCells
			// 
			// If .Col > 0 Then
			// .Editable = flexEDKbdMouse
			// .EditCell
			// Else
			// .Editable = flexEDNone
			// End If
			// End With
		}

		private void vsEditInfo_KeyDownEdit(object sender, KeyEventArgs e)
		{
			// Select Case KeyCode
			// Case vbKeyC
			// If Shift = 2 And Col > 0 Then
			// KeyCode = 0
			// CopyInformation Row
			// End If
			// End Select
		}
		// Private Sub vsEditInfo_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
		// Dim lngMR           As Long
		//
		// With vsEditInfo
		// lngMR = .MouseRow
		// Select Case lngMR
		// Case lngRowAddress1, lngRowAddress2, lngRowAddress3
		// .ToolTipText = ""
		// Case lngRowBookPage
		// .ToolTipText = "Book and Page"
		// Case lngRowMapLot
		// .ToolTipText = "Map and Lot"
		// Case lngRowOwner
		// .ToolTipText = ""
		// Case lngRowRef1
		// .ToolTipText = "Reference 1"
		// Case lngRowSecondOwner
		// .ToolTipText = ""
		// Case lngRowStreetName
		// .ToolTipText = ""
		// Case lngRowStreetNumber
		// .ToolTipText = ""
		// Case lngRowUseRef1OnLien
		// .ToolTipText = "Use the Ref 1 field instead of the Book and Page fields on 30 DN and Liens."
		// End Select
		// End With
		// End Sub
		private void vsEditInfo_RowColChange(object sender, System.EventArgs e)
		{
			if (vsEditInfo.Col > 0)
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsEditInfo.EditCell();
			}
			else
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsEditInfo.Row == vsEditInfo.Rows - 1)
			{
				vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void Grid_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsSearch_AfterUserResize(grid.GetFlexRowIndex(e.RowIndex), -1);
		}

		private void Grid_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsSearch_AfterUserResize(-1, grid.GetFlexColIndex(e.Column.Index));
		}

		private void vsSearch_AfterUserResize(int row, int col)
		{
			int intCT;
			int lngWid = 0;
			for (intCT = 0; intCT <= vsSearch.Cols - 1; intCT++)
			{
				lngWid += vsSearch.ColWidth(intCT);
			}
			//FC:FINAL:DSE:#480 Form redesign
			/*
            if (lngWid > vsSearch.Width)
            {
                vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
                // set the height of the grid
                if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                {
                    vsSearch.Height = ((vsSearch.Rows + 1) * vsSearch.RowHeight(0));
                    vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
                }
                else
                {
                    vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
                    vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
                }
            }
            else
            {
                // set the height of the grid
                if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                {
                    vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
                    vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
                }
                else
                {
                    vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
                    vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                }
            }
            */
		}

		private void vsSearch_BeforeSort(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (vsSearch.Col == lngColLocation || vsSearch.Col == lngColLocationNumber)
			{
				if (vsSearch.Rows > 1)
				{
					vsSearch.Select(1, lngColLocationNumber, 1, lngColLocation);
				}
			}
		}

		private void vsSearch_ClickEvent(object sender, System.EventArgs e)
		{
			txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
		}

		private void vsSearch_DblClick(object sender, System.EventArgs e)
		{
			if (vsSearch.Row > 0)
			{
				txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
				cmdGetAccountNumber_Click();
			}
		}

		private void vsSearch_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// captures the return key to accept the account highlighted in the listbox
			if (e.KeyCode == Keys.Return)
			{
				cmdGetAccountNumber_Click();
			}
		}

		private void mnuProcessClearSearch_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuProcessGetAccount_Click(object sender, System.EventArgs e)
		{
			if (fraEditInfo.Visible)
			{
				// this is a save and exit
				SaveInformation();
				Close();
			}
			else
			{
				if (Strings.Trim(txtSearch.Text) != "")
				{
					cmdSearch_Click();
				}
				else
				{
					cmdGetAccountNumber_Click();
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSearch_Click(object sender, System.EventArgs e)
		{
			if (fraGetAccount.Visible)
			{
				cmdSearch_Click();
			}
			else
			{
				// go back to that screen
				if (boolDirty)
				{
					switch (MessageBox.Show("Changes have been made.  Would you like to save the information?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								SaveInformation();
								break;
							}
						case DialogResult.No:
							{
								// do nothing
								break;
							}
						case DialogResult.Cancel:
							{
								return;
							}
					}
					//end switch
				}
				btnProcessGetAccount.Text = "Process";
				btnProcessClearSearch.Enabled = true;
				fraSearch.Visible = true;
				fraGetAccount.Visible = true;
				fraSearchCriteria.Visible = true;
				fraEditInfo.Visible = false;
			}
		}

		private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdGetAccountNumber_Click();
			}
		}

		private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
			{
				if (KeyAscii == Keys.Back)
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdSearch_Click();
			}
		}

		private void ShowSearch()
		{
			// this will align the Search Screen and Show it
			boolSearch = true;
			//fraSearch.Left = FCConvert.ToInt32((this.Width - fraSearch.Width) / 2.0);
			//fraSearch.Top = FCConvert.ToInt32((this.Height - fraSearch.Height) / 2.0);
			fraSearch.Visible = true;
		}

		private void vsSearch_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngR;
			int lngC;
			lngC = vsSearch.MouseCol;
			lngR = vsSearch.MouseRow;
			// vssearch.ToolTipText =
		}

		private void vsSearch_RowColChange(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6)) != 0)
			{
				txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
			}
		}

		private void FormatSearchGrid()
		{
			int lngWid;
			vsSearch.Cols = 2;
			// set the height of the grid
			//FC:FINAL:DSE:#480 Form redesign 
			//vsSearch.Height = FCConvert.ToInt32(this.Height * 0.8);
			//vsSearch.Width = FCConvert.ToInt32(this.Width * 0.5);
			//vsSearch.Left = FCConvert.ToInt32(((this.Width - vsSearch.Width) / 2.0));
			//vsSearch.Top = FCConvert.ToInt32(this.Height * 0.1);
			vsSearch.ExtendLastCol = true;
			lngWid = vsSearch.Width;
			// set the column widths
			vsSearch.ColWidth(lngColAcct, FCConvert.ToInt32(lngWid * 0.2));
			vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.4));
			vsSearch.ColAlignment(lngColAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.TextMatrix(0, lngColAcct, "Account");
			vsSearch.TextMatrix(0, lngColName, "Name");
			lblSearchListInstruction.Text = "Select an account by double clicking or pressing 'Enter' while the account is highlighted.";
			lblSearchListInstruction.Top = 0;
			lblSearchListInstruction.Width = lngWid;
			lblSearchListInstruction.Left = vsSearch.Left;
			lblSearchListInstruction.Visible = true;
		}

		private void ShowEditAccount_2(int lngShowAccount)
		{
			ShowEditAccount(ref lngShowAccount);
		}

		private void ShowEditAccount(ref int lngShowAccount)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will show the years that this account has been billed
				// and let the user edit the name and address information
				clsDRWrapper rsAcct = new clsDRWrapper();
				vsYearInfo.Rows = 1;
				btnFileSave.Enabled = true;
				btnProcessGetAccount.Text = "Save and Exit";
				btnProcessClearSearch.Enabled = true;
				FormatGrids();
				rsAcct.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngShowAccount) + " ORDER BY InvoiceNumber desc", modCLCalculations.strARDatabase);
				if (!rsAcct.EndOfFile())
				{
					while (!rsAcct.EndOfFile())
					{
						vsYearInfo.AddItem("");
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDBillkey, FCConvert.ToString(rsAcct.Get_Fields_Int32("ID")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDInvoice, FCConvert.ToString(rsAcct.Get_Fields_String("InvoiceNumber")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDName1, FCConvert.ToString(rsAcct.Get_Fields_String("BName")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDName2, FCConvert.ToString(rsAcct.Get_Fields_String("BName2")));
						rsAcct.MoveNext();
					}
					vsYearInfo.Select(1, 0, 1, vsYearInfo.Cols - 1);
					//FC:FINAL:DSE #i461 Event RowColChanged not fired by .Select method
					vsYearInfo_RowColChange(vsYearInfo, EventArgs.Empty);
					EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
					fraEditInfo.Visible = true;
					fraSearch.Visible = false;
					vsSearch.Visible = false;
					//FC:FINAL:DSE:#480 Search criteria group should not be visible when editing an account
					fraSearchCriteria.Visible = false;
					fraEditInfo.Text = "Edit Account " + FCConvert.ToString(lngShowAccount) + " Information";
				}
				else
				{
					MessageBox.Show("Cannot load account #" + FCConvert.ToString(lngShowAccount) + ".", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				AdjustGridHeight();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - ", "Error Showing Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatGrids()
		{
			// this will format the two grids vsYearInfo and vsEditinfo
			int wid = 0;
			wid = vsYearInfo.WidthOriginal;
			vsYearInfo.ExtendLastCol = true;
			vsYearInfo.ColWidth(lngColEDBillkey, 0);
			vsYearInfo.ColWidth(lngColEDInvoice, FCConvert.ToInt32(wid * 0.15));
			vsYearInfo.ColAlignment(lngColEDInvoice, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsYearInfo.ColWidth(lngColEDName1, FCConvert.ToInt32(wid * 0.4));
			vsYearInfo.ColWidth(lngColEDName2, FCConvert.ToInt32(wid * 0.4));
			vsYearInfo.TextMatrix(0, lngColEDInvoice, "Invoice");
			vsYearInfo.TextMatrix(0, lngColEDName1, "Owner");
			vsYearInfo.TextMatrix(0, lngColEDName2, "Second Owner");
			vsYearInfo.ColHidden(lngColEDName2, true);
			vsEditInfo.Cols = 2;
			vsEditInfo.Rows = 13;
			vsEditInfo.ColWidth(lngColEDBillkey, FCConvert.ToInt32(wid * 0.3));
			vsEditInfo.ColWidth(lngColEDInvoice, FCConvert.ToInt32(wid * 0.65));
			vsEditInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsEditInfo.TextMatrix(lngRowOwner, 0, "Owner");
			vsEditInfo.TextMatrix(lngRowSecondOwner, 0, "Second Owner");
			vsEditInfo.RowHidden(lngRowSecondOwner, true);
			vsEditInfo.TextMatrix(lngRowAddress1, 0, "Address 1");
			vsEditInfo.TextMatrix(lngRowAddress2, 0, "Address 2");
			vsEditInfo.TextMatrix(lngRowAddress3, 0, "Address 3");
			vsEditInfo.TextMatrix(lngRowCity, 0, "City");
			vsEditInfo.TextMatrix(lngRowState, 0, "State");
			vsEditInfo.TextMatrix(lngRowZip, 0, "Zip Code");
			vsEditInfo.TextMatrix(lngRowZip4, 0, "Zip 4");
			vsEditInfo.TextMatrix(lngRowReference, 0, "");
			vsEditInfo.TextMatrix(lngRowControl1, 0, "");
			vsEditInfo.TextMatrix(lngRowControl2, 0, "");
			vsEditInfo.TextMatrix(lngRowControl3, 0, "");
			//FC:FINAL:DSE #i461 Dirty flag should not be set while initializing the grid
			boolDirty = false;
		}
		// vbPorter upgrade warning: lngBK As int	OnWrite(string)
		private void EditBillInformation(int lngBK)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will show the information for the bill that has been selected
				clsDRWrapper rsAcct = new clsDRWrapper();
				clsDRWrapper rsBillType = new clsDRWrapper();
				rsAcct.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBK) + " ORDER BY InvoiceNumber desc", modCLCalculations.strARDatabase);
				if (!rsAcct.EndOfFile())
				{
					vsEditInfo.TextMatrix(lngRowOwner, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BName")));
					vsEditInfo.TextMatrix(lngRowSecondOwner, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BName2")));
					vsEditInfo.TextMatrix(lngRowAddress1, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BAddress1")));
					vsEditInfo.TextMatrix(lngRowAddress2, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BAddress2")));
					vsEditInfo.TextMatrix(lngRowAddress3, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BAddress3")));
					vsEditInfo.TextMatrix(lngRowCity, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BCity")));
					vsEditInfo.TextMatrix(lngRowState, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BState")));
					vsEditInfo.TextMatrix(lngRowZip, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BZip")));
					vsEditInfo.TextMatrix(lngRowZip4, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BZip4")));
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					rsBillType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsAcct.Get_Fields("BillType"));
					if (rsBillType.EndOfFile() != true && rsBillType.BeginningOfFile() != true)
					{
						if (Strings.Trim(FCConvert.ToString(rsBillType.Get_Fields_String("Reference"))) != "")
						{
							vsEditInfo.RowHidden(lngRowReference, false);
							vsEditInfo.TextMatrix(lngRowReference, 0, FCConvert.ToString(rsBillType.Get_Fields_String("Reference")));
							vsEditInfo.TextMatrix(lngRowReference, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Reference")));
						}
						else
						{
							vsEditInfo.RowHidden(lngRowReference, true);
						}
						if (Strings.Trim(FCConvert.ToString(rsBillType.Get_Fields_String("Control1"))) != "")
						{
							vsEditInfo.RowHidden(lngRowControl1, false);
							vsEditInfo.TextMatrix(lngRowControl1, 0, FCConvert.ToString(rsBillType.Get_Fields_String("Control1")));
							vsEditInfo.TextMatrix(lngRowControl1, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Control1")));
						}
						else
						{
							vsEditInfo.RowHidden(lngRowControl1, true);
						}
						if (Strings.Trim(FCConvert.ToString(rsBillType.Get_Fields_String("Control2"))) != "")
						{
							vsEditInfo.RowHidden(lngRowControl2, false);
							vsEditInfo.TextMatrix(lngRowControl2, 0, FCConvert.ToString(rsBillType.Get_Fields_String("Control2")));
							vsEditInfo.TextMatrix(lngRowControl2, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Control2")));
						}
						else
						{
							vsEditInfo.RowHidden(lngRowControl2, true);
						}
						if (Strings.Trim(FCConvert.ToString(rsBillType.Get_Fields_String("Control3"))) != "")
						{
							vsEditInfo.RowHidden(lngRowControl3, false);
							vsEditInfo.TextMatrix(lngRowControl3, 0, FCConvert.ToString(rsBillType.Get_Fields_String("Control3")));
							vsEditInfo.TextMatrix(lngRowControl3, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Control3")));
						}
						else
						{
							vsEditInfo.RowHidden(lngRowControl3, true);
						}
					}
					else
					{
						vsEditInfo.TextMatrix(lngRowReference, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Reference")));
						vsEditInfo.TextMatrix(lngRowControl1, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Control1")));
						vsEditInfo.TextMatrix(lngRowControl2, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Control2")));
						vsEditInfo.TextMatrix(lngRowControl3, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Control3")));
					}
				}
				else
				{
					vsEditInfo.TextMatrix(lngRowOwner, 1, "");
					vsEditInfo.TextMatrix(lngRowSecondOwner, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress1, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress2, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress3, 1, "");
					vsEditInfo.TextMatrix(lngRowCity, 1, "");
					vsEditInfo.TextMatrix(lngRowState, 1, "");
					vsEditInfo.TextMatrix(lngRowZip, 1, "");
					vsEditInfo.TextMatrix(lngRowZip4, 1, "");
					vsEditInfo.TextMatrix(lngRowReference, 1, "");
					vsEditInfo.TextMatrix(lngRowControl1, 1, "");
					vsEditInfo.TextMatrix(lngRowControl2, 1, "");
					vsEditInfo.TextMatrix(lngRowControl3, 1, "");
				}
				boolDirty = false;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Editing Bill Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsYearInfo_Enter(object sender, System.EventArgs e)
		{
			vsYearInfo.Row = 1;
			vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsYearInfo_RowColChange(object sender, System.EventArgs e)
		{
			if (vsYearInfo.Row > 0 && lngLastRow != vsYearInfo.Row)
			{
				if (boolDirty)
				{
					switch (MessageBox.Show("Changes have been made.  Would you like to save the information?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								SaveInformation();
								EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
								break;
							}
						case DialogResult.No:
							{
								EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
								break;
							}
						case DialogResult.Cancel:
							{
								// do nothing
								break;
							}
					}
					//end switch
				}
				else
				{
					EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
				}
				lngLastRow = vsYearInfo.Row;
			}
			if (vsYearInfo.Row == vsYearInfo.Rows - 1)
			{
				vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void SaveInformation()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strName1 = "";
				string strName2 = "";
				string strAddr1 = "";
				string strAddr2 = "";
				string strAddr3 = "";
				string strCity = "";
				string strState = "";
				string strZip = "";
				string strZip4 = "";
				string strReference = "";
				string strControl1 = "";
				string strControl2 = "";
				string strControl3 = "";
				clsDRWrapper rsAcct = new clsDRWrapper();
				vsEditInfo.Select(0, 0);
				// use lngLastRow to get the from from the grid, this is the last row selected and is the way that I will find the billkey
				if (lngLastRow > 0)
				{
					rsAcct.OpenRecordset("SELECT * FROM Bill WHERE ID = " + vsYearInfo.TextMatrix(lngLastRow, lngColEDBillkey), modCLCalculations.strARDatabase);
					if (!rsAcct.EndOfFile())
					{
						// this will save the information into the correct bill
						strName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowOwner, 1));
						strName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowSecondOwner, 1));
						strAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress1, 1));
						strAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress2, 1));
						strAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress3, 1));
						strCity = Strings.Trim(vsEditInfo.TextMatrix(lngRowCity, 1));
						strState = Strings.Trim(vsEditInfo.TextMatrix(lngRowState, 1));
						strZip = Strings.Trim(vsEditInfo.TextMatrix(lngRowZip, 1));
						strZip4 = Strings.Trim(vsEditInfo.TextMatrix(lngRowZip4, 1));
						strReference = Strings.Trim(vsEditInfo.TextMatrix(lngRowReference, 1));
						strControl1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowControl1, 1));
						strControl2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowControl2, 1));
						strControl3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowControl3, 1));
						rsAcct.Edit();
						rsAcct.Set_Fields("BName", strName1);
						rsAcct.Set_Fields("BName2", strName2);
						rsAcct.Set_Fields("BAddress1", strAddr1);
						rsAcct.Set_Fields("BAddress2", strAddr2);
						rsAcct.Set_Fields("BAddress3", strAddr3);
						rsAcct.Set_Fields("BCity", strCity);
						rsAcct.Set_Fields("BState", strState);
						rsAcct.Set_Fields("BZip", strZip);
						rsAcct.Set_Fields("BZip4", strZip4);
						rsAcct.Set_Fields("Reference", strReference);
						rsAcct.Set_Fields("Control1", strControl1);
						rsAcct.Set_Fields("Control2", strControl2);
						rsAcct.Set_Fields("Control3", strControl3);
						rsAcct.Update();
						vsYearInfo.TextMatrix(lngLastRow, lngColEDName1, strName1);
						vsYearInfo.TextMatrix(lngLastRow, lngColEDName2, strName2);
						modGlobalFunctions.AddCYAEntry_26("AR", "Editing Bill information.", "Bill = " + vsYearInfo.TextMatrix(lngLastRow, lngColEDBillkey));
					}
					boolDirty = false;
				}
				MessageBox.Show("Save Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Bill", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CopyInformation(int lngRow = -1)
		{
			// this will copy the information into the virtual clipboard from the grid
			strCopyName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowOwner, 1));
			strCopyName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowSecondOwner, 1));
			strCopyAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress1, 1));
			strCopyAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress2, 1));
			strCopyAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress3, 1));
			strCopyCity = Strings.Trim(vsEditInfo.TextMatrix(lngRowCity, 1));
			strCopyState = Strings.Trim(vsEditInfo.TextMatrix(lngRowState, 1));
			strCopyZip = Strings.Trim(vsEditInfo.TextMatrix(lngRowZip, 1));
			strCopyZip4 = Strings.Trim(vsEditInfo.TextMatrix(lngRowZip4, 1));
			strCopyReference = Strings.Trim(vsEditInfo.TextMatrix(lngRowReference, 1));
			strCopyControl1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowControl1, 1));
			strCopyControl2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowControl2, 1));
			strCopyControl3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowControl3, 1));
		}

		private void PasteInformation()
		{
			// this will paste the information into the grid from the virtual clipboard
			if (strCopyName1 != "" && (strCopyName2 != "" || strCopyAddr1 != "" || strCopyAddr2 != "" || strCopyAddr3 != ""))
			{
				vsEditInfo.TextMatrix(lngRowOwner, 1, strCopyName1);
				vsEditInfo.TextMatrix(lngRowSecondOwner, 1, strCopyName2);
				vsEditInfo.TextMatrix(lngRowAddress1, 1, strCopyAddr1);
				vsEditInfo.TextMatrix(lngRowAddress2, 1, strCopyAddr2);
				vsEditInfo.TextMatrix(lngRowAddress3, 1, strCopyAddr3);
				vsEditInfo.TextMatrix(lngRowCity, 1, strCopyCity);
				vsEditInfo.TextMatrix(lngRowState, 1, strCopyState);
				vsEditInfo.TextMatrix(lngRowZip, 1, strCopyZip);
				vsEditInfo.TextMatrix(lngRowZip4, 1, strCopyZip4);
				vsEditInfo.TextMatrix(lngRowReference, 1, strCopyReference);
				vsEditInfo.TextMatrix(lngRowControl1, 1, strCopyControl1);
				vsEditInfo.TextMatrix(lngRowControl2, 1, strCopyControl2);
				vsEditInfo.TextMatrix(lngRowControl3, 1, strCopyControl3);
			}
		}

		private void AdjustGridHeight()
		{
			//vsEditInfo.Height = FCConvert.ToInt32((vsEditInfo.Rows * vsEditInfo.RowHeight(0)) + (vsEditInfo.RowHeight(0) * 0.15));
			//vsYearInfo.Height = FCConvert.ToInt32((8 * vsYearInfo.RowHeight(0)) + (vsYearInfo.RowHeight(0) * 0.15));
		}
	}
}
