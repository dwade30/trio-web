﻿namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptCustomerList.
	/// </summary>
	partial class rptCustomerList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomerList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldCustomer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDataEntryMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.linDataEntryMessage = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.rtfDataEntryMessage = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.lblMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.linMessage = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.rtfMessage = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.lblBillMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.linBillMessage = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.rtfBillMessage = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.fldPhone1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPhone2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPhone3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDataEntryMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhone1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhone2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhone3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCustomer,
				this.fldName,
				this.fldStatus,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.lblDataEntryMessage,
				this.linDataEntryMessage,
				this.rtfDataEntryMessage,
				this.lblMessage,
				this.linMessage,
				this.rtfMessage,
				this.lblBillMessage,
				this.linBillMessage,
				this.rtfBillMessage,
				this.fldPhone1,
				this.fldEmail,
				this.fldPhone2,
				this.fldPhone3,
				this.Line2
			});
			this.Detail.Height = 1.520833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldCustomer
			// 
			this.fldCustomer.CanShrink = true;
			this.fldCustomer.Height = 0.1875F;
			this.fldCustomer.Left = 0.03125F;
			this.fldCustomer.Name = "fldCustomer";
			this.fldCustomer.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCustomer.Text = "Field1";
			this.fldCustomer.Top = 0.0625F;
			this.fldCustomer.Width = 0.375F;
			// 
			// fldName
			// 
			this.fldName.CanShrink = true;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.46875F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldName.Text = "Field2";
			this.fldName.Top = 0.0625F;
			this.fldName.Width = 2.40625F;
			// 
			// fldStatus
			// 
			this.fldStatus.CanShrink = true;
			this.fldStatus.Height = 0.1875F;
			this.fldStatus.Left = 5.03125F;
			this.fldStatus.Name = "fldStatus";
			this.fldStatus.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldStatus.Text = "Field4";
			this.fldStatus.Top = 0.0625F;
			this.fldStatus.Width = 0.4375F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.CanShrink = true;
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 0.46875F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAddress1.Text = "Field1";
			this.fldAddress1.Top = 0.25F;
			this.fldAddress1.Width = 2.40625F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.CanShrink = true;
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 0.46875F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAddress2.Text = "Field1";
			this.fldAddress2.Top = 0.4375F;
			this.fldAddress2.Width = 2.40625F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.CanShrink = true;
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 0.46875F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAddress3.Text = "Field1";
			this.fldAddress3.Top = 0.625F;
			this.fldAddress3.Width = 2.40625F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.CanShrink = true;
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 0.46875F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAddress4.Text = "Field1";
			this.fldAddress4.Top = 0.8125F;
			this.fldAddress4.Width = 2.40625F;
			// 
			// lblDataEntryMessage
			// 
			this.lblDataEntryMessage.CanShrink = true;
			this.lblDataEntryMessage.Height = 0.19F;
			this.lblDataEntryMessage.Left = 0.09375F;
			this.lblDataEntryMessage.Name = "lblDataEntryMessage";
			this.lblDataEntryMessage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.lblDataEntryMessage.Text = "Data Entry Message";
			this.lblDataEntryMessage.Top = 1.0625F;
			this.lblDataEntryMessage.Width = 1.15625F;
			// 
			// linDataEntryMessage
			// 
			this.linDataEntryMessage.Height = 0F;
			this.linDataEntryMessage.Left = 0.09375F;
			this.linDataEntryMessage.LineWeight = 1F;
			this.linDataEntryMessage.Name = "linDataEntryMessage";
			this.linDataEntryMessage.Top = 1.21875F;
			this.linDataEntryMessage.Width = 1.15625F;
			this.linDataEntryMessage.X1 = 0.09375F;
			this.linDataEntryMessage.X2 = 1.25F;
			this.linDataEntryMessage.Y1 = 1.21875F;
			this.linDataEntryMessage.Y2 = 1.21875F;
			// 
			// rtfDataEntryMessage
			// 
			this.rtfDataEntryMessage.AutoReplaceFields = true;
			this.rtfDataEntryMessage.Font = new System.Drawing.Font("Arial", 10F);
			this.rtfDataEntryMessage.Height = 0.1875F;
			this.rtfDataEntryMessage.Left = 0.125F;
			this.rtfDataEntryMessage.Name = "rtfDataEntryMessage";
			this.rtfDataEntryMessage.RTF = resources.GetString("rtfDataEntryMessage.RTF");
			this.rtfDataEntryMessage.Top = 1.25F;
			this.rtfDataEntryMessage.Width = 2.46875F;
			// 
			// lblMessage
			// 
			this.lblMessage.CanShrink = true;
			this.lblMessage.Height = 0.19F;
			this.lblMessage.Left = 5.34375F;
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.lblMessage.Text = "Customer Message";
			this.lblMessage.Top = 1.0625F;
			this.lblMessage.Width = 1.15625F;
			// 
			// linMessage
			// 
			this.linMessage.Height = 0F;
			this.linMessage.Left = 5.34375F;
			this.linMessage.LineWeight = 1F;
			this.linMessage.Name = "linMessage";
			this.linMessage.Top = 1.21875F;
			this.linMessage.Width = 1.15625F;
			this.linMessage.X1 = 5.34375F;
			this.linMessage.X2 = 6.5F;
			this.linMessage.Y1 = 1.21875F;
			this.linMessage.Y2 = 1.21875F;
			// 
			// rtfMessage
			// 
			this.rtfMessage.AutoReplaceFields = true;
			this.rtfMessage.Font = new System.Drawing.Font("Arial", 10F);
			this.rtfMessage.Height = 0.1875F;
			this.rtfMessage.Left = 5.375F;
			this.rtfMessage.Name = "rtfMessage";
			this.rtfMessage.RTF = resources.GetString("rtfMessage.RTF");
			this.rtfMessage.Top = 1.25F;
			this.rtfMessage.Width = 2.4375F;
			// 
			// lblBillMessage
			// 
			this.lblBillMessage.CanShrink = true;
			this.lblBillMessage.Height = 0.19F;
			this.lblBillMessage.Left = 2.6875F;
			this.lblBillMessage.Name = "lblBillMessage";
			this.lblBillMessage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.lblBillMessage.Text = "Bill Message";
			this.lblBillMessage.Top = 1.0625F;
			this.lblBillMessage.Width = 1.15625F;
			// 
			// linBillMessage
			// 
			this.linBillMessage.Height = 0F;
			this.linBillMessage.Left = 2.6875F;
			this.linBillMessage.LineWeight = 1F;
			this.linBillMessage.Name = "linBillMessage";
			this.linBillMessage.Top = 1.21875F;
			this.linBillMessage.Width = 1.15625F;
			this.linBillMessage.X1 = 2.6875F;
			this.linBillMessage.X2 = 3.84375F;
			this.linBillMessage.Y1 = 1.21875F;
			this.linBillMessage.Y2 = 1.21875F;
			// 
			// rtfBillMessage
			// 
			this.rtfBillMessage.AutoReplaceFields = true;
			this.rtfBillMessage.Font = new System.Drawing.Font("Arial", 10F);
			this.rtfBillMessage.Height = 0.1875F;
			this.rtfBillMessage.Left = 2.71875F;
			this.rtfBillMessage.Name = "rtfBillMessage";
			this.rtfBillMessage.RTF = resources.GetString("rtfBillMessage.RTF");
			this.rtfBillMessage.Top = 1.25F;
			this.rtfBillMessage.Width = 2.46875F;
			// 
			// fldPhone1
			// 
			this.fldPhone1.CanShrink = true;
			this.fldPhone1.Height = 0.1875F;
			this.fldPhone1.Left = 5.65625F;
			this.fldPhone1.Name = "fldPhone1";
			this.fldPhone1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPhone1.Text = "Field1";
			this.fldPhone1.Top = 0.0625F;
			this.fldPhone1.Width = 1.96875F;
			// 
			// fldEmail
			// 
			this.fldEmail.CanShrink = true;
			this.fldEmail.Height = 0.1875F;
			this.fldEmail.Left = 3F;
			this.fldEmail.Name = "fldEmail";
			this.fldEmail.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldEmail.Text = "Field1";
			this.fldEmail.Top = 0.0625F;
			this.fldEmail.Width = 1.90625F;
			// 
			// fldPhone2
			// 
			this.fldPhone2.CanShrink = true;
			this.fldPhone2.Height = 0.1875F;
			this.fldPhone2.Left = 5.65625F;
			this.fldPhone2.Name = "fldPhone2";
			this.fldPhone2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPhone2.Text = "Field1";
			this.fldPhone2.Top = 0.25F;
			this.fldPhone2.Width = 1.96875F;
			// 
			// fldPhone3
			// 
			this.fldPhone3.CanShrink = true;
			this.fldPhone3.Height = 0.1875F;
			this.fldPhone3.Left = 5.65625F;
			this.fldPhone3.Name = "fldPhone3";
			this.fldPhone3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPhone3.Text = "Field1";
			this.fldPhone3.Top = 0.4375F;
			this.fldPhone3.Width = 1.96875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.46875F;
			this.Line2.Width = 7.75F;
			this.Line2.X1 = 0.125F;
			this.Line2.X2 = 7.875F;
			this.Line2.Y1 = 1.46875F;
			this.Line2.Y2 = 1.46875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Label8,
				this.Label9,
				this.Label11,
				this.Line1,
				this.Field1,
				this.lblEmail
			});
			this.PageHeader.Height = 0.7083333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Customer List";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.15625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.65625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.03125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "Cust";
			this.Label8.Top = 0.53125F;
			this.Label8.Width = 0.375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.46875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label9.Text = "Name";
			this.Label9.Top = 0.53125F;
			this.Label9.Width = 2.40625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.03125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label11.Text = "Status";
			this.Label11.Top = 0.53125F;
			this.Label11.Width = 0.4375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.6875F;
			this.Line1.Width = 7.90625F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.9375F;
			this.Line1.Y1 = 0.6875F;
			this.Line1.Y2 = 0.6875F;
			// 
			// Field1
			// 
			this.Field1.CanShrink = true;
			this.Field1.Height = 0.19F;
			this.Field1.Left = 5.65625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Field1.Text = "Phone Number";
			this.Field1.Top = 0.53125F;
			this.Field1.Width = 1F;
			// 
			// lblEmail
			// 
			this.lblEmail.CanShrink = true;
			this.lblEmail.Height = 0.19F;
			this.lblEmail.Left = 3.03125F;
			this.lblEmail.Name = "lblEmail";
			this.lblEmail.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.lblEmail.Text = "Email";
			this.lblEmail.Top = 0.53125F;
			this.lblEmail.Width = 0.40625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptCustomerList
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptCustomerList_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			((System.ComponentModel.ISupportInitialize)(this.fldCustomer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDataEntryMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhone1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhone2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhone3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDataEntryMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Line linDataEntryMessage;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtfDataEntryMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Line linMessage;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtfMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblBillMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Line linBillMessage;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtfBillMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhone1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhone2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhone3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblEmail;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
