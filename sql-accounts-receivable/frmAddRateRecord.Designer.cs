﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmAddRateRecord.
	/// </summary>
	partial class frmAddRateRecord : BaseForm
	{
		public fecherFoundation.FCTextBox txtRateOrFlatAmount;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCFrame fraRateRecType;
		public fecherFoundation.FCComboBox cboBillType;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboMonth;
		public fecherFoundation.FCComboBox cboYear;
		public Global.T2KDateBox t2kBilldate;
		public Global.T2KDateBox txtEndDate;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtIntDate;
		public fecherFoundation.FCLabel lblBillDate;
		public fecherFoundation.FCLabel lblIntRateOrFlatAmount;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel lblStartDateTitle;
		public fecherFoundation.FCLabel lblEndDateTitle;
		public fecherFoundation.FCLabel lblIntDateTitle;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtRateOrFlatAmount = new fecherFoundation.FCTextBox();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.fraRateRecType = new fecherFoundation.FCFrame();
			this.cboBillType = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cboMonth = new fecherFoundation.FCComboBox();
			this.cboYear = new fecherFoundation.FCComboBox();
			this.t2kBilldate = new Global.T2KDateBox();
			this.txtEndDate = new Global.T2KDateBox();
			this.txtStartDate = new Global.T2KDateBox();
			this.txtIntDate = new Global.T2KDateBox();
			this.lblBillDate = new fecherFoundation.FCLabel();
			this.lblIntRateOrFlatAmount = new fecherFoundation.FCLabel();
			this.Label24 = new fecherFoundation.FCLabel();
			this.lblStartDateTitle = new fecherFoundation.FCLabel();
			this.lblEndDateTitle = new fecherFoundation.FCLabel();
			this.lblIntDateTitle = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRateRecType)).BeginInit();
			this.fraRateRecType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kBilldate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIntDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 450);
			this.BottomPanel.Size = new System.Drawing.Size(814, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtRateOrFlatAmount);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.fraRateRecType);
			this.ClientArea.Controls.Add(this.t2kBilldate);
			this.ClientArea.Controls.Add(this.txtEndDate);
			this.ClientArea.Controls.Add(this.txtStartDate);
			this.ClientArea.Controls.Add(this.txtIntDate);
			this.ClientArea.Controls.Add(this.lblBillDate);
			this.ClientArea.Controls.Add(this.lblIntRateOrFlatAmount);
			this.ClientArea.Controls.Add(this.Label24);
			this.ClientArea.Controls.Add(this.lblStartDateTitle);
			this.ClientArea.Controls.Add(this.lblEndDateTitle);
			this.ClientArea.Controls.Add(this.lblIntDateTitle);
			this.ClientArea.Size = new System.Drawing.Size(814, 390);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(814, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(224, 30);
			this.HeaderText.Text = "Add A Rate Record";
			// 
			// txtRateOrFlatAmount
			// 
			this.txtRateOrFlatAmount.BackColor = System.Drawing.SystemColors.Window;
			this.txtRateOrFlatAmount.Location = new System.Drawing.Point(192, 103);
			this.txtRateOrFlatAmount.Name = "txtRateOrFlatAmount";
			this.txtRateOrFlatAmount.Size = new System.Drawing.Size(97, 40);
			this.txtRateOrFlatAmount.TabIndex = 3;
			this.txtRateOrFlatAmount.Enter += new System.EventHandler(this.txtRateOrFlatAmount_Enter);
			this.txtRateOrFlatAmount.TextChanged += new System.EventHandler(this.txtRateOrFlatAmount_TextChanged);
			// 
			// txtDescription
			// 
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Location = new System.Drawing.Point(195, 30);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(583, 40);
			this.txtDescription.TabIndex = 1;
			this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
			// 
			// fraRateRecType
			// 
			this.fraRateRecType.AppearanceKey = "groupBoxLeftBorder";
			this.fraRateRecType.Controls.Add(this.cboBillType);
			this.fraRateRecType.Controls.Add(this.Frame1);
			this.fraRateRecType.Location = new System.Drawing.Point(30, 163);
			this.fraRateRecType.Name = "fraRateRecType";
			this.fraRateRecType.Size = new System.Drawing.Size(377, 200);
			this.fraRateRecType.TabIndex = 4;
			this.fraRateRecType.Text = "Bill Type";
			// 
			// cboBillType
			// 
			this.cboBillType.BackColor = System.Drawing.SystemColors.Window;
			this.cboBillType.Location = new System.Drawing.Point(20, 30);
			this.cboBillType.Name = "cboBillType";
			this.cboBillType.Size = new System.Drawing.Size(340, 40);
			this.cboBillType.SelectedIndexChanged += new System.EventHandler(this.cboBillType_SelectedIndexChanged);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cboMonth);
			this.Frame1.Controls.Add(this.cboYear);
			this.Frame1.Location = new System.Drawing.Point(20, 90);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(340, 90);
			this.Frame1.TabIndex = 1;
			this.Frame1.Text = "Billing Period";
			// 
			// cboMonth
			// 
			this.cboMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
			this.cboMonth.Location = new System.Drawing.Point(20, 30);
			this.cboMonth.Name = "cboMonth";
			this.cboMonth.Size = new System.Drawing.Size(140, 40);
			// 
			// cboYear
			// 
			this.cboYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboYear.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
			this.cboYear.Location = new System.Drawing.Point(180, 30);
			this.cboYear.Name = "cboYear";
			this.cboYear.Size = new System.Drawing.Size(140, 40);
			this.cboYear.TabIndex = 1;
			// 
			// t2kBilldate
			// 
			this.t2kBilldate.Location = new System.Drawing.Point(648, 103);
			this.t2kBilldate.MaxLength = 10;
			this.t2kBilldate.Name = "t2kBilldate";
			this.t2kBilldate.Size = new System.Drawing.Size(130, 22);
			this.t2kBilldate.TabIndex = 6;
			this.t2kBilldate.TextChanged += new System.EventHandler(this.t2kBilldate_Change);
			this.t2kBilldate.Validating += new System.ComponentModel.CancelEventHandler(this.t2kBilldate_Validate);
			// 
			// txtEndDate
			// 
			this.txtEndDate.Location = new System.Drawing.Point(648, 223);
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.Size = new System.Drawing.Size(130, 22);
			this.txtEndDate.TabIndex = 10;
			this.txtEndDate.TextChanged += new System.EventHandler(this.txtEndDate_Change);
			// 
			// txtStartDate
			// 
			this.txtStartDate.Location = new System.Drawing.Point(648, 163);
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(130, 22);
			this.txtStartDate.TabIndex = 8;
			this.txtStartDate.TextChanged += new System.EventHandler(this.txtStartDate_Change);
			// 
			// txtIntDate
			// 
			this.txtIntDate.Location = new System.Drawing.Point(648, 283);
			this.txtIntDate.MaxLength = 10;
			this.txtIntDate.Name = "txtIntDate";
			this.txtIntDate.Size = new System.Drawing.Size(130, 22);
			this.txtIntDate.TabIndex = 12;
			this.txtIntDate.TextChanged += new System.EventHandler(this.txtIntDate_Change);
			this.txtIntDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtIntDate_Validate);
			// 
			// lblBillDate
			// 
			this.lblBillDate.Location = new System.Drawing.Point(456, 117);
			this.lblBillDate.Name = "lblBillDate";
			this.lblBillDate.Size = new System.Drawing.Size(124, 25);
			this.lblBillDate.TabIndex = 5;
			this.lblBillDate.Text = "BILL DATE";
			// 
			// lblIntRateOrFlatAmount
			// 
			this.lblIntRateOrFlatAmount.Location = new System.Drawing.Point(30, 117);
			this.lblIntRateOrFlatAmount.Name = "lblIntRateOrFlatAmount";
			this.lblIntRateOrFlatAmount.Size = new System.Drawing.Size(139, 25);
			this.lblIntRateOrFlatAmount.TabIndex = 2;
			this.lblIntRateOrFlatAmount.Text = "INTEREST RATE";
			// 
			// Label24
			// 
			this.Label24.Location = new System.Drawing.Point(30, 44);
			this.Label24.Name = "Label24";
			this.Label24.Size = new System.Drawing.Size(141, 20);
			this.Label24.Text = "DESCRIPTION / TITLE";
			// 
			// lblStartDateTitle
			// 
			this.lblStartDateTitle.Location = new System.Drawing.Point(456, 177);
			this.lblStartDateTitle.Name = "lblStartDateTitle";
			this.lblStartDateTitle.Size = new System.Drawing.Size(124, 25);
			this.lblStartDateTitle.TabIndex = 7;
			this.lblStartDateTitle.Text = "START DATE";
			// 
			// lblEndDateTitle
			// 
			this.lblEndDateTitle.Location = new System.Drawing.Point(456, 237);
			this.lblEndDateTitle.Name = "lblEndDateTitle";
			this.lblEndDateTitle.Size = new System.Drawing.Size(124, 25);
			this.lblEndDateTitle.TabIndex = 9;
			this.lblEndDateTitle.Text = "END DATE";
			// 
			// lblIntDateTitle
			// 
			this.lblIntDateTitle.Location = new System.Drawing.Point(456, 297);
			this.lblIntDateTitle.Name = "lblIntDateTitle";
			this.lblIntDateTitle.Size = new System.Drawing.Size(146, 25);
			this.lblIntDateTitle.TabIndex = 11;
			this.lblIntDateTitle.Text = "INTEREST START DATE";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSave,
            this.mnuFileSaveExit,
            this.mnuSepar,
            this.mnuFileExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 1;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Exit";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 2;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 3;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(334, 34);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 40);
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmAddRateRecord
			// 
			this.ClientSize = new System.Drawing.Size(814, 546);
			this.KeyPreview = true;
			this.Name = "frmAddRateRecord";
			this.Text = "Add A Rate Record";
			this.Load += new System.EventHandler(this.frmAddRateRecord_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAddRateRecord_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAddRateRecord_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRateRecType)).EndInit();
			this.fraRateRecType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.t2kBilldate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIntDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
