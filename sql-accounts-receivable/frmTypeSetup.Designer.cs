﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmTypeSetup.
	/// </summary>
	partial class frmTypeSetup : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkChanges;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkMandatory;
		public fecherFoundation.FCFrame fraNewType;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdNewCode;
		public fecherFoundation.FCTextBox txtCode;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCPanel fraReceiptList;
		public fecherFoundation.FCComboBox cboPayee;
		public fecherFoundation.FCFrame fraDefaultAccount;
		public FCGrid txtReceivableAccount;
		public FCGrid txtDefaultAccount;
		public FCGrid txtOverrideIntAccount;
		public FCGrid txtOverrideSalesTaxAccount;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblDefaultAccount;
		public fecherFoundation.FCCheckBox chkChanges_0;
		public fecherFoundation.FCCheckBox chkChanges_1;
		public fecherFoundation.FCCheckBox chkChanges_2;
		public fecherFoundation.FCCheckBox chkChanges_3;
		public FCGrid vsBill;
		public fecherFoundation.FCPanel fraOptions;
		public fecherFoundation.FCComboBox cboInterestStartDate;
		public fecherFoundation.FCCheckBox chkSalesTax;
		public fecherFoundation.FCPanel fraPerDiem;
		public Global.T2KBackFillDecimal txtPerDiem;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCComboBox cboInterestMethod;
		public fecherFoundation.FCPanel fraFlat;
		public Global.T2KBackFillDecimal txtFlatAmount;
		public fecherFoundation.FCComboBox cboFrequencyFirstMonth;
		public fecherFoundation.FCComboBox cboFrequencyCode;
		public fecherFoundation.FCTextBox txtTitle;
		public fecherFoundation.FCComboBox cmbType;
		public FCGrid cmbCode;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCCheckBox chkMandatory_3;
		public fecherFoundation.FCCheckBox chkMandatory_2;
		public fecherFoundation.FCCheckBox chkMandatory_1;
		public fecherFoundation.FCCheckBox chkMandatory_0;
		public fecherFoundation.FCTextBox txtCTRL3Title;
		public fecherFoundation.FCTextBox txtRefTitle;
		public fecherFoundation.FCTextBox txtCTRL1Title;
		public fecherFoundation.FCTextBox txtCTRL2Title;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel lblAccountTitle2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
            this.fraNewType = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdNewCode = new fecherFoundation.FCButton();
            this.txtCode = new fecherFoundation.FCTextBox();
            this.Label15 = new fecherFoundation.FCLabel();
            this.fraReceiptList = new fecherFoundation.FCPanel();
            this.cboPayee = new fecherFoundation.FCComboBox();
            this.fraDefaultAccount = new fecherFoundation.FCFrame();
            this.txtReceivableAccount = new fecherFoundation.FCGrid();
            this.txtDefaultAccount = new fecherFoundation.FCGrid();
            this.txtOverrideIntAccount = new fecherFoundation.FCGrid();
            this.txtOverrideSalesTaxAccount = new fecherFoundation.FCGrid();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblDefaultAccount = new fecherFoundation.FCLabel();
            this.chkChanges_0 = new fecherFoundation.FCCheckBox();
            this.chkChanges_1 = new fecherFoundation.FCCheckBox();
            this.chkChanges_2 = new fecherFoundation.FCCheckBox();
            this.chkChanges_3 = new fecherFoundation.FCCheckBox();
            this.vsBill = new fecherFoundation.FCGrid();
            this.fraOptions = new fecherFoundation.FCPanel();
            this.fraFlat = new fecherFoundation.FCPanel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.txtFlatAmount = new Global.T2KBackFillDecimal();
            this.fraPerDiem = new fecherFoundation.FCPanel();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.txtPerDiem = new Global.T2KBackFillDecimal();
            this.Label12 = new fecherFoundation.FCLabel();
            this.cboInterestMethod = new fecherFoundation.FCComboBox();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.cboFrequencyFirstMonth = new fecherFoundation.FCComboBox();
            this.cboInterestStartDate = new fecherFoundation.FCComboBox();
            this.cboFrequencyCode = new fecherFoundation.FCComboBox();
            this.chkSalesTax = new fecherFoundation.FCCheckBox();
            this.txtTitle = new fecherFoundation.FCTextBox();
            this.cmbType = new fecherFoundation.FCComboBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.cmbCode = new fecherFoundation.FCGrid();
            this.chkMandatory_3 = new fecherFoundation.FCCheckBox();
            this.chkMandatory_2 = new fecherFoundation.FCCheckBox();
            this.chkMandatory_1 = new fecherFoundation.FCCheckBox();
            this.chkMandatory_0 = new fecherFoundation.FCCheckBox();
            this.txtCTRL3Title = new fecherFoundation.FCTextBox();
            this.txtRefTitle = new fecherFoundation.FCTextBox();
            this.txtCTRL1Title = new fecherFoundation.FCTextBox();
            this.txtCTRL2Title = new fecherFoundation.FCTextBox();
            this.Label14 = new fecherFoundation.FCLabel();
            this.lblAccountTitle2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblAccountTitle = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcessSave = new fecherFoundation.FCButton();
            this.btnFileForward = new fecherFoundation.FCButton();
            this.btnFileBack = new fecherFoundation.FCButton();
            this.btnProcessDelete = new fecherFoundation.FCButton();
            this.btnProcessNew = new fecherFoundation.FCButton();
            this.btnProcessSaveExit = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraNewType)).BeginInit();
            this.fraNewType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceiptList)).BeginInit();
            this.fraReceiptList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultAccount)).BeginInit();
            this.fraDefaultAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceivableAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideIntAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideSalesTaxAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOptions)).BeginInit();
            this.fraOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFlat)).BeginInit();
            this.fraFlat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFlatAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPerDiem)).BeginInit();
            this.fraPerDiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPerDiem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMandatory_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMandatory_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMandatory_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMandatory_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileForward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSaveExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcessSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 574);
            this.BottomPanel.Size = new System.Drawing.Size(1038, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraNewType);
            this.ClientArea.Controls.Add(this.fraReceiptList);
            this.ClientArea.Size = new System.Drawing.Size(1038, 514);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnProcessNew);
            this.TopPanel.Controls.Add(this.btnProcessDelete);
            this.TopPanel.Controls.Add(this.btnFileBack);
            this.TopPanel.Controls.Add(this.btnFileForward);
            this.TopPanel.Controls.Add(this.btnProcessSave);
            this.TopPanel.Size = new System.Drawing.Size(1038, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessSave, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileForward, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileBack, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessNew, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(138, 30);
            this.HeaderText.Text = "Type Setup";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // fraNewType
            // 
            this.fraNewType.Controls.Add(this.cmdCancel);
            this.fraNewType.Controls.Add(this.cmdNewCode);
            this.fraNewType.Controls.Add(this.txtCode);
            this.fraNewType.Controls.Add(this.Label15);
            this.fraNewType.Location = new System.Drawing.Point(635, 332);
            this.fraNewType.Name = "fraNewType";
            this.fraNewType.Size = new System.Drawing.Size(293, 91);
            this.fraNewType.TabIndex = 28;
            this.fraNewType.Text = "New Type Code";
            this.ToolTip1.SetToolTip(this.fraNewType, null);
            this.fraNewType.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "toolbarButton";
            this.cmdCancel.Location = new System.Drawing.Point(104, 90);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(60, 23);
            this.cmdCancel.TabIndex = 22;
            this.cmdCancel.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdCancel, null);
            this.cmdCancel.Visible = false;
            this.cmdCancel.Enter += new System.EventHandler(this.cmdCancel_Enter);
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdNewCode
            // 
            this.cmdNewCode.AppearanceKey = "toolbarButton";
            this.cmdNewCode.Location = new System.Drawing.Point(30, 90);
            this.cmdNewCode.Name = "cmdNewCode";
            this.cmdNewCode.Size = new System.Drawing.Size(60, 23);
            this.cmdNewCode.TabIndex = 21;
            this.cmdNewCode.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdNewCode, null);
            this.cmdNewCode.Visible = false;
            this.cmdNewCode.Enter += new System.EventHandler(this.cmdNewCode_Enter);
            this.cmdNewCode.Click += new System.EventHandler(this.cmdNewCode_Click);
            // 
            // txtCode
            // 
            this.txtCode.AutoSize = false;
            this.txtCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode.LinkItem = null;
            this.txtCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCode.LinkTopic = null;
            this.txtCode.Location = new System.Drawing.Point(123, 30);
            this.txtCode.MaxLength = 3;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(150, 40);
            this.txtCode.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.txtCode, null);
            this.txtCode.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCode_KeyDown);
            this.txtCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCode_KeyPress);
            this.txtCode.Enter += new System.EventHandler(this.txtCode_Enter);
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(30, 44);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(47, 16);
            this.Label15.TabIndex = 29;
            this.Label15.Text = "CODE";
            this.ToolTip1.SetToolTip(this.Label15, null);
            // 
            // fraReceiptList
            // 
            this.fraReceiptList.Controls.Add(this.cboPayee);
            this.fraReceiptList.Controls.Add(this.fraDefaultAccount);
            this.fraReceiptList.Controls.Add(this.chkChanges_0);
            this.fraReceiptList.Controls.Add(this.chkChanges_1);
            this.fraReceiptList.Controls.Add(this.chkChanges_2);
            this.fraReceiptList.Controls.Add(this.chkChanges_3);
            this.fraReceiptList.Controls.Add(this.vsBill);
            this.fraReceiptList.Controls.Add(this.fraOptions);
            this.fraReceiptList.Controls.Add(this.chkMandatory_3);
            this.fraReceiptList.Controls.Add(this.chkMandatory_2);
            this.fraReceiptList.Controls.Add(this.chkMandatory_1);
            this.fraReceiptList.Controls.Add(this.chkMandatory_0);
            this.fraReceiptList.Controls.Add(this.txtCTRL3Title);
            this.fraReceiptList.Controls.Add(this.txtRefTitle);
            this.fraReceiptList.Controls.Add(this.txtCTRL1Title);
            this.fraReceiptList.Controls.Add(this.txtCTRL2Title);
            this.fraReceiptList.Controls.Add(this.Label14);
            this.fraReceiptList.Controls.Add(this.lblAccountTitle2);
            this.fraReceiptList.Controls.Add(this.Label3);
            this.fraReceiptList.Controls.Add(this.lblAccountTitle);
            this.fraReceiptList.Controls.Add(this.Label8);
            this.fraReceiptList.Controls.Add(this.Label6);
            this.fraReceiptList.Controls.Add(this.Label7);
            this.fraReceiptList.Controls.Add(this.Label13);
            this.fraReceiptList.Controls.Add(this.Label11);
            this.fraReceiptList.Controls.Add(this.Label10);
            this.fraReceiptList.Controls.Add(this.Label9);
            this.fraReceiptList.Location = new System.Drawing.Point(0, 0);
            this.fraReceiptList.Name = "fraReceiptList";
            this.fraReceiptList.Size = new System.Drawing.Size(1012, 910);
            this.fraReceiptList.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.fraReceiptList, null);
            // 
            // cboPayee
            // 
            this.cboPayee.AutoSize = false;
            this.cboPayee.BackColor = System.Drawing.SystemColors.Window;
            this.cboPayee.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPayee.FormattingEnabled = true;
            this.cboPayee.Location = new System.Drawing.Point(734, 30);
            this.cboPayee.Name = "cboPayee";
            this.cboPayee.Size = new System.Drawing.Size(270, 40);
            this.cboPayee.TabIndex = 57;
            this.ToolTip1.SetToolTip(this.cboPayee, null);
            // 
            // fraDefaultAccount
            // 
            this.fraDefaultAccount.Controls.Add(this.txtReceivableAccount);
            this.fraDefaultAccount.Controls.Add(this.txtDefaultAccount);
            this.fraDefaultAccount.Controls.Add(this.txtOverrideIntAccount);
            this.fraDefaultAccount.Controls.Add(this.txtOverrideSalesTaxAccount);
            this.fraDefaultAccount.Controls.Add(this.Label17);
            this.fraDefaultAccount.Controls.Add(this.Label16);
            this.fraDefaultAccount.Controls.Add(this.Label4);
            this.fraDefaultAccount.Controls.Add(this.lblDefaultAccount);
            this.fraDefaultAccount.Location = new System.Drawing.Point(30, 733);
            this.fraDefaultAccount.Name = "fraDefaultAccount";
            this.fraDefaultAccount.Size = new System.Drawing.Size(898, 130);
            this.fraDefaultAccount.TabIndex = 41;
            this.ToolTip1.SetToolTip(this.fraDefaultAccount, null);
            // 
            // txtReceivableAccount
            // 
            this.txtReceivableAccount.AllowSelection = false;
            this.txtReceivableAccount.AllowUserToResizeColumns = false;
            this.txtReceivableAccount.AllowUserToResizeRows = false;
            this.txtReceivableAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.txtReceivableAccount.BackColorAlternate = System.Drawing.Color.Empty;
            this.txtReceivableAccount.BackColorBkg = System.Drawing.Color.Empty;
            this.txtReceivableAccount.BackColorFixed = System.Drawing.Color.Empty;
            this.txtReceivableAccount.BackColorSel = System.Drawing.Color.Empty;
            this.txtReceivableAccount.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.txtReceivableAccount.Cols = 1;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.txtReceivableAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.txtReceivableAccount.ColumnHeadersHeight = 30;
            this.txtReceivableAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.txtReceivableAccount.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.txtReceivableAccount.DefaultCellStyle = dataGridViewCellStyle2;
            this.txtReceivableAccount.DragIcon = null;
            this.txtReceivableAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.txtReceivableAccount.ExtendLastCol = true;
            this.txtReceivableAccount.FixedCols = 0;
            this.txtReceivableAccount.FixedRows = 0;
            this.txtReceivableAccount.ForeColorFixed = System.Drawing.Color.Empty;
            this.txtReceivableAccount.FrozenCols = 0;
            this.txtReceivableAccount.GridColor = System.Drawing.Color.Empty;
            this.txtReceivableAccount.GridColorFixed = System.Drawing.Color.Empty;
            this.txtReceivableAccount.Location = new System.Drawing.Point(237, 30);
            this.txtReceivableAccount.Name = "txtReceivableAccount";
            this.txtReceivableAccount.OutlineCol = 0;
            this.txtReceivableAccount.ReadOnly = true;
            this.txtReceivableAccount.RowHeadersVisible = false;
            this.txtReceivableAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.txtReceivableAccount.RowHeightMin = 0;
            this.txtReceivableAccount.Rows = 1;
            this.txtReceivableAccount.ScrollTipText = null;
            this.txtReceivableAccount.ShowColumnVisibilityMenu = false;
            this.txtReceivableAccount.Size = new System.Drawing.Size(169, 42);
            this.txtReceivableAccount.StandardTab = true;
            this.txtReceivableAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.txtReceivableAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.txtReceivableAccount.TabIndex = 51;
            this.ToolTip1.SetToolTip(this.txtReceivableAccount, null);
            this.txtReceivableAccount.Enter += new System.EventHandler(this.txtReceivableAccount_Enter);
            this.txtReceivableAccount.Leave += new System.EventHandler(this.txtReceivableAccount_Leave);
            // 
            // txtDefaultAccount
            // 
            this.txtDefaultAccount.AllowSelection = false;
            this.txtDefaultAccount.AllowUserToResizeColumns = false;
            this.txtDefaultAccount.AllowUserToResizeRows = false;
            this.txtDefaultAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.txtDefaultAccount.BackColorAlternate = System.Drawing.Color.Empty;
            this.txtDefaultAccount.BackColorBkg = System.Drawing.Color.Empty;
            this.txtDefaultAccount.BackColorFixed = System.Drawing.Color.Empty;
            this.txtDefaultAccount.BackColorSel = System.Drawing.Color.Empty;
            this.txtDefaultAccount.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.txtDefaultAccount.Cols = 1;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.txtDefaultAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.txtDefaultAccount.ColumnHeadersHeight = 30;
            this.txtDefaultAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.txtDefaultAccount.ColumnHeadersVisible = false;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.txtDefaultAccount.DefaultCellStyle = dataGridViewCellStyle4;
            this.txtDefaultAccount.DragIcon = null;
            this.txtDefaultAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.txtDefaultAccount.ExtendLastCol = true;
            this.txtDefaultAccount.FixedCols = 0;
            this.txtDefaultAccount.FixedRows = 0;
            this.txtDefaultAccount.ForeColorFixed = System.Drawing.Color.Empty;
            this.txtDefaultAccount.FrozenCols = 0;
            this.txtDefaultAccount.GridColor = System.Drawing.Color.Empty;
            this.txtDefaultAccount.GridColorFixed = System.Drawing.Color.Empty;
            this.txtDefaultAccount.Location = new System.Drawing.Point(237, 80);
            this.txtDefaultAccount.Name = "txtDefaultAccount";
            this.txtDefaultAccount.OutlineCol = 0;
            this.txtDefaultAccount.ReadOnly = true;
            this.txtDefaultAccount.RowHeadersVisible = false;
            this.txtDefaultAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.txtDefaultAccount.RowHeightMin = 0;
            this.txtDefaultAccount.Rows = 1;
            this.txtDefaultAccount.ScrollTipText = null;
            this.txtDefaultAccount.ShowColumnVisibilityMenu = false;
            this.txtDefaultAccount.Size = new System.Drawing.Size(169, 42);
            this.txtDefaultAccount.StandardTab = true;
            this.txtDefaultAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.txtDefaultAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.txtDefaultAccount.TabIndex = 52;
            this.ToolTip1.SetToolTip(this.txtDefaultAccount, null);
            this.txtDefaultAccount.Enter += new System.EventHandler(this.txtDefaultAccount_Enter);
            this.txtDefaultAccount.Leave += new System.EventHandler(this.txtDefaultAccount_Leave);
            // 
            // txtOverrideIntAccount
            // 
            this.txtOverrideIntAccount.AllowSelection = false;
            this.txtOverrideIntAccount.AllowUserToResizeColumns = false;
            this.txtOverrideIntAccount.AllowUserToResizeRows = false;
            this.txtOverrideIntAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.txtOverrideIntAccount.BackColorAlternate = System.Drawing.Color.Empty;
            this.txtOverrideIntAccount.BackColorBkg = System.Drawing.Color.Empty;
            this.txtOverrideIntAccount.BackColorFixed = System.Drawing.Color.Empty;
            this.txtOverrideIntAccount.BackColorSel = System.Drawing.Color.Empty;
            this.txtOverrideIntAccount.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.txtOverrideIntAccount.Cols = 1;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.txtOverrideIntAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.txtOverrideIntAccount.ColumnHeadersHeight = 30;
            this.txtOverrideIntAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.txtOverrideIntAccount.ColumnHeadersVisible = false;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.txtOverrideIntAccount.DefaultCellStyle = dataGridViewCellStyle6;
            this.txtOverrideIntAccount.DragIcon = null;
            this.txtOverrideIntAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.txtOverrideIntAccount.ExtendLastCol = true;
            this.txtOverrideIntAccount.FixedCols = 0;
            this.txtOverrideIntAccount.FixedRows = 0;
            this.txtOverrideIntAccount.ForeColorFixed = System.Drawing.Color.Empty;
            this.txtOverrideIntAccount.FrozenCols = 0;
            this.txtOverrideIntAccount.GridColor = System.Drawing.Color.Empty;
            this.txtOverrideIntAccount.GridColorFixed = System.Drawing.Color.Empty;
            this.txtOverrideIntAccount.Location = new System.Drawing.Point(706, 30);
            this.txtOverrideIntAccount.Name = "txtOverrideIntAccount";
            this.txtOverrideIntAccount.OutlineCol = 0;
            this.txtOverrideIntAccount.ReadOnly = true;
            this.txtOverrideIntAccount.RowHeadersVisible = false;
            this.txtOverrideIntAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.txtOverrideIntAccount.RowHeightMin = 0;
            this.txtOverrideIntAccount.Rows = 1;
            this.txtOverrideIntAccount.ScrollTipText = null;
            this.txtOverrideIntAccount.ShowColumnVisibilityMenu = false;
            this.txtOverrideIntAccount.Size = new System.Drawing.Size(169, 42);
            this.txtOverrideIntAccount.StandardTab = true;
            this.txtOverrideIntAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.txtOverrideIntAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.txtOverrideIntAccount.TabIndex = 53;
            this.ToolTip1.SetToolTip(this.txtOverrideIntAccount, null);
            this.txtOverrideIntAccount.Enter += new System.EventHandler(this.txtOverrideIntAccount_Enter);
            this.txtOverrideIntAccount.Leave += new System.EventHandler(this.txtOverrideIntAccount_Leave);
            // 
            // txtOverrideSalesTaxAccount
            // 
            this.txtOverrideSalesTaxAccount.AllowSelection = false;
            this.txtOverrideSalesTaxAccount.AllowUserToResizeColumns = false;
            this.txtOverrideSalesTaxAccount.AllowUserToResizeRows = false;
            this.txtOverrideSalesTaxAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.txtOverrideSalesTaxAccount.BackColorAlternate = System.Drawing.Color.Empty;
            this.txtOverrideSalesTaxAccount.BackColorBkg = System.Drawing.Color.Empty;
            this.txtOverrideSalesTaxAccount.BackColorFixed = System.Drawing.Color.Empty;
            this.txtOverrideSalesTaxAccount.BackColorSel = System.Drawing.Color.Empty;
            this.txtOverrideSalesTaxAccount.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.txtOverrideSalesTaxAccount.Cols = 1;
            dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.txtOverrideSalesTaxAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.txtOverrideSalesTaxAccount.ColumnHeadersHeight = 30;
            this.txtOverrideSalesTaxAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.txtOverrideSalesTaxAccount.ColumnHeadersVisible = false;
            dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.txtOverrideSalesTaxAccount.DefaultCellStyle = dataGridViewCellStyle8;
            this.txtOverrideSalesTaxAccount.DragIcon = null;
            this.txtOverrideSalesTaxAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.txtOverrideSalesTaxAccount.ExtendLastCol = true;
            this.txtOverrideSalesTaxAccount.FixedCols = 0;
            this.txtOverrideSalesTaxAccount.FixedRows = 0;
            this.txtOverrideSalesTaxAccount.ForeColorFixed = System.Drawing.Color.Empty;
            this.txtOverrideSalesTaxAccount.FrozenCols = 0;
            this.txtOverrideSalesTaxAccount.GridColor = System.Drawing.Color.Empty;
            this.txtOverrideSalesTaxAccount.GridColorFixed = System.Drawing.Color.Empty;
            this.txtOverrideSalesTaxAccount.Location = new System.Drawing.Point(706, 80);
            this.txtOverrideSalesTaxAccount.Name = "txtOverrideSalesTaxAccount";
            this.txtOverrideSalesTaxAccount.OutlineCol = 0;
            this.txtOverrideSalesTaxAccount.ReadOnly = true;
            this.txtOverrideSalesTaxAccount.RowHeadersVisible = false;
            this.txtOverrideSalesTaxAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.txtOverrideSalesTaxAccount.RowHeightMin = 0;
            this.txtOverrideSalesTaxAccount.Rows = 1;
            this.txtOverrideSalesTaxAccount.ScrollTipText = null;
            this.txtOverrideSalesTaxAccount.ShowColumnVisibilityMenu = false;
            this.txtOverrideSalesTaxAccount.Size = new System.Drawing.Size(169, 42);
            this.txtOverrideSalesTaxAccount.StandardTab = true;
            this.txtOverrideSalesTaxAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.txtOverrideSalesTaxAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.txtOverrideSalesTaxAccount.TabIndex = 55;
            this.ToolTip1.SetToolTip(this.txtOverrideSalesTaxAccount, null);
            this.txtOverrideSalesTaxAccount.Enter += new System.EventHandler(this.txtOverrideSalesTaxAccount_Enter);
            this.txtOverrideSalesTaxAccount.Leave += new System.EventHandler(this.txtOverrideSalesTaxAccount_Leave);
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(464, 94);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(224, 16);
            this.Label17.TabIndex = 56;
            this.Label17.Text = "OVERRIDE SALES TAX ACCOUNT";
            this.ToolTip1.SetToolTip(this.Label17, null);
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(464, 44);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(218, 16);
            this.Label16.TabIndex = 54;
            this.Label16.Text = "OVERRIDE INTEREST ACCOUNT";
            this.ToolTip1.SetToolTip(this.Label16, null);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(20, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(162, 16);
            this.Label4.TabIndex = 43;
            this.Label4.Text = "RECEIVABLE ACCOUNT";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // lblDefaultAccount
            // 
            this.lblDefaultAccount.AutoSize = true;
            this.lblDefaultAccount.Location = new System.Drawing.Point(20, 94);
            this.lblDefaultAccount.Name = "lblDefaultAccount";
            this.lblDefaultAccount.Size = new System.Drawing.Size(198, 16);
            this.lblDefaultAccount.TabIndex = 42;
            this.lblDefaultAccount.Text = "ALTERNATE CASH ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblDefaultAccount, null);
            // 
            // chkChanges_0
            // 
            this.chkChanges_0.Location = new System.Drawing.Point(959, 128);
            this.chkChanges_0.Name = "chkChanges_0";
            this.chkChanges_0.Size = new System.Drawing.Size(22, 22);
            this.chkChanges_0.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.chkChanges_0, "Check if this field will be different on each invoice.");
            // 
            // chkChanges_1
            // 
            this.chkChanges_1.Location = new System.Drawing.Point(959, 178);
            this.chkChanges_1.Name = "chkChanges_1";
            this.chkChanges_1.Size = new System.Drawing.Size(22, 22);
            this.chkChanges_1.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.chkChanges_1, "Check if this field will be different on each invoice.");
            // 
            // chkChanges_2
            // 
            this.chkChanges_2.Location = new System.Drawing.Point(959, 228);
            this.chkChanges_2.Name = "chkChanges_2";
            this.chkChanges_2.Size = new System.Drawing.Size(22, 22);
            this.chkChanges_2.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.chkChanges_2, "Check if this field will be different on each invoice.");
            // 
            // chkChanges_3
            // 
            this.chkChanges_3.Location = new System.Drawing.Point(959, 278);
            this.chkChanges_3.Name = "chkChanges_3";
            this.chkChanges_3.Size = new System.Drawing.Size(22, 22);
            this.chkChanges_3.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.chkChanges_3, "Check if this field will be different on each invoice.");
            // 
            // vsBill
            // 
            this.vsBill.AllowSelection = false;
            this.vsBill.AllowUserToResizeColumns = false;
            this.vsBill.AllowUserToResizeRows = false;
            this.vsBill.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsBill.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsBill.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsBill.BackColorBkg = System.Drawing.Color.Empty;
            this.vsBill.BackColorFixed = System.Drawing.Color.Empty;
            this.vsBill.BackColorSel = System.Drawing.Color.Empty;
            this.vsBill.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsBill.Cols = 6;
            dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsBill.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.vsBill.ColumnHeadersHeight = 30;
            this.vsBill.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsBill.DefaultCellStyle = dataGridViewCellStyle10;
            this.vsBill.DragIcon = null;
            this.vsBill.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsBill.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsBill.FrozenCols = 0;
            this.vsBill.GridColor = System.Drawing.Color.Empty;
            this.vsBill.GridColorFixed = System.Drawing.Color.Empty;
            this.vsBill.Location = new System.Drawing.Point(30, 391);
            this.vsBill.Name = "vsBill";
            this.vsBill.OutlineCol = 0;
            this.vsBill.ReadOnly = true;
            this.vsBill.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsBill.RowHeightMin = 0;
            this.vsBill.Rows = 7;
            this.vsBill.ScrollTipText = null;
            this.vsBill.ShowColumnVisibilityMenu = false;
            this.vsBill.Size = new System.Drawing.Size(952, 272);
            this.vsBill.StandardTab = true;
            this.vsBill.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsBill.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsBill.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.vsBill, null);
            this.vsBill.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsBill_KeyPressEdit);
            this.vsBill.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(this.VsBill_EditingControlShowing);
            this.vsBill.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsBill_ValidateEdit);
            this.vsBill.CurrentCellChanged += new System.EventHandler(this.vsBill_RowColChange);
            this.vsBill.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBill_KeyDownEvent);
            this.vsBill.Enter += new System.EventHandler(this.vsBill_Enter);
            this.vsBill.Click += new System.EventHandler(this.vsBill_ClickEvent);
            // 
            // fraOptions
            // 
            this.fraOptions.Controls.Add(this.fraFlat);
            this.fraOptions.Controls.Add(this.fraPerDiem);
            this.fraOptions.Controls.Add(this.cboInterestMethod);
            this.fraOptions.Controls.Add(this.fcLabel2);
            this.fraOptions.Controls.Add(this.fcLabel1);
            this.fraOptions.Controls.Add(this.cboFrequencyFirstMonth);
            this.fraOptions.Controls.Add(this.cboInterestStartDate);
            this.fraOptions.Controls.Add(this.cboFrequencyCode);
            this.fraOptions.Controls.Add(this.chkSalesTax);
            this.fraOptions.Controls.Add(this.txtTitle);
            this.fraOptions.Controls.Add(this.cmbType);
            this.fraOptions.Controls.Add(this.Label5);
            this.fraOptions.Controls.Add(this.Label1);
            this.fraOptions.Controls.Add(this.Label2);
            this.fraOptions.Controls.Add(this.cmbCode);
            this.fraOptions.Location = new System.Drawing.Point(0, 0);
            this.fraOptions.Name = "fraOptions";
            this.fraOptions.Size = new System.Drawing.Size(599, 376);
            this.fraOptions.TabIndex = 34;
            this.ToolTip1.SetToolTip(this.fraOptions, null);
            // 
            // fraFlat
            // 
            this.fraFlat.Controls.Add(this.fcLabel3);
            this.fraFlat.Controls.Add(this.txtFlatAmount);
            this.fraFlat.Location = new System.Drawing.Point(0, 220);
            this.fraFlat.Name = "fraFlat";
            this.fraFlat.Size = new System.Drawing.Size(437, 60);
            this.fraFlat.TabIndex = 39;
            this.fraFlat.Text = "Flat Amount";
            this.ToolTip1.SetToolTip(this.fraFlat, null);
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(30, 24);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(102, 16);
            this.fcLabel3.TabIndex = 7;
            this.fcLabel3.Text = "FLAT AMOUNT";
            this.ToolTip1.SetToolTip(this.fcLabel3, null);
            // 
            // txtFlatAmount
            // 
            this.txtFlatAmount.Location = new System.Drawing.Point(214, 10);
            this.txtFlatAmount.MaxLength = 8;
            this.txtFlatAmount.Name = "txtFlatAmount";
            this.txtFlatAmount.Size = new System.Drawing.Size(111, 22);
            this.txtFlatAmount.TabIndex = 6;
            this.txtFlatAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtFlatAmount, null);
            // 
            // fraPerDiem
            // 
            this.fraPerDiem.Controls.Add(this.fcLabel4);
            this.fraPerDiem.Controls.Add(this.txtPerDiem);
            this.fraPerDiem.Controls.Add(this.Label12);
            this.fraPerDiem.Location = new System.Drawing.Point(0, 220);
            this.fraPerDiem.Name = "fraPerDiem";
            this.fraPerDiem.Size = new System.Drawing.Size(437, 60);
            this.fraPerDiem.TabIndex = 46;
            this.fraPerDiem.Text = "Per Diem Rate";
            this.ToolTip1.SetToolTip(this.fraPerDiem, null);
            this.fraPerDiem.Visible = false;
            // 
            // fcLabel4
            // 
            this.fcLabel4.AutoSize = true;
            this.fcLabel4.Location = new System.Drawing.Point(30, 24);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(112, 16);
            this.fcLabel4.TabIndex = 51;
            this.fcLabel4.Text = "PER DIEM RATE";
            this.ToolTip1.SetToolTip(this.fcLabel4, null);
            // 
            // txtPerDiem
            // 
            this.txtPerDiem.Location = new System.Drawing.Point(214, 10);
            this.txtPerDiem.MaxLength = 8;
            this.txtPerDiem.Name = "txtPerDiem";
            this.txtPerDiem.Size = new System.Drawing.Size(111, 22);
            this.txtPerDiem.TabIndex = 47;
            this.txtPerDiem.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtPerDiem, null);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(333, 24);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(20, 16);
            this.Label12.TabIndex = 50;
            this.Label12.Text = "%";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // cboInterestMethod
            // 
            this.cboInterestMethod.AutoSize = false;
            this.cboInterestMethod.BackColor = System.Drawing.SystemColors.Window;
            this.cboInterestMethod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboInterestMethod.FormattingEnabled = true;
            this.cboInterestMethod.Items.AddRange(new object[] {
            "AF  -  Auto Flat Rate",
            "AP  -  Auto Per Diem",
            "DF  -  Demand Flat Rate",
            "DP  -  Demand Per Diem"});
            this.cboInterestMethod.Location = new System.Drawing.Point(214, 180);
            this.cboInterestMethod.Name = "cboInterestMethod";
            this.cboInterestMethod.Size = new System.Drawing.Size(223, 40);
            this.cboInterestMethod.TabIndex = 44;
            this.ToolTip1.SetToolTip(this.cboInterestMethod, null);
            this.cboInterestMethod.SelectedIndexChanged += new System.EventHandler(this.cboInterestMethod_SelectedIndexChanged);
            this.cboInterestMethod.DropDown += new System.EventHandler(this.cboInterestMethod_DropDown);
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(30, 194);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(136, 16);
            this.fcLabel2.TabIndex = 51;
            this.fcLabel2.Text = "INTEREST METHOD";
            this.ToolTip1.SetToolTip(this.fcLabel2, null);
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(29, 144);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(92, 16);
            this.fcLabel1.TabIndex = 50;
            this.fcLabel1.Text = "FREQUENCY";
            this.ToolTip1.SetToolTip(this.fcLabel1, null);
            // 
            // cboFrequencyFirstMonth
            // 
            this.cboFrequencyFirstMonth.AutoSize = false;
            this.cboFrequencyFirstMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboFrequencyFirstMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboFrequencyFirstMonth.FormattingEnabled = true;
            this.cboFrequencyFirstMonth.Location = new System.Drawing.Point(409, 130);
            this.cboFrequencyFirstMonth.Name = "cboFrequencyFirstMonth";
            this.cboFrequencyFirstMonth.Size = new System.Drawing.Size(173, 40);
            this.cboFrequencyFirstMonth.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cboFrequencyFirstMonth, null);
            this.cboFrequencyFirstMonth.DropDown += new System.EventHandler(this.cboFrequencyFirstMonth_DropDown);
            // 
            // cboInterestStartDate
            // 
            this.cboInterestStartDate.AutoSize = false;
            this.cboInterestStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.cboInterestStartDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboInterestStartDate.FormattingEnabled = true;
            this.cboInterestStartDate.Location = new System.Drawing.Point(214, 318);
            this.cboInterestStartDate.Name = "cboInterestStartDate";
            this.cboInterestStartDate.Size = new System.Drawing.Size(223, 40);
            this.cboInterestStartDate.TabIndex = 49;
            this.ToolTip1.SetToolTip(this.cboInterestStartDate, null);
            // 
            // cboFrequencyCode
            // 
            this.cboFrequencyCode.AutoSize = false;
            this.cboFrequencyCode.BackColor = System.Drawing.SystemColors.Window;
            this.cboFrequencyCode.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboFrequencyCode.FormattingEnabled = true;
            this.cboFrequencyCode.Items.AddRange(new object[] {
            "B  -  Bi Monthly",
            "D  -  Demand",
            "M  -  Monthly",
            "Q  -  Quarterly",
            "S  -  Semi Annual",
            "Y  -  Yearly"});
            this.cboFrequencyCode.Location = new System.Drawing.Point(214, 130);
            this.cboFrequencyCode.Name = "cboFrequencyCode";
            this.cboFrequencyCode.Size = new System.Drawing.Size(189, 40);
            this.cboFrequencyCode.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cboFrequencyCode, null);
            this.cboFrequencyCode.SelectedIndexChanged += new System.EventHandler(this.cboFrequencyCode_SelectedIndexChanged);
            this.cboFrequencyCode.DropDown += new System.EventHandler(this.cboFrequencyCode_DropDown);
            // 
            // chkSalesTax
            // 
            this.chkSalesTax.Location = new System.Drawing.Point(214, 280);
            this.chkSalesTax.Name = "chkSalesTax";
            this.chkSalesTax.Size = new System.Drawing.Size(133, 23);
            this.chkSalesTax.TabIndex = 5;
            this.chkSalesTax.Text = "Charge Sales Tax";
            this.ToolTip1.SetToolTip(this.chkSalesTax, null);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle.LinkItem = null;
            this.txtTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTitle.LinkTopic = null;
            this.txtTitle.Location = new System.Drawing.Point(214, 80);
            this.txtTitle.MaxLength = 30;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(368, 40);
            this.txtTitle.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtTitle, null);
            this.txtTitle.Enter += new System.EventHandler(this.txtTitle_Enter);
            this.txtTitle.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
            // 
            // cmbType
            // 
            this.cmbType.AutoSize = false;
            this.cmbType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(214, 30);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(284, 40);
            this.cmbType.Sorted = true;
            this.cmbType.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.cmbType, null);
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            this.cmbType.DropDown += new System.EventHandler(this.cmbType_DropDown);
            this.cmbType.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbType_KeyDown);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(30, 332);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(161, 16);
            this.Label5.TabIndex = 48;
            this.Label5.Text = "INTEREST START DATE";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(47, 16);
            this.Label1.TabIndex = 36;
            this.Label1.Text = "CODE";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(30, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(99, 16);
            this.Label2.TabIndex = 35;
            this.Label2.Text = "DESCRIPTION";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // cmbCode
            // 
            this.cmbCode.AllowSelection = false;
            this.cmbCode.AllowUserToResizeColumns = false;
            this.cmbCode.AllowUserToResizeRows = false;
            this.cmbCode.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.cmbCode.BackColorAlternate = System.Drawing.Color.Empty;
            this.cmbCode.BackColorBkg = System.Drawing.Color.Empty;
            this.cmbCode.BackColorFixed = System.Drawing.Color.Empty;
            this.cmbCode.BackColorSel = System.Drawing.Color.Empty;
            this.cmbCode.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.cmbCode.Cols = 1;
            dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.cmbCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.cmbCode.ColumnHeadersHeight = 30;
            this.cmbCode.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.cmbCode.ColumnHeadersVisible = false;
            dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.cmbCode.DefaultCellStyle = dataGridViewCellStyle12;
            this.cmbCode.DragIcon = null;
            this.cmbCode.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.cmbCode.FixedCols = 0;
            this.cmbCode.FixedRows = 0;
            this.cmbCode.ForeColorFixed = System.Drawing.Color.Empty;
            this.cmbCode.FrozenCols = 0;
            this.cmbCode.GridColor = System.Drawing.Color.Empty;
            this.cmbCode.GridColorFixed = System.Drawing.Color.Empty;
            this.cmbCode.Location = new System.Drawing.Point(504, 30);
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.OutlineCol = 0;
            this.cmbCode.ReadOnly = true;
            this.cmbCode.RowHeadersVisible = false;
            this.cmbCode.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.cmbCode.RowHeightMin = 0;
            this.cmbCode.Rows = 1;
            this.cmbCode.ScrollTipText = null;
            this.cmbCode.ShowColumnVisibilityMenu = false;
            this.cmbCode.Size = new System.Drawing.Size(78, 40);
            this.cmbCode.StandardTab = true;
            this.cmbCode.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.cmbCode.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.cmbCode.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbCode, null);
            this.cmbCode.Visible = false;
            this.cmbCode.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDownEdit);
            this.cmbCode.ComboDropDown += new System.EventHandler(this.cmbCode_ComboDropDown);
            this.cmbCode.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.cmbCode_ChangeEdit);
            this.cmbCode.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.cmbCode_ValidateEdit);
            this.cmbCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDownEvent);
            this.cmbCode.Enter += new System.EventHandler(this.cmbCode_Enter);
            this.cmbCode.Click += new System.EventHandler(this.cmbCode_ClickEvent);
            this.cmbCode.DoubleClick += new System.EventHandler(this.cmbCode_DblClick);
            this.cmbCode.Validating += new System.ComponentModel.CancelEventHandler(this.cmbCode_Validating);
            // 
            // chkMandatory_3
            // 
            this.chkMandatory_3.Location = new System.Drawing.Point(901, 278);
            this.chkMandatory_3.Name = "chkMandatory_3";
            this.chkMandatory_3.Size = new System.Drawing.Size(22, 22);
            this.chkMandatory_3.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.chkMandatory_3, "Check if this field is required for this type.");
            // 
            // chkMandatory_2
            // 
            this.chkMandatory_2.Location = new System.Drawing.Point(901, 228);
            this.chkMandatory_2.Name = "chkMandatory_2";
            this.chkMandatory_2.Size = new System.Drawing.Size(22, 22);
            this.chkMandatory_2.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.chkMandatory_2, "Check if this field is required for this type.");
            // 
            // chkMandatory_1
            // 
            this.chkMandatory_1.Location = new System.Drawing.Point(901, 178);
            this.chkMandatory_1.Name = "chkMandatory_1";
            this.chkMandatory_1.Size = new System.Drawing.Size(22, 22);
            this.chkMandatory_1.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.chkMandatory_1, "Check if this field is required for this type.");
            // 
            // chkMandatory_0
            // 
            this.chkMandatory_0.Location = new System.Drawing.Point(901, 128);
            this.chkMandatory_0.Name = "chkMandatory_0";
            this.chkMandatory_0.Size = new System.Drawing.Size(22, 22);
            this.chkMandatory_0.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.chkMandatory_0, "Check if this field is required for this type.");
            // 
            // txtCTRL3Title
            // 
            this.txtCTRL3Title.AutoSize = false;
            this.txtCTRL3Title.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTRL3Title.LinkItem = null;
            this.txtCTRL3Title.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCTRL3Title.LinkTopic = null;
            this.txtCTRL3Title.Location = new System.Drawing.Point(734, 271);
            this.txtCTRL3Title.MaxLength = 15;
            this.txtCTRL3Title.Name = "txtCTRL3Title";
            this.txtCTRL3Title.Size = new System.Drawing.Size(140, 40);
            this.txtCTRL3Title.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.txtCTRL3Title, null);
            this.txtCTRL3Title.Enter += new System.EventHandler(this.txtCTRL3Title_Enter);
            this.txtCTRL3Title.TextChanged += new System.EventHandler(this.txtCTRL3Title_TextChanged);
            // 
            // txtRefTitle
            // 
            this.txtRefTitle.AutoSize = false;
            this.txtRefTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtRefTitle.LinkItem = null;
            this.txtRefTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtRefTitle.LinkTopic = null;
            this.txtRefTitle.Location = new System.Drawing.Point(733, 121);
            this.txtRefTitle.MaxLength = 15;
            this.txtRefTitle.Name = "txtRefTitle";
            this.txtRefTitle.Size = new System.Drawing.Size(140, 40);
            this.txtRefTitle.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtRefTitle, null);
            this.txtRefTitle.Enter += new System.EventHandler(this.txtRefTitle_Enter);
            this.txtRefTitle.TextChanged += new System.EventHandler(this.txtRefTitle_TextChanged);
            this.txtRefTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtRefTitle_Validating);
            // 
            // txtCTRL1Title
            // 
            this.txtCTRL1Title.AutoSize = false;
            this.txtCTRL1Title.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTRL1Title.LinkItem = null;
            this.txtCTRL1Title.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCTRL1Title.LinkTopic = null;
            this.txtCTRL1Title.Location = new System.Drawing.Point(734, 171);
            this.txtCTRL1Title.MaxLength = 15;
            this.txtCTRL1Title.Name = "txtCTRL1Title";
            this.txtCTRL1Title.Size = new System.Drawing.Size(140, 40);
            this.txtCTRL1Title.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtCTRL1Title, null);
            this.txtCTRL1Title.Enter += new System.EventHandler(this.txtCTRL1Title_Enter);
            this.txtCTRL1Title.TextChanged += new System.EventHandler(this.txtCTRL1Title_TextChanged);
            // 
            // txtCTRL2Title
            // 
            this.txtCTRL2Title.AutoSize = false;
            this.txtCTRL2Title.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTRL2Title.LinkItem = null;
            this.txtCTRL2Title.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCTRL2Title.LinkTopic = null;
            this.txtCTRL2Title.Location = new System.Drawing.Point(734, 221);
            this.txtCTRL2Title.MaxLength = 15;
            this.txtCTRL2Title.Name = "txtCTRL2Title";
            this.txtCTRL2Title.Size = new System.Drawing.Size(140, 40);
            this.txtCTRL2Title.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtCTRL2Title, null);
            this.txtCTRL2Title.Enter += new System.EventHandler(this.txtCTRL2Title_Enter);
            this.txtCTRL2Title.TextChanged += new System.EventHandler(this.txtCTRL2Title_TextChanged);
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(620, 44);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(52, 16);
            this.Label14.TabIndex = 58;
            this.Label14.Text = "PAYEE";
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // lblAccountTitle2
            // 
            this.lblAccountTitle2.Location = new System.Drawing.Point(30, 876);
            this.lblAccountTitle2.Name = "lblAccountTitle2";
            this.lblAccountTitle2.Size = new System.Drawing.Size(500, 16);
            this.lblAccountTitle2.TabIndex = 45;
            this.lblAccountTitle2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblAccountTitle2, null);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(938, 90);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(74, 16);
            this.Label3.TabIndex = 40;
            this.Label3.Text = "CHANGES";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label3, "Check if this field is required for this type.");
            // 
            // lblAccountTitle
            // 
            this.lblAccountTitle.Location = new System.Drawing.Point(162, 672);
            this.lblAccountTitle.Name = "lblAccountTitle";
            this.lblAccountTitle.Size = new System.Drawing.Size(500, 16);
            this.lblAccountTitle.TabIndex = 32;
            this.ToolTip1.SetToolTip(this.lblAccountTitle, null);
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(30, 672);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(114, 16);
            this.Label8.TabIndex = 33;
            this.Label8.Text = "ACCOUNT TITLE";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(894, 90);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(37, 16);
            this.Label6.TabIndex = 31;
            this.Label6.Text = "REQ";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label6, "Check if this field is required for this type.");
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(620, 285);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(84, 16);
            this.Label7.TabIndex = 30;
            this.Label7.Text = "CONTROL 3";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(734, 90);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(118, 16);
            this.Label13.TabIndex = 27;
            this.Label13.Text = "DEFAULT TITLES";
            this.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(620, 135);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(90, 16);
            this.Label11.TabIndex = 26;
            this.Label11.Text = "REFERENCE";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(620, 185);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(84, 16);
            this.Label10.TabIndex = 25;
            this.Label10.Text = "CONTROL 1";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(620, 235);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(84, 16);
            this.Label9.TabIndex = 24;
            this.Label9.Text = "CONTROL 2";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessSave.AppearanceKey = "toolbarButton";
            this.btnProcessSave.Location = new System.Drawing.Point(967, 29);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.btnProcessSave.Size = new System.Drawing.Size(49, 24);
            this.btnProcessSave.TabIndex = 1;
            this.btnProcessSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.btnProcessSave, null);
            this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // btnFileForward
            // 
            this.btnFileForward.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileForward.AppearanceKey = "toolbarButton";
            this.btnFileForward.Location = new System.Drawing.Point(918, 29);
            this.btnFileForward.Name = "btnFileForward";
            this.btnFileForward.Shortcut = Wisej.Web.Shortcut.F8;
            this.btnFileForward.Size = new System.Drawing.Size(45, 24);
            this.btnFileForward.TabIndex = 2;
            this.btnFileForward.Text = "Next";
            this.ToolTip1.SetToolTip(this.btnFileForward, null);
            this.btnFileForward.Click += new System.EventHandler(this.mnuFileForward_Click);
            // 
            // btnFileBack
            // 
            this.btnFileBack.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileBack.AppearanceKey = "toolbarButton";
            this.btnFileBack.Location = new System.Drawing.Point(848, 29);
            this.btnFileBack.Name = "btnFileBack";
            this.btnFileBack.Shortcut = Wisej.Web.Shortcut.F7;
            this.btnFileBack.Size = new System.Drawing.Size(66, 24);
            this.btnFileBack.TabIndex = 3;
            this.btnFileBack.Text = "Previous";
            this.ToolTip1.SetToolTip(this.btnFileBack, null);
            this.btnFileBack.Click += new System.EventHandler(this.mnuFileBack_Click);
            // 
            // btnProcessDelete
            // 
            this.btnProcessDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessDelete.AppearanceKey = "toolbarButton";
            this.btnProcessDelete.Location = new System.Drawing.Point(793, 29);
            this.btnProcessDelete.Name = "btnProcessDelete";
            this.btnProcessDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
            this.btnProcessDelete.Size = new System.Drawing.Size(51, 24);
            this.btnProcessDelete.TabIndex = 4;
            this.btnProcessDelete.Text = "Delete";
            this.ToolTip1.SetToolTip(this.btnProcessDelete, null);
            this.btnProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
            // 
            // btnProcessNew
            // 
            this.btnProcessNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessNew.AppearanceKey = "toolbarButton";
            this.btnProcessNew.Location = new System.Drawing.Point(744, 29);
            this.btnProcessNew.Name = "btnProcessNew";
            this.btnProcessNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.btnProcessNew.Size = new System.Drawing.Size(44, 24);
            this.btnProcessNew.TabIndex = 5;
            this.btnProcessNew.Text = "New";
            this.ToolTip1.SetToolTip(this.btnProcessNew, null);
            this.btnProcessNew.Click += new System.EventHandler(this.mnuProcessNew_Click);
            // 
            // btnProcessSaveExit
            // 
            this.btnProcessSaveExit.AppearanceKey = "acceptButton";
            this.btnProcessSaveExit.Location = new System.Drawing.Point(424, 30);
            this.btnProcessSaveExit.Name = "btnProcessSaveExit";
            this.btnProcessSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcessSaveExit.Size = new System.Drawing.Size(148, 48);
            this.btnProcessSaveExit.TabIndex = 0;
            this.btnProcessSaveExit.Text = "Save & Advance";
            this.ToolTip1.SetToolTip(this.btnProcessSaveExit, null);
            this.btnProcessSaveExit.Click += new System.EventHandler(this.mnuProcessSaveExit_Click);
            // 
            // frmTypeSetup
            // 
            this.ClientSize = new System.Drawing.Size(1038, 682);
            this.KeyPreview = true;
            this.Name = "frmTypeSetup";
            this.Text = "Type Setup";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmTypeSetup_Load);
            this.Activated += new System.EventHandler(this.frmTypeSetup_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTypeSetup_KeyDown);
            this.Resize += new System.EventHandler(this.frmTypeSetup_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraNewType)).EndInit();
            this.fraNewType.ResumeLayout(false);
            this.fraNewType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceiptList)).EndInit();
            this.fraReceiptList.ResumeLayout(false);
            this.fraReceiptList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultAccount)).EndInit();
            this.fraDefaultAccount.ResumeLayout(false);
            this.fraDefaultAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceivableAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideIntAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideSalesTaxAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChanges_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOptions)).EndInit();
            this.fraOptions.ResumeLayout(false);
            this.fraOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFlat)).EndInit();
            this.fraFlat.ResumeLayout(false);
            this.fraFlat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFlatAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPerDiem)).EndInit();
            this.fraPerDiem.ResumeLayout(false);
            this.fraPerDiem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPerDiem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMandatory_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMandatory_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMandatory_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMandatory_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileForward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSaveExit)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCLabel fcLabel1;
		private FCLabel fcLabel2;
		private FCLabel fcLabel4;
		private FCLabel fcLabel3;
		private FCButton btnProcessSave;
		private FCButton btnFileForward;
		private FCButton btnFileBack;
		private FCButton btnProcessDelete;
		private FCButton btnProcessNew;
		private FCButton btnProcessSaveExit;
	}
}
