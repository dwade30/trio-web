﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillPrint.
	/// </summary>
	public partial class frmBillPrint : BaseForm
	{
		public frmBillPrint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillPrint InstancePtr
		{
			get
			{
				return (frmBillPrint)Sys.GetInstance(typeof(frmBillPrint));
			}
		}

		protected frmBillPrint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int SelectCol;
		int BillKeyCol;
		int AccountCol;
		int NameCol;
		int AmountCol;
		// vbPorter upgrade warning: intBillType As short --> As int	OnWrite(string, int)
		public int intBillType;
		string strVoidedBillsSQL;
		public string strBillSQL = "";

		private void frmBillPrint_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBillPrint_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBillPrint.FillStyle	= 0;
			//frmBillPrint.ScaleWidth	= 9045;
			//frmBillPrint.ScaleHeight	= 7350;
			//frmBillPrint.LinkTopic	= "Form2";
			//frmBillPrint.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			SelectCol = 0;
			BillKeyCol = 1;
			AccountCol = 2;
			NameCol = 3;
			AmountCol = 4;
			vsBills.ColHidden(BillKeyCol, true);
			vsBills.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsBills.ColAlignment(SelectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBills.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBills.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBills.ColWidth(AccountCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.164551));
			vsBills.ColWidth(SelectCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.1410437));
			vsBills.ColWidth(NameCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.3996238));
			vsBills.TextMatrix(0, SelectCol, "Print");
			vsBills.TextMatrix(0, AccountCol, "Cust #");
			vsBills.TextMatrix(0, NameCol, "Name");
			vsBills.TextMatrix(0, AmountCol, "Amount");
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			strVoidedBillsSQL = "(";
			if (strBillSQL == "")
			{
				LoadRateKeys();
				cboRateKeys.SelectedIndex = cboRateKeys.Items.Count - 1;
			}
			else
			{
				LoadBillGrid();
			}
			FormatTotal();
		}

		private void frmBillPrint_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int lngJournal = 0;
			clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
			clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
			if (strVoidedBillsSQL != "(")
			{
				strVoidedBillsSQL = Strings.Left(strVoidedBillsSQL, strVoidedBillsSQL.Length - 2) + ")";
				lngJournal = 0;
				modARCalculations.CreateJournalEntry_3(ref lngJournal, strVoidedBillsSQL);
				if (lngJournal == 0)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the Budgetary Journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					if (modSecurity.ValidPermissions_8(null, modGlobal.AUTOPOSTBILLINGJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptARBills))
					{
						if (MessageBox.Show("The reversing entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							clsJournalInfo = new clsPostingJournalInfo();
							clsJournalInfo.JournalNumber = lngJournal;
							clsJournalInfo.JournalType = "GJ";
							clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
							clsJournalInfo.CheckDate = "";
							clsPostInfo.AddJournal(clsJournalInfo);
							clsPostInfo.AllowPreview = true;
							clsPostInfo.WaitForReportToEnd = true;
							clsPostInfo.PostJournals();
						}
					}
					else
					{
						MessageBox.Show("The reversing entries have been saved into journal " + Strings.Format(lngJournal, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
			frmBillSelect.InstancePtr.Unload();
			//MDIParent.InstancePtr.Focus();
		}

		private void frmBillPrint_Resize(object sender, System.EventArgs e)
		{
			vsBills.ColWidth(AccountCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.164551));
			vsBills.ColWidth(SelectCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.1410437));
			vsBills.ColWidth(NameCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.3996238));
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (vsBills.Row > 0)
			{
				ans = MessageBox.Show("Once you void this bill you will not be able to get the information back.  Are you sure you wish to do this?", "Void Bill?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					rsDelete.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + vsBills.TextMatrix(vsBills.Row, BillKeyCol));
					if (rsDelete.EndOfFile() != true && rsDelete.BeginningOfFile() != true)
					{
						MessageBox.Show("You may not void this bill because there are already payments for it.", "Unable to Void", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else
					{
						rsDelete.OpenRecordset("SELECT * FROM Bill WHERE ID = " + vsBills.TextMatrix(vsBills.Row, BillKeyCol));
						rsDelete.Edit();
						rsDelete.Set_Fields("BillStatus", "V");
						rsDelete.Update();
						if (modGlobalConstants.Statics.gboolBD)
						{
							strVoidedBillsSQL += FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vsBills.TextMatrix(vsBills.Row, BillKeyCol)))) + ", ";
						}
						modGlobalFunctions.AddCYAEntry_8("AR", "Void of Bill " + vsBills.TextMatrix(vsBills.Row, BillKeyCol));
						vsBills.RemoveItem(vsBills.Row);
						CalculateTotal();
						MessageBox.Show("Bill Voided Successfully!", "Bill Voided", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
		}

		public void mnuProcessDelete_Click()
		{
			mnuProcessDelete_Click(mnuProcessDelete, new System.EventArgs());
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void LoadRateKeys()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboRateKeys.Clear();
			//FC:FINAL:SBE - #418 - in VB6 form is loaded when you access any property from it. Force form load
			frmBillSelect.InstancePtr.LoadForm();
			rsInfo.OpenRecordset("SELECT * FROM RateKeys WHERE BillMonth = " + FCConvert.ToString(FCConvert.ToDateTime(frmBillSelect.InstancePtr.cboMonth.Text + "/1/2000").Month) + " AND BillYear = " + frmBillSelect.InstancePtr.cboYear.Text + " AND BillType = " + FCConvert.ToString(intBillType) + " ORDER BY BillDate DESC");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					cboRateKeys.AddItem(Strings.Format(rsInfo.Get_Fields_Int32("ID"), "000") + "  " + Strings.Format(rsInfo.Get_Fields("BillType"), "000") + "   " + Strings.Format(rsInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "   " + rsInfo.Get_Fields_String("Description"));
					cboRateKeys.ItemData(cboRateKeys.NewIndex, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("id")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadBillGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			vsBills.Rows = 1;
			if (strBillSQL == "")
			{
				int lngTemp = 0;
				lngTemp = cboRateKeys.ItemData(cboRateKeys.SelectedIndex);
				rsInfo.OpenRecordset("SELECT * FROM Bill WHERE BillNumber = " + FCConvert.ToString(lngTemp) + " ORDER BY BName");
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM Bill WHERE ID IN " + strBillSQL + " ORDER BY BName");
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsBills.AddItem(true + "\t" + rsInfo.Get_Fields_Int32("ID") + "\t" + Strings.Format(rsInfo.Get_Fields_Int32("ActualAccountNumber"), "0000") + "\t" + rsInfo.Get_Fields_String("BName") + "\t" + Strings.Format(Decimal.Round(rsInfo.Get_Fields_Decimal("PrinOwed") + rsInfo.Get_Fields_Decimal("TaxOwed"),2), "#,##0.00"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				CalculateTotal();
				lblRateSelect.Visible = false;
				cboRateKeys.Visible = false;
				lblSelectAccounts.Visible = true;
				vsBills.Visible = true;
				lblTotal.Visible = true;
				//Line1.Visible = true;
				txtTotal.Visible = true;
				vsBills.Row = 1;
				if (frmBillPrint.InstancePtr.Visible)
				{
					vsBills.Focus();
				}
			}
			else
			{
				MessageBox.Show("There are no accounts that have not been billed for the selected rate record.", "No Accounts Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnData = false;
			if (cboRateKeys.Visible == true)
			{
				LoadBillGrid();
			}
			else
			{
				blnData = false;
				for (counter = 1; counter <= vsBills.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsBills.TextMatrix(counter, SelectCol)) == true)
					{
						blnData = true;
					}
				}
				if (!blnData)
				{
					MessageBox.Show("You must select at least one customer before printing bills.", "No Customers Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					PrintBills();
				}
			}
		}

		private void PrintBills()
		{
			int counter;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strSQL;
			strSQL = "SELECT * FROM Bill WHERE ID IN (";
			for (counter = 1; counter <= vsBills.Rows - 1; counter++)
			{
				if (FCConvert.CBool(vsBills.TextMatrix(counter, SelectCol)))
				{
					strSQL += vsBills.TextMatrix(counter, BillKeyCol) + ", ";
				}
			}
			strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ") ORDER BY BName";
			//rptCustomBill.InstancePtr.Hide();
			clsCustomBillAR clsBill = new clsCustomBillAR();
			clsBill.SQL = strSQL;
			clsBill.UsePrinterFonts = true;
			clsBill.FormatID = frmChooseCustomBillType.InstancePtr.Init("AR", "TWAR0000.vb1");
			Close();
			if (clsBill.FormatID > 0)
			{
				clsBill.DataDBFile = "Twar0000.vb1";
				clsTemp.OpenRecordset("select * from custombills where ID = " + FCConvert.ToString(clsBill.FormatID), "twar0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("islaser")))
					{
						clsBill.DotMatrixFormat = false;
					}
					else
					{
						clsBill.DotMatrixFormat = true;
					}
				}
				rptCustomBill.InstancePtr.Init(clsBill);
			}
		}

		private void vsBills_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsBills.Row > 0)
			{
				if (FCUtils.CBool(vsBills.TextMatrix(vsBills.Row, SelectCol)) == true)
				{
					vsBills.TextMatrix(vsBills.Row, SelectCol, FCConvert.ToString(false));
				}
				else
				{
					vsBills.TextMatrix(vsBills.Row, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsBills_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (vsBills.Row > 0)
			{
				if (e.KeyCode == Keys.Space)
				{
					//FC:TODO:AM
					//e.KeyCode = 0;
					if (FCUtils.CBool(vsBills.TextMatrix(vsBills.Row, SelectCol)) == true)
					{
						vsBills.TextMatrix(vsBills.Row, SelectCol, FCConvert.ToString(false));
					}
					else
					{
						vsBills.TextMatrix(vsBills.Row, SelectCol, FCConvert.ToString(true));
					}
				}
				else if (e.KeyCode == Keys.Delete)
				{
					mnuProcessDelete_Click();
				}
			}
		}

		private void CalculateTotal()
		{
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotal;
			int counter;
			curTotal = 0;
			for (counter = 1; counter <= vsBills.Rows - 1; counter++)
			{
				curTotal += FCConvert.ToDecimal(vsBills.TextMatrix(counter, AmountCol));
			}
			txtTotal.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void FormatTotal()
		{
			//int lngLeft;
			//int counter;
			//lngLeft = vsBills.Left;
			//for (counter = 0; counter <= AmountCol - 1; counter++)
			//{
			//    if (vsBills.ColHidden(counter) == false)
			//    {
			//        lngLeft += vsBills.ColWidth(counter);
			//    }
			//}
			//lblTotal.Left = lngLeft - lblTotal.Width;
			//Line1.X1 = lblTotal.Left;
			//txtTotal.Left = lngLeft;
			//txtTotal.Width = (vsBills.Left + vsBills.Width) - lngLeft;
			//Line1.X2 = vsBills.Left + vsBills.Width;
		}

		private void cmdVoidBills_Click(object sender, EventArgs e)
		{
			mnuProcessDelete_Click(sender, e);
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
