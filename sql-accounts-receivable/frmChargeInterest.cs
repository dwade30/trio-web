﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmChargeInterest.
	/// </summary>
	public partial class frmChargeInterest : BaseForm
	{
		public frmChargeInterest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChargeInterest InstancePtr
		{
			get
			{
				return (frmChargeInterest)Sys.GetInstance(typeof(frmChargeInterest));
			}
		}

		protected frmChargeInterest _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmChargeInterest_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
			if (cboBillType.Items.Count == 0)
			{
				MessageBox.Show("You have no bill types set up for demand interest.", "No Bill Types Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
			}
		}

		private void frmChargeInterest_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChargeInterest.FillStyle	= 0;
			//frmChargeInterest.ScaleWidth	= 3885;
			//frmChargeInterest.ScaleHeight	= 2505;
			//frmChargeInterest.LinkTopic	= "Form2";
			//frmChargeInterest.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			LoadBillCombo();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			cmbSelected.SelectedIndex = 0;
		}

		private void frmChargeInterest_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadBillCombo()
		{
			clsDRWrapper rsType = new clsDRWrapper();
			rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE left(InterestMethod, 1) = 'D' ORDER BY TypeTitle", modCLCalculations.strARDatabase);
			if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
			{
				do
				{
					cboBillType.AddItem(rsType.Get_Fields_String("TypeTitle"));
					cboBillType.ItemData(cboBillType.NewIndex, FCConvert.ToInt32(rsType.Get_Fields_Int32("TypeCode")));
					rsType.MoveNext();
				}
				while (rsType.EndOfFile() != true);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will go through the each bill and charge any current interest to each
				clsDRWrapper rsBills = new clsDRWrapper();
				clsDRWrapper rsF = new clsDRWrapper();
				double dblTotalDue;
				double dblCurrentInterest = 0;
				int lngKey = 0;
				bool boolLien;
				string strBillTypeSQL = "";
				int counter;
				if (cmbSelected.SelectedIndex == 0)
				{
					strBillTypeSQL = "(";
					for (counter = 0; counter <= cboBillType.Items.Count - 1; counter++)
					{
						strBillTypeSQL += FCConvert.ToString(cboBillType.ItemData(counter)) + ", ";
					}
					strBillTypeSQL = Strings.Left(strBillTypeSQL, strBillTypeSQL.Length - 2) + ")";
					rsBills.OpenRecordset("SELECT * FROM Bill WHERE BillStatus <> 'V' AND BillType IN " + strBillTypeSQL, modCLCalculations.strARDatabase);
					modGlobalFunctions.AddCYAEntry_8("AR", "Interest On Demand for Bill Type(s): All");
				}
				else
				{
					rsBills.OpenRecordset("SELECT * FROM Bill WHERE BillStatus <> 'V' AND BillType = " + FCConvert.ToString(cboBillType.ItemData(cboBillType.SelectedIndex)), modCLCalculations.strARDatabase);
					modGlobalFunctions.AddCYAEntry_8("AR", "Interest On Demand for Bill Type(s): " + cboBillType.Text);
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Calculating Interest");
				// , True, .RecordCount, True
				while (!rsBills.EndOfFile())
				{
					// frmWait.IncrementProgress
					//Application.DoEvents();
					dblCurrentInterest = 0;
					dblTotalDue = modARCalculations.CalculateAccountAR(rsBills, DateTime.Today, ref dblCurrentInterest, true);
					lngKey = FCConvert.ToInt32(rsBills.Get_Fields_Int32("ID"));
					if (dblCurrentInterest != 0)
					{
						// this will edit the bill records charged interest
						rsBills.Edit();
						rsBills.Set_Fields("IntPaidDate", DateTime.Today);
						// FC:FINAL:VGE #773 - explicit cast to avoid "Operator '+' cannot be applied to operands of type 'decimal' and 'double'"
						// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						rsBills.Set_Fields("IntAdded", rsBills.Get_Fields("IntAdded") + FCConvert.ToDecimal(dblCurrentInterest));
						rsBills.Update();
						modARStatusPayments.CreateARCHGINTForBill_6(lngKey, FCConvert.ToInt32(rsBills.Get_Fields_Int32("AccountKey")), dblCurrentInterest);
					}
					rsBills.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("Interest charged successfully!", "Interest Charged", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Application.DoEvents();
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + FCConvert.ToString(Information.Err(ex).Number) + ".", "Error Calculating Interest", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbSelected.SelectedIndex == 0)
			{
				cboBillType.SelectedIndex = -1;
				cboBillType.Enabled = false;
				fraSelected.Enabled = false;
			}
			else if (cmbSelected.SelectedIndex == 1)
			{
				cboBillType.SelectedIndex = 0;
				cboBillType.Enabled = true;
				fraSelected.Enabled = true;
				cboBillType.Focus();
			}
		}
	}
}
