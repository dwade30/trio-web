﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillPrint.
	/// </summary>
	partial class frmBillPrint : BaseForm
	{
		public fecherFoundation.FCTextBox txtTotal;
		public fecherFoundation.FCGrid vsBills;
		public fecherFoundation.FCComboBox cboRateKeys;
		public fecherFoundation.FCLabel lblRateSelect;
		public fecherFoundation.FCLabel lblSelectAccounts;
		public fecherFoundation.FCLabel lblTotal;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDelete;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle15 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle16 = new Wisej.Web.DataGridViewCellStyle();
			this.txtTotal = new fecherFoundation.FCTextBox();
			this.vsBills = new fecherFoundation.FCGrid();
			this.cboRateKeys = new fecherFoundation.FCComboBox();
			this.lblRateSelect = new fecherFoundation.FCLabel();
			this.lblSelectAccounts = new fecherFoundation.FCLabel();
			this.lblTotal = new fecherFoundation.FCLabel();
			//this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdVoidBills = new fecherFoundation.FCButton();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdVoidBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 515);
			this.BottomPanel.Size = new System.Drawing.Size(789, 96);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtTotal);
			this.ClientArea.Controls.Add(this.vsBills);
			this.ClientArea.Controls.Add(this.cboRateKeys);
			this.ClientArea.Controls.Add(this.lblRateSelect);
			this.ClientArea.Controls.Add(this.lblSelectAccounts);
			this.ClientArea.Controls.Add(this.lblTotal);
			this.ClientArea.Size = new System.Drawing.Size(789, 455);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdVoidBills);
			this.TopPanel.Size = new System.Drawing.Size(789, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdVoidBills, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(115, 30);
			this.HeaderText.Text = "Print Bills";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// txtTotal
			// 
			this.txtTotal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.txtTotal.AutoSize = false;
			this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotal.Location = new System.Drawing.Point(587, 346);
			this.txtTotal.LockedOriginal = true;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.ReadOnly = true;
			this.txtTotal.Size = new System.Drawing.Size(174, 40);
			this.txtTotal.TabIndex = 5;
			this.txtTotal.Text = "Text1";
			this.txtTotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtTotal, null);
			this.txtTotal.Visible = false;
			// 
			// vsBills
			// 
			this.vsBills.AllowSelection = false;
			this.vsBills.AllowUserToResizeColumns = false;
			this.vsBills.AllowUserToResizeRows = false;
			this.vsBills.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsBills.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBills.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBills.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBills.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBills.BackColorSel = System.Drawing.Color.Empty;
			this.vsBills.Cols = 5;
			dataGridViewCellStyle15.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBills.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
			this.vsBills.ColumnHeadersHeight = 30;
			this.vsBills.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle16.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBills.DefaultCellStyle = dataGridViewCellStyle16;
			this.vsBills.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBills.FixedCols = 0;
			this.vsBills.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBills.FrozenCols = 0;
			this.vsBills.GridColor = System.Drawing.Color.Empty;
			this.vsBills.Location = new System.Drawing.Point(30, 60);
			this.vsBills.Name = "vsBills";
			this.vsBills.ReadOnly = true;
			this.vsBills.RowHeadersVisible = false;
			this.vsBills.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBills.RowHeightMin = 0;
			this.vsBills.Rows = 1;
			this.vsBills.ShowColumnVisibilityMenu = false;
			this.vsBills.Size = new System.Drawing.Size(731, 269);
			this.vsBills.StandardTab = true;
			this.vsBills.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBills.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.vsBills, null);
			this.vsBills.Visible = false;
			this.vsBills.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBills_KeyDownEvent);
			this.vsBills.Click += new System.EventHandler(this.vsBills_ClickEvent);
			// 
			// cboRateKeys
			// 
			this.cboRateKeys.AutoSize = false;
			this.cboRateKeys.BackColor = System.Drawing.SystemColors.Window;
			this.cboRateKeys.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboRateKeys.FormattingEnabled = true;
			this.cboRateKeys.Location = new System.Drawing.Point(30, 60);
			this.cboRateKeys.Name = "cboRateKeys";
			this.cboRateKeys.Size = new System.Drawing.Size(405, 40);
			this.cboRateKeys.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.cboRateKeys, "<Rate Key Code>  <Bill Type Code>  <Bill Date>   <Rate Key Description>");
			// 
			// lblRateSelect
			// 
			this.lblRateSelect.Location = new System.Drawing.Point(30, 30);
			this.lblRateSelect.Name = "lblRateSelect";
			this.lblRateSelect.Size = new System.Drawing.Size(405, 20);
			this.lblRateSelect.TabIndex = 1;
			this.lblRateSelect.Text = "PLEASE SELECT THE RATE KEY YOU WISH TO CREATE BILLS FOR";
			this.ToolTip1.SetToolTip(this.lblRateSelect, null);
			// 
			// lblSelectAccounts
			// 
			this.lblSelectAccounts.Location = new System.Drawing.Point(30, 30);
			this.lblSelectAccounts.Name = "lblSelectAccounts";
			this.lblSelectAccounts.Size = new System.Drawing.Size(405, 20);
			this.lblSelectAccounts.TabIndex = 0;
			this.lblSelectAccounts.Text = "PLEASE SELECT THE CUSTOMERS YOU WISH TO PRINT BILLS FOR";
			this.ToolTip1.SetToolTip(this.lblSelectAccounts, null);
			this.lblSelectAccounts.Visible = false;
			// 
			// lblTotal
			// 
			this.lblTotal.Location = new System.Drawing.Point(478, 360);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Size = new System.Drawing.Size(54, 20);
			this.lblTotal.TabIndex = 4;
			this.lblTotal.Text = "TOTAL";
			this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblTotal, null);
			this.lblTotal.Visible = false;
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuProcess});
			//this.MainMenu1.Name = null;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessDelete,
				this.mnuProcessSeperator,
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessDelete
			// 
			this.mnuProcessDelete.Index = 0;
			this.mnuProcessDelete.Name = "mnuProcessDelete";
			this.mnuProcessDelete.Shortcut = Wisej.Web.Shortcut.Delete;
			this.mnuProcessDelete.Text = "Void Bill";
			this.mnuProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
			// 
			// mnuProcessSeperator
			// 
			this.mnuProcessSeperator.Index = 1;
			this.mnuProcessSeperator.Name = "mnuProcessSeperator";
			this.mnuProcessSeperator.Text = "-";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 2;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 3;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 4;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdVoidBills
			// 
			this.cmdVoidBills.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdVoidBills.AppearanceKey = "toolbarButton";
			this.cmdVoidBills.Location = new System.Drawing.Point(693, 29);
			this.cmdVoidBills.Name = "cmdVoidBills";
			this.cmdVoidBills.Shortcut = Wisej.Web.Shortcut.Delete;
			this.cmdVoidBills.Size = new System.Drawing.Size(67, 24);
			this.cmdVoidBills.TabIndex = 1;
			this.cmdVoidBills.Text = "Void Bill";
			this.ToolTip1.SetToolTip(this.cmdVoidBills, null);
			this.cmdVoidBills.Click += new System.EventHandler(this.cmdVoidBills_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(344, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(100, 48);
			this.cmdProcess.TabIndex = 0;
			this.cmdProcess.Text = "Process";
			this.ToolTip1.SetToolTip(this.cmdProcess, null);
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// frmBillPrint
			// 
			this.ClientSize = new System.Drawing.Size(789, 611);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			//this.Menu = this.MainMenu1;
			this.Name = "frmBillPrint";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Bills";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmBillPrint_Load);
			this.Activated += new System.EventHandler(this.frmBillPrint_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBillPrint_KeyPress);
			this.Resize += new System.EventHandler(this.frmBillPrint_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdVoidBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdVoidBills;
		private FCButton cmdProcess;
	}
}
