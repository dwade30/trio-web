﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using TWSharedLibrary;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for arTypeListing.
	/// </summary>
	public partial class arTypeListing : BaseSectionReport
	{
		public static arTypeListing InstancePtr
		{
			get
			{
				return (arTypeListing)Sys.GetInstance(typeof(arTypeListing));
			}
		}

		protected arTypeListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arTypeListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/15/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();

		public arTypeListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Type List";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//    switch (KeyCode)
		//    {
		//        case Keys.Escape:
		//            {
		//                Close();
		//                break;
		//            }
		//    } //end switch
		//}
		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			//this.Zoom = -1;
			Detail.CanShrink = true;
			// this lets the detail section shrink when I move/hide some of the fields
			strSQL = "SELECT * FROM DefaultBillTypes WHERE TypeCode >= " + FCConvert.ToString(frmBillTypeListing.InstancePtr.StartNumber) + " AND TypeCode <= " + FCConvert.ToString(frmBillTypeListing.InstancePtr.EndNumber) + " AND TypeTitle IS NOT NULL AND TypeTitle <> '' ORDER BY TypeCode";
			// set the data control
			rsData.OpenRecordset(strSQL);
			// set the date
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void BindFields()
		{
			// this will bring the fields on the form to the fields in the recordset
			fldCode.Text = FCConvert.ToString(rsData.Get_Fields_Int32("TypeCode"));
			fldType.Text = FCConvert.ToString(rsData.Get_Fields_String("TypeTitle"));
			fldFrequency.Text = rsData.Get_Fields_String("FrequencyCode") + "  " + rsData.Get_Fields_Int32("FirstMonth");
			fldIntMethod.Text = FCConvert.ToString(rsData.Get_Fields_String("InterestMethod"));
			if (Strings.Right(fldIntMethod.Text, 1) == "F")
			{
				fldFlatAmount.Text = "Flt Amt: " + Strings.Format(rsData.Get_Fields_Decimal("FlatAmount"), "#,##0.00");
			}
			else
			{
				fldFlatAmount.Text = "Int Rate: " + Strings.Format(rsData.Get_Fields_Double("PerDiemRate"), "0.00") + "%";
			}
			fldAccount1.Text = FCConvert.ToString(rsData.Get_Fields_String("Account1"));
			fldAccount2.Text = FCConvert.ToString(rsData.Get_Fields_String("Account2"));
			fldAccount3.Text = FCConvert.ToString(rsData.Get_Fields_String("Account3"));
			fldAccount4.Text = FCConvert.ToString(rsData.Get_Fields_String("Account4"));
			fldAccount5.Text = FCConvert.ToString(rsData.Get_Fields_String("Account5"));
			fldAccount6.Text = FCConvert.ToString(rsData.Get_Fields_String("Account6"));
			fldTitle1.Text = FCConvert.ToString(rsData.Get_Fields_String("Title1"));
			fldTitle2.Text = FCConvert.ToString(rsData.Get_Fields_String("Title2"));
			fldTitle3.Text = FCConvert.ToString(rsData.Get_Fields_String("Title3"));
			fldTitle4.Text = FCConvert.ToString(rsData.Get_Fields_String("Title4"));
			fldTitle5.Text = FCConvert.ToString(rsData.Get_Fields_String("Title5"));
			fldTitle6.Text = FCConvert.ToString(rsData.Get_Fields_String("Title6"));
			if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DefaultAccount"))) != "")
			{
				if (Strings.InStr(1, "_", FCConvert.ToString(rsData.Get_Fields_String("DefaultAccount")), CompareConstants.vbBinaryCompare) <= 0)
				{
					fldAltCash.Text = FCConvert.ToString(rsData.Get_Fields_String("DefaultAccount"));
					lblAltCash.Text = "Alt Cash Acct:";
				}
				else
				{
					lblAltCash.Text = "";
					fldAltCash.Text = "";
				}
			}
			else
			{
				lblAltCash.Text = "";
				fldAltCash.Text = "";
			}
			if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ReceivableAccount"))) != "")
			{
				if (Strings.InStr(1, "_", FCConvert.ToString(rsData.Get_Fields_String("ReceivableAccount")), CompareConstants.vbBinaryCompare) <= 0)
				{
					fldRecAcct.Text = FCConvert.ToString(rsData.Get_Fields_String("ReceivableAccount"));
					lblRecAcct.Text = "Rec Acct:";
				}
				else
				{
					lblRecAcct.Text = "";
					fldRecAcct.Text = "";
				}
			}
			else
			{
				lblRecAcct.Text = "";
				fldRecAcct.Text = "";
			}
			lblScreenTitle1.Text = FCConvert.ToString(rsData.Get_Fields_String("Title1Abbrev"));
			lblScreenTitle2.Text = FCConvert.ToString(rsData.Get_Fields_String("Title2Abbrev"));
			lblScreenTitle3.Text = FCConvert.ToString(rsData.Get_Fields_String("Title3Abbrev"));
			lblScreenTitle4.Text = FCConvert.ToString(rsData.Get_Fields_String("Title4Abbrev"));
			lblScreenTitle5.Text = FCConvert.ToString(rsData.Get_Fields_String("Title5Abbrev"));
			lblScreenTitle6.Text = FCConvert.ToString(rsData.Get_Fields_String("Title6Abbrev"));
			fldDefault1.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount1"), "#,##0.00");
			fldDefault2.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount2"), "#,##0.00");
			fldDefault3.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount3"), "#,##0.00");
			fldDefault4.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount4"), "#,##0.00");
			fldDefault5.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount5"), "#,##0.00");
			fldDefault6.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount6"), "#,##0.00");
			fldRefDesc.Text = FCConvert.ToString(rsData.Get_Fields_String("Reference"));
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("ReferenceMandatory")))
			{
				fldRefDesc.Text = fldRefDesc.Text;
				fldReq1.Text = "(R)";
			}
			else
			{
				fldReq1.Text = "";
			}
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("ReferenceChanges")))
			{
				fldChange1.Text = "(C)";
			}
			else
			{
				fldChange1.Text = "";
			}
			fldControl1Desc.Text = FCConvert.ToString(rsData.Get_Fields_String("Control1"));
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control1Mandatory")))
			{
				fldControl1Desc.Text = fldControl1Desc.Text;
				fldReq2.Text = "(R)";
			}
			else
			{
				fldReq2.Text = "";
			}
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control1Changes")))
			{
				fldChange2.Text = "(C)";
			}
			else
			{
				fldChange2.Text = "";
			}
			fldControl2Desc.Text = FCConvert.ToString(rsData.Get_Fields_String("Control2"));
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control2Mandatory")))
			{
				fldControl2Desc.Text = fldControl2Desc.Text;
				fldReq3.Text = "(R)";
			}
			else
			{
				fldReq3.Text = "";
			}
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control2Changes")))
			{
				fldChange3.Text = "(C)";
			}
			else
			{
				fldChange3.Text = "";
			}
			fldControl3Desc.Text = FCConvert.ToString(rsData.Get_Fields_String("Control3"));
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control3Mandatory")))
			{
				fldControl3Desc.Text = fldControl3Desc.Text;
				fldReq4.Text = "(R)";
			}
			else
			{
				fldReq4.Text = "";
			}
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control3Changes")))
			{
				fldChange4.Text = "(C)";
			}
			else
			{
				fldChange4.Text = "";
			}
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("SalesTax")))
			{
				fldSalesTax.Text = "Y";
			}
			else
			{
				fldSalesTax.Text = "N";
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsData.EndOfFile() != true)
			{
				BindFields();
				if (Strings.Trim(rsData.Get_Fields_String("Title1") + " ") != "")
				{
					fldAccount1.Visible = true;
					fldTitle1.Visible = true;
					fldDefault1.Visible = true;
					lblScreenTitle1.Visible = true;
				}
				else
				{
					fldAccount1.Visible = false;
					fldTitle1.Visible = false;
					fldDefault1.Visible = false;
					lblScreenTitle1.Visible = false;
				}
				if (Strings.Trim(rsData.Get_Fields_String("Title2") + " ") != "")
				{
					fldAccount2.Visible = true;
					fldTitle2.Visible = true;
					fldDefault2.Visible = true;
					lblScreenTitle2.Visible = true;
				}
				else
				{
					fldAccount2.Visible = false;
					fldTitle2.Visible = false;
					fldDefault2.Visible = false;
					lblScreenTitle2.Visible = false;
				}
				if (Strings.Trim(rsData.Get_Fields_String("Title3") + " ") != "")
				{
					fldAccount3.Visible = true;
					fldTitle3.Visible = true;
					fldDefault3.Visible = true;
					lblScreenTitle3.Visible = true;
				}
				else
				{
					fldAccount3.Visible = false;
					fldTitle3.Visible = false;
					fldDefault3.Visible = false;
					lblScreenTitle3.Visible = false;
				}
				if (Strings.Trim(rsData.Get_Fields_String("Title4") + " ") != "")
				{
					fldAccount4.Visible = true;
					fldTitle4.Visible = true;
					fldDefault4.Visible = true;
					lblScreenTitle4.Visible = true;
				}
				else
				{
					fldAccount4.Visible = false;
					fldTitle4.Visible = false;
					fldDefault4.Visible = false;
					lblScreenTitle4.Visible = false;
				}
				if (Strings.Trim(rsData.Get_Fields_String("Title5") + " ") != "")
				{
					fldAccount5.Visible = true;
					fldTitle5.Visible = true;
					fldDefault5.Visible = true;
					lblScreenTitle5.Visible = true;
				}
				else
				{
					fldAccount5.Visible = false;
					fldTitle5.Visible = false;
					fldDefault5.Visible = false;
					lblScreenTitle5.Visible = false;
				}
				if (Strings.Trim(rsData.Get_Fields_String("Title6") + " ") != "")
				{
					fldAccount6.Visible = true;
					fldTitle6.Visible = true;
					fldDefault6.Visible = true;
					lblScreenTitle6.Visible = true;
				}
				else
				{
					fldAccount6.Visible = false;
					fldTitle6.Visible = false;
					fldDefault6.Visible = false;
					lblScreenTitle6.Visible = false;
				}
				rsData.MoveNext();
			}
		}

		
	}
}
