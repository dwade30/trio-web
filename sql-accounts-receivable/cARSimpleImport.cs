//Fecher vbPorter - Version 1.0.0.62
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using System.IO;

namespace TWAR0000
{
	public class cARSimpleImport
	{

		//=========================================================

		private string strLastError = "";
		private int lngLastError;
		private FCCollection collBadAccounts = new FCCollection();

		public FCCollection BadAccounts
		{
			get
			{
				return collBadAccounts;
			}
		}

		public string LastErrorMessage
		{
			get
			{
					string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
					int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
					object HadError = null;
				HadError = lngLastError!=0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
			collBadAccounts = new FCCollection();
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		private FCCollection GetItems(string strFileName)
		{
			FCCollection collReturn = new FCCollection();
			Dictionary<object, object> dAccounts = new Dictionary<object, object>();

			if (!File.Exists(strFileName)) {
				fecherFoundation.Information.Err().Raise(9999,"" , "File not found", null, null);
			}
			string strLine = "";
			StreamReader ts = null;

			string[] strAry = null;
			cSimpleImportItem sItem;

			ts = File.OpenText(strFileName);
			while (!ts.EndOfStream)
			{
				strLine = ts.ReadLine();
				if (fecherFoundation.Strings.Trim(strLine) != "")
				{
					strAry = Strings.Split(strLine, "\t", -1,CompareConstants.vbBinaryCompare);
					if (Information.UBound((string[])strAry, 1) > 3)
					{
						// more than 4 elements
						sItem = new cSimpleImportItem();
						sItem.AccountNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(((object[])strAry)[0])));
						if (sItem.AccountNumber > 0)
						{
							sItem.Name = fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strAry)[1]).Replace(FCConvert.ToString(Convert.ToChar(34)), ""));
							sItem.Description = fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strAry)[3]));
							if (sItem.Description.Length > 1)
							{
								if (Strings.Left(sItem.Description, 1) == FCConvert.ToString(Convert.ToChar(34)))
								{
									sItem.Description = Strings.Mid(sItem.Description, 2);
								}
							}
							if (sItem.Description.Length > 1)
							{
								if (Strings.Right(sItem.Description, 1) == FCConvert.ToString(Convert.ToChar(34)))
								{
									sItem.Description = Strings.Mid(sItem.Description, 1, sItem.Description.Length - 1);
								}
							}
							sItem.SetAmount(1, Conversion.Val(FCConvert.ToString(((object[])strAry)[4]).Replace("$", "")));
							collReturn.Add(sItem);
						}
					}
				}
			}
			return collReturn;
		}

		private void CreateCustomerBills(ref FCCollection collItems,int lngBillType)
		{
			if (!(collItems==null)) {
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsCust = new clsDRWrapper();
				short x;
				foreach (cSimpleImportItem sItem in collItems)
				{
					//Application.DoEvents();
					rsCust.OpenRecordset("select * from customermaster where customerid = " + FCConvert.ToString(sItem.AccountNumber), "AccountsReceivable");
					if (rsCust.EndOfFile())
					{
						collBadAccounts.Add(sItem.AccountNumber);
						SetError(9998, "Customer account not found");
					}
					else
					{
						if (FCConvert.ToString(rsCust.Get_Fields("status")) != "A")
						{
							collBadAccounts.Add(sItem.AccountNumber);
							SetError(9998, "Customer is not active");
						}
						else
						{
							rsSave.OpenRecordset("select * from customerbills where customerid = " + FCConvert.ToString(sItem.AccountNumber) + " and [Type] = " + FCConvert.ToString(lngBillType), "AccountsReceivable");
							if (rsSave.EndOfFile())
							{
								rsSave.AddNew();
								rsSave.Set_Fields("Customerid", sItem.AccountNumber);
								rsSave.Set_Fields("type", lngBillType);
							}
							else
							{
								rsSave.Edit();
							}
							rsSave.Set_Fields("reference", "");
							rsSave.Set_Fields("Control1", "");
							rsSave.Set_Fields("Control2", "");
							rsSave.Set_Fields("Control3", "");
							rsSave.Set_Fields("Exclude", true);
							rsSave.Set_Fields("Prorate", false);
							rsSave.Set_Fields("Comment", "");
							for(x = 1; x <= 6; x++)
							{
								rsSave.Set_Fields("quantity" + x, 0);
								rsSave.Set_Fields("Amount" + x, sItem.GetAmount(x));
								if (sItem.GetAmount(x) > 0)
								{
									rsSave.Set_Fields("quantity" + x, 1);
									rsSave.Set_Fields("EXCLUDE", false);
								}
							}
							rsSave.Set_Fields("Startdate", null);
							rsSave.Update();
							rsCust.Edit();
							rsCust.Set_Fields("Billmessage", sItem.Description);
							rsCust.Set_Fields("OneTimeBillMessage", true);
							rsCust.Update();
						}
					}
				}
			}
		}

		public void ImportBillItems(string strFileName, int lngBillType)
		{
			try
			{	// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ClearErrors();
				FCCollection collItems = new FCCollection();
				collItems = GetItems(strFileName);
				CreateCustomerBills(ref collItems, lngBillType);
				return;
			}
			catch (Exception ex)
			{	
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
			}
		}

		public string GetBadAccountsMessage()
		{
			string GetBadAccountsMessage = "";
			string strReturn = "";
			string strSepar = "";
			if (collBadAccounts.Count == 0)
			{
				return GetBadAccountsMessage;
			}
			if (collBadAccounts.Count == 1)
			{
				strReturn = "The following account is inactive or could not be found ";
			}
			else
			{
				strReturn = "The following accounts are inactive or could not be found ";
			}
			foreach (int lngAccount in collBadAccounts)
			{
				strReturn += strSepar+lngAccount;
				strSepar = ", ";
			}
			GetBadAccountsMessage = strReturn;
			return GetBadAccountsMessage;
		}

	}
}