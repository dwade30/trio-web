﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWAR0000
{
	
	public partial class frmBillPrep : BaseForm
    {
        public int BilledMonth { get; set; } = 0;
        public int BilledYear { get; set; } = 0;
        private bool loadComplete = false;

		public frmBillPrep()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtTitle = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.lblTitle = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.txtTitle.AddControlArrayElement(txtTitle_3, 3);
			this.txtTitle.AddControlArrayElement(txtTitle_0, 0);
			this.txtTitle.AddControlArrayElement(txtTitle_1, 1);
			this.txtTitle.AddControlArrayElement(txtTitle_2, 2);
			this.lblTitle.AddControlArrayElement(lblTitle_3, 3);
			this.lblTitle.AddControlArrayElement(lblTitle_0, 0);
			this.lblTitle.AddControlArrayElement(lblTitle_1, 1);
			this.lblTitle.AddControlArrayElement(lblTitle_2, 2);
            vsAccounts.BeforeRowColChange += VsAccounts_BeforeRowColChange;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.cmbName.SelectedIndex = 0;
		}

        private void VsAccounts_BeforeRowColChange(object sender, BeforeRowColChangeEventArgs e)
        {
            if (e.NewCol == lngSelectCol)
            {
                vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            }
            else
            {
                vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
            }
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmBillPrep InstancePtr
		{
			get
			{
				return (frmBillPrep)Sys.GetInstance(typeof(frmBillPrep));
			}
		}

		protected frmBillPrep _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int lngBillKeyCol;
		int lngSelectCol;
		int lngDataEntryMessageCol;
		int lngColAccount;
		int lngColName;
		int lngColTax1;
		int lngColTax2;
		int lngColTax3;
		int lngColTax4;
		int lngColTax5;
		int lngColTax6;
		int lngColQuantity1;
		int lngColQuantity2;
		int lngColQuantity3;
		int lngColQuantity4;
		int lngColQuantity5;
		int lngColQuantity6;
		int lngControl1Col;
		int lngControl2Col;
		int lngControl3Col;
		int lngReferenceCol;
		bool blnReferenceRequired;
		bool blnControl1Required;
		bool blnControl2Required;
		bool blnControl3Required;
		bool blnFee1;
		bool blnFee2;
		bool blnFee3;
		bool blnFee4;
		bool blnFee5;
		bool blnFee6;
		bool blnReference;
		bool blnControl1;
		bool blnControl2;
		bool blnControl3;
		// vbPorter upgrade warning: intBillType As int	OnWrite(string)
		public int intBillType;
		clsDRWrapper rsAR = new clsDRWrapper();
		bool blnMoveToCreate;
		int lngOriginalGridWidth;
		public bool blnFromCustomerMaster;
		bool blnDemandBill;

		private void frmBillPrep_Activated(object sender, System.EventArgs e)
		{
			//FC:FINAL:SBE - #290 - added flag for triggering BeforeRowColChange after form was loaded
			loadComplete = false;
			int counter;
			clsDRWrapper rsExclude = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
			//Application.DoEvents();
			if (!blnFromCustomerMaster)
			{
				lngOriginalGridWidth = vsAccounts.WidthOriginal;
				FormatFeesGrid();
				ProcessType();
				FormatGrid();
				if (!blnDemandBill)
				{
					LoadAccountInformation();
				}
				else
				{
					rsExclude.Execute("UPDATE CustomerBills SET Exclude = 1 WHERE Type = " + FCConvert.ToString(intBillType), "TWAR0000.vb1");
				}
				SetGridHeight();
				if (vsAccounts.Rows > 1)
				{
					for (counter = 2; counter <= vsAccounts.Cols - 1; counter++)
					{
						if (vsAccounts.ColHidden(counter) != true)
						{
							vsAccounts.Select(1, counter);
							break;
						}
					}
					vsAccounts.Focus();
				}
			}
			//FC:FINAL:SBE - #290 - added flag for triggering BeforeRowColChange after form was loaded
			loadComplete = true;
		}

		private void frmBillPrep_Load(object sender, System.EventArgs e)
		{
			//FC:FINAL:SBE - #290 - added flag for triggering BeforeRowColChange after form was loaded
			loadComplete = false;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			blnMoveToCreate = false;
			// these will keep track of the columns even if I move them
			lngSelectCol = 0;
			lngColAccount = 1;
			lngColName = 2;
			lngReferenceCol = 3;
			lngControl1Col = 4;
			lngControl2Col = 5;
			lngControl3Col = 6;
			lngColTax1 = 7;
			lngColTax2 = 8;
			lngColTax3 = 9;
			lngColTax4 = 10;
			lngColTax5 = 11;
			lngColTax6 = 12;
			lngColQuantity1 = 13;
			lngColQuantity2 = 14;
			lngColQuantity3 = 15;
			lngColQuantity4 = 16;
			lngColQuantity5 = 17;
			lngColQuantity6 = 18;
			lngBillKeyCol = 19;
			lngDataEntryMessageCol = 20;
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(rsDefaultInfo.Get_Fields_String("FrequencyCode")) == "D")
				{
					btnAddCustomer.Enabled = true;
					btnFileNewCustomer.Enabled = true;
					rsAR.OpenRecordset("Select * FROM CustomerMaster");
					blnDemandBill = true;
				}
				else
				{
					btnAddCustomer.Enabled = false;
					btnFileNewCustomer.Enabled = false;
					blnDemandBill = false;
				}
			}
			else
			{
				btnAddCustomer.Enabled = false;
			}
            btnImportBills.Visible = modGlobalConstants.Statics.MuniName.ToLower() == "islesboro";
            
            
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:AM: attach resize event handler here
			this.Resize += frmBillPrep_Resize;
			//FC:FINAL:SBE - #290 - added flag for triggering BeforeRowColChange after form was loaded
			loadComplete = true;
		}

		private void frmBillPrep_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				//Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//JEI:IT289: it should be Unload not Hide
			//frmBillSelect.InstancePtr.Hide();
			frmBillSelect.InstancePtr.Unload();
			if (blnMoveToCreate == false)
			{
				//MDIParent.InstancePtr.Focus();
			}
		}

		private void frmBillPrep_Resize(object sender, System.EventArgs e)
		{
			vsAccounts.ColWidth(lngSelectCol, FCConvert.ToInt32(lngOriginalGridWidth * 0.1));
			vsAccounts.ColWidth(lngColAccount, FCConvert.ToInt32(lngOriginalGridWidth * 0.25));
			vsAccounts.ColWidth(lngColName, FCConvert.ToInt32(lngOriginalGridWidth * 0.64));
			vsAccounts.ColWidth(lngColTax1, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax2, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax3, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax4, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax5, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax6, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccountSearch.ColWidth(0, 0);
			vsAccountSearch.ColWidth(1, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.12));
			// account number
			vsAccountSearch.ColWidth(2, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.5));
			// name
			vsAccountSearch.ColWidth(3, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.36));
			// maplot
			vsFees.ColWidth(0, 0);
			// Category Number
			vsFees.ColWidth(1, FCConvert.ToInt32(vsFees.WidthOriginal * 0.2));
			// Title
			vsFees.ColWidth(2, FCConvert.ToInt32(vsFees.WidthOriginal * 0.2));
			// Amount
			vsFees.ColWidth(3, FCConvert.ToInt32(vsFees.WidthOriginal * 0.2));
			// Amount
			vsFees.ColWidth(4, FCConvert.ToInt32(vsFees.WidthOriginal * 0.2));
			// Amount
		}

		private void mnuAddCustomer_Click(object sender, System.EventArgs e)
		{
			// this will show the search frame
			btnAddCustomer.Enabled = false;
            //FC:FINAL:BSE #2139 remove save button
			//btnFileSave.Enabled = false;
			btnProcessSave.Enabled = false;
			btnFileBillingEditReport.Enabled = false;
			fraAccountSearch.Top = vsAccounts.Top;
			//fraAccountSearch.Left = FCConvert.ToInt32((this.Width - fraAccountSearch.Width) / 2.0);
			fraAccountSearch.Left = 30;
			fraAccountSearch.Height = this.ClientArea.Height - 96;
			fraAccountSearch.Width = this.ClientArea.Width - 60;
			vsAccounts.Visible = false;
			fraAccountSearch.Visible = true;
			if (txtSearchName.Enabled && txtSearchName.Visible)
			{
				txtSearchName.Focus();
			}
			txtSearchName.Text = "";
			vsAccountSearch.Visible = false;
			vsAccountSearch.Rows = 1;
		}

		private void mnuFileBillingEditReport_Click(object sender, System.EventArgs e)
		{
			if (SaveInformation())
            {
                rptBillingEditReport.InstancePtr.BillMonth = BilledMonth;
                rptBillingEditReport.InstancePtr.BillYear = BilledYear;

                frmReportViewer.InstancePtr.Init(rptBillingEditReport.InstancePtr);
                
			}
		}

		private void mnuFileClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
			{
				vsAccounts.TextMatrix(counter, lngSelectCol, FCConvert.ToString(false));
			}
		}

		private void mnuFileNewCustomer_Click(object sender, System.EventArgs e)
		{
			//FC:FINA:KV:IIT807+FC-8697
			modGlobal.Statics.blnFromBillPrep = true;
			frmCustomerMaster.InstancePtr.blnEdit = false;
			frmCustomerMaster.InstancePtr.Show(App.MainForm);
			this.Hide();
			// show the blankform
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInformation();
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
			{
				vsAccounts.TextMatrix(counter, lngSelectCol, FCConvert.ToString(true));
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FormatGrid()
		{
			vsAccounts.Cols = 21;
			vsAccounts.ColHidden(lngBillKeyCol, true);
			vsAccounts.ColHidden(lngDataEntryMessageCol, true);
			vsAccounts.ColWidth(lngSelectCol, FCConvert.ToInt32(lngOriginalGridWidth * 0.1));
			vsAccounts.ColWidth(lngColAccount, FCConvert.ToInt32(lngOriginalGridWidth * 0.25));
			vsAccounts.ColWidth(lngColName, FCConvert.ToInt32(lngOriginalGridWidth * 0.64));
			vsAccounts.ColWidth(lngColTax1, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax2, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax3, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax4, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax5, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColTax6, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColQuantity1, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColQuantity2, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColQuantity3, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColQuantity4, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColQuantity5, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			vsAccounts.ColWidth(lngColQuantity6, FCConvert.ToInt32(lngOriginalGridWidth * 0.13));
			// .ExtendLastCol = True
			// .ScrollBars = flexScrollBarNone
			// .FrozenCols = 2
			// set the alignment
			vsAccounts.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngColTax1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngReferenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngControl1Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngControl2Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngControl3Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColFormat(lngColTax1, "##0.00");
			vsAccounts.ColFormat(lngColTax2, "##0.00");
			vsAccounts.ColFormat(lngColTax3, "##0.00");
			vsAccounts.ColFormat(lngColTax4, "##0.00");
			vsAccounts.ColFormat(lngColTax5, "##0.00");
			vsAccounts.ColFormat(lngColTax6, "##0.00");
			vsAccounts.ColDataType(lngSelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsAccounts.TextMatrix(0, lngColAccount, "Acct");
			vsAccounts.TextMatrix(0, lngColName, "Name");
			SetGridHeadings();
            vsAccounts.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
			vsAccounts.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSortShow;
			// show a border between the titles and the data input section of the grid
			vsAccounts.Select(0, 0, 0, vsAccounts.Cols - 1);
			vsAccounts.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			//FC:FINAL:AM:#i413 - ClearDetail is triggered in VB6 by vsAccounts.Select
			ClearDetail();
		}

		private void SetGridHeadings()
		{
			clsDRWrapper rsTypeInfo = new clsDRWrapper();
			rsTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
			if (rsTypeInfo.EndOfFile() != true && rsTypeInfo.BeginningOfFile() != true)
			{
				vsAccounts.TextMatrix(0, lngColTax1, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title1")));
				vsAccounts.TextMatrix(0, lngColTax2, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title2")));
				vsAccounts.TextMatrix(0, lngColTax3, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title3")));
				vsAccounts.TextMatrix(0, lngColTax4, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title4")));
				vsAccounts.TextMatrix(0, lngColTax5, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title5")));
				vsAccounts.TextMatrix(0, lngColTax6, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title6")));
				vsAccounts.TextMatrix(0, lngReferenceCol, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Reference")));
				vsAccounts.TextMatrix(0, lngControl1Col, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control1")));
				vsAccounts.TextMatrix(0, lngControl2Col, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control2")));
				vsAccounts.TextMatrix(0, lngControl3Col, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control3")));
				blnReferenceRequired = FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("ReferenceMandatory"));
				blnControl1Required = FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control1Mandatory"));
				blnControl2Required = FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control2Mandatory"));
				blnControl3Required = FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control3Mandatory"));
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title1"))) != "")
				{
					blnFee1 = true;
				}
				else
				{
					blnFee1 = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title2"))) != "")
				{
					blnFee2 = true;
				}
				else
				{
					blnFee2 = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title3"))) != "")
				{
					blnFee3 = true;
				}
				else
				{
					blnFee3 = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title4"))) != "")
				{
					blnFee4 = true;
				}
				else
				{
					blnFee4 = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title5"))) != "")
				{
					blnFee5 = true;
				}
				else
				{
					blnFee5 = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title6"))) != "")
				{
					blnFee6 = true;
				}
				else
				{
					blnFee6 = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Reference"))) != "" && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("ReferenceChanges")))
				{
					blnReference = true;
				}
				else
				{
					blnReference = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control1"))) != "" && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control1Changes")))
				{
					blnControl1 = true;
				}
				else
				{
					blnControl1 = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control2"))) != "" && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control2Changes")))
				{
					blnControl2 = true;
				}
				else
				{
					blnControl2 = false;
				}
				if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control3"))) != "" && FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control3Changes")))
				{
					blnControl3 = true;
				}
				else
				{
					blnControl3 = false;
				}
			}
			else
			{
				vsAccounts.TextMatrix(0, lngColTax1, "");
				vsAccounts.TextMatrix(0, lngColTax2, "");
				vsAccounts.TextMatrix(0, lngColTax3, "");
				vsAccounts.TextMatrix(0, lngColTax4, "");
				vsAccounts.TextMatrix(0, lngColTax5, "");
				vsAccounts.TextMatrix(0, lngColTax6, "");
				vsAccounts.TextMatrix(0, lngReferenceCol, "");
				vsAccounts.TextMatrix(0, lngControl1Col, "");
				vsAccounts.TextMatrix(0, lngControl2Col, "");
				vsAccounts.TextMatrix(0, lngControl3Col, "");
				blnReferenceRequired = false;
				blnControl1Required = false;
				blnControl2Required = false;
				blnControl3Required = false;
				blnFee1 = false;
				blnFee2 = false;
				blnFee3 = false;
				blnFee4 = false;
				blnFee5 = false;
				blnFee6 = false;
				blnReference = false;
				blnControl1 = false;
				blnControl2 = false;
				blnControl3 = false;
			}
			vsAccounts.ColHidden(lngColTax1, true);
			vsAccounts.ColHidden(lngColTax2, true);
			vsAccounts.ColHidden(lngColTax3, true);
			vsAccounts.ColHidden(lngColTax4, true);
			vsAccounts.ColHidden(lngColTax5, true);
			vsAccounts.ColHidden(lngColTax6, true);
			vsAccounts.ColHidden(lngReferenceCol, true);
			vsAccounts.ColHidden(lngControl1Col, true);
			vsAccounts.ColHidden(lngControl2Col, true);
			vsAccounts.ColHidden(lngControl3Col, true);
			vsAccounts.ColHidden(lngColQuantity1, true);
			vsAccounts.ColHidden(lngColQuantity2, true);
			vsAccounts.ColHidden(lngColQuantity3, true);
			vsAccounts.ColHidden(lngColQuantity4, true);
			vsAccounts.ColHidden(lngColQuantity5, true);
			vsAccounts.ColHidden(lngColQuantity6, true);
		}

		private void LoadAccountInformation(int lngAcct = -1)
		{
			// this function will accept an account number and will fill the grid with the name from that account
			clsDRWrapper rsBillCheck = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			string strExistingCustomers;
			int counter;
			strExistingCustomers = "(";
			for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
			{
				strExistingCustomers += vsAccounts.TextMatrix(counter, lngColAccount) + ", ";
			}
			if (strExistingCustomers != "(")
			{
				strExistingCustomers = Strings.Left(strExistingCustomers, strExistingCustomers.Length - 2) + ")";
			}
			else
			{
				strExistingCustomers = "";
			}
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
			if (lngAcct == -1)
			{
				// rsBillCheck.OpenRecordset "SELECT CustomerBills.*, CustomerMaster.CustomerID as CustomerID, Name, Status, DataEntryMessage FROM CustomerBills INNER JOIN CustomerMaster ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE Status = 'A' AND Type = " & intBillType & " ORDER BY Name"
				rsBillCheck.OpenRecordset("SELECT b.*, c.CustomerID as CustomerID, c.Status, c.PartyID, c.DataEntryMessage, p.* FROM CustomerBills as b INNER JOIN CustomerMaster as c ON b.CustomerID = c.CustomerID CROSS APPLY " + rsBillCheck.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE c.Status = 'A' AND b.Type = " + FCConvert.ToString(intBillType) + " ORDER BY p.FullName");
			}
			else
			{
				if (strExistingCustomers != "")
				{
					// rsBillCheck.OpenRecordset "SELECT CustomerBills.*, CustomerMaster.CustomerID as CustomerID, Name, Status, DataEntryMessage FROM CustomerBills INNER JOIN CustomerMaster ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE CustomerMaster.CustomerID = " & lngAcct & " AND CustomerMaster.CustomerID NOT IN " & strExistingCustomers & " AND Status = 'A' AND Type = " & intBillType & " ORDER BY Name"
					rsBillCheck.OpenRecordset("SELECT b.*, c.CustomerID as CustomerID, c.Status, c.PartyID, c.DataEntryMessage, p.* FROM CustomerBills as b INNER JOIN CustomerMaster as c ON b.CustomerID = c.CustomerID CROSS APPLY " + rsBillCheck.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE c.CustomerID = " + FCConvert.ToString(lngAcct) + " AND c.CustomerID NOT IN " + strExistingCustomers + " AND c.Status = 'A' AND b.Type = " + FCConvert.ToString(intBillType) + " ORDER BY p.FullName");
				}
				else
				{
					// rsBillCheck.OpenRecordset "SELECT CustomerBills.*, CustomerMaster.CustomerID as CustomerID, Name, Status, DataEntryMessage FROM CustomerBills INNER JOIN CustomerMaster ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE CustomerMaster.CustomerID = " & lngAcct & " AND Status = 'A' AND Type = " & intBillType & " ORDER BY Name"
					rsBillCheck.OpenRecordset("SELECT b.*, c.CustomerID as CustomerID, c.Status, c.PartyID, c.DataEntryMessage, p.* FROM CustomerBills as b INNER JOIN CustomerMaster as c ON b.CustomerID = c.CustomerID CROSS APPLY " + rsBillCheck.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE c.CustomerID = " + FCConvert.ToString(lngAcct) + " AND c.Status = 'A' AND b.Type = " + FCConvert.ToString(intBillType) + " ORDER BY p.FullName");
				}
			}
			if (rsBillCheck.EndOfFile() != true && rsBillCheck.BeginningOfFile() != true)
			{
				do
				{
					vsAccounts.AddItem("");
					// matching account number is the current record
					if (lngAcct == -1)
					{
						if (rsBillCheck.Get_Fields_Boolean("Exclude") == true)
						{
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngSelectCol, FCConvert.ToString(false));
						}
						else
						{
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngSelectCol, FCConvert.ToString(true));
						}
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngSelectCol, FCConvert.ToString(true));
					}
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColAccount, FCConvert.ToString(rsBillCheck.Get_Fields_Int32("CustomerID")));
					if (Strings.Trim(FCConvert.ToString(rsBillCheck.Get_Fields_String("DataEntryMessage"))) != "")
					{
						vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Rows - 1, lngColAccount, vsAccounts.Rows - 1, lngColName, Color.Blue);
					}
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColName, FCConvert.ToString(rsBillCheck.Get_Fields_String("FullName")));
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngBillKeyCol, FCConvert.ToString(rsBillCheck.Get_Fields_Int32("ID")));
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngDataEntryMessageCol, FCConvert.ToString(rsBillCheck.Get_Fields_String("DataEntryMessage")));
                    vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity1,"1.00");
                    vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity2, "1.00");
                    vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity3,"1.00");
                    vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity4, "1.00");
                    vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity5, "1.00");
                    vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity6, "1.00");

                    if (blnFee1)
					{
						// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBillCheck.Get_Fields("Amount1")) >= 0)
						{
							// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax1, Strings.Format(rsBillCheck.Get_Fields("Amount1"), "##0.00##"));
                            vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity1,
                                Strings.Format(rsBillCheck.Get_Fields_Double("Quantity1"),"##0.00##"));
                        }
						else
						{
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax1, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount1"), "##0.00##"));
						}
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax1, "");
					}
					if (blnFee2)
					{
						// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBillCheck.Get_Fields("Amount2")) >= 0)
						{
							// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax2, Strings.Format(rsBillCheck.Get_Fields("Amount2"), "##0.0000"));
                            vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity2,
                                Strings.Format(rsBillCheck.Get_Fields_Double("Quantity2"), "##0.00##"));
                        }
						else
						{
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax2, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount2"), "##0.0000"));
						}
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax2, "");
					}
					if (blnFee3)
					{
						// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBillCheck.Get_Fields("Amount3")) >= 0)
						{
							// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax3, Strings.Format(rsBillCheck.Get_Fields("Amount3"), "##0.0000"));
                            vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity3,
                                Strings.Format(rsBillCheck.Get_Fields_Double("Quantity3"), "##0.00##"));
                        }
						else
						{
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax3, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount3"), "##0.0000"));
						}
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax3, "");
					}
					if (blnFee4)
					{
						// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBillCheck.Get_Fields("Amount4")) >= 0)
						{
							// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax4, Strings.Format(rsBillCheck.Get_Fields("Amount4"), "##0.0000"));
                            vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity4,
                                Strings.Format(rsBillCheck.Get_Fields_Double("Quantity4"), "##0.00##"));
                        }
						else
						{
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax4, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount4"), "##0.0000"));
						}
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax4, "");
					}
					if (blnFee5)
					{
						// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBillCheck.Get_Fields("Amount5")) >= 0)
						{
							// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax5, Strings.Format(rsBillCheck.Get_Fields("Amount5"), "##0.0000"));
                            vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity5,
                                Strings.Format(rsBillCheck.Get_Fields_Double("Quantity5"), "##0.00##"));
                        }
						else
						{
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax5, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount5"), "##0.0000"));
						}
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax5, "");
					}
					if (blnFee6)
					{
						// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBillCheck.Get_Fields("Amount6")) >= 0)
						{
							// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax6, Strings.Format(rsBillCheck.Get_Fields("Amount6"), "##0.0000"));
                            vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity6,
                                Strings.Format(rsBillCheck.Get_Fields_Double("Quantity6"), "##0.00##"));
                        }
						else
						{
							vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax6, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount6"), "##0.0000"));
						}
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax6, "");
					}
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngReferenceCol, FCConvert.ToString(rsBillCheck.Get_Fields_String("Reference")));
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngControl1Col, FCConvert.ToString(rsBillCheck.Get_Fields_String("Control1")));
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngControl2Col, FCConvert.ToString(rsBillCheck.Get_Fields_String("Control2")));
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngControl3Col, FCConvert.ToString(rsBillCheck.Get_Fields_String("Control3")));

					// SetGridHeight
					rsBillCheck.MoveNext();
				}
				while (rsBillCheck.EndOfFile() != true);
			}
			else
			{
				if (lngAcct != -1)
				{
					LoadAddedAccountInformation(lngAcct);
				}
			}
			if (blnReference && blnReferenceRequired)
			{
				txtTitle[0].BackColor = ColorTranslator.FromOle(0xC0FFFF);
			}
			if (blnControl1 && blnControl1Required)
			{
				txtTitle[1].BackColor = ColorTranslator.FromOle(0xC0FFFF);
			}
			if (blnControl2 && blnControl1Required)
			{
				txtTitle[2].BackColor = ColorTranslator.FromOle(0xC0FFFF);
			}
			if (blnControl3 && blnControl1Required)
			{
				txtTitle[3].BackColor = ColorTranslator.FromOle(0xC0FFFF);
			}
			vsAccounts.Row = vsAccounts.Rows - 1;
		}

		private void SetGridHeight()
		{
			int lngGridWidth;
			bool blnHorScrollBar = false;
			bool blnVerScrollBar;
			int counter;
			lngGridWidth = 0;
			for (counter = 0; counter <= vsAccounts.Cols - 1; counter++)
			{
				if (!vsAccounts.ColHidden(counter))
				{
					lngGridWidth += vsAccounts.ColWidth(counter);
				}
			}
			if (lngGridWidth > lngOriginalGridWidth)
			{
				blnHorScrollBar = true;
			}
			else
			{
				blnHorScrollBar = false;
			}
			if (vsAccounts.Rows > 27 || (vsAccounts.Rows == 27 && blnHorScrollBar))
			{
				blnVerScrollBar = true;
			}
			else
			{
				blnVerScrollBar = false;
			}
			if (vsAccounts.Rows > 6)
			{
				// too many rows to be shown
				//vsAccounts.Height = (9 * vsAccounts.RowHeight(0)) + 70;
				vsAccounts.Height = 232;
			}
			else
			{
				vsAccounts.Height = (vsAccounts.Rows - 1) * 40 + vsAccounts.ColumnHeadersHeight + 2;
			}

		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intAnswer As short, int --> As DialogResult
			DialogResult intAnswer;
			// save the information
			if (SaveInformation())
			{
				if (btnAddCustomer.Enabled == true)
				{
					intAnswer = MessageBox.Show("Would you like to create bills now?", "Create Bills?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intAnswer == DialogResult.Yes)
					{
						blnMoveToCreate = true;
						frmBillCreate.InstancePtr.intBillType = intBillType;
						frmBillCreate.InstancePtr.Show(App.MainForm);
					}
					else
					{
						blnMoveToCreate = false;
					}
				}
				else
				{
					blnMoveToCreate = false;
				}
				Close();
			}
		}

		private void Grid_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsAccounts_AfterUserResize(grid.GetFlexRowIndex(e.RowIndex), -1);
		}

		private void Grid_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsAccounts_AfterUserResize(-1, grid.GetFlexColIndex(e.Column.Index));
		}

		private void vsAccounts_AfterUserResize(int row, int col)
		{
			SetGridHeight();
		}

		private void vsAccounts_BeforeRowColChange(object sender, System.EventArgs e)
		{
			//FC:FINAL:SBE - We don't have to check the row. Event will be triggered only on row change
			//if (e.oldRow!=e.newRow) 
			//FC:FINAL:SBE - #290 - added flag for triggering BeforeRowColChange after form was loaded
			if (loadComplete)
			{
				SaveDetails();
			}
		}

		private void vsAccounts_Enter(object sender, System.EventArgs e)
		{
			vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsAccounts_ClickEvent(object sender, System.EventArgs e)
		{
			//if (vsAccounts.MouseRow > 0)
			//{
			//	if (FCUtils.CBool(vsAccounts.TextMatrix(vsAccounts.MouseRow, lngSelectCol)) == true)
			//	{
			//		vsAccounts.TextMatrix(vsAccounts.MouseRow, lngSelectCol, FCConvert.ToString(false));
			//	}
			//	else
			//	{
			//		vsAccounts.TextMatrix(vsAccounts.MouseRow, lngSelectCol, FCConvert.ToString(true));
			//	}
			//}
		}

		private void vsAccounts_KeyDownEvent(object sender, KeyEventArgs e)
		{
			//if (vsAccounts.Row > 0)
			//{
			//	if (e.KeyCode == Keys.Space)
			//	{
			//		//FC:TODO:AM
			//		//e.KeyCode = 0;
			//		if (FCUtils.CBool(vsAccounts.TextMatrix(vsAccounts.Row, lngSelectCol)) == true)
			//		{
			//			vsAccounts.TextMatrix(vsAccounts.Row, lngSelectCol, FCConvert.ToString(false));
			//		}
			//		else
			//		{
			//			vsAccounts.TextMatrix(vsAccounts.Row, lngSelectCol, FCConvert.ToString(true));
			//		}
			//	}
			//}
		}
		// Private Sub vsAccounts_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
		// Select Case Col
		// Case lngColTax1
		// Select Case KeyAscii
		// Case 48 To 57, 8, 13, 46, 45
		// Case Else
		// KeyAscii = 0
		// End Select
		// Case lngColTax2
		// Select Case KeyAscii
		// Case 48 To 57, 8, 13, 46, 45
		// Case Else
		// KeyAscii = 0
		// End Select
		// Case lngColTax3
		// Select Case KeyAscii
		// Case 48 To 57, 8, 13, 46, 45
		// Case Else
		// KeyAscii = 0
		// End Select
		// Case lngColTax4
		// Select Case KeyAscii
		// Case 48 To 57, 8, 13, 46, 45
		// Case Else
		// KeyAscii = 0
		// End Select
		// Case lngColTax5
		// Select Case KeyAscii
		// Case 48 To 57, 8, 13, 46, 45
		// Case Else
		// KeyAscii = 0
		// End Select
		// Case lngColTax6
		// Select Case KeyAscii
		// Case 48 To 57, 8, 13, 46, 45
		// Case Else
		// KeyAscii = 0
		// End Select
		// End Select
		// End Sub
		private void vsAccounts_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsAccounts[e.ColumnIndex, e.RowIndex];

            if (vsAccounts.GetFlexRowIndex(e.RowIndex) > 0)
			{
				//ToolTip1.SetToolTip(vsAccounts, vsAccounts.TextMatrix(vsAccounts.MouseRow, lngDataEntryMessageCol));
				cell.ToolTipText =  vsAccounts.TextMatrix(vsAccounts.GetFlexRowIndex(e.RowIndex), lngDataEntryMessageCol);
			}
			else
			{
                //ToolTip1.SetToolTip(vsAccounts, "");
                cell.ToolTipText = "";
			}
		}

		private void vsAccounts_RowColChange(object sender, System.EventArgs e)
		{
			// If .Row = 0 Then
			// .Editable = flexEDNone
			// Exit Sub
			// End If
			// Select Case .Col
			// Case lngColAccount
			// .Editable = flexEDKbdMouse
			// Case lngColName     'allow the name to be edited
			// .Editable = flexEDKbdMouse
			// Case lngColTax1
			// If blnFee1 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngColTax2
			// If blnFee2 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngColTax3
			// If blnFee3 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngColTax4
			// If blnFee4 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngColTax5
			// If blnFee5 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngColTax6
			// If blnFee6 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngReferenceCol
			// If blnReference Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngControl1Col
			// If blnControl1 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngControl2Col
			// If blnControl2 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case lngControl3Col
			// If blnControl3 Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case Else
			// .Editable = flexEDKbdMouse
			// End Select
			// 
			// If .Editable = flexEDKbdMouse And .Col <> lngSelectCol Then
			// .EditCell
			// End If
			if (vsAccounts.Row > 0)
			{
				ShowDetail();
			}
			else
			{
				ClearDetail();
			}
			if (vsAccounts.Row == vsAccounts.Rows - 1 && vsAccounts.Col == vsAccounts.Cols - 1)
			{
				vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
		}

		private bool ValidateInformation()
		{
			bool ValidateInformation = false;
			int lngRow;
			Decimal curTotal;
			int counter;
			ValidateInformation = true;
			vsAccounts.Select(0, 0);
			// this will make the edittext be put into the grid
			for (lngRow = 1; lngRow <= vsAccounts.Rows - 1; lngRow++)
			{
				if (FCUtils.CBool(vsAccounts.TextMatrix(lngRow, lngSelectCol)) == true)
				{
					if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax1)) == 0 || Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColQuantity1)) == 0)
					{
						if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax2)) == 0 || Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColQuantity2)) == 0)
						{
							if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax3)) == 0 || Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColQuantity3)) == 0)
							{
								if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax4)) == 0 || Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColQuantity4)) == 0)
								{
									if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax5)) == 0 || Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColQuantity5)) == 0)
									{
										if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax6)) == 0 || Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColQuantity6)) == 0)
										{
											ValidateInformation = false;
											MessageBox.Show("All customers shown must have amounts to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
											if (vsAccounts.Visible && vsAccounts.Enabled)
											{
												vsAccounts.Focus();
												for (counter = 2; counter <= vsAccounts.Cols - 1; counter++)
												{
													if (vsAccounts.ColHidden(counter) != true)
													{
														// vsAccounts.Select lngRow, counter
														vsAccounts.Row = lngRow;
														break;
													}
												}
											}
											return ValidateInformation;
										}
									}
								}
							}
						}
					}
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngReferenceCol)) == "" && blnReference && blnReferenceRequired)
				{
					ValidateInformation = false;
					MessageBox.Show("You must fill in the " + vsAccounts.TextMatrix(0, lngReferenceCol) + " field to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						// vsAccounts.Select lngRow, lngReferenceCol
						vsAccounts.Row = lngRow;
					}
					return ValidateInformation;
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngControl1Col)) == "" && blnControl1 && blnControl1Required)
				{
					ValidateInformation = false;
					MessageBox.Show("You must fill in the " + vsAccounts.TextMatrix(0, lngControl1Col) + " field to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						// vsAccounts.Select lngRow, lngControl1Col
						vsAccounts.Row = lngRow;
					}
					return ValidateInformation;
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngControl2Col)) == "" && blnControl2 && blnControl2Required)
				{
					ValidateInformation = false;
					MessageBox.Show("You must fill in the " + vsAccounts.TextMatrix(0, lngControl2Col) + " field to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						// vsAccounts.Select lngRow, lngControl2Col
						vsAccounts.Row = lngRow;
					}
					return ValidateInformation;
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngControl3Col)) == "" && blnControl3 && blnControl3Required)
				{
					ValidateInformation = false;
					MessageBox.Show("You must fill in the " + vsAccounts.TextMatrix(0, lngControl3Col) + " field to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						// vsAccounts.Select lngRow, lngControl3Col
						vsAccounts.Row = lngRow;
					}
					return ValidateInformation;
				}
			}
			return ValidateInformation;
		}

		private bool SaveInformation()
		{
			bool SaveInformation = false;
			SaveInformation = true;
			if (vsAccounts.Row > 0)
			{
				vsAccounts.Focus();
				SaveDetails();
			}
			if (ValidateInformation())
			{
				if (LoadInformation())
				{
					MessageBox.Show("The save was successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					SaveInformation = false;
					MessageBox.Show("The save was not successful.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				SaveInformation = false;
			}
			return SaveInformation;
		}

		private bool LoadInformation()
		{
			bool LoadInformation = false;
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				LoadInformation = true;
				rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
				for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
				{
					if (FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngBillKeyCol)) == 0)
					{
						rsInfo.OpenRecordset("SELECT * FROM CustomerBills WHERE ID = 0");
						rsInfo.AddNew();
						rsInfo.Set_Fields("Type", intBillType);
						rsInfo.Set_Fields("CustomerID", vsAccounts.TextMatrix(counter, lngColAccount));
						rsInfo.Set_Fields("StartDate", null);
						rsInfo.Set_Fields("Prorate", false);
						rsInfo.Set_Fields("Comment", "");
					}
					else
					{
						rsInfo.OpenRecordset("SELECT * FROM CustomerBills WHERE ID = " + vsAccounts.TextMatrix(counter, lngBillKeyCol));
						rsInfo.Edit();
					}
					if (FCUtils.CBool(vsAccounts.TextMatrix(counter, lngSelectCol)) == true)
					{
						rsInfo.Set_Fields("Exclude", false);
					}
					else
					{
						rsInfo.Set_Fields("Exclude", true);
						goto UpdateRecord;
					}
					if (blnFee1)
					{
						if (vsAccounts.TextMatrix(counter, lngColTax1) == "")
						{
							vsAccounts.TextMatrix(counter, lngColTax1, "0.00");
						}
						if (rsDefaultInfo.Get_Fields_Double("DefaultAmount1") != FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColTax1)))
						{
							rsInfo.Set_Fields("Amount1", vsAccounts.TextMatrix(counter, lngColTax1));
						}
						else
						{
							rsInfo.Set_Fields("Amount1", -1);
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount1", -1);
					}
					if (blnFee2)
					{
						if (vsAccounts.TextMatrix(counter, lngColTax2) == "")
						{
							vsAccounts.TextMatrix(counter, lngColTax2, "0.00");
						}
						if (rsDefaultInfo.Get_Fields_Double("DefaultAmount2") != FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColTax2)))
						{
							rsInfo.Set_Fields("Amount2", vsAccounts.TextMatrix(counter, lngColTax2));
						}
						else
						{
							rsInfo.Set_Fields("Amount2", -1);
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount2", -1);
					}
					if (blnFee3)
					{
						if (vsAccounts.TextMatrix(counter, lngColTax3) == "")
						{
							vsAccounts.TextMatrix(counter, lngColTax3, "0.00");
						}
						if (rsDefaultInfo.Get_Fields_Double("DefaultAmount3") != FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColTax3)))
						{
							rsInfo.Set_Fields("Amount3", vsAccounts.TextMatrix(counter, lngColTax3));
						}
						else
						{
							rsInfo.Set_Fields("Amount3", -1);
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount3", -1);
					}
					if (blnFee4)
					{
						if (vsAccounts.TextMatrix(counter, lngColTax4) == "")
						{
							vsAccounts.TextMatrix(counter, lngColTax4, "0.00");
						}
						if (rsDefaultInfo.Get_Fields_Double("DefaultAmount4") != FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColTax4)))
						{
							rsInfo.Set_Fields("Amount4", vsAccounts.TextMatrix(counter, lngColTax4));
						}
						else
						{
							rsInfo.Set_Fields("Amount4", -1);
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount4", -1);
					}
					if (blnFee5)
					{
						if (vsAccounts.TextMatrix(counter, lngColTax5) == "")
						{
							vsAccounts.TextMatrix(counter, lngColTax5, "0.00");
						}
						if (rsDefaultInfo.Get_Fields_Double("DefaultAmount5") != FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColTax5)))
						{
							rsInfo.Set_Fields("Amount5", vsAccounts.TextMatrix(counter, lngColTax5));
						}
						else
						{
							rsInfo.Set_Fields("Amount5", -1);
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount5", -1);
					}
					if (blnFee6)
					{
						if (vsAccounts.TextMatrix(counter, lngColTax6) == "")
						{
							vsAccounts.TextMatrix(counter, lngColTax6, "0.00");
						}
						if (rsDefaultInfo.Get_Fields_Double("DefaultAmount6") != FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColTax6)))
						{
							rsInfo.Set_Fields("Amount6", vsAccounts.TextMatrix(counter, lngColTax6));
						}
						else
						{
							rsInfo.Set_Fields("Amount6", -1);
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount6", -1);
					}
					if (blnReference)
					{
						rsInfo.Set_Fields("Reference", Strings.Trim(vsAccounts.TextMatrix(counter, lngReferenceCol)));
					}
					if (blnControl1)
					{
						rsInfo.Set_Fields("Control1", Strings.Trim(vsAccounts.TextMatrix(counter, lngControl1Col)));
					}
					if (blnControl2)
					{
						rsInfo.Set_Fields("Control2", Strings.Trim(vsAccounts.TextMatrix(counter, lngControl2Col)));
					}
					if (blnControl3)
					{
						rsInfo.Set_Fields("Control3", Strings.Trim(vsAccounts.TextMatrix(counter, lngControl3Col)));
					}
					rsInfo.Set_Fields("Quantity1", Strings.Trim(vsAccounts.TextMatrix(counter, lngColQuantity1)));
					rsInfo.Set_Fields("Quantity2", Strings.Trim(vsAccounts.TextMatrix(counter, lngColQuantity2)));
					rsInfo.Set_Fields("Quantity3", Strings.Trim(vsAccounts.TextMatrix(counter, lngColQuantity3)));
					rsInfo.Set_Fields("Quantity4", Strings.Trim(vsAccounts.TextMatrix(counter, lngColQuantity4)));
					rsInfo.Set_Fields("Quantity5", Strings.Trim(vsAccounts.TextMatrix(counter, lngColQuantity5)));
					rsInfo.Set_Fields("Quantity6", Strings.Trim(vsAccounts.TextMatrix(counter, lngColQuantity6)));
					UpdateRecord:
					;
					rsInfo.Update(true);
					vsAccounts.TextMatrix(counter, lngBillKeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
				}
				return LoadInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				LoadInformation = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Load Back Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadInformation;
		}

		private void txtSearchName_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						SearchForAccounts();
						break;
					}
			}
			//end switch
		}

		private void SearchForAccounts()
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			string strSQL = "";
			FormatSearchGrid();
			if (cmbName.SelectedIndex == 0)
			{
				rsSearch.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsSearch.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.FullName >= '" + Strings.Trim(txtSearchName.Text) + "    ' AND p.FullName <= '" + Strings.Trim(txtSearchName.Text) + "zzzz' ORDER BY p.FullName");
			}
			else if (cmbName.SelectedIndex == 1)
			{
				rsSearch.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsSearch.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE c.CustomerID = " + FCConvert.ToString(Conversion.Val(Strings.Trim(txtSearchName.Text))) + " ORDER BY p.FullName");
			}
			else if (cmbName.SelectedIndex == 2)
			{
				rsSearch.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsSearch.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.Address1 >= '" + Strings.Trim(txtSearchName.Text) + "    ' AND p.Address1 <= '" + Strings.Trim(txtSearchName.Text) + "zzzz' ORDER BY p.FullName");
			}
			if (rsSearch.EndOfFile())
			{
				// no accounts
				MessageBox.Show("No accounts match the search criteria.", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				vsAccountSearch.Rows = 1;
				while (!rsSearch.EndOfFile())
				{
					vsAccountSearch.AddItem("\t" + rsSearch.Get_Fields_Int32("CustomerID") + "\t" + rsSearch.Get_Fields_String("FullName") + "\t" + rsSearch.Get_Fields_String("Address1"));
					rsSearch.MoveNext();
				}
			}
			vsAccountSearch.Visible = true;
			// resize the grid
			SetSearchGridHeight();
		}

		private void vsAccountSearch_DblClick(object sender, System.EventArgs e)
		{
			// selecting an account to use
			int lngRW;
			int lngAccount;
			lngRW = vsAccountSearch.MouseRow;
			lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(lngRW, 1))));
			ReturnFromSearch(lngAccount);
		}

		public void ReturnFromSearch(int lngAcct)
		{
			fraAccountSearch.Visible = false;
			vsAccounts.Visible = true;
			btnAddCustomer.Enabled = true;
            //FC:FINAL:BSE #2139 remove save button
			//btnFileSave.Enabled = true;
			btnProcessSave.Enabled = true;
			btnFileBillingEditReport.Enabled = true;
			if (lngAcct == 0)
			{
				if (vsAccounts.Rows > 1)
				{
					vsAccounts.Focus();
				}
			}
			else
			{
				LoadAccountInformation(lngAcct);
				vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpCustomFormat, vsAccounts.Rows - 2, lngColTax1, vsAccounts.Rows - 2, lngColTax6, "#,##0.0000");
				vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsAccounts.Rows - 2, lngColTax1, vsAccounts.Rows - 2, lngColTax6, FCGrid.AlignmentSettings.flexAlignRightCenter);
				SetGridHeight();
				ShowDetail();
			}
		}

		private void vsAccountSearch_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int lngAccount = 0;
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				// return key
				keyAscii = 0;
				if (vsAccountSearch.Row > 0)
				{
					// selecting an account to use
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(vsAccountSearch.Row, 1))));
					ReturnFromSearch(lngAccount);
				}
			}
		}

		private void LoadAddedAccountInformation(int lngAcct)
		{
			// this function will accept an account number and will fill the grid with the name from that account
			clsDRWrapper rsBillCheck = new clsDRWrapper();
			clsDRWrapper rsPayments = new clsDRWrapper();
			string strDesc = "";
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
			vsAccounts.AddItem("");
			rsAR.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsAR.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE CustomerID = " + FCConvert.ToString(lngAcct));
			if (rsAR.EndOfFile() != true && rsAR.BeginningOfFile() != true)
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngSelectCol, FCConvert.ToString(true));
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColAccount, FCConvert.ToString(rsAR.Get_Fields_Int32("CustomerID")));
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColName, FCConvert.ToString(rsAR.Get_Fields_String("FullName")));
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngBillKeyCol, FCConvert.ToString(0));
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngDataEntryMessageCol, "");
				if (blnFee1)
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax1, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount1"), "##0.0000"));
				}
				else
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax1, "");
				}
				if (blnFee2)
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax2, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount2"), "##0.0000"));
				}
				else
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax2, "");
				}
				if (blnFee3)
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax3, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount3"), "##0.0000"));
				}
				else
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax3, "");
				}
				if (blnFee4)
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax4, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount4"), "##0.0000"));
				}
				else
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax4, "");
				}
				if (blnFee5)
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax5, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount5"), "##0.0000"));
				}
				else
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax5, "");
				}
				if (blnFee6)
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax6, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount6"), "##0.0000"));
				}
				else
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax6, "");
				}
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngReferenceCol, "");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngControl1Col, "");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngControl2Col, "");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngControl3Col, "");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity1, "1.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity2, "1.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity3, "1.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity4, "1.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity5, "1.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColQuantity6, "1.00");
				SetGridHeight();
				if (blnReference && blnReferenceRequired)
				{
					txtTitle[0].BackColor = ColorTranslator.FromOle(0xC0FFFF);
				}
				if (blnControl1 && blnControl1Required)
				{
					txtTitle[1].BackColor = ColorTranslator.FromOle(0xC0FFFF);
				}
				if (blnControl2 && blnControl1Required)
				{
					txtTitle[2].BackColor = ColorTranslator.FromOle(0xC0FFFF);
				}
				if (blnControl3 && blnControl1Required)
				{
					txtTitle[3].BackColor = ColorTranslator.FromOle(0xC0FFFF);
				}
			}
			else
			{
				// no matching account number
				MessageBox.Show("There is no Accounts Receivable account with that number.  Please try again.", "Incorrect Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
				vsAccounts.Select(vsAccounts.Row, lngColAccount);
			}
		}

		private void FormatSearchGrid()
		{
			vsAccountSearch.Cols = 4;
			vsAccountSearch.ColWidth(0, 0);
			vsAccountSearch.ColWidth(1, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.12));
			// account number
			vsAccountSearch.ColWidth(2, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.5));
			// name
			vsAccountSearch.ColWidth(3, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.36));
			// maplot
			vsAccountSearch.ExtendLastCol = true;
			vsAccountSearch.TextMatrix(0, 1, "Account");
			vsAccountSearch.TextMatrix(0, 2, "Name");
			vsAccountSearch.TextMatrix(0, 3, "Location");
			vsAccountSearch.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSort;
		}

		private void SetSearchGridHeight()
		{
			//FC:FINAL:SBE - grid is anchored
			//if ((vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70 > fraAccountSearch.Height * 0.7)
			//{
			//    // too many rows to be shown
			//    vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//    vsAccountSearch.Height = FCConvert.ToInt32(fraAccountSearch.Height * 0.7);
			//}
			//else
			//{
			//    vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//    vsAccountSearch.Height = (vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70;
			//}
		}

		private void ClearDetail()
		{
			txtTitle[0].Text = "";
			txtTitle[1].Text = "";
			txtTitle[2].Text = "";
			txtTitle[3].Text = "";
			vsFees.TextMatrix(1, 2, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(2, 2, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(3, 2, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(4, 2, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(5, 2, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(6, 2, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(1, 3, Strings.Format(0, "##0.0000"));
			vsFees.TextMatrix(2, 3, Strings.Format(0, "##0.0000"));
			vsFees.TextMatrix(3, 3, Strings.Format(0, "##0.0000"));
			vsFees.TextMatrix(4, 3, Strings.Format(0, "##0.0000"));
			vsFees.TextMatrix(5, 3, Strings.Format(0, "##0.0000"));
			vsFees.TextMatrix(6, 3, Strings.Format(0, "##0.0000"));
			vsFees.TextMatrix(1, 4, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(2, 4, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(3, 4, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(4, 4, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(5, 4, Strings.Format(0, "##0.00"));
			vsFees.TextMatrix(6, 4, Strings.Format(0, "##0.00"));
			lblTotal.Text = Strings.Format(0, "##0.0000");
			Frame2.Enabled = false;
			vsFees.Enabled = false;
		}

		private void ShowDetail()
		{
			txtTitle[0].Text = vsAccounts.TextMatrix(vsAccounts.Row, lngReferenceCol);
			txtTitle[1].Text = vsAccounts.TextMatrix(vsAccounts.Row, lngControl1Col);
			txtTitle[2].Text = vsAccounts.TextMatrix(vsAccounts.Row, lngControl2Col);
			txtTitle[3].Text = vsAccounts.TextMatrix(vsAccounts.Row, lngControl3Col);
			vsFees.TextMatrix(1, 2, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax1), "##0.00"));
			vsFees.TextMatrix(2, 2, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax2), "##0.00"));
			vsFees.TextMatrix(3, 2, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax3), "##0.00"));
			vsFees.TextMatrix(4, 2, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax4), "##0.00"));
			vsFees.TextMatrix(5, 2, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax5), "##0.00"));
			vsFees.TextMatrix(6, 2, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax6), "##0.00"));
			vsFees.TextMatrix(1, 3, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity1), "##0.0000"));
			vsFees.TextMatrix(2, 3, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity2), "##0.0000"));
			vsFees.TextMatrix(3, 3, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity3), "##0.0000"));
			vsFees.TextMatrix(4, 3, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity4), "##0.0000"));
			vsFees.TextMatrix(5, 3, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity5), "##0.0000"));
			vsFees.TextMatrix(6, 3, Strings.Format(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity6), "##0.0000"));
			if (vsFees.RowHidden(1) == false)
			{
				vsFees.TextMatrix(1, 4, Strings.Format(FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax1)) * FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity1)), 4)), "##0.0000"));
			}
			if (vsFees.RowHidden(2) == false)
			{
				vsFees.TextMatrix(2, 4, Strings.Format(FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax2)) * FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity2)), 4)), "##0.0000"));
			}
			if (vsFees.RowHidden(3) == false)
			{
				vsFees.TextMatrix(3, 4, Strings.Format(FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax3)) * FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity3)), 2)), "##0.00"));
			}
			if (vsFees.RowHidden(4) == false)
			{
				vsFees.TextMatrix(4, 4, Strings.Format(FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax4)) * FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity4)), 2)), "##0.00"));
			}
			if (vsFees.RowHidden(5) == false)
			{
				vsFees.TextMatrix(5, 4, Strings.Format(FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax5)) * FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity5)), 2)), "##0.00"));
			}
			if (vsFees.RowHidden(6) == false)
			{
				vsFees.TextMatrix(6, 4, Strings.Format(FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColTax6)) * FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity6)), 2)), "##0.00"));
			}
			lblTotal.Text = Strings.Format(CalculateTotal(), "##0.00");
			Frame2.Enabled = true;
			vsFees.Enabled = true;
		}

		private void SaveDetails()
		{
			vsAccounts.TextMatrix(vsAccounts.Row, lngReferenceCol, txtTitle[0].Text);
			vsAccounts.TextMatrix(vsAccounts.Row, lngControl1Col, txtTitle[1].Text);
			vsAccounts.TextMatrix(vsAccounts.Row, lngControl2Col, txtTitle[2].Text);
			vsAccounts.TextMatrix(vsAccounts.Row, lngControl3Col, txtTitle[3].Text);
			if (Information.IsNumeric(vsFees.TextMatrix(1, 2)))
			{
				vsAccounts.TextMatrix(vsAccounts.Row, lngColTax1, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(1, 2).ToDecimalValue(), 4)));
			}
			if (Information.IsNumeric(vsFees.TextMatrix(2, 2)))
			{
				vsAccounts.TextMatrix(vsAccounts.Row, lngColTax2, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(2, 2).ToDecimalValue(), 4)));
			}
			if (Information.IsNumeric(vsFees.TextMatrix(3, 2)))
			{
				vsAccounts.TextMatrix(vsAccounts.Row, lngColTax3, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(3, 2).ToDecimalValue(), 4)));
			}
			if (Information.IsNumeric(vsFees.TextMatrix(4, 2)))
			{
				vsAccounts.TextMatrix(vsAccounts.Row, lngColTax4, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(4, 2).ToDecimalValue(), 4)));
			}
			if (Information.IsNumeric(vsFees.TextMatrix(5, 2)))
			{
				vsAccounts.TextMatrix(vsAccounts.Row, lngColTax5, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(5, 2).ToDecimalValue(), 4)));
			}
			if (Information.IsNumeric(vsFees.TextMatrix(6, 2)))
			{
				vsAccounts.TextMatrix(vsAccounts.Row, lngColTax6, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(6, 2).ToDecimalValue(), 4)));
			}
			vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity1, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(1, 3).ToDecimalValue(), 4)));
			vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity2, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(2, 3).ToDecimalValue(), 4)));
			vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity3, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(3, 3).ToDecimalValue(), 4)));
			vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity4, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(4, 3).ToDecimalValue(), 4)));
			vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity5, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(5, 3).ToDecimalValue(), 4)));
			vsAccounts.TextMatrix(vsAccounts.Row, lngColQuantity6, FCConvert.ToString(FCUtils.Round(vsFees.TextMatrix(6, 3).ToDecimalValue(), 4)));
		}

		public void ProcessType()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsType = new clsDRWrapper();
				int lngTypeNumber;
				int intCT = 0;
				int I;

				rsType.DefaultDB = "TWAR0000.vb1";
				if (!modGlobal.ValidateBillType(ref intBillType))
				{
					MessageBox.Show("There are problems with 1 or more of the accounts used in this bill type.  You must correct these problems before you may create any bills of this type.", "Invalid Accounts in Bill Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
				if (rsType.BeginningOfFile() != true && rsType.EndOfFile() != true)
				{
					bool executeACCT2 = false;
					if (FCConvert.ToString(rsType.Get_Fields_String("Title1")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account1")) != "")
					{
						// remove validation
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account1")))
							{
								executeACCT2 = true;
								goto ACCT2;
							}
						}
						vsFees.TextMatrix(1, 1, FCConvert.ToString(rsType.Get_Fields_String("Title1")));
						vsFees.TextMatrix(1, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount1"), "##0.00"));
					}
					else
					{
						executeACCT2 = true;
						goto ACCT2;
					}
					ACCT2:
					;
					if (executeACCT2)
					{
						vsFees.TextMatrix(1, 1, "");
						vsFees.TextMatrix(1, 2, FCConvert.ToString(0));
					}
					bool executeACCT3 = false;
					if (FCConvert.ToString(rsType.Get_Fields_String("Title2")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account2")) != "")
					{
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account2")))
							{
								executeACCT3 = true;
								goto ACCT3;
							}
						}
						// Print #15, "16 - " & Now
						vsFees.TextMatrix(2, 1, FCConvert.ToString(rsType.Get_Fields_String("Title2")));
						vsFees.TextMatrix(2, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount2"), "##0.00"));
					}
					else
					{
						executeACCT3 = true;
						goto ACCT3;
					}
					ACCT3:
					;
					if (executeACCT3)
					{
						// Print #15, "17 - " & Now
						vsFees.TextMatrix(2, 1, "");
						vsFees.TextMatrix(2, 2, FCConvert.ToString(0));
					}
					bool executeACCT4 = false;
					if (FCConvert.ToString(rsType.Get_Fields_String("Title3")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account3")) != "")
					{
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account3")))
							{
								executeACCT4 = true;
								goto ACCT4;
							}
						}
						// Print #15, "18 - " & Now
						vsFees.TextMatrix(3, 1, FCConvert.ToString(rsType.Get_Fields_String("Title3")));
						vsFees.TextMatrix(3, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount3"), "##0.00"));
					}
					else
					{
						executeACCT4 = true;
						goto ACCT4;
					}
					// Print #15, "19 - " & Now
					ACCT4:
					;
					if (executeACCT4)
					{
						vsFees.TextMatrix(3, 1, "");
						vsFees.TextMatrix(3, 2, FCConvert.ToString(0));
					}
					bool executeACCT5 = false;
					if (FCConvert.ToString(rsType.Get_Fields_String("Title4")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account4")) != "")
					{
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account4")))
							{
								executeACCT5 = true;
								goto ACCT5;
							}
						}
						// Print #15, "20 - " & Now
						vsFees.TextMatrix(4, 1, FCConvert.ToString(rsType.Get_Fields_String("Title4")));
						vsFees.TextMatrix(4, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount4"), "##0.00"));
					}
					else
					{
						executeACCT5 = true;
						goto ACCT5;
					}
					// Print #15, "21 - " & Now
					ACCT5:
					;
					if (executeACCT5)
					{
						vsFees.TextMatrix(4, 1, "");
						vsFees.TextMatrix(4, 2, FCConvert.ToString(0));
					}
					bool executeACCT6 = false;
					if (FCConvert.ToString(rsType.Get_Fields_String("Title5")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account5")) != "")
					{
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account5")))
							{
								executeACCT6 = true;
								goto ACCT6;
							}
						}
						// Print #15, "22 - " & Now
						vsFees.TextMatrix(5, 1, FCConvert.ToString(rsType.Get_Fields_String("Title5")));
						vsFees.TextMatrix(5, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount5"), "##0.00"));
					}
					else
					{
						executeACCT6 = true;
						goto ACCT6;
					}
					ACCT6:
					;
					if (executeACCT6)
					{
						// Print #15, "23 - " & Now
						vsFees.TextMatrix(5, 1, "");
						vsFees.TextMatrix(5, 2, FCConvert.ToString(0));
					}
					bool executeEXITACCT = false;
					if (FCConvert.ToString(rsType.Get_Fields_String("Title6")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account6")) != "")
					{
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account6")))
							{
								executeEXITACCT = true;
								goto EXITACCT;
							}
						}
						// Print #15, "24 - " & Now
						vsFees.TextMatrix(6, 1, FCConvert.ToString(rsType.Get_Fields_String("Title6")));
						vsFees.TextMatrix(6, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount6"), "##0.00"));
					}
					else
					{
						executeEXITACCT = true;
						goto EXITACCT;
					}
					// Print #15, "25 - " & Now
					EXITACCT:
					;
					if (executeEXITACCT)
					{
						vsFees.TextMatrix(6, 1, "");
						vsFees.TextMatrix(6, 2, FCConvert.ToString(0));
					}
					// Print #15, "26 - " & Now
					// check to see if any of the accounts need to be updated by the Year or need to be changed by the CurrentYear function
					// and at the same time set the heights of the rows
					// vsFees.AutoSize 0, vsFees.Cols - 1
					intCT = 1;
					for (I = 1; I <= 6; I++)
					{
						if (Strings.Trim(vsFees.TextMatrix(I, 1)) == "" && Conversion.Val(vsFees.TextMatrix(I, 2)) == 0)
						{
							// .RowHeight(i) = 0
							vsFees.RowHidden(I, true);
						}
						else
						{
							intCT += 1;
							vsFees.RowHidden(I, false);
							// .RowHeight(i) = .RowHeight(0)
						}
					}
					//vsFees.Height = (intCT * vsFees.RowHeight(0)) + 50;
					vsFees.Height = (intCT - 1) * 40 + 32;
					lblTotal.Top = vsFees.Top + vsFees.Height + 10;
					lblTotalLabel.Top = vsFees.Top + vsFees.Height + 10;
					// Print #15, "27 - " & Now
					// Print #15, "30 - " & Now
					// fill in the labels names
					lblTitle[0].Text = rsType.Get_Fields_String("Reference") + ":";
					lblTitle[1].Text = rsType.Get_Fields_String("Control1") + ":";
					lblTitle[2].Text = rsType.Get_Fields_String("Control2") + ":";
					lblTitle[3].Text = rsType.Get_Fields_String("Control3") + ":";
					if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("ReferenceMandatory")) && !FCConvert.ToBoolean(rsType.Get_Fields_Boolean("ReferenceChanges")))
					{
						txtTitle[0].Tag = "True";
						txtTitle[0].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
					}
					else
					{
						txtTitle[0].Tag = "False";
						txtTitle[0].BackColor = Color.White;
					}
					if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control1Mandatory")) && !FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control1Changes")))
					{
						txtTitle[1].Tag = "True";
						txtTitle[1].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
					}
					else
					{
						txtTitle[1].Tag = "False";
						txtTitle[1].BackColor = Color.White;
					}
					if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control2Mandatory")) && !FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control2Changes")))
					{
						txtTitle[2].Tag = "True";
						txtTitle[2].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
					}
					else
					{
						txtTitle[2].Tag = "False";
						txtTitle[2].BackColor = Color.White;
					}
					if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control3Mandatory")) && !FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control3Changes")))
					{
						txtTitle[3].Tag = "True";
						txtTitle[3].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
					}
					else
					{
						txtTitle[3].Tag = "False";
						txtTitle[3].BackColor = Color.White;
					}
					txtTitle[0].Text = "";
					txtTitle[1].Text = "";
					txtTitle[2].Text = "";
					txtTitle[3].Text = "";
					// Print #15, "31 - " & Now
					for (I = 0; I <= 3; I++)
					{
						if (Strings.Trim(lblTitle[FCConvert.ToInt16(I)].Text) == ":")
						{
							lblTitle[FCConvert.ToInt16(I)].Visible = false;
							txtTitle[FCConvert.ToInt16(I)].Visible = false;
						}
						else
						{
							lblTitle[FCConvert.ToInt16(I)].Visible = true;
							txtTitle[FCConvert.ToInt16(I)].Visible = true;
						}
					}
				}
				else
				{
					MessageBox.Show("Could not find type #" + FCConvert.ToString(intBillType) + ".", "Error Loading Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// Close #15
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Processing Bill Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatFeesGrid()
		{
			// sets all of the widths, datatypes and titles for the grid
			int Width = 0;
			vsFees.Cols = 5;
			vsFees.EditMaxLength = 12;
			Width = vsFees.WidthOriginal;
			vsFees.ColWidth(0, 0);
			// Category Number
			vsFees.ColWidth(1, FCConvert.ToInt32(Width * 0.2));
			// Title
			vsFees.ColWidth(2, FCConvert.ToInt32(Width * 0.2));
			// Amount
			vsFees.ColWidth(3, FCConvert.ToInt32(Width * 0.2));
			// Amount
			vsFees.ColWidth(4, FCConvert.ToInt32(Width * 0.2));
			// Amount
			vsFees.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsFees.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsFees.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// sets the titles for the flexgrid
			vsFees.TextMatrix(0, 0, "Fee");
			vsFees.TextMatrix(0, 1, "Title");
			vsFees.TextMatrix(0, 2, "Unit Cost");
			vsFees.TextMatrix(0, 3, "Quantity");
			vsFees.TextMatrix(0, 4, "Total");
			vsFees.ColDataType(2, FCGrid.DataTypeSettings.flexDTCurrency);
			vsFees.ColDataType(3, FCGrid.DataTypeSettings.flexDTDouble);
			vsFees.ColDataType(4, FCGrid.DataTypeSettings.flexDTCurrency);
			vsFees.ColFormat(2, "##0.0000");
			vsFees.ColFormat(3, "##0.0000");
			vsFees.ColFormat(4, "##0.00");

		}

		private void vsFees_RowColChange(object sender, System.EventArgs e)
		{
			if (vsFees.Col == 2 || vsFees.Col == 3)
			{
				vsFees.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsFees.EditCell();
			}
			else
			{
				vsFees.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsFees_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsFees.GetFlexRowIndex(e.RowIndex);
			int col = vsFees.GetFlexColIndex(e.ColumnIndex);
			if (col == 2)
			{
				vsFees.TextMatrix(row, 4, Strings.Format((FCUtils.Round(Conversion.Val(vsFees.TextMatrix(row, 3)) * FCUtils.Round(vsFees.EditText, 2), 2)), "#,##0.00"));
			}
			else if (col == 3)
			{
				vsFees.TextMatrix(row, 4, Strings.Format((FCUtils.Round(Conversion.Val(vsFees.TextMatrix(row, 2)) * FCUtils.Round(vsFees.EditText, 4), 2)), "#,##0.00"));
			}
			lblTotal.Text = Strings.Format(CalculateTotal(), "#,##0.00");
		}

		private Decimal CalculateTotal()
		{
			Decimal CalculateTotal = 0;
			int counter;
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotal;
			curTotal = 0;
			for (counter = 1; counter <= 6; counter++)
			{
				if (vsFees.RowHidden(counter) == false)
				{
					curTotal += FCConvert.ToDecimal(vsFees.TextMatrix(counter, 4));
				}
			}
			CalculateTotal = curTotal;
			return CalculateTotal;
		}

        private void btnImportBills_Click(object sender, EventArgs e)
        {
            ImportBills();   
        }

        private void ImportBills()
        {
            //frmBusy bWait = new frmBusy();
            try
            {   // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                string strFileName;
                strFileName = BrowseForImportFile();
                
                //bWait.StartBusy();
                cARSimpleImport arImp = new cARSimpleImport();
				//bWait.Show(FCForm.FormShowEnum.Modeless, this);
				this.ShowWait();
				FCUtils.StartTask(this, () =>
				{
					this.UpdateWait("Loading Data ...");
					arImp.ImportBillItems(strFileName, intBillType);
					//bWait.StopBusy();
					//FCUtils.Unload(bWait);
					/*- bWait = null; */
					if (!FCConvert.ToBoolean(arImp.HadError))
					{
						MessageBox.Show("File imported", "Imported", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						if (arImp.LastErrorNumber != 9998)
						{
							MessageBox.Show("Error " + FCConvert.ToString(arImp.LastErrorNumber) + " " + arImp.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
						string strMessage = "";
						strMessage = arImp.GetBadAccountsMessage();
						if (strMessage != "")
						{
							MessageBox.Show(strMessage, "Bad Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
					}
					vsAccounts.Rows = 1;
					LoadAccountInformation();
					SetGridHeight();
					this.EndWait();
				});
				return;
            }
            catch (Exception ex)
            {   
				// ErrorHandler:
				this.EndWait();
				//bWait.StopBusy();
				//FCUtils.Unload(bWait);
				/*- bWait = null; */
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private string BrowseForImportFile()
        {
            fecherFoundation.FCCommonDialog fileDialog = new FCCommonDialog();
            fileDialog.DialogTitle = "Choose file to import";
            fileDialog.CancelError = true;
            try
            {
                fileDialog.ShowOpen();
            }
            catch
            {
            }
            if (Information.Err().Number != 0)
            {
                Information.Err().Clear();
                return "";
            }
            if (!String.IsNullOrWhiteSpace(fileDialog.FileName))
            {
                return fileDialog.FileName;
            }
            return "";
        }
    }
}
