﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomerListSetup.
	/// </summary>
	partial class frmCustomerListSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSpecific;
		public fecherFoundation.FCComboBox cmbNumber;
		public fecherFoundation.FCLabel lblNumber;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCComboBox cmbMessageNo;
		public fecherFoundation.FCComboBox cmbAltAddressYes;
		public fecherFoundation.FCComboBox cmbStatusAll;
		public fecherFoundation.FCFrame fraClassCodes;
		public fecherFoundation.FCFrame fraSpecificBillTypes;
		public fecherFoundation.FCGrid vsBillTypes;
		public fecherFoundation.FCFrame fraListSelection;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCComboBox cboEnd;
		public fecherFoundation.FCComboBox cboStart;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCCheckBox chkBillMessage;
		public fecherFoundation.FCCheckBox chkEmail;
		public fecherFoundation.FCCheckBox Check1;
		public fecherFoundation.FCCheckBox chkCustomerMessage;
		public fecherFoundation.FCCheckBox chkDataEntryMessage;
		public fecherFoundation.FCFrame fraDataEntryMessage;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCCheckBox chkAddress;
		public fecherFoundation.FCCheckBox chkAlternateAddress;
		public fecherFoundation.FCFrame fraAlternateAddress;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraStatus;
		public fecherFoundation.FCCheckBox chkDeleted;
		public fecherFoundation.FCCheckBox chkActive;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbSpecific = new fecherFoundation.FCComboBox();
			this.cmbNumber = new fecherFoundation.FCComboBox();
			this.lblNumber = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.cmbMessageNo = new fecherFoundation.FCComboBox();
			this.cmbAltAddressYes = new fecherFoundation.FCComboBox();
			this.cmbStatusAll = new fecherFoundation.FCComboBox();
			this.fraClassCodes = new fecherFoundation.FCFrame();
			this.fraSpecificBillTypes = new fecherFoundation.FCFrame();
			this.vsBillTypes = new fecherFoundation.FCGrid();
			this.fraListSelection = new fecherFoundation.FCFrame();
			this.fraRange = new fecherFoundation.FCFrame();
			this.cboEnd = new fecherFoundation.FCComboBox();
			this.cboStart = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraInformation = new fecherFoundation.FCFrame();
			this.chkBillMessage = new fecherFoundation.FCCheckBox();
			this.chkEmail = new fecherFoundation.FCCheckBox();
			this.Check1 = new fecherFoundation.FCCheckBox();
			this.chkCustomerMessage = new fecherFoundation.FCCheckBox();
			this.chkDataEntryMessage = new fecherFoundation.FCCheckBox();
			this.fraDataEntryMessage = new fecherFoundation.FCFrame();
			this.Label5 = new fecherFoundation.FCLabel();
			this.chkAddress = new fecherFoundation.FCCheckBox();
			this.chkAlternateAddress = new fecherFoundation.FCCheckBox();
			this.fraAlternateAddress = new fecherFoundation.FCFrame();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraStatus = new fecherFoundation.FCFrame();
			this.chkDeleted = new fecherFoundation.FCCheckBox();
			this.chkActive = new fecherFoundation.FCCheckBox();
			this.btnFilePreview = new fecherFoundation.FCButton();
			this.btnFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraClassCodes)).BeginInit();
			this.fraClassCodes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificBillTypes)).BeginInit();
			this.fraSpecificBillTypes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBillTypes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraListSelection)).BeginInit();
			this.fraListSelection.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
			this.fraInformation.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBillMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCustomerMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDataEntryMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDataEntryMessage)).BeginInit();
			this.fraDataEntryMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAlternateAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAlternateAddress)).BeginInit();
			this.fraAlternateAddress.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraStatus)).BeginInit();
			this.fraStatus.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFilePreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 532);
			this.BottomPanel.Size = new System.Drawing.Size(968, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraClassCodes);
			this.ClientArea.Controls.Add(this.cmbNumber);
			this.ClientArea.Controls.Add(this.lblNumber);
			this.ClientArea.Controls.Add(this.fraListSelection);
			this.ClientArea.Controls.Add(this.fraInformation);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(968, 472);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(968, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFilePrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(153, 30);
			this.HeaderText.Text = "Print Listings";
			// 
			// cmbSpecific
			// 
			this.cmbSpecific.AutoSize = false;
			this.cmbSpecific.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSpecific.FormattingEnabled = true;
			this.cmbSpecific.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbSpecific.Location = new System.Drawing.Point(20, 30);
			this.cmbSpecific.Name = "cmbSpecific";
			this.cmbSpecific.Size = new System.Drawing.Size(360, 40);
			this.cmbSpecific.TabIndex = 32;
			this.cmbSpecific.SelectedIndexChanged += new System.EventHandler(this.optSpecific_CheckedChanged);
			// 
			// cmbNumber
			// 
			this.cmbNumber.AutoSize = false;
			this.cmbNumber.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbNumber.FormattingEnabled = true;
			this.cmbNumber.Items.AddRange(new object[] {
				"Name",
				"Number"
			});
			this.cmbNumber.Location = new System.Drawing.Point(168, 30);
			this.cmbNumber.Name = "cmbNumber";
			this.cmbNumber.Size = new System.Drawing.Size(312, 40);
			this.cmbNumber.TabIndex = 29;
			// 
			// lblNumber
			// 
			this.lblNumber.AutoSize = true;
			this.lblNumber.Location = new System.Drawing.Point(30, 44);
			this.lblNumber.Name = "lblNumber";
			this.lblNumber.Size = new System.Drawing.Size(53, 15);
			this.lblNumber.TabIndex = 30;
			this.lblNumber.Text = "LIST BY";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Selected Range"
			});
			this.cmbRange.Location = new System.Drawing.Point(20, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(430, 40);
			this.cmbRange.TabIndex = 15;
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// cmbMessageNo
			// 
			this.cmbMessageNo.AutoSize = false;
			this.cmbMessageNo.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbMessageNo.FormattingEnabled = true;
			this.cmbMessageNo.Items.AddRange(new object[] {
				"Yes",
				"No"
			});
			this.cmbMessageNo.Location = new System.Drawing.Point(355, 20);
			this.cmbMessageNo.Name = "cmbMessageNo";
			this.cmbMessageNo.Size = new System.Drawing.Size(220, 40);
			this.cmbMessageNo.TabIndex = 27;
			// 
			// cmbAltAddressYes
			// 
			this.cmbAltAddressYes.AutoSize = false;
			this.cmbAltAddressYes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAltAddressYes.FormattingEnabled = true;
			this.cmbAltAddressYes.Items.AddRange(new object[] {
				"Yes",
				"No"
			});
			this.cmbAltAddressYes.Location = new System.Drawing.Point(355, 20);
			this.cmbAltAddressYes.Name = "cmbAltAddressYes";
			this.cmbAltAddressYes.Size = new System.Drawing.Size(220, 40);
			this.cmbAltAddressYes.TabIndex = 11;
			// 
			// cmbStatusAll
			// 
			this.cmbStatusAll.AutoSize = false;
			this.cmbStatusAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbStatusAll.FormattingEnabled = true;
			this.cmbStatusAll.Items.AddRange(new object[] {
				"All",
				"Selected Status"
			});
			this.cmbStatusAll.Location = new System.Drawing.Point(20, 30);
			this.cmbStatusAll.Name = "cmbStatusAll";
			this.cmbStatusAll.Size = new System.Drawing.Size(430, 40);
			this.cmbStatusAll.TabIndex = 4;
			this.cmbStatusAll.SelectedIndexChanged += new System.EventHandler(this.optStatusAll_CheckedChanged);
			// 
			// fraClassCodes
			// 
			this.fraClassCodes.Controls.Add(this.fraSpecificBillTypes);
			this.fraClassCodes.Controls.Add(this.cmbSpecific);
			this.fraClassCodes.Location = new System.Drawing.Point(520, 90);
			this.fraClassCodes.Name = "fraClassCodes";
			this.fraClassCodes.Size = new System.Drawing.Size(450, 412);
			this.fraClassCodes.TabIndex = 28;
			this.fraClassCodes.Text = "Bill Types To Print";
			// 
			// fraSpecificBillTypes
			// 
			this.fraSpecificBillTypes.AppearanceKey = "groupBoxNoBorders";
			this.fraSpecificBillTypes.Controls.Add(this.vsBillTypes);
			this.fraSpecificBillTypes.Enabled = false;
			this.fraSpecificBillTypes.Location = new System.Drawing.Point(0, 70);
			this.fraSpecificBillTypes.Name = "fraSpecificBillTypes";
			this.fraSpecificBillTypes.Size = new System.Drawing.Size(450, 342);
			this.fraSpecificBillTypes.TabIndex = 31;
			// 
			// vsBillTypes
			// 
			this.vsBillTypes.AllowSelection = false;
			this.vsBillTypes.AllowUserToResizeColumns = false;
			this.vsBillTypes.AllowUserToResizeRows = false;
			this.vsBillTypes.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBillTypes.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBillTypes.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBillTypes.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBillTypes.BackColorSel = System.Drawing.Color.Empty;
			this.vsBillTypes.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBillTypes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsBillTypes.ColumnHeadersHeight = 30;
			this.vsBillTypes.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBillTypes.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsBillTypes.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBillTypes.FixedCols = 0;
			this.vsBillTypes.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBillTypes.FrozenCols = 0;
			this.vsBillTypes.GridColor = System.Drawing.Color.Empty;
			this.vsBillTypes.Location = new System.Drawing.Point(20, 20);
			this.vsBillTypes.Name = "vsBillTypes";
			this.vsBillTypes.ReadOnly = true;
			this.vsBillTypes.RowHeadersVisible = false;
			this.vsBillTypes.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBillTypes.RowHeightMin = 0;
			this.vsBillTypes.Rows = 1;
			this.vsBillTypes.ShowColumnVisibilityMenu = false;
			this.vsBillTypes.Size = new System.Drawing.Size(410, 302);
			this.vsBillTypes.StandardTab = true;
			this.vsBillTypes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBillTypes.TabIndex = 32;
			this.vsBillTypes.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBillTypes_KeyDownEvent);
			this.vsBillTypes.Click += new System.EventHandler(this.vsBillTypes_ClickEvent);
			// 
			// fraListSelection
			// 
			this.fraListSelection.Controls.Add(this.fraRange);
			this.fraListSelection.Controls.Add(this.cmbRange);
			this.fraListSelection.Location = new System.Drawing.Point(30, 292);
			this.fraListSelection.Name = "fraListSelection";
			this.fraListSelection.Size = new System.Drawing.Size(470, 210);
			this.fraListSelection.TabIndex = 11;
			this.fraListSelection.Text = "List Selection";
			// 
			// fraRange
			// 
			this.fraRange.AppearanceKey = "groupBoxNoBorders";
			this.fraRange.Controls.Add(this.cboEnd);
			this.fraRange.Controls.Add(this.cboStart);
			this.fraRange.Controls.Add(this.Label1);
			this.fraRange.Controls.Add(this.Label2);
			this.fraRange.Enabled = false;
			this.fraRange.Location = new System.Drawing.Point(0, 90);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(450, 100);
			this.fraRange.TabIndex = 14;
			// 
			// cboEnd
			// 
			this.cboEnd.AutoSize = false;
			this.cboEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cboEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEnd.FormattingEnabled = true;
			this.cboEnd.Location = new System.Drawing.Point(98, 60);
			this.cboEnd.Name = "cboEnd";
			this.cboEnd.Size = new System.Drawing.Size(352, 40);
			this.cboEnd.TabIndex = 37;
			// 
			// cboStart
			// 
			this.cboStart.AutoSize = false;
			this.cboStart.BackColor = System.Drawing.SystemColors.Window;
			this.cboStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStart.FormattingEnabled = true;
			this.cboStart.Location = new System.Drawing.Point(98, 0);
			this.cboStart.Name = "cboStart";
			this.cboStart.Size = new System.Drawing.Size(352, 40);
			this.cboStart.TabIndex = 36;
			// 
			// Label1
			// 
			this.Label1.Enabled = false;
			this.Label1.Location = new System.Drawing.Point(20, 14);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(38, 16);
			this.Label1.TabIndex = 16;
			this.Label1.Text = "START";
			// 
			// Label2
			// 
			this.Label2.Enabled = false;
			this.Label2.Location = new System.Drawing.Point(20, 74);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(28, 16);
			this.Label2.TabIndex = 15;
			this.Label2.Text = "END";
			// 
			// fraInformation
			// 
			this.fraInformation.Controls.Add(this.chkBillMessage);
			this.fraInformation.Controls.Add(this.chkEmail);
			this.fraInformation.Controls.Add(this.Check1);
			this.fraInformation.Controls.Add(this.chkCustomerMessage);
			this.fraInformation.Controls.Add(this.chkDataEntryMessage);
			this.fraInformation.Controls.Add(this.fraDataEntryMessage);
			this.fraInformation.Controls.Add(this.chkAddress);
			this.fraInformation.Controls.Add(this.chkAlternateAddress);
			this.fraInformation.Controls.Add(this.fraAlternateAddress);
			this.fraInformation.Location = new System.Drawing.Point(30, 522);
			this.fraInformation.Name = "fraInformation";
			this.fraInformation.Size = new System.Drawing.Size(940, 260);
			this.fraInformation.TabIndex = 4;
			this.fraInformation.Text = "Information To Report";
			// 
			// chkBillMessage
			// 
			this.chkBillMessage.Location = new System.Drawing.Point(20, 214);
			this.chkBillMessage.Name = "chkBillMessage";
			this.chkBillMessage.Size = new System.Drawing.Size(120, 27);
			this.chkBillMessage.TabIndex = 35;
			this.chkBillMessage.Text = "Bill Message";
			// 
			// chkEmail
			// 
			this.chkEmail.Location = new System.Drawing.Point(20, 122);
			this.chkEmail.Name = "chkEmail";
			this.chkEmail.Size = new System.Drawing.Size(133, 27);
			this.chkEmail.TabIndex = 34;
			this.chkEmail.Text = "Email Address";
			// 
			// Check1
			// 
			this.Check1.Location = new System.Drawing.Point(20, 168);
			this.Check1.Name = "Check1";
			this.Check1.Size = new System.Drawing.Size(137, 27);
			this.Check1.TabIndex = 33;
			this.Check1.Text = "Phone Number";
			// 
			// chkCustomerMessage
			// 
			this.chkCustomerMessage.Location = new System.Drawing.Point(20, 76);
			this.chkCustomerMessage.Name = "chkCustomerMessage";
			this.chkCustomerMessage.Size = new System.Drawing.Size(94, 27);
			this.chkCustomerMessage.TabIndex = 27;
			this.chkCustomerMessage.Text = "Message";
			// 
			// chkDataEntryMessage
			// 
			this.chkDataEntryMessage.Location = new System.Drawing.Point(295, 136);
			this.chkDataEntryMessage.Name = "chkDataEntryMessage";
			this.chkDataEntryMessage.Size = new System.Drawing.Size(176, 27);
			this.chkDataEntryMessage.TabIndex = 22;
			this.chkDataEntryMessage.Text = "Data Entry Message";
			this.chkDataEntryMessage.CheckedChanged += new System.EventHandler(this.chkDataEntryMessage_CheckedChanged);
			// 
			// fraDataEntryMessage
			// 
			this.fraDataEntryMessage.AppearanceKey = "groupBoxNoBorders";
			this.fraDataEntryMessage.Controls.Add(this.Label5);
			this.fraDataEntryMessage.Controls.Add(this.cmbMessageNo);
			this.fraDataEntryMessage.Enabled = false;
			this.fraDataEntryMessage.Location = new System.Drawing.Point(295, 162);
			this.fraDataEntryMessage.Name = "fraDataEntryMessage";
			this.fraDataEntryMessage.Size = new System.Drawing.Size(575, 79);
			this.fraDataEntryMessage.TabIndex = 23;
			// 
			// Label5
			// 
			this.Label5.Enabled = false;
			this.Label5.Location = new System.Drawing.Point(0, 34);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(300, 16);
			this.Label5.TabIndex = 26;
			this.Label5.Text = "ONLY CUSTOMERS HAVING A DATA ENTRY MESSAGE";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// chkAddress
			// 
			this.chkAddress.Location = new System.Drawing.Point(20, 30);
			this.chkAddress.Name = "chkAddress";
			this.chkAddress.Size = new System.Drawing.Size(87, 27);
			this.chkAddress.TabIndex = 6;
			this.chkAddress.Text = "Address";
			// 
			// chkAlternateAddress
			// 
			this.chkAlternateAddress.Location = new System.Drawing.Point(295, 30);
			this.chkAlternateAddress.Name = "chkAlternateAddress";
			this.chkAlternateAddress.Size = new System.Drawing.Size(162, 27);
			this.chkAlternateAddress.TabIndex = 5;
			this.chkAlternateAddress.Text = "Alternate  Address";
			this.chkAlternateAddress.CheckedChanged += new System.EventHandler(this.chkAlternateAddress_CheckedChanged);
			// 
			// fraAlternateAddress
			// 
			this.fraAlternateAddress.AppearanceKey = "groupBoxNoBorders";
			this.fraAlternateAddress.Controls.Add(this.Label4);
			this.fraAlternateAddress.Controls.Add(this.cmbAltAddressYes);
			this.fraAlternateAddress.Enabled = false;
			this.fraAlternateAddress.Location = new System.Drawing.Point(295, 56);
			this.fraAlternateAddress.Name = "fraAlternateAddress";
			this.fraAlternateAddress.Size = new System.Drawing.Size(575, 80);
			this.fraAlternateAddress.TabIndex = 7;
			// 
			// Label4
			// 
			this.Label4.Enabled = false;
			this.Label4.Location = new System.Drawing.Point(0, 34);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(285, 18);
			this.Label4.TabIndex = 10;
			this.Label4.Text = "ONLY CUSTOMERS WITH AN ALTERNATE ADDRESS";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.fraStatus);
			this.Frame1.Controls.Add(this.cmbStatusAll);
			this.Frame1.Location = new System.Drawing.Point(30, 90);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(470, 182);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Status Selection";
			// 
			// fraStatus
			// 
			this.fraStatus.AppearanceKey = "groupBoxNoBorders";
			this.fraStatus.Controls.Add(this.chkDeleted);
			this.fraStatus.Controls.Add(this.chkActive);
			this.fraStatus.Enabled = false;
			this.fraStatus.Location = new System.Drawing.Point(0, 70);
			this.fraStatus.Name = "fraStatus";
			this.fraStatus.Size = new System.Drawing.Size(309, 112);
			this.fraStatus.TabIndex = 3;
			// 
			// chkDeleted
			// 
			this.chkDeleted.Enabled = false;
			this.chkDeleted.Location = new System.Drawing.Point(20, 66);
			this.chkDeleted.Name = "chkDeleted";
			this.chkDeleted.Size = new System.Drawing.Size(84, 27);
			this.chkDeleted.TabIndex = 21;
			this.chkDeleted.Text = "Deleted";
			// 
			// chkActive
			// 
			this.chkActive.Enabled = false;
			this.chkActive.Location = new System.Drawing.Point(20, 20);
			this.chkActive.Name = "chkActive";
			this.chkActive.Size = new System.Drawing.Size(72, 27);
			this.chkActive.TabIndex = 20;
			this.chkActive.Text = "Active";
			// 
			// btnFilePreview
			// 
			this.btnFilePreview.AppearanceKey = "acceptButton";
			this.btnFilePreview.Location = new System.Drawing.Point(410, 30);
			this.btnFilePreview.Name = "btnFilePreview";
			this.btnFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFilePreview.Size = new System.Drawing.Size(148, 48);
			this.btnFilePreview.TabIndex = 0;
			this.btnFilePreview.Text = "Print / Preview";
			this.btnFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// btnFilePrint
			// 
			this.btnFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFilePrint.AppearanceKey = "toolbarButton";
			this.btnFilePrint.Location = new System.Drawing.Point(890, 29);
			this.btnFilePrint.Name = "btnFilePrint";
			this.btnFilePrint.Size = new System.Drawing.Size(42, 24);
			this.btnFilePrint.TabIndex = 1;
			this.btnFilePrint.Text = "Print";
			this.btnFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmCustomerListSetup
			// 
			this.ClientSize = new System.Drawing.Size(968, 640);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCustomerListSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Listings";
			this.Load += new System.EventHandler(this.frmCustomerListSetup_Load);
			this.Activated += new System.EventHandler(this.frmCustomerListSetup_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomerListSetup_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraClassCodes)).EndInit();
			this.fraClassCodes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificBillTypes)).EndInit();
			this.fraSpecificBillTypes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsBillTypes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraListSelection)).EndInit();
			this.fraListSelection.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
			this.fraInformation.ResumeLayout(false);
			this.fraInformation.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBillMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCustomerMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDataEntryMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDataEntryMessage)).EndInit();
			this.fraDataEntryMessage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAlternateAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAlternateAddress)).EndInit();
			this.fraAlternateAddress.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraStatus)).EndInit();
			this.fraStatus.ResumeLayout(false);
			this.fraStatus.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFilePreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnFilePreview;
		private FCButton btnFilePrint;
	}
}
