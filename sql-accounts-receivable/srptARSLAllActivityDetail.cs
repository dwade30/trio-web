﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for srptARSLAllActivityDetail.
	/// </summary>
	public partial class srptARSLAllActivityDetail : FCSectionReport
	{
		public static srptARSLAllActivityDetail InstancePtr
		{
			get
			{
				return (srptARSLAllActivityDetail)Sys.GetInstance(typeof(srptARSLAllActivityDetail));
			}
		}

		protected srptARSLAllActivityDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptARSLAllActivityDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/31/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/01/2006              *
		// ********************************************************
		int lngBillKey;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsBill = new clsDRWrapper();
		double dblPrincipal;
		double dblTax;
		double dblInterest;
		double dblTotalPrincipal;
		// vbPorter upgrade warning: dblTotalTax As object	OnWrite(object, double)
		double dblTotalTax;
		double dblTotalInterest;
		bool boolInterestLine;
		double dblCurrentInterest;
		double dblTotal;

		public srptARSLAllActivityDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Activity Detail";
            this.ReportEnd += SrptARSLAllActivityDetail_ReportEnd;
		}

        private void SrptARSLAllActivityDetail_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
			rsBill.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				if (frmARStatusList.InstancePtr.chkCurrentInterest.CheckState == Wisej.Web.CheckState.Checked && boolInterestLine != true)
				{
					dblTotal = modARCalculations.CalculateAccountAR(rsBill, modGlobal.Statics.gdtARStatusListAsOfDate, ref dblCurrentInterest);
					if (dblCurrentInterest != 0)
					{
						boolInterestLine = true;
					}
				}
				else
				{
					boolInterestLine = false;
				}
				rsBill.MoveNext();
				eArgs.EOF = rsBill.EndOfFile();
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strAODate = "";
				// this is how I will pass the billkey into this subreport
				lngBillKey = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
				if (modGlobal.Statics.gboolARUseAsOfDate)
				{
					strAODate = " RecordedTransactionDate <= #" + FCConvert.ToString(modGlobal.Statics.gdtARStatusListAsOfDate) + "# AND ";
				}
				else
				{
					strAODate = "";
				}
				if (lngBillKey > 0)
				{
					rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey));
					rsData.OpenRecordset("SELECT * FROM PaymentRec WHERE " + strAODate + " BillKey = " + FCConvert.ToString(lngBillKey));
					if (rsData.EndOfFile())
					{
						// no payments were found for this account
						this.Cancel();
					}
					else
					{
						// rock on
						dblTotalPrincipal = FCConvert.ToDouble(rsBill.Get_Fields_Decimal("PrinOwed"));
						dblTotalTax = FCConvert.ToDouble(rsBill.Get_Fields_Decimal("TaxOwed"));
						dblTotalInterest = 0;
					}
				}
				else
				{
					// no bill key was passed in
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void BindFields()
		{
			// this will put the information into the fields
			if (!rsData.EndOfFile())
			{
				// Date
				fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy");
				// Code
				fldCode.Text = rsData.Get_Fields_String("Code");
				// Ref
				fldRef.Text = rsData.Get_Fields_String("Reference");
				// Payment
				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblPrincipal = FCConvert.ToDouble(rsData.Get_Fields("Principal"));
				// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
				dblTax = FCConvert.ToDouble(rsData.Get_Fields("Tax"));
				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
				dblInterest = FCConvert.ToDouble(rsData.Get_Fields("Interest"));
				// show the payment values
				fldPrincipal.Text = Strings.Format(dblPrincipal, "#,##0.00");
				fldTax.Text = Strings.Format(dblTax, "#,##0.00");
				fldInterest.Text = Strings.Format(dblInterest, "#,##0.00");
				fldTotal.Text = Strings.Format(dblPrincipal + dblTax + dblInterest, "#,##0.00");
				// update the running total
				dblTotalPrincipal -= dblPrincipal;
				dblTotalTax -= dblTax;
				dblTotalInterest -= dblInterest;
				rsData.MoveNext();
			}
			else if (boolInterestLine)
			{
				fldDate.Text = "";
				// Code
				fldCode.Text = "";
				// Ref
				fldRef.Text = "CURINT";
				// Payment
				dblPrincipal = 0;
				dblTax = 0;
				dblInterest = dblCurrentInterest;
				// show the payment values
				fldPrincipal.Text = Strings.Format(dblPrincipal, "#,##0.00");
				fldTax.Text = Strings.Format(dblTax, "#,##0.00");
				fldInterest.Text = Strings.Format(dblInterest, "#,##0.00");
				fldTotal.Text = Strings.Format(dblPrincipal + dblInterest + dblTax, "#,##0.00");
				// update the running total
				dblTotalInterest -= dblInterest;
			}
			else
			{
				fldDate.Visible = false;
				fldCode.Visible = false;
				fldRef.Visible = false;
				fldPrincipal.Visible = false;
				fldTax.Visible = false;
				fldInterest.Visible = false;
				fldTotal.Visible = false;
				Detail.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalPrincipal.Text = Strings.Format(dblTotalPrincipal, "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTotalTax, "#,##0.00");
			fldTotalInterest.Text = Strings.Format(dblTotalInterest, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotalPrincipal + dblTotalInterest + dblTotalTax, "#,##0.00");
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			fldOriginalPrincipal.Text = Strings.Format(dblTotalPrincipal, "#,##0.00");
			fldOriginalTax.Text = Strings.Format(dblTotalTax, "#,##0.00");
			fldOriginalInterest.Text = Strings.Format(dblTotalInterest, "#,##0.00");
			fldOriginalTotal.Text = Strings.Format(dblTotalPrincipal + dblTotalInterest + dblTotalTax, "#,##0.00");
		}

		private void srptARSLAllActivityDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptARSLAllActivityDetail.Caption	= "ActivityDetail";
			//srptARSLAllActivityDetail.Icon	= "srptARSLAllActivityDetail.dsx":0000";
			//srptARSLAllActivityDetail.Left	= 0;
			//srptARSLAllActivityDetail.Top	= 0;
			//srptARSLAllActivityDetail.Width	= 11880;
			//srptARSLAllActivityDetail.Height	= 8580;
			//srptARSLAllActivityDetail.StartUpPosition	= 3;
			//srptARSLAllActivityDetail.SectionData	= "srptARSLAllActivityDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
