﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.SectionReportModel;
using TWSharedLibrary;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptCustomLabels.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               09/12/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               08/27/2003              *
		// ********************************************************
		// THIS REPORT IS TOTOALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsMort = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		bool boolEmpty;
		string strLeftAdjustment;
		// vbPorter upgrade warning: lngVertAdjust As int	OnWriteFCConvert.ToDouble(
		float lngVertAdjust;
		int intTypeOfLabel;
		string strReport;
		clsPrintLabel labLabels = new clsPrintLabel();
		bool boolDotMatrix;
		// vbPorter upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		int lngPrintWidth;
		string strOldDefaultPrinter;

		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
            this.ReportEnd += RptCustomLabels_ReportEnd;
		}

        private void RptCustomLabels_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
		}
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		public void Init(string strSQL, string strReportType, short intLabelType, ref string strPrinterName, string strFonttoUse)
		{
			string strDBName = "";
			int intReturn;
			int x;
			bool boolUseFont;
			double dblLabelsAdjustment = 0;
			clsDRWrapper rsAdjustInfo = new clsDRWrapper();
			rsAdjustInfo.OpenRecordset("SELECT * FROM Customize");
			if (rsAdjustInfo.EndOfFile() != true && rsAdjustInfo.BeginningOfFile() != true)
			{
				dblLabelsAdjustment = Conversion.Val(rsAdjustInfo.Get_Fields_Double("LabelLaserAdjustment"));
			}
			else
			{
				dblLabelsAdjustment = 0;
			}
			strReport = strReportType;
			strFont = strFonttoUse;
			this.Document.Printer.PrinterName = strPrinterName;
			if (frmCustomerLabels.InstancePtr.cmbNumber.SelectedIndex == 0)
			{
				rsData.OpenRecordset("SELECT c.*, p.* FROM (" + strSQL + ") as c CROSS APPLY " + rsData.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p ORDER BY p.FullName");
			}
			else
			{
				rsData.OpenRecordset("SELECT c.*, p.* FROM (" + strSQL + ") as c CROSS APPLY " + rsData.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p ORDER BY c.CustomerID");
			}
			intTypeOfLabel = intLabelType;
			// make them choose the printer first if you have to use a custom form
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			boolDifferentPageSize = false;
			strLeftAdjustment = "";
			lngVertAdjust = FCConvert.ToSingle(240F * dblLabelsAdjustment) / 1440F;
			int intIndex;
			int cnt;
			intIndex = labLabels.Get_IndexFromID(intTypeOfLabel);
			if (labLabels.Get_IsDymoLabel(intIndex))
			{
				switch (labLabels.Get_ID(intIndex))
				{
					case modLabels.CNSTLBLTYPEDYMO30256:
						{
							strPrinterName = this.Document.Printer.PrinterName;
							this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Margins.Top = FCConvert.ToSingle(1.128);
							PageSettings.Margins.Bottom = 0;
							PageSettings.Margins.Right = 0;
							PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
							PrintWidth = FCConvert.ToSingle((4) - PageSettings.Margins.Left - PageSettings.Margins.Right);
							intLabelWidth = 4;
							lngPrintWidth = FCConvert.ToInt32((4) - PageSettings.Margins.Left - PageSettings.Margins.Right);
							Detail.Height = FCConvert.ToSingle(2.3125 - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f);
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = 4;
							PageSettings.PaperWidth = FCConvert.ToSingle(2.31);
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							break;
						}
					case modLabels.CNSTLBLTYPEDYMO30252:
						{
							strPrinterName = this.Document.Printer.PrinterName;
							this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
							PageSettings.Margins.Bottom = 0;
							PageSettings.Margins.Right = 0;
							PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
							// 
							PrintWidth = FCConvert.ToSingle((3.5) - PageSettings.Margins.Left - PageSettings.Margins.Right);
							intLabelWidth = FCConvert.ToInt32((3.5));
							lngPrintWidth = FCConvert.ToInt32((3.5) - PageSettings.Margins.Left - PageSettings.Margins.Right);
							Detail.Height = FCConvert.ToSingle((1.1) - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f);
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = FCConvert.ToSingle(3.5);
							PageSettings.PaperWidth = FCConvert.ToSingle(1.10);
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							break;
						}
					case modLabels.CNSTLBLTYPEDYMO30320:
						{
							strPrinterName = this.Document.Printer.PrinterName;
							this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
							PageSettings.Margins.Bottom = 0;
							PageSettings.Margins.Right = 0;
							PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
							PrintWidth = FCConvert.ToSingle((3.5) - PageSettings.Margins.Left - PageSettings.Margins.Right);
							intLabelWidth = FCConvert.ToInt32((3.5));
							lngPrintWidth = FCConvert.ToInt32((3.5) - PageSettings.Margins.Left - PageSettings.Margins.Right);
							Detail.Height = FCConvert.ToSingle((1.125) - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f);
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = FCConvert.ToSingle(3.5);
							PageSettings.PaperWidth = FCConvert.ToSingle(1.1250);
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							break;
						}
				}
			}
			else if (labLabels.Get_IsLaserLabel(intIndex))
			{
				PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
				PageSettings.Margins.Top += FCConvert.ToSingle(dblLabelsAdjustment * 270 / 1440f);
				PageSettings.Margins.Left = labLabels.Get_LeftMargin(intIndex);
				PageSettings.Margins.Right = labLabels.Get_RightMargin(intIndex);
				PrintWidth = FCConvert.ToSingle(labLabels.Get_PageWidth(intIndex) - labLabels.Get_LeftMargin(intIndex) - labLabels.Get_RightMargin(intIndex));
				intLabelWidth = labLabels.Get_LabelWidth(intIndex);
				Detail.Height = labLabels.Get_LabelHeight(intIndex) + labLabels.Get_VerticalSpace(intIndex);
				if (labLabels.Get_LabelsWide(intIndex) > 0)
				{
					Detail.ColumnCount = labLabels.Get_LabelsWide(intIndex);
					Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intIndex);
				}
				lngPrintWidth = FCConvert.ToInt32(PrintWidth);
			}
			CreateDataFields();
			rsAdjustInfo.DisposeOf();
			frmReportViewer.InstancePtr.Init(this, strPrinterName);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intReturn = 0;
			int const_PrintToolID;
			int cnt;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_PrintToolID = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_PrintToolID;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//} // cnt
			if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30320)
			{
				for (cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
				{
					switch (intTypeOfLabel)
					{
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30320:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30320 ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
									break;
								}
								break;
							}
					}
					//end switch
				}
				// cnt
			}
			else
			{
                this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomLabel", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight) * 100);
			}
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intCol;
			int intNumber;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 4;
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
                NewField = Detail.AddControlWithName<TextBox>("txtData" + FCConvert.ToString(intRow));
				//Detail.Controls.Add(NewField);
				NewField.CanGrow = false;
				//NewField.Name = "txtData" + FCConvert.ToString(intRow);
				NewField.Top = ((intRow - 1) * 225 / 1440f) + lngVertAdjust;
				NewField.Left = 144 / 1440f;
				// one space
				NewField.Width = (FCConvert.ToInt32(PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
				NewField.Height = 225 / 1440f;
				NewField.Text = string.Empty;
				NewField.MultiLine = false;
				NewField.WordWrap = true;
				if (Strings.Trim(strFont) != string.Empty)
				{
					NewField.Font = new Font(strFont, NewField.Font.Size);
				}
				else
				{
					NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
				}
				if (intRow == 1)
				{
                    NewField = Detail.AddControlWithName<TextBox>("txtAcct");
					//Detail.Controls.Add(NewField);
					NewField.CanGrow = false;
					//NewField.Name = "txtAcct";
					NewField.Top = ((intRow - 1) * 225 / 1440f) + lngVertAdjust;
					NewField.Left = (FCConvert.ToInt32(PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 1145 / 1440f;
					NewField.Width = 999 / 1440f;
					NewField.Height = 225 / 1440f;
					NewField.Text = string.Empty;
					NewField.MultiLine = false;
					NewField.WordWrap = true;
					if (Strings.Trim(strFont) != string.Empty)
					{
						NewField.Font = new Font(strFont, NewField.Font.Size);
					}
					else
					{
						NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
					}
				}
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			
		}
		
		private void PrintLabels()
		{
			// this will fill in the fields for labels
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			if (rsData.EndOfFile() != true)
			{
				if (strReport == "CUSTOMERS")
				{
					str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("FullName")));
					str2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
					str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
					str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
					// TODO: Field [PartyState] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("PartyState"))) != "")
					{
						// TODO: Field [PartyState] not found!! (maybe it is an alias?)
						str5 = Strings.Trim(rsData.Get_Fields_String("City") + ", " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("PartyState"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip"))));
					}
					else
					{
						str5 = Strings.Trim(rsData.Get_Fields_String("City") + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip"))));
					}
				}
				if (intTypeOfLabel == 2)
				{
					// small labels
					if (str1.Length > 18)
					{
						str5 = str4;
						str4 = str3;
						str3 = str2;
						str2 = Strings.Right(str1, str1.Length - 18);
						str1 = Strings.Left(str1, 18);
					}
				}
				else
				{
					if (str1.Length > 35)
					{
						// 4" labels
						str5 = str4;
						str4 = str3;
						str3 = str2;
						str2 = Strings.Right(str1, str1.Length - 35);
						str1 = Strings.Left(str1, 35);
					}
				}
				// condense the labels if some are blank
				// If boolMort Then
				if (Strings.Trim(str4) == string.Empty)
				{
					str4 = str5;
					str5 = "";
				}
				// End If
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						switch (intRow)
						{
							case 1:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str1;
									break;
								}
							case 2:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str2;
									break;
								}
							case 3:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str3;
									break;
								}
							case 4:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str4;
									break;
								}
							case 5:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str5;
									break;
								}
						}
						//end switch
					}
				}
				// intControl
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			PrintLabels();
		}

		private void rptCustomLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCustomLabels.Caption	= "Custom Labels";
			//rptCustomLabels.Icon	= "rptCustomLabels.dsx":0000";
			//rptCustomLabels.Left	= 0;
			//rptCustomLabels.Top	= 0;
			//rptCustomLabels.Width	= 11880;
			//rptCustomLabels.Height	= 8595;
			//rptCustomLabels.StartUpPosition	= 3;
			//rptCustomLabels.SectionData	= "rptCustomLabels.dsx":508A;
			//End Unmaped Properties
		}
	}
}
