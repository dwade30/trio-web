﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptARStatement.
	/// </summary>
	public partial class rptARStatement : BaseSectionReport
	{
		public static rptARStatement InstancePtr
		{
			get
			{
				return (rptARStatement)Sys.GetInstance(typeof(rptARStatement));
			}
		}

		protected rptARStatement _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptARStatement	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsCustomerInfo = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsBills = new clsDRWrapper();
		clsDRWrapper rsPayments = new clsDRWrapper();
		clsDRWrapper rsDefualts = new clsDRWrapper();
		bool blnDefaultPayeeSelected;
		int lngSelectedPayeeID;
		// vbPorter upgrade warning: datLowDate As DateTime	OnWrite(string)
		DateTime datLowDate;
		// vbPorter upgrade warning: datHighDate As DateTime	OnWrite(string)
		DateTime datHighDate;
		bool blnAllowZeroBalance;
		// 0 - Current, 1 - 30 Days, 2 - 60 Days, 3 - 90 Days, 4 - 120 Days, 5 - Finance Charges
		Decimal[] curTotals = new Decimal[5 + 1];
		// vbPorter upgrade warning: curInitialBalance As Decimal	OnWrite(short, Decimal)	OnRead(string)
		Decimal curInitialBalance;
		string strOrderBy = "";
		string strPaymentOrderBy = "";
		string strBillIDs;
		clsDRWrapper rsPayee = new clsDRWrapper();
		// vbPorter upgrade warning: curCurrentBalance As Decimal	OnWrite(short, string, Decimal)
		Decimal curCurrentBalance;

		public rptARStatement()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "AR Statement";
		}

		private struct Transaction
		{
			public string InvoiceNumber;
			public DateTime TransactionDate;
			public string Description;
			public string Reference;
			// vbPorter upgrade warning: Amount As string	OnWrite(double, Decimal, string)	OnRead(string, Decimal)
			public string Amount;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public Transaction(int unusedParam)
			{
				this.InvoiceNumber = string.Empty;
				this.TransactionDate = DateTime.FromOADate(0);
				this.Description = string.Empty;
				this.Reference = string.Empty;
				this.Amount = string.Empty;
			}
		};

		Transaction[] Transactions = null;
		int lngTransactionCounter;
		int lngCurrentTrans;
		// vbPorter upgrade warning: curNewTotals As Decimal	OnWrite(short, Decimal)	OnRead(Decimal, string)
		Decimal[] curNewTotals = new Decimal[5 + 1];

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				lngCurrentTrans = 0;
				eArgs.EOF = false;
			}
			else
			{
				lngCurrentTrans += 1;
				if (lngCurrentTrans < lngTransactionCounter)
				{
					eArgs.EOF = false;
				}
				else
				{
					SetNextCustomer();
					if (rsCustomerInfo.EndOfFile() != true)
					{
						eArgs.EOF = false;
						lngCurrentTrans = 0;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = rsCustomerInfo.Get_Fields_Int32("CustomerID");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
            rsCustomerInfo.DisposeOf();
            rsBills.DisposeOf();
            rsPayments.DisposeOf();
            rsDefualts.DisposeOf();
            rsPayee.DisposeOf();
			frmSetupARStatement.InstancePtr.Unload();
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strOrder = "";
			string strStatusSQL = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BATH")
			{
				fldReference.Visible = true;
				lblReference.Visible = true;
				linReference.Visible = true;
			}
			else
			{
				fldReference.Visible = false;
				lblReference.Visible = false;
				linReference.Visible = false;
			}
			rsDefualts.OpenRecordset("SELECT * FROM Customize");
			if (frmSetupARStatement.InstancePtr.cboPayee.SelectedIndex > 0)
			{
				blnDefaultPayeeSelected = false;
				lngSelectedPayeeID = frmSetupARStatement.InstancePtr.cboPayee.ItemData(frmSetupARStatement.InstancePtr.cboPayee.SelectedIndex);
				if (rsDefualts.EndOfFile() != true && rsDefualts.BeginningOfFile() != true)
				{
					if (lngSelectedPayeeID == FCConvert.ToInt32(rsDefualts.Get_Fields_Int32("DefaultPayeeID")))
					{
						blnDefaultPayeeSelected = true;
					}
					else
					{
						blnDefaultPayeeSelected = false;
					}
				}
				else
				{
					blnDefaultPayeeSelected = false;
				}
			}
			else
			{
				blnDefaultPayeeSelected = false;
				lngSelectedPayeeID = 0;
			}
			blnAllowZeroBalance = frmSetupARStatement.InstancePtr.chkZeroBalanceIncluded.CheckState == Wisej.Web.CheckState.Checked;
			datHighDate = FCConvert.ToDateTime(frmSetupARStatement.InstancePtr.txtEndDate.Text);
			datLowDate = FCConvert.ToDateTime(frmSetupARStatement.InstancePtr.txtStartDate.Text);
			if (frmSetupARStatement.InstancePtr.cmbTransactionDate.SelectedIndex == 0)
			{
				strOrder = " ORDER BY BillDate, InvoiceNumber";
				strPaymentOrderBy = " ORDER BY RecordedTransactionDate, PaymentRec.InvoiceNumber";
			}
			else
			{
				strOrder = " ORDER BY InvoiceNumber, BillDate";
				strPaymentOrderBy = " ORDER BY PaymentRec.InvoiceNumber, RecordedTransactionDate";
			}
			rsPayee.OpenRecordset("SELECT * FROM Payees WHERE PayeeID = " + FCConvert.ToString(frmSetupARStatement.InstancePtr.cboStatementPayee.ItemData(frmSetupARStatement.InstancePtr.cboStatementPayee.SelectedIndex)));
			if (rsPayee.EndOfFile() != true && rsPayee.BeginningOfFile() != true)
			{
				if (File.Exists(FCConvert.ToString(rsPayee.Get_Fields_String("LogoPath"))))
				{
					imgIcon.Image = FCUtils.LoadPicture(rsPayee.Get_Fields_String("LogoPath"));
				}
				fldRemittanceMessage.Text = Strings.Trim(FCConvert.ToString(rsPayee.Get_Fields_String("RemittanceMessage")));
			}
			else
			{
				fldRemittanceMessage.Text = "";
				imgIcon.Image = null;
			}
			if (frmSetupARStatement.InstancePtr.cmbAllCustomers.SelectedIndex == 0)
			{
				rsCustomerInfo.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsCustomerInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p ORDER BY p.FullName");
			}
			else
			{
				rsCustomerInfo.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsCustomerInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.FullName >= '" + Strings.Trim(frmSetupARStatement.InstancePtr.cboStart.Text) + "' AND p.FullName < '" + Strings.Trim(frmSetupARStatement.InstancePtr.cboEnd.Text) + "zzzz' ORDER BY p.FullName");
			}
			if (rsCustomerInfo.EndOfFile() != true && rsCustomerInfo.BeginningOfFile() != true)
			{
				SetNextCustomer();
				if (rsCustomerInfo.EndOfFile() == true)
				{
					this.Cancel();
					MessageBox.Show("No customers were found that matched the search criteria", "No Customers Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				this.Cancel();
				MessageBox.Show("No customers were found that matched the search criteria", "No Customers Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void SetNextCustomer()
		{
			int counter;
			double dblDue = 0;
			double dblCurrentInterest = 0;
			int lngAgedIndex = 0;
			bool blnAdded = false;
			if (!blnFirstRecord)
			{
				rsCustomerInfo.MoveNext();
			}
			for (counter = 0; counter <= 5; counter++)
			{
				curTotals[counter] = curNewTotals[counter];
			}
			for (counter = 0; counter <= 5; counter++)
			{
				curNewTotals[counter] = 0;
			}
			curCurrentBalance = 0;
			curInitialBalance = 0;
			CheckNext:
			;
			Transactions = new Transaction[0 + 1];
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			Transactions[0] = new Transaction(0);
			lngTransactionCounter = 0;
			lngCurrentTrans = 0;
			strBillIDs = "(";
			if (rsCustomerInfo.EndOfFile() != true)
			{
				if (lngSelectedPayeeID == 0)
				{
					// rsBills.OpenRecordset "SELECT * FROM Bill INNER JOIN RateKeys ON Bill.BillNumber = RateKeys.Ratekey INNER JOIN DefaultBillTypes ON Bill.BillType = DefaultBillTypes.TypeCode WHERE BillDate <= #" & datHighDate & "#" & strOrderBy
					rsBills.OpenRecordset("SELECT * FROM Bill INNER JOIN DefaultBillTypes ON Bill.BillType = DefaultBillTypes.TypeCode WHERE ActualAccountNumber = " + rsCustomerInfo.Get_Fields_Int32("CustomerID") + " AND BillStatus <> 'V' AND BillDate <= '" + FCConvert.ToString(datHighDate) + "'" + strOrderBy);
				}
				else
				{
					if (blnDefaultPayeeSelected)
					{
						rsBills.OpenRecordset("SELECT * FROM Bill INNER JOIN DefaultBillTypes ON Bill.BillType = DefaultBillTypes.TypeCode WHERE ActualAccountNumber = " + rsCustomerInfo.Get_Fields_Int32("CustomerID") + " AND BillStatus <> 'V' AND BillDate <= '" + FCConvert.ToString(datHighDate) + "' AND (isnull(PayeeID, 0) = 0 or isnull(PayeeID, 0) = " + FCConvert.ToString(lngSelectedPayeeID) + ")" + strOrderBy);
					}
					else
					{
						rsBills.OpenRecordset("SELECT * FROM Bill INNER JOIN DefaultBillTypes ON Bill.BillType = DefaultBillTypes.TypeCode WHERE ActualAccountNumber = " + rsCustomerInfo.Get_Fields_Int32("CustomerID") + " AND BillStatus <> 'V' AND BillDate <= '" + FCConvert.ToString(datHighDate) + "' AND (isnull(PayeeID, 0) = " + FCConvert.ToString(lngSelectedPayeeID) + ")" + strOrderBy);
					}
				}
				if (rsBills.EndOfFile() != true && rsBills.BeginningOfFile() != true)
				{
					do
					{
						dblCurrentInterest = 0;
						dblDue = modARCalculations.CalculateAccountAR(rsBills, datHighDate, ref dblCurrentInterest);
						if (rsBills.Get_Fields_DateTime("BillDate").ToOADate() < datLowDate.ToOADate())
						{
							curInitialBalance += FCConvert.ToDecimal(dblDue);
							lngAgedIndex = FindIndexForAgedList_2(rsBills.Get_Fields_DateTime("BillDate"), datHighDate);
							curNewTotals[lngAgedIndex] += FCConvert.ToDecimal(dblDue);
						}
						else
						{
							lngAgedIndex = FindIndexForAgedList_2(rsBills.Get_Fields_DateTime("BillDate"), datHighDate);
							curNewTotals[lngAgedIndex] += FCConvert.ToDecimal(dblDue) - FCConvert.ToDecimal(dblCurrentInterest);
							curNewTotals[5] += FCConvert.ToDecimal(dblCurrentInterest);
							Array.Resize(ref Transactions, lngTransactionCounter + 1);
							//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
							Transactions[lngTransactionCounter] = new Transaction(0);
							Transactions[lngTransactionCounter].Amount = FCConvert.ToString(rsBills.Get_Fields_Decimal("PrinOwed") + rsBills.Get_Fields_Decimal("TaxOwed"));
							Transactions[lngTransactionCounter].Description = "Invoice";
							// TODO: Field [Bill.Reference] not found!! (maybe it is an alias?)
							Transactions[lngTransactionCounter].Reference = FCConvert.ToString(rsBills.Get_Fields("Reference"));
							Transactions[lngTransactionCounter].InvoiceNumber = FCConvert.ToString(rsBills.Get_Fields_String("InvoiceNumber"));
							Transactions[lngTransactionCounter].TransactionDate = (DateTime)rsBills.Get_Fields_DateTime("BillDate");
							lngTransactionCounter += 1;
						}
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						strBillIDs += rsBills.Get_Fields("Bill") + ",";
						rsBills.MoveNext();
					}
					while (rsBills.EndOfFile() != true);
					if (strBillIDs != "(")
					{
						strBillIDs = Strings.Left(strBillIDs, strBillIDs.Length - 1);
					}
					strBillIDs += ")";
					if (curNewTotals[5] != 0)
					{
						Array.Resize(ref Transactions, lngTransactionCounter + 1);
						//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
						Transactions[lngTransactionCounter] = new Transaction(0);
						Transactions[lngTransactionCounter].Amount = FCConvert.ToString(curNewTotals[5]);
						Transactions[lngTransactionCounter].Description = "Finance Charges as of Reporting Date";
						Transactions[lngTransactionCounter].InvoiceNumber = "Fin Chg";
						Transactions[lngTransactionCounter].Reference = "";
						Transactions[lngTransactionCounter].TransactionDate = datHighDate;
						lngTransactionCounter += 1;
					}
				}
			}
			else
			{
				return;
			}
			if (!blnAllowZeroBalance)
			{
				if (curInitialBalance + curNewTotals[0] + curNewTotals[1] + curNewTotals[2] + curNewTotals[3] + curNewTotals[4] + curNewTotals[5] == 0)
				{
					rsCustomerInfo.MoveNext();
					goto CheckNext;
				}
			}
			if (rsCustomerInfo.EndOfFile() != true)
			{
				rsPayments.OpenRecordset("SELECT PaymentRec.*, Bill.BillDate as BillDate FROM PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.ID WHERE PaymentRec.ActualAccountNumber = " + rsCustomerInfo.Get_Fields_Int32("CustomerID") + " AND RecordedTransactionDate >= '" + FCConvert.ToString(datLowDate) + "' AND RecordedTransactionDate <= '" + FCConvert.ToString(datHighDate) + "' " + strPaymentOrderBy);
				// AND BillKey IN " & strBillIDs
			}
			else
			{
				rsPayments.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0");
			}
			if (rsPayments.EndOfFile() != true && rsPayments.BeginningOfFile() != true)
			{
				do
				{
					blnAdded = false;
					for (counter = 0; counter <= lngTransactionCounter - 1; counter++)
					{
						if (frmSetupARStatement.InstancePtr.cmbTransactionDate.SelectedIndex == 0)
						{
							if (rsPayments.Get_Fields_DateTime("RecordedTransactionDate").ToOADate() < Transactions[counter].TransactionDate.ToOADate())
							{
								IncrementTransactions(ref counter);
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
								Transactions[counter].Amount = FCConvert.ToString((rsPayments.Get_Fields("Principal") + rsPayments.Get_Fields("Tax") + rsPayments.Get_Fields("Interest")) * -1);
								if (rsPayments.Get_Fields_String("Code") == "C")
								{
									Transactions[counter].Description = "Correction";
								}
								else if (rsPayments.Get_Fields_String("Code") == "I")
								{
									Transactions[counter].Description = "Charged Interest";
								}
								else
								{
									Transactions[counter].Description = "Payment";
								}
								Transactions[counter].Reference = "";
								Transactions[counter].InvoiceNumber = FCConvert.ToString(rsPayments.Get_Fields_String("InvoiceNumber"));
								Transactions[counter].TransactionDate = (DateTime)rsPayments.Get_Fields_DateTime("RecordedTransactionDate");
								blnAdded = true;
								break;
							}
						}
						else
						{
							if (Strings.CompareString(rsPayments.Get_Fields_String("InvoiceNumber"), "<", Transactions[counter].InvoiceNumber))
							{
								IncrementTransactions(ref counter);
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
								Transactions[counter].Amount = FCConvert.ToString((rsPayments.Get_Fields("Principal") + rsPayments.Get_Fields("Tax") + rsPayments.Get_Fields("Interest")) * -1);
								if (rsPayments.Get_Fields_String("Code") == "C")
								{
									Transactions[counter].Description = "Correction";
								}
								else if (rsPayments.Get_Fields_String("Code") == "I")
								{
									Transactions[counter].Description = "Charged Interest";
								}
								else
								{
									Transactions[counter].Description = "Payment";
								}
								Transactions[counter].Reference = "";
								Transactions[counter].InvoiceNumber = FCConvert.ToString(rsPayments.Get_Fields_String("InvoiceNumber"));
								Transactions[counter].TransactionDate = (DateTime)rsPayments.Get_Fields_DateTime("RecordedTransactionDate");
								blnAdded = true;
								break;
							}
						}
					}
					if (!blnAdded)
					{
						Array.Resize(ref Transactions, lngTransactionCounter + 1);
						//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
						Transactions[lngTransactionCounter] = new Transaction(0);
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						Transactions[lngTransactionCounter].Amount = FCConvert.ToString((rsPayments.Get_Fields("Principal") + rsPayments.Get_Fields("Tax") + rsPayments.Get_Fields("Interest")) * -1);
						if (rsPayments.Get_Fields_String("Code") == "C")
						{
							Transactions[lngTransactionCounter].Description = "Correction";
						}
						else if (rsPayments.Get_Fields_String("Code") == "I")
						{
							Transactions[lngTransactionCounter].Description = "Charged Interest";
						}
						else
						{
							Transactions[lngTransactionCounter].Description = "Payment";
						}
						Transactions[lngTransactionCounter].Reference = "";
						Transactions[lngTransactionCounter].InvoiceNumber = FCConvert.ToString(rsPayments.Get_Fields_String("InvoiceNumber"));
						Transactions[lngTransactionCounter].TransactionDate = (DateTime)rsPayments.Get_Fields_DateTime("RecordedTransactionDate");
						lngTransactionCounter += 1;
					}
					if (rsPayments.Get_Fields_DateTime("BillDate").ToOADate() < datLowDate.ToOADate())
					{
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						curInitialBalance += rsPayments.Get_Fields("Principal") + rsPayments.Get_Fields("Tax") + rsPayments.Get_Fields("Interest");
					}
					rsPayments.MoveNext();
				}
				while (rsPayments.EndOfFile() != true);
			}
			IncrementTransactions_2(0);
			Transactions[0].Amount = FCConvert.ToString(curInitialBalance);
			Transactions[0].Description = "Beginning Balance";
			Transactions[0].InvoiceNumber = "";
			Transactions[0].Reference = "";
			Transactions[0].TransactionDate = datLowDate;
		}

		private void IncrementTransactions_2(int lngStartIndex)
		{
			IncrementTransactions(ref lngStartIndex);
		}

		private void IncrementTransactions(ref int lngStartIndex)
		{
			int counter;
			Array.Resize(ref Transactions, lngTransactionCounter + 1);
			lngTransactionCounter += 1;
			if (lngTransactionCounter > 1)
			{
				for (counter = lngTransactionCounter - 1; counter >= lngStartIndex + 1; counter--)
				{
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					Transactions[counter] = new Transaction(0);
					Transactions[counter].Amount = Transactions[counter - 1].Amount;
					Transactions[counter].Description = Transactions[counter - 1].Description;
					Transactions[counter].Reference = Transactions[counter - 1].Reference;
					Transactions[counter].InvoiceNumber = Transactions[counter - 1].InvoiceNumber;
					Transactions[counter].TransactionDate = Transactions[counter - 1].TransactionDate;
				}
			}
		}

		private int FindIndexForAgedList_2(DateTime datBillDate, DateTime dtCheckDate)
		{
			return FindIndexForAgedList(ref datBillDate, ref dtCheckDate);
		}

		private int FindIndexForAgedList(ref DateTime datBillDate, ref DateTime dtCheckDate)
		{
			int FindIndexForAgedList = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// vbPorter upgrade warning: lngDays As int --> As long
				long lngDays;
				lngDays = DateAndTime.DateDiff("D", datBillDate, dtCheckDate);
				if (lngDays < 30)
				{
					// Current Taxes
					FindIndexForAgedList = 0;
				}
				else if (lngDays >= 30 && lngDays <= 59)
				{
					// 30 Days
					FindIndexForAgedList = 1;
				}
				else if (lngDays >= 60 && lngDays <= 89)
				{
					// 60 Days
					FindIndexForAgedList = 2;
				}
				else if (lngDays >= 90 && lngDays <= 119)
				{
					// 90 Days
					FindIndexForAgedList = 3;
				}
				else if (lngDays > 120)
				{
					// 120 Days
					FindIndexForAgedList = 4;
				}
				return FindIndexForAgedList;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FindIndexForAgedList = 0;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Index", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindIndexForAgedList;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldInvoice.Text = Transactions[lngCurrentTrans].InvoiceNumber;
			fldDate.Text = Strings.Format(Transactions[lngCurrentTrans].TransactionDate, "MM/dd/yyyy");
			fldTransaction.Text = Transactions[lngCurrentTrans].Description;
			fldReference.Text = Transactions[lngCurrentTrans].Reference;
			if (Transactions[lngCurrentTrans].Description == "Beginning Balance")
			{
				fldAmount.Text = "";
				curCurrentBalance = FCConvert.ToDecimal(Transactions[lngCurrentTrans].Amount);
				fldBalance.Text = Strings.Format(curCurrentBalance, "#,##0.00");
			}
			else
			{
				fldAmount.Text = Strings.Format(Transactions[lngCurrentTrans].Amount, "#,##0.00");
				curCurrentBalance += FCConvert.ToDecimal(Transactions[lngCurrentTrans].Amount);
				fldBalance.Text = Strings.Format(curCurrentBalance, "#,##0.00");
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldCurrent.Text = Strings.Format(curTotals[0], "#,##0.00");
			fldOver30.Text = Strings.Format(curTotals[1], "#,##0.00");
			fldOver60.Text = Strings.Format(curTotals[2], "#,##0.00");
			fldOver90.Text = Strings.Format(curTotals[3], "#,##0.00");
			fldOver120.Text = Strings.Format(curTotals[4], "#,##0.00");
			fldFinanceCharge.Text = Strings.Format(curTotals[5], "#,##0.00");
			fldTotal.Text = Strings.Format(curTotals[0] + curTotals[1] + curTotals[2] + curTotals[3] + curTotals[4] + curTotals[5], "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldCustomerName.Text = rsCustomerInfo.Get_Fields_String("FullName");
			fldAddress1.Text = rsCustomerInfo.Get_Fields_String("Address1");
			fldAddress2.Text = rsCustomerInfo.Get_Fields_String("Address2");
			fldAddress3.Text = rsCustomerInfo.Get_Fields_String("Address3");
			if (Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Zip4"))) != "")
			{
				fldAddress4.Text = rsCustomerInfo.Get_Fields_String("City") + ", " + rsCustomerInfo.Get_Fields_String("PartyState") + " " + rsCustomerInfo.Get_Fields_String("Zip") + "-" + rsCustomerInfo.Get_Fields_String("Zip4");
			}
			else
			{
				fldAddress4.Text = rsCustomerInfo.Get_Fields_String("City") + ", " + rsCustomerInfo.Get_Fields_String("PartyState") + " " + rsCustomerInfo.Get_Fields_String("Zip");
			}
			SetAddressLines();
			fldCustomerNumber.Text = Strings.Format(rsCustomerInfo.Get_Fields_Int32("CustomerID"), "00000");
			fldStatementDate.Text = Strings.Format(datHighDate, "MM/dd/yyyy");
		}

		private void SetAddressLines()
		{
			if (Strings.Trim(fldAddress1.Text) == "")
			{
				fldAddress1.Text = fldAddress2.Text;
				fldAddress2.Text = fldAddress3.Text;
				fldAddress3.Text = fldAddress4.Text;
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress1.Text) == "")
			{
				fldAddress1.Text = fldAddress2.Text;
				fldAddress2.Text = fldAddress3.Text;
				fldAddress3.Text = fldAddress4.Text;
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress1.Text) == "")
			{
				fldAddress1.Text = fldAddress2.Text;
				fldAddress2.Text = fldAddress3.Text;
				fldAddress3.Text = fldAddress4.Text;
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress2.Text) == "")
			{
				fldAddress2.Text = fldAddress3.Text;
				fldAddress3.Text = fldAddress4.Text;
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress2.Text) == "")
			{
				fldAddress2.Text = fldAddress3.Text;
				fldAddress3.Text = fldAddress4.Text;
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress3.Text) == "")
			{
				fldAddress3.Text = fldAddress4.Text;
				fldAddress4.Text = "";
			}
		}

		private void rptARStatement_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptARStatement.Caption	= "AR Statement";
			//rptARStatement.Icon	= "rptARStatement.dsx":0000";
			//rptARStatement.Left	= 0;
			//rptARStatement.Top	= 0;
			//rptARStatement.Width	= 20280;
			//rptARStatement.Height	= 11115;
			//rptARStatement.StartUpPosition	= 3;
			//rptARStatement.SectionData	= "rptARStatement.dsx":508A;
			//End Unmaped Properties
		}

		private void rptARStatement_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
