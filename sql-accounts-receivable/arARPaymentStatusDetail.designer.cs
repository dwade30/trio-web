﻿namespace Global
{
	/// <summary>
	/// Summary description for arARAccountDetail.
	/// </summary>
	partial class arARPaymentStatusDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arARPaymentStatusDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldPerDiem1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPerDiem1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPerDiemHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPerDiem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiemHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldName,
            this.fldBN,
            this.fldDate,
            this.fldCode,
            this.fldRef,
            this.fldPrincipal,
            this.fldTotal,
            this.fldInterest,
            this.lnTotals,
            this.fldTax});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Visible = false;
			this.fldName.Width = 5.9375F;
			// 
			// fldBN
			// 
			this.fldBN.Height = 0.1875F;
			this.fldBN.Left = 0F;
			this.fldBN.Name = "fldBN";
			this.fldBN.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldBN.Text = null;
			this.fldBN.Top = 0F;
			this.fldBN.Width = 0.875F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0.9375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.8125F;
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 2.625F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldCode.Text = null;
			this.fldCode.Top = 0F;
			this.fldCode.Width = 0.1875F;
			// 
			// fldRef
			// 
			this.fldRef.Height = 0.1875F;
			this.fldRef.Left = 1.75F;
			this.fldRef.Name = "fldRef";
			this.fldRef.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldRef.Text = null;
			this.fldRef.Top = 0F;
			this.fldRef.Width = 0.875F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 3.125F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 1.0625F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.375F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.125F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 5.3125F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 1.0625F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 2.375F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0F;
			this.lnTotals.Visible = false;
			this.lnTotals.Width = 5.125F;
			this.lnTotals.X1 = 2.375F;
			this.lnTotals.X2 = 7.5F;
			this.lnTotals.Y1 = 0F;
			this.lnTotals.Y2 = 0F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 4.25F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 1F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldPerDiem1,
            this.lblPerDiem1,
            this.lblPerDiemHeader,
            this.Line3});
			this.ReportFooter.Height = 2.53125F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldPerDiem1
			// 
			this.fldPerDiem1.Height = 0.1875F;
			this.fldPerDiem1.Left = 2F;
			this.fldPerDiem1.Name = "fldPerDiem1";
			this.fldPerDiem1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldPerDiem1.Text = null;
			this.fldPerDiem1.Top = 0.1875F;
			this.fldPerDiem1.Width = 1F;
			// 
			// lblPerDiem1
			// 
			this.lblPerDiem1.Height = 0.1875F;
			this.lblPerDiem1.HyperLink = null;
			this.lblPerDiem1.Left = 0.375F;
			this.lblPerDiem1.Name = "lblPerDiem1";
			this.lblPerDiem1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblPerDiem1.Text = null;
			this.lblPerDiem1.Top = 0.1875F;
			this.lblPerDiem1.Width = 1.625F;
			// 
			// lblPerDiemHeader
			// 
			this.lblPerDiemHeader.Height = 0.1875F;
			this.lblPerDiemHeader.HyperLink = null;
			this.lblPerDiemHeader.Left = 0.375F;
			this.lblPerDiemHeader.Name = "lblPerDiemHeader";
			this.lblPerDiemHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblPerDiemHeader.Text = "Per Diem";
			this.lblPerDiemHeader.Top = 0F;
			this.lblPerDiemHeader.Width = 2.625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.1875F;
			this.Line3.Width = 2.625F;
			this.Line3.X1 = 0.375F;
			this.Line3.X2 = 3F;
			this.Line3.Y1 = 0.1875F;
			this.Line3.Y2 = 0.1875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblAcct,
            this.lblDateHeader,
            this.lblRef,
            this.lblCode,
            this.lblPrincipal,
            this.lblInterest,
            this.lblTotal,
            this.Line1,
            this.lblName,
            this.lblLocation,
            this.lblTax,
            this.lblAddress1,
            this.lblAddress2,
            this.lblAddress3,
            this.lblAddress4});
			this.PageHeader.Height = 1.635417F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.45F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0.05F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
			this.lblHeader.Text = "Account Detail";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.45F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 3.125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.25F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.25F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.25F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.25F;
			// 
			// lblAcct
			// 
			this.lblAcct.Height = 0.1875F;
			this.lblAcct.HyperLink = null;
			this.lblAcct.Left = 0F;
			this.lblAcct.Name = "lblAcct";
			this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
			this.lblAcct.Text = "Bill";
			this.lblAcct.Top = 1.4375F;
			this.lblAcct.Width = 0.875F;
			// 
			// lblDateHeader
			// 
			this.lblDateHeader.Height = 0.1875F;
			this.lblDateHeader.HyperLink = null;
			this.lblDateHeader.Left = 0.9375F;
			this.lblDateHeader.Name = "lblDateHeader";
			this.lblDateHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblDateHeader.Text = "Date";
			this.lblDateHeader.Top = 1.4375F;
			this.lblDateHeader.Width = 0.8125F;
			// 
			// lblRef
			// 
			this.lblRef.Height = 0.1875F;
			this.lblRef.HyperLink = null;
			this.lblRef.Left = 1.75F;
			this.lblRef.Name = "lblRef";
			this.lblRef.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblRef.Text = "Reference";
			this.lblRef.Top = 1.4375F;
			this.lblRef.Width = 0.875F;
			// 
			// lblCode
			// 
			this.lblCode.Height = 0.1875F;
			this.lblCode.HyperLink = null;
			this.lblCode.Left = 2.625F;
			this.lblCode.Name = "lblCode";
			this.lblCode.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblCode.Text = "C";
			this.lblCode.Top = 1.4375F;
			this.lblCode.Width = 0.1875F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 3.125F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 1.4375F;
			this.lblPrincipal.Width = 1.0625F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 5.3125F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 1.4375F;
			this.lblInterest.Width = 1.0625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 6.375F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 1.4375F;
			this.lblTotal.Width = 1.125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.625F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 1.625F;
			this.Line1.Y2 = 1.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.375F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.375F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblName.Text = null;
			this.lblName.Top = 0.5F;
			this.lblName.Width = 3.75F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 0.375F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblLocation.Text = null;
			this.lblLocation.Top = 0.875F;
			this.lblLocation.Width = 3.5F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 4.25F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 1.4375F;
			this.lblTax.Width = 1F;
			// 
			// lblAddress1
			// 
			this.lblAddress1.Height = 0.1875F;
			this.lblAddress1.HyperLink = null;
			this.lblAddress1.Left = 4.125F;
			this.lblAddress1.Name = "lblAddress1";
			this.lblAddress1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAddress1.Text = null;
			this.lblAddress1.Top = 0.5F;
			this.lblAddress1.Width = 3.25F;
			// 
			// lblAddress2
			// 
			this.lblAddress2.Height = 0.1875F;
			this.lblAddress2.HyperLink = null;
			this.lblAddress2.Left = 4.125F;
			this.lblAddress2.Name = "lblAddress2";
			this.lblAddress2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAddress2.Text = null;
			this.lblAddress2.Top = 0.6875F;
			this.lblAddress2.Width = 3.25F;
			// 
			// lblAddress3
			// 
			this.lblAddress3.Height = 0.1875F;
			this.lblAddress3.HyperLink = null;
			this.lblAddress3.Left = 4.125F;
			this.lblAddress3.Name = "lblAddress3";
			this.lblAddress3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAddress3.Text = null;
			this.lblAddress3.Top = 0.875F;
			this.lblAddress3.Width = 3.25F;
			// 
			// lblAddress4
			// 
			this.lblAddress4.Height = 0.1875F;
			this.lblAddress4.HyperLink = null;
			this.lblAddress4.Left = 4.125F;
			this.lblAddress4.Name = "lblAddress4";
			this.lblAddress4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAddress4.Text = null;
			this.lblAddress4.Top = 1.0625F;
			this.lblAddress4.Width = 3.25F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// arARPaymentStatusDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPerDiem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPerDiemHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPerDiem1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiem1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiemHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRef;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
