﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWAR0000
{
	public class modCLCalculations
	{
		public const string strARDatabase = "TWAR0000.VB1";
		public const string strBDDatabase = "TWBD0000.vb1";
		public const string strCLDatabase = "TWCL0000.vb1";
		public const string strCKDatabase = "TWCK0000.vb1";
		public const string strCRDatabase = "TWCR0000.vb1";
		
		public const string strMVDatabase = "TWMV0000.vb1";
		public const string strPPDatabase = "TWPP0000.vb1";
		
		public const string strREDatabase = "TWRE0000.vb1";
		public const string strUTDatabase = "TWUT0000.vb1";
		public const string strVRDatabase = "TWVR0000.vb1";
		// vbPorter upgrade warning: 'Return' As double	OnWrite(string)
		public static double Rounding(float nValue, short nPositiveNumberOfDigits)
		{
			double Rounding = 0;
			string strFormat;
			strFormat = "#,###." + Strings.StrDup(nPositiveNumberOfDigits, "0");
			Rounding = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(nValue), strFormat));
			return Rounding;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		//public static string MostRecentDate(ref string ApplyDate, ref string InterestStartDate)
		//{
		//	string MostRecentDate = "";
		//	if (Information.IsDate(ApplyDate) == true)
		//	{
		//		if (fecherFoundation.Strings.CompareString(Strings.Format(InterestStartDate, "MM/dd/yyyy"), Strings.Format(ApplyDate, "MM/dd/yyyy"), true) < 0)
		//		{
		//			MostRecentDate = Strings.Format(ApplyDate, "MM/dd/yyyy");
		//		}
		//		else
		//		{
		//			MostRecentDate = Strings.Format(InterestStartDate, "MM/dd/yyyy");
		//		}
		//	}
		//	else
		//	{
		//		MostRecentDate = Strings.Format(InterestStartDate, "MM/dd/yyyy");
		//	}
		//	return MostRecentDate;
		//}

		public static bool IsThisCR()
		{
			bool IsThisCR = false;
			// this will return true if this is the CR executable, false otherwise
			if (Strings.UCase(App.EXEName) == "TWCR0000" && modGlobalConstants.Statics.gboolCR)
			{
				IsThisCR = true;
			}
			else
			{
				IsThisCR = false;
			}
			return IsThisCR;
		}

		//public static void GetCustomer(ref int lngCustNumber)
		//{
		//	frmGetCustomerMaster.InstancePtr.StartProgram(lngCustNumber);
		//}

		
	}

	public class modUseCR
	{
		public static void FillARInfo()
		{
			// this is a dummy function for this version of the form instead CR
		}

		public static void DeleteARPaymentFromShoppingList(ref int lngID)
		{
			// this is a dummy function for this version of the form instead CR
		}
	}
}
