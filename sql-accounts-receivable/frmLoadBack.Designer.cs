﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmLoadBack.
	/// </summary>
	partial class frmLoadBack : BaseForm
	{
		public fecherFoundation.FCFrame fraAccountSearch;
		public fecherFoundation.FCTextBox txtSearchName;
		public fecherFoundation.FCGrid vsAccountSearch;
		public fecherFoundation.FCLabel lblSearchName;
		public fecherFoundation.FCFrame fraAccountInfo;
		public fecherFoundation.FCComboBox cboRateKeys;
		public Global.T2KDateBox txtDefaultDate;
		public fecherFoundation.FCLabel lblInterestPTD;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCFrame fraAccounts;
		public fecherFoundation.FCGrid vsAccounts;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.fraAccountSearch = new fecherFoundation.FCFrame();
			this.txtSearchName = new fecherFoundation.FCTextBox();
			this.vsAccountSearch = new fecherFoundation.FCGrid();
			this.lblSearchName = new fecherFoundation.FCLabel();
			this.fraAccountInfo = new fecherFoundation.FCFrame();
			this.cboRateKeys = new fecherFoundation.FCComboBox();
			this.txtDefaultDate = new Global.T2KDateBox();
			this.lblInterestPTD = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.fraAccounts = new fecherFoundation.FCFrame();
			this.vsAccounts = new fecherFoundation.FCGrid();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnFileSave = new fecherFoundation.FCButton();
			this.btnFileSearch = new fecherFoundation.FCButton();
			this.btnFileDelete = new fecherFoundation.FCButton();
			this.btnFileNew = new fecherFoundation.FCButton();
			this.btnFileAdd = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).BeginInit();
			this.fraAccountSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountInfo)).BeginInit();
			this.fraAccountInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccounts)).BeginInit();
			this.fraAccounts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileAdd)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraAccountInfo);
			this.ClientArea.Controls.Add(this.fraAccounts);
			this.ClientArea.Controls.Add(this.fraAccountSearch);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnFileAdd);
			this.TopPanel.Controls.Add(this.btnFileNew);
			this.TopPanel.Controls.Add(this.btnFileDelete);
			this.TopPanel.Controls.Add(this.btnFileSearch);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileAdd, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(145, 30);
			this.HeaderText.Text = "Manual Bills";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// fraAccountSearch
			// 
			this.fraAccountSearch.AppearanceKey = "groupBoxNoBorders";
			this.fraAccountSearch.Controls.Add(this.txtSearchName);
			this.fraAccountSearch.Controls.Add(this.vsAccountSearch);
			this.fraAccountSearch.Controls.Add(this.lblSearchName);
			this.fraAccountSearch.Location = new System.Drawing.Point(30, 194);
			this.fraAccountSearch.Name = "fraAccountSearch";
			this.fraAccountSearch.Size = new System.Drawing.Size(1032, 324);
			this.fraAccountSearch.TabIndex = 6;
			this.fraAccountSearch.Text = "Account Search";
			this.ToolTip1.SetToolTip(this.fraAccountSearch, null);
			this.fraAccountSearch.Visible = false;
			// 
			// txtSearchName
			// 
			this.txtSearchName.AutoSize = false;
			this.txtSearchName.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearchName.Location = new System.Drawing.Point(99, 30);
			this.txtSearchName.Name = "txtSearchName";
			this.txtSearchName.Size = new System.Drawing.Size(122, 40);
			this.txtSearchName.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtSearchName, null);
			this.txtSearchName.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearchName_KeyDown);
			// 
			// vsAccountSearch
			// 
			this.vsAccountSearch.AllowSelection = false;
			this.vsAccountSearch.AllowUserToResizeColumns = false;
			this.vsAccountSearch.AllowUserToResizeRows = false;
			this.vsAccountSearch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAccountSearch.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorSel = System.Drawing.Color.Empty;
			this.vsAccountSearch.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAccountSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsAccountSearch.ColumnHeadersHeight = 30;
			this.vsAccountSearch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAccountSearch.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsAccountSearch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAccountSearch.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAccountSearch.FrozenCols = 0;
			this.vsAccountSearch.GridColor = System.Drawing.Color.Empty;
			this.vsAccountSearch.Location = new System.Drawing.Point(0, 80);
			this.vsAccountSearch.Name = "vsAccountSearch";
			this.vsAccountSearch.ReadOnly = true;
			this.vsAccountSearch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAccountSearch.RowHeightMin = 0;
			this.vsAccountSearch.Rows = 1;
			this.vsAccountSearch.ShowColumnVisibilityMenu = false;
			this.vsAccountSearch.Size = new System.Drawing.Size(1026, 238);
			this.vsAccountSearch.StandardTab = true;
			this.vsAccountSearch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsAccountSearch.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.vsAccountSearch, null);
			this.vsAccountSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsAccountSearch_KeyPressEvent);
			this.vsAccountSearch.DoubleClick += new System.EventHandler(this.vsAccountSearch_DblClick);
			// 
			// lblSearchName
			// 
			this.lblSearchName.Location = new System.Drawing.Point(0, 44);
			this.lblSearchName.Name = "lblSearchName";
			this.lblSearchName.Size = new System.Drawing.Size(48, 16);
			this.lblSearchName.TabIndex = 8;
			this.lblSearchName.Text = "NAME";
			this.ToolTip1.SetToolTip(this.lblSearchName, null);
			// 
			// fraAccountInfo
			// 
			this.fraAccountInfo.Controls.Add(this.cboRateKeys);
			this.fraAccountInfo.Controls.Add(this.txtDefaultDate);
			this.fraAccountInfo.Controls.Add(this.lblInterestPTD);
			this.fraAccountInfo.Controls.Add(this.lblYear);
			this.fraAccountInfo.Location = new System.Drawing.Point(30, 30);
			this.fraAccountInfo.Name = "fraAccountInfo";
			this.fraAccountInfo.Size = new System.Drawing.Size(895, 150);
			this.fraAccountInfo.TabIndex = 2;
			this.fraAccountInfo.Text = "Default Information";
			this.ToolTip1.SetToolTip(this.fraAccountInfo, null);
			// 
			// cboRateKeys
			// 
			this.cboRateKeys.AutoSize = false;
			this.cboRateKeys.BackColor = System.Drawing.SystemColors.Window;
			this.cboRateKeys.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboRateKeys.FormattingEnabled = true;
			this.cboRateKeys.Location = new System.Drawing.Point(321, 30);
			this.cboRateKeys.Name = "cboRateKeys";
			this.cboRateKeys.Size = new System.Drawing.Size(556, 40);
			this.cboRateKeys.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.cboRateKeys, "<Bill Type Code>  <Bill Date>   <Rate Key Description>");
			this.cboRateKeys.SelectedIndexChanged += new System.EventHandler(this.cboRateKeys_SelectedIndexChanged);
			// 
			// txtDefaultDate
			//
			this.txtDefaultDate.MaxLength = 10;
			this.txtDefaultDate.Location = new System.Drawing.Point(321, 89);
			this.txtDefaultDate.Mask = "##/##/####";
			this.txtDefaultDate.Name = "txtDefaultDate";
			this.txtDefaultDate.Size = new System.Drawing.Size(115, 40);
			this.txtDefaultDate.TabIndex = 5;
			this.txtDefaultDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtDefaultDate, null);
			this.txtDefaultDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDefaultDate_Validate);
			// 
			// lblInterestPTD
			// 
			this.lblInterestPTD.Location = new System.Drawing.Point(20, 104);
			this.lblInterestPTD.Name = "lblInterestPTD";
			this.lblInterestPTD.Size = new System.Drawing.Size(239, 16);
			this.lblInterestPTD.TabIndex = 4;
			this.lblInterestPTD.Text = "DEFAULT INTEREST PAID THROUGH DATE";
			this.ToolTip1.SetToolTip(this.lblInterestPTD, null);
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(20, 44);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(56, 16);
			this.lblYear.TabIndex = 3;
			this.lblYear.Text = "RATE KEY";
			this.ToolTip1.SetToolTip(this.lblYear, null);
			// 
			// fraAccounts
			// 
			this.fraAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraAccounts.AppearanceKey = "groupBoxNoBorders";
			this.fraAccounts.Controls.Add(this.vsAccounts);
			this.fraAccounts.Enabled = false;
			this.fraAccounts.Location = new System.Drawing.Point(30, 200);
			this.fraAccounts.Name = "fraAccounts";
			this.fraAccounts.Size = new System.Drawing.Size(1032, 307);
			this.fraAccounts.TabIndex = 0;
			this.fraAccounts.Text = "Add Accounts";
			this.ToolTip1.SetToolTip(this.fraAccounts, null);
			// 
			// vsAccounts
			// 
			this.vsAccounts.AllowSelection = false;
			this.vsAccounts.AllowUserToResizeColumns = false;
			this.vsAccounts.AllowUserToResizeRows = false;
			this.vsAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAccounts.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorSel = System.Drawing.Color.Empty;
			this.vsAccounts.Cols = 19;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsAccounts.ColumnHeadersHeight = 30;
			this.vsAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAccounts.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAccounts.FixedCols = 0;
			this.vsAccounts.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.FrozenCols = 0;
			this.vsAccounts.GridColor = System.Drawing.Color.Empty;
			this.vsAccounts.Location = new System.Drawing.Point(0, 30);
			this.vsAccounts.Name = "vsAccounts";
			this.vsAccounts.ReadOnly = true;
			this.vsAccounts.RowHeadersVisible = false;
			this.vsAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAccounts.RowHeightMin = 0;
			this.vsAccounts.Rows = 1;
			this.vsAccounts.ShowColumnVisibilityMenu = false;
			this.vsAccounts.Size = new System.Drawing.Size(1026, 257);
			this.vsAccounts.StandardTab = true;
			this.vsAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsAccounts.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.vsAccounts, null);
			this.vsAccounts.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsAccounts_BeforeEdit);
			this.vsAccounts.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsAccounts_ValidateEdit);
			this.vsAccounts.CurrentCellChanged += new System.EventHandler(this.vsAccounts_RowColChange);
			this.vsAccounts.MouseMove += new Wisej.Web.MouseEventHandler(this.vsAccounts_MouseMoveEvent);
			this.vsAccounts.KeyDown += new Wisej.Web.KeyEventHandler(this.vsAccounts_KeyDownEvent);
			this.vsAccounts.Enter += new System.EventHandler(this.vsAccounts_Enter);
			// 
			// btnFileSave
			// 
			this.btnFileSave.AppearanceKey = "acceptButton";
			this.btnFileSave.Location = new System.Drawing.Point(503, 30);
			this.btnFileSave.Name = "btnFileSave";
			this.btnFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileSave.Size = new System.Drawing.Size(73, 48);
			this.btnFileSave.TabIndex = 0;
			this.btnFileSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.btnFileSave, null);
			this.btnFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// btnFileSearch
			// 
			this.btnFileSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileSearch.AppearanceKey = "toolbarButton";
			this.btnFileSearch.ImageSource = "button-search";
			this.btnFileSearch.Location = new System.Drawing.Point(968, 29);
			this.btnFileSearch.Name = "btnFileSearch";
			this.btnFileSearch.Shortcut = Wisej.Web.Shortcut.F5;
			this.btnFileSearch.Size = new System.Drawing.Size(81, 24);
			this.btnFileSearch.TabIndex = 1;
			this.btnFileSearch.Text = "Search";
			this.ToolTip1.SetToolTip(this.btnFileSearch, null);
			this.btnFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// btnFileDelete
			// 
			this.btnFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileDelete.AppearanceKey = "toolbarButton";
			this.btnFileDelete.Location = new System.Drawing.Point(911, 29);
			this.btnFileDelete.Name = "btnFileDelete";
			this.btnFileDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.btnFileDelete.Size = new System.Drawing.Size(53, 24);
			this.btnFileDelete.TabIndex = 2;
			this.btnFileDelete.Text = "Delete";
			this.ToolTip1.SetToolTip(this.btnFileDelete, null);
			this.btnFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// btnFileNew
			// 
			this.btnFileNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileNew.AppearanceKey = "toolbarButton";
			this.btnFileNew.Location = new System.Drawing.Point(864, 29);
			this.btnFileNew.Name = "btnFileNew";
			this.btnFileNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.btnFileNew.Size = new System.Drawing.Size(42, 24);
			this.btnFileNew.TabIndex = 3;
			this.btnFileNew.Text = "New";
			this.ToolTip1.SetToolTip(this.btnFileNew, null);
			this.btnFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
			// 
			// btnFileAdd
			// 
			this.btnFileAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileAdd.AppearanceKey = "toolbarButton";
			this.btnFileAdd.Location = new System.Drawing.Point(760, 29);
			this.btnFileAdd.Name = "btnFileAdd";
			this.btnFileAdd.Shortcut = Wisej.Web.Shortcut.CtrlA;
			this.btnFileAdd.Size = new System.Drawing.Size(99, 24);
			this.btnFileAdd.TabIndex = 4;
			this.btnFileAdd.Text = "Add Rate Key";
			this.ToolTip1.SetToolTip(this.btnFileAdd, null);
			this.btnFileAdd.Click += new System.EventHandler(this.btnFileAdd_Click);
			// 
			// frmLoadBack
			// 
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLoadBack";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Manual Bills";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmLoadBack_Load);
			this.Activated += new System.EventHandler(this.frmLoadBack_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLoadBack_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLoadBack_KeyPress);
			this.Resize += new System.EventHandler(this.frmLoadBack_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).EndInit();
			this.fraAccountSearch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountInfo)).EndInit();
			this.fraAccountInfo.ResumeLayout(false);
			this.fraAccountInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccounts)).EndInit();
			this.fraAccounts.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileAdd)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		internal FCButton btnFileSearch;
		internal FCButton btnFileSave;
		internal FCButton btnFileDelete;
		internal FCButton btnFileNew;
		internal FCButton btnFileAdd;
	}
}
