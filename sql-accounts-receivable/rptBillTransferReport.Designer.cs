﻿namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptBillTransferReport.
	/// </summary>
	partial class rptBillTransferReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillTransferReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRegular = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPastDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldAcctNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOverride = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFooter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverride)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcctNum,
				this.fldName,
				this.fldAmount,
				this.fldInvoice,
				this.fldRegular,
				this.fldTax,
				this.fldPastDue,
				this.fldInterest
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldFooter,
				this.fldTotalAmount,
				this.Line2,
				this.fldTotalRegular,
				this.fldTotalTax,
				this.fldTotalPastDue,
				this.fldTotalInterest
			});
			this.ReportFooter.Height = 0.5729167F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.lblReportType,
				this.lblAccount,
				this.lblName,
				this.lblRegular,
				this.lblAmount,
				this.lblBook,
				this.lblTax,
				this.lblInterest,
				this.lblPastDue,
				this.Line1
			});
			this.PageHeader.Height = 0.8645833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblOverride
			});
			this.PageFooter.Height = 0.1770833F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Created Bills Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 10F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.5625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 8.0625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.9375F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 8.0625F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.9375F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.3125F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblReportType.Text = null;
			this.lblReportType.Top = 0.25F;
			this.lblReportType.Width = 10F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 6.21875F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.65625F;
			this.lblAccount.Width = 0.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 7.3125F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.65625F;
			this.lblName.Width = 1.6875F;
			// 
			// lblRegular
			// 
			this.lblRegular.Height = 0.1875F;
			this.lblRegular.HyperLink = null;
			this.lblRegular.Left = 1.1875F;
			this.lblRegular.Name = "lblRegular";
			this.lblRegular.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblRegular.Text = "Regular";
			this.lblRegular.Top = 0.65625F;
			this.lblRegular.Width = 0.8125F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 4.8125F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.65625F;
			this.lblAmount.Width = 1.1875F;
			// 
			// lblBook
			// 
			this.lblBook.Height = 0.1875F;
			this.lblBook.HyperLink = null;
			this.lblBook.Left = 0F;
			this.lblBook.Name = "lblBook";
			this.lblBook.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblBook.Text = "Invoice #";
			this.lblBook.Top = 0.65625F;
			this.lblBook.Width = 1.03125F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 2.125F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.65625F;
			this.lblTax.Width = 0.6875F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 3.9375F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0.65625F;
			this.lblInterest.Width = 0.75F;
			// 
			// lblPastDue
			// 
			this.lblPastDue.Height = 0.1875F;
			this.lblPastDue.HyperLink = null;
			this.lblPastDue.Left = 2.8125F;
			this.lblPastDue.Name = "lblPastDue";
			this.lblPastDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblPastDue.Text = "Past Due";
			this.lblPastDue.Top = 0.65625F;
			this.lblPastDue.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.84375F;
			this.Line1.Width = 10F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10F;
			this.Line1.Y1 = 0.84375F;
			this.Line1.Y2 = 0.84375F;
			// 
			// fldAcctNum
			// 
			this.fldAcctNum.Height = 0.1875F;
			this.fldAcctNum.Left = 6.21875F;
			this.fldAcctNum.Name = "fldAcctNum";
			this.fldAcctNum.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAcctNum.Text = null;
			this.fldAcctNum.Top = 0F;
			this.fldAcctNum.Width = 0.625F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 7.3125F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 1.6875F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 4.8125F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.1875F;
			// 
			// fldInvoice
			// 
			this.fldInvoice.Height = 0.1875F;
			this.fldInvoice.Left = 0F;
			this.fldInvoice.Name = "fldInvoice";
			this.fldInvoice.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldInvoice.Text = "080010001";
			this.fldInvoice.Top = 0F;
			this.fldInvoice.Width = 1.03125F;
			// 
			// fldRegular
			// 
			this.fldRegular.Height = 0.1875F;
			this.fldRegular.Left = 1.1875F;
			this.fldRegular.Name = "fldRegular";
			this.fldRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldRegular.Text = null;
			this.fldRegular.Top = 0F;
			this.fldRegular.Width = 0.8125F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 2.125F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 0.6875F;
			// 
			// fldPastDue
			// 
			this.fldPastDue.Height = 0.1875F;
			this.fldPastDue.Left = 2.8125F;
			this.fldPastDue.Name = "fldPastDue";
			this.fldPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldPastDue.Text = null;
			this.fldPastDue.Top = 0F;
			this.fldPastDue.Width = 1F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 3.9375F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 0.75F;
			// 
			// lblOverride
			// 
			this.lblOverride.Height = 0.1875F;
			this.lblOverride.HyperLink = null;
			this.lblOverride.Left = 8.875F;
			this.lblOverride.Name = "lblOverride";
			this.lblOverride.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.lblOverride.Text = "* = Prorated";
			this.lblOverride.Top = 0F;
			this.lblOverride.Width = 1.125F;
			// 
			// fldFooter
			// 
			this.fldFooter.Height = 0.1875F;
			this.fldFooter.Left = 0F;
			this.fldFooter.Name = "fldFooter";
			this.fldFooter.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.fldFooter.Text = null;
			this.fldFooter.Top = 0.25F;
			this.fldFooter.Width = 1.0625F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 4.6875F;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalAmount.Text = null;
			this.fldTotalAmount.Top = 0.25F;
			this.fldTotalAmount.Width = 1.3125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.9375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.125F;
			this.Line2.Width = 5.0625F;
			this.Line2.X1 = 0.9375F;
			this.Line2.X2 = 6F;
			this.Line2.Y1 = 0.125F;
			this.Line2.Y2 = 0.125F;
			// 
			// fldTotalRegular
			// 
			this.fldTotalRegular.Height = 0.1875F;
			this.fldTotalRegular.Left = 1.0625F;
			this.fldTotalRegular.Name = "fldTotalRegular";
			this.fldTotalRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalRegular.Text = null;
			this.fldTotalRegular.Top = 0.25F;
			this.fldTotalRegular.Width = 0.9375F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 2F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalTax.Text = null;
			this.fldTotalTax.Top = 0.25F;
			this.fldTotalTax.Width = 0.8125F;
			// 
			// fldTotalPastDue
			// 
			this.fldTotalPastDue.Height = 0.1875F;
			this.fldTotalPastDue.Left = 2.8125F;
			this.fldTotalPastDue.Name = "fldTotalPastDue";
			this.fldTotalPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalPastDue.Text = null;
			this.fldTotalPastDue.Top = 0.25F;
			this.fldTotalPastDue.Width = 1F;
			// 
			// fldTotalInterest
			// 
			this.fldTotalInterest.Height = 0.1875F;
			this.fldTotalInterest.Left = 3.8125F;
			this.fldTotalInterest.Name = "fldTotalInterest";
			this.fldTotalInterest.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalInterest.Text = null;
			this.fldTotalInterest.Top = 0.25F;
			this.fldTotalInterest.Width = 0.875F;
			// 
			// rptBillTransferReport
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.01042F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverride)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcctNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRegular;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOverride;
	}
}
