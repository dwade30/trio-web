﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomerLabels.
	/// </summary>
	public partial class frmCustomerLabels : BaseForm
	{
		public frmCustomerLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomerLabels InstancePtr
		{
			get
			{
				return (frmCustomerLabels)Sys.GetInstance(typeof(frmCustomerLabels));
			}
		}

		protected frmCustomerLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		string strBillTypeSQL;
		int SelectCol;
		int CodeCol;
		int DescriptionCol;
		clsPrintLabel labLabelTypes = new clsPrintLabel();

		private void cboLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intIndex;
			intIndex = labLabelTypes.Get_IndexFromID(cboLabelType.ItemData(cboLabelType.SelectedIndex));
			lblLabelDescription.Text = labLabelTypes.Get_Description(intIndex);
		}

		private void frmCustomerLabels_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCustomerLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomerLabels.FillStyle	= 0;
			//frmCustomerLabels.ScaleWidth	= 9045;
			//frmCustomerLabels.ScaleHeight	= 6930;
			//frmCustomerLabels.LinkTopic	= "Form2";
			//frmCustomerLabels.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsBillTypeInfo = new clsDRWrapper();
			SelectCol = 0;
			CodeCol = 1;
			DescriptionCol = 2;
			vsBillTypes.ColWidth(SelectCol, FCConvert.ToInt32(vsBillTypes.WidthOriginal * 0.2));
			vsBillTypes.ColWidth(DescriptionCol, FCConvert.ToInt32(vsBillTypes.WidthOriginal * 0.75));
			vsBillTypes.TextMatrix(0, SelectCol, "Select");
			vsBillTypes.TextMatrix(0, DescriptionCol, "Bill Type");
			vsBillTypes.ColHidden(CodeCol, true);
			vsBillTypes.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			rsBillTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE rTrim(TypeTitle) <> '' ORDER BY TypeTitle");
			if (rsBillTypeInfo.EndOfFile() != true && rsBillTypeInfo.BeginningOfFile() != true)
			{
				do
				{
					vsBillTypes.AddItem(false + "\t" + Strings.Format(rsBillTypeInfo.Get_Fields_Int32("TypeCode"), "00") + "\t" + rsBillTypeInfo.Get_Fields_String("TypeTitle"));
					rsBillTypeInfo.MoveNext();
				}
				while (rsBillTypeInfo.EndOfFile() != true);
			}
			FillLabelTypeCombo();
			cboLabelType.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			cmbAll.SelectedIndex = 0;
			cmbComplete.SelectedIndex = 0;
			cmbNumber.SelectedIndex = 0;
		}

		private void frmCustomerLabels_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnBillTypesSelected = false;
			string strSQL = "";
			clsDRWrapper rsInfo = new clsDRWrapper();
			string strPrinter = "";
			int NumFonts = 0;
			bool boolUseFont = false;
			int intCPI = 0;
			int x;
			string strFont = "";
			if (cmbComplete.SelectedIndex == 1)
			{
				if (cboStart.SelectedIndex > cboEnd.SelectedIndex)
				{
					MessageBox.Show("This is not a valid range of vendors.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbAll.SelectedIndex == 1)
			{
				blnBillTypesSelected = false;
				for (counter = 1; counter <= vsBillTypes.Rows - 1; counter++)
				{
					if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(counter, SelectCol)) == true)
					{
						blnBillTypesSelected = true;
						break;
					}
				}
				if (!blnBillTypesSelected)
				{
					MessageBox.Show("You must select at least one bill type or choose the all option for bill types before you may continue", "Invalid Bill Type Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			strBillTypeSQL = "";
			if (cmbAll.SelectedIndex == 1)
			{
				CreateSQL();
			}
			if (strBillTypeSQL != "")
			{
				if (cmbComplete.SelectedIndex == 0)
				{
					strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, PartyID FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL;
				}
				else
				{
					if (cmbNumber.SelectedIndex == 1)
					{
						strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, PartyID FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL + " AND CustomerMaster.CustomerID >= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmCustomerLabels.InstancePtr.cboStart.Text, 4))) + " AND CustomerMaster.CustomerID <= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmCustomerLabels.InstancePtr.cboEnd.Text, 4)));
					}
					else
					{
						strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, PartyID FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL;
					}
				}
			}
			else
			{
				if (cmbComplete.SelectedIndex == 0)
				{
					strSQL = "SELECT DISTINCT CustomerID, PartyID FROM CustomerMaster";
				}
				else
				{
					if (cmbNumber.SelectedIndex == 1)
					{
						strSQL = "SELECT DISTINCT CustomerID, PartyID FROM CustomerMaster WHERE CustomerID >= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmCustomerLabels.InstancePtr.cboStart.Text, 4))) + " AND CustomerID <= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmCustomerLabels.InstancePtr.cboEnd.Text, 4)));
					}
					else
					{
						strSQL = "SELECT DISTINCT CustomerID, PartyID FROM CustomerMaster";
					}
				}
			}
			rsInfo.OpenRecordset(strSQL);
			if (cmbComplete.SelectedIndex != 0 && cmbNumber.SelectedIndex != 1)
			{
				rsInfo.InsertName("PartyID", "", false, true, false, string.Empty, false, string.Empty, false, "FullName >= '" + Strings.Trim(Strings.Right(cboStart.Text, cboStart.Text.Length - 4)) + "' AND FullName <= '" + Strings.Trim(Strings.Right(cboEnd.Text, cboEnd.Text.Length - 4)) + "'");
			}
			rsInfo.MoveNext();
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				strSQL = "SELECT * FROM CustomerMaster WHERE CustomerID IN (";
				do
				{
					strSQL += rsInfo.Get_Fields_Int32("CustomerID") + ", ";
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ")";
			}
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				
				boolUseFont = false;
				
				// SHOW THE REPORT
				if (!boolUseFont)
					strFont = "";
				this.Hide();
				rptCustomLabels.InstancePtr.Init(strSQL, "CUSTOMERS", FCConvert.ToInt16(cboLabelType.ItemData(cboLabelType.SelectedIndex)), ref strPrinter, strFont);
				Close();
			}
			else
			{
				MessageBox.Show("No customers were found that matched the criteria you selected.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAll.SelectedIndex == 0)
			{
				int counter;
				for (counter = 1; counter <= vsBillTypes.Rows - 1; counter++)
				{
					vsBillTypes.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
				}
				fraSpecificBillTypes.Enabled = false;
			}
			else
			{
				fraSpecificBillTypes.Enabled = true;
			}
		}

		private void optComplete_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbComplete.SelectedIndex == 0)
			{
				cboStart.Clear();
				cboEnd.Clear();
				fraRange.Visible = false;
				//fraClassCodes.Top = 320;
			}
			else
			{
				FillCustomerRangeCombos();
				fraRange.Visible = true;
				cboStart.Focus();
				//fraClassCodes.Top = 490;
			}
		}

		private void FillCustomerRangeCombos()
		{
			cboStart.Clear();
			cboEnd.Clear();
            clsDRWrapper rsCustomerInfo = new clsDRWrapper();

            try
            {
                if (cmbNumber.SelectedIndex == 1)
                {
                    rsCustomerInfo.OpenRecordset("SELECT * FROM CustomerMaster WHERE Status <> 'D' ORDER BY CustomerID");
                    rsCustomerInfo.InsertName("PartyID", "", false, true);
                }
                else
                {
                    rsCustomerInfo.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsCustomerInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.PartyID) as p ORDER BY p.FullName");
                }

                if (rsCustomerInfo.EndOfFile() || rsCustomerInfo.BeginningOfFile()) 
                    return;

                do
                {
                    var customerID = modValidateAccount.GetFormat_6(FCConvert.ToString(rsCustomerInfo.Get_Fields_Int32("CustomerID")), 4);
                    var fullName = Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("FullName")));
                    cboStart.AddItem(customerID + "  " + fullName);
                    cboEnd.AddItem(customerID + "  " + fullName);
                    rsCustomerInfo.MoveNext();
                }
                while (rsCustomerInfo.EndOfFile() != true);
                cboStart.SelectedIndex = 0;
                cboEnd.SelectedIndex = 0;
            }
            finally
            {
                rsCustomerInfo.DisposeOf();
            }
		}

		private void optNumber_CheckedChanged(object sender, System.EventArgs e)
		{
            if (cmbComplete.SelectedIndex != 1) return;

            FillCustomerRangeCombos();
            cboStart.Focus();
        }

		private void FillLabelTypeCombo()
		{
			int counter;
			// hide labels that are not supported by BD because of the 5 lines I need to print for an address
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
                if (labLabelTypes.Get_ID(counter) != modLabels.CNSTLBLTYPE5066) 
                    continue;

                labLabelTypes.Set_Visible(counter, false);
                break;
            }
			// fill combo box with all available types of labels
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_Visible(counter))
				{
					cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
					cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
				}
				else
				{
					if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30320 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30252 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE4065)
					{
						cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
						cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
					}
				}
			}
		}

		private void CreateSQL()
		{
			int counter;
			strBillTypeSQL = "(";
			for (counter = 1; counter <= vsBillTypes.Rows - 1; counter++)
			{
				if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(counter, SelectCol)) == true)
				{
					strBillTypeSQL += "Type = " + vsBillTypes.TextMatrix(counter, CodeCol) + " OR ";
				}
			}
			strBillTypeSQL = Strings.Left(strBillTypeSQL, strBillTypeSQL.Length - 4) + ")";
		}

		private void vsBillTypes_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsBillTypes.Row > 0)
			{
				if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol)) == true)
				{
					vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol, FCConvert.ToString(false));
				}
				else
				{
					vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsBillTypes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Space)
			{
				if (vsBillTypes.Row > 0)
				{
					if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol)) == true)
					{
						vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol, FCConvert.ToString(false));
					}
					else
					{
						vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol, FCConvert.ToString(true));
					}
				}
			}
		}
	}
}
