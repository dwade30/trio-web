//Fecher vbPorter - Version 1.0.0.62

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWAR0000
{
	public class cSimpleImportItem
	{
        public cSimpleImportItem() : base()
        {
            AccountNumber = 0;
            // AccountID = 0
            Name = "";
            Description = "";
            short x;
            for (x = 1; x <= 6; x++)
            {
                dblAmount[x] = 0;
            }
        }
		void InitializeComponentEx()
		{
		}


		//=========================================================

		private int lngAccountNumber;
		// Private lngAccountID As Long
		private string strName = string.Empty;
		private string strDescription = string.Empty;
		private double []dblAmount = new double[6 + 1];

		public int AccountNumber
		{
			set
			{
				lngAccountNumber = value;
			}

			get
			{
					int AccountNumber = 0;
				AccountNumber = lngAccountNumber;
				return AccountNumber;
			}
		}



		// Public Property Let AccountID(ByVal lngID As Long)
		// lngAccountID = lngID
		// End Property
		// 
		// Public Property Get AccountID() As Long
		// AccountID = lngAccountID
		// End Property

		public string Name
		{
			set
			{
				strName = value;
			}

			get
			{
					string Name = "";
				Name = strName;
				return Name;
			}
		}



		public string Description
		{
			set
			{
				strDescription = value;
			}

			get
			{
					string Description = "";
				Description = strDescription;
				return Description;
			}
		}



		public double GetAmount(short intNum)
		{
			double GetAmount = 0;
			if (intNum>=Information.LBound(dblAmount) && intNum<=Information.UBound(dblAmount)) {
				GetAmount = dblAmount[intNum];
			}
			return GetAmount;
		}

		public void SetAmount(short intNum, double dblValue)
		{
			if (intNum>=Information.LBound(dblAmount) && intNum<=Information.UBound(dblAmount)) {
				dblAmount[intNum] = dblValue;
			}
		}

	}

}

