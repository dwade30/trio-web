﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomerBills.
	/// </summary>
	public partial class frmCustomerBills : BaseForm
	{
		public frmCustomerBills()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtTitle = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.lblTitle = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.txtTitle.AddControlArrayElement(txtTitle_2, 2);
			this.txtTitle.AddControlArrayElement(txtTitle_1, 1);
			this.txtTitle.AddControlArrayElement(txtTitle_0, 0);
			this.txtTitle.AddControlArrayElement(txtTitle_3, 3);
			this.lblTitle.AddControlArrayElement(lblTitle_2, 2);
			this.lblTitle.AddControlArrayElement(lblTitle_1, 1);
			this.lblTitle.AddControlArrayElement(lblTitle_0, 0);
			this.lblTitle.AddControlArrayElement(lblTitle_3, 3);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomerBills InstancePtr
		{
			get
			{
				return (frmCustomerBills)Sys.GetInstance(typeof(frmCustomerBills));
			}
		}

		protected frmCustomerBills _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/08/2004              *
		// ********************************************************
		bool boolLoaded;
		// True if form has been loaded already, False otherwise
		bool boolLoad;
		// True if this form should load
		bool boolQuit;
		int lngCurType;
		public int lngCustomerID;
		public int lngPartyID;
		int[] lngBillID = null;
		int intNumberOfBills;
		bool blnDirty;
		bool blnMove;

		private void StartUp()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			// this runs the Input Routine
			// format the grid
			FormatGrid();
			// fill the main labels
			FillMainLabels();
			mnuFileMovePrevious.Enabled = false;
			rsInfo.OpenRecordset("SELECT * FROM CustomerBills WHERE CustomerID = " + FCConvert.ToString(lngCustomerID));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				LoadBills();
				cboBill.SelectedIndex = 0;
				if (cboBill.Items.Count == 1)
				{
					mnuFileMoveNext.Enabled = false;
				}
				else
				{
					mnuFileMoveNext.Enabled = true;
				}
			}
			else
			{
				mnuFileMoveNext.Enabled = false;
				intNumberOfBills = 0;
				CreateNewBill();
			}
		}

		private void CreateNewBill()
		{
			intNumberOfBills += 1;
			cboBill.AddItem(intNumberOfBills.ToString());
			Array.Resize(ref lngBillID, intNumberOfBills - 1 + 1);
			lngBillID[intNumberOfBills - 1] = 0;
			cboBill.SelectedIndex = cboBill.Items.Count - 1;
			if (cboType.Visible)
			{
				cboType.Focus();
			}
			ClearBillInformation();
			blnDirty = false;
			if (intNumberOfBills > 1)
			{
				mnuFileMovePrevious.Enabled = true;
			}
			else
			{
				mnuFileMovePrevious.Enabled = false;
			}
			mnuFileMoveNext.Enabled = false;
		}

		private void FillMainLabels()
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			pInfo = pCont.GetParty(FCConvert.ToInt32(frmCustomerMaster.InstancePtr.txtCustomerNumber.Text));
			// this sub will fill all of the Labels and Textboxes above the grid on the form
			HeaderText.Text = "Customer: " + frmCustomerMaster.InstancePtr.txtCustomerNumber.Text + "  -  " + pInfo.FullName;
			// loads the combo information for the Type combobox
			FillComboType();
		}

		private void LoadBills()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			intNumberOfBills = 0;
			rsInfo.OpenRecordset("SELECT * FROM CustomerBills WHERE CustomerID = " + FCConvert.ToString(lngCustomerID) + " ORDER BY ID");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					intNumberOfBills += 1;
					cboBill.AddItem(intNumberOfBills.ToString());
					Array.Resize(ref lngBillID, intNumberOfBills - 1 + 1);
					lngBillID[intNumberOfBills - 1] = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void cboBill_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("Any changes you have made to this bill will be lost if you select another bill.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					LoadBillInfo();
					if (cboBill.SelectedIndex == cboBill.Items.Count - 1)
					{
						mnuFileMoveNext.Enabled = false;
					}
					else
					{
						mnuFileMoveNext.Enabled = true;
					}
					if (cboBill.SelectedIndex == 0)
					{
						mnuFileMovePrevious.Enabled = false;
					}
					else
					{
						mnuFileMovePrevious.Enabled = true;
					}
				}
			}
			else
			{
				LoadBillInfo();
				if (cboBill.SelectedIndex == cboBill.Items.Count - 1)
				{
					mnuFileMoveNext.Enabled = false;
				}
				else
				{
					mnuFileMoveNext.Enabled = true;
				}
				if (cboBill.SelectedIndex == 0)
				{
					mnuFileMovePrevious.Enabled = false;
				}
				else
				{
					mnuFileMovePrevious.Enabled = true;
				}
			}
		}

		private void LoadBillInfo()
		{
			clsDRWrapper rsBillInfo = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			rsBillInfo.OpenRecordset("SELECT * FROM CustomerBills WHERE ID = " + FCConvert.ToString(lngBillID[cboBill.SelectedIndex]));
			if (rsBillInfo.EndOfFile() != true && rsBillInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (!modGlobal.ValidateBillType_2(FCConvert.ToInt32(rsBillInfo.Get_Fields("Type"))))
				{
					ClearBillInformation();
					return;
				}
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				SetBillTypeCombo_2(FCConvert.ToInt32(rsBillInfo.Get_Fields("Type")));
				// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
				rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsBillInfo.Get_Fields("Type"));
				ProcessType();
				txtTitle[0].Text = FCConvert.ToString(rsBillInfo.Get_Fields_String("Reference"));
				txtTitle[1].Text = FCConvert.ToString(rsBillInfo.Get_Fields_String("Control1"));
				txtTitle[2].Text = FCConvert.ToString(rsBillInfo.Get_Fields_String("Control2"));
				txtTitle[3].Text = FCConvert.ToString(rsBillInfo.Get_Fields_String("Control3"));
				if (txtStartDate.Enabled == true)
				{
					if (Information.IsDate(rsBillInfo.Get_Fields("StartDate")))
					{
						txtStartDate.Text = Strings.Format(rsBillInfo.Get_Fields_DateTime("StartDate"), "MM/dd/yyyy");
					}
					else
					{
						txtStartDate.Text = "";
					}
				}
				if (chkProrate.Enabled == true)
				{
					if (FCConvert.ToBoolean(rsBillInfo.Get_Fields_Boolean("Prorate")))
					{
						chkProrate.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkProrate.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				txtComment.Text = FCConvert.ToString(rsBillInfo.Get_Fields_String("Comment"));
				// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsBillInfo.Get_Fields("Amount1")) >= 0)
				{
					// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					if (rsBillInfo.Get_Fields("Amount1") != FCConvert.ToDecimal(vsFees.TextMatrix(1, 2)))
					{
						// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
						vsFees.TextMatrix(1, 2, Strings.Format(rsBillInfo.Get_Fields("Amount1"), "#,##0.00"));
					}
				}
				else
				{
					vsFees.TextMatrix(1, 2, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount1"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsBillInfo.Get_Fields("Amount2")) >= 0)
				{
					// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					if (rsBillInfo.Get_Fields("Amount2") != FCConvert.ToDecimal(vsFees.TextMatrix(2, 2)))
					{
						// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
						vsFees.TextMatrix(2, 2, Strings.Format(rsBillInfo.Get_Fields("Amount2"), "#,##0.00"));
					}
				}
				else
				{
					vsFees.TextMatrix(2, 2, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount2"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsBillInfo.Get_Fields("Amount3")) >= 0)
				{
					// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
					if (rsBillInfo.Get_Fields("Amount3") != FCConvert.ToDecimal(vsFees.TextMatrix(3, 2)))
					{
						// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
						vsFees.TextMatrix(3, 2, Strings.Format(rsBillInfo.Get_Fields("Amount3"), "#,##0.00"));
					}
				}
				else
				{
					vsFees.TextMatrix(3, 2, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount3"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsBillInfo.Get_Fields("Amount4")) >= 0)
				{
					// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
					if (rsBillInfo.Get_Fields("Amount4") != FCConvert.ToDecimal(vsFees.TextMatrix(4, 2)))
					{
						// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
						vsFees.TextMatrix(4, 2, Strings.Format(rsBillInfo.Get_Fields("Amount4"), "#,##0.00"));
					}
				}
				else
				{
					vsFees.TextMatrix(4, 2, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount4"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsBillInfo.Get_Fields("Amount5")) >= 0)
				{
					// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
					if (rsBillInfo.Get_Fields("Amount5") != FCConvert.ToDecimal(vsFees.TextMatrix(5, 2)))
					{
						// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
						vsFees.TextMatrix(5, 2, Strings.Format(rsBillInfo.Get_Fields("Amount5"), "#,##0.00"));
					}
				}
				else
				{
					vsFees.TextMatrix(5, 2, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount5"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsBillInfo.Get_Fields("Amount6")) >= 0)
				{
					// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
					if (rsBillInfo.Get_Fields("Amount6") != FCConvert.ToDecimal(vsFees.TextMatrix(6, 2)))
					{
						// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
						vsFees.TextMatrix(6, 2, Strings.Format(rsBillInfo.Get_Fields("Amount6"), "#,##0.00"));
					}
				}
				else
				{
					vsFees.TextMatrix(6, 2, Strings.Format(rsDefaultInfo.Get_Fields_Double("DefaultAmount6"), "#,##0.00"));
				}
			}
			else
			{
				ClearBillInformation();
			}
			blnDirty = false;
		}

		private void SetBillTypeCombo_2(int lngSearch)
		{
			SetBillTypeCombo(ref lngSearch);
		}

		private void SetBillTypeCombo(ref int lngSearch)
		{
			int counter;
			for (counter = 0; counter <= cboType.Items.Count - 1; counter++)
			{
				if (Conversion.Val(Strings.Left(cboType.Items[counter].ToString(), 3)) == lngSearch)
				{
					cboType.SelectedIndex = counter;
					break;
				}
			}
		}

		private void cboType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void chkPrint_Click()
		{
			blnDirty = true;
		}

		private void cmbType_ChangeEdit()
		{
			blnDirty = true;
		}

		private void cboType_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboType.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void frmCustomerBills_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
			cboBill.Focus();
			// vsFees.AutoSize 0, vsFees.Cols - 1
		}

		private void frmCustomerBills_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			bool boolFound;
			int I;
			string strTemp = "";
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						Close();
						frmCustomerMaster.InstancePtr.Unload();
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						// If intCurFrame = 1 Then
						// txtFirst.SetFocus
						// End If
						// mnuProcessReceipt_Click
						break;
					}
			}
			//end switch
		}

		private void frmCustomerBills_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// return
				KeyAscii = (Keys)0;
				Support.SendKeys("{Tab}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCustomerBills_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomerBills.ScaleWidth	= 9045;
			//frmCustomerBills.ScaleHeight	= 7320;
			//frmCustomerBills.LinkTopic	= "Form1";
			//End Unmaped Properties
			StartUp();
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			SetCustomFormColors();
			//FC:FINAL:SBE - #286 - set allowed keys for client side check. KeyPress on server side does not support key restrictions
			vsFees.ColAllowedKeys(2, "'Left','Right','Delete','Home','End','Insert',8,9,13,48..57,'.'");
		}

		private void frmCustomerBills_Resize(object sender, System.EventArgs e)
		{
			vsFees.ColWidth(0, 0);
			// Category Number
			vsFees.ColWidth(1, FCConvert.ToInt32(vsFees.WidthOriginal * 0.61));
			// Title
			vsFees.ColWidth(2, FCConvert.ToInt32(vsFees.WidthOriginal * 0.30));
            // Amount
            //FC:FINAL:BSE #2130 align header to match text alignment
            vsFees.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsFees.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void mnuFileAdd_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("Any changes you have made to this bill will be lost if you select another bill.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					CreateNewBill();
				}
			}
			else
			{
				CreateNewBill();
			}
		}

		private void mnuFileCustomer_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			//FC:FINAL:PJ: Fix CS0019
			int ans;
			clsDRWrapper rsDelete = new clsDRWrapper();
			//FC:FINAL:PJ: Fix CS0019
			DialogResult dlgResult;
			dlgResult = MessageBox.Show("Are you sure you wish to delete this bill?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dlgResult == DialogResult.Yes)
			{
				if (lngBillID[cboBill.SelectedIndex] != 0)
				{
					rsDelete.Execute("DELETE FROM CustomerBills WHERE ID = " + FCConvert.ToString(lngBillID[cboBill.SelectedIndex]), "TWAR0000.vb1");
				}
				for (ans = cboBill.SelectedIndex + 1; ans <= cboBill.Items.Count - 1; ans++)
				{
					lngBillID[ans - 1] = lngBillID[ans];
				}
				intNumberOfBills -= 1;
				if (intNumberOfBills == 0)
				{
					lngBillID = new int[0 + 1];
					cboBill.Clear();
					lngBillID[0] = 0;
					CreateNewBill();
				}
				else
				{
					Array.Resize(ref lngBillID, intNumberOfBills - 1 + 1);
					cboBill.Items.RemoveAt(cboBill.SelectedIndex);
					cboBill.Clear();
					for (ans = 1; ans <= intNumberOfBills; ans++)
					{
						cboBill.AddItem(ans.ToString());
					}
					cboBill.SelectedIndex = 0;
					LoadBillInfo();
					if (intNumberOfBills > 1)
					{
						mnuFileMoveNext.Enabled = true;
					}
					else
					{
						mnuFileMoveNext.Enabled = false;
					}
					mnuFileMovePrevious.Enabled = false;
				}
			}
		}

		private void mnuFileMoveNext_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("Any changes you have made to this bill will be lost if you move to another bill.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					cboBill.SelectedIndex = cboBill.SelectedIndex + 1;
					if (cboBill.SelectedIndex == cboBill.Items.Count - 1)
					{
						mnuFileMoveNext.Enabled = false;
					}
					mnuFileMovePrevious.Enabled = true;
					LoadBillInfo();
				}
			}
			else
			{
				cboBill.SelectedIndex = cboBill.SelectedIndex + 1;
				if (cboBill.SelectedIndex == cboBill.Items.Count - 1)
				{
					mnuFileMoveNext.Enabled = false;
				}
				mnuFileMovePrevious.Enabled = true;
				LoadBillInfo();
			}
		}

		public void mnuFileMoveNext_Click()
		{
			mnuFileMoveNext_Click(mnuFileMoveNext, new System.EventArgs());
		}

		private void mnuFileMovePrevious_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("Any changes you have made to this bill will be lost if you move to another bill.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					cboBill.SelectedIndex = cboBill.SelectedIndex - 1;
					if (cboBill.SelectedIndex == 0)
					{
						mnuFileMovePrevious.Enabled = false;
					}
					mnuFileMoveNext.Enabled = true;
					LoadBillInfo();
				}
			}
			else
			{
				cboBill.SelectedIndex = cboBill.SelectedIndex - 1;
				if (cboBill.SelectedIndex == 0)
				{
					mnuFileMovePrevious.Enabled = false;
				}
				mnuFileMoveNext.Enabled = true;
				LoadBillInfo();
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnMove = false;
			SaveInfo();
		}

		private void mnuFileSaveAndContinue_Click(object sender, System.EventArgs e)
		{
			blnMove = true;
			SaveInfo();
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			Close();
			// this will call the unload event
			frmCustomerMaster.InstancePtr.Unload();
		}

		private void txtComment_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtComment_Enter(object sender, System.EventArgs e)
		{
			txtComment.SelectionStart = 0;
			txtComment.SelectionLength = txtComment.Text.Length;
		}

		private void FillComboType()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill all of the types except for the ones w/o titles into cmbType
				int Number;
				string Name = "";
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				int intCT;
				rsTemp.DefaultDB = "TWAR0000.vb1";
				cboType.Clear();
				// type
				rsTemp.OpenRecordset("SELECT * FROM DefaultBillTypes ORDER BY TypeCode");
				if (rsTemp.BeginningOfFile() != true && rsTemp.EndOfFile() != true)
				{
					rsTemp.MoveFirst();
					do
					{
						cboType.AddItem(Strings.Format(rsTemp.Get_Fields_Int32("TypeCode"), "00") + "  -  " + rsTemp.Get_Fields_String("TypeTitle"));
						rsTemp.MoveNext();
					}
					while (!rsTemp.EndOfFile());
				}
				else
				{
					MessageBox.Show("No types were loaded into the combobox, please go to 'Receipt Type Setup' and create one.", "No Receipt Types Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatGrid()
		{
			// sets all of the widths, datatypes and titles for the grid
			int Width = 0;
			vsFees.Cols = 3;
			vsFees.EditMaxLength = 12;
			Width = vsFees.WidthOriginal;
			vsFees.ColWidth(0, 0);
			// Category Number
			vsFees.ColWidth(1, FCConvert.ToInt32(Width * 0.61));
			// Title
			vsFees.ColWidth(2, FCConvert.ToInt32(Width * 0.30));
			// Amount
			// .ColFormat(2) = "#,##0.00"
			vsFees.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//vsFees.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// sets the titles for the flexgrid
			vsFees.TextMatrix(0, 0, "Fee");
			vsFees.TextMatrix(0, 1, "Title");
			vsFees.TextMatrix(0, 2, "Unit Cost");
			vsFees.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2, true);
		}

		private void txtStartDate_Change(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtTitle_TextChanged(int Index, object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtTitle_TextChanged(object sender, System.EventArgs e)
		{
			int index = txtTitle.IndexOf((FCTextBox)sender);
			txtTitle_TextChanged(index, sender, e);
		}

		private void txtTitle_Enter(int Index, object sender, System.EventArgs e)
		{
			txtTitle[Index].SelectionStart = 0;
			txtTitle[Index].SelectionLength = txtTitle[Index].Text.Length;
		}

		private void txtTitle_Enter(object sender, System.EventArgs e)
		{
			int index = txtTitle.IndexOf((FCTextBox)sender);
			txtTitle_Enter(index, sender, e);
		}

		private void txtTitle_KeyDown(int Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int intTotBoxes;
			int intNextBox = 0;
			bool boolBad = false;
			intTotBoxes = 4;
			switch (KeyCode)
			{
				case Keys.Right:
					{
						if (txtTitle[Index].SelectionStart == txtTitle[Index].Text.Length)
						{
							KeyCode = (Keys)0;
							txtTitle_Validate(Index, ref boolBad);
							if (boolBad)
							{
							}
							else
							{
								intNextBox = (Index + 1) % intTotBoxes;
								if (txtTitle[FCConvert.ToInt16(intNextBox)].Visible == true)
								{
									txtTitle[FCConvert.ToInt16(intNextBox)].Focus();
								}
								else
								{
									if (txtTitle[0].Visible == true)
									{
										txtTitle[0].Focus();
									}
									else
									{
										txtTitle[Index].Focus();
									}
								}
							}
						}
						break;
					}
				case Keys.Left:
					{
						if (txtTitle[Index].SelectionStart == 0)
						{
							KeyCode = (Keys)0;
							txtTitle_Validate(Index, ref boolBad);
							if (boolBad)
							{
							}
							else
							{
								intNextBox = ((Index - 1) + intTotBoxes) % intTotBoxes;
								if (txtTitle[FCConvert.ToInt16(intNextBox)].Visible == true)
								{
									txtTitle[FCConvert.ToInt16(intNextBox)].Focus();
								}
								else
								{
									if (txtTitle[0].Visible == true)
									{
										txtTitle[0].Focus();
									}
									else
									{
										txtTitle[Index].Focus();
									}
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void txtTitle_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int index = txtTitle.IndexOf((FCTextBox)sender);
			txtTitle_KeyDown(index, sender, e);
		}

		private void txtTitle_KeyPress(int Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTitle_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int index = txtTitle.IndexOf((FCTextBox)sender);
			txtTitle_KeyPress(index, sender, e);
		}

		private void txtTitle_Validating(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strTemp = "";
			if (FCConvert.ToString(txtTitle[Index].Tag) == "True")
			{
				// check to see if this field is required
				if (Strings.Trim(txtTitle[Index].Text) != "")
				{
				}
				else
				{
					MessageBox.Show("Please enter a value into the " + Strings.Trim(Strings.Left(lblTitle[Index].Text, lblTitle[Index].Text.Length - 1)) + " field.", "Input Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
				}
			}
		}

		public void txtTitle_Validate(int Index, ref bool Cancel)
		{
			txtTitle_Validating(Index, txtTitle[Index], new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtTitle_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int index = txtTitle.IndexOf((FCTextBox)sender);
			txtTitle_Validating(index, sender, e);
		}

		private void vsFees_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsFees.IsCurrentCellInEditMode)
			{
				blnDirty = true;
			}
		}

		private void vsFees_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsFees.Col == 2)
			{
				vsFees.EditCell();
			}
		}

		private void vsFees_DblClick(object sender, System.EventArgs e)
		{
			if (vsFees.Col == 2)
			{
				vsFees.EditCell();
			}
		}

		private void vsFees_Enter(object sender, System.EventArgs e)
		{
			vsFees.Row = 1;
			vsFees.Col = 1;
		}

		private void vsFees_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (vsFees.Col == 2)
			{
				switch (e.KeyCode)
				{
					case Keys.Return:
						{
							// if return is pressed
							if (vsFees.Row < vsFees.Rows - 1)
							{
								// then move down a row
								vsFees.Row = vsFees.Row + 1;
								//FC:TODO:AM
								//e.KeyCode = 0;
							}
							break;
						}
				}
				//end switch
			}
		}

		private void vsFees_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (vsFees.Col == 2)
			{
				if ((keyAscii >= FCConvert.ToInt32(Keys.D0)) && keyAscii <= FCConvert.ToInt32(Keys.D9) || (keyAscii == FCConvert.ToInt32(Keys.Back)) || (keyAscii == 46) || (keyAscii == 45))
				{
					// do nothing
				}
				else
				{
					keyAscii = 0;
				}
			}
		}

		private void vsFees_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngType;
				int lngBotRow;
				if (cboType.SelectedIndex >= 0)
				{
					lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboType.Text, 3))));
					if (vsFees.RowHidden(vsFees.Row))
					{
						if (vsFees.Row < vsFees.Rows - 1)
						{
							vsFees.Row += 1;
						}
					}
					else
					{
						switch (vsFees.Col)
						{
							case 1:
								{
									// .EditMaxLength = 30
									// .Editable = True
									vsFees.Col = 2;
									return;
								}
							case 2:
								{
									vsFees.EditMaxLength = 13;
									vsFees.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
									vsFees.EditCell();
									break;
								}
							default:
								{
									vsFees.Editable = FCGrid.EditableSettings.flexEDNone;
									break;
								}
						}
						//end switch
						// find the lowest row that is shown
						for (lngBotRow = vsFees.Rows - 1; lngBotRow >= 1; lngBotRow--)
						{
							if (vsFees.RowHeight(lngBotRow) != 0)
								break;
						}
						if (vsFees.Col == 2 && vsFees.Row == lngBotRow)
						{
							vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
						}
						else
						{
							vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
						}
					}
				}
				else
				{
					vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fee Grid Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsFees_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			e.FormattedValue = Strings.Format(vsFees.EditText, "#,##0.00");
		}

		public void ProcessType(bool boolRefresh = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsType = new clsDRWrapper();
				int lngTypeNumber = 0;
				int intCT = 0;
				int I;

				// this will adjust the titles and defaults of the fees and the headings
				rsType.DefaultDB = "TWAR0000.vb1";
				// Print #15, "02 - " & Now
				if (cboType.SelectedIndex != -1)
				{
					// Print #15, "03 - " & Now
					lngTypeNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboType.Text, 3))));
                    //FC:FINAL:BSE #2130 remove label
					//lblTypeDescription.Text = Strings.Trim(Strings.Right(cboType.Text, cboType.Text.Length - 7));
					if (lngTypeNumber != 0)
					{
						if (!modGlobal.ValidateBillType(ref lngTypeNumber))
						{
							MessageBox.Show("There are problems with 1 or more of the accounts used in this bill type.  You must correct these problems before you may create any bills of this type.", "Invalid Accounts in Bill Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(lngTypeNumber));
						if (rsType.BeginningOfFile() != true && rsType.EndOfFile() != true)
						{
							lngCurType = lngTypeNumber;
							ShowNextBillDate_8(rsType.Get_Fields_String("FrequencyCode"), FCConvert.ToInt16(rsType.Get_Fields_Int32("FirstMonth")));
							if (FCConvert.ToString(rsType.Get_Fields_String("FrequencyCode")) == "D")
							{
								chkProrate.CheckState = Wisej.Web.CheckState.Unchecked;
								chkProrate.Enabled = false;
								txtStartDate.Text = "";
								txtStartDate.Enabled = false;
								Label2.Enabled = false;
							}
							else
							{
								chkProrate.Enabled = true;
								txtStartDate.Enabled = true;
								Label2.Enabled = true;
							}
							bool executeACCT2 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title1")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account1")) != "")
							{
								// remove validation
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account1")))
									{
										executeACCT2 = true;
										goto ACCT2;
									}
								}
								vsFees.TextMatrix(1, 1, FCConvert.ToString(rsType.Get_Fields_String("Title1")));
								vsFees.TextMatrix(1, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount1"), "#,##0.00"));
							}
							else
							{
								executeACCT2 = true;
								goto ACCT2;
							}
							ACCT2:
							;
							if (executeACCT2)
							{
								vsFees.TextMatrix(1, 1, "");
								vsFees.TextMatrix(1, 2, FCConvert.ToString(0));
							}
							bool executeACCT3 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title2")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account2")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account2")))
									{
										executeACCT3 = true;
										goto ACCT3;
									}
								}
								// Print #15, "16 - " & Now
								vsFees.TextMatrix(2, 1, FCConvert.ToString(rsType.Get_Fields_String("Title2")));
								vsFees.TextMatrix(2, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount2"), "#,##0.00"));
							}
							else
							{
								executeACCT3 = true;
								goto ACCT3;
							}
							// Print #15, "17 - " & Now
							ACCT3:
							;
							if (executeACCT3)
							{
								vsFees.TextMatrix(2, 1, "");
								vsFees.TextMatrix(2, 2, FCConvert.ToString(0));
							}
							bool executeACCT4 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title3")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account3")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account3")))
									{
										executeACCT4 = true;
										goto ACCT4;
									}
								}
								// Print #15, "18 - " & Now
								vsFees.TextMatrix(3, 1, FCConvert.ToString(rsType.Get_Fields_String("Title3")));
								vsFees.TextMatrix(3, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount3"), "#,##0.00"));
							}
							else
							{
								executeACCT4 = true;
								goto ACCT4;
							}
							// Print #15, "19 - " & Now
							ACCT4:
							;
							if (executeACCT4)
							{
								vsFees.TextMatrix(3, 1, "");
								vsFees.TextMatrix(3, 2, FCConvert.ToString(0));
							}
							bool executeACCT5 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title4")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account4")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account4")))
									{
										executeACCT5 = true;
										goto ACCT5;
									}
								}
								// Print #15, "20 - " & Now
								vsFees.TextMatrix(4, 1, FCConvert.ToString(rsType.Get_Fields_String("Title4")));
								vsFees.TextMatrix(4, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount4"), "#,##0.00"));
							}
							else
							{
								executeACCT5 = true;
								goto ACCT5;
							}
							// Print #15, "21 - " & Now
							ACCT5:
							;
							if (executeACCT5)
							{
								vsFees.TextMatrix(4, 1, "");
								vsFees.TextMatrix(4, 2, FCConvert.ToString(0));
							}
							bool executeACCT6 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title5")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account5")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account5")))
									{
										executeACCT6 = true;
										goto ACCT6;
									}
								}
								// Print #15, "22 - " & Now
								vsFees.TextMatrix(5, 1, FCConvert.ToString(rsType.Get_Fields_String("Title5")));
								vsFees.TextMatrix(5, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount5"), "#,##0.00"));
							}
							else
							{
								executeACCT6 = true;
								goto ACCT6;
							}
							ACCT6:
							;
							if (executeACCT6)
							{
								// Print #15, "23 - " & Now
								vsFees.TextMatrix(5, 1, "");
								vsFees.TextMatrix(5, 2, FCConvert.ToString(0));
							}
							bool executeEXITACCT = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title6")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account6")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account6")))
									{
										executeEXITACCT = true;
										goto EXITACCT;
									}
								}
								// Print #15, "24 - " & Now
								vsFees.TextMatrix(6, 1, FCConvert.ToString(rsType.Get_Fields_String("Title6")));
								vsFees.TextMatrix(6, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount6"), "#,##0.00"));
							}
							else
							{
								executeEXITACCT = true;
								goto EXITACCT;
							}
							// Print #15, "25 - " & Now
							EXITACCT:
							;
							if (executeEXITACCT)
							{
								vsFees.TextMatrix(6, 1, "");
								vsFees.TextMatrix(6, 2, FCConvert.ToString(0));
							}

                            intCT = 1;
							for (I = 1; I <= 6; I++)
							{
								if (Strings.Trim(vsFees.TextMatrix(I, 1)) == "" && Conversion.Val(vsFees.TextMatrix(I, 2)) == 0)
								{
									// .RowHeight(i) = 0
									vsFees.RowHidden(I, true);
								}
								else
								{
									intCT += 1;
									vsFees.RowHidden(I, false);
									// .RowHeight(i) = .RowHeight(0)
								}
							}
							//vsFees.Height = (intCT * vsFees.RowHeight(0)) + 50;

							// fill in the labels names
							lblTitle[0].Text = rsType.Get_Fields_String("Reference") + ":";
							lblTitle[1].Text = rsType.Get_Fields_String("Control1") + ":";
							lblTitle[2].Text = rsType.Get_Fields_String("Control2") + ":";
							lblTitle[3].Text = rsType.Get_Fields_String("Control3") + ":";
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("ReferenceMandatory")) && !FCConvert.ToBoolean(rsType.Get_Fields_Boolean("ReferenceChanges")))
							{
								txtTitle[0].Tag = "True";
								txtTitle[0].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							}
							else
							{
								txtTitle[0].Tag = "False";
								txtTitle[0].BackColor = Color.White;
							}
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control1Mandatory")) && !FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control1Changes")))
							{
								txtTitle[1].Tag = "True";
								txtTitle[1].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							}
							else
							{
								txtTitle[1].Tag = "False";
								txtTitle[1].BackColor = Color.White;
							}
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control2Mandatory")) && !FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control2Changes")))
							{
								txtTitle[2].Tag = "True";
								txtTitle[2].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							}
							else
							{
								txtTitle[2].Tag = "False";
								txtTitle[2].BackColor = Color.White;
							}
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control3Mandatory")) && !FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control3Changes")))
							{
								txtTitle[3].Tag = "True";
								txtTitle[3].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							}
							else
							{
								txtTitle[3].Tag = "False";
								txtTitle[3].BackColor = Color.White;
							}
							txtTitle[0].Text = "";
							txtTitle[1].Text = "";
							txtTitle[2].Text = "";
							txtTitle[3].Text = "";
							// Print #15, "31 - " & Now
							for (I = 0; I <= 3; I++)
							{
								if (Strings.Trim(lblTitle[FCConvert.ToInt16(I)].Text) == ":")
								{
									lblTitle[FCConvert.ToInt16(I)].Visible = false;
									txtTitle[FCConvert.ToInt16(I)].Visible = false;
								}
								else
								{
									lblTitle[FCConvert.ToInt16(I)].Visible = true;
									txtTitle[FCConvert.ToInt16(I)].Visible = true;
								}
							}
						}
						else
						{
							MessageBox.Show("Could not find type #" + FCConvert.ToString(lngTypeNumber) + ".", "Error Loading Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// Close #15
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Processing Bill Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowNextBillDate_8(string strFreq, short intFirst)
		{
			ShowNextBillDate(ref strFreq, ref intFirst);
		}

		private void ShowNextBillDate(ref string strFreq, ref short intFirst)
		{
			if (strFreq == "Y")
			{
				lblNextScheduledBilling.Text = "Next Annual Billing:";
				if (DateTime.Today.Month > intFirst)
				{
					lblNextScheduledBillingDate.Text = Strings.Format(FCConvert.ToString(intFirst) + "/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
				}
				else
				{
					lblNextScheduledBillingDate.Text = Strings.Format(FCConvert.ToString(intFirst) + "/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
				}
			}
			else if (strFreq == "M")
			{
				lblNextScheduledBilling.Text = "Next Monthly Billing:";
				lblNextScheduledBillingDate.Text = Strings.Format(FCConvert.ToString(DateTime.Today.Month) + "/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
			}
			else if (strFreq == "Q")
			{
				lblNextScheduledBilling.Text = "Next Quarterly Billing:";
				switch (intFirst)
				{
					case 1:
						{
							switch (DateTime.Today.Month)
							{
								case 2:
								case 3:
								case 4:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("4/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 5:
								case 6:
								case 7:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("7/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 8:
								case 9:
								case 10:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("10/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 11:
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("1/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
										break;
									}
								case 1:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("1/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
					case 2:
						{
							switch (DateTime.Today.Month)
							{
								case 3:
								case 4:
								case 5:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("5/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 6:
								case 7:
								case 8:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("8/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 9:
								case 10:
								case 11:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("11/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("2/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
										break;
									}
								case 1:
								case 2:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("2/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
					case 3:
						{
							switch (DateTime.Today.Month)
							{
								case 4:
								case 5:
								case 6:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("6/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 7:
								case 8:
								case 9:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("9/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 10:
								case 11:
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("12/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 1:
								case 2:
								case 3:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("3/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
				}
				//end switch
			}
			else if (strFreq == "B")
			{
				lblNextScheduledBilling.Text = "Next Bi Monthly Billing:";
				switch (intFirst)
				{
					case 1:
						{
							switch (DateTime.Today.Month)
							{
								case 2:
								case 3:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("3/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 4:
								case 5:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("5/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 6:
								case 7:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("7/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 8:
								case 9:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("9/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 10:
								case 11:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("11/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("1/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
										break;
									}
								case 1:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("1/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
					case 2:
						{
							switch (DateTime.Today.Month)
							{
								case 1:
								case 2:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("2/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 3:
								case 4:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("4/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 5:
								case 6:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("6/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 7:
								case 8:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("8/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 9:
								case 10:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("10/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 11:
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("12/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
				}
				//end switch
			}
			else if (strFreq == "D")
			{
				lblNextScheduledBilling.Text = "Next Scheduled Billing:";
				lblNextScheduledBillingDate.Text = "On Demand";
			}
			else if (strFreq == "S")
			{
				lblNextScheduledBilling.Text = "Next Semi Annual Billing:";
				switch (intFirst)
				{
					case 1:
						{
							switch (DateTime.Today.Month)
							{
								case 2:
								case 3:
								case 4:
								case 5:
								case 6:
								case 7:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("7/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 8:
								case 9:
								case 10:
								case 11:
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("1/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
										break;
									}
								case 1:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("1/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
					case 2:
						{
							switch (DateTime.Today.Month)
							{
								case 3:
								case 4:
								case 5:
								case 6:
								case 7:
								case 8:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("8/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 9:
								case 10:
								case 11:
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("2/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
										break;
									}
								case 1:
								case 2:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("2/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
					case 3:
						{
							switch (DateTime.Today.Month)
							{
								case 4:
								case 5:
								case 6:
								case 7:
								case 8:
								case 9:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("9/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 10:
								case 11:
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("3/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
										break;
									}
								case 1:
								case 2:
								case 3:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("3/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
					case 4:
						{
							switch (DateTime.Today.Month)
							{
								case 5:
								case 6:
								case 7:
								case 8:
								case 9:
								case 10:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("10/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 11:
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("4/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
										break;
									}
								case 1:
								case 2:
								case 3:
								case 4:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("4/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
					case 5:
						{
							switch (DateTime.Today.Month)
							{
								case 6:
								case 7:
								case 8:
								case 9:
								case 10:
								case 11:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("11/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("5/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year + 1);
										break;
									}
								case 1:
								case 2:
								case 3:
								case 4:
								case 5:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("5/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
					case 6:
						{
							switch (DateTime.Today.Month)
							{
								case 7:
								case 8:
								case 9:
								case 10:
								case 11:
								case 12:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("12/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
								case 1:
								case 2:
								case 3:
								case 4:
								case 5:
								case 6:
									{
										lblNextScheduledBillingDate.Text = Strings.Format("6/1/2000", "MMMM") + " " + FCConvert.ToString(DateTime.Today.Year);
										break;
									}
							}
							//end switch
							break;
						}
				}
				//end switch
			}
		}

		private void ClearBillInformation()
		{
			if (cboType.Items.Count > 0)
			{
				cboType.SelectedIndex = 0;
			}
			else
			{
				cboType.SelectedIndex = -1;
                //FC:FINAL:BSE #2130 remove label
				//lblTypeDescription.Text = "";
			}
			ProcessType();
			txtTitle[0].Text = "";
			txtTitle[1].Text = "";
			txtTitle[2].Text = "";
			txtTitle[3].Text = "";
			txtStartDate.Text = "";
			chkProrate.CheckState = Wisej.Web.CheckState.Unchecked;
			chkProrate.Enabled = true;
			txtStartDate.Enabled = true;
			Label2.Enabled = true;
			txtComment.Text = "";
			lblNextScheduledBillingDate.Text = "";
		}

		private void SetCustomFormColors()
		{
			//Label3.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			int counter;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngRecord = 0;
			for (counter = 0; counter <= 3; counter++)
			{
				if (ColorTranslator.ToOle(txtTitle[FCConvert.ToInt16(counter)].BackColor) == modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT && Strings.Trim(txtTitle[FCConvert.ToInt16(counter)].Text) == "")
				{
					MessageBox.Show("You must fill in all the mandatory control and reference information beofre you may continue.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtTitle[FCConvert.ToInt16(counter)].Focus();
					return;
				}
			}
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboType.Text, 3))));
			if (lngBillID[cboBill.SelectedIndex] == 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM CustomerBills WHERE ID = 0");
				rsInfo.AddNew();
				lngRecord = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID"));
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM CustomerBills WHERE ID = " + FCConvert.ToString(lngBillID[cboBill.SelectedIndex]));
				rsInfo.Edit();
				lngRecord = lngBillID[cboBill.SelectedIndex];
			}
			rsInfo.Set_Fields("Reference", Strings.Trim(txtTitle[0].Text));
			rsInfo.Set_Fields("Control1", Strings.Trim(txtTitle[1].Text));
			rsInfo.Set_Fields("Control2", Strings.Trim(txtTitle[2].Text));
			rsInfo.Set_Fields("Control3", Strings.Trim(txtTitle[3].Text));
			rsInfo.Set_Fields("Type", FCConvert.ToString(Conversion.Val(Strings.Left(cboType.Text, 3))));
			rsInfo.Set_Fields("CustomerID", lngCustomerID);
			if (Information.IsDate(txtStartDate.Text))
			{
				rsInfo.Set_Fields("StartDate", DateAndTime.DateValue(txtStartDate.Text));
			}
			else
			{
				rsInfo.Set_Fields("StartDate", null);
			}
			if (chkProrate.CheckState == Wisej.Web.CheckState.Checked)
			{
				rsInfo.Set_Fields("Prorate", true);
			}
			else
			{
				rsInfo.Set_Fields("Prorate", false);
			}
			rsInfo.Set_Fields("Comment", Strings.Trim(txtComment.Text));
			if (rsDefaultInfo.Get_Fields_Double("DefaultAmount1") != FCConvert.ToDouble(vsFees.TextMatrix(1, 2)))
			{
				rsInfo.Set_Fields("Amount1", vsFees.TextMatrix(1, 2));
			}
			else
			{
				rsInfo.Set_Fields("Amount1", -1);
			}
			if (rsDefaultInfo.Get_Fields_Double("DefaultAmount2") != FCConvert.ToDouble(vsFees.TextMatrix(2, 2)))
			{
				rsInfo.Set_Fields("Amount2", vsFees.TextMatrix(2, 2));
			}
			else
			{
				rsInfo.Set_Fields("Amount2", -1);
			}
			if (rsDefaultInfo.Get_Fields_Double("DefaultAmount3") != FCConvert.ToDouble(vsFees.TextMatrix(3, 2)))
			{
				rsInfo.Set_Fields("Amount3", vsFees.TextMatrix(3, 2));
			}
			else
			{
				rsInfo.Set_Fields("Amount3", -1);
			}
			if (rsDefaultInfo.Get_Fields_Double("DefaultAmount4") != FCConvert.ToDouble(vsFees.TextMatrix(4, 2)))
			{
				rsInfo.Set_Fields("Amount4", vsFees.TextMatrix(4, 2));
			}
			else
			{
				rsInfo.Set_Fields("Amount4", -1);
			}
			if (rsDefaultInfo.Get_Fields_Double("DefaultAmount5") != FCConvert.ToDouble(vsFees.TextMatrix(5, 2)))
			{
				rsInfo.Set_Fields("Amount5", vsFees.TextMatrix(5, 2));
			}
			else
			{
				rsInfo.Set_Fields("Amount5", -1);
			}
			if (rsDefaultInfo.Get_Fields_Double("DefaultAmount6") != FCConvert.ToDouble(vsFees.TextMatrix(6, 2)))
			{
				rsInfo.Set_Fields("Amount6", vsFees.TextMatrix(6, 2));
			}
			else
			{
				rsInfo.Set_Fields("Amount6", -1);
			}
			rsInfo.Update();
			lngBillID[cboBill.ListIndex] = lngRecord;
			MessageBox.Show("Save Successful", "Bill Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			blnDirty = false;
			if (blnMove)
			{
				if (mnuFileMoveNext.Enabled == true)
				{
					mnuFileMoveNext_Click();
				}
			}
		}

		private void cmdSaveContinue_Click(object sender, EventArgs e)
		{
			mnuFileSaveAndContinue_Click(sender, e);
		}

		private void cmdCustomer_Click(object sender, EventArgs e)
		{
			mnuFileCustomer_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, e);
		}
	}
}
