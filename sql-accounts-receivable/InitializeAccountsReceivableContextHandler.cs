﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Commands;
using SharedApplication.Messaging;

namespace TWAR0000
{
	public class InitializeAccountsReceivableContextHandler : CommandHandler<InitializeAccountsReceivableContext>
	{
		protected override void Handle(InitializeAccountsReceivableContext command)
		{
			modGlobal.InitializeAccountsReceivableStatics();
		}
	}
}
