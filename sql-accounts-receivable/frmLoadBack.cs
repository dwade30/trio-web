﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmLoadBack.
	/// </summary>
	public partial class frmLoadBack : BaseForm
	{
		public frmLoadBack()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLoadBack InstancePtr
		{
			get
			{
				return (frmLoadBack)Sys.GetInstance(typeof(frmLoadBack));
			}
		}

		protected frmLoadBack _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/29/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/13/2004              *
		// ********************************************************
		clsDRWrapper rsAR = new clsDRWrapper();
		bool boolLoaded;
		int lngRateKey;
		DateTime dtDefaultInterestPTD;
		// these will keep track of the columns even if I move them
		const int lngColAccount = 0;
		const int lngColName = 1;
		const int lngColInterestPTD = 2;
		const int lngColOverwrite = 3;
		// if the value is 0 then do not overwrite, if it is -1 then overwrite
		const int lngReferenceCol = 4;
		const int lngControl1Col = 5;
		const int lngControl2Col = 6;
		const int lngControl3Col = 7;
		const int lngColTax1 = 8;
		const int lngColTax2 = 9;
		const int lngColTax3 = 10;
		const int lngColTax4 = 11;
		const int lngColTax5 = 12;
		const int lngColTax6 = 13;
		const int lngColIntOwed = 14;
		const int lngColTaxOwed = 15;
		const int lngPrincipalPaidCol = 16;
		const int lngInterestPaidCol = 17;
		const int lngTaxPaidCol = 18;
		clsDRWrapper rsRK = new clsDRWrapper();
		bool blnReferenceRequired;
		bool blnControl1Required;
		bool blnControl2Required;
		bool blnControl3Required;
		bool blnFee1;
		bool blnFee2;
		bool blnFee3;
		bool blnFee4;
		bool blnFee5;
		bool blnFee6;
		bool blnReference;
		bool blnControl1;
		bool blnControl2;
		bool blnControl3;
		string strLoadBackSQL;
		public string strType = "";
		public int lngReturnedRateKey;

		private void cboRateKeys_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (vsAccounts.Rows > 1)
			{
				ans = MessageBox.Show("If you change the rate record without first saving you will lose all of your entered data.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					vsAccounts.Rows = 1;
					lngRateKey = cboRateKeys.ItemData(cboRateKeys.SelectedIndex);
					SetGridHeadings();
					SetGridHeight();
				}
				else
				{
					SetRateKeyCombo(ref lngRateKey);
				}
			}
			else
			{
				lngRateKey = cboRateKeys.ItemData(cboRateKeys.SelectedIndex);
				SetGridHeadings();
				SetGridHeight();
			}
		}

		private void frmLoadBack_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
			else
			{
				// FormatGrid
			}
		}

		private void frmLoadBack_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				KeyCode = (Keys)0;
				if (btnFileNew.Visible && btnFileNew.Enabled)
				{
					mnuFileNew_Click();
				}
			}
		}

		private void frmLoadBack_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			strLoadBackSQL = "(";
			lngReturnedRateKey = -1;
			if (strType == "M")
			{
				this.Text = "Manual Bills";
			}
			else
			{
				this.Text = "Load Back";
			}
			this.HeaderText.Text = this.Text;
			rsAR.OpenRecordset("Select * FROM CustomerMaster");
			if (rsAR.EndOfFile())
			{
				MessageBox.Show("No Accounts Receivable accounts have been found.", "Missing Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			LoadRateKeys();
			if (cboRateKeys.Items.Count > 0)
			{
				cboRateKeys.SelectedIndex = 0;
				lngRateKey = cboRateKeys.ItemData(cboRateKeys.SelectedIndex);
			}
			else
			{
				MessageBox.Show("No rate keys found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			txtDefaultDate.ToolTipText = "Effective interest date to start calculating interest for this bill will be the day after this date.";
			ToolTip1.SetToolTip(lblInterestPTD, txtDefaultDate.ToolTipText);
			// "Effective interest date to start calculating interest for this bill."
			FormatGrid();
		}

		private void frmLoadBack_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraAccountSearch.Visible)
				{
					ReturnFromSearch_2(0);
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int lngJournal = 0;
			clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
			clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
			boolLoaded = false;
			if (strLoadBackSQL != "(")
			{
				if (modGlobalConstants.Statics.gboolBD && strType == "M")
				{
					ans = MessageBox.Show("Would you like to have a journal created in the budgetary system for the bills you have just entered.", "Create Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						strLoadBackSQL = Strings.Left(strLoadBackSQL, strLoadBackSQL.Length - 2) + ")";
						modARCalculations.CreateJournalEntry_3(ref lngJournal, strLoadBackSQL);
						if (lngJournal == 0)
						{
							MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the Budgetary Journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							if (modSecurity.ValidPermissions_8(null, modGlobal.AUTOPOSTBILLINGJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptARBills))
							{
								if (MessageBox.Show("The billing entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									clsJournalInfo = new clsPostingJournalInfo();
									clsJournalInfo.JournalNumber = lngJournal;
									clsJournalInfo.JournalType = "GJ";
									clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
									clsJournalInfo.CheckDate = "";
									clsPostInfo.AddJournal(clsJournalInfo);
									clsPostInfo.AllowPreview = true;
									clsPostInfo.WaitForReportToEnd = true;
									clsPostInfo.PostJournals();
								}
							}
							else
							{
								MessageBox.Show("The billing entries have been saved into journal " + Strings.Format(lngJournal, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
					}
				}
			}
		}

		private void frmLoadBack_Resize(object sender, System.EventArgs e)
		{
			if (fraAccountSearch.Visible)
			{
				SetSearchGridHeight();
			}
			else
			{
				SetGridHeight();
			}
			vsAccounts.ColWidth(lngColAccount, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.05));
			vsAccounts.ColWidth(lngColName, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.15));
			vsAccounts.ColWidth(lngColInterestPTD, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
			vsAccounts.ColWidth(lngColTax1, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
			vsAccounts.ColWidth(lngColTax2, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
			vsAccounts.ColWidth(lngColTax3, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
			vsAccounts.ColWidth(lngColTax4, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
			vsAccounts.ColWidth(lngColTax5, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
			vsAccounts.ColWidth(lngColTax6, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
			vsAccounts.ColWidth(lngPrincipalPaidCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.08));
			vsAccounts.ColWidth(lngInterestPaidCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.08));
			vsAccounts.ColWidth(lngTaxPaidCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.08));
			vsAccounts.ColWidth(lngColIntOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.08));
			vsAccounts.ColWidth(lngColTaxOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.08));
		}

		private void mnuFileAdd_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			lngReturnedRateKey = -1;
			frmAddRateRecord.InstancePtr.Init(1);
			LoadRateKeys();
			if (lngReturnedRateKey != -1)
			{
				SetRateKeyCombo(ref lngReturnedRateKey);
				lngRateKey = lngReturnedRateKey;
			}
            else
            {
                if (cboRateKeys.Items.Count > 0)
                {
                    cboRateKeys.SelectedIndex = 0;
                    lngRateKey = cboRateKeys.ItemData(cboRateKeys.SelectedIndex);
                }
			}

			this.Show(App.MainForm);
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (vsAccounts.Row > 0)
			{
				if (strType == "M")
				{
					ans = MessageBox.Show("Are you sure you want to delete this manual bill information?", "Delete Info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				}
				else
				{
					ans = MessageBox.Show("Are you sure you want to delete this load back information?", "Delete Info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				}
				if (ans == DialogResult.Yes)
				{
					vsAccounts.RemoveItem(vsAccounts.Row);
					SetGridHeight();
				}
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
            if (cboRateKeys.SelectedIndex < 0)
            {
                MessageBox.Show("You must have a rate key selected before you may add bills.", "No Rate Key", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // add a line to the grid and put the focus there
			vsAccounts.AddItem("" + "\t" + "" + "\t" + txtDefaultDate.Text + "\t" + "0" + "\t" + "" + "\t" + "" + "\t" + "" + "\t" + "" + "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t" + "0" + "\t" + "0");
			// vsAccounts.Select vsAccounts.rows - 1, 0, vsAccounts.rows - 1, vsAccounts.Cols - 1
			// Call vsAccounts.CellBorder(RGB(1, 1, 1), -1, -1, -1, 1, -1, -1)
			if (blnFee1)
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax1, "0.00");
			}
			else
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax1, "");
			}
			if (blnFee2)
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax2, "0.00");
			}
			else
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax2, "");
			}
			if (blnFee3)
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax3, "0.00");
			}
			else
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax3, "");
			}
			if (blnFee4)
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax4, "0.00");
			}
			else
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax4, "");
			}
			if (blnFee5)
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax5, "0.00");
			}
			else
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax5, "");
			}
			if (blnFee6)
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax6, "0.00");
			}
			else
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTax6, "");
			}
			vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColTaxOwed, "0.00");
			vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColIntOwed, "0.00");
			vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngInterestPaidCol, "0.00");
			vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngPrincipalPaidCol, "0.00");
			vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngTaxPaidCol, "0.00");
			//FC:FINAL:DDU:#2832 - aligned currency column headers to right
			//vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpCustomFormat, vsAccounts.Rows - 1, lngColTax1, vsAccounts.Rows - 1, vsAccounts.Cols - 1, "#,##0.00");
			//vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsAccounts.Rows - 1, lngColTax1, vsAccounts.Rows - 1, vsAccounts.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(11, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(13, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(14, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(15, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(16, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(17, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(18, FCGrid.AlignmentSettings.flexAlignRightCenter);
			SetGridHeight();
			if (vsAccounts.Visible && vsAccounts.Enabled)
			{
				vsAccounts.Focus();
				vsAccounts.Select(vsAccounts.Rows - 1, lngColAccount);
			}
		}

		public void mnuFileNew_Click()
		{
			mnuFileNew_Click(btnFileNew, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			// save all of the information
			if (SaveInformation())
			{
				// this will clear the grid so that the user can keep going
				vsAccounts.Rows = 1;
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			int lngRW = 0;
			int lngAccount = 0;
			if (fraAccountSearch.Visible)
			{
				// search for accounts
				// SearchForAccounts
				// load the selected account
				if (vsAccountSearch.Rows > 1 && vsAccountSearch.Row > 0)
				{
					lngRW = vsAccountSearch.Row;
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(lngRW, 1))));
					ReturnFromSearch(ref lngAccount);
				}
			}
			else
			{
				// save the information
				if (SaveInformation())
				{
					Close();
				}
			}
		}

		private bool LoadBackInformation()
		{
			bool LoadBackInformation = false;
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curTotalAmount As Decimal	OnWriteFCConvert.ToInt16(
			Decimal curTotalAmount;
			clsDRWrapper rsPaymentRecord = new clsDRWrapper();
			clsDRWrapper rsLastInvoice = new clsDRWrapper();
			// vbPorter upgrade warning: lngCurrentInvoice As int	OnWrite(double, int)
			int lngCurrentInvoice = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				LoadBackInformation = true;
				rsInfo.OpenRecordset("SELECT * FROM Bill WHERE ID = 0");
				rsLastInvoice.OpenRecordset("SELECT TOP 1 * FROM Bill WHERE BillType = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboRateKeys.Text, 3))) + " AND convert(int, left(InvoiceNumber, 2)) = " + FCConvert.ToString(Conversion.Val(Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yy"), 2))) + " ORDER BY InvoiceNumber DESC");
				if (rsLastInvoice.EndOfFile() != true && rsLastInvoice.BeginningOfFile() != true)
				{
					lngCurrentInvoice = FCConvert.ToInt32(Conversion.Val(Strings.Right(FCConvert.ToString(rsLastInvoice.Get_Fields_String("InvoiceNumber")), 4)) + 1);
				}
				else
				{
					lngCurrentInvoice = 1;
				}
				for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
				{
					if (FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColOverwrite)) == 1)
					{
						rsInfo.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + vsAccounts.TextMatrix(counter, lngColAccount) + " AND BillNumber = " + FCConvert.ToString(lngRateKey));
						rsInfo.Edit();
					}
					else
					{
						rsInfo.AddNew();
					}
					// rsAccountInfo.OpenRecordset "SELECT * FROM CustomerMaster WHERE CustomerID = " & vsAccounts.TextMatrix(counter, lngColAccount)
					rsAccountInfo.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsAccountInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE c.CustomerID = " + vsAccounts.TextMatrix(counter, lngColAccount));
					rsInfo.Set_Fields("AccountKey", rsAccountInfo.Get_Fields_Int32("ID"));
					rsInfo.Set_Fields("BillStatus", "A");
					rsInfo.Set_Fields("BName", rsAccountInfo.Get_Fields_String("FullName"));
					rsInfo.Set_Fields("BAddress1", rsAccountInfo.Get_Fields_String("Address1"));
					rsInfo.Set_Fields("BAddress2", rsAccountInfo.Get_Fields_String("Address2"));
					rsInfo.Set_Fields("BAddress3", rsAccountInfo.Get_Fields_String("Address3"));
					rsInfo.Set_Fields("BCity", rsAccountInfo.Get_Fields_String("City"));
					// TODO: Field [PartyState] not found!! (maybe it is an alias?)
					rsInfo.Set_Fields("BState", rsAccountInfo.Get_Fields("PartyState"));
					rsInfo.Set_Fields("BZip", rsAccountInfo.Get_Fields_String("Zip"));
					rsInfo.Set_Fields("BZip4", "");
					rsInfo.Set_Fields("ActualAccountNumber", FCConvert.ToString(Conversion.Val(vsAccounts.TextMatrix(counter, lngColAccount))));
					rsInfo.Set_Fields("BillType", FCConvert.ToString(Conversion.Val(Strings.Left(cboRateKeys.Text, 3))));
					rsInfo.Set_Fields("BillNumber", cboRateKeys.ItemData(cboRateKeys.SelectedIndex));
					rsInfo.Set_Fields("InvoiceNumber", Strings.Mid(cboRateKeys.Text, 15, 2) + Strings.Left(cboRateKeys.Text, 3) + Strings.Format(lngCurrentInvoice, "0000"));
					rsInfo.Set_Fields("BillDate", Strings.Mid(cboRateKeys.Text, 7, 10));
					lngCurrentInvoice += 1;
					curTotalAmount = 0;
					if (blnFee1)
					{
						if (Strings.Trim(vsAccounts.TextMatrix(counter, lngColTax1)) != "")
						{
							rsInfo.Set_Fields("Amount1", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColTax1)));
							rsInfo.Set_Fields("Description1", Strings.Trim(vsAccounts.TextMatrix(0, lngColTax1)));
						}
						else
						{
							rsInfo.Set_Fields("Amount1", 0);
							rsInfo.Set_Fields("Description1", "");
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount1", 0);
						rsInfo.Set_Fields("Description1", "");
					}
					if (blnFee2)
					{
						if (Strings.Trim(vsAccounts.TextMatrix(counter, lngColTax2)) != "")
						{
							rsInfo.Set_Fields("Amount2", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColTax2)));
							rsInfo.Set_Fields("Description2", Strings.Trim(vsAccounts.TextMatrix(0, lngColTax2)));
						}
						else
						{
							rsInfo.Set_Fields("Amount2", 0);
							rsInfo.Set_Fields("Description2", "");
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount2", 0);
						rsInfo.Set_Fields("Description2", "");
					}
					if (blnFee3)
					{
						if (Strings.Trim(vsAccounts.TextMatrix(counter, lngColTax3)) != "")
						{
							rsInfo.Set_Fields("Amount3", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColTax3)));
							rsInfo.Set_Fields("Description3", Strings.Trim(vsAccounts.TextMatrix(0, lngColTax3)));
						}
						else
						{
							rsInfo.Set_Fields("Amount3", 0);
							rsInfo.Set_Fields("Description3", "");
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount3", 0);
						rsInfo.Set_Fields("Description3", "");
					}
					if (blnFee4)
					{
						if (Strings.Trim(vsAccounts.TextMatrix(counter, lngColTax4)) != "")
						{
							rsInfo.Set_Fields("Amount4", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColTax4)));
							rsInfo.Set_Fields("Description4", Strings.Trim(vsAccounts.TextMatrix(0, lngColTax4)));
						}
						else
						{
							rsInfo.Set_Fields("Amount4", 0);
							rsInfo.Set_Fields("Description4", "");
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount4", 0);
						rsInfo.Set_Fields("Description4", "");
					}
					if (blnFee5)
					{
						if (Strings.Trim(vsAccounts.TextMatrix(counter, lngColTax5)) != "")
						{
							rsInfo.Set_Fields("Amount5", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColTax5)));
							rsInfo.Set_Fields("Description5", Strings.Trim(vsAccounts.TextMatrix(0, lngColTax5)));
						}
						else
						{
							rsInfo.Set_Fields("Amount5", 0);
							rsInfo.Set_Fields("Description5", "");
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount5", 0);
						rsInfo.Set_Fields("Description5", "");
					}
					if (blnFee6)
					{
						if (Strings.Trim(vsAccounts.TextMatrix(counter, lngColTax6)) != "")
						{
							rsInfo.Set_Fields("Amount6", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColTax6)));
							rsInfo.Set_Fields("Description6", Strings.Trim(vsAccounts.TextMatrix(0, lngColTax6)));
						}
						else
						{
							rsInfo.Set_Fields("Amount6", 0);
							rsInfo.Set_Fields("Description6", "");
						}
					}
					else
					{
						rsInfo.Set_Fields("Amount6", 0);
						rsInfo.Set_Fields("Description6", "");
					}
					rsInfo.Set_Fields("Quantity1", 1);
					rsInfo.Set_Fields("Quantity2", 1);
					rsInfo.Set_Fields("Quantity3", 1);
					rsInfo.Set_Fields("Quantity4", 1);
					rsInfo.Set_Fields("Quantity5", 1);
					rsInfo.Set_Fields("Quantity6", 1);			
					rsInfo.Set_Fields("PrinOwed", FCConvert.ToDecimal(rsInfo.Get_Fields("Amount1") + rsInfo.Get_Fields("Amount2") + rsInfo.Get_Fields("Amount3") + rsInfo.Get_Fields("Amount4") + rsInfo.Get_Fields("Amount5") + rsInfo.Get_Fields("Amount6")));
					rsInfo.Set_Fields("TaxOwed", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColTaxOwed)));
					rsInfo.Set_Fields("IntAdded", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColIntOwed)) * -1);
					rsInfo.Set_Fields("PrinPaid", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngPrincipalPaidCol)));
					rsInfo.Set_Fields("IntPaid", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngInterestPaidCol)));
					rsInfo.Set_Fields("TaxPaid", FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngTaxPaidCol)));
					rsInfo.Set_Fields("IntPaidDate", vsAccounts.TextMatrix(counter, lngColInterestPTD));
					rsInfo.Set_Fields("Reference", Strings.Trim(vsAccounts.TextMatrix(counter, lngReferenceCol)));
					rsInfo.Set_Fields("Control1", Strings.Trim(vsAccounts.TextMatrix(counter, lngControl1Col)));
					rsInfo.Set_Fields("Control2", Strings.Trim(vsAccounts.TextMatrix(counter, lngControl2Col)));
					rsInfo.Set_Fields("Control3", Strings.Trim(vsAccounts.TextMatrix(counter, lngControl3Col)));
					rsInfo.Set_Fields("CreationDate", DateTime.Today);
					rsInfo.Set_Fields("LastUpdatedDate", DateTime.Today);
                    rsInfo.Update(true);
					// create payment for charged interest
					// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsInfo.Get_Fields("IntAdded")) != 0)
					{
						rsPaymentRecord.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0");
						rsPaymentRecord.AddNew();
						rsPaymentRecord.Set_Fields("AccountKey", rsInfo.Get_Fields_Int32("AccountKey"));
						rsPaymentRecord.Set_Fields("ActualAccountNumber", rsInfo.Get_Fields_Int32("ActualAccountNumber"));
						// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
						rsPaymentRecord.Set_Fields("BillType", rsInfo.Get_Fields("BillType"));
						rsPaymentRecord.Set_Fields("InvoiceNumber", rsInfo.Get_Fields_String("InvoiceNumber"));
						rsPaymentRecord.Set_Fields("BillKey", rsInfo.Get_Fields_Int32("ID"));
						rsPaymentRecord.Set_Fields("BillNumber", rsInfo.Get_Fields_Int32("BillNumber"));
						rsPaymentRecord.Set_Fields("CHGINTNumber", 0);
						rsPaymentRecord.Set_Fields("CHGINTDate", null);
						rsPaymentRecord.Set_Fields("ActualSystemDate", DateTime.Now);
						rsPaymentRecord.Set_Fields("EffectiveInterestDate", rsInfo.Get_Fields_DateTime("IntPaidDate"));
						rsPaymentRecord.Set_Fields("RecordedTransactionDate", rsInfo.Get_Fields_DateTime("IntPaidDate"));
						rsPaymentRecord.Set_Fields("Teller", "");
						rsPaymentRecord.Set_Fields("Reference", "CHGINT");
						rsPaymentRecord.Set_Fields("Code", "I");
						rsPaymentRecord.Set_Fields("ReceiptNumber", 0);
						rsPaymentRecord.Set_Fields("Principal", 0);
						rsPaymentRecord.Set_Fields("Tax", 0);
						// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						rsPaymentRecord.Set_Fields("Interest", FCConvert.ToDouble(rsInfo.Get_Fields("IntAdded")));
						rsPaymentRecord.Set_Fields("TransNumber", 0);
						rsPaymentRecord.Set_Fields("PaidBy", "");
						rsPaymentRecord.Set_Fields("Comments", "");
						rsPaymentRecord.Set_Fields("CashDrawer", "Y");
						rsPaymentRecord.Set_Fields("BudgetaryAccountNumber", "");
						rsPaymentRecord.Set_Fields("GeneralLedger", "Y");
						rsPaymentRecord.Set_Fields("DailyCloseOut", 1);
						rsPaymentRecord.Update();
					}
					// create payment record for paid principal and interest
					// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsInfo.Get_Fields("PrinPaid")) != 0 || FCConvert.ToInt32(rsInfo.Get_Fields("IntPaid")) != 0 || FCConvert.ToInt32(rsInfo.Get_Fields("TaxPaid")) != 0)
					{
						rsPaymentRecord.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0");
						rsPaymentRecord.AddNew();
						rsPaymentRecord.Set_Fields("AccountKey", rsInfo.Get_Fields_Int32("AccountKey"));
						rsPaymentRecord.Set_Fields("ActualAccountNumber", rsInfo.Get_Fields_Int32("ActualAccountNumber"));
						// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
						rsPaymentRecord.Set_Fields("BillType", rsInfo.Get_Fields("BillType"));
						rsPaymentRecord.Set_Fields("InvoiceNumber", rsInfo.Get_Fields_String("InvoiceNumber"));
						rsPaymentRecord.Set_Fields("BillKey", rsInfo.Get_Fields_Int32("ID"));
						rsPaymentRecord.Set_Fields("BillNumber", rsInfo.Get_Fields_Int32("BillNumber"));
						rsPaymentRecord.Set_Fields("CHGINTNumber", 0);
						rsPaymentRecord.Set_Fields("CHGINTDate", null);
						rsPaymentRecord.Set_Fields("ActualSystemDate", DateTime.Now);
						rsPaymentRecord.Set_Fields("EffectiveInterestDate", rsInfo.Get_Fields_DateTime("IntPaidDate"));
						rsPaymentRecord.Set_Fields("RecordedTransactionDate", rsInfo.Get_Fields_DateTime("IntPaidDate"));
						rsPaymentRecord.Set_Fields("Teller", "");
						rsPaymentRecord.Set_Fields("Reference", "AUTOPAY");
						rsPaymentRecord.Set_Fields("Code", "P");
						rsPaymentRecord.Set_Fields("ReceiptNumber", 0);
						// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
						rsPaymentRecord.Set_Fields("Principal", rsInfo.Get_Fields("PrinPaid"));
						// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
						rsPaymentRecord.Set_Fields("Tax", rsInfo.Get_Fields("TaxPaid"));
						// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
						rsPaymentRecord.Set_Fields("Interest", rsInfo.Get_Fields("IntPaid"));
						rsPaymentRecord.Set_Fields("TransNumber", 0);
						rsPaymentRecord.Set_Fields("PaidBy", "");
						rsPaymentRecord.Set_Fields("Comments", "");
						rsPaymentRecord.Set_Fields("CashDrawer", "Y");
						rsPaymentRecord.Set_Fields("BudgetaryAccountNumber", "");
						rsPaymentRecord.Set_Fields("GeneralLedger", "Y");
						rsPaymentRecord.Set_Fields("DailyCloseOut", 1);
						rsPaymentRecord.Update();
					}
					
					strLoadBackSQL += rsInfo.Get_Fields_Int32("ID") + ", ";
				}
				return LoadBackInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				LoadBackInformation = false;
				if (strType == "M")
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Manual Bill Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Load Back Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return LoadBackInformation;
		}

		private bool SaveInformation()
		{
			bool SaveInformation = false;
			SaveInformation = true;
			if (ValidateInformation())
			{
				if (LoadBackInformation())
				{
					MessageBox.Show("The save was successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					SaveInformation = false;
					MessageBox.Show("The save was not successful.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				SaveInformation = false;
			}
			return SaveInformation;
		}

		private bool ValidateInformation()
		{
			bool ValidateInformation = false;
			int lngRow;
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotal;
			ValidateInformation = true;
			vsAccounts.Select(0, 0);
			// this will make the edittext be put into the grid
			// check to make sure that there are accounts loaded
			if (vsAccounts.Rows <= 1)
			{
				ValidateInformation = false;
				MessageBox.Show("Please add an account to be loaded.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return ValidateInformation;
			}
			for (lngRow = 1; lngRow <= vsAccounts.Rows - 1; lngRow++)
			{
				if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColAccount)) == 0)
				{
					ValidateInformation = false;
					MessageBox.Show("You must enter an account number before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngColAccount);
					}
					return ValidateInformation;
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngColName)) == "")
				{
					ValidateInformation = false;
					MessageBox.Show("You must enter a name before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngColName);
					}
					return ValidateInformation;
				}
				if (!Information.IsDate(vsAccounts.TextMatrix(lngRow, lngColInterestPTD)))
				{
					ValidateInformation = false;
					MessageBox.Show("You must enter an interest paid through date before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngColInterestPTD);
					}
					return ValidateInformation;
				}
				if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax1)) == 0)
				{
					if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax2)) == 0)
					{
						if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax3)) == 0)
						{
							if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax4)) == 0)
							{
								if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax5)) == 0)
								{
									if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax6)) == 0)
									{
										ValidateInformation = false;
										MessageBox.Show("All accounts shown must have amounts to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										if (vsAccounts.Visible && vsAccounts.Enabled)
										{
											vsAccounts.Focus();
											vsAccounts.Select(lngRow, lngColIntOwed);
										}
										return ValidateInformation;
									}
								}
							}
						}
					}
				}
				if (FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColIntOwed)) < FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngInterestPaidCol)))
				{
					ValidateInformation = false;
					MessageBox.Show("Your interest paid amount must be equal to or less than your interest owed.", "Invalid Interest Paid Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngInterestPaidCol);
					}
					return ValidateInformation;
				}
				if (FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColTaxOwed)) < FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngTaxPaidCol)))
				{
					ValidateInformation = false;
					MessageBox.Show("Your tax paid amount must be equal to or less than your tax owed.", "Invalid Tax Paid Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngTaxPaidCol);
					}
					return ValidateInformation;
				}
				curTotal = 0;
				if (blnFee1)
				{
					curTotal += FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColTax1));
				}
				if (blnFee2)
				{
					curTotal += FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColTax2));
				}
				if (blnFee3)
				{
					curTotal += FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColTax3));
				}
				if (blnFee4)
				{
					curTotal += FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColTax4));
				}
				if (blnFee5)
				{
					curTotal += FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColTax5));
				}
				if (blnFee6)
				{
					curTotal += FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColTax6));
				}
				if ((FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColTaxOwed)) + curTotal) < FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngPrincipalPaidCol)))
				{
					ValidateInformation = false;
					MessageBox.Show("Your principal paid amount must be equal to or less than your tax owed plus all your amounts.", "Invalid Principal Paid Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngPrincipalPaidCol);
					}
					return ValidateInformation;
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngReferenceCol)) == "" && blnReferenceRequired)
				{
					ValidateInformation = false;
					MessageBox.Show("You must fill in the reference field to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngReferenceCol);
					}
					return ValidateInformation;
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngControl1Col)) == "" && blnControl1Required)
				{
					ValidateInformation = false;
					MessageBox.Show("You must fill in the control 1 field to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngControl1Col);
					}
					return ValidateInformation;
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngControl2Col)) == "" && blnControl2Required)
				{
					ValidateInformation = false;
					MessageBox.Show("You must fill in the control 2 field to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngControl2Col);
					}
					return ValidateInformation;
				}
				if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngControl3Col)) == "" && blnControl3Required)
				{
					ValidateInformation = false;
					MessageBox.Show("You must fill in the control 3 field to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngControl3Col);
					}
					return ValidateInformation;
				}
			}
			return ValidateInformation;
		}

		private void SetSearchGridHeight()
		{
			if ((vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70 > fraAccountSearch.Height * 0.7)
			{
				// too many rows to be shown
				vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				vsAccountSearch.Height = FCConvert.ToInt32(fraAccountSearch.Height * 0.7);
			}
			else
			{
				vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				vsAccountSearch.Height = (vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70;
			}
		}

		private void FormatSearchGrid()
		{
			vsAccountSearch.Cols = 4;
			vsAccountSearch.ColWidth(0, 0);
			vsAccountSearch.ColWidth(1, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.12));
			// account number
			vsAccountSearch.ColWidth(2, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.5));
			// name
			vsAccountSearch.ColWidth(3, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.36));
			// maplot
			vsAccountSearch.ExtendLastCol = true;
			vsAccountSearch.TextMatrix(0, 1, "Account");
			vsAccountSearch.TextMatrix(0, 2, "Name");
			vsAccountSearch.TextMatrix(0, 3, "Location");
			vsAccountSearch.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSort;
			//FC:FINAL:CHN: Set column data type to fix ordering.
			vsAccountSearch.ColDataType(1, FCGrid.DataTypeSettings.flexDTLong);
            //FC:FINAL:SGA #2810 Align column to left
            vsAccountSearch.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

		private void SetGridHeight()
		{
			if (((vsAccounts.Rows - 1) * 40) + vsAccounts.ColumnHeadersHeight > fraAccounts.Height - 50)
			{
				// too many rows to be shown
				// .ScrollBars = flexScrollBarVertical
				vsAccounts.Height = fraAccounts.Height - 50;
			}
			else
			{
				// .ScrollBars = flexScrollBarNone
				vsAccounts.Height = (vsAccounts.Rows - 1) * 40 + vsAccounts.ColumnHeadersHeight + 20;
			}
		}

		private void FormatGrid()
		{
			vsAccounts.Cols = 19;
			vsAccounts.ColWidth(lngColAccount, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.05));
			vsAccounts.ColWidth(lngColName, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.15));
			vsAccounts.ColWidth(lngColInterestPTD, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.11));
			vsAccounts.ColWidth(lngColOverwrite, 0);
			vsAccounts.ColWidth(lngColTax1, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngColTax2, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngColTax3, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngColTax4, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngColTax5, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngColTax6, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngPrincipalPaidCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngInterestPaidCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngTaxPaidCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngColIntOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			vsAccounts.ColWidth(lngColTaxOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
			//FC:FINAL:RPU:#379- due to this last col width is not calculated ok
			//vsAccounts.ExtendLastCol = true;
			// .ScrollBars = flexScrollBarNone
			vsAccounts.FrozenCols = 2;
			// set the alignment
			vsAccounts.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngColInterestPTD, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColFormat(lngColInterestPTD, "MM/dd/yyyy");
			vsAccounts.ColFormat(lngColTax1, "#,##0.00");
			vsAccounts.ColFormat(lngColTax2, "#,##0.00");
			vsAccounts.ColFormat(lngColTax3, "#,##0.00");
			vsAccounts.ColFormat(lngColTax4, "#,##0.00");
			vsAccounts.ColFormat(lngColTax5, "#,##0.00");
			vsAccounts.ColFormat(lngColTax6, "#,##0.00");
			vsAccounts.ColFormat(lngColIntOwed, "#,##0.00");
			vsAccounts.ColFormat(lngColTaxOwed, "#,##0.00");
			vsAccounts.ColFormat(lngPrincipalPaidCol, "#,##0.00");
			vsAccounts.ColFormat(lngInterestPaidCol, "#,##0.00");
			vsAccounts.ColFormat(lngTaxPaidCol, "#,##0.00");
			vsAccounts.TextMatrix(0, lngColAccount, "Acct");
			vsAccounts.TextMatrix(0, lngColName, "Name");
			vsAccounts.TextMatrix(0, lngColInterestPTD, "Int Pd Date");
			vsAccounts.TextMatrix(0, lngPrincipalPaidCol, "Prin Paid");
			vsAccounts.TextMatrix(0, lngInterestPaidCol, "Int Paid");
			vsAccounts.TextMatrix(0, lngTaxPaidCol, "Tax Paid");
			vsAccounts.TextMatrix(0, lngColIntOwed, "Int Owed");
			vsAccounts.TextMatrix(0, lngColTaxOwed, "Tax Owed");
			SetGridHeadings();
			vsAccounts.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
			vsAccounts.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSortShow;
			vsAccounts.ColAlignment(lngReferenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngControl1Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngControl2Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngControl3Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set the default information
			txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			fraAccounts.Enabled = true;
		}

		private void SetGridHeadings()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsTypeInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
				rsTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsInfo.Get_Fields("BillType"));
				if (rsTypeInfo.EndOfFile() != true && rsTypeInfo.BeginningOfFile() != true)
				{
					vsAccounts.TextMatrix(0, lngColTax1, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title1")));
					vsAccounts.TextMatrix(0, lngColTax2, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title2")));
					vsAccounts.TextMatrix(0, lngColTax3, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title3")));
					vsAccounts.TextMatrix(0, lngColTax4, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title4")));
					vsAccounts.TextMatrix(0, lngColTax5, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title5")));
					vsAccounts.TextMatrix(0, lngColTax6, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title6")));
					vsAccounts.TextMatrix(0, lngReferenceCol, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Reference")));
					vsAccounts.TextMatrix(0, lngControl1Col, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control1")));
					vsAccounts.TextMatrix(0, lngControl2Col, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control2")));
					vsAccounts.TextMatrix(0, lngControl3Col, FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control3")));
					blnReferenceRequired = FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("ReferenceMandatory"));
					blnControl1Required = FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control1Mandatory"));
					blnControl2Required = FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control2Mandatory"));
					blnControl3Required = FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("Control3Mandatory"));
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title1"))) != "")
					{
						blnFee1 = true;
					}
					else
					{
						blnFee1 = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title2"))) != "")
					{
						blnFee2 = true;
					}
					else
					{
						blnFee2 = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title3"))) != "")
					{
						blnFee3 = true;
					}
					else
					{
						blnFee3 = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title4"))) != "")
					{
						blnFee4 = true;
					}
					else
					{
						blnFee4 = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title5"))) != "")
					{
						blnFee5 = true;
					}
					else
					{
						blnFee5 = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title6"))) != "")
					{
						blnFee6 = true;
					}
					else
					{
						blnFee6 = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Reference"))) != "")
					{
						blnReference = true;
					}
					else
					{
						blnReference = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control1"))) != "")
					{
						blnControl1 = true;
					}
					else
					{
						blnControl1 = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control2"))) != "")
					{
						blnControl2 = true;
					}
					else
					{
						blnControl2 = false;
					}
					if (Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Control3"))) != "")
					{
						blnControl3 = true;
					}
					else
					{
						blnControl3 = false;
					}
				}
				else
				{
					vsAccounts.TextMatrix(0, lngColTax1, "");
					vsAccounts.TextMatrix(0, lngColTax2, "");
					vsAccounts.TextMatrix(0, lngColTax3, "");
					vsAccounts.TextMatrix(0, lngColTax4, "");
					vsAccounts.TextMatrix(0, lngColTax5, "");
					vsAccounts.TextMatrix(0, lngColTax6, "");
					vsAccounts.TextMatrix(0, lngReferenceCol, "");
					vsAccounts.TextMatrix(0, lngControl1Col, "");
					vsAccounts.TextMatrix(0, lngControl2Col, "");
					vsAccounts.TextMatrix(0, lngControl3Col, "");
					blnReferenceRequired = false;
					blnControl1Required = false;
					blnControl2Required = false;
					blnControl3Required = false;
					blnFee1 = false;
					blnFee2 = false;
					blnFee3 = false;
					blnFee4 = false;
					blnFee5 = false;
					blnFee6 = false;
					blnReference = false;
					blnControl1 = false;
					blnControl2 = false;
					blnControl3 = false;
				}
			}
			else
			{
				vsAccounts.TextMatrix(0, lngColTax1, "");
				vsAccounts.TextMatrix(0, lngColTax2, "");
				vsAccounts.TextMatrix(0, lngColTax3, "");
				vsAccounts.TextMatrix(0, lngColTax4, "");
				vsAccounts.TextMatrix(0, lngColTax5, "");
				vsAccounts.TextMatrix(0, lngColTax6, "");
				vsAccounts.TextMatrix(0, lngReferenceCol, "");
				vsAccounts.TextMatrix(0, lngControl1Col, "");
				vsAccounts.TextMatrix(0, lngControl2Col, "");
				vsAccounts.TextMatrix(0, lngControl3Col, "");
				blnReferenceRequired = false;
				blnControl1Required = false;
				blnControl2Required = false;
				blnControl3Required = false;
				blnFee1 = false;
				blnFee2 = false;
				blnFee3 = false;
				blnFee4 = false;
				blnFee5 = false;
				blnFee6 = false;
				blnReference = false;
				blnControl1 = false;
				blnControl2 = false;
				blnControl3 = false;
			}
			if (blnFee1)
			{
				vsAccounts.ColHidden(lngColTax1, false);
			}
			else
			{
				vsAccounts.ColHidden(lngColTax1, true);
			}
			if (blnFee2)
			{
				vsAccounts.ColHidden(lngColTax2, false);
			}
			else
			{
				vsAccounts.ColHidden(lngColTax2, true);
			}
			if (blnFee3)
			{
				vsAccounts.ColHidden(lngColTax3, false);
			}
			else
			{
				vsAccounts.ColHidden(lngColTax3, true);
			}
			if (blnFee4)
			{
				vsAccounts.ColHidden(lngColTax4, false);
			}
			else
			{
				vsAccounts.ColHidden(lngColTax4, true);
			}
			if (blnFee5)
			{
				vsAccounts.ColHidden(lngColTax5, false);
			}
			else
			{
				vsAccounts.ColHidden(lngColTax5, true);
			}
			if (blnFee6)
			{
				vsAccounts.ColHidden(lngColTax6, false);
			}
			else
			{
				vsAccounts.ColHidden(lngColTax6, true);
			}
			if (blnReference)
			{
				vsAccounts.ColHidden(lngReferenceCol, false);
			}
			else
			{
				vsAccounts.ColHidden(lngReferenceCol, true);
			}
			if (blnControl1)
			{
				vsAccounts.ColHidden(lngControl1Col, false);
			}
			else
			{
				vsAccounts.ColHidden(lngControl1Col, true);
			}
			if (blnControl2)
			{
				vsAccounts.ColHidden(lngControl2Col, false);
			}
			else
			{
				vsAccounts.ColHidden(lngControl2Col, true);
			}
			if (blnControl3)
			{
				vsAccounts.ColHidden(lngControl3Col, false);
			}
			else
			{
				vsAccounts.ColHidden(lngControl3Col, true);
			}
		}
		// vbPorter upgrade warning: lngAcct As int	OnWrite(double, int)
		private void LoadAccountInformation_2(int lngAcct, ref int lngRow)
		{
			LoadAccountInformation(ref lngAcct, ref lngRow);
		}

		private void LoadAccountInformation_6(int lngAcct, int lngRow)
		{
			LoadAccountInformation(ref lngAcct, ref lngRow);
		}

		private void LoadAccountInformation(ref int lngAcct, ref int lngRow)
		{
			// this function will accept an account number and will fill the grid with the name from that account
			clsDRWrapper rsBillCheck = new clsDRWrapper();
			clsDRWrapper rsPayments = new clsDRWrapper();
			int lngBillingYearNumber;
			bool blnNewRecord;
			string strDesc = "";
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			if (lngRow == 0)
			{
				// this will add a row so that the search does not write over any information
				vsAccounts.AddItem("");
				blnNewRecord = true;
				lngRow = vsAccounts.Rows - 1;
			}
			else
			{
				blnNewRecord = false;
			}
			lngBillingYearNumber = 1;
			// check the billingmaster to find out if any other bills have been created for this year
			rsBillCheck.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND BillNumber = " + FCConvert.ToString(lngRateKey));
			if (rsBillCheck.EndOfFile())
			{
				// none created...keep going
				vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
			}
			else
			{
				// ask the user if they still want to create a bill for this account even though it has been created before
				if (strType == "M")
				{
					strDesc = "manual bill";
				}
				else
				{
					strDesc = "load back";
				}
				switch (MessageBox.Show("There is a bill already created for customer " + FCConvert.ToString(lngAcct) + ".  Do you want to continue the " + strDesc + " process for this customer?", "Existing Bill", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							rsBillCheck.MoveFirst();
							// check to see if there are already payments.  If so, then do not allow the overwrite
							rsPayments.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsBillCheck.Get_Fields_Int32("ID"));
							if (rsPayments.EndOfFile())
							{
								vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(1));
							}
							else
							{
								// the user has payments on this bill
								MessageBox.Show("This bill already has payments or adjustments made against it.  Please write off this bill manually for audit purposes.", "Cannot Overwrite", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
								vsAccounts.TextMatrix(lngRow, lngColAccount, "");
								vsAccounts.Select(lngRow, lngColAccount);
								return;
							}
							break;
						}
					case DialogResult.No:
					case DialogResult.Cancel:
						{
							vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
							vsAccounts.TextMatrix(lngRow, lngColAccount, "");
							vsAccounts.Select(lngRow, lngColAccount);
							return;
						}
				}
				//end switch
			}
			rsAR.FindFirstRecord("CustomerID", lngAcct);
			if (rsAR.NoMatch)
			{
				// no matching account number
				MessageBox.Show("There is no Accounts Receivable account with that number.  Please try again.", "Incorrect Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
				vsAccounts.Select(vsAccounts.Row, lngColAccount);
			}
			else
			{
				// matching account number is the current record
				vsAccounts.TextMatrix(lngRow, lngColAccount, FCConvert.ToString(lngAcct));
				pInfo = pCont.GetParty(FCConvert.ToInt32(rsAR.Get_Fields_Int32("PartyID")));
				if (pInfo == null)
				{
					vsAccounts.TextMatrix(lngRow, lngColName, "");
				}
				else
				{
					vsAccounts.TextMatrix(lngRow, lngColName, pInfo.FullName);
				}
				vsAccounts.TextMatrix(lngRow, lngColInterestPTD, txtDefaultDate.Text);
				if (blnFee1)
				{
					vsAccounts.TextMatrix(lngRow, lngColTax1, "0.00");
				}
				else
				{
					vsAccounts.TextMatrix(lngRow, lngColTax1, "");
				}
				if (blnFee2)
				{
					vsAccounts.TextMatrix(lngRow, lngColTax2, "0.00");
				}
				else
				{
					vsAccounts.TextMatrix(lngRow, lngColTax2, "");
				}
				if (blnFee3)
				{
					vsAccounts.TextMatrix(lngRow, lngColTax3, "0.00");
				}
				else
				{
					vsAccounts.TextMatrix(lngRow, lngColTax3, "");
				}
				if (blnFee4)
				{
					vsAccounts.TextMatrix(lngRow, lngColTax4, "0.00");
				}
				else
				{
					vsAccounts.TextMatrix(lngRow, lngColTax4, "");
				}
				if (blnFee5)
				{
					vsAccounts.TextMatrix(lngRow, lngColTax5, "0.00");
				}
				else
				{
					vsAccounts.TextMatrix(lngRow, lngColTax5, "");
				}
				if (blnFee6)
				{
					vsAccounts.TextMatrix(lngRow, lngColTax6, "0.00");
				}
				else
				{
					vsAccounts.TextMatrix(lngRow, lngColTax6, "");
				}
				vsAccounts.TextMatrix(lngRow, lngPrincipalPaidCol, "0.00");
				vsAccounts.TextMatrix(lngRow, lngInterestPaidCol, "0.00");
				vsAccounts.TextMatrix(lngRow, lngTaxPaidCol, "0.00");
				vsAccounts.TextMatrix(lngRow, lngColTaxOwed, "0.00");
				vsAccounts.TextMatrix(lngRow, lngColIntOwed, "0.00");
				vsAccounts.TextMatrix(lngRow, lngReferenceCol, "");
				vsAccounts.TextMatrix(lngRow, lngControl1Col, "");
				vsAccounts.TextMatrix(lngRow, lngControl2Col, "");
				vsAccounts.TextMatrix(lngRow, lngControl3Col, "");
				// vsAccounts.Select lngRow, 0, lngRow, vsAccounts.Cols - 1
				// Call vsAccounts.CellBorder(RGB(1, 1, 1), -1, -1, -1, 1, -1, -1)
				SetGridHeight();
				if (vsAccounts.Visible && vsAccounts.Enabled)
				{
					vsAccounts.Focus();
                    //FC:FINAL:AM:#2824 - don't select the cell; it will cause the entered account number to dissapear
					//vsAccounts.Select(lngRow, lngColName);
				}
			}
		}

		private void mnuFileSearch_Click(object sender, System.EventArgs e)
		{
			// this will show the search frame
			btnFileSearch.Enabled = false;
			btnFileSave.Enabled = false;
			btnFileNew.Enabled = false;
			fraAccountSearch.Top = fraAccounts.Top;
			//fraAccountSearch.Left = FCConvert.ToInt32((this.Width - fraAccountSearch.Width) / 2.0);
			fraAccounts.Visible = false;
			fraAccountSearch.Visible = true;
			if (txtSearchName.Enabled && txtSearchName.Visible)
			{
				txtSearchName.Focus();
			}
			txtSearchName.Text = "";
			vsAccountSearch.Visible = false;
			vsAccountSearch.Rows = 1;
		}

		private void txtDefaultDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// this should check to make sure that the interest date is after
			// or the same as the due date of the rate rec that has been selected
			DateTime dttemp;
			// check this against the Rate Record information
			if (Strings.Trim(txtDefaultDate.Text) != "")
			{
				if (Information.IsDate(txtDefaultDate.Text))
				{
					dttemp = DateAndTime.DateValue(txtDefaultDate.Text);
					rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey));
					if (!(rsRK.EndOfFile()))
					{
						if (DateAndTime.DateDiff("d", dttemp, (DateTime)rsRK.Get_Fields_DateTime("IntStart")) <= 0)
						{
							// this is ok
						}
						else
						{
							txtDefaultDate.Text = Strings.Format(rsRK.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy");
                        }
					}
					else
					{
						txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
					}
				}
				else
				{
					txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				}
			}
			else
			{
				txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
		}

		private void txtSearchName_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						SearchForAccounts();
						break;
					}
			}
			//end switch
		}

		private void vsAccounts_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsAccounts.Col == lngColInterestPTD)
			{
				vsAccounts.EditMask = "##/##/####";
			}
			else
			{
				vsAccounts.EditMask = "";
			}
		}

		private void vsAccounts_Enter(object sender, System.EventArgs e)
		{
			vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsAccounts_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						//FC:FINAL:MW doesn't make sense
						//e.KeyCode = 0;
						switch (MessageBox.Show("Are you sure that you would like to delete this row?", "Delete Row", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
						{
							case DialogResult.Yes:
								{
									vsAccounts.RemoveItem(vsAccounts.Row);
									SetGridHeight();
									break;
								}
							case DialogResult.No:
							case DialogResult.Cancel:
								{
									break;
								}
						}
						//end switch
						break;
					}
			}
			//end switch
		}

		private void vsAccounts_MouseMoveEvent(object sender, MouseEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsAccounts.MouseRow;
			lngMC = vsAccounts.MouseCol;
			if (lngMR > 0)
			{
				switch (lngMC)
				{
					case lngColName:
					case lngColInterestPTD:
						{
							ToolTip1.SetToolTip(vsAccounts, vsAccounts.TextMatrix(lngMR, lngMC));
							break;
						}
					default:
						{
							ToolTip1.SetToolTip(vsAccounts, "");
							break;
						}
				}
				//end switch
			}
		}

		private void vsAccounts_RowColChange(object sender, System.EventArgs e)
		{
			switch (vsAccounts.Col)
			{
				case lngColAccount:
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				case lngColInterestPTD:
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				case lngColName:
					{
						// allow the name to be edited
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				case lngColTax1:
					{
						if (blnFee1)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngColTax2:
					{
						if (blnFee2)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngColTax3:
					{
						if (blnFee3)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngColTax4:
					{
						if (blnFee4)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngColTax5:
					{
						if (blnFee5)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngColTax6:
					{
						if (blnFee6)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngColOverwrite:
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case lngReferenceCol:
					{
						if (blnReference)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngControl1Col:
					{
						if (blnControl1)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngControl2Col:
					{
						if (blnControl2)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				case lngControl3Col:
					{
						if (blnControl3)
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
				default:
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
			}
			//end switch
			if (vsAccounts.Editable == FCGrid.EditableSettings.flexEDKbdMouse)
			{
				vsAccounts.EditCell();
			}
			if (vsAccounts.Row == vsAccounts.Rows - 1 && vsAccounts.Col == vsAccounts.Cols - 1)
			{
				vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
		}

		private void vsAccounts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsAccounts.GetFlexRowIndex(e.RowIndex);
			int col = vsAccounts.GetFlexColIndex(e.ColumnIndex);
			switch (col)
			{
				case lngColAccount:
					{
						// if this is validating an account
						if (vsAccounts.EditText != "")
						{
							if (Strings.Trim(vsAccounts.EditText) != vsAccounts.TextMatrix(row, col))
							{
								int temp = row;
								LoadAccountInformation_2(FCConvert.ToInt32(Conversion.Val(vsAccounts.EditText)), ref temp);
								vsAccounts.Row = temp;
							}
						}
						break;
					}
				case lngColInterestPTD:
					{
						// validate the paid through date
						if (Information.IsDate(vsAccounts.EditText))
						{
							vsAccounts.TextMatrix(row, col, Strings.Format(vsAccounts.EditText, "MM/dd/yyyy"));
						}
						else
						{
							vsAccounts.TextMatrix(row, col, "");
							e.Cancel = true;
						}
						break;
					}
			}
			//end switch
		}

		private void SearchForAccounts()
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			string strSQL = "";
			FormatSearchGrid();
			// rsSearch.OpenRecordset "SELECT * FROM CustomerMaster"  ' WHERE Name > '" & Trim(txtSearchName.Text) & "     ' AND Name < '" & Trim(txtSearchName.Text) & "ZZZZZ' ORDER BY Name"
			// rsSearch.InsertName "PartyID", "", , True, , , , "FullName", , "FullName > '" & Trim(txtSearchName.Text) & "     ' AND FullName < '" & Trim(txtSearchName.Text) & "ZZZZZ'"
			rsSearch.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsSearch.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p  WHERE p.FullName > '" + Strings.Trim(txtSearchName.Text) + "     ' AND p.FullName < '" + Strings.Trim(txtSearchName.Text) + "ZZZZZ'  ORDER BY p.FullName");
			if (rsSearch.EndOfFile())
			{
				// no accounts
				MessageBox.Show("No accounts match the search criteria.", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				vsAccountSearch.Rows = 1;
				while (!rsSearch.EndOfFile())
				{
					vsAccountSearch.AddItem("\t" + rsSearch.Get_Fields_Int32("CustomerID") + "\t" + rsSearch.Get_Fields_String("FullName") + "\t" + rsSearch.Get_Fields_String("Address1"));
					rsSearch.MoveNext();
				}
			}
			vsAccountSearch.Visible = true;
			// resize the grid
			SetSearchGridHeight();
		}

		private void vsAccountSearch_DblClick(object sender, System.EventArgs e)
		{
			// selecting an account to use
			int lngRW;
			int lngAccount;
			lngRW = vsAccountSearch.MouseRow;
			lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(lngRW, 1))));
			ReturnFromSearch(ref lngAccount);
		}

		private void ReturnFromSearch_2(int lngAcct)
		{
			ReturnFromSearch(ref lngAcct);
		}

		private void ReturnFromSearch(ref int lngAcct)
		{
			fraAccountSearch.Visible = false;
			fraAccounts.Visible = true;
			btnFileSave.Enabled = true;
			btnFileSearch.Enabled = true;
			btnFileNew.Enabled = true;
			if (lngAcct == 0)
			{
				if (vsAccounts.Visible && vsAccounts.Enabled)
				{
					vsAccounts.Focus();
				}
				else
				{
				}
			}
			else
			{
				LoadAccountInformation_6(lngAcct, 0);
				vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpCustomFormat, vsAccounts.Rows - 2, lngColTax1, vsAccounts.Rows - 2, lngColTax6, "#,##0.00");
				vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsAccounts.Rows - 2, lngColTax1, vsAccounts.Rows - 2, lngColTax6, FCGrid.AlignmentSettings.flexAlignRightCenter);
				SetGridHeight();
			}
		}

		private void vsAccountSearch_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int lngAccount = 0;
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				// return key
				keyAscii = 0;
				if (vsAccountSearch.Row > 0)
				{
					// selecting an account to use
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(vsAccountSearch.Row, 1))));
					ReturnFromSearch(ref lngAccount);
				}
			}
		}

		public void LoadRateKeys()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboRateKeys.Clear();
			rsInfo.OpenRecordset("SELECT * FROM RateKeys ORDER BY BillDate DESC");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					cboRateKeys.AddItem(Strings.Format(rsInfo.Get_Fields("BillType"), "000") + "   " + Strings.Format(rsInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "   " + rsInfo.Get_Fields_String("Description"));
					cboRateKeys.ItemData(cboRateKeys.NewIndex, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void SetRateKeyCombo(ref int lngKey)
		{
			int counter;
			for (counter = 0; counter <= cboRateKeys.Items.Count - 1; counter++)
			{
				if (cboRateKeys.ItemData(counter) == lngKey)
				{
					cboRateKeys.SelectedIndex = counter;
					return;
				}
			}
		}

		private void btnFileAdd_Click(object sender, EventArgs e)
		{
			mnuFileAdd_Click(sender, e);
		}
	}
}
