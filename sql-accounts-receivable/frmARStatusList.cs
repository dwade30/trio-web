﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Linq;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmARStatusList.
	/// </summary>
	public partial class frmARStatusList : BaseForm
	{
		public frmARStatusList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkInclude = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkInclude.AddControlArrayElement(chkInclude_2, 2);
			this.chkInclude.AddControlArrayElement(chkInclude_1, 1);
			this.chkInclude.AddControlArrayElement(chkInclude_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmARStatusList InstancePtr
		{
			get
			{
				return (frmARStatusList)Sys.GetInstance(typeof(frmARStatusList));
			}
		}

		protected frmARStatusList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/02/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/23/2007              *
		// ********************************************************
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolSaveReport;
		string strYearChoice = "";
		bool boolLoaded;
		string strGridToolTipText = "";
		// these will be to pass the string on to the reports when needed
		public string strRSWhere = string.Empty;
		public string strRSOrder = string.Empty;
		public string strBinaryWhere = "";
		public int lngMax;
		public bool boolShowCurrentInterest;
		public int intMasterReportType;
		public const int lngRowReportType = 0;
		public const int lngRowAccount = 1;
		public const int lngRowName = 2;
		public const int lngRowRateKey = 3;
		public const int lngRowBillType = 4;
		public const int lngRowBalanceDue = 5;
		public const int lngRowGroupBy = 6;
		public const int lngRowPaymentType = 7;
		public const int lngRowAsOfDate = 8;
		public const int lngRowShowPaymentFrom = 9;

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
			for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
			{
				lstTypes.SetSelected(intCounter, false);
			}
			vsWhere.EditText = "";
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				// set the cursor to an hourglass
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (chkCurrentInterest.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolShowCurrentInterest = true;
				}
				else
				{
					boolShowCurrentInterest = false;
				}
				BuildSQL();
				// Dim rsSQL As New clsDRWrapper
				// set focus to another object in order to validate the where grid info
				vsWhere.Select(0, 0);
				if (!ValidateWhereGrid())
				{
					return;
				}
				// SHOW THE REPORT
				if (Strings.UCase(vsWhere.TextMatrix(lngRowReportType, 1)) == "REGULAR")
				{
					// Regular Status List
					frmReportViewer.InstancePtr.Init(arARStatusLists.InstancePtr);
				}
				else
				{
					// Aged Account Listing
					frmReportViewer.InstancePtr.Init(arARAgedLists.InstancePtr);
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("ERROR #:" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Print Status Lists ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}
		// vbPorter upgrade warning: 'Return' As bool	OnWrite(string)
		public bool BuildSQL()
		{
			bool BuildSQL = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
				int intCounter;
				string strPaymentDateRage = "";
				string strSort;
				string strWhere;
				bool boolNoLien;
				bool boolAgedList;
				strSort = " ";
				strWhere = " ";
				boolAgedList = FCConvert.CBool(Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRowReportType, 1), 3)) == "AGE");
				vsWhere.Select(0, 0);
				if (modGlobal.Statics.gboolARSLDateRange)
				{
					strPaymentDateRage = " AND (RecordedTransactionDate >= #" + FCConvert.ToString(modGlobal.Statics.gdtARSLPaymentDate1) + "# AND RecordedTransactionDate <= #" + FCConvert.ToString(modGlobal.Statics.gdtARSLPaymentDate2) + "#)";
				}
				else
				{
					strPaymentDateRage = "";
				}
				// GET THE FIELDS TO SORT BY
				for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
				{
					if (lstSort.Selected(intCounter))
					{
						// it is checked
						if (strSort != " ")
							strSort += ", ";
						// lstSort.ItemData(intCounter)
						if (Strings.Left(lstSort.List(intCounter), 2) == "Ac")
						{
							// 0
							strSort += "ActualAccountNumber";
						}
						else if (Strings.Left(lstSort.List(intCounter), 2) == "Na")
						{
							// 1
							strSort += "BName";
						}
					}
				}
				if (Strings.Trim(strSort) == "")
				{
					if (!boolAgedList)
					{
						strSort = "BName, ActualAccountNumber, InvoiceNumber";
						// default sort order
					}
					else
					{
						strSort = "";
					}
				}
				// create the where string here
				if (!boolAgedList)
				{
					if (Strings.Trim(vsWhere.TextMatrix(lngRowAccount, 1)) != "")
					{
						// Account Number
						if (Strings.Trim(vsWhere.TextMatrix(lngRowAccount, 2)) != "")
						{
							strWhere += "ActualAccountNumber <= " + vsWhere.TextMatrix(lngRowAccount, 2) + " AND ActualAccountNumber  >= " + vsWhere.TextMatrix(lngRowAccount, 1);
						}
						else
						{
							strWhere += "ActualAccountNumber  >= " + vsWhere.TextMatrix(lngRowAccount, 1);
						}
					}
					else
					{
						if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
						{
							strWhere += "ActualAccountNumber  <= " + vsWhere.TextMatrix(lngRowAccount, 2);
						}
					}
					if (Strings.Trim(vsWhere.TextMatrix(lngRowName, 1)) != "")
					{
						// Name
						if (Strings.Trim(vsWhere.TextMatrix(lngRowName, 1)) == "")
						{
							if (strWhere != " ")
								strWhere += " AND ";
							strWhere += "BName >= '" + vsWhere.TextMatrix(lngRowName, 1) + "    ' AND BName < '" + vsWhere.TextMatrix(lngRowName, 1) + "zzzz'";
						}
						else
						{
							// this is when they both have values in the fields and will be a range by name
							if (strWhere != " ")
								strWhere += " AND ";
							strWhere += "BName >= '" + vsWhere.TextMatrix(lngRowName, 1) + "    ' AND BName < '" + vsWhere.TextMatrix(lngRowName, 2) + "zzzz'";
						}
					}
					if (Strings.Trim(vsWhere.TextMatrix(lngRowBillType, 1)) != "")
					{
						// Account Number
						if (Strings.Trim(vsWhere.TextMatrix(lngRowBillType, 2)) != "")
						{
							strWhere += "BillType <= " + vsWhere.TextMatrix(lngRowBillType, 2) + " AND BillType  >= " + vsWhere.TextMatrix(lngRowBillType, 1);
						}
						else
						{
							strWhere += "BillType  >= " + vsWhere.TextMatrix(lngRowBillType, 1);
						}
					}
					else
					{
						if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
						{
							strWhere += "BillType  <= " + vsWhere.TextMatrix(lngRowBillType, 2);
						}
					}
					if (lstTypes.SelectedItems.Any())
					{
						for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
						{
							if (lstTypes.Selected(intCounter))
							{
								strTemp += FCConvert.ToString(lstTypes.ItemData(intCounter)) + ",";
							}
						}
						if (strTemp.Length > 1)
						{
							if (Strings.Trim(strWhere) != "")
							{
								strWhere += " AND BillType IN (" + Strings.Left(strTemp, strTemp.Length - 1) + ")";
							}
							else
							{
								strWhere = " BillType IN (" + Strings.Left(strTemp, strTemp.Length - 1) + ")";
							}
						}
					}
					if (Strings.Trim(vsWhere.TextMatrix(lngRowRateKey, 1)) != "")
					{
						// Bill Number
						if (Strings.Trim(vsWhere.TextMatrix(lngRowRateKey, 2)) != "")
						{
							if (strWhere != " ")
								strWhere += " AND ";
							strWhere += "BillNumber >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(lngRowRateKey, 1))) + " AND BillNumber <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(lngRowRateKey, 2)));
						}
						else
						{
							if (strWhere != " ")
								strWhere += " AND ";
							strWhere += "BillNumber = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(lngRowRateKey, 1)));
						}
					}
				}
				else
				{
					if (Strings.Trim(vsWhere.TextMatrix(lngRowAccount, 1)) != "")
					{
						// Account Number
						if (Strings.Trim(vsWhere.TextMatrix(lngRowAccount, 2)) != "")
						{
							strWhere += "CustomerID <= " + vsWhere.TextMatrix(lngRowAccount, 2) + " AND CustomerID  >= " + vsWhere.TextMatrix(lngRowAccount, 1);
						}
						else
						{
							strWhere += "CustomerID >= " + vsWhere.TextMatrix(lngRowAccount, 1);
						}
					}
					else
					{
						if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
						{
							strWhere += "CustomerID  <= " + vsWhere.TextMatrix(lngRowAccount, 2);
						}
					}
					if (Strings.Trim(vsWhere.TextMatrix(lngRowName, 1)) != "")
					{
						// Name
						if (Strings.Trim(vsWhere.TextMatrix(lngRowName, 1)) == "")
						{
							if (strWhere != " ")
								strWhere += " AND ";
							strWhere += "BName >= '" + vsWhere.TextMatrix(lngRowName, 1) + "    ' AND BName < '" + vsWhere.TextMatrix(lngRowName, 1) + "zzzz'";
						}
						else
						{
							// this is when they both have values in the fields and will be a range by name
							if (strWhere != " ")
								strWhere += " AND ";
							strWhere += "BName >= '" + vsWhere.TextMatrix(lngRowName, 1) + "    ' AND BName < '" + vsWhere.TextMatrix(lngRowName, 2) + "zzzz'";
						}
					}
					if (Strings.Trim(vsWhere.TextMatrix(lngRowBillType, 1)) != "")
					{
						// Account Number
						if (Strings.Trim(vsWhere.TextMatrix(lngRowBillType, 2)) != "")
						{
							strWhere += "BillType <= " + vsWhere.TextMatrix(lngRowBillType, 2) + " AND BillType  >= " + vsWhere.TextMatrix(lngRowBillType, 1);
						}
						else
						{
							strWhere += "BillType  >= " + vsWhere.TextMatrix(lngRowBillType, 1);
						}
					}
					else
					{
						if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
						{
							strWhere += "BillType  <= " + vsWhere.TextMatrix(lngRowBillType, 2);
						}
					}
					if (lstTypes.SelectedItems.Any())
					{
						for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
						{
							if (lstTypes.Selected(intCounter))
							{
								strTemp += FCConvert.ToString(lstTypes.ItemData(intCounter)) + ",";
							}
						}
						if (strTemp.Length > 1)
						{
							if (Strings.Trim(strWhere) != "")
							{
								strWhere += " AND BillType IN (" + Strings.Left(strTemp, strTemp.Length - 1) + ")";
							}
							else
							{
								strWhere = " BillType IN (" + Strings.Left(strTemp, strTemp.Length - 1) + ")";
							}
						}
					}
				}
				if (Strings.Trim(vsWhere.TextMatrix(lngRowAsOfDate, 1)) != "")
				{
					if (Information.IsDate(vsWhere.TextMatrix(lngRowAsOfDate, 1)))
					{
						modGlobal.Statics.gdtARStatusListAsOfDate = DateAndTime.DateValue(vsWhere.TextMatrix(lngRowAsOfDate, 1));
						modGlobal.Statics.gboolARUseAsOfDate = true;
						// If strWhere <> " " Then strWhere = strWhere & " AND "
						// strWhere = strWhere & " CreationDate <= #" & gdtARStatusListAsOfDate & "#"
					}
					else
					{
						modGlobal.Statics.gdtARStatusListAsOfDate = DateTime.Today;
						// set the default as of date
						modGlobal.Statics.gboolARUseAsOfDate = false;
					}
				}
				else
				{
					modGlobal.Statics.gdtARStatusListAsOfDate = DateTime.Today;
					// set the default as of date
					modGlobal.Statics.gboolARUseAsOfDate = false;
				}
				if (Strings.Trim(strWhere) == "")
				{
					// make something up
					strWhere = "BillStatus <> 'V' ";
				}
				else
				{
					strWhere = "BillStatus <> 'V' AND " + strWhere;
				}
				strRSWhere = strWhere;
				strRSOrder = strSort;
				return BuildSQL;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				BuildSQL = FCConvert.CBool("");
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building SQL", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildSQL;
		}

		private void frmARStatusList_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
			}
			else
			{
				lngMax = 7;
				EnableFrames_2(true);
				//Application.DoEvents();
				Format_WhereGrid_2(true);
				ShowAutomaticFields();
				modARStatusList.SetupStatusListCombos_2(true);
				Fill_Lists();
				FillTypeList();
				boolLoaded = true;
			}
		}

		private void frmARStatusList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmARStatusList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmARStatusList.ScaleWidth	= 9045;
			//frmARStatusList.ScaleHeight	= 7395;
			//frmARStatusList.LinkTopic	= "Form1";
			//frmARStatusList.LockControls	= -1  'True;
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			this.Text = "Status Lists";
			modGlobalFunctions.SetTRIOColors(this);
			Set_Note_Text();
			modGlobal.Statics.gdtARStatusListAsOfDate = DateTime.Today;
			// set the default as of date
		}

		private void frmARStatusList_Resize(object sender, System.EventArgs e)
		{
			Format_WhereGrid_2(false);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
			{
				lstTypes.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//    MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//    int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//    float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//    float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//    // THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//    // CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//    // WHERE TO SWAP THE TWO ITEMS.
		//    //
		//    // THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//    // WILL BE DISPLAYED ON THE REPORT ITSELF
		//    intStart = lstSort.SelectedIndex;
		//}
		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//    MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//    int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//    float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//    float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//    // THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//    // ITEMS THAT ARE TO BE SWAPED
		//    //
		//    // THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//    // WILL BE DISPLAYED ON THE REPORT ITSELF
		//    bool boolSecondSelected = false;
		//    // IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//    if (intStart != lstSort.SelectedIndex)
		//    {
		//        // SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//        strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
		//        intID = lstSort.ItemData(lstSort.SelectedIndex);
		//        boolSecondSelected = lstSort.Selected(lstSort.SelectedIndex);
		//        // CHANGE THE NEW ITEM
		//        lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//        lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//        // SAVE THE OLD ITEM
		//        lstSort.Items[intStart].Text = strTemp;
		//        lstSort.ItemData(intStart, intID);
		//        // SET BOTH ITEMS TO BE SELECTED
		//        lstSort.SetSelected(lstSort.ListIndex, true);
		//        lstSort.SetSelected(intStart, boolSecondSelected);
		//    }
		//}
		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEED TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			// vsWhere.TextMatrix(Row, 2) = vsWhere.ComboData
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
			{
				switch (vsWhere.Row)
				{
					case lngRowAsOfDate:
					case lngRowShowPaymentFrom:
						{
							vsWhere.EditMask = "##/##/####";
							break;
						}
					case lngRowReportType:
					case lngRowGroupBy:
					case lngRowPaymentType:
						{
							if (vsWhere.Col == 1)
							{
								vsWhere.ComboList = modARStatusList.Statics.strComboList[vsWhere.Row, 0];
							}
							break;
						}
					case lngRowAccount:
					case lngRowName:
					case lngRowRateKey:
					case lngRowBillType:
						{
							if (vsWhere.Col == 1 || vsWhere.Col == 2)
							{
								vsWhere.ComboList = modARStatusList.Statics.strComboList[vsWhere.Row, 0];
							}
							break;
						}
					case lngRowBalanceDue:
						{
							if (vsWhere.Col == 1)
							{
								// only show the list if the user is in the first col
								vsWhere.ComboList = modARStatusList.Statics.strComboList[vsWhere.Row, 0];
							}
							break;
						}
					default:
						{
							break;
						}
				}
				//end switch
			}
		}

		private void vsWhere_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsWhere.IsCurrentCellInEditMode)
			{
				ShowAutomaticFields();
			}
		}

		private void vsWhere_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (vsWhere.Col > 0)
			{
				switch (e.KeyCode)
				{
					case Keys.Delete:
						{
							switch (vsWhere.Row)
							{
							// MAL@20070830:Added to not allow Report Type and W/S To Be Cleared
								case lngRowReportType:
									{
										// Do Nothing
										break;
									}
								default:
									{
										vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
										break;
									}
							}
							//end switch
							// vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col) = ""
							break;
						}
				}
				//end switch
			}
		}

		private void vsWhere_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						switch (vsWhere.Row)
						{
						// MAL@20070830:Added to not allow Report Type and W/S To Be Cleared
							case lngRowReportType:
								{
									// Do Nothing
									break;
								}
							default:
								{
									vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
									break;
								}
						}
						//end switch
						break;
					}
				case Keys.Escape:
					{
						vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
		}

		private void vsWhere_Leave(object sender, System.EventArgs e)
		{
			vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			vsWhere.Select(0, 1);
		}

		private void vsWhere_MouseMoveEvent(object sender, MouseEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsWhere.MouseRow;
			lngMC = vsWhere.MouseCol;
			ToolTip1.SetToolTip(vsWhere, "");
			if (lngMC == 0)
			{
				switch (lngMR)
				{
					case lngRowReportType:
						{
							ToolTip1.SetToolTip(vsWhere, "This will determine whether to show a regular status list of bills or an aged list.");
							break;
						}
					case lngRowAccount:
						{
							ToolTip1.SetToolTip(vsWhere, "Show a range of accounts.");
							break;
						}
					case lngRowName:
						{
							ToolTip1.SetToolTip(vsWhere, "Show a range of names.");
							break;
						}
					case lngRowRateKey:
						{
							ToolTip1.SetToolTip(vsWhere, "Show a range by rate record.");
							break;
						}
					case lngRowBillType:
						{
							ToolTip1.SetToolTip(vsWhere, "Show only a certain type of bill.");
							break;
						}
					case lngRowGroupBy:
						{
							ToolTip1.SetToolTip(vsWhere, "Show subtotals for the bills grouped on this selection.");
							break;
						}
					case lngRowPaymentType:
						{
							ToolTip1.SetToolTip(vsWhere, "Only show bills with this payment type.");
							break;
						}
				//FC:COMP:MW label value (7) is defined twice
				//case lngRowPaymentType:
				//    {
				//        ToolTip1.SetToolTip(vsWhere, "Show bills that have this type of payment.");
				//        break;
				//    }
					case lngRowAsOfDate:
						{
							ToolTip1.SetToolTip(vsWhere, "Show bills with the payments that would have been recorded at this date.");
							break;
						}
					case lngRowShowPaymentFrom:
						{
							ToolTip1.SetToolTip(vsWhere, "Accounts with payments withing this range of dates.");
							break;
						}
				}
				//end switch
			}
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			int lngRow;
			int lngCol;
			lngRow = vsWhere.Row;
			lngCol = vsWhere.Col;
			if (lngRow > 0)
			{
				if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngCol) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					if (lngRow == vsWhere.Rows - 1)
					{
						vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
					}
					if (modARStatusList.Statics.strComboList[lngRow, 0] != string.Empty)
					{
						vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsWhere.ComboList = modARStatusList.Statics.strComboList[lngRow, 0];
					}
					if (vsWhere.Col == 2)
					{
						if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							Support.SendKeys("{Tab}", false);
						}
					}
				}
				else
				{
					vsWhere.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsWhere.GetFlexRowIndex(e.RowIndex);
				int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
				switch (row)
				{
					case lngRowReportType:
						{
							if (vsWhere.EditText == "Regular")
							{
								modARStatusList.SetupStatusListCombos_2(true);
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowGroupBy, 1, Color.White);
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowRateKey, 1, Color.White);
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowRateKey, 2, Color.White);
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowAsOfDate, 1, Color.White);
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowPaymentType, 1, Color.White);
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowShowPaymentFrom, 1, lngRowShowPaymentFrom, 2, Color.White);
								chkShowPayments.Enabled = true;
							}
							else
							{
								modARStatusList.SetupStatusListCombos_2(false);
								vsWhere.TextMatrix(lngRowGroupBy, 1, "");
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowGroupBy, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								vsWhere.TextMatrix(lngRowRateKey, 1, "");
								vsWhere.TextMatrix(lngRowRateKey, 2, "");
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowAsOfDate, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								vsWhere.TextMatrix(lngRowAsOfDate, 1, "");
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowPaymentType, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								vsWhere.TextMatrix(lngRowPaymentType, 1, "");
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowShowPaymentFrom, 1, lngRowShowPaymentFrom, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								vsWhere.TextMatrix(lngRowShowPaymentFrom, 1, "");
								vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowRateKey, 1, lngRowRateKey, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
								chkShowPayments.CheckState = Wisej.Web.CheckState.Unchecked;
								chkShowPayments.Enabled = false;
							}
							break;
						}
					case lngRowAsOfDate:
					case lngRowShowPaymentFrom:
						{
							if (Strings.Trim(vsWhere.EditText) == "/  /")
							{
								vsWhere.EditMask = string.Empty;
								vsWhere.EditText = string.Empty;
								vsWhere.TextMatrix(row, col, string.Empty);
								vsWhere.Refresh();
								return;
							}
							break;
						}
				}
				//end switch
				ShowAutomaticFields();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validate Edit", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void EnableFrames_2(bool boolEnable)
		{
			EnableFrames(ref boolEnable);
		}

		private void EnableFrames(ref bool boolEnable)
		{
			fraFields.Enabled = boolEnable;
			fraSort.Enabled = boolEnable;
			fraWhere.Enabled = boolEnable;
			vsWhere.Enabled = boolEnable;
			lstTypes.Enabled = boolEnable;
			lstSort.Enabled = boolEnable;
		}

		private void Set_Note_Text()
		{
			string strTemp;
			strTemp = "1. The Fields To Sort By section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
			strTemp += "2. Set any criteria needed in the Select Search Criteria list." + "\r\n" + "\r\n";
			strTemp += "Other Notes:" + "\r\n" + "Some fields will be printed automatically." + "\r\n" + "\r\n";
			// strTemp = strTemp & vbCrLf & "If you choose a default report, only the account number and the tax year criteria will affect the report.  Any changes in the other fields will not be used in the creation of the report." & vbCrLf
		}

		private string GetWhereConstraint(ref short intRow)
		{
			string GetWhereConstraint = "";
			// this will go to the grid and get the constraint for that row and return it
			// if there is none, then it will return a nullstring
			int lngType;
			string strTemp;
			strTemp = vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 1) + "|||" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 2);
			GetWhereConstraint = strTemp;
			return GetWhereConstraint;
		}

		private bool ItemInSortList(ref short intCounter)
		{
			bool ItemInSortList = false;
			// this will return true if the item has already been added to the Sort List
			int intTemp;
			ItemInSortList = false;
			for (intTemp = 0; intTemp <= lstSort.Items.Count - 1; intTemp++)
			{
				if (lstSort.ItemData(intTemp) == intCounter)
				{
					ItemInSortList = true;
					break;
				}
			}
			return ItemInSortList;
		}

		public void LoadSortList()
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			lstSort.Clear();
			lstSort.AddItem("Account Number");
			lstSort.ItemData(lstSort.NewIndex, 0);
			lstSort.AddItem("Name");
			lstSort.ItemData(lstSort.NewIndex, 1);
		}

		private void ShowAutomaticFields()
		{
			if (vsWhere.Row == lngRowReportType && vsWhere.Col == 1 && vsWhere.EditText != "")
			{
				if (vsWhere.EditText != "Regular")
				{
					// this is an Aged Report
					lblShowFields.Text = "Fields automatically included on report:" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Amount Due" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Payment Received" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Adjustments" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Balance Due" + "\r\n";
				}
				else
				{
					// this is a Regular Report
					lblShowFields.Text = "Fields automatically included on the Status List:" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Invoice Number" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Original Amount" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Payments / Adjustments" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Total Due" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Breakdown Of Amount Owed" + "\r\n";
				}
			}
			else
			{
				if (vsWhere.TextMatrix(lngRowReportType, 1) != "Regular")
				{
					// this is an Aged Report
					lblShowFields.Text = "Fields automatically included on report:" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Amount Due" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Payment Received" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Adjustments" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Balance Due" + "\r\n";
				}
				else
				{
					// this is a Regular Report
					lblShowFields.Text = "Fields automatically included on the Status List:" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Invoice Number" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Original Amount" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Payments / Adjustments" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Total Due" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Breakdown Of Amount Owed" + "\r\n";
				}
			}
		}

		private bool ValidateWhereGrid()
		{
			bool ValidateWhereGrid = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSwap = "";
				// lngRowReportType
				// lngRowWS
				// lngRowAccount
				if (Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 2)))
					{
						strSwap = vsWhere.TextMatrix(0, 1);
						vsWhere.TextMatrix(lngRowAccount, 1, vsWhere.TextMatrix(lngRowAccount, 2));
						vsWhere.TextMatrix(lngRowAccount, 2, strSwap);
					}
				}
				// lngRowName
				// lngRowRateKey
				if (Conversion.Val(vsWhere.TextMatrix(lngRowRateKey, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowRateKey, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowRateKey, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowRateKey, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowRateKey, 1);
						vsWhere.TextMatrix(lngRowRateKey, 1, vsWhere.TextMatrix(lngRowRateKey, 2));
						vsWhere.TextMatrix(lngRowRateKey, 2, strSwap);
					}
				}
				// lngRowBillType
				if (Conversion.Val(vsWhere.TextMatrix(lngRowBillType, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowBillType, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowBillType, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowBillType, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowBillType, 1);
						vsWhere.TextMatrix(lngRowBillType, 1, vsWhere.TextMatrix(lngRowBillType, 2));
						vsWhere.TextMatrix(lngRowBillType, 2, strSwap);
					}
				}
				// Show Payments from Date range
				modGlobal.Statics.gboolARSLDateRange = false;
				if (Strings.Trim(vsWhere.TextMatrix(lngRowShowPaymentFrom, 1)) != "")
				{
					// make sure that there is something in the field
					if (Information.IsDate(vsWhere.TextMatrix(lngRowShowPaymentFrom, 1)))
					{
						// make sure that it is a date
						modGlobal.Statics.gdtARSLPaymentDate1 = DateAndTime.DateValue(vsWhere.TextMatrix(lngRowShowPaymentFrom, 1));
						modGlobal.Statics.gboolARSLDateRange = true;
						if (Strings.Trim(vsWhere.TextMatrix(lngRowShowPaymentFrom, 2)) != "")
						{
							if (Information.IsDate(vsWhere.TextMatrix(lngRowShowPaymentFrom, 2)))
							{
								modGlobal.Statics.gdtARSLPaymentDate2 = DateAndTime.DateValue(vsWhere.TextMatrix(lngRowShowPaymentFrom, 2));
							}
							else
							{
								modGlobal.Statics.gdtARSLPaymentDate2 = DateTime.Today;
							}
						}
						else
						{
							modGlobal.Statics.gdtARSLPaymentDate2 = DateTime.Today;
						}
					}
				}
				ValidateWhereGrid = true;
				return ValidateWhereGrid;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				ValidateWhereGrid = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Where Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateWhereGrid;
		}

		public void Format_WhereGrid_2(bool boolReset)
		{
			Format_WhereGrid(boolReset);
		}

		public void Format_WhereGrid(bool boolReset = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCounter;
				int lngWid = 0;
				if (boolReset)
				{
					vsWhere.Rows = 0;
				}
				vsWhere.Rows = 10;
				vsWhere.Cols = 4;
				lngWid = vsWhere.WidthOriginal;
				vsWhere.ColWidth(0, FCConvert.ToInt32(lngWid * 0.38));
				vsWhere.ColWidth(1, FCConvert.ToInt32(lngWid * 0.285));
				vsWhere.ColWidth(2, FCConvert.ToInt32(lngWid * 0.285));
				vsWhere.ColWidth(3, 0);
				// Report Type (ie Regular or Aged Listing)
				vsWhere.TextMatrix(lngRowReportType, 0, "Report Type");
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowReportType, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// Account Range
				vsWhere.TextMatrix(lngRowAccount, 0, "Account");
				// Name Range
				vsWhere.TextMatrix(lngRowName, 0, "Name");
				// Invoice Range
				vsWhere.TextMatrix(lngRowRateKey, 0, "Rate Record");
				// Bill Type
				vsWhere.TextMatrix(lngRowBillType, 0, "Bill Type");
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowBillType, 2) = Me.BackColor
				// Balance Due
				vsWhere.TextMatrix(lngRowBalanceDue, 0, "Balance Due");
				// Group By
				vsWhere.TextMatrix(lngRowGroupBy, 0, "Group By");
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowGroupBy, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// Payment Type
				vsWhere.TextMatrix(lngRowPaymentType, 0, "Only Accounts With Type");
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowPaymentType, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// As Of Date
				vsWhere.TextMatrix(lngRowAsOfDate, 0, "As Of Date");
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowAsOfDate, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// Show Payment From
				vsWhere.TextMatrix(lngRowShowPaymentFrom, 0, "Show Payments From");
				// .Height = (.rows * .RowHeight(0)) + 70
				vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				// set the defaults
				if (boolReset)
				{
					vsWhere.TextMatrix(lngRowReportType, 1, "Regular");
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Where Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Fill_Lists()
		{
			// this routine will fill the extra fields and the sort lists
			lstSort.Clear();
			lstSort.AddItem("Account Number");
			lstSort.AddItem("Name");
			lstSort.SetSelected(1, true);
			// automatically select name
		}

		private void FillTypeList()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL;
				clsDRWrapper rsT = new clsDRWrapper();
				strSQL = "SELECT TypeCode, TypeTitle FROM DefaultBillTypes WHERE TypeTitle <> '' ORDER BY TypeCode";
				rsT.OpenRecordset(strSQL, modCLCalculations.strARDatabase);
				lstTypes.Clear();
				//FC:FINAL:DDU:#2149 - add column to display all content
				lstTypes.Columns.Add("");
				lstTypes.Columns[0].Width = FCConvert.ToInt32(lstTypes.Width * 0.25);
				lstTypes.Columns[1].Width = FCConvert.ToInt32(lstTypes.Width * 0.7);
				while (!rsT.EndOfFile())
				{
					lstTypes.AddItem(Strings.Format(rsT.Get_Fields_Int32("TypeCode"), "000") + "\t" + rsT.Get_Fields_String("TypeTitle"));
					lstTypes.ItemData(lstTypes.NewIndex, FCConvert.ToInt32(rsT.Get_Fields_Int32("TypeCode")));
					rsT.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Type List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
