﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomerMaster.
	/// </summary>
	public partial class frmCustomerMaster : BaseForm
	{
		public frmCustomerMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:AM:#i419 - moved code from load
			ClearScreen();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomerMaster InstancePtr
		{
			get
			{
				return (frmCustomerMaster)Sys.GetInstance(typeof(frmCustomerMaster));
			}
		}

		protected frmCustomerMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         8/5/03
		// This screen will be used to add or update voter information
		// ********************************************************
		public bool blnAlreadyAdded;
		public bool blnEdit;
		public int RecordNumber;
		public int FirstRecordShown;
		bool blnUnload;
		// vbPorter upgrade warning: TempKey As string	OnWriteFCConvert.ToInt32(
		string TempKey = "";
		int lngCurrentPartyID;
		clsDRWrapper rsCustInfo = new clsDRWrapper();

		private void cboStatus_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboStatus.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cmdEdit_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtCustomerNumber.Text)));
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID);
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID);
			}
		}

		private void frmCustomerMaster_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCustomerMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomerMaster.FillStyle	= 0;
			//frmCustomerMaster.ScaleWidth	= 9045;
			//frmCustomerMaster.ScaleHeight	= 7245;
			//frmCustomerMaster.LinkTopic	= "Form2";
			//frmCustomerMaster.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			//ClearScreen();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
		}

		private void frmCustomerMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (!modGlobal.Statics.blnFromBillPrep)
			{
				frmGetCustomerMaster.InstancePtr.Show(App.MainForm);
			}
			else
			{
				modGlobal.Statics.blnFromBillPrep = false;
				frmBillPrep.InstancePtr.Show(App.MainForm);
				if (TempKey != "")
				{
					frmBillPrep.InstancePtr.ReturnFromSearch(FCConvert.ToInt32(FCConvert.ToDouble(TempKey)));
				}
			}
		}

		private void Image1_Click(object sender, System.EventArgs e)
		{
			mnuFileComments_Click();
		}

		private void mnuFileBillScreen_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtCustomerNumber.Text) > 0)
			{
				frmCustomerBills.InstancePtr.lngCustomerID = RecordNumber;
				frmCustomerBills.InstancePtr.Show(App.MainForm);
				//frmCustomerBills.InstancePtr.ShowDialog();
			}
			else
			{
				MessageBox.Show("You must enter a customer before you may proceed to the bill screen.", "Enter Customer Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuFileComments_Click(object sender, System.EventArgs e)
		{
            if (!frmCommentOld.InstancePtr.Init("AR", "CustomerMaster", "Comment", "CustomerID",  RecordNumber,"Comment" ,"" ,0 ,false , true))
            {
                Image1.Visible = true;
            }
            else
            {
                Image1.Visible = false;
            }
        }

		public void mnuFileComments_Click()
		{
			mnuFileComments_Click(mnuFileComments, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessNextEntry_Click(object sender, System.EventArgs e)
		{
			if (RecordNumber == FirstRecordShown)
			{
				// move to next entry
				modGlobal.Statics.CustomerSearchResults.MoveFirst();
				if (FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID")) == FirstRecordShown)
				{
					modGlobal.Statics.CustomerSearchResults.MoveNext();
					modGlobal.Statics.CustomerSearchResults.MoveNext();
					if (modGlobal.Statics.CustomerSearchResults.EndOfFile() == true)
					{
						mnuProcessNextEntry.Enabled = false;
					}
					modGlobal.Statics.CustomerSearchResults.MovePrevious();
				}
				else
				{
					modGlobal.Statics.CustomerSearchResults.MoveNext();
					if (FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID")) == FirstRecordShown)
					{
						modGlobal.Statics.CustomerSearchResults.MoveNext();
						if (modGlobal.Statics.CustomerSearchResults.EndOfFile() == true)
						{
							mnuProcessNextEntry.Enabled = false;
						}
						modGlobal.Statics.CustomerSearchResults.MovePrevious();
						modGlobal.Statics.CustomerSearchResults.MovePrevious();
					}
					else
					{
						if (modGlobal.Statics.CustomerSearchResults.EndOfFile() == true)
						{
							mnuProcessNextEntry.Enabled = false;
						}
						modGlobal.Statics.CustomerSearchResults.MovePrevious();
					}
				}
			}
			else
			{
				modGlobal.Statics.CustomerSearchResults.MoveNext();
				if (FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID")) == FirstRecordShown)
				{
					modGlobal.Statics.CustomerSearchResults.MoveNext();
				}
				modGlobal.Statics.CustomerSearchResults.MoveNext();
				if (modGlobal.Statics.CustomerSearchResults.EndOfFile() != true)
				{
					if (FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID")) == FirstRecordShown)
					{
						modGlobal.Statics.CustomerSearchResults.MoveNext();
						if (modGlobal.Statics.CustomerSearchResults.EndOfFile() == true)
						{
							mnuProcessNextEntry.Enabled = false;
						}
						modGlobal.Statics.CustomerSearchResults.MovePrevious();
						modGlobal.Statics.CustomerSearchResults.MovePrevious();
					}
					else
					{
						if (modGlobal.Statics.CustomerSearchResults.EndOfFile() == true)
						{
							mnuProcessNextEntry.Enabled = false;
						}
						modGlobal.Statics.CustomerSearchResults.MovePrevious();
					}
				}
				else
				{
					mnuProcessNextEntry.Enabled = false;
					modGlobal.Statics.CustomerSearchResults.MovePrevious();
				}
			}
			if (mnuProcessPreviousEntry.Enabled == false)
			{
				mnuProcessPreviousEntry.Enabled = true;
			}
			RecordNumber = FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID"));
			ClearScreen();
			GetCustomerData();
		}

		private void mnuProcessPreviousEntry_Click(object sender, System.EventArgs e)
		{
			modGlobal.Statics.CustomerSearchResults.MovePrevious();
			// move to previous record
			if (modGlobal.Statics.CustomerSearchResults.BeginningOfFile() == true)
			{
				modGlobal.Statics.CustomerSearchResults.MoveNext();
				while (FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID")) != FirstRecordShown)
				{
					modGlobal.Statics.CustomerSearchResults.MoveNext();
				}
				mnuProcessPreviousEntry.Enabled = false;
			}
			else
			{
				if (FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID")) == FirstRecordShown)
				{
					modGlobal.Statics.CustomerSearchResults.MovePrevious();
					if (modGlobal.Statics.CustomerSearchResults.BeginningOfFile() == true)
					{
						modGlobal.Statics.CustomerSearchResults.MoveNext();
						mnuProcessPreviousEntry.Enabled = false;
					}
				}
			}
			if (mnuProcessNextEntry.Enabled == false)
			{
				mnuProcessNextEntry.Enabled = true;
			}
			ClearScreen();
			RecordNumber = FCConvert.ToInt32(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID"));
			GetCustomerData();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cboStatus.Focus();
			blnUnload = true;
			SaveInfo();
		}

		private void SaveInfo()
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngIDNumber;
			// vbPorter upgrade warning: lngRecordNumber As int	OnRead(string, int)
			int lngRecordNumber = 0;
			if (lngCurrentPartyID == 0)
			{
				MessageBox.Show("You must enter a customer number before you may proceed.", "Invalid Customer Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtCustomerNumber.Focus();
				return;
			}
			if (cboStatus.SelectedIndex == -1)
			{
				MessageBox.Show("You must enter an account status before you may proceed.", "Invalid Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cboStatus.Focus();
				return;
			}
			if (blnEdit || blnAlreadyAdded)
			{
				rsInfo.OpenRecordset("SELECT * FROM CustomerMaster WHERE CustomerID = " + lblVoterNumber.Text);
				rsInfo.Edit();
				rsInfo.Set_Fields("TimeUpdated", DateAndTime.TimeOfDay);
				rsInfo.Set_Fields("DateUpdated", DateTime.Today);
				rsInfo.Set_Fields("UpdatedBy", modGlobalConstants.Statics.gstrUserID);
				lngRecordNumber = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("CustomerID"));
			}
			else
			{
				rsInfo.OpenRecordset("SELECT TOP 1 * FROM CustomerMaster ORDER BY CustomerID DESC");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					lngRecordNumber = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("CustomerID")) + 1;
				}
				else
				{
					lngRecordNumber = 1;
				}
				if (modGlobal.Statics.blnFromBillPrep)
				{
					TempKey = FCConvert.ToString(lngRecordNumber);
				}
				rsInfo.AddNew();
				rsInfo.Set_Fields("TimeUpdated", DateAndTime.TimeOfDay);
				rsInfo.Set_Fields("DateUpdated", DateTime.Today);
				rsInfo.Set_Fields("UpdatedBy", modGlobalConstants.Statics.gstrUserID);
				rsInfo.Set_Fields("CustomerID", lngRecordNumber);
			}
			rsInfo.Set_Fields("Status", Strings.Left(cboStatus.Text, 1));
			rsInfo.Set_Fields("PartyID", lngCurrentPartyID);
			rsInfo.Set_Fields("DataEntryMessage", Strings.Trim(txtDataEntryMessage.Text));
			rsInfo.Set_Fields("BillMessage", Strings.Trim(txtBillMessage.Text));
			rsInfo.Set_Fields("Message", Strings.Trim(txtMessage.Text));
			if (chkOneTimeBillMessage.CheckState == Wisej.Web.CheckState.Checked)
			{
				rsInfo.Set_Fields("OneTimeBillMessage", true);
			}
			else
			{
				rsInfo.Set_Fields("OneTimeBillMessage", false);
			}
			if (chkSalesTax.CheckState == Wisej.Web.CheckState.Checked)
			{
				rsInfo.Set_Fields("SalesTax", true);
			}
			else
			{
				rsInfo.Set_Fields("SalesTax", false);
			}
			rsInfo.Update();
			if (!blnEdit)
			{
				if (blnAlreadyAdded)
				{
					MessageBox.Show("Save Successful", "Record Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("This account was saved as Account # " + FCConvert.ToString(lngRecordNumber), "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				MessageBox.Show("Save Successful", "Record Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			modRegistry.SaveRegistryKey("CURRCUST", FCConvert.ToString(lngRecordNumber));
			if (blnUnload)
			{
				Close();
			}
			else
			{
				if (!blnEdit)
				{
					lblVoterNumber.Text = FCConvert.ToString(lngRecordNumber);
					RecordNumber = lngRecordNumber;
					blnAlreadyAdded = true;
					mnuFileComments.Enabled = true;
					mnuFileBillScreen.Enabled = true;
				}
			}
		}

		private void GetCustomerData()
		{
			SetStatusCombo_2(modGlobal.Statics.CustomerSearchResults.Get_Fields_String("Status"));
			txtCustomerNumber.Text = FCConvert.ToString(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("PartyID"));
			lblCustomerInfo.Text = "";
			if (Strings.Trim(FCConvert.ToString(modGlobal.Statics.CustomerSearchResults.Get_Fields_String("Comment"))) != "")
			{
				Image1.Visible = true;
			}
			else
			{
				Image1.Visible = false;
			}
			txtDataEntryMessage.Text = FCConvert.ToString(modGlobal.Statics.CustomerSearchResults.Get_Fields_String("DataEntryMessage"));
			txtBillMessage.Text = FCConvert.ToString(modGlobal.Statics.CustomerSearchResults.Get_Fields_String("BillMessage"));
			txtMessage.Text = FCConvert.ToString(modGlobal.Statics.CustomerSearchResults.Get_Fields_String("Message"));
			if (FCConvert.ToBoolean(modGlobal.Statics.CustomerSearchResults.Get_Fields_Boolean("OneTimeBillMessage")))
			{
				chkOneTimeBillMessage.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkOneTimeBillMessage.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modGlobal.Statics.CustomerSearchResults.Get_Fields_Boolean("SalesTax")))
			{
				chkSalesTax.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			lblVoterNumber.Text = FCConvert.ToString(modGlobal.Statics.CustomerSearchResults.Get_Fields_Int32("CustomerID"));
		}

		private void ClearScreen()
		{
			cboStatus.SelectedIndex = 0;
			txtCustomerNumber.Text = "";
			lblCustomerInfo.Text = "";
			txtBillMessage.Text = "";
			txtDataEntryMessage.Text = "";
			chkOneTimeBillMessage.CheckState = Wisej.Web.CheckState.Unchecked;
			chkSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
			lblVoterNumber.Text = "";
			Image1.Visible = false;
		}

		public void SetStatusCombo_2(string strSearch)
		{
			SetStatusCombo(strSearch);
		}

		public void SetStatusCombo(string strSearch)
		{
			if (strSearch == "A")
			{
				cboStatus.SelectedIndex = 0;
			}
			else if (strSearch == "D")
			{
				cboStatus.SelectedIndex = 1;
			}
			else
			{
				cboStatus.SelectedIndex = -1;
			}
		}

		private void txtCustomerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCustomerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtCustomerNumber.Text) && Conversion.Val(txtCustomerNumber.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt16(FCConvert.ToDouble(txtCustomerNumber.Text)));
			}
			else
			{
				ClearPartyInfo();
			}
		}

		public void ClearPartyInfo()
		{
			lngCurrentPartyID = 0;
			lblCustomerInfo.Text = "";
			Image1.Visible = false;
			cmdEdit.Enabled = false;
		}

		public void GetPartyInfo(int intPartyID)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			FCCollection pComments = new FCCollection();
			pInfo = pCont.GetParty(intPartyID);
			if (!(pInfo == null))
			{
				lngCurrentPartyID = pInfo.ID;
				lblCustomerInfo.Text = pInfo.FullName;
				pAdd = pInfo.GetAddress("AR", intPartyID);
				if (!(pAdd == null))
				{
					lblCustomerInfo.Text = lblCustomerInfo.Text + "\r\n" + pAdd.GetFormattedAddress();
				}
				pComments = pInfo.GetModuleSpecificComments("AR");
				if (pComments.Count > 0)
				{
					Image1.Visible = true;
				}
				else
				{
					Image1.Visible = false;
				}
				cmdEdit.Enabled = true;
			}
		}
	}
}
