﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillVoid.
	/// </summary>
	public partial class frmBillVoid : BaseForm
	{
		public frmBillVoid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillVoid InstancePtr
		{
			get
			{
				return (frmBillVoid)Sys.GetInstance(typeof(frmBillVoid));
			}
		}

		protected frmBillVoid _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int BillKeyCol;
		int DateCol;
		int InvoiceCol;
		int AmountCol;
		int TypeCol;
		clsDRWrapper rsBills = new clsDRWrapper();
		string strVoidedBillsSQL;

		private void frmBillVoid_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBillVoid_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBillVoid.FillStyle	= 0;
			//frmBillVoid.ScaleWidth	= 5880;
			//frmBillVoid.ScaleHeight	= 4125;
			//frmBillVoid.LinkTopic	= "Form2";
			//frmBillVoid.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			BillKeyCol = 0;
			DateCol = 1;
			TypeCol = 3;
			InvoiceCol = 2;
			AmountCol = 4;
			vsBills.ColHidden(BillKeyCol, true);
			vsBills.ColFormat(AmountCol, "#,##0.00");
			vsBills.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBills.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBills.ColAlignment(InvoiceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBills.TextMatrix(0, DateCol, "Bill Date");
			vsBills.TextMatrix(0, TypeCol, "Bill Type");
			vsBills.TextMatrix(0, InvoiceCol, "Invoice #");
			vsBills.TextMatrix(0, AmountCol, "Amount");
			vsBills.ColWidth(DateCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.2));
			vsBills.ColWidth(TypeCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.4));
			vsBills.ColWidth(InvoiceCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.2));
			strVoidedBillsSQL = "(";
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			LoadCustomers();
			if (cboCustomers.Items.Count > 0)
			{
				cboCustomers.SelectedIndex = 0;
			}
		}

		private void frmBillVoid_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (cboCustomers.Visible == true)
				{
					Close();
				}
				else
				{
					lblCustomerSelect.Visible = true;
					cboCustomers.Visible = true;
					lblBillSelect.Visible = false;
					vsBills.Visible = false;
					cboCustomers.Focus();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int lngJournal = 0;
			clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
			clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
			if (strVoidedBillsSQL != "(")
			{
				strVoidedBillsSQL = Strings.Left(strVoidedBillsSQL, strVoidedBillsSQL.Length - 2) + ")";
				lngJournal = 0;
				modARCalculations.CreateJournalEntry_3(ref lngJournal, strVoidedBillsSQL);
				if (lngJournal == 0)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the Budgetary Journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					if (modSecurity.ValidPermissions_8(null, modGlobal.AUTOPOSTBILLINGJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptARBills))
					{
						if (MessageBox.Show("The reversing entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							clsJournalInfo = new clsPostingJournalInfo();
							clsJournalInfo.JournalNumber = lngJournal;
							clsJournalInfo.JournalType = "GJ";
							clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
							clsJournalInfo.CheckDate = "";
							clsPostInfo.AddJournal(clsJournalInfo);
							clsPostInfo.AllowPreview = true;
							clsPostInfo.WaitForReportToEnd = true;
							clsPostInfo.PostJournals();
						}
					}
					else
					{
						MessageBox.Show("The reversing entries have been saved into journal " + Strings.Format(lngJournal, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
		}

		private void frmBillVoid_Resize(object sender, System.EventArgs e)
		{
			vsBills.ColWidth(DateCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.2));
			vsBills.ColWidth(TypeCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.4));
			vsBills.ColWidth(InvoiceCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.2));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadCustomers()
		{
			clsDRWrapper rsCustomers = new clsDRWrapper();
			// rsCustomers.OpenRecordset "SELECT * FROM CustomerMaster WHERE Status = 'A' ORDER BY Name"
			rsCustomers.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsCustomers.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE c.Status = 'A' ORDER BY p.FullName");
			if (rsCustomers.EndOfFile() != true && rsCustomers.BeginningOfFile() != true)
			{
				do
				{
					cboCustomers.AddItem(rsCustomers.Get_Fields_String("FullName"));
					cboCustomers.ItemData(cboCustomers.NewIndex, FCConvert.ToInt32(rsCustomers.Get_Fields_Int32("CustomerID")));
					rsCustomers.MoveNext();
				}
				while (rsCustomers.EndOfFile() != true);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (cboCustomers.Visible)
			{
				rsBills.OpenRecordset("SELECT * FROM Bill WHERE BillStatus = 'A' AND AccountKey = " + FCConvert.ToString(cboCustomers.ItemData(cboCustomers.SelectedIndex)) + " AND IsNull(PrinPaid, 0) = 0 AND IsNull(IntPaid, 0) = 0 AND IsNull(TaxPaid, 0) = 0 ORDER BY BillNumber");
				if (rsBills.EndOfFile() != true && rsBills.BeginningOfFile() != true)
				{
					LoadGrid();
				}
				else
				{
					MessageBox.Show("No bills eligible to be voided were found for this customer", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else
			{
				if (vsBills.Row > 0)
				{
					VoidBill();
				}
			}
		}

		private void LoadGrid()
		{
			clsDRWrapper rsRateInfo = new clsDRWrapper();
			clsDRWrapper rsTypeInfo = new clsDRWrapper();
			vsBills.Rows = 1;
			do
			{
				rsRateInfo.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsBills.Get_Fields_Int32("BillNumber"));
				// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
				rsTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsBills.Get_Fields("BillType"));
				// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
				vsBills.AddItem(rsBills.Get_Fields_Int32("ID") + "\t" + Strings.Format(rsRateInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "\t" + rsBills.Get_Fields_String("InvoiceNumber") + "\t" + rsTypeInfo.Get_Fields_String("TypeTitle") + "\t" + Strings.Format(rsBills.Get_Fields_Decimal("PrinOwed") + rsBills.Get_Fields("IntAdded") + rsBills.Get_Fields_Decimal("TaxOwed"), "#,##0.00"));
				rsBills.MoveNext();
			}
			while (rsBills.EndOfFile() != true);
			lblCustomerSelect.Visible = false;
			cboCustomers.Visible = false;
			lblBillSelect.Visible = true;
			vsBills.Visible = true;
			vsBills.Row = 1;
			vsBills.Focus();
		}

		private void vsBills_DblClick(object sender, System.EventArgs e)
		{
			if (vsBills.Row > 0)
			{
				VoidBill();
			}
		}

		private void vsBills_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (vsBills.Row > 0)
			{
				if (e.KeyCode == Keys.Return)
				{
					//FC:TODO:AM
					//e.KeyCode = 0;
					VoidBill();
				}
			}
		}

		private void VoidBill()
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsDelete = new clsDRWrapper();
			ans = MessageBox.Show("Once you void this bill you will not be able to get the information back.  Are you sure you wish to do this?", "Void Bill?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.Yes)
			{
				rsDelete.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + vsBills.TextMatrix(vsBills.Row, BillKeyCol));
				if (rsDelete.EndOfFile() != true && rsDelete.BeginningOfFile() != true)
				{
					MessageBox.Show("You may not void this bill because there are already payments for it.", "Unable to Void", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else
				{
					rsDelete.OpenRecordset("SELECT * FROM Bill WHERE ID = " + vsBills.TextMatrix(vsBills.Row, BillKeyCol));
					rsDelete.Edit();
					rsDelete.Set_Fields("BillStatus", "V");
					rsDelete.Update();
					if (modGlobalConstants.Statics.gboolBD)
					{
						strVoidedBillsSQL += FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vsBills.TextMatrix(vsBills.Row, BillKeyCol)))) + ", ";
					}
					modGlobalFunctions.AddCYAEntry_8("AR", "Void of Bill " + vsBills.TextMatrix(vsBills.Row, BillKeyCol));
					vsBills.RemoveItem(vsBills.Row);
					MessageBox.Show("Bill Voided Successfully!", "Bill Voided", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
