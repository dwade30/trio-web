﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using Global;
using SharedApplication;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWAR0000.Receipting
{
	/// <summary>
	/// Summary description for frmARPaymentStatus.
	/// </summary>
	public partial class frmARPaymentStatus : BaseForm, IView<IAccountsReceivablePaymentViewModel>
	{
		public frmARPaymentStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblCode = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label1.AddControlArrayElement(Label1_9, FCConvert.ToInt16(9));
			this.Label1.AddControlArrayElement(Label1_11, FCConvert.ToInt16(11));
			this.Label1.AddControlArrayElement(Label1_1, FCConvert.ToInt16(1));
			this.Label1.AddControlArrayElement(Label1_10, FCConvert.ToInt16(10));
			this.Label1.AddControlArrayElement(Label1_8, FCConvert.ToInt16(8));
			this.Label1.AddControlArrayElement(Label1_12, FCConvert.ToInt16(12));
			this.lblCode.AddControlArrayElement(lblCode_2, FCConvert.ToInt16(2));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmARPaymentStatus InstancePtr
		{
			get
			{
				return (frmARPaymentStatus)Sys.GetInstance(typeof(frmARPaymentStatus));
			}
		}

		protected frmARPaymentStatus _InstancePtr = null;

		private IAccountsReceivablePaymentViewModel viewModel { get; set; }
		private bool cancelling = true;

		int ParentLine;

		string strStatusString = "";
		string strPaymentString = "";
		
		//clsGridAccount clsAcct = new clsGridAccount();
		GridAccountUtility clsAcct = new GridAccountUtility();
		public int lngGRIDCOLTree;
		public int lngGRIDColInvoiceNumber;
		public int lngGridColBillKey;
		public int lngGRIDColRateKey;
		public int lngGRIDColDate;
		public int lngGRIDColRef;
		public int lngGRIDColPaymentCode;
		public int lngGRIDColPrincipal;
		public int lngGRIDColPTC;
		public int lngGRIDColTax;
		public int lngGRIDColInterest;
		public int lngGRIDColTotal;
		public int lngGRIDColLineCode;
		public int lngGRIDColPending;
		public int lngGRIDColPaymentKey;
		public int lngGRIDColCHGINTNumber;
		public int lngGRIDColPerDiem;

		public int lngPayGridColInvoice;
		public int lngPayGridColBillID;
		public int lngPayGridColDate;
		public int lngPayGridColRef;
		public int lngPayGridColCode;
		public int lngPayGridColCDAC;
		public int lngPayGridColPrincipal;
		public int lngPayGridColTax;
		public int lngPayGridColInterest;
		public int lngPayGridColArrayIndex;
		public int lngPayGridColKeyNumber;
		public int lngPayGridColCHGINTNumber;
		public int lngPayGridColTotal;

		decimal sumPrin;
		decimal sumTax;
		decimal sumInt;
		decimal sumTotal;

		public frmARPaymentStatus(IAccountsReceivablePaymentViewModel viewModel) : this()
		{
			lngGRIDCOLTree = 0;
			lngGRIDColInvoiceNumber = 1;
			lngGridColBillKey = 2;
			lngGRIDColRateKey = 3;
			lngGRIDColDate = 4;
			lngGRIDColRef = 5;
			lngGRIDColPaymentCode = 6;
			lngGRIDColPrincipal = 7;
			lngGRIDColPTC = 8;
			lngGRIDColTax = 9;
			lngGRIDColInterest = 10;
			lngGRIDColTotal = 11;
			lngGRIDColLineCode = 12;
			lngGRIDColPaymentKey = 13;
			lngGRIDColCHGINTNumber = 14;
			lngGRIDColPerDiem = 15;
			lngGRIDColPending = 16;
			lngPayGridColInvoice = 0;
			lngPayGridColBillID = 1;
			lngPayGridColDate = 2;
			lngPayGridColRef = 3;
			lngPayGridColCode = 4;
			lngPayGridColCDAC = 5;
			lngPayGridColPrincipal = 6;
			lngPayGridColTax = 7;
			lngPayGridColInterest = 8;
			lngPayGridColArrayIndex = 9;
			lngPayGridColKeyNumber = 10;
			lngPayGridColCHGINTNumber = 11;
			lngPayGridColTotal = 12;

			clsAcct.GRID7Light = txtAcctNumber;
			clsAcct.DefaultAccountType = "G";
			clsAcct.AccountCol = -1;
			
			strStatusString = "&Go To Status";
			strPaymentString = "&Go To Payments/Adj";

			GRID.AddExpandButton();
			GRID.RowHeadersWidth = 10;

			this.Resize += new System.EventHandler(this.frmARPaymentStatus_Resize);
			viewModel.AccountChanged += ViewModel_AccountChanged;
			viewModel.PendingPaymentAdded += Viewmodel_PendingPaymentAdded;

			Format_PaymentGrid();

			this.ViewModel = viewModel;
		}

		private void Viewmodel_PendingPaymentAdded(object sender, ARPayment payment)
		{
			AddPaymentRow(payment);
		}

		private void ViewModel_AccountChanged(object sender, EventArgs e)
		{
			SetupView();
		}

		public IAccountsReceivablePaymentViewModel ViewModel
		{
			get => viewModel;
			set => viewModel = value;
		}

		public void SetupView()
		{
			mnuProcess.Enabled = false;
			if (viewModel.StartedFromCashReceipts || viewModel.IsCashReceiptsActive())
			{
				mnuProcessGoTo.Enabled = false;
				mnuProcessGoTo.Visible = false;
			}

			if (viewModel.Customer == null)
			{
				viewModel.Cancel();
				frmWait.InstancePtr.Unload();
				MessageBox.Show("No billing records exist for this account.", "No Billing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}

			viewModel.DontFireCollapse = true;

			StartUp();

			viewModel.DontFireCollapse = false;

			if (viewModel.IsPaymentScreen)
			{
				FillARCodeCombo();
				ResetARPaymentFrame();
				fraPayment.Visible = true;
				Format_PaymentGrid();
				if (txtReference.Enabled && txtReference.Visible)
				{
					txtReference.Focus();
				}
				mnuPaymentSave.Enabled = true;
				mnuPaymentSaveExit.Enabled = true;
				cmdPaymentSave.Enabled = true;
				cmdProcess.Enabled = true;
				cmdProcess.Visible = true;
				Format_ARvsPeriod();
			}
			else
			{
				fraPayment.Visible = false;
				cmdProcess.Visible = false;
				GRID.Visible = true;
				if (GRID.Visible && GRID.Enabled)
				{
					GRID.Focus();
				}
			}
			
			if (viewModel.IsPaymentScreen)
			{
				mnuPayment.Visible = true;
				mnuProcessGoTo.Text = strStatusString;
				vsPayments.Visible = true;
				lblPaymentInfo.Visible = true;
				if (viewModel.NegativeBillsExist)
				{
					MessageBox.Show("You have some years with negative balances.  Press F11 to apply the appropriate entries.", "Negative Bill Balances", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				lblPaymentInfo.Visible = false;
				mnuPayment.Visible = false;
				mnuProcessGoTo.Text = strPaymentString;
			}
			mnuProcess.Enabled = true;
			frmWait.InstancePtr.Unload();
			if (viewModel.AccountComment != null)
			{
				if (viewModel.PopUpComment())
				{
					MessageBox.Show(
						"Account " + viewModel.Customer.CustomerId + " comment." + "\r\n" +
						viewModel.AccountComment.Text.Trim(), "Account Note");
				}
				imgNote.Visible = true;
				mnuFileEditNote.Visible = true;
				mnuFilePriority.Visible = true;
			}
			else
			{
				imgNote.Visible = false;
				mnuFilePriority.Visible = false;
			}
		}

		public void Format_ARvsPeriod()
		{
			int wid = 0;
			wid = vsPeriod.WidthOriginal;
			vsPeriod.ColWidth(0, FCConvert.ToInt32(wid * 0.34));
			vsPeriod.ColWidth(1, FCConvert.ToInt32(wid * 0.61));
			vsPeriod.ColWidth(2, 0);
			vsPeriod.ColWidth(3, 0);
			vsPeriod.ColWidth(4, 0);
		}

		private void FillARCodeCombo()
		{
			cmbCode.Clear();
			cmbCode.AddItem("Y   - PrePayment");
			cmbCode.AddItem("C   - Correction");
			cmbCode.AddItem("P   - Regular Payment");
		}

		public void ResetARPaymentFrame()
		{
			ResetARComboBoxes();
			txtTransactionDate.Text = viewModel.EffectiveDate.ToString("MM/dd/yyyy");
			txtReference.Text = "";
			txtPrincipal.Text = Strings.Format(0, "#,##0.00");
			txtInterest.Text = Strings.Format(0, "#,##0.00");
			txtTax.Text = Strings.Format(0, "#,##0.00");
			txtComments.Text = "";
		}

		public void ResetARComboBoxes()
		{
			int lngBill;
			int intCT;
			txtCD.Text = "Y";
			txtCash.Text = "Y";

			for (intCT = 0; intCT <= cmbCode.Items.Count - 1; intCT++)
			{
				if ("P" == cmbCode.Items[intCT].ToString().Left(1))
				{
					cmbCode.SelectedIndex = intCT;
					break;
				}
			}

			FillARInvoiceNumber();
		}

		private void FillARInvoiceNumber()
		{
			bool boolAddPrePayLine = true;

			cmbBillNumber.Clear();

			foreach (var bill in viewModel.Bills)
			{
				cmbBillNumber.AddItem(bill.InvoiceNumber);
				if (bill.InvoiceNumber == "0")
				{
					boolAddPrePayLine = false;
				}
			}

			if (boolAddPrePayLine)
			{
				cmbBillNumber.AddItem("0*");
			}
			cmbBillNumber.AddItem("Auto");
			cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
		}

		private void chkShowAll_CheckedChanged(object sender, System.EventArgs e)
		{
			viewModel.ShowAllBills = chkShowAll.Checked;
			StartUp();
			if (viewModel.IsPaymentScreen)
			{
				FillARInvoiceNumber();
			}
		}

		private void cmbCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strTemp;

			strTemp = cmbCode.Items[cmbCode.SelectedIndex].ToString().Left(1);
			if (ARYearCodeValidate())
			{
				if (strTemp == "P")
				{
					txtCash.Text = "Y";
					txtCD.Text = "Y";
					txtAcctNumber.TextMatrix(0, 0, "");
					txtAcctNumber.Enabled = false;
					InputBoxAdjustment(true);
				}
				else if (strTemp == "C")
				{
					InputBoxAdjustment(false);
				}
				else if (strTemp == "Y")
				{
					if (Strings.Right(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString(), 1) != "*" && Conversion.Val(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString()) != 0)
					{
						cmbCode.SelectedIndex = 1;
					}
					else
					{
						txtCash.Text = "Y";
						txtCD.Text = "Y";
						txtAcctNumber.TextMatrix(0, 0, "");
						txtAcctNumber.Enabled = false;
					}
					InputBoxAdjustment(true);
				}
				else
				{
					txtCash.Text = "Y";
					txtCD.Text = "Y";
					txtAcctNumber.TextMatrix(0, 0, "");
					txtAcctNumber.Enabled = false;
					InputBoxAdjustment(true);
				}
			}
			else
			{
				cmbCode.SelectedIndex = 1;
			}
			CheckCash();
		}

		public bool ARYearCodeValidate()
		{
			if (cmbBillNumber.SelectedIndex >= 0 && cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString().Left(4) == "Auto")
			{
				if (cmbCode.Items[cmbCode.SelectedIndex].ToString().Left(1) != "P")
				{
					return false;
				}
			}

			return true;
		}
		
		private void cmbCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

			switch (KeyCode)
			{
				case Keys.Left:
					{
						txtReference.Focus();
						break;
					}
				case Keys.Right:
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
						break;
					}
			}
		}

		private void cmbBillNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strInvoice = "";


			if (cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString() == "Auto")
			{
				if (!ARYearCodeValidate())
				{
					cmbCode.SelectedIndex = 1;
				}
				vsPeriod.TextMatrix(0, 0, "Total Due:");
				vsPeriod.TextMatrix(0, 1, txtTotalTotal.Text);
				vsPeriod.TextMatrix(0, 2, "");
				vsPeriod.TextMatrix(0, 3, "");
				vsPeriod.TextMatrix(0, 4, "");
			}
			else
			{
				strInvoice = cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString();
				if (NextSpacerLine(GRID.FindRow(strInvoice, 1, lngGRIDColInvoiceNumber)) - 1 > 0)
				{
					var bill = viewModel.GetBill(strInvoice);
					if (bill != null)
					{
						vsPeriod.TextMatrix(0, 0, "Total Due:");
						vsPeriod.TextMatrix(0, 1, Strings.Format(bill.BalanceRemaining, "#,##0.00"));
					}
				}
			}
			CheckCash();
		}

		public void cmbBillNumber_Click()
		{
			cmbBillNumber_SelectedIndexChanged(cmbBillNumber, new System.EventArgs());
		}

		private void cmdRIClose_Click(object sender, System.EventArgs e)
		{
			fraRateInfo.Visible = false;
		}

		private void frmARPaymentStatus_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void frmARPaymentStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Return:
					{
						break;
					}
				case Keys.Escape:
					{
						if (fraRateInfo.Visible == true)
						{
							cmdRIClose_Click(cmdRIClose, new System.EventArgs());
						}
						else
						{
							Close();
						}
						break;
					}
				case Keys.F10:
					{
						break;
					}
				case Keys.Insert:
					{
						if (viewModel.IsPaymentScreen && cmbBillNumber.SelectedIndex >= 0)
						{
							if (cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString() == "Auto")
							{
								CreateAROppositionLine(GRID.Rows - 1);
							}
							else
							{
								CreateAROppositionLine(NextSpacerLine(GRID.FindRow(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString(), 1, lngGRIDColInvoiceNumber)) - 1);
							}
						}
						break;
					}
			}
		}

		public void CreateAROppositionLine(int Row, bool boolReversal = false)
		{
			int lngBill;
			string strCode = "";
			string strPaymentCode = "";
			bool boolGrandTotal = false;
			decimal dblP = 0;
			decimal dblTax = 0;
			decimal dblInt = 0;

			viewModel.ChargedInterestToReverse = null;

			if (Row > 0)
			{
				boolGrandTotal = GRID.RowOutlineLevel(Row) == 0 || (GRID.RowOutlineLevel(Row) == 2 && GRID.TextMatrix(Row, lngGRIDColRef).Trim() == "Total");

				var payment = viewModel.GetPayment(GRID.TextMatrix(Row, lngGRIDColPaymentKey).ToIntegerValue());
				if (boolReversal && payment == null)
				{
					MessageBox.Show("You can't reverse charged interest.  If you need this removed you must reverse the payment record that caused the charged interest to be created.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}

				string search = "";

				if (Row == GRID.Rows - 1)
				{
					search = "Auto";
				}
				else
				{
					search = GRID.TextMatrix(LastParentRow(Row), lngGRIDColInvoiceNumber).Trim();
				}

				for (int intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
				{
					if (search == cmbBillNumber.Items[intCT].ToString().Trim().Replace("*", ""))
					{
						cmbBillNumber.SelectedIndex = intCT;
						break;
					}
				}
				
				if (boolGrandTotal)
				{
					lngBill = 0;
					strCode = "=";
					strPaymentCode = "P";
				}
				else
				{
					lngBill = GRID.TextMatrix(LastParentRow(Row), lngGridColBillKey).ToIntegerValue();
					strCode = GRID.TextMatrix(Row, lngGRIDColLineCode);
					strPaymentCode = GRID.TextMatrix(Row, lngGRIDColPaymentCode);
				}
				
				if (!boolReversal)
				{
					txtCD.Text = "Y";
					txtCash.Text = "Y";
				}
				else
				{
					if (payment != null)
					{
						if (payment.CashDrawer == "Y" && payment.DailyCloseOut == 0)
						{
							txtCD.Text = "Y";
							txtCash.Text = "Y";
						}
						else
						{
							txtCD.Text = "N";
							if (payment.GeneralLedger == "N")
							{
								txtCash.Text = "N";
								txtAcctNumber.TextMatrix(0, 0, payment.BudgetaryAccountNumber);
							}
							else
							{
								txtCash.Text = "Y";
							}
						}
					}
				}

				txtTransactionDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
				if (GRID.TextMatrix(Row, lngGRIDColRef).Trim() != "Total" && GRID.TextMatrix(Row, lngGRIDColRef).Trim() != "CURINT" && GRID.RowOutlineLevel(Row) != 0)
				{
					txtReference.Text = "REVERS";
				}
				else
				{
					txtReference.Text = "";
				}

				if (strPaymentCode == "P")
				{
					for (int intCT = 0; intCT <= cmbCode.Items.Count - 1; intCT++)
					{
						if (boolGrandTotal)
						{
							if ("P" == cmbCode.Items[intCT].ToString().Left(1))
							{
								cmbCode.SelectedIndex = intCT;
								break;
							}
						}
						else
						{
							if ("C" == cmbCode.Items[intCT].ToString().Left(1))
							{
								cmbCode.SelectedIndex = intCT;
								break;
							}
						}
					}

					if (!boolGrandTotal)
					{
						if (viewModel.ChargedInterestExistsOnBill(payment.BillId))
						{
							if (viewModel.GetBill(payment.BillId).PreviousInterestDate == null)
							{
								MessageBox.Show("This transaction contains no previous interest date information, please correct manually.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}

							if (!viewModel.LatestPaymentOnBill(payment))
							{
								MessageBox.Show(
									"Only the last payment applied can be reversed when interest was charged.", "ERROR",
									MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
						}

						if (payment.ChargedInterestRecord != null)
						{
							viewModel.ChargedInterestToReverse = payment.ChargedInterestRecord;
						}
					}
				}
				else 
				{
					strPaymentCode = "C";
					for (int intCT = 0; intCT <= cmbCode.Items.Count - 1; intCT++)
					{
						if (strPaymentCode == cmbCode.Items[intCT].ToString().Left(1))
						{
							cmbCode.SelectedIndex = intCT;
							break;
						}
					}
				}
				
				if (strCode == "=" || strCode == "-1")
				{
					dblP = GRID.TextMatrix(Row, lngGRIDColPrincipal).ToDecimalValue();
					dblTax = GRID.TextMatrix(Row, lngGRIDColTax).ToDecimalValue();
					dblInt = GRID.TextMatrix(Row, lngGRIDColInterest).ToDecimalValue();
					if (strPaymentCode != "C")
					{
						dblInt = dblP + dblInt + dblTax;
						dblTax = 0;
						dblP = 0;
					}
					txtComments.Text = "";
				}
				else
				{
					dblP = GRID.TextMatrix(Row, lngGRIDColPrincipal).ToDecimalValue() * -1;
					dblTax = GRID.TextMatrix(Row, lngGRIDColTax).ToDecimalValue() * -1;
					dblInt = GRID.TextMatrix(Row, lngGRIDColInterest).ToDecimalValue() * -1;
					if (strPaymentCode == "Y")
					{
						dblInt = dblP + dblInt + dblTax;
						dblP = 0;
						dblTax = 0;
					}
					txtComments.Text = "";
				}

				if (strCode == "=")
				{
					if (!boolGrandTotal)
					{
						for (int intCT = 1; intCT <= vsPayments.Rows - 1; intCT++)
						{
							if (vsPayments.TextMatrix(intCT, lngPayGridColBillID).ToIntegerValue() == lngBill)
							{
								if (vsPayments.TextMatrix(intCT, lngPayGridColCode) == "P")
								{
									dblP -= vsPayments.TextMatrix(intCT, lngPayGridColPrincipal).ToDecimalValue();
									dblTax -= vsPayments.TextMatrix(intCT, lngPayGridColTax).ToDecimalValue();
									dblInt -= vsPayments.TextMatrix(intCT, lngPayGridColInterest).ToDecimalValue();
								}
								else
								{
									dblP += vsPayments.TextMatrix(intCT, lngPayGridColPrincipal).ToDecimalValue();
									dblTax += vsPayments.TextMatrix(intCT, lngPayGridColTax).ToDecimalValue();
									dblInt += vsPayments.TextMatrix(intCT, lngPayGridColInterest).ToDecimalValue();
								}
							}
						}
					}
				}

				txtPrincipal.Text = Strings.Format(dblP, "#,##0.00");
				txtTax.Text = Strings.Format(dblTax, "#,##0.00");
				txtInterest.Text = Strings.Format(dblInt, "#,##0.00");
			}
		}

		private void FillTotalPayment()
		{
			SetBillCombo("Auto");
			txtCD.Text = "Y";
			txtCash.Text = "Y";
			txtReference.Text = "";
			txtTransactionDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
			SetCodeCombo("P");
			txtComments.Text = "";
			txtPrincipal.Text = Strings.Format(0, "#,##0.00");
			txtTax.Text = Strings.Format(0, "#,##0.00");
			txtInterest.Text = Strings.Format(txtTotalTotal.Text.ToDecimalValue(), "#,##0.00");
		}

		private void SetBillCombo(string bill)
		{
			for (int intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
			{
				if (bill == cmbBillNumber.Items[intCT].ToString().Trim().Replace("*", ""))
				{
					cmbBillNumber.SelectedIndex = intCT;
					break;
				}
			}
		}

		private void SetCodeCombo(string code)
		{
			for (int intCT = 0; intCT <= cmbCode.Items.Count - 1; intCT++)
			{
				if (code == cmbCode.Items[intCT].ToString().Left(1))
				{
					cmbCode.SelectedIndex = intCT;
					break;
				}
			}
		}

		private void frmARPaymentStatus_Resize(object sender, System.EventArgs e)
		{
			Format_Grid();
			AutoSize_GRID();
			Format_PaymentGrid();
		}

		private bool StartUp()
		{
			try
			{
				sumPrin = 0;
				sumInt = 0;
				sumTax = 0;
				sumTotal = 0;

				this.Text = "Accounts Receivable Status";

				GRID.Rows = 1;
				Format_Grid();
				GRID.Redraw = false;
				GRID.Visible = false;

				FillInARInfo();

				if (viewModel.Bills.Count() != 0)
				{
					foreach (var bill in viewModel.Bills)
					{
						FillGrid(bill);
					}

					if (GRID.Rows > 1)
					{
						GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, GRID.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						GRID_Coloring();
						GRID.Select(0, lngGRIDColInvoiceNumber, 0, lngGRIDColTotal);
						GRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
						Create_Account_Totals();
						GRID.Redraw = true;
						GRID.Visible = true;
						GRID.Refresh();
					}


					if (viewModel.IsPaymentScreen)
					{
						if (GRID.Rows > 1)
						{
							GridPanel.Width = this.ClientArea.Width - this.GridPanel.Left - 30;
							fraPayment.Width = this.ClientArea.Width - this.fraPayment.Left - 30;
							GRID.Visible = true;
							AutoSize_GRID();
						}
					}
					else
					{
						GridPanel.Width = this.ClientArea.Width - this.GridPanel.Left - 30;
						AutoSize_GRID();
					}

					Refresh();
					
					if ((viewModel.GroupInfo?.Comment?.Trim() ?? "") != "")
						MessageBox.Show(viewModel.GroupInfo?.Comment?.Trim(), "Group " + viewModel.GroupInfo?.GroupNumber + " Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, GRID.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					GRID_Coloring();
					GRID.Select(0, lngGRIDColInvoiceNumber, 0, lngGRIDColTotal);
					GRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
					Create_Account_Totals();
					GRID.Redraw = true;
					GRID.Visible = true;
					GRID.Refresh();
				}

				GRID.Outline(1);

				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Startup", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return false;
			}
		}

		private void CreateMasterLine(ARBill bill)
		{
			try
			{
				Add(0);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPaymentKey, bill.Id);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColInvoiceNumber, bill.BillNumber);
				if (bill.BillNumber == 0)
				{
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColDate, "No RK");
				}
				else
				{
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColDate, Strings.Format(bill.BillDate, "MM/dd/yy"));
				}
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPending, " ");
				ParentLine = GRID.Rows - 1;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Master Line", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillGrid(ARBill bill)
		{
			string nameString = "";
			decimal currentInterest = 0;

			CreateMasterLine(bill);

			if (bill.Bname2.Trim() != "")
			{
				nameString = bill.Bname.Trim() + " & " + bill.Bname2.Trim();
			}
			else
			{
				nameString = bill.Bname.Trim();
			}
			if (nameString.ToUpper() != lblOwnersName.Text.Trim().ToUpper())
			{
				Add(1);

				GRID.MergeRow(GRID.Rows - 1, true, lngGRIDColPaymentCode, lngGRIDColInterest);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColRef, "Billed To:");
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPaymentCode, nameString);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, GRID.Rows - 1, lngGRIDColPaymentCode, GRID.Rows - 1, lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPending, " ");
			}

			GRID.TextMatrix(ParentLine, lngGridColBillKey, bill.Id);
			GRID.TextMatrix(ParentLine, lngGRIDColRateKey, bill.BillNumber);
			GRID.TextMatrix(ParentLine, lngGRIDColInvoiceNumber, bill.InvoiceNumber);
			GRID.TextMatrix(ParentLine, lngGRIDColRef, "Original");
			GRID.TextMatrix(ParentLine, lngGRIDColPrincipal, bill.PrinOwed);
			GRID.TextMatrix(ParentLine, lngGRIDColTax, bill.TaxOwed);
			GRID.TextMatrix(ParentLine, lngGRIDColInterest, "0.00");
			GRID.TextMatrix(ParentLine, lngGRIDColPTC, bill.PrinOwed + bill.TaxOwed);
			GRID.TextMatrix(ParentLine, lngGRIDColTotal, bill.PrinOwed + bill.TaxOwed);
			GRID.TextMatrix(ParentLine, lngGRIDColLineCode, "+");

			PaymentRecords(bill);

			GRID.TextMatrix(ParentLine, lngGRIDColPerDiem, FCConvert.ToString(bill.PerDiem));

			if (bill.CurrentInterest - bill.PendingChargedInterest != 0)
			{
				currentInterest = bill.CurrentInterest - bill.PendingChargedInterest;

				Add(1);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColInvoiceNumber, "");
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColDate, "");
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPending, " ");
				if (currentInterest < 0)
				{
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColRef, "CURINT");
				}
				else
				{
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColRef, "EARNINT");
				}
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPrincipal, 0);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPTC, 0);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColTax, 0);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColInterest, currentInterest);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColTotal, currentInterest);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColLineCode, "-");
			}

			Add(1);
			int CurrentRow = GRID.Rows - 1;
			GRID.TextMatrix(CurrentRow, lngGRIDColPending, " ");
			Add(1);
			GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPending, " ");
			GRID.TextMatrix(CurrentRow, lngGRIDColLineCode, "=");
			GRID.TextMatrix(CurrentRow, lngGRIDColRef, "Total");
			int last = LastParentRow(CurrentRow);
			for (int col = lngGRIDColPrincipal; col <= lngGRIDColTotal; col++)
			{
				decimal sum = 0;
				for (int sumRow = last; sumRow <= CurrentRow - 1; sumRow++)
				{
					if (GRID.TextMatrix(sumRow, lngGRIDColLineCode) == "+")
					{
						sum = sum + GRID.TextMatrix(sumRow, col).ToDecimalValue();
					}
					else if (GRID.TextMatrix(sumRow, lngGRIDColLineCode) == "-")
					{
						sum = sum - GRID.TextMatrix(sumRow, col).ToDecimalValue();
					}
					else if (FCConvert.ToBoolean(GRID.TextMatrix(sumRow, lngGRIDColLineCode)))
					{
						sum = GRID.TextMatrix(sumRow, col).ToDecimalValue();
					}
				}
				GRID.TextMatrix(CurrentRow, col, sum);
				GRID.TextMatrix(GRID.Rows - 1, col, sum);
			}

			GRID.TextMatrix(GRID.Rows - 1, lngGRIDColDate, GRID.TextMatrix(ParentLine, lngGRIDColDate));
			GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPaymentKey, bill.BillNumber);
			if (GRID.TextMatrix(ParentLine, lngGRIDColRef) == "Original")
			{
				SwapRows(ParentLine, NextSpacerLine(ParentLine));
			}

			GRID.Select(0, 0);
		}

		private void Format_Grid()
		{
			int wid;
			GRID.Cols = lngGRIDColPending + 1;
			GridPanel.Width = this.ClientArea.Width - this.GridPanel.Left - 30;
			if (viewModel.IsPaymentScreen)
			{
				GridPanel.Height = FCConvert.ToInt32(frmARPaymentStatus.InstancePtr.Height * 0.40);
			}
			else
			{
				//fraStatusLabels.Visible = true;
				GridPanel.Height = FCConvert.ToInt32((frmARPaymentStatus.InstancePtr.ClientArea.Height - GridPanel.Top) * 0.96);
			}

			GRID.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
			GRID.TextMatrix(0, lngGRIDColInvoiceNumber, "Invoice");
			GRID.TextMatrix(0, lngGRIDColDate, "Date");
			GRID.TextMatrix(0, lngGRIDColRef, "Ref");
			GRID.TextMatrix(0, lngGRIDColPaymentCode, "C");
			GRID.TextMatrix(0, lngGRIDColPrincipal, "Principal");
			GRID.TextMatrix(0, lngGRIDColPTC, "PTC");
			GRID.TextMatrix(0, lngGRIDColTax, "Tax");
			GRID.TextMatrix(0, lngGRIDColInterest, "Interest");
			GRID.TextMatrix(0, lngGRIDColTotal, "Total");
			AutoSize_GRID();
			GRID.ColFormat(lngGRIDColDate, "MM/dd/yy");
			GRID.ColFormat(lngGRIDColPrincipal, "#,##0.00");
			GRID.ColFormat(lngGRIDColPTC, "#,##0.00");
			GRID.ColFormat(lngGRIDColTax, "#,##0.00");
			GRID.ColFormat(lngGRIDColInterest, "#,##0.00");
			GRID.ColFormat(lngGRIDColTotal, "#,##0.00");
			GRID.ColAlignment(lngGRIDColInvoiceNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GRID.ColAlignment(lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GRID.ColAlignment(lngGRIDColRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);

			GRID.ColAlignment(lngGRIDColPTC, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.ColAlignment(lngGRIDColTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.ColAlignment(lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.ColAlignment(lngGRIDColPrincipal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.ColAlignment(lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPrincipal, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPTC, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColRef, 0, lngGRIDColPaymentCode, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColDate, 0, lngGRIDColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);

			vsPayments.Width = GRID.Width - 20;
			fraPayment.Width = this.ClientArea.Width - this.fraPayment.Left - 30;
			fraPayment.Top = GridPanel.Bottom;
			
			this.txtComments.Width = (this.vsPayments.Left + this.vsPayments.Width) - this.txtComments.Left;
			this.txtTotalPendingDue.Left = (this.vsPayments.Left + this.vsPayments.Width) - this.txtTotalPendingDue.Width;
			this.lblTotalPendingDue.Left = this.txtTotalPendingDue.Left - 126;
			this.vsPeriod.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
		}

		private void Add(int lvl)
		{
			GRID.Rows = GRID.Rows + 1;

			lvl += 1;
			if (lvl == 1)
			{
				GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GRID.Rows - 1, lngGRIDColInvoiceNumber, GRID.Rows - 1, GRID.Cols - 1, Color.White);
			}
			else if (lvl == 2)
			{
				GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, GRID.Rows - 1, lngGRIDColRef, GRID.Rows - 1, GRID.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GRID.Rows - 1, lngGRIDColInvoiceNumber, GRID.Rows - 1, GRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Rows - 1, lngGRIDColInvoiceNumber, GRID.Rows - 1, lngGRIDColInvoiceNumber, Color.Black);
			}
			else
			{
				GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GRID.Rows - 1, lngGRIDColInvoiceNumber, GRID.Rows - 1, GRID.Cols - 1, Color.White);
			}
			GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Rows - 1, lngGRIDColTotal, GRID.Rows - 1, lngGRIDColTotal, Color.FromArgb(5, 204, 71));
			GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, GRID.Rows - 1, lngGRIDColTotal, GRID.Rows - 1, lngGRIDColTotal, true);
			GRID.RowOutlineLevel(GRID.Rows - 1, lvl);
			if (lvl <= 1)
				GRID.IsSubtotal(GRID.Rows - 1, true);
		}

		private void AutoSize_GRID()
		{
			int lngTemp;
			int i;
			int wid = 0;
			int amountWidth;

			wid = GRID.WidthOriginal;
			amountWidth = FCConvert.ToInt32(wid * 0.13);

			GRID.ColWidth(lngGRIDColInvoiceNumber, FCConvert.ToInt32(wid * 0.15));
			GRID.ColWidth(lngGridColBillKey, 0);
			GRID.ColWidth(lngGRIDColRateKey, 0);
			GRID.ColWidth(lngGRIDColDate, FCConvert.ToInt32(wid * 0.11));
			GRID.ColWidth(lngGRIDColRef, FCConvert.ToInt32(wid * 0.1));
			GRID.ColWidth(lngGRIDColPaymentCode, FCConvert.ToInt32(wid * 0.04));
			GRID.ColWidth(lngGRIDColPrincipal, amountWidth);
			GRID.ColWidth(lngGRIDColPTC, 0);
			GRID.ColWidth(lngGRIDColTax, amountWidth);
			GRID.ColWidth(lngGRIDColInterest, amountWidth);
			GRID.ColWidth(lngGRIDColTotal, amountWidth);
			GRID.ColWidth(lngGRIDColLineCode, 0);
			GRID.ColWidth(lngGRIDColPaymentKey, 0);
			GRID.ColWidth(lngGRIDColCHGINTNumber, 0);
			GRID.ColWidth(lngGRIDColPerDiem, 0);
			GRID.ColWidth(lngGRIDColPending, FCConvert.ToInt32(wid * 0.04));

			amountWidth = amountWidth / 15;
			txtTotalPrincipal.Width = amountWidth;
			txtTotalInterest.Width = amountWidth;
			txtTotalTax.Width = amountWidth;
			txtTotalTotal.Width = amountWidth;
			
			int initialWidth = 0;

			for (int counter = 0; counter < lngGRIDColPrincipal; counter++)
			{
				initialWidth = initialWidth + GRID.ColWidth(counter);
			}

			initialWidth = initialWidth / 15;

			txtTotalPrincipal.Left = GridPanel.Left - 20 + initialWidth;
			txtTotalInterest.Left = txtTotalPrincipal.Left + amountWidth;
			txtTotalTax.Left = txtTotalInterest.Left + amountWidth;
			txtTotalTotal.Left = txtTotalTax.Left + amountWidth;
		}

		

		private void txtTax_Enter(object sender, System.EventArgs e)
		{
			txtTax.SelectionStart = 0;
			txtTax.SelectionLength = txtTax.Text.Length;
		}

		private void txtTax_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtTax.SelectionStart == 0)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtTax.SelectionStart == txtTax.Text.Length)
						{
							txtInterest.Focus();
						}
						break;
					}
			}
		}

		private void txtTax_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Execute)
			{
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtTax.Text) == 0)
				{
					txtTax.Text = "0.00";
				}
				txtTax.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtTax.Text)), "#,##0.00");
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = txtTax.Text.Length;
			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				KeyAscii = (Keys)0;
				txtTax.Text = "0.00";
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				if (txtTax.Text != "")
				{
					if (FCConvert.ToDecimal(txtTax.Text) > 99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTax.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtTax.Text) < -99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTax.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtTax.Text = Strings.Format(FCConvert.ToDecimal(txtTax.Text), "#,##0.00");
					}
				}
				else
				{
					txtTax.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GRID_ClickEvent(object sender, System.EventArgs e)
		{
			GRID_Click();
		}

		private void GRID_Click()
		{
			int cRow;
			int intCT;
			if (GRID.RowOutlineLevel(GRID.Row) == 1)
			{
				cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
				for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
				{
					if (cmbBillNumber.Items[cRow].ToString().Trim() == GRID.TextMatrix(GRID.Row, lngGRIDColInvoiceNumber).Trim())
					{
						cmbBillNumber.SelectedIndex = cRow;
						break;
					}
				}
			}
			else if (GRID.RowOutlineLevel(GRID.Row) == 0)
			{
				if (cmbBillNumber.SelectedIndex != -1 && "Auto" == cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString())
				{
					cmbBillNumber_Click();
				}
				else
				{
					for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
					{
						if ("Auto" == cmbBillNumber.Items[intCT].ToString().Replace("*", ""))
						{
							cmbBillNumber.SelectedIndex = intCT;
							break;
						}
					}
				}
			}
		}

		private void GRID_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			GRID_Collapsed(this.GRID.GetFlexRowIndex(e.RowIndex), false);
		}

		private void GRID_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			GRID_Collapsed(this.GRID.GetFlexRowIndex(e.RowIndex), true);
		}

		private void GRID_Collapsed(int row, bool isCollapsed)
		{
			int temp;
			int counter;
			int rows = 0;
			bool DeptFlag = false;
			bool DivisionFlag = false;
			int cRow;
			int lngTempRow = 0;

			lngTempRow = row;
			if (lngTempRow > 0)
			{
				if (GRID.RowOutlineLevel(lngTempRow) == 1)
				{
					for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
					{
						if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(GRID.TextMatrix(lngTempRow, lngGRIDColInvoiceNumber)))
						{
							cmbBillNumber.SelectedIndex = cRow;
							break;
						}
					}
				}
			}
			if (viewModel.DontFireCollapse == false)
			{
				for (counter = 1; counter <= GRID.Rows - 1; counter++)
				{
					if (GRID.RowOutlineLevel(counter) == 0)
					{
						if (GRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DeptFlag = true;
						}
						else
						{
							rows += 1;
							DeptFlag = false;
						}
					}
					else if (GRID.RowOutlineLevel(counter) == 1)
					{
						if (DeptFlag == true)
						{
							// do nothing
						}
						else if (GRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DivisionFlag = true;
							if (GRID.TextMatrix(counter, lngGRIDColRef) == "Original")
							{
								SwapRows(counter, NextSpacerLine(counter));
							}
						}
						else
						{
							rows += 1;
							DivisionFlag = false;
							if (GRID.TextMatrix(counter, lngGRIDColRef) != "Original")
							{
								SwapRows(counter, NextSpacerLine(counter));
							}
						}
					}
					else
					{
						if (DeptFlag == true || DivisionFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
			}
		}

		private void GRID_DblClick(object sender, System.EventArgs e)
		{
			GRID_DblClick();
		}

		private void GRID_DblClick()
		{
			int cRow = 0;
			int mRow = 0;
			int mCol = 0;
			mRow = GRID.MouseRow;
			mCol = GRID.MouseCol;
			cRow = GRID.Row;
			if (mRow > 0 && mCol > 0)
			{
				if (GRID.TextMatrix(cRow, lngGRIDColRef).Trim() == "REVERS")
				{
					MessageBox.Show("You may not reverse a reversal transaction.", "Invalid Reversal", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}

				if (GRID.RowOutlineLevel(cRow) == 1)
				{
					if (GRID.Col == lngGRIDColTotal || GRID.Col == lngGRIDColPending)
					{
						// do nothing
					}
					else if (GRID.Col == 1)
					{
						// set the year
						for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
						{
							if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(GRID.TextMatrix(GRID.Row, GRID.Col)))
							{
								cmbBillNumber.SelectedIndex = cRow;
								break;
							}
						}
					}
					// expand the rows
					if (GRID.IsCollapsed(GRID.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						GRID.IsCollapsed(GRID.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
					}
					else
					{
						GRID.IsCollapsed(GRID.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					}
				}
				else if (GRID.RowOutlineLevel(cRow) == 2 && fraPayment.Visible && (GRID.TextMatrix(cRow, lngGRIDColRef) != "CHGINT" && GRID.TextMatrix(cRow, lngGRIDColRef) != "EARNINT" && GRID.TextMatrix(cRow, lngGRIDColRef) != "CNVRSN" && GRID.TextMatrix(cRow, lngGRIDColRef) != "Interest"))
				{
					if (GRID.TextMatrix(cRow, lngGRIDColRef) != "Billed To:")
					{
						if (GRID.TextMatrix(cRow, lngGRIDColRef).Trim() != "Total" && GRID.TextMatrix(cRow, lngGRIDColRef).Trim() != "CURINT")
						{
							if (Strings.Trim(GRID.TextMatrix(cRow, lngGRIDColPaymentCode)) != "D")
							{
								if (MessageBox.Show("Are you sure that you would like to reverse this payment?", "Reverse Payment", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									CreateAROppositionLine(cRow, true);
								}
							}
							else
							{
								if (MessageBox.Show("Are you sure that you would like to reverse this discount?", "Reverse Payment", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									CreateAROppositionLine(cRow, true);
								}
							}
						}
						else
						{
							CreateAROppositionLine(cRow);
						}
					}
				}
				else if (GRID.RowOutlineLevel(cRow) == 0)
				{
					CreateAROppositionLine(cRow);
				}
			}
		}

		private void GRID_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void GRID_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 32)
			{
				GRID_DblClick();
			}
			else if (keyAscii == 13 && GRID.RowOutlineLevel(GRID.Row) == 1)
			{
				GRID_Click();
			}
		}

		private void GRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngMRow = 0;
			int lngBK = 0;
			lngMRow = GRID.MouseRow;
			if (e.Button == MouseButtons.Right)
			{
				if (GRID.RowOutlineLevel(lngMRow) == 1)
				{
					GRID.Select(lngMRow, 1);
					var bill = viewModel.GetBill(GRID.TextMatrix(lngMRow, lngGRIDColInvoiceNumber));
					ShowRateInfo(bill);
				}
				else if (GRID.RowOutlineLevel(lngMRow) == 2)
				{
					var payment = viewModel.GetPayment(GRID.TextMatrix(lngMRow, lngGRIDColPaymentKey).ToIntegerValue());
					ShowPaymentInfo(payment);
				}
			}
		}

		private void GRID_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			string strTemp = "";
			int lngMR = 0;

			if (e.ColumnIndex > -1 && e.RowIndex > -1)
			{
				DataGridViewCell cell = GRID[e.ColumnIndex, e.RowIndex];
				if (e.ColumnIndex == 0)
				{
					if (e.ColumnIndex == lngGRIDColInvoiceNumber)
					{
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColDate)
					{
						strTemp = "Recorded Date";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColRef)
					{
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColPaymentCode)
					{
						strTemp += " C - Correction" + ", ";
						strTemp += " I - Interest" + ", ";
						strTemp += " P - Payment" + ", ";
						strTemp += " Y - PrePayment";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColPTC)
					{
						strTemp = "Principal + Tax";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColPrincipal)
					{
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColInterest || e.ColumnIndex == lngGRIDColTax)
					{
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColTotal)
					{
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else
					{
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
				}
				else
				{
					lngMR = GRID.MouseRow;
					if (lngMR > 0 && lngMR < GRID.Rows)
					{
						if (GRID.TextMatrix(lngMR, lngGRIDColLineCode) != "" && GRID.TextMatrix(lngMR, lngGRIDColLineCode) != "=")
						{
							if (GRID.TextMatrix(lngMR, lngGRIDColRef) != "CURINT" && GRID.TextMatrix(lngMR, lngGRIDColRef) != "EARNINT")
							{
								if (GRID.RowOutlineLevel(lngMR) == 1 && e.ColumnIndex == lngGRIDColInvoiceNumber)
								{
									cell.ToolTipText = "Right-Click for Rate information and totals.";
								}
								else if (GRID.RowOutlineLevel(lngMR) == 2)
								{
									cell.ToolTipText = "Right-Click for Payment information.";
								}
								else
								{
									cell.ToolTipText = "";
								}
							}
							else
							{
								cell.ToolTipText = "";
							}
						}
						else
						{
							cell.ToolTipText = "";
						}
					}
					else
					{
						cell.ToolTipText = "";
					}
				}
			}
		}

		public int LastParentRow(int CurRow)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			if (l > 0)
			{
				curLvl = GRID.RowOutlineLevel(l);
				l -= 1;
				while (GRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
				LastParentRow = l;
			}
			else
			{
				LastParentRow = 0;
			}
			return LastParentRow;
		}

		private void SwapRows(int x, int y)
		{
			int intCol;
			string strTemp = "";
			for (intCol = lngGRIDColDate; intCol <= GRID.Cols - 1; intCol++)
			{
				strTemp = GRID.TextMatrix(x, intCol);
				GRID.TextMatrix(x, intCol, GRID.TextMatrix(y, intCol));
				GRID.TextMatrix(y, intCol, strTemp);
			}
		}

		private int NextSpacerLine(int x, string strCheck = "=")
		{
			int NextSpacerLine = 0;
			NextSpacerLine = GRID.FindRow(strCheck, x, lngGRIDColLineCode) + 1;
			return NextSpacerLine;
		}

		private void GRID_Coloring()
		{
			int lngRow = NextSpacerLine(1);
			while (lngRow > 0)
			{
				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, 0, lngRow, GRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
				GRID.Select(lngRow - 1, lngGRIDColPrincipal, lngRow - 1, lngGRIDColTotal);                               
				GRID.CellBorder(Color.Blue, 0, 1, 0, 0, 0, 0);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow - 1, 1, lngRow - 1, GRID.Cols - 1, Color.FromArgb(5, 204, 71));
				if (GRID.TextMatrix(lngRow - 1, lngGRIDColPrincipal).ToDecimalValue() != 0)
				{
					sumPrin += GRID.TextMatrix(lngRow - 1, lngGRIDColPrincipal).ToDecimalValue();
				}
				if (GRID.TextMatrix(lngRow - 1, lngGRIDColInterest).ToDecimalValue() != 0)
				{
					sumInt += GRID.TextMatrix(lngRow - 1, lngGRIDColInterest).ToDecimalValue();
				}
				if (GRID.TextMatrix(lngRow - 1, lngGRIDColTax).ToDecimalValue() != 0)
				{
					sumTax += GRID.TextMatrix(lngRow - 1, lngGRIDColTax).ToDecimalValue();
				}
				if (GRID.TextMatrix(lngRow - 1, lngGRIDColTotal).ToDecimalValue() != 0)
				{
					sumTotal += GRID.TextMatrix(lngRow - 1, lngGRIDColTotal).ToDecimalValue();
				}
				lngRow = NextSpacerLine(lngRow + 1);
			}

			GRID.Select(0, 1);
		}

		private bool PaymentRecords(ARBill bill)
		{
			foreach (var payment in viewModel.BillPayments(bill))
			{
				Add(1);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColDate, payment.RecordedTransactionDate == null ? "" : payment.RecordedTransactionDate.ToString());
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColRef, payment.Reference.Trim());
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPaymentCode, payment.Code.Trim());
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPrincipal, payment.Principal ?? 0);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColTax, payment.Tax ?? 0);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColInterest, payment.Interest ?? 0);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPTC, payment.Principal + payment.Tax);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColTotal, payment.Principal + payment.Tax + payment.Interest);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColLineCode, "-");
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPaymentKey, payment.Id);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColCHGINTNumber, payment.TransactionId);
				GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPending, payment.Pending ? "*" : " ");

				if (payment.ChargedInterestRecord != null)
				{
					Add(1);
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColDate, payment.ChargedInterestRecord.RecordedTransactionDate == null ? "" : payment.RecordedTransactionDate.ToString());
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColRef, payment.ChargedInterestRecord.Reference.Trim());
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPaymentCode, payment.ChargedInterestRecord.Code.Trim());
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPrincipal, payment.ChargedInterestRecord.Principal ?? 0);
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColTax, payment.ChargedInterestRecord.Tax ?? 0);
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColInterest, payment.ChargedInterestRecord.Interest ?? 0);
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPTC, payment.ChargedInterestRecord.Principal + payment.ChargedInterestRecord.Tax);
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColTotal, payment.ChargedInterestRecord.Principal + payment.ChargedInterestRecord.Tax + payment.ChargedInterestRecord.Interest);
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColLineCode, "-");
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPaymentKey, payment.ChargedInterestRecord.Id);
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColCHGINTNumber, payment.TransactionId);
					GRID.TextMatrix(GRID.Rows - 1, lngGRIDColPending, payment.ChargedInterestRecord.Pending ? "*" : " ");
				}
			}
			
			return true;
		}

		private void imgNote_DoubleClick(object sender, System.EventArgs e)
		{
			EditNote();
		}

		private void EditNote()
		{
			string strTemp;
			var originalComment = (viewModel.AccountComment?.Text ?? "").Trim();

			strTemp = Interaction.InputBox("The current account comment is:" + "\r\n" + "\r\n" + originalComment + "\r\n" + "\r\n" + "Edit the text below to change it or clear the text to remove the note from the account", "Account " + FCConvert.ToString(viewModel.Customer.CustomerId) + " Comment", originalComment);
			strTemp = modGlobalFunctions.RemoveApostrophe(strTemp).Trim();
			if (strTemp != originalComment)
			{ 
				if (strTemp == "")
				{
					if (MessageBox.Show("Are you sure that you would like to delete this comment?", "Delete Comment", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						viewModel.UpdateComment(strTemp);
						imgNote.Visible = false;
						mnuFilePriority.Visible = false;
					}
				}
				else
				{
					viewModel.UpdateComment(strTemp);
					imgNote.Visible = true;
					mnuFilePriority.Visible = true;
				}
			}
		}

		private void mnuFileEditNote_Click(object sender, System.EventArgs e)
		{
			EditNote();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			arARPaymentStatusDetail.InstancePtr.SetPaymentForm(this);
			frmReportViewer.InstancePtr.Init(arARPaymentStatusDetail.InstancePtr);
		}

		private void mnuFilePriority_Click(object sender, System.EventArgs e)
		{
			DialogResult lngAnswer;
			int priorityLevel = 0;

			lngAnswer = MessageBox.Show("Would you like this note to pop up when then account is accessed.", "Note Priority", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			switch (lngAnswer)
			{
				case DialogResult.Yes:
				{
					priorityLevel = 1;
					break;
				}
				case DialogResult.No:
				{
					priorityLevel = 0;
					break;
				}
			}

			viewModel.UpdateCommentPriority(priorityLevel);
		}

		private void DeleteAllPendingPayments()
		{
			viewModel.RemoveAllPendingPayments();

			vsPayments.Rows = 1;
			StartUp();
			CheckAllARPending();
		}

		private void mnuPaymentClearList_Click(object sender, System.EventArgs e)
		{
			DeleteAllPendingPayments();
		}

		private void mnuPaymentClearPayment_Click(object sender, System.EventArgs e)
		{
			ResetARPaymentFrame();
		}

		private void mnuPaymentSave_Click(object sender, System.EventArgs e)
		{
			SavePayment();
		}

		private bool SavePayment()
		{
			try
			{
				bool excessAmount = false;
				string invoiceNumber = cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString();
				string code = cmbCode.Items[cmbCode.SelectedIndex].ToString().Left(1);

                if (invoiceNumber.ToUpper() == "AUTO")
                {
                    if (viewModel.PendingPaymentsExist())
                    {
                        MessageBox.Show("You may not make an auto payment because pending payments exist.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

				if (viewModel.PendingPaymentsExistForSelectedBill(invoiceNumber))
				{
					MessageBox.Show("Payments already exists from this bill.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (code == "P" && txtInterest.Text.ToDecimalValue() < 0)
				{
					MessageBox.Show("Payments may not contain negative values.  If you need to adjust amounts on a bill you may do a correction instead.", "Negative Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (txtCD.Text == "N" && txtCash.Text == "N")
				{
					if (!viewModel.ValidManualAccount(txtAcctNumber.Text, cmbBillNumber.Text))
					{
						MessageBox.Show("This account is either invalid or the account's fund does not match the fund for this bill type.", "Invalid Account / Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}
				}

				if (!viewModel.IsPaymentScreen)
				{
					return false;
				}

				if (txtPrincipal.Text == "")
					txtPrincipal.Text = "0.00";
				if (txtTax.Text == "")
					txtTax.Text = "0.00";
				if (txtInterest.Text == "")
					txtInterest.Text = "0.00";

				if (txtPrincipal.Text.ToDecimalValue() != 0 || txtTax.Text.ToDecimalValue() != 0 || txtInterest.Text.ToDecimalValue() != 0)
				{
					if (code == "P" || code == "C" || code == "Y")
					{
						excessAmount = ViewModel.PaymentGreaterThanOwed(
							invoiceNumber,txtPrincipal.Text.ToDecimalValue() + txtTax.Text.ToDecimalValue() +
							txtInterest.Text.ToDecimalValue());

						if (excessAmount)
						{
							if (MessageBox.Show("The payment entered is greater than owed, would you like to continue?", "Payment Amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
							{
								return false;
							}
						}

						viewModel.AddPendingPayment(invoiceNumber, code,txtReference.Text.Trim(),txtCD.Text, txtCash.Text, txtAcctNumber.TextMatrix(0, 0), txtComments.Text.Trim(), txtTransactionDate.Text.Trim() == "" ? (DateTime?)null : txtTransactionDate.Text.ToDate(), txtPrincipal.Text.ToDecimalValue(), txtTax.Text.ToDecimalValue(), txtInterest.Text.ToDecimalValue());
					}
					else
					{
						MessageBox.Show("No Code entered!", "Missing Code ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return false;
					}
				}
				else
				{
					if (vsPayments.Rows == 1)
					{
						if (viewModel.NegativeBillsExist)
						{
							if (MessageBox.Show("Apply the negative account values to bills with outstanding balances?", "Negative Account Balances", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								viewModel.AdjustNegativeBillsToAddPayment();
							}
						}
						else
						{
							MessageBox.Show("There are no values in the payment fields.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}

				ResetARPaymentFrame();
				CheckAllARPending();
				StartUp();

				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Payment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return false;
			}
		}

		private void AddPaymentRow(ARPayment payment)
		{
			vsPayments.AddItem("");

			var bill = viewModel.GetBill(payment);

			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColInvoice, bill.InvoiceNumber);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColBillID, "");
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColDate, ((DateTime)payment.RecordedTransactionDate).ToString("MM/dd/yyyy"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColRef, payment.Reference);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCode, payment.Code);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCDAC, payment.CashDrawer + "   " + payment.GeneralLedger);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColPrincipal, Strings.Format(payment.Principal, "#,##0.00"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColTax, Strings.Format(payment.Tax, "#,##0.00"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColInterest, Strings.Format(payment.Interest, "#,##0.00"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColKeyNumber, payment.PendingId);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCHGINTNumber, "");
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColTotal, Strings.Format(payment.Principal + payment.Tax + payment.Interest, "#,##0.00"));

			if (payment.ChargedInterestRecord != null)
			{
				vsPayments.AddItem("");
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColInvoice, bill.InvoiceNumber);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColBillID, "");
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColDate, ((DateTime)payment.ChargedInterestRecord.RecordedTransactionDate).ToString("MM/dd/yyyy"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColRef, payment.ChargedInterestRecord.Reference);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCode, payment.ChargedInterestRecord.Code);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCDAC, payment.ChargedInterestRecord.CashDrawer + "   " + payment.GeneralLedger);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColPrincipal, Strings.Format(payment.ChargedInterestRecord.Principal, "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColTax, Strings.Format(payment.ChargedInterestRecord.Tax, "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColInterest, Strings.Format(payment.ChargedInterestRecord.Interest, "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColKeyNumber, payment.PendingId);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCHGINTNumber, "");
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColTotal, Strings.Format(payment.ChargedInterestRecord.Principal + payment.ChargedInterestRecord.Tax + payment.ChargedInterestRecord.Interest, "#,##0.00"));
			}
		}

		public void CheckAllARPending()
		{
			foreach (var bill in viewModel.Bills)
			{
				if (bill.InvoiceNumber != "0")
				{
					int billRow = GRID.FindRow(bill.InvoiceNumber, 1, lngGRIDColInvoiceNumber);
					bool pending = viewModel.PendingPaymentsExistForSelectedBill(bill.InvoiceNumber);
					
					GRID.TextMatrix(billRow, lngGRIDColPending, pending ? "*" : " ");
					GRID.TextMatrix(GRID.FindRow("=", billRow, lngGRIDColLineCode), lngGRIDColPending, pending ? "*" : " ");
					GRID.TextMatrix(GRID.FindRow("=", billRow, lngGRIDColLineCode) + 1, lngGRIDColPending, pending ? "*" : " ");
				}
			}

			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPending, GRID.Rows - 1, lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			txtTotalPendingDue.Text = Strings.Format(viewModel.TotalPendingPaymentsAmount(), "#,##0.00");
		}
		
		private void mnuPaymentSaveExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (!viewModel.IsPaymentScreen)
				{
					return;
				}

				if (txtPrincipal.Text.ToDecimalValue() != 0 || txtTax.Text.ToDecimalValue() != 0 || txtInterest.Text.ToDecimalValue() != 0)
				{
					if (!SavePayment())
					{
						return;
					}
				}

				FillTransactionInformation();

				viewModel.CompleteTransaction();
				cancelling = false;
				this.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving/Exit Payment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTransactionInformation()
		{
			viewModel.Transaction.Bills = viewModel.Bills;
		}
		
		private void mnuProcessChangeAccount_Click(object sender, System.EventArgs e)
		{
			bool boolGoBack = false;
			if (vsPayments.Rows > 1)
			{
				if (MessageBox.Show("Are you sure that you would like to leave this screen?  Any payments that you have added this session will be lost unless saved.", "Change Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					boolGoBack = true;
				}
			}
			else
			{
				boolGoBack = true;
			}
			if (boolGoBack)
			{
				cancelling = false;
				Close();
				viewModel.GoToSearchScreen();
			}
		}

		private void mnuProcessEffective_Click(object sender, System.EventArgs e)
		{
			if (viewModel.PendingPaymentsExist())
			{
				MessageBox.Show("You may not change the effective date with existing pending transactions.", "Unable to Change Effective Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			else
			{
				object strDate = viewModel.EffectiveDate.ToString("MM/dd/yyyy");
				if (frmInput.InstancePtr.Init(ref strDate, "Date Change", "Please input the new effective date and then press OK.", intDataType: modGlobalConstants.InputDTypes.idtDate, strInitValue: viewModel.EffectiveDate.ToString("MM/dd/yyyy")))
				{
					DateTime newEffectiveDate;
					if (DateTime.TryParse(strDate.ToString(), out newEffectiveDate))
					{
						viewModel.EffectiveDate = newEffectiveDate;
					}

					StartUp();
				}
			}
		}

		private void Create_Account_Totals()
		{
			//int lngCounter = 0;

			//lngCounter = GRID.Rows;
			//Add(-1);

			//GRID.MergeRow(lngCounter, true, 1, 4);
			//GRID.TextMatrix(lngCounter, 1, "Account Totals as of " + viewModel.EffectiveDate.ToString("MM/dd/yyyy"));
			//GRID.TextMatrix(lngCounter, lngGRIDColPrincipal, Strings.Format(sumPrin, "0.00"));
			//GRID.TextMatrix(lngCounter, lngGRIDColTax, Strings.Format(sumTax, "0.00"));
			//GRID.TextMatrix(lngCounter, lngGRIDColInterest, Strings.Format(sumInt, "0.00"));
			//GRID.TextMatrix(lngCounter, lngGRIDColPTC, Strings.Format(sumPrin + sumTax, "0.00"));
			//if (Math.Round(sumPrin + sumInt + sumTax, 2, MidpointRounding.AwayFromZero) == Math.Round(sumTotal, 2, MidpointRounding.AwayFromZero))
			//{
			//	GRID.TextMatrix(lngCounter, lngGRIDColTotal, Strings.Format(sumTotal, "0.00"));
			//}
			//else
			//{
			//	GRID.TextMatrix(lngCounter, lngGRIDColTotal, "ERROR(" + FCConvert.ToString(sumTotal) + ")");
			//}

			txtAccountTotalsAsOf.Text = "Account Totals as of " + viewModel.EffectiveDate.ToString("MM/dd/yyyy");
			txtTotalPrincipal.Text = Strings.Format(sumPrin, "0.00");
			txtTotalTax.Text =  Strings.Format(sumTax, "0.00");
			txtTotalInterest.Text = Strings.Format(sumInt, "0.00");
			if (Math.Round(sumPrin + sumInt + sumTax, 2, MidpointRounding.AwayFromZero) == Math.Round(sumTotal, 2, MidpointRounding.AwayFromZero))
			{
				txtTotalTotal.Text = Strings.Format(sumTotal, "0.00");
			}
			else
			{
				txtTotalTotal.Text = "ERROR(" + FCConvert.ToString(sumTotal) + ")";
			}

			//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCounter, 1, lngCounter, GRID.Cols - 1, Color.White);
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, 1, lngCounter, 6, Color.Black);
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, 7, lngCounter, GRID.Cols - 1, Color.FromArgb(5, 204, 71));
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngCounter, 1, lngCounter, GRID.Cols - 1, true);
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void Format_PaymentGrid()
		{
			int wid = 0;
			vsPayments.Cols = 14;
			wid = vsPayments.WidthOriginal;
			vsPayments.ColWidth(lngPayGridColInvoice, FCConvert.ToInt32(wid * 0.11));
			vsPayments.ColWidth(lngPayGridColBillID, wid * 0);
			vsPayments.ColWidth(lngPayGridColDate, FCConvert.ToInt32(wid * 0.1));
			vsPayments.ColWidth(lngPayGridColRef, FCConvert.ToInt32(wid * 0.1));
			vsPayments.ColWidth(lngPayGridColCode, FCConvert.ToInt32(wid * 0.05));
			vsPayments.ColWidth(lngPayGridColCDAC, FCConvert.ToInt32(wid * 0.06));
			vsPayments.ColWidth(lngPayGridColPrincipal, FCConvert.ToInt32(wid * 0.14));
			vsPayments.ColWidth(lngPayGridColTax, FCConvert.ToInt32(wid * 0.13));
			vsPayments.ColWidth(lngPayGridColInterest, FCConvert.ToInt32(wid * 0.13));
			vsPayments.ColWidth(lngPayGridColArrayIndex, 0);
			vsPayments.ColWidth(lngPayGridColKeyNumber, 0);
			vsPayments.ColWidth(lngPayGridColCHGINTNumber, 0);
			vsPayments.ColWidth(lngPayGridColTotal, FCConvert.ToInt32(wid * 0.15));

			vsPayments.ColAlignment(lngPayGridColInvoice, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColCode, FCGrid.AlignmentSettings.flexAlignLeftCenter);

			vsPayments.ExtendLastCol = true;
			vsPayments.TextMatrix(0, lngPayGridColInvoice, "Bill");
			vsPayments.TextMatrix(0, lngPayGridColDate, "Date");
			vsPayments.TextMatrix(0, lngPayGridColRef, "Reference");
			vsPayments.TextMatrix(0, lngPayGridColCode, "Code");
			vsPayments.TextMatrix(0, lngPayGridColCDAC, "Cash");
			vsPayments.TextMatrix(0, lngPayGridColPrincipal, "Principal");
			vsPayments.TextMatrix(0, lngPayGridColTax, "Tax");
			vsPayments.TextMatrix(0, lngPayGridColInterest, "Interest");
			vsPayments.TextMatrix(0, lngPayGridColTotal, "Total");
		}

		private void mnuProcessGoTo_Click(object sender, System.EventArgs e)
		{
			if (!viewModel.StartedFromCashReceipts)
			{
				if (viewModel.IsPaymentScreen)
				{
					GoToStatus();
				}
				else
				{
					GoToPayment();
				}
			}
		}

		private void txtAcctNumber_Enter(object sender, System.EventArgs e)
		{
			if (txtAcctNumber.Enabled == false)
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtAcctNumber_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (FCConvert.ToInt32(KeyCode))
			{
				case 37:
					{
						if (txtAcctNumber.EditSelStart == 0)
						{
							if (txtCash.Enabled == true)
							{
								if (txtCash.Visible && txtCash.Enabled)
								{
									txtCash.Focus();
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
						}
						break;
					}
				case 39:
					{
						if (txtAcctNumber.EditSelStart == txtAcctNumber.Text.Length - 1)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						break;
					}
			}
		}

		private void txtCash_DoubleClick(object sender, System.EventArgs e)
		{
			if (txtCash.Text == "N")
			{
				txtCash.Text = "Y";
			}
			else
			{
				txtCash.Text = "N";
			}
			CheckCash();
		}

		public void txtCash_DblClick()
		{
			txtCash_DoubleClick(txtCash, new System.EventArgs());
		}

		private void txtCash_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;

			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
						break;
					}
				case Keys.Right:
					{
						if (txtCash.Text == "N")
						{
							if (txtAcctNumber.Visible && txtPrincipal.Enabled)
							{
								txtAcctNumber.Focus();
							}
						}
						else
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						break;
					}
				case Keys.Space:
					{
						txtCash_DblClick();
						break;
					}
				case Keys.N:
					{
						txtCash.Text = "N";
						break;
					}
				case Keys.Y:
					{
						txtCash.Text = "Y";
						break;
					}
				default:
					{
						break;
					}
			}
			CheckCash();
		}

		private void txtCash_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.N:
				case Keys.Y:
				case Keys.Space:
				case Keys.Back:
					{
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCD_DoubleClick(object sender, System.EventArgs e)
		{
			if (txtCD.Text == "N")
			{
				txtCD.Text = "Y";
			}
			else
			{
				txtCD.Text = "N";
			}
			CheckCD();
		}

		public void txtCD_DblClick()
		{
			txtCD_DoubleClick(txtCD, new System.EventArgs());
		}

		private void txtCD_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						cmbCode.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						if (txtCD.Text == "N")
						{
							if (txtCash.Visible && txtCash.Enabled)
							{
								txtCash.Focus();
							}
						}
						else
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						txtCD_DblClick();
						break;
					}
				case Keys.N:
					{
						txtCD.Text = "N";
						break;
					}
				case Keys.Y:
					{
						txtCD.Text = "Y";
						break;
					}
				default:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			CheckCD();
		}

		private void txtCD_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.N:
				case Keys.Y:
				case Keys.Space:
				case Keys.Back:
					{
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtComments_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtComments.SelectionStart == 0)
						{
							if (txtInterest.Visible && txtInterest.Enabled)
							{
								txtInterest.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						if (txtComments.SelectionStart == txtComments.Text.Length)
						{
							vsPayments.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtInterest_Enter(object sender, System.EventArgs e)
		{
			txtInterest.SelectionStart = 0;
			txtInterest.SelectionLength = txtInterest.Text.Length;
		}

		private void txtInterest_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtInterest.SelectionStart == 0)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						if (txtInterest.SelectionStart == txtInterest.Text.Length)
						{
							if (txtComments.Visible && txtComments.Enabled)
							{
								txtComments.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtInterest_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
			{
				switch (KeyAscii)
				{
					case Keys.NumPad3:
					case Keys.C:
					case Keys.Back:
						{
							KeyAscii = (Keys)0;
							txtInterest.Text = "0.00";
							txtInterest.SelectionStart = 0;
							txtInterest.SelectionLength = 4;
							break;
						}
				}
			}
			else
			{
				if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
				{
					// do nothing
				}
				else if (KeyAscii == Keys.Execute)
				{
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtInterest.Text) == 0)
					{
						txtInterest.Text = "0.00";
					}
					txtInterest.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtInterest.Text)), "#,##0.00");
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = txtInterest.Text.Length;
				}
				else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
				{
					KeyAscii = (Keys)0;
					txtInterest.Text = "0.00";
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = 4;
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtInterest_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				if (txtInterest.Text != "")
				{
					if (FCConvert.ToDecimal(txtInterest.Text) > 9999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $9,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInterest.Text = Strings.Format(9999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtInterest.Text) < -9999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-9,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInterest.Text = Strings.Format(-9999999.99, "#,##0.00");
					}
					else
					{
						txtInterest.Text = Strings.Format(FCConvert.ToDecimal(txtInterest.Text), "#,##0.00");
					}
				}
				else
				{
					txtInterest.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtPrincipal_Enter(object sender, System.EventArgs e)
		{
			txtPrincipal.SelectionStart = 0;
			txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
		}

		private void txtPrincipal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtPrincipal.SelectionStart == 0)
						{
							if (txtCD.Text == "N")
							{
								if (txtCash.Text == "N")
								{
									txtAcctNumber.Focus();
								}
								else
								{
									txtAcctNumber.Enabled = false;
									if (txtCash.Visible && txtCash.Enabled)
									{
										txtCash.Focus();
									}
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						if (txtPrincipal.SelectionStart == txtPrincipal.Text.Length)
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtPrincipal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Execute)
			{
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtPrincipal.Text) == 0)
				{
					txtPrincipal.Text = "0.00";
				}
				txtPrincipal.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtPrincipal.Text)), "#,##0.00");
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				KeyAscii = (Keys)0;
				txtPrincipal.Text = "0.00";
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPrincipal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				if (txtPrincipal.Text != "")
				{
					if (FCConvert.ToDecimal(txtPrincipal.Text) > 99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrincipal.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtPrincipal.Text) < -99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrincipal.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtPrincipal.Text = Strings.Format(FCConvert.ToDecimal(txtPrincipal.Text), "#,##0.00");
					}
				}
				else
				{
					txtPrincipal.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}


		private void txtReference_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			bool boolTest = false;
			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtReference.SelectionStart == 0)
						{
							if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
							{
								txtTransactionDate.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						if (txtReference.SelectionStart == txtReference.Text.Length)
						{
							cmbCode.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Return:
					{
						txtReference_Validate(ref boolTest);
						if (boolTest)
						{
						}
						else
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtReference_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if ((Strings.UCase(Strings.Trim(txtReference.Text)) == "CHGINT") || (Strings.UCase(Strings.Trim(txtReference)) == "CNVRSN") || (Strings.UCase(Strings.Trim(txtReference.Text)) == "REVRSE"))
			{
				MessageBox.Show(Strings.UCase(Strings.Trim(txtReference.Text)) + " is a reserved reference string.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		public void txtReference_Validate(ref bool Cancel)
		{
			txtReference_Validating(txtReference, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtTotalPendingDue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTransactionDate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (FCConvert.ToInt32(KeyCode) == 37)
			{
				if (txtTransactionDate.SelStart == 0)
				{
					cmbBillNumber.Focus();
					KeyCode = 0;
				}
			}
			else if (FCConvert.ToInt32(KeyCode) == 39)
			{
				if (txtTransactionDate.SelStart == txtTransactionDate.Text.Length)
				{
					txtReference.Focus();
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Return)
			{
				if (txtReference.Visible && txtReference.Enabled)
				{
					txtReference.Focus();
				}
				else
				{
					Support.SendKeys("{tab}", false);
				}
			}
			else if ((KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (FCConvert.ToInt32(KeyCode) >= 48 && FCConvert.ToInt32(KeyCode) <= 57) || (FCConvert.ToInt32(KeyCode) == 191) || (FCConvert.ToInt32(KeyCode) == 111) || (FCConvert.ToInt32(KeyCode) >= 96 && FCConvert.ToInt32(KeyCode) <= 105))
			{
				// backspace, delete, 0-9, "/", numberpad "/", 0-9 all can pass
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtTransactionDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(Strings.Left(txtTransactionDate.Text, 2)) > 12)
			{
				e.Cancel = true;
				MessageBox.Show("Please use the date format MM/dd/yyyy.", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsPayments_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int l;
			if (KeyCode == Keys.Delete)
			{
				if (MessageBox.Show("Are you sure that you would like to delete this payment?", "Delete Payment Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "AUTO" || vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "OVERPAY" || vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "PREPAY-A")
					{
						for (l = vsPayments.Rows - 1; l >= 1; l--)
						{
							if (vsPayments.TextMatrix(l, lngPayGridColRef) == "AUTO" || vsPayments.TextMatrix(l, lngPayGridColRef) == "OVERPAY" || vsPayments.TextMatrix(l, lngPayGridColRef) == "PREPAY-A")
							{
								DeletePaymentRow(l);
							}
						}
					}
					else if (vsPayments.Row != 0)
					{
						DeletePaymentRow(vsPayments.Row);
					}
					CheckAllARPending();
				}
			}
		}

		private void GoToStatus()
		{
			fraPayment.Visible = false;
			GridPanel.Top = lblOwnersName.Top + lblOwnersName.Height + 10;
			GridPanel.Height = FCConvert.ToInt32((this.ClientArea.Height - GridPanel.Top) * 0.96);
			mnuProcessGoTo.Text = strPaymentString;
			if (GRID.Enabled && GRID.Visible)
			{
				GRID.Focus();
			}
			mnuPaymentSave.Enabled = false;
			mnuPaymentSaveExit.Enabled = false;
			cmdPaymentSave.Enabled = false;
			cmdProcess.Enabled = false;
			cmdProcess.Visible = false;
			viewModel.IsPaymentScreen = false;
		}

		private void GoToPayment()
		{
			//fraStatusLabels.Visible = false;
			fraPayment.Visible = true;
			mnuPayment.Visible = true;
			vsPayments.Visible = true;
			mnuProcessGoTo.Text = strStatusString;
			GridPanel.Top = lblOwnersName.Top + lblOwnersName.Height + 10;
			GridPanel.Height = FCConvert.ToInt32(frmARPaymentStatus.InstancePtr.ClientArea.Height * 0.40);
			fraPayment.Top = GridPanel.Bottom;
			this.Refresh();
			FillARCodeCombo();
			ResetARPaymentFrame();
			Format_PaymentGrid();
			Format_ARvsPeriod();
			txtReference.Focus();
			mnuPaymentSave.Enabled = true;
			mnuPaymentSaveExit.Enabled = true;
			cmdPaymentSave.Enabled = true;
			cmdProcess.Enabled = true;
			cmdProcess.Visible = true;
			viewModel.IsPaymentScreen = true;
		}
		
		private void DeletePaymentRow(int Row)
		{
			var payment = viewModel.GetPendingPayment(new Guid(vsPayments.TextMatrix(Row, lngPayGridColKeyNumber)));

			for (int row = vsPayments.Rows - 1; row >= 1; row--)
			{
				if (vsPayments.TextMatrix(row, lngPayGridColKeyNumber) == payment.PendingId.ToString() || vsPayments.TextMatrix(row, lngPayGridColKeyNumber) == (payment.ChargedInterestRecord != null ? payment.ChargedInterestRecord.PendingId.ToString() : ""))
				{
					if (row != 0)
					{
						vsPayments.RemoveItem(row);
					}
				}
			}

			viewModel.RemovePendingPayment(payment);
			StartUp();
		}

		private void FillInARInfo()
		{
			try
			{
				lblOwnersName.Text = viewModel.Customer.FullName;
				lblOwnersName.Visible = true;
				lblName.Visible = true;
				lblOwnersName.BackColor = Color.White;
				lblAccount.Text = viewModel.Transaction.AccountNumber.ToString();
				lblAccount.Visible = true;
				lblPaymentInfo.Text = "Customer #: " + lblAccount.Text;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling UT Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowRateInfo(ARBill bill)
		{
			try
			{
				string strAddr1 = "";
				string strAddr2 = "";
				string strAddr3 = "";
				string strAddr4 = "";

				var billType = viewModel.GetDefaultBillType(bill.BillType);

				vsRateInfo.Cols = 1;
				vsRateInfo.Cols = 5;
				vsRateInfo.Rows = 26;
				
				vsRateInfo.RowHeight(-1, 380);
				vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				fraRateInfo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);

				vsRateInfo.ColWidth(0, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.25));
				vsRateInfo.ColWidth(1, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.19));
				vsRateInfo.ColWidth(2, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.04));
				vsRateInfo.ColWidth(3, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.23));
				vsRateInfo.ColWidth(4, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.29));
				vsRateInfo.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsRateInfo.Select(0, 0);
				vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				vsRateInfo.TextMatrix(0, 0, "Bill ID");
				vsRateInfo.TextMatrix(1, 0, "Rate Record");
				vsRateInfo.TextMatrix(2, 0, "Bill Description");
				vsRateInfo.TextMatrix(3, 0, "Created By");
				vsRateInfo.TextMatrix(4, 0, "");
				if (billType != null)
				{
					vsRateInfo.TextMatrix(5, 0, billType.Reference);
					vsRateInfo.TextMatrix(6, 0, billType.Control1);
					vsRateInfo.TextMatrix(7, 0, billType.Control2);
					vsRateInfo.TextMatrix(8, 0, billType.Control3);
				}
				else
				{
					vsRateInfo.TextMatrix(5, 0, "");
					vsRateInfo.TextMatrix(6, 0, "");
					vsRateInfo.TextMatrix(7, 0, "");
					vsRateInfo.TextMatrix(8, 0, "");
				}
				vsRateInfo.TextMatrix(10, 0, "Billing Date");
				vsRateInfo.TextMatrix(11, 0, "Creation Date");
				vsRateInfo.TextMatrix(12, 0, "Period Start");
				vsRateInfo.TextMatrix(13, 0, "Period End");
				vsRateInfo.TextMatrix(14, 0, "Interest Date");
				vsRateInfo.TextMatrix(16, 0, "");
				vsRateInfo.TextMatrix(17, 0, "Principal");
				vsRateInfo.TextMatrix(18, 0, "Tax");
				vsRateInfo.TextMatrix(19, 0, "Interest");
				vsRateInfo.TextMatrix(22, 0, "Paid");
				vsRateInfo.TextMatrix(23, 0, "");
				vsRateInfo.TextMatrix(24, 0, "Total Due");
				vsRateInfo.TextMatrix(25, 0, "Per Diem");

				if (bill.BillNumber > 0)
				{
					if (bill.InterestMethod == "AP")
					{
						vsRateInfo.TextMatrix(15, 0, "Interest Rate");
						vsRateInfo.TextMatrix(15, 1, Strings.Format(bill.InterestRate, "#0.00%"));
					}
					else if (bill.InterestMethod == "AF")
					{
						vsRateInfo.TextMatrix(15, 0, "Late Fee");
						vsRateInfo.TextMatrix(15, 1, Strings.Format(bill.FlatRate, "#,##0.00"));
					}
					else
					{
						vsRateInfo.TextMatrix(15, 0, "");
						vsRateInfo.TextMatrix(15, 1, "");
					}

					vsRateInfo.TextMatrix(0, 1, bill.Id);
					vsRateInfo.TextMatrix(1, 1, bill.BillNumber);
					vsRateInfo.TextMatrix(2, 1, billType != null ? billType.TypeTitle : "");
					vsRateInfo.TextMatrix(3, 1, bill.CreatedBy);
					vsRateInfo.TextMatrix(4, 1, "");
					vsRateInfo.TextMatrix(5, 1, bill.Reference);
					vsRateInfo.TextMatrix(6, 1, bill.Control1);
					vsRateInfo.TextMatrix(7, 1, bill.Control2);
					vsRateInfo.TextMatrix(8, 1, bill.Control3);
					vsRateInfo.TextMatrix(10, 1, Strings.Format(bill.BillDate, "MM/dd/yyyy"));
					vsRateInfo.TextMatrix(11, 1, Strings.Format(bill.RateCreatedDate, "MM/dd/yyyy"));
					vsRateInfo.TextMatrix(12, 1, Strings.Format(bill.RateStart, "MM/dd/yyyy"));
					vsRateInfo.TextMatrix(13, 1, Strings.Format(bill.RateEnd, "MM/dd/yyyy"));
					vsRateInfo.TextMatrix(14, 1, Strings.Format(bill.InterestStartDate, "MM/dd/yyyy"));
					vsRateInfo.TextMatrix(16, 1, "");

					vsRateInfo.TextMatrix(17, 1, Strings.Format(bill.PrinOwed, "#,##0.00"));
					vsRateInfo.TextMatrix(18, 1, Strings.Format(bill.TaxOwed, "#,##0.00"));
					vsRateInfo.TextMatrix(19, 1, Strings.Format((double)((-1 * bill.CurrentInterest) + (bill.IntAdded ?? 0)), "#,##0.00"));

					vsRateInfo.TextMatrix(20, 1, "");
					vsRateInfo.TextMatrix(21, 1, "");

					vsRateInfo.TextMatrix(22, 1, Strings.Format(bill.PrinPaid + bill.TaxPaid + bill.IntPaid, "#,##0.00"));
					vsRateInfo.TextMatrix(23, 1, "");
					vsRateInfo.TextMatrix(24, 1, Strings.Format(bill.BalanceRemaining, "#,##0.00"));

					vsRateInfo.TextMatrix(25, 1,
						Strings.Format(bill.PerDiem, "#,##0.0000"));
				}
				else
				{
					vsRateInfo.TextMatrix(0, 1, "");
					vsRateInfo.TextMatrix(1, 1, "");
					vsRateInfo.TextMatrix(2, 1, "");
					vsRateInfo.TextMatrix(3, 1, "");
					vsRateInfo.TextMatrix(4, 1, "");
					vsRateInfo.TextMatrix(5, 1, "");
					vsRateInfo.TextMatrix(6, 1, "");
					vsRateInfo.TextMatrix(7, 1, "");
					vsRateInfo.TextMatrix(8, 1, "");
					vsRateInfo.TextMatrix(9, 1, "");
					vsRateInfo.TextMatrix(10, 1, "");
					vsRateInfo.TextMatrix(11, 1, "");
					vsRateInfo.TextMatrix(12, 1, "");
					vsRateInfo.TextMatrix(13, 1, "");
					vsRateInfo.TextMatrix(14, 1, "");
					vsRateInfo.TextMatrix(15, 0, "");
					vsRateInfo.TextMatrix(16, 0, "");
					vsRateInfo.TextMatrix(17, 0, "");
					vsRateInfo.TextMatrix(18, 0, "");
					vsRateInfo.TextMatrix(19, 0, "");
					vsRateInfo.TextMatrix(20, 0, "");
					vsRateInfo.TextMatrix(21, 0, "");
					vsRateInfo.TextMatrix(22, 0, "");
					vsRateInfo.TextMatrix(23, 0, "");
					vsRateInfo.TextMatrix(24, 0, "");
					vsRateInfo.TextMatrix(25, 0, "");
				}

				vsRateInfo.TextMatrix(0, 3, "Billed");
				vsRateInfo.TextMatrix(1, 3, "Address");
				vsRateInfo.TextMatrix(2, 3, "");
				vsRateInfo.TextMatrix(3, 3, "");
				vsRateInfo.TextMatrix(4, 3, "");
				vsRateInfo.TextMatrix(5, 3, "Customer");
				vsRateInfo.TextMatrix(6, 3, "Address");
				vsRateInfo.TextMatrix(7, 3, "");
				vsRateInfo.TextMatrix(8, 3, "");
				vsRateInfo.TextMatrix(9, 3, "");
				vsRateInfo.TextMatrix(10, 3, "Interest Paid Date");
				vsRateInfo.TextMatrix(11, 3, "Total Per Diem");
				vsRateInfo.TextMatrix(12, 3, "");
				if (billType != null)
				{
					vsRateInfo.TextMatrix(13, 3, billType.Title1);
					vsRateInfo.TextMatrix(14, 3, billType.Title2);
					vsRateInfo.TextMatrix(15, 3, billType.Title3);
					vsRateInfo.TextMatrix(16, 3, billType.Title4);
					vsRateInfo.TextMatrix(17, 3, billType.Title5);
					vsRateInfo.TextMatrix(18, 3, billType.Title6);
					vsRateInfo.TextMatrix(19, 3, "Total");
				}
				else
				{
					vsRateInfo.TextMatrix(13, 3, "Fee 1");
					vsRateInfo.TextMatrix(14, 3, "Fee 2");
					vsRateInfo.TextMatrix(15, 3, "Fee 3");
					vsRateInfo.TextMatrix(16, 3, "Fee 4");
					vsRateInfo.TextMatrix(17, 3, "Fee 5");
					vsRateInfo.TextMatrix(18, 3, "Fee 6");
					vsRateInfo.TextMatrix(19, 3, "Total");
				}
				if (bill.Bname2.Trim() != "")
				{
					vsRateInfo.TextMatrix(0, 4, bill.Bname.Trim() + " and " + bill.Bname2.Trim());
				}
				else
				{
					vsRateInfo.TextMatrix(0, 4, bill.Bname.Trim());
				}
				strAddr1 = bill.Address1;
				strAddr2 = bill.Address2;
				strAddr3 = bill.Address3;
				if (bill.Zip4.Trim() != "")
				{
					strAddr4 = bill.City + ", " + bill.State + " " + bill.Zip + "-" + bill.Zip4;
				}
				else
				{
					strAddr4 = bill.City + ", " + bill.State + " " + bill.Zip;
				}
				if (strAddr3 == "")
				{
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				if (strAddr2 == "")
				{
					strAddr2 = strAddr3;
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				if (strAddr1 == "")
				{
					strAddr1 = strAddr2;
					strAddr2 = strAddr3;
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				vsRateInfo.TextMatrix(1, 4, strAddr1);
				vsRateInfo.TextMatrix(2, 4, strAddr2);
				vsRateInfo.TextMatrix(3, 4, strAddr3);
				vsRateInfo.TextMatrix(4, 4, strAddr4);
				if(viewModel.Customer != null)
				{
					vsRateInfo.TextMatrix(5, 4, viewModel.Customer.FullName);
					strAddr1 = FCConvert.ToString(viewModel.Customer.Address1);
					strAddr2 = FCConvert.ToString(viewModel.Customer.Address2);
					strAddr3 = FCConvert.ToString(viewModel.Customer.Address3);
					// TODO: Field [PartyState] not found!! (maybe it is an alias?)
					strAddr4 = viewModel.Customer.City + ", " + viewModel.Customer.State + " " + viewModel.Customer.Zip;
					if (strAddr3 == "")
					{
						strAddr3 = strAddr4;
						strAddr4 = "";
					}
					if (strAddr2 == "")
					{
						strAddr2 = strAddr3;
						strAddr3 = strAddr4;
						strAddr4 = "";
					}
					if (strAddr1 == "")
					{
						strAddr1 = strAddr2;
						strAddr2 = strAddr3;
						strAddr3 = strAddr4;
						strAddr4 = "";
					}
					vsRateInfo.TextMatrix(6, 4, strAddr1);
					vsRateInfo.TextMatrix(7, 4, strAddr2);
					vsRateInfo.TextMatrix(8, 4, strAddr3);
					vsRateInfo.TextMatrix(9, 4, strAddr4);
				}
				else
				{
					vsRateInfo.TextMatrix(5, 4, "");
					vsRateInfo.TextMatrix(6, 4, "");
					vsRateInfo.TextMatrix(7, 4, "");
					vsRateInfo.TextMatrix(8, 4, "");
					vsRateInfo.TextMatrix(9, 4, "");
				}
				if (bill.LastInterestDate != null)
				{
					vsRateInfo.TextMatrix(10, 4, Strings.Format(bill.LastInterestDate, "MM/dd/yyyy"));
				}
				else
				{
					vsRateInfo.TextMatrix(10, 4, "");
				}
				vsRateInfo.TextMatrix(11, 4, Strings.Format(viewModel.TotalPerDiem, "#,##0.0000"));
				vsRateInfo.TextMatrix(12, 4, "");
				if (bill.Amount1 != 0)
				{
					vsRateInfo.TextMatrix(13, 4, Strings.Format(bill.Amount1, "#,##0.00"));
				}
				if (bill.Amount2 != 0)
				{
					vsRateInfo.TextMatrix(14, 4, Strings.Format(bill.Amount2, "#,##0.00"));
				}
				if (bill.Amount3 != 0)
				{
					vsRateInfo.TextMatrix(15, 4, Strings.Format(bill.Amount3, "#,##0.00"));
				}
				if (bill.Amount4 != 0)
				{
					vsRateInfo.TextMatrix(16, 4, Strings.Format(bill.Amount4, "#,##0.00"));
				}
				if (bill.Amount5 != 0)
				{
					vsRateInfo.TextMatrix(17, 4, Strings.Format(bill.Amount5, "#,##0.00"));
				}
				if (bill.Amount6 != 0)
				{
					vsRateInfo.TextMatrix(18, 4, Strings.Format(bill.Amount6, "#,##0.00"));
				}
				
				vsRateInfo.TextMatrix(19, 4, Strings.Format(bill.Amount1 + bill.Amount2 + bill.Amount3 + bill.Amount4 + bill.Amount5 + bill.Amount6, "#,##0.00"));
				
				fraRateInfo.Text = "Rate Information";
				fraRateInfo.CenterToContainer(this.ClientArea);
				fraRateInfo.Visible = true;
				cmdRIClose.Focus();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowPaymentInfo(ARPayment payment)
		{
			try
			{
				if (payment != null)
				{
					vsRateInfo.Cols = 1;
					vsRateInfo.Cols = 5;
					vsRateInfo.Rows = 20;
					vsRateInfo.RowHeight(-1, 400);
					vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
					vsRateInfo.ColWidth(0, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.3));
					vsRateInfo.ColWidth(1, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.19));
					vsRateInfo.ColWidth(2, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.01));
					vsRateInfo.ColWidth(3, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.3));
					vsRateInfo.ColWidth(4, Convert.ToInt32(vsRateInfo.WidthOriginal * 0.19));
					vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);

					vsRateInfo.Select(19, 0, 19, 1);
					vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
					vsRateInfo.Select(15, 0, 15, 1);
					vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
					vsRateInfo.Select(0, 0);
					vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 19, 1, 19, 4,
						FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;

					vsRateInfo.TextMatrix(0, 0, "Account");
					vsRateInfo.TextMatrix(1, 0, "");
					vsRateInfo.TextMatrix(2, 0, "Bill ID");
					vsRateInfo.TextMatrix(3, 0, "");
					vsRateInfo.TextMatrix(4, 0, "Receipt Number");
					vsRateInfo.TextMatrix(5, 0, "");
					vsRateInfo.TextMatrix(6, 0, "Recorded Transaction");
					vsRateInfo.TextMatrix(7, 0, "Effective Interest Date");
					vsRateInfo.TextMatrix(8, 0, "Actual Transaction Date");
					vsRateInfo.TextMatrix(9, 0, "");
					vsRateInfo.TextMatrix(10, 0, "Teller ID");
					vsRateInfo.TextMatrix(11, 0, "");
					vsRateInfo.TextMatrix(12, 0, "Cash Drawer");
					vsRateInfo.TextMatrix(13, 0, "Affect Cash");
					vsRateInfo.TextMatrix(14, 0, "BD Account");
					vsRateInfo.TextMatrix(0, 3, "");
					vsRateInfo.TextMatrix(1, 3, "");
					vsRateInfo.TextMatrix(2, 3, "Reference");
					vsRateInfo.TextMatrix(3, 3, "Paid By");
					vsRateInfo.TextMatrix(4, 3, "Daily Close Out");
					vsRateInfo.TextMatrix(5, 3, "");
					vsRateInfo.TextMatrix(6, 3, "Principal");
					vsRateInfo.TextMatrix(7, 3, "Interest");
					vsRateInfo.TextMatrix(8, 3, "");
					vsRateInfo.TextMatrix(9, 3, "");
					vsRateInfo.TextMatrix(10, 3, "");
					vsRateInfo.TextMatrix(11, 3, "Total");
					vsRateInfo.TextMatrix(12, 3, "");
					vsRateInfo.TextMatrix(13, 3, "");
					vsRateInfo.TextMatrix(14, 3, "");
					vsRateInfo.TextMatrix(15, 3, "");
					vsRateInfo.TextMatrix(16, 3, "");
					vsRateInfo.TextMatrix(17, 3, "");
					vsRateInfo.TextMatrix(18, 3, "");
					vsRateInfo.TextMatrix(19, 0, "Comments");


					vsRateInfo.TextMatrix(0, 1, viewModel.Customer.CustomerId);

					vsRateInfo.TextMatrix(2, 1, payment.BillId);
					vsRateInfo.TextMatrix(4, 1, viewModel.GetPaymentReceiptNumber(payment.ReceiptId ?? 0, payment.ActualSystemDate));
					vsRateInfo.TextMatrix(5, 1, "");
					vsRateInfo.TextMatrix(6, 1,
						Strings.Format(payment.RecordedTransactionDate, "MM/dd/yyyy"));
					vsRateInfo.TextMatrix(7, 1,
						Strings.Format(payment.EffectiveInterestDate, "MM/dd/yyyy"));
					vsRateInfo.TextMatrix(8, 1,
						Strings.Format(payment.ActualSystemDate, "MM/dd/yyyy"));
					vsRateInfo.TextMatrix(9, 1, "");
					vsRateInfo.TextMatrix(10, 1, payment.Teller);
					if (payment.CashDrawer == "Y")
					{
						vsRateInfo.TextMatrix(12, 1, "YES");
						vsRateInfo.TextMatrix(13, 1, "YES");
					}
					else
					{
						vsRateInfo.TextMatrix(12, 1, "NO");
						if (payment.GeneralLedger == "Y")
						{
							vsRateInfo.TextMatrix(13, 1, "YES");
						}
						else
						{
							vsRateInfo.TextMatrix(13, 1, "NO");
							vsRateInfo.TextMatrix(14, 1, payment.BudgetaryAccountNumber);
						}
					}

					vsRateInfo.TextMatrix(0, 4, "");
					vsRateInfo.TextMatrix(1, 4, "");
					vsRateInfo.TextMatrix(2, 4, payment.Reference);
					vsRateInfo.TextMatrix(3, 4, payment.PaidBy);
					if (payment.DailyCloseOut != 0)
					{
						vsRateInfo.TextMatrix(4, 4, "True");
					}
					else
					{
						vsRateInfo.TextMatrix(4, 4, "False");
					}

					vsRateInfo.TextMatrix(5, 4, "");
		
					vsRateInfo.TextMatrix(6, 4, Strings.Format(payment.Principal, "#,##0.00"));
					vsRateInfo.TextMatrix(7, 4, Strings.Format(payment.Interest, "#,##0.00"));
					vsRateInfo.TextMatrix(8, 4, Strings.Format(payment.Tax, "#,##0.00"));
					vsRateInfo.TextMatrix(10, 4, "");
					vsRateInfo.TextMatrix(11, 4, Strings.Format(payment.Principal + payment.Interest + payment.Tax,"#,##0.00"));
					vsRateInfo.TextMatrix(19, 1, payment.Comments);
					vsRateInfo.TextMatrix(19, 2, vsRateInfo.TextMatrix(19, 1));
					vsRateInfo.TextMatrix(19, 3, vsRateInfo.TextMatrix(19, 1));
					vsRateInfo.TextMatrix(19, 4, vsRateInfo.TextMatrix(19, 1));
					vsRateInfo.MergeRow(19, true, 1 , 4);
				}
				else
				{
					vsRateInfo.TextMatrix(0, 1, "");
					vsRateInfo.TextMatrix(1, 1, "");
					vsRateInfo.TextMatrix(2, 1, "");
					vsRateInfo.TextMatrix(3, 1, "");
					vsRateInfo.TextMatrix(4, 1, "");
					vsRateInfo.TextMatrix(5, 1, "");
					vsRateInfo.TextMatrix(6, 1, "");
					vsRateInfo.TextMatrix(7, 1, "");
					vsRateInfo.TextMatrix(8, 1, "");
					vsRateInfo.TextMatrix(9, 1, "");
					vsRateInfo.TextMatrix(0, 4, "");
					vsRateInfo.TextMatrix(1, 4, "");
					vsRateInfo.TextMatrix(2, 4, "");
					vsRateInfo.TextMatrix(3, 4, "");
					vsRateInfo.TextMatrix(4, 4, "");
					vsRateInfo.TextMatrix(5, 0, "");
					vsRateInfo.TextMatrix(6, 0, "");
					vsRateInfo.TextMatrix(7, 0, "");
					vsRateInfo.TextMatrix(8, 0, "");
					vsRateInfo.TextMatrix(19, 0, "");
				}
					
				fraRateInfo.Text = "Payment Information";
				fraRateInfo.CenterToContainer(this.ClientArea);
				fraRateInfo.Visible = true;
				cmdRIClose.Focus();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Payment Information Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CheckCD()
		{
			if (txtCD.Text == "N")
			{
				txtCash.Enabled = true;
				txtCash.TabStop = true;
			}
			else
			{
				if (txtCash.Text == "N")
					txtCash.Text = "Y";
				txtCash.TabStop = false;
				txtCash.Enabled = false;
				txtAcctNumber.Enabled = false;
				txtAcctNumber.TabStop = false;
			}
		}

		private void CheckCash()
		{
			if (txtCD.Text == "N" && cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString().Right(1) != "*" && cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString().ToIntegerValue() != 0 && cmbCode.Items[cmbCode.SelectedIndex].ToString().Left(1) != "Y")
			{
				if (txtCash.Text == "N")
				{
					txtAcctNumber.Enabled = true;
					txtAcctNumber.TabStop = true;
				}
				else
				{
					txtAcctNumber.Enabled = false;
					txtAcctNumber.TabStop = false;
				}
			}
			else
			{
				if (txtCash.Text == "N")
					txtCash.Text = "Y";
				txtAcctNumber.Text = "";
			}
		}

		private void vsRateInfo_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsRateInfo[e.ColumnIndex, e.RowIndex];

            if (vsRateInfo.GetFlexRowIndex(e.RowIndex) >= 0 && vsRateInfo.GetFlexColIndex(e.ColumnIndex) >= 0)
			{
				//ToolTip1.SetToolTip(vsRateInfo, vsRateInfo.TextMatrix(vsRateInfo.MouseRow, vsRateInfo.MouseCol));
				cell.ToolTipText =  vsRateInfo.TextMatrix(vsRateInfo.GetFlexRowIndex(e.RowIndex), vsRateInfo.GetFlexColIndex(e.ColumnIndex));
			}
		}

		private void InputBoxAdjustment(bool boolCollapse)
		{
			double dblTemp = 0;
			if (txtPrincipal.Text.ToDecimalValue() == 0)
				txtPrincipal.Text = "0.00";
			if (txtInterest.Text.ToDecimalValue() == 0)
				txtInterest.Text = "0.00";
			if (txtTax.Text.ToDecimalValue() == 0)
				txtTax.Text = "0.00";

			txtPrincipal.Visible = !boolCollapse;
			txtTax.Visible = !boolCollapse;
			lblPrincipal.Visible = !boolCollapse;
			lblTax.Visible = !boolCollapse;
			if (boolCollapse)
			{
				dblTemp = FCConvert.ToDouble(FCConvert.ToDecimal(txtPrincipal.Text) + FCConvert.ToDecimal(txtTax.Text));
				txtPrincipal.Text = "0.00";
				txtTax.Text = "0.00";
				txtInterest.Text = Strings.Format(FCConvert.ToDouble(txtInterest.Text) + dblTemp, "#,##0.00");
				lblInterest.Text = "Amount";
			}
			else
			{
				lblInterest.Text = "Interest";
			}
		}

		private void vsPeriod_DblClick(object sender, System.EventArgs e)
		{
			int intCT;
			double dblAmt;
			dblAmt = FCConvert.ToDouble(vsPeriod.TextMatrix(0, 1));

			for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
			{
				if (Strings.Left(cmbBillNumber.Items[intCT].ToString(), 1) == "A")
				{
					cmbBillNumber.SelectedIndex = intCT;
					break;
				}
			}			
			vsPeriod.TextMatrix(0, 1, Strings.Format(dblAmt, "#,##0.00"));
			txtInterest.Text = Strings.Format(dblAmt, "#,##0.00");
		}

		private void cmdEffectiveDate_Click(object sender, EventArgs e)
		{
			mnuProcessEffective_Click(sender, e);
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			mnuFilePrint_Click(sender, e);
		}

		private void cmdChangeAccount_Click(object sender, EventArgs e)
		{
			mnuProcessChangeAccount_Click(sender, e);
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
			mnuPaymentSaveExit_Click(sender, e);
		}

		private void cmdPaymentSave_Click(object sender, EventArgs e)
		{
			SavePayment();
		}

		private void frmARPaymentStatus_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (cancelling)
			{
				ViewModel.Cancel();
			}
		}

		private void txtTotalTotal_DoubleClick(object sender, EventArgs e)
		{
			FillTotalPayment();
		}
	}
}
