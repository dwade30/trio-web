﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using Global;
using SharedApplication;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Extensions;

namespace TWAR0000.Receipting
{
	/// <summary>
	/// Summary description for frmARGetMasterAccount.
	/// </summary>
	public partial class frmCustomerSearch : BaseForm, IView<IAccountsReceivableCustomerSearchViewModel>
	{
		private IAccountsReceivableCustomerSearchViewModel viewModel { get; set; }
		private bool cancelling = true;
		private int lngColHidden;
		private int lngColAccountNumber;
		private int lngColName;
		private int lngColLocation;
		private int lngColInvNum;

		public frmCustomerSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.cmbSearchBy.SelectedIndex = 0;
		}
		
		public frmCustomerSearch(IAccountsReceivableCustomerSearchViewModel viewModel) : this()
		{
			this.Text = "Select Master Account";
			lngColHidden = 0;
			lngColAccountNumber = 1;
			lngColName = 2;
			lngColLocation = 3;
			lngColInvNum = 4;
			this.ViewModel = viewModel;
		}

		public IAccountsReceivableCustomerSearchViewModel ViewModel
		{
			get { return viewModel; }
			set
			{
				viewModel = value;
				SetupView();
			}
		}

		public void SetupView()
		{
			ShowLastAccount();
		}

		private void ShowLastAccount()
		{
			if (ViewModel.ShowLastAccount())
			{
				txtGetAccountNumber.Text = ViewModel.LastAccountAccessed.ToString();
			}
			if (txtGetAccountNumber.Enabled && txtGetAccountNumber.Visible)
			{
				txtGetAccountNumber.Focus();
			}
			txtGetAccountNumber.SelectionStart = 0;
			txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
		}

		public void StartProgram(int lngCustNumber)
		{
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Account");
			cancelling = false;
			Close();
			
			viewModel.LastAccountAccessed = lngCustNumber;
			viewModel.ProceedToNextScreen(lngCustNumber);
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			ClearSearch();
		}

		private void ClearSearch()
		{
			cmbSearchBy.SelectedIndex = 0;
			txtSearch.Text = "";
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			GetCustomerData();
		}

		private void GetCustomerData()
		{
			if (!viewModel.SearchResultsVisible)
			{
				int account = txtGetAccountNumber.Text.ToIntegerValue();
				if (account != 0)
				{
					viewModel.SetSelectedCustomer(account);
					if (viewModel.SelectedCustomer != null)
					{
						if (viewModel.SelectedCustomer.Deleted ?? false)
						{
							MessageBox.Show("Account has been deleted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SetAct(0);
							ShowLastAccount();
						}
						else
						{
							StartProgram(account);
						}
					}
					else
					{
						MessageBox.Show("There is no account with that number.", "Error Loading Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else
				{
					MessageBox.Show("Please select a valid customer.", "Missing Customer Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					if (txtGetAccountNumber.Visible && txtGetAccountNumber.Enabled)
					{
						txtGetAccountNumber.Focus();
					}
				}
			}
			else if (viewModel.SearchResultsVisible)
			{
				SelectAccount(vsSearch.Row);
			}
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			SearchCustomers();
		}

		private void SearchCustomers()
		{
			int lngCurrentAccountKey;
			string strSQL;

			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
			if (Strings.Trim(txtSearch.Text) != "")
			{
				viewModel.Search(cmbSearchBy.Text == "Name" ? AccountsReceivableCustomerSearchViewModel.SearchType.Name : cmbSearchBy.Text == "Address" ? AccountsReceivableCustomerSearchViewModel.SearchType.Address : AccountsReceivableCustomerSearchViewModel.SearchType.Invoice, txtSearch.Text.Trim());
			}
			else
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Please enter a search string.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			if (viewModel.SearchResults.Count() > 0)
			{
				if (viewModel.SearchResults.Count() > 1)
				{
					FillSearchGrid();
					SetAct(1);
					frmWait.InstancePtr.Unload();
				}
				else
				{
					StartProgram(viewModel.SearchResults.FirstOrDefault()?.CustomerId ?? 0);
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("No accounts found.", "No Results", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void SetAct(int intAct)
		{
			if (intAct == 0)
			{
				lblPaidInvoicesMsg.Visible = false;
				vsSearch.Visible = false;
				viewModel.SearchResultsVisible = false;
			}
			else if (intAct == 1)
			{
				if (cmbSearchBy.Text == "Invoice")
				{
					lblPaidInvoicesMsg.Visible = true;
				}
				else
				{
					lblPaidInvoicesMsg.Visible = false;
				}
				vsSearch.Visible = true;
				viewModel.SearchResultsVisible = true;
			}
			vsSearch.ColAlignment(lngColAccountNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void frmARGetMasterAccount_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
			vsSearch.Visible = false;
			fraCriteria.Visible = true;
		}

		private void frmARGetMasterAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (Strings.Trim(txtSearch.Text) != "" && (!viewModel.SearchResultsVisible || vsSearch.Row <= 0))
				{
					SearchCustomers();
				}
				else
				{
					GetCustomerData();
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (vsSearch.Visible)
				{
					SetAct(0);
				}
				else
				{
					Close();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmARGetMasterAccount_Resize(object sender, System.EventArgs e)
		{
			if (viewModel.SearchResultsVisible)
			{
				FormatGrid();
			}
		}

		private void mnuFileClearSearch_Click(object sender, System.EventArgs e)
		{
			ClearSearch();
		}

		private void mnuFileSearch_Click(object sender, System.EventArgs e)
		{
			SearchCustomers();
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSelectAccount_Click(object sender, System.EventArgs e)
		{
			GetCustomerData();
		}

		private void mnuUndeleteMaster_Click(object sender, System.EventArgs e)
		{
			int AcctNum = 0;
			string strAcctNum;

			strAcctNum = Interaction.InputBox("Please enter the Account Number that you wish to Undelete", "Undelete", "000000");
			if (Information.IsNumeric(strAcctNum) == false || strAcctNum.ToIntegerValue() == 0)
			{
				MessageBox.Show("Account Numbers must be numeric and nonzero.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			else
			{
				AcctNum = strAcctNum.ToIntegerValue();
				viewModel.FindCustomer(AcctNum, true);
				if (viewModel.SelectedCustomer == null)
				{
					MessageBox.Show("No account with that number has been deleted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					if (viewModel.UndeleteCustomer(viewModel.SelectedCustomer.CustomerId ?? 0))
					{
						MessageBox.Show("Account Number #" + FCConvert.ToString(AcctNum) + " has been undeleted.", "Successful Undeletion", MessageBoxButtons.OK, MessageBoxIcon.Information);
						StartProgram(AcctNum);
					}
				}
			}
		}

		private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

			if (KeyAscii == Keys.Right || KeyAscii == Keys.F13)
				KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillSearchGrid()
		{
			string strAcct = "";
			string strName = "";
			string strLocation = "";
			string strInvNum = "";
			string strCSZ;

			FormatGrid(true);
			foreach (var result in viewModel.SearchResults)
			{
				strAcct = result.CustomerId.ToString();
				strName = result.FullName;

				if (result.Address1 != "")
				{
					strLocation = result.Address1.Trim() + " " + result.Address2.ToString();
				}
				else
				{
					strLocation = result.Address1.Trim();
				}
				if (result.Address3.Trim() != "")
				{
					strLocation = strLocation + " " + result.Address3.Trim();
				}
				if (result.City.Trim() != "")
				{
					strLocation = strLocation + " " + " " + result.City.Trim();
					strLocation = strLocation.Trim();
				}
				if (result.Zip.Trim() != "")
				{
					strLocation = strLocation + " " + " " + result.Zip.Trim();
					strLocation = strLocation.Trim();
				}

				strInvNum = result.InvoiceNumber;

				vsSearch.AddItem("");
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAccountNumber, strAcct);
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strName);
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, strLocation);
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColInvNum, strInvNum);
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColHidden, result.Id);
				if (result.Paid)
				{
					vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, Color.Yellow);
				}
			}
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsSearch.WidthOriginal;
			if (boolReset)
			{
				vsSearch.Rows = 1;
				vsSearch.TextMatrix(0, lngColAccountNumber, "Acct");
				vsSearch.TextMatrix(0, lngColName, "Name");
				vsSearch.TextMatrix(0, lngColLocation, "Address");
				vsSearch.TextMatrix(0, lngColInvNum, "Invoice");
				vsSearch.Cols = 5;
				vsSearch.ExtendLastCol = true;
			}
			if (cmbSearchBy.Text == "Invoice")
			{
				vsSearch.ColWidth(lngColAccountNumber, FCConvert.ToInt32(lngWid * 0.1));
				vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.29));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.29));
				vsSearch.ColWidth(lngColInvNum, FCConvert.ToInt32(lngWid * 0.29));
			}
			else
			{
				vsSearch.ColWidth(lngColHidden, 0);
				vsSearch.ColWidth(lngColAccountNumber, FCConvert.ToInt32(lngWid * 0.19));
				vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.39));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.39));
                vsSearch.ColWidth(lngColInvNum, 0);
			}
			vsSearch.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
		}

		private void SelectAccount(int lngRow)
		{
			int lngAcct = 0;
			if (lngRow > 0)
			{
				
				lngAcct = vsSearch.TextMatrix(lngRow, lngColAccountNumber).ToIntegerValue();
				if (lngAcct > 0)
				{
					StartProgram(lngAcct);
				}
			}
			SetAct(0);
		}

		private void vsSearch_DblClick(object sender, System.EventArgs e)
		{
			if (vsSearch.Row > 0)
			{
				SelectAccount(vsSearch.MouseRow);
			}
		}

		private void frmCustomerSearch_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (cancelling)
			{
				ViewModel.Cancel();
			}
		}
	}
}
