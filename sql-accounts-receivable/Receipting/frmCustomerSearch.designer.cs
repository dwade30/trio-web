//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWAR0000.Receipting
{
	/// <summary>
	/// Summary description for frmARGetMasterAccount.
	/// </summary>
	partial class frmCustomerSearch : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSearchBy;
		public fecherFoundation.FCLabel lblHidden;
		public fecherFoundation.FCPanel fraCriteria;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCLabel Label1;
		public FCGrid vsSearch;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSelectAccount;
		public fecherFoundation.FCToolStripMenuItem mnuFileSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFileClearSearch;
		public fecherFoundation.FCToolStripMenuItem mnuUndeleteMaster;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbSearchBy = new fecherFoundation.FCComboBox();
			this.lblHidden = new fecherFoundation.FCLabel();
			this.fraCriteria = new fecherFoundation.FCPanel();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.lblSearchInfo = new fecherFoundation.FCLabel();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.vsSearch = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSelectAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClearSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUndeleteMaster = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmbUndelete = new fecherFoundation.FCButton();
			this.lblPaidInvoicesMsg = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).BeginInit();
			this.fraCriteria.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbUndelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdGetAccountNumber);
			this.BottomPanel.Location = new System.Drawing.Point(0, 423);
			this.BottomPanel.Size = new System.Drawing.Size(915, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblPaidInvoicesMsg);
			this.ClientArea.Controls.Add(this.fraCriteria);
			this.ClientArea.Controls.Add(this.vsSearch);
			this.ClientArea.Size = new System.Drawing.Size(915, 363);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmbUndelete);
			this.TopPanel.Controls.Add(this.cmdQuit);
			this.TopPanel.Controls.Add(this.cmdSearch);
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(915, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdQuit, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmbUndelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(258, 30);
			this.HeaderText.Text = "Select Master Account";
			// 
			// cmbSearchBy
			// 
			this.cmbSearchBy.Items.AddRange(new object[] {
            "Name",
            "Address",
            "Invoice"});
			this.cmbSearchBy.Location = new System.Drawing.Point(386, 29);
			this.cmbSearchBy.Name = "cmbSearchBy";
			this.cmbSearchBy.Size = new System.Drawing.Size(135, 40);
			this.cmbSearchBy.TabIndex = 9;
			// 
			// lblHidden
			// 
			this.lblHidden.AutoSize = true;
			this.lblHidden.Location = new System.Drawing.Point(288, 44);
			this.lblHidden.Name = "lblHidden";
			this.lblHidden.Size = new System.Drawing.Size(85, 16);
			this.lblHidden.TabIndex = 10;
			this.lblHidden.Text = "SEARCH BY";
			// 
			// fraCriteria
			// 
			this.fraCriteria.Controls.Add(this.txtSearch);
			this.fraCriteria.Controls.Add(this.Frame2);
			this.fraCriteria.Controls.Add(this.cmbSearchBy);
			this.fraCriteria.Controls.Add(this.txtGetAccountNumber);
			this.fraCriteria.Controls.Add(this.lblHidden);
			this.fraCriteria.Controls.Add(this.lblLastAccount);
			this.fraCriteria.Controls.Add(this.Label1);
			this.fraCriteria.Name = "fraCriteria";
			this.fraCriteria.Size = new System.Drawing.Size(910, 90);
			this.fraCriteria.TabIndex = 1;
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Location = new System.Drawing.Point(534, 30);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(181, 40);
			this.txtSearch.TabIndex = 8;
			this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.lblSearchInfo);
			this.Frame2.Location = new System.Drawing.Point(30, 93);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(755, 93);
			this.Frame2.TabIndex = 5;
			this.Frame2.Text = "Search";
			this.Frame2.Visible = false;
			// 
			// lblSearchInfo
			// 
			this.lblSearchInfo.AutoSize = true;
			this.lblSearchInfo.Location = new System.Drawing.Point(284, 45);
			this.lblSearchInfo.Name = "lblSearchInfo";
			this.lblSearchInfo.Size = new System.Drawing.Size(253, 16);
			this.lblSearchInfo.TabIndex = 13;
			this.lblSearchInfo.Text = "ENTER THE CRITERIA TO SEARCH BY";
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(121, 30);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(136, 40);
			this.txtGetAccountNumber.TabIndex = 3;
			this.txtGetAccountNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGetAccountNumber_KeyPress);
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.Location = new System.Drawing.Point(591, 44);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(58, 20);
			this.lblLastAccount.TabIndex = 16;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(74, 16);
			this.Label1.TabIndex = 14;
			this.Label1.Text = "ACCOUNT";
			// 
			// cmdSearch
			// 
			this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSearch.ImageSource = "button-search";
			this.cmdSearch.Location = new System.Drawing.Point(800, 29);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdSearch.TabIndex = 7;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(702, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(92, 24);
			this.cmdClear.TabIndex = 6;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// cmdQuit
			// 
			this.cmdQuit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdQuit.Location = new System.Drawing.Point(481, 29);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(39, 24);
			this.cmdQuit.TabIndex = 4;
			this.cmdQuit.Text = "Exit";
			this.cmdQuit.Visible = false;
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(332, 30);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(139, 48);
			this.cmdGetAccountNumber.TabIndex = 2;
			this.cmdGetAccountNumber.Text = "Select Account";
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// vsSearch
			// 
			this.vsSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsSearch.Cols = 5;
			this.vsSearch.FixedCols = 0;
			this.vsSearch.Location = new System.Drawing.Point(30, 137);
			this.vsSearch.Name = "vsSearch";
			this.vsSearch.RowHeadersVisible = false;
			this.vsSearch.Rows = 1;
			this.vsSearch.Size = new System.Drawing.Size(856, 202);
			this.vsSearch.TabIndex = 3;
			this.vsSearch.Visible = false;
			this.vsSearch.DoubleClick += new System.EventHandler(this.vsSearch_DblClick);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSelectAccount,
            this.mnuFileSearch,
            this.mnuFileClearSearch,
            this.mnuUndeleteMaster,
            this.Seperator,
            this.mnuQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSelectAccount
			// 
			this.mnuSelectAccount.Index = 0;
			this.mnuSelectAccount.Name = "mnuSelectAccount";
			this.mnuSelectAccount.Text = "Select Account";
			this.mnuSelectAccount.Click += new System.EventHandler(this.mnuSelectAccount_Click);
			// 
			// mnuFileSearch
			// 
			this.mnuFileSearch.Index = 1;
			this.mnuFileSearch.Name = "mnuFileSearch";
			this.mnuFileSearch.Text = "Search";
			this.mnuFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// mnuFileClearSearch
			// 
			this.mnuFileClearSearch.Index = 2;
			this.mnuFileClearSearch.Name = "mnuFileClearSearch";
			this.mnuFileClearSearch.Text = "Clear Search";
			this.mnuFileClearSearch.Click += new System.EventHandler(this.mnuFileClearSearch_Click);
			// 
			// mnuUndeleteMaster
			// 
			this.mnuUndeleteMaster.Index = 3;
			this.mnuUndeleteMaster.Name = "mnuUndeleteMaster";
			this.mnuUndeleteMaster.Text = "Undelete Master Account";
			this.mnuUndeleteMaster.Click += new System.EventHandler(this.mnuUndeleteMaster_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 5;
			this.mnuQuit.Name = "mnuQuit";
			this.mnuQuit.Text = "Exit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// cmbUndelete
			// 
			this.cmbUndelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmbUndelete.Location = new System.Drawing.Point(525, 29);
			this.cmbUndelete.Name = "cmbUndelete";
			this.cmbUndelete.Size = new System.Drawing.Size(172, 24);
			this.cmbUndelete.TabIndex = 8;
			this.cmbUndelete.Text = "Undelete Master Account";
			this.cmbUndelete.Click += new System.EventHandler(this.mnuUndeleteMaster_Click);
			// 
			// lblPaidInvoicesMsg
			// 
			this.lblPaidInvoicesMsg.AutoSize = true;
			this.lblPaidInvoicesMsg.Location = new System.Drawing.Point(30, 109);
			this.lblPaidInvoicesMsg.Name = "lblPaidInvoicesMsg";
			this.lblPaidInvoicesMsg.Size = new System.Drawing.Size(465, 16);
			this.lblPaidInvoicesMsg.TabIndex = 2;
			this.lblPaidInvoicesMsg.Text = "HIGHLIGHTED ROWS INDICATE THAT THE SELECTED INVOICE IS PAID";
			this.lblPaidInvoicesMsg.Visible = false;
			// 
			// frmCustomerSearch
			// 
			this.ClientSize = new System.Drawing.Size(915, 531);
			this.KeyPreview = true;
			this.Name = "frmCustomerSearch";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select Master Account";
			this.Activated += new System.EventHandler(this.frmARGetMasterAccount_Activated);
			this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmCustomerSearch_FormClosing);
			this.Resize += new System.EventHandler(this.frmARGetMasterAccount_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmARGetMasterAccount_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).EndInit();
			this.fraCriteria.ResumeLayout(false);
			this.fraCriteria.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbUndelete)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmbUndelete;
		private FCLabel lblPaidInvoicesMsg;
	}
}