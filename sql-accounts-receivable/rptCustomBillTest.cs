using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptCustomBillTest.
	/// </summary>
	public partial class rptCustomBillTest : GrapeCity.ActiveReports.SectionReport
	{

		public static rptCustomBillTest InstancePtr
		{
			get
			{
				return (rptCustomBillTest)Sys.GetInstance(typeof(rptCustomBillTest));
			}
		}

		protected rptCustomBillTest _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptCustomBillTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
	}
}
