﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmGetCustomerMaster.
	/// </summary>
	partial class frmGetCustomerMaster : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCLabel lblSearchType;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCButton cmdGet;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblLastAccount;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbSearchType = new fecherFoundation.FCComboBox();
			this.lblSearchType = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.cmdGet = new fecherFoundation.FCButton();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.vs1 = new fecherFoundation.FCGrid();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.Label5 = new fecherFoundation.FCLabel();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.lblSearchInfo = new fecherFoundation.FCLabel();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdGetAccountNumber);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblLastAccount);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Controls.Add(this.cmdSearch);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.TabIndex = 0;
			this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(165, 30);
			this.HeaderText.Text = "Get Customer";
			// 
			// cmbSearchType
			// 
			this.cmbSearchType.Items.AddRange(new object[] {
            "Name",
            "Address"});
			this.cmbSearchType.Location = new System.Drawing.Point(388, 23);
			this.cmbSearchType.Name = "cmbSearchType";
			this.cmbSearchType.Size = new System.Drawing.Size(135, 40);
			this.cmbSearchType.TabIndex = 2;
			// 
			// lblSearchType
			// 
			this.lblSearchType.AutoSize = true;
			this.lblSearchType.Location = new System.Drawing.Point(287, 38);
			this.lblSearchType.Name = "lblSearchType";
			this.lblSearchType.Size = new System.Drawing.Size(85, 16);
			this.lblSearchType.TabIndex = 1;
			this.lblSearchType.Text = "SEARCH BY";
			// 
			// Frame3
			// 
			this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.BackColor = System.Drawing.Color.White;
			this.Frame3.Controls.Add(this.cmdGet);
			this.Frame3.Controls.Add(this.cmdReturn);
			this.Frame3.Controls.Add(this.vs1);
			this.Frame3.Location = new System.Drawing.Point(30, 86);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(1045, 403);
			this.Frame3.TabIndex = 3;
			this.Frame3.Text = "Multiple Records";
			this.Frame3.Visible = false;
			// 
			// cmdGet
			// 
			this.cmdGet.AppearanceKey = "actionButton";
			this.cmdGet.Location = new System.Drawing.Point(226, 375);
			this.cmdGet.Name = "cmdGet";
			this.cmdGet.Size = new System.Drawing.Size(155, 40);
			this.cmdGet.TabIndex = 2;
			this.cmdGet.Text = "Retrieve Record";
			this.cmdGet.Visible = false;
			this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(0, 375);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(211, 40);
			this.cmdReturn.TabIndex = 1;
			this.cmdReturn.Text = "Return to Search Screen";
			this.cmdReturn.Visible = false;
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// vs1
			// 
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.Cols = 4;
			this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vs1.ExtendLastCol = true;
			this.vs1.FixedCols = 0;
			this.vs1.Location = new System.Drawing.Point(0, 30);
			this.vs1.Name = "vs1";
			this.vs1.RowHeadersVisible = false;
			this.vs1.Rows = 1;
			this.vs1.ShowFocusCell = false;
			this.vs1.Size = new System.Drawing.Size(1015, 325);
			this.vs1.TabIndex = 3;
			this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(121, 23);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(136, 40);
			this.txtGetAccountNumber.TabIndex = 1;
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(274, 30);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(120, 48);
			this.cmdGetAccountNumber.Text = "Process";
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxNoBorders";
			this.Frame2.Controls.Add(this.txtGetAccountNumber);
			this.Frame2.Controls.Add(this.Label5);
			this.Frame2.Controls.Add(this.cmbSearchType);
			this.Frame2.Controls.Add(this.lblSearchType);
			this.Frame2.Controls.Add(this.txtSearch);
			this.Frame2.Controls.Add(this.lblSearchInfo);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(1025, 87);
			this.Frame2.TabIndex = 4;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(30, 38);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(60, 33);
			this.Label5.TabIndex = 17;
			this.Label5.Text = "ACCOUNT";
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Location = new System.Drawing.Point(544, 23);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(181, 40);
			this.txtSearch.TabIndex = 3;
			this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
			// 
			// lblSearchInfo
			// 
			this.lblSearchInfo.AutoSize = true;
			this.lblSearchInfo.Location = new System.Drawing.Point(616, 44);
			this.lblSearchInfo.Name = "lblSearchInfo";
			this.lblSearchInfo.Size = new System.Drawing.Size(179, 16);
			this.lblSearchInfo.TabIndex = 4;
			this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
			this.lblSearchInfo.Visible = false;
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(923, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(93, 24);
			this.cmdClear.TabIndex = 1;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSearch.ImageSource = "button-search";
			this.cmdSearch.Location = new System.Drawing.Point(1020, 29);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdSearch.TabIndex = 1;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(177, 20);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "LAST CUSTOMER ACCESSED ";
			this.Label2.Visible = false;
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.Location = new System.Drawing.Point(440, 30);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(55, 19);
			this.lblLastAccount.TabIndex = 2;
			this.lblLastAccount.Visible = false;
			// 
			// frmGetCustomerMaster
			// 
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.KeyPreview = true;
			this.Name = "frmGetCustomerMaster";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Get Customer";
			this.Load += new System.EventHandler(this.frmGetCustomerMaster_Load);
			this.Activated += new System.EventHandler(this.frmGetCustomerMaster_Activated);
			this.Resize += new System.EventHandler(this.frmGetCustomerMaster_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetCustomerMaster_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetCustomerMaster_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdGet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
