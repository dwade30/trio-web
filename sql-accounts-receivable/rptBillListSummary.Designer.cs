﻿namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptBillListSummary.
	/// </summary>
	partial class rptBillListSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillListSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldInvoiceNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOptions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCustomer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCustomerNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCustomerTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldInvoiceNumber,
				this.fldBillDate,
				this.fldAmount,
				this.fldType,
				this.fldStatus
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldInvoiceNumber
			// 
			this.fldInvoiceNumber.Height = 0.1875F;
			this.fldInvoiceNumber.Left = 2.53125F;
			this.fldInvoiceNumber.Name = "fldInvoiceNumber";
			this.fldInvoiceNumber.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldInvoiceNumber.Text = "Field1";
			this.fldInvoiceNumber.Top = 0F;
			this.fldInvoiceNumber.Width = 0.84375F;
			// 
			// fldBillDate
			// 
			this.fldBillDate.Height = 0.1875F;
			this.fldBillDate.Left = 3.53125F;
			this.fldBillDate.Name = "fldBillDate";
			this.fldBillDate.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldBillDate.Text = "Field1";
			this.fldBillDate.Top = 0F;
			this.fldBillDate.Width = 0.9375F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 6.84375F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.9375F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 4.5625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.fldType.Text = "Field1";
			this.fldType.Top = 0F;
			this.fldType.Width = 1.75F;
			// 
			// fldStatus
			// 
			this.fldStatus.Height = 0.1875F;
			this.fldStatus.Left = 1.8125F;
			this.fldStatus.Name = "fldStatus";
			this.fldStatus.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldStatus.Text = "Field1";
			this.fldStatus.Top = 0F;
			this.fldStatus.Width = 0.65625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblOptions,
				this.lblDateRange,
				this.Line4,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13
			});
			this.PageHeader.Height = 0.8854167F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.53125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Bill List";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.15625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblOptions
			// 
			this.lblOptions.Height = 0.1875F;
			this.lblOptions.HyperLink = null;
			this.lblOptions.Left = 0.375F;
			this.lblOptions.Name = "lblOptions";
			this.lblOptions.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblOptions.Text = "Bill List";
			this.lblOptions.Top = 0.40625F;
			this.lblOptions.Width = 7.625F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 1.53125F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Bill List";
			this.lblDateRange.Top = 0.21875F;
			this.lblDateRange.Width = 5.15625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.875F;
			this.Line4.Width = 7.90625F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.90625F;
			this.Line4.Y1 = 0.875F;
			this.Line4.Y2 = 0.875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2.53125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label9.Text = "Invoice #";
			this.Label9.Top = 0.6875F;
			this.Label9.Width = 0.84375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.53125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label10.Text = "Date ";
			this.Label10.Top = 0.6875F;
			this.Label10.Width = 0.9375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.84375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label11.Text = "Total";
			this.Label11.Top = 0.6875F;
			this.Label11.Width = 0.9375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.5625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label12.Text = "Type";
			this.Label12.Top = 0.6875F;
			this.Label12.Width = 1.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.8125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label13.Text = "Status";
			this.Label13.Top = 0.6875F;
			this.Label13.Width = 0.65625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Binder,
				this.fldCustomer,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.fldCustomerNumber
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.9791667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 4.21875F;
			this.Binder.Name = "Binder";
			this.Binder.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.125F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.5625F;
			// 
			// fldCustomer
			// 
			this.fldCustomer.Height = 0.1875F;
			this.fldCustomer.Left = 0.46875F;
			this.fldCustomer.Name = "fldCustomer";
			this.fldCustomer.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldCustomer.Text = "Field1";
			this.fldCustomer.Top = 0.03125F;
			this.fldCustomer.Width = 2.28125F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 0.46875F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldAddress1.Text = "Field1";
			this.fldAddress1.Top = 0.21875F;
			this.fldAddress1.Width = 2.28125F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 0.46875F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldAddress2.Text = "Field1";
			this.fldAddress2.Top = 0.40625F;
			this.fldAddress2.Width = 2.28125F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 0.46875F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldAddress3.Text = "Field1";
			this.fldAddress3.Top = 0.59375F;
			this.fldAddress3.Width = 2.28125F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 0.46875F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldAddress4.Text = "Field1";
			this.fldAddress4.Top = 0.78125F;
			this.fldAddress4.Width = 2.28125F;
			// 
			// fldCustomerNumber
			// 
			this.fldCustomerNumber.Height = 0.1875F;
			this.fldCustomerNumber.Left = 0F;
			this.fldCustomerNumber.Name = "fldCustomerNumber";
			this.fldCustomerNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.fldCustomerNumber.Text = "0000";
			this.fldCustomerNumber.Top = 0.03125F;
			this.fldCustomerNumber.Width = 0.4375F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line3,
				this.Field3,
				this.fldCustomerTotal
			});
			this.GroupFooter1.Height = 0.2604167F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 5.28125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 2.53125F;
			this.Line3.X1 = 5.28125F;
			this.Line3.X2 = 7.8125F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.19F;
			this.Field3.Left = 5.28125F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Field3.Text = "Customer Total:";
			this.Field3.Top = 0.03125F;
			this.Field3.Width = 1.5F;
			// 
			// fldCustomerTotal
			// 
			this.fldCustomerTotal.Height = 0.19F;
			this.fldCustomerTotal.Left = 6.8125F;
			this.fldCustomerTotal.Name = "fldCustomerTotal";
			this.fldCustomerTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldCustomerTotal.Text = "Field1";
			this.fldCustomerTotal.Top = 0.03125F;
			this.fldCustomerTotal.Width = 1F;
			// 
			// rptBillListSummary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptBillListSummary_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceNumber)).EndInit();
            this.ReportEnd += new System.EventHandler(rptBillListSummary_ReportEnd);
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatus;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOptions;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomerNumber;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomerTotal;
	}
}
