﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using TWSharedLibrary;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptBillTransferReport.
	/// </summary>
	public partial class rptBillTransferReport : BaseSectionReport
	{
		public static rptBillTransferReport InstancePtr
		{
			get
			{
				return (rptBillTransferReport)Sys.GetInstance(typeof(rptBillTransferReport));
			}
		}

		protected rptBillTransferReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillTransferReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/07/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/12/2006              *
		// ********************************************************
		public int lngRateKey;
		clsDRWrapper rsData = new clsDRWrapper();
		public double dblTotalSumAmount1;
		public double dblTotalSumAmount3;
		public double dblTotalSumAmount4;
		public double dblTotalSumAmount5;
		public double dblTotalSumAmount6;
		public int lngBillCount;
		int lngCT;
		public DateTime? dtBillingDate;
		public int lngOrder;

		public rptBillTransferReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Created Bills Report";
            this.ReportEnd += RptBillTransferReport_ReportEnd;
		}

        private void RptBillTransferReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        // vbPorter upgrade warning: dtBillDate As DateTime	OnWrite(string, DateTime)
        public void Init(int lngRK, DateTime dtBillDate)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngRateKey = lngRK;
				dtBillingDate = dtBillDate;
				frmReportViewer.InstancePtr.Init(this);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				//clsDRWrapper rsOrder = new clsDRWrapper();
				// force this report to be landscape
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				// this loads the data
				if (LoadAccounts())
				{
					// rock on
					FillHeaderLabels();
					lblReportType.Text = "Rate Record : " + FCConvert.ToString(lngRateKey);
					lblReportType.Text = lblReportType.Text + "    Interest As Of: " + Strings.Format(dtBillingDate, "MM/dd/yyyy");
				}
				else
				{
					MessageBox.Show("No eligible accounts found for rate key " + FCConvert.ToString(lngRateKey) + ".", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss AMPM");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblAmount = 0;
				double dblCurInt = 0;
				double dblCHGInt = 0;
				double dblTotal = 0;
				double dblPaid = 0;
				if (!rsData.EndOfFile())
				{
					dblCurInt = 0;
					dblCHGInt = 0;
					dblPaid = 0;
					fldAcctNum.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ActualAccountNumber"));
					fldName.Text = rsData.Get_Fields_String("BName");
					fldInvoice.Text = rsData.Get_Fields_String("InvoiceNumber");
					if (rsData.Get_Fields_Boolean("Prorated") == true)
					{
						fldInvoice.Text = "*" + fldInvoice.Text;
					}
					fldRegular.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("PrinOwed")), "#,##0.00");
					fldTax.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("TaxOwed")), "#,##0.00");
					// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
					dblPaid = FCConvert.ToDouble(rsData.Get_Fields("PrinPaid") + rsData.Get_Fields("TaxPaid") + rsData.Get_Fields("IntPaid"));
					dblTotal = modARCalculations.CalculateAccountARTotal(FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey")), ref dblCurInt, ref dblCHGInt, ref dtBillingDate);
					if (dblTotal != 0)
					{
						fldPastDue.Text = Strings.Format(dblTotal - (dblCurInt + dblCHGInt) - FCConvert.ToDouble(fldRegular.Text) - FCConvert.ToDouble(fldTax.Text) + dblPaid, "#,##0.00");
						// remove the totals from this bill and the interest amounts
						fldInterest.Text = Strings.Format(dblCurInt + dblCHGInt, "#,##0.00");
					}
					else
					{
						fldPastDue.Text = "0.00";
						fldInterest.Text = "0.00";
					}
					dblAmount = FCConvert.ToDouble(fldRegular.Text) + FCConvert.ToDouble(fldTax.Text) + FCConvert.ToDouble(fldPastDue.Text) + FCConvert.ToDouble(fldInterest.Text);
					fldAmount.Text = Strings.Format(dblAmount, "#,##0.00");
					// update the master totals
					dblTotalSumAmount1 += FCConvert.ToDouble(fldRegular.Text);
					dblTotalSumAmount3 += FCConvert.ToDouble(fldTax.Text);
					dblTotalSumAmount4 += FCConvert.ToDouble(fldPastDue.Text);
					dblTotalSumAmount5 += FCConvert.ToDouble(fldInterest.Text);
					dblTotalSumAmount6 += FCConvert.ToDouble(fldAmount.Text);
					lngBillCount += 1;
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fldTotalRegular.Text = Strings.Format(dblTotalSumAmount1, "#,##0.00");
				fldTotalTax.Text = Strings.Format(dblTotalSumAmount3, "#,##0.00");
				fldTotalPastDue.Text = Strings.Format(dblTotalSumAmount4, "#,##0.00");
				fldTotalInterest.Text = Strings.Format(dblTotalSumAmount5, "#,##0.00");
				fldTotalAmount.Text = Strings.Format(dblTotalSumAmount6, "#,##0.00");
				if (lngBillCount == 1)
				{
					fldFooter.Text = "Total: " + "\r\n" + "1 bill";
				}
				else
				{
					fldFooter.Text = "Total: " + "\r\n" + FCConvert.ToString(lngBillCount) + " bills";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Totals", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			FillTotals();
		}

		private bool LoadAccounts()
		{
			bool LoadAccounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				rsData.OpenRecordset("SELECT * FROM Bill WHERE BillStatus <> 'V' AND BillNumber = " + FCConvert.ToString(lngRateKey) + " ORDER BY BName", modCLCalculations.strARDatabase);
				if (!rsData.EndOfFile())
				{
					LoadAccounts = true;
				}
				else
				{
					LoadAccounts = false;
				}
				return LoadAccounts;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadAccounts;
		}

		private void rptBillTransferReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBillTransferReport.Caption	= "Created Bills Report";
			//rptBillTransferReport.Icon	= "rptBillTransferReport.dsx":0000";
			//rptBillTransferReport.Left	= 0;
			//rptBillTransferReport.Top	= 0;
			//rptBillTransferReport.Width	= 11880;
			//rptBillTransferReport.Height	= 6585;
			//rptBillTransferReport.WindowState	= 2;
			//rptBillTransferReport.SectionData	= "rptBillTransferReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
