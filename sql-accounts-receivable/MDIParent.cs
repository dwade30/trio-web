﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Messaging;
using TWSharedLibrary;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for MDIParent.
	/// </summary>
	public class MDIParent
	//: BaseForm
	{
		public MDIParent()
		{
			//
			// Required for Windows Form Designer support
			//
			//
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// Last Updated:  6/27/01
		private int[] LabelNumber = new int[200 + 1];
		int CurRow;
		int lngFCode;
		const string strTrio = "TRIO Software Corporation";
		private CommandDispatcher commandDispatcher;

		public void Init(CommandDispatcher commandDispatcher)
		{
			modGlobalFunctions.LoadTRIOColors();
			this.commandDispatcher = commandDispatcher;
			App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
			App.MainForm.NavigationMenu.Owner = this;
            App.MainForm.NavigationMenu.OriginName = "Accounts Receivable";
			App.MainForm.menuTree.ImageList = CreateImageList();
			GetMainMenu();
		}

		public ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-billing-process", "billing-process"),
				new ImageListEntry("menutree-setup-projects", "setup-projects"),
				new ImageListEntry("menutree-bills", "bills"),
				new ImageListEntry("menutree-collections", "collections"),
				new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
				new ImageListEntry("menutree-printing-active", "printing-active"),
				new ImageListEntry("menutree-billing-process-active", "billing-process-active"),
				new ImageListEntry("menutree-setup-projects-active", "setup-projects-active"),
				new ImageListEntry("menutree-bills-active", "bills-active"),
				new ImageListEntry("menutree-collections-active", "collections-active"),
			});
			return imageList;
		}

		public void Menu1()
		{
			object ans;
			string strRec1 = "";
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmGetCustomerMaster.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Bill")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmBillSelect.InstancePtr.intMenuOption = 1;
				frmBillSelect.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "File")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmCustomize.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Collections")
			{
				commandDispatcher.Send(new MakeAccountsReceivableTransaction
				{
					CorrelationId = Guid.NewGuid(),
					Id = Guid.NewGuid(),
					AccountNumber = 0,
					StartedFromCashReceipts = false,
					ShowPaymentScreen = false
				});

				//FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//frmARGetMasterAccount.InstancePtr.Init(1);
				//// 1 is status, 2 is payment
				//FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmCustomerListSetup.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu10()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
			}
			else
			{
			}
		}

		public void Menu11()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu11_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu12()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu12_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu13()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu13_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu14()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu14_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu15()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu15_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
			}
			else
			{
			}
		}

		public void Menu16()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu16_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
			}
			else
			{
			}
		}

		public void Menu17()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu17_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
			}
			else
			{
			}
		}

		public void Menu18()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu18_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
			}
			else
			{
			}
		}

		public void Menu2()
		{
			int ans;
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();

			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				//FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//GetBillingMenu();
				//FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "File")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				FCGlobal.Screen.MousePointer = 0;
				MessageBox.Show("Database update complete", "Update Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else if (vbPorterVar == "Bill")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmBillSelect.InstancePtr.intMenuOption = 2;
				frmBillSelect.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Collections")
			{
				commandDispatcher.Send(new MakeAccountsReceivableTransaction
				{
					CorrelationId = Guid.NewGuid(),
					Id = Guid.NewGuid(),
					AccountNumber = 0,
					StartedFromCashReceipts = false,
					ShowPaymentScreen = true
				});

				//FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//frmARGetMasterAccount.InstancePtr.Init(2);
				//// 1 is status, 2 is payment
				//FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmARStatusList.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu3()
		{
			int ans;
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu3_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				//FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//GetCollectionsMenu();
				//FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "File")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmCustomBill.InstancePtr.Init("AR");
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Bill")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmBillSelect.InstancePtr.intMenuOption = 3;
				frmBillSelect.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmCustomerLabels.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Collections")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmARDailyAudit.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu4()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu4_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				//FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//GetPrintMenu();
				//FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Bill")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmLoadBack.InstancePtr.strType = "M";
				frmLoadBack.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "File")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// Edit Rate Key Information
				frmRateRecChoice.InstancePtr.intRateType = 200;
				frmRateRecChoice.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Collections")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmLoadBack.InstancePtr.strType = "L";
				frmLoadBack.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmBillTypeListing.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu5()
		{
			int ans;
			clsDRWrapper rs = new clsDRWrapper();
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu5_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmTypeSetup.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "File")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmAREditBillInfo.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Bill")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmBillVoid.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Collections")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmChargeInterest.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmBillList.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu6()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu6_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				//FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//GetFileMenu();
				//FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "File")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmGetPayee.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Bill")
			{
				modARCalculations.CreateDemandJournal();
			}
			else if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmARRPTViewer.InstancePtr.Init("LastARDailyAudit", "Recreate Accounts Receivable Daily Audit", 1);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Collections")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				GetMainMenu();
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu7()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu7_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				modBlockEntry.WriteYY();
				//FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
                App.MainForm.OpenModule("TWGNENTY");
				/*? On Error Resume Next  *//*? On Error GoTo 0 */
				foreach (Form f in FCGlobal.Statics.Forms)
				{
					f.Close();
				}
			}
			else if (vbPorterVar == "Bill")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				GetMainMenu();
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// Created Bills Report
				frmRateRecChoice.InstancePtr.intRateType = 201;
				frmRateRecChoice.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "File")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				GetMainMenu();
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu8()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu8_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmSetupARStatement.InstancePtr.Show(App.MainForm);
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		public void Menu9()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "lblMenu9_Click"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				FCGlobal.Screen.MousePointer = 0;
			}
			else if (vbPorterVar == "Print")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				GetMainMenu();
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
			}
		}

		//private void GRID_KeyDownEvent(object sender, KeyEventArgs e)
		//{
		//	int intCounter;
		//	int intPlaceHolder = 0;
		//	if (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9)
		//	{
		//		//FC:TODO:AM
		//		//e.KeyCode -= 48;
		//	}
		//	if ((e.KeyCode == Keys.Down) || (e.KeyCode == Keys.Up))
		//	{
		//		intPlaceHolder = GRID.Row;
		//		if (e.KeyCode == Keys.Down)
		//		{
		//			KeepGoing:
		//			if ((GRID.Row + 1) <= (GRID.Rows - 1))
		//			{
		//				GRID.Row += 1;
		//				if (GRID.TextMatrix(GRID.Row, 1) == string.Empty)
		//				{
		//					GRID.Row -= 1;
		//				}
		//				else if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				{
		//					goto KeepGoing;
		//				}
		//			}
		//		}
		//		else
		//		{
		//			KeepGoingUP:
		//			if ((GRID.Row - 1) >= 1)
		//			{
		//				GRID.Row -= 1;
		//				if (GRID.Row < 1)
		//					GRID.Row = 1;
		//				if (GRID.TextMatrix(GRID.Row, 1) == string.Empty)
		//				{
		//				}
		//				else if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				{
		//					goto KeepGoingUP;
		//				}
		//			}
		//			else
		//			{
		//				GRID.Row = intPlaceHolder;
		//			}
		//		}
		//		GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, GRID.Rows - 1, null);
		//		GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, GRID.Row, 0, ImageList1.Images["Left"]);
		//		//e.KeyCode = 0;
		//		return;
		//	}
		//	for (intCounter = 1; intCounter <= GRID.Rows - 1; intCounter++)
		//	{
		//		if (Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1) == FCConvert.ToString(Convert.ToChar(e.KeyCode)))
		//		{
		//			if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intCounter, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				return;
		//			FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(intCounter), CallType.Method);
		//			//e.KeyCode = 0;
		//			break;
		//		}
		//	}
		//}

		//private void GRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.MouseRow, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION || GRID.TextMatrix(GRID.MouseRow, 1) == string.Empty)
		//		return;
		//	FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(LabelNumber[Convert.ToByte(Strings.UCase(Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, (GRID.Row), 1)), 1))[0])]), CallType.Method);
		//}

		private void MDIParent_Activated(object sender, System.EventArgs e)
		{
			//modArchiveImage.ShowArchiveImage(ref modGlobalConstants.Statics.gstrArchiveYear);
			//AutoSizeMenu_6(GRID, 20);
		}
		//private void MDIParent_Load(object sender, System.EventArgs e)
		//{
		//    //Begin Unmaped Properties
		//    //MDIParent.LinkTopic	= "MDIForm1";
		//    //End Unmaped Properties
		//    int counter;
		//    // gstrFormName = Me.Name
		//    // On Error GoTo CallErrorRoutine
		//    // gstrCurrentRoutine = "MDIForm_Load"
		//    // Dim Mouse As clsMousePointer
		//    // Set Mouse = New clsMousePointer
		//    // GoTo ResumeCode
		//    // CallErrorRoutine:
		//    // Call SetErrorHandler
		//    // Exit Sub
		//    // ResumeCode:
		//    // MsgBox "Loading TRIO Colors"
		//    modGlobalFunctions.LoadTRIOColors();
		//    // MsgBox "Format Menu"
		//    this.GRID.ColWidth(0, 200);
		//    this.GRID.ColWidth(1, 2400);
		//    GRID.Width = 2700;
		//    GRID.Select(0, 1);
		//    string strReturn = "";
		//    if (strReturn == string.Empty) strReturn = "False";
		//    modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
		//    mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
		//    SetMenuOptions("Main");
		//    StatusBar1.Panels[0].Text = modGlobalConstants.MuniName;
		//    modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "Maximize Forms", ref strReturn);
		//    modGlobalFunctions.SetTRIOColors(this);
		//    // For counter = 0 To GRID.Rows - 1
		//    // GRID.RowHeight(counter) = 400
		//    // Next
		//    //modGlobalFunctions.SetHelpMenuOptions();
		//    //modGlobalFunctions.SetMenuBackColor();
		//    GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, GRID.Cols - 1, GRID.BackColor);
		//    AutoSizeMenu_6(GRID, 20);
		//    modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(modRegistry.GetRegistryKey_3("Maximize Forms", FCConvert.ToString(false)));
		//    if (modGlobalConstants.Statics.boolMaxForms)
		//    {
		//        mnuOMaxForms.Checked = 0 != (Wisej.Web.CheckState.Checked);
		//    }
		//    else
		//    {
		//        mnuOMaxForms.Checked = 0 != (Wisej.Web.CheckState.Unchecked);
		//    }
		//}
		private void MDIForm_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				modBlockEntry.WriteYY();
                //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal);
                App.MainForm.OpenModule("TWGNENTY");
				/*? On Error Resume Next  *//*? On Error GoTo 0 *///FC:FINAL:DSE #925 Cannot change a collection inside a foreach statement
				//foreach (Form f in Application.OpenForms)
				//{
				//    f.Close();
				//}
				FCUtils.CloseFormsOnProject(false);
			}
		}

		private void MDIForm_Unload(object sender, FCFormClosingEventArgs e)
		{
			Application.Exit();
		}

		private void mnuBudgetaryHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWBD0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuCashReceiptsHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWCR0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuCentralParties_Click(object sender, System.EventArgs e)
		{
			frmCentralPartySearch.InstancePtr.Show(App.MainForm);
		}

		private void mnuClerkHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWCK0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuCodeEnforcementHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWCE0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuEnhanced911Help_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWE90000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuFExit_Click(object sender, System.EventArgs e)
		{
			modBlockEntry.WriteYY();
            //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
            //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
            App.MainForm.OpenModule("TWGNENTY");
            /*? On Error Resume Next  *//*? On Error GoTo 0 */
            foreach (Form f in FCGlobal.Statics.Forms)
			{
				f.Close();
			}
		}

		private void mnuFileFormsPrintForms_Click(object sender, System.EventArgs e)
		{
			frmScreenPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
			//this.ZOrder(ZOrderConstants.SendToBack);
		}

		private void mnuGeneralEntryHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWGN0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuHAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout.InstancePtr.Show();
		}

		private void mnuMotorVehicleRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWMV0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		//private void mnuOMaxForms_Click(object sender, System.EventArgs e)
		//{
		//	// On Error GoTo CallErrorRoutine
		//	// gstrCurrentRoutine = "mnuOMaxForms_Click"
		//	// Dim Mouse As clsMousePointer
		//	// Set Mouse = New clsMousePointer
		//	// GoTo ResumeCode
		//	// CallErrorRoutine:
		//	// Call SetErrorHandler
		//	// Exit Sub
		//	// ResumeCode:
		//	//modAPIsConst.RegCreateKeyWrp(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, 0);
		//	if (mnuOMaxForms.Checked)
		//	{
		//		modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "False");
		//	}
		//	else
		//	{
		//		modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "True");
		//	}
		//	modGlobalConstants.Statics.boolMaxForms = !modGlobalConstants.Statics.boolMaxForms;
		//	mnuOMaxForms.Checked = !mnuOMaxForms.Checked;
		//}

		//private void GRID_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	// On Error GoTo CallErrorRoutine
		//	// gstrCurrentRoutine = "grid_MouseMove"
		//	// Dim Mouse As clsMousePointer
		//	// Set Mouse = New clsMousePointer
		//	// GoTo ResumeCode
		//	// CallErrorRoutine:
		//	// Call SetErrorHandler
		//	// Exit Sub
		//	// ResumeCode:
		//	double i;
		//	// avoid extra work
		//	if (GRID.MouseRow == Statics.R)
		//		return;
		//	Statics.R = GRID.MouseRow;
		//	// move selection (even with no click)
		//	GRID.Select(Statics.R, 1);
		//	// remove cursor image
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, GRID.Rows - 1, null);
		//	// show cursor image if there is some text in column 5
		//	if (Statics.R == 0)
		//		return;
		//	if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Statics.R, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION || GRID.TextMatrix(Statics.R, 1) == string.Empty)
		//	{
		//		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
		//		return;
		//	}
		//	else
		//	{
		//		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
		//	}
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, Statics.R, 0, ImageList1.Images["Left"]);
		//}

		public void GetMainMenu()
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "GetCollectionsMenu"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			string strTemp = "";
			bool boolDisable = false;
			//ClearLabels();
			string menu = "Main";
			string imageKey = "";
			App.MainForm.Caption = "ACCOUNTS RECEIVABLE";
            App.MainForm.NavigationMenu.OriginName = "Accounts Receivable";
			App.MainForm.ApplicationIcon = "icon-accounts-receivable";
			string assemblyName = this.GetType().Assembly.GetName().Name;
			if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
			{
				App.MainForm.ApplicationIcons.Add(assemblyName, "icon-accounts-receivable");
			}
			// set the caption for the current menu labels
			for (int CurRow = 1; CurRow <= 6; CurRow++)
			{
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Customer / Bill Update";
							imageKey = "bills";
							lngFCode = modGlobal.ARSECCUSTOMERBILLUPDATE;
							break;
						}
					case 2:
						{
							strTemp = "Billing Process";
							imageKey = "billing-process";
							lngFCode = modGlobal.ARSECBILLINGPROCESS;
							break;
						}
					case 3:
						{
							strTemp = "Collections";
							imageKey = "collections";
							lngFCode = modGlobal.ARSECCOLLECTIONPROCESS;
							break;
						}
					case 4:
						{
							strTemp = "Printing";
							imageKey = "printing";
							lngFCode = modGlobal.ARSECPRINTING;
							break;
						}
					case 5:
						{
							strTemp = "Type Setup";
							imageKey = "setup-projects";
							lngFCode = modGlobal.ARSECTYPESETUP;
							break;
						}
					case 6:
						{
							strTemp = "File Maintenance";
							imageKey = "file-maintenance";
							lngFCode = modGlobal.ARSECFILEMAINTENANCE;
							break;
						}
				//case 7:
				//    {
				//        strTemp = "X. Exit Accounts Receivable";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0 && lngFCode != 999999)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				else if (lngFCode == 999999)
				{
					boolDisable = true;
				}
				FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 1, imageKey);
				switch (CurRow)
				{
					case 2:
						{
							GetBillingMenu(newItem);
							break;
						}
					case 3:
						{
							GetCollectionsMenu(newItem);
							break;
						}
					case 4:
						{
							GetPrintMenu(newItem);
							break;
						}
					case 6:
						{
							GetFileMenu(newItem);
							break;
						}
				}
			}
			// set the int array so that the alpha characters can be mapped to numbers
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("M")] = 6;
			LabelNumber[Strings.Asc("X")] = 7;
			//AutoSizeMenu_6(GRID, 20);
			App.MainForm.Text = "TRIO Software - Accounts Receivable [Main]";
		}

		public void GetBillingMenu(FCMenuItem parent)
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "GetCollectionsMenu"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			string strTemp = "";
			//ClearLabels();
			bool boolDisable = false;
			string menu = "Bill";
			// set the caption for the current menu labels
			for (int CurRow = 1; CurRow <= 6; CurRow++)
			{
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Bill Prep";
							lngFCode = 0;
							break;
						}
					case 2:
						{
							strTemp = "Create Bill Records";
							lngFCode = 0;
							break;
						}
					case 3:
						{
							strTemp = "Print Bills";
							lngFCode = 0;
							break;
						}
					case 4:
						{
							strTemp = "Manual Bills";
							lngFCode = 0;
							break;
						}
					case 5:
						{
							strTemp = "Void Bills";
							lngFCode = 0;
							break;
						}
					case 6:
						{
							strTemp = "Create Demand Bill Journal";
							lngFCode = 0;
							break;
						}
				//case 7:
				//    {
				//        strTemp = "X. Return to Main";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0 && lngFCode != 999999)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				else if (lngFCode == 999999)
				{
					boolDisable = true;
				}
				if (CurRow == 6)
				{
					if (!modGlobalConstants.Statics.gboolBD)
					{
						boolDisable = true;
					}
				}
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
			}
			// set the int array so that the alpha characters can be mapped to numbers
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("X")] = 7;
			//AutoSizeMenu_6(GRID, 20);
			App.MainForm.Text = "TRIO Software - Accounts Receivable [Billing]";
		}

		public void GetCollectionsMenu(FCMenuItem parent)
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "GetCollectionsMenu"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			string strTemp = "";
			//ClearLabels();
			bool boolDisable = false;
			bool dontShow = false;
			string menu = "Collections";
			// set the caption for the current menu labels
			for (int CurRow = 1; CurRow <= 5; CurRow++)
            {
                dontShow = false;
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Status";
							lngFCode = modGlobal.ARSECVIEWSTATUS;
							break;
						}
					case 2:
						{
							strTemp = "Payments & Adjustments";
							lngFCode = modGlobal.ARSECPAYMENTSADJUSTMENTS;
							break;
						}
					case 3:
						{
							strTemp = "Daily Audit Report";
							lngFCode = modGlobal.ARSECDAILYAUDITREPORT;
							break;
						}
					case 4:
						{
							strTemp = "Load of Back Information";
							lngFCode = modGlobal.ARSECLOADBACK;
							break;
						}
					case 5:
						{
							strTemp = "Charge Interest";
							lngFCode = modGlobal.ARSECCHARGEINTEREST;
							break;
						}
				//case 6:
				//    {
				//        strTemp = "Return to Main";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0 && lngFCode != 999999)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				else if (lngFCode == 999999)
				{
					boolDisable = true;
				}
				if (modGlobalConstants.Statics.gboolCR && CurRow == 2)
				{
					boolDisable = true;
					dontShow = true;
					// Else
					// DisableMenuOption True, 1
				}

				if (!dontShow)
				{
					parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
				}
			}
			// set the int array so that the alpha characters can be mapped to numbers
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("X")] = 6;
			//AutoSizeMenu_6(GRID, 20);
			App.MainForm.Text = "TRIO Software - Accounts Receivable [Collections]";
		}

		public void GetFileMenu(FCMenuItem parent)
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "GetCollectionsMenu"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			string strTemp = "";
			bool boolDisable = false;
			//ClearLabels();
			string menu = "File";
			// set the caption for the current menu labels
			for (int CurRow = 1; CurRow <= 6; CurRow++)
			{
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Customize";
							lngFCode = modGlobal.ARSECCUSTOMIZE;
							break;
						}
					case 2:
						{
							strTemp = "Check Database Structure";
							lngFCode = 0;
							break;
						}
					case 3:
						{
							strTemp = "Edit Custom Bill Types";
							lngFCode = modGlobal.ARSECEDITCUSTOMBILLTYPES;
							break;
						}
					case 4:
						{
							strTemp = "Rate Record Update";
							lngFCode = 0;
							break;
						}
					case 5:
						{
							strTemp = "Edit Bill Information";
							lngFCode = 0;
							break;
						}
					case 6:
						{
							strTemp = "Payees";
							lngFCode = 0;
							break;
						}
				//case 7:
				//    {
				//        strTemp = "Return to Main";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0 && lngFCode != 999999)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				else if (lngFCode == 999999)
				{
					boolDisable = true;
				}
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
			}
			// set the int array so that the alpha characters can be mapped to numbers
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("X")] = 7;
			//AutoSizeMenu_6(GRID, 20);
			App.MainForm.Text = "TRIO Software - Accounts Receivable [File Maintenance]";
		}

		public void GetPrintMenu(FCMenuItem parent)
		{
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "GetCollectionsMenu"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Sub
			// ResumeCode:
			string strTemp = "";
			//ClearLabels();
			bool boolDisable = false;
			string menu = "Print";
			// set the caption for the current menu labels
			for (int CurRow = 1; CurRow <= 8; CurRow++)
			{
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Customer Lists";
							lngFCode = 0;
							break;
						}
					case 2:
						{
							strTemp = "Status Lists";
							lngFCode = 0;
							break;
						}
					case 3:
						{
							strTemp = "Customer Labels";
							lngFCode = 0;
							break;
						}
					case 4:
						{
							strTemp = "Type List";
							lngFCode = 0;
							break;
						}
					case 5:
						{
							strTemp = "Bill List";
							lngFCode = 0;
							break;
						}
					case 6:
						{
							strTemp = "Redisplay Daily Audit";
							lngFCode = 0;
							break;
						}
					case 7:
						{
							strTemp = "Created Bills Report";
							lngFCode = 0;
							break;
						}
					case 8:
						{
							strTemp = "AR Statements";
							lngFCode = 0;
							break;
						}
				//case 9:
				//    {
				//        strTemp = "Return to Main";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0 && lngFCode != 999999)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				else if (lngFCode == 999999)
				{
					boolDisable = true;
				}
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
			}
			// set the int array so that the alpha characters can be mapped to numbers
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("7")] = 7;
			LabelNumber[Strings.Asc("8")] = 8;
			LabelNumber[Strings.Asc("X")] = 9;
			//AutoSizeMenu_6(GRID, 20);
			App.MainForm.Text = "TRIO Software - Accounts Receivable [Printing]";
		}
		// vbPorter upgrade warning: GRID As vsFlexGrid	OnWrite(VSFlex6DAOCtl.vsFlexGrid)
		//public void AutoSizeMenu_6(FCGrid GRID, short intLabels) { AutoSizeMenu(ref GRID, ref intLabels); }
		//public void AutoSizeMenu(ref FCGrid GRID, ref short intLabels)
		//{
		//    int i;
		//    // vbPorter upgrade warning: FullRowHeight As int	OnWriteFCConvert.ToDouble(
		//    int FullRowHeight = 0; // this will store the height of the labels with text
		//    // .RowHeight(0) = 0                   'the first row in the grid is height = 0
		//    FullRowHeight = FCConvert.ToInt32(GRID.Height / intLabels); // find the average height of the row
		//    for (i = 1; i <= 20; i++)
		//    { // this just assigns the row height
		//        GRID.RowHeight(i, FullRowHeight);
		//    }
		//    GRID.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//    GRID.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//}
		private void DisableMenuOption_2(bool boolState, int intRow)
		{
			DisableMenuOption(ref boolState, ref intRow);
		}

		private void DisableMenuOption_8(bool boolState, int intRow)
		{
			DisableMenuOption(ref boolState, ref intRow);
		}

		private void DisableMenuOption(ref bool boolState, ref int intRow)
		{
			boolState = !boolState;
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1, modGlobalFunctions.SetMenuOptionColor(ref boolState));
		}

		private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWPY0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWPP0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWRE0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuRedBookHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWRB0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWTB0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWTC0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWUT0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWVR0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		public class StaticVariables
		{
			//}
			//    FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(LabelNumber[Convert.ToByte(Strings.UCase(Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, (GRID.Row), 1)), 1))[0])]), CallType.Method);
			//    if (GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1) == MDIParent.InstancePtr.BackColor || GRID.TextMatrix(GRID.Row, 1) == string.Empty) return;
			//    if (keyAscii != 13) return;
			//    int keyAscii = Strings.Asc(e.KeyChar);
			//{
			//private void GRID_KeyPressEvent(object sender, KeyPressEventArgs e)
			public int R = 0, c;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
