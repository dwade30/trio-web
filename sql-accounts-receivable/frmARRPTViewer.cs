﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmARRPTViewer.
	/// </summary>
	public partial class frmARRPTViewer : BaseForm
	{
		public frmARRPTViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmARRPTViewer InstancePtr
		{
			get
			{
				return (frmARRPTViewer)Sys.GetInstance(typeof(frmARRPTViewer));
			}
		}

		protected frmARRPTViewer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/22/2005              *
		// ********************************************************
		string strRPTName;
		int intType;
		string strEmailAttachmentName = "";

		public void Init(string strReportName, string strFormCaption, short intPassType)
		{
			strRPTName = strReportName;
            //FC:FINAL:RPU:#i987 - Call the load as in original if the form is not loaded 
            if (!this.IsLoaded)
            {
                this.LoadForm();
            }
            this.Text = strFormCaption;
			this.HeaderText.Text = strFormCaption;
			//this.Show(App.MainForm);
			//arView.ReportSource.Top = 0;
			//arView.ReportSource.Width = this.Width-100;
			//arView.ReportSource.Left = 0;
			//arView.ReportSource.Height = this.Height-400;
			//arView.Zoom = -1;
			LoadRecreateCombo();
			//xxLoadCombo();
			intType = intPassType;
			switch (intType)
			{
				case 1:
				case 2:
					{
						// this is the audits
						// ShowReport
						mnuFileChange_Click();
						break;
					}
				case 10:
					{
						// this is the purge list
						//xx ShowReport();
						break;
					}
				case 11:
					{
						// bankruptcy report
						//cmdFileRecreate.Visible = false;
						break;
					}
				case 12:
					{
						// Reprint of CMF Report
						//cmdFileRecreate.Visible = false;
						break;
					}
				case 13:
					{
						// reprint of fee lists
						//cmdFileRecreate.Visible = false;
						break;
					}
			}

            this.Show(App.MainForm);
            //end switch
            // If arView.Visible Then
            // arView.SetFocus
            // End If
        }

		private void frmARRPTViewer_Activated(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Maximized && this.WindowState != FormWindowState.Minimized)
			{
				this.Height = this.Height - 15;
			}
			//arView.ReportSource.Top = 0;
			//arView.ReportSource.Width = this.Width-100;
			//arView.ReportSource.Left = 0;
			//arView.ReportSource.Height = this.Height-400;
			//arView.Zoom = -1;
			LoadRecreateCombo();
		}

		private void frmARRPTViewer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmARRPTViewer.FillStyle	= 0;
			//frmARRPTViewer.ScaleWidth	= 9045;
			//frmARRPTViewer.ScaleHeight	= 7410;
			//frmARRPTViewer.LinkTopic	= "Form2";
			//frmARRPTViewer.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Redisplay Daily Audit Report";
			this.WindowState = FormWindowState.Normal;
		}

		private void frmARRPTViewer_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches escape keys
			if (KeyAscii == Keys.Escape)
			{
				//mnuFileExport.Enabled = false;
				//mnuFileEmail.Enabled = false;
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmARRPTViewer_Resize(object sender, System.EventArgs e)
		{
			//arView.ReportSource.Top = 0;
			//arView.ReportSource.Width = this.Width-100;
			//arView.ReportSource.Left = 0;
			//arView.ReportSource.Height = this.Height-400;
			// MAL@20070907: Added option so control does not drop to the bottom of the screen
			if (fraRecreateReport.Visible)
			{
				//FC:FINAL:RPU:#i429: Set this directly in designer
				//fraRecreateReport.Top = FCConvert.ToInt32((arView.Height / 6.0));
			}
			if (fraNumber.Visible)
			{
				//FC FINAL:RPU:#i429: Set this directly in designer
				//fraNumber.Top = FCConvert.ToInt32((arView.Height / 6.0));
			}
		}

		private void mnuFileChange_Click()
		{
            // this will let the user switch between the numbered reports
            TopWrapper.Visible = true;
            arView.Visible = false;
            arView.Parent = null;
			fraNumber.Visible = false;
            fraRecreateReport.Visible = true;
			cmdSave.Visible = true;
			//FC:FINAL:RPU:#i429Set this directly in designer
			//fraNumber.Left = FCConvert.ToInt32((this.Width - fraNumber.Width) / 2.0);
			//fraNumber.Top = FCConvert.ToInt32((this.Height - fraNumber.Height) / 3.0);
		}

		private void mnuFileRecreate_Click(object sender, System.EventArgs e)
		{
			// this will recreate the audit report
			lblRecreateInstructions.Text = "Select the report to recreate.";
			// show the recreate frame
			fraRecreateReport.Visible = true;
			// hide the rest of the frames
			fraNumber.Visible = false;
            TopWrapper.Visible = true;
            arView.Visible = false;
            arView.Parent = null;
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (cmbRecreate.SelectedIndex != -1)
			{
				modGlobalConstants.Statics.gblnRecreate = true;
				// load the report
				ReCreateReport_2(FCConvert.ToInt32(Conversion.Val(Strings.Left(cmbRecreate.Items[cmbRecreate.SelectedIndex].ToString(), 5))));
			}
		}

		// vbPorter upgrade warning: lngCloseOut As int	OnWriteFCConvert.ToDouble(
		private void ReCreateReport_2(int lngCloseOut)
		{
			ReCreateReport(ref lngCloseOut);
		}

		private void ReCreateReport(ref int lngCloseOut)
		{
			modGlobal.Statics.glngARCurrentCloseOut = lngCloseOut;
			rptARDailyAuditMaster.InstancePtr.StartUp(true, false);
			frmReportViewer.InstancePtr.Init(rptARDailyAuditMaster.InstancePtr);
			frmReportViewer.InstancePtr.Focus();
		}

		private void LoadRecreateCombo()
		{
			// this will fill the recreation combo with all of the closeouts done in the CloseOut table
			clsDRWrapper rsCloseOut = new clsDRWrapper();
			string listItem;
			cmbRecreate.Clear();
			rsCloseOut.OpenRecordset("SELECT * FROM CloseOut ORDER BY CloseOutDate desc", modCLCalculations.strARDatabase);
			while (!rsCloseOut.EndOfFile())
			{
				listItem = modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsCloseOut.Get_Fields_Int32("ID")), 5);
				if (rsCloseOut.Get_Fields_DateTime("CloseOutDate") > DateAndTime.DateValue("12/30/1899"))
				{
					listItem += " - " + rsCloseOut.Get_Fields_DateTime("CloseOutDate");
				}
				else
				{
					listItem += " - 00/00/0000";
				}

				cmbRecreate.AddItem(listItem);
				cmbRecreate.ItemData(cmbRecreate.NewIndex, FCConvert.ToInt32(rsCloseOut.Get_Fields_Int32("ID")));
				rsCloseOut.MoveNext();
			}
		}
	}
}
