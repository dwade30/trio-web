﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomerLabels.
	/// </summary>
	partial class frmCustomerLabels : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCComboBox cmbComplete;
		public fecherFoundation.FCLabel lblComplete;
		public fecherFoundation.FCComboBox cmbNumber;
		public fecherFoundation.FCLabel lblNumber;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboLabelType;
		public fecherFoundation.FCLabel lblLabelDescription;
		public fecherFoundation.FCFrame fraClassCodes;
		public fecherFoundation.FCFrame fraSpecificBillTypes;
		public fecherFoundation.FCGrid vsBillTypes;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCComboBox cboEnd;
		public fecherFoundation.FCComboBox cboStart;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraWarrant;
		public fecherFoundation.FCComboBox cboWarrant;
		public fecherFoundation.FCFrame fraJournal;
		public fecherFoundation.FCComboBox cboJournal;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.cmbComplete = new fecherFoundation.FCComboBox();
			this.lblComplete = new fecherFoundation.FCLabel();
			this.cmbNumber = new fecherFoundation.FCComboBox();
			this.lblNumber = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboLabelType = new fecherFoundation.FCComboBox();
			this.lblLabelDescription = new fecherFoundation.FCLabel();
			this.fraClassCodes = new fecherFoundation.FCFrame();
			this.fraSpecificBillTypes = new fecherFoundation.FCFrame();
			this.vsBillTypes = new fecherFoundation.FCGrid();
			this.fraRange = new fecherFoundation.FCFrame();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.cboEnd = new fecherFoundation.FCComboBox();
			this.cboStart = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.fraWarrant = new fecherFoundation.FCFrame();
			this.cboWarrant = new fecherFoundation.FCComboBox();
			this.fraJournal = new fecherFoundation.FCFrame();
			this.cboJournal = new fecherFoundation.FCComboBox();
			this.btnProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraClassCodes)).BeginInit();
			this.fraClassCodes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificBillTypes)).BeginInit();
			this.fraSpecificBillTypes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBillTypes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWarrant)).BeginInit();
			this.fraWarrant.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraJournal)).BeginInit();
			this.fraJournal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 359);
			this.BottomPanel.Size = new System.Drawing.Size(904, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.fraClassCodes);
			this.ClientArea.Controls.Add(this.fraRange);
			this.ClientArea.Controls.Add(this.cmbComplete);
			this.ClientArea.Controls.Add(this.lblComplete);
			this.ClientArea.Controls.Add(this.cmbNumber);
			this.ClientArea.Controls.Add(this.lblNumber);
			this.ClientArea.Controls.Add(this.fraJournal);
			this.ClientArea.Size = new System.Drawing.Size(904, 299);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(904, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(198, 30);
			this.HeaderText.Text = "Customer Labels";
			// 
			// cmbAll
			// 
			this.cmbAll.AutoSize = false;
			this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAll.FormattingEnabled = true;
			this.cmbAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbAll.Location = new System.Drawing.Point(20, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(427, 40);
			this.cmbAll.TabIndex = 18;
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.optAll_CheckedChanged);
			// 
			// cmbComplete
			// 
			this.cmbComplete.AutoSize = false;
			this.cmbComplete.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbComplete.FormattingEnabled = true;
			this.cmbComplete.Items.AddRange(new object[] {
				"Complete List",
				"Range of Customers"
			});
			this.cmbComplete.Location = new System.Drawing.Point(165, 200);
			this.cmbComplete.Name = "cmbComplete";
			this.cmbComplete.Size = new System.Drawing.Size(195, 40);
			this.cmbComplete.TabIndex = 20;
			this.cmbComplete.SelectedIndexChanged += new System.EventHandler(this.optComplete_CheckedChanged);
			// 
			// lblComplete
			// 
			this.lblComplete.Location = new System.Drawing.Point(30, 214);
			this.lblComplete.Name = "lblComplete";
			this.lblComplete.Size = new System.Drawing.Size(95, 15);
			this.lblComplete.TabIndex = 21;
			this.lblComplete.Text = "LABEL OPTIONS";
			// 
			// cmbNumber
			// 
			this.cmbNumber.AutoSize = false;
			this.cmbNumber.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbNumber.FormattingEnabled = true;
			this.cmbNumber.Items.AddRange(new object[] {
				"Name",
				"Number"
			});
			this.cmbNumber.Location = new System.Drawing.Point(165, 260);
			this.cmbNumber.Name = "cmbNumber";
			this.cmbNumber.Size = new System.Drawing.Size(195, 40);
			this.cmbNumber.TabIndex = 22;
			this.cmbNumber.SelectedIndexChanged += new System.EventHandler(this.optNumber_CheckedChanged);
			// 
			// lblNumber
			// 
			this.lblNumber.Location = new System.Drawing.Point(30, 274);
			this.lblNumber.Name = "lblNumber";
			this.lblNumber.Size = new System.Drawing.Size(70, 15);
			this.lblNumber.TabIndex = 23;
			this.lblNumber.Text = "ORDER BY";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboLabelType);
			this.Frame2.Controls.Add(this.lblLabelDescription);
			this.Frame2.Location = new System.Drawing.Point(30, 30);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(350, 150);
			this.Frame2.TabIndex = 19;
			this.Frame2.Text = "Select Label Type";
			// 
			// cboLabelType
			// 
			this.cboLabelType.AutoSize = false;
			this.cboLabelType.BackColor = System.Drawing.SystemColors.Window;
			this.cboLabelType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboLabelType.FormattingEnabled = true;
			this.cboLabelType.Location = new System.Drawing.Point(20, 30);
			this.cboLabelType.Name = "cboLabelType";
			this.cboLabelType.Size = new System.Drawing.Size(310, 40);
			this.cboLabelType.TabIndex = 20;
			this.cboLabelType.SelectedIndexChanged += new System.EventHandler(this.cboLabelType_SelectedIndexChanged);
			// 
			// lblLabelDescription
			// 
			this.lblLabelDescription.Location = new System.Drawing.Point(20, 90);
			this.lblLabelDescription.Name = "lblLabelDescription";
			this.lblLabelDescription.Size = new System.Drawing.Size(310, 50);
			this.lblLabelDescription.TabIndex = 21;
			// 
			// fraClassCodes
			// 
			this.fraClassCodes.Controls.Add(this.fraSpecificBillTypes);
			this.fraClassCodes.Controls.Add(this.cmbAll);
			this.fraClassCodes.Location = new System.Drawing.Point(410, 30);
			this.fraClassCodes.Name = "fraClassCodes";
			this.fraClassCodes.Size = new System.Drawing.Size(467, 300);
			this.fraClassCodes.TabIndex = 14;
			this.fraClassCodes.Text = "Bill Types To Print";
			// 
			// fraSpecificBillTypes
			// 
			this.fraSpecificBillTypes.AppearanceKey = "groupBoxNoBorders";
			this.fraSpecificBillTypes.Controls.Add(this.vsBillTypes);
			this.fraSpecificBillTypes.Enabled = false;
			this.fraSpecificBillTypes.Location = new System.Drawing.Point(0, 70);
			this.fraSpecificBillTypes.Name = "fraSpecificBillTypes";
			this.fraSpecificBillTypes.Size = new System.Drawing.Size(467, 230);
			this.fraSpecificBillTypes.TabIndex = 17;
			// 
			// vsBillTypes
			// 
			this.vsBillTypes.AllowSelection = false;
			this.vsBillTypes.AllowUserToResizeColumns = false;
			this.vsBillTypes.AllowUserToResizeRows = false;
			this.vsBillTypes.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBillTypes.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBillTypes.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBillTypes.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBillTypes.BackColorSel = System.Drawing.Color.Empty;
			this.vsBillTypes.Cols = 3;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBillTypes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsBillTypes.ColumnHeadersHeight = 30;
			this.vsBillTypes.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBillTypes.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsBillTypes.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBillTypes.FixedCols = 0;
			this.vsBillTypes.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBillTypes.FrozenCols = 0;
			this.vsBillTypes.GridColor = System.Drawing.Color.Empty;
			this.vsBillTypes.Location = new System.Drawing.Point(20, 20);
			this.vsBillTypes.Name = "vsBillTypes";
			this.vsBillTypes.ReadOnly = true;
			this.vsBillTypes.RowHeadersVisible = false;
			this.vsBillTypes.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBillTypes.RowHeightMin = 0;
			this.vsBillTypes.Rows = 1;
			this.vsBillTypes.ShowColumnVisibilityMenu = false;
			this.vsBillTypes.Size = new System.Drawing.Size(427, 190);
			this.vsBillTypes.StandardTab = true;
			this.vsBillTypes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBillTypes.TabIndex = 18;
			this.vsBillTypes.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBillTypes_KeyDownEvent);
			this.vsBillTypes.Click += new System.EventHandler(this.vsBillTypes_ClickEvent);
			// 
			// fraRange
			// 
			this.fraRange.Controls.Add(this.fcLabel1);
			this.fraRange.Controls.Add(this.cboEnd);
			this.fraRange.Controls.Add(this.cboStart);
			this.fraRange.Controls.Add(this.Label1);
			this.fraRange.Location = new System.Drawing.Point(30, 340);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(600, 150);
			this.fraRange.TabIndex = 10;
			this.fraRange.Text = "Select Range";
			this.fraRange.Visible = false;
			// 
			// fcLabel1
			// 
			this.fcLabel1.Location = new System.Drawing.Point(20, 44);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(35, 15);
			this.fcLabel1.TabIndex = 14;
			this.fcLabel1.Text = "FROM";
			this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cboEnd
			// 
			this.cboEnd.AutoSize = false;
			this.cboEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cboEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEnd.FormattingEnabled = true;
			this.cboEnd.Location = new System.Drawing.Point(95, 90);
			this.cboEnd.Name = "cboEnd";
			this.cboEnd.Size = new System.Drawing.Size(485, 40);
			this.cboEnd.TabIndex = 12;
			// 
			// cboStart
			// 
			this.cboStart.AutoSize = false;
			this.cboStart.BackColor = System.Drawing.SystemColors.Window;
			this.cboStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStart.FormattingEnabled = true;
			this.cboStart.Location = new System.Drawing.Point(95, 30);
			this.cboStart.Name = "cboStart";
			this.cboStart.Size = new System.Drawing.Size(485, 40);
			this.cboStart.TabIndex = 11;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 104);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(21, 15);
			this.Label1.TabIndex = 13;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraWarrant
			// 
			this.fraWarrant.Controls.Add(this.cboWarrant);
			this.fraWarrant.Location = new System.Drawing.Point(0, 0);
			this.fraWarrant.Name = "fraWarrant";
			this.fraWarrant.Size = new System.Drawing.Size(108, 90);
			this.fraWarrant.TabIndex = 2;
			this.fraWarrant.Text = "Select Warrant";
			this.fraWarrant.Visible = false;
			// 
			// cboWarrant
			// 
			this.cboWarrant.AutoSize = false;
			this.cboWarrant.BackColor = System.Drawing.SystemColors.Window;
			this.cboWarrant.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboWarrant.FormattingEnabled = true;
			this.cboWarrant.Location = new System.Drawing.Point(20, 30);
			this.cboWarrant.Name = "cboWarrant";
			this.cboWarrant.Size = new System.Drawing.Size(68, 40);
			this.cboWarrant.TabIndex = 3;
			this.cboWarrant.Text = "Combo1";
			// 
			// fraJournal
			// 
			this.fraJournal.Controls.Add(this.cboJournal);
			this.fraJournal.Controls.Add(this.fraWarrant);
			this.fraJournal.Location = new System.Drawing.Point(635, 340);
			this.fraJournal.Name = "fraJournal";
			this.fraJournal.Size = new System.Drawing.Size(108, 90);
			this.fraJournal.TabIndex = 0;
			this.fraJournal.Text = "Select Journal";
			this.fraJournal.Visible = false;
			// 
			// cboJournal
			// 
			this.cboJournal.AutoSize = false;
			this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournal.FormattingEnabled = true;
			this.cboJournal.Location = new System.Drawing.Point(20, 30);
			this.cboJournal.Name = "cboJournal";
			this.cboJournal.Size = new System.Drawing.Size(68, 40);
			this.cboJournal.TabIndex = 1;
			this.cboJournal.Text = "Combo1";
			// 
			// btnProcessSave
			// 
			this.btnProcessSave.AppearanceKey = "acceptButton";
			this.btnProcessSave.Location = new System.Drawing.Point(291, 30);
			this.btnProcessSave.Name = "btnProcessSave";
			this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcessSave.Size = new System.Drawing.Size(117, 48);
			this.btnProcessSave.TabIndex = 0;
			this.btnProcessSave.Text = "Save & Exit";
			this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmCustomerLabels
			// 
			this.ClientSize = new System.Drawing.Size(904, 467);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCustomerLabels";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Customer Labels";
			this.Load += new System.EventHandler(this.frmCustomerLabels_Load);
			this.Activated += new System.EventHandler(this.frmCustomerLabels_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomerLabels_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraClassCodes)).EndInit();
			this.fraClassCodes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificBillTypes)).EndInit();
			this.fraSpecificBillTypes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsBillTypes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraWarrant)).EndInit();
			this.fraWarrant.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraJournal)).EndInit();
			this.fraJournal.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCLabel fcLabel1;
		private FCButton btnProcessSave;
	}
}
