﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmChargeInterest.
	/// </summary>
	partial class frmChargeInterest : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSelected;
		public fecherFoundation.FCFrame fraSelected;
		public fecherFoundation.FCComboBox cboBillType;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbSelected = new fecherFoundation.FCComboBox();
            this.fraSelected = new fecherFoundation.FCFrame();
            this.cboBillType = new fecherFoundation.FCComboBox();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcessSave = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelected)).BeginInit();
            this.fraSelected.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 328);
            this.BottomPanel.Size = new System.Drawing.Size(769, 96);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnProcessSave);
            this.ClientArea.Controls.Add(this.fraSelected);
            this.ClientArea.Controls.Add(this.cmbSelected);
            this.ClientArea.Size = new System.Drawing.Size(769, 268);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(769, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(603, 30);
            this.HeaderText.Text = "Select the type of bill(s) you wish to charge interest for";
            // 
            // cmbSelected
            // 
            this.cmbSelected.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbSelected.Location = new System.Drawing.Point(30, 30);
            this.cmbSelected.Name = "cmbSelected";
            this.cmbSelected.Size = new System.Drawing.Size(326, 40);
            this.cmbSelected.TabIndex = 1;
            this.cmbSelected.SelectedIndexChanged += new System.EventHandler(this.optSelected_CheckedChanged);
            // 
            // fraSelected
            // 
            this.fraSelected.AppearanceKey = "groupBoxNoBorders";
            this.fraSelected.Controls.Add(this.cboBillType);
            this.fraSelected.Enabled = false;
            this.fraSelected.Location = new System.Drawing.Point(0, 70);
            this.fraSelected.Name = "fraSelected";
            this.fraSelected.Size = new System.Drawing.Size(356, 60);
            this.fraSelected.TabIndex = 1;
            // 
            // cboBillType
            // 
            this.cboBillType.BackColor = System.Drawing.SystemColors.Window;
            this.cboBillType.Enabled = false;
            this.cboBillType.Location = new System.Drawing.Point(30, 20);
            this.cboBillType.Name = "cboBillType";
            this.cboBillType.Size = new System.Drawing.Size(326, 40);
            this.cboBillType.TabIndex = 2;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.AppearanceKey = "acceptButton";
            this.btnProcessSave.Location = new System.Drawing.Point(30, 167);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcessSave.Size = new System.Drawing.Size(100, 48);
            this.btnProcessSave.Text = "Process";
            this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmChargeInterest
            // 
            this.ClientSize = new System.Drawing.Size(769, 424);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmChargeInterest";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Charge Interest";
            this.Load += new System.EventHandler(this.frmChargeInterest_Load);
            this.Activated += new System.EventHandler(this.frmChargeInterest_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChargeInterest_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelected)).EndInit();
            this.fraSelected.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcessSave;
	}
}
