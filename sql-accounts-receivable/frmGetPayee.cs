﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmGetPayee.
	/// </summary>
	public partial class frmGetPayee : BaseForm
	{
		public frmGetPayee()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetPayee InstancePtr
		{
			get
			{
				return (frmGetPayee)Sys.GetInstance(typeof(frmGetPayee));
			}
		}

		protected frmGetPayee _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();
		bool EditFlag;
		bool SearchFlag;
		int[] recordNum = null;
		bool OKFlag;

		public void StartProgram(int Record)
		{
			int counter;
			string[] strFiles = null;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			string strTemp = "";
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Customer Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			modGlobal.Statics.lngCurrentPayee = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
			// initialize the current account number
			rs.OpenRecordset("SELECT * FROM Payees WHERE PayeeID = " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				//FC:FINAL:SBE - move show before, to execute the Load handler. In vb6 Load is executed when any control or property is accessed
				frmPayeeMaster.InstancePtr.Show(App.MainForm);
				// show the form
				frmPayeeMaster.InstancePtr.RecordNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("PayeeID"));
				frmPayeeMaster.InstancePtr.FirstRecordShown = FCConvert.ToInt32(rs.Get_Fields_Int32("PayeeID"));
				frmPayeeMaster.InstancePtr.blnEdit = true;
				frmPayeeMaster.InstancePtr.blnAlreadyAdded = false;
				frmPayeeMaster.InstancePtr.txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
				strTemp = FCConvert.ToString(rs.Get_Fields_String("LogoPath"));
				if (strTemp != string.Empty)
				{
					if (File.Exists(strTemp))
					{
						if (!frmPayeeMaster.InstancePtr.LoadLogoPicture(ref strTemp))
						{
							File.Delete(strTemp);
						}
					}
					else
					{
						frmPayeeMaster.InstancePtr.imgSignature.Image = null;
					}
				}
				else
				{
					frmPayeeMaster.InstancePtr.imgSignature.Image = null;
				}
				frmPayeeMaster.InstancePtr.txtRemittanceMessage.TextRTF = FCConvert.ToString(rs.Get_Fields_String("RemittanceMessage"));
				frmPayeeMaster.InstancePtr.lblVoterNumber.Text = FCConvert.ToString(rs.Get_Fields_Int32("PayeeID"));
				modGlobal.Statics.PayeeSearchResults.MoveFirst();
				frmPayeeMaster.InstancePtr.btnProcessPreviousEntry.Enabled = false;
				while (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) != Record)
				{
					//Application.DoEvents();
					modGlobal.Statics.PayeeSearchResults.MoveNext();
					frmPayeeMaster.InstancePtr.btnProcessPreviousEntry.Enabled = true;
				}
				if (modGlobal.Statics.PayeeSearchResults.RecordCount() > 1)
				{
					modGlobal.Statics.PayeeSearchResults.MoveNext();
					if (modGlobal.Statics.PayeeSearchResults.EndOfFile() != true)
					{
						frmPayeeMaster.InstancePtr.btnProcessNextEntry.Enabled = true;
					}
					else
					{
						frmPayeeMaster.InstancePtr.btnProcessNextEntry.Enabled = false;
					}
					modGlobal.Statics.PayeeSearchResults.MovePrevious();
				}
				else
				{
					frmPayeeMaster.InstancePtr.btnProcessNextEntry.Enabled = false;
				}
				frmPayeeMaster.InstancePtr.btnFileDelete.Enabled = true;
				//FC:FINAL:SBE - move show before, to execute the Load handler. In vb6 Load is executed when any control or property is accessed
				//frmPayeeMaster.InstancePtr.Show(App.MainForm); // show the form
				// frmWait.Refresh
				//Application.DoEvents();
				modRegistry.SaveRegistryKey("CURRPAY", FCConvert.ToString(modGlobal.Statics.lngCurrentPayee));
				frmWait.InstancePtr.Unload();
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				MessageBox.Show("No Payee Found", "Non-Existent Payee", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			Close();
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:PB - issue #2840 incorrect index
            //cmbDescription.SelectedIndex = 0;
            cmbDescription.SelectedIndex = -1;
            //optHidden.Checked = true; // clear the search info
            txtSearch.Text = "";
			vs1.Clear();
			vs1.TextMatrix(0, 0, "Payee ID#");
			vs1.TextMatrix(0, 1, "Description");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, 4);
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtGetAccountNumber.Text) != 0)
			{
				// if there is a valid account number
				modGlobal.Statics.lngCurrentPayee = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
				// set the account number to the one last used
				modGlobal.Statics.PayeeSearchResults.OpenRecordset("SELECT * FROM Payees WHERE PayeeID = " + FCConvert.ToString(modGlobal.Statics.lngCurrentPayee) + " ORDER BY Description");
				StartProgram(modGlobal.Statics.lngCurrentPayee);
				// call the procedure to retrieve the info
			}
			else
			{
				frmPayeeMaster.InstancePtr.blnEdit = false;
				frmPayeeMaster.InstancePtr.blnAlreadyAdded = false;
				frmPayeeMaster.InstancePtr.btnProcessNextEntry.Enabled = false;
				frmPayeeMaster.InstancePtr.btnProcessPreviousEntry.Enabled = false;
				frmPayeeMaster.InstancePtr.btnFileDelete.Enabled = false;
				frmPayeeMaster.InstancePtr.RecordNumber = 0;
				if (modGlobal.Statics.PayeeSearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					modGlobal.Statics.PayeeSearchResults.Reset();
				}
				//! Load frmWait; // shwo the wait form
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading New Entry";
				// frmWait.Refresh
				frmWait.InstancePtr.Show();
				frmPayeeMaster.InstancePtr.Show(App.MainForm);
				// show the blankform
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
				Close();
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(null, new System.EventArgs());
		}

		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			int intAccount;
			intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
			// get the account number of that record
			if (intAccount != 0)
			{
				// if there is a valid account number
				modGlobal.Statics.lngCurrentPayee = intAccount;
				// gets the Account Number for the item that is double clicked
				txtGetAccountNumber.Text = FCConvert.ToString(intAccount);
				StartProgram(recordNum[vs1.Row - 1]);
			}
			cmdClear_Click();
			// clear the search
			Frame3.Visible = false;
			// make the list of records invisible
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void cmdQuit_Click()
		{
			Close();
			// unload this form
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			vs1.Clear();
			Frame3.Visible = false;
			// make the listbox invisible
			cmdClear_Click();
			// clear the search
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string SearchType = "";
			string SearchCriteria = "";
			bool BadSearchFlag = false;
			string strStreetSearch = "";
			int lngStreetNumber;
			//! Load frmWait; // shwo the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
            //FC:FINAL:PB - issue #2836 incorrect index
            //if (cmbDescription.SelectedIndex != 1)
            if (cmbDescription.SelectedIndex != 0)
            {
				// make sure a search type is clicked
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("You must choose a Search Type (Description).", "Search Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				SearchType = "Description";
			}
			if (SearchType == "Description")
			{
				if (txtSearch.Text == "")
				{
					// make sure there is some criteria to search for
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					SearchCriteria = txtSearch.Text;
					// remember the search criteria
				}
			}
			if (SearchType == "Description")
			{
				modGlobal.Statics.PayeeSearchResults.OpenRecordset("SELECT * FROM Payees WHERE Description > '" + Strings.Trim(txtSearch.Text) + "    ' AND Description < '" + Strings.Trim(txtSearch.Text) + "zzzz' ORDER BY Description");
				rs.OpenRecordset("SELECT * FROM Payees WHERE Description > '" + Strings.Trim(txtSearch.Text) + "    ' AND Description < '" + Strings.Trim(txtSearch.Text) + "zzzz' ORDER BY Description");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			if (!BadSearchFlag)
			{
				// if there is a record
				rs.MoveLast();
				rs.MoveFirst();
				modGlobal.Statics.PayeeSearchResults.MoveLast();
				modGlobal.Statics.PayeeSearchResults.MoveFirst();
				frmWait.InstancePtr.Unload();
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					frmPayeeMaster.InstancePtr.btnProcessNextEntry.Enabled = false;
					frmPayeeMaster.InstancePtr.btnProcessPreviousEntry.Enabled = false;
					frmPayeeMaster.InstancePtr.btnFileDelete.Enabled = true;
					recordNum = new int[1 + 1];
					recordNum[0] = FCConvert.ToInt32(rs.Get_Fields_Int32("PayeeID"));
					cmdClear_Click();
					// clear the search
					modGlobal.Statics.lngCurrentPayee = FCConvert.ToInt32(rs.Get_Fields_Int32("PayeeID"));
					// save the account number
					txtGetAccountNumber.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentPayee);
					StartProgram(recordNum[0]);
					// call the procedure to retrieve the info
				}
				else
				{
					frmPayeeMaster.InstancePtr.btnProcessNextEntry.Enabled = true;
					frmPayeeMaster.InstancePtr.btnProcessPreviousEntry.Enabled = false;
					frmPayeeMaster.InstancePtr.btnFileDelete.Enabled = true;
					Fill_List(ref SearchType);
					// else show the user all possible records
					frmWait.InstancePtr.Unload();
					Frame3.Visible = true;
					// make the list of records visible
					vs1.Focus();
				}
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				BadSearchFlag = false;
				MessageBox.Show("No Records Found That Match The Criteria", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmGetPayee_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtGetAccountNumber.Focus();
			txtGetAccountNumber.SelectionStart = 0;
			txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
		}

		private void frmGetPayee_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Up)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{UP}", false);
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{DOWN}", false);
				}
			}
		}

		private void frmGetPayee_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				if (this.ActiveControl.GetName() == "vs1")
				{
					// do nothing
				}
				else
				{
					KeyAscii = (Keys)0;
					if (this.ActiveControl.GetName() == "txtGetAccountNumber")
					{
						cmdGetAccountNumber_Click();
						return;
					}
					if (this.ActiveControl.GetName() == "txtSearch")
						cmdSearch_Click();
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				if (Frame3.Visible == true)
				{
					cmdClear_Click();
					Frame3.Visible = false;
				}
				else
				{
					Close();
					//MDIParent.InstancePtr.Focus();
				}
			}
			else if (KeyAscii == Keys.Right || KeyAscii == Keys.F13)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Fill_List(ref string x)
		{
			int I;
			recordNum = new int[rs.RecordCount() + 1];
			vs1.Rows = 1;
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				recordNum[I - 1] = FCConvert.ToInt32(rs.Get_Fields_Int32("PayeeID"));
				vs1.AddItem("");
				vs1.TextMatrix(I, 0, FCConvert.ToString(rs.Get_Fields_Int32("PayeeID")));
				vs1.TextMatrix(I, 1, FCConvert.ToString(rs.Get_Fields_String("Description")));
				if (I < rs.RecordCount())
					rs.MoveNext();
			}
		}

		private void frmGetPayee_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetPayee.FillStyle	= 0;
			//frmGetPayee.ScaleWidth	= 9045;
			//frmGetPayee.ScaleHeight	= 7305;
			//frmGetPayee.LinkTopic	= "Form2";
			//frmGetPayee.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			modGlobal.Statics.lngCurrentPayee = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRCUST"))));
			txtGetAccountNumber.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentPayee);
			lblLastAccount.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentPayee);
			vs1.TextMatrix(0, 0, "Payee#");
			vs1.TextMatrix(0, 1, "Name");
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.2));
			vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.65));
            //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, 4);
            vs1.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //FC:FINAL:PB: - issue #2839 fixed alignment
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmGetPayee_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.2));
			vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.65));
		}

		private void mnuFileExit_Click()
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click()
		{
			cmdGetAccountNumber_Click();
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			int intAccount = 0;
			// if there is a record selected
			if (vs1.MouseRow > 0)
			{
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					modGlobal.Statics.lngCurrentPayee = intAccount;
					// gets the Account Number for the item that is double clicked
					txtGetAccountNumber.Text = FCConvert.ToString(modGlobal.Statics.lngCurrentPayee);
					StartProgram(recordNum[vs1.Row - 1]);
				}
				cmdClear_Click();
				// clear the search
				Frame3.Visible = false;
				// make the listbox invisible
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				if (vs1.Row == 1)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.Row == vs1.Rows - 1)
				{
					KeyCode = 0;
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int intAccount = 0;
			// if there is a record selected
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					modGlobal.Statics.lngCurrentPayee = intAccount;
					// gets the Account Number for the item that is double clicked
					StartProgram(recordNum[vs1.Row - 1]);
				}
				cmdClear_Click();
				// clear the search
				Frame3.Visible = false;
				// make the listbox invisible
			}
		}

		private void txtSearch_Enter(object sender, System.EventArgs e)
		{
			//FC:FINAL:CHN: Incorrect indexes.
			// if (cmbDescription.SelectedIndex != 1)
			if (cmbDescription.SelectedIndex != 0)
			{
				//optSearchType[0].Focus();
				// cmbDescription.SelectedIndex = 1;
				cmbDescription.SelectedIndex = 0;
			}
		}

		private void optSearchType_MouseDown(int Index, object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			if (Button == MouseButtonConstants.LeftButton && Shift == 0)
			{
				OKFlag = true;
			}
		}

		private void optSearchType_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		{
			int index = FCConvert.ToInt16(cmbDescription.SelectedIndex);
			optSearchType_MouseDown(index, sender, e);
		}
	}
}
