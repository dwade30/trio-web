﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmSetupARStatement.
	/// </summary>
	partial class frmSetupARStatement : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAllCustomers;
		public fecherFoundation.FCComboBox cmbTransactionDate;
		public fecherFoundation.FCLabel lblTransactionDate;
		public fecherFoundation.FCComboBox cboStatementPayee;
		public fecherFoundation.FCComboBox cboEnd;
		public fecherFoundation.FCComboBox cboStart;
		public fecherFoundation.FCLabel lblTo2;
		public fecherFoundation.FCCheckBox chkZeroBalanceIncluded;
		public fecherFoundation.FCComboBox cboPayee;
		public fecherFoundation.FCButton cmdRequestDate;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupARStatement));
			this.cmbAllCustomers = new fecherFoundation.FCComboBox();
			this.cmbTransactionDate = new fecherFoundation.FCComboBox();
			this.lblTransactionDate = new fecherFoundation.FCLabel();
			this.cboStatementPayee = new fecherFoundation.FCComboBox();
			this.cboEnd = new fecherFoundation.FCComboBox();
			this.cboStart = new fecherFoundation.FCComboBox();
			this.lblTo2 = new fecherFoundation.FCLabel();
			this.chkZeroBalanceIncluded = new fecherFoundation.FCCheckBox();
			this.cboPayee = new fecherFoundation.FCComboBox();
			this.cmdRequestDate = new fecherFoundation.FCButton();
			this.txtStartDate = new Global.T2KDateBox();
			this.txtEndDate = new Global.T2KDateBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessPreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.Command1 = new fecherFoundation.FCButton();
			this.fcLabel2 = new fecherFoundation.FCLabel();
			this.fcLabel3 = new fecherFoundation.FCLabel();
			this.fcLabel4 = new fecherFoundation.FCLabel();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkZeroBalanceIncluded)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRequestDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 587);
			this.BottomPanel.Size = new System.Drawing.Size(898, 10);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdPrintPreview);
			this.ClientArea.Controls.Add(this.fcLabel4);
			this.ClientArea.Controls.Add(this.cboStatementPayee);
			this.ClientArea.Controls.Add(this.cboPayee);
			this.ClientArea.Controls.Add(this.fcLabel3);
			this.ClientArea.Controls.Add(this.cboEnd);
			this.ClientArea.Controls.Add(this.fcLabel2);
			this.ClientArea.Controls.Add(this.lblTo2);
			this.ClientArea.Controls.Add(this.cboStart);
			this.ClientArea.Controls.Add(this.cmbAllCustomers);
			this.ClientArea.Controls.Add(this.Command1);
			this.ClientArea.Controls.Add(this.fcLabel1);
			this.ClientArea.Controls.Add(this.txtEndDate);
			this.ClientArea.Controls.Add(this.cmdRequestDate);
			this.ClientArea.Controls.Add(this.lblTo);
			this.ClientArea.Controls.Add(this.txtStartDate);
			this.ClientArea.Controls.Add(this.cmbTransactionDate);
			this.ClientArea.Controls.Add(this.lblTransactionDate);
			this.ClientArea.Controls.Add(this.chkZeroBalanceIncluded);
			this.ClientArea.Size = new System.Drawing.Size(898, 527);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(898, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(177, 30);
			this.HeaderText.Text = "AR Statements";
			// 
			// cmbAllCustomers
			// 
			this.cmbAllCustomers.AutoSize = false;
			this.cmbAllCustomers.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllCustomers.FormattingEnabled = true;
			this.cmbAllCustomers.Items.AddRange(new object[] {
				"All",
				"Range"
			});
			this.cmbAllCustomers.Location = new System.Drawing.Point(214, 90);
			this.cmbAllCustomers.Name = "cmbAllCustomers";
			this.cmbAllCustomers.Size = new System.Drawing.Size(224, 40);
			this.cmbAllCustomers.TabIndex = 14;
			this.cmbAllCustomers.SelectedIndexChanged += new System.EventHandler(this.optAllCustomers_CheckedChanged);
			// 
			// cmbTransactionDate
			// 
			this.cmbTransactionDate.AutoSize = false;
			this.cmbTransactionDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTransactionDate.FormattingEnabled = true;
			this.cmbTransactionDate.Items.AddRange(new object[] {
				"Transaction Date",
				"Invoice Number"
			});
			this.cmbTransactionDate.Location = new System.Drawing.Point(214, 270);
			this.cmbTransactionDate.Name = "cmbTransactionDate";
			this.cmbTransactionDate.Size = new System.Drawing.Size(224, 40);
			this.cmbTransactionDate.TabIndex = 20;
			// 
			// lblTransactionDate
			// 
			this.lblTransactionDate.AutoSize = true;
			this.lblTransactionDate.Location = new System.Drawing.Point(30, 284);
			this.lblTransactionDate.Name = "lblTransactionDate";
			this.lblTransactionDate.Size = new System.Drawing.Size(144, 15);
			this.lblTransactionDate.TabIndex = 21;
			this.lblTransactionDate.Text = "TRANSACTION ORDER";
			// 
			// cboStatementPayee
			// 
			this.cboStatementPayee.AutoSize = false;
			this.cboStatementPayee.BackColor = System.Drawing.SystemColors.Window;
			this.cboStatementPayee.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStatementPayee.FormattingEnabled = true;
			this.cboStatementPayee.Location = new System.Drawing.Point(214, 378);
			this.cboStatementPayee.Name = "cboStatementPayee";
			this.cboStatementPayee.Size = new System.Drawing.Size(224, 40);
			this.cboStatementPayee.TabIndex = 20;
			// 
			// cboEnd
			// 
			this.cboEnd.AutoSize = false;
			this.cboEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cboEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEnd.Enabled = false;
			this.cboEnd.FormattingEnabled = true;
			this.cboEnd.Location = new System.Drawing.Point(563, 150);
			this.cboEnd.Name = "cboEnd";
			this.cboEnd.Size = new System.Drawing.Size(310, 40);
			this.cboEnd.TabIndex = 15;
			// 
			// cboStart
			// 
			this.cboStart.AutoSize = false;
			this.cboStart.BackColor = System.Drawing.SystemColors.Window;
			this.cboStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStart.Enabled = false;
			this.cboStart.FormattingEnabled = true;
			this.cboStart.Location = new System.Drawing.Point(214, 150);
			this.cboStart.Name = "cboStart";
			this.cboStart.Size = new System.Drawing.Size(310, 40);
			this.cboStart.TabIndex = 14;
			// 
			// lblTo2
			// 
			this.lblTo2.AutoSize = true;
			this.lblTo2.Location = new System.Drawing.Point(533, 164);
			this.lblTo2.Name = "lblTo2";
			this.lblTo2.Size = new System.Drawing.Size(25, 15);
			this.lblTo2.TabIndex = 16;
			this.lblTo2.Text = "TO";
			this.lblTo2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// chkZeroBalanceIncluded
			// 
			this.chkZeroBalanceIncluded.Location = new System.Drawing.Point(30, 330);
			this.chkZeroBalanceIncluded.Name = "chkZeroBalanceIncluded";
			this.chkZeroBalanceIncluded.Size = new System.Drawing.Size(249, 26);
			this.chkZeroBalanceIncluded.TabIndex = 8;
			this.chkZeroBalanceIncluded.Text = "Include zero balance accounts";
			// 
			// cboPayee
			// 
			this.cboPayee.AutoSize = false;
			this.cboPayee.BackColor = System.Drawing.SystemColors.Window;
			this.cboPayee.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboPayee.FormattingEnabled = true;
			this.cboPayee.Location = new System.Drawing.Point(214, 210);
			this.cboPayee.Name = "cboPayee";
			this.cboPayee.Size = new System.Drawing.Size(224, 40);
			this.cboPayee.TabIndex = 7;
			// 
			// cmdRequestDate
			// 
			this.cmdRequestDate.AppearanceKey = "imageButton";
			this.cmdRequestDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdRequestDate.Location = new System.Drawing.Point(361, 30);
			this.cmdRequestDate.Name = "cmdRequestDate";
			this.cmdRequestDate.Size = new System.Drawing.Size(40, 40);
			this.cmdRequestDate.TabIndex = 1;
			this.cmdRequestDate.Click += new System.EventHandler(this.cmdRequestDate_Click);
			// 
			// txtStartDate
			//
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Location = new System.Drawing.Point(214, 30);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(140, 40);
			this.txtStartDate.TabIndex = 3;
			this.txtStartDate.Text = "  /  /";
			// 
			// txtEndDate
			//
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Location = new System.Drawing.Point(443, 30);
			this.txtEndDate.Mask = "##/##/####";
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.Size = new System.Drawing.Size(140, 40);
			this.txtEndDate.TabIndex = 4;
			this.txtEndDate.Text = "  /  /";
			// 
			// lblTo
			// 
			this.lblTo.AutoSize = true;
			this.lblTo.Location = new System.Drawing.Point(409, 44);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(25, 15);
			this.lblTo.TabIndex = 5;
			this.lblTo.Text = "TO";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessPreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessPreview
			// 
			this.mnuProcessPreview.Index = 0;
			this.mnuProcessPreview.Name = "mnuProcessPreview";
			this.mnuProcessPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessPreview.Text = "Print Preview";
			this.mnuProcessPreview.Click += new System.EventHandler(this.mnuProcessPreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(30, 44);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(88, 15);
			this.fcLabel1.TabIndex = 22;
			this.fcLabel1.Text = "DATE RANGE";
			// 
			// Command1
			// 
			this.Command1.AppearanceKey = "imageButton";
			this.Command1.ImageSource = "icon - calendar?color=#707884";
            this.Command1.Location = new System.Drawing.Point(590, 30);
			this.Command1.Name = "Command1";
			this.Command1.Size = new System.Drawing.Size(40, 40);
			this.Command1.TabIndex = 2;
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			// 
			// fcLabel2
			// 
			this.fcLabel2.AutoSize = true;
			this.fcLabel2.Location = new System.Drawing.Point(30, 104);
			this.fcLabel2.Name = "fcLabel2";
			this.fcLabel2.Size = new System.Drawing.Size(86, 15);
			this.fcLabel2.TabIndex = 23;
			this.fcLabel2.Text = "CUSTOMERS";
			// 
			// fcLabel3
			// 
			this.fcLabel3.AutoSize = true;
			this.fcLabel3.Location = new System.Drawing.Point(30, 224);
			this.fcLabel3.Name = "fcLabel3";
			this.fcLabel3.Size = new System.Drawing.Size(85, 15);
			this.fcLabel3.TabIndex = 24;
			this.fcLabel3.Text = "BILL TYPE(S)";
			// 
			// fcLabel4
			// 
			this.fcLabel4.AutoSize = true;
			this.fcLabel4.Location = new System.Drawing.Point(30, 392);
			this.fcLabel4.Name = "fcLabel4";
			this.fcLabel4.Size = new System.Drawing.Size(122, 15);
			this.fcLabel4.TabIndex = 25;
			this.fcLabel4.Text = "REMIT TO FORMAT";
			// 
			// cmdPrintPreview
			// 
			this.cmdPrintPreview.AppearanceKey = "acceptButton";
			this.cmdPrintPreview.Location = new System.Drawing.Point(30, 453);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrintPreview.Size = new System.Drawing.Size(127, 40);
			this.cmdPrintPreview.TabIndex = 26;
			this.cmdPrintPreview.Text = "Print Preview";
			this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
			// 
			// frmSetupARStatement
			// 
			this.ClientSize = new System.Drawing.Size(898, 597);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupARStatement";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "AR Statements";
			this.Load += new System.EventHandler(this.frmSetupARStatement_Load);
			this.Activated += new System.EventHandler(this.frmSetupARStatement_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupARStatement_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkZeroBalanceIncluded)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRequestDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCButton Command1;
		private FCLabel fcLabel1;
		private FCLabel fcLabel3;
		private FCLabel fcLabel2;
		private FCLabel fcLabel4;
		private FCButton cmdPrintPreview;
	}
}
