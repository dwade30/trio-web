//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmRateRecChoice.
	/// </summary>
	partial class frmRateRecChoice : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCComboBox cmbRange;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkPrint;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtRange;
		public fecherFoundation.FCFrame fraPrintOptions;
		public fecherFoundation.FCComboBox cmbExtraLines;
		public fecherFoundation.FCCheckBox chkPrint_3;
		public fecherFoundation.FCCheckBox chkPrint_2;
		public fecherFoundation.FCCheckBox chkPrint_1;
		public fecherFoundation.FCCheckBox chkPrint_0;
		public fecherFoundation.FCLabel lblExtraLines;
		public fecherFoundation.FCFrame fraRateInfo;
		public fecherFoundation.FCButton cmdRIClose;
		public fecherFoundation.FCGrid vsRateInfo;
		public fecherFoundation.FCFrame fraPreLienEdit;
		public fecherFoundation.FCComboBox cmbReportDetail;
		public Global.T2KDateBox txtMailDate;
		public fecherFoundation.FCLabel lblReportLen;
		public fecherFoundation.FCLabel lblMailDate;
		public fecherFoundation.FCFrame fraRange;
		public Wisej.Web.VScrollBar vsbMinAmount;
		public fecherFoundation.FCTextBox txtMinimumAmount;
		public fecherFoundation.FCFrame fraTextRange;
		public fecherFoundation.FCTextBox txtRange_1;
		public fecherFoundation.FCTextBox txtRange_0;
		public fecherFoundation.FCLabel lblMinAmount;
		public fecherFoundation.FCPanel fraRate;
		public FCGrid vsRate;
		public fecherFoundation.FCPanel fraQuestions;
		public fecherFoundation.FCComboBox cmbBillType;
		public fecherFoundation.FCLabel lblInstructions;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbPrint = new fecherFoundation.FCComboBox();
			this.lblPrint = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.fraPrintOptions = new fecherFoundation.FCFrame();
			this.cmbExtraLines = new fecherFoundation.FCComboBox();
			this.chkPrint_3 = new fecherFoundation.FCCheckBox();
			this.chkPrint_2 = new fecherFoundation.FCCheckBox();
			this.chkPrint_1 = new fecherFoundation.FCCheckBox();
			this.chkPrint_0 = new fecherFoundation.FCCheckBox();
			this.lblExtraLines = new fecherFoundation.FCLabel();
			this.fraRateInfo = new fecherFoundation.FCFrame();
			this.cmdRIClose = new fecherFoundation.FCButton();
			this.vsRateInfo = new fecherFoundation.FCGrid();
			this.fraPreLienEdit = new fecherFoundation.FCFrame();
			this.cmbReportDetail = new fecherFoundation.FCComboBox();
			this.txtMailDate = new Global.T2KDateBox();
			this.lblReportLen = new fecherFoundation.FCLabel();
			this.lblMailDate = new fecherFoundation.FCLabel();
			this.fraRange = new fecherFoundation.FCFrame();
			this.vsbMinAmount = new Wisej.Web.VScrollBar();
			this.txtMinimumAmount = new fecherFoundation.FCTextBox();
			this.fraTextRange = new fecherFoundation.FCFrame();
			this.txtRange_1 = new fecherFoundation.FCTextBox();
			this.txtRange_0 = new fecherFoundation.FCTextBox();
			this.lblMinAmount = new fecherFoundation.FCLabel();
			this.fraRate = new fecherFoundation.FCPanel();
			this.vsRate = new fecherFoundation.FCGrid();
			this.fraQuestions = new fecherFoundation.FCPanel();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.cmbBillType = new fecherFoundation.FCComboBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.fraPrint = new fecherFoundation.FCPanel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPrintOptions)).BeginInit();
			this.fraPrintOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).BeginInit();
			this.fraRateInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPreLienEdit)).BeginInit();
			this.fraPreLienEdit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtMailDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTextRange)).BeginInit();
			this.fraTextRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRate)).BeginInit();
			this.fraRate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
			this.fraQuestions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPrint)).BeginInit();
			this.fraPrint.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 538);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraPrint);
			this.ClientArea.Controls.Add(this.fraPrintOptions);
			this.ClientArea.Controls.Add(this.fraRateInfo);
			this.ClientArea.Controls.Add(this.fraPreLienEdit);
			this.ClientArea.Controls.Add(this.fraRange);
			this.ClientArea.Controls.Add(this.fraRate);
			this.ClientArea.Controls.Add(this.fraQuestions);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(1078, 478);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(225, 30);
			this.HeaderText.Text = "Select Rate Record";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbPrint
			// 
			this.cmbPrint.AutoSize = false;
			this.cmbPrint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPrint.FormattingEnabled = true;
			this.cmbPrint.Items.AddRange(new object[] {
				"Labels",
				"Certified Mail Forms"
			});
			this.cmbPrint.Location = new System.Drawing.Point(232, 30);
			this.cmbPrint.Name = "cmbPrint";
			this.cmbPrint.Size = new System.Drawing.Size(200, 40);
			this.cmbPrint.TabIndex = 27;
			this.ToolTip1.SetToolTip(this.cmbPrint, null);
			this.cmbPrint.SelectedIndexChanged += new System.EventHandler(this.optPrint_KeyDown);
			// 
			// lblPrint
			// 
			this.lblPrint.AutoSize = true;
			this.lblPrint.Location = new System.Drawing.Point(30, 44);
			this.lblPrint.Name = "lblPrint";
			this.lblPrint.Size = new System.Drawing.Size(132, 15);
			this.lblPrint.TabIndex = 28;
			this.lblPrint.Text = "SELECT PRINT TYPE";
			this.ToolTip1.SetToolTip(this.lblPrint, null);
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All Accounts",
				"Range by Name",
				"Range by Account"
			});
			this.cmbRange.Location = new System.Drawing.Point(30, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(378, 40);
			this.cmbRange.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.cmbRange, null);
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// fraPrintOptions
			// 
			this.fraPrintOptions.Controls.Add(this.cmbExtraLines);
			this.fraPrintOptions.Controls.Add(this.chkPrint_3);
			this.fraPrintOptions.Controls.Add(this.chkPrint_2);
			this.fraPrintOptions.Controls.Add(this.chkPrint_1);
			this.fraPrintOptions.Controls.Add(this.chkPrint_0);
			this.fraPrintOptions.Controls.Add(this.lblExtraLines);
			this.fraPrintOptions.Location = new System.Drawing.Point(421, 688);
			this.fraPrintOptions.Name = "fraPrintOptions";
			this.fraPrintOptions.Size = new System.Drawing.Size(348, 186);
			this.fraPrintOptions.TabIndex = 26;
			this.fraPrintOptions.Text = "Print Options";
			this.ToolTip1.SetToolTip(this.fraPrintOptions, null);
			// 
			// cmbExtraLines
			// 
			this.cmbExtraLines.AutoSize = false;
			this.cmbExtraLines.BackColor = System.Drawing.SystemColors.Window;
			this.cmbExtraLines.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbExtraLines.FormattingEnabled = true;
			this.cmbExtraLines.Location = new System.Drawing.Point(178, 30);
			this.cmbExtraLines.Name = "cmbExtraLines";
			this.cmbExtraLines.Size = new System.Drawing.Size(150, 40);
			this.cmbExtraLines.TabIndex = 31;
			this.cmbExtraLines.Text = "Combo1";
			this.ToolTip1.SetToolTip(this.cmbExtraLines, null);
			// 
			// chkPrint_3
			// 
			this.chkPrint_3.Location = new System.Drawing.Point(178, 138);
			this.chkPrint_3.Name = "chkPrint_3";
			this.chkPrint_3.Size = new System.Drawing.Size(87, 26);
			this.chkPrint_3.TabIndex = 30;
			this.chkPrint_3.Text = "Address";
			this.ToolTip1.SetToolTip(this.chkPrint_3, null);
			// 
			// chkPrint_2
			// 
			this.chkPrint_2.Location = new System.Drawing.Point(178, 90);
			this.chkPrint_2.Name = "chkPrint_2";
			this.chkPrint_2.Size = new System.Drawing.Size(89, 26);
			this.chkPrint_2.TabIndex = 29;
			this.chkPrint_2.Text = "Location";
			this.ToolTip1.SetToolTip(this.chkPrint_2, null);
			// 
			// chkPrint_1
			// 
			this.chkPrint_1.Location = new System.Drawing.Point(30, 138);
			this.chkPrint_1.Name = "chkPrint_1";
			this.chkPrint_1.Size = new System.Drawing.Size(107, 26);
			this.chkPrint_1.TabIndex = 28;
			this.chkPrint_1.Text = "Book Page";
			this.ToolTip1.SetToolTip(this.chkPrint_1, null);
			// 
			// chkPrint_0
			// 
			this.chkPrint_0.Location = new System.Drawing.Point(30, 90);
			this.chkPrint_0.Name = "chkPrint_0";
			this.chkPrint_0.Size = new System.Drawing.Size(86, 26);
			this.chkPrint_0.TabIndex = 27;
			this.chkPrint_0.Text = "Map Lot";
			this.ToolTip1.SetToolTip(this.chkPrint_0, null);
			// 
			// lblExtraLines
			// 
			this.lblExtraLines.Location = new System.Drawing.Point(30, 44);
			this.lblExtraLines.Name = "lblExtraLines";
			this.lblExtraLines.Size = new System.Drawing.Size(78, 16);
			this.lblExtraLines.TabIndex = 32;
			this.lblExtraLines.Text = "EXTRA LINES";
			this.ToolTip1.SetToolTip(this.lblExtraLines, null);
			// 
			// fraRateInfo
			// 
			this.fraRateInfo.BackColor = System.Drawing.Color.White;
			this.fraRateInfo.Controls.Add(this.cmdRIClose);
			this.fraRateInfo.Controls.Add(this.vsRateInfo);
			this.fraRateInfo.Location = new System.Drawing.Point(3, 312);
			this.fraRateInfo.Name = "fraRateInfo";
			this.fraRateInfo.Size = new System.Drawing.Size(412, 456);
			this.fraRateInfo.TabIndex = 23;
			this.fraRateInfo.Text = "Year Information";
			this.ToolTip1.SetToolTip(this.fraRateInfo, null);
			this.fraRateInfo.Visible = false;
			// 
			// cmdRIClose
			// 
			this.cmdRIClose.AppearanceKey = "actionButton";
			this.cmdRIClose.Location = new System.Drawing.Point(149, 410);
			this.cmdRIClose.Name = "cmdRIClose";
			this.cmdRIClose.Size = new System.Drawing.Size(87, 40);
			this.cmdRIClose.TabIndex = 24;
			this.cmdRIClose.Text = "Close";
			this.ToolTip1.SetToolTip(this.cmdRIClose, null);
			this.cmdRIClose.Click += new System.EventHandler(this.cmdRIClose_Click);
			// 
			// vsRateInfo
			// 
			this.vsRateInfo.AllowSelection = false;
			this.vsRateInfo.AllowUserToResizeColumns = false;
			this.vsRateInfo.AllowUserToResizeRows = false;
			this.vsRateInfo.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsRateInfo.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsRateInfo.BackColorBkg = System.Drawing.Color.Empty;
			this.vsRateInfo.BackColorFixed = System.Drawing.Color.Empty;
			this.vsRateInfo.BackColorSel = System.Drawing.Color.Empty;
			this.vsRateInfo.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsRateInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsRateInfo.ColumnHeadersHeight = 30;
			this.vsRateInfo.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsRateInfo.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsRateInfo.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsRateInfo.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsRateInfo.FixedCols = 0;
			this.vsRateInfo.FixedRows = 0;
			this.vsRateInfo.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsRateInfo.FrozenCols = 0;
			this.vsRateInfo.GridColor = System.Drawing.Color.Empty;
			this.vsRateInfo.Location = new System.Drawing.Point(30, 30);
			this.vsRateInfo.Name = "vsRateInfo";
			this.vsRateInfo.ReadOnly = true;
			this.vsRateInfo.RowHeadersVisible = false;
			this.vsRateInfo.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsRateInfo.RowHeightMin = 0;
			this.vsRateInfo.Rows = 12;
			this.vsRateInfo.ShowColumnVisibilityMenu = false;
			this.vsRateInfo.Size = new System.Drawing.Size(352, 360);
			this.vsRateInfo.StandardTab = true;
			this.vsRateInfo.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsRateInfo.TabIndex = 25;
			this.ToolTip1.SetToolTip(this.vsRateInfo, null);
			// 
			// fraPreLienEdit
			// 
			this.fraPreLienEdit.Controls.Add(this.cmbReportDetail);
			this.fraPreLienEdit.Controls.Add(this.txtMailDate);
			this.fraPreLienEdit.Controls.Add(this.lblReportLen);
			this.fraPreLienEdit.Controls.Add(this.lblMailDate);
			this.fraPreLienEdit.Location = new System.Drawing.Point(421, 312);
			this.fraPreLienEdit.Name = "fraPreLienEdit";
			this.fraPreLienEdit.Size = new System.Drawing.Size(420, 150);
			this.fraPreLienEdit.TabIndex = 16;
			this.fraPreLienEdit.Text = "Lien Edit Report Format";
			this.ToolTip1.SetToolTip(this.fraPreLienEdit, null);
			this.fraPreLienEdit.Visible = false;
			// 
			// cmbReportDetail
			// 
			this.cmbReportDetail.AutoSize = false;
			this.cmbReportDetail.BackColor = System.Drawing.SystemColors.Window;
			this.cmbReportDetail.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbReportDetail.FormattingEnabled = true;
			this.cmbReportDetail.Location = new System.Drawing.Point(244, 90);
			this.cmbReportDetail.Name = "cmbReportDetail";
			this.cmbReportDetail.Size = new System.Drawing.Size(156, 40);
			this.cmbReportDetail.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.cmbReportDetail, null);
			this.cmbReportDetail.DropDown += new System.EventHandler(this.cmbReportDetail_DropDown);
			this.cmbReportDetail.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbReportDetail_KeyDown);
			// 
			// txtMailDate
			// 
			this.txtMailDate.Location = new System.Drawing.Point(244, 30);
			this.txtMailDate.Mask = "##/##/####";
			this.txtMailDate.Name = "txtMailDate";
			this.txtMailDate.Size = new System.Drawing.Size(115, 40);
			this.txtMailDate.TabIndex = 2;
			this.txtMailDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtMailDate, null);
			this.txtMailDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtMailDate_Validate);
			// 
			// lblReportLen
			// 
			this.lblReportLen.AutoSize = true;
			this.lblReportLen.Location = new System.Drawing.Point(30, 104);
			this.lblReportLen.Name = "lblReportLen";
			this.lblReportLen.Size = new System.Drawing.Size(106, 15);
			this.lblReportLen.TabIndex = 18;
			this.lblReportLen.Text = "REPORT DETAIL";
			this.ToolTip1.SetToolTip(this.lblReportLen, null);
			// 
			// lblMailDate
			// 
			this.lblMailDate.AutoSize = true;
			this.lblMailDate.Location = new System.Drawing.Point(30, 44);
			this.lblMailDate.Name = "lblMailDate";
			this.lblMailDate.Size = new System.Drawing.Size(144, 15);
			this.lblMailDate.TabIndex = 17;
			this.lblMailDate.Text = "MAIL / INTEREST DATE";
			this.ToolTip1.SetToolTip(this.lblMailDate, null);
			// 
			// fraRange
			// 
			this.fraRange.Controls.Add(this.vsbMinAmount);
			this.fraRange.Controls.Add(this.cmbRange);
			this.fraRange.Controls.Add(this.txtMinimumAmount);
			this.fraRange.Controls.Add(this.fraTextRange);
			this.fraRange.Controls.Add(this.lblMinAmount);
			this.fraRange.Location = new System.Drawing.Point(421, 485);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(482, 197);
			this.fraRange.TabIndex = 13;
			this.fraRange.Text = "Selection Criteria";
			this.ToolTip1.SetToolTip(this.fraRange, null);
			this.fraRange.Visible = false;
			// 
			// vsbMinAmount
			// 
			this.vsbMinAmount.LargeChange = 3;
			this.vsbMinAmount.Location = new System.Drawing.Point(391, 150);
			this.vsbMinAmount.Maximum = 2;
			this.vsbMinAmount.Name = "vsbMinAmount";
			this.vsbMinAmount.Size = new System.Drawing.Size(17, 40);
			this.vsbMinAmount.SmallChange = 3;
			this.vsbMinAmount.TabIndex = 10;
			this.vsbMinAmount.TabStop = true;
			this.ToolTip1.SetToolTip(this.vsbMinAmount, null);
			this.vsbMinAmount.Value = 1;
			this.vsbMinAmount.ValueChanged += new System.EventHandler(this.vsbMinAmount_ValueChanged);
			// 
			// txtMinimumAmount
			// 
			this.txtMinimumAmount.AutoSize = false;
			this.txtMinimumAmount.BackColor = System.Drawing.SystemColors.Window;
			this.txtMinimumAmount.Location = new System.Drawing.Point(311, 150);
			this.txtMinimumAmount.Name = "txtMinimumAmount";
			this.txtMinimumAmount.Size = new System.Drawing.Size(80, 40);
			this.txtMinimumAmount.TabIndex = 9;
			this.txtMinimumAmount.Text = "0.00";
			this.txtMinimumAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtMinimumAmount, "Minimum principal due in order to process.");
			this.txtMinimumAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMinimumAmount_KeyPress);
			this.txtMinimumAmount.Enter += new System.EventHandler(this.txtMinimumAmount_Enter);
			this.txtMinimumAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtMinimumAmount_Validating);
			// 
			// fraTextRange
			// 
			this.fraTextRange.Controls.Add(this.txtRange_1);
			this.fraTextRange.Controls.Add(this.txtRange_0);
			this.fraTextRange.Location = new System.Drawing.Point(0, 70);
			this.fraTextRange.Name = "fraTextRange";
			this.fraTextRange.Size = new System.Drawing.Size(427, 60);
			this.fraTextRange.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.fraTextRange, null);
			// 
			// txtRange_1
			// 
			this.txtRange_1.AutoSize = false;
			this.txtRange_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtRange_1.Enabled = false;
			this.txtRange_1.Location = new System.Drawing.Point(229, 20);
			this.txtRange_1.Name = "txtRange_1";
			this.txtRange_1.Size = new System.Drawing.Size(179, 40);
			this.txtRange_1.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtRange_1, null);
			this.txtRange_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRange_KeyPress);
			// 
			// txtRange_0
			// 
			this.txtRange_0.AutoSize = false;
			this.txtRange_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtRange_0.Enabled = false;
			this.txtRange_0.Location = new System.Drawing.Point(30, 20);
			this.txtRange_0.Name = "txtRange_0";
			this.txtRange_0.Size = new System.Drawing.Size(179, 40);
			this.txtRange_0.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtRange_0, null);
			this.txtRange_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRange_KeyPress);
			// 
			// lblMinAmount
			// 
			this.lblMinAmount.Location = new System.Drawing.Point(30, 164);
			this.lblMinAmount.Name = "lblMinAmount";
			this.lblMinAmount.Size = new System.Drawing.Size(246, 16);
			this.lblMinAmount.TabIndex = 15;
			this.lblMinAmount.Text = "MINIMUM PRINCIPAL AMOUNT TO PROCESS";
			this.ToolTip1.SetToolTip(this.lblMinAmount, null);
			// 
			// fraRate
			// 
			this.fraRate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraRate.Controls.Add(this.vsRate);
			this.fraRate.Location = new System.Drawing.Point(0, 103);
			this.fraRate.Name = "fraRate";
			this.fraRate.Size = new System.Drawing.Size(1078, 500);
			this.fraRate.TabIndex = 12;
			this.fraRate.Text = "Select Rate Record";
			this.ToolTip1.SetToolTip(this.fraRate, null);
			this.fraRate.Visible = false;
			// 
			// vsRate
			// 
			this.vsRate.AllowSelection = false;
			this.vsRate.AllowUserToResizeColumns = false;
			this.vsRate.AllowUserToResizeRows = false;
			this.vsRate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsRate.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsRate.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsRate.BackColorBkg = System.Drawing.Color.Empty;
			this.vsRate.BackColorFixed = System.Drawing.Color.Empty;
			this.vsRate.BackColorSel = System.Drawing.Color.Empty;
			this.vsRate.Cols = 5;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsRate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsRate.ColumnHeadersHeight = 30;
			this.vsRate.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsRate.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsRate.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsRate.FixedCols = 0;
			this.vsRate.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsRate.FrozenCols = 0;
			this.vsRate.GridColor = System.Drawing.Color.Empty;
			this.vsRate.Location = new System.Drawing.Point(30, 17);
			this.vsRate.Name = "vsRate";
			this.vsRate.ReadOnly = true;
			this.vsRate.RowHeadersVisible = false;
			this.vsRate.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsRate.RowHeightMin = 0;
			this.vsRate.Rows = 1;
			this.vsRate.ShowColumnVisibilityMenu = false;
			this.vsRate.Size = new System.Drawing.Size(1018, 462);
			this.vsRate.StandardTab = true;
			this.vsRate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsRate.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.vsRate, null);
			this.vsRate.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsRate_CellChanged);
			this.vsRate.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRate_ValidateEdit);
			this.vsRate.CurrentCellChanged += new System.EventHandler(this.vsRate_RowColChange);
			this.vsRate.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsRate_MouseDownEvent);
			this.vsRate.Enter += new System.EventHandler(this.vsRate_Enter);
			this.vsRate.DoubleClick += new System.EventHandler(this.vsRate_DblClick);
			// 
			// fraQuestions
			// 
			this.fraQuestions.Controls.Add(this.fcLabel1);
			this.fraQuestions.Controls.Add(this.cmbBillType);
			this.fraQuestions.Location = new System.Drawing.Point(0, 0);
			this.fraQuestions.Name = "fraQuestions";
			this.fraQuestions.Size = new System.Drawing.Size(500, 90);
			this.fraQuestions.TabIndex = 11;
			this.fraQuestions.Text = "Bill Type";
			this.ToolTip1.SetToolTip(this.fraQuestions, null);
			this.fraQuestions.Visible = false;
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(30, 44);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(68, 15);
			this.fcLabel1.TabIndex = 1;
			this.fcLabel1.Text = "BILL TYPE";
			this.ToolTip1.SetToolTip(this.fcLabel1, null);
			// 
			// cmbBillType
			// 
			this.cmbBillType.AutoSize = false;
			this.cmbBillType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBillType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBillType.FormattingEnabled = true;
			this.cmbBillType.Location = new System.Drawing.Point(197, 30);
			this.cmbBillType.Name = "cmbBillType";
			this.cmbBillType.Size = new System.Drawing.Size(300, 40);
			this.cmbBillType.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.cmbBillType, null);
			this.cmbBillType.SelectedIndexChanged += new System.EventHandler(this.cmbBillType_SelectedIndexChanged);
			this.cmbBillType.Enter += new System.EventHandler(this.cmbBillType_Enter);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(482, 52);
			this.lblInstructions.TabIndex = 22;
			this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblInstructions, null);
			this.lblInstructions.Visible = false;
			// 
			// fraPrint
			// 
			this.fraPrint.Controls.Add(this.lblPrint);
			this.fraPrint.Controls.Add(this.cmbPrint);
			this.fraPrint.Location = new System.Drawing.Point(0, 0);
			this.fraPrint.Name = "fraPrint";
			this.fraPrint.Size = new System.Drawing.Size(500, 90);
			this.fraPrint.TabIndex = 29;
			this.ToolTip1.SetToolTip(this.fraPrint, null);
			this.fraPrint.Visible = false;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.AutoSize = true;
			this.btnProcess.Location = new System.Drawing.Point(461, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(110, 48);
			this.btnProcess.TabIndex = 0;
			this.btnProcess.Text = "Continue";
			this.ToolTip1.SetToolTip(this.btnProcess, null);
			this.btnProcess.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmRateRecChoice
			// 
			this.ClientSize = new System.Drawing.Size(1078, 646);
			this.KeyPreview = true;
			this.Name = "frmRateRecChoice";
			this.Text = "Select Rate Record";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmRateRecChoice_Load);
			this.Activated += new System.EventHandler(this.frmRateRecChoice_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRateRecChoice_KeyDown);
			this.Enter += new System.EventHandler(this.frmRateRecChoice_Enter);
			this.Resize += new System.EventHandler(this.frmRateRecChoice_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.BottomPanel.PerformLayout();
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPrintOptions)).EndInit();
			this.fraPrintOptions.ResumeLayout(false);
			this.fraPrintOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).EndInit();
			this.fraRateInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPreLienEdit)).EndInit();
			this.fraPreLienEdit.ResumeLayout(false);
			this.fraPreLienEdit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtMailDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTextRange)).EndInit();
			this.fraTextRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRate)).EndInit();
			this.fraRate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
			this.fraQuestions.ResumeLayout(false);
			this.fraQuestions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPrint)).EndInit();
			this.fraPrint.ResumeLayout(false);
			this.fraPrint.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCPanel fraPrint;
		private FCLabel fcLabel1;
		private FCButton btnProcess;
	}
}