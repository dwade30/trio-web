﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillCreate.
	/// </summary>
	partial class frmBillCreate : BaseForm
	{
		public fecherFoundation.FCFrame fraBillInfo;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame fraFlat;
		public Global.T2KBackFillDecimal txtFlatAmount;
		public fecherFoundation.FCComboBox cboInterestMethod;
		public fecherFoundation.FCFrame fraPerDiem;
		public Global.T2KBackFillDecimal txtPerDiem;
		public fecherFoundation.FCLabel Label12;
		public Global.T2KDateBox txtBillDate;
		public Global.T2KDateBox txtInterestStartDate;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCGrid vsAccounts;
		public fecherFoundation.FCComboBox cboRateKeys;
		public fecherFoundation.FCLabel lblRateSelect;
		public fecherFoundation.FCLabel lblSelectAccounts;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.fraBillInfo = new fecherFoundation.FCFrame();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboInterestMethod = new fecherFoundation.FCComboBox();
			this.fraPerDiem = new fecherFoundation.FCFrame();
			this.txtPerDiem = new Global.T2KBackFillDecimal();
			this.fraFlat = new fecherFoundation.FCFrame();
			this.txtFlatAmount = new Global.T2KBackFillDecimal();
			this.Label12 = new fecherFoundation.FCLabel();
			this.txtBillDate = new Global.T2KDateBox();
			this.txtInterestStartDate = new Global.T2KDateBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.vsAccounts = new fecherFoundation.FCGrid();
			this.cboRateKeys = new fecherFoundation.FCComboBox();
			this.lblRateSelect = new fecherFoundation.FCLabel();
			this.lblSelectAccounts = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBillInfo)).BeginInit();
			this.fraBillInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPerDiem)).BeginInit();
			this.fraPerDiem.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPerDiem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFlat)).BeginInit();
			this.fraFlat.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtFlatAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterestStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 475);
			this.BottomPanel.Size = new System.Drawing.Size(521, 96);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsAccounts);
			this.ClientArea.Controls.Add(this.cboRateKeys);
			this.ClientArea.Controls.Add(this.lblRateSelect);
			this.ClientArea.Controls.Add(this.lblSelectAccounts);
			this.ClientArea.Controls.Add(this.fraBillInfo);
			this.ClientArea.Size = new System.Drawing.Size(521, 415);
			this.ClientArea.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(521, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(138, 30);
			this.HeaderText.Text = "Create Bills";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// fraBillInfo
			// 
			this.fraBillInfo.AppearanceKey = "groupBoxLeftBorder";
			this.fraBillInfo.Controls.Add(this.Frame2);
			this.fraBillInfo.Controls.Add(this.txtBillDate);
			this.fraBillInfo.Controls.Add(this.txtInterestStartDate);
			this.fraBillInfo.Controls.Add(this.Label1);
			this.fraBillInfo.Controls.Add(this.Label2);
			this.fraBillInfo.Location = new System.Drawing.Point(30, 30);
			this.fraBillInfo.Name = "fraBillInfo";
			this.fraBillInfo.Size = new System.Drawing.Size(380, 354);
			this.fraBillInfo.TabIndex = 4;
			this.fraBillInfo.Text = "Bill Information";
			this.ToolTip1.SetToolTip(this.fraBillInfo, null);
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxLeftBorder";
			this.Frame2.Controls.Add(this.cboInterestMethod);
			this.Frame2.Controls.Add(this.fraPerDiem);
			this.Frame2.Controls.Add(this.fraFlat);
			this.Frame2.Location = new System.Drawing.Point(20, 166);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(298, 186);
			this.Frame2.TabIndex = 4;
			this.Frame2.Text = "Interest Method";
			this.ToolTip1.SetToolTip(this.Frame2, null);
			// 
			// cboInterestMethod
			// 
			this.cboInterestMethod.AutoSize = false;
			this.cboInterestMethod.BackColor = System.Drawing.SystemColors.Window;
			this.cboInterestMethod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboInterestMethod.FormattingEnabled = true;
			this.cboInterestMethod.Items.AddRange(new object[] {
				"AF  -  Auto Flat Rate",
				"AP  -  Auto Per Diem",
				"DF  -  Demand Flat Rate",
				"DP  -  Demand Per Diem"
			});
			this.cboInterestMethod.Location = new System.Drawing.Point(20, 30);
			this.cboInterestMethod.Name = "cboInterestMethod";
			this.cboInterestMethod.Size = new System.Drawing.Size(257, 40);
			this.cboInterestMethod.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.cboInterestMethod, null);
			this.cboInterestMethod.SelectedIndexChanged += new System.EventHandler(this.cboInterestMethod_SelectedIndexChanged);
			this.cboInterestMethod.DropDown += new System.EventHandler(this.cboInterestMethod_DropDown);
			// 
			// fraPerDiem
			// 
			this.fraPerDiem.Controls.Add(this.txtPerDiem);
            this.fraPerDiem.Controls.Add(this.Label12);
			this.fraPerDiem.Location = new System.Drawing.Point(20, 90);
			this.fraPerDiem.Name = "fraPerDiem";
			this.fraPerDiem.Size = new System.Drawing.Size(257, 82);
			this.fraPerDiem.TabIndex = 2;
			this.fraPerDiem.Text = "Per Diem Rate";
			this.ToolTip1.SetToolTip(this.fraPerDiem, null);
			this.fraPerDiem.Visible = false;
			// 
			// txtPerDiem
			//
			this.txtPerDiem.MaxLength = 8;
			this.txtPerDiem.Location = new System.Drawing.Point(20, 30);
			this.txtPerDiem.MaxLength = 8;
			this.txtPerDiem.Name = "txtPerDiem";
			this.txtPerDiem.Size = new System.Drawing.Size(90, 40);
			this.txtPerDiem.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.txtPerDiem, null);
			// 
			// fraFlat
			// 
			this.fraFlat.Controls.Add(this.txtFlatAmount);
			this.fraFlat.Location = new System.Drawing.Point(20, 90);
			this.fraFlat.Name = "fraFlat";
			this.fraFlat.Size = new System.Drawing.Size(257, 95);
			this.fraFlat.TabIndex = 1;
			this.fraFlat.Text = "Flat Amount";
			this.ToolTip1.SetToolTip(this.fraFlat, null);
			// 
			// txtFlatAmount
			//
			this.txtFlatAmount.MaxLength = 8;
			this.txtFlatAmount.Location = new System.Drawing.Point(20, 30);
			this.txtFlatAmount.MaxLength = 8;
			this.txtFlatAmount.Name = "txtFlatAmount";
			this.txtFlatAmount.Size = new System.Drawing.Size(90, 40);
			this.txtFlatAmount.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.txtFlatAmount, null);
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(116, 45);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(66, 21);
			this.Label12.TabIndex = 1;
			this.Label12.Text = "%";
			this.ToolTip1.SetToolTip(this.Label12, null);
			// 
			// txtBillDate
			//
			this.txtBillDate.MaxLength = 10;
			this.txtBillDate.Location = new System.Drawing.Point(218, 30);
			this.txtBillDate.Mask = "##/##/####";
			this.txtBillDate.MaxLength = 10;
			this.txtBillDate.Name = "txtBillDate";
			this.txtBillDate.Size = new System.Drawing.Size(112, 40);
			this.txtBillDate.TabIndex = 1;
			this.txtBillDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtBillDate, null);
			this.txtBillDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtBillDate_Validate);
			// 
			// txtInterestStartDate
			//
			this.txtInterestStartDate.MaxLength = 10;
			this.txtInterestStartDate.Location = new System.Drawing.Point(218, 90);
			this.txtInterestStartDate.Mask = "##/##/####";
			this.txtInterestStartDate.MaxLength = 10;
			this.txtInterestStartDate.Name = "txtInterestStartDate";
			this.txtInterestStartDate.Size = new System.Drawing.Size(112, 40);
			this.txtInterestStartDate.TabIndex = 3;
			this.txtInterestStartDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtInterestStartDate, null);
			this.txtInterestStartDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtInterestStartDate_Validate);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 104);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(141, 19);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "INTEREST START DATE";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(66, 19);
			this.Label2.TabIndex = 0;
			this.Label2.Text = "BILL DATE";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// vsAccounts
			// 
			this.vsAccounts.AllowSelection = false;
			this.vsAccounts.AllowUserToResizeColumns = false;
			this.vsAccounts.AllowUserToResizeRows = false;
			this.vsAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAccounts.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorSel = System.Drawing.Color.Empty;
			this.vsAccounts.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsAccounts.ColumnHeadersHeight = 30;
			this.vsAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAccounts.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAccounts.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsAccounts.ExtendLastCol = true;
			this.vsAccounts.FixedCols = 0;
			this.vsAccounts.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.FrozenCols = 0;
			this.vsAccounts.GridColor = System.Drawing.Color.Empty;
			this.vsAccounts.Location = new System.Drawing.Point(30, 94);
			this.vsAccounts.Name = "vsAccounts";
			this.vsAccounts.ReadOnly = true;
			this.vsAccounts.RowHeadersVisible = false;
			this.vsAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAccounts.RowHeightMin = 0;
			this.vsAccounts.Rows = 1;
			this.vsAccounts.ShowColumnVisibilityMenu = false;
			this.vsAccounts.Size = new System.Drawing.Size(474, 232);
			this.vsAccounts.StandardTab = true;
			this.vsAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsAccounts.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.vsAccounts, null);
			this.vsAccounts.Visible = false;
			// 
			// cboRateKeys
			// 
			this.cboRateKeys.AutoSize = false;
			this.cboRateKeys.BackColor = System.Drawing.SystemColors.Window;
			this.cboRateKeys.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboRateKeys.FormattingEnabled = true;
			this.cboRateKeys.Location = new System.Drawing.Point(30, 110);
			this.cboRateKeys.Name = "cboRateKeys";
			this.cboRateKeys.Size = new System.Drawing.Size(355, 40);
			this.cboRateKeys.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.cboRateKeys, "<Bill Type Code>  <Bill Date>   <Rate Key Description>");
			// 
			// lblRateSelect
			// 
			this.lblRateSelect.Location = new System.Drawing.Point(30, 70);
			this.lblRateSelect.Name = "lblRateSelect";
			this.lblRateSelect.Size = new System.Drawing.Size(474, 16);
			this.lblRateSelect.TabIndex = 1;
			this.lblRateSelect.Text = "PLEASE SELECT THE RATE RECORD YOU WISH TO CREATE BILLS FOR";
			this.ToolTip1.SetToolTip(this.lblRateSelect, null);
			// 
			// lblSelectAccounts
			// 
			this.lblSelectAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.lblSelectAccounts.Location = new System.Drawing.Point(30, 30);
			this.lblSelectAccounts.Name = "lblSelectAccounts";
			this.lblSelectAccounts.Size = new System.Drawing.Size(474, 34);
			this.lblSelectAccounts.TabIndex = 0;
			this.lblSelectAccounts.Text = "IF THE LIST IS INACCURATE GO BACK TO BILL PREP AND RUN THAT STEP AGAIN FOR THE CO" + "RRECT ACCOUNTS";
			this.ToolTip1.SetToolTip(this.lblSelectAccounts, null);
			this.lblSelectAccounts.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(210, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(100, 48);
			this.cmdProcess.TabIndex = 0;
			this.cmdProcess.Text = "Process";
			this.ToolTip1.SetToolTip(this.cmdProcess, null);
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// frmBillCreate
			// 
			this.ClientSize = new System.Drawing.Size(521, 571);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBillCreate";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Create Bills";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmBillCreate_Load);
			this.Activated += new System.EventHandler(this.frmBillCreate_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBillCreate_KeyPress);
			this.Resize += new System.EventHandler(this.frmBillCreate_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBillInfo)).EndInit();
			this.fraBillInfo.ResumeLayout(false);
			this.fraBillInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraPerDiem)).EndInit();
			this.fraPerDiem.ResumeLayout(false);
			this.fraPerDiem.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPerDiem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFlat)).EndInit();
			this.fraFlat.ResumeLayout(false);
			this.fraFlat.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtFlatAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInterestStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcess;
	}
}
