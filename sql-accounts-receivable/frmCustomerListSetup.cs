﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomerListSetup.
	/// </summary>
	public partial class frmCustomerListSetup : BaseForm
	{
		public frmCustomerListSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomerListSetup InstancePtr
		{
			get
			{
				return (frmCustomerListSetup)Sys.GetInstance(typeof(frmCustomerListSetup));
			}
		}

		protected frmCustomerListSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/17/02
		// This screen will be used by towns to set up what and how
		// information is shown on the vendor list report
		// ********************************************************
		int SelectCol;
		int CodeCol;
		int DescriptionCol;
		string strBillTypeSQL;
		public string strSQL = "";

		private void chkDataEntryMessage_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDataEntryMessage.CheckState == Wisej.Web.CheckState.Checked)
			{
				fraDataEntryMessage.Enabled = true;
				cmbMessageNo.Enabled = true;
				Label5.Enabled = true;
			}
			else
			{
				fraDataEntryMessage.Enabled = false;
				cmbMessageNo.Enabled = false;
				Label5.Enabled = false;
			}
		}

		private void chkAlternateAddress_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAlternateAddress.CheckState == Wisej.Web.CheckState.Checked)
			{
				fraAlternateAddress.Enabled = true;
				cmbAltAddressYes.Enabled = true;
				Label4.Enabled = true;
			}
			else
			{
				fraAlternateAddress.Enabled = false;
				cmbAltAddressYes.Enabled = false;
				Label4.Enabled = false;
			}
		}

		private void frmCustomerListSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCustomerListSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomerListSetup.FillStyle	= 0;
			//frmCustomerListSetup.ScaleWidth	= 9045;
			//frmCustomerListSetup.ScaleHeight	= 7470;
			//frmCustomerListSetup.LinkTopic	= "Form2";
			//frmCustomerListSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsBillTypeInfo = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			SelectCol = 0;
			CodeCol = 1;
			DescriptionCol = 2;
			vsBillTypes.TextMatrix(0, SelectCol, "Select");
			vsBillTypes.TextMatrix(0, DescriptionCol, "Bill Type");
			vsBillTypes.ColWidth(SelectCol, FCConvert.ToInt32(vsBillTypes.WidthOriginal * 0.3));
			vsBillTypes.ColWidth(DescriptionCol, FCConvert.ToInt32(vsBillTypes.WidthOriginal * 0.65));
			vsBillTypes.ColHidden(CodeCol, true);
			vsBillTypes.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			rsBillTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE rTrim(TypeTitle) <> '' ORDER BY TypeTitle");
			if (rsBillTypeInfo.EndOfFile() != true && rsBillTypeInfo.BeginningOfFile() != true)
			{
				do
				{
					vsBillTypes.AddItem(0 + "\t" + Strings.Format(rsBillTypeInfo.Get_Fields_Int32("TypeCode"), "00") + "\t" + rsBillTypeInfo.Get_Fields_String("TypeTitle"));
					rsBillTypeInfo.MoveNext();
				}
				while (rsBillTypeInfo.EndOfFile() != true);
			}
			modGlobalFunctions.SetTRIOColors(this);
			cmbNumber.SelectedIndex = 0;
			cmbStatusAll.SelectedIndex = 0;
			cmbRange.SelectedIndex = 0;
			cmbSpecific.SelectedIndex = 0;
			cmbAltAddressYes.SelectedIndex = 0;
			cmbMessageNo.SelectedIndex = 0;
		}

		private void frmCustomerListSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			bool blnBillTypesSelected = false;
			int counter;
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkActive.CheckState == Wisej.Web.CheckState.Unchecked && chkDeleted.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 status before you may proceed", "Invalid Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (cmbRange.SelectedIndex == 1)
			{
				if (cboStart.SelectedIndex > cboEnd.SelectedIndex)
				{
					MessageBox.Show("This is not a valid range of vendors.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbSpecific.SelectedIndex == 1)
			{
				blnBillTypesSelected = false;
				for (counter = 1; counter <= vsBillTypes.Rows - 1; counter++)
				{
					if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(counter, SelectCol)) == true)
					{
						blnBillTypesSelected = true;
						break;
					}
				}
				if (!blnBillTypesSelected)
				{
					MessageBox.Show("You must select at least one bill type or choose the all option for bill types before you may continue", "Invalid Bill Type Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			strBillTypeSQL = "";
			if (cmbSpecific.SelectedIndex == 1)
			{
				CreateSQL();
			}
			if (strBillTypeSQL != "")
			{
				if (cmbRange.SelectedIndex == 0)
				{
					strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, PartyID FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL;
				}
				else
				{
					if (cmbNumber.SelectedIndex == 1)
					{
						strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, PartyID FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL + " AND CustomerMaster.CustomerID >= " + FCConvert.ToString(Conversion.Val(Strings.Left(cboStart.Text, 4))) + " AND CustomerMaster.CustomerID <= " + FCConvert.ToString(Conversion.Val(Strings.Left(cboEnd.Text, 4)));
					}
					else
					{
						strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, PartyID FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL;
					}
				}
			}
			else
			{
				if (cmbRange.SelectedIndex == 0)
				{
					strSQL = "SELECT DISTINCT CustomerID, PartyID FROM CustomerMaster";
				}
				else
				{
					if (cmbNumber.SelectedIndex == 1)
					{
						strSQL = "SELECT DISTINCT CustomerID, PartyID FROM CustomerMaster WHERE CustomerID >= " + FCConvert.ToString(Conversion.Val(Strings.Left(cboStart.Text, 4))) + " AND CustomerID <= " + FCConvert.ToString(Conversion.Val(Strings.Left(cboEnd.Text, 4)));
					}
					else
					{
						strSQL = "SELECT DISTINCT CustomerID, PartyID FROM CustomerMaster";
					}
				}
			}
			rsInfo.OpenRecordset(strSQL);
			if (cmbRange.SelectedIndex != 0 && cmbNumber.SelectedIndex != 1)
			{
				rsInfo.InsertName("PartyID", "", false, true, false, string.Empty, false, string.Empty, false, "FullName >= '" + Strings.Trim(Strings.Right(cboStart.Text, cboStart.Text.Length - 4)) + "' AND FullName <= '" + Strings.Trim(Strings.Right(cboEnd.Text, cboEnd.Text.Length - 4)) + "'");
			}
			rsInfo.MoveNext();
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (cmbStatusAll.SelectedIndex == 1)
				{
					if (chkActive.CheckState == Wisej.Web.CheckState.Checked && chkDeleted.CheckState == Wisej.Web.CheckState.Checked)
					{
						strSQL = "SELECT * FROM CustomerMaster WHERE CustomerID IN (";
					}
					else if (chkActive.CheckState == Wisej.Web.CheckState.Checked)
					{
						strSQL = "SELECT * FROM CustomerMaster WHERE Status = 'A' AND CustomerID IN (";
					}
					else
					{
						strSQL = "SELECT * FROM CustomerMaster WHERE Status = 'D' AND CustomerID IN (";
					}
				}
				else
				{
					strSQL = "SELECT * FROM CustomerMaster WHERE CustomerID IN (";
				}
				do
				{
					strSQL += rsInfo.Get_Fields_Int32("CustomerID") + ", ";
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ")";
			}
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				this.Hide();
				frmReportViewer.InstancePtr.Init(rptCustomerList.InstancePtr);
			}
			else
			{
				MessageBox.Show("No customers found that match this criteria.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			bool blnBillTypesSelected = false;
			int counter;
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkActive.CheckState == Wisej.Web.CheckState.Unchecked && chkDeleted.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 status before you may proceed", "Invalid Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (cmbRange.SelectedIndex == 1)
			{
				if (cboStart.SelectedIndex > cboEnd.SelectedIndex)
				{
					MessageBox.Show("This is not a valid range of vendors.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbSpecific.SelectedIndex == 1)
			{
				blnBillTypesSelected = false;
				for (counter = 1; counter <= vsBillTypes.Rows - 1; counter++)
				{
					if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(counter, SelectCol)) == true)
					{
						blnBillTypesSelected = true;
						break;
					}
				}
				if (!blnBillTypesSelected)
				{
					MessageBox.Show("You must select at least one bill type or choose the all option for bill types before you may continue", "Invalid Bill Type Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			strBillTypeSQL = "";
			if (cmbSpecific.SelectedIndex == 1)
			{
				CreateSQL();
			}
			if (strBillTypeSQL != "")
			{
				if (cmbRange.SelectedIndex == 0)
				{
					strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, Name FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL;
				}
				else
				{
					if (cmbNumber.SelectedIndex == 1)
					{
						strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, Name FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL + " AND CustomerMaster.CustomerID >= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmCustomerLabels.InstancePtr.cboStart.Text, 4))) + " AND CustomerMaster.CustomerID <= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmCustomerLabels.InstancePtr.cboEnd.Text, 4)));
					}
					else
					{
						strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, Name FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE " + strBillTypeSQL + " AND CustomerMaster.Name >= '" + frmCustomerLabels.InstancePtr.cboStart.Text + "' AND CustomerMaster.Name <= '" + frmCustomerLabels.InstancePtr.cboEnd.Text + "'";
					}
				}
			}
			else
			{
				if (cmbRange.SelectedIndex == 0)
				{
					strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, Name FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID";
				}
				else
				{
					if (cmbNumber.SelectedIndex == 1)
					{
						strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, Name FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE CustomerMaster.CustomerID >= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmCustomerLabels.InstancePtr.cboStart.Text, 4))) + " AND CustomerMaster.CustomerID <= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmCustomerLabels.InstancePtr.cboEnd.Text, 4)));
					}
					else
					{
						strSQL = "SELECT DISTINCT CustomerMaster.CustomerID as CustomerID, Name FROM CustomerMaster INNER JOIN CustomerBills ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE CustomerMaster.Name >= '" + frmCustomerLabels.InstancePtr.cboStart.Text + "' AND CustomerMaster.Name <= '" + frmCustomerLabels.InstancePtr.cboEnd.Text + "'";
					}
				}
			}
			if (cmbNumber.SelectedIndex == 0)
			{
				strSQL += " ORDER BY Name";
			}
			else
			{
				strSQL += " ORDER BY CustomerID";
			}
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (cmbStatusAll.SelectedIndex == 1)
				{
					if (chkActive.CheckState == Wisej.Web.CheckState.Checked && chkDeleted.CheckState == Wisej.Web.CheckState.Checked)
					{
						strSQL = "SELECT * FROM CustomerMaster WHERE CustomerID IN (";
					}
					else if (chkActive.CheckState == Wisej.Web.CheckState.Checked)
					{
						strSQL = "SELECT * FROM CustomerMaster WHERE Status = 'A' AND CustomerID IN (";
					}
					else
					{
						strSQL = "SELECT * FROM CustomerMaster WHERE Status = 'D' AND CustomerID IN (";
					}
				}
				else
				{
					strSQL = "SELECT * FROM CustomerMaster WHERE CustomerID IN (";
				}
				do
				{
					strSQL += rsInfo.Get_Fields_Int32("CustomerID") + ", ";
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ")";
			}
			if (cmbNumber.SelectedIndex == 0)
			{
				strSQL += " ORDER BY CustomerMaster.Name";
			}
			else
			{
				strSQL += " ORDER BY CustomerMaster.CustomerID";
			}
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				modDuplexPrinting.DuplexPrintReport(rptCustomerList.InstancePtr);
			}
			else
			{
				MessageBox.Show("No customers found that match this criteria.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbRange.SelectedIndex == 0)
			{
				cboStart.Clear();
				cboEnd.Clear();
				Label1.Enabled = false;
				Label2.Enabled = false;
				fraRange.Enabled = false;
			}
			else
			{
				clsDRWrapper rsCustomerInfo = new clsDRWrapper();
				fraRange.Enabled = true;
				Label1.Enabled = true;
				Label2.Enabled = true;
				cboStart.Clear();
				cboEnd.Clear();
				if (cmbNumber.SelectedIndex == 0)
				{
					rsCustomerInfo.OpenRecordset("SELECT * FROM CustomerMaster WHERE Status <> 'D' ORDER BY CustomerID");
					rsCustomerInfo.InsertName("PartyID", "", false, true);
				}
				else
				{
					rsCustomerInfo.OpenRecordset("SELECT * FROM CustomerMaster WHERE Status <> 'D'");
					rsCustomerInfo.InsertName("PartyID", "", false, true, false, string.Empty, false, "FullName DESC");
				}
				if (rsCustomerInfo.EndOfFile() != true && rsCustomerInfo.BeginningOfFile() != true)
				{
					do
					{
						cboStart.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsCustomerInfo.Get_Fields_Int32("CustomerID")), 4) + "  " + Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("FullName"))));
						cboEnd.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsCustomerInfo.Get_Fields_Int32("CustomerID")), 4) + "  " + Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("FullName"))));
						rsCustomerInfo.MoveNext();
					}
					while (rsCustomerInfo.EndOfFile() != true);
					cboStart.SelectedIndex = 0;
					cboEnd.SelectedIndex = 0;
				}
				cboStart.Focus();
			}
		}

		private void optSpecific_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbSpecific.SelectedIndex == 0)
			{
				int counter;
				for (counter = 1; counter <= vsBillTypes.Rows - 1; counter++)
				{
					vsBillTypes.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
				}
				fraSpecificBillTypes.Enabled = false;
			}
			else
			{
				fraSpecificBillTypes.Enabled = true;
			}
		}

		private void optStatusAll_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbStatusAll.SelectedIndex == 0)
			{
				chkActive.CheckState = Wisej.Web.CheckState.Unchecked;
				chkDeleted.CheckState = Wisej.Web.CheckState.Unchecked;
				chkActive.Enabled = false;
				chkDeleted.Enabled = false;
				fraStatus.Enabled = false;
			}
			else
			{
				fraStatus.Enabled = true;
				chkActive.Enabled = true;
				chkDeleted.Enabled = true;
			}
		}

		private void vsBillTypes_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsBillTypes.Row > 0)
			{
				if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol)) == true)
				{
					vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol, FCConvert.ToString(false));
				}
				else
				{
					vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsBillTypes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Space)
			{
				if (vsBillTypes.Row > 0)
				{
					if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol)) == true)
					{
						vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol, FCConvert.ToString(false));
					}
					else
					{
						vsBillTypes.TextMatrix(vsBillTypes.Row, SelectCol, FCConvert.ToString(true));
					}
				}
			}
		}

		private void CreateSQL()
		{
			int counter;
			strBillTypeSQL = "(";
			for (counter = 1; counter <= vsBillTypes.Rows - 1; counter++)
			{
				if (fecherFoundation.FCConvert.CBool(vsBillTypes.TextMatrix(counter, SelectCol)) == true)
				{
					strBillTypeSQL += "Type = " + vsBillTypes.TextMatrix(counter, CodeCol) + " OR ";
				}
			}
			strBillTypeSQL = Strings.Left(strBillTypeSQL, strBillTypeSQL.Length - 4) + ")";
		}
	}
}
