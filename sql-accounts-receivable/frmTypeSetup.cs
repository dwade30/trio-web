﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmTypeSetup.
	/// </summary>
	public partial class frmTypeSetup : BaseForm
	{
		public frmTypeSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkChanges = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkMandatory = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkChanges.AddControlArrayElement(chkChanges_0, 0);
			this.chkChanges.AddControlArrayElement(chkChanges_1, 1);
			this.chkChanges.AddControlArrayElement(chkChanges_2, 2);
			this.chkChanges.AddControlArrayElement(chkChanges_3, 3);
			this.chkMandatory.AddControlArrayElement(chkMandatory_3, 3);
			this.chkMandatory.AddControlArrayElement(chkMandatory_2, 2);
			this.chkMandatory.AddControlArrayElement(chkMandatory_1, 1);
			this.chkMandatory.AddControlArrayElement(chkMandatory_0, 0);
			//FC:FINAL:DSE #i486 Fix column width
			this.txtReceivableAccount.Columns[0].Width = 150;
			this.txtDefaultAccount.Columns[0].Width = 150;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTypeSetup InstancePtr
		{
			get
			{
				return (frmTypeSetup)Sys.GetInstance(typeof(frmTypeSetup));
			}
		}

		protected frmTypeSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/08/2004              *
		// ********************************************************
		bool boolDirty;
		// this will keep track of new codes being added and not saved
		bool boolLockType;
		bool boolLoaded;
		int intCurCode;
		bool boolLeftArrow;
		bool boolAccountBox;
		string strComboList = "";
		bool boolChangingRows;
		bool boolTRIOOverride;
		bool boolDoNotChangeType;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsReceivableGrid = new clsGridAccount();
		//private clsGridAccount vsDefaultGrid = new clsGridAccount();
		//private clsGridAccount vsDetailGrid = new clsGridAccount();
		//private clsGridAccount vsOverrideIntGrid = new clsGridAccount();
		//private clsGridAccount vsOverrideSalesTaxGrid = new clsGridAccount();
		private clsGridAccount vsReceivableGrid_AutoInitialized;

		private clsGridAccount vsReceivableGrid
		{
			get
			{
				if (vsReceivableGrid_AutoInitialized == null)
				{
					vsReceivableGrid_AutoInitialized = new clsGridAccount();
				}
				return vsReceivableGrid_AutoInitialized;
			}
			set
			{
				vsReceivableGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsDefaultGrid_AutoInitialized;

		private clsGridAccount vsDefaultGrid
		{
			get
			{
				if (vsDefaultGrid_AutoInitialized == null)
				{
					vsDefaultGrid_AutoInitialized = new clsGridAccount();
				}
				return vsDefaultGrid_AutoInitialized;
			}
			set
			{
				vsDefaultGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsDetailGrid_AutoInitialized;

		private clsGridAccount vsDetailGrid
		{
			get
			{
				if (vsDetailGrid_AutoInitialized == null)
				{
					vsDetailGrid_AutoInitialized = new clsGridAccount();
				}
				return vsDetailGrid_AutoInitialized;
			}
			set
			{
				vsDetailGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsOverrideIntGrid_AutoInitialized;

		private clsGridAccount vsOverrideIntGrid
		{
			get
			{
				if (vsOverrideIntGrid_AutoInitialized == null)
				{
					vsOverrideIntGrid_AutoInitialized = new clsGridAccount();
				}
				return vsOverrideIntGrid_AutoInitialized;
			}
			set
			{
				vsOverrideIntGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsOverrideSalesTaxGrid_AutoInitialized;

		private clsGridAccount vsOverrideSalesTaxGrid
		{
			get
			{
				if (vsOverrideSalesTaxGrid_AutoInitialized == null)
				{
					vsOverrideSalesTaxGrid_AutoInitialized = new clsGridAccount();
				}
				return vsOverrideSalesTaxGrid_AutoInitialized;
			}
			set
			{
				vsOverrideSalesTaxGrid_AutoInitialized = value;
			}
		}

		private void FormatGrid()
		{
			// sets all of the widths, datatypes and titles for the grid
			int Width = 0;
			vsBill.Cols = 8;
			vsBill.EditMaxLength = 20;
			Width = vsBill.WidthOriginal;
			vsBill.ColWidth(0, FCConvert.ToInt32(Width * 0.08));
			// Category Number
			vsBill.ColWidth(1, FCConvert.ToInt32(Width * 0.31));
			// Title
			vsBill.ColWidth(2, FCConvert.ToInt32(Width * 0.15));
			// Abbrev Title
			vsBill.ColWidth(3, FCConvert.ToInt32(Width * 0.19));
			// Account
			vsBill.ColWidth(4, FCConvert.ToInt32(Width * 0.08));
			// Year
			vsBill.ColWidth(5, FCConvert.ToInt32(Width * 0.19));
			// Default $
			// hidden column
			vsBill.ColWidth(6, 0);
			// Primary Key in table
			vsBill.ColHidden(6, true);
			vsBill.ColHidden(4, true);
			vsBill.ColWidth(7, FCConvert.ToInt32(Width * 0.08));
			// Default $
			// check box for the Year column
			vsBill.ColDataType(4, FCGrid.DataTypeSettings.flexDTBoolean);
			vsBill.ColDataType(7, FCGrid.DataTypeSettings.flexDTBoolean);
			// alignment
			//vsBill.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			 // default format for the Default Dollar column
            // .ColFormat(5) = "#,##0.00"
            // bold the header
            vsBill.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsBill.Cols - 1, true);
			vsBill.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsBill.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// sets the titles for the flexgrid
			vsBill.TextMatrix(0, 0, "Cat");
			vsBill.TextMatrix(0, 1, "Screen Title");
			vsBill.TextMatrix(0, 2, "Report Title");
			vsBill.TextMatrix(0, 3, "Account");
			vsBill.TextMatrix(0, 4, "Year");
			vsBill.TextMatrix(0, 5, "Default $");
			vsBill.TextMatrix(0, 7, "%");
			vsBill.ColHidden(7, true);
            //FC:FINAL:AM: set the alignment
            vsBill.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //FC:FINAL:BSE: #2128 align header and cols
            vsBill.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBill.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBill.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBill.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBill.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBill.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);

            // this will make two cols and the second col has a length of 0
            cmbCode.Cols = 2;
			cmbCode.ColWidth(1, 0);
		}

		private void cboFrequencyCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int counter;
			cboFrequencyFirstMonth.Clear();
			for (counter = 0; counter <= 3; counter++)
			{
				chkChanges[FCConvert.ToInt16(counter)].Enabled = true;
			}
			if (Strings.Left(cboFrequencyCode.Text, 1) == "Y")
			{
				for (counter = 1; counter <= 12; counter++)
				{
					cboFrequencyFirstMonth.AddItem(Strings.Format(counter, "00") + "  -  " + modBudgetaryAccounting.MonthCalc(counter));
				}
			}
			else if (Strings.Left(cboFrequencyCode.Text, 1) == "M")
			{
				cboFrequencyFirstMonth.AddItem("00  -  Every Month");
			}
			else if (Strings.Left(cboFrequencyCode.Text, 1) == "D")
			{
				cboFrequencyFirstMonth.AddItem("00  -  On Demand");
				for (counter = 0; counter <= 3; counter++)
				{
					chkChanges[FCConvert.ToInt16(counter)].CheckState = Wisej.Web.CheckState.Checked;
					chkChanges[FCConvert.ToInt16(counter)].Enabled = false;
				}
			}
			else if (Strings.Left(cboFrequencyCode.Text, 1) == "Q")
			{
				for (counter = 1; counter <= 3; counter++)
				{
					cboFrequencyFirstMonth.AddItem(Strings.Format(counter, "00") + "  -  " + modBudgetaryAccounting.MonthCalc(counter));
				}
			}
			else if (Strings.Left(cboFrequencyCode.Text, 1) == "B")
			{
				for (counter = 1; counter <= 2; counter++)
				{
					cboFrequencyFirstMonth.AddItem(Strings.Format(counter, "00") + "  -  " + modBudgetaryAccounting.MonthCalc(counter));
				}
			}
			else if (Strings.Left(cboFrequencyCode.Text, 1) == "S")
			{
				for (counter = 1; counter <= 6; counter++)
				{
					cboFrequencyFirstMonth.AddItem(Strings.Format(counter, "00") + "  -  " + modBudgetaryAccounting.MonthCalc(counter));
				}
			}
			if (cboFrequencyFirstMonth.Items.Count > 0)
			{
				cboFrequencyFirstMonth.SelectedIndex = 0;
			}
		}

		public void cboFrequencyCode_Click()
		{
			cboFrequencyCode_SelectedIndexChanged(cboFrequencyCode, new System.EventArgs());
		}

		private void cboFrequencyCode_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboFrequencyCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboFrequencyFirstMonth_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboFrequencyFirstMonth.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboInterestMethod_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (Strings.Mid(cboInterestMethod.Text, 2, 1) == "F")
			{
				fraFlat.Visible = true;
				fraPerDiem.Visible = false;
				txtPerDiem.Text = Strings.Format(modGlobal.Statics.dblDefaultPerDiem, "0.00");
			}
			else
			{
				fraPerDiem.Visible = true;
				fraFlat.Visible = false;
				txtFlatAmount.Text = Strings.Format(modGlobal.Statics.curDefaultFlatRate, "#,##0.00");
			}
		}

		private void cboInterestMethod_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboInterestMethod.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 250, 0);
		}

		private void cmbCode_ChangeEdit(object sender, System.EventArgs e)
		{
			if (cmbCode.IsCurrentCellInEditMode)
			{
				ProcessType();
				// If RestrictedCode Then
				// cmbCopies.SetFocus
				// Else
				// txtTitle.SetFocus
				// End If
				cmbCode.Focus();
				cmbCode.EditCell();
			}
		}

		private void cmbCode_ClickEvent(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_ComboDropDown(object sender, System.EventArgs e)
		{
			cmbCode.ComboList = strComboList;
		}

		private void cmbCode_DblClick(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void cmbCode_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						ProcessType();
						if (txtTitle.Enabled && txtTitle.Visible)
						{
							txtTitle.Focus();
						}
						else if (cmbType.Visible && cmbType.Enabled)
						{
							cmbType.Focus();
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCode_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						ProcessType();
						if (txtTitle.Enabled && txtTitle.Visible)
						{
							txtTitle.Focus();
						}
						else if (cmbType.Visible && cmbType.Enabled)
						{
							cmbType.Focus();
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			ProcessType();
		}

		private void cmbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolDoNotChangeType)
			{
				ProcessType();
			}
		}

		private void cmbType_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbType.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbType_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will force the combobox to open with a spacebar
						if (modAPIsConst.SendMessageByNum(cmbType.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbType.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			// this goes back to the original state where the user has to choose the type
			btnFileBack.Enabled = true;
			btnFileForward.Enabled = true;
			txtCode.Text = "";
			ShowFrame(fraReceiptList);
			fraNewType.Visible = false;
			if (cmbType.Visible && cmbType.Enabled)
			{
				cmbType.Focus();
			}
			// cmbCode.SetFocus
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdCancel_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void cmdNewCode_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			string strCode;
			clsDRWrapper rsCh = new clsDRWrapper();
			strCode = txtCode.Text;
			btnFileBack.Enabled = true;
			btnFileForward.Enabled = true;
			if (Conversion.Val(strCode) > 0 && Conversion.Val(strCode) < 1000)
			{
				//switch (Conversion.Val(strCode)) {
				//default: {
				rsCh.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(Conversion.Val(strCode)));
				if (rsCh.EndOfFile() != true && rsCh.BeginningOfFile() != true)
				{
					// if there is already a match
					MessageBox.Show("The code " + strCode + " is already in use.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					// this number is available
					intTemp = MessageBox.Show("Are you sure that you want to create the Code: " + Strings.Left(strCode, 3) + "?", "New Code", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intTemp == DialogResult.Yes)
					{
						// yes
						CreateNewType();
					}
				}
				//break;
				//}
				//} //end switch
			}
			else
			{
				MessageBox.Show("Please enter a valid Type Code value. (001 - 999)", "Type Code Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtCode.SelectionStart = 0;
				txtCode.SelectionLength = strCode.Length;
				txtCode.Focus();
			}
			if (txtTitle.Enabled && txtTitle.Visible)
			{
				txtTitle.Focus();
			}
			else if (cmbCode.Enabled && cmbCode.Visible)
			{
				cmbCode.Focus();
			}
		}

		public void cmdNewCode_Click()
		{
			cmdNewCode_Click(cmdNewCode, new System.EventArgs());
		}

		private void CreateNewType()
		{
			// this will create a new code
			int I;
			int intCode;
			clsDRWrapper rsDefaults = new clsDRWrapper();
			// boolDirty = True
			clsDRWrapper rsSave = new clsDRWrapper();
			intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(txtCode.Text)));
			//Application.DoEvents();
			// create a new record with the correct code
			rsSave.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE ID = 0");
			rsSave.AddNew();
			rsSave.Set_Fields("TypeCode", FCConvert.ToInt16(intCode));
			rsSave.Set_Fields("FrequencyCode", "Y");
			rsSave.Set_Fields("FirstMonth", 1);
			rsDefaults.OpenRecordset("SELECT * FROM Customize");
			if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
			{
				if (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("DefaultInterestMethod"))) != "")
				{
					rsSave.Set_Fields("InterestMethod", rsDefaults.Get_Fields_String("DefaultInterestMethod"));
				}
				else
				{
					rsSave.Set_Fields("InterestMethod", "DF");
				}
				rsSave.Set_Fields("PayeeID", FCConvert.ToString(Conversion.Val(rsDefaults.Get_Fields_Int32("DefaultPayeeID"))));
			}
			else
			{
				rsSave.Set_Fields("InterestMethod", "DF");
				rsSave.Set_Fields("PayeeID", 0);
			}
			if (Strings.Right(FCConvert.ToString(rsSave.Get_Fields_String("InterestMethod")), 1) == "F")
			{
				rsSave.Set_Fields("FlatAmount", modGlobal.Statics.curDefaultFlatRate);
			}
			else
			{
				rsSave.Set_Fields("PerDiemRate", modGlobal.Statics.dblDefaultPerDiem);
			}
			rsSave.Set_Fields("InterestStartDate", modGlobal.Statics.intDefaultInterestStartDate);
			rsSave.Update();
			// clear the code combobox and refresh its list
			// cmbCode.ComboList = ""
			//Application.DoEvents();
			FillCodeCombo();
			// cmbCode.ComboIndex = 0
			ShowFrame(fraReceiptList);
			// cmbCode.Select 0, 1
			// cmbCode.Select 0, 0
			cmbCode.Refresh();
			// clear the new code textbox and any info in the acct textbox
			txtCode.Text = "";
			// txtAcct(6).Text = ""
			// txtAcct(1).Text = ""
			// txtAcct(2).Text = ""
			// txtAcct(3).Text = ""
			// txtAcct(4).Text = ""
			// txtAcct(5).Text = ""
			//Application.DoEvents();
			// choose the new code
			for (I = 0; I <= cmbType.Items.Count - 1; I++)
			{
				if (intCode == Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3)))
				{
					cmbType.SelectedIndex = I;
					break;
				}
			}
			// cmbCode.ComboList = strComboList
			// cmbCode.EditCell
			// cmbCode.TextMatrix(0, 0) = intCode
			// 
			// cmbCode.ComboIndex = FindNextComboIndex(intCode)
			ShowType(intCode);
			fraNewType.Visible = false;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short FindNextComboIndex(ref short intCD)
		{
			short FindNextComboIndex = 0;
			// this will return the combo index on the next type
			int intCT;
			FindNextComboIndex = 0;
			for (intCT = 0; intCT <= cmbCode.ComboCount - 1; intCT++)
			{
				if (Conversion.Val(cmbCode.ComboItem(intCT)) == intCD)
				{
					FindNextComboIndex = FCConvert.ToInt16(intCT);
					return FindNextComboIndex;
				}
			}
			return FindNextComboIndex;
		}

		private void ChangeTypeCode()
		{
			// this sub will save the current information under a new code and delete the old one
			int I;
			clsDRWrapper rsSave = new clsDRWrapper();
			clsDRWrapper rsRb = new clsDRWrapper();
			rsSave.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE ID = 0");
			rsSave.AddNew();
			rsSave.Set_Fields("TypeCode", FCConvert.ToString(Conversion.Val(Strings.Left(txtCode.Text, 3))));
			rsSave.Set_Fields("TypeTitle", Strings.Trim(txtTitle.Text));
			rsSave.Set_Fields("Reference", Strings.Trim(txtRefTitle.Text));
			rsSave.Set_Fields("Control1", Strings.Trim(txtCTRL1Title.Text));
			rsSave.Set_Fields("Control2", Strings.Trim(txtCTRL2Title.Text));
			rsSave.Set_Fields("Control3", Strings.Trim(txtCTRL3Title.Text));
			rsSave.Set_Fields("InterestMethod", cboInterestMethod.Text);
			if (cboPayee.SelectedIndex >= 0)
			{
				rsSave.Set_Fields("PayeeID", cboPayee.ItemData(cboPayee.SelectedIndex));
			}
			else
			{
				rsSave.Set_Fields("PayeeID", 0);
			}
			if (Strings.Mid(cboInterestMethod.Text, 2, 1) == "F")
			{
				rsSave.Set_Fields("FlatAmount", FCConvert.ToDecimal(txtFlatAmount.Text));
				rsSave.Set_Fields("PerDiemRate", 0);
			}
			else
			{
				rsSave.Set_Fields("FlatAmount", 0);
				rsSave.Set_Fields("PerDiemRate", FCConvert.ToDouble(txtPerDiem.Text));
			}
			if (cboInterestStartDate.SelectedIndex == 0)
			{
				rsSave.Set_Fields("InterestStartDate", 30);
			}
			else if (cboInterestStartDate.SelectedIndex == 1)
			{
				rsSave.Set_Fields("InterestStartDate", 60);
			}
			else if (cboInterestStartDate.SelectedIndex == 2)
			{
				rsSave.Set_Fields("InterestStartDate", 90);
			}
			rsSave.Set_Fields("FrequencyCode", Strings.Left(cboFrequencyCode.Text, 1));
			rsSave.Set_Fields("FirstMonth", FCConvert.ToString(Conversion.Val(Strings.Left(cboFrequencyFirstMonth.Text, 2))));
			rsSave.Set_Fields("Title1", vsBill.TextMatrix(1, 1));
			rsSave.Set_Fields("Title1Abbrev", vsBill.TextMatrix(1, 2));
			rsSave.Set_Fields("Account1", vsBill.TextMatrix(1, 3));
			rsSave.Set_Fields("DefaultAmount1", vsBill.TextMatrix(1, 5));
			//FC:FINAL:DSE #i470 Convert numeric string to boolean
			//rsSave.Set_Fields("Year1", vsBill.TextMatrix(1, 4));
			rsSave.Set_Fields("Year1", FCConvert.CBool(vsBill.TextMatrix(1, 4)));
			if (Strings.Left(vsBill.TextMatrix(1, 3), 1) != "G")
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 4, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title2", vsBill.TextMatrix(2, 1));
			rsSave.Set_Fields("Title2Abbrev", vsBill.TextMatrix(2, 2));
			rsSave.Set_Fields("Account2", vsBill.TextMatrix(2, 3));
			rsSave.Set_Fields("DefaultAmount2", vsBill.TextMatrix(2, 5));
			rsSave.Set_Fields("Year2", vsBill.TextMatrix(2, 4));
			if (Strings.Left(vsBill.TextMatrix(2, 3), 1) != "G")
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 4, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title3", vsBill.TextMatrix(3, 1));
			rsSave.Set_Fields("Title3Abbrev", vsBill.TextMatrix(3, 2));
			rsSave.Set_Fields("Account3", vsBill.TextMatrix(3, 3));
			rsSave.Set_Fields("DefaultAmount3", vsBill.TextMatrix(3, 5));
			rsSave.Set_Fields("Year3", vsBill.TextMatrix(3, 4));
			if (Strings.Left(vsBill.TextMatrix(3, 3), 1) != "G")
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 4, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title4", vsBill.TextMatrix(4, 1));
			rsSave.Set_Fields("Title4Abbrev", vsBill.TextMatrix(4, 2));
			rsSave.Set_Fields("Account4", vsBill.TextMatrix(4, 3));
			rsSave.Set_Fields("DefaultAmount4", vsBill.TextMatrix(4, 5));
			rsSave.Set_Fields("Year4", vsBill.TextMatrix(4, 4));
			if (Strings.Left(vsBill.TextMatrix(4, 3), 1) != "G")
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, 4, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title5", vsBill.TextMatrix(5, 1));
			rsSave.Set_Fields("Title5Abbrev", vsBill.TextMatrix(5, 2));
			rsSave.Set_Fields("Account5", vsBill.TextMatrix(5, 3));
			rsSave.Set_Fields("DefaultAmount5", vsBill.TextMatrix(5, 5));
			rsSave.Set_Fields("Year5", vsBill.TextMatrix(5, 4));
			if (Strings.Left(vsBill.TextMatrix(5, 3), 1) != "G")
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, 4, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title6", vsBill.TextMatrix(6, 1));
			rsSave.Set_Fields("Title6Abbrev", vsBill.TextMatrix(6, 2));
			rsSave.Set_Fields("Account6", vsBill.TextMatrix(6, 3));
			rsSave.Set_Fields("DefaultAmount6", vsBill.TextMatrix(6, 5));
			rsSave.Set_Fields("Year6", vsBill.TextMatrix(6, 4));
			if (Strings.Left(vsBill.TextMatrix(6, 3), 1) != "G")
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 7, 4, Information.RGB(255, 255, 255));
			}
			rsSave.Update();
			// delete the old code
			rsSave.Execute("DELETE FROM Type WHERE TypeCode = " + FCConvert.ToString(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))), "TWAR0000.vb1");
			// clear the code combobox and refresh its list
			cmbCode.Clear();
			FillCodeCombo();
			// choose the new code
			for (I = 0; I <= cmbType.Items.Count - 1; I++)
			{
				if (Conversion.Val(txtCode.Text) == Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3)))
				{
					cmbType.SelectedIndex = I;
					break;
				}
			}
			// clear the new code textbox
			txtCode.Text = "";
			ShowFrame(fraReceiptList);
			fraNewType.Visible = false;
		}

		private void cmdNewCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void frmTypeSetup_Activated(object sender, System.EventArgs e)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDefaults = new clsDRWrapper();
				intErr = 1;
				if (modGlobal.FormExist(this))
					return;
				if (!boolLoaded)
				{
					intErr = 2;
					boolDirty = false;
					intErr = 4;
					//fraReceiptList.Left = FCConvert.ToInt32((this.Width - fraReceiptList.Width) / 2.0);
					//fraReceiptList.Top = FCConvert.ToInt32((this.Height - fraReceiptList.Height) / 3.0);
					fraReceiptList.Visible = true;
					intErr = 6;
					FormatGrid();
					intErr = 9;
					FillCodeCombo();
					rsDefaults.OpenRecordset("SELECT * FROM Customize");
					if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
					{
						if (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("DefaultInterestMethod"))) != "")
						{
							SetInterestMethodCombo_2(rsDefaults.Get_Fields_String("DefaultInterestMethod"));
						}
						else
						{
							SetInterestMethodCombo_2("DF");
						}
					}
					intErr = 10;
					// this should show the first type
					if (cmbType.Items.Count > 0)
					{
						cmbType.SelectedIndex = 0;
					}
					if (cmbType.Visible && cmbType.Enabled)
					{
						cmbType.Focus();
					}
					boolLoaded = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Activate - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmTypeSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: lngTemp As int	OnWrite(string)
			int lngTemp = 0;
			int intIndex;
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Escape:
					{
						// exit
						KeyCode = (Keys)0;
						if (fraNewType.Visible)
						{
							cmdCancel_Click();
						}
						else
						{
							Close();
							return;
						}
						break;
					}
				case Keys.F9:
					{
						if (Shift == 3)
						{
							// this will be CTRL-SHIFT-F1
							KeyCode = (Keys)0;
							lngTemp = FCConvert.ToInt32(Interaction.InputBox("Please enter the code supplied to you by a TRIO staff member", "Save Override Code", null));
							if (modModDayCode.CheckWinModCode(lngTemp, "AR"))
							{
								boolTRIOOverride = true;
								MessageBox.Show("TRIO Save override code has been activated.", "Override Activated", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							else
							{
								boolTRIOOverride = false;
								MessageBox.Show("TRIO Save override code has not been activated.", "Override Not Activated", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							// ElseIf (Me.ActiveControl.Name = "vsBill" And vsBill.Row > 0 And vsBill.Col = 3) Or Me.ActiveControl.Name = "txtDefaultAccount" Or Me.ActiveControl.Name = "txtReceivableAccount" Then
							// KeyCode = 0
							// 
							// frmLoadValidAccounts.Show vbModal, MDIParent
							// 
							// If gstrPassAccountFromForm = "" Then
							// 
							// Else
							// for
							// If Me.ActiveControl.Name = "vsBill" Then
							// vsBill.EditText = gstrPassAccountFromForm
							// vsBill.TextMatrix(vsBill.Row, vsBill.Col) = gstrPassAccountFromForm
							// ElseIf Me.ActiveControl.Name = "txtDefaultAccount" Then
							// txtDefaultAccount.TextMatrix(0, 0) = gstrPassAccountFromForm
							// Else
							// txtReceivableAccount.TextMatrix(0, 0) = gstrPassAccountFromForm
							// End If
							// End If
						}
						break;
					}
			}
			//end switch
			if (Strings.UCase(this.ActiveControl.GetName()) == Strings.UCase("vsBill"))
			{
				if (vsBill.Col == 3 && vsBill.Row > 0)
				{
					modNewAccountBox.CheckFormKeyDown(vsBill, vsBill.Row, vsBill.Col, KeyCode, Shift, vsBill.EditSelStart, vsBill.EditText, vsBill.EditSelLength);
				}
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "TXTRECEIVABLEACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(txtReceivableAccount, txtReceivableAccount.Row, txtReceivableAccount.Col, KeyCode, Shift, txtReceivableAccount.EditSelStart, txtReceivableAccount.EditText, txtReceivableAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "TXTDEFAULTACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(txtDefaultAccount, txtDefaultAccount.Row, txtDefaultAccount.Col, KeyCode, Shift, txtDefaultAccount.EditSelStart, txtDefaultAccount.EditText, txtDefaultAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "TXTOVERRIDEINTACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(txtOverrideIntAccount, txtOverrideIntAccount.Row, txtOverrideIntAccount.Col, KeyCode, Shift, txtOverrideIntAccount.EditSelStart, txtOverrideIntAccount.EditText, txtOverrideIntAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "TXTOVERRIDESALESTAXACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(txtOverrideSalesTaxAccount, txtOverrideSalesTaxAccount.Row, txtOverrideSalesTaxAccount.Col, KeyCode, Shift, txtOverrideSalesTaxAccount.EditSelStart, txtOverrideSalesTaxAccount.EditText, txtOverrideSalesTaxAccount.EditSelLength);
			}
		}

		private void frmTypeSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTypeSetup.FillStyle	= 0;
			//frmTypeSetup.ScaleWidth	= 9045;
			//frmTypeSetup.ScaleHeight	= 7320;
			//frmTypeSetup.LinkTopic	= "Form1";
			//End Unmaped Properties
			// this will size the form to the MDIParent
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			vsBill.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			if (modGlobalConstants.Statics.gboolBD)
			{
				modValidateAccount.SetBDFormats();
			}
			vsBill.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			modNewAccountBox.GetFormats();
			FillDefaultInterestStartDateOptions();
			FillPayeeCombo();
			vsReceivableGrid.GRID7Light = txtReceivableAccount;
			vsDefaultGrid.GRID7Light = txtDefaultAccount;
			vsDetailGrid.GRID7Light = vsBill;
			vsOverrideIntGrid.GRID7Light = txtOverrideIntAccount;
			vsOverrideSalesTaxGrid.GRID7Light = txtOverrideSalesTaxAccount;
			if (modGlobalConstants.Statics.gboolBD)
			{
				vsDefaultGrid.DefaultAccountType = "R";
				vsReceivableGrid.DefaultAccountType = "G";
				vsDetailGrid.DefaultAccountType = "R";
				vsOverrideIntGrid.DefaultAccountType = "R";
				vsOverrideSalesTaxGrid.DefaultAccountType = "R";
			}
			else
			{
				vsDefaultGrid.DefaultAccountType = "M";
				vsReceivableGrid.DefaultAccountType = "M";
				vsDetailGrid.DefaultAccountType = "M";
				vsOverrideIntGrid.DefaultAccountType = "M";
				vsOverrideSalesTaxGrid.DefaultAccountType = "M";
			}
			vsReceivableGrid.AccountCol = -1;
			vsDefaultGrid.AccountCol = -1;
			vsOverrideIntGrid.AccountCol = -1;
			vsOverrideSalesTaxGrid.AccountCol = -1;
			vsDetailGrid.AccountCol = 3;
			//FC:FINAL:SBE - set allowed keys for client side check. KeyPress on server side does not support key restrictions
			vsBill.ColAllowedKeys(5, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
		}
		// vbPorter upgrade warning: intTypeCode As short	OnWrite(int, double)
		private void ShowType_2(int intTypeCode)
		{
			ShowType(intTypeCode);
		}

		private void ShowType(int intTypeCode = 0)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsType = new clsDRWrapper();
				int intCT;
				intErr = 0;
				rsType.DefaultDB = "TWAR0000.vb1";
				intErr = 1;
				if (intTypeCode == 0)
				{
					// new type or blank to start
					intErr = 2;
					vsBill.Rows = 1;
					// clears grid
					vsBill.Rows = 7;
					txtRefTitle.Text = "";
					// clears the default titles
					txtCTRL1Title.Text = "";
					txtCTRL2Title.Text = "";
					txtCTRL3Title.Text = "";
					chkMandatory[0].CheckState = Wisej.Web.CheckState.Unchecked;
					chkMandatory[1].CheckState = Wisej.Web.CheckState.Unchecked;
					chkMandatory[2].CheckState = Wisej.Web.CheckState.Unchecked;
					chkMandatory[3].CheckState = Wisej.Web.CheckState.Unchecked;
					chkChanges[0].CheckState = Wisej.Web.CheckState.Unchecked;
					chkChanges[1].CheckState = Wisej.Web.CheckState.Unchecked;
					chkChanges[2].CheckState = Wisej.Web.CheckState.Unchecked;
					chkChanges[3].CheckState = Wisej.Web.CheckState.Unchecked;
					chkSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
					txtTitle.Text = "";
					boolLockType = false;
					lblAccountTitle.Text = "";
					ColorGrid_2(false);
				}
				else
				{
					// edit or examine a code that is already created
					intErr = 3;
					lblAccountTitle.Text = "";
					rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intTypeCode));
					if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
					{
						intErr = 4;
						txtTitle.Text = FCConvert.ToString(rsType.Get_Fields_String("TypeTitle"));
						intErr = 8;
						txtRefTitle.Text = FCConvert.ToString(rsType.Get_Fields_String("Reference"));
						txtCTRL1Title.Text = FCConvert.ToString(rsType.Get_Fields_String("Control1"));
						txtCTRL2Title.Text = FCConvert.ToString(rsType.Get_Fields_String("Control2"));
						txtCTRL3Title.Text = FCConvert.ToString(rsType.Get_Fields_String("Control3"));
						SetPayeeCombo_2(FCConvert.ToInt32(Conversion.Val(rsType.Get_Fields_Int32("PayeeID"))));
						SetInterestMethodCombo_2(rsType.Get_Fields_String("InterestMethod"));
						if (Strings.Mid(cboInterestMethod.Text, 2, 1) == "F")
						{
							txtFlatAmount.Text = Strings.Format(FCConvert.ToString(Conversion.Val(rsType.Get_Fields_Decimal("FlatAmount"))), "#,##0.00");
							txtPerDiem.Text = "0.00";
						}
						else
						{
							txtPerDiem.Text = Strings.Format(FCConvert.ToString(Conversion.Val(rsType.Get_Fields_Double("PerDiemRate")) * 100), "0.00");
							txtFlatAmount.Text = "0.00";
						}
						SetInterestStartDateCombo_2(FCConvert.ToInt16(rsType.Get_Fields_Int32("InterestStartDate")));
						SetFrequencyCodeCombo_2(FCConvert.ToString(rsType.Get_Fields_String("FrequencyCode")));
						cboFrequencyCode_Click();
						SetFrequencyFirstMonthCombo_2(FCConvert.ToString(rsType.Get_Fields_Int32("FirstMonth")));
						if (rsType.Get_Fields_Boolean("SalesTax") == true)
						{
							chkSalesTax.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 9;
						if (rsType.Get_Fields_Boolean("ReferenceMandatory") == true)
						{
							chkMandatory[0].CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkMandatory[0].CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 10;
						if (rsType.Get_Fields_Boolean("Control1Mandatory") == true)
						{
							chkMandatory[1].CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkMandatory[1].CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 11;
						if (rsType.Get_Fields_Boolean("Control2Mandatory") == true)
						{
							chkMandatory[2].CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkMandatory[2].CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 12;
						if (rsType.Get_Fields_Boolean("Control3Mandatory") == true)
						{
							chkMandatory[3].CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkMandatory[3].CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (rsType.Get_Fields_Boolean("ReferenceChanges") == true)
						{
							chkChanges[0].CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkChanges[0].CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (rsType.Get_Fields_Boolean("Control1Changes") == true)
						{
							chkChanges[1].CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkChanges[1].CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (rsType.Get_Fields_Boolean("Control2Changes") == true)
						{
							chkChanges[2].CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkChanges[2].CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (rsType.Get_Fields_Boolean("Control3Changes") == true)
						{
							chkChanges[3].CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkChanges[3].CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 16;
						vsBill.TextMatrix(1, 0, FCConvert.ToString(1));
						vsBill.TextMatrix(1, 1, FCConvert.ToString(rsType.Get_Fields_String("Title1")));
						vsBill.TextMatrix(1, 2, FCConvert.ToString(rsType.Get_Fields_String("Title1Abbrev")));
						vsBill.TextMatrix(1, 3, FCConvert.ToString(rsType.Get_Fields_String("Account1")));
						vsBill.TextMatrix(1, 4, FCConvert.ToString(rsType.Get_Fields_Boolean("Year1")));
						vsBill.TextMatrix(1, 5, Strings.Format(rsType.Get_Fields_Double("DefaultAmount1"), "#,##0.00"));
						intErr = 18;
						if (Strings.Left(vsBill.TextMatrix(1, 3), 1) != "G")
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 4, Information.RGB(255, 255, 255));
						}
						intErr = 19;
						vsBill.TextMatrix(2, 0, FCConvert.ToString(2));
						vsBill.TextMatrix(2, 1, FCConvert.ToString(rsType.Get_Fields_String("Title2")));
						vsBill.TextMatrix(2, 2, FCConvert.ToString(rsType.Get_Fields_String("Title2Abbrev")));
						vsBill.TextMatrix(2, 3, FCConvert.ToString(rsType.Get_Fields_String("Account2")));
						vsBill.TextMatrix(2, 4, FCConvert.ToString(rsType.Get_Fields_Boolean("Year2")));
						vsBill.TextMatrix(2, 5, Strings.Format(rsType.Get_Fields_Double("DefaultAmount2"), "#,##0.00"));
						intErr = 21;
						if (Strings.Left(vsBill.TextMatrix(2, 3), 1) != "G")
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 4, Information.RGB(255, 255, 255));
						}
						intErr = 22;
						vsBill.TextMatrix(3, 0, FCConvert.ToString(3));
						vsBill.TextMatrix(3, 1, FCConvert.ToString(rsType.Get_Fields_String("Title3")));
						vsBill.TextMatrix(3, 2, FCConvert.ToString(rsType.Get_Fields_String("Title3Abbrev")));
						vsBill.TextMatrix(3, 3, FCConvert.ToString(rsType.Get_Fields_String("Account3")));
						vsBill.TextMatrix(3, 4, FCConvert.ToString(rsType.Get_Fields_Boolean("Year3")));
						vsBill.TextMatrix(3, 5, Strings.Format(rsType.Get_Fields_Double("DefaultAmount3"), "#,##0.00"));
						intErr = 24;
						if (Strings.Left(vsBill.TextMatrix(3, 3), 1) != "G")
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 4, Information.RGB(255, 255, 255));
						}
						intErr = 25;
						vsBill.TextMatrix(4, 0, FCConvert.ToString(4));
						vsBill.TextMatrix(4, 1, FCConvert.ToString(rsType.Get_Fields_String("Title4")));
						vsBill.TextMatrix(4, 2, FCConvert.ToString(rsType.Get_Fields_String("Title4Abbrev")));
						vsBill.TextMatrix(4, 3, FCConvert.ToString(rsType.Get_Fields_String("Account4")));
						vsBill.TextMatrix(4, 4, FCConvert.ToString(rsType.Get_Fields_Boolean("Year4")));
						vsBill.TextMatrix(4, 5, Strings.Format(rsType.Get_Fields_Double("DefaultAmount4"), "#,##0.00"));
						intErr = 27;
						if (Strings.Left(vsBill.TextMatrix(4, 3), 1) != "G")
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, 4, Information.RGB(255, 255, 255));
						}
						intErr = 28;
						vsBill.TextMatrix(5, 0, FCConvert.ToString(5));
						vsBill.TextMatrix(5, 1, FCConvert.ToString(rsType.Get_Fields_String("Title5")));
						vsBill.TextMatrix(5, 2, FCConvert.ToString(rsType.Get_Fields_String("Title5Abbrev")));
						vsBill.TextMatrix(5, 3, FCConvert.ToString(rsType.Get_Fields_String("Account5")));
						vsBill.TextMatrix(5, 4, FCConvert.ToString(rsType.Get_Fields_Boolean("Year5")));
						vsBill.TextMatrix(5, 5, Strings.Format(rsType.Get_Fields_Double("DefaultAmount5"), "#,##0.00"));
						intErr = 30;
						if (Strings.Left(vsBill.TextMatrix(5, 3), 1) != "G")
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, 4, Information.RGB(255, 255, 255));
						}
						intErr = 31;
						vsBill.TextMatrix(6, 0, FCConvert.ToString(6));
						vsBill.TextMatrix(6, 1, FCConvert.ToString(rsType.Get_Fields_String("Title6")));
						vsBill.TextMatrix(6, 2, FCConvert.ToString(rsType.Get_Fields_String("Title6Abbrev")));
						vsBill.TextMatrix(6, 3, FCConvert.ToString(rsType.Get_Fields_String("Account6")));
						vsBill.TextMatrix(6, 4, FCConvert.ToString(rsType.Get_Fields_Boolean("Year6")));
						vsBill.TextMatrix(6, 5, Strings.Format(rsType.Get_Fields_Double("DefaultAmount6"), "#,##0.00"));
						intErr = 33;
						if (Strings.Left(vsBill.TextMatrix(6, 3), 1) != "G")
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 4, Information.RGB(255, 255, 255));
						}
						intErr = 34;
						if (modGlobalConstants.Statics.gboolBD)
						{
							txtDefaultAccount.TextMatrix(0, 0, FCConvert.ToString(rsType.Get_Fields_String("DefaultAccount")));
							txtReceivableAccount.TextMatrix(0, 0, FCConvert.ToString(rsType.Get_Fields_String("ReceivableAccount")));
							txtOverrideIntAccount.TextMatrix(0, 0, FCConvert.ToString(rsType.Get_Fields_String("OverrideIntAccount")));
							txtOverrideSalesTaxAccount.TextMatrix(0, 0, FCConvert.ToString(rsType.Get_Fields_String("OverrideTaxAccount")));
							txtDefaultAccount.Enabled = true;
							txtOverrideIntAccount.Enabled = true;
							txtOverrideSalesTaxAccount.Enabled = true;
							Label16.Enabled = true;
							Label17.Enabled = true;
							txtReceivableAccount.Enabled = true;
							Label4.Enabled = true;
							lblDefaultAccount.Enabled = true;
						}
						else
						{
							txtDefaultAccount.TextMatrix(0, 0, "");
							txtReceivableAccount.TextMatrix(0, 0, "");
							txtOverrideIntAccount.TextMatrix(0, 0, "");
							txtOverrideSalesTaxAccount.TextMatrix(0, 0, "");
							Label16.Enabled = false;
							Label17.Enabled = false;
							txtOverrideIntAccount.Enabled = false;
							txtOverrideSalesTaxAccount.Enabled = false;
							txtDefaultAccount.Enabled = false;
							txtReceivableAccount.Enabled = false;
							Label4.Enabled = false;
							lblDefaultAccount.Enabled = false;
						}
						intCurCode = intTypeCode;
						cmbCode.Text = FCConvert.ToString(intTypeCode);
						ColorGrid_2(false);
					}
					boolDirty = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Type - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmTypeSetup_Resize(object sender, System.EventArgs e)
		{
			if (fraNewType.Visible == true)
			{
				ShowFrame(fraNewType);
			}
			else
			{
				// ShowFrame fraDefaultAccount
				ShowFrame(fraReceiptList);
			}
			vsBill.Height = ((vsBill as DataGridView).Rows[0].Height * (vsBill.Rows - 1)) + vsBill.ColumnHeadersHeight + 2;
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (boolDirty)
			{
				if (Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3)) != 0)
				{
					intTemp = MessageBox.Show("Do you want to save Code " + Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3) + "?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					// If Val(cmbCode.ComboItem(cmbCode.ComboIndex)) <> 0 Then
					// intTemp = MsgBox("Do you want to save Code " & cmbCode.ComboItem(cmbCode.ComboIndex) & "?", vbYesNoCancel + vbQuestion, "Save Changes")
					if (intTemp == DialogResult.Yes)
					{
						// yes
						// save it and exit
						SaveCode();
					}
					else if (intTemp == DialogResult.No)
					{
						// no
						// exit w/o saving
					}
					else if (intTemp == DialogResult.Cancel)
					{
						// cancel
						// go back to the screen and
						e.Cancel = true;
						return;
					}
				}
			}
			// this will reload any types that have been changed
			// grsType.OpenRecordset "SELECT * FROM DefaultBillTypes", DEFAULTDATABASE
			boolLoaded = false;
			//MDIParent.InstancePtr.Show();
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuFileBack_Click(object sender, System.EventArgs e)
		{
			MoveBack();
		}

		private void mnuFileForward_Click(object sender, System.EventArgs e)
		{
			MoveForward();
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			vsBill.Rows = 1;
			vsBill.Rows = 7;
			txtTitle.Text = "";
			txtRefTitle.Text = "";
			txtCTRL1Title.Text = "";
			txtCTRL2Title.Text = "";
			txtCTRL3Title.Text = "";
			boolDirty = true;
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessNew_Click(object sender, System.EventArgs e)
		{
			int intCT;
			clsDRWrapper rsNew = new clsDRWrapper();
			// find the next availible type number and insert it into the box as default
			rsNew.OpenRecordset("SELECT * FROM DefaultBillTypes ORDER BY TypeCode");
			intCT = 1;
			if (rsNew.EndOfFile() != true && rsNew.BeginningOfFile() != true)
			{
				rsNew.MoveFirst();
				for (intCT = 1; intCT <= 990; intCT++)
				{
					if (!rsNew.EndOfFile())
					{
						if (FCConvert.ToInt32(rsNew.Get_Fields_Int32("TypeCode")) > intCT)
						{
							// if the database is greater than the counter
							break;
						}
						else
						{
							rsNew.MoveNext();
							// they match, so try the next one
						}
					}
					else
					{
						break;
					}
				}
			}
			if (intCT >= 1000)
			{
				MessageBox.Show("There are no more bill types.", "Type Table Full", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			else
			{
				txtCode.Text = modGlobal.PadToString_6(intCT, 3);
			}
			// do not allow the user to use these menu options
			btnFileBack.Enabled = false;
			btnFileForward.Enabled = false;
			// hide this frame
			fraReceiptList.Visible = false;
			// show the frame
			ShowFrame(fraNewType);
			if (txtCode.Visible && txtCode.Enabled)
			{
				txtCode.Focus();
				txtCode.SelectionStart = 0;
				txtCode.SelectionLength = txtCode.Text.Length;
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int I;
			int intCode = 0;
			if (fraNewType.Visible)
			{
				// save the new code
				cmdNewCode_Click();
			}
			else
			{
				cmbType.Focus();
				if (ValidateType())
				{
					intCode = intCurCode;
					// saves the current Type Code
					SaveCode();
					// set the combo box to the last code
					for (I = 0; I <= cmbType.Items.Count - 1; I++)
					{
						if (intCode == Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3)))
						{
							cmbType.SelectedIndex = I;
							break;
						}
					}
				}
			}
		}

		private void mnuProcessSaveExit_Click(object sender, System.EventArgs e)
		{
			int intCode = 0;
			int intCT;
			if (fraNewType.Visible)
			{
				// save the new code
				cmdNewCode_Click();
			}
			else
			{
				cmbType.Focus();
				if (ValidateType())
				{
					intCode = intCurCode;
					if (SaveCode())
					{
						// this will check to see if the code is still in the combobox, if it is then it will set
						for (intCT = 0; intCT <= cmbType.Items.Count - 1; intCT++)
						{
							if (intCode == Conversion.Val(Strings.Left(cmbType.Items[intCT].ToString(), 3)))
							{
								intCurCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[intCT].ToString(), 3))));
								break;
							}
							else
							{
								intCurCode = -1;
							}
						}
						vsBill.EditText = "";
						// this will stop the edittext from bleeding into the next type
						//Application.DoEvents();
						MoveForward();
					}
					else
					{
						MessageBox.Show("This type was not saved correctly.", "Unsuccessful Save", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
			}
		}

		private void txtCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void txtCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						cmdNewCode_Click();
						break;
					}
			}
			//end switch
		}

		private void txtCode_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// this will not allow characters into the textbox '8,13
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
			{
				// the numbers, backspace and return
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillCodeCombo()
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill all of the types into cmbType
				int Number = 0;
				string Name = "";
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				int intCT = 0;
				rsTemp.DefaultDB = "TWAR0000.vb1";
				cmbType.Clear();
				intErr = 1;
				FCUtils.EraseSafe(modGlobal.Statics.strTypeDescriptions);
				intErr = 2;
				rsTemp.OpenRecordset("SELECT * FROM DefaultBillTypes ORDER BY TypeCode");
				if (rsTemp.BeginningOfFile() != true && rsTemp.EndOfFile() != true)
				{
					intErr = 3;
					rsTemp.MoveFirst();
					intErr = 4;
					intCurCode = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("TypeCode"));
					do
					{
						intErr = 5;
						// this fills all of the types into the combobox
						Number = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("TypeCode"));
						intErr = 6;
						Name = FCConvert.ToString(rsTemp.Get_Fields_String("TypeTitle"));
						intErr = 7;
						modGlobal.Statics.strTypeDescriptions[Number] = Name;
						intErr = 8;
						strTemp = modGlobal.PadToString_6(Number, 3) + " - " + Name;
						intErr = 9;
						cmbType.AddItem(strTemp);
						intCT += 1;
						rsTemp.MoveNext();
					}
					while (!rsTemp.EndOfFile());
				}
				else
				{
					MessageBox.Show("No types were loaded.", "No Bill Types", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fill Combo Error - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtCTRL1Title_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCTRL1Title_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtCTRL1Title.ReadOnly = false;
		}

		private void txtCTRL2Title_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCTRL2Title_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtCTRL2Title.ReadOnly = false;
		}

		private void txtCTRL3Title_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCTRL3Title_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtCTRL3Title.ReadOnly = false;
		}

		private void txtDefaultAccount_Leave(object sender, System.EventArgs e)
		{
			lblAccountTitle2.Text = "";
		}

		private void txtReceivableAccount_Leave(object sender, System.EventArgs e)
		{
			lblAccountTitle2.Text = "";
		}

		private void txtOverrideIntAccount_Leave(object sender, System.EventArgs e)
		{
			lblAccountTitle2.Text = "";
		}

		private void txtOverrideSalesTaxAccount_Leave(object sender, System.EventArgs e)
		{
			lblAccountTitle2.Text = "";
		}

		private void txtRefTitle_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtRefTitle_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtRefTitle.ReadOnly = false;
		}

		private void txtRefTitle_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// if txtRefTitle.Tag
		}

		private void txtTitle_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtTitle_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtTitle.ReadOnly = false;
		}
		// Private Sub vsBill_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
		// If Col = 3 Then
		// vsBill.EditMaxLength = 17
		// End If
		// End Sub
		//FC:FINAL:AM:#i484 - use EditingControlShowing instead
		//private void vsBill_ChangeEdit(object sender, System.EventArgs e)
		//{
		//    //FC:FINAL:AM: check for edit mode
		//    if (vsBill.IsCurrentCellInEditMode)
		//    {
		//        boolDirty = true;
		//        if (vsBill.Col == 3)
		//        { // if this is the account col then
		//            if (modGlobalConstants.Statics.gboolBD)
		//            {
		//                lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsBill.EditText);
		//            }
		//        }
		//    }
		//}
		private void VsBill_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				e.Control.KeyUp -= Control_KeyUp;
				e.Control.KeyUp += Control_KeyUp;
			}
		}

		private void Control_KeyUp(object sender, KeyEventArgs e)
		{
			if (vsBill.Col == 3)
			{
				// if this is the account col then
				if (modGlobalConstants.Statics.gboolBD)
				{
					lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsBill.EditText);
				}
			}
		}

		private void vsBill_ClickEvent(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCode;
				if (cmbType.SelectedIndex != -1)
				{
					intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
				}
				lngRow = vsBill.Row;
				switch (vsBill.Col)
				{
					case 1:
					case 5:
						{
							vsBill.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsBill.EditMaxLength = 40;
							vsBill.EditCell();
							break;
						}
					case 2:
						{
							vsBill.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsBill.EditMaxLength = 40;
							vsBill.EditCell();
							// Case 3
							// DoEvents
							// vsBill.Editable = True
							// vsBill.EditMaxLength = 17
							// SetGridFormat vsBill, vsBill.Row, vsBill.Col, False
							// vsBill.EditCell
							// vsBill.EditSelStart = 0
							// vsBill.EditSelLength = 1
							break;
						}
					case 4:
						{
							if (Strings.Left(vsBill.TextMatrix(lngRow, 3), 1) == "G")
							{
								if (Conversion.Val(vsBill.TextMatrix(lngRow, vsBill.Col)) == -1)
								{
									vsBill.TextMatrix(lngRow, vsBill.Col, FCConvert.ToString(0));
								}
								else
								{
									vsBill.TextMatrix(lngRow, vsBill.Col, FCConvert.ToString(-1));
								}
							}
							else
							{
								vsBill.TextMatrix(lngRow, vsBill.Col, FCConvert.ToString(0));
							}
							// 
							// If Left$(vsBill.TextMatrix(lngRow, 3), 1) = "G" Then
							// vsBill.Editable = True
							// Else
							// vsBill.Editable = False
							// If vsBill.TextMatrix(lngRow, 4) = True Then vsBill.TextMatrix(lngRow, 4) = False
							// End If
							break;
						}
					case 7:
						{
							vsBill.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							break;
						}
					default:
						{
							vsBill.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Grid Click Error - " + FCConvert.ToString(lngRow), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsBill_Enter(object sender, System.EventArgs e)
		{
			int lngRow;
			// this is the row that was clicked on
			lngRow = vsBill.Row;
			if (!boolAccountBox)
			{
				// vsBill.Select 1, 1
				vsBill.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			boolAccountBox = false;
		}

		private void vsBill_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsBill.Col == 5 && e.KeyCode == Keys.Left)
			{
				boolLeftArrow = true;
			}
			else
			{
				boolLeftArrow = false;
			}
			if (vsBill.Col == 4 && FCConvert.ToInt32(KeyCode) == 32)
			{
				vsBill_ClickEvent(sender, EventArgs.Empty);
			}
		}
		// Private Sub vsBill_KeyDownEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
		// If vsBill.Col = 3 Then
		// this is for the left and right arrow key
		// Debug.Print " IN " & KeyCode
		// CheckKeyDownEdit vsBill, Row, Col, KeyCode, Shift, vsBill.EditSelStart, vsBill.EditText, vsBill.EditSelLength
		// Debug.Print " IN " & KeyCode
		// End If
		// End Sub
		private void vsBill_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			switch (vsBill.Col)
			{
				case 5:
					{
						if ((keyAscii >= 48 && keyAscii <= 57) || (keyAscii == 8) || (keyAscii == 13) || (keyAscii == 46) || (keyAscii == 45))
						{
						}
						else
						{
							keyAscii = 0;
						}
						// Case 3
						// Debug.Print "KeyPressEdit Start TM - " & vsBill.TextMatrix(vsBill.Row, vsBill.Col) & "   ET - " & vsBill.EditText & "   SS - " & vsBill.EditSelStart
						// CheckAccountKeyPress vsBill, Row, Col, KeyAscii, vsBill.EditSelStart, vsBill.EditText
						// Debug.Print "KeyPressEdit End   TM - " & vsBill.TextMatrix(vsBill.Row, vsBill.Col) & "   ET - " & vsBill.EditText & "   SS - " & vsBill.EditSelStart
						break;
					}
			}
			//end switch
		}
		// Private Sub vsBill_KeyUpEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
		// If Col = 3 Then
		// If KeyCode <> 40 And KeyCode <> 38 And KeyCode <> 123 And KeyCode <> 122 Then     'up and down arrows
		// Debug.Print "KeyUpEdit    Start TM - " & vsBill.TextMatrix(vsBill.Row, vsBill.Col) & "   ET - " & vsBill.EditText & "   SS - " & vsBill.EditSelStart
		// CheckAccountKeyCode vsBill, Row, Col, KeyCode, 0, vsBill.EditSelStart, vsBill.EditText, vsBill.EditSelLength
		// Debug.Print "KeyUpEdit    End   TM - " & vsBill.TextMatrix(vsBill.Row, vsBill.Col) & "   ET - " & vsBill.EditText & "   SS - " & vsBill.EditSelStart
		// End If
		// End If
		// End Sub
		private void vsBill_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCode;
				int lngRow;
				int lngCol;
				lngRow = vsBill.Row;
				lngCol = vsBill.Col;
				if (!boolChangingRows)
				{
					boolChangingRows = true;
					if (cmbType.SelectedIndex != -1)
					{
						intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
					}
					// show the account description in the label at the bottom of the page
					if (Strings.Trim(vsBill.TextMatrix(lngRow, 3)) != "")
					{
						if (Strings.InStr(1, vsBill.TextMatrix(lngRow, 3), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.InStr(1, vsBill.TextMatrix(lngRow, 3), "--", CompareConstants.vbBinaryCompare) == 0 && Strings.Right(vsBill.TextMatrix(lngRow, 3), 1) != "-")
						{
							if (modGlobalConstants.Statics.gboolBD)
							{
								lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsBill.TextMatrix(lngRow, 3));
							}
						}
						else
						{
							lblAccountTitle.Text = "";
						}
					}
					else
					{
						lblAccountTitle.Text = "";
					}
					switch (vsBill.Col)
					{
						case 1:
						case 5:
							{
								vsBill.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsBill.EditMaxLength = 40;
								vsBill.EditCell();
								break;
							}
						case 2:
							{
								vsBill.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsBill.EditMaxLength = 8;
								vsBill.EditCell();
								// Case 3
								// .Editable = flexEDKbdMouse
								// Debug.Print "RowColChange Start TM - " & vsBill.TextMatrix(vsBill.Row, vsBill.Col) & "   ET - " & vsBill.EditText & "   SS - " & vsBill.EditSelStart
								// SetGridFormat vsBill, vsBill.Row, vsBill.Col, True
								// Debug.Print "RowColChange End   TM - " & vsBill.TextMatrix(vsBill.Row, vsBill.Col) & "   ET - " & vsBill.EditText & "   SS - " & vsBill.EditSelStart
								// .Editable = False
								// txtAcct(lngRow).Text = .TextMatrix(lngRow, .Col)
								// txtAcct(lngRow).Left = .Left + .ColWidth(0) + .ColWidth(1) + .ColWidth(2)
								// txtAcct(lngRow).Top = .Top + (lngRow * .RowHeight(0))
								// txtAcct(lngRow).Tag = lngRow
								// txtAcct(lngRow).Visible = True
								// txtAcct(lngRow).Enabled = True
								// txtAcct(lngRow).SetFocus
								break;
							}
						case 4:
							{
								vsBill.Editable = FCGrid.EditableSettings.flexEDNone;
								// If Left$(.TextMatrix(lngRow, 3), 1) = "G" Then
								// .Editable = flexEDKbdMouse
								// .EditCell
								// Else
								// .Editable = False
								// If .TextMatrix(lngRow, 4) <> "" Then
								// If .TextMatrix(lngRow, 4) = True Then .TextMatrix(lngRow, 4) = False
								// End If
								// If boolLeftArrow Then
								// .Col = 3
								// Else
								// .Col = 5
								// .Editable = flexEDKbdMouse
								// .EditCell
								// End If
								// End If
								break;
							}
						case 7:
							{
								vsBill.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								break;
							}
						default:
							{
								vsBill.Editable = FCGrid.EditableSettings.flexEDNone;
								break;
							}
					}
					//end switch
					// If .Col = 3 Then
					// If txtAcct(lngRow).Visible Then
					// txtAcct(lngRow).SetFocus
					// End If
					// End If
					if (vsBill.Col >= vsBill.RightCol - 1 && lngRow == vsBill.BottomRow)
					{
						vsBill.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						vsBill.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
					}
					boolChangingRows = false;
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Validation Error (Row Column Change)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowFrame(Control fraFrame)
		{
			// this will place the frame in the middle of the form
			//fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			//fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			if (fraFrame is GroupBox)
			{
				fraFrame.Top = 30;
				fraFrame.Left = 30;
			}
			else
			{
				fraFrame.Top = 0;
				fraFrame.Left = 0;
			}
			fraFrame.Visible = true;
		}

		private bool SaveCode()
		{
			bool SaveCode = false;
			// this sub will save all of the information, as long as it has a title
			clsDRWrapper rsSave = new clsDRWrapper();
			clsDRWrapper rsRb = new clsDRWrapper();
			int intCT;
			SaveCode = true;
			for (intCT = 1; intCT <= 6; intCT++)
			{
				// If txtAcct(intCT).Visible Then
				// SendKeys "{tab}"
				// txtAcct_Validate intCT, False
				// End If
			}
			if (cmbType.Visible)
			{
				cmbType.Focus();
			}
			// check to see if there are any receipts of this type that have not been closed out.
			rsSave.OpenRecordset("SELECT * FROM CustomerBills WHERE Type = " + FCConvert.ToString(intCurCode));
			if (rsSave.EndOfFile())
			{
				// this means that there are no receipts with this type so it is ok to let the user save
			}
			else
			{
				if (MessageBox.Show("There is already a bill, that has not been closed out, of this bill type.  Would you like to save this type anyway?", "Type In Use", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					modGlobalFunctions.AddCYAEntry_26("AR", "Overriding type setup in order to save.", "Type = " + FCConvert.ToString(intCurCode));
				}
				else
				{
					SaveCode = false;
					return SaveCode;
				}
			}
			if (Strings.Trim(txtTitle.Text) != "")
			{
				rsSave.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intCurCode));
				if (rsSave.BeginningOfFile() != true && rsSave.EndOfFile() != true)
				{
					rsSave.Edit();
					rsSave.Set_Fields("SalesTax", chkSalesTax.CheckState == Wisej.Web.CheckState.Checked);
					rsSave.Set_Fields("TypeTitle", Strings.Trim(txtTitle.Text));
					rsSave.Set_Fields("Reference", Strings.Trim(txtRefTitle.Text));
					rsSave.Set_Fields("ReferenceMandatory", chkMandatory[0].CheckState);
					rsSave.Set_Fields("ReferenceChanges", chkChanges[0].CheckState);
					rsSave.Set_Fields("Control1", Strings.Trim(txtCTRL1Title.Text));
					rsSave.Set_Fields("Control1Mandatory", chkMandatory[1].CheckState);
					rsSave.Set_Fields("Control1Changes", chkChanges[1].CheckState);
					rsSave.Set_Fields("Control2", Strings.Trim(txtCTRL2Title.Text));
					rsSave.Set_Fields("Control2Mandatory", chkMandatory[2].CheckState);
					rsSave.Set_Fields("Control2Changes", chkChanges[2].CheckState);
					rsSave.Set_Fields("Control3", Strings.Trim(txtCTRL3Title.Text));
					rsSave.Set_Fields("Control3Mandatory", chkMandatory[3].CheckState);
					rsSave.Set_Fields("Control3Changes", chkChanges[3].CheckState);
					rsSave.Set_Fields("InterestMethod", Strings.Left(cboInterestMethod.Text, 2));
					if (cboPayee.SelectedIndex >= 0)
					{
						rsSave.Set_Fields("PayeeID", cboPayee.ItemData(cboPayee.SelectedIndex));
					}
					else
					{
						rsSave.Set_Fields("PayeeID", 0);
					}
					if (Strings.Mid(cboInterestMethod.Text, 2, 1) == "F")
					{
						rsSave.Set_Fields("FlatAmount", FCConvert.ToDecimal(txtFlatAmount.Text));
						rsSave.Set_Fields("PerDiemRate", 0);
					}
					else
					{
						rsSave.Set_Fields("FlatAmount", 0);
						rsSave.Set_Fields("PerDiemRate", FCConvert.ToDouble(txtPerDiem.Text) / 100);
					}
					rsSave.Set_Fields("InterestStartDate", cboInterestStartDate.ItemData(cboInterestStartDate.SelectedIndex));
					rsSave.Set_Fields("FrequencyCode", Strings.Left(cboFrequencyCode.Text, 1));
					rsSave.Set_Fields("FirstMonth", FCConvert.ToString(Conversion.Val(Strings.Left(cboFrequencyFirstMonth.Text, 2))));
					if (Strings.Trim(vsBill.TextMatrix(1, 1)) != "" && Strings.Trim(vsBill.TextMatrix(1, 2)) != "" && vsBill.TextMatrix(1, 3) != "")
					{
						rsSave.Set_Fields("Title1", vsBill.TextMatrix(1, 1));
						if (Strings.Trim(vsBill.TextMatrix(1, 2)) == "")
						{
							rsSave.Set_Fields("Title1Abbrev", Strings.Trim(Strings.Left(vsBill.TextMatrix(1, 1) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title1Abbrev", vsBill.TextMatrix(1, 2));
						}
						rsSave.Set_Fields("Account1", RemoveUnderscore(vsBill.TextMatrix(1, 3)));
						if (Conversion.Val(vsBill.TextMatrix(1, 5)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount1", vsBill.TextMatrix(1, 5));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount1", 0);
						}
						//FC:FINAL:DSE #i470 Convert numeric string to boolean
						//rsSave.Set_Fields("Year1", vsBill.TextMatrix(1, 4));
						rsSave.Set_Fields("Year1", FCConvert.CBool(vsBill.TextMatrix(1, 4)));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account1")))
						{
							MessageBox.Show("Invalid account number for fee 1.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Title1", "");
						rsSave.Set_Fields("Title1Abbrev", "");
						rsSave.Set_Fields("Account1", "");
						rsSave.Set_Fields("DefaultAmount1", 0);
						rsSave.Set_Fields("Year1", false);
					}
					if (Strings.Trim(vsBill.TextMatrix(2, 1)) != "" && Strings.Trim(vsBill.TextMatrix(2, 2)) != "" && vsBill.TextMatrix(2, 3) != "")
					{
						rsSave.Set_Fields("Title2", vsBill.TextMatrix(2, 1));
						if (Strings.Trim(vsBill.TextMatrix(2, 2)) == "")
						{
							rsSave.Set_Fields("Title2Abbrev", Strings.Trim(Strings.Left(vsBill.TextMatrix(2, 1) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title2Abbrev", vsBill.TextMatrix(2, 2));
						}
						rsSave.Set_Fields("Account2", RemoveUnderscore(vsBill.TextMatrix(2, 3)));
						if (Conversion.Val(vsBill.TextMatrix(2, 5)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount2", vsBill.TextMatrix(2, 5));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount2", 0);
						}
						rsSave.Set_Fields("Year2", vsBill.TextMatrix(2, 4));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account2")))
						{
							MessageBox.Show("Invalid account number for fee 2.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Title2", "");
						rsSave.Set_Fields("Title2Abbrev", "");
						rsSave.Set_Fields("Account2", "");
						rsSave.Set_Fields("DefaultAmount2", 0);
						rsSave.Set_Fields("Year2", false);
					}
					if (Strings.Trim(vsBill.TextMatrix(3, 1)) != "" && Strings.Trim(vsBill.TextMatrix(3, 2)) != "" && vsBill.TextMatrix(3, 3) != "")
					{
						rsSave.Set_Fields("Title3", vsBill.TextMatrix(3, 1));
						if (Strings.Trim(vsBill.TextMatrix(3, 2)) == "")
						{
							rsSave.Set_Fields("Title3Abbrev", Strings.Trim(Strings.Left(vsBill.TextMatrix(3, 1) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title3Abbrev", vsBill.TextMatrix(3, 2));
						}
						rsSave.Set_Fields("Account3", RemoveUnderscore(vsBill.TextMatrix(3, 3)));
						if (Conversion.Val(vsBill.TextMatrix(3, 5)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount3", vsBill.TextMatrix(3, 5));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount3", 0);
						}
						rsSave.Set_Fields("Year3", vsBill.TextMatrix(3, 4));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account3")))
						{
							MessageBox.Show("Invalid account number for fee 3.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Title3", "");
						rsSave.Set_Fields("Title3Abbrev", "");
						rsSave.Set_Fields("Account3", "");
						rsSave.Set_Fields("DefaultAmount3", 0);
						rsSave.Set_Fields("Year3", false);
					}
					if (Strings.Trim(vsBill.TextMatrix(4, 1)) != "" && Strings.Trim(vsBill.TextMatrix(4, 2)) != "" && vsBill.TextMatrix(4, 3) != "")
					{
						rsSave.Set_Fields("Title4", vsBill.TextMatrix(4, 1));
						if (Strings.Trim(vsBill.TextMatrix(4, 2)) == "")
						{
							rsSave.Set_Fields("Title4Abbrev", Strings.Trim(Strings.Left(vsBill.TextMatrix(4, 1) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title4Abbrev", vsBill.TextMatrix(4, 2));
						}
						rsSave.Set_Fields("Account4", RemoveUnderscore(vsBill.TextMatrix(4, 3)));
						if (Conversion.Val(vsBill.TextMatrix(4, 5)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount4", vsBill.TextMatrix(4, 5));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount4", 0);
						}
						rsSave.Set_Fields("Year4", vsBill.TextMatrix(4, 4));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account4")))
						{
							MessageBox.Show("Invalid account number for fee 4.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Title4", "");
						rsSave.Set_Fields("Title4Abbrev", "");
						rsSave.Set_Fields("Account4", "");
						rsSave.Set_Fields("DefaultAmount4", 0);
						rsSave.Set_Fields("Year4", false);
					}
					if (Strings.Trim(vsBill.TextMatrix(5, 1)) != "" && Strings.Trim(vsBill.TextMatrix(5, 2)) != "" && vsBill.TextMatrix(5, 3) != "")
					{
						rsSave.Set_Fields("Title5", vsBill.TextMatrix(5, 1));
						if (Strings.Trim(vsBill.TextMatrix(5, 2)) == "")
						{
							rsSave.Set_Fields("Title5Abbrev", Strings.Trim(Strings.Left(vsBill.TextMatrix(5, 1) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title5Abbrev", vsBill.TextMatrix(5, 2));
						}
						rsSave.Set_Fields("Account5", RemoveUnderscore(vsBill.TextMatrix(5, 3)));
						if (Conversion.Val(vsBill.TextMatrix(5, 5)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount5", vsBill.TextMatrix(5, 5));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount5", 0);
						}
						rsSave.Set_Fields("Year5", vsBill.TextMatrix(5, 4));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account5")))
						{
							MessageBox.Show("Invalid account number for fee 5.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Title5", "");
						rsSave.Set_Fields("Title5Abbrev", "");
						rsSave.Set_Fields("Account5", "");
						rsSave.Set_Fields("DefaultAmount5", 0);
						rsSave.Set_Fields("Year5", false);
					}
					if (Strings.Trim(vsBill.TextMatrix(6, 1)) != "" && Strings.Trim(vsBill.TextMatrix(6, 2)) != "" && vsBill.TextMatrix(6, 3) != "")
					{
						rsSave.Set_Fields("Title6", vsBill.TextMatrix(6, 1));
						if (Strings.Trim(vsBill.TextMatrix(6, 2)) == "")
						{
							rsSave.Set_Fields("Title6Abbrev", Strings.Trim(Strings.Left(vsBill.TextMatrix(6, 1) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title6Abbrev", vsBill.TextMatrix(6, 2));
						}
						rsSave.Set_Fields("Account6", RemoveUnderscore(vsBill.TextMatrix(6, 3)));
						if (Conversion.Val(vsBill.TextMatrix(6, 5)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount6", vsBill.TextMatrix(6, 5));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount6", 0);
						}
						rsSave.Set_Fields("Year6", vsBill.TextMatrix(6, 4));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account6")))
						{
							MessageBox.Show("Invalid account number for fee 6.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Title6", "");
						rsSave.Set_Fields("Title6Abbrev", "");
						rsSave.Set_Fields("Account6", "");
						rsSave.Set_Fields("DefaultAmount6", 0);
						rsSave.Set_Fields("Year6", false);
					}
					if (txtDefaultAccount.TextMatrix(0, 0) != "" && Strings.InStr(1, txtDefaultAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						rsSave.Set_Fields("DefaultAccount", txtDefaultAccount.TextMatrix(0, 0));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("DefaultAccount")))
						{
							MessageBox.Show("Invalid account number for alternate cash account.  You will not be able to use this bill type until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("DefaultAccount", "");
					}
					if (txtReceivableAccount.TextMatrix(0, 0) != "" && Strings.InStr(1, txtReceivableAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						rsSave.Set_Fields("ReceivableAccount", txtReceivableAccount.TextMatrix(0, 0));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("ReceivableAccount")))
						{
							MessageBox.Show("Invalid account number for receivable account.  You will not be able to use this bill type until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("ReceivableAccount", "");
					}
					if (txtOverrideIntAccount.TextMatrix(0, 0) != "" && Strings.InStr(1, txtOverrideIntAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						rsSave.Set_Fields("OverrideIntAccount", txtOverrideIntAccount.TextMatrix(0, 0));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("OverrideIntAccount")))
						{
							MessageBox.Show("Invalid account number for override interest account.  You will not be able to use this bill type until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("OverrideIntAccount", "");
					}
					if (txtOverrideSalesTaxAccount.TextMatrix(0, 0) != "" && Strings.InStr(1, txtOverrideSalesTaxAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						rsSave.Set_Fields("OverrideTaxAccount", txtOverrideSalesTaxAccount.TextMatrix(0, 0));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("OverrideTaxAccount")))
						{
							MessageBox.Show("Invalid account number for override sales tax account.  You will not be able to use this bill type until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("OverrideTaxAccount", "");
					}
					if (rsSave.Update())
					{
						boolDirty = false;
					}
				}
				else
				{
					MessageBox.Show("Type Code not found.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					SaveCode = false;
					return SaveCode;
				}
			}
			else
			{
				// this will clear the type code
				if (boolDirty)
				{
					if (rsSave.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intCurCode)))
					{
						if (rsSave.BeginningOfFile() != true && rsSave.EndOfFile() != true)
						{
							if (FCConvert.ToString(rsSave.Get_Fields_String("TypeTitle")) != "")
							{
								if (MessageBox.Show("Are you sure that you would like this Code cleared?", "Clear Code", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									rsSave.Edit();
									rsSave.Set_Fields("TypeTitle", "");
									rsSave.Set_Fields("Reference", "");
									rsSave.Set_Fields("Control1", "");
									rsSave.Set_Fields("Control2", "");
									rsSave.Set_Fields("Control3", "");
									rsSave.Set_Fields("InterestMethod", "");
									rsSave.Set_Fields("FlatAmount", 0);
									rsSave.Set_Fields("PerDiemRate", 0);
									rsSave.Set_Fields("InterestStartDate", 0);
									rsSave.Set_Fields("PayeeID", 0);
									rsSave.Set_Fields("FrequencyCode", "Y");
									rsSave.Set_Fields("FirstMonth", 1);
									rsSave.Set_Fields("DefaultAccount", "");
									rsSave.Set_Fields("OverrideIntAccount", "");
									rsSave.Set_Fields("OverrideTaxAccount", "");
									rsSave.Set_Fields("Title1", "");
									rsSave.Set_Fields("Title1Abbrev", "");
									rsSave.Set_Fields("Account1", 0);
									rsSave.Set_Fields("DefaultAmount1", 0);
									rsSave.Set_Fields("Year1", false);
									rsSave.Set_Fields("Title2", "");
									rsSave.Set_Fields("Title2Abbrev", "");
									rsSave.Set_Fields("Account2", 0);
									rsSave.Set_Fields("DefaultAmount2", 0);
									rsSave.Set_Fields("Year2", false);
									rsSave.Set_Fields("Title3", "");
									rsSave.Set_Fields("Title3Abbrev", "");
									rsSave.Set_Fields("Account3", 0);
									rsSave.Set_Fields("DefaultAmount3", 0);
									rsSave.Set_Fields("Year3", false);
									rsSave.Set_Fields("Title4", "");
									rsSave.Set_Fields("Title4Abbrev", "");
									rsSave.Set_Fields("Account4", 0);
									rsSave.Set_Fields("DefaultAmount4", 0);
									rsSave.Set_Fields("Year4", false);
									rsSave.Set_Fields("Title5", "");
									rsSave.Set_Fields("Title5Abbrev", "");
									rsSave.Set_Fields("Account5", 0);
									rsSave.Set_Fields("DefaultAmount5", 0);
									rsSave.Set_Fields("Year5", false);
									rsSave.Set_Fields("Title6", "");
									rsSave.Set_Fields("Title6Abbrev", "");
									rsSave.Set_Fields("Account6", 0);
									rsSave.Set_Fields("DefaultAmount6", 0);
									rsSave.Set_Fields("Year6", false);
									if (rsSave.Update())
									{
										boolDirty = false;
									}
								}
								else
								{
									// nothing
								}
							}
							else
							{
							}
						}
					}
					else
					{
						MessageBox.Show("Error #" + FCConvert.ToString(rsSave.ErrorNumber) + " - " + rsSave.ErrorDescription + ".", "Clear Type Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						SaveCode = false;
						return SaveCode;
					}
				}
				else
				{
				}
			}
			MessageBox.Show("Save Complete!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			//Application.DoEvents();
			FillCodeCombo();
			// For intCT = 1 To 6
			// If txtAcct(intCT).Visible Then
			// txtAcct(intCT).Visible = False
			// End If
			// Next
			if (txtCode.Visible && txtCode.Enabled)
			{
				txtCode.Focus();
			}
			return SaveCode;
		}

		private void MoveBack()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				CheckForSave();
				if (cmbType.SelectedIndex <= 0)
				{
					// it is at the beginning
					cmbType.SelectedIndex = cmbType.Items.Count - 1;
				}
				else
				{
					cmbType.SelectedIndex = cmbType.SelectedIndex - 1;
				}
				ShowType_2(FCConvert.ToInt16(Conversion.Val(cmbType.Items[cmbType.SelectedIndex].ToString())));
				intCurCode = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbType.Items[cmbType.SelectedIndex].ToString())));
				// txtAcct(6).Visible = False
				// txtAcct(1).Visible = False
				// txtAcct(2).Visible = False
				// txtAcct(3).Visible = False
				// txtAcct(4).Visible = False
				// txtAcct(5).Visible = False
				if (txtTitle.Enabled && txtTitle.Visible)
				{
					txtTitle.Focus();
				}
				else if (cmbType.Visible && cmbType.Enabled)
				{
					cmbType.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Move Back", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MoveForward()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intIndex;
				CheckForSave();
				// this will find the
				for (intIndex = 0; intIndex <= cmbType.Items.Count - 1; intIndex++)
				{
					if (intCurCode == Conversion.Val(Strings.Left(cmbType.Items[intIndex].ToString(), 3)))
					{
						break;
					}
				}
				// ShowType Val(Left(cmbType.List(intIndex), 3))
				// intCurCode = Val(Left(cmbType.List(intIndex), 3))
				// txtAcct.Visible = False
				if (intIndex < cmbType.Items.Count - 1)
				{
					cmbType.SelectedIndex = intIndex + 1 % cmbType.Items.Count;
				}
				else if (cmbType.Items.Count > 0)
				{
					cmbType.SelectedIndex = 0;
				}
				else
				{
					cmbType.SelectedIndex = -1;
				}
				// cmbCode.SetFocus
				// cmbCode.ComboIndex = ((cmbCode.ComboIndex + 1) Mod cmbCode.ComboCount)
				// If Trim$(cmbCode.ComboItem(cmbCode.ComboIndex) & " ") <> "" Then
				// ShowType Val(cmbCode.ComboItem(cmbCode.ComboIndex))
				// txtAcct.Visible = False
				// cmbCode.Text = cmbCode.ComboItem(cmbCode.ComboIndex)
				// End If
				if (txtTitle.Enabled && txtTitle.Visible)
				{
					txtTitle.Focus();
				}
				else if (cmbType.Visible && cmbType.Enabled)
				{
					cmbType.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Move Forward", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckForSave()
		{
			bool CheckForSave = false;
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			CheckForSave = true;
			if (boolDirty)
			{
				if (Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3)) != 0)
				{
					if (intCurCode > 0)
					{
						intTemp = MessageBox.Show("Do you want to save the changes in Code " + FCConvert.ToString(intCurCode) + "?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
						if (intTemp == DialogResult.Yes)
						{
							// yes
							// save it and exit
							SaveCode();
						}
						else if (intTemp == DialogResult.No)
						{
							// no
							// do nothing
						}
					}
					else
					{
						// CheckForSave = False
					}
				}
				else
				{
				}
				// If Val(cmbCode.ComboItem(cmbCode.ComboIndex)) <> 0 Then
				// intTemp = MsgBox("Do you want to save the changes in Code " & intCurCode & "?", vbYesNo + vbQuestion, "Save Changes")
				// If intTemp = 6 Then         'yes
				// save it and exit
				// SaveCode
				// ElseIf intTemp = 7 Then     'no
				// do nothing
				// End If
				// End If
			}
			boolDirty = false;
			return CheckForSave;
		}

		private void ProcessType()
		{
			int intTypeCode = 0;
			bool boolCancel;
			int intTemp;
			clsDRWrapper rsTemp;
			boolCancel = false;
			lblAccountTitle.Text = "";
			lblAccountTitle2.Text = "";
			if (cmbType.SelectedIndex != -1)
			{
				intTypeCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
				if (CheckForSave())
				{
					intCurCode = intTypeCode;
					ShowType(intCurCode);
				}
			}
			// If cmbCode.ComboIndex <> -1 Then
			// If CheckForSave Then
			// If Not boolCancel Then
			// If Val(cmbCode.ComboItem(cmbCode.ComboIndex)) = Val(cmbCode.Text) Then  'is the text showing in the combo box the same as the current type number
			// ShowType Val(cmbCode.ComboItem(cmbCode.ComboIndex))
			// boolDirty = False
			// intTypeCode = Val(cmbCode.ComboItem(cmbCode.ComboIndex))
			// intCurCode = intTypeCode
			// Else
			// boolDirty = False
			// intTypeCode = Val(cmbCode.Text)
			// intCurCode = intTypeCode
			// End If
			// 
			// If RestrictedCode Then
			// boolLockType = True
			// Else
			// boolLockType = False
			// End If
			// End If
			// End If
			// End If
		}

		private void vsBill_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsBill.IsCurrentCellInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsBill.GetFlexRowIndex(e.RowIndex);
				int col = vsBill.GetFlexColIndex(e.ColumnIndex);
				// this will make sure the format is correct
				switch (col)
				{
					case 3:
						{
							if (modGlobalConstants.Statics.gboolBD)
							{
								e.Cancel = modNewAccountBox.CheckAccountValidate(vsBill, row, col, e.Cancel);
							}
							else
							{
								if (FCConvert.CBool(Strings.Left(vsBill.EditText, 1) != "M"))
								{
									vsBill.EditText = "";
								}
							}
							if (!e.Cancel)
							{
								if (Strings.Left(vsBill.EditText, 1) != "G")
								{
									vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row, 4, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
								}
								else
								{
									vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row, 4, Information.RGB(255, 255, 255));
								}
							}
							return;
						}
					case 5:
						{
							if (Conversion.Val(vsBill.TextMatrix(row, 7)) == -1)
							{
								vsBill.EditText = Strings.Format(vsBill.EditText, "#0");
							}
							else
							{
								vsBill.EditText = Strings.Format(vsBill.EditText, "#,##0.00");
							}
							break;
						}
				}
				//end switch
			}
		}

		private void ColorGrid_2(bool boolRestricted)
		{
			ColorGrid(ref boolRestricted);
		}

		private void ColorGrid(ref bool boolRestricted)
		{
			if (boolRestricted)
			{
				// color the grid with grayed out columns
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 6, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 6, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			}
			else
			{
				// set the grid back to white
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 6, 2, Color.White);
				vsBill.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 6, 2, modGlobalConstants.Statics.TRIOCOLORBLACK);
			}
		}

		private string RemoveUnderscore(string strAcct)
		{
			string RemoveUnderscore = "";
			// this function will remove the underscores from any M account and will
			// return "" if there is any underscores for all other accounts
			if (Strings.Trim(strAcct) != "")
			{
				if (Strings.Left(strAcct, 1) != "M")
				{
					if (Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) != 0)
					{
						RemoveUnderscore = "";
					}
					else
					{
						RemoveUnderscore = strAcct;
					}
				}
				else
				{
					RemoveUnderscore = strAcct.Replace("_", "");
				}
			}
			else
			{
				RemoveUnderscore = "";
			}
			return RemoveUnderscore;
		}

		private void SetFrequencyCodeCombo_2(string strCriteria)
		{
			SetFrequencyCodeCombo(ref strCriteria);
		}

		private void SetFrequencyCodeCombo(ref string strCriteria)
		{
			int counter;
			if (strCriteria == "")
			{
				strCriteria = "Y";
			}
			for (counter = 0; counter <= cboFrequencyCode.Items.Count - 1; counter++)
			{
				if (Strings.Left(cboFrequencyCode.Items[counter].ToString(), 1) == strCriteria)
				{
					cboFrequencyCode.SelectedIndex = counter;
					break;
				}
			}
		}

		private void SetFrequencyFirstMonthCombo_2(string strCriteria)
		{
			SetFrequencyFirstMonthCombo(ref strCriteria);
		}

		private void SetFrequencyFirstMonthCombo(ref string strCriteria)
		{
			int counter;
			for (counter = 0; counter <= cboFrequencyFirstMonth.Items.Count - 1; counter++)
			{
				if (Strings.Left(cboFrequencyFirstMonth.Items[counter].ToString(), 2) == Strings.Format(strCriteria, "00"))
				{
					cboFrequencyFirstMonth.SelectedIndex = counter;
					break;
				}
			}
		}

		private void txtDefaultAccount_Change()
		{
			boolDirty = true;
			if (modGlobalConstants.Statics.gboolBD)
			{
				lblAccountTitle2.Text = modAccountTitle.ReturnAccountDescription(txtDefaultAccount.TextMatrix(0, 0));
			}
		}

		private void txtDefaultAccount_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			if (modGlobalConstants.Statics.gboolBD)
			{
				lblAccountTitle2.Text = modAccountTitle.ReturnAccountDescription(txtDefaultAccount.TextMatrix(0, 0));
			}
		}

		private void txtOverrideIntAccount_Change()
		{
			boolDirty = true;
			if (modGlobalConstants.Statics.gboolBD)
			{
				lblAccountTitle2.Text = modAccountTitle.ReturnAccountDescription(txtOverrideIntAccount.TextMatrix(0, 0));
			}
		}

		private void txtOverrideIntAccount_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			if (modGlobalConstants.Statics.gboolBD)
			{
				lblAccountTitle2.Text = modAccountTitle.ReturnAccountDescription(txtOverrideIntAccount.TextMatrix(0, 0));
			}
		}

		private void txtOverrideSalesTaxAccount_Change()
		{
			boolDirty = true;
			if (modGlobalConstants.Statics.gboolBD)
			{
				lblAccountTitle2.Text = modAccountTitle.ReturnAccountDescription(txtOverrideSalesTaxAccount.TextMatrix(0, 0));
			}
		}

		private void txtOverrideSalesTaxAccount_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			if (modGlobalConstants.Statics.gboolBD)
			{
				lblAccountTitle2.Text = modAccountTitle.ReturnAccountDescription(txtOverrideSalesTaxAccount.TextMatrix(0, 0));
			}
		}

		private void txtReceivableAccount_Change()
		{
			boolDirty = true;
			if (modGlobalConstants.Statics.gboolBD)
			{
				lblAccountTitle2.Text = modAccountTitle.ReturnAccountDescription(txtReceivableAccount.TextMatrix(0, 0));
			}
		}

		private void txtReceivableAccount_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			if (modGlobalConstants.Statics.gboolBD)
			{
				lblAccountTitle2.Text = modAccountTitle.ReturnAccountDescription(txtReceivableAccount.TextMatrix(0, 0));
			}
		}

		private void SetCustomFormColors()
		{
			Label8.ForeColor = Color.Blue;
			lblAccountTitle.ForeColor = Color.Blue;
			lblAccountTitle2.ForeColor = Color.Blue;
		}

		public void SetInterestMethodCombo_2(string strSearch)
		{
			SetInterestMethodCombo(ref strSearch);
		}

		public void SetInterestMethodCombo(ref string strSearch)
		{
			int counter;
			for (counter = 0; counter <= cboInterestMethod.Items.Count - 1; counter++)
			{
				if (strSearch == Strings.Left(cboInterestMethod.Items[counter].ToString(), 2))
				{
					cboInterestMethod.SelectedIndex = counter;
					break;
				}
			}
		}

		public bool ValidateType()
		{
			bool ValidateType = false;
			int I;
			int intCode;
			bool boolNoSave = false;
			string strAcct = "";
			int lngFund = 0;
			int intCT;
			clsDRWrapper rsCRType = new clsDRWrapper();
			ValidateType = true;
			if (Strings.Trim(txtReceivableAccount.TextMatrix(0, 0)) != "" && Strings.Left(txtReceivableAccount.TextMatrix(0, 0), 1) != "M" && Strings.InStr(1, txtReceivableAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
			{
				// this will check to see if all of the accounts for this type are from the same fund as this account
				strAcct = txtReceivableAccount.TextMatrix(0, 0);
				if (Strings.Trim(strAcct + " ") != "" && Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Left(strAcct, 1) != "M")
				{
					lngFund = modBudgetaryAccounting.GetFundFromAccount(strAcct, true);
					if (lngFund > 0)
					{
						for (intCT = 1; intCT <= 6; intCT++)
						{
							if (vsBill.TextMatrix(intCT, 3) != "")
							{
								if (lngFund != modBudgetaryAccounting.GetFundFromAccount(vsBill.TextMatrix(intCT, 3), true))
								{
									MessageBox.Show("All of the accounts in category 1 - 6 must be from the same fund as the Receivable, Alternate Cash, Override Interest, and Override Sales Tax Accounts.", "Incorrect Fund", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									ValidateType = false;
									return ValidateType;
								}
							}
						}
					}
					else if (lngFund == -1)
					{
						// this is a bad account so blank the acct box out
						MessageBox.Show("Invalid account or the system could not find the fund for this account.", "Bad Account / Missing Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtReceivableAccount.Focus();
						ValidateType = false;
						return ValidateType;
					}
				}
			}
			else if (modGlobalConstants.Statics.gboolBD)
			{
				MessageBox.Show("You must enter a receivable account before you may save the type.", "Invalid Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtReceivableAccount.Focus();
				ValidateType = false;
				return ValidateType;
			}
			if (boolNoSave == false)
			{
				if (Strings.Trim(txtDefaultAccount.TextMatrix(0, 0)) != "" && Strings.Left(txtDefaultAccount.TextMatrix(0, 0), 1) != "M" && Strings.InStr(1, txtDefaultAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					// this will check to see if all of the accounts for this type are from the same fund as this account
					strAcct = txtDefaultAccount.TextMatrix(0, 0);
					if (Strings.Trim(strAcct + " ") != "" && Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Left(strAcct, 1) != "M")
					{
						lngFund = modBudgetaryAccounting.GetFundFromAccount(strAcct, true);
						if (lngFund > 0)
						{
							for (intCT = 1; intCT <= 6; intCT++)
							{
								if (vsBill.TextMatrix(intCT, 3) != "")
								{
									if (lngFund != modBudgetaryAccounting.GetFundFromAccount(vsBill.TextMatrix(intCT, 3), true))
									{
										MessageBox.Show("All of the accounts in category 1 - 6 must be from the same fund as the Receivable, Alternate Cash, Override Interest, and Override Sales Tax Accounts.", "Incorrect Fund", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										ValidateType = false;
										return ValidateType;
									}
								}
							}
						}
						else if (lngFund == -1)
						{
							// this is a bad account so blank the acct box out
							MessageBox.Show("Invalid account or the system could not find the fund for this account.", "Bad Account / Missing Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtDefaultAccount.Focus();
							ValidateType = false;
							return ValidateType;
						}
					}
				}
			}
			if (boolNoSave == false)
			{
				if (Strings.Trim(txtOverrideIntAccount.TextMatrix(0, 0)) != "" && Strings.Left(txtOverrideIntAccount.TextMatrix(0, 0), 1) != "M" && Strings.InStr(1, txtOverrideIntAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					// this will check to see if all of the accounts for this type are from the same fund as this account
					strAcct = txtOverrideIntAccount.TextMatrix(0, 0);
					if (Strings.Trim(strAcct + " ") != "" && Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Left(strAcct, 1) != "M")
					{
						lngFund = modBudgetaryAccounting.GetFundFromAccount(strAcct, true);
						if (lngFund > 0)
						{
							for (intCT = 1; intCT <= 6; intCT++)
							{
								if (vsBill.TextMatrix(intCT, 3) != "")
								{
									if (lngFund != modBudgetaryAccounting.GetFundFromAccount(vsBill.TextMatrix(intCT, 3), true))
									{
										MessageBox.Show("All of the accounts in category 1 - 6 must be from the same fund as the Receivable, Alternate Cash, Override Interest, and Override Sales Tax Accounts.", "Incorrect Fund", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										ValidateType = false;
										return ValidateType;
									}
								}
							}
						}
						else if (lngFund == -1)
						{
							// this is a bad account so blank the acct box out
							MessageBox.Show("Invalid account or the system could not find the fund for this account.", "Bad Account / Missing Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtDefaultAccount.Focus();
							ValidateType = false;
							return ValidateType;
						}
					}
				}
			}
			if (boolNoSave == false)
			{
				if (Strings.Trim(txtOverrideSalesTaxAccount.TextMatrix(0, 0)) != "" && Strings.Left(txtOverrideSalesTaxAccount.TextMatrix(0, 0), 1) != "M" && Strings.InStr(1, txtOverrideSalesTaxAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					// this will check to see if all of the accounts for this type are from the same fund as this account
					strAcct = txtOverrideSalesTaxAccount.TextMatrix(0, 0);
					if (Strings.Trim(strAcct + " ") != "" && Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Left(strAcct, 1) != "M")
					{
						lngFund = modBudgetaryAccounting.GetFundFromAccount(strAcct, true);
						if (lngFund > 0)
						{
							for (intCT = 1; intCT <= 6; intCT++)
							{
								if (vsBill.TextMatrix(intCT, 3) != "")
								{
									if (lngFund != modBudgetaryAccounting.GetFundFromAccount(vsBill.TextMatrix(intCT, 3), true))
									{
										MessageBox.Show("All of the accounts in category 1 - 6 must be from the same fund as the Receivable, Alternate Cash, Override Interest, and Override Sales Tax Accounts.", "Incorrect Fund", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										ValidateType = false;
										return ValidateType;
									}
								}
							}
						}
						else if (lngFund == -1)
						{
							// this is a bad account so blank the acct box out
							MessageBox.Show("Invalid account or the system could not find the fund for this account.", "Bad Account / Missing Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtDefaultAccount.Focus();
							ValidateType = false;
							return ValidateType;
						}
					}
				}
			}
			for (intCT = 1; intCT <= 6; intCT++)
			{
				if (Strings.Trim(vsBill.TextMatrix(intCT, 1)) != "" || Strings.Trim(vsBill.TextMatrix(intCT, 2)) != "" || vsBill.TextMatrix(intCT, 3) != "")
				{
					if (Strings.Trim(vsBill.TextMatrix(intCT, 1)) == "" || Strings.Trim(vsBill.TextMatrix(intCT, 2)) == "" || vsBill.TextMatrix(intCT, 3) == "")
					{
						MessageBox.Show("You must enter a screen title, report title, and account for each fee.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
						ValidateType = false;
						return ValidateType;
					}
				}
			}
			return ValidateType;
		}

		private void SetInterestStartDateCombo_2(short intDays)
		{
			SetInterestStartDateCombo(ref intDays);
		}

		private void SetInterestStartDateCombo(ref short intDays)
		{
			switch (intDays)
			{
				case 30:
					{
						cboInterestStartDate.SelectedIndex = 0;
						break;
					}
				case 60:
					{
						cboInterestStartDate.SelectedIndex = 1;
						break;
					}
				case 90:
					{
						cboInterestStartDate.SelectedIndex = 2;
						break;
					}
				default:
					{
						cboInterestStartDate.SelectedIndex = 0;
						break;
					}
			}
			//end switch
		}

		private void FillDefaultInterestStartDateOptions()
		{
			cboInterestStartDate.AddItem("30 Days After Billing");
			cboInterestStartDate.ItemData(cboInterestStartDate.NewIndex, 30);
			cboInterestStartDate.AddItem("60 Days After Billing");
			cboInterestStartDate.ItemData(cboInterestStartDate.NewIndex, 60);
			cboInterestStartDate.AddItem("90 Days After Billing");
			cboInterestStartDate.ItemData(cboInterestStartDate.NewIndex, 90);
		}

		private void FillPayeeCombo()
		{
			clsDRWrapper rsPayeeInfo = new clsDRWrapper();
			rsPayeeInfo.OpenRecordset("SELECT * FROM Payees ORDER BY Description", "TWAR0000.vb1");
			if (rsPayeeInfo.EndOfFile() != true && rsPayeeInfo.BeginningOfFile() != true)
			{
				do
				{
					cboPayee.AddItem(rsPayeeInfo.Get_Fields_String("Description"));
					cboPayee.ItemData(cboPayee.NewIndex, FCConvert.ToInt32(rsPayeeInfo.Get_Fields_Int32("PayeeID")));
					rsPayeeInfo.MoveNext();
				}
				while (rsPayeeInfo.EndOfFile() != true);
			}
		}
		// vbPorter upgrade warning: intPayeeID As int	OnWrite(double, int)
		private void SetPayeeCombo_2(int intPayeeID)
		{
			SetPayeeCombo(ref intPayeeID);
		}

		private void SetPayeeCombo(ref int intPayeeID)
		{
			int counter;
			clsDRWrapper rsDefaults = new clsDRWrapper();
			if (intPayeeID > 0)
			{
				// do nothing
			}
			else
			{
				rsDefaults.OpenRecordset("SELECT * FROM Customize", "TWAR0000.vb1");
				if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
				{
					intPayeeID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDefaults.Get_Fields_Int32("DefaultPayeeID"))));
				}
			}
			for (counter = 0; counter <= cboPayee.Items.Count - 1; counter++)
			{
				if (cboPayee.ItemData(counter) == intPayeeID)
				{
					cboPayee.SelectedIndex = counter;
					break;
				}
			}
		}
	}
}
