﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWAR0000
{
    /// <summary>
    /// Summary description for frmUTRPTViewer.
    /// </summary>
    partial class frmARRPTViewer : BaseForm
    {
        public fecherFoundation.FCFrame fraRecreateReport;
        public fecherFoundation.FCComboBox cmbRecreate;
        public fecherFoundation.FCLabel lblRecreateInstructions;
        public ARViewer arView;
        public fecherFoundation.FCFrame fraNumber;
        public fecherFoundation.FCComboBox cmbNumber;
        public fecherFoundation.FCLabel lblInstruction;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.fraRecreateReport = new fecherFoundation.FCFrame();
			this.cmbRecreate = new fecherFoundation.FCComboBox();
			this.lblRecreateInstructions = new fecherFoundation.FCLabel();
			this.arView = new Global.ARViewer();
			this.fraNumber = new fecherFoundation.FCFrame();
			this.cmbNumber = new fecherFoundation.FCComboBox();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.cmdSave = new fecherFoundation.FCButton();
			this.TopWrapper = new fecherFoundation.FCPanel();
			this.cmdFileRecreate = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRecreateReport)).BeginInit();
			this.fraRecreateReport.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraNumber)).BeginInit();
			this.fraNumber.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TopWrapper)).BeginInit();
			this.TopWrapper.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRecreate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 606);
			this.BottomPanel.Size = new System.Drawing.Size(726, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraRecreateReport);
			this.ClientArea.Controls.Add(this.fraNumber);
			this.ClientArea.Controls.Add(this.arView);
			this.ClientArea.Controls.Add(this.TopWrapper);
			this.ClientArea.Size = new System.Drawing.Size(746, 606);
			this.ClientArea.Controls.SetChildIndex(this.TopWrapper, 0);
			this.ClientArea.Controls.SetChildIndex(this.arView, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraNumber, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraRecreateReport, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileRecreate);
			this.TopPanel.Size = new System.Drawing.Size(746, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileRecreate, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(317, 30);
			this.HeaderText.Text = "Recreate Daily Audit Report";
			// 
			// fraRecreateReport
			// 
			this.fraRecreateReport.AppearanceKey = "groupBoxNoBorders";
			this.fraRecreateReport.Controls.Add(this.cmbRecreate);
			this.fraRecreateReport.Controls.Add(this.lblRecreateInstructions);
			this.fraRecreateReport.Location = new System.Drawing.Point(10, 49);
			this.fraRecreateReport.Name = "fraRecreateReport";
			this.fraRecreateReport.Size = new System.Drawing.Size(338, 106);
			this.fraRecreateReport.TabIndex = 1;
			this.fraRecreateReport.Visible = false;
			// 
			// cmbRecreate
			// 
			this.cmbRecreate.BackColor = System.Drawing.SystemColors.Window;
			this.cmbRecreate.Location = new System.Drawing.Point(20, 54);
			this.cmbRecreate.Name = "cmbRecreate";
			this.cmbRecreate.Size = new System.Drawing.Size(234, 40);
			this.cmbRecreate.TabIndex = 1;
			// 
			// lblRecreateInstructions
			// 
			this.lblRecreateInstructions.Location = new System.Drawing.Point(20, 28);
			this.lblRecreateInstructions.Name = "lblRecreateInstructions";
			this.lblRecreateInstructions.Size = new System.Drawing.Size(267, 15);
			this.lblRecreateInstructions.TabIndex = 2;
			this.lblRecreateInstructions.Text = "SELECT AUDIT TO RECREATE";
			// 
			// arView
			// 
			this.arView.Dock = Wisej.Web.DockStyle.Fill;
			this.arView.Name = "arView";
			this.arView.ScrollBars = false;
			this.arView.Size = new System.Drawing.Size(746, 606);
			this.arView.TabIndex = 2;
			// 
			// fraNumber
			// 
			this.fraNumber.AppearanceKey = "groupBoxNoBorders";
			this.fraNumber.Controls.Add(this.cmbNumber);
			this.fraNumber.Controls.Add(this.lblInstruction);
			this.fraNumber.Location = new System.Drawing.Point(30, 49);
			this.fraNumber.Name = "fraNumber";
			this.fraNumber.Size = new System.Drawing.Size(292, 100);
			this.fraNumber.TabIndex = 1001;
			this.fraNumber.Visible = false;
			// 
			// cmbNumber
			// 
			this.cmbNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbNumber.Location = new System.Drawing.Point(20, 50);
			this.cmbNumber.Name = "cmbNumber";
			this.cmbNumber.Size = new System.Drawing.Size(231, 40);
			this.cmbNumber.TabIndex = 1;
			// 
			// lblInstruction
			// 
			this.lblInstruction.Location = new System.Drawing.Point(20, 28);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(270, 15);
			this.lblInstruction.TabIndex = 2;
			this.lblInstruction.Text = "CHOOSE THE REPORT NUMBER TO VIEW";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 50);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(167, 48);
			this.cmdSave.Text = "Save & Continue";
			this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// TopWrapper
			// 
			this.TopWrapper.Controls.Add(this.cmdSave);
			this.TopWrapper.Location = new System.Drawing.Point(0, 120);
			this.TopWrapper.Name = "TopWrapper";
			this.TopWrapper.Size = new System.Drawing.Size(300, 110);
			this.TopWrapper.TabIndex = 1002;
			this.TopWrapper.Visible = false;
			// 
			// cmdFileRecreate
			// 
			this.cmdFileRecreate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileRecreate.Enabled = false;
			this.cmdFileRecreate.Location = new System.Drawing.Point(578, 29);
			this.cmdFileRecreate.Name = "cmdFileRecreate";
			this.cmdFileRecreate.Size = new System.Drawing.Size(140, 24);
			this.cmdFileRecreate.TabIndex = 1;
			this.cmdFileRecreate.Text = "Recreate Daily Audit";
			this.cmdFileRecreate.Visible = false;
			this.cmdFileRecreate.Click += new System.EventHandler(this.mnuFileRecreate_Click);
			// 
			// frmARRPTViewer
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(746, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmARRPTViewer";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Recreate Daily Audit Report";
			this.Load += new System.EventHandler(this.frmARRPTViewer_Load);
			this.Activated += new System.EventHandler(this.frmARRPTViewer_Activated);
			this.Resize += new System.EventHandler(this.frmARRPTViewer_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmARRPTViewer_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRecreateReport)).EndInit();
			this.fraRecreateReport.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraNumber)).EndInit();
			this.fraNumber.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TopWrapper)).EndInit();
			this.TopWrapper.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRecreate)).EndInit();
			this.ResumeLayout(false);

        }
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSave;
        private FCButton cmdFileRecreate;
        //FC:FINAL:CHN - issues #1375, 1216: Redesign.
        private FCPanel TopWrapper;
    }
}
