//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillPrep.
	/// </summary>
	partial class frmBillPrep : BaseForm
	{
		public fecherFoundation.FCComboBox cmbName;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtTitle;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTitle;
		public fecherFoundation.FCFrame fraAccountSearch;
		public fecherFoundation.FCTextBox txtSearchName;
		public fecherFoundation.FCGrid vsAccountSearch;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtTitle_3;
		public fecherFoundation.FCTextBox txtTitle_0;
		public fecherFoundation.FCTextBox txtTitle_1;
		public fecherFoundation.FCTextBox txtTitle_2;
		public fecherFoundation.FCLabel lblTitle_3;
		public fecherFoundation.FCLabel lblTitle_0;
		public fecherFoundation.FCLabel lblTitle_1;
		public fecherFoundation.FCLabel lblTitle_2;
		public fecherFoundation.FCGrid vsAccounts;
		public fecherFoundation.FCGrid vsFees;
		public fecherFoundation.FCLabel lblTotal;
		public fecherFoundation.FCLabel lblTotalLabel;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbName = new fecherFoundation.FCComboBox();
            this.fraAccountSearch = new fecherFoundation.FCFrame();
            this.txtSearchName = new fecherFoundation.FCTextBox();
            this.vsAccountSearch = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtTitle_3 = new fecherFoundation.FCTextBox();
            this.txtTitle_0 = new fecherFoundation.FCTextBox();
            this.txtTitle_1 = new fecherFoundation.FCTextBox();
            this.txtTitle_2 = new fecherFoundation.FCTextBox();
            this.lblTitle_3 = new fecherFoundation.FCLabel();
            this.lblTitle_0 = new fecherFoundation.FCLabel();
            this.lblTitle_1 = new fecherFoundation.FCLabel();
            this.lblTitle_2 = new fecherFoundation.FCLabel();
            this.vsAccounts = new fecherFoundation.FCGrid();
            this.vsFees = new fecherFoundation.FCGrid();
            this.lblTotal = new fecherFoundation.FCLabel();
            this.lblTotalLabel = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip();
            this.btnProcessSave = new fecherFoundation.FCButton();
            this.btnFileNewCustomer = new fecherFoundation.FCButton();
            this.btnAddCustomer = new fecherFoundation.FCButton();
            this.btnFileSelectAll = new fecherFoundation.FCButton();
            this.btnFileClearAll = new fecherFoundation.FCButton();
            this.btnFileBillingEditReport = new fecherFoundation.FCButton();
            this.btnImportBills = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).BeginInit();
            this.fraAccountSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileNewCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileBillingEditReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportBills)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 661);
            this.BottomPanel.Size = new System.Drawing.Size(878, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.vsAccounts);
            this.ClientArea.Controls.Add(this.vsFees);
            this.ClientArea.Controls.Add(this.lblTotal);
            this.ClientArea.Controls.Add(this.lblTotalLabel);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.fraAccountSearch);
            this.ClientArea.Size = new System.Drawing.Size(898, 444);
            this.ClientArea.Controls.SetChildIndex(this.fraAccountSearch, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTotalLabel, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTotal, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsFees, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsAccounts, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnImportBills);
            this.TopPanel.Controls.Add(this.btnFileBillingEditReport);
            this.TopPanel.Controls.Add(this.btnFileClearAll);
            this.TopPanel.Controls.Add(this.btnFileSelectAll);
            this.TopPanel.Controls.Add(this.btnAddCustomer);
            this.TopPanel.Controls.Add(this.btnFileNewCustomer);
            this.TopPanel.Size = new System.Drawing.Size(898, 60);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileNewCustomer, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnAddCustomer, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileSelectAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileClearAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileBillingEditReport, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnImportBills, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(104, 30);
            this.HeaderText.Text = "Bill Prep";
            // 
            // cmbName
            // 
            this.cmbName.Items.AddRange(new object[] {
            "Name",
            "Customer #",
            "Address"});
            this.cmbName.Location = new System.Drawing.Point(20, 30);
            this.cmbName.Name = "cmbName";
            this.cmbName.Size = new System.Drawing.Size(171, 40);
            this.cmbName.TabIndex = 8;
            // 
            // fraAccountSearch
            // 
            this.fraAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraAccountSearch.BackColor = System.Drawing.Color.White;
            this.fraAccountSearch.Controls.Add(this.txtSearchName);
            this.fraAccountSearch.Controls.Add(this.cmbName);
            this.fraAccountSearch.Controls.Add(this.vsAccountSearch);
            this.fraAccountSearch.Location = new System.Drawing.Point(30, 66);
            this.fraAccountSearch.Name = "fraAccountSearch";
            this.fraAccountSearch.Size = new System.Drawing.Size(320, 479);
            this.fraAccountSearch.TabIndex = 6;
            this.fraAccountSearch.Text = "Account Search";
            this.fraAccountSearch.Visible = false;
            // 
            // txtSearchName
            // 
            this.txtSearchName.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearchName.Location = new System.Drawing.Point(20, 80);
            this.txtSearchName.Name = "txtSearchName";
            this.txtSearchName.Size = new System.Drawing.Size(171, 40);
            this.txtSearchName.TabIndex = 7;
            this.txtSearchName.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearchName_KeyDown);
            // 
            // vsAccountSearch
            // 
            this.vsAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsAccountSearch.Cols = 4;
            this.vsAccountSearch.Location = new System.Drawing.Point(20, 130);
            this.vsAccountSearch.Name = "vsAccountSearch";
            this.vsAccountSearch.Rows = 1;
            this.vsAccountSearch.Size = new System.Drawing.Size(280, 329);
            this.vsAccountSearch.TabIndex = 8;
            this.vsAccountSearch.DoubleClick += new System.EventHandler(this.vsAccountSearch_DblClick);
            this.vsAccountSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsAccountSearch_KeyPressEvent);
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.txtTitle_3);
            this.Frame2.Controls.Add(this.txtTitle_0);
            this.Frame2.Controls.Add(this.txtTitle_1);
            this.Frame2.Controls.Add(this.txtTitle_2);
            this.Frame2.Controls.Add(this.lblTitle_3);
            this.Frame2.Controls.Add(this.lblTitle_0);
            this.Frame2.Controls.Add(this.lblTitle_1);
            this.Frame2.Controls.Add(this.lblTitle_2);
            this.Frame2.Location = new System.Drawing.Point(30, 298);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(692, 194);
            this.Frame2.TabIndex = 2;
            // 
            // txtTitle_3
            // 
            this.txtTitle_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle_3.Location = new System.Drawing.Point(385, 131);
            this.txtTitle_3.MaxLength = 10;
            this.txtTitle_3.Name = "txtTitle_3";
            this.txtTitle_3.Size = new System.Drawing.Size(170, 40);
            this.txtTitle_3.TabIndex = 7;
            // 
            // txtTitle_0
            // 
            this.txtTitle_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle_0.Location = new System.Drawing.Point(0, 47);
            this.txtTitle_0.MaxLength = 10;
            this.txtTitle_0.Name = "txtTitle_0";
            this.txtTitle_0.Size = new System.Drawing.Size(170, 40);
            this.txtTitle_0.TabIndex = 1;
            // 
            // txtTitle_1
            // 
            this.txtTitle_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle_1.Location = new System.Drawing.Point(0, 131);
            this.txtTitle_1.MaxLength = 10;
            this.txtTitle_1.Name = "txtTitle_1";
            this.txtTitle_1.Size = new System.Drawing.Size(170, 40);
            this.txtTitle_1.TabIndex = 3;
            // 
            // txtTitle_2
            // 
            this.txtTitle_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle_2.Location = new System.Drawing.Point(191, 131);
            this.txtTitle_2.MaxLength = 10;
            this.txtTitle_2.Name = "txtTitle_2";
            this.txtTitle_2.Size = new System.Drawing.Size(170, 40);
            this.txtTitle_2.TabIndex = 5;
            // 
            // lblTitle_3
            // 
            this.lblTitle_3.Location = new System.Drawing.Point(385, 104);
            this.lblTitle_3.Name = "lblTitle_3";
            this.lblTitle_3.Size = new System.Drawing.Size(170, 18);
            this.lblTitle_3.TabIndex = 6;
            this.lblTitle_3.Text = "CONTROL3";
            // 
            // lblTitle_0
            // 
            this.lblTitle_0.Location = new System.Drawing.Point(0, 20);
            this.lblTitle_0.Name = "lblTitle_0";
            this.lblTitle_0.Size = new System.Drawing.Size(170, 16);
            this.lblTitle_0.TabIndex = 8;
            this.lblTitle_0.Text = "REF";
            // 
            // lblTitle_1
            // 
            this.lblTitle_1.Location = new System.Drawing.Point(0, 104);
            this.lblTitle_1.Name = "lblTitle_1";
            this.lblTitle_1.Size = new System.Drawing.Size(170, 16);
            this.lblTitle_1.TabIndex = 2;
            this.lblTitle_1.Text = "CONTROL1";
            // 
            // lblTitle_2
            // 
            this.lblTitle_2.Location = new System.Drawing.Point(191, 104);
            this.lblTitle_2.Name = "lblTitle_2";
            this.lblTitle_2.Size = new System.Drawing.Size(170, 18);
            this.lblTitle_2.TabIndex = 4;
            this.lblTitle_2.Text = "CONTROL2";
            // 
            // vsAccounts
            // 
            this.vsAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsAccounts.Cols = 21;
            this.vsAccounts.ExtendLastCol = true;
            this.vsAccounts.FixedCols = 0;
            this.vsAccounts.FrozenCols = 1;
            this.vsAccounts.Location = new System.Drawing.Point(30, 66);
            this.vsAccounts.Name = "vsAccounts";
            this.vsAccounts.RowHeadersVisible = false;
            this.vsAccounts.Rows = 1;
            this.vsAccounts.Size = new System.Drawing.Size(651, 232);
            this.vsAccounts.TabIndex = 1;
            this.vsAccounts.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.vsAccounts_BeforeRowColChange);
            this.vsAccounts.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsAccounts_MouseMoveEvent);
            this.vsAccounts.CurrentCellChanged += new System.EventHandler(this.vsAccounts_RowColChange);
            this.vsAccounts.ColumnWidthChanged += new Wisej.Web.DataGridViewColumnEventHandler(this.Grid_ColumnWidthChanged);
            this.vsAccounts.RowHeightChanged += new Wisej.Web.DataGridViewRowEventHandler(this.Grid_RowHeightChanged);
            this.vsAccounts.Enter += new System.EventHandler(this.vsAccounts_Enter);
            this.vsAccounts.Click += new System.EventHandler(this.vsAccounts_ClickEvent);
            this.vsAccounts.KeyDown += new Wisej.Web.KeyEventHandler(this.vsAccounts_KeyDownEvent);
            // 
            // vsFees
            // 
            this.vsFees.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsFees.Cols = 5;
            this.vsFees.ExtendLastCol = true;
            this.vsFees.Location = new System.Drawing.Point(30, 506);
            this.vsFees.Name = "vsFees";
            this.vsFees.Rows = 7;
            this.vsFees.Size = new System.Drawing.Size(651, 129);
            this.vsFees.StandardTab = false;
            this.vsFees.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsFees.TabIndex = 3;
            this.vsFees.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsFees_ValidateEdit);
            this.vsFees.CurrentCellChanged += new System.EventHandler(this.vsFees_RowColChange);
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblTotal.Location = new System.Drawing.Point(571, 643);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(106, 18);
            this.lblTotal.TabIndex = 5;
            this.lblTotal.Text = "FINAL TOTAL";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTotalLabel
            // 
            this.lblTotalLabel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblTotalLabel.Location = new System.Drawing.Point(474, 643);
            this.lblTotalLabel.Name = "lblTotalLabel";
            this.lblTotalLabel.Size = new System.Drawing.Size(81, 18);
            this.lblTotalLabel.TabIndex = 4;
            this.lblTotalLabel.Text = "FINAL TOTAL";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(113, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(317, 18);
            this.Label1.TabIndex = 10;
            this.Label1.Text = " = DATA ENTRY MESSAGE EXISTS FOR THIS CUSTOMER";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(74, 16);
            this.Label2.TabIndex = 11;
            this.Label2.Text = "BLUE TEXT";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.AppearanceKey = "acceptButton";
            this.btnProcessSave.Location = new System.Drawing.Point(328, 30);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcessSave.Size = new System.Drawing.Size(153, 48);
            this.btnProcessSave.Text = "Save & Process";
            this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // btnFileNewCustomer
            // 
            this.btnFileNewCustomer.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileNewCustomer.Location = new System.Drawing.Point(677, 29);
            this.btnFileNewCustomer.Name = "btnFileNewCustomer";
            this.btnFileNewCustomer.Shortcut = Wisej.Web.Shortcut.F6;
            this.btnFileNewCustomer.Size = new System.Drawing.Size(134, 24);
            this.btnFileNewCustomer.TabIndex = 6;
            this.btnFileNewCustomer.Text = "Add New Customer";
            this.btnFileNewCustomer.Click += new System.EventHandler(this.mnuFileNewCustomer_Click);
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnAddCustomer.Location = new System.Drawing.Point(518, 29);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Shortcut = Wisej.Web.Shortcut.F5;
            this.btnAddCustomer.Size = new System.Drawing.Size(155, 24);
            this.btnAddCustomer.TabIndex = 5;
            this.btnAddCustomer.Text = "Add Existing Customer";
            this.btnAddCustomer.Click += new System.EventHandler(this.mnuAddCustomer_Click);
            // 
            // btnFileSelectAll
            // 
            this.btnFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileSelectAll.Location = new System.Drawing.Point(441, 29);
            this.btnFileSelectAll.Name = "btnFileSelectAll";
            this.btnFileSelectAll.Shortcut = Wisej.Web.Shortcut.F4;
            this.btnFileSelectAll.Size = new System.Drawing.Size(72, 24);
            this.btnFileSelectAll.TabIndex = 4;
            this.btnFileSelectAll.Text = "Select All";
            this.btnFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
            // 
            // btnFileClearAll
            // 
            this.btnFileClearAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileClearAll.Location = new System.Drawing.Point(368, 29);
            this.btnFileClearAll.Name = "btnFileClearAll";
            this.btnFileClearAll.Shortcut = Wisej.Web.Shortcut.F3;
            this.btnFileClearAll.Size = new System.Drawing.Size(68, 24);
            this.btnFileClearAll.TabIndex = 3;
            this.btnFileClearAll.Text = "Clear All";
            this.btnFileClearAll.Click += new System.EventHandler(this.mnuFileClearAll_Click);
            // 
            // btnFileBillingEditReport
            // 
            this.btnFileBillingEditReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileBillingEditReport.Location = new System.Drawing.Point(239, 29);
            this.btnFileBillingEditReport.Name = "btnFileBillingEditReport";
            this.btnFileBillingEditReport.Size = new System.Drawing.Size(124, 24);
            this.btnFileBillingEditReport.TabIndex = 2;
            this.btnFileBillingEditReport.Text = "Billing Edit Report";
            this.btnFileBillingEditReport.Click += new System.EventHandler(this.mnuFileBillingEditReport_Click);
            // 
            // btnImportBills
            // 
            this.btnImportBills.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnImportBills.Location = new System.Drawing.Point(146, 29);
            this.btnImportBills.Name = "btnImportBills";
            this.btnImportBills.Size = new System.Drawing.Size(88, 24);
            this.btnImportBills.TabIndex = 1;
            this.btnImportBills.Text = "Import Bills";
            this.btnImportBills.Click += new System.EventHandler(this.btnImportBills_Click);
            // 
            // frmBillPrep
            // 
            this.ClientSize = new System.Drawing.Size(898, 504);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBillPrep";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Bill Prep";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmBillPrep_Load);
            this.Activated += new System.EventHandler(this.frmBillPrep_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBillPrep_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).EndInit();
            this.fraAccountSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileNewCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileBillingEditReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportBills)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		internal FCButton btnProcessSave;
		internal FCButton btnFileNewCustomer;
		internal FCButton btnAddCustomer;
		internal FCButton btnFileSelectAll;
		//internal FCButton btnFileSave;
		private FCButton btnFileClearAll;
		internal FCButton btnFileBillingEditReport;
        internal FCButton btnImportBills;
    }
}