﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using TWSharedLibrary;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptBillList.
	/// </summary>
	public partial class rptBillList : BaseSectionReport
	{
		public static rptBillList InstancePtr
		{
			get
			{
				return (rptBillList)Sys.GetInstance(typeof(rptBillList));
			}
		}

		protected rptBillList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curInvoiceTotal As Decimal	OnWrite(Decimal, short)
		Decimal curInvoiceTotal;
		// vbPorter upgrade warning: curCustomerTotal As Decimal	OnWrite(Decimal, short)
		Decimal curCustomerTotal;

		public rptBillList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bill List";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
			this.Fields.Add("InvoiceBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = rsInfo.Get_Fields_Int32("ActualAccountNumber");
				this.Fields["InvoiceBinder"].Value = rsInfo.Get_Fields_String("InvoiceNumber");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strStatus = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = "TRIO Software Corp";
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			if (frmBillList.InstancePtr.cmbStatusSelected.SelectedIndex == 1)
			{
				lblOptions.Text = "Status: All";
			}
			else
			{
				strStatus = "";
				if (frmBillList.InstancePtr.chkActive.CheckState == Wisej.Web.CheckState.Checked)
				{
					strStatus += "Outstanding, ";
				}
				if (frmBillList.InstancePtr.chkPaid.CheckState == Wisej.Web.CheckState.Checked)
				{
					strStatus += "Paid, ";
				}
				if (frmBillList.InstancePtr.chkVoid.CheckState == Wisej.Web.CheckState.Checked)
				{
					strStatus += "Voided, ";
				}
				strStatus = Strings.Left(strStatus, strStatus.Length - 2);
				lblOptions.Text = "Status: " + strStatus;
			}
			lblOptions.Text = lblOptions.Text + "          ";
			lblOptions.Text = lblOptions.Text + "Customer(s): " + frmBillList.InstancePtr.cboCustomers.Text;
			lblOptions.Text = lblOptions.Text + "          ";
			lblOptions.Text = lblOptions.Text + "Bill Type(s): " + frmBillList.InstancePtr.cboBillType.Text;
			lblDateRange.Text = frmBillList.InstancePtr.txtStartDate.Text + " To " + frmBillList.InstancePtr.txtEndDate.Text;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsTypeInfo = new clsDRWrapper();
			// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
			rsTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsInfo.Get_Fields("BillType"));
			fldDescription1.Visible = true;
			fldAmount1.Visible = true;
			fldDescription2.Visible = true;
			fldAmount2.Visible = true;
			fldDescription3.Visible = true;
			fldAmount3.Visible = true;
			fldDescription4.Visible = true;
			fldAmount4.Visible = true;
			fldDescription5.Visible = true;
			fldAmount5.Visible = true;
			fldDescription6.Visible = true;
			fldAmount6.Visible = true;
			fldDescription7.Visible = true;
			fldAmount7.Visible = true;
			fldReferenceDescription.Visible = true;
			fldReference.Visible = true;
			fldControl1Description.Visible = true;
			fldControl1.Visible = true;
			fldControl2Description.Visible = true;
			fldControl2.Visible = true;
			fldControl3Description.Visible = true;
			fldControl3.Visible = true;
			fldDescription1.Text = rsInfo.Get_Fields_String("Description1");
			// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
			fldAmount1.Text = Strings.Format(rsInfo.Get_Fields("Amount1"), "#,##0.00");
			fldDescription2.Text = rsInfo.Get_Fields_String("Description2");
			// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
			fldAmount2.Text = Strings.Format(rsInfo.Get_Fields("Amount2"), "#,##0.00");
			fldDescription3.Text = rsInfo.Get_Fields_String("Description3");
			// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
			fldAmount3.Text = Strings.Format(rsInfo.Get_Fields("Amount3"), "#,##0.00");
			fldDescription4.Text = rsInfo.Get_Fields_String("Description4");
			// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
			fldAmount4.Text = Strings.Format(rsInfo.Get_Fields("Amount4"), "#,##0.00");
			fldDescription5.Text = rsInfo.Get_Fields_String("Description5");
			// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
			fldAmount5.Text = Strings.Format(rsInfo.Get_Fields("Amount5"), "#,##0.00");
			fldDescription6.Text = rsInfo.Get_Fields_String("Description6");
			// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
			fldAmount6.Text = Strings.Format(rsInfo.Get_Fields("Amount6"), "#,##0.00");
			if (rsInfo.Get_Fields_Decimal("TaxOwed") != 0)
			{
				fldDescription7.Text = "Sales Tax";
				fldAmount7.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TaxOwed"), "#,##0.00");
			}
			else
			{
				fldDescription7.Text = "";
				fldAmount7.Text = "0.00";
			}
			fldReferenceDescription.Text = rsTypeInfo.Get_Fields_String("Reference");
			fldControl1Description.Text = rsTypeInfo.Get_Fields_String("Control1");
			fldControl2Description.Text = rsTypeInfo.Get_Fields_String("Control2");
			fldControl3Description.Text = rsTypeInfo.Get_Fields_String("Control3");
			fldReference.Text = rsInfo.Get_Fields_String("Reference");
			fldControl1.Text = rsInfo.Get_Fields_String("Control1");
			fldControl2.Text = rsInfo.Get_Fields_String("Control2");
			fldControl3.Text = rsInfo.Get_Fields_String("Control3");
			if (Strings.Trim(fldDescription6.Text) == "")
			{
				fldDescription6.Text = Strings.Trim(fldDescription7.Text);
				fldAmount6.Text = fldAmount7.Text;
				fldDescription7.Text = "";
				fldAmount7.Text = "0.00";
			}
			if (Strings.Trim(fldDescription5.Text) == "")
			{
				fldDescription5.Text = Strings.Trim(fldDescription6.Text);
				fldAmount5.Text = fldAmount6.Text;
				fldDescription6.Text = Strings.Trim(fldDescription7.Text);
				fldAmount6.Text = fldAmount7.Text;
				fldDescription7.Text = "";
				fldAmount7.Text = "0.00";
			}
			if (Strings.Trim(fldDescription4.Text) == "")
			{
				fldDescription4.Text = Strings.Trim(fldDescription5.Text);
				fldAmount4.Text = fldAmount5.Text;
				fldDescription5.Text = Strings.Trim(fldDescription6.Text);
				fldAmount5.Text = fldAmount6.Text;
				fldDescription6.Text = Strings.Trim(fldDescription7.Text);
				fldAmount6.Text = fldAmount7.Text;
				fldDescription7.Text = "";
				fldAmount7.Text = "0.00";
			}
			if (Strings.Trim(fldDescription3.Text) == "")
			{
				fldDescription3.Text = Strings.Trim(fldDescription4.Text);
				fldAmount3.Text = fldAmount4.Text;
				fldDescription4.Text = Strings.Trim(fldDescription5.Text);
				fldAmount4.Text = fldAmount5.Text;
				fldDescription5.Text = Strings.Trim(fldDescription6.Text);
				fldAmount5.Text = fldAmount6.Text;
				fldDescription6.Text = Strings.Trim(fldDescription7.Text);
				fldAmount6.Text = fldAmount7.Text;
				fldDescription7.Text = "";
				fldAmount7.Text = "0.00";
			}
			if (Strings.Trim(fldDescription2.Text) == "")
			{
				fldDescription2.Text = Strings.Trim(fldDescription3.Text);
				fldAmount2.Text = fldAmount3.Text;
				fldDescription3.Text = Strings.Trim(fldDescription4.Text);
				fldAmount3.Text = fldAmount4.Text;
				fldDescription4.Text = Strings.Trim(fldDescription5.Text);
				fldAmount4.Text = fldAmount5.Text;
				fldDescription5.Text = Strings.Trim(fldDescription6.Text);
				fldAmount5.Text = fldAmount6.Text;
				fldDescription6.Text = Strings.Trim(fldDescription7.Text);
				fldAmount6.Text = fldAmount7.Text;
				fldDescription7.Text = "";
				fldAmount7.Text = "0.00";
			}
			if (Strings.Trim(fldDescription1.Text) == "")
			{
				fldDescription1.Text = Strings.Trim(fldDescription2.Text);
				fldAmount1.Text = fldAmount2.Text;
				fldDescription2.Text = Strings.Trim(fldDescription3.Text);
				fldAmount2.Text = fldAmount3.Text;
				fldDescription3.Text = Strings.Trim(fldDescription4.Text);
				fldAmount3.Text = fldAmount4.Text;
				fldDescription4.Text = Strings.Trim(fldDescription5.Text);
				fldAmount4.Text = fldAmount5.Text;
				fldDescription5.Text = Strings.Trim(fldDescription6.Text);
				fldAmount5.Text = fldAmount6.Text;
				fldDescription6.Text = Strings.Trim(fldDescription7.Text);
				fldAmount6.Text = fldAmount7.Text;
				fldDescription7.Text = "";
				fldAmount7.Text = "0.00";
			}
			if (Strings.Trim(fldDescription1.Text) == "")
			{
				fldDescription1.Visible = false;
				fldAmount1.Visible = false;
			}
			if (Strings.Trim(fldDescription2.Text) == "")
			{
				fldDescription2.Visible = false;
				fldAmount2.Visible = false;
			}
			if (Strings.Trim(fldDescription3.Text) == "")
			{
				fldDescription3.Visible = false;
				fldAmount3.Visible = false;
			}
			if (Strings.Trim(fldDescription4.Text) == "")
			{
				fldDescription4.Visible = false;
				fldAmount4.Visible = false;
			}
			if (Strings.Trim(fldDescription5.Text) == "")
			{
				fldDescription5.Visible = false;
				fldAmount5.Visible = false;
			}
			if (Strings.Trim(fldDescription6.Text) == "")
			{
				fldDescription6.Visible = false;
				fldAmount6.Visible = false;
			}
			if (Strings.Trim(fldDescription7.Text) == "")
			{
				fldDescription7.Visible = false;
				fldAmount7.Visible = false;
			}
			if (Strings.Trim(fldReferenceDescription.Text) == "")
			{
				fldReferenceDescription.Visible = false;
				fldReference.Visible = false;
			}
			if (Strings.Trim(fldControl1Description.Text) == "")
			{
				fldControl1Description.Visible = false;
				fldControl1.Visible = false;
			}
			if (Strings.Trim(fldControl2Description.Text) == "")
			{
				fldControl2Description.Visible = false;
				fldControl2.Visible = false;
			}
			if (Strings.Trim(fldControl3Description.Text) == "")
			{
				fldControl3Description.Visible = false;
				fldControl3.Visible = false;
			}
			curInvoiceTotal += rsInfo.Get_Fields_Decimal("PrinOwed") + rsInfo.Get_Fields_Decimal("TaxOwed");
			curCustomerTotal += rsInfo.Get_Fields_Decimal("PrinOwed") + rsInfo.Get_Fields_Decimal("TaxOwed");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldCustomerTotal.Text = Strings.Format(curCustomerTotal, "#,##0.00");
			curCustomerTotal = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldInvoiceTotal.Text = Strings.Format(curInvoiceTotal, "#,##0.00");
			curInvoiceTotal = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldAddress1.Visible = true;
			fldAddress2.Visible = true;
			fldAddress3.Visible = true;
			fldAddress4.Visible = true;
			fldCustomerNumber.Text = Strings.Format(rsInfo.Get_Fields_Int32("ActualAccountNumber"), "0000");
			fldCustomer.Text = rsInfo.Get_Fields_String("BName");
			fldAddress1.Text = rsInfo.Get_Fields_String("BAddress1");
			fldAddress2.Text = rsInfo.Get_Fields_String("BAddress2");
			fldAddress3.Text = rsInfo.Get_Fields_String("BAddress3");
			if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("BZip4"))) != "")
			{
				fldAddress4.Text = rsInfo.Get_Fields_String("BCity") + ", " + rsInfo.Get_Fields_String("BState") + " " + rsInfo.Get_Fields_String("BZip") + "-" + rsInfo.Get_Fields_String("BZip4");
			}
			else
			{
				fldAddress4.Text = rsInfo.Get_Fields_String("BCity") + ", " + rsInfo.Get_Fields_String("BState") + " " + rsInfo.Get_Fields_String("BZip");
			}
			if (Strings.Trim(fldAddress3.Text) == "")
			{
				fldAddress3.Text = Strings.Trim(fldAddress4.Text);
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress2.Text) == "")
			{
				fldAddress2.Text = Strings.Trim(fldAddress3.Text);
				fldAddress3.Text = Strings.Trim(fldAddress4.Text);
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress1.Text) == "")
			{
				fldAddress1.Text = Strings.Trim(fldAddress2.Text);
				fldAddress2.Text = Strings.Trim(fldAddress3.Text);
				fldAddress3.Text = Strings.Trim(fldAddress4.Text);
				fldAddress4.Text = "";
			}
			if (fldAddress1.Text == "" || frmBillList.InstancePtr.chkPrintAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				fldAddress1.Visible = false;
			}
			if (fldAddress2.Text == "" || frmBillList.InstancePtr.chkPrintAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				fldAddress2.Visible = false;
			}
			if (fldAddress3.Text == "" || frmBillList.InstancePtr.chkPrintAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				fldAddress3.Visible = false;
			}
			if (fldAddress4.Text == "" || frmBillList.InstancePtr.chkPrintAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				fldAddress4.Visible = false;
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			fldInvoiceNumber.Text = rsInfo.Get_Fields_String("InvoiceNumber");
			fldBillDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		public void Init(ref string strSQL)
		{
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				frmReportViewer.InstancePtr.Init(this);
			}
			else
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
                //FC:FINAL:AM:#4409 - close form
                frmBillList.InstancePtr.Close();
                return;
			}
		}

		private void rptBillList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBillList.Caption	= "Bill List";
			//rptBillList.Icon	= "rptBillList.dsx":0000";
			//rptBillList.Left	= 0;
			//rptBillList.Top	= 0;
			//rptBillList.Width	= 11880;
			//rptBillList.Height	= 8595;
			//rptBillList.StartUpPosition	= 3;
			//rptBillList.SectionData	= "rptBillList.dsx":508A;
			//End Unmaped Properties
		}

		private void rptBillList_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}

        private void rptBillList_ReportEnd(object sender, System.EventArgs e)
        {
			rsInfo.DisposeOf();
            frmBillList.InstancePtr.Close();
        }
    }
}
