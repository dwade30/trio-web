﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;
using System.Linq;
using GrapeCity.ActiveReports;
using TWSharedLibrary;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for arARStatusLists.
	/// </summary>
	public partial class arARStatusLists : BaseSectionReport
	{
		public static arARStatusLists InstancePtr
		{
			get
			{
				return (arARStatusLists)Sys.GetInstance(typeof(arARStatusLists));
			}
		}

		protected arARStatusLists _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillingEditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/10/2005              *
		// ********************************************************
		string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[8 + 1];
		// 0 - Tax Due, 1 - Payment Received, 2 - Total Adjustment, 3 - Total Due, 4 - Total Abatement, 5 - Principal, 6 - Tax, 7 - Interest
		double[] dblGroupTotals = new double[8 + 1];
		// 0 - Tax Due, 1 - Payment Received, 2 - Total Adjustment, 3 - Total Due, 4 - Total Abatement, 5 - Principal, 6 - Tax, 7 - Interest
		int lngCount;
		double dblSpecialTotal;
		double dblPDTotal;
		bool boolAdjustedSummary;
		bool boolSummaryOnly;
		int lngExtraRows;
		bool boolShowRegular;
		bool boolFirstRecord;
		bool boolGroupTotals;
		int lngLastAccount;
		string strLastName = "";
		bool boolFix;
		bool boolPrincipal;
		bool boolInterest;
		bool boolTaxes;
		string strPaymentService = "";
		int intTestCT;
		Dictionary<object, object> objDictionary = new Dictionary<object, object>();
		// these are for the summaries in the report footer
		double[] dblYearTotals = new double[4000 + 1];
		// billingyear - 19800
		double[,] dblPayments = new double[4 + 1, 6 + 1];
		// 0 - C, 1 - I, 2 - P, 3 - Total
		// 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
		double[,] dblBPayments = new double[4 + 1, 6 + 1];
		// 0 - C, 1 - I, 2 - P, 3 - Total
		// 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
		double[,] dblPaymentsRev = new double[4 + 1, 6 + 1];
		// 0 - C, 1 - I, 2 - P, 3 - Total
		// 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
		double[,] dblBPaymentsRev = new double[4 + 1, 6 + 1];
		// 0 - C, 1 - I, 2 - P, 3 - Total
		// 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
		public arARStatusLists()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Status Lists";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//		{
		//			Close();
		//			break;
		//		}
		//	} //end switch
		//}
		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				lngCount = 0;
				boolFix = false;
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				boolPrincipal = FCConvert.CBool(frmARStatusList.InstancePtr.chkInclude[0].CheckState == Wisej.Web.CheckState.Checked);
				boolTaxes = FCConvert.CBool(frmARStatusList.InstancePtr.chkInclude[1].CheckState == Wisej.Web.CheckState.Checked);
				boolInterest = FCConvert.CBool(frmARStatusList.InstancePtr.chkInclude[2].CheckState == Wisej.Web.CheckState.Checked);
				if (frmARStatusList.InstancePtr.chkShowPayments.CheckState == Wisej.Web.CheckState.Checked)
				{
					srptSLAllActivityDetailOB.Visible = true;
				}
				else
				{
					srptSLAllActivityDetailOB.Visible = false;
				}
				boolFirstRecord = true;
				if (frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowGroupBy, 1) != "")
				{
					boolGroupTotals = true;
				}
				else
				{
					boolGroupTotals = false;
					GroupFooter1.Visible = false;
				}
				boolSummaryOnly = FCConvert.CBool(frmARStatusList.InstancePtr.chkSummaryOnly.CheckState == Wisej.Web.CheckState.Checked);
				SetupFields();
				// Moves/shows the correct fields into the right places
				SetReportHeader();
                // Sets the titles and moves labels in the header
                GrapeCity.ActiveReports.SectionReportModel.TextBox obNew1 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("fldSummaryTotal");
				//obNew1.Name = "fldSummaryTotal";
				//ReportFooter.Controls.Add(obNew1);
				GrapeCity.ActiveReports.SectionReportModel.Label obNew2 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblPerDiemTotal");
                //obNew2.Name = "lblPerDiemTotal";
                //ReportFooter.Controls.Add(obNew2);
                GrapeCity.ActiveReports.SectionReportModel.Line obNew3 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Line>("lnFooterSummaryTotal");
    //            obNew3.Name = "lnFooterSummaryTotal";
				//ReportFooter.Controls.Add(obNew3);
				strSQL = BuildSQL();
				// Generates the SQL String
				rsData.OpenRecordset(strSQL, modCLCalculations.strARDatabase);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading", true, rsData.RecordCount());
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				lblOutstanding.Text = "-    -    -    Outstanding    -    -    -";
				if (rsData.EndOfFile() && rsData.BeginningOfFile())
				{
					frmWait.InstancePtr.Unload();
					NoRecords();
				}
                else
                {
                    //FC:FINAL:AM:#2785 add fields here
                    List<int> billNumbers = new List<int>();
                    rsData.MoveNext();
                    while (!rsData.EndOfFile())
                    {
                        int billNumber = rsData.Get_Fields_Int32("BillNumber");
                        if (!billNumbers.Contains(billNumber))
                        {
                            billNumbers.Add(billNumber);
                        }
                        rsData.MoveNext();
                    }
                    rsData.MoveFirst();
                    for (int i = 2; i <= billNumbers.Count; i++)
                    {
                        AddSummaryRow(i, 0, 0, true);
                    }
                }
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void NoRecords()
		{
			// this will terminate the report and show a message to the user
			MessageBox.Show("There are no records matching the criteria selected.", "Status List", MessageBoxButtons.OK, MessageBoxIcon.Information);
			this.Close();
			return;
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause = "";
			string strREPPBill = "";
			string strREPPPayment = "";
			string strREInnerJoin = "";
			string strAsOfDateString = "";
			string strSelectFields = "";
			string strBillingType = "";
			string strOrder = "";
			if (modGlobal.Statics.gboolARUseAsOfDate)
			{
				strAsOfDateString = " AND RecordedTransactionDate <= #" + FCConvert.ToString(modGlobal.Statics.gdtARStatusListAsOfDate) + "#";
			}
			else
			{
				strAsOfDateString = "";
			}
			// This is a regular report using the Where Criteria to report the correct information
			// MAL@20071018: Added new order to help with duplicates
			if (Strings.Trim(frmARStatusList.InstancePtr.strRSOrder) != "" && Strings.Trim(Strings.Mid(frmARStatusList.InstancePtr.strRSOrder, 1, frmARStatusList.InstancePtr.strRSOrder.Length)) != "ActualAccountNumber")
			{
				strOrder = frmARStatusList.InstancePtr.strRSOrder + ",ActualAccountNumber";
			}
			else
			{
				strOrder = frmARStatusList.InstancePtr.strRSOrder;
			}
			if (Strings.Trim(frmARStatusList.InstancePtr.strRSWhere) != "")
			{
				if (Strings.Trim(strOrder) != "")
				{
					strTemp = "SELECT * FROM Bill WHERE " + frmARStatusList.InstancePtr.strRSWhere + " ORDER BY " + strOrder;
				}
				else
				{
					strTemp = "SELECT * FROM Bill WHERE " + frmARStatusList.InstancePtr.strRSWhere;
				}
			}
			else
			{
				if (Strings.Trim(strOrder) != "")
				{
					strTemp = "SELECT * FROM Bill ORDER BY " + strOrder;
				}
				else
				{
					strTemp = "SELECT * FROM Bill";
				}
			}
			BuildSQL = strTemp;
			return BuildSQL;
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2;
			float lngHt;
			if (boolSummaryOnly)
			{
				lblInvoiceNumber.Visible = false;
				fldInvoiceNumber.Visible = false;
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldPaymentReceived.Visible = false;
				fldTaxDue.Visible = false;
				fldDue.Visible = false;
				lblAccount.Visible = false;
				lnHeader.Visible = false;
				lnTotals.Visible = false;
				lblFakeHeader1.Visible = true;
				lblFakeHeader2.Visible = true;
				lblFakeHeader3.Visible = true;
				lblFakeHeader4.Visible = true;
				lblFakeHeader5.Visible = true;
				lblFakeHeader6.Visible = true;
				lblFakeHeader7.Visible = true;
				lblTotals.Visible = true;
				fldTotalTaxDue.Visible = true;
				fldTotalPaymentReceived.Visible = true;
				fldTotalDue.Visible = true;
				fldTotalPrincipal.Visible = true;
				fldTotalTax.Visible = true;
				fldTotalInterest.Visible = true;
				GroupHeader1.Height = 0;
				GroupHeader1.Visible = false;
				GroupFooter1.Height = 0;
				GroupFooter1.Visible = false;
				Detail.Height = 0;
				Detail.Visible = false;
				return;
			}
			intRow = 2;
			lngHt = 270 / 1440F;
			lblInvoiceNumber.Visible = true;
			fldInvoiceNumber.Visible = true;
			this.Detail.Height = lngHt + 100 / 1440f;
		}

		private void BindFields()
		{
			var rsPayment = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill the information into the fields
                //clsDRWrapper rsPayment = new clsDRWrapper();
                //	clsDRWrapper rsMaster = new clsDRWrapper();
                string strTemp = "";
                double dblTotalPayment;
                double dblTotalPPayment;
                double dblTotalAbate;
                double dblTotalCorrection;
                DateTime dtPaymentDate;
                bool boolMasterInfo;
                double dblCurrentInterest = 0;
                double dblInterestCharged;
                double dblInterestPaid;
                double dblDue;
                string strSLPayRange = "";
                double dblTest = 0;
                double dblTestInt = 0;
                bool boolShowName;
                string strCode = "";
                double dblPPrin = 0;
                double dblPTax = 0;
                double dblAmtDue;
                double dblAmtDueP;
                double dblAmtDueT;
                double dblAmtDueI;
                bool boolShowOrig = false;
                int lngBillKey;
                double dblChargedInt = 0;
                bool blnIsPrepayment = false;
                TRYNEXTACCOUNT: ;
                //Application.DoEvents();
                // reset fields and variables
                ClearFields();
                // reset the account payments
                FCUtils.EraseSafe(dblBPayments);
                rsPayment.Reset();
                frmWait.InstancePtr.IncrementProgress();
                dblTotalPayment = 0;
                dblTotalCorrection = 0;
                dblTotalAbate = 0;
                dblTotalPPayment = 0;
                dtPaymentDate = DateTime.Today;
                boolShowName = true;
                if (rsData.EndOfFile())
                {
                    srptSLAllActivityDetailOB.Visible = false;
                    return;
                }

                if (modGlobal.Statics.gboolARSLDateRange)
                {
                    strSLPayRange = " AND (RecordedTransactionDate >= '" +
                                    FCConvert.ToString(modGlobal.Statics.gdtARSLPaymentDate1) +
                                    "' AND RecordedTransactionDate <= '" +
                                    FCConvert.ToString(modGlobal.Statics.gdtARSLPaymentDate2) + "')";
                }

                if (frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowPaymentType, 1) != "")
                {
                    if (Strings.UCase(Strings.Left(
                            frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowPaymentType, 1), 2)) ==
                        "PA")
                    {
                        strCode = " AND CODE = 'P'";
                    }
                    else if (Strings.UCase(Strings.Left(
                        frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowPaymentType, 1),
                        2)) == "CO")
                    {
                        strCode = " AND CODE = 'C'";
                    }

                    if (modGlobal.Statics.gboolARUseAsOfDate)
                    {
                        rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " +
                                                rsData.Get_Fields_Int32("AccountKey") + " AND BillKey = " +
                                                rsData.Get_Fields_Int32("ID") + strCode + ") " + strSLPayRange +
                                                " AND isnull(ReceiptNumber, 0) >= 0 AND RecordedTransactionDate <= '" +
                                                FCConvert.ToString(modGlobal.Statics.gdtARStatusListAsOfDate) + "'");
                    }
                    else
                    {
                        rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " +
                                                rsData.Get_Fields_Int32("AccountKey") + " AND BillKey = " +
                                                rsData.Get_Fields_Int32("ID") + strCode + ") " + strSLPayRange +
                                                " AND isnull(ReceiptNumber, 0) >= 0");
                    }

                    if (rsPayment.EndOfFile())
                    {
                        // if there is no payment then go to the next account
                        rsData.MoveNext();
                        goto TRYNEXTACCOUNT;
                    }
                }
                else if (modGlobal.Statics.gboolARSLDateRange)
                {
                    rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " +
                                            rsData.Get_Fields_Int32("AccountKey") + " AND BillKey = " +
                                            rsData.Get_Fields_Int32("ID") + ")" + strSLPayRange +
                                            " AND isnull(ReceiptNumber, 0) >= 0");
                    if (rsPayment.EndOfFile())
                    {
                        // if there is no payment then go to the next account
                        rsData.MoveNext();
                        goto TRYNEXTACCOUNT;
                    }
                }

                fldName.Text = rsData.Get_Fields_String("BName");
                fldAccount.Text = rsData.Get_Fields_String("ActualAccountNumber");
                if (Conversion.Val(fldAccount.Text) == lngLastAccount && Strings.UCase(Strings.Trim(fldName.Text)) ==
                    Strings.UCase(Strings.Trim(strLastName)))
                {
                    HideNameField_2(true);
                }
                else
                {
                    HideNameField_2(false);
                }

                dblTotalPayment = 0;
                // MAL@20071018: Add check for duplicates
                lngBillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
                if (modGlobal.Statics.gboolARUseAsOfDate)
                {
                    // MAL@20070911: Added check for bill date being after As Of Date
                    // MAL@20070917: Added check for return being a date before checking against As Of Date
                    if (Information.IsDate(GetBillingDate_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillNumber")))))
                    {
                        if (Convert.ToDateTime(
                                GetBillingDate_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillNumber")))) <=
                            modGlobal.Statics.gdtARStatusListAsOfDate)
                        {
                            fldInvoiceNumber.Text = rsData.Get_Fields_String("InvoiceNumber");
                            // find the information about the payments here
                            strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " +
                                      rsData.Get_Fields_Int32("AccountKey") + " AND BillKey = " +
                                      rsData.Get_Fields_Int32("ID") + ") " + strSLPayRange +
                                      " AND isnull(ReceiptNumber, 0) >= 0 AND RecordedTransactionDate <= '" +
                                      FCConvert.ToString(modGlobal.Statics.gdtARStatusListAsOfDate) + "'";
                            if (frmARStatusList.InstancePtr.boolShowCurrentInterest)
                            {
                                dblDue = modARCalculations.CalculateAccountAR(rsData,
                                    modGlobal.Statics.gdtARStatusListAsOfDate, ref dblCurrentInterest);
                            }
                        }
                        else
                        {
                            // MAL@20080125: Check if any pre-payments existed as of the date entered
                            // Tracker Reference: 12076
                            fldInvoiceNumber.Text = "Not Billed";
                            blnIsPrepayment = true;
                            strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " +
                                      rsData.Get_Fields_Int32("AccountKey") + " AND BillKey = " +
                                      rsData.Get_Fields_Int32("ID") + ") " + strSLPayRange +
                                      " AND isnull(ReceiptNumber, 0) >= 0 AND RecordedTransactionDate <= '" +
                                      FCConvert.ToString(modGlobal.Statics.gdtARStatusListAsOfDate) +
                                      "' AND Reference = 'PREPAY-A'";
                            // rsData.MoveNext
                            // GoTo TRYNEXTACCOUNT
                        }

                        // MAL@20071017: As Of Date Was Not Including Pre-Payments Correctly
                        // Tracker Ref: 10929
                    }
                    else if (GetBillingDate_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillNumber"))) == "Not Billed")
                    {
                        rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE (AccountKey = " +
                                                rsData.Get_Fields_Int32("AccountKey") + " AND BillKey = " +
                                                rsData.Get_Fields_Int32("ID") + ") " + strSLPayRange +
                                                " AND isnull(ReceiptNumber, 0) >= 0 AND RecordedTransactionDate <= '" +
                                                FCConvert.ToString(modGlobal.Statics.gdtARStatusListAsOfDate) + "'");
                        if (!rsPayment.EndOfFile())
                        {
                            fldInvoiceNumber.Text = "Not Billed";
                            // TODO: Field [Bill.ID] not found!! (maybe it is an alias?)
                            strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " +
                                      rsData.Get_Fields_Int32("AccountKey") + " AND BillKey = " +
                                      rsData.Get_Fields("Bill.ID") + ") " + strSLPayRange +
                                      " AND isnull(ReceiptNumber, 0) >= 0 AND RecordedTransactionDate <= '" +
                                      FCConvert.ToString(modGlobal.Statics.gdtARStatusListAsOfDate) + "'";
                            if (frmARStatusList.InstancePtr.boolShowCurrentInterest)
                            {
                                dblDue = modARCalculations.CalculateAccountAR(rsData,
                                    modGlobal.Statics.gdtARStatusListAsOfDate, ref dblCurrentInterest);
                            }
                        }
                        else
                        {
                            rsData.MoveNext();
                            goto TRYNEXTACCOUNT;
                        }
                    }
                    else
                    {
                        rsData.MoveNext();
                        goto TRYNEXTACCOUNT;
                    }
                }
                else
                {
                    fldInvoiceNumber.Text = rsData.Get_Fields_String("InvoiceNumber");
                    // find the information about the payments here
                    strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") +
                              " AND BillKey = " + rsData.Get_Fields_Int32("ID") + strSLPayRange +
                              " AND isnull(ReceiptNumber, 0) >= 0)";
                    if (frmARStatusList.InstancePtr.boolShowCurrentInterest)
                    {
                        dblDue = modARCalculations.CalculateAccountAR(rsData, DateTime.Today, ref dblCurrentInterest);
                    }
                }

                // 0 - C, 1 - I, 2 - P, 3 - Total
                // 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
                AddToPaymentArray_242(3, 0, 0, 0, dblCurrentInterest * -1);
                // current interest
                // open the payment records
                rsPayment.OpenRecordset(strTemp, modGlobal.DEFAULTDATABASE);
                if (rsPayment.EndOfFile() != true)
                {
                    // add up all of the payment records
                    while (!rsPayment.EndOfFile())
                    {
                        dblPPrin = 0;
                        dblPTax = 0;
                        dblChargedInt = 0;
                        if (boolPrincipal)
                        {
                            // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            dblPPrin = FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
                        }

                        if (boolTaxes)
                        {
                            // TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            dblPTax = FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
                        }

                        if (boolInterest)
                        {
                            // TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            dblChargedInt = FCConvert.ToDouble(rsPayment.Get_Fields("Interest"));
                        }

                        // 0 - C, 1 - I, 2 - P, 3 - Total
                        // 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
                        if (dblPPrin != 0 || dblPTax != 0 || dblChargedInt != 0)
                        {
                            if ((Strings.UCase(rsPayment.Get_Fields_String("Code")) == "P") ||
                                (Strings.UCase(rsPayment.Get_Fields_String("Code")) == "Y"))
                            {
                                dblTotalPayment += (dblPPrin + dblPTax + dblChargedInt);
                                AddToPaymentArray_164(2, dblPPrin, dblPTax, dblChargedInt, 0);
                                dblTotalPPayment += (dblPPrin + dblPTax + dblChargedInt);
                            }
                            else if (Strings.UCase(rsPayment.Get_Fields_String("Code")) == "C")
                            {
                                dblTotalAbate += (dblPPrin + dblPTax + dblChargedInt);
                                AddToPaymentArray_164(0, dblPPrin, dblPTax, dblChargedInt, 0);
                                dblTotalCorrection += (dblPPrin + dblPTax + dblChargedInt);
                            }
                            else if (Strings.UCase(rsPayment.Get_Fields_String("Code")) == "I")
                            {
                                // dblTotalAbate = dblTotalAbate + (dblPPrin + dblPTax + dblChargedInt)
                                AddToPaymentArray_164(1, dblPPrin, dblPTax, dblChargedInt, 0);
                            }
                        }

                        rsPayment.MoveNext();
                    }
                }
                else if (blnIsPrepayment)
                {
                    ReversePaymentsFromStatusArray(ref rsPayment);
                    rsData.MoveNext();
                    // clear fields
                    goto TRYNEXTACCOUNT;
                }
                else
                {
                    // if no payment records, then make everything zeros
                    fldPaymentReceived.Text = "0.00";
                }

                // this is the current balance due
                // this is the original tax due
                dblAmtDue = 0;
                dblAmtDueP = 0;
                dblAmtDueT = 0;
                dblAmtDueI = 0;
                if (modGlobal.Statics.gboolARUseAsOfDate)
                {
                    if (Strings.Left(fldInvoiceNumber.Text, 3) != "Not")
                    {
                        boolShowOrig =
                            FCConvert.CBool(
                                DateAndTime.DateValue(
                                        GetBillingDate_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillNumber"))))
                                    .ToOADate() <= modGlobal.Statics.gdtARStatusListAsOfDate.ToOADate());
                    }
                    else if (blnIsPrepayment)
                    {
                        boolShowOrig = false;
                    }
                    else
                    {
                        boolShowOrig = true;
                    }
                }
                else
                {
                    boolShowOrig = true;
                }

                if (boolShowOrig)
                {
                    // DJW changed + to - when adding in interest since it is a negative number to start
                    if (frmARStatusList.InstancePtr.boolShowCurrentInterest && boolInterest)
                    {
                        // TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
                        dblAmtDueI = (FCConvert.ToDouble(rsData.Get_Fields("IntAdded")) * -1) + dblCurrentInterest;
                        // TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
                        // TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
                        dblAmtDue = (FCConvert.ToDouble(rsData.Get_Fields("IntAdded")) * -1) + dblCurrentInterest;
                        // - rsData.Get_Fields("IntPaid")
                    }
                    else if (boolInterest)
                    {
                        // TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
                        dblAmtDueI = (FCConvert.ToDouble(rsData.Get_Fields("IntAdded")) * -1);
                        // TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
                        // TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
                        dblAmtDue = (FCConvert.ToDouble(rsData.Get_Fields("IntAdded")) * -1);
                        // - rsData.Get_Fields("IntPaid")
                    }

                    if (boolPrincipal)
                    {
                        dblAmtDueP = FCConvert.ToDouble(rsData.Get_Fields_Decimal("PrinOwed"));
                        dblAmtDue += FCConvert.ToDouble(rsData.Get_Fields_Decimal("PrinOwed"));
                    }

                    if (boolTaxes)
                    {
                        dblAmtDueT = FCConvert.ToDouble(rsData.Get_Fields_Decimal("TaxOwed"));
                        dblAmtDue += FCConvert.ToDouble(rsData.Get_Fields_Decimal("TaxOwed"));
                    }
                }

                fldTaxDue.Text = Strings.Format(dblAmtDue - dblCurrentInterest, "#,##0.00");
                // this is the total amount due
                fldDue.Text = Strings.Format(dblAmtDue - (dblTotalPayment + dblTotalAbate), "#,##0.00");
                // + dblTotalRefundAbate
                if (modGlobal.Statics.gboolARSLDateRange)
                {
                    dblTest = modARCalculations.CalculateAccountAR(rsData, DateTime.Today, ref dblTestInt);
                    if (!frmARStatusList.InstancePtr.boolShowCurrentInterest)
                    {
                        dblTest -= dblTestInt;
                    }

                    fldDue.Text = Strings.Format(dblTest, "#,##0.00");
                }

                if (modGlobal.Statics.gboolARUseAsOfDate)
                {
                    if (!blnIsPrepayment)
                    {
                        // MAL@20080125
                        // show the amounts owed for prin/tax/int/costs
                        // 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
                        if (boolPrincipal)
                        {
                            fldPrincipal.Text = Strings.Format(dblAmtDueP - GetBillPaymentAmount_2(0), "#,##0.00");
                        }
                        else
                        {
                            fldPrincipal.Text = "0.00";
                        }

                        if (boolTaxes)
                        {
                            fldTax.Text = Strings.Format(dblAmtDueT - GetBillPaymentAmount_2(3), "#,##0.00");
                        }
                        else
                        {
                            fldTax.Text = "0.00";
                        }

                        if (frmARStatusList.InstancePtr.boolShowCurrentInterest && boolInterest)
                        {
                            fldInterest.Text =
                                Strings.Format(dblAmtDueI - GetBillPaymentAmount_2(1) - GetBillPaymentAmount_2(2),
                                    "#,##0.00");
                            // + dblCurrentInterest
                        }
                        else
                        {
                            fldInterest.Text = "0.00";
                        }
                    }
                    else
                    {
                        // MAL@20080311: Change to show pre-payment amount in the Outstanding Principal
                        // Tracker Reference: 12674
                        // fldPrincipal.Text = "0.00"
                        fldPrincipal.Text = Strings.Format((dblTotalPayment * -1), "#,##0.00");
                        fldTax.Text = "0.00";
                        fldInterest.Text = "0.00";
                    }
                }
                else
                {
                    // show the amounts owed for prin/tax/int/costs
                    if (boolPrincipal)
                    {
                        // TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
                        fldPrincipal.Text =
                            Strings.Format(dblAmtDueP - FCConvert.ToDouble(rsData.Get_Fields("PrinPaid")), "#,##0.00");
                    }
                    else
                    {
                        fldPrincipal.Text = "0.00";
                    }

                    if (boolTaxes)
                    {
                        // TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
                        fldTax.Text = Strings.Format(dblAmtDueT - FCConvert.ToDouble(rsData.Get_Fields("TaxPaid")),
                            "#,##0.00");
                    }
                    else
                    {
                        fldTax.Text = "0.00";
                    }

                    if (boolInterest)
                    {
                        // TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
                        fldInterest.Text = Strings.Format(dblAmtDueI - FCConvert.ToDouble(rsData.Get_Fields("IntPaid")),
                            "#,##0.00");
                    }
                    else
                    {
                        fldInterest.Text = "0.00";
                    }
                }

                // this is how much was recieved in the payments for Principal, Interest and Costs
                fldPaymentReceived.Text =
                    Strings.Format(dblTotalPayment + dblTotalAbate - dblCurrentInterest, "#,##0.00");
                // now check to make sure that this account matches the balance due criteria, if not go to the next account
                if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowBalanceDue, 1)) !=
                    "" && Strings.Trim(
                        frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowBalanceDue, 2)) != "")
                {
                    // Check the balance due
                    if (CheckBalanceDue_2(FCConvert.ToDouble(fldDue.Text)))
                    {
                        // this is ok keep going
                        ResetReversalValues();
                    }
                    else
                    {
                        // reverse the payments that have already been calculated for this account so that they are not shown in the payment summary totals at the bottom
                        ReversePaymentsFromStatusArray(ref rsPayment);
                        rsData.MoveNext();
                        // clear fields
                        goto TRYNEXTACCOUNT;
                    }
                }

                srptSLAllActivityDetailOB.Report = new srptARSLAllActivityDetail();
                srptSLAllActivityDetailOB.Report.UserData = rsData.Get_Fields_Int32("ID");
                lngLastAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(fldAccount.Text)));
                strLastName = Strings.Trim(fldName.Text);
                lngCount += 1;
                // overall totals
                dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
                dblTotals[1] += FCConvert.ToDouble(fldPaymentReceived.Text);
                dblTotals[2] += dblTotalAbate;
                if (Strings.Left(fldDue.Text, 2) != "**")
                {
                    dblTotals[3] += FCConvert.ToDouble(fldDue.Text);
                    // keep track for the year totals
                    // MAL@20070912: Add check for lien and use the lien Rate Record instead of billing key
                    dblYearTotals[rsData.Get_Fields_Int32("BillNumber")] += FCConvert.ToDouble(fldDue.Text);
                }
                else
                {
                    dblTotals[3] += FCConvert.ToDouble(Strings.Right(fldDue.Text, fldDue.Text.Length - 2));
                    // keep track for the year totals
                    dblYearTotals[rsData.Get_Fields_Int32("BillNumber")] +=
                        FCConvert.ToDouble(Strings.Right(fldDue.Text, fldDue.Text.Length - 2));
                }

                // Principal
                dblTotals[4] += FCConvert.ToDouble(fldPrincipal.Text);
                // Tax
                dblTotals[5] += FCConvert.ToDouble(fldTax.Text);
                // Interest
                dblTotals[6] += FCConvert.ToDouble(fldInterest.Text);
                // group totals
                dblGroupTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
                dblGroupTotals[1] += FCConvert.ToDouble(fldPaymentReceived.Text);
                dblGroupTotals[2] += dblTotalAbate;
                if (Strings.Left(fldDue.Text, 2) != "**")
                {
                    dblGroupTotals[3] += FCConvert.ToDouble(fldDue.Text);
                }
                else
                {
                    dblGroupTotals[3] += FCConvert.ToDouble(Strings.Right(fldDue.Text, fldDue.Text.Length - 2));
                }

                // Principal
                dblGroupTotals[4] += FCConvert.ToDouble(fldPrincipal.Text);
                // Tax
                dblGroupTotals[5] += FCConvert.ToDouble(fldTax.Text);
                // Interest
                dblGroupTotals[6] += FCConvert.ToDouble(fldInterest.Text);
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                if (Information.Err(ex).Number == -2147418105)
                {
                    // do nothing
                }
                else
                {
                    MessageBox.Show(
                        "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                        Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK,
                        MessageBoxIcon.Hand);
                }
            }
            finally
            {
				rsPayment.Dispose();
            }
		}

		private bool CheckBalanceDue_2(double dblAmt)
		{
			return CheckBalanceDue(ref dblAmt);
		}

		private bool CheckBalanceDue(ref double dblAmt)
		{
			bool CheckBalanceDue = false;
			// this function will return true if this amount matches the
			double dblTestAmt = 0;
			dblTestAmt = FCConvert.ToDouble(frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowBalanceDue, 2));
			string vbPorterVar = frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowBalanceDue, 1);
			if (vbPorterVar == ">")
			{
				CheckBalanceDue = dblAmt > dblTestAmt;
			}
			else if (vbPorterVar == "<")
			{
				CheckBalanceDue = dblAmt < dblTestAmt;
			}
			else if (vbPorterVar == "<>")
			{
				CheckBalanceDue = dblAmt != dblTestAmt;
			}
			else if (vbPorterVar == "=")
			{
				CheckBalanceDue = dblAmt == dblTestAmt;
			}
			else
			{
				CheckBalanceDue = false;
			}
			return CheckBalanceDue;
		}

		private void ClearFields()
		{
			// this routine will clear the fields
			fldAccount.Text = "";
			fldName.Text = "";
			fldInvoiceNumber.Text = "";
			fldTaxDue.Text = "";
			fldPaymentReceived.Text = "";
			fldDue.Text = "";
			fldPrincipal.Text = "";
			fldInterest.Text = "";
			fldTax.Text = "";
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!boolFirstRecord)
				{
					rsData.MoveNext();
				}
				else
				{
					boolFirstRecord = false;
				}
				BindFields();
				if (rsData.EndOfFile())
				{
					Detail.Height = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				if (Information.Err(ex).Number > 0)
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void SetupGroupTotals()
		{
			fldGroupTaxDue.Text = Strings.Format(dblGroupTotals[0], "#,##0.00");
			fldGroupPayments.Text = Strings.Format(dblGroupTotals[1], "#,##0.00");
			fldGroupTotal.Text = Strings.Format(dblGroupTotals[3], "#,##0.00");
			fldGroupPrincipal.Text = Strings.Format(dblGroupTotals[4], "#,##0.00");
			fldGroupTax.Text = Strings.Format(dblGroupTotals[5], "#,##0.00");
			fldGroupInterest.Text = Strings.Format(dblGroupTotals[6], "#,##0.00");
			lblGroupTotals.Text = "Group Total:";
		}

		private void SetupTotals()
		{
			int intSumRows;
			int intCT;
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew1;
			GrapeCity.ActiveReports.SectionReportModel.Label obNew2;
			GrapeCity.ActiveReports.SectionReportModel.Line obNew3;
			// this sub will fill in the totals line at the bottom of the report
			SetupTotalSummary();
			// Load Summary List
			intSumRows = 1;
			for (intCT = 0; intCT <= Information.UBound(dblYearTotals, 1) - 1; intCT++)
			{
				if (dblYearTotals[intCT] != 0)
				{
					AddSummaryRow(intSumRows, dblYearTotals[intCT], intCT, false);
					intSumRows += 1;
				}
			}
			fldTotalTaxDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotalDue.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotalPrincipal.Text = Strings.Format(dblTotals[4], "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTotals[5], "#,##0.00");
			fldTotalInterest.Text = Strings.Format(dblTotals[6], "#,##0.00");
			if (lngCount > 1)
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
			}
			else if (lngCount == 1)
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bill:";
			}
			else
			{
				lblTotals.Text = "No Bills";
			}
			// create the total fields and fill them
			// add a field
			//ReportFooter.Controls.Add(obNew1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox());
			//obNew1.Name = "fldSummaryTotal";
			obNew1 = ReportFooter.Controls["fldSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
			obNew1.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
			obNew1.Left = fldSummary1.Left;
			obNew1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
			obNew1.Width = fldSummary1.Width;
			obNew1.Font = lblSummary1.Font;
			obNew1.Text = Strings.Format(dblPDTotal, "#,##0.00");
			//obNew1.Name = "fldSummaryTotal";
			obNew1.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
			obNew1.Left = fldSummary1.Left;
			obNew1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
			obNew1.Width = fldSummary1.Width;
			obNew1.Font = lblSummary1.Font;
			obNew1.Text = Strings.Format(dblPDTotal, "#,##0.00");
			// add a label
			//ReportFooter.Controls.Add(obNew2 = new GrapeCity.ActiveReports.SectionReportModel.Label());
			//obNew2.Name = "lblPerDiemTotal";
			obNew2 = ReportFooter.Controls["lblPerDiemTotal"] as GrapeCity.ActiveReports.SectionReportModel.Label;
			obNew2.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
			obNew2.Left = lblSummary1.Left;
			obNew2.Font = lblSummary1.Font;
			obNew2.Value = "Total";
			obNew2.Name = "lblPerDiemTotal";
			obNew2.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
			obNew2.Left = lblSummary1.Left;
			obNew2.Font = lblSummary1.Font;
			obNew2.Text = "Total";
			// add a line
			//ReportFooter.Controls.Add(obNew3 = new GrapeCity.ActiveReports.SectionReportModel.Line());
			//obNew3.Name = "lnFooterSummaryTotal";
			obNew3 = ReportFooter.Controls["lnFooterSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.Line;
			obNew3.X1 = fldSummary1.Left;
			obNew3.X2 = fldSummary1.Left + fldSummary1.Width;
			obNew3.Y1 = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
			obNew3.Y2 = obNew3.Y1;
			ReportFooter.Height = lblSummary1.Top + (intSumRows * lblSummary1.Height);
			lngExtraRows = FCConvert.ToInt32(ReportFooter.Height);
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			SetupGroupTotals();
			dblGroupTotals[0] = 0;
			dblGroupTotals[1] = 0;
			dblGroupTotals[3] = 0;
			dblGroupTotals[5] = 0;
			dblGroupTotals[6] = 0;
			dblGroupTotals[7] = 0;
			dblGroupTotals[8] = 0;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				SetupTotals();
				lblSpecialTotal.Visible = false;
				lblSpecialText.Visible = false;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Footer Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
			//clsDRWrapper rsTitle = new clsDRWrapper();
			string strTypes = "";
			for (intCT = 0; intCT <= frmARStatusList.InstancePtr.vsWhere.Rows - 1; intCT++)
			{
				if (frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) != "" || frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2) != "")
				{
					switch (intCT)
					{
						case frmARStatusList.lngRowAccount:
							{
								// Account Number
								if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += "Account: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
										}
										else
										{
											strTemp += "Account: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
										}
									}
									else
									{
										strTemp += "Below Account: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								}
								else
								{
									strTemp += "Above Account: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
								break;
							}
						case frmARStatusList.lngRowBillType:
							{
								// rsTitle.OpenRecordset "SELECT * FROM DefaultBillTypes WHERE TypeCode = " & Val(.TextMatrix(intCT, 1)), strARDatabase
								// If rsTitle.EndOfFile <> True And rsTitle.BeginningOfFile <> True Then
								// strTemp = strTemp & " Bill Type: " & rsTitle.Get_Fields("TypeTitle")
								// Else
								// strTemp = strTemp & " Bill Type: " & .TextMatrix(intCT, 1)
								// End If
								if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += "Bill Type: " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1))) + " ";
										}
										else
										{
											strTemp += "Bill Type: " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1))) + " To " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)));
										}
									}
									else
									{
										strTemp += "Below Bill Type: " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)));
									}
								}
								else
								{
									strTemp += "Above Bill Type: " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)));
								}
								break;
							}
						case frmARStatusList.lngRowName:
							{
								// Name
								if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) != "")
									{
										if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += " Name: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
										}
										else
										{
											strTemp += " Name: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
										}
									}
									else
									{
										strTemp += " Below Name: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								}
								else
								{
									strTemp += " Above Name: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
								break;
							}
						case frmARStatusList.lngRowRateKey:
							{
								// Rate Record
								if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) != "")
									{
										if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += " Rate Record: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
										}
										else
										{
											strTemp += " Rate Record: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
										}
									}
									else
									{
										strTemp += " Below Rate Record: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								}
								else
								{
									strTemp += " Above Rate Record: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
								break;
							}
						case frmARStatusList.lngRowBalanceDue:
							{
								// Balance Due
								strTemp += " Balance Due " + Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) + " " + Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2));
								// Case frmARStatusList.lngRowAsOfDate
								// strTemp = strTemp & " As Of Date : " & Trim(.TextMatrix(intCT, 1))
								break;
							}
						case frmARStatusList.lngRowPaymentType:
							{
								strTemp += " Payment Type : " + Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1));
								break;
							}
						case frmARStatusList.lngRowShowPaymentFrom:
							{
								strTemp += " Show Payments From: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
								break;
							}
						default:
							{
								// Exit For
								break;
							}
					}
					//end switch
				}
			}
			if (frmARStatusList.InstancePtr.lstTypes.SelectedItems.Any())
			{
				strTypes = "";
				for (intCT = 0; intCT <= frmARStatusList.InstancePtr.lstTypes.Items.Count - 1; intCT++)
				{
					if (frmARStatusList.InstancePtr.lstTypes.Selected(intCT))
					{
						strTypes += FCConvert.ToString(frmARStatusList.InstancePtr.lstTypes.ItemData(intCT)) + ",";
					}
				}
				if (strTypes.Length > 1)
				{
					if (strTemp != "")
					{
						strTemp += " Bill Types: " + Strings.Left(strTypes, strTypes.Length - 1);
					}
					else
					{
						strTemp = "Bill Types: " + Strings.Left(strTypes, strTypes.Length - 1);
					}
				}
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List";
			}
			else
			{
				lblReportType.Text = strTemp;
			}
			lblReportType.Text = lblReportType.Text + " Showing :";
			if (boolPrincipal)
			{
				lblReportType.Text = lblReportType.Text + " Principal";
			}
			if (boolTaxes)
			{
				lblReportType.Text = lblReportType.Text + " Tax";
			}
			if (boolInterest)
			{
				lblReportType.Text = lblReportType.Text + " Interest";
			}
			lblHeader.Text = "Status List";
			if (modGlobal.Statics.gboolARUseAsOfDate)
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As Of Date: " + Strings.Format(modGlobal.Statics.gdtARStatusListAsOfDate, "MM/dd/yyyy");
			}
		}
		// vbPorter upgrade warning: intRNum As short	OnWriteFCConvert.ToInt32(
		private void AddSummaryRow(int intRNum, double dblAmount, int lngYear, bool createObjects)
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew1;
			GrapeCity.ActiveReports.SectionReportModel.Label obNew2;
			// this will add another per diem line in the report footer
			if (intRNum == 1)
			{
				fldSummary1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fldSummary1.Text = Strings.Format(dblAmount, "#,##0.00");
				// MAL@20080129: Check for As of Date
				// Tracker Reference: 12076
				if (modGlobal.Statics.gboolARUseAsOfDate)
				{
					if (lngYear > 0)
					{
						if (DateAndTime.DateValue(GetBillingDate(ref lngYear)).ToOADate() > modGlobal.Statics.gdtARStatusListAsOfDate.ToOADate())
						{
							lblSummary1.Text = "Not Billed";
						}
						else
						{
							lblSummary1.Text = GetBillingDescription(ref lngYear);
						}
					}
					else
					{
						lblSummary1.Text = "Not Billed";
					}
				}
				else
				{
					lblSummary1.Text = GetBillingDescription(ref lngYear);
				}
				dblPDTotal += dblAmount;
			}
			else
			{
				if (createObjects)
				{
					// add a field
					obNew1 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("fldSummary" + FCConvert.ToString(intRNum));
					//obNew1.Name = "fldSummary" + FCConvert.ToString(intRNum);
					//ReportFooter.Controls.Add(obNew1);
				}
				else
				{
					obNew1 = ReportFooter.Controls["fldSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				}
				if (obNew1 != null)
				{
					//obNew1.Name = "fldSummary" + FCConvert.ToString(intRNum);
					obNew1.Top = fldSummary1.Top + ((intRNum - 1) * fldSummary1.Height);
					obNew1.Left = fldSummary1.Left;
					obNew1.Width = fldSummary1.Width;
					obNew1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew1.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obNew1.Text = Strings.Format(dblAmount, "#,##0.00");
					dblPDTotal += dblAmount;
				}
				if (createObjects)
				{
					obNew2 = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblSummary" + FCConvert.ToString(intRNum));
					//obNew2.Name = "lblSummary" + FCConvert.ToString(intRNum);
					//ReportFooter.Controls.Add(obNew2);
				}
				else
				{
					obNew2 = ReportFooter.Controls["lblSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
				}
				if (obNew2 != null)
				{
					//obNew2.Name = "lblSummary" + FCConvert.ToString(intRNum);
					obNew2.Top = lblSummary1.Top + ((intRNum - 1) * lblSummary1.Height);
					obNew2.Left = lblSummary1.Left;
					obNew2.Width = lblSummary1.Width;
					obNew2.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
				}
				// MAL@20080129: Check for As of Date
				// Tracker Reference: 12076
				if (modGlobal.Statics.gboolARUseAsOfDate)
				{
					if (DateAndTime.DateValue(GetBillingDate(ref lngYear)).ToOADate() > modGlobal.Statics.gdtARStatusListAsOfDate.ToOADate())
					{
						obNew2.Text = "Not Billed";
					}
					else
					{
						obNew2.Text = GetBillingDescription(ref lngYear);
					}
				}
				else
				{
					obNew2.Text = GetBillingDescription(ref lngYear);
				}
			}
		}

		private void SetupTotalSummary()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the summary labels at the bottom of the page
				// and hide/show the labels when needed
				int intCT;
				int intRow;
				// this will keep track of the row  that I am adding values to
				string strDesc = "";
				double[] dblTotal = new double[6 + 1];
				double dblSubTotal;
				bool boolAffectThis = false;
				bool boolShowSubtotal = false;
				// fill in the titles
				lblSumHeaderType.Text = "Type";
				lblSumHeaderPrin.Text = "Principal";
				lblSumHeaderTax.Text = "Tax";
				lblSumHeaderInt.Text = "Interest";
				lblSumHeaderTotal.Text = "Total";
				intRow = 1;
				// start at the first row
				// 0 - C, 1 - I, 2 - P, 3 - Total
				// 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
				for (intCT = 0; intCT <= 3; intCT++)
				{
					// this will fill the totals element
					dblPayments[intCT, 4] = dblPayments[intCT, 0] + dblPayments[intCT, 1] + dblPayments[intCT, 2] + dblPayments[intCT, 3];
				}
				// first set of payments
				if (dblPayments[2, 4] != 0)
				{
					strDesc = "P - Payment";
					boolAffectThis = true;
					boolShowSubtotal = true;
					if (boolAffectThis)
					{
						FillSummaryLine(ref intRow, ref strDesc, ref dblPayments[2, 0], ref dblPayments[2, 3], ref dblPayments[2, 1], ref dblPayments[2, 2], ref dblPayments[2, 4]);
						dblTotal[0] += dblPayments[2, 0];
						// this will total all of the seperated payments for the total line
						dblTotal[1] += dblPayments[2, 1];
						dblTotal[2] += dblPayments[2, 2];
						dblTotal[3] += dblPayments[2, 3];
						dblTotal[4] += dblPayments[2, 4];
						dblTotal[5] += dblPayments[2, 5];
						intRow += 1;
					}
				}
				if (boolShowSubtotal)
				{
					// create a subtotal line if needed
					FillSummaryLine_6(intRow, "Subtotal", dblTotal[0], dblTotal[3], dblTotal[1], dblTotal[2], dblTotal[4]);
					SetSummarySubTotalLine(ref intRow);
					intRow += 1;
				}
				else
				{
					lnSubtotal.Visible = false;
				}
				// the second set of payment types
				for (intCT = 0; intCT <= 1; intCT++)
				{
					boolAffectThis = false;
					if (dblPayments[intCT, 4] != 0)
					{
						switch (intCT)
						{
							case 0:
								{
									strDesc = "C - Correction";
									boolAffectThis = true;
									break;
								}
							case 1:
								{
									if (boolInterest)
									{
										strDesc = "I - Interest Charged";
										boolAffectThis = true;
									}
									break;
								}
						}
						//end switch
						if (boolAffectThis)
						{
							FillSummaryLine(ref intRow, ref strDesc, ref dblPayments[intCT, 0], ref dblPayments[intCT, 3], ref dblPayments[intCT, 1], ref dblPayments[intCT, 2], ref dblPayments[intCT, 4]);
							dblTotal[0] += dblPayments[intCT, 0];
							// this will total all of the seperated payments for the total line
							dblTotal[1] += dblPayments[intCT, 1];
							dblTotal[2] += dblPayments[intCT, 2];
							dblTotal[3] += dblPayments[intCT, 3];
							dblTotal[4] += dblPayments[intCT, 4];
							dblTotal[5] += dblPayments[intCT, 5];
							intRow += 1;
						}
					}
				}
				// show the total line
				FillSummaryLine_6(intRow, "Total", dblTotal[0], dblTotal[3], dblTotal[1], dblTotal[2], dblTotal[4]);
				SetSummaryTotalLine(FCConvert.ToInt16(intRow));
				for (intCT = intRow + 1; intCT <= 11; intCT++)
				{
					HideSummaryRow(FCConvert.ToInt16(intCT));
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Summary Table", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void SetSummaryTotalLine(short intRw)
		{
			switch (intRw)
			{
				case 1:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal1.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal1.Top;
						break;
					}
				case 2:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal2.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal2.Top;
						break;
					}
				case 3:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal3.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal3.Top;
						break;
					}
				case 4:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal4.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal4.Top;
						break;
					}
				case 5:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal5.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal5.Top;
						break;
					}
				case 6:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal6.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal6.Top;
						break;
					}
				case 7:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal7.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal7.Top;
						break;
					}
				case 8:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal8.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal8.Top;
						break;
					}
				case 9:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal9.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal9.Top;
						break;
					}
				case 10:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal10.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal10.Top;
						break;
					}
				case 11:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal11.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal11.Top;
						break;
					}
				case 12:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal12.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal12.Top;
						break;
					}
				case 13:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal13.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal13.Top;
						break;
					}
			}
			//end switch
		}
		// vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void SetSummarySubTotalLine(ref int intRw)
		{
			lnSubtotal.X1 = lblSumHeaderTotal.Left;
			lnSubtotal.X2 = lblSumHeaderTotal.Left + lblSumHeaderTotal.Width;
			switch (intRw)
			{
				case 1:
					{
						lnSubtotal.Y1 = lblSummaryTotal1.Top;
						lnSubtotal.Y2 = lblSummaryTotal1.Top;
						break;
					}
				case 2:
					{
						lnSubtotal.Y1 = lblSummaryTotal2.Top;
						lnSubtotal.Y2 = lblSummaryTotal2.Top;
						break;
					}
				case 3:
					{
						lnSubtotal.Y1 = lblSummaryTotal3.Top;
						lnSubtotal.Y2 = lblSummaryTotal3.Top;
						break;
					}
				case 4:
					{
						lnSubtotal.Y1 = lblSummaryTotal4.Top;
						lnSubtotal.Y2 = lblSummaryTotal4.Top;
						break;
					}
				case 5:
					{
						lnSubtotal.Y1 = lblSummaryTotal5.Top;
						lnSubtotal.Y2 = lblSummaryTotal5.Top;
						break;
					}
				case 6:
					{
						lnSubtotal.Y1 = lblSummaryTotal6.Top;
						lnSubtotal.Y2 = lblSummaryTotal6.Top;
						break;
					}
				case 7:
					{
						lnSubtotal.Y1 = lblSummaryTotal7.Top;
						lnSubtotal.Y2 = lblSummaryTotal7.Top;
						break;
					}
				case 8:
					{
						lnSubtotal.Y1 = lblSummaryTotal8.Top;
						lnSubtotal.Y2 = lblSummaryTotal8.Top;
						break;
					}
				case 9:
					{
						lnSubtotal.Y1 = lblSummaryTotal9.Top;
						lnSubtotal.Y2 = lblSummaryTotal9.Top;
						break;
					}
				case 10:
					{
						lnSubtotal.Y1 = lblSummaryTotal10.Top;
						lnSubtotal.Y2 = lblSummaryTotal10.Top;
						break;
					}
				case 11:
					{
						lnSubtotal.Y1 = lblSummaryTotal11.Top;
						lnSubtotal.Y2 = lblSummaryTotal11.Top;
						break;
					}
				case 12:
					{
						lnSubtotal.Y1 = lblSummaryTotal12.Top;
						lnSubtotal.Y2 = lblSummaryTotal12.Top;
						break;
					}
				case 13:
					{
						lnSubtotal.Y1 = lblSummaryTotal13.Top;
						lnSubtotal.Y2 = lblSummaryTotal13.Top;
						break;
					}
			}
			//end switch
			lnSubtotal.Visible = true;
		}
		// vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void FillSummaryLine_6(int intRw, string strDescription, double dblPrin, double dblTax, double dblChargedInt, double dblCurInt, double dblTotal, bool boolTotalLine = false)
		{
			FillSummaryLine(ref intRw, ref strDescription, ref dblPrin, ref dblTax, ref dblChargedInt, ref dblCurInt, ref dblTotal, boolTotalLine);
		}

		private void FillSummaryLine(ref int intRw, ref string strDescription, ref double dblPrin, ref double dblTax, ref double dblChargedInt, ref double dblCurInt, ref double dblTotal, bool boolTotalLine = false)
		{
			// this routine will fill in the line summary row
			switch (intRw)
			{
				case 1:
					{
						lblSummaryPaymentType1.Text = strDescription;
						lblSumPrin1.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax1.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt1.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal1.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 2:
					{
						lblSummaryPaymentType2.Text = strDescription;
						lblSumPrin2.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax2.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt2.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal2.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 3:
					{
						lblSummaryPaymentType3.Text = strDescription;
						lblSumPrin3.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax3.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt3.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal3.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 4:
					{
						lblSummaryPaymentType4.Text = strDescription;
						lblSumPrin4.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax4.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt4.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal4.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 5:
					{
						lblSummaryPaymentType5.Text = strDescription;
						lblSumPrin5.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax5.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt5.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal5.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 6:
					{
						lblSummaryPaymentType6.Text = strDescription;
						lblSumPrin6.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax6.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt6.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal6.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 7:
					{
						lblSummaryPaymentType7.Text = strDescription;
						lblSumPrin7.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax7.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt7.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal7.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 8:
					{
						lblSummaryPaymentType8.Text = strDescription;
						lblSumPrin8.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax8.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt8.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal8.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 9:
					{
						lblSummaryPaymentType9.Text = strDescription;
						lblSumPrin9.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax9.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt9.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal9.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 10:
					{
						lblSummaryPaymentType10.Text = strDescription;
						lblSumPrin10.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax10.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt10.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal10.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 11:
					{
						lblSummaryPaymentType11.Text = strDescription;
						lblSumPrin11.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax11.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt11.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal11.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 12:
					{
						lblSummaryPaymentType12.Text = strDescription;
						lblSumPrin12.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax12.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt12.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal12.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 13:
					{
						lblSummaryPaymentType13.Text = strDescription;
						lblSumPrin13.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax13.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt13.Text = Strings.Format(dblChargedInt + dblCurInt, "#,##0.00");
						lblSummaryTotal13.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
			}
			//end switch
		}

		private void AddToPaymentArray_164(int lngIndex, double dblPrin, double dblTax, double dblChargedInt, double dblCurInt)
		{
			AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblTax, ref dblChargedInt, ref dblCurInt);
		}

		private void AddToPaymentArray_242(int lngIndex, double dblPrin, double dblTax, double dblChargedInt, double dblCurInt)
		{
			AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblTax, ref dblChargedInt, ref dblCurInt);
		}

		private void AddToPaymentArray(ref int lngIndex, ref double dblPrin, ref double dblTax, ref double dblChargedInt, ref double dblCurInt)
		{
			// 0 - C, 1 - I, 2 - P, 3 - Total
			// 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
			dblPayments[lngIndex, 0] += dblPrin;
			dblPayments[lngIndex, 3] += dblTax;
			dblPayments[lngIndex, 1] += dblChargedInt;
			dblPayments[lngIndex, 2] += dblCurInt;
			dblBPayments[lngIndex, 0] += dblPrin;
			dblBPayments[lngIndex, 3] += dblTax;
			dblBPayments[lngIndex, 1] += dblChargedInt;
			dblBPayments[lngIndex, 2] += dblCurInt;
			dblPaymentsRev[lngIndex, 0] += dblPrin;
			dblPaymentsRev[lngIndex, 3] += dblTax;
			dblPaymentsRev[lngIndex, 1] += dblChargedInt;
			dblPaymentsRev[lngIndex, 2] += dblCurInt;
			dblBPaymentsRev[lngIndex, 0] += dblPrin;
			dblBPaymentsRev[lngIndex, 3] += dblTax;
			dblBPaymentsRev[lngIndex, 1] += dblChargedInt;
			dblBPaymentsRev[lngIndex, 2] += dblCurInt;
		}
		// vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void HideSummaryRow(short intRw)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				for (intCT = intRw; intCT <= 11; intCT++)
				{
					switch (intRw)
					{
						case 1:
							{
								lblSummaryPaymentType1.Visible = false;
								lblSumPrin1.Visible = false;
								lblSumTax1.Visible = false;
								lblSumInt1.Visible = false;
								lblSummaryTotal1.Visible = false;
								break;
							}
						case 2:
							{
								lblSummaryPaymentType2.Visible = false;
								lblSumPrin2.Visible = false;
								lblSumTax2.Visible = false;
								lblSumInt2.Visible = false;
								lblSummaryTotal2.Visible = false;
								break;
							}
						case 3:
							{
								lblSummaryPaymentType3.Visible = false;
								lblSumPrin3.Visible = false;
								lblSumTax3.Visible = false;
								lblSumInt3.Visible = false;
								lblSummaryTotal3.Visible = false;
								break;
							}
						case 4:
							{
								lblSummaryPaymentType4.Visible = false;
								lblSumPrin4.Visible = false;
								lblSumTax4.Visible = false;
								lblSumInt4.Visible = false;
								lblSummaryTotal4.Visible = false;
								break;
							}
						case 5:
							{
								lblSummaryPaymentType5.Visible = false;
								lblSumPrin5.Visible = false;
								lblSumTax5.Visible = false;
								lblSumInt5.Visible = false;
								lblSummaryTotal5.Visible = false;
								break;
							}
						case 6:
							{
								lblSummaryPaymentType6.Visible = false;
								lblSumPrin6.Visible = false;
								lblSumTax6.Visible = false;
								lblSumInt6.Visible = false;
								lblSummaryTotal6.Visible = false;
								break;
							}
						case 7:
							{
								lblSummaryPaymentType7.Visible = false;
								lblSumPrin7.Visible = false;
								lblSumTax7.Visible = false;
								lblSumInt7.Visible = false;
								lblSummaryTotal7.Visible = false;
								break;
							}
						case 8:
							{
								lblSummaryPaymentType8.Visible = false;
								lblSumPrin8.Visible = false;
								lblSumTax8.Visible = false;
								lblSumInt8.Visible = false;
								lblSummaryTotal8.Visible = false;
								break;
							}
						case 9:
							{
								lblSummaryPaymentType9.Visible = false;
								lblSumPrin9.Visible = false;
								lblSumTax9.Visible = false;
								lblSumInt9.Visible = false;
								lblSummaryTotal9.Visible = false;
								break;
							}
						case 10:
							{
								lblSummaryPaymentType10.Visible = false;
								lblSumPrin10.Visible = false;
								lblSumTax10.Visible = false;
								lblSumInt10.Visible = false;
								lblSummaryTotal10.Visible = false;
								break;
							}
						case 11:
							{
								lblSummaryPaymentType11.Visible = false;
								lblSumPrin11.Visible = false;
								lblSumTax11.Visible = false;
								lblSumInt11.Visible = false;
								lblSummaryTotal11.Visible = false;
								break;
							}
						case 12:
							{
								lblSummaryPaymentType12.Visible = false;
								lblSumPrin12.Visible = false;
								lblSumTax12.Visible = false;
								lblSumInt12.Visible = false;
								lblSummaryTotal12.Visible = false;
								break;
							}
						case 13:
							{
								lblSummaryPaymentType13.Visible = false;
								lblSumPrin13.Visible = false;
								lblSumTax13.Visible = false;
								lblSumInt13.Visible = false;
								lblSummaryTotal13.Visible = false;
								break;
							}
					}
					//end switch
				}
				if (!boolAdjustedSummary)
				{
					SetYearSummaryTop_2(lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 300 / 1440f);
					boolAdjustedSummary = true;
				}
				// set the size of the report footer depending on how many rows have been used
				// and move the error label to 300 pixels after the summary list
				// lblRTError.Top = lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 300
				// ReportFooter.Height = lblRTError.Top + lblRTError.Height + 100
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Hiding Summary Rows", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetYearSummaryTop_2(float lngTop)
		{
			SetYearSummaryTop(ref lngTop);
		}

		private void SetYearSummaryTop(ref float lngTop)
		{
			// this will start the year summary at the right height
			lblSummary.Top = lngTop;
			Line1.Y1 = lngTop + lblSummary.Height;
			Line1.Y2 = lngTop + lblSummary.Height;
			lblSummary1.Top = lngTop + lblSummary.Height;
			fldSummary1.Top = lngTop + lblSummary.Height;
		}

		private void ReversePaymentsFromStatusArray(ref clsDRWrapper rsRev)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int counter;
				int counter2;
				for (counter = 0; counter <= 4; counter++)
				{
					for (counter2 = 0; counter2 <= 6; counter2++)
					{
						dblPayments[counter, counter2] -= dblPaymentsRev[counter, counter2];
						dblBPayments[counter, counter2] -= dblBPaymentsRev[counter, counter2];
						dblPaymentsRev[counter, counter2] = 0;
						dblBPaymentsRev[counter, counter2] = 0;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Reversing Payment Counts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResetReversalValues()
		{
			int counter;
			int counter2;
			for (counter = 0; counter <= 4; counter++)
			{
				for (counter2 = 0; counter2 <= 6; counter2++)
				{
					dblPaymentsRev[counter, counter2] = 0;
					dblBPaymentsRev[counter, counter2] = 0;
				}
			}
		}

		private string GetBillingDate_2(int lngRK)
		{
			return GetBillingDate(ref lngRK);
		}

		private string GetBillingDate(ref int lngRK)
		{
			string GetBillingDate = "";
			var rsRK = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                //clsDRWrapper rsRK = new clsDRWrapper();
                rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK),
                    modCLCalculations.strARDatabase);
                if (rsRK.EndOfFile())
                {
                    GetBillingDate = "Not Billed";
                }
                else
                {
                    GetBillingDate = Strings.Format(rsRK.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
                }

                return GetBillingDate;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Reversing Payment Counts", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
                rsRK.Dispose();
            }
			return GetBillingDate;
		}

		private string GetBillingDescription(ref int lngRK)
		{
			string GetBillingDescription = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
                using (clsDRWrapper rsRK = new clsDRWrapper())
                {
                    rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK),
                        modCLCalculations.strARDatabase);
                    if (rsRK.EndOfFile())
                    {
                        GetBillingDescription = "Not Billed";
                    }
                    else
                    {
                        GetBillingDescription = FCConvert.ToString(rsRK.Get_Fields_String("Description"));
                    }
                }

                return GetBillingDescription;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Reversing Payment Counts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetBillingDescription;
		}

		private void HideNameField_2(bool boolHide)
		{
			HideNameField(ref boolHide);
		}

		private void HideNameField(ref bool boolHide)
		{
			int intCT = 0;
			if (boolHide)
			{
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldInvoiceNumber.Top = 0;
				fldTaxDue.Top = 0;
				fldPaymentReceived.Top = 0;
				fldDue.Top = 0;
				fldPrincipal.Top = 0;
				fldTax.Top = 0;
				fldInterest.Top = 0;
				intCT = 1;
			}
			else
			{
				fldName.Visible = true;
				fldAccount.Visible = true;
				fldInvoiceNumber.Top = 270 / 1440f;
				fldTaxDue.Top = 270 / 1440f;
				fldPaymentReceived.Top = 270 / 1440f;
				fldDue.Top = 270 / 1440f;
				fldPrincipal.Top = 270 / 1440f;
				fldTax.Top = 270 / 1440f;
				fldInterest.Top = 270 / 1440f;
				intCT = 2;
			}
			Detail.Height = (270 / 1440f * intCT) + 50 / 1440f;
		}
		// vbPorter upgrade warning: intIndex As object	OnWriteFCConvert.ToInt16(
		private double GetBillPaymentAmount_2(int intIndex)
		{
			return GetBillPaymentAmount(ref intIndex);
		}

		private double GetBillPaymentAmount(ref int intIndex)
		{
			double GetBillPaymentAmount = 0;
			int intCT;
			for (intCT = 0; intCT <= 3; intCT++)
			{
				GetBillPaymentAmount += dblBPayments[intCT, intIndex];
			}
			return GetBillPaymentAmount;
		}

		
	}
}
