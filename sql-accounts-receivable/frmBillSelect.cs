﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillSelect.
	/// </summary>
	public partial class frmBillSelect : BaseForm
	{
		public frmBillSelect()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillSelect InstancePtr
		{
			get
			{
				return (frmBillSelect)Sys.GetInstance(typeof(frmBillSelect));
			}
		}

		protected frmBillSelect _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public int intMenuOption;
		// 1 = Bill Prep
		int CodeCol;
		int DescriptionCol;

		private void frmBillSelect_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBillSelect_Load(object sender, System.EventArgs e)
		{
			string strVal;
			CodeCol = 0;
			DescriptionCol = 1;
			LoadYears();
			strVal = modRegistry.GetRegistryKey("BILLMONTH");
			if (Strings.Trim(strVal) != "")
			{
				SetMonthCombo(ref strVal);
			}
			else
			{
				cboMonth.SelectedIndex = DateTime.Today.Month - 1;
			}
			strVal = modRegistry.GetRegistryKey("BILLYEAR");
			if (Strings.Trim(strVal) != "")
			{
				SetYearCombo(ref strVal);
			}
			else
			{
				SetYearCombo_2(Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4));
			}
			vsBills.ColHidden(CodeCol, true);
			vsBills.ColWidth(DescriptionCol, FCConvert.ToInt32(vsBills.WidthOriginal * 0.9));
			vsBills.TextMatrix(0, DescriptionCol, "Bill Type");
			vsBills.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DescriptionCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmBillSelect_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuProcessQuit_Click();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != "vsBills")
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click()
		{
			if (vsBills.Visible == true)
			{
				vsBills.Visible = false;
				lblSelectBill.Visible = false;
				lblSelectMonth.Visible = true;
				cboMonth.Visible = true;
				cboYear.Visible = true;
			}
			else
			{
				Close();
			}
		}

		private void LoadYears()
		{
			int counter;
			cboYear.Clear();
			for (counter = DateTime.Today.Year - 2; counter <= DateTime.Today.Year + 3; counter++)
			{
				cboYear.AddItem(counter.ToString());
			}
		}

		private void SetYearCombo_2(string strCriteria)
		{
			SetYearCombo(ref strCriteria);
		}

		private void SetYearCombo(ref string strCriteria)
		{
			int counter;
			cboYear.SelectedIndex = -1;
			if (strCriteria == "")
			{
				return;
			}
			for (counter = 0; counter <= cboYear.Items.Count - 1; counter++)
			{
				if (Strings.Left(cboYear.Items[counter].ToString(), strCriteria.Length) == strCriteria)
				{
					cboYear.SelectedIndex = counter;
					break;
				}
			}
		}

		private void SetMonthCombo(ref string strCriteria)
		{
			int counter;
			cboMonth.SelectedIndex = -1;
			if (strCriteria == "")
			{
				return;
			}
			for (counter = 0; counter <= cboMonth.Items.Count - 1; counter++)
			{
				if (Strings.Left(cboMonth.Items[counter].ToString(), strCriteria.Length) == strCriteria)
				{
					cboMonth.SelectedIndex = counter;
					break;
				}
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBillCheck = new clsDRWrapper();
			clsDRWrapper rsFrequency = new clsDRWrapper();
			if (vsBills.Visible == false)
			{
				modRegistry.SaveRegistryKey("BILLMONTH", cboMonth.Text);
				modRegistry.SaveRegistryKey("BILLYEAR", cboYear.Text);
				FillGrid();
			}
			else
			{
				if (!modGlobal.ValidateBillType_2(FCConvert.ToInt32(FCConvert.ToDouble(vsBills.TextMatrix(vsBills.Row, CodeCol)))))
				{
					MessageBox.Show("There are problems with 1 or more of the accounts used in this bill type.  You must correct these problems before you may proceed.", "Invalid Accounts in Bill Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				// rsBillCheck.OpenRecordset "SELECT CustomerBills.*, CustomerMaster.CustomerID as CustomerID, Name, Status FROM CustomerBills INNER JOIN CustomerMaster ON CustomerBills.CustomerID = CustomerMaster.CustomerID WHERE Status = 'A' AND Type = " & vsBills.TextMatrix(vsBills.Row, CodeCol) & " ORDER BY Name"
				rsBillCheck.OpenRecordset("SELECT b.*, c.CustomerID as CustomerID, c.Status, c.PartyID, p.* FROM CustomerBills as b INNER JOIN CustomerMaster as c ON b.CustomerID = c.CustomerID CROSS APPLY " + rsBillCheck.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE c.Status = 'A' AND b.Type = " + vsBills.TextMatrix(vsBills.Row, CodeCol) + " ORDER BY p.FullName");
				if (rsBillCheck.EndOfFile() != true && rsBillCheck.BeginningOfFile() != true)
				{
					if (intMenuOption == 1)
					{
                        //FC:FINA:KV:IIT807+FC-8697
                        this.Hide();
                        frmBillPrep.InstancePtr.intBillType = FCConvert.ToInt32(vsBills.TextMatrix(vsBills.Row, CodeCol));
                        frmBillPrep.InstancePtr.BilledMonth = cboMonth.ListIndex + 1;
                        frmBillPrep.InstancePtr.BilledYear = cboYear.Text.ToIntegerValue();
                        frmBillPrep.InstancePtr.Show(App.MainForm);
					}
					else if (intMenuOption == 2)
					{
                        //FC:FINA:KV:IIT807+FC-8697
                        this.Hide();
                        frmBillCreate.InstancePtr.intBillType = FCConvert.ToInt32(vsBills.TextMatrix(vsBills.Row, CodeCol));
						frmBillCreate.InstancePtr.Show(App.MainForm);
					}
					else if (intMenuOption == 3)
					{
						rsBillCheck.OpenRecordset("SELECT * FROM RateKeys WHERE BillMonth = " + FCConvert.ToString(FCConvert.ToDateTime(cboMonth.Text + "/1/2000").Month) + " AND BillYear = " + cboYear.Text + " AND BillType = " + vsBills.TextMatrix(vsBills.Row, CodeCol) + " ORDER BY BillDate DESC");
						if (rsBillCheck.EndOfFile() != true && rsBillCheck.BeginningOfFile() != true)
						{
                            //FC:FINA:KV:IIT807+FC-8697
                            this.Hide();
                            frmBillPrint.InstancePtr.intBillType = FCConvert.ToInt32(vsBills.TextMatrix(vsBills.Row, CodeCol));
							frmBillPrint.InstancePtr.Show(App.MainForm);
						}
						else
						{
							MessageBox.Show("There are no bills of this type to print.", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
					}
				}
				else
				{
					if (intMenuOption == 1)
					{
						rsFrequency.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + vsBills.TextMatrix(vsBills.Row, CodeCol));
						if (rsFrequency.EndOfFile() != true && rsFrequency.BeginningOfFile() != true)
						{
							if (FCConvert.ToString(rsFrequency.Get_Fields_String("FrequencyCode")) == "D" || modGlobalConstants.Statics.MuniName.ToLower() == "islesboro")
							{
								frmBillPrep.InstancePtr.intBillType = FCConvert.ToInt32(vsBills.TextMatrix(vsBills.Row, CodeCol));
                                frmBillPrep.InstancePtr.BilledMonth = cboMonth.ListIndex + 1;
                                frmBillPrep.InstancePtr.BilledYear = cboYear.Text.ToIntegerValue();
                                frmBillPrep.InstancePtr.Show(App.MainForm);
							}
							else
							{
								MessageBox.Show("There are no active customers that have a bill of this type.", "No Customers", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							MessageBox.Show("There are no active customers that have a bill of this type.", "No Customers", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else if (intMenuOption == 3)
					{
						rsFrequency.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + vsBills.TextMatrix(vsBills.Row, CodeCol));
						if (rsFrequency.EndOfFile() != true && rsFrequency.BeginningOfFile() != true)
						{
							frmBillPrint.InstancePtr.intBillType = FCConvert.ToInt32(vsBills.TextMatrix(vsBills.Row, CodeCol));
							frmBillPrint.InstancePtr.Show(App.MainForm);
						}
						else
						{
							MessageBox.Show("There are no active customers that have a bill of this type.", "No Customers", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else
					{
						MessageBox.Show("There are no active customers that have a bill of this type.", "No Customers", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(btnProcessSave, new System.EventArgs());
		}

		private void FillGrid()
		{
			clsDRWrapper rsBillInfo = new clsDRWrapper();
			vsBills.Rows = 1;
			rsBillInfo.OpenRecordset("SELECT * FROM DefaultBillTypes ORDER BY TypeTitle");
			if (rsBillInfo.EndOfFile() != true && rsBillInfo.BeginningOfFile() != true)
			{
				do
				{
					if (ReadyToBill_26(rsBillInfo.Get_Fields_String("FrequencyCode"), FCConvert.ToInt16(rsBillInfo.Get_Fields_Int32("FirstMonth")), FCConvert.ToInt16(rsBillInfo.Get_Fields_Int32("TypeCode"))))
					{
						vsBills.AddItem(rsBillInfo.Get_Fields_Int32("TypeCode") + "\t" + rsBillInfo.Get_Fields_String("TypeTitle"));
					}
					rsBillInfo.MoveNext();
				}
				while (rsBillInfo.EndOfFile() != true);
			}
			if (vsBills.Rows > 1)
			{
				lblSelectMonth.Visible = false;
				cboMonth.Visible = false;
				cboYear.Visible = false;
				lblSelectBill.Visible = true;
				vsBills.Visible = true;
				vsBills.Row = 1;
				vsBills.Focus();
			}
			else
			{
				MessageBox.Show("No bills were found that need to be processed for the month selected.", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		
		private bool ReadyToBill_26(string strFreq, short intFirst, short intBillCode)
		{
			return ReadyToBill(ref strFreq, ref intFirst, ref intBillCode);
		}

		private bool ReadyToBill(ref string strFreq, ref short intFirst, ref short intBillCode)
		{
			bool ReadyToBill = false;
			clsDRWrapper rsRateInfo = new clsDRWrapper();
			ReadyToBill = false;
			if (strFreq == "Y")
			{
				if (cboMonth.SelectedIndex + 1 == intFirst)
				{
					ReadyToBill = true;
				}
			}
			else if (strFreq == "M")
			{
				ReadyToBill = true;
			}
			else if (strFreq == "Q")
			{
				switch (intFirst)
				{
					case 1:
						{
							if (cboMonth.SelectedIndex + 1 == 1 || cboMonth.SelectedIndex + 1 == 4 || cboMonth.SelectedIndex + 1 == 7 || cboMonth.SelectedIndex + 1 == 10)
							{
								ReadyToBill = true;
							}
							break;
						}
					case 2:
						{
							if (cboMonth.SelectedIndex + 1 == 2 || cboMonth.SelectedIndex + 1 == 5 || cboMonth.SelectedIndex + 1 == 8 || cboMonth.SelectedIndex + 1 == 11)
							{
								ReadyToBill = true;
							}
							break;
						}
					case 3:
						{
							if (cboMonth.SelectedIndex + 1 == 3 || cboMonth.SelectedIndex + 1 == 6 || cboMonth.SelectedIndex + 1 == 9 || cboMonth.SelectedIndex + 1 == 12)
							{
								ReadyToBill = true;
							}
							break;
						}
				}
				//end switch
			}
			else if (strFreq == "B")
			{
				switch (intFirst)
				{
					case 1:
						{
							if (cboMonth.SelectedIndex + 1 == 1 || cboMonth.SelectedIndex + 1 == 3 || cboMonth.SelectedIndex + 1 == 5 || cboMonth.SelectedIndex + 1 == 7 || cboMonth.SelectedIndex + 1 == 9 || cboMonth.SelectedIndex + 1 == 11)
							{
								ReadyToBill = true;
							}
							break;
						}
					case 2:
						{
							if (cboMonth.SelectedIndex + 1 == 2 || cboMonth.SelectedIndex + 1 == 4 || cboMonth.SelectedIndex + 1 == 6 || cboMonth.SelectedIndex + 1 == 8 || cboMonth.SelectedIndex + 1 == 10 || cboMonth.SelectedIndex + 1 == 12)
							{
								ReadyToBill = true;
							}
							break;
						}
				}
				//end switch
			}
			else if (strFreq == "D")
			{
				ReadyToBill = true;
			}
			else if (strFreq == "S")
			{
				switch (intFirst)
				{
					case 1:
						{
							if (cboMonth.SelectedIndex + 1 == 1 || cboMonth.SelectedIndex + 1 == 7)
							{
								ReadyToBill = true;
							}
							break;
						}
					case 2:
						{
							if (cboMonth.SelectedIndex + 1 == 2 || cboMonth.SelectedIndex + 1 == 8)
							{
								ReadyToBill = true;
							}
							break;
						}
					case 3:
						{
							if (cboMonth.SelectedIndex + 1 == 3 || cboMonth.SelectedIndex + 1 == 9)
							{
								ReadyToBill = true;
							}
							break;
						}
					case 4:
						{
							if (cboMonth.SelectedIndex + 1 == 4 || cboMonth.SelectedIndex + 1 == 10)
							{
								ReadyToBill = true;
							}
							break;
						}
					case 5:
						{
							if (cboMonth.SelectedIndex + 1 == 5 || cboMonth.SelectedIndex + 1 == 11)
							{
								ReadyToBill = true;
							}
							break;
						}
					case 6:
						{
							if (cboMonth.SelectedIndex + 1 == 6 || cboMonth.SelectedIndex + 1 == 12)
							{
								ReadyToBill = true;
							}
							break;
						}
				}
				//end switch
			}
			return ReadyToBill;
		}

		private void vsBills_DblClick(object sender, System.EventArgs e)
		{
			if (vsBills.Row > 0)
			{
				mnuProcessSave_Click();
			}
		}

		private void vsBills_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (vsBills.Row > 0)
			{
				if (keyAscii == 13)
				{
					keyAscii = 0;
					mnuProcessSave_Click();
				}
			}
		}
	}
}
