﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmPayeeMaster.
	/// </summary>
	public partial class frmPayeeMaster : BaseForm
	{
		public frmPayeeMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPayeeMaster InstancePtr
		{
			get
			{
				return (frmPayeeMaster)Sys.GetInstance(typeof(frmPayeeMaster));
			}
		}

		protected frmPayeeMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         8/5/03
		// This screen will be used to add or update voter information
		// ********************************************************
		public bool blnAlreadyAdded;
		public bool blnEdit;
		public int RecordNumber;
		public int FirstRecordShown;
		bool blnUnload;
		string TempKey = "";
		private string strNewPicture = string.Empty;
		private string strNewFileName = "";

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			string strFileName = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// Dim strPath             As String
				App.MainForm.CommonDialog1.Filter = "*.jpg||*.gif";
				// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				App.MainForm.CommonDialog1.ShowOpen();
				strFileName = Strings.Trim(App.MainForm.CommonDialog1.FileName);
				if (Strings.Trim(strFileName) != string.Empty)
				{
					if (LoadLogoPicture(ref strFileName))
					{
						strNewPicture = strFileName;
						strNewFileName = (new DirectoryInfo(App.MainForm.CommonDialog1.FileName)).Name;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				if (!App.MainForm.CommonDialog1.CancelError)
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Browse Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			imgSignature.Image = null;
			strNewPicture = "Clear";
		}

		public bool LoadLogoPicture(ref string strFileName)
		{
			bool LoadLogoPicture = false;
			try
			{
				// On Error GoTo ErrorHandler
				double dblRatio;
				int lngWidth;
				int lngHeight;
				int lngMaxWidth;
				int lngMaxHeight;
				// vbPorter upgrade warning: lngNewHeight As int	OnWriteFCConvert.ToDouble(
				int lngNewHeight;
				// vbPorter upgrade warning: lngNewWidth As int	OnWriteFCConvert.ToDouble(
				int lngNewWidth = 0;
				LoadLogoPicture = false;
				imgSignature.Image = FCUtils.LoadPicture(strFileName);
				lngMaxHeight = Shape1.Height;
				lngMaxWidth = Shape1.Width;
				dblRatio = (double)imgSignature.Image.Height / imgSignature.Image.Width;
				lngNewHeight = FCConvert.ToInt32(dblRatio * lngMaxWidth);
				if (lngNewHeight > lngMaxHeight)
				{
					imgSignature.Height = lngMaxHeight;
					lngNewWidth = FCConvert.ToInt32(lngMaxHeight / dblRatio);
					imgSignature.Width = lngNewWidth;
				}
				else
				{
					imgSignature.Width = lngMaxWidth;
					imgSignature.Height = lngNewHeight;
				}
				LoadLogoPicture = true;
				return LoadLogoPicture;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 481)
				{
					MessageBox.Show("You have selected an invalid image file for your signature.  Please try again.", "Invalid Signature File", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadSigPicture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return LoadLogoPicture;
		}

		private void frmPayeeMaster_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmPayeeMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPayeeMaster.FillStyle	= 0;
			//frmPayeeMaster.ScaleWidth	= 9045;
			//frmPayeeMaster.ScaleHeight	= 7185;
			//frmPayeeMaster.LinkTopic	= "Form2";
			//frmPayeeMaster.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			ClearScreen();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			lblVoterNumber.Font = this.HeaderText.Font;
		}

		private void frmPayeeMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			frmGetPayee.InstancePtr.Show(App.MainForm);
		}

		private void frmPayeeMaster_Resize(object sender, System.EventArgs e)
		{
			double dblRatio = 0;
			int lngWidth;
			int lngHeight;
			int lngMaxWidth = 0;
			int lngMaxHeight = 0;
			// vbPorter upgrade warning: lngNewHeight As int	OnWriteFCConvert.ToDouble(
			int lngNewHeight = 0;
			// vbPorter upgrade warning: lngNewWidth As int	OnWriteFCConvert.ToDouble(
			int lngNewWidth = 0;
			//FC:SBE - check for null to avoid exception
			//if (imgSignature.Image.Height > 0 && imgSignature.Image.Width > 0)
			if (imgSignature.Image != null && imgSignature.Image.Height > 0 && imgSignature.Image.Width > 0)
			{
				lngMaxHeight = Shape1.Height;
				lngMaxWidth = Shape1.Width;
				dblRatio = (double)imgSignature.Image.Height / imgSignature.Image.Width;
				lngNewHeight = FCConvert.ToInt32(dblRatio * lngMaxWidth);
				if (lngNewHeight > lngMaxHeight)
				{
					imgSignature.Height = lngMaxHeight;
					lngNewWidth = FCConvert.ToInt32(lngMaxHeight / dblRatio);
					imgSignature.Width = lngNewWidth;
				}
				else
				{
					imgSignature.Width = lngMaxWidth;
					imgSignature.Height = lngNewHeight;
				}
			}
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			int lngRecordToDelete = 0;
			clsDRWrapper rsJournalEntriesLeft = new clsDRWrapper();
			// make sure they want to delete this item
			counter = MessageBox.Show("Are you sure you want to delete this payee?", "Delete This Payee?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (counter == DialogResult.Yes)
			{
				if (modGlobal.Statics.PayeeSearchResults.RecordCount() > 1)
				{
					if (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) == FirstRecordShown)
					{
						lngRecordToDelete = FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID"));
						modGlobal.Statics.PayeeSearchResults.MoveFirst();
						if (lngRecordToDelete == FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")))
						{
							modGlobal.Statics.PayeeSearchResults.MoveNext();
							FirstRecordShown = FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID"));
							RecordNumber = FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID"));
							modGlobal.Statics.PayeeSearchResults.MovePrevious();
						}
						else
						{
							FirstRecordShown = FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID"));
							RecordNumber = FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID"));
							do
							{
								modGlobal.Statics.PayeeSearchResults.MoveNext();
							}
							while (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) != lngRecordToDelete);
						}
						modGlobal.Statics.PayeeSearchResults.Delete();
						modGlobal.Statics.PayeeSearchResults.MoveNext();
						modGlobal.Statics.PayeeSearchResults.MoveFirst();
						GetPayeeData();
						btnProcessPreviousEntry.Enabled = false;
						if (modGlobal.Statics.PayeeSearchResults.RecordCount() > 1)
						{
							btnProcessNextEntry.Enabled = true;
						}
						else
						{
							btnProcessNextEntry.Enabled = false;
						}
					}
					else
					{
						modGlobal.Statics.PayeeSearchResults.Delete();
						if (btnProcessNextEntry.Enabled)
						{
							if (btnProcessPreviousEntry.Enabled == false)
							{
								mnuProcessNextEntry_Click();
								btnProcessPreviousEntry.Enabled = false;
							}
							else
							{
								mnuProcessNextEntry_Click();
							}
						}
						else
						{
							mnuProcessPreviousEntry_Click();
							btnProcessNextEntry.Enabled = false;
						}
					}
				}
				else
				{
					modGlobal.Statics.PayeeSearchResults.Delete();
					mnuProcessQuit_Click();
				}
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessNextEntry_Click(object sender, System.EventArgs e)
		{
			if (RecordNumber == FirstRecordShown)
			{
				// move to next entry
				modGlobal.Statics.PayeeSearchResults.MoveFirst();
				if (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) == FirstRecordShown)
				{
					modGlobal.Statics.PayeeSearchResults.MoveNext();
					modGlobal.Statics.PayeeSearchResults.MoveNext();
					if (modGlobal.Statics.PayeeSearchResults.EndOfFile() == true)
					{
						btnProcessNextEntry.Enabled = false;
					}
					modGlobal.Statics.PayeeSearchResults.MovePrevious();
				}
				else
				{
					modGlobal.Statics.PayeeSearchResults.MoveNext();
					if (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) == FirstRecordShown)
					{
						modGlobal.Statics.PayeeSearchResults.MoveNext();
						if (modGlobal.Statics.PayeeSearchResults.EndOfFile() == true)
						{
							btnProcessNextEntry.Enabled = false;
						}
						modGlobal.Statics.PayeeSearchResults.MovePrevious();
						modGlobal.Statics.PayeeSearchResults.MovePrevious();
					}
					else
					{
						if (modGlobal.Statics.PayeeSearchResults.EndOfFile() == true)
						{
							btnProcessNextEntry.Enabled = false;
						}
						modGlobal.Statics.PayeeSearchResults.MovePrevious();
					}
				}
			}
			else
			{
				modGlobal.Statics.PayeeSearchResults.MoveNext();
				if (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) == FirstRecordShown)
				{
					modGlobal.Statics.PayeeSearchResults.MoveNext();
				}
				modGlobal.Statics.PayeeSearchResults.MoveNext();
				if (modGlobal.Statics.PayeeSearchResults.EndOfFile() != true)
				{
					if (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) == FirstRecordShown)
					{
						modGlobal.Statics.PayeeSearchResults.MoveNext();
						if (modGlobal.Statics.PayeeSearchResults.EndOfFile() == true)
						{
							btnProcessNextEntry.Enabled = false;
						}
						modGlobal.Statics.PayeeSearchResults.MovePrevious();
						modGlobal.Statics.PayeeSearchResults.MovePrevious();
					}
					else
					{
						if (modGlobal.Statics.PayeeSearchResults.EndOfFile() == true)
						{
							btnProcessNextEntry.Enabled = false;
						}
						modGlobal.Statics.PayeeSearchResults.MovePrevious();
					}
				}
				else
				{
					btnProcessNextEntry.Enabled = false;
					modGlobal.Statics.PayeeSearchResults.MovePrevious();
				}
			}
			if (btnProcessPreviousEntry.Enabled == false)
			{
				btnProcessPreviousEntry.Enabled = true;
			}
			RecordNumber = FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID"));
			ClearScreen();
			GetPayeeData();
		}

		public void mnuProcessNextEntry_Click()
		{
			mnuProcessNextEntry_Click(btnProcessNextEntry, new System.EventArgs());
		}

		private void mnuProcessPreviousEntry_Click(object sender, System.EventArgs e)
		{
			modGlobal.Statics.PayeeSearchResults.MovePrevious();
			// move to previous record
			if (modGlobal.Statics.PayeeSearchResults.BeginningOfFile() == true)
			{
				modGlobal.Statics.PayeeSearchResults.MoveNext();
				while (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) != FirstRecordShown)
				{
					modGlobal.Statics.PayeeSearchResults.MoveNext();
				}
				btnProcessPreviousEntry.Enabled = false;
			}
			else
			{
				if (FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID")) == FirstRecordShown)
				{
					modGlobal.Statics.PayeeSearchResults.MovePrevious();
					if (modGlobal.Statics.PayeeSearchResults.BeginningOfFile() == true)
					{
						modGlobal.Statics.PayeeSearchResults.MoveNext();
						btnProcessPreviousEntry.Enabled = false;
					}
				}
			}
			if (btnProcessNextEntry.Enabled == false)
			{
				btnProcessNextEntry.Enabled = true;
			}
			ClearScreen();
			RecordNumber = FCConvert.ToInt32(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID"));
			GetPayeeData();
		}

		public void mnuProcessPreviousEntry_Click()
		{
			mnuProcessPreviousEntry_Click(btnProcessPreviousEntry, new System.EventArgs());
		}

		private void mnuProcessQuit_Click()
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			txtDescription.Focus();
			blnUnload = true;
			SaveInfo();
		}

		private void SaveInfo()
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsPhoneInfo = new clsDRWrapper();
			int lngIDNumber;
			int lngRecordNumber = 0;
			if (Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("You must enter a payee description before you may proceed.", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (blnEdit || blnAlreadyAdded)
			{
				rsInfo.OpenRecordset("SELECT * FROM Payees WHERE PayeeID = " + lblVoterNumber.Text);
				rsInfo.Edit();
				lngRecordNumber = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("PayeeID"));
			}
			else
			{
				rsInfo.OpenRecordset("SELECT TOP 1 * FROM Payees ORDER BY PayeeID DESC");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					lngRecordNumber = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("PayeeID")) + 1;
				}
				else
				{
					lngRecordNumber = 1;
				}
				rsInfo.AddNew();
				rsInfo.Set_Fields("PayeeID", lngRecordNumber);
			}
			rsInfo.Set_Fields("Description", Strings.Trim(txtDescription.Text));
			if (strNewPicture == "Clear")
			{
				rsInfo.Set_Fields("LogoPath", "");
			}
			else if (strNewPicture != "")
			{
				if (Directory.Exists(FCFileSystem.Statics.UserDataFolder + "\\Logos\\") != true)
				{
					Directory.CreateDirectory(FCFileSystem.Statics.UserDataFolder + "\\Logos\\");
				}
				File.Copy(strNewPicture, FCFileSystem.Statics.UserDataFolder + "\\Logos\\" + strNewFileName, true);
				rsInfo.Set_Fields("LogoPath", FCFileSystem.Statics.UserDataFolder + "\\Logos\\" + strNewFileName);
			}
			else
			{
				// do nothing
			}
			rsInfo.Set_Fields("RemittanceMessage", Strings.Trim(txtRemittanceMessage.Text));
			rsInfo.Update();
			if (!blnEdit)
			{
				if (blnAlreadyAdded)
				{
					MessageBox.Show("Save Successful", "Record Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("This payee was saved as Payee # " + FCConvert.ToString(lngRecordNumber), "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				MessageBox.Show("Save Successful", "Record Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			modRegistry.SaveRegistryKey("CURRPAY", FCConvert.ToString(lngRecordNumber));
			if (blnUnload)
			{
				Close();
			}
			else
			{
				if (!blnEdit)
				{
					lblVoterNumber.Text = FCConvert.ToString(lngRecordNumber);
					RecordNumber = lngRecordNumber;
					blnAlreadyAdded = true;
				}
			}
		}

		private void GetPayeeData()
		{
			string strTemp;
			txtDescription.Text = FCConvert.ToString(modGlobal.Statics.PayeeSearchResults.Get_Fields_String("Description"));
			strNewPicture = "";
			strNewFileName = "";
			strTemp = FCConvert.ToString(modGlobal.Statics.PayeeSearchResults.Get_Fields_String("LogoPath"));
			if (strTemp != string.Empty)
			{
				if (File.Exists(strTemp))
				{
					if (!LoadLogoPicture(ref strTemp))
					{
						File.Delete(strTemp);
					}
				}
				else
				{
					imgSignature.Image = null;
				}
			}
			else
			{
				imgSignature.Image = null;
			}
			txtRemittanceMessage.TextRTF = FCConvert.ToString(modGlobal.Statics.PayeeSearchResults.Get_Fields_String("RemittanceMessage"));
			lblVoterNumber.Text = FCConvert.ToString(modGlobal.Statics.PayeeSearchResults.Get_Fields_Int32("PayeeID"));
		}

		private void ClearScreen()
		{
			txtDescription.Text = "";
			imgSignature.Image = null;
			txtRemittanceMessage.Text = "";
			lblVoterNumber.Text = "";
			strNewPicture = "";
			strNewFileName = "";
		}
	}
}
