﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillTypeListing.
	/// </summary>
	partial class frmBillTypeListing : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbEnd;
		public fecherFoundation.FCComboBox cmbStart;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbEnd = new fecherFoundation.FCComboBox();
			this.cmbStart = new fecherFoundation.FCComboBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 326);
			this.BottomPanel.Size = new System.Drawing.Size(989, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdPreview);
			this.ClientArea.Controls.Add(this.cmbEnd);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.cmbStart);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Size = new System.Drawing.Size(989, 266);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(989, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(185, 30);
			this.HeaderText.Text = "Bill Type Listing";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All Types",
				"Range"
			});
			this.cmbRange.Location = new System.Drawing.Point(138, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(115, 40);
			this.cmbRange.TabIndex = 6;
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// lblRange
			// 
			this.lblRange.Location = new System.Drawing.Point(30, 44);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(42, 16);
			this.lblRange.TabIndex = 7;
			this.lblRange.Text = "RANGE";
			// 
			// cmbEnd
			// 
			this.cmbEnd.AutoSize = false;
			this.cmbEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cmbEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbEnd.Enabled = false;
			this.cmbEnd.FormattingEnabled = true;
			this.cmbEnd.Location = new System.Drawing.Point(628, 90);
			this.cmbEnd.Name = "cmbEnd";
			this.cmbEnd.Size = new System.Drawing.Size(324, 40);
			this.cmbEnd.Sorted = true;
			this.cmbEnd.TabIndex = 4;
			this.cmbEnd.DropDown += new System.EventHandler(this.cmbEnd_DropDown);
			this.cmbEnd.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbEnd_KeyDown);
			// 
			// cmbStart
			// 
			this.cmbStart.AutoSize = false;
			this.cmbStart.BackColor = System.Drawing.SystemColors.Window;
			this.cmbStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbStart.Enabled = false;
			this.cmbStart.FormattingEnabled = true;
			this.cmbStart.Location = new System.Drawing.Point(138, 90);
			this.cmbStart.Name = "cmbStart";
			this.cmbStart.Size = new System.Drawing.Size(324, 40);
			this.cmbStart.Sorted = true;
			this.cmbStart.TabIndex = 3;
			this.cmbStart.DropDown += new System.EventHandler(this.cmbStart_DropDown);
			this.cmbStart.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbStart_KeyDown);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(534, 104);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(27, 16);
			this.Label3.TabIndex = 8;
			this.Label3.Text = "END";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(38, 16);
			this.Label2.TabIndex = 7;
			this.Label2.Text = "START";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePreview,
				this.mnuFileSeperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 0;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 1;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.Location = new System.Drawing.Point(30, 173);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.Size = new System.Drawing.Size(132, 48);
			this.cmdPreview.TabIndex = 2;
			this.cmdPreview.Text = "Print Preview";
			this.cmdPreview.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// frmBillTypeListing
			// 
			this.ClientSize = new System.Drawing.Size(989, 434);
			this.Name = "frmBillTypeListing";
			this.Text = "Bill Type Listing";
			this.Load += new System.EventHandler(this.frmBillTypeListing_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBillTypeListing_KeyDown);
			this.Resize += new System.EventHandler(this.frmBillTypeListing_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdPreview;
	}
}
