﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmSetupARStatement.
	/// </summary>
	public partial class frmSetupARStatement : BaseForm
	{
		public frmSetupARStatement()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.cmbAllCustomers.SelectedIndex = 0;
			this.cmbTransactionDate.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupARStatement InstancePtr
		{
			get
			{
				return (frmSetupARStatement)Sys.GetInstance(typeof(frmSetupARStatement));
			}
		}

		protected frmSetupARStatement _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// vbPorter upgrade warning: datReturnedDate As DateTime	OnWriteFCConvert.ToInt16(
		DateTime datReturnedDate;

		private void frmSetupARStatement_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupARStatement_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupARStatement.FillStyle	= 0;
			//frmSetupARStatement.ScaleWidth	= 9045;
			//frmSetupARStatement.ScaleHeight	= 6930;
			//frmSetupARStatement.LinkTopic	= "Form2";
			//frmSetupARStatement.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsDefaults = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			txtStartDate.Text = "01/01/1900";
			txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			FillCustomersCombo();
			FillPayeeCombo();
			cboPayee.SelectedIndex = 0;
			rsDefaults.OpenRecordset("SELECT * FROM Customize");
			if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsDefaults.Get_Fields_Int32("DefaultPayeeID")) > 0)
				{
					SetStatementayeeCombo_2(FCConvert.ToInt32(rsDefaults.Get_Fields_Int32("DefaultPayeeID")));
				}
				else
				{
					if (cboStatementPayee.Items.Count > 0)
					{
						cboStatementPayee.SelectedIndex = 0;
					}
				}
			}
			else
			{
				if (cboStatementPayee.Items.Count > 0)
				{
					cboStatementPayee.SelectedIndex = 0;
				}
			}
		}

		private void frmSetupARStatement_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void SetStatementayeeCombo_2(int intPayeeID)
		{
			SetStatementayeeCombo(ref intPayeeID);
		}

		private void SetStatementayeeCombo(ref int intPayeeID)
		{
			int counter;
			for (counter = 0; counter <= cboStatementPayee.Items.Count - 1; counter++)
			{
				if (cboStatementPayee.ItemData(counter) == intPayeeID)
				{
					cboStatementPayee.SelectedIndex = counter;
					break;
				}
			}
		}

		private void mnuProcessPreview_Click(object sender, System.EventArgs e)
		{
			string strSQL = "";
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date for the start date to report on.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date for the end date to report on.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtEndDate.Focus();
				return;
			}
			else if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your start date must be earlier then your end date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			if (cboStatementPayee.SelectedIndex < 0)
			{
				MessageBox.Show("You must select a statement payee before you may continue.", "Invalid Statement Payee", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cboStatementPayee.Focus();
				return;
			}
			if (cmbAllCustomers.SelectedIndex == 1)
			{
				if (cboStart.SelectedIndex > cboEnd.SelectedIndex)
				{
					MessageBox.Show("You haeve selected an invalid range fo customers to report on.", "Invalid Customer Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cboStart.Focus();
					return;
				}
			}
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptARStatement.InstancePtr);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdRequestDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtStartDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEndDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void FillCustomersCombo()
		{
			clsDRWrapper rsCust = new clsDRWrapper();
			cboStart.Clear();
			cboEnd.Clear();
			rsCust.OpenRecordset("SELECT p.FullName FROM CustomerMaster as c CROSS APPLY " + rsCust.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.PartyID) as p ORDER BY p.FullName");
			if (rsCust.EndOfFile() != true && rsCust.BeginningOfFile() != true)
			{
				do
				{
					cboStart.AddItem(rsCust.Get_Fields_String("FullName"));
					cboEnd.AddItem(rsCust.Get_Fields_String("FullName"));
					rsCust.MoveNext();
				}
				while (rsCust.EndOfFile() != true);
			}
		}

		private void FillPayeeCombo()
		{
			clsDRWrapper rsPayeeInfo = new clsDRWrapper();
			cboPayee.Clear();
			cboStatementPayee.Clear();
			cboPayee.AddItem("All Payees");
			rsPayeeInfo.OpenRecordset("SELECT * FROM Payees ORDER BY Description", "TWAR0000.vb1");
			if (rsPayeeInfo.EndOfFile() != true && rsPayeeInfo.BeginningOfFile() != true)
			{
				do
				{
					cboPayee.AddItem(rsPayeeInfo.Get_Fields_String("Description"));
					cboPayee.ItemData(cboPayee.NewIndex, FCConvert.ToInt32(rsPayeeInfo.Get_Fields_Int32("PayeeID")));
					cboStatementPayee.AddItem(rsPayeeInfo.Get_Fields_String("Description"));
					cboStatementPayee.ItemData(cboStatementPayee.NewIndex, FCConvert.ToInt32(rsPayeeInfo.Get_Fields_Int32("PayeeID")));
					rsPayeeInfo.MoveNext();
				}
				while (rsPayeeInfo.EndOfFile() != true);
			}
		}

		private void optAllCustomers_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAllCustomers.SelectedIndex == 0)
			{
				//fraSelectedCustomers.Enabled = false;
				cboStart.SelectedIndex = -1;
				cboEnd.SelectedIndex = -1;
				cboStart.Enabled = false;
				cboEnd.Enabled = false;
				lblTo2.Enabled = false;
			}
			else
			{
				//fraSelectedCustomers.Enabled = true;
				cboStart.Enabled = true;
				cboEnd.Enabled = true;
				lblTo2.Enabled = true;
			}
		}

		private void cmdPrintPreview_Click(object sender, EventArgs e)
		{
			mnuProcessPreview_Click(sender, e);
		}
		//private void optRangeCustomers_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    //fraSelectedCustomers.Enabled = true;
		//    cboStart.Enabled = true;
		//    cboEnd.Enabled = true;
		//    lblTo2.Enabled = true;
		//}
	}
}
