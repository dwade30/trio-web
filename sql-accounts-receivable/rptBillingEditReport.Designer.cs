﻿namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptBillingEditReport.
	/// </summary>
	partial class rptBillingEditReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillingEditReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDetail1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnitCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnitCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnitCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnitCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnitCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnitCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCustomerID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFeeDescription1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFeeDescription2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFeeDescription3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFeeDescription4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFeeDescription5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFeeDescription6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSalesTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInvoiceTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldQuantity1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldQuantity2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldQuantity3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldQuantity4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldQuantity5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldQuantity6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoiceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.linInvoiceTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldTotalAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalSalesTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalFeeDescription1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalFeeDescription2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalFeeDescription3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalFeeDescription4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalFeeDescription5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalFeeDescription6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalSalesTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.linGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillInfo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSalesTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalSalesTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalSalesTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDetail1,
				this.fldDetail2,
				this.fldDetail3,
				this.fldDetail4,
				this.fldUnitCost1,
				this.fldUnitCost2,
				this.fldUnitCost3,
				this.fldUnitCost4,
				this.fldUnitCost5,
				this.fldUnitCost6,
				this.fldCustomerID,
				this.fldName,
				this.lblFeeDescription1,
				this.lblFeeDescription2,
				this.lblFeeDescription3,
				this.lblFeeDescription4,
				this.lblFeeDescription5,
				this.lblFeeDescription6,
				this.lblSalesTax,
				this.lblInvoiceTotal,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Line2,
				this.fldQuantity1,
				this.fldQuantity2,
				this.fldQuantity3,
				this.fldQuantity4,
				this.fldQuantity5,
				this.fldQuantity6,
				this.fldAmount1,
				this.fldAmount2,
				this.fldAmount3,
				this.fldAmount4,
				this.fldAmount5,
				this.fldAmount6,
				this.fldSalesTax,
				this.fldInvoiceTotal,
				this.linInvoiceTotal
			});
			this.Detail.Height = 2.15625F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDetail1
			// 
			this.fldDetail1.Height = 0.1875F;
			this.fldDetail1.Left = 3.09375F;
			this.fldDetail1.Name = "fldDetail1";
			this.fldDetail1.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDetail1.Text = "Field1";
			this.fldDetail1.Top = 0.125F;
			this.fldDetail1.Width = 0.75F;
			// 
			// fldDetail2
			// 
			this.fldDetail2.Height = 0.1875F;
			this.fldDetail2.Left = 3.875F;
			this.fldDetail2.Name = "fldDetail2";
			this.fldDetail2.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDetail2.Text = "Field2";
			this.fldDetail2.Top = 0.125F;
			this.fldDetail2.Width = 0.75F;
			// 
			// fldDetail3
			// 
			this.fldDetail3.Height = 0.1875F;
			this.fldDetail3.Left = 4.65625F;
			this.fldDetail3.Name = "fldDetail3";
			this.fldDetail3.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDetail3.Text = "Field3";
			this.fldDetail3.Top = 0.125F;
			this.fldDetail3.Width = 0.75F;
			// 
			// fldDetail4
			// 
			this.fldDetail4.Height = 0.1875F;
			this.fldDetail4.Left = 5.4375F;
			this.fldDetail4.Name = "fldDetail4";
			this.fldDetail4.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDetail4.Text = "Field4";
			this.fldDetail4.Top = 0.125F;
			this.fldDetail4.Width = 0.75F;
			// 
			// fldUnitCost1
			// 
			this.fldUnitCost1.Height = 0.1875F;
			this.fldUnitCost1.Left = 1.40625F;
			this.fldUnitCost1.Name = "fldUnitCost1";
			this.fldUnitCost1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldUnitCost1.Text = "Field5";
			this.fldUnitCost1.Top = 0.59375F;
			this.fldUnitCost1.Width = 0.75F;
			// 
			// fldUnitCost2
			// 
			this.fldUnitCost2.Height = 0.1875F;
			this.fldUnitCost2.Left = 1.40625F;
			this.fldUnitCost2.Name = "fldUnitCost2";
			this.fldUnitCost2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldUnitCost2.Text = "Field6";
			this.fldUnitCost2.Top = 0.78125F;
			this.fldUnitCost2.Width = 0.75F;
			// 
			// fldUnitCost3
			// 
			this.fldUnitCost3.Height = 0.1875F;
			this.fldUnitCost3.Left = 1.40625F;
			this.fldUnitCost3.Name = "fldUnitCost3";
			this.fldUnitCost3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldUnitCost3.Text = "Field7";
			this.fldUnitCost3.Top = 0.96875F;
			this.fldUnitCost3.Width = 0.75F;
			// 
			// fldUnitCost4
			// 
			this.fldUnitCost4.Height = 0.1875F;
			this.fldUnitCost4.Left = 1.40625F;
			this.fldUnitCost4.Name = "fldUnitCost4";
			this.fldUnitCost4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldUnitCost4.Text = "Field8";
			this.fldUnitCost4.Top = 1.15625F;
			this.fldUnitCost4.Width = 0.75F;
			// 
			// fldUnitCost5
			// 
			this.fldUnitCost5.Height = 0.1875F;
			this.fldUnitCost5.Left = 1.40625F;
			this.fldUnitCost5.Name = "fldUnitCost5";
			this.fldUnitCost5.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldUnitCost5.Text = "Field9";
			this.fldUnitCost5.Top = 1.34375F;
			this.fldUnitCost5.Width = 0.75F;
			// 
			// fldUnitCost6
			// 
			this.fldUnitCost6.Height = 0.1875F;
			this.fldUnitCost6.Left = 1.40625F;
			this.fldUnitCost6.Name = "fldUnitCost6";
			this.fldUnitCost6.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldUnitCost6.Text = "Field10";
			this.fldUnitCost6.Top = 1.53125F;
			this.fldUnitCost6.Width = 0.75F;
			// 
			// fldCustomerID
			// 
			this.fldCustomerID.CanShrink = true;
			this.fldCustomerID.Height = 0.1875F;
			this.fldCustomerID.Left = 0F;
			this.fldCustomerID.Name = "fldCustomerID";
			this.fldCustomerID.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCustomerID.Text = "00000";
			this.fldCustomerID.Top = 0.125F;
			this.fldCustomerID.Width = 0.40625F;
			// 
			// fldName
			// 
			this.fldName.CanShrink = true;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.4375F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldName.Text = "Field2";
			this.fldName.Top = 0.125F;
			this.fldName.Width = 2.53125F;
			// 
			// lblFeeDescription1
			// 
			this.lblFeeDescription1.Height = 0.1875F;
			this.lblFeeDescription1.HyperLink = null;
			this.lblFeeDescription1.Left = 0.3125F;
			this.lblFeeDescription1.Name = "lblFeeDescription1";
			this.lblFeeDescription1.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblFeeDescription1.Text = "Label8";
			this.lblFeeDescription1.Top = 0.59375F;
			this.lblFeeDescription1.Width = 1.03125F;
			// 
			// lblFeeDescription2
			// 
			this.lblFeeDescription2.Height = 0.1875F;
			this.lblFeeDescription2.HyperLink = null;
			this.lblFeeDescription2.Left = 0.3125F;
			this.lblFeeDescription2.Name = "lblFeeDescription2";
			this.lblFeeDescription2.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblFeeDescription2.Text = "Label12";
			this.lblFeeDescription2.Top = 0.78125F;
			this.lblFeeDescription2.Width = 1.03125F;
			// 
			// lblFeeDescription3
			// 
			this.lblFeeDescription3.Height = 0.1875F;
			this.lblFeeDescription3.HyperLink = null;
			this.lblFeeDescription3.Left = 0.3125F;
			this.lblFeeDescription3.Name = "lblFeeDescription3";
			this.lblFeeDescription3.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblFeeDescription3.Text = "Label8";
			this.lblFeeDescription3.Top = 0.96875F;
			this.lblFeeDescription3.Width = 1.03125F;
			// 
			// lblFeeDescription4
			// 
			this.lblFeeDescription4.Height = 0.1875F;
			this.lblFeeDescription4.HyperLink = null;
			this.lblFeeDescription4.Left = 0.3125F;
			this.lblFeeDescription4.Name = "lblFeeDescription4";
			this.lblFeeDescription4.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblFeeDescription4.Text = "Label14";
			this.lblFeeDescription4.Top = 1.15625F;
			this.lblFeeDescription4.Width = 1.03125F;
			// 
			// lblFeeDescription5
			// 
			this.lblFeeDescription5.Height = 0.1875F;
			this.lblFeeDescription5.HyperLink = null;
			this.lblFeeDescription5.Left = 0.3125F;
			this.lblFeeDescription5.Name = "lblFeeDescription5";
			this.lblFeeDescription5.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblFeeDescription5.Text = "Label8";
			this.lblFeeDescription5.Top = 1.34375F;
			this.lblFeeDescription5.Width = 1.03125F;
			// 
			// lblFeeDescription6
			// 
			this.lblFeeDescription6.Height = 0.1875F;
			this.lblFeeDescription6.HyperLink = null;
			this.lblFeeDescription6.Left = 0.3125F;
			this.lblFeeDescription6.Name = "lblFeeDescription6";
			this.lblFeeDescription6.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblFeeDescription6.Text = "Label16";
			this.lblFeeDescription6.Top = 1.53125F;
			this.lblFeeDescription6.Width = 1.03125F;
			// 
			// lblSalesTax
			// 
			this.lblSalesTax.Height = 0.1875F;
			this.lblSalesTax.HyperLink = null;
			this.lblSalesTax.Left = 0.3125F;
			this.lblSalesTax.Name = "lblSalesTax";
			this.lblSalesTax.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblSalesTax.Text = "Sales Tax";
			this.lblSalesTax.Top = 1.71875F;
			this.lblSalesTax.Width = 1.03125F;
			// 
			// lblInvoiceTotal
			// 
			this.lblInvoiceTotal.Height = 0.1875F;
			this.lblInvoiceTotal.HyperLink = null;
			this.lblInvoiceTotal.Left = 0.3125F;
			this.lblInvoiceTotal.Name = "lblInvoiceTotal";
			this.lblInvoiceTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblInvoiceTotal.Text = "Invoice Total";
			this.lblInvoiceTotal.Top = 1.96875F;
			this.lblInvoiceTotal.Width = 1.03125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.3125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label9.Text = "Fees";
			this.Label9.Top = 0.375F;
			this.Label9.Width = 1.03125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.40625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "Unit Cost";
			this.Label10.Top = 0.375F;
			this.Label10.Width = 0.75F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.25F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.Label11.Text = "Quantity";
			this.Label11.Top = 0.375F;
			this.Label11.Width = 0.75F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.09375F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.Label12.Text = "Line Total";
			this.Label12.Top = 0.375F;
			this.Label12.Width = 0.75F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.3125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.5625F;
			this.Line2.Width = 3.59375F;
			this.Line2.X1 = 0.3125F;
			this.Line2.X2 = 3.90625F;
			this.Line2.Y1 = 0.5625F;
			this.Line2.Y2 = 0.5625F;
			// 
			// fldQuantity1
			// 
			this.fldQuantity1.Height = 0.1875F;
			this.fldQuantity1.Left = 2.25F;
			this.fldQuantity1.Name = "fldQuantity1";
			this.fldQuantity1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldQuantity1.Text = "Field5";
			this.fldQuantity1.Top = 0.59375F;
			this.fldQuantity1.Width = 0.75F;
			// 
			// fldQuantity2
			// 
			this.fldQuantity2.Height = 0.1875F;
			this.fldQuantity2.Left = 2.25F;
			this.fldQuantity2.Name = "fldQuantity2";
			this.fldQuantity2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldQuantity2.Text = "Field6";
			this.fldQuantity2.Top = 0.78125F;
			this.fldQuantity2.Width = 0.75F;
			// 
			// fldQuantity3
			// 
			this.fldQuantity3.Height = 0.1875F;
			this.fldQuantity3.Left = 2.25F;
			this.fldQuantity3.Name = "fldQuantity3";
			this.fldQuantity3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldQuantity3.Text = "Field7";
			this.fldQuantity3.Top = 0.96875F;
			this.fldQuantity3.Width = 0.75F;
			// 
			// fldQuantity4
			// 
			this.fldQuantity4.Height = 0.1875F;
			this.fldQuantity4.Left = 2.25F;
			this.fldQuantity4.Name = "fldQuantity4";
			this.fldQuantity4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldQuantity4.Text = "Field8";
			this.fldQuantity4.Top = 1.15625F;
			this.fldQuantity4.Width = 0.75F;
			// 
			// fldQuantity5
			// 
			this.fldQuantity5.Height = 0.1875F;
			this.fldQuantity5.Left = 2.25F;
			this.fldQuantity5.Name = "fldQuantity5";
			this.fldQuantity5.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldQuantity5.Text = "Field9";
			this.fldQuantity5.Top = 1.34375F;
			this.fldQuantity5.Width = 0.75F;
			// 
			// fldQuantity6
			// 
			this.fldQuantity6.Height = 0.1875F;
			this.fldQuantity6.Left = 2.25F;
			this.fldQuantity6.Name = "fldQuantity6";
			this.fldQuantity6.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldQuantity6.Text = "Field10";
			this.fldQuantity6.Top = 1.53125F;
			this.fldQuantity6.Width = 0.75F;
			// 
			// fldAmount1
			// 
			this.fldAmount1.Height = 0.1875F;
			this.fldAmount1.Left = 3.09375F;
			this.fldAmount1.Name = "fldAmount1";
			this.fldAmount1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAmount1.Text = "Field5";
			this.fldAmount1.Top = 0.59375F;
			this.fldAmount1.Width = 0.75F;
			// 
			// fldAmount2
			// 
			this.fldAmount2.Height = 0.1875F;
			this.fldAmount2.Left = 3.09375F;
			this.fldAmount2.Name = "fldAmount2";
			this.fldAmount2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAmount2.Text = "Field6";
			this.fldAmount2.Top = 0.78125F;
			this.fldAmount2.Width = 0.75F;
			// 
			// fldAmount3
			// 
			this.fldAmount3.Height = 0.1875F;
			this.fldAmount3.Left = 3.09375F;
			this.fldAmount3.Name = "fldAmount3";
			this.fldAmount3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAmount3.Text = "Field7";
			this.fldAmount3.Top = 0.96875F;
			this.fldAmount3.Width = 0.75F;
			// 
			// fldAmount4
			// 
			this.fldAmount4.Height = 0.1875F;
			this.fldAmount4.Left = 3.09375F;
			this.fldAmount4.Name = "fldAmount4";
			this.fldAmount4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAmount4.Text = "Field8";
			this.fldAmount4.Top = 1.15625F;
			this.fldAmount4.Width = 0.75F;
			// 
			// fldAmount5
			// 
			this.fldAmount5.Height = 0.1875F;
			this.fldAmount5.Left = 3.09375F;
			this.fldAmount5.Name = "fldAmount5";
			this.fldAmount5.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAmount5.Text = "Field9";
			this.fldAmount5.Top = 1.34375F;
			this.fldAmount5.Width = 0.75F;
			// 
			// fldAmount6
			// 
			this.fldAmount6.Height = 0.1875F;
			this.fldAmount6.Left = 3.09375F;
			this.fldAmount6.Name = "fldAmount6";
			this.fldAmount6.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAmount6.Text = "Field10";
			this.fldAmount6.Top = 1.53125F;
			this.fldAmount6.Width = 0.75F;
			// 
			// fldSalesTax
			// 
			this.fldSalesTax.Height = 0.1875F;
			this.fldSalesTax.Left = 3.09375F;
			this.fldSalesTax.Name = "fldSalesTax";
			this.fldSalesTax.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldSalesTax.Text = "Field11";
			this.fldSalesTax.Top = 1.71875F;
			this.fldSalesTax.Width = 0.75F;
			// 
			// fldInvoiceTotal
			// 
			this.fldInvoiceTotal.Height = 0.1875F;
			this.fldInvoiceTotal.Left = 3.09375F;
			this.fldInvoiceTotal.Name = "fldInvoiceTotal";
			this.fldInvoiceTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldInvoiceTotal.Text = "Field12";
			this.fldInvoiceTotal.Top = 1.96875F;
			this.fldInvoiceTotal.Width = 0.75F;
			// 
			// linInvoiceTotal
			// 
			this.linInvoiceTotal.Height = 0F;
			this.linInvoiceTotal.Left = 0.28125F;
			this.linInvoiceTotal.LineWeight = 1F;
			this.linInvoiceTotal.Name = "linInvoiceTotal";
			this.linInvoiceTotal.Top = 1.90625F;
			this.linInvoiceTotal.Width = 3.59375F;
			this.linInvoiceTotal.X1 = 0.28125F;
			this.linInvoiceTotal.X2 = 3.875F;
			this.linInvoiceTotal.Y1 = 1.90625F;
			this.linInvoiceTotal.Y2 = 1.90625F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalAmount1,
				this.fldTotalAmount2,
				this.fldTotalAmount3,
				this.fldTotalAmount4,
				this.fldTotalAmount5,
				this.fldTotalAmount6,
				this.fldTotalSalesTax,
				this.fldGrandTotal,
				this.lblTotalFeeDescription1,
				this.lblTotalFeeDescription2,
				this.lblTotalFeeDescription3,
				this.lblTotalFeeDescription4,
				this.lblTotalFeeDescription5,
				this.lblTotalFeeDescription6,
				this.lblTotalSalesTax,
				this.lblGrandTotal,
				this.linGrandTotal,
				this.Label21,
				this.Line5
			});
			this.ReportFooter.Height = 1.854167F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldTotalAmount1
			// 
			this.fldTotalAmount1.Height = 0.1875F;
			this.fldTotalAmount1.Left = 4.1875F;
			this.fldTotalAmount1.Name = "fldTotalAmount1";
			this.fldTotalAmount1.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalAmount1.Text = "Field17";
			this.fldTotalAmount1.Top = 0.28125F;
			this.fldTotalAmount1.Width = 0.9375F;
			// 
			// fldTotalAmount2
			// 
			this.fldTotalAmount2.Height = 0.1875F;
			this.fldTotalAmount2.Left = 4.1875F;
			this.fldTotalAmount2.Name = "fldTotalAmount2";
			this.fldTotalAmount2.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalAmount2.Text = "Field24";
			this.fldTotalAmount2.Top = 0.46875F;
			this.fldTotalAmount2.Width = 0.9375F;
			// 
			// fldTotalAmount3
			// 
			this.fldTotalAmount3.Height = 0.1875F;
			this.fldTotalAmount3.Left = 4.1875F;
			this.fldTotalAmount3.Name = "fldTotalAmount3";
			this.fldTotalAmount3.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalAmount3.Text = "Field13";
			this.fldTotalAmount3.Top = 0.65625F;
			this.fldTotalAmount3.Width = 0.9375F;
			// 
			// fldTotalAmount4
			// 
			this.fldTotalAmount4.Height = 0.1875F;
			this.fldTotalAmount4.Left = 4.1875F;
			this.fldTotalAmount4.Name = "fldTotalAmount4";
			this.fldTotalAmount4.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalAmount4.Text = "Field14";
			this.fldTotalAmount4.Top = 0.84375F;
			this.fldTotalAmount4.Width = 0.9375F;
			// 
			// fldTotalAmount5
			// 
			this.fldTotalAmount5.Height = 0.1875F;
			this.fldTotalAmount5.Left = 4.1875F;
			this.fldTotalAmount5.Name = "fldTotalAmount5";
			this.fldTotalAmount5.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalAmount5.Text = "Field15";
			this.fldTotalAmount5.Top = 1.03125F;
			this.fldTotalAmount5.Width = 0.9375F;
			// 
			// fldTotalAmount6
			// 
			this.fldTotalAmount6.Height = 0.1875F;
			this.fldTotalAmount6.Left = 4.1875F;
			this.fldTotalAmount6.Name = "fldTotalAmount6";
			this.fldTotalAmount6.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalAmount6.Text = "Field16";
			this.fldTotalAmount6.Top = 1.21875F;
			this.fldTotalAmount6.Width = 0.9375F;
			// 
			// fldTotalSalesTax
			// 
			this.fldTotalSalesTax.Height = 0.1875F;
			this.fldTotalSalesTax.Left = 4.1875F;
			this.fldTotalSalesTax.Name = "fldTotalSalesTax";
			this.fldTotalSalesTax.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalSalesTax.Text = "Field17";
			this.fldTotalSalesTax.Top = 1.40625F;
			this.fldTotalSalesTax.Width = 0.9375F;
			// 
			// fldGrandTotal
			// 
			this.fldGrandTotal.Height = 0.1875F;
			this.fldGrandTotal.Left = 4.1875F;
			this.fldGrandTotal.Name = "fldGrandTotal";
			this.fldGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldGrandTotal.Text = "Field24";
			this.fldGrandTotal.Top = 1.65625F;
			this.fldGrandTotal.Width = 0.9375F;
			// 
			// lblTotalFeeDescription1
			// 
			this.lblTotalFeeDescription1.Height = 0.1875F;
			this.lblTotalFeeDescription1.HyperLink = null;
			this.lblTotalFeeDescription1.Left = 3.03125F;
			this.lblTotalFeeDescription1.Name = "lblTotalFeeDescription1";
			this.lblTotalFeeDescription1.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblTotalFeeDescription1.Text = "Label8";
			this.lblTotalFeeDescription1.Top = 0.28125F;
			this.lblTotalFeeDescription1.Width = 1.09375F;
			// 
			// lblTotalFeeDescription2
			// 
			this.lblTotalFeeDescription2.Height = 0.1875F;
			this.lblTotalFeeDescription2.HyperLink = null;
			this.lblTotalFeeDescription2.Left = 3.03125F;
			this.lblTotalFeeDescription2.Name = "lblTotalFeeDescription2";
			this.lblTotalFeeDescription2.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblTotalFeeDescription2.Text = "Label12";
			this.lblTotalFeeDescription2.Top = 0.46875F;
			this.lblTotalFeeDescription2.Width = 1.09375F;
			// 
			// lblTotalFeeDescription3
			// 
			this.lblTotalFeeDescription3.Height = 0.1875F;
			this.lblTotalFeeDescription3.HyperLink = null;
			this.lblTotalFeeDescription3.Left = 3.03125F;
			this.lblTotalFeeDescription3.Name = "lblTotalFeeDescription3";
			this.lblTotalFeeDescription3.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblTotalFeeDescription3.Text = "Label8";
			this.lblTotalFeeDescription3.Top = 0.65625F;
			this.lblTotalFeeDescription3.Width = 1.09375F;
			// 
			// lblTotalFeeDescription4
			// 
			this.lblTotalFeeDescription4.Height = 0.1875F;
			this.lblTotalFeeDescription4.HyperLink = null;
			this.lblTotalFeeDescription4.Left = 3.03125F;
			this.lblTotalFeeDescription4.Name = "lblTotalFeeDescription4";
			this.lblTotalFeeDescription4.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblTotalFeeDescription4.Text = "Label14";
			this.lblTotalFeeDescription4.Top = 0.84375F;
			this.lblTotalFeeDescription4.Width = 1.09375F;
			// 
			// lblTotalFeeDescription5
			// 
			this.lblTotalFeeDescription5.Height = 0.1875F;
			this.lblTotalFeeDescription5.HyperLink = null;
			this.lblTotalFeeDescription5.Left = 3.03125F;
			this.lblTotalFeeDescription5.Name = "lblTotalFeeDescription5";
			this.lblTotalFeeDescription5.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblTotalFeeDescription5.Text = "Label8";
			this.lblTotalFeeDescription5.Top = 1.03125F;
			this.lblTotalFeeDescription5.Width = 1.09375F;
			// 
			// lblTotalFeeDescription6
			// 
			this.lblTotalFeeDescription6.Height = 0.1875F;
			this.lblTotalFeeDescription6.HyperLink = null;
			this.lblTotalFeeDescription6.Left = 3.03125F;
			this.lblTotalFeeDescription6.Name = "lblTotalFeeDescription6";
			this.lblTotalFeeDescription6.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblTotalFeeDescription6.Text = "Label16";
			this.lblTotalFeeDescription6.Top = 1.21875F;
			this.lblTotalFeeDescription6.Width = 1.09375F;
			// 
			// lblTotalSalesTax
			// 
			this.lblTotalSalesTax.Height = 0.1875F;
			this.lblTotalSalesTax.HyperLink = null;
			this.lblTotalSalesTax.Left = 3.03125F;
			this.lblTotalSalesTax.Name = "lblTotalSalesTax";
			this.lblTotalSalesTax.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblTotalSalesTax.Text = "Sales Tax";
			this.lblTotalSalesTax.Top = 1.40625F;
			this.lblTotalSalesTax.Width = 1.09375F;
			// 
			// lblGrandTotal
			// 
			this.lblGrandTotal.Height = 0.1875F;
			this.lblGrandTotal.HyperLink = null;
			this.lblGrandTotal.Left = 3.03125F;
			this.lblGrandTotal.Name = "lblGrandTotal";
			this.lblGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblGrandTotal.Text = "Grand Total";
			this.lblGrandTotal.Top = 1.65625F;
			this.lblGrandTotal.Width = 1.09375F;
			// 
			// linGrandTotal
			// 
			this.linGrandTotal.Height = 0F;
			this.linGrandTotal.Left = 3F;
			this.linGrandTotal.LineWeight = 1F;
			this.linGrandTotal.Name = "linGrandTotal";
			this.linGrandTotal.Top = 1.59375F;
			this.linGrandTotal.Width = 2.125F;
			this.linGrandTotal.X1 = 3F;
			this.linGrandTotal.X2 = 5.125F;
			this.linGrandTotal.Y1 = 1.59375F;
			this.linGrandTotal.Y2 = 1.59375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 3.5625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Label21.Text = "Summary";
			this.Label21.Top = 0.0625F;
			this.Label21.Width = 0.875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 3.5625F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.21875F;
			this.Line5.Width = 0.875F;
			this.Line5.X1 = 3.5625F;
			this.Line5.X2 = 4.4375F;
			this.Line5.Y1 = 0.21875F;
			this.Line5.Y2 = 0.21875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblBillInfo,
				this.lblTitle1,
				this.lblTitle2,
				this.lblTitle3,
				this.lblTitle4,
				this.Line1,
				this.Label8
			});
			this.PageHeader.Height = 0.9270833F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Billing Edit Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 8F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.71875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.71875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblBillInfo
			// 
			this.lblBillInfo.Height = 0.21875F;
			this.lblBillInfo.HyperLink = null;
			this.lblBillInfo.Left = 0F;
			this.lblBillInfo.Name = "lblBillInfo";
			this.lblBillInfo.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblBillInfo.Text = "Expense";
			this.lblBillInfo.Top = 0.375F;
			this.lblBillInfo.Width = 8F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.1875F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 3.09375F;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblTitle1.Text = "Label8";
			this.lblTitle1.Top = 0.71875F;
			this.lblTitle1.Width = 0.75F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.1875F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 3.875F;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblTitle2.Text = "Label8";
			this.lblTitle2.Top = 0.71875F;
			this.lblTitle2.Width = 0.75F;
			// 
			// lblTitle3
			// 
			this.lblTitle3.Height = 0.1875F;
			this.lblTitle3.HyperLink = null;
			this.lblTitle3.Left = 4.65625F;
			this.lblTitle3.Name = "lblTitle3";
			this.lblTitle3.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblTitle3.Text = "Label8";
			this.lblTitle3.Top = 0.71875F;
			this.lblTitle3.Width = 0.75F;
			// 
			// lblTitle4
			// 
			this.lblTitle4.Height = 0.1875F;
			this.lblTitle4.HyperLink = null;
			this.lblTitle4.Left = 5.4375F;
			this.lblTitle4.Name = "lblTitle4";
			this.lblTitle4.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.lblTitle4.Text = "Label10";
			this.lblTitle4.Top = 0.71875F;
			this.lblTitle4.Width = 0.75F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.90625F;
			this.Line1.Width = 8F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 8F;
			this.Line1.Y1 = 0.90625F;
			this.Line1.Y2 = 0.90625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label8.Text = "Customer";
			this.Label8.Top = 0.71875F;
			this.Label8.Width = 0.75F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptBillingEditReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptBillingEditReport_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldDetail1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCustomerID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFeeDescription6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSalesTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldQuantity6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoiceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalSalesTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalFeeDescription6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalSalesTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnitCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnitCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnitCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnitCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnitCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnitCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCustomerID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFeeDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFeeDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFeeDescription3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFeeDescription4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFeeDescription5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFeeDescription6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSalesTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInvoiceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuantity1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuantity2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuantity3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuantity4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuantity5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldQuantity6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoiceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line linInvoiceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalSalesTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalFeeDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalFeeDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalFeeDescription3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalFeeDescription4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalFeeDescription5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalFeeDescription6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalSalesTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line linGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillInfo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
