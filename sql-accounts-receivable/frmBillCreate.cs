﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWAR0000
{
    /// <summary>
    /// Summary description for frmBillCreate.
    /// </summary>
    public partial class frmBillCreate : BaseForm
    {
        public frmBillCreate()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmBillCreate InstancePtr
        {
            get
            {
                return (frmBillCreate)Sys.GetInstance(typeof(frmBillCreate));
            }
        }

        protected frmBillCreate _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By     Dave Wade
        // Date           5/10/2008
        // This form will be used to select an already existing Rate Record
        // or add a new one and then create bills
        // ********************************************************
        int BillKeyCol;
        int AccountCol;
        int NameCol;
        // vbPorter upgrade warning: intBillType As short --> As int	OnWrite(string, int)
        public int intBillType;
        bool blnDemandBill;
        int intInterestDays;
        int intBillMonth;
        int intBillYear;

        private void frmBillCreate_Activated(object sender, System.EventArgs e)
        {
            if (modGlobal.FormExist(this))
            {
                return;
            }
            if (cboRateKeys.Items.Count == 1 && !blnDemandBill)
            {
                mnuProcessSave_Click();
            }
            Refresh();
        }

        private void frmBillCreate_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmBillCreate.FillStyle	= 0;
            //frmBillCreate.ScaleWidth	= 5880;
            //frmBillCreate.ScaleHeight	= 4410;
            //frmBillCreate.LinkTopic	= "Form2";
            //frmBillCreate.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties
            clsDRWrapper rsBillInfo = new clsDRWrapper();
            BillKeyCol = 0;
            AccountCol = 1;
            NameCol = 2;
            vsAccounts.ColHidden(BillKeyCol, true);
            vsAccounts.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAccounts.ColWidth(AccountCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.3));
            vsAccounts.TextMatrix(0, AccountCol, "Cust #");
            vsAccounts.TextMatrix(0, NameCol, "Name");
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
            modGlobalFunctions.SetTRIOColors(this);
            LoadRateKeys();
            cboRateKeys.SelectedIndex = cboRateKeys.Items.Count - 1;
            rsBillInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
            if (rsBillInfo.EndOfFile() != true && rsBillInfo.BeginningOfFile() != true)
            {
                if (FCConvert.ToString(rsBillInfo.Get_Fields_String("FrequencyCode")) == "D")
                {
                    blnDemandBill = true;
                }
                else
                {
                    blnDemandBill = false;
                }
                intInterestDays = FCConvert.ToInt32(rsBillInfo.Get_Fields_Int32("InterestStartDate"));
                FillDefaultBillInformation();
            }
            else
            {
                blnDemandBill = false;
                intInterestDays = -1;
            }
            if (blnDemandBill)
            {
                fraBillInfo.Visible = true;
                lblRateSelect.Visible = false;
                cboRateKeys.Visible = false;
            }
            else
            {
                fraBillInfo.Visible = false;
                lblRateSelect.Visible = true;
                cboRateKeys.Visible = true;
            }
            intBillMonth = modGlobal.PeriodCalc(modRegistry.GetRegistryKey("BILLMONTH"));
            intBillYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("BILLYEAR"))));
        }

        private void frmBillCreate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // catches the escape and enter keys
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                Close();
            }
            else if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            frmBillSelect.InstancePtr.Unload();
            //MDIParent.InstancePtr.Focus();
        }

        private void frmBillCreate_Resize(object sender, System.EventArgs e)
        {
            vsAccounts.ColWidth(AccountCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.3));
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void LoadRateKeys()
        {
            clsDRWrapper rsInfo = new clsDRWrapper();
            cboRateKeys.Clear();
            cboRateKeys.AddItem("Create New Rate Record");
            //FC:FINAL:SBE - #418 - in VB6 form is loaded when you access any property from it. Force form load
            frmBillSelect.InstancePtr.LoadForm();
            rsInfo.OpenRecordset("SELECT * FROM RateKeys WHERE BillMonth = " + FCConvert.ToString(FCConvert.ToDateTime(frmBillSelect.InstancePtr.cboMonth.Text + "/1/2000").Month) + " AND BillYear = " + frmBillSelect.InstancePtr.cboYear.Text + " AND BillType = " + FCConvert.ToString(intBillType) + " ORDER BY ID DESC");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                do
                {
                    // TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
                    cboRateKeys.AddItem(Strings.Format(rsInfo.Get_Fields("BillType"), "000") + "   " + Strings.Format(rsInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "   " + rsInfo.Get_Fields_String("Description"));
                    cboRateKeys.ItemData(cboRateKeys.NewIndex, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID")));
                    rsInfo.MoveNext();
                }
                while (rsInfo.EndOfFile() != true);
            }
        }

        private void txtBillDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Information.IsDate(txtBillDate.Text))
            {
                txtInterestStartDate.Text = Strings.Format(DateAndTime.DateAdd("d", intInterestDays, DateAndTime.DateValue(txtBillDate.Text)), "MM/dd/yyyy");
                txtInterestStartDate.ToolTipText = FCConvert.ToString(DateAndTime.DateDiff("d", DateAndTime.DateValue(txtBillDate.Text), DateAndTime.DateValue(txtInterestStartDate.Text))) + " days after billing";
            }
            else
            {
                MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void txtInterestStartDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Information.IsDate(txtInterestStartDate.Text))
            {
                txtInterestStartDate.ToolTipText = FCConvert.ToString(DateAndTime.DateDiff("d", DateAndTime.DateValue(txtBillDate.Text), DateAndTime.DateValue(txtInterestStartDate.Text))) + " days after billing";
            }
            else
            {
                MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void mnuProcessSave_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: counter As short, int --> As DialogResult
            DialogResult counter;
            bool blnData;
            int lngRateRecord = 0;
            if (isEnteringBillInfo())
            {
                if (isValidBillInfo())
                {
                    lngRateRecord = CreateRateRecord();
                    LoadRateKeys();
                    SetRateCombo(ref lngRateRecord);
                    fraBillInfo.Visible = false;
                    LoadBillGrid();
                }
            }
            else if (isChoosingRateKeys())
            {
                if (cboRateKeys.SelectedIndex == 0)
                {
                    Hide();
                    frmAddRateRecord.InstancePtr.Init(2, DateTime.Today, lngPassBillYear: FCConvert.ToInt32(FCConvert.ToDouble(frmBillSelect.InstancePtr.cboYear.Text)), intPassBillMonth: FCConvert.ToInt16(FCConvert.ToDateTime(frmBillSelect.InstancePtr.cboMonth.Text + "/1/2000").Month), lngPassType: intBillType);
                    LoadRateKeys();
                    if (cboRateKeys.Items.Count > 1)
                    {
                        cboRateKeys.SelectedIndex = 1;
                    }
                    else
                    {
                        cboRateKeys.SelectedIndex = 0;
                    }
                    Show(App.MainForm);
                }
                else
                {
                    LoadBillGrid();
                }
            }
            else
            {
                if (hasSelectedAccount())
                {
                    CreateBills();
                }
                else
                {
                    MessageBox.Show("You must select at least one customer before creating bills.", "No Customers Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private bool isEnteringBillInfo()
        {
            return (txtBillDate.Visible == true);
        }

        private bool isChoosingRateKeys()
        {
            return (cboRateKeys.Visible == true);
        }

        private bool hasSelectedAccount()
        {
            return (vsAccounts.Rows >= 1);
        }

        private bool isValidBillInfo()
        {
            return (hasValidBillDate() && hasValidInterestStartDate() && hasValidFlatAmount() && hasValidPerDiem());
        }

        private bool hasValidBillDate()
        {
            if (Information.IsDate(txtBillDate.Text))
            {
                return true;
            }
            else
            {
                MessageBox.Show("You must enter a valid date for the bill date before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        private bool hasValidInterestStartDate()
        {
            if (Information.IsDate(txtInterestStartDate.Text))
            {
                return true;
            }
            else
            {
                MessageBox.Show("You must enter a valid date for the interest start date before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        private bool hasValidFlatAmount()
        {
            bool ret = false;
            if (!txtFlatAmount.Visible)
            {
                ret = true;
            }
            else if (FCConvert.ToDecimal(txtFlatAmount.Text) > 0)
            {
                ret = true;
            }
            else
            {
                DialogResult resp = MessageBox.Show(
                        "You have not entered an interest amount to be applied to the bill.  Do you wish to continue the billing process with no late penalty?", "No Late Penalty?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resp == DialogResult.Yes)
                {
                    ret = true;
                }
            }
            return ret;
        }

        private bool hasValidPerDiem()
        {
            bool ret = false;
            if (!txtPerDiem.Visible)
            {
                ret = true;
            }
            else if (FCConvert.ToDouble(txtPerDiem.Text) > 0)
            {
                ret = true;
            }
            else
            {
                DialogResult resp = MessageBox.Show(
                    "You have not entered a per diem to be applied to the bill.  Do you wish to continue the billing process with no per diem?", "No Per Diem?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resp == DialogResult.Yes)
                {
                    ret = true;
                }

            }
            return ret;
        }

        public void mnuProcessSave_Click()
        {
            mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
        }

        private void LoadBillGrid()
        {
            clsDRWrapper rsInfo = new clsDRWrapper();
            // vbPorter upgrade warning: intAns As short, int --> As DialogResult
            DialogResult intAns;
            clsDRWrapper rsTypeInfo = new clsDRWrapper();

            try
            {
                int lngRateRecord = 0;
                vsAccounts.Rows = 1;
                // rsInfo.OpenRecordset "SELECT * FROM CustomerMaster INNER JOIN CustomerBills ON CustomerMaster.CustomerID = CustomerBills.CustomerID WHERE Exclude = 0 AND CustomerMaster.Status = 'A' AND CustomerBills.Type = " & intBillType & " AND CustomerMaster.CustomerID NOT IN (SELECT ActualAccountNumber FROM Bill WHERE BillStatus <> 'V' AND BillNumber = " & Val(cboRateKeys.ItemData(cboRateKeys.ListIndex)) & ") ORDER BY Name"
                rsInfo.OpenRecordset("SELECT b.ID as ID, c.CustomerID as CustomerID, p.* FROM CustomerBills as b INNER JOIN CustomerMaster as c ON b.CustomerID = c.CustomerID CROSS APPLY " + rsInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE Exclude = 0 AND c.Status = 'A' AND b.Type = " + FCConvert.ToString(intBillType) + " AND c.CustomerID NOT IN (SELECT ActualAccountNumber FROM Bill WHERE BillStatus <> 'V' AND BillNumber = " + FCConvert.ToString(cboRateKeys.ItemData(cboRateKeys.SelectedIndex)) + ") ORDER BY p.FullName");
                if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                {
                    do
                    {
                        vsAccounts.AddItem(rsInfo.Get_Fields_Int32("ID") + "\t" + Strings.Format(rsInfo.Get_Fields_Int32("CustomerID"), "0000") + "\t" + rsInfo.Get_Fields_String("FullName"));
                        rsInfo.MoveNext();
                    }
                    while (rsInfo.EndOfFile() != true);
                    lblRateSelect.Visible = false;
                    cboRateKeys.Visible = false;
                    lblSelectAccounts.Visible = true;
                    vsAccounts.Visible = true;
                }
                else
                {
                    rsTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboRateKeys.Text, 3))));
                    if (FCConvert.ToString(rsTypeInfo.Get_Fields_String("FrequencyCode")) == "D")
                    {
                        intAns = MessageBox.Show("There are no accounts that have not been billed for the selected rate record.  Would you like to create another rate record to create bills for?", "Create New Rate Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (intAns == DialogResult.Yes)
                        {
                            lngRateRecord = CreateRateRecord(true);
                            LoadRateKeys();
                            SetRateCombo(ref lngRateRecord);
                            fraBillInfo.Visible = false;
                            LoadBillGrid();
                        }
                        else
                        {
                            Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("There are no accounts that have not been billed for the selected rate record.  If you wish to recreate a bill you must go into the Print Bills screen and delete the current bill then create bills again for the rate record.", "No Accounts Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            finally
            {
                rsTypeInfo.DisposeOf();
                rsInfo.DisposeOf();
            }
        }

        private void CreateBills()
        {
            // vbPorter upgrade warning: counter As short, int --> As DialogResult
            //FC:FINAL:PJ: Use correct type for counter
            int counter;
            clsDRWrapper rsBill = new clsDRWrapper();
            clsDRWrapper rsCustomerBill = new clsDRWrapper();
            clsDRWrapper rsTypeInfo = new clsDRWrapper();
            // vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
            Decimal curTotalAmount;
            string strBillSQL;
            int lngJournal = 0;
            clsDRWrapper rsLastInvoice = new clsDRWrapper();
            // vbPorter upgrade warning: lngCurrentInvoice As int	OnWrite(double, int)
            int lngCurrentInvoice = 0;
            clsDRWrapper rsRateKeyInfo = new clsDRWrapper();
            DateTime datStart;
            DateTime datEnd;
            clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
            clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();

            try
            { 

            strBillSQL = "(";
            var rateKeyPrefix = FCConvert.ToString(Conversion.Val(Strings.Left(cboRateKeys.Text, 3)));
            var rateKeyInvoiceStart = FCConvert.ToString(Conversion.Val(Strings.Mid(cboRateKeys.Text, 15, 2)));

            rsTypeInfo.OpenRecordset($"SELECT * FROM DefaultBillTypes WHERE TypeCode = {rateKeyPrefix}");
            rsLastInvoice.OpenRecordset($"SELECT TOP 1 * FROM Bill WHERE BillType = {rateKeyPrefix} AND convert(int, left(InvoiceNumber, 2)) = {rateKeyInvoiceStart} ORDER BY InvoiceNumber DESC");
            if (rsLastInvoice.EndOfFile() != true && rsLastInvoice.BeginningOfFile() != true)
            {
                lngCurrentInvoice = FCConvert.ToInt32(Conversion.Val(Strings.Right(FCConvert.ToString(rsLastInvoice.Get_Fields_String("InvoiceNumber")), 4)) + 1);
            }
            else
            {
                lngCurrentInvoice = 1;
            }
            for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
            {
                // rsCustomerBill.OpenRecordset "SELECT * FROM CustomerBills INNER JOIN CustomerMaster ON CustomerMaster.CustomerID = CustomerBills.CustomerID WHERE Exclude = 0 AND CustomerMaster.CustomerID = " & Val(vsAccounts.TextMatrix(counter, AccountCol)) & " AND CustomerBills.ID = " & Val(vsAccounts.TextMatrix(counter, BillKeyCol))
                rsCustomerBill.OpenRecordset("SELECT b.*, c.SalesTax, c.ID as CustomerMasterID, p.* FROM CustomerBills as b INNER JOIN CustomerMaster as c ON b.CustomerID = c.CustomerID CROSS APPLY " + rsCustomerBill.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE Exclude = 0 AND c.CustomerID = " + FCConvert.ToString(Conversion.Val(vsAccounts.TextMatrix(FCConvert.ToInt32(counter), AccountCol))) + " AND b.ID = " + FCConvert.ToString(Conversion.Val(vsAccounts.TextMatrix(FCConvert.ToInt32(counter), BillKeyCol))));
                if (rsCustomerBill.EndOfFile() != true && rsCustomerBill.BeginningOfFile() != true)
                {
                    // TODO: Field [CustomerMasterID] not found!! (maybe it is an alias?)
                    rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsCustomerBill.Get_Fields("CustomerMasterID") + " AND IsNull(BillNumber, 0) = 0");
                    if (rsBill.EndOfFile() != true && rsBill.BeginningOfFile() != true)
                    {
                        rsBill.Edit();
                    }
                    else
                    {
                        rsBill.AddNew();
                        rsBill.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                    }
                    // TODO: Field [CustomerMasterID] not found!! (maybe it is an alias?)
                    rsBill.Set_Fields("AccountKey", rsCustomerBill.Get_Fields("CustomerMasterID"));
                    rsBill.Set_Fields("BillStatus", "A");
                    rsBill.Set_Fields("BName", rsCustomerBill.Get_Fields_String("FullName"));
                    rsBill.Set_Fields("BAddress1", rsCustomerBill.Get_Fields_String("Address1"));
                    rsBill.Set_Fields("BAddress2", rsCustomerBill.Get_Fields_String("Address2"));
                    rsBill.Set_Fields("BAddress3", rsCustomerBill.Get_Fields_String("Address3"));
                    rsBill.Set_Fields("BCity", rsCustomerBill.Get_Fields_String("City"));
                    // TODO: Field [PartyState] not found!! (maybe it is an alias?)
                    rsBill.Set_Fields("BState", rsCustomerBill.Get_Fields("PartyState"));
                    rsBill.Set_Fields("BZip", rsCustomerBill.Get_Fields_String("Zip"));
                    rsBill.Set_Fields("BZip4", "");
                    rsBill.Set_Fields("ActualAccountNumber", FCConvert.ToString(Conversion.Val(vsAccounts.TextMatrix(FCConvert.ToInt32(counter), AccountCol))));
                    rsBill.Set_Fields("BillType", rateKeyPrefix);
                    rsBill.Set_Fields("BillNumber", cboRateKeys.ItemData(cboRateKeys.SelectedIndex));
                    rsBill.Set_Fields("InvoiceNumber", Strings.Mid(cboRateKeys.Text, 15, 2) + Strings.Left(cboRateKeys.Text, 3) + Strings.Format(lngCurrentInvoice, "0000"));
                    lngCurrentInvoice += 1;
                    curTotalAmount = 0;
                    rsRateKeyInfo.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(cboRateKeys.ItemData(cboRateKeys.SelectedIndex)));
                    if (rsRateKeyInfo.EndOfFile() != true && rsRateKeyInfo.BeginningOfFile() != true)
                    {
                        datStart = (DateTime)rsRateKeyInfo.Get_Fields_DateTime("Start");
                        // TODO: Check the table for the column [End] and replace with corresponding Get_Field method
                        datEnd = (DateTime)rsRateKeyInfo.Get_Fields("End");
                        rsBill.Set_Fields("BillDate", rsRateKeyInfo.Get_Fields_DateTime("BillDate"));
                    }
                    else
                    {
                        datStart = DateTime.Today;
                        datEnd = DateTime.Today;
                        rsBill.Set_Fields("BillDate", DateTime.Today);
                    }
                    if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("Prorate")))
                    {
                        rsBill.Set_Fields("Prorated", true);
                        rsBill.Set_Fields("ProrateStartDate", rsCustomerBill.Get_Fields_DateTime("StartDate"));
                    }
                    else
                    {
                        rsBill.Set_Fields("Prorated", false);
                        rsBill.Set_Fields("ProrateStartDate", null);
                    }
                    if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("Prorate")))
                    {
                        // TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount1")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount1", ProrateBillAmount_8(Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount1")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount1", ProrateBillAmount_8(Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount1")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                    }
                    else
                    {
                        // TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount1")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount1", Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount1")));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount1", Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount1")));
                        }
                    }
                    rsBill.Set_Fields("Description1", Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title1"))));
                    // TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
                    curTotalAmount += Convert.ToDecimal(Math.Round(Convert.ToDouble(rsBill.Get_Fields("Amount1")) * Convert.ToDouble(rsCustomerBill.Get_Fields_Double("Quantity1")), 2));
                    if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("Prorate")))
                    {
                        // TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount2")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount2", ProrateBillAmount_8(Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount2")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount2", ProrateBillAmount_8(Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount2")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                    }
                    else
                    {
                        // TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount2")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount2", Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount2")));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount2", Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount2")));
                        }
                    }
                    rsBill.Set_Fields("Description2", Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title2"))));
                    // TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
                    curTotalAmount += Convert.ToDecimal(Math.Round(Convert.ToDouble(rsBill.Get_Fields("Amount2")) * Convert.ToDouble(rsCustomerBill.Get_Fields_Double("Quantity2")), 2));
                    if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("Prorate")))
                    {
                        // TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount3")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount3", ProrateBillAmount_8(Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount3")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount3", ProrateBillAmount_8(Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount3")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                    }
                    else
                    {
                        // TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount3")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount3", Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount3")));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount3", Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount3")));
                        }
                    }
                    rsBill.Set_Fields("Description3", Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title3"))));
                    // TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
                    curTotalAmount += Convert.ToDecimal(Math.Round(Convert.ToDouble(rsBill.Get_Fields("Amount3")) * Convert.ToDouble(rsCustomerBill.Get_Fields_Double("Quantity3")), 2));
                    if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("Prorate")))
                    {
                        // TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount4")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount4", ProrateBillAmount_8(Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount4")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount4", ProrateBillAmount_8(Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount4")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                    }
                    else
                    {
                        // TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount4")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount4", Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount4")));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount4", Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount4")));
                        }
                    }
                    rsBill.Set_Fields("Description4", Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title4"))));
                    // TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
                    curTotalAmount += Convert.ToDecimal(Math.Round(Convert.ToDecimal(rsBill.Get_Fields("Amount4")) * Convert.ToDecimal(rsCustomerBill.Get_Fields_Double("Quantity4")), 2));
                    if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("Prorate")))
                    {
                        // TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount5")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount5", ProrateBillAmount_8(Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount5")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount5", ProrateBillAmount_8(Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount5")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                    }
                    else
                    {
                        // TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount5")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount5", Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount5")));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount5", Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount5")));
                        }
                    }
                    rsBill.Set_Fields("Description5", Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title5"))));
                    // TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
                    curTotalAmount += Convert.ToDecimal(Math.Round(Convert.ToDouble(rsBill.Get_Fields("Amount5")) * Convert.ToDouble(rsCustomerBill.Get_Fields_Double("Quantity5")), 2));
                    if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("Prorate")))
                    {
                        // TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount6")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount6", ProrateBillAmount_8(Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount6")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount6", ProrateBillAmount_8(Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount6")), rsCustomerBill.Get_Fields_DateTime("StartDate"), datStart, datEnd));
                        }
                    }
                    else
                    {
                        // TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
                        if (Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount6")) >= 0)
                        {
                            // TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
                            rsBill.Set_Fields("Amount6", Convert.ToDecimal(rsCustomerBill.Get_Fields("Amount6")));
                        }
                        else
                        {
                            rsBill.Set_Fields("Amount6", Convert.ToDecimal(rsTypeInfo.Get_Fields_Double("DefaultAmount6")));
                        }
                    }
                    rsBill.Set_Fields("Description6", Strings.Trim(FCConvert.ToString(rsTypeInfo.Get_Fields_String("Title6"))));
                    // TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
                    curTotalAmount += Convert.ToDecimal(Math.Round(Convert.ToDouble(rsBill.Get_Fields("Amount6")) * Convert.ToDouble(rsCustomerBill.Get_Fields_Double("Quantity6")), 2));
                    rsBill.Set_Fields("Quantity1", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_Double("Quantity1"))));
                    rsBill.Set_Fields("Quantity2", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_Double("Quantity2"))));
                    rsBill.Set_Fields("Quantity3", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_Double("Quantity3"))));
                    rsBill.Set_Fields("Quantity4", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_Double("Quantity4"))));
                    rsBill.Set_Fields("Quantity5", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_Double("Quantity5"))));
                    rsBill.Set_Fields("Quantity6", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_Double("Quantity6"))));
                    rsBill.Set_Fields("PrinOwed", curTotalAmount);
                    if (FCConvert.ToBoolean(rsTypeInfo.Get_Fields_Boolean("SalesTax")))
                    {
                        if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("SalesTax")))
                        {
                            rsBill.Set_Fields("TaxOwed", FCConvert.ToDecimal(Math.Round(FCConvert.ToDouble(curTotalAmount) * FCConvert.ToDouble(modGlobal.Statics.dblSalesTax), 2)));
                        }
                        else
                        {
                            rsBill.Set_Fields("TaxOwed", 0);
                        }
                    }
                    rsBill.Set_Fields("Reference", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_String("Reference"))));
                    rsBill.Set_Fields("Control1", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_String("Control1"))));
                    rsBill.Set_Fields("Control2", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_String("Control2"))));
                    rsBill.Set_Fields("Control3", Strings.Trim(FCConvert.ToString(rsCustomerBill.Get_Fields_String("Control3"))));
                    rsBill.Set_Fields("CreationDate", DateTime.Today);
                    rsBill.Set_Fields("LastUpdatedDate", DateTime.Today);
                    rsBill.Update(true);
                    strBillSQL += rsBill.Get_Fields_Int32("ID") + ", ";
                    if (FCConvert.ToBoolean(rsCustomerBill.Get_Fields_Boolean("Prorate")))
                    {
                        rsCustomerBill.Edit();
                        rsCustomerBill.Set_Fields("Prorate", false);
                        rsCustomerBill.Set_Fields("StartDate", null);
                        rsCustomerBill.Update();
                    }
                }
            }
            if (blnDemandBill)
            {
                rsCustomerBill.Execute("DELETE FROM CustomerBills WHERE Type = " + FCConvert.ToString(intBillType), "TWAR0000.vb1");
            }
            else
            {
                rsCustomerBill.Execute("UPDATE CustomerBills SET Exclude = 0 WHERE Type = " + FCConvert.ToString(intBillType), "TWAR0000.vb1");
            }
            
            var freqIsD = FCConvert.ToString(rsTypeInfo.Get_Fields_String("FrequencyCode")) == "D";
            if (strBillSQL != "(" && !freqIsD)
            {
                lngJournal = 0;
                strBillSQL = Strings.Left(strBillSQL, strBillSQL.Length - 2) + ")";
                if (modGlobalConstants.Statics.gboolBD)
                {
                    modARCalculations.CreateJournalEntry_3(ref lngJournal, strBillSQL);
                    if (lngJournal == 0)
                    {
                        MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the Budgetary Journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        if (modSecurity.ValidPermissions_8(null, modGlobal.AUTOPOSTBILLINGJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptARBills))
                        {
                            if (MessageBox.Show("The billing entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                clsJournalInfo = new clsPostingJournalInfo();
                                clsJournalInfo.JournalNumber = lngJournal;
                                clsJournalInfo.JournalType = "GJ";
                                clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
                                clsJournalInfo.CheckDate = "";
                                clsPostInfo.AddJournal(clsJournalInfo);
                                clsPostInfo.AllowPreview = true;
                                clsPostInfo.WaitForReportToEnd = true;
                                clsPostInfo.PostJournals();
                            }
                        }
                        else
                        {
                            MessageBox.Show("The billing entries have been saved into journal " + Strings.Format(lngJournal, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }

            if (strBillSQL != "(" && freqIsD)
            {
                if (Strings.Right(strBillSQL, 1) != ")")
                {
                    strBillSQL = Strings.Left(strBillSQL, strBillSQL.Length - 2) + ")";
                }
                //FC:FINAL:PJ: Use correct type for DialogResult
                DialogResult dlgResult = MessageBox.Show("Bills created successfully!  Would you like to print bills now?", "Print Bills?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult == DialogResult.Yes)
                {
                    frmBillPrint.InstancePtr.intBillType = intBillType;
                    frmBillPrint.InstancePtr.strBillSQL = strBillSQL;
                    Close();
                    frmBillPrint.InstancePtr.Show(App.MainForm);
                    frmBillPrint.InstancePtr.Focus();
                }
                else
                {
                    Close();
                }
            }
            else
            {
                MessageBox.Show("Bills created successfully!", "Billed Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                rptBillTransferReport.InstancePtr.Init(cboRateKeys.ItemData(cboRateKeys.SelectedIndex), FCConvert.ToDateTime(modGlobal.GetBillingDateFromRate_2(cboRateKeys.ItemData(cboRateKeys.SelectedIndex))));
                Close();
            }
            }
            finally
            {
              rsTypeInfo.DisposeOf();
              rsBill.DisposeOf();
              rsCustomerBill.DisposeOf();
              rsLastInvoice.DisposeOf();
              rsRateKeyInfo.DisposeOf();
            }
        }
        // vbPorter upgrade warning: 'Return' As Decimal	OnWrite(int, Decimal)
        private Decimal ProrateBillAmount_8(Decimal curAmt, DateTime datProratedStart, DateTime datBillStart, DateTime datBillEnd)
        {
            return ProrateBillAmount(ref curAmt, ref datProratedStart, ref datBillStart, ref datBillEnd);
        }

        private Decimal ProrateBillAmount(ref Decimal curAmt, ref DateTime datProratedStart, ref DateTime datBillStart, ref DateTime datBillEnd)
        {
            Decimal ProrateBillAmount = 0;
            // vbPorter upgrade warning: intTotalDays As short, int --> As long
            long intTotalDays;
            // vbPorter upgrade warning: intProratedDays As short, int --> As long
            long intProratedDays;
            intTotalDays = DateAndTime.DateDiff("d", datBillStart, datBillEnd);
            intProratedDays = DateAndTime.DateDiff("d", datProratedStart, datBillEnd);
            ProrateBillAmount = FCConvert.ToDecimal(FCUtils.Round(FCConvert.ToDouble((curAmt / intTotalDays) * intProratedDays), 4));
            if (ProrateBillAmount > curAmt)
            {
                ProrateBillAmount = curAmt;
            }
            return ProrateBillAmount;
        }

        private void cboInterestMethod_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            clsDRWrapper rsTitleInfo = new clsDRWrapper();
            rsTitleInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
            if (Strings.Mid(cboInterestMethod.Text, 2, 1) == "F")
            {
                fraFlat.Visible = true;
                fraPerDiem.Visible = false;
                if (rsTitleInfo.EndOfFile() != true && rsTitleInfo.BeginningOfFile() != true)
                {
                    if (Strings.Right(FCConvert.ToString(rsTitleInfo.Get_Fields_String("InterestMethod")), 1) == "P")
                    {
                        txtFlatAmount.Text = Strings.Format(rsTitleInfo.Get_Fields_Decimal("FlatAmount"), "#,##0.00");
                    }
                    else
                    {
                        txtFlatAmount.Text = Strings.Format(modGlobal.Statics.curDefaultFlatRate, "#,##0.00");
                    }
                }
                else
                {
                    txtFlatAmount.Text = Strings.Format(modGlobal.Statics.curDefaultFlatRate, "#,##0.00");
                }
            }
            else
            {
                fraPerDiem.Visible = true;
                fraFlat.Visible = false;
                if (rsTitleInfo.EndOfFile() != true && rsTitleInfo.BeginningOfFile() != true)
                {
                    txtPerDiem.Text = Strings.Right(FCConvert.ToString(rsTitleInfo.Get_Fields_String("InterestMethod")), 1) == "P" 
                        ? Strings.Format(FCConvert.ToInt16(rsTitleInfo.Get_Fields_Double("PerDiemRate")) * 100, "0.00") 
                        : Strings.Format(modGlobal.Statics.dblDefaultPerDiem, "0.00");
                }
                else
                {
                    txtPerDiem.Text = Strings.Format(modGlobal.Statics.dblDefaultPerDiem, "0.00");
                }
            }
        }

        private void cboInterestMethod_DropDown(object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(cboInterestMethod.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
        }

        private void FillDefaultBillInformation()
        {
            clsDRWrapper rsDefaultInfo = new clsDRWrapper();
            rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
            if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
            {
                txtBillDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                txtInterestStartDate.Text = Strings.Format(DateAndTime.DateAdd("d", rsDefaultInfo.Get_Fields_Int32("InterestStartDate"), DateTime.Today), "MM/dd/yyyy");
                txtInterestStartDate.ToolTipText = rsDefaultInfo.Get_Fields_Int32("InterestStartDate") + " days after billing";
                intInterestDays = FCConvert.ToInt32(rsDefaultInfo.Get_Fields_Int32("InterestStartDate"));
                SetInterestMethodCombo_2(rsDefaultInfo.Get_Fields_String("InterestMethod"));
                if (Strings.Mid(cboInterestMethod.Text, 2, 1) == "F")
                {
                    txtFlatAmount.Text = Strings.Format(Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("FlatAmount")), "#,##0.00");
                    txtPerDiem.Text = "0.00";
                }
                else
                {
                    txtPerDiem.Text = Strings.Format(Conversion.Val(rsDefaultInfo.Get_Fields_Double("PerDiemRate")) * 100, "0.00");
                    txtFlatAmount.Text = "0.00";
                }
            }
            else
            {
                MessageBox.Show("Bill Type information cannot be found.", "Invalid Bill Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void SetInterestMethodCombo_2(string strSearch)
        {
            SetInterestMethodCombo(ref strSearch);
        }

        public void SetInterestMethodCombo(ref string strSearch)
        {
            int counter;
            for (counter = 0; counter <= cboInterestMethod.Items.Count - 1; counter++)
            {
                if (strSearch == Strings.Left(cboInterestMethod.Items[counter].ToString(), 2))
                {
                    cboInterestMethod.SelectedIndex = counter;
                    break;
                }
            }
        }

        private int CreateRateRecord(bool blnForce = false)
        {
            int CreateRateRecord = 0;
            clsDRWrapper rsRate = new clsDRWrapper();
            clsDRWrapper rsTitleInfo = new clsDRWrapper();
            if (Strings.Mid(cboInterestMethod.Text, 2, 1) == "F")
            {
                rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE BillType = " + FCConvert.ToString(intBillType) + " AND BillMonth = " + FCConvert.ToString(intBillMonth) + " AND BillYear = " + FCConvert.ToString(intBillYear) + " AND BillDate = '" + FCConvert.ToString(DateAndTime.DateValue(txtBillDate.Text)) + "' AND IntStart = '" + FCConvert.ToString(DateAndTime.DateValue(txtInterestStartDate.Text)) + "' AND InterestMethod = '" + Strings.Left(cboInterestMethod.Text, 2) + "' AND FlatRate = " + FCConvert.ToString(FCConvert.ToDecimal(txtFlatAmount.Text)));
            }
            else
            {
                rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE BillType = " + FCConvert.ToString(intBillType) + " AND BillMonth = " + FCConvert.ToString(intBillMonth) + " AND BillYear = " + FCConvert.ToString(intBillYear) + " AND BillDate = '" + FCConvert.ToString(DateAndTime.DateValue(txtBillDate.Text)) + "' AND IntStart = '" + FCConvert.ToString(DateAndTime.DateValue(txtInterestStartDate.Text)) + "' AND InterestMethod = '" + Strings.Left(cboInterestMethod.Text, 2) + "' AND IntRate = " + FCConvert.ToString(FCConvert.ToDouble(txtPerDiem.Text) / 100));
            }
            if (rsRate.EndOfFile() != true && rsRate.BeginningOfFile() != true && !blnForce)
            {
                CreateRateRecord = FCConvert.ToInt32(rsRate.Get_Fields_Int32("ID"));
                return CreateRateRecord;
            }
            else
            {
                rsRate.AddNew();
            }
            rsRate.Set_Fields("BillType", intBillType);
            rsRate.Set_Fields("BillDate", DateAndTime.DateValue(txtBillDate.Text));
            rsRate.Set_Fields("DateCreated", DateTime.Now);
            rsRate.Set_Fields("Start", DateAndTime.DateValue(txtBillDate.Text));
            rsRate.Set_Fields("End", DateAndTime.DateValue(txtBillDate.Text));
            rsRate.Set_Fields("IntStart", DateAndTime.DateValue(txtInterestStartDate.Text));
            if (Strings.Mid(cboInterestMethod.Text, 2, 1) == "P")
            {
                if (Conversion.Val(txtPerDiem.Text) == 0)
                {
                    rsRate.Set_Fields("IntRate", 0);
                }
                else
                {
                    rsRate.Set_Fields("IntRate", FCConvert.ToDouble(txtPerDiem.Text) / 100);
                }
            }
            else
            {
                if (Conversion.Val(txtFlatAmount.Text) == 0)
                {
                    rsRate.Set_Fields("FlatRate", 0);
                }
                else
                {
                    rsRate.Set_Fields("FlatRate", FCConvert.ToDecimal(txtFlatAmount.Text));
                }
            }
            rsRate.Set_Fields("InterestMethod", Strings.Left(cboInterestMethod.Text, 2));
            rsTitleInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intBillType));
            if (rsTitleInfo.EndOfFile() != true && rsTitleInfo.BeginningOfFile() != true)
            {
                rsRate.Set_Fields("Description", rsTitleInfo.Get_Fields_String("TypeTitle") + " " + Strings.Format(intBillMonth, "00") + "/" + FCConvert.ToString(intBillYear));
            }
            else
            {
                rsRate.Set_Fields("Description", "Bill Type: " + FCConvert.ToString(intBillType) + " " + Strings.Format(intBillMonth, "00") + "/" + FCConvert.ToString(intBillYear));
            }
            rsRate.Set_Fields("BillMonth", intBillMonth);
            rsRate.Set_Fields("BillYear", intBillYear);
            rsRate.Update();
            CreateRateRecord = FCConvert.ToInt32(rsRate.Get_Fields_Int32("ID"));
            return CreateRateRecord;
        }

        private void SetRateCombo(ref int lngRateKey)
        {
            int counter;
            for (counter = 1; counter <= cboRateKeys.Items.Count - 1; counter++)
            {
                if (cboRateKeys.ItemData(counter) == lngRateKey)
                {
                    cboRateKeys.SelectedIndex = counter;
                    return;
                }
            }
        }

        private void cmdProcess_Click(object sender, EventArgs e)
        {
            mnuProcessSave_Click();
        }
    }
}
