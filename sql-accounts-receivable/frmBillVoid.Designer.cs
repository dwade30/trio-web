﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillVoid.
	/// </summary>
	partial class frmBillVoid : BaseForm
	{
		public fecherFoundation.FCGrid vsBills;
		public fecherFoundation.FCComboBox cboCustomers;
		public fecherFoundation.FCLabel lblCustomerSelect;
		public fecherFoundation.FCLabel lblBillSelect;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsBills = new fecherFoundation.FCGrid();
			this.cboCustomers = new fecherFoundation.FCComboBox();
			this.lblCustomerSelect = new fecherFoundation.FCLabel();
			this.lblBillSelect = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 458);
			this.BottomPanel.Size = new System.Drawing.Size(659, 15);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Controls.Add(this.vsBills);
			this.ClientArea.Controls.Add(this.cboCustomers);
			this.ClientArea.Controls.Add(this.lblCustomerSelect);
			this.ClientArea.Controls.Add(this.lblBillSelect);
			this.ClientArea.Size = new System.Drawing.Size(659, 398);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(659, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(113, 30);
			this.HeaderText.Text = "Void Bills";
			// 
			// vsBills
			// 
			this.vsBills.AllowSelection = false;
			this.vsBills.AllowUserToResizeColumns = false;
			this.vsBills.AllowUserToResizeRows = false;
			this.vsBills.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBills.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBills.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBills.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBills.BackColorSel = System.Drawing.Color.Empty;
			this.vsBills.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBills.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsBills.ColumnHeadersHeight = 30;
			this.vsBills.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBills.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsBills.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBills.FixedCols = 0;
			this.vsBills.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBills.FrozenCols = 0;
			this.vsBills.GridColor = System.Drawing.Color.Empty;
			this.vsBills.Location = new System.Drawing.Point(30, 82);
			this.vsBills.Name = "vsBills";
			this.vsBills.ReadOnly = true;
			this.vsBills.RowHeadersVisible = false;
			this.vsBills.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBills.RowHeightMin = 0;
			this.vsBills.Rows = 1;
			this.vsBills.ShowColumnVisibilityMenu = false;
			this.vsBills.Size = new System.Drawing.Size(602, 217);
			this.vsBills.StandardTab = true;
			this.vsBills.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBills.TabIndex = 3;
			this.vsBills.Visible = false;
			this.vsBills.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBills_KeyDownEvent);
			this.vsBills.DoubleClick += new System.EventHandler(this.vsBills_DblClick);
			// 
			// cboCustomers
			// 
			this.cboCustomers.AutoSize = false;
			this.cboCustomers.BackColor = System.Drawing.SystemColors.Window;
			this.cboCustomers.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCustomers.FormattingEnabled = true;
			this.cboCustomers.Location = new System.Drawing.Point(30, 68);
			this.cboCustomers.Name = "cboCustomers";
			this.cboCustomers.Size = new System.Drawing.Size(461, 40);
			this.cboCustomers.Sorted = true;
			this.cboCustomers.TabIndex = 1;
			// 
			// lblCustomerSelect
			// 
			this.lblCustomerSelect.Location = new System.Drawing.Point(30, 30);
			this.lblCustomerSelect.Name = "lblCustomerSelect";
			this.lblCustomerSelect.Size = new System.Drawing.Size(461, 19);
			this.lblCustomerSelect.TabIndex = 0;
			this.lblCustomerSelect.Text = "PLEASE SELECT THE CUSTOMER YOU WISH TO VOID A BILL FOR";
			// 
			// lblBillSelect
			// 
			this.lblBillSelect.Location = new System.Drawing.Point(30, 50);
			this.lblBillSelect.Name = "lblBillSelect";
			this.lblBillSelect.Size = new System.Drawing.Size(304, 19);
			this.lblBillSelect.TabIndex = 2;
			this.lblBillSelect.Text = "PLEASE SELECT THE BILL YOU WISH TO VOID";
			this.lblBillSelect.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(30, 328);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(96, 40);
			this.cmdProcess.TabIndex = 0;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// frmBillVoid
			// 
			this.ClientSize = new System.Drawing.Size(659, 473);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBillVoid";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Void Bills";
			this.Load += new System.EventHandler(this.frmBillVoid_Load);
			this.Activated += new System.EventHandler(this.frmBillVoid_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBillVoid_KeyPress);
			this.Resize += new System.EventHandler(this.frmBillVoid_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdProcess;
	}
}
