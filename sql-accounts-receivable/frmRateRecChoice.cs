﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmRateRecChoice.
	/// </summary>
	public partial class frmRateRecChoice : BaseForm
	{
		public frmRateRecChoice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkPrint = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.txtRange = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.chkPrint.AddControlArrayElement(chkPrint_3, 3);
			this.chkPrint.AddControlArrayElement(chkPrint_2, 2);
			this.chkPrint.AddControlArrayElement(chkPrint_1, 1);
			this.chkPrint.AddControlArrayElement(chkPrint_0, 0);
			this.txtRange.AddControlArrayElement(txtRange_1, 1);
			this.txtRange.AddControlArrayElement(txtRange_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			// this will set the default column numbers
			lngBillDateCol = 1;
			lngBillingDateCol = 2;
			lngDueDate1Col = 3;
			lngStatusCol = 4;
			lngDescriptionCol = 5;
			lngHiddenCol = 6;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRateRecChoice InstancePtr
		{
			get
			{
				return (frmRateRecChoice)Sys.GetInstance(typeof(frmRateRecChoice));
			}
		}

		protected frmRateRecChoice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/20/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/13/2006              *
		// ********************************************************
		public int intRateType;
		// this will be set by the form calling this form and will tell which form this should be setup for
		public int intBillType;
		public int lngRateRecNumber;
		int intLienedAccounts;
		clsDRWrapper rsRateRec = new clsDRWrapper();
		bool boolLoaded;
		int intAction;
		int lngBillDateCol;
		// these are the column numbers for each variable so that they can be
		int lngBillingDateCol;
		// moved with minimal effort, since I know they will be (haha)
		int lngDueDate1Col;
		int lngStatusCol;
		int lngDescriptionCol;
		int lngHiddenCol;
		bool boolFormatting;
		bool boolUnloadMe;
		public string strRateKeyList = "";
		bool boolReminderInitial;
		bool boolLoading;

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this function will determine from intRateType which report is calling it and will
			// create the SQL string accordingly then pass it back
			switch (intRateType)
			{
			// *******************************Reminder Notices********************************
				case 30:
					{
						// Reminder Notices
						BuildSQL = "SELECT * FROM RateKeys WHERE BillType = " + FCConvert.ToString(intBillType);
						// *******************************   Load Back    ********************************
						break;
					}
				case 100:
					{
						// this is to select a ID for the load back function
						BuildSQL = "SELECT * FROM RateKeys WHERE BillType = " + FCConvert.ToString(intBillType);
						break;
					}
				case 200:
				case 201:
					{
						// this is the edit type
						BuildSQL = "SELECT * FROM RateKeys WHERE BillType = " + FCConvert.ToString(intBillType);
						break;
					}
				default:
					{
						BuildSQL = "SELECT * FROM RateKeys WHERE BillType = 0";
						// this should produce 0 records
						break;
					}
			}
			//end switch
			return BuildSQL;
		}

		private void cmbBillType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbBillType.SelectedIndex > -1)
			{
				if (intBillType != cmbBillType.ItemData(cmbBillType.SelectedIndex))
				{
					intBillType = cmbBillType.ItemData(cmbBillType.SelectedIndex);
					// save the last year selected for the default year
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "AR\\", "LastRateRecChoiceType", intBillType.ToString());
					if (intRateType == 30)
					{
						// this will force the check of the liened years first
						boolReminderInitial = true;
						ChangeRateKeys();
					}
					else
					{
						// update the rate grid
						ChangeRateKeys();
					}
				}
			}
			else
			{
				intBillType = 0;
			}
		}

		private void cmbBillType_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void cmbReportDetail_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cmbReportDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmdRIClose_Click(object sender, System.EventArgs e)
		{
			fraRateInfo.Visible = false;
		}

		private void frmRateRecChoice_Activated(object sender, System.EventArgs e)
		{
			if (boolUnloadMe || intRateType == -1)
			{
				Close();
				return;
			}
			if (!boolLoaded)
			{
				boolLoaded = true;
				// show that the form was loaded
				boolFormatting = true;
				txtMailDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				if (!fraPrint.Visible)
				{
					SetAct(0, false);
					// load the grid
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				boolFormatting = false;
				if (!modGlobalConstants.Statics.boolMaxForms)
				{
					this.Width = this.Width + 1;
				}
			}
			if (intRateType == 30)
			{
				boolReminderInitial = true;
			}
		}

		private void frmRateRecChoice_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void frmRateRecChoice_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						Support.SendKeys("{Tab}", false);
						break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						if (fraRateInfo.Visible)
						{
							fraRateInfo.Visible = false;
						}
						else
						{
							Close();
						}
						break;
					}
				case Keys.F10:
					{
						// turn off the F10 key
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmRateRecChoice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRateRecChoice.ScaleWidth	= 9045;
			//frmRateRecChoice.ScaleHeight	= 7380;
			//frmRateRecChoice.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			// set the TRIO colors for the controls on this form
			boolFormatting = true;
			// this will make sure that the form knows that I am formatting it instead of the user changing the size of the form
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// set the size of the form
			//Application.DoEvents();
			FormatGrid();
			// format the grid
			intBillType = 0;
			// lblReprint.Text = "Choose inital run only if accounts have changed."
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			boolLoading = true;
			if (FillYearCombo())
			{
				if (intRateType >= 10 && intRateType < 100 && intRateType != 30 && intRateType != 50 && intRateType != 60)
				{
					fraRate.Visible = false;
					fraQuestions.Visible = false;
					//fraPrintType.Left = FCConvert.ToInt32((this.Width - fraPrintType.Width) / 2.0);
					//fraPrintType.Top = FCConvert.ToInt32((this.Height - fraPrintType.Height) / 3.0);
					fraPrint.Visible = true;
					fraPrintOptions.Visible = false;
				}
				else
				{
					//fraPrintOptions.Left = fraPreLienEdit.Left;
					//fraPrintOptions.Top = fraPreLienEdit.Top + fraPreLienEdit.Height; // + 100
					if (intRateType == 100 || intRateType == 200 || intRateType == 201)
					{
						fraPrintOptions.Visible = false;
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				boolFormatting = false;
			}
			else
			{
				boolFormatting = false;
				boolUnloadMe = true;
			}
			boolLoading = false;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			boolUnloadMe = false;
			intAction = 0;
		}

		private void SetAct(short intAct, bool boolQuery = true)
		{
			int intErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				int intTemp;
				// this will set up the grid and show/hide all of the frames depending on intRateType
				// set the cursor to the hourglass
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				intErrCode = 1;
				switch (intRateType)
				{
					case 100:
						{
							// position the grid
							//fraRate.Top = (fraQuestions.Top + fraQuestions.Height) + 500;
							//fraRate.Left = FCConvert.ToInt32((frmRateRecChoice.InstancePtr.Width - fraRate.Width) / 2.0);
							fraRate.Visible = true;
							fraRate.Enabled = true;
							intErrCode = 17;
							// hide the questions
							//fraQuestions.Left = FCConvert.ToInt32((frmRateRecChoice.InstancePtr.Width - fraQuestions.Width) / 2.0);
							fraQuestions.Visible = true;
							fraQuestions.Enabled = true;
							intErrCode = 18;
							// hide the reprint frame
							fraPreLienEdit.Visible = false;
							break;
						}
					case 30:
						{
							// position the grid
							//fraRate.Top = (fraQuestions.Top + fraQuestions.Height);
							//fraRate.Left = FCConvert.ToInt32((frmRateRecChoice.InstancePtr.Width - fraRate.Width) / 2.0);
							fraRate.Visible = true;
							fraRate.Enabled = true;
							intErrCode = 123;
							fraPrint.Visible = false;
							fraPrintOptions.Visible = false;
							// show the questions
							//fraQuestions.Left = FCConvert.ToInt32((frmRateRecChoice.InstancePtr.Width - fraQuestions.Width) / 2.0);
							fraQuestions.Visible = true;
							fraQuestions.Enabled = true;
							intErrCode = 124;
							// hide the reprint frame
							fraPreLienEdit.Visible = false;
							break;
						}
					case 50:
					case 60:
						{
							// position the grid
							//fraRate.Top = (fraQuestions.Top + fraQuestions.Height) + 500;
							//fraRate.Left = FCConvert.ToInt32((frmRateRecChoice.InstancePtr.Width - fraRate.Width) / 2.0);
							fraRate.Visible = true;
							fraRate.Enabled = true;
							intErrCode = 123;
							fraPrint.Visible = false;
							fraPrintOptions.Visible = false;
							// show the questions
							//fraQuestions.Left = FCConvert.ToInt32((frmRateRecChoice.InstancePtr.Width - fraQuestions.Width) / 2.0);
							fraQuestions.Visible = true;
							fraQuestions.Enabled = true;
							intErrCode = 124;
							// hide the reprint frame
							fraPreLienEdit.Visible = false;
							break;
						}
					default:
						{
							// position the grid
							//fraRate.Top = (fraQuestions.Top + fraQuestions.Height) + 500;
							//fraRate.Left = FCConvert.ToInt32((frmRateRecChoice.InstancePtr.Width - fraRate.Width) / 2.0);
							fraRate.Visible = true;
							fraRate.Enabled = true;
							intErrCode = 23;
							// show the questions
							//fraQuestions.Left = FCConvert.ToInt32((frmRateRecChoice.InstancePtr.Width - fraQuestions.Width) / 2.0);
							fraQuestions.Visible = true;
							fraQuestions.Enabled = true;
							intErrCode = 24;
							// hide the reprint frame
							fraPreLienEdit.Visible = false;
							break;
						}
				}
				//end switch
				cmbBillType.Visible = true;
				cmbBillType.Enabled = true;
				if (cmbBillType.Visible)
				{
					// set the default year from the last time this was entered
					intErrCode = 25;
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "AR\\", "LastRateRecChoiceType", ref strTemp);
					if (cmbBillType.Items.Count > 0)
					{
						// set the default year if it is in the list
						for (intTemp = 0; intTemp <= cmbBillType.Items.Count - 1; intTemp++)
						{
							if (cmbBillType.ItemData(intTemp) == Conversion.Val(strTemp))
							{
								cmbBillType.SelectedIndex = intTemp;
								break;
							}
							else
							{
								// If cmbBillType.ListCount > 0 Then
								// cmbBillType.ListIndex = 0
								// End If
							}
						}
					}
					intErrCode = 26;
					cmbBillType.Focus();
				}
				// hide/show the menu options
				btnProcess.Visible = true;
				intErrCode = 27;
				intErrCode = 28;
				// set the cursor back to the default
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Setup Error - " + FCConvert.ToString(intErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmRateRecChoice_Resize(object sender, System.EventArgs e)
		{
			if (!boolFormatting)
			{
				if (!boolLoaded)
				{
					FormatGrid();
				}
				// SetAct intAction        'this will move everything to the correct place after the resize
				// set the height of the grid after resizing
				if (fraPrint.Visible)
				{
					//fraPrintType.Left = FCConvert.ToInt32((this.Width - fraPrintType.Width) / 2.0);
					//fraPrintType.Top = FCConvert.ToInt32((this.Height - fraPrintType.Height) / 3.0);
				}
				if (vsRate.Rows > 1)
				{
					//if ((vsRate.Rows * vsRate.RowHeight(0)) + 70 > (fraRate.Height * 0.9))
					//{
					//    vsRate.Height = FCConvert.ToInt32(fraRate.Height * 0.9);
					//    vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
					//}
					//else
					//{
					//    vsRate.Height = ((vsRate.Rows - 1) * (vsRate as DataGridView).Rows[0].Height) + vsRate.ColumnHeadersHeight + 2;
					//    vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
					//}
					if (vsRate.Rows == 2 || (intRateType == 6 && vsRate.Rows == 3))
					{
						// this only has one rate record, so select it automatically
						vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
					}
				}
				if (fraRateInfo.Visible == true)
				{
					fraRateInfo.Left = FCConvert.ToInt32((this.Width - fraRateInfo.Width) / 2.0);
					fraRateInfo.Top = FCConvert.ToInt32((this.Height - fraRateInfo.Height) / 2.0);
				}
			}
		}

		private void LoadGrid()
		{
			// this will take a recordset with records and fill the grid with it
			vsRate.Rows = 1;
			vsRate.Cols = 7;
			while (!rsRateRec.EndOfFile())
			{
				vsRate.AddItem("");
				vsRate.TextMatrix(vsRate.Rows - 1, lngBillingDateCol, Strings.Format(rsRateRec.Get_Fields_DateTime("Start"), "MM/dd/yyyy"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngDueDate1Col, Strings.Format(rsRateRec.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngDescriptionCol, FCConvert.ToString(rsRateRec.Get_Fields_String("Description")));
				vsRate.TextMatrix(vsRate.Rows - 1, lngHiddenCol, FCConvert.ToString(rsRateRec.Get_Fields_Int32("ID")));
				vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Rows - 1, 0, vsRate.Rows - 1, vsRate.Cols - 1, Color.White);
				if (Convert.ToDateTime(rsRateRec.Get_Fields_DateTime("BillDate")).ToOADate() != 0)
				{
					vsRate.TextMatrix(vsRate.Rows - 1, lngBillDateCol, FCConvert.ToString(rsRateRec.Get_Fields_DateTime("BillDate")));
				}
				else
				{
					vsRate.TextMatrix(vsRate.Rows - 1, lngBillDateCol, "");
					vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Rows - 1, lngBillDateCol, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
				rsRateRec.MoveNext();
			}
			if (intRateType == 6)
			{
				vsRate.AddItem("\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New");
			}
			// this sets the height
			if (vsRate.Rows > 1)
			{
				//if ((vsRate.Rows * vsRate.RowHeight(0)) + 70 > (fraRate.Height * 0.85))
				//{
				//    vsRate.Height = FCConvert.ToInt32(fraRate.Height * 0.85);
				//    vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//    vsRate.Height = ((vsRate.Rows -1) * (vsRate as DataGridView).Rows[0].Height) + vsRate.ColumnHeadersHeight + 2;
				//    vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				if (vsRate.Rows == 2 || (intRateType == 6 && vsRate.Rows == 3))
				{
					// this only has one rate record, so select it automatically
					vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
				}
			}
		}

		private void FormatGrid()
		{
			// this sub will set the format of the grid
			int wid = 0;
			vsRate.Cols = 7;
			vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			// set the widths
			wid = vsRate.WidthOriginal;
			vsRate.ColWidth(0, FCConvert.ToInt32(wid * 0.08));
			vsRate.ColWidth(lngBillDateCol, FCConvert.ToInt32(wid * 0.18));
			// Commitment Date
			vsRate.ColWidth(lngBillingDateCol, FCConvert.ToInt32(wid * 0.18));
			// BillingDate
			vsRate.ColWidth(lngDueDate1Col, FCConvert.ToInt32(wid * 0.18));
			// DueDate1
			vsRate.ColWidth(lngStatusCol, FCConvert.ToInt32(wid * 0.0));
			// Status
			vsRate.ColWidth(lngDescriptionCol, FCConvert.ToInt32(wid * 0.365));
			// Description
			vsRate.ColWidth(lngHiddenCol, 0);
			// set the alignment values
            vsRate.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngBillDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngBillingDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngDueDate1Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngStatusCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngDescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set the formatting
			vsRate.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsRate.ColFormat(lngBillDateCol, "MM/dd/yyyy");
			vsRate.ColFormat(lngBillingDateCol, "MM/dd/yyyy");
			vsRate.ColFormat(lngDueDate1Col, "MM/dd/yyyy");
			vsRate.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 5, true);
			//vsRate.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// set the header titles
			vsRate.TextMatrix(0, 0, "Select");
			vsRate.TextMatrix(0, lngBillDateCol, "Billing Date");
			vsRate.TextMatrix(0, lngBillingDateCol, "PerStart Date");
			vsRate.TextMatrix(0, lngDueDate1Col, "Int Start Date");
			vsRate.TextMatrix(0, lngStatusCol, "Status");
			vsRate.TextMatrix(0, lngDescriptionCol, "Description");
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL = "";
				bool boolLienRR;
				if (!fraPrint.Visible)
				{
					if (!ValidateAnswers())
					{
						return;
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (fraPrint.Visible == false)
				{
					// this will check to see if the print type is being determined
					// if it is not then process normally
					if (vsRate.Enabled && vsRate.Visible)
					{
						vsRate.Select(0, 0);
					}
					//Application.DoEvents();
					// CreateBillMasterYearQuery
					// If intAction = 0 Then
					// SetAct 2
					// Else
					// this will load the next screen depending on how the user got here
					switch (intRateType)
					{
						case 0:
							{
								// this is nothing
								MessageBox.Show("The Rate Type was not setup.", "Rate Rec Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								// do nothing else...if this happens then there is a bug
								// Case 30         'Reminder Notices
								// If ValidateAnswers Then
								// Me.Hide
								// lngRateRecNumber = GetRateKey(boolLienRR)
								// strSQL = BuildLabelSQL(30)
								// 
								// frmWait.Init "Please Wait..." & vbCrLf & "Loading Reminder Information"
								// frmReminderNotices.Init strRateKeyList, cmbBillType.ItemData(cmbBillType.ListIndex)
								// Unload frmWait
								// End If
								// Case 100        'load back
								// If ValidateAnswers Then
								// Me.Hide
								// lngRateRecNumber = GetRateKey(boolLienRR)
								// frmLoadBack.Init lngRateRecNumber, Left(cmbBillType.List(cmbBillType.ListIndex), 4), boolLienRR
								// End If
								// 
								break;
							}
						case 200:
							{
								if (ValidateAnswers())
								{
									//this.Hide();
									lngRateRecNumber = GetRateKey();
									frmAddRateRecord.InstancePtr.Init(200, lngPassRateKey: lngRateRecNumber);
                                    // this will allow the user to edit the rate key
                                    //FC:FINAL:AM:#4428 - close the form
                                    this.Close();
                                }
								break;
							}
						case 201:
							{
								if (ValidateAnswers())
								{
									//this.Hide();
									lngRateRecNumber = GetRateKey();
									rptBillTransferReport.InstancePtr.Init(lngRateRecNumber, DateTime.Today);
                                    //FC:FINAL:AM:#4428 - close the form
                                    this.Close();
                                }
								break;
							}
						default:
							{
								// this is nothing
								MessageBox.Show("The Rate Type was not setup.", "Rate Rec Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								// do nothing else...if this happens then there is a bug
								break;
							}
					}
					//end switch
					// End If
				}
				else
				{
					if (cmbPrint.SelectedIndex == 0)
					{
						PrintTypeSetup_2(0);
					}
					else
					{
						PrintTypeSetup_2(1);
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void optLien_Click(ref short Index)
		{
			// update the rate grid
			ChangeRateKeys();
		}

		private void optPrint_KeyDown(int Index, object sender, System.EventArgs e)
		{
			//FC:FINAL:DDU:TODO
			//Keys KeyCode = e.KeyCode;
			//int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			//switch (KeyCode)
			//{
			//    case Keys.Return:
			//        {
			//            if (cmbPrint.Visible)
			//            {
			//                KeyCode = (Keys)0;
			//                PrintTypeSetup(ref Index);
			//            }
			//            break;
			//        }
			//} //end switch
		}

		private void optPrint_KeyDown(object sender, System.EventArgs e)
		{
			int index = cmbPrint.SelectedIndex;
			optPrint_KeyDown(index, sender, e);
		}

		private void PrintTypeSetup_2(int intIndex)
		{
			PrintTypeSetup(ref intIndex);
		}

		private void PrintTypeSetup(ref int intIndex)
		{
			if (!boolFormatting)
			{
				fraPrint.Visible = false;
				if (intIndex == 1)
				{
					// this will set the rate type to forms rather than labels
					intRateType += 10;
				}
				boolFormatting = true;
				SetAct(0);
				boolFormatting = false;
			}
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// All Accounts
						txtRange[0].Enabled = false;
						txtRange[1].Enabled = false;
						break;
					}
				case 1:
					{
						// Range by Name
						txtRange[0].Enabled = true;
						txtRange[1].Enabled = true;
						txtRange[0].MaxLength = 20;
						txtRange[1].MaxLength = 20;
						if (txtRange[0].Enabled && txtRange[0].Visible)
						{
							txtRange[0].Focus();
						}
						break;
					}
				case 2:
					{
						// Range by Account Number
						txtRange[0].Enabled = true;
						txtRange[1].Enabled = true;
						txtRange[0].MaxLength = 10;
						txtRange[1].MaxLength = 10;
						if (txtRange[0].Enabled && txtRange[0].Visible)
						{
							txtRange[0].Focus();
						}
						break;
					}
			}
			//end switch
			txtRange[0].Text = "";
			txtRange[1].Text = "";
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void optReminderLien_Click(ref short Index)
		{
			if (intRateType == 30)
			{
				ChangeRateKeys();
			}
		}

		private void txtMailDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// set the default mail date from the last time this was entered
			string txtMailDateTemp = txtMailDate.Text;
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "LastLienEditReportDate", txtMailDateTemp);
			txtMailDate.Text = txtMailDateTemp;
		}

		private void txtMinimumAmount_Enter(object sender, System.EventArgs e)
		{
			txtMinimumAmount.SelectionStart = 0;
			txtMinimumAmount.SelectionLength = txtMinimumAmount.Text.Length;
		}

		private void txtMinimumAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
			{
			}
			else if (KeyAscii == Keys.Delete)
			{
				if (Strings.InStr(1, txtMinimumAmount.Text, ".", CompareConstants.vbBinaryCompare) > 0)
				{
					KeyAscii = (Keys)0;
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMinimumAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMinimumAmount.Text) > 0)
			{
				txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text), "#,##0.00");
			}
			else
			{
				txtMinimumAmount.Text = "0.00";
			}
		}

		private void txtRange_KeyPress(int Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			bool boolText;
			boolText = cmbRange.SelectedIndex == 0;
			if (boolText)
			{
				// this will allow all characters
				switch (KeyAscii)
				{
					case Keys.Right:
						{
							// stop single quotes
							KeyAscii = (Keys)0;
							break;
						}
				}
				//end switch
			}
			else
			{
				// this will only allow numbers
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtRange_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int index = txtRange.IndexOf((FCTextBox)sender);
			txtRange_KeyPress(index, sender, e);
		}

		private void vsbMinAmount_ValueChanged(object sender, System.EventArgs e)
		{
			// this allows the scroll bar to adjust the value in the text box
			if (vsbMinAmount.Value == 0)
			{
				if (FCConvert.ToDouble(txtMinimumAmount.Text) <= 0)
				{
					txtMinimumAmount.Text = "0.00";
				}
				else
				{
					txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text) - 0.1, "#,##0.00");
					vsbMinAmount.Value = 1;
				}
			}
			else if (vsbMinAmount.Value == 2)
			{
				txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text) + 0.1, "#,##0.00");
				vsbMinAmount.Value = 1;
			}
		}

		private void vsRate_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			int lngRW;
			int intTemp = 0;
			if (vsRate.Col == 0)
			{
				switch (intRateType)
				{
					case 100:
					case 200:
					case 201:
						{
							if (vsRate.Row > 0 && vsRate.Row <= vsRate.Rows - 1)
							{
								for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
								{
									// this does not have the extra add row feature
									// this will clear any other selection in the grid so the user can only choose one
									if (lngRW != vsRate.Row)
									{
										vsRate.TextMatrix(lngRW, 0, FCConvert.ToString(0));
										vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0, "");
									}
									else
									{
										// and it will show the picture if chosen
										// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0) = MDIParent.ImageList1.ListImages(5).Picture
									}
								}
							}
							break;
						}
					case 6:
					case 12:
					case 22:
						{
							// 7, 12, 22 'MAL@20071001: Change to allow multiple rate key selection for 7
							if ((vsRate.Row > 0 && vsRate.Row < vsRate.Rows - 1) || intRateType == 7 || intRateType == 22)
							{
								if (intRateType == 7 || intRateType == 22)
								{
									intTemp = 1;
								}
								else
								{
									intTemp = 2;
								}
								for (lngRW = 1; lngRW <= vsRate.Rows - intTemp; lngRW++)
								{
									// this will clear any other selection in the grid so the user can only choose one
									if (lngRW != vsRate.Row)
									{
										vsRate.TextMatrix(lngRW, 0, FCConvert.ToString(0));
										vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0, "");
									}
									else
									{
										// and it will show the picture if chosen
										// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0) = MDIParent.ImageList1.ListImages(5).Picture
									}
								}
							}
							break;
						}
					default:
						{
							if (vsRate.Row > 0)
							{
								if (FCConvert.ToDouble(vsRate.TextMatrix(vsRate.GetFlexRowIndex(e.RowIndex), 0)) == -1)
								{
									// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, Row, 0) = MDIParent.ImageList1.ListImages(5).Picture
									vsRate.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, vsRate.Row, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
								}
								else
								{
									vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, vsRate.Row, 0, "");
								}
							}
							break;
						}
				}
				//end switch
			}
		}

		private void vsRate_DblClick(object sender, System.EventArgs e)
		{
			int lngCT;
			int lngMR = 0;
			int lngMC = 0;
			if ((intRateType == 1) || (intRateType == 2) || (intRateType == 3) || (intRateType == 5) || (intRateType >= 10 && intRateType <= 100))
			{
				lngMR = vsRate.MouseRow;
				lngMC = vsRate.MouseCol;
				if (lngMR == 0 && lngMC == 0)
				{
					// this will select all
					if (vsRate.Rows > 1)
					{
						for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
						{
							vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
						}
					}
				}
			}
		}

		private void vsRate_Enter(object sender, System.EventArgs e)
		{
			if (vsRate.Rows > 1)
			{
				vsRate.Select(1, 0);
			}
			// If vsRate.Row = vsRate.rows - 1 And vsRate.Col = vsRate.Cols - 1 Then
			// vsRate.TabBehavior = flexTabControls
			// Else
			vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			// End If
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void vsRate_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mouseRow = vsRate.GetFlexRowIndex(e.RowIndex);
			int mouseCol = vsRate.GetFlexColIndex(e.ColumnIndex);
			if (e.Button == MouseButtons.Right && mouseRow > 0)
			{
				ShowRateInfo_18(mouseRow, mouseCol, FCConvert.ToInt32(Conversion.Val(vsRate.TextMatrix(mouseRow, lngHiddenCol))));
			}
		}

		private void vsRate_RowColChange(object sender, System.EventArgs e)
		{
			if (vsRate.Col == 0)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				if (vsRate.Row > 0)
				{
					//FC:FINAL:SBE - #i802 - no need for this hack from the original application
					//vsRate.EditCell();
					//if (FCConvert.ToDouble(vsRate.EditText) == 1)
					//{ // vsRate.TextMatrix(vsRate.Row, vsRate.Col) = 0 Then
					//    vsRate.EditText = FCConvert.ToString(-1); // vsRate.TextMatrix(vsRate.Row, vsRate.Col)
					//    vsRate.TextMatrix(vsRate.Row, vsRate.Col, FCConvert.ToString(-1));
					//}
					//else
					//{
					//    vsRate.EditText = FCConvert.ToString(0);
					//    vsRate.TextMatrix(vsRate.Row, vsRate.Col, FCConvert.ToString(0));
					//}
				}
			}
			else if (vsRate.Col == lngBillDateCol)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			else if (vsRate.Col == vsRate.Cols - 1)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
				if (vsRate.Row == vsRate.Rows - 1)
				{
					vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsRate.Col == 5 && vsRate.Row == vsRate.Rows - 1)
			{
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool FillYearCombo()
		{
			bool FillYearCombo = false;
			// this will fill the Year combobox with all of the years that have bills associated with them
			clsDRWrapper rsBillType = new clsDRWrapper();
			string strSQL = "";
			bool boolUseFullYear;
			int intTemp;
			string strTemp = "";
			cmbBillType.Clear();
			switch (intRateType)
			{
				case 0:
					{
						// load nothing
						strSQL = "";
						break;
					}
				case 30:
					{
						// Reminder Notices
						strSQL = "SELECT DISTINCT BillType FROM Bill";
						break;
					}
				case 100:
					{
						strSQL = "SELECT DISTINCT BillType FROM RateKeys";
						break;
					}
				case 200:
				case 201:
					{
						strSQL = "SELECT DISTINCT BillType FROM RateKeys";
						// "SELECT DISTINCT BillingYear AS Year FROM BillingMaster ORDER BY BillingYear"
						break;
					}
				default:
					{
						// load nothing
						strSQL = "";
						break;
					}
			}
			//end switch
			if (strSQL != "")
			{
				rsBillType.OpenRecordset("SELECT * FROM (" + strSQL + ") as temp INNER JOIN DefaultBillTypes ON temp.BillType = DefaultBillTypes.TypeCode ORDER BY BillType", modCLCalculations.strARDatabase);
				while (!rsBillType.EndOfFile())
				{
					cmbBillType.AddItem(Strings.Format(rsBillType.Get_Fields_Int32("TypeCode"), "000") + "  " + rsBillType.Get_Fields_String("TypeTitle"));
					cmbBillType.ItemData(cmbBillType.NewIndex, FCConvert.ToInt32(rsBillType.Get_Fields_Int32("TypeCode")));
					rsBillType.MoveNext();
				}
			}
			if (cmbBillType.Items.Count > 0)
			{
				// set the default year from the last time this was entered
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "AR\\", "LastRateRecChoiceType", ref strTemp);
				if (cmbBillType.Items.Count > 0)
				{
					// set the default year if it is in the list
					for (intTemp = 0; intTemp <= cmbBillType.Items.Count - 1; intTemp++)
					{
						if (cmbBillType.ItemData(intTemp) == Conversion.Val(strTemp))
						{
							cmbBillType.SelectedIndex = intTemp;
							break;
						}
						else
						{
							// cmbBillType.ListIndex = 0
						}
					}
				}
				FillYearCombo = true;
			}
			else
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				FillYearCombo = false;
				if (intRateType == 100)
				{
					// frmAuditInfo.Init 0
					intRateType = -1;
					// this will force this form to unload
				}
				else if (intRateType == -1)
				{
					// do nothing
				}
				else
				{
					if (intRateType != 0)
					{
						if (intRateType != 61)
						{
							MessageBox.Show("There are no accounts eligible.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
						}
					}
					else
					{
						// load the combo again
						intRateType = 100;
						FillYearCombo = FillYearCombo;
						return FillYearCombo;
					}
				}
			}
			return FillYearCombo;
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			// this function will check to make sure that all of the answers that are given are valid
			// including the rate tables being checked and the ranges being correct
			int intCT;
			bool boolFound;
			ValidateAnswers = true;
			boolFound = false;
			for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
			{
				if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
				{
					boolFound = true;
					if (vsRate.TextMatrix(intCT, lngBillDateCol) == "")
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please fill the commitment date field in for the selected rate table.", "Rate Table Selection", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						ValidateAnswers = false;
						SetAct(0);
						//Application.DoEvents();
						if (vsRate.Visible && vsRate.Enabled)
						{
							vsRate.Focus();
							vsRate.Select(intCT, lngBillDateCol);
							vsRate.EditCell();
						}
						return ValidateAnswers;
					}
				}
			}
			if (!boolFound)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Please select a rate table.", "Rate Table Selection", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				ValidateAnswers = false;
				SetAct(0);
				vsRate.Focus();
				return ValidateAnswers;
			}
			if (cmbRange.SelectedIndex == 0)
			{
				if (fecherFoundation.Strings.CompareString(txtRange[0].Text, txtRange[1].Text, true) > 0)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a value in the second section of the range that is greater than the first.", "Range Criteria", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ValidateAnswers = false;
					SetAct(2);
					txtRange[1].Focus();
					return ValidateAnswers;
				}
			}
			if (cmbRange.SelectedIndex == 1)
			{
				if (Conversion.Val(txtRange[0].Text) > Conversion.Val(txtRange[1].Text))
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a value in the second section of the range that is greater than the first.", "Range Criteria", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ValidateAnswers = false;
					SetAct(2);
					txtRange[1].Focus();
					return ValidateAnswers;
				}
			}
			if (intRateType == 1)
			{
				if (txtMailDate.Text != "")
				{
					if (!Information.IsDate(txtMailDate.Text))
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter a valid date in the mail date field.", "Mail Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						ValidateAnswers = false;
						SetAct(2);
						txtMailDate.Focus();
						return ValidateAnswers;
					}
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a value in the mail date field.", "Mail Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ValidateAnswers = false;
					SetAct(2);
					txtMailDate.Focus();
					return ValidateAnswers;
				}
				if (cmbReportDetail.SelectedIndex < 0)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a value in the report detail field.", "Report Detail", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ValidateAnswers = false;
					SetAct(2);
					cmbReportDetail.Focus();
					return ValidateAnswers;
				}
			}
			return ValidateAnswers;
		}

		private void vsRate_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// If intRateType = 6 And Row = vsRate.rows - 1 Then
			// skip the validation of this row
			// Else
			// If Col = lngBillDateCol Then      'is it the right column
			// If IsDate(vsRate.EditText) Then     'is this a valid date
			// save it in the database
			// rsRateRec.FindFirstRecord , , "ID = " & vsRate.TextMatrix(Row, lngHiddenCol)
			// If rsRateRec.NoMatch Then
			// MsgBox "Error finding rate record.  Please reload the rate records.", vbInformation, "Rate Record Error"
			// SetAct 0
			// Else
			// rsRateRec.Edit
			// rsRateRec.Get_Fields("BillDate") = Format(vsRate.EditText, "MM/dd/yyyy")
			// rsRateRec.Update
			// vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, Col) = vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, Col - 1)
			// End If
			// Else
			// MsgBox "Please enter a valid date in the bill field. (MM/dd/yyyy)", vbInformation, "Commitment Date"
			// If Val(vsRate.TextMatrix(Row, 0)) = -1 Then
			// Cancel = True
			// End If
			// End If
			// End If
			// End If
		}

		private void ChangeRateKeys()
		{
			// show the Rate Keys
			rsRateRec.OpenRecordset(BuildSQL(), modCLCalculations.strARDatabase);
			if (rsRateRec.EndOfFile() != true)
			{
				// show the rate keys and size the grid
				if (intAction != 2)
				{
					LoadGrid();
				}
			}
			else
			{
				// show nothing
				if (intBillType == 0)
				{
					MessageBox.Show("Please select a bill type.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					if (intRateType == 6)
					{
						vsRate.Rows = 1;
						vsRate.AddItem("\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New");
						// this sets the height
						if (vsRate.Rows > 1)
						{
							if ((vsRate.Rows * vsRate.RowHeight(0)) + 70 > (fraRate.Height * 0.9))
							{
								vsRate.Height = FCConvert.ToInt32(fraRate.Height * 0.9);
								vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
							}
							else
							{
								vsRate.Height = ((vsRate.Rows - 1) * (vsRate as DataGridView).Rows[0].Height) + vsRate.ColumnHeadersHeight + 2;
								vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
							}
							if (vsRate.Rows == 2 || (intRateType == 6 && vsRate.Rows == 3))
							{
								// this only has one rate record, so select it automatically
								boolFormatting = true;
								// this will make sure that it does not automatically go to the rate screen
								vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
								boolFormatting = false;
							}
						}
					}
					else
					{
						if (intRateType != 30)
						{
							if (!boolLoading)
							{
								MessageBox.Show("There are no rate records for the type selected.", "No Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							if (!boolReminderInitial)
							{
								MessageBox.Show("There are no rate records for the type selected.", "No Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
					}
					boolReminderInitial = false;
				}
			}
		}

		private void FillReportDetailCombo()
		{
			// this will fill the combo box with the different options for the detail level of the report
			cmbReportDetail.Clear();
			cmbReportDetail.AddItem("Account Information Only");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 0);
			cmbReportDetail.AddItem("Show Mortgage Holders");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 1);
			cmbReportDetail.AddItem("Show Mortgage Holders and Address");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 2);
			cmbReportDetail.SelectedIndex = 0;
		}

		private int GetRateKey(bool boolType = false)
		{
			int GetRateKey = 0;
			// this will check the grid and return whatever rate key is selected
			int lngRW;
			for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
			{
				if (Conversion.Val(vsRate.TextMatrix(lngRW, 0)) == -1)
				{
					GetRateKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsRate.TextMatrix(lngRW, lngHiddenCol))));
					break;
				}
			}
			return GetRateKey;
		}

		private void SetToolTips()
		{
			// this procedure will set all of the tool tips
			// for each of the controls on the form
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWriteFCConvert.ToDouble(
		private void ShowRateInfo_18(int cRow, int cCol, int lngRateKey)
		{
			ShowRateInfo(ref cRow, ref cCol, ref lngRateKey);
		}

		private void ShowRateInfo(ref int cRow, ref int cCol, ref int lngRateKey)
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsRI = new clsDRWrapper();
				clsDRWrapper rsLR = new clsDRWrapper();
				int intBillType = 0;
				int I;
				int RK;
				int LRN;
				lngErrCode = 1;
				RK = lngRateKey;
				lngErrCode = 2;
				rsRI.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(RK), modCLCalculations.strARDatabase);
				// format grid
				vsRateInfo.Cols = 2;
				vsRateInfo.Rows = 9;
				vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				fraRateInfo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				lngErrCode = 3;
				intBillType = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbBillType.Items[cmbBillType.SelectedIndex].ToString(), 4))));
				lngErrCode = 4;
				vsRateInfo.ColWidth(0, FCConvert.ToInt32(vsRateInfo.WidthOriginal * 0.45));
				vsRateInfo.ColWidth(1, FCConvert.ToInt32(vsRateInfo.WidthOriginal * 0.5));
				vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsRateInfo.ExtendLastCol = true;
				vsRateInfo.Height = (vsRateInfo as DataGridView).Rows[0].Height * vsRateInfo.Rows + 2;
				vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				lngErrCode = 5;
				if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
				{
					// show the first set of information (rate information and period info)--------------
					vsRateInfo.TextMatrix(0, 0, "Rate Record");
					vsRateInfo.TextMatrix(1, 0, "Bill Description");
					vsRateInfo.TextMatrix(2, 0, "");
					vsRateInfo.TextMatrix(3, 0, "Billing Date");
					vsRateInfo.TextMatrix(4, 0, "Creation Date");
					vsRateInfo.TextMatrix(5, 0, "Period Start");
					vsRateInfo.TextMatrix(6, 0, "Period End");
					vsRateInfo.TextMatrix(7, 0, "Interest Date");
					string vbPorterVar = rsRI.Get_Fields_String("InterestMethod");
					if (vbPorterVar == "AP")
					{
						vsRateInfo.TextMatrix(8, 0, "Interest Rate");
						vsRateInfo.TextMatrix(8, 1, Strings.Format(rsRI.Get_Fields_Double("IntRate"), "#0.00%"));
					}
					else if (vbPorterVar == "AF")
					{
						vsRateInfo.TextMatrix(8, 0, "Late Fee");
						vsRateInfo.TextMatrix(8, 1, Strings.Format(rsRI.Get_Fields_Decimal("FlatRate"), "#,##0.00"));
					}
					else
					{
						vsRateInfo.TextMatrix(8, 0, "");
						vsRateInfo.TextMatrix(8, 1, "");
					}
					lngErrCode = 7;
					vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("ID")));
					lngErrCode = 8;
					vsRateInfo.TextMatrix(1, 1, modARStatusPayments.GetBillDescription(intBillType));
					lngErrCode = 9;
					vsRateInfo.TextMatrix(2, 1, "");
					lngErrCode = 10;
					vsRateInfo.TextMatrix(3, 1, Strings.Format(rsRI.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy"));
					lngErrCode = 11;
					vsRateInfo.TextMatrix(4, 1, Strings.Format(rsRI.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy"));
					lngErrCode = 12;
					vsRateInfo.TextMatrix(5, 1, Strings.Format(rsRI.Get_Fields_DateTime("Start"), "MM/dd/yyyy"));
					lngErrCode = 13;
					// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
					vsRateInfo.TextMatrix(6, 1, Strings.Format(rsRI.Get_Fields("End"), "MM/dd/yyyy"));
					lngErrCode = 14;
					vsRateInfo.TextMatrix(7, 1, Strings.Format(rsRI.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy"));
					lngErrCode = 15;
				}
				else
				{
					vsRateInfo.TextMatrix(0, 1, "");
					vsRateInfo.TextMatrix(1, 1, "");
					vsRateInfo.TextMatrix(2, 1, "");
					vsRateInfo.TextMatrix(3, 1, "");
					vsRateInfo.TextMatrix(4, 1, "");
					vsRateInfo.TextMatrix(5, 1, "");
					vsRateInfo.TextMatrix(6, 1, "");
					vsRateInfo.TextMatrix(7, 1, "");
					vsRateInfo.TextMatrix(8, 1, "");
				}
				// show the frame-----------------------------------------------------
				fraRateInfo.Text = "Rate Information - " + FCConvert.ToString(intBillType);
				fraRateInfo.Left = FCConvert.ToInt32((this.Width - fraRateInfo.Width) / 2.0);
				fraRateInfo.Top = FCConvert.ToInt32((this.Height - fraRateInfo.Height) / 2.0);
				fraRateInfo.Visible = true;
				if (cmdRIClose.Visible)
				{
					cmdRIClose.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public string RateKeyList()
		{
			string RateKeyList = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the string of rate keys
				string strTemp = "";
				int lngCT;
				for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(lngCT, 0)) == -1)
					{
						if (strTemp == "")
						{
							strTemp = "(" + vsRate.TextMatrix(lngCT, lngHiddenCol);
						}
						else
						{
							strTemp += "," + vsRate.TextMatrix(lngCT, lngHiddenCol);
						}
					}
				}
				if (strTemp != "")
				{
					strTemp += ")";
				}
				RateKeyList = strTemp;
				return RateKeyList;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating RK List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return RateKeyList;
		}
	}
}
