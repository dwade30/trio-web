﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillList.
	/// </summary>
	public partial class frmBillList : BaseForm
	{
		public frmBillList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            //FC:FINAL:BSE: #2131 default value should be 1
			this.cmbStatusSelected.SelectedIndex = 1;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillList InstancePtr
		{
			get
			{
				return (frmBillList)Sys.GetInstance(typeof(frmBillList));
			}
		}

		protected frmBillList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// vbPorter upgrade warning: datReturnedDate As DateTime	OnWriteFCConvert.ToInt16(
		DateTime datReturnedDate;

		private void frmBillList_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBillList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBillList.FillStyle	= 0;
			//frmBillList.ScaleWidth	= 9045;
			//frmBillList.ScaleHeight	= 7515;
			//frmBillList.LinkTopic	= "Form2";
			//frmBillList.LockControls	= -1  'True;
			//frmBillList.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			txtStartDate.Text = "01/01/1900";
			txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			FillCustomersCombo();
			FillTypeCombo();
			cboCustomers.SelectedIndex = 0;
			cboBillType.SelectedIndex = 0;
		}

		private void frmBillList_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			if (cmbStatusSelected.SelectedIndex == 0)
			{
				if (chkActive.CheckState != Wisej.Web.CheckState.Checked && chkVoid.CheckState != Wisej.Web.CheckState.Checked && chkPaid.CheckState != Wisej.Web.CheckState.Checked)
				{
					MessageBox.Show("You must select at least one bill status to show before you may proceed.", "Invalid Bill Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date for the start date to report on.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date for the end date to report on.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtEndDate.Focus();
				return;
			}
			else if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your start date must be earlier then your end date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			strSQL = "SELECT * FROM Bill WHERE ";
			// add status SQL piece
			if (cmbStatusSelected.SelectedIndex == 1)
			{
				// do nothing
			}
			else
			{
				strSQL += "(";
				if (chkActive.CheckState == Wisej.Web.CheckState.Checked)
				{
					strSQL += "BillStatus = 'A' OR ";
				}
				if (chkVoid.CheckState == Wisej.Web.CheckState.Checked)
				{
					strSQL += "BillStatus = 'V' OR ";
				}
				if (chkPaid.CheckState == Wisej.Web.CheckState.Checked)
				{
					strSQL += "BillStatus = 'P' OR ";
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ") AND ";
			}
			// add date section of SQL
			strSQL += "(BillDate >= '" + txtStartDate.Text + "' AND BillDate <= '" + txtEndDate.Text + "') AND ";
			if (cboCustomers.SelectedIndex == 0)
			{
				// do nothing they want all customers
			}
			else
			{
				strSQL += "BName = '" + cboCustomers.Text + "' AND ";
			}
			if (cboBillType.SelectedIndex == 0)
			{
				// do nothing they want all bill types
			}
			else
			{
				strSQL += "BillType = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboBillType.Text, 3))) + " AND ";
			}
			strSQL = Strings.Left(strSQL, strSQL.Length - 5) + " ORDER BY BName";
			this.Hide();
			if (chkShowDetail.CheckState == Wisej.Web.CheckState.Checked)
			{
				rptBillList.InstancePtr.Init(ref strSQL);
			}
			else
			{
				rptBillListSummary.InstancePtr.Init(ref strSQL);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdRequestDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtStartDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEndDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void FillCustomersCombo()
		{
			clsDRWrapper rsCust = new clsDRWrapper();
			cboCustomers.Clear();
			cboCustomers.AddItem("All Customers");
			rsCust.OpenRecordset("SELECT DISTINCT BName FROM Bill ORDER BY BName");
			if (rsCust.EndOfFile() != true && rsCust.BeginningOfFile() != true)
			{
				do
				{
					cboCustomers.AddItem(rsCust.Get_Fields_String("BName"));
					rsCust.MoveNext();
				}
				while (rsCust.EndOfFile() != true);
			}
		}

		private void FillTypeCombo()
		{
			clsDRWrapper rsType = new clsDRWrapper();
			cboBillType.Clear();
			cboBillType.AddItem("All Bill Types");
			rsType.OpenRecordset("SELECT * FROM DefaultBillTypes ORDER BY TypeCode");
			if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
			{
				do
				{
					cboBillType.AddItem(Strings.Format(rsType.Get_Fields_Int32("TypeCode"), "00") + " - " + rsType.Get_Fields_String("TypeTitle"));
					rsType.MoveNext();
				}
				while (rsType.EndOfFile() != true);
			}
		}

		private void cmdPrintPreview_Click(object sender, EventArgs e)
		{
			mnuFilePreview_Click(sender, e);
		}

		private void cmbStatusSelected_SelectedIndexChanged(object sender, EventArgs e)
		{
		}
	}
}
