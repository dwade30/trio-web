﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmAREditBillInfo.
	/// </summary>
	partial class frmAREditBillInfo : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRE;
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCLabel lblSearchType;
		public fecherFoundation.FCFrame fraEditInfo;
		public FCGrid vsEditInfo;
		public FCGrid vsYearInfo;
		public fecherFoundation.FCGrid vsSearch;
		public fecherFoundation.FCPanel fraSearch;
		public fecherFoundation.FCFrame fraGetAccount;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCLabel lblInstructions1;
		public fecherFoundation.FCFrame fraSearchCriteria;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCTextBox txtHold;
		public fecherFoundation.FCLabel lblSearchListInstruction;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAREditBillInfo));
			this.cmbRE = new fecherFoundation.FCComboBox();
			this.cmbSearchType = new fecherFoundation.FCComboBox();
			this.lblSearchType = new fecherFoundation.FCLabel();
			this.fraEditInfo = new fecherFoundation.FCFrame();
			this.vsEditInfo = new fecherFoundation.FCGrid();
			this.vsYearInfo = new fecherFoundation.FCGrid();
			this.vsSearch = new fecherFoundation.FCGrid();
			this.fraSearch = new fecherFoundation.FCPanel();
			this.fraGetAccount = new fecherFoundation.FCFrame();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.lblInstructions1 = new fecherFoundation.FCLabel();
			this.fraSearchCriteria = new fecherFoundation.FCFrame();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.cmdClear = new fecherFoundation.FCButton();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.txtHold = new fecherFoundation.FCTextBox();
			this.lblSearchListInstruction = new fecherFoundation.FCLabel();
			this.btnProcessGetAccount = new fecherFoundation.FCButton();
			this.btnFileSave = new fecherFoundation.FCButton();
			this.btnProcessSearch = new fecherFoundation.FCButton();
			this.btnProcessClearSearch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEditInfo)).BeginInit();
			this.fraEditInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsEditInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsYearInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
			this.fraSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGetAccount)).BeginInit();
			this.fraGetAccount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearchCriteria)).BeginInit();
			this.fraSearchCriteria.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessGetAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessClearSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcessGetAccount);
			this.BottomPanel.Location = new System.Drawing.Point(0, 744);
			this.BottomPanel.Size = new System.Drawing.Size(1065, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraEditInfo);
			this.ClientArea.Controls.Add(this.fraSearchCriteria);
			this.ClientArea.Controls.Add(this.fraSearch);
			this.ClientArea.Controls.Add(this.txtHold);
			this.ClientArea.Controls.Add(this.lblSearchListInstruction);
			this.ClientArea.Size = new System.Drawing.Size(1065, 684);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnProcessClearSearch);
			this.TopPanel.Controls.Add(this.btnProcessSearch);
			this.TopPanel.Controls.Add(this.btnFileSave);
			this.TopPanel.Size = new System.Drawing.Size(1065, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileSave, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessClearSearch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(225, 30);
			this.HeaderText.Text = "Edit Bill Information";
			// 
			// cmbRE
			// 
			this.cmbRE.AutoSize = false;
			this.cmbRE.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRE.FormattingEnabled = true;
			this.cmbRE.Items.AddRange(new object[] {
				"Real Estate",
				"Personal Property"
			});
			this.cmbRE.Location = new System.Drawing.Point(260, 66);
			this.cmbRE.Name = "cmbRE";
			this.cmbRE.Size = new System.Drawing.Size(121, 40);
			this.cmbRE.TabIndex = 15;
			this.cmbRE.Visible = false;
			// 
			// cmbSearchType
			// 
			this.cmbSearchType.AutoSize = false;
			this.cmbSearchType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSearchType.FormattingEnabled = true;
			this.cmbSearchType.Items.AddRange(new object[] {
				"Name",
				"Street Name",
				"Map / Lot"
			});
			this.cmbSearchType.Location = new System.Drawing.Point(388, 23);
			this.cmbSearchType.Name = "cmbSearchType";
			this.cmbSearchType.Size = new System.Drawing.Size(135, 40);
			this.cmbSearchType.TabIndex = 16;
			// 
			// lblSearchType
			// 
			this.lblSearchType.Location = new System.Drawing.Point(20, 44);
			this.lblSearchType.Name = "lblSearchType";
			this.lblSearchType.Size = new System.Drawing.Size(75, 15);
			this.lblSearchType.TabIndex = 17;
			this.lblSearchType.Text = "SEARCH BY";
			this.lblSearchType.Visible = false;
			// 
			// fraEditInfo
			// 
			this.fraEditInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraEditInfo.Controls.Add(this.vsEditInfo);
			this.fraEditInfo.Controls.Add(this.vsYearInfo);
			this.fraEditInfo.Location = new System.Drawing.Point(30, 30);
			this.fraEditInfo.Name = "fraEditInfo";
			this.fraEditInfo.Size = new System.Drawing.Size(1005, 624);
			this.fraEditInfo.TabIndex = 19;
			this.fraEditInfo.Text = "Edit Account Information";
			this.fraEditInfo.Visible = false;
			// 
			// vsEditInfo
			// 
			this.vsEditInfo.AllowSelection = false;
			this.vsEditInfo.AllowUserToResizeColumns = false;
			this.vsEditInfo.AllowUserToResizeRows = false;
			this.vsEditInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsEditInfo.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsEditInfo.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsEditInfo.BackColorBkg = System.Drawing.Color.Empty;
			this.vsEditInfo.BackColorFixed = System.Drawing.Color.Empty;
			this.vsEditInfo.BackColorSel = System.Drawing.Color.Empty;
			this.vsEditInfo.Cols = 2;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsEditInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.vsEditInfo.ColumnHeadersHeight = 30;
			this.vsEditInfo.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsEditInfo.ColumnHeadersVisible = false;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsEditInfo.DefaultCellStyle = dataGridViewCellStyle8;
			this.vsEditInfo.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsEditInfo.FixedRows = 0;
			this.vsEditInfo.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsEditInfo.FrozenCols = 0;
			this.vsEditInfo.GridColor = System.Drawing.Color.Empty;
			this.vsEditInfo.Location = new System.Drawing.Point(20, 282);
			this.vsEditInfo.Name = "vsEditInfo";
			this.vsEditInfo.ReadOnly = true;
			this.vsEditInfo.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsEditInfo.RowHeightMin = 0;
			this.vsEditInfo.Rows = 10;
			this.vsEditInfo.ShowColumnVisibilityMenu = false;
			this.vsEditInfo.Size = new System.Drawing.Size(965, 322);
			this.vsEditInfo.StandardTab = true;
			this.vsEditInfo.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsEditInfo.TabIndex = 21;
			this.vsEditInfo.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsEditInfo_KeyDownEdit);
			this.vsEditInfo.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsEditInfo_ChangeEdit);
			this.vsEditInfo.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsEditInfo_AfterEdit);
			this.vsEditInfo.CurrentCellChanged += new System.EventHandler(this.vsEditInfo_RowColChange);
			this.vsEditInfo.Enter += new System.EventHandler(this.vsEditInfo_Enter);
			// 
			// vsYearInfo
			// 
			this.vsYearInfo.AllowSelection = false;
			this.vsYearInfo.AllowUserToResizeColumns = false;
			this.vsYearInfo.AllowUserToResizeRows = false;
			this.vsYearInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsYearInfo.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsYearInfo.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsYearInfo.BackColorBkg = System.Drawing.Color.Empty;
			this.vsYearInfo.BackColorFixed = System.Drawing.Color.Empty;
			this.vsYearInfo.BackColorSel = System.Drawing.Color.Empty;
			this.vsYearInfo.Cols = 4;
			dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsYearInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
			this.vsYearInfo.ColumnHeadersHeight = 30;
			this.vsYearInfo.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsYearInfo.DefaultCellStyle = dataGridViewCellStyle10;
			this.vsYearInfo.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsYearInfo.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsYearInfo.FrozenCols = 0;
			this.vsYearInfo.GridColor = System.Drawing.Color.Empty;
			this.vsYearInfo.Location = new System.Drawing.Point(20, 30);
			this.vsYearInfo.Name = "vsYearInfo";
			this.vsYearInfo.ReadOnly = true;
			this.vsYearInfo.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsYearInfo.RowHeightMin = 0;
			this.vsYearInfo.Rows = 1;
			this.vsYearInfo.ShowColumnVisibilityMenu = false;
			this.vsYearInfo.Size = new System.Drawing.Size(965, 232);
			this.vsYearInfo.StandardTab = true;
			this.vsYearInfo.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsYearInfo.TabIndex = 20;
			this.vsYearInfo.CurrentCellChanged += new System.EventHandler(this.vsYearInfo_RowColChange);
			this.vsYearInfo.Enter += new System.EventHandler(this.vsYearInfo_Enter);
			// 
			// vsSearch
			// 
			this.vsSearch.AllowSelection = false;
			this.vsSearch.AllowUserToResizeColumns = false;
			this.vsSearch.AllowUserToResizeRows = false;
			this.vsSearch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSearch.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSearch.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSearch.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSearch.BackColorSel = System.Drawing.Color.Empty;
			this.vsSearch.Cols = 5;
			dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
			this.vsSearch.ColumnHeadersHeight = 30;
			this.vsSearch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSearch.DefaultCellStyle = dataGridViewCellStyle12;
			this.vsSearch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsSearch.FixedCols = 0;
			this.vsSearch.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsSearch.FrozenCols = 0;
			this.vsSearch.GridColor = System.Drawing.Color.Empty;
			this.vsSearch.Location = new System.Drawing.Point(30, 30);
			//new System.Drawing.Point(583, 24);
			this.vsSearch.Name = "vsSearch";
			this.vsSearch.ReadOnly = true;
			this.vsSearch.RowHeadersVisible = false;
			this.vsSearch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSearch.RowHeightMin = 0;
			this.vsSearch.Rows = 1;
			this.vsSearch.ShowColumnVisibilityMenu = false;
			this.vsSearch.Size = new System.Drawing.Size(610, 243);
			//new System.Drawing.Size(114, 56);
			this.vsSearch.StandardTab = true;
			this.vsSearch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsSearch.TabIndex = 8;
			this.vsSearch.Visible = false;
			this.vsSearch.CurrentCellChanged += new System.EventHandler(this.vsSearch_RowColChange);
			this.vsSearch.ColumnWidthChanged += new Wisej.Web.DataGridViewColumnEventHandler(this.Grid_ColumnWidthChanged);
			this.vsSearch.RowHeightChanged += new Wisej.Web.DataGridViewRowEventHandler(this.Grid_RowHeightChanged);
			this.vsSearch.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsSearch_MouseMoveEvent);
			this.vsSearch.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsSearch_BeforeSort);
			this.vsSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSearch_KeyDownEvent);
			this.vsSearch.Click += new System.EventHandler(this.vsSearch_ClickEvent);
			this.vsSearch.DoubleClick += new System.EventHandler(this.vsSearch_DblClick);
			// 
			// fraSearch
			// 
			//this.fraSearch.AppearanceKey = "groupBoxNoBorders";
			//this.fraSearch.Controls.Add(this.fraGetAccount);
			this.fraSearch.Controls.Add(this.vsSearch);
			//this.fraSearch.Controls.Add(this.fraSearchCriteria);
			this.fraSearch.Controls.Add(this.lblLastAccount);
			this.fraSearch.Location = new System.Drawing.Point(30, 86);
			this.fraSearch.Name = "fraSearch";
			this.fraSearch.Size = new System.Drawing.Size(1025, 403);
			this.fraSearch.TabIndex = 4;
			this.fraSearch.Visible = false;
			// 
			// fraGetAccount
			// 
			//this.fraGetAccount.Controls.Add(this.cmdGetAccountNumber);
			//this.fraGetAccount.Controls.Add(this.cmbRE);
			//this.fraGetAccount.Controls.Add(this.txtGetAccountNumber);
			//this.fraGetAccount.Controls.Add(this.lblInstructions1);
			this.fraGetAccount.Location = new System.Drawing.Point(30, 30);
			this.fraGetAccount.Name = "fraGetAccount";
			this.fraGetAccount.Size = new System.Drawing.Size(525, 126);
			this.fraGetAccount.TabIndex = 9;
			this.fraGetAccount.Text = "Last Account Accessed ...";
			this.fraGetAccount.Visible = false;
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.AppearanceKey = "toolbarButton";
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(426, 66);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(92, 28);
			this.cmdGetAccountNumber.TabIndex = 14;
			this.cmdGetAccountNumber.Text = "Get Account";
			this.cmdGetAccountNumber.Visible = false;
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.AutoSize = false;
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(121, 23);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(136, 40);
			this.txtGetAccountNumber.TabIndex = 10;
			this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
			// this.txtGetAccountNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGetAccountNumber_KeyPress);
			// 
			// lblInstructions1
			// 
			this.lblInstructions1.Location = new System.Drawing.Point(30, 38);
			this.lblInstructions1.Name = "lblInstructions1";
			this.lblInstructions1.Size = new System.Drawing.Size(80, 20);
			this.lblInstructions1.TabIndex = 13;
			//this.lblInstructions1.Text = "PLEASE ENTER THE ACCOUNT NUMBER OR HIT ENTER TO VIEW THE ACCOUNT SHOWN.";
			this.lblInstructions1.Text = "ACCOUNT";
			// 
			// fraSearchCriteria
			// 
			this.fraSearchCriteria.AppearanceKey = "groupBoxNoBorders";
			this.fraSearchCriteria.Controls.Add(this.lblInstructions1);
			this.fraSearchCriteria.Controls.Add(this.txtGetAccountNumber);
			this.fraSearchCriteria.Controls.Add(this.cmdGetAccountNumber);
			this.fraSearchCriteria.Controls.Add(this.cmbRE);
			this.fraSearchCriteria.Controls.Add(this.fcLabel1);
			this.fraSearchCriteria.Controls.Add(this.cmdSearch);
			this.fraSearchCriteria.Controls.Add(this.txtSearch);
			this.fraSearchCriteria.Controls.Add(this.cmdClear);
			this.fraSearchCriteria.Controls.Add(this.cmbSearchType);
			this.fraSearchCriteria.Controls.Add(this.lblSearchType);
			this.fraSearchCriteria.Location = new System.Drawing.Point(0, 0);
			this.fraSearchCriteria.Name = "fraSearchCriteria";
			this.fraSearchCriteria.Size = new System.Drawing.Size(750, 87);
			this.fraSearchCriteria.TabIndex = 5;
			//this.fraSearchCriteria.Text = "Search";
			// 
			// fcLabel1
			// 
			this.fcLabel1.Location = new System.Drawing.Point(287, 38);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(79, 15);
			this.fcLabel1.TabIndex = 19;
			this.fcLabel1.Text = "SEARCH FOR";
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "toolbarButton";
			this.cmdSearch.Location = new System.Drawing.Point(110, 135);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(78, 28);
			this.cmdSearch.TabIndex = 17;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Visible = false;
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// txtSearch
			// 
			this.txtSearch.AutoSize = false;
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Location = new System.Drawing.Point(534, 23);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(181, 40);
			this.txtSearch.TabIndex = 18;
			this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
			// 
			// cmdClear
			// 
			this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(226, 135);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(99, 28);
			this.cmdClear.TabIndex = 16;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Visible = false;
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.AutoSize = true;
			this.lblLastAccount.Location = new System.Drawing.Point(426, 44);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(4, 14);
			this.lblLastAccount.TabIndex = 7;
			// 
			// txtHold
			// 
			this.txtHold.AutoSize = false;
			this.txtHold.BackColor = System.Drawing.SystemColors.Window;
			this.txtHold.Location = new System.Drawing.Point(9, 102);
			this.txtHold.Name = "txtHold";
			this.txtHold.Size = new System.Drawing.Size(132, 40);
			this.txtHold.TabIndex = 3;
			this.txtHold.Visible = false;
			// 
			// lblSearchListInstruction
			// 
			this.lblSearchListInstruction.Location = new System.Drawing.Point(85, 24);
			this.lblSearchListInstruction.Name = "lblSearchListInstruction";
			this.lblSearchListInstruction.Size = new System.Drawing.Size(236, 22);
			this.lblSearchListInstruction.TabIndex = 22;
			this.lblSearchListInstruction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblSearchListInstruction.Visible = false;
			// 
			// btnProcessGetAccount
			// 
			this.btnProcessGetAccount.AppearanceKey = "acceptButton";
			this.btnProcessGetAccount.AutoSize = true;
			this.btnProcessGetAccount.Location = new System.Drawing.Point(405, 30);
			this.btnProcessGetAccount.Name = "btnProcessGetAccount";
			this.btnProcessGetAccount.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcessGetAccount.Size = new System.Drawing.Size(88, 48);
			this.btnProcessGetAccount.TabIndex = 0;
			this.btnProcessGetAccount.Text = "Process";
			this.btnProcessGetAccount.Click += new System.EventHandler(this.mnuProcessGetAccount_Click);
			// 
			// btnFileSave
			// 
			this.btnFileSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileSave.AppearanceKey = "toolbarButton";
			this.btnFileSave.Enabled = false;
			this.btnFileSave.Location = new System.Drawing.Point(982, 29);
			this.btnFileSave.Name = "btnFileSave";
			this.btnFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.btnFileSave.Size = new System.Drawing.Size(50, 24);
			this.btnFileSave.TabIndex = 1;
			this.btnFileSave.Text = "Save";
			this.btnFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// btnProcessSearch
			// 
			this.btnProcessSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessSearch.AppearanceKey = "toolbarButton";
			this.btnProcessSearch.ImageSource = "button-search";
			this.btnProcessSearch.Location = new System.Drawing.Point(890, 29);
			this.btnProcessSearch.Name = "btnProcessSearch";
			this.btnProcessSearch.Size = new System.Drawing.Size(81, 24);
			this.btnProcessSearch.TabIndex = 2;
			this.btnProcessSearch.Text = "Search";
			this.btnProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
			// 
			// btnProcessClearSearch
			// 
			this.btnProcessClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessClearSearch.AppearanceKey = "toolbarButton";
			this.btnProcessClearSearch.Location = new System.Drawing.Point(792, 29);
			this.btnProcessClearSearch.Name = "btnProcessClearSearch";
			this.btnProcessClearSearch.Size = new System.Drawing.Size(95, 24);
			this.btnProcessClearSearch.TabIndex = 3;
			this.btnProcessClearSearch.Text = "Clear Search";
			this.btnProcessClearSearch.Click += new System.EventHandler(this.mnuProcessClearSearch_Click);
			// 
			// frmAREditBillInfo
			// 
			this.ClientSize = new System.Drawing.Size(1065, 852);
			this.KeyPreview = true;
			this.Name = "frmAREditBillInfo";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Bill Information";
			this.Load += new System.EventHandler(this.frmAREditBillInfo_Load);
			this.Activated += new System.EventHandler(this.frmAREditBillInfo_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAREditBillInfo_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAREditBillInfo_KeyPress);
			this.Resize += new System.EventHandler(this.frmAREditBillInfo_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.BottomPanel.PerformLayout();
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEditInfo)).EndInit();
			this.fraEditInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsEditInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsYearInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
			this.fraSearch.ResumeLayout(false);
			this.fraSearch.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGetAccount)).EndInit();
			this.fraGetAccount.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearchCriteria)).EndInit();
			this.fraSearchCriteria.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessGetAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessClearSearch)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel fcLabel1;
		internal FCButton btnProcessGetAccount;
		internal FCButton btnFileSave;
		internal FCButton btnProcessSearch;
		internal FCButton btnProcessClearSearch;
	}
}
