//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmARStatusList.
	/// </summary>
	partial class frmARStatusList : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkInclude;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCListBox lstTypes;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCFrame fraQuestions;
		public fecherFoundation.FCCheckBox chkCurrentInterest;
		public fecherFoundation.FCFrame fraInclude;
		public fecherFoundation.FCCheckBox chkInclude_2;
		public fecherFoundation.FCCheckBox chkInclude_1;
		public fecherFoundation.FCCheckBox chkInclude_0;
		public fecherFoundation.FCCheckBox chkShowPayments;
		public fecherFoundation.FCCheckBox chkSummaryOnly;
		public fecherFoundation.FCLabel lblShowFields;
		public Wisej.Web.ImageList ImageList1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmARStatusList));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.fraWhere = new fecherFoundation.FCFrame();
			this.vsWhere = new fecherFoundation.FCGrid();
			this.cmdClear = new fecherFoundation.FCButton();
			this.fraSort = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCDraggableListBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.fraFields = new fecherFoundation.FCFrame();
			this.lstTypes = new fecherFoundation.FCListBox();
			this.cmdExit = new fecherFoundation.FCButton();
			this.fraQuestions = new fecherFoundation.FCFrame();
			this.chkCurrentInterest = new fecherFoundation.FCCheckBox();
			this.fraInclude = new fecherFoundation.FCFrame();
			this.chkInclude_2 = new fecherFoundation.FCCheckBox();
			this.chkInclude_1 = new fecherFoundation.FCCheckBox();
			this.chkInclude_0 = new fecherFoundation.FCCheckBox();
			this.chkShowPayments = new fecherFoundation.FCCheckBox();
			this.chkSummaryOnly = new fecherFoundation.FCCheckBox();
			this.lblShowFields = new fecherFoundation.FCLabel();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
			this.fraWhere.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
			this.fraSort.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
			this.fraFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
			this.fraQuestions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCurrentInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraInclude)).BeginInit();
			this.fraInclude.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 655);
			this.BottomPanel.Size = new System.Drawing.Size(957, 96);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraWhere);
			this.ClientArea.Controls.Add(this.fraSort);
			this.ClientArea.Controls.Add(this.fraFields);
			this.ClientArea.Controls.Add(this.fraQuestions);
			this.ClientArea.Size = new System.Drawing.Size(957, 595);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Controls.Add(this.cmdExit);
			this.TopPanel.Size = new System.Drawing.Size(957, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.cmdExit, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(127, 30);
			this.HeaderText.Text = "Status List";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// fraWhere
			// 
			this.fraWhere.Controls.Add(this.vsWhere);
			this.fraWhere.Location = new System.Drawing.Point(30, 409);
			this.fraWhere.Name = "fraWhere";
			this.fraWhere.Size = new System.Drawing.Size(763, 206);
			this.fraWhere.TabIndex = 4;
			this.fraWhere.Text = "Select Search Criteria";
			this.ToolTip1.SetToolTip(this.fraWhere, null);
			this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
			// 
			// vsWhere
			// 
			this.vsWhere.AllowSelection = false;
			this.vsWhere.AllowUserToResizeColumns = false;
			this.vsWhere.AllowUserToResizeRows = false;
			this.vsWhere.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsWhere.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsWhere.BackColorBkg = System.Drawing.Color.Empty;
			this.vsWhere.BackColorFixed = System.Drawing.Color.Empty;
			this.vsWhere.BackColorSel = System.Drawing.Color.Empty;
			this.vsWhere.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsWhere.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsWhere.ColumnHeadersHeight = 30;
			this.vsWhere.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsWhere.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsWhere.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsWhere.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsWhere.FixedRows = 0;
			this.vsWhere.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsWhere.FrozenCols = 0;
			this.vsWhere.GridColor = System.Drawing.Color.Empty;
			this.vsWhere.Location = new System.Drawing.Point(20, 30);
			this.vsWhere.Name = "vsWhere";
			this.vsWhere.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsWhere.RowHeightMin = 0;
			this.vsWhere.Rows = 0;
			this.vsWhere.ShowColumnVisibilityMenu = false;
			this.vsWhere.Size = new System.Drawing.Size(720, 151);
			this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsWhere.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.vsWhere, null);
			this.vsWhere.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEdit);
			this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
			this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
			this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
			this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
			this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
			this.vsWhere.MouseMove += new Wisej.Web.MouseEventHandler(this.vsWhere_MouseMoveEvent);
			this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
			this.vsWhere.Leave += new System.EventHandler(this.vsWhere_Leave);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(785, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(146, 24);
			this.cmdClear.TabIndex = 0;
			this.cmdClear.Text = "Clear Search Criteria";
			this.ToolTip1.SetToolTip(this.cmdClear, null);
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// fraSort
			// 
			this.fraSort.Controls.Add(this.lstSort);
			this.fraSort.Location = new System.Drawing.Point(524, 226);
			this.fraSort.Name = "fraSort";
			this.fraSort.Size = new System.Drawing.Size(269, 156);
			this.fraSort.TabIndex = 2;
			this.fraSort.Text = "Fields To Sort By";
			this.ToolTip1.SetToolTip(this.fraSort, null);
			this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
			// 
			// lstSort
			// 
			this.lstSort.AllowDrag = true;
			this.lstSort.AllowDrop = true;
			this.lstSort.Appearance = 0;
			this.lstSort.BackColor = System.Drawing.SystemColors.Window;
			this.lstSort.CheckBoxes = true;
			//this.lstSort.Columns = 0;
			this.lstSort.Location = new System.Drawing.Point(20, 30);
			this.lstSort.MultiSelect = 0;
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(232, 101);
			this.lstSort.Sorted = false;
			this.lstSort.Style = 1;
			this.lstSort.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lstSort, null);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(411, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(96, 48);
			this.cmdPrint.TabIndex = 5;
			this.cmdPrint.Text = "Process";
			this.ToolTip1.SetToolTip(this.cmdPrint, null);
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// fraFields
			// 
			this.fraFields.Controls.Add(this.lstTypes);
			this.fraFields.Enabled = false;
			this.fraFields.Location = new System.Drawing.Point(30, 226);
			this.fraFields.Name = "fraFields";
			this.fraFields.Size = new System.Drawing.Size(453, 156);
			this.fraFields.TabIndex = 1;
			this.fraFields.Text = "Bill Types";
			this.ToolTip1.SetToolTip(this.fraFields, null);
			this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
			// 
			// lstTypes
			// 
			this.lstTypes.Appearance = 0;
			this.lstTypes.BackColor = System.Drawing.SystemColors.Window;
			this.lstTypes.CheckBoxes = true;
			//this.lstTypes.Columns = 0;
			this.lstTypes.Location = new System.Drawing.Point(20, 30);
			this.lstTypes.MultiSelect = 0;
			this.lstTypes.Name = "lstTypes";
			this.lstTypes.Size = new System.Drawing.Size(407, 101);
			this.lstTypes.Sorted = false;
			this.lstTypes.Style = 1;
			this.lstTypes.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lstTypes, null);
			// 
			// cmdExit
			// 
			this.cmdExit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdExit.AppearanceKey = "toolbarButton";
			this.cmdExit.Location = new System.Drawing.Point(736, 29);
			this.cmdExit.Name = "cmdExit";
			this.cmdExit.Size = new System.Drawing.Size(43, 24);
			this.cmdExit.TabIndex = 6;
			this.cmdExit.Text = "Exit";
			this.ToolTip1.SetToolTip(this.cmdExit, null);
			this.cmdExit.Visible = false;
			this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
			// 
			// fraQuestions
			// 
			this.fraQuestions.AppearanceKey = " groupBoxNoBorder";
			this.fraQuestions.Controls.Add(this.chkCurrentInterest);
			this.fraQuestions.Controls.Add(this.fraInclude);
			this.fraQuestions.Controls.Add(this.chkShowPayments);
			this.fraQuestions.Controls.Add(this.chkSummaryOnly);
			this.fraQuestions.Controls.Add(this.lblShowFields);
			this.fraQuestions.Location = new System.Drawing.Point(30, 30);
			this.fraQuestions.Name = "fraQuestions";
			this.fraQuestions.Size = new System.Drawing.Size(868, 176);
			this.fraQuestions.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.fraQuestions, null);
			// 
			// chkCurrentInterest
			// 
			this.chkCurrentInterest.Location = new System.Drawing.Point(396, 30);
			this.chkCurrentInterest.Name = "chkCurrentInterest";
			this.chkCurrentInterest.Size = new System.Drawing.Size(187, 27);
			this.chkCurrentInterest.TabIndex = 1;
			this.chkCurrentInterest.Text = "Show Current Interest";
			this.ToolTip1.SetToolTip(this.chkCurrentInterest, null);
			// 
			// fraInclude
			// 
			this.fraInclude.Controls.Add(this.chkInclude_2);
			this.fraInclude.Controls.Add(this.chkInclude_1);
			this.fraInclude.Controls.Add(this.chkInclude_0);
			this.fraInclude.Location = new System.Drawing.Point(636, 30);
			this.fraInclude.Name = "fraInclude";
			this.fraInclude.Size = new System.Drawing.Size(222, 74);
			this.fraInclude.TabIndex = 4;
			this.fraInclude.Text = "Include";
			this.ToolTip1.SetToolTip(this.fraInclude, null);
			// 
			// chkInclude_2
			// 
			this.chkInclude_2.Checked = true;
			this.chkInclude_2.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkInclude_2.Location = new System.Drawing.Point(160, 30);
			this.chkInclude_2.Name = "chkInclude_2";
			this.chkInclude_2.Size = new System.Drawing.Size(31, 27);
			this.chkInclude_2.TabIndex = 2;
			this.chkInclude_2.Text = "I";
			this.ToolTip1.SetToolTip(this.chkInclude_2, "Show Interest on the report.");
			// 
			// chkInclude_1
			// 
			this.chkInclude_1.Checked = true;
			this.chkInclude_1.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkInclude_1.Location = new System.Drawing.Point(90, 30);
			this.chkInclude_1.Name = "chkInclude_1";
			this.chkInclude_1.Size = new System.Drawing.Size(37, 27);
			this.chkInclude_1.TabIndex = 1;
			this.chkInclude_1.Text = "T";
			this.ToolTip1.SetToolTip(this.chkInclude_1, "Show Tax on the report.");
			// 
			// chkInclude_0
			// 
			this.chkInclude_0.Checked = true;
			this.chkInclude_0.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkInclude_0.Location = new System.Drawing.Point(20, 30);
			this.chkInclude_0.Name = "chkInclude_0";
			this.chkInclude_0.Size = new System.Drawing.Size(38, 27);
			this.chkInclude_0.TabIndex = 0;
			this.chkInclude_0.Text = "P";
			this.ToolTip1.SetToolTip(this.chkInclude_0, "Show Principal on the report.");
			// 
			// chkShowPayments
			// 
			this.chkShowPayments.Location = new System.Drawing.Point(396, 78);
			this.chkShowPayments.Name = "chkShowPayments";
			this.chkShowPayments.Size = new System.Drawing.Size(146, 27);
			this.chkShowPayments.TabIndex = 2;
			this.chkShowPayments.Text = "Show Payments";
			this.ToolTip1.SetToolTip(this.chkShowPayments, "This will show all of the payments for this account.");
			// 
			// chkSummaryOnly
			// 
			this.chkSummaryOnly.Location = new System.Drawing.Point(396, 126);
			this.chkSummaryOnly.Name = "chkSummaryOnly";
			this.chkSummaryOnly.Size = new System.Drawing.Size(181, 27);
			this.chkSummaryOnly.TabIndex = 3;
			this.chkSummaryOnly.Text = "Show Summary Only";
			this.ToolTip1.SetToolTip(this.chkSummaryOnly, null);
			// 
			// lblShowFields
			// 
			this.lblShowFields.Location = new System.Drawing.Point(20, 30);
			this.lblShowFields.Name = "lblShowFields";
			this.lblShowFields.Size = new System.Drawing.Size(344, 124);
			this.lblShowFields.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lblShowFields, null);
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1
			});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClear,
				this.mnuFileSeperator2,
				this.mnuPrint,
				this.mnuSP1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear Search Criteria";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 1;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 2;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Process";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 3;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// mnuLayout
			// 
			this.mnuLayout.Enabled = false;
			this.mnuLayout.Index = -1;
			this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAddRow,
				this.mnuAddColumn,
				this.mnuDeleteRow,
				this.mnuDeleteColumn
			});
			this.mnuLayout.Name = "mnuLayout";
			this.mnuLayout.Text = "Layout";
			this.mnuLayout.Visible = false;
			// 
			// mnuAddRow
			// 
			this.mnuAddRow.Index = 0;
			this.mnuAddRow.Name = "mnuAddRow";
			this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuAddRow.Text = "Add Row";
			// 
			// mnuAddColumn
			// 
			this.mnuAddColumn.Index = 1;
			this.mnuAddColumn.Name = "mnuAddColumn";
			this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuAddColumn.Text = "Add Column";
			// 
			// mnuDeleteRow
			// 
			this.mnuDeleteRow.Index = 2;
			this.mnuDeleteRow.Name = "mnuDeleteRow";
			this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuDeleteRow.Text = "Delete Row";
			// 
			// mnuDeleteColumn
			// 
			this.mnuDeleteColumn.Index = 3;
			this.mnuDeleteColumn.Name = "mnuDeleteColumn";
			this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuDeleteColumn.Text = "Delete Column";
			// 
			// frmARStatusList
			// 
			this.ClientSize = new System.Drawing.Size(957, 751);
			this.KeyPreview = true;
			this.Name = "frmARStatusList";
			this.Text = "Status Lists";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmARStatusList_Load);
			this.Activated += new System.EventHandler(this.frmARStatusList_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmARStatusList_KeyDown);
			this.Resize += new System.EventHandler(this.frmARStatusList_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
			this.fraWhere.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
			this.fraSort.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
			this.fraFields.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
			this.fraQuestions.ResumeLayout(false);
			this.fraQuestions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCurrentInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraInclude)).EndInit();
			this.fraInclude.ResumeLayout(false);
			this.fraInclude.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}