﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	public class modARStatusList
	{
		public const string CUSTOMREPORTDATABASE = "TWAR0000.VB1";
		// for frmCustomLabels
		// These constants are for frmCustomLabels
		public const int GRIDNONE = 0;
		public const int GRIDTEXT = 1;
		public const int GRIDDATE = 2;
		public const int GRIDCOMBOIDTEXT = 3;
		public const int GRIDNUMRANGE = 4;
		public const int GRIDCOMBOIDNUM = 5;
		public const int GRIDCOMBOTEXT = 6;
		public const int GRIDTEXTRANGE = 7;
		public const int GRIDCOMBOANDNUM = 8;
		// THIS HAS A COMBO BOX AND NUMERIC FIELD
		public const int GRIDCOMBORANGE = 9;
		public const int GRIDDATERANGE = 10;
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string FixQuotes(string strValue)
		{
			string FixQuotes = "";
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 20; intCount++)
			{
				Statics.strComboList[intCount, 0] = string.Empty;
				Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool IsValidDate(string DateToCheck)
		{
			bool IsValidDate = false;
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As short --> As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As short --> As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							MessageBox.Show("Invalid day on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							MessageBox.Show("Invalid day on Date", "ErTRIO Softwarevror", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}

		public static void SetupStatusListCombos_2(bool boolRegular)
		{
			SetupStatusListCombos(boolRegular);
		}

		public static void SetupStatusListCombos(bool boolRegular = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strRK = "";
				string strBook = "";
				// this will reset the arrays
				ClearComboListArray();
				// fill the book string
				rsTemp.OpenRecordset("SELECT * FROM DefaultBillTypes ORDER BY TypeCode asc", modCLCalculations.strARDatabase);
				while (!rsTemp.EndOfFile())
				{
					strBook += "#" + rsTemp.Get_Fields_Int32("TypeCode") + ";" + modGlobal.PadToString_8(rsTemp.Get_Fields_Int32("TypeCode"), 3) + "\t" + rsTemp.Get_Fields_String("TypeTitle") + "|";
					rsTemp.MoveNext();
				}
				// fill the book string
				rsTemp.OpenRecordset("SELECT * FROM RateKeys ORDER BY ID desc", modCLCalculations.strARDatabase);
				while (!rsTemp.EndOfFile())
				{
					strRK += "#" + rsTemp.Get_Fields_Int32("ID") + ";" + modGlobal.PadToString_8(rsTemp.Get_Fields_Int32("ID"), 5) + "\t" + "  " + rsTemp.Get_Fields_String("Description") + "|";
					rsTemp.MoveNext();
				}
				// fill them correctly
				if (boolRegular)
				{
					Statics.strComboList[frmARStatusList.lngRowReportType, 0] = "#0;Regular|#1;Aged Report";
					Statics.strComboList[frmARStatusList.lngRowBillType, 0] = strBook;
					Statics.strComboList[frmARStatusList.lngRowBillType, 1] = strBook;
					Statics.strComboList[frmARStatusList.lngRowRateKey, 0] = strRK;
					Statics.strComboList[frmARStatusList.lngRowRateKey, 1] = strRK;
					Statics.strComboList[frmARStatusList.lngRowBalanceDue, 0] = "#0;<" + "\t" + "Less Than|#1;>" + "\t" + "Greater Than|#2;=" + "\t" + "Equals|#3;<>" + "\t" + "Not Equal";
					Statics.strComboList[frmARStatusList.lngRowGroupBy, 0] = "#0;Account";
					Statics.strComboList[frmARStatusList.lngRowPaymentType, 0] = "#0;Payments|#1;Corrections";
				}
				else
				{
					Statics.strComboList[frmARStatusList.lngRowReportType, 0] = "#0;Regular|#1;Aged Report";
					Statics.strComboList[frmARStatusList.lngRowBillType, 0] = strBook;
					Statics.strComboList[frmARStatusList.lngRowBillType, 1] = strBook;
					Statics.strComboList[frmARStatusList.lngRowRateKey, 0] = strRK;
					Statics.strComboList[frmARStatusList.lngRowRateKey, 1] = strRK;
					Statics.strComboList[frmARStatusList.lngRowBalanceDue, 0] = "#0;<" + "\t" + "Less Than|#1;>" + "\t" + "Greater Than|#2;=" + "\t" + "Equals|#3;<>" + "\t" + "Not Equal";
					Statics.strComboList[frmARStatusList.lngRowGroupBy, 0] = "";
					Statics.strComboList[frmARStatusList.lngRowPaymentType, 0] = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Combos", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public class StaticVariables
		{
			public string[,] strComboList = new string[25 + 1, 25 + 1];
			
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
