﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using TWSharedLibrary;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptBillListSummary.
	/// </summary>
	public partial class rptBillListSummary : BaseSectionReport
	{
		public static rptBillListSummary InstancePtr
		{
			get
			{
				return (rptBillListSummary)Sys.GetInstance(typeof(rptBillListSummary));
			}
		}

		protected rptBillListSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillListSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		Decimal curInvoiceTotal;
		// vbPorter upgrade warning: curCustomerTotal As Decimal	OnWrite(Decimal, short)
		Decimal curCustomerTotal;

		public rptBillListSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bill List";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
			this.Fields.Add("InvoiceBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = rsInfo.Get_Fields_Int32("ActualAccountNumber");
				this.Fields["InvoiceBinder"].Value = rsInfo.Get_Fields_String("InvoiceNumber");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strStatus = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = "TRIO Software Corp";
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			if (frmBillList.InstancePtr.cmbStatusSelected.SelectedIndex == 1)
			{
				lblOptions.Text = "Status: All";
			}
			else
			{
				strStatus = "";
				if (frmBillList.InstancePtr.chkActive.CheckState == Wisej.Web.CheckState.Checked)
				{
					strStatus += "Outstanding, ";
				}
				if (frmBillList.InstancePtr.chkPaid.CheckState == Wisej.Web.CheckState.Checked)
				{
					strStatus += "Paid, ";
				}
				if (frmBillList.InstancePtr.chkVoid.CheckState == Wisej.Web.CheckState.Checked)
				{
					strStatus += "Voided, ";
				}
				strStatus = Strings.Left(strStatus, strStatus.Length - 2);
				lblOptions.Text = "Status: " + strStatus;
			}
			lblOptions.Text = lblOptions.Text + "          ";
			lblOptions.Text = lblOptions.Text + "Customer(s): " + frmBillList.InstancePtr.cboCustomers.Text;
			lblOptions.Text = lblOptions.Text + "          ";
			lblOptions.Text = lblOptions.Text + "Bill Type(s): " + frmBillList.InstancePtr.cboBillType.Text;
			lblDateRange.Text = frmBillList.InstancePtr.txtStartDate.Text + " To " + frmBillList.InstancePtr.txtEndDate.Text;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsTypeInfo = new clsDRWrapper();
			// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
			rsTypeInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsInfo.Get_Fields("BillType"));
			if (rsTypeInfo.EndOfFile() != true && rsTypeInfo.BeginningOfFile() != true)
			{
				fldType.Text = rsTypeInfo.Get_Fields_String("TypeTitle");
			}
			else
			{
				fldType.Text = "UNKNOWN";
			}
			fldStatus.Text = rsInfo.Get_Fields_String("BillStatus");
			fldInvoiceNumber.Text = rsInfo.Get_Fields_String("InvoiceNumber");
			fldBillDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
			fldAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("PrinOwed") + rsInfo.Get_Fields_Decimal("TaxOwed"), "#,##0.00");
			curCustomerTotal += rsInfo.Get_Fields_Decimal("PrinOwed") + rsInfo.Get_Fields_Decimal("TaxOwed");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldCustomerTotal.Text = Strings.Format(curCustomerTotal, "#,##0.00");
			curCustomerTotal = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldAddress1.Visible = true;
			fldAddress2.Visible = true;
			fldAddress3.Visible = true;
			fldAddress4.Visible = true;
			fldCustomerNumber.Text = Strings.Format(rsInfo.Get_Fields_Int32("ActualAccountNumber"), "0000");
			fldCustomer.Text = rsInfo.Get_Fields_String("BName");
			fldAddress1.Text = rsInfo.Get_Fields_String("BAddress1");
			fldAddress2.Text = rsInfo.Get_Fields_String("BAddress2");
			fldAddress3.Text = rsInfo.Get_Fields_String("BAddress3");
			if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("BZip4"))) != "")
			{
				fldAddress4.Text = rsInfo.Get_Fields_String("BCity") + ", " + rsInfo.Get_Fields_String("BState") + " " + rsInfo.Get_Fields_String("BZip") + "-" + rsInfo.Get_Fields_String("BZip4");
			}
			else
			{
				fldAddress4.Text = rsInfo.Get_Fields_String("BCity") + ", " + rsInfo.Get_Fields_String("BState") + " " + rsInfo.Get_Fields_String("BZip");
			}
			if (Strings.Trim(fldAddress3.Text) == "")
			{
				fldAddress3.Text = Strings.Trim(fldAddress4.Text);
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress2.Text) == "")
			{
				fldAddress2.Text = Strings.Trim(fldAddress3.Text);
				fldAddress3.Text = Strings.Trim(fldAddress4.Text);
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress1.Text) == "")
			{
				fldAddress1.Text = Strings.Trim(fldAddress2.Text);
				fldAddress2.Text = Strings.Trim(fldAddress3.Text);
				fldAddress3.Text = Strings.Trim(fldAddress4.Text);
				fldAddress4.Text = "";
			}
			if (fldAddress1.Text == "" || frmBillList.InstancePtr.chkPrintAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				fldAddress1.Visible = false;
			}
			if (fldAddress2.Text == "" || frmBillList.InstancePtr.chkPrintAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				fldAddress2.Visible = false;
			}
			if (fldAddress3.Text == "" || frmBillList.InstancePtr.chkPrintAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				fldAddress3.Visible = false;
			}
			if (fldAddress4.Text == "" || frmBillList.InstancePtr.chkPrintAddress.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				fldAddress4.Visible = false;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		public void Init(ref string strSQL)
		{
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				frmReportViewer.InstancePtr.Init(this);
			}
			else
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
                //FC:FINAL:AM:#4409 - close form
                frmBillList.InstancePtr.Close();
                return;
			}
		}

		private void rptBillListSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBillListSummary.Caption	= "Bill List";
			//rptBillListSummary.Icon	= "rptBillListSummary.dsx":0000";
			//rptBillListSummary.Left	= 0;
			//rptBillListSummary.Top	= 0;
			//rptBillListSummary.Width	= 11880;
			//rptBillListSummary.Height	= 8595;
			//rptBillListSummary.StartUpPosition	= 3;
			//rptBillListSummary.SectionData	= "rptBillListSummary.dsx":508A;
			//End Unmaped Properties
		}

		private void rptBillListSummary_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}

        private void rptBillListSummary_ReportEnd(object sender, System.EventArgs e)
        {
			rsInfo.DisposeOf();
            frmBillList.InstancePtr.Close();
        }
    }
}
