﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmGetPayee.
	/// </summary>
	partial class frmGetPayee : BaseForm
	{
		public fecherFoundation.FCComboBox cmbDescription;
		public fecherFoundation.FCLabel lblHidden;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdGet;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label5;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbDescription = new fecherFoundation.FCComboBox();
            this.lblHidden = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.cmdGet = new fecherFoundation.FCButton();
            this.vs1 = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.lblSearchInfo = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdQuit = new fecherFoundation.FCButton();
            this.lblLastAccount = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.btnProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 384);
            this.BottomPanel.Size = new System.Drawing.Size(744, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Size = new System.Drawing.Size(744, 324);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdQuit);
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Controls.Add(this.cmdSearch);
            this.TopPanel.Size = new System.Drawing.Size(744, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdQuit, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Get Payee";
            // 
            // cmbDescription
            // 
            this.cmbDescription.AutoSize = false;
            this.cmbDescription.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDescription.FormattingEnabled = true;
            this.cmbDescription.Items.AddRange(new object[] {
            "Description"});
            this.cmbDescription.Location = new System.Drawing.Point(388, 23);
            this.cmbDescription.Name = "cmbDescription";
            this.cmbDescription.Size = new System.Drawing.Size(135, 40);
            this.cmbDescription.TabIndex = 11;
            // 
            // lblHidden
            // 
            this.lblHidden.Location = new System.Drawing.Point(287, 38);
            this.lblHidden.Name = "lblHidden";
            this.lblHidden.Size = new System.Drawing.Size(79, 15);
            this.lblHidden.TabIndex = 12;
            this.lblHidden.Text = "SEARCH BY";
            // 
            // Frame3
            // 
            this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame3.BackColor = System.Drawing.Color.White;
            this.Frame3.Controls.Add(this.cmdReturn);
            this.Frame3.Controls.Add(this.cmdGet);
            this.Frame3.Controls.Add(this.vs1);
            this.Frame3.Location = new System.Drawing.Point(30, 86);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(595, 339);
            this.Frame3.TabIndex = 0;
            this.Frame3.Text = "Multiple Records";
            this.Frame3.Visible = false;
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "toolbarButton";
            this.cmdReturn.Location = new System.Drawing.Point(20, 293);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(178, 24);
            this.cmdReturn.TabIndex = 3;
            this.cmdReturn.Text = "Return to Search Screen";
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // cmdGet
            // 
            this.cmdGet.AppearanceKey = "toolbarButton";
            this.cmdGet.Location = new System.Drawing.Point(204, 293);
            this.cmdGet.Name = "cmdGet";
            this.cmdGet.Size = new System.Drawing.Size(124, 24);
            this.cmdGet.TabIndex = 2;
            this.cmdGet.Text = "Retrieve Record";
            this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
            // 
            // vs1
            // 
            this.vs1.AllowSelection = false;
            this.vs1.AllowUserToResizeColumns = false;
            this.vs1.AllowUserToResizeRows = false;
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
            this.vs1.BackColorBkg = System.Drawing.Color.Empty;
            this.vs1.BackColorFixed = System.Drawing.Color.Empty;
            this.vs1.BackColorSel = System.Drawing.Color.Empty;
            this.vs1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vs1.Cols = 4;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vs1.ColumnHeadersHeight = 30;
            this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
            this.vs1.DragIcon = null;
            this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vs1.FixedCols = 0;
            this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
            this.vs1.FrozenCols = 0;
            this.vs1.GridColor = System.Drawing.Color.Empty;
            this.vs1.GridColorFixed = System.Drawing.Color.Empty;
            this.vs1.Location = new System.Drawing.Point(20, 30);
            this.vs1.Name = "vs1";
            this.vs1.OutlineCol = 0;
            this.vs1.ReadOnly = true;
            this.vs1.RowHeadersVisible = false;
            this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vs1.RowHeightMin = 0;
            this.vs1.Rows = 1;
            this.vs1.ScrollTipText = null;
            this.vs1.ShowColumnVisibilityMenu = false;
            this.vs1.Size = new System.Drawing.Size(555, 243);
            this.vs1.StandardTab = true;
            this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vs1.TabIndex = 1;
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.txtSearch);
            this.Frame2.Controls.Add(this.cmbDescription);
            this.Frame2.Controls.Add(this.lblHidden);
            this.Frame2.Controls.Add(this.lblSearchInfo);
            this.Frame2.Controls.Add(this.Label5);
            this.Frame2.Controls.Add(this.txtGetAccountNumber);
            this.Frame2.Location = new System.Drawing.Point(0, 0);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(720, 87);
            this.Frame2.TabIndex = 7;
            // 
            // txtSearch
            // 
            this.txtSearch.AutoSize = false;
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.LinkItem = null;
            this.txtSearch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSearch.LinkTopic = null;
            this.txtSearch.Location = new System.Drawing.Point(534, 23);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(181, 40);
            this.txtSearch.TabIndex = 10;
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            // 
            // lblSearchInfo
            // 
            this.lblSearchInfo.Location = new System.Drawing.Point(20, 104);
            this.lblSearchInfo.Name = "lblSearchInfo";
            this.lblSearchInfo.Size = new System.Drawing.Size(142, 16);
            this.lblSearchInfo.TabIndex = 14;
            this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
            // 
            // Label5
            // 
            this.Label5.Font = new System.Drawing.Font("Proxima Nova Regular", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label5.Location = new System.Drawing.Point(30, 38);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(177, 20);
            this.Label5.TabIndex = 15;
            this.Label5.Text = "PAYEE";
            // 
            // txtGetAccountNumber
            // 
            this.txtGetAccountNumber.AutoSize = false;
            this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtGetAccountNumber.LinkItem = null;
            this.txtGetAccountNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtGetAccountNumber.LinkTopic = null;
            this.txtGetAccountNumber.Location = new System.Drawing.Point(121, 23);
            this.txtGetAccountNumber.Name = "txtGetAccountNumber";
            this.txtGetAccountNumber.Size = new System.Drawing.Size(136, 40);
            this.txtGetAccountNumber.TabIndex = 4;
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSearch.AppearanceKey = "toolbarButton";
            this.cmdSearch.ImageSource = "button-search";
            this.cmdSearch.Location = new System.Drawing.Point(614, 29);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(81, 24);
            this.cmdSearch.TabIndex = 9;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.AppearanceKey = "toolbarButton";
            this.cmdClear.Location = new System.Drawing.Point(517, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(93, 24);
            this.cmdClear.TabIndex = 8;
            this.cmdClear.Text = "Clear Search";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdQuit
            // 
            this.cmdQuit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdQuit.AppearanceKey = "toolbarButton";
            this.cmdQuit.Location = new System.Drawing.Point(472, 29);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(41, 24);
            this.cmdQuit.TabIndex = 5;
            this.cmdQuit.Text = "Quit";
            this.cmdQuit.Visible = false;
            this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
            // 
            // lblLastAccount
            // 
            this.lblLastAccount.Font = new System.Drawing.Font("Proxima Nova Regular", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblLastAccount.Location = new System.Drawing.Point(213, 30);
            this.lblLastAccount.Name = "lblLastAccount";
            this.lblLastAccount.Size = new System.Drawing.Size(55, 19);
            this.lblLastAccount.TabIndex = 17;
            this.lblLastAccount.Visible = false;
            // 
            // Label2
            // 
            this.Label2.Font = new System.Drawing.Font("Proxima Nova Regular", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label2.Location = new System.Drawing.Point(30, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(162, 16);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "LAST PAYEE ACCESSED ..";
            this.Label2.Visible = false;
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.AppearanceKey = "acceptButton";
            this.btnProcessSave.Location = new System.Drawing.Point(361, 30);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcessSave.Size = new System.Drawing.Size(88, 48);
            this.btnProcessSave.TabIndex = 0;
            this.btnProcessSave.Text = "Process";
            this.btnProcessSave.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
            // 
            // frmGetPayee
            // 
            this.ClientSize = new System.Drawing.Size(744, 492);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmGetPayee";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Get Payee";
            this.Load += new System.EventHandler(this.frmGetPayee_Load);
            this.Activated += new System.EventHandler(this.frmGetPayee_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetPayee_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetPayee_KeyPress);
            this.Resize += new System.EventHandler(this.frmGetPayee_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcessSave;
	}
}
