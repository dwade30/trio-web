﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using TWSharedLibrary;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for rptCustomerList.
	/// </summary>
	public partial class rptCustomerList : BaseSectionReport
	{
		public static rptCustomerList InstancePtr
		{
			get
			{
				return (rptCustomerList)Sys.GetInstance(typeof(rptCustomerList));
			}
		}

		protected rptCustomerList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomerList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		clsDRWrapper rsCustomerInfo = new clsDRWrapper();

		public rptCustomerList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Customer List";
            this.ReportEnd += RptCustomerList_ReportEnd;
		}

        private void RptCustomerList_ReportEnd(object sender, EventArgs e)
        {
            rsCustomerInfo.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsCustomerInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
					return;
				}
			}
			else
			{
				rsCustomerInfo.MoveNext();
				if (rsCustomerInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
					return;
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmCustomerListSetup.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strOrder = "";
			string strStatusSQL = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			if (frmCustomerListSetup.InstancePtr.cmbNumber.SelectedIndex == 0)
			{
				rsCustomerInfo.OpenRecordset("SELECT c.*, p.* FROM (" + frmCustomerListSetup.InstancePtr.strSQL + ") as c CROSS APPLY " + rsCustomerInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p ORDER BY p.FullName");
			}
			else
			{
				rsCustomerInfo.OpenRecordset("SELECT c.*, p.* FROM (" + frmCustomerListSetup.InstancePtr.strSQL + ") as c CROSS APPLY " + rsCustomerInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p ORDER BY c.CustomerID");
			}
			if (rsCustomerInfo.EndOfFile() != true && rsCustomerInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				this.Cancel();
				MessageBox.Show("No customers were found that matched the search criteria", "No Customers Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			Decimal curTotal;
			//clsDRWrapper rsAdjustments = new clsDRWrapper();
			int intCounter;
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			// vbPorter upgrade warning: cPhone As cPartyPhoneNumber	OnWrite(Collection)
			cPartyPhoneNumber cPhone;
			//FCCollection pPhoneNumbers = new FCCollection();
			Line2.Visible = true;
			if (frmCustomerListSetup.InstancePtr.chkDataEntryMessage.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("DataEntryMessage"))) != "")
				{
					lblDataEntryMessage.Visible = true;
					linDataEntryMessage.Visible = true;
					rtfDataEntryMessage.Visible = true;
					rtfDataEntryMessage.Text = Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("DataEntryMessage")));			}
				else
				{
					if (frmCustomerListSetup.InstancePtr.cmbMessageNo.SelectedIndex == 0)
					{
						lblEmail.Visible = false;
						fldCustomer.Text = "";
						fldName.Text = "";
						fldPhone1.Text = "";
						fldPhone2.Text = "";
						fldPhone3.Text = "";
						fldStatus.Text = "";
						fldEmail.Text = "";
						fldAddress1.Text = "";
						fldAddress2.Text = "";
						fldAddress3.Text = "";
						fldAddress4.Text = "";
						lblBillMessage.Visible = false;
						linBillMessage.Visible = false;
						rtfBillMessage.Visible = false;
						lblDataEntryMessage.Visible = false;
						linDataEntryMessage.Visible = false;
						rtfDataEntryMessage.Visible = false;
						lblMessage.Visible = false;
						linMessage.Visible = false;
						rtfMessage.Visible = false;
						Line2.Visible = false;
						return;
					}
					else
					{
						lblDataEntryMessage.Visible = false;
						linDataEntryMessage.Visible = false;
						rtfDataEntryMessage.Visible = false;
					}
				}
			}
			else
			{
				lblDataEntryMessage.Visible = false;
				linDataEntryMessage.Visible = false;
				rtfDataEntryMessage.Visible = false;
			}
			if (frmCustomerListSetup.InstancePtr.chkCustomerMessage.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Message"))) != "")
				{
					lblMessage.Visible = true;
					linMessage.Visible = true;
					rtfMessage.Visible = true;
					rtfMessage.Text = Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Message")));
				}
				else
				{
					lblMessage.Visible = false;
					linMessage.Visible = false;
					rtfMessage.Visible = false;
				}
			}
			else
			{
				lblMessage.Visible = false;
				linMessage.Visible = false;
				rtfMessage.Visible = false;
			}
			if (frmCustomerListSetup.InstancePtr.chkBillMessage.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("BillMessage"))) != "")
				{
					lblBillMessage.Visible = true;
					linBillMessage.Visible = true;
					rtfBillMessage.Visible = true;
					rtfBillMessage.Text = Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("BillMessage")));
				}
				else
				{
					lblBillMessage.Visible = false;
					linBillMessage.Visible = false;
					rtfBillMessage.Visible = false;
				}
			}
			else
			{
				lblBillMessage.Visible = false;
				linBillMessage.Visible = false;
				rtfBillMessage.Visible = false;
			}
			if (frmCustomerListSetup.InstancePtr.chkEmail.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblEmail.Visible = true;
				if (Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Email"))) != "")
				{
					fldEmail.Visible = true;
					fldEmail.Text = rsCustomerInfo.Get_Fields_String("Email");
				}
				else
				{
					fldEmail.Visible = false;
				}
			}
			else
			{
				lblEmail.Visible = false;
				fldEmail.Visible = false;
			}
			if (frmCustomerListSetup.InstancePtr.chkAlternateAddress.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (FCConvert.ToString(rsCustomerInfo.Get_Fields_String("AddressType")) == "Primary" || FCConvert.ToString(rsCustomerInfo.Get_Fields_String("AddressType")) == "")
				{
					if (frmCustomerListSetup.InstancePtr.cmbAltAddressYes.SelectedIndex == 0)
					{
						lblEmail.Visible = false;
						fldCustomer.Text = "";
						fldName.Text = "";
						fldPhone1.Text = "";
						fldPhone2.Text = "";
						fldPhone3.Text = "";
						fldStatus.Text = "";
						fldEmail.Text = "";
						fldAddress1.Text = "";
						fldAddress2.Text = "";
						fldAddress3.Text = "";
						fldAddress4.Text = "";
						lblBillMessage.Visible = false;
						linBillMessage.Visible = false;
						rtfBillMessage.Visible = false;
						lblDataEntryMessage.Visible = false;
						linDataEntryMessage.Visible = false;
						rtfDataEntryMessage.Visible = false;
						lblMessage.Visible = false;
						linMessage.Visible = false;
						rtfMessage.Visible = false;
						Line2.Visible = false;
						return;
					}
				}
			}
			fldCustomer.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(rsCustomerInfo.Get_Fields_Int32("CustomerID")), 4);
			fldName.Text = rsCustomerInfo.Get_Fields_String("FullName");
			fldStatus.Text = rsCustomerInfo.Get_Fields_String("Status");
			pInfo = pCont.GetParty(FCConvert.ToInt32(rsCustomerInfo.Get_Fields_Int32("PartyID")));
			if (!(pInfo == null))
			{
				if (pInfo.PhoneNumbers.Count > 0)
				{
					cPhone = pInfo.PhoneNumbers[1];
					fldPhone1.Text = cPhone.Description + ": " + cPhone.PhoneNumber;
					if (cPhone.Extension != "")
					{
						fldPhone1.Text += " Ext: " + cPhone.Extension;
					}
					if (pInfo.PhoneNumbers.Count > 1)
					{
						cPhone = pInfo.PhoneNumbers[2];
						fldPhone1.Text = cPhone.Description + ": " + cPhone.PhoneNumber;
						if (cPhone.Extension != "")
						{
							fldPhone1.Text += " Ext: " + cPhone.Extension;
						}
						if (pInfo.PhoneNumbers.Count > 2)
						{
							cPhone = pInfo.PhoneNumbers[3];
							fldPhone1.Text = cPhone.Description + ": " + cPhone.PhoneNumber;
							if (cPhone.Extension != "")
							{
								fldPhone1.Text += " Ext: " + cPhone.Extension;
							}
						}
						else
						{
							fldPhone3.Text = "";
						}
					}
					else
					{
						fldPhone2.Text = "";
						fldPhone3.Text = "";
					}
				}
				else
				{
					fldPhone1.Text = "";
					fldPhone2.Text = "";
					fldPhone3.Text = "";
				}
			}
			if (frmCustomerListSetup.InstancePtr.chkAddress.CheckState == Wisej.Web.CheckState.Checked)
			{
				fldAddress1.Text = Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Address1")));
				fldAddress2.Text = Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Address2")));
				fldAddress3.Text = Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Address3")));
				// TODO: Field [PartyState] not found!! (maybe it is an alias?)
				if (Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields("PartyState"))) != "")
				{
					// TODO: Field [PartyState] not found!! (maybe it is an alias?)
					fldAddress4.Text = Strings.Trim(rsCustomerInfo.Get_Fields_String("City") + ", " + Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields("PartyState"))) + " " + Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Zip"))));
				}
				else
				{
					fldAddress4.Text = Strings.Trim(rsCustomerInfo.Get_Fields_String("City") + " " + Strings.Trim(FCConvert.ToString(rsCustomerInfo.Get_Fields_String("Zip"))));
				}
			}
			else
			{
				fldAddress1.Text = "";
				fldAddress2.Text = "";
				fldAddress3.Text = "";
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress3.Text) == "")
			{
				fldAddress3.Text = fldAddress4.Text;
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress2.Text) == "")
			{
				fldAddress2.Text = fldAddress3.Text;
				fldAddress3.Text = "";
			}
			if (Strings.Trim(fldAddress1.Text) == "")
			{
				fldAddress1.Text = fldAddress2.Text;
				fldAddress2.Text = "";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptCustomerList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCustomerList.Caption	= "Customer List";
			//rptCustomerList.Icon	= "rptCustomerList.dsx":0000";
			//rptCustomerList.Left	= 0;
			//rptCustomerList.Top	= 0;
			//rptCustomerList.Width	= 11880;
			//rptCustomerList.Height	= 8595;
			//rptCustomerList.StartUpPosition	= 3;
			//rptCustomerList.SectionData	= "rptCustomerList.dsx":508A;
			//End Unmaped Properties
		}

		private void rptCustomerList_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
