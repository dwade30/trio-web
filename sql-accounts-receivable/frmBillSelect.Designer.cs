﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmBillSelect.
	/// </summary>
	partial class frmBillSelect : BaseForm
	{
		public fecherFoundation.FCGrid vsBills;
		public fecherFoundation.FCComboBox cboMonth;
		public fecherFoundation.FCComboBox cboYear;
		public fecherFoundation.FCLabel lblSelectMonth;
		public fecherFoundation.FCLabel lblSelectBill;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsBills = new fecherFoundation.FCGrid();
			this.cboMonth = new fecherFoundation.FCComboBox();
			this.cboYear = new fecherFoundation.FCComboBox();
			this.lblSelectMonth = new fecherFoundation.FCLabel();
			this.lblSelectBill = new fecherFoundation.FCLabel();
			this.btnProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 351);
			this.BottomPanel.Size = new System.Drawing.Size(734, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsBills);
			this.ClientArea.Controls.Add(this.cboMonth);
			this.ClientArea.Controls.Add(this.cboYear);
			this.ClientArea.Controls.Add(this.lblSelectMonth);
			this.ClientArea.Controls.Add(this.lblSelectBill);
			this.ClientArea.Size = new System.Drawing.Size(734, 291);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(734, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(120, 30);
			this.HeaderText.Text = "Select Bill";
			// 
			// vsBills
			// 
			this.vsBills.AllowSelection = false;
			this.vsBills.AllowUserToResizeColumns = false;
			this.vsBills.AllowUserToResizeRows = false;
			this.vsBills.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsBills.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBills.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBills.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBills.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBills.BackColorSel = System.Drawing.Color.Empty;
			this.vsBills.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBills.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsBills.ColumnHeadersHeight = 30;
			this.vsBills.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBills.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsBills.DragIcon = null;
			this.vsBills.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBills.ExtendLastCol = true;
			this.vsBills.FixedCols = 0;
			this.vsBills.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBills.FrozenCols = 0;
			this.vsBills.GridColor = System.Drawing.Color.Empty;
			this.vsBills.GridColorFixed = System.Drawing.Color.Empty;
			this.vsBills.Location = new System.Drawing.Point(30, 66);
			this.vsBills.Name = "vsBills";
			this.vsBills.ReadOnly = true;
			this.vsBills.RowHeadersVisible = false;
			this.vsBills.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBills.RowHeightMin = 0;
			this.vsBills.Rows = 1;
			this.vsBills.ScrollTipText = null;
			this.vsBills.ShowColumnVisibilityMenu = false;
			this.vsBills.Size = new System.Drawing.Size(674, 203);
			this.vsBills.StandardTab = true;
			this.vsBills.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsBills.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBills.TabIndex = 0;
			this.vsBills.Visible = false;
			this.vsBills.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsBills_KeyPressEvent);
			this.vsBills.DoubleClick += new System.EventHandler(this.vsBills_DblClick);
			// 
			// cboMonth
			// 
			this.cboMonth.AutoSize = false;
			this.cboMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboMonth.FormattingEnabled = true;
			this.cboMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboMonth.Location = new System.Drawing.Point(30, 66);
			this.cboMonth.Name = "cboMonth";
			this.cboMonth.Size = new System.Drawing.Size(149, 40);
			this.cboMonth.TabIndex = 1;
			// 
			// cboYear
			// 
			this.cboYear.AutoSize = false;
			this.cboYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboYear.FormattingEnabled = true;
			this.cboYear.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboYear.Location = new System.Drawing.Point(185, 66);
			this.cboYear.Name = "cboYear";
			this.cboYear.Size = new System.Drawing.Size(100, 40);
			this.cboYear.TabIndex = 2;
			// 
			// lblSelectMonth
			// 
			this.lblSelectMonth.Location = new System.Drawing.Point(30, 30);
			this.lblSelectMonth.Name = "lblSelectMonth";
			this.lblSelectMonth.Size = new System.Drawing.Size(268, 16);
			this.lblSelectMonth.TabIndex = 4;
			this.lblSelectMonth.Text = "PLEASE SELECT THE MONTH YOU ARE BILLING";
			// 
			// lblSelectBill
			// 
			this.lblSelectBill.Location = new System.Drawing.Point(30, 30);
			this.lblSelectBill.Name = "lblSelectBill";
			this.lblSelectBill.Size = new System.Drawing.Size(336, 16);
			this.lblSelectBill.TabIndex = 3;
			this.lblSelectBill.Text = "PLEASE SELECT THE TYPE OF BILL YOU WISH TO PROCESS";
			this.lblSelectBill.Visible = false;
			// 
			// btnProcessSave
			// 
			this.btnProcessSave.AppearanceKey = "acceptButton";
			this.btnProcessSave.Location = new System.Drawing.Point(306, 30);
			this.btnProcessSave.Name = "btnProcessSave";
			this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcessSave.Size = new System.Drawing.Size(88, 48);
			this.btnProcessSave.TabIndex = 0;
			this.btnProcessSave.Text = "Process";
			this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmBillSelect
			// 
			this.ClientSize = new System.Drawing.Size(734, 459);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBillSelect";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select Bill";
			this.Load += new System.EventHandler(this.frmBillSelect_Load);
			this.Activated += new System.EventHandler(this.frmBillSelect_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBillSelect_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcessSave;
	}
}
