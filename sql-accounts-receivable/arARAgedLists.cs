﻿using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using TWSharedLibrary;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for arARAgedLists.
	/// </summary>
	public partial class arARAgedLists : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/08/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/04/2006              *
		// ********************************************************
		string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[5 + 1];
		// 0 - Current Tax, 1 - 30 Day, 2 - 60 Day, 3 - 90 Day, 4 - 120 Day, 5 - Total
		int lngCount;
		clsDRWrapper rsCalBill = new clsDRWrapper();
		clsDRWrapper rsRK = new clsDRWrapper();
		DateTime dtCurrentDate;
		bool boolSummaryOnly;
		bool boolPrincipal;
		bool boolInterest;
		bool boolTaxes;
		// these are for the summaries in the report footer
		double[] dblYearTotals = new double[2000 + 1];
		// billingyear - 19800
		double[,] dblPayments = new double[4 + 1, 6 + 1];
		// 0 - C, 1 - I, 2 - P, 3 - Total
		// 0 - Principal, 1 - Charged Interest, 2 - Current Interest, 3 - Tax, 4 - Total
		public static arARAgedLists InstancePtr
		{
			get
			{
				return (arARAgedLists)Sys.GetInstance(typeof(arARAgedLists));
			}
		}

		protected arARAgedLists _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsRK.Dispose();
				rsCalBill.Dispose();
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		public arARAgedLists()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				lngCount = 0;
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				// this is what the time scale will be
				if (modGlobal.Statics.gboolARUseAsOfDate)
				{
					dtCurrentDate = modGlobal.Statics.gdtARStatusListAsOfDate;
				}
				else
				{
					dtCurrentDate = DateTime.Today;
				}
				boolSummaryOnly = FCConvert.CBool(frmARStatusList.InstancePtr.chkSummaryOnly.CheckState == CheckState.Checked);
				boolPrincipal = FCConvert.CBool(frmARStatusList.InstancePtr.chkInclude[0].CheckState == CheckState.Checked);
				boolTaxes = FCConvert.CBool(frmARStatusList.InstancePtr.chkInclude[1].CheckState == CheckState.Checked);
				boolInterest = FCConvert.CBool(frmARStatusList.InstancePtr.chkInclude[2].CheckState == CheckState.Checked);
				SetupFields();
				// Moves/shows the correct fields into the right places
				SetReportHeader();
				// Sets the titles and moves labels in the header
				strSQL = BuildSQL();
				// Generates the SQL String
				rsData.OpenRecordset(strSQL, modCLCalculations.strARDatabase);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading", true, rsData.RecordCount());
				if (rsData.EndOfFile() && rsData.BeginningOfFile())
				{
					frmWait.InstancePtr.Unload();
					NoRecords();
				}
				else
				{
					// open the lien table to be used laters
					rsRK.OpenRecordset("SELECT * FROM RateKeys", modCLCalculations.strARDatabase);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Aged List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void NoRecords()
		{
			// this will terminate the report and show a message to the user
			MessageBox.Show("There are no records matching the criteria selected.", "Status List", MessageBoxButtons.OK, MessageBoxIcon.Information);
			this.Close();
			return;
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause = "";
			string strREPPBill = "";
			string strREPPPayment = "";
			string strREInnerJoin = "";
			string strAsOfDateString = "";
			string strSelectFields = "";
			string strBillingType = "";
			// This is an aged report using the Where Criteria to report the correct information
			if (Strings.Trim(frmARStatusList.InstancePtr.strRSWhere) != "")
			{
				strTemp = "SELECT DISTINCT ActualAccountNumber, BName FROM (SELECT * FROM Bill WHERE " + frmARStatusList.InstancePtr.strRSWhere + ") as temp";
			}
			else
			{
				strTemp = "SELECT DISTINCT ActualAccountNumber, BName FROM (SELECT * FROM Bill) as temp";
			}
			if (Strings.Trim(frmARStatusList.InstancePtr.strRSOrder) != "")
			{
				strTemp += " ORDER BY " + frmARStatusList.InstancePtr.strRSOrder;
			}
			else
			{
				strTemp += " ORDER BY BName, ActualAccountNumber";
			}
			BuildSQL = strTemp;
			return BuildSQL;
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2;
			float lngHt;
			if (boolSummaryOnly)
			{
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldCurrent.Visible = false;
				fld30Day.Visible = false;
				fld60Day.Visible = false;
				fld90Day.Visible = false;
				fld120Day.Visible = false;
				fldTotal.Visible = false;
				lblAccount.Visible = false;
				lnHeader.Visible = false;
				lnTotals.Visible = false;
				Detail.Height = 0;
				return;
			}
			intRow = 2;
			lngHt = 270 / 1440F;
			this.Detail.Height = 1 * lngHt + 100 / 1440F;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				//clsDRWrapper rsPayment = new clsDRWrapper();
				int lngRK = 0;
				int lngIndex = 0;
				string strTemp = "";
				bool boolMasterInfo;
				double dblCurrentInterest;
				// this is the calculated current interest for each bill
				double[] dblDue = new double[5 + 1];
				// 0 - Current, 1 - 30 Day, 2 - 60 Day, 3 - 90 Day, 4 - Lien, 5 - Total
				double dblTotalDue = 0;
				TRYNEXTACCOUNT:
				;
				// reset fields and variables
				ClearFields();
				frmWait.InstancePtr.IncrementProgress();
				dblCurrentInterest = 0;
				dblDue[0] = 0;
				dblDue[1] = 0;
				dblDue[2] = 0;
				dblDue[3] = 0;
				dblDue[4] = 0;
				dblDue[5] = 0;
				if (rsData.EndOfFile())
				{
					return;
				}
				// get all the bills for this account in reverse order so the newest is first
				if (Strings.Trim(frmARStatusList.InstancePtr.strRSWhere) != "")
				{
					rsCalBill.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND BName = '" + modARStatusList.FixQuotes(rsData.Get_Fields_String("BName")) + "' AND " + frmARStatusList.InstancePtr.strRSWhere + " ORDER BY BillNumber desc", modCLCalculations.strARDatabase);
				}
				else
				{
					rsCalBill.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND BName = '" + modARStatusList.FixQuotes(rsData.Get_Fields_String("BName")) + "' ORDER BY BillNumber desc", modCLCalculations.strARDatabase);
				}
				if (rsCalBill.EndOfFile())
				{
					// if there are no bills
					rsData.MoveNext();
					goto TRYNEXTACCOUNT;
				}
				// Name
				fldName.Text = rsData.Get_Fields_String("BName");
				// Account Number
				fldAccount.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ActualAccountNumber"));
				dblCurrentInterest = 0;
				while (!rsCalBill.EndOfFile())
				{
					dblTotalDue = 0;
					dblCurrentInterest = 0;
					if (FCConvert.ToInt32(rsCalBill.Get_Fields_Int32("BillNumber")) == 0)
					{
						lngIndex = 0;
						if (boolPrincipal)
						{
							// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
							dblTotalDue = FCConvert.ToDouble(rsCalBill.Get_Fields("PrinPaid")) * -1;
						}
						dblDue[lngIndex] += dblTotalDue;
					}
					else
					{
						// find the information about the payments here
						lngRK = FCConvert.ToInt32(rsCalBill.Get_Fields_Int32("BillNumber"));
						lngIndex = FindIndexForAgedList(ref lngRK, ref dtCurrentDate);
						if (frmARStatusList.InstancePtr.boolShowCurrentInterest)
						{
							dblTotalDue = modARCalculations.CalculateAccountAR(rsCalBill, DateTime.Today, ref dblCurrentInterest);
						}
						// 10/24/2006
						dblTotalDue = 0;
						if (boolPrincipal)
						{
							// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
							dblTotalDue += FCConvert.ToDouble(rsCalBill.Get_Fields_Decimal("PrinOwed") - rsCalBill.Get_Fields("PrinPaid"));
						}
						if (boolTaxes)
						{
							// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
							dblTotalDue += FCConvert.ToDouble(rsCalBill.Get_Fields_Decimal("TaxOwed") - rsCalBill.Get_Fields("TaxPaid"));
						}
						// DJW changed + to - when adding in interest since it is a negative number to start
						if (frmARStatusList.InstancePtr.boolShowCurrentInterest && boolInterest)
						{
							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
							dblTotalDue += FCConvert.ToDouble(-rsCalBill.Get_Fields("IntAdded") - rsCalBill.Get_Fields("IntPaid")) + dblCurrentInterest;
						}
						else if (boolInterest)
						{
							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
							dblTotalDue += FCConvert.ToDouble(-rsCalBill.Get_Fields("IntAdded") - rsCalBill.Get_Fields("IntPaid"));
						}
						dblDue[lngIndex] += dblTotalDue;
					}
					rsCalBill.MoveNext();
				}
				// find the total
				dblDue[5] = FCUtils.Round(dblDue[0] + dblDue[1] + dblDue[2] + dblDue[3] + dblDue[4], 2);
				if (!CheckBalanceDue(ref dblDue[5]))
				{
					rsData.MoveNext();
					goto TRYNEXTACCOUNT;
				}
				// now check to make sure that this account matches the balance due criteria, if not go to the next account
				lngCount += 1;
				// fill in all of the fields
				fldCurrent.Text = Strings.Format(dblDue[0], "#,##0.00");
				fld30Day.Text = Strings.Format(dblDue[1], "#,##0.00");
				fld60Day.Text = Strings.Format(dblDue[2], "#,##0.00");
				fld90Day.Text = Strings.Format(dblDue[3], "#,##0.00");
				fld120Day.Text = Strings.Format(dblDue[4], "#,##0.00");
				fldTotal.Text = Strings.Format(dblDue[5], "#,##0.00");
				// add the totals for the sum
				dblTotals[0] += dblDue[0];
				// Current
				dblTotals[1] += dblDue[1];
				// 30 Day
				dblTotals[2] += dblDue[2];
				// 60 Day
				dblTotals[3] += dblDue[3];
				// 90 Day
				dblTotals[4] += dblDue[4];
				// 120 Day
				dblTotals[5] += dblDue[5];
				// Total
				// move to the next record in the query
				rsData.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckBalanceDue(ref double dblAmt)
		{
			bool CheckBalanceDue = false;
			// this function will return true if this amount matches the
			double dblTestAmt = 0;
			if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowBalanceDue, 2)) != "")
			{
				dblTestAmt = FCConvert.ToDouble(frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowBalanceDue, 2));
				string vbPorterVar = frmARStatusList.InstancePtr.vsWhere.TextMatrix(frmARStatusList.lngRowBalanceDue, 1);
				if (vbPorterVar == ">")
				{
					CheckBalanceDue = dblAmt > dblTestAmt;
				}
				else if (vbPorterVar == "<")
				{
					CheckBalanceDue = dblAmt < dblTestAmt;
				}
				else if (vbPorterVar == "<>")
				{
					CheckBalanceDue = dblAmt != dblTestAmt;
				}
				else if (vbPorterVar == "=")
				{
					CheckBalanceDue = dblAmt == dblTestAmt;
				}
				else if (vbPorterVar == "")
				{
					CheckBalanceDue = true;
				}
				else
				{
					CheckBalanceDue = false;
				}
			}
			else
			{
				CheckBalanceDue = true;
			}
			return CheckBalanceDue;
		}

		private void ClearFields()
		{
			// this routine will clear the fields
			fldAccount.Text = "";
			fldName.Text = "";
			fldCurrent.Text = "";
			fld30Day.Text = "";
			fld60Day.Text = "";
			fld90Day.Text = "";
			fld120Day.Text = "";
			fldTotal.Text = "";
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the totals line at the bottom of the report
			fldTotalCurrent.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotal30Day.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotal60Day.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldTotal90Day.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotal120Day.Text = Strings.Format(dblTotals[4], "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotals[5], "#,##0.00");
			if (lngCount > 1)
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Accounts:";
			}
			else if (lngCount == 1)
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Account:";
			}
			else
			{
				lblTotals.Text = "No Accounts";
			}
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
			string strTypes = "";
			for (intCT = 0; intCT <= frmARStatusList.InstancePtr.vsWhere.Rows - 1; intCT++)
			{
				if (frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) != "" || frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2) != "")
				{
					switch (intCT)
					{
						case frmARStatusList.lngRowAccount:
							{
								// Account Number
								if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += "Account: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
										}
										else
										{
											strTemp += "Account: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
										}
									}
									else
									{
										strTemp += "Below Account: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								}
								else
								{
									strTemp += "Above Account: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
								break;
							}
						case frmARStatusList.lngRowName:
							{
								// Name
								if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += " Name: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
										}
										else
										{
											strTemp += " Name: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
										}
									}
									else
									{
										strTemp += " Below Name: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								}
								else
								{
									strTemp += " Above Name: " + frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
								break;
							}
						case frmARStatusList.lngRowBillType:
							{
								// Bill Type
								if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										if (Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += "Bill Type: " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1))) + " ";
										}
										else
										{
											strTemp += "Bill Type: " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1))) + " To " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)));
										}
									}
									else
									{
										strTemp += "Below Bill Type: " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)));
									}
								}
								else
								{
									strTemp += "Above Bill Type: " + FCConvert.ToString(Conversion.Val(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)));
								}
								break;
							}
						case frmARStatusList.lngRowBalanceDue:
							{
								// Balance Due
								strTemp += " Balance Due " + Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) + Strings.Trim(frmARStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2));
								break;
							}
						default:
							{
								// Exit For
								break;
							}
					}
					//end switch
				}
			}
			if (frmARStatusList.InstancePtr.lstTypes.SelectedItems.Any())
			{
				strTypes = "";
				for (intCT = 0; intCT <= frmARStatusList.InstancePtr.lstTypes.Items.Count - 1; intCT++)
				{
					if (frmARStatusList.InstancePtr.lstTypes.Selected(intCT))
					{
						strTypes += FCConvert.ToString(frmARStatusList.InstancePtr.lstTypes.ItemData(intCT)) + ",";
					}
				}
				if (strTypes.Length > 1)
				{
					if (strTemp != "")
					{
						strTemp += " Bill Types: " + Strings.Left(strTypes, strTypes.Length - 1);
					}
					else
					{
						strTemp = "Bill Types: " + Strings.Left(strTypes, strTypes.Length - 1);
					}
				}
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List";
			}
			else
			{
				lblReportType.Text = strTemp;
			}
			lblReportType.Text = lblReportType.Text + " Showing :";
			if (boolPrincipal)
			{
				lblReportType.Text = lblReportType.Text + " Principal";
			}
			if (boolTaxes)
			{
				lblReportType.Text = lblReportType.Text + " Tax";
			}
			if (boolInterest)
			{
				lblReportType.Text = lblReportType.Text + " Interest";
			}
			lblHeader.Text = "Status List";
			if (modGlobal.Statics.gboolARUseAsOfDate)
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As Of Date: " + Strings.Format(modGlobal.Statics.gdtARStatusListAsOfDate, "MM/dd/yyyy");
			}
		}

		private int FindIndexForAgedList(ref int lngRateKey, ref DateTime dtCheckDate)
		{
			int FindIndexForAgedList = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// vbPorter upgrade warning: lngDays As int --> As long
				long lngDays;
				rsRK.FindFirstRecord("ID", lngRateKey);
				if (!rsRK.NoMatch)
				{
					lngDays = DateAndTime.DateDiff("D", (DateTime)rsRK.Get_Fields_DateTime("BillDate"), dtCheckDate);
					if (lngDays < 30)
					{
						// Current Taxes
						FindIndexForAgedList = 0;
					}
					else if (lngDays >= 30 && lngDays <= 59)
					{
						// 30 Days
						FindIndexForAgedList = 1;
					}
					else if (lngDays >= 60 && lngDays <= 89)
					{
						// 60 Days
						FindIndexForAgedList = 2;
					}
					else if (lngDays >= 90 && lngDays <= 119)
					{
						// 90 Days
						FindIndexForAgedList = 3;
					}
					else if (lngDays > 120)
					{
						// 120 Days
						FindIndexForAgedList = 4;
					}
				}
				else
				{
					// if no RK is found then put the amount in the current category
					FindIndexForAgedList = 0;
				}
				return FindIndexForAgedList;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FindIndexForAgedList = 0;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Index", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindIndexForAgedList;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalCurrent.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotal30Day.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotal60Day.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldTotal90Day.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotal120Day.Text = Strings.Format(dblTotals[4], "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotals[5], "#,##0.00");
			if (lngCount == 1)
			{
				lblTotals.Text = "Total for 1 account:";
			}
			else
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " accounts:";
			}
		}

		
	}
}
