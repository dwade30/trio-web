﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomerBills.
	/// </summary>
	partial class frmCustomerBills : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtTitle;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTitle;
		public fecherFoundation.FCPanel fraReceipt;
		public fecherFoundation.FCComboBox cboType;
		public fecherFoundation.FCComboBox cboBill;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtTitle_2;
		public fecherFoundation.FCTextBox txtTitle_1;
		public fecherFoundation.FCTextBox txtTitle_0;
		public fecherFoundation.FCTextBox txtTitle_3;
		public fecherFoundation.FCLabel lblTitle_2;
		public fecherFoundation.FCLabel lblTitle_1;
		public fecherFoundation.FCLabel lblTitle_0;
		public fecherFoundation.FCLabel lblTitle_3;
		public fecherFoundation.FCFrame fraAcct;
		public Global.T2KDateBox txtStartDate;
		public fecherFoundation.FCCheckBox chkProrate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCTextBox txtComment;
		public fecherFoundation.FCGrid vsFees;
		public fecherFoundation.FCLabel lblNextScheduledBillingDate;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblNextScheduledBilling;
		//public fecherFoundation.FCLabel lblTypeDescription;
		public fecherFoundation.FCLabel lblArrayIndex;
		public fecherFoundation.FCLabel lblComment;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileCustomer;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator3;
		public fecherFoundation.FCToolStripMenuItem mnuFileAdd;
		public fecherFoundation.FCToolStripMenuItem mnuFileDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator1;
		public fecherFoundation.FCToolStripMenuItem mnuFileMovePrevious;
		public fecherFoundation.FCToolStripMenuItem mnuFileMoveNext;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveAndContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSpacer;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
			this.fraReceipt = new fecherFoundation.FCPanel();
			this.cboType = new fecherFoundation.FCComboBox();
			this.cboBill = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtTitle_2 = new fecherFoundation.FCTextBox();
			this.txtTitle_1 = new fecherFoundation.FCTextBox();
			this.txtTitle_0 = new fecherFoundation.FCTextBox();
			this.txtTitle_3 = new fecherFoundation.FCTextBox();
			this.lblTitle_2 = new fecherFoundation.FCLabel();
			this.lblTitle_1 = new fecherFoundation.FCLabel();
			this.lblTitle_0 = new fecherFoundation.FCLabel();
			this.lblTitle_3 = new fecherFoundation.FCLabel();
			this.fraAcct = new fecherFoundation.FCFrame();
			this.txtStartDate = new Global.T2KDateBox();
			this.chkProrate = new fecherFoundation.FCCheckBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.txtComment = new fecherFoundation.FCTextBox();
			this.vsFees = new fecherFoundation.FCGrid();
			this.lblNextScheduledBillingDate = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblNextScheduledBilling = new fecherFoundation.FCLabel();
			//this.lblTypeDescription = new fecherFoundation.FCLabel();
			this.lblArrayIndex = new fecherFoundation.FCLabel();
			this.lblComment = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFileAdd = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMovePrevious = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMoveNext = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileCustomer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveAndContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSpacer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdCustomer = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReceipt)).BeginInit();
			this.fraReceipt.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAcct)).BeginInit();
			this.fraAcct.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkProrate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsFees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCustomer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 630);
			this.BottomPanel.Size = new System.Drawing.Size(922, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraReceipt);
			this.ClientArea.Size = new System.Drawing.Size(922, 570);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSave);
			this.TopPanel.Controls.Add(this.cmdCustomer);
			this.TopPanel.Size = new System.Drawing.Size(922, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdCustomer, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(119, 30);
			this.HeaderText.Text = "Customer";
			// 
			// fraReceipt
			// 
			this.fraReceipt.AppearanceKey = "groupBoxNoBorders";
			this.fraReceipt.Controls.Add(this.cboType);
			this.fraReceipt.Controls.Add(this.cboBill);
			this.fraReceipt.Controls.Add(this.Frame1);
			this.fraReceipt.Controls.Add(this.fraAcct);
			this.fraReceipt.Controls.Add(this.txtComment);
			this.fraReceipt.Controls.Add(this.vsFees);
			this.fraReceipt.Controls.Add(this.lblNextScheduledBillingDate);
			this.fraReceipt.Controls.Add(this.Label4);
			this.fraReceipt.Controls.Add(this.Label1);
			this.fraReceipt.Controls.Add(this.lblNextScheduledBilling);
			//this.fraReceipt.Controls.Add(this.lblTypeDescription);
			this.fraReceipt.Controls.Add(this.lblArrayIndex);
			this.fraReceipt.Controls.Add(this.lblComment);
			this.fraReceipt.Location = new System.Drawing.Point(0, 0);
			this.fraReceipt.Name = "fraReceipt";
			this.fraReceipt.Size = new System.Drawing.Size(919, 657);
			this.fraReceipt.TabIndex = 0;
			// 
			// cboType
			// 
			this.cboType.AutoSize = false;
			this.cboType.BackColor = System.Drawing.SystemColors.Window;
			this.cboType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboType.FormattingEnabled = true;
			this.cboType.Location = new System.Drawing.Point(330, 30);
			this.cboType.Name = "cboType";
			this.cboType.Size = new System.Drawing.Size(296, 40);
			this.cboType.Sorted = true;
			this.cboType.TabIndex = 4;
			this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
			this.cboType.DropDown += new System.EventHandler(this.cboType_DropDown);
			// 
			// cboBill
			// 
			this.cboBill.AutoSize = false;
			this.cboBill.BackColor = System.Drawing.SystemColors.Window;
			this.cboBill.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBill.FormattingEnabled = true;
			this.cboBill.Location = new System.Drawing.Point(95, 30);
			this.cboBill.Name = "cboBill";
			this.cboBill.Size = new System.Drawing.Size(131, 40);
			this.cboBill.TabIndex = 1;
			this.cboBill.Text = "Combo1";
			this.cboBill.SelectedIndexChanged += new System.EventHandler(this.cboBill_SelectedIndexChanged);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.txtTitle_2);
			this.Frame1.Controls.Add(this.txtTitle_1);
			this.Frame1.Controls.Add(this.txtTitle_0);
			this.Frame1.Controls.Add(this.txtTitle_3);
			this.Frame1.Controls.Add(this.lblTitle_2);
			this.Frame1.Controls.Add(this.lblTitle_1);
			this.Frame1.Controls.Add(this.lblTitle_0);
			this.Frame1.Controls.Add(this.lblTitle_3);
			this.Frame1.Location = new System.Drawing.Point(10, 118);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(504, 164);
			this.Frame1.TabIndex = 9;
			// 
			// txtTitle_2
			//
			this.txtTitle_2.MaxLength = 10;
			this.txtTitle_2.AutoSize = false;
            this.txtTitle_2.CharacterCasing = CharacterCasing.Upper;
			this.txtTitle_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle_2.Location = new System.Drawing.Point(177, 102);
			this.txtTitle_2.Name = "txtTitle_2";
			this.txtTitle_2.Size = new System.Drawing.Size(130, 40);
			this.txtTitle_2.TabIndex = 5;
			this.txtTitle_2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTitle_KeyDown);
			this.txtTitle_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
			this.txtTitle_2.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle_2.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
			this.txtTitle_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
			// 
			// txtTitle_1
			//
			this.txtTitle_1.MaxLength = 10;
			this.txtTitle_1.AutoSize = false;
            this.txtTitle_1.CharacterCasing = CharacterCasing.Upper;
            this.txtTitle_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle_1.Location = new System.Drawing.Point(20, 102);
			this.txtTitle_1.Name = "txtTitle_1";
			this.txtTitle_1.Size = new System.Drawing.Size(130, 40);
			this.txtTitle_1.TabIndex = 3;
			this.txtTitle_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTitle_KeyDown);
			this.txtTitle_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
			this.txtTitle_1.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle_1.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
			this.txtTitle_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
			// 
			// txtTitle_0
			//
			this.txtTitle_0.MaxLength = 10;
			this.txtTitle_0.AutoSize = false;
            this.txtTitle_0.CharacterCasing = CharacterCasing.Upper;
            this.txtTitle_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle_0.Location = new System.Drawing.Point(177, 20);
			this.txtTitle_0.Name = "txtTitle_0";
			this.txtTitle_0.Size = new System.Drawing.Size(130, 40);
			this.txtTitle_0.TabIndex = 1;
			this.txtTitle_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTitle_KeyDown);
			this.txtTitle_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
			this.txtTitle_0.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle_0.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
			this.txtTitle_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
			// 
			// txtTitle_3
			//
			this.txtTitle_3.MaxLength = 10;
			this.txtTitle_3.AutoSize = false;
            this.txtTitle_3.CharacterCasing = CharacterCasing.Upper;
            this.txtTitle_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle_3.Location = new System.Drawing.Point(337, 102);
			this.txtTitle_3.Name = "txtTitle_3";
			this.txtTitle_3.Size = new System.Drawing.Size(130, 40);
			this.txtTitle_3.TabIndex = 7;
			this.txtTitle_3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTitle_KeyDown);
			this.txtTitle_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
			this.txtTitle_3.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle_3.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
			this.txtTitle_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
			// 
			// lblTitle_2
			// 
			this.lblTitle_2.Location = new System.Drawing.Point(177, 78);
			this.lblTitle_2.Name = "lblTitle_2";
			this.lblTitle_2.Size = new System.Drawing.Size(114, 18);
			this.lblTitle_2.TabIndex = 4;
			this.lblTitle_2.Text = "CONTROL2";
			this.lblTitle_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTitle_1
			// 
			this.lblTitle_1.Location = new System.Drawing.Point(20, 78);
			this.lblTitle_1.Name = "lblTitle_1";
			this.lblTitle_1.Size = new System.Drawing.Size(114, 18);
			this.lblTitle_1.TabIndex = 2;
			this.lblTitle_1.Text = "CONTROL1";
			this.lblTitle_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTitle_0
			// 
			this.lblTitle_0.Location = new System.Drawing.Point(20, 34);
			this.lblTitle_0.Name = "lblTitle_0";
			this.lblTitle_0.Size = new System.Drawing.Size(133, 20);
			this.lblTitle_0.TabIndex = 0;
			this.lblTitle_0.Text = "REF";
			this.lblTitle_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTitle_3
			// 
			this.lblTitle_3.Location = new System.Drawing.Point(337, 78);
			this.lblTitle_3.Name = "lblTitle_3";
			this.lblTitle_3.Size = new System.Drawing.Size(114, 18);
			this.lblTitle_3.TabIndex = 6;
			this.lblTitle_3.Text = "CONTROL3";
			this.lblTitle_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraAcct
			// 
			this.fraAcct.AppearanceKey = "groupBoxNoBorders";
			this.fraAcct.Controls.Add(this.txtStartDate);
			this.fraAcct.Controls.Add(this.chkProrate);
			this.fraAcct.Controls.Add(this.Label2);
			this.fraAcct.Location = new System.Drawing.Point(529, 151);
			this.fraAcct.Name = "fraAcct";
			this.fraAcct.Size = new System.Drawing.Size(260, 120);
			this.fraAcct.TabIndex = 11;
			// 
			// txtStartDate
			//
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Location = new System.Drawing.Point(128, 70);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(115, 40);
			this.txtStartDate.TabIndex = 2;
			this.txtStartDate.Text = "  /  /";
			this.txtStartDate.TextChanged += new System.EventHandler(this.txtStartDate_Change);
			// 
			// chkProrate
			// 
			this.chkProrate.Location = new System.Drawing.Point(20, 30);
			this.chkProrate.Name = "chkProrate";
			this.chkProrate.Size = new System.Drawing.Size(81, 26);
			this.chkProrate.TabIndex = 0;
			this.chkProrate.Text = "Prorate";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 84);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(79, 18);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "START DATE";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtComment
			//
			this.txtComment.MaxLength = 255;
			this.txtComment.AutoSize = false;
			this.txtComment.BackColor = System.Drawing.SystemColors.Window;
			this.txtComment.Location = new System.Drawing.Point(152, 603);
			this.txtComment.Name = "txtComment";
			this.txtComment.Size = new System.Drawing.Size(641, 40);
			this.txtComment.TabIndex = 13;
			this.txtComment.Enter += new System.EventHandler(this.txtComment_Enter);
			this.txtComment.TextChanged += new System.EventHandler(this.txtComment_TextChanged);
			// 
			// vsFees
			// 
			this.vsFees.AllowSelection = false;
			this.vsFees.AllowUserToResizeColumns = false;
			this.vsFees.AllowUserToResizeRows = false;
			this.vsFees.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsFees.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsFees.BackColorBkg = System.Drawing.Color.Empty;
			this.vsFees.BackColorFixed = System.Drawing.Color.Empty;
			this.vsFees.BackColorSel = System.Drawing.Color.Empty;
			this.vsFees.Cols = 3;
			dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsFees.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
			this.vsFees.ColumnHeadersHeight = 30;
			this.vsFees.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsFees.DefaultCellStyle = dataGridViewCellStyle12;
			this.vsFees.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsFees.ExtendLastCol = true;
			this.vsFees.FixedCols = 2;
			this.vsFees.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsFees.FrozenCols = 1;
			this.vsFees.GridColor = System.Drawing.Color.Empty;
			this.vsFees.Location = new System.Drawing.Point(30, 286);
			this.vsFees.Name = "vsFees";
			this.vsFees.ReadOnly = true;
			this.vsFees.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsFees.RowHeightMin = 0;
			this.vsFees.Rows = 7;
			this.vsFees.ShowColumnVisibilityMenu = false;
			this.vsFees.Size = new System.Drawing.Size(763, 291);
			this.vsFees.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsFees.TabIndex = 10;
			this.vsFees.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsFees_KeyDownEdit);
			this.vsFees.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsFees_KeyPressEdit);
			this.vsFees.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsFees_ChangeEdit);
			this.vsFees.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsFees_ValidateEdit);
			this.vsFees.CurrentCellChanged += new System.EventHandler(this.vsFees_RowColChange);
			this.vsFees.Enter += new System.EventHandler(this.vsFees_Enter);
			this.vsFees.Click += new System.EventHandler(this.vsFees_ClickEvent);
			this.vsFees.DoubleClick += new System.EventHandler(this.vsFees_DblClick);
			// 
			// lblNextScheduledBillingDate
			// 
			this.lblNextScheduledBillingDate.Location = new System.Drawing.Point(226, 92);
			this.lblNextScheduledBillingDate.Name = "lblNextScheduledBillingDate";
			this.lblNextScheduledBillingDate.Size = new System.Drawing.Size(245, 19);
			this.lblNextScheduledBillingDate.TabIndex = 7;
			this.lblNextScheduledBillingDate.Text = "CUSTOMER";
			this.lblNextScheduledBillingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 44);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(64, 20);
			this.Label4.TabIndex = 0;
			this.Label4.Text = "BILL #";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(258, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(48, 20);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "TYPE";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblNextScheduledBilling
			// 
			this.lblNextScheduledBilling.Location = new System.Drawing.Point(30, 92);
			this.lblNextScheduledBilling.Name = "lblNextScheduledBilling";
			this.lblNextScheduledBilling.Size = new System.Drawing.Size(182, 20);
			this.lblNextScheduledBilling.TabIndex = 6;
			this.lblNextScheduledBilling.Text = "NEXT SCHEDULED BILLING";
			this.lblNextScheduledBilling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblTypeDescription
			// 
			//this.lblTypeDescription.Location = new System.Drawing.Point(637, 44);
			//this.lblTypeDescription.Name = "lblTypeDescription";
			//this.lblTypeDescription.Size = new System.Drawing.Size(260, 24);
			//this.lblTypeDescription.TabIndex = 5;
			// 
			// lblArrayIndex
			// 
			this.lblArrayIndex.Location = new System.Drawing.Point(20, 320);
			this.lblArrayIndex.Name = "lblArrayIndex";
			this.lblArrayIndex.Size = new System.Drawing.Size(56, 19);
			this.lblArrayIndex.TabIndex = 8;
			this.lblArrayIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblComment
			// 
			this.lblComment.Location = new System.Drawing.Point(30, 613);
			this.lblComment.Name = "lblComment";
			this.lblComment.Size = new System.Drawing.Size(64, 19);
			this.lblComment.TabIndex = 12;
			this.lblComment.Text = "COMMENT";
			this.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileAdd,
				this.mnuFileDelete,
				this.mnuFileMovePrevious,
				this.mnuFileMoveNext
			});
			this.MainMenu1.Name = null;
			// 
			// mnuFileAdd
			// 
			this.mnuFileAdd.Index = 0;
			this.mnuFileAdd.Name = "mnuFileAdd";
			this.mnuFileAdd.Text = "Add Bill";
			this.mnuFileAdd.Click += new System.EventHandler(this.mnuFileAdd_Click);
			// 
			// mnuFileDelete
			// 
			this.mnuFileDelete.Index = 1;
			this.mnuFileDelete.Name = "mnuFileDelete";
			this.mnuFileDelete.Text = "Delete Bill";
			this.mnuFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// mnuFileMovePrevious
			// 
			this.mnuFileMovePrevious.Index = 2;
			this.mnuFileMovePrevious.Name = "mnuFileMovePrevious";
			this.mnuFileMovePrevious.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuFileMovePrevious.Text = "Move Previous";
			this.mnuFileMovePrevious.Click += new System.EventHandler(this.mnuFileMovePrevious_Click);
			// 
			// mnuFileMoveNext
			// 
			this.mnuFileMoveNext.Index = 3;
			this.mnuFileMoveNext.Name = "mnuFileMoveNext";
			this.mnuFileMoveNext.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuFileMoveNext.Text = "Move Next";
			this.mnuFileMoveNext.Click += new System.EventHandler(this.mnuFileMoveNext_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileCustomer
			// 
			this.mnuFileCustomer.Index = -1;
			this.mnuFileCustomer.Name = "mnuFileCustomer";
			this.mnuFileCustomer.Shortcut = Wisej.Web.Shortcut.F9;
			this.mnuFileCustomer.Text = "Customer Screen";
			this.mnuFileCustomer.Click += new System.EventHandler(this.mnuFileCustomer_Click);
			// 
			// mnuFileSeperator3
			// 
			this.mnuFileSeperator3.Index = -1;
			this.mnuFileSeperator3.Name = "mnuFileSeperator3";
			this.mnuFileSeperator3.Text = "-";
			// 
			// mnuFileSeperator1
			// 
			this.mnuFileSeperator1.Index = -1;
			this.mnuFileSeperator1.Name = "mnuFileSeperator1";
			this.mnuFileSeperator1.Text = "-";
			// 
			// mnuSeperator2
			// 
			this.mnuSeperator2.Index = -1;
			this.mnuSeperator2.Name = "mnuSeperator2";
			this.mnuSeperator2.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = -1;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveAndContinue
			// 
			this.mnuFileSaveAndContinue.Index = -1;
			this.mnuFileSaveAndContinue.Name = "mnuFileSaveAndContinue";
			this.mnuFileSaveAndContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveAndContinue.Text = "Save & Continue";
			this.mnuFileSaveAndContinue.Click += new System.EventHandler(this.mnuFileSaveAndContinue_Click);
			// 
			// mnuSpacer
			// 
			this.mnuSpacer.Index = -1;
			this.mnuSpacer.Name = "mnuSpacer";
			this.mnuSpacer.Text = "-";
			// 
			// mnuProcessExit
			// 
			this.mnuProcessExit.Index = -1;
			this.mnuProcessExit.Name = "mnuProcessExit";
			this.mnuProcessExit.Text = "Exit";
			this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
			// 
			// cmdCustomer
			// 
			this.cmdCustomer.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdCustomer.AppearanceKey = "toolbarButton";
			this.cmdCustomer.Location = new System.Drawing.Point(726, 29);
			this.cmdCustomer.Name = "cmdCustomer";
			this.cmdCustomer.Shortcut = Wisej.Web.Shortcut.F9;
			this.cmdCustomer.Size = new System.Drawing.Size(121, 24);
			this.cmdCustomer.TabIndex = 1;
			this.cmdCustomer.Text = "Customer Screen";
			this.cmdCustomer.Click += new System.EventHandler(this.cmdCustomer_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.AppearanceKey = "toolbarButton";
			this.cmdSave.Location = new System.Drawing.Point(852, 29);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdSave.Size = new System.Drawing.Size(45, 24);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(364, 24);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(150, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.cmdSaveContinue_Click);
			// 
			// frmCustomerBills
			// 
			this.ClientSize = new System.Drawing.Size(922, 726);
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmCustomerBills";
			this.Text = "Customer Bills";
			this.Load += new System.EventHandler(this.frmCustomerBills_Load);
			this.Activated += new System.EventHandler(this.frmCustomerBills_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomerBills_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomerBills_KeyPress);
			this.Resize += new System.EventHandler(this.frmCustomerBills_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReceipt)).EndInit();
			this.fraReceipt.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraAcct)).EndInit();
			this.fraAcct.ResumeLayout(false);
			this.fraAcct.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkProrate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsFees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCustomer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdCustomer;
		private FCButton cmdSaveContinue;
		private FCButton cmdSave;
	}
}
