﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using TWAR0000.Receipting;

namespace TWAR0000.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<frmCustomerSearch>().As<IView<IAccountsReceivableCustomerSearchViewModel>>();
			builder.RegisterType<frmARPaymentStatus>().As<IView<IAccountsReceivablePaymentViewModel>>();
		}
	}
}
