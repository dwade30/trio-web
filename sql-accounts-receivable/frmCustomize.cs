﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWAR0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	public partial class frmCustomize : BaseForm
	{
		public frmCustomize()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTaxRate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTaxRate.AddControlArrayElement(lblTaxRate_5, 5);
			this.lblTaxRate.AddControlArrayElement(lblTaxRate_4, 4);
			this.lblTaxRate.AddControlArrayElement(lblTaxRate_3, 3);
			this.lblTaxRate.AddControlArrayElement(lblTaxRate_2, 2);
			this.lblTaxRate.AddControlArrayElement(lblTaxRate_0, 0);
			this.lblTaxRate.AddControlArrayElement(lblTaxRate_1, 1);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

            txtFlatRate.AllowOnlyNumericInput(true);
            txtPerDiemRate.AllowOnlyNumericInput(true);
            txtTaxRate.AllowOnlyNumericInput(true);

        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomize InstancePtr
		{
			get
			{
				return (frmCustomize)Sys.GetInstance(typeof(frmCustomize));
			}
		}

		protected frmCustomize _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsInfo = new clsDRWrapper();
		//private clsGridAccount vsSalesTaxReceivableGrid = new clsGridAccount();
		//private clsGridAccount vsSalesTaxPayableGrid = new clsGridAccount();
		//private clsGridAccount vsPrePayReceivableGrid = new clsGridAccount();
		clsDRWrapper rsInfo_AutoInitialized;

		private clsDRWrapper rsInfo
		{
			get
			{
				if (rsInfo_AutoInitialized == null)
				{
					rsInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsInfo_AutoInitialized;
			}
			set
			{
				rsInfo_AutoInitialized = value;
			}
		}

		private clsGridAccount vsSalesTaxReceivableGrid_AutoInitialized;

		private clsGridAccount vsSalesTaxReceivableGrid
		{
			get
			{
				if (vsSalesTaxReceivableGrid_AutoInitialized == null)
				{
					vsSalesTaxReceivableGrid_AutoInitialized = new clsGridAccount();
				}
				return vsSalesTaxReceivableGrid_AutoInitialized;
			}
			set
			{
				vsSalesTaxReceivableGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsSalesTaxPayableGrid_AutoInitialized;

		private clsGridAccount vsSalesTaxPayableGrid
		{
			get
			{
				if (vsSalesTaxPayableGrid_AutoInitialized == null)
				{
					vsSalesTaxPayableGrid_AutoInitialized = new clsGridAccount();
				}
				return vsSalesTaxPayableGrid_AutoInitialized;
			}
			set
			{
				vsSalesTaxPayableGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsPrePayReceivableGrid_AutoInitialized;

		private clsGridAccount vsPrePayReceivableGrid
		{
			get
			{
				if (vsPrePayReceivableGrid_AutoInitialized == null)
				{
					vsPrePayReceivableGrid_AutoInitialized = new clsGridAccount();
				}
				return vsPrePayReceivableGrid_AutoInitialized;
			}
			set
			{
				vsPrePayReceivableGrid_AutoInitialized = value;
			}
		}

		private void frmCustomize_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCustomize_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Strings.UCase(this.ActiveControl.GetName()) == "TXTSALESTAXRECEIVABLE")
			{
				modNewAccountBox.CheckFormKeyDown(txtSalesTaxReceivable, txtSalesTaxReceivable.Row, txtSalesTaxReceivable.Col, KeyCode, Shift, txtSalesTaxReceivable.EditSelStart, txtSalesTaxReceivable.EditText, txtSalesTaxReceivable.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "TXTSALESTAXPAYABLE")
			{
				modNewAccountBox.CheckFormKeyDown(txtSalesTaxPayable, txtSalesTaxPayable.Row, txtSalesTaxPayable.Col, KeyCode, Shift, txtSalesTaxPayable.EditSelStart, txtSalesTaxPayable.EditText, txtSalesTaxPayable.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "TXTPREPAYRECEIVABLE")
			{
				modNewAccountBox.CheckFormKeyDown(txtPrePayReceivable, txtPrePayReceivable.Row, txtPrePayReceivable.Col, KeyCode, Shift, txtPrePayReceivable.EditSelStart, txtPrePayReceivable.EditText, txtPrePayReceivable.EditSelLength);
			}
		}

		private void frmCustomize_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomize.FillStyle	= 0;
			//frmCustomize.ScaleWidth	= 9045;
			//frmCustomize.ScaleHeight	= 7515;
			//frmCustomize.LinkTopic	= "Form2";
			//frmCustomize.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// load state information into the combo box
			LoadPayees();
			LoadStates();
			FillDefaultInterestStartDateOptions();
			vsSalesTaxReceivableGrid.GRID7Light = txtSalesTaxReceivable;
			vsSalesTaxPayableGrid.GRID7Light = txtSalesTaxPayable;
			vsPrePayReceivableGrid.GRID7Light = txtPrePayReceivable;
			if (modGlobalConstants.Statics.gboolBD)
			{
				vsSalesTaxReceivableGrid.DefaultAccountType = "G";
				vsSalesTaxPayableGrid.DefaultAccountType = "G";
				vsPrePayReceivableGrid.DefaultAccountType = "G";
			}
			else
			{
				vsSalesTaxReceivableGrid.DefaultAccountType = "M";
				vsSalesTaxPayableGrid.DefaultAccountType = "M";
				vsPrePayReceivableGrid.DefaultAccountType = "M";
			}
			vsSalesTaxReceivableGrid.AccountCol = -1;
			vsSalesTaxPayableGrid.AccountCol = -1;
			vsPrePayReceivableGrid.AccountCol = -1;
			rsInfo.OpenRecordset("SELECT * FROM Customize");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("Basis")) == 365)
				{
					cmbBasis.SelectedIndex = 0;
				}
				else
				{
					cmbBasis.SelectedIndex = 1;
				}
				txtLabelAdjustment.Text = FCConvert.ToString(Conversion.Val(modARCalculations.GetARVariable("LabelLaserAdjustment")));
				txtBillAdjustment.Text = FCConvert.ToString(Conversion.Val(modARCalculations.GetARVariable("BillLaserAdjustment")));
				txtTaxRate.Text = FCConvert.ToString(rsInfo.Get_Fields_Double("TaxRate") * 100);
				txtPerDiemRate.Text = FCConvert.ToString(rsInfo.Get_Fields_Double("DefaultPerDiemRate") * 100);
				txtFlatRate.Text = Strings.Format(rsInfo.Get_Fields_Decimal("DefaultFlatRate"), "0.00");
				SetInterestStartDateCombo_2(FCConvert.ToInt16(rsInfo.Get_Fields_Int32("DefaultInterestStartDate")));
				txtCity.Text = FCConvert.ToString(rsInfo.Get_Fields_String("DefaultCity"));
				SetDefaultStateCombo_2(rsInfo.Get_Fields_String("DefaultState"));
				SetDefaultPayeeCombo_2(FCConvert.ToInt32(Conversion.Val(rsInfo.Get_Fields_Int32("DefaultPayeeID"))));
				if (modGlobalConstants.Statics.gboolBD)
				{
					chkAutoCreateJournal.Enabled = true;
					if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("AutoCreateJournal")))
					{
						chkAutoCreateJournal.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkAutoCreateJournal.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					chkAutoCreateJournal.Enabled = false;
					chkAutoCreateJournal.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtZip.Text = FCConvert.ToString(rsInfo.Get_Fields_String("DefaultZip"));
				if (FCConvert.ToString(rsInfo.Get_Fields_String("DefaultInterestMethod")) == "AP")
				{
					cmbInterestType.SelectedItem = "Auto Per Diem";
				}
				else if (rsInfo.Get_Fields_String("DefaultInterestMethod") == "AF")
				{
					cmbInterestType.SelectedItem = "Auto Flat Rate";
				}
				else if (rsInfo.Get_Fields_String("DefaultInterestMethod") == "DP")
				{
					cmbInterestType.SelectedItem = "Demand Per Diem";
				}
				else
				{
					cmbInterestType.SelectedItem = "Demand Flat Rate";
				}
				if (modGlobalConstants.Statics.gboolCR)
				{
					if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("ShowLastARAccountInCR")))
					{
						chkDefaultAccount.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDefaultAccount.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					chkDefaultAccount.CheckState = Wisej.Web.CheckState.Unchecked;
					chkDefaultAccount.Enabled = false;
				}
				if (modGlobalConstants.Statics.gboolBD)
				{
					if (FCConvert.ToString(rsInfo.Get_Fields_String("RecordSalesTax")) == "B")
					{
						cmbSalesTaxReceipt.SelectedIndex = 1;
					}
					else
					{
						cmbSalesTaxReceipt.SelectedIndex = 0;
					}
					txtSalesTaxPayable.TextMatrix(0, 0, FCConvert.ToString(rsInfo.Get_Fields_String("SalesTaxPayableAccount")));
					txtSalesTaxReceivable.TextMatrix(0, 0, FCConvert.ToString(rsInfo.Get_Fields_String("SalesTaxReceivableAccount")));
					txtPrePayReceivable.TextMatrix(0, 0, FCConvert.ToString(rsInfo.Get_Fields_String("PrePayReceivableAccount")));
				}
				else
				{
					lblSalesTaxPayable.Enabled = false;
					lblSalesTaxReceivable.Enabled = false;
					cmbSalesTaxReceipt.Enabled = false;
					txtSalesTaxPayable.Enabled = false;
					txtSalesTaxReceivable.Enabled = false;
					txtPrePayReceivable.Enabled = false;
					lblPrePayReceivable.Enabled = false;
					fraPrePayReceivable.Enabled = false;
				}
			}
			//FC:FINAL:SBE - #213 - increase input size
			txtSalesTaxPayable.ColWidth(0, FCConvert.ToInt32((txtSalesTaxPayable.WidthOriginal * 0.98)));
			txtSalesTaxReceivable.ColWidth(0, FCConvert.ToInt32((txtSalesTaxReceivable.WidthOriginal * 0.98)));
			txtPrePayReceivable.ColWidth(0, FCConvert.ToInt32((txtPrePayReceivable.WidthOriginal * 0.98)));
		}

		private void frmCustomize_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsRanges = new clsDRWrapper();
			txtCity.Focus();
			if (!Information.IsNumeric(txtBillAdjustment.Text))
			{
				MessageBox.Show("You must enter a numeric number in for your bill laser printer adjustment before you may proceed.", "Invalid Bill Line Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!Information.IsNumeric(txtLabelAdjustment.Text))
			{
				MessageBox.Show("You must enter a numeric number in for your label laser printer line adjustment before you may proceed.", "Invalid Label Line Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (modGlobalConstants.Statics.gboolBD)
			{
				if (Strings.InStr(1, txtSalesTaxReceivable.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(txtSalesTaxReceivable.TextMatrix(0, 0)) != "")
				{
					if (Strings.Left(txtSalesTaxReceivable.TextMatrix(0, 0), 1) != "G")
					{
						MessageBox.Show("The Sales Tax Receivable Account must be a G account.", "Invalid Sales Tax Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtSalesTaxReceivable.Focus();
						return;
					}
					else
					{
						if (modValidateAccount.AccountValidate(txtSalesTaxReceivable.TextMatrix(0, 0)))
						{
							rsRanges.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'A'", "TWBD0000.vb1");
							// TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [High] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(modBudgetaryAccounting.GetAccount(txtSalesTaxReceivable.TextMatrix(0, 0))) >= FCConvert.ToInt32(rsRanges.Get_Fields("Low")) && FCConvert.ToInt32(modBudgetaryAccounting.GetAccount(txtSalesTaxReceivable.TextMatrix(0, 0))) <= FCConvert.ToInt32(rsRanges.Get_Fields("High")))
							{
								// do nothing
							}
							else
							{
								MessageBox.Show("Your Sales Tax Receivable account must be an Asset Account", "Invalid Sales Tax Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								txtSalesTaxReceivable.Focus();
								return;
							}
						}
						else
						{
							MessageBox.Show("You must enter a valid acocunt for the Sales Tax Receivable Account.", "Invalid Sales Tax Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtSalesTaxReceivable.Focus();
							return;
						}
					}
				}
				if (Strings.InStr(1, txtSalesTaxPayable.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(txtSalesTaxPayable.TextMatrix(0, 0)) != "")
				{
					if (Strings.Left(txtSalesTaxPayable.TextMatrix(0, 0), 1) != "G")
					{
						MessageBox.Show("The Sales Tax Payable Account must be a G account.", "Invalid Sales Tax Payable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtSalesTaxPayable.Focus();
						return;
					}
					else
					{
						if (modValidateAccount.AccountValidate(txtSalesTaxPayable.TextMatrix(0, 0)))
						{
							rsRanges.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'L'", "TWBD0000.vb1");
							// TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [High] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(modBudgetaryAccounting.GetAccount(txtSalesTaxPayable.TextMatrix(0, 0))) >= FCConvert.ToInt32(rsRanges.Get_Fields("Low")) && FCConvert.ToInt32(modBudgetaryAccounting.GetAccount(txtSalesTaxPayable.TextMatrix(0, 0))) <= FCConvert.ToInt32(rsRanges.Get_Fields("High")))
							{
								// do nothing
							}
							else
							{
								MessageBox.Show("Your Sales Tax Payable account must be a Liability Account", "Invalid Sales Tax Payable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								txtSalesTaxPayable.Focus();
								return;
							}
						}
						else
						{
							MessageBox.Show("You must enter a valid acocunt for the Sales Tax Payable Account.", "Invalid Sales Tax Payable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtSalesTaxPayable.Focus();
							return;
						}
					}
				}
				if (Strings.InStr(1, txtPrePayReceivable.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(txtPrePayReceivable.TextMatrix(0, 0)) != "")
				{
					if (Strings.Left(txtPrePayReceivable.TextMatrix(0, 0), 1) != "G")
					{
						MessageBox.Show("The Pre Pay Receivable Account must be a G account.", "Invalid Pre Pay Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrePayReceivable.Focus();
						return;
					}
					else
					{
						if (modValidateAccount.AccountValidate(txtPrePayReceivable.TextMatrix(0, 0)))
						{
							rsRanges.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'A'", "TWBD0000.vb1");
							// TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [High] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(modBudgetaryAccounting.GetAccount(txtPrePayReceivable.TextMatrix(0, 0))) >= FCConvert.ToInt32(rsRanges.Get_Fields("Low")) && FCConvert.ToInt32(modBudgetaryAccounting.GetAccount(txtPrePayReceivable.TextMatrix(0, 0))) <= FCConvert.ToInt32(rsRanges.Get_Fields("High")))
							{
								// do nothing
							}
							else
							{
								MessageBox.Show("Your Pre Pay Receivable account must be an Asset Account", "Invalid Pre Pay Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								txtPrePayReceivable.Focus();
								return;
							}
						}
						else
						{
							MessageBox.Show("You must enter a valid acocunt for the Pre Pay Receivable Account.", "Invalid Pre Pay Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtPrePayReceivable.Focus();
							return;
						}
					}
				}
			}
			if (Information.IsNumeric(txtTaxRate.Text))
			{
				if (FCConvert.ToDecimal(txtTaxRate.Text) < 1 || FCConvert.ToDecimal(txtTaxRate.Text) > 100)
				{
					MessageBox.Show("You must enter a sales tax percentage that is between 1 and 100 before you may continue.", "Invalid Sales Tax Rate", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtTaxRate.Focus();
					return;
				}
			}
			else
			{
				MessageBox.Show("You must enter a sales tax percentage that is between 1 and 100 before you may continue.", "Invalid Sales Tax Rate", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtTaxRate.Focus();
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM Customize");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				rsInfo.Edit();
			}
			else
			{
				rsInfo.AddNew();
			}
			if (cmbBasis.SelectedIndex == 0)
			{
				rsInfo.Set_Fields("Basis", 365);
			}
			else
			{
				rsInfo.Set_Fields("Basis", 360);
			}
			modGlobal.UpdateARVariable("LabelLaserAdjustment", Conversion.Val(txtLabelAdjustment.Text));
			modGlobal.UpdateARVariable("BillLaserAdjustment", Conversion.Val(txtBillAdjustment.Text));
			rsInfo.Set_Fields("TaxRate", FCConvert.ToDouble(txtTaxRate.Text) / 100);
			modGlobal.Statics.dblSalesTax = rsInfo.Get_Fields_Double("TaxRate");
			rsInfo.Set_Fields("DefaultPerDiemRate", FCConvert.ToDouble(txtPerDiemRate.Text) / 100);
			modGlobal.Statics.dblDefaultPerDiem = rsInfo.Get_Fields_Double("DefaultPerDiemRate");
			if (chkAutoCreateJournal.CheckState == Wisej.Web.CheckState.Checked)
			{
				rsInfo.Set_Fields("AutoCreateJournal", true);
			}
			else
			{
				rsInfo.Set_Fields("AutoCreateJournal", false);
			}
			if (!Information.IsNumeric(txtFlatRate.Text))
			{
				rsInfo.Set_Fields("DefaultFlatRate", 0);
			}
			else
			{
				rsInfo.Set_Fields("DefaultFlatRate", FCConvert.ToDecimal(txtFlatRate.Text));
			}
			modGlobal.Statics.curDefaultFlatRate = FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("DefaultFlatRate"));
			if (cboInterestStartDate.SelectedIndex == 0)
			{
				rsInfo.Set_Fields("DefaultInterestStartDate", 30);
			}
			else if (cboInterestStartDate.SelectedIndex == 1)
			{
				rsInfo.Set_Fields("DefaultInterestStartDate", 60);
			}
			else if (cboInterestStartDate.SelectedIndex == 2)
			{
				rsInfo.Set_Fields("DefaultInterestStartDate", 90);
			}
			modGlobal.Statics.intDefaultInterestStartDate = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("DefaultInterestStartDate"));
			rsInfo.Set_Fields("DefaultCity", txtCity.Text);
			rsInfo.Set_Fields("DefaultState", Strings.Left(cboState.Text, 2));
			rsInfo.Set_Fields("DefaultZip", txtZip.Text);
			if (cboDefaultPayee.SelectedIndex >= 0)
			{
				rsInfo.Set_Fields("DefaultPayeeID", cboDefaultPayee.ItemData(cboDefaultPayee.SelectedIndex));
			}
			else
			{
				rsInfo.Set_Fields("DefaultPayeeID", 0);
			}
			if (cmbInterestType.SelectedItem == "Auto Per Diem")
			{
				rsInfo.Set_Fields("DefaultInterestMethod", "AP");
			}
			else if (cmbInterestType.SelectedItem == "Auto Flat Rate")
			{
				rsInfo.Set_Fields("DefaultInterestMethod", "AF");
			}
			else if (cmbInterestType.SelectedItem == "Demand Per Diem")
			{
				rsInfo.Set_Fields("DefaultInterestMethod", "DP");
			}
			else
			{
				rsInfo.Set_Fields("DefaultInterestMethod", "DF");
			}
			if (modGlobalConstants.Statics.gboolBD)
			{
				if (chkDefaultAccount.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsInfo.Set_Fields("ShowLastARAccountInCR", true);
				}
				else
				{
					rsInfo.Set_Fields("ShowLastARAccountInCR", false);
				}
				if (cmbSalesTaxReceipt.SelectedIndex == 1)
				{
					rsInfo.Set_Fields("RecordSalesTax", "B");
				}
				else
				{
					rsInfo.Set_Fields("RecordSalesTax", "R");
				}
				if (Strings.InStr(1, txtSalesTaxPayable.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(txtSalesTaxPayable.TextMatrix(0, 0)) != "")
				{
					rsInfo.Set_Fields("SalesTaxPayableAccount", Strings.Trim(txtSalesTaxPayable.TextMatrix(0, 0)));
				}
				else
				{
					rsInfo.Set_Fields("SalesTaxPayableAccount", "");
				}
				if (Strings.InStr(1, txtSalesTaxReceivable.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(txtSalesTaxReceivable.TextMatrix(0, 0)) != "")
				{
					rsInfo.Set_Fields("SalesTaxReceivableAccount", Strings.Trim(txtSalesTaxReceivable.TextMatrix(0, 0)));
				}
				else
				{
					rsInfo.Set_Fields("SalesTaxReceivableAccount", "");
				}
				if (Strings.InStr(1, txtPrePayReceivable.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(txtPrePayReceivable.TextMatrix(0, 0)) != "")
				{
					rsInfo.Set_Fields("PrePayReceivableAccount", Strings.Trim(txtPrePayReceivable.TextMatrix(0, 0)));
				}
				else
				{
					rsInfo.Set_Fields("PrePayReceivableAccount", "");
				}
			}
			else
			{
				rsInfo.Set_Fields("ShowLastARAccountInCR", false);
				rsInfo.Set_Fields("RecordSalesTax", "R");
				rsInfo.Set_Fields("SalesTaxPayableAccount", "");
				rsInfo.Set_Fields("SalesTaxReceivableAccount", "");
				rsInfo.Set_Fields("PrePayReceivableAccount", "");
			}
			rsInfo.Update();
			MessageBox.Show("Save Successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //FC:FINAL:PB - issue #2827: don't close form after save
            //Close();
            //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
            App.MainForm.ReloadNavigationMenu();
        }

		private void SetDefaultStateCombo_2(string strState)
		{
			SetDefaultStateCombo(ref strState);
		}

		private void SetDefaultStateCombo(ref string strState)
		{
			int counter;
			for (counter = 0; counter <= cboState.Items.Count - 1; counter++)
			{
				if (Strings.Left(cboState.Items[counter].ToString(), strState.Length) == strState)
				{
					cboState.SelectedIndex = counter;
					break;
				}
			}
		}
		// vbPorter upgrade warning: intPayeeID As int	OnWriteFCConvert.ToDouble(
		private void SetDefaultPayeeCombo_2(int intPayeeID)
		{
			SetDefaultPayeeCombo(ref intPayeeID);
		}

		private void SetDefaultPayeeCombo(ref int intPayeeID)
		{
			int counter;
			for (counter = 0; counter <= cboDefaultPayee.Items.Count - 1; counter++)
			{
				if (cboDefaultPayee.ItemData(counter) == intPayeeID)
				{
					cboDefaultPayee.SelectedIndex = counter;
					break;
				}
			}
		}

		private void LoadStates()
		{
			clsDRWrapper rsStateInfo = new clsDRWrapper();
			rsStateInfo.OpenRecordset("SELECT * FROM tblStates", "CentralData");
			if (rsStateInfo.EndOfFile() != true && rsStateInfo.BeginningOfFile() != true)
			{
				do
				{
					cboState.AddItem(rsStateInfo.Get_Fields_String("StateCode") + "  -  " + rsStateInfo.Get_Fields_String("Description"));
					rsStateInfo.MoveNext();
				}
				while (rsStateInfo.EndOfFile() != true);
			}
		}

		private void LoadPayees()
		{
			clsDRWrapper rsPayeeInfo = new clsDRWrapper();
			rsPayeeInfo.OpenRecordset("SELECT * FROM Payees ORDER BY Description", "TWAR0000.vb1");
			if (rsPayeeInfo.EndOfFile() != true && rsPayeeInfo.BeginningOfFile() != true)
			{
				do
				{
					cboDefaultPayee.AddItem(rsPayeeInfo.Get_Fields_String("Description"));
					cboDefaultPayee.ItemData(cboDefaultPayee.NewIndex, FCConvert.ToInt32(rsPayeeInfo.Get_Fields_Int32("PayeeID")));
					rsPayeeInfo.MoveNext();
				}
				while (rsPayeeInfo.EndOfFile() != true);
			}
		}

		private void FillDefaultInterestStartDateOptions()
		{
			cboInterestStartDate.AddItem("30 Days After Billing");
			cboInterestStartDate.ItemData(cboInterestStartDate.NewIndex, 30);
			cboInterestStartDate.AddItem("60 Days After Billing");
			cboInterestStartDate.ItemData(cboInterestStartDate.NewIndex, 60);
			cboInterestStartDate.AddItem("90 Days After Billing");
			cboInterestStartDate.ItemData(cboInterestStartDate.NewIndex, 90);
		}

		private void SetInterestStartDateCombo_2(short intDays)
		{
			SetInterestStartDateCombo(ref intDays);
		}

		private void SetInterestStartDateCombo(ref short intDays)
		{
			switch (intDays)
			{
				case 30:
					{
						cboInterestStartDate.SelectedIndex = 0;
						break;
					}
				case 60:
					{
						cboInterestStartDate.SelectedIndex = 1;
						break;
					}
				case 90:
					{
						cboInterestStartDate.SelectedIndex = 2;
						break;
					}
				default:
					{
						cboInterestStartDate.SelectedIndex = 0;
						break;
					}
			}
			//end switch
		}

		private void optSalesTaxBilling_CheckedChanged(object sender, System.EventArgs e)
		{
			txtSalesTaxReceivable.Enabled = true;
			txtSalesTaxPayable.Enabled = true;
			lblSalesTaxPayable.Enabled = true;
			lblSalesTaxReceivable.Enabled = true;
		}

		private void optSalesTaxReceipt_CheckedChanged(object sender, System.EventArgs e)
		{
			txtSalesTaxReceivable.TextMatrix(0, 0, "");
			txtSalesTaxPayable.TextMatrix(0, 0, "");
			txtSalesTaxReceivable.Enabled = false;
			txtSalesTaxPayable.Enabled = false;
			lblSalesTaxPayable.Enabled = false;
			lblSalesTaxReceivable.Enabled = false;
		}

		private void txtFlatRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFlatRate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFlatRate.Text))
			{
				MessageBox.Show("You may only enter a numeric value in this field.", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		private void txtPerDiemRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPerDiemRate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtPerDiemRate.Text))
			{
				MessageBox.Show("You may only enter a numeric value in this field.", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		private void txtTaxRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTaxRate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtTaxRate.Text))
			{
				MessageBox.Show("You may only enter a numeric value in this field.", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}

		private void cmbSalesTaxReceipt_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbSalesTaxReceipt.SelectedIndex == 0)
			{
				optSalesTaxReceipt_CheckedChanged(sender, e);
			}
			else
			{
				optSalesTaxBilling_CheckedChanged(sender, e);
			}
		}
	}
}
