﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedDataAccess;
using SharedDataAccess.AccountsReceivable;
using SharedDataAccess.CentralData;
using Xunit;

namespace SharedDataAccessTests
{
	public class CustomerQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingCustomer()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testBillMessage = "Test BILL MESSAGE";
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;

				accountsReceivableContext.CustomerMasters.Add(new CustomerMaster
				{
					BillMessage = testBillMessage,
					Comment = testComment,
					CustomerId = testAccountNumber,
					Deleted = true
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CustomerQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerSearchCriteria
				{
					AccountNumber = testAccountNumber,
					deletedOption = DeletedSelection.All
				}).FirstOrDefault();

				Assert.True(result.BillMessage == testBillMessage);
				Assert.True(result.Comment == testComment);
				Assert.True(result.CustomerId == testAccountNumber);
			}
		}

		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingCustomerNotMatchingDeletedOptions()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testBillMessage = "Test BILL MESSAGE";
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;

				accountsReceivableContext.CustomerMasters.Add(new CustomerMaster
				{
					BillMessage = testBillMessage,
					Comment = testComment,
					CustomerId = testAccountNumber,
					Deleted = true
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CustomerQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerSearchCriteria
				{
					AccountNumber = testAccountNumber,
					deletedOption = DeletedSelection.NotDeleted
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingCustomertMatchingDeletedOptions()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testBillMessage = "Test BILL MESSAGE";
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;

				accountsReceivableContext.CustomerMasters.Add(new CustomerMaster
				{
					BillMessage = testBillMessage,
					Comment = testComment,
					CustomerId = testAccountNumber,
					Deleted = true
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CustomerQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerSearchCriteria
				{
					AccountNumber = testAccountNumber,
					deletedOption = DeletedSelection.OnlyDeleted
				}).FirstOrDefault();

				Assert.True(result.BillMessage == testBillMessage);
				Assert.True(result.Comment == testComment);
				Assert.True(result.CustomerId == testAccountNumber);
			}
		}

		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingId()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testBillMessage = "Test BILL MESSAGE";
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;

				accountsReceivableContext.CustomerMasters.Add(new CustomerMaster
				{
					BillMessage = testBillMessage,
					Comment = testComment,
					CustomerId = testAccountNumber,
					Deleted = true
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CustomerQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerSearchCriteria
				{
					Id = accountsReceivableContext.CustomerMasters.FirstOrDefault()?.Id ?? 0,
					deletedOption = DeletedSelection.All
				}).FirstOrDefault();

				Assert.True(result.BillMessage == testBillMessage);
				Assert.True(result.Comment == testComment);
				Assert.True(result.CustomerId == testAccountNumber);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
