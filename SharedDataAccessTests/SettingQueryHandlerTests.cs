﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedDataAccess;
using SharedDataAccess.CentralData;
using Xunit;

namespace SharedDataAccessTests
{
	public class SettingQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectSettingInformationForExistingSetting()
		{
			var globalSettings = new cGlobalSettings();
			var userInformation = new UserInformation();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			string testOwnerId = "";
			string testOwnerType = "";
			string testSettingName = "SETTING";
			string testSettingValue = "VALUE";
			string testSettingType = "SETTINGTYPE";

			using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
			{
				centralDataContext.Settings.Add(new Setting
				{
					OwnerType = testOwnerType,
					Owner = testOwnerId,
					SettingName = testSettingName,
					SettingType = testSettingType,
					SettingValue = testSettingValue
				});

				centralDataContext.SaveChanges();

				trioContextFactory.GetCentralDataContext().Returns(centralDataContext);
				globalSettings.MachineOwnerID = testOwnerId;

				var service = new SettingQueryHandler(trioContextFactory, globalSettings, userInformation);

				var result = service.ExecuteQuery(new SettingSearchCriteria
				{
					OwnerType = SettingOwnerType.Global,
					SettingType = testSettingType,
					SettingName = testSettingName
				}).FirstOrDefault();

				Assert.True(result.Owner == testOwnerId);
				Assert.True(result.OwnerType == testOwnerType);
				Assert.True(result.SettingName == testSettingName);
				Assert.True(result.SettingType == testSettingType);
				Assert.True(result.SettingValue == testSettingValue);

			}

			
		}

		[Fact]
		public void ReturnCorrectSettingInformationForExistingSettingNoDefaultValues()
		{
			var globalSettings = new cGlobalSettings();
			var userInformation = new UserInformation();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			string testOwnerId = "TEST";
			SettingOwnerType testOwnerType = SettingOwnerType.Machine;
			string testSettingName = "SETTING";
			string testSettingValue = "VALUE";
			string testSettingType = "SETTINGTYPE";

			using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
			{
				centralDataContext.Settings.Add(new Setting
				{
					OwnerType = "Machine",
					Owner = testOwnerId,
					SettingName = testSettingName,
					SettingType = testSettingType,
					SettingValue = testSettingValue
				});

				centralDataContext.SaveChanges();

				trioContextFactory.GetCentralDataContext().Returns(centralDataContext);
				globalSettings.MachineOwnerID = testOwnerId;

				var service = new SettingQueryHandler(trioContextFactory, globalSettings, userInformation);

				var result = service.ExecuteQuery(new SettingSearchCriteria
				{
					SettingType = testSettingType,
					SettingName = testSettingName,
					OwnerType = testOwnerType,
					Owner = testOwnerId
				}).FirstOrDefault();

				Assert.True(result.Owner == testOwnerId);
				Assert.True(result.OwnerType == "Machine");
				Assert.True(result.SettingName == testSettingName);
				Assert.True(result.SettingType == testSettingType);
				Assert.True(result.SettingValue == testSettingValue);

			}


		}

		[Fact]
		public void ReturnCorrectSettingInformationForNoMatchSetting()
		{
			var globalSettings = new cGlobalSettings();
			var userInformation = new UserInformation();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			string testOwnerId = "TEST";
			string testOwnerType = "TYPE";
			string testSettingName = "SETTING";
			string testSettingValue = "VALUE";
			string testSettingType = "SETTINGTYPE";

			using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
			{
				centralDataContext.Settings.Add(new Setting
				{
					OwnerType = testOwnerType,
					Owner = testOwnerId,
					SettingName = "NOMATCH",
					SettingType = testSettingType,
					SettingValue = testSettingValue
				});

				centralDataContext.SaveChanges();

				trioContextFactory.GetCentralDataContext().Returns(centralDataContext);
				globalSettings.MachineOwnerID = testOwnerId;

				var service = new SettingQueryHandler(trioContextFactory, globalSettings, userInformation);

				var result = service.ExecuteQuery(new SettingSearchCriteria
				{
					SettingType = testSettingType,
					SettingName = testSettingName
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
