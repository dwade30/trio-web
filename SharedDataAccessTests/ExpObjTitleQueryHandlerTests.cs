﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Models;
using SharedDataAccess;
using SharedDataAccess.Budgetary;
using Xunit;

namespace SharedDataAccessTests
{
	public class ExpObjTitleQueryHandlerTests
	{
		[Fact]
		public void ExecuteQueryReturnsCorrectExpenseAccount()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "10",
					Object = "000",
					ShortDescription = "Exp Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new ExpObjTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new ExpObjTitleSearchCriteria
				{
					Expense = "10",
					Object = "000"
				});
				Assert.True(result.Count() == 1);
				Assert.True(result.First().ShortDescription == "Exp Short");
				Assert.True(result.First().Expense == "10");
				Assert.True(result.First().Object == "000");
			}
		}

		[Fact]
		public void ExecuteQueryReturnsAllWithNoParameters()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "10",
					Object = "000",
					ShortDescription = "Exp Short"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "10",
					Object = "100",
					ShortDescription = "Obj Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new ExpObjTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new ExpObjTitleSearchCriteria
				{
					Expense = "",
					Object = ""
				});
				Assert.True(result.Count() == 2);
			}
		}

		[Fact]
		public void ExecuteQueryReturnsEmptyListOnNoMatch()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "10",
					Object = "000",
					ShortDescription = "Exp Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new ExpObjTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new ExpObjTitleSearchCriteria
				{
					Expense = "10",
					Object = "100"
				});
				Assert.True(result.Count() == 0);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
