﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Models;
using SharedDataAccess;
using SharedDataAccess.Budgetary;
using Xunit;

namespace SharedDataAccessTests
{
	public class RevTitleQueryHandlerTests
	{
		[Fact]
		public void ExecuteQueryReturnsCorrectRevenueAccount()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "99",
					ShortDescription = "Rev Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new RevTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new RevTitleSearchCriteria
				{
					Department = "10",
					Division = "100",
					Revenue = "99"
				});
				Assert.True(result.Count() == 1);
				Assert.True(result.First().ShortDescription == "Rev Short");
				Assert.True(result.First().Department == "10");
				Assert.True(result.First().Division == "100");
				Assert.True(result.First().Revenue == "99");
			}
		}

		[Fact]
		public void ExecuteQueryReturnsAllWithNoCriteria()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "99",
					ShortDescription = "Rev Short"
				});

				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "88",
					ShortDescription = "Rev Short 2"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new RevTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new RevTitleSearchCriteria
				{
					Department = "",
					Division = "",
					Revenue = ""
				});
				Assert.True(result.Count() == 2);
			}
		}

		[Fact]
		public void ExecuteQueryReturnsEmptyListOnNoMatch()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "99",
					ShortDescription = "Rev Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new RevTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new RevTitleSearchCriteria
				{
					Department = "10",
					Division = "200",
					Revenue = "99"
				});
				Assert.True(result.Count() == 0);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
