﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Receipting;
using SharedDataAccess;
using SharedDataAccess.AccountsReceivable;
using Xunit;

namespace SharedDataAccessTests
{
	public class FinalizeAccountsReceivableTransactionsCommandHandlerTests
	{
		[Fact]
		public void SavePaymentForExistingBill()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();
			
			int testAccountNumber = 10;
			string testName = "Test Name";

			using (var accountsReceivableDataContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					FlatRate = 0,
					IntRate = .2,
					InterestMethod = "AP",
					IntStart = DateTime.Today.AddMonths(-2),
				};

				var testCustomer = new CustomerPaymentStatusResult
				{
					CustomerId = testAccountNumber,
					FullName = testName,
					PartyId = 10
				};

				accountsReceivableDataContext.RateKeys.Add(testRateKey);

				accountsReceivableDataContext.CustomerMasters.Add(new CustomerMaster
				{
					CustomerId = testAccountNumber,
				});

				accountsReceivableDataContext.SaveChanges();

				var testBill = new Bill
				{
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					BillStatus = "A",
					Bname = testName,
					ActualAccountNumber = testAccountNumber,
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					IntAdded = 0,
					PrinOwed = 200,
					TaxOwed = 100,
					PrinPaid = 0,
					TaxPaid = 0,
					IntPaid = 0,
					IntPaidDate = DateTime.Today.AddMonths(-1),
					Payments = new List<PaymentRec>
					{

					}
				};

				accountsReceivableDataContext.Bills.Add(testBill);

				accountsReceivableDataContext.SaveChanges();

				var inMemoryBill = new ARBill
				{
					BillNumber = testBill.BillNumber,
					BillStatus = testBill.BillStatus,
					Bname = testBill.Bname,
					PrinOwed = testBill.PrinOwed,
					TaxOwed = testBill.TaxOwed,
					IntAdded = testBill.IntAdded,
					IntPaidDate = testBill.IntPaidDate,
					PendingPrincipalAmount = 100,
					PendingTaxAmount = 50,
					PendingInterestAmount = 0,
					Id = testBill.Id,
					BalanceRemaining = 150,
					Payments = new List<ARPayment>
					{
						new ARPayment
						{
							ChargedInterestRecord = null,
							Interest = 0,
							Tax = 50,
							Principal = 100,
							Pending = true,
							ActualSystemDate = DateTime.Today,
							BillId = testBill.Id,
							BudgetaryAccountNumber = "",
							CashDrawer = "Y",
							Code = "P",
							Comments = "Test Comment",
							DailyCloseOut = 0,
							EffectiveInterestDate = DateTime.Today,
							GeneralLedger = "Y",
							IsReversal = false,
							PaidBy = testName,
							PendingId = Guid.NewGuid(),
							ReceiptId = 0,
							RecordedTransactionDate = DateTime.Today,
							Reference = "",
							Teller = ""
						}
					}
				};

				var testCorrelationId = Guid.NewGuid();

				var cashReceiptsTransaction = new AccountsReceivableTransactionBase
				{
					ActualDateTime = DateTime.Today,
					EffectiveDateTime = DateTime.Today,
					RecordedTransactionDate = DateTime.Today,
					AccountNumber = testAccountNumber,
					Comment = "Test Comment",
					Controls = new List<ControlItem>
					{
						new ControlItem
						{
							Name = "Control1",
							Description = "Permit #",
							Value = "1092"
						}
					},
					CorrelationIdentifier = testCorrelationId,
					Id = Guid.NewGuid(),
					PayerName = testName,
					PayerPartyId = testCustomer.PartyId,
					AffectCashDrawer = true,
					AffectCash = true,
					Code = "P",
					BudgetaryAccountNumber = "",
					DefaultCashAccount = "",
					EPmtAccountID = "",
					Reference = "",
					TransactionTypeCode = "97",
					TransactionDetails = new AccountsReceivableTransaction
					{
						Id = Guid.NewGuid(),
						CorrelationId = testCorrelationId,
						AccountNumber = testCustomer.CustomerId,
						Bills = new List<ARBill>
						{
							inMemoryBill
						}
					},
					TransactionTypeDescription = "TEST BILL TYPE"
				};

				cashReceiptsTransaction.AddTransactionAmounts(new List<TransactionAmountItem>
				{
					new TransactionAmountItem
					{
						Name = "Principal",
						Description = "Principal",
						Total = inMemoryBill.PendingPrincipalAmount
					},
					new TransactionAmountItem
					{
						Name = "Interest",
						Description = "Interest",
						Total = inMemoryBill.PendingInterestAmount
					},
					new TransactionAmountItem
					{
						Name = "Sales Tax",
						Description = "Sales Tax",
						Total = inMemoryBill.PendingTaxAmount
					}
				});


				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableDataContext);

				customerPartyQueryHandler.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(new List<CustomerPaymentStatusResult>
				{
					testCustomer
				});

				var service = new FinalizeAccountsReceivableTransactionsCommandHandler(trioContextFactory, customerPartyQueryHandler);

				var result = service.Handle(new FinalizeTransactions<AccountsReceivableTransactionBase>
				{
					Id = Guid.NewGuid(),
					CorrelationIdentifier = testCorrelationId,
					ReceiptId = 0,
					TellerId = "617",
					Transactions = new List<AccountsReceivableTransactionBase>
					{
						cashReceiptsTransaction
					}
				}, new CancellationToken());

				Assert.True(result.Result);

				var bill = accountsReceivableDataContext.Bills.FirstOrDefault();
				Assert.True(bill.PrinPaid == inMemoryBill.PendingPrincipalAmount);
				Assert.True(bill.TaxPaid == inMemoryBill.PendingTaxAmount);
				Assert.True(bill.IntPaid == inMemoryBill.PendingInterestAmount);
				Assert.True(bill.IntAdded == 0);
				Assert.True(bill.BillStatus == "A");
				Assert.True(bill.Payments.Count == 1);

				var payment = bill.Payments.FirstOrDefault();
				Assert.True(payment.Interest == 0);
				Assert.True(payment.Tax == bill.TaxPaid);
				Assert.True(payment.Principal == bill.PrinPaid);
				Assert.True(payment.ActualAccountNumber == testAccountNumber);
				Assert.True(payment.BillKey == testBill.Id);
				Assert.True(payment.BillNumber == testBill.BillNumber);
				Assert.True(payment.DailyCloseOut == 0);
				Assert.True(payment.Chgintdate == null);
				Assert.True(payment.ActualSystemDate == DateTime.Today);
				Assert.True(payment.CashDrawer == "Y");
				Assert.True(payment.Code == "P");
				Assert.True(payment.GeneralLedger == "Y");
				Assert.True(payment.Comments == "Test Comment");
				Assert.True(payment.Teller == "617");
			}
		}

		[Fact]
		public void SavePaymentForExistingBillWithChargedInterest()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();

			int testAccountNumber = 10;
			string testName = "Test Name";

			using (var accountsReceivableDataContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					FlatRate = 0,
					IntRate = .2,
					InterestMethod = "AP",
					IntStart = DateTime.Today.AddMonths(-2),
				};

				var testCustomer = new CustomerPaymentStatusResult
				{
					CustomerId = testAccountNumber,
					FullName = testName,
					PartyId = 10
				};

				accountsReceivableDataContext.RateKeys.Add(testRateKey);

				accountsReceivableDataContext.CustomerMasters.Add(new CustomerMaster
				{
					CustomerId = testAccountNumber,
				});

				accountsReceivableDataContext.SaveChanges();

				var testBill = new Bill
				{
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					BillStatus = "A",
					Bname = testName,
					ActualAccountNumber = testAccountNumber,
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					IntAdded = 0,
					PrinOwed = 200,
					TaxOwed = 100,
					PrinPaid = 0,
					TaxPaid = 0,
					IntPaid = 0,
					IntPaidDate = DateTime.Today.AddMonths(-1),
					Payments = new List<PaymentRec>
					{

					}
				};

				accountsReceivableDataContext.Bills.Add(testBill);

				accountsReceivableDataContext.SaveChanges();

				var inMemoryBill = new ARBill
				{
					BillNumber = testBill.BillNumber,
					BillStatus = testBill.BillStatus,
					Bname = testBill.Bname,
					PrinOwed = testBill.PrinOwed,
					TaxOwed = testBill.TaxOwed,
					IntAdded = testBill.IntAdded,
					IntPaidDate = testBill.IntPaidDate,
					PendingPrincipalAmount = 100,
					PendingTaxAmount = 50,
					PendingInterestAmount = 20,
					PendingChargedInterest = - 20,
					Id = testBill.Id,
					BalanceRemaining = 150,
					Payments = new List<ARPayment>
					{
						new ARPayment
						{
							ChargedInterestRecord = new ARPayment
							{
								Interest = -20,
								Tax = 0,
								Principal = 0,
								Pending = true,
								ActualSystemDate = DateTime.Today,
								BillId = testBill.Id,
								BudgetaryAccountNumber = "",
								CashDrawer = "Y",
								Code = "I",
								Comments = "",
								DailyCloseOut = 0,
								EffectiveInterestDate = DateTime.Today,
								ChargedInterestDate = DateTime.Today,
								GeneralLedger = "Y",
								IsReversal = false,
								PaidBy = testName,
								PendingId = Guid.NewGuid(),
								ReceiptId = 0,
								RecordedTransactionDate = DateTime.Today,
								Reference = "CHGINT",
								Teller = ""
							},
							Interest = 20,
							Tax = 50,
							Principal = 100,
							Pending = true,
							ActualSystemDate = DateTime.Today,
							BillId = testBill.Id,
							BudgetaryAccountNumber = "",
							CashDrawer = "Y",
							Code = "P",
							Comments = "Test Comment",
							DailyCloseOut = 0,
							EffectiveInterestDate = DateTime.Today,
							GeneralLedger = "Y",
							IsReversal = false,
							PaidBy = testName,
							PendingId = Guid.NewGuid(),
							ReceiptId = 0,
							RecordedTransactionDate = DateTime.Today,
							Reference = "",
							Teller = ""
						}
					}
				};

				var testCorrelationId = Guid.NewGuid();

				var cashReceiptsTransaction = new AccountsReceivableTransactionBase
				{
					ActualDateTime = DateTime.Today,
					EffectiveDateTime = DateTime.Today,
					RecordedTransactionDate = DateTime.Today,
					AccountNumber = testAccountNumber,
					Comment = "Test Comment",
					Controls = new List<ControlItem>
					{
						new ControlItem
						{
							Name = "Control1",
							Description = "Permit #",
							Value = "1092"
						}
					},
					CorrelationIdentifier = testCorrelationId,
					Id = Guid.NewGuid(),
					PayerName = testName,
					PayerPartyId = testCustomer.PartyId,
					AffectCashDrawer = true,
					AffectCash = true,
					Code = "P",
					BudgetaryAccountNumber = "",
					DefaultCashAccount = "",
					EPmtAccountID = "",
					Reference = "",
					TransactionTypeCode = "97",
					TransactionDetails = new AccountsReceivableTransaction
					{
						Id = Guid.NewGuid(),
						CorrelationId = testCorrelationId,
						AccountNumber = testCustomer.CustomerId,
						Bills = new List<ARBill>
						{
							inMemoryBill
						}
					},
					TransactionTypeDescription = "TEST BILL TYPE"
				};

				cashReceiptsTransaction.AddTransactionAmounts(new List<TransactionAmountItem>
				{
					new TransactionAmountItem
					{
						Name = "Principal",
						Description = "Principal",
						Total = inMemoryBill.PendingPrincipalAmount
					},
					new TransactionAmountItem
					{
						Name = "Interest",
						Description = "Interest",
						Total = inMemoryBill.PendingInterestAmount
					},
					new TransactionAmountItem
					{
						Name = "Sales Tax",
						Description = "Sales Tax",
						Total = inMemoryBill.PendingTaxAmount
					}
				});


				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableDataContext);

				customerPartyQueryHandler.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(new List<CustomerPaymentStatusResult>
				{
					testCustomer
				});

				var service = new FinalizeAccountsReceivableTransactionsCommandHandler(trioContextFactory, customerPartyQueryHandler);

				var result = service.Handle(new FinalizeTransactions<AccountsReceivableTransactionBase>
				{
					Id = Guid.NewGuid(),
					CorrelationIdentifier = testCorrelationId,
					ReceiptId = 0,
					TellerId = "617",
					Transactions = new List<AccountsReceivableTransactionBase>
					{
						cashReceiptsTransaction
					}
				}, new CancellationToken());

				Assert.True(result.Result);

				var bill = accountsReceivableDataContext.Bills.FirstOrDefault();
				Assert.True(bill.PrinPaid == inMemoryBill.PendingPrincipalAmount);
				Assert.True(bill.TaxPaid == inMemoryBill.PendingTaxAmount);
				Assert.True(bill.IntPaid == inMemoryBill.PendingInterestAmount);
				Assert.True(bill.IntAdded == -20);
				Assert.True(bill.BillStatus == "A");
				Assert.True(bill.Payments.Count == 2);

				var payment = bill.Payments.FirstOrDefault(x => x.Reference != "CHGINT");
				Assert.True(payment.Interest == bill.IntPaid);
				Assert.True(payment.Tax == bill.TaxPaid);
				Assert.True(payment.Principal == bill.PrinPaid);
				Assert.True(payment.ActualAccountNumber == testAccountNumber);
				Assert.True(payment.BillKey == testBill.Id);
				Assert.True(payment.BillNumber == testBill.BillNumber);
				Assert.True(payment.DailyCloseOut == 0);
				Assert.True(payment.Chgintdate == null);
				Assert.True(payment.ActualSystemDate == DateTime.Today);
				Assert.True(payment.CashDrawer == "Y");
				Assert.True(payment.Code == "P");
				Assert.True(payment.GeneralLedger == "Y");
				Assert.True(payment.Comments == "Test Comment");
				Assert.True(payment.Teller == "617");

				var chargedInt = bill.Payments.FirstOrDefault(x => x.Reference == "CHGINT");
				Assert.True(chargedInt.Interest == bill.IntAdded);
				Assert.True(chargedInt.Tax == 0);
				Assert.True(chargedInt.Principal == 0);
				Assert.True(chargedInt.ActualAccountNumber == testAccountNumber);
				Assert.True(chargedInt.BillKey == testBill.Id);
				Assert.True(chargedInt.BillNumber == testBill.BillNumber);
				Assert.True(chargedInt.DailyCloseOut == 0);
				Assert.True(chargedInt.Chgintdate == DateTime.Today);
				Assert.True(chargedInt.ActualSystemDate == DateTime.Today);
				Assert.True(chargedInt.CashDrawer == "Y");
				Assert.True(chargedInt.Code == "I");
				Assert.True(chargedInt.GeneralLedger == "Y");
				Assert.True(chargedInt.Comments == "");
				Assert.True(chargedInt.Teller == "617");
			}
		}

		[Fact]
		public void SavePaymentForPrePayBill()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
			var customerPartyQueryHandler = Substitute
				.For<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>>>();

			int testAccountNumber = 10;
			string testName = "Test Name";

			using (var accountsReceivableDataContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					FlatRate = 0,
					IntRate = .2,
					InterestMethod = "AP",
					IntStart = DateTime.Today.AddMonths(-2),
				};

				var testCustomer = new CustomerPaymentStatusResult
				{
					CustomerId = testAccountNumber,
					FullName = testName,
					PartyId = 10
				};

				accountsReceivableDataContext.RateKeys.Add(testRateKey);

				accountsReceivableDataContext.CustomerMasters.Add(new CustomerMaster
				{
					CustomerId = testAccountNumber,
				});

				accountsReceivableDataContext.SaveChanges();

				var inMemoryBill = new ARBill
				{
					BillNumber = 0,
					BillStatus = "A",
					Bname = testName,
					PrinOwed = 0,
					TaxOwed = 0,
					IntAdded = 0,
					IntPaidDate = null,
					PendingPrincipalAmount = 100,
					PendingTaxAmount = 0,
					PendingInterestAmount = 0,
					Id = 0,
					Pending = true,
					BalanceRemaining = 0,
					Payments = new List<ARPayment>
					{
						new ARPayment
						{
							ChargedInterestRecord = null,
							Interest = 0,
							Tax = 0,
							Principal = 100,
							Pending = true,
							ActualSystemDate = DateTime.Today,
							BillId = 0,
							BudgetaryAccountNumber = "",
							CashDrawer = "Y",
							Code = "Y",
							Comments = "Test Comment",
							DailyCloseOut = 0,
							EffectiveInterestDate = DateTime.Today,
							GeneralLedger = "Y",
							IsReversal = false,
							PaidBy = testName,
							PendingId = Guid.NewGuid(),
							ReceiptId = 0,
							RecordedTransactionDate = DateTime.Today,
							Reference = "",
							Teller = ""
						}
					}
				};

				var testCorrelationId = Guid.NewGuid();

				var cashReceiptsTransaction = new AccountsReceivableTransactionBase
				{
					ActualDateTime = DateTime.Today,
					EffectiveDateTime = DateTime.Today,
					RecordedTransactionDate = DateTime.Today,
					AccountNumber = testAccountNumber,
					Comment = "Test Comment",
					Controls = new List<ControlItem>
					{
						
					},
					CorrelationIdentifier = testCorrelationId,
					Id = Guid.NewGuid(),
					PayerName = testName,
					PayerPartyId = testCustomer.PartyId,
					AffectCashDrawer = true,
					AffectCash = true,
					Code = "Y",
					BudgetaryAccountNumber = "",
					DefaultCashAccount = "",
					EPmtAccountID = "",
					Reference = "",
					TransactionTypeCode = "97",
					TransactionDetails = new AccountsReceivableTransaction
					{
						Id = Guid.NewGuid(),
						CorrelationId = testCorrelationId,
						AccountNumber = testCustomer.CustomerId,
						Bills = new List<ARBill>
						{
							inMemoryBill
						}
					},
					TransactionTypeDescription = "TEST BILL TYPE"
				};

				cashReceiptsTransaction.AddTransactionAmounts(new List<TransactionAmountItem>
				{
					new TransactionAmountItem
					{
						Name = "Principal",
						Description = "Principal",
						Total = inMemoryBill.PendingPrincipalAmount
					},
					new TransactionAmountItem
					{
						Name = "Interest",
						Description = "Interest",
						Total = inMemoryBill.PendingInterestAmount
					},
					new TransactionAmountItem
					{
						Name = "Sales Tax",
						Description = "Sales Tax",
						Total = inMemoryBill.PendingTaxAmount
					}
				});


				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableDataContext);

				customerPartyQueryHandler.ExecuteQuery(Arg.Is<CustomerPartySearchCriteria>(x => x.AccountNumber == testAccountNumber)).Returns(new List<CustomerPaymentStatusResult>
				{
					testCustomer
				});

				var service = new FinalizeAccountsReceivableTransactionsCommandHandler(trioContextFactory, customerPartyQueryHandler);

				var result = service.Handle(new FinalizeTransactions<AccountsReceivableTransactionBase>
				{
					Id = Guid.NewGuid(),
					CorrelationIdentifier = testCorrelationId,
					ReceiptId = 0,
					TellerId = "617",
					Transactions = new List<AccountsReceivableTransactionBase>
					{
						cashReceiptsTransaction
					}
				}, new CancellationToken());

				Assert.True(result.Result);

				var bill = accountsReceivableDataContext.Bills.FirstOrDefault();
				Assert.True(bill.PrinPaid == inMemoryBill.PendingPrincipalAmount);
				Assert.True(bill.TaxPaid == inMemoryBill.PendingTaxAmount);
				Assert.True(bill.IntPaid == inMemoryBill.PendingInterestAmount);
				Assert.True(bill.IntAdded == 0);
				Assert.True(bill.BillStatus == "A");
				Assert.True(bill.Payments.Count == 1);

				var payment = bill.Payments.FirstOrDefault();
				Assert.True(payment.Interest == 0);
				Assert.True(payment.Tax == bill.TaxPaid);
				Assert.True(payment.Principal == bill.PrinPaid);
				Assert.True(payment.ActualAccountNumber == testAccountNumber);
				Assert.True(payment.BillKey == bill.Id);
				Assert.True(payment.BillNumber == bill.BillNumber);
				Assert.True(payment.DailyCloseOut == 0);
				Assert.True(payment.Chgintdate == null);
				Assert.True(payment.ActualSystemDate == DateTime.Today);
				Assert.True(payment.CashDrawer == "Y");
				Assert.True(payment.Code == "Y");
				Assert.True(payment.GeneralLedger == "Y");
				Assert.True(payment.Comments == "Test Comment");
				Assert.True(payment.Teller == "617");
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
