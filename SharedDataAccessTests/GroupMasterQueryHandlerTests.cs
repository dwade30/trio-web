﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedDataAccess;
using SharedDataAccess.CentralData;
using Xunit;

namespace SharedDataAccessTests
{
	public class GroupMasterQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectGroupMasterInformationForExistingGroup()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testGroupNumber = 1;
			string testComment = "Test Comment";
			int testAccountnumber  = 20;
			string testModule = "AR";
			
			using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
			{
				centralDataContext.GroupMasters.Add(new GroupMaster
				{
					GroupNumber = testGroupNumber,
					Comment = testComment,
					DateCreated = DateTime.Today
				});

				centralDataContext.SaveChanges();

				trioContextFactory.GetCentralDataContext().Returns(centralDataContext);
				
				var service = new GroupMasterQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new GroupMasterSearchCriteria
				{
					GroupNumber = testGroupNumber
				}).FirstOrDefault();

				Assert.True(result.GroupNumber == testGroupNumber);
				Assert.True(result.Comment == testComment);
			}
		}

		[Fact]
		public void ReturnNullForNotExistingGroup()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testGroupNumber = 1;
			string testComment = "Test Comment";
			int testAccountnumber = 20;
			string testModule = "AR";

			using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
			{
				centralDataContext.GroupMasters.Add(new GroupMaster
				{
					GroupNumber = testGroupNumber,
					Comment = testComment,
					DateCreated = DateTime.Today
				});

				centralDataContext.SaveChanges();

				trioContextFactory.GetCentralDataContext().Returns(centralDataContext);

				var service = new GroupMasterQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new GroupMasterSearchCriteria
				{
					GroupNumber = testGroupNumber + 1
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		[Fact]
		public void ReturnCorrectGroupMasterInformationForExistingModuleAccount()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testGroupNumber = 1;
			string testComment = "Test Comment";
			int testAccountnumber = 20;
			string testModule = "AR";

			using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
			{
				centralDataContext.GroupMasters.Add(new GroupMaster
				{
					GroupNumber = testGroupNumber,
					Comment = testComment,
					DateCreated = DateTime.Today
				});

				centralDataContext.SaveChanges();

				centralDataContext.ModuleAssociations.Add(new ModuleAssociation
				{
					GroupNumber = centralDataContext.GroupMasters.FirstOrDefault().ID,
					Account = testAccountnumber,
					Module = testModule,
				});

				centralDataContext.SaveChanges();

				trioContextFactory.GetCentralDataContext().Returns(centralDataContext);

				var service = new GroupMasterQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new GroupMasterSearchCriteria
				{
					AssociatedWith = new ModuleAssociationSearchCriteria
					{
						AccountNumber = testAccountnumber,
						Module = testModule
					}
				}).FirstOrDefault();

				Assert.True(result.GroupNumber == testGroupNumber);
				Assert.True(result.Comment == testComment);
			}
		}

		[Fact]
		public void ReturnNullForNonExistingModuleAccount()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testGroupNumber = 1;
			string testComment = "Test Comment";
			int testAccountnumber = 20;
			string testModule = "AR";

			using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
			{
				centralDataContext.GroupMasters.Add(new GroupMaster
				{
					GroupNumber = testGroupNumber,
					Comment = testComment,
					DateCreated = DateTime.Today
				});

				centralDataContext.SaveChanges();

				centralDataContext.ModuleAssociations.Add(new ModuleAssociation
				{
					GroupNumber = centralDataContext.GroupMasters.FirstOrDefault().ID,
					Account = testAccountnumber,
					Module = testModule,
				});

				centralDataContext.SaveChanges();

				trioContextFactory.GetCentralDataContext().Returns(centralDataContext);

				var service = new GroupMasterQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new GroupMasterSearchCriteria
				{
					AssociatedWith = new ModuleAssociationSearchCriteria
					{
						AccountNumber = testAccountnumber + 1,
						Module = testModule
					}
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
