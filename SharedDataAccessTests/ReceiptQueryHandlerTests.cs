﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedDataAccess;
using SharedDataAccess.CashReceipts;
using SharedDataAccess.CentralData;
using Xunit;

namespace SharedDataAccessTests
{
	public class ReceiptQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectInfoForExistingReceipt()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testReceiptId = 20;
			int testReceiptNumber = 100;

			using (var cashReceiptsDataContext = new CashReceiptsContext(GetDBOptions<CashReceiptsContext>()))
			{
				cashReceiptsDataContext.Receipts.Add(new Receipt
				{
					Id = testReceiptId,
					ReceiptNumber = testReceiptNumber,
					ReceiptDate = DateTime.Today
				});

				cashReceiptsDataContext.SaveChanges();

				trioContextFactory.GetCashReceiptsContext().Returns(cashReceiptsDataContext);

				var service = new ReceiptQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new ReceiptSearchCriteria
				{
					ReceiptId = testReceiptId,
					ReceiptDate = DateTime.Today
				}).FirstOrDefault();

				Assert.True(result != null);
				Assert.True(result.ReceiptNumber == testReceiptNumber);
			}
		}

		[Fact]
		public void ReturnCorrectInfoForExistingReceiptNotSameDateWithNoDateCheck()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testReceiptId = 20;
			int testReceiptNumber = 100;

			using (var cashReceiptsDataContext = new CashReceiptsContext(GetDBOptions<CashReceiptsContext>()))
			{
				cashReceiptsDataContext.Receipts.Add(new Receipt
				{
					Id = testReceiptId,
					ReceiptNumber = testReceiptNumber,
					ReceiptDate = DateTime.Today.AddDays(-10)
				});

				cashReceiptsDataContext.SaveChanges();

				trioContextFactory.GetCashReceiptsContext().Returns(cashReceiptsDataContext);

				var service = new ReceiptQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new ReceiptSearchCriteria
				{
					ReceiptId = testReceiptId,
				}).FirstOrDefault();

				Assert.True(result != null);
				Assert.True(result.ReceiptNumber == testReceiptNumber);
			}
		}

		[Fact]
		public void ReturnCorrectInfoForExistingReceiptNotSameIdNoIdCheck()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testReceiptId = 20;
			int testReceiptNumber = 100;

			using (var cashReceiptsDataContext = new CashReceiptsContext(GetDBOptions<CashReceiptsContext>()))
			{
				cashReceiptsDataContext.Receipts.Add(new Receipt
				{
					Id = testReceiptId,
					ReceiptNumber = testReceiptNumber,
					ReceiptDate = DateTime.Today
				});

				cashReceiptsDataContext.SaveChanges();

				trioContextFactory.GetCashReceiptsContext().Returns(cashReceiptsDataContext);

				var service = new ReceiptQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new ReceiptSearchCriteria
				{
					ReceiptDate = DateTime.Today
				}).FirstOrDefault();

				Assert.True(result != null);
				Assert.True(result.ReceiptNumber == testReceiptNumber);
			}
		}

		[Fact]
		public void ReturnCorrectInfoForNoReceipt()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testReceiptId = 20;
			int testReceiptNumber = 100;

			using (var cashReceiptsDataContext = new CashReceiptsContext(GetDBOptions<CashReceiptsContext>()))
			{
				trioContextFactory.GetCashReceiptsContext().Returns(cashReceiptsDataContext);

				var service = new ReceiptQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new ReceiptSearchCriteria
				{
					ReceiptId = testReceiptId,
					ReceiptDate = DateTime.Today
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
