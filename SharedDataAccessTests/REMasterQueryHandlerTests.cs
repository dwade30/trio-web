﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedDataAccess;
using SharedDataAccess.RealEstate;
using Xunit;

namespace SharedDataAccessTests
{
	public class REMasterQueryHandlerTests
	{
		[Fact]
		public void ReturnMasterInfoForMatchingId()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var realEstateContext = new RealEstateContext(GetDBOptions<RealEstateContext>()))
			{
				string testDeedName1 = "Test Deed Name 1";
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;

				realEstateContext.REMasters.Add(new REMaster
				{
					Rsaccount = testAccountNumber,
					DeedName1 = testDeedName1,
					Rsdeleted = false
				});

				realEstateContext.SaveChanges();

				trioContextFactory.GetRealEstateContext().Returns(realEstateContext);

				var service = new MasterQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new REMasterSearchCriteria
				{
					Id = realEstateContext.REMasters.FirstOrDefault()?.Id ?? 0,
					deletedOption = REMasterSearchCriteria.DeletedSelection.NotDeleted
				}).FirstOrDefault();

				Assert.True(result.Rsaccount == testAccountNumber);
				Assert.True(result.DeedName1 == testDeedName1);
			}
		}

		[Fact]
		public void ReturnNoMasterInfoForMatchingIdButDeleted()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var realEstateContext = new RealEstateContext(GetDBOptions<RealEstateContext>()))
			{
				string testDeedName1 = "Test Deed Name 1";
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;

				realEstateContext.REMasters.Add(new REMaster
				{
					Rsaccount = testAccountNumber,
					DeedName1 = testDeedName1,
					Rsdeleted = true
				});

				realEstateContext.SaveChanges();

				trioContextFactory.GetRealEstateContext().Returns(realEstateContext);

				var service = new MasterQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new REMasterSearchCriteria
				{
					Id = realEstateContext.REMasters.FirstOrDefault()?.Id ?? 0,
					deletedOption = REMasterSearchCriteria.DeletedSelection.NotDeleted
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		[Fact]
		public void ReturnMasterInfoForMatchingAccountNumber()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var realEstateContext = new RealEstateContext(GetDBOptions<RealEstateContext>()))
			{
				string testDeedName1 = "Test Deed Name 1";
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;

				realEstateContext.REMasters.Add(new REMaster
				{
					Rsaccount = testAccountNumber,
					DeedName1 = testDeedName1,
					Rsdeleted = false
				});

				realEstateContext.SaveChanges();

				trioContextFactory.GetRealEstateContext().Returns(realEstateContext);

				var service = new MasterQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new REMasterSearchCriteria
				{
					AccountNumber = testAccountNumber,
					deletedOption = REMasterSearchCriteria.DeletedSelection.NotDeleted
				}).FirstOrDefault();

				Assert.True(result.Rsaccount == testAccountNumber);
				Assert.True(result.DeedName1 == testDeedName1);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
