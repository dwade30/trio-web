﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedDataAccess;
using SharedDataAccess.AccountsReceivable;
using Xunit;

namespace SharedDataAccessTests
{
	public class UpdateCommentHandlerTests
	{
		[Fact]
		public void UpdateCommentCorrectlyCommentExists()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;
				string testUpdatedComment = "THIS IS THE NEW COMMENT.";

				accountsReceivableContext.Comments.Add(new Comment
				{
					Text = testComment,
					Account = testAccountNumber,
					Priority = testPriority
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new UpdateCommentHandler(trioContextFactory);

				var result = service.Handle(new UpdateComment
				{
					Comment = testUpdatedComment,
					CustomerId = testAccountNumber
				}, new CancellationToken()); 


				Assert.True(result.Result);
				Assert.True(accountsReceivableContext.Comments.FirstOrDefault().Text == testUpdatedComment);
				Assert.True(accountsReceivableContext.Comments.FirstOrDefault().Priority == testPriority);
			}
		}

		[Fact]
		public void UpdateCommentCorrectlyNoCommentExists()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;
				string testUpdatedComment = "THIS IS THE NEW COMMENT.";

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new UpdateCommentHandler(trioContextFactory);

				var result = service.Handle(new UpdateComment
				{
					Comment = testUpdatedComment,
					CustomerId = testAccountNumber
				}, new CancellationToken());


				Assert.True(result.Result);
				Assert.True(accountsReceivableContext.Comments.FirstOrDefault().Text == testUpdatedComment);
				Assert.True(accountsReceivableContext.Comments.FirstOrDefault().Priority == 0);
			}
		}

		[Fact]
		public void RemoveCommentCorrectlyCommentExists()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;
				string testUpdatedComment = "THIS IS THE NEW COMMENT.";

				accountsReceivableContext.Comments.Add(new Comment
				{
					Text = "",
					Account = testAccountNumber,
					Priority = testPriority
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new UpdateCommentHandler(trioContextFactory);

				var result = service.Handle(new UpdateComment
				{
					Comment = "",
					CustomerId = testAccountNumber
				}, new CancellationToken());


				Assert.True(result.Result);
				Assert.True(accountsReceivableContext.Comments.Count() == 0);
			}
		}

		[Fact]
		public void UpdateCommentPriorityCorrectlyCommentExists()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;
				string testUpdatedComment = "THIS IS THE NEW COMMENT.";

				accountsReceivableContext.Comments.Add(new Comment
				{
					Text = testComment,
					Account = testAccountNumber,
					Priority = testPriority
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new UpdateCommentHandler(trioContextFactory);

				var result = service.Handle(new UpdateComment
				{
					Priority = 0,
					CustomerId = testAccountNumber
				}, new CancellationToken());


				Assert.True(result.Result);
				Assert.True(accountsReceivableContext.Comments.FirstOrDefault().Text == testComment);
				Assert.True(accountsReceivableContext.Comments.FirstOrDefault().Priority == 0);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
