﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedDataAccess;
using SharedDataAccess.AccountsReceivable;
using Xunit;

namespace SharedDataAccessTests
{
	public class CustomerPartyQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingName()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testName = "Will Wallace";
				int testAccountNumber = 999;

				accountsReceivableContext.CustomerParties.Add(new CustomerParty
				{
					FullName = testName,
					CustomerId = testAccountNumber,
					Deleted = false
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CustomerPartyQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerPartySearchCriteria
				{
					Name = testName
				}).FirstOrDefault();

				Assert.True(result.FullName == testName);
				Assert.True(result.InvoiceNumber == "");
				Assert.True(result.CustomerId == testAccountNumber);
			}
		}

		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingAddress()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testName = "Will Wallace";
				string testAddress = "10 North Main St";
				int testAccountNumber = 999;

				accountsReceivableContext.CustomerParties.Add(new CustomerParty
				{
					FullName = testName,
					Address1 = testAddress,
					CustomerId = testAccountNumber,
					Deleted = false
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CustomerPartyQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerPartySearchCriteria
				{
					Address = "10 N"
				}).FirstOrDefault();

				Assert.True(result.FullName == testName);
				Assert.True(result.Address1 == testAddress);
				Assert.True(result.InvoiceNumber == "");
				Assert.True(result.CustomerId == testAccountNumber);
			}
		}

		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingInvoice()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testName = "Will Wallace";
				string testAddress = "10 North Main St";
				int testAccountNumber = 999;
				string testInvoice = "12345";

				accountsReceivableContext.CustomerParties.Add(new CustomerParty
				{
					FullName = testName,
					Address1 = testAddress,
					CustomerId = testAccountNumber,
					Deleted = false,
				});

				accountsReceivableContext.Bills.Add(new Bill
				{
					InvoiceNumber = testInvoice,
					AccountKey = testAccountNumber,
					PrinPaid = 10m,
					PrinOwed = 10m
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CustomerPartyQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerPartySearchCriteria
				{
					Invoice = testInvoice
				}).FirstOrDefault();

				Assert.True(result.FullName == testName);
				Assert.True(result.Address1 == testAddress);
				Assert.True(result.InvoiceNumber == testInvoice);
				Assert.True(result.CustomerId == testAccountNumber);
				Assert.True(result.Paid == true);
			}
		}

		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingDeletedCustomer()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testName = "Dave Wade";
				int testAccountNumber = 999;

				accountsReceivableContext.CustomerParties.Add(new CustomerParty
				{
					FullName = testName,
					CustomerId = testAccountNumber,
					Deleted = true
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CustomerPartyQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerPartySearchCriteria
				{
					Name = testName
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
