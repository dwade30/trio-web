﻿using System;
using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Models;
using SharedDataAccess;
using SharedDataAccess.TaxCollections;
using Xunit;

namespace SharedDataAccessTests.TaxCollections
{
    public class PropertyTaxStatusQueryHandlerTests
    {
        [Fact]
        public void FindRealAsOfReturnsZeroOriginalDueForBillsCreatedAfterEffectiveDate()
        {
            var dateTimeService = Substitute.For<IDateTimeService>();
            dateTimeService.Today().Returns(new DateTime(2021, 1, 1));
            dateTimeService.Now().Returns(new DateTime(2021, 1, 1, 10, 30, 0));
            var contextFactory = Substitute.For<ITrioContextInterfaceFactory>();
            var collectionsContext = Substitute.For<ITaxCollectionsContext>();
            
            contextFactory.GetTaxCollectionsContextInterface().Returns(collectionsContext);
            var queryHandler = new PropertyTaxStatusQueryHandler( contextFactory, dateTimeService);

            var effectiveDate = new DateTime(2020,6,5);

            var billAccount = new RealEstateBillAccountPartyView()
            {
                Id = 1,
                Account = 2,
                TransferFromBillingDateFirst = effectiveDate.LastDayOfMonth(),
                Acres = 1,
                BillingYear = 20201,
                RateKey = 1,
                BillingType = "RE",
                TaxDue1 = 100,
                TaxDue2 = 100,
                TaxDue3 = 100,
                TaxDue4 = 100,
                LienRecordNumber = 0

            };
            var rateRec = new RateRec()
            {
                Id = 1,
                BillingDate = effectiveDate.LastDayOfMonth(),
                CommitmentDate = effectiveDate.LastDayOfMonth(),
                CreationDate = effectiveDate.LastDayOfMonth(),
                DueDate1 = new DateTime(2020,8,1),
                DueDate2 = new DateTime(2020,9,1),
                DueDate3 = new DateTime(2020,10,1),
                DueDate4 = new DateTime(2020,11,1),
                NumberOfPeriods =  4,
                RateType = "R",
                InterestStartDate1 = new DateTime(2020,8,2),
                InterestStartDate2 = new DateTime(2020,9,2),
                InterestStartDate3 = new DateTime(2020,10,2),
                InterestStartDate4 = new DateTime(2020,11,2),
                TaxRate = .07,
                Weight1 = .25,
                Weight2 = .25,
                Weight3 = .25,
                Weight4 = .25,
                Year = 2020
            };
            collectionsContext.RealEstateBillAccountParties.Returns(f =>
                new List<RealEstateBillAccountPartyView>() { billAccount }.AsQueryable());
            collectionsContext.RateRecords.Returns(f => new List<RateRec>() { rateRec}.AsQueryable());
            var bills = queryHandler.FindRealAsOf(b => b.BillingType == "RE", true, effectiveDate);
            Assert.Equal(0,bills.Sum(b => b.TaxBill.TaxInstallments.GetOriginalCharged()));
        }

        [Fact]
        public void FindRealAsOfReturnsFullOriginalDueForBillCreatedBeforeEffectiveDate()
        {
            var dateTimeService = Substitute.For<IDateTimeService>();
            dateTimeService.Today().Returns(new DateTime(2021, 1, 1));
            dateTimeService.Now().Returns(new DateTime(2021, 1, 1, 10, 30, 0));
            var contextFactory = Substitute.For<ITrioContextInterfaceFactory>();
            var collectionsContext = Substitute.For<ITaxCollectionsContext>();

            contextFactory.GetTaxCollectionsContextInterface().Returns(collectionsContext);
            var queryHandler = new PropertyTaxStatusQueryHandler(contextFactory, dateTimeService);

            var effectiveDate = new DateTime(2020, 7, 8);
            
            var billAccount = new RealEstateBillAccountPartyView()
            {
                Id = 1,
                Account = 2,
                TransferFromBillingDateFirst = effectiveDate.OneMonthBefore(),
                Acres = 1,
                BillingYear = 20201,
                RateKey = 1,
                BillingType = "RE",
                TaxDue1 = 100,
                TaxDue2 = 100,
                TaxDue3 = 100,
                TaxDue4 = 100,
                LienRecordNumber =  0
                
            };
            
            var rateRec = new RateRec()
            {
                Id = 1,
                BillingDate = effectiveDate.OneMonthBefore(),
                CommitmentDate = effectiveDate.OneMonthBefore(),
                CreationDate = effectiveDate.OneMonthBefore(),
                DueDate1 = new DateTime(2020, 8, 1),
                DueDate2 = new DateTime(2020, 9, 1),
                DueDate3 = new DateTime(2020, 10, 1),
                DueDate4 = new DateTime(2020, 11, 1),
                NumberOfPeriods = 4,
                RateType = "R",
                InterestStartDate1 = new DateTime(2020, 8, 2),
                InterestStartDate2 = new DateTime(2020, 9, 2),
                InterestStartDate3 = new DateTime(2020, 10, 2),
                InterestStartDate4 = new DateTime(2020, 11, 2),
                TaxRate = .07,
                Weight1 = .25,
                Weight2 = .25,
                Weight3 = .25,
                Weight4 = .25,
                Year = 2020
            };
            collectionsContext.RealEstateBillAccountParties.Returns(f =>
                new List<RealEstateBillAccountPartyView>() {billAccount}.AsQueryable());
            collectionsContext.RateRecords.Returns(f => new List<RateRec>() { rateRec }.AsQueryable());
            var bills = queryHandler.FindRealAsOf(b => b.BillingType == "RE", true, effectiveDate);
            Assert.Equal(400, bills.Sum(b => b.TaxBill.TaxInstallments.GetOriginalCharged()));
        }

        [Fact]
        public void FindRealAsOfReturnsOnlyPaymentsBeforeEffectiveDate()
        {
            var dateTimeService = Substitute.For<IDateTimeService>();
            dateTimeService.Today().Returns(new DateTime(2021, 1, 1));
            dateTimeService.Now().Returns(new DateTime(2021, 1, 1, 10, 30, 0));
            var contextFactory = Substitute.For<ITrioContextInterfaceFactory>();
            var collectionsContext = Substitute.For<ITaxCollectionsContext>();

            contextFactory.GetTaxCollectionsContextInterface().Returns(collectionsContext);
            var queryHandler = new PropertyTaxStatusQueryHandler(contextFactory, dateTimeService);

            var effectiveDate = new DateTime(2020, 7, 8);

            var billAccount = new RealEstateBillAccountPartyView()
            {
                Id = 1,
                Account = 2,
                TransferFromBillingDateFirst = effectiveDate.OneMonthBefore(),
                Acres = 1,
                BillingYear = 20201,
                RateKey = 1,
                BillingType = "RE",
                TaxDue1 = 100,
                TaxDue2 = 0,
                TaxDue3 = 0,
                TaxDue4 = 0,
                LienRecordNumber = 0
                

            };

            var rateRec = new RateRec()
            {
                Id = 1,
                BillingDate = effectiveDate.OneMonthBefore(),
                CommitmentDate = effectiveDate.OneMonthBefore(),
                CreationDate = effectiveDate.OneMonthBefore(),
                DueDate1 = new DateTime(2020, 8, 1),
                NumberOfPeriods = 1,
                RateType = "R",
                InterestStartDate1 = new DateTime(2020, 8, 2),
                InterestStartDate2 = new DateTime(2020, 9, 2),
                InterestStartDate3 = new DateTime(2020, 10, 2),
                InterestStartDate4 = new DateTime(2020, 11, 2),
                TaxRate = .07,
                Weight1 = 1,
                Weight2 = 0,
                Weight3 = 0,
                Weight4 = 0,
                Year = 2020
            };

            var payments = new List<PaymentRec>()
            {
                new PaymentRec()
                {
                    Account = 2,
                    ActualSystemDate = effectiveDate.OneMonthBefore().EndOfDay(),
                    BillCode = "R",
                    Code = "P",
                    BillId = 1,
                    EffectiveInterestDate = effectiveDate.OneMonthBefore().EndOfDay(),
                    RecordedTransactionDate = effectiveDate.OneMonthBefore().EndOfDay(),
                    Principal = 50,
                    Period = "A",
                    ReceiptNumber = 1,
                    Year = 20201,
                    Reference = ""
                },
                new PaymentRec()
                {
                    Account = 2,
                    ActualSystemDate = effectiveDate.LastDayOfMonth(),
                    BillCode = "R",
                    Code = "P",
                    BillId = 1,
                    EffectiveInterestDate = effectiveDate.LastDayOfMonth(),
                    RecordedTransactionDate = effectiveDate.LastDayOfMonth(),
                    Principal = 40,
                    Period = "A",
                    ReceiptNumber = 2,
                    Year = 20201,
                    Reference = ""
                }
            };
            collectionsContext.PaymentRecords.Returns(f => payments.AsQueryable());
            collectionsContext.RealEstateBillAccountParties.Returns(f =>
                new List<RealEstateBillAccountPartyView>() { billAccount }.AsQueryable());
            collectionsContext.RateRecords.Returns(f => new List<RateRec>() { rateRec }.AsQueryable());
            var bill = queryHandler.FindRealAsOf(b => b.BillingType == "RE", true, effectiveDate).FirstOrDefault();
            Assert.Equal(50,bill.TaxBill.BillSummary.GetNetOwed());
        }

        
    }
}