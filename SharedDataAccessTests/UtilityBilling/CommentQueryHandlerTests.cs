﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedDataAccess;
using SharedDataAccess.UtilityBilling;
using Xunit;

namespace SharedDataAccessTests.UtilityBilling

{
	public class CommentQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectComment()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;

				utilityBillingContext.Comments.Add(new Comment
				{
					Text = testComment,
					Account = testAccountNumber,
					Priority = testPriority
				});

				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new CommentQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CommentSearchCriteria
				{
					AccountId = testAccountNumber
				}).FirstOrDefault();

				Assert.True(result.Text == testComment);
				Assert.True(result.Account == testAccountNumber);
				Assert.True(result.Priority == testPriority);
			}
		}

		[Fact]
		public void ReturnCorrectNoComment()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;


				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new CommentQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CommentSearchCriteria
				{
					AccountId = testAccountNumber
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
