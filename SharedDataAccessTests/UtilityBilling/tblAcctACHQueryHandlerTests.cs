﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedDataAccess;
using SharedDataAccess.UtilityBilling;
using Xunit;

namespace SharedDataAccessTests.UtilityBilling
{
	public class tblAcctACHQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectACHInfo()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testACHAccount = "TEST ACCT";
				int testAccountId = 999;
				int testPriority = 1;

				utilityBillingContext.tblAcctACHs.Add(new tblAcctACH
				{
					AccountKey = testAccountId,
					ACHAcctNumber = testACHAccount,
					ACHActive = true
				});

				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new tblAcctACHQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new tblAcctACHSearchCriteria
				{
					AccountId = testAccountId
				}).FirstOrDefault();

				Assert.True(result.ACHAcctNumber == testACHAccount);
				Assert.True(result.ACHActive == true);
			}
		}

		[Fact]
		public void ReturnNoACHInfo()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testACHAccount = "TEST ACCT";
				int testAccountId = 999;
				int testPriority = 1;

				utilityBillingContext.tblAcctACHs.Add(new tblAcctACH
				{
					AccountKey = testAccountId,
					ACHAcctNumber = testACHAccount,
					ACHActive = true
				});

				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new tblAcctACHQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new tblAcctACHSearchCriteria
				{
					AccountId = testAccountId - 1
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
