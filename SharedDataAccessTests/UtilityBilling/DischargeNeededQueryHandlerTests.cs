﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedDataAccess;
using SharedDataAccess.UtilityBilling;
using Xunit;

namespace SharedDataAccessTests.UtilityBilling
{
	public class DischargeNeededQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectDischargeNeeded()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testLienKey = 1;

				utilityBillingContext.DischargeNeededs.Add(new DischargeNeeded
				{
					LienId = testLienKey,
					Printed = true,
					Book = 2
				});

				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new DischargeNeededQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new DischargeNeededSearchCriteria
				{
					Ids = new List<int>
					{
						testLienKey
					} 
				}).FirstOrDefault();

				Assert.True(result.Printed == true);
				Assert.True(result.Book == 2);
			}
		}

		[Fact]
		public void ReturnNoDischargeNeeded()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testLienKey = 1;

				utilityBillingContext.DischargeNeededs.Add(new DischargeNeeded
				{
					LienId = testLienKey,
					Printed = true,
					Book = 2
				});

				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new DischargeNeededQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new DischargeNeededSearchCriteria
				{
					Ids = new List<int>
					{
						testLienKey + 1
					}
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
