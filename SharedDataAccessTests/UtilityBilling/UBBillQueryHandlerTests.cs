﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;
using SharedDataAccess;
using SharedDataAccess.UtilityBilling;
using Xunit;

namespace SharedDataAccessTests.UtilityBilling
{
	public class UBBillQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectOnlyBillNoPayments()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
			var billCalculationService = Substitute.For<IBillCalculationService>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testName = "John Doe";
				string testOwnerName = "Billy Bob";
				int testAccountNumber = 999;
				int testAccountId = 1;
				string testAddress = "10 Noeth Main St";
				string testCity = "Bangor";
				string testState = "ME";
				string testZip = "02983";

				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					DateCreated = DateTime.Today.AddMonths(-6),
					Description = "Test Rate",
					Start = DateTime.Today.AddMonths(-12),
					End = DateTime.Today.AddMonths(-9),
					IntStart = DateTime.Today,
					SIntRate = .1,
					WIntRate = .2,
				};

				utilityBillingContext.RateKeys.Add(testRateKey);
				utilityBillingContext.SaveChanges();

				var testBill = new Bill
				{
					AccountId = testAccountId,
					ActualAccountNumber = testAccountNumber,
					BAddress1 = testAddress,
					BAddress2 = "",
					BAddress3 = "",
					BCity = testCity,
					BState = testState,
					BZip = testZip,
					BZip4 = "",
					BName = testName,
					BName2 = "",
					BillNumber = testRateKey.ID,
					BillDate = testRateKey.BillDate,
					BillStatus = "A",
					Service = "W",
					OName = testOwnerName,
					WLienRecordNumber = 0,
					WIntPaidDate = null,
					WPreviousInterestPaidDate = null,
					WCostAdded = -10,
					WIntOwed = 15,
					WCostPaid = 10,
					WBillOwner = false,
					WCostOwed = 10,
					WIntAdded = -5,
					WIntPaid = 20,
					WPrinOwed = 200,
					WPrinPaid = 100,
					WTaxOwed = 50,
					WTaxPaid = 50,
				};

				utilityBillingContext.Bills.Add(testBill);
				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new UBBillQueryHandler(trioContextFactory, billCalculationService);

				var result = service.ExecuteQuery(new UBBillSearchCriteria
				{
					AccountId = testAccountId,
					VoidedSelection = VoidedStatusSelection.NotVoided
				});

				var testResult = result.FirstOrDefault();
				var testDetails = testResult.UtilityDetails.FirstOrDefault();

				Assert.True(result.Count() == 1);
				Assert.True(testResult.IsLien == false);
				Assert.True(testResult.Pending == false);
				Assert.True(testResult.ContainsService(UtilityType.Water) == true);
				Assert.True(testResult.BilledAddress1 == testAddress);
				Assert.True(testResult.BilledAddress2 == "");
				Assert.True(testResult.BilledAddress3 == "");
				Assert.True(testResult.BilledCity == testCity);
				Assert.True(testResult.BilledState == testState);
				Assert.True(testResult.BilledZip == testZip);
				Assert.True(testResult.BilledZip4 == "");
				Assert.True(testResult.BillDate == testRateKey.BillDate);
				Assert.True(testResult.BillNumber == testRateKey.ID);
				Assert.True(testResult.BillStatus == "A");
				Assert.True(testResult.Bname == testName);
				Assert.True(testResult.Bname2 == "");
				Assert.True(testResult.Oname2 == "");
				Assert.True(testResult.Oname == testOwnerName);
				Assert.True(testResult.BillPayments(UtilityType.All).Count == 0);
				Assert.True(testResult.UtilityDetails.Count == 1);

				Assert.True(testDetails.BillOwner == false);
				Assert.True(testDetails.IsIn30DayNoticeStatus == false);
				Assert.True(testDetails.IsLiened == false);
				Assert.True(testDetails.LienMainBill == false);
				Assert.True(testDetails.AbatementTotal == 0);
				Assert.True(testDetails.BalanceDue == 110);
				Assert.True(testDetails.CostAdded == (decimal)testBill.WCostAdded);
				Assert.True(testDetails.CostDue == ((decimal)testBill.WCostOwed - (decimal)testBill.WCostAdded -(decimal)testBill.WCostPaid));
				Assert.True(testDetails.CostOwed == (decimal)testBill.WCostOwed);
				Assert.True(testDetails.CostPaid == (decimal)testBill.WCostPaid);
				Assert.True(testDetails.CurrentInterest == 0);
				Assert.True(testDetails.DemandGroupId == 0);
				Assert.True(testDetails.IntAdded == (decimal)testBill.WIntAdded);
				Assert.True(testDetails.IntOwed == (decimal)testBill.WIntOwed);
				Assert.True(testDetails.IntPaid == (decimal)testBill.WIntPaid);
				Assert.True(testDetails.InterestDue == (decimal)testBill.WIntOwed - (decimal)testBill.WIntAdded - (decimal)testBill.WIntPaid);
				Assert.True(testDetails.LienId == 0);
				Assert.True(testDetails.LastInterestDate == testRateKey.IntStart);
				Assert.True(testDetails.PreLienInterestPaid == 0);
				Assert.True(testDetails.MaturityFee == 0);
				Assert.True(testDetails.PendingChargedInterest == 0);
				Assert.True(testDetails.PendingCostAmount == 0);
				Assert.True(testDetails.PendingPrincipalAmount == 0);
				Assert.True(testDetails.PendingCurrentInterestAmount == 0);
				Assert.True(testDetails.PendingPreLienInterestAmount == 0);
				Assert.True(testDetails.PendingTaxAmount == 0);
				Assert.True(testDetails.PerDiem == 0);
				Assert.True(testDetails.PrincipalDue == (decimal)testBill.WPrinOwed - (decimal)testBill.WPrinPaid);
				Assert.True(testDetails.PrincipalOwed == (decimal)testBill.WPrinOwed);
				Assert.True(testDetails.PrincipalPaid == (decimal)testBill.WPrinPaid);
				Assert.True(testDetails.Payments.Count == 0);
				Assert.True(testDetails.PreviousInterestDate == null);
				Assert.True(testDetails.RateCreatedDate == testRateKey.DateCreated);
				Assert.True(testDetails.RateEnd == testRateKey.End);
				Assert.True(testDetails.RateStart == testRateKey.Start);
				Assert.True(testDetails.SavedPreviousInterestDate == null);
				Assert.True(testDetails.Service == UtilityType.Water);
				Assert.True(testDetails.ServiceCode == "W");
			}
		}

		[Fact]
		public void ReturnCorrectOnlyBillWithPaymentNoChargedInterest()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
			var billCalculationService = Substitute.For<IBillCalculationService>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testName = "John Doe";
				string testName2 = "Billy Bob";
				string testOwnerName = "Billy Bob";
				string testOwnerName2 = "Sally Sue";
				int testAccountNumber = 999;
				int testAccountId = 1;
				string testAddress = "10 Noeth Main St";
				string testCity = "Bangor";
				string testState = "ME";
				string testZip = "02983";
				string testZip4 = "0982";
				Guid testTransactionIdentifier = Guid.NewGuid();

				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					DateCreated = DateTime.Today.AddMonths(-6),
					Description = "Test Rate",
					Start = DateTime.Today.AddMonths(-12),
					End = DateTime.Today.AddMonths(-9),
					IntStart = DateTime.Today,
					SIntRate = .1,
					WIntRate = .2,
				};

				utilityBillingContext.RateKeys.Add(testRateKey);
				utilityBillingContext.SaveChanges();

				var testBill = new Bill
				{
					AccountId = testAccountId,
					ActualAccountNumber = testAccountNumber,
					BAddress1 = testAddress,
					BAddress2 = "",
					BAddress3 = "",
					BCity = testCity,
					BState = testState,
					BZip = testZip,
					BZip4 = testZip4,
					BName = testName,
					BName2 = testName2,
					BillNumber = testRateKey.ID,
					BillDate = testRateKey.BillDate,
					BillStatus = "A",
					Service = "W",
					OName = testOwnerName,
					OName2 = testOwnerName2,
					WLienRecordNumber = 0,
					WIntPaidDate = DateTime.Today,
					WPreviousInterestPaidDate = null,
					WCostAdded = -10,
					WIntOwed = 15,
					WCostPaid = 10,
					WBillOwner = false,
					WCostOwed = 10,
					WIntAdded = -5,
					WIntPaid = 20,
					WPrinOwed = 200,
					WPrinPaid = 100,
					WTaxOwed = 50,
					WTaxPaid = 50,
				};

				utilityBillingContext.Bills.Add(testBill);
				utilityBillingContext.SaveChanges();

				var testPayment = new PaymentRec
				{
					AccountKey = testAccountId,
					BillId = testBill.ID,
					ActualSystemDate = DateTime.Today,
					CashDrawer = "Y",
					Comments = "TEST COMMENT",
					Code = "P",
					EffectiveInterestDate = DateTime.Today,
					GeneralLedger = "Y",
					PreLienInterest = 10,
					CurrentInterest = 10,
					Tax = 50,
					Principal = 100,
					LienCost = 10,
					LienPayment = false,
					Service = "W",
					TransactionIdentifier = testTransactionIdentifier
				};

				utilityBillingContext.PaymentRecs.Add(testPayment);
				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new UBBillQueryHandler(trioContextFactory, billCalculationService);

				var result = service.ExecuteQuery(new UBBillSearchCriteria
				{
					AccountId = testAccountId,
					VoidedSelection = VoidedStatusSelection.NotVoided
				});

				var testResult = result.FirstOrDefault();
				var testDetails = testResult.UtilityDetails.FirstOrDefault();
				var resultPayment = testDetails.Payments.FirstOrDefault();

				Assert.True(result.Count() == 1);
				Assert.True(testResult.IsLien == false);
				Assert.True(testResult.Pending == false);
				Assert.True(testResult.ContainsService(UtilityType.Water) == true);
				Assert.True(testResult.BilledAddress1 == testAddress);
				Assert.True(testResult.BilledAddress2 == "");
				Assert.True(testResult.BilledAddress3 == "");
				Assert.True(testResult.BilledCity == testCity);
				Assert.True(testResult.BilledState == testState);
				Assert.True(testResult.BilledZip == testZip);
				Assert.True(testResult.BilledZip4 == testZip4);
				Assert.True(testResult.BilledZipFormatted == testZip + "-" + testZip4);
				Assert.True(testResult.BillDate == testRateKey.BillDate);
				Assert.True(testResult.BillNumber == testRateKey.ID);
				Assert.True(testResult.BillStatus == "A");
				Assert.True(testResult.Bname == testName);
				Assert.True(testResult.Bname2 == testName2);
				Assert.True(testResult.BilledNameFormatted == testName + " and " + testName2);
				Assert.True(testResult.Oname2 == testOwnerName2);
				Assert.True(testResult.Oname == testOwnerName);
				Assert.True(testResult.OwnerZipFormatted == "");
				Assert.True(testResult.OwnerNameFormatted == testOwnerName + " and " + testOwnerName2);
				Assert.True(testResult.BillPayments(UtilityType.All).Count == 1);
				Assert.True(testResult.UtilityDetails.Count == 1);

				Assert.True(testDetails.BillOwner == false);
				Assert.True(testDetails.IsIn30DayNoticeStatus == false);
				Assert.True(testDetails.IsLiened == false);
				Assert.True(testDetails.LienMainBill == false);
				Assert.True(testDetails.AbatementTotal == 0);
				Assert.True(testDetails.BalanceDue == 110);
				Assert.True(testDetails.CostAdded == (decimal)testBill.WCostAdded);
				Assert.True(testDetails.CostDue == ((decimal)testBill.WCostOwed - (decimal)testBill.WCostAdded - (decimal)testBill.WCostPaid));
				Assert.True(testDetails.CostOwed == (decimal)testBill.WCostOwed);
				Assert.True(testDetails.CostPaid == (decimal)testBill.WCostPaid);
				Assert.True(testDetails.TotalCost == ((decimal)testBill.WCostOwed - (decimal)testBill.WCostAdded));
				Assert.True(testDetails.CurrentInterest == 0);
				Assert.True(testDetails.DemandGroupId == 0);
				Assert.True(testDetails.IntAdded == (decimal)testBill.WIntAdded);
				Assert.True(testDetails.IntOwed == (decimal)testBill.WIntOwed);
				Assert.True(testDetails.IntPaid == (decimal)testBill.WIntPaid);
				Assert.True(testDetails.InterestDue == (decimal)testBill.WIntOwed - (decimal)testBill.WIntAdded - (decimal)testBill.WIntPaid);
				Assert.True(testDetails.TotalInterest == (decimal)testBill.WIntOwed - (decimal)testBill.WIntAdded - testDetails.CurrentInterest);
				Assert.True(testDetails.LienId == 0);
				Assert.True(testDetails.TotalPayment == (decimal)testBill.WIntPaid + (decimal)testBill.WCostPaid + (decimal)testBill.WPrinPaid + (decimal)testBill.WTaxPaid);
				Assert.True(testDetails.LastInterestDate == testRateKey.IntStart);
				Assert.True(testDetails.PreLienInterestPaid == 0);
				Assert.True(testDetails.MaturityFee == 0);
				Assert.True(testDetails.PendingChargedInterest == 0);
				Assert.True(testDetails.PendingCostAmount == 0);
				Assert.True(testDetails.PendingPrincipalAmount == 0);
				Assert.True(testDetails.PendingCurrentInterestAmount == 0);
				Assert.True(testDetails.PendingPreLienInterestAmount == 0);
				Assert.True(testDetails.PendingTaxAmount == 0);
				Assert.True(testDetails.PerDiem == 0);
				Assert.True(testDetails.PrincipalDue == (decimal)testBill.WPrinOwed - (decimal)testBill.WPrinPaid);
				Assert.True(testDetails.PrincipalOwed == (decimal)testBill.WPrinOwed);
				Assert.True(testDetails.PrincipalPaid == (decimal)testBill.WPrinPaid);
				Assert.True(testDetails.Payments.Count == 1);
				Assert.True(testDetails.PreviousInterestDate == null);
				Assert.True(testDetails.RateCreatedDate == testRateKey.DateCreated);
				Assert.True(testDetails.RateEnd == testRateKey.End);
				Assert.True(testDetails.RateStart == testRateKey.Start);
				Assert.True(testDetails.SavedPreviousInterestDate == null);
				Assert.True(testDetails.Service == UtilityType.Water);
				Assert.True(testDetails.ServiceCode == "W");
				Assert.True(testDetails.IsIn30DayNoticeStatus == false);
				Assert.True(testDetails.ServiceCode == "W");

				Assert.True(resultPayment.IsReversal == false);
				Assert.True(resultPayment.ActualSystemDate == testPayment.ActualSystemDate);
				Assert.True(resultPayment.BillId == testPayment.BillId);
				Assert.True(resultPayment.BudgetaryAccountNumber == testPayment.BudgetaryAccountNumber);
				Assert.True(resultPayment.CashDrawer == testPayment.CashDrawer);
				Assert.True(resultPayment.ChargedInterestDate == testPayment.CHGINTDate);
				Assert.True(resultPayment.Code == testPayment.Code);
				Assert.True(resultPayment.Comments == testPayment.Comments);
				Assert.True(resultPayment.Cost == testPayment.LienCost);
				Assert.True(resultPayment.CurrentInterest == testPayment.CurrentInterest);
				Assert.True(resultPayment.PreLienInterest == testPayment.PreLienInterest);

				Assert.True(resultPayment.EffectiveInterestDate == testPayment.EffectiveInterestDate);
				Assert.True(resultPayment.GeneralLedger == testPayment.GeneralLedger);
				Assert.True(resultPayment.InterestTotal == testPayment.CurrentInterest + testPayment.PreLienInterest);
				Assert.True(resultPayment.Tax == testPayment.Tax);
				Assert.True(resultPayment.Principal == testPayment.Principal);
				Assert.True(resultPayment.Pending == false);
				Assert.True(resultPayment.TransactionId == testPayment.TransactionIdentifier);
			}
		}

		[Fact]
		public void ReturnCorrectOnlyBillWithPaymentWithChargedInterest()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
			var billCalculationService = Substitute.For<IBillCalculationService>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testName = "John Doe";
				string testName2 = "Billy Bob";
				string testOwnerName = "Billy Bob";
				string testOwnerName2 = "Sally Sue";
				int testAccountNumber = 999;
				int testAccountId = 1;
				string testAddress = "10 Noeth Main St";
				string testCity = "Bangor";
				string testState = "ME";
				string testZip = "02983";
				string testZip4 = "0982";
				Guid testTransactionIdentifier = Guid.NewGuid();

				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					DateCreated = DateTime.Today.AddMonths(-6),
					Description = "Test Rate",
					Start = DateTime.Today.AddMonths(-12),
					End = DateTime.Today.AddMonths(-9),
					IntStart = DateTime.Today,
					SIntRate = .1,
					WIntRate = .2,
				};

				utilityBillingContext.RateKeys.Add(testRateKey);
				utilityBillingContext.SaveChanges();

				var testBill = new Bill
				{
					AccountId = testAccountId,
					ActualAccountNumber = testAccountNumber,
					BAddress1 = testAddress,
					BAddress2 = "",
					BAddress3 = "",
					BCity = testCity,
					BState = testState,
					BZip = testZip,
					BZip4 = testZip4,
					BName = testName,
					BName2 = testName2,
					BillNumber = testRateKey.ID,
					BillDate = testRateKey.BillDate,
					BillStatus = "A",
					Service = "W",
					OName = testOwnerName,
					OName2 = testOwnerName2,
					WLienRecordNumber = 0,
					WIntPaidDate = DateTime.Today,
					WPreviousInterestPaidDate = null,
					WCostAdded = -10,
					WIntOwed = 15,
					WCostPaid = 10,
					WBillOwner = false,
					WCostOwed = 10,
					WIntAdded = -5,
					WIntPaid = 20,
					WPrinOwed = 200,
					WPrinPaid = 100,
					WTaxOwed = 50,
					WTaxPaid = 50,
				};

				utilityBillingContext.Bills.Add(testBill);
				utilityBillingContext.SaveChanges();

				var testPayment = new PaymentRec
				{
					AccountKey = testAccountId,
					BillId = testBill.ID,
					ActualSystemDate = DateTime.Today,
					CashDrawer = "Y",
					Comments = "TEST COMMENT",
					Code = "P",
					EffectiveInterestDate = DateTime.Today,
					GeneralLedger = "Y",
					PreLienInterest = 10,
					CurrentInterest = 10,
					Tax = 50,
					Principal = 100,
					LienCost = 10,
					LienPayment = false,
					Service = "W",
					TransactionIdentifier = testTransactionIdentifier
				};

				var testChargedInterest = new PaymentRec
				{
					AccountKey = testAccountId,
					BillId = testBill.ID,
					ActualSystemDate = DateTime.Today,
					CashDrawer = "Y",
					Comments = "TEST COMMENT",
					Code = "I",
					EffectiveInterestDate = DateTime.Today,
					GeneralLedger = "Y",
					PreLienInterest = 0,
					CurrentInterest = -10,
					Reference = "CHGINT",
					CHGINTDate = DateTime.Today,
					Tax = 0,
					Principal = 0,
					LienCost = 0,
					LienPayment = false,
					Service = "W",
					TransactionIdentifier = testTransactionIdentifier
				};

				utilityBillingContext.PaymentRecs.Add(testPayment);
				utilityBillingContext.PaymentRecs.Add(testChargedInterest);
				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new UBBillQueryHandler(trioContextFactory, billCalculationService);

				var result = service.ExecuteQuery(new UBBillSearchCriteria
				{
					AccountId = testAccountId,
					VoidedSelection = VoidedStatusSelection.NotVoided
				});

				var testResult = result.FirstOrDefault();
				var testDetails = testResult.UtilityDetails.FirstOrDefault();
				var resultPayment = testDetails.Payments.FirstOrDefault();
				var resultChargedInterest = resultPayment.ChargedInterestRecord;

				Assert.True(testDetails.Payments.Count == 1);
				
				Assert.True(resultChargedInterest != null);

				Assert.True(resultChargedInterest.ActualSystemDate == testChargedInterest.ActualSystemDate);
				Assert.True(resultChargedInterest.BillId == testChargedInterest.BillId);
				Assert.True(resultChargedInterest.CashDrawer == testChargedInterest.CashDrawer);
				Assert.True(resultChargedInterest.ChargedInterestDate == testChargedInterest.CHGINTDate);
				Assert.True(resultChargedInterest.Code == testChargedInterest.Code);
				Assert.True(resultChargedInterest.Reference == testChargedInterest.Reference);
				Assert.True(resultChargedInterest.Comments == testChargedInterest.Comments);
				Assert.True(resultChargedInterest.Cost == testChargedInterest.LienCost);
				Assert.True(resultChargedInterest.CurrentInterest == testChargedInterest.CurrentInterest);
				Assert.True(resultChargedInterest.PreLienInterest == testChargedInterest.PreLienInterest);

				Assert.True(resultChargedInterest.EffectiveInterestDate == testChargedInterest.EffectiveInterestDate);
				Assert.True(resultChargedInterest.GeneralLedger == testChargedInterest.GeneralLedger);
				Assert.True(resultChargedInterest.InterestTotal == testChargedInterest.CurrentInterest + testChargedInterest.PreLienInterest);
				Assert.True(resultChargedInterest.Tax == testChargedInterest.Tax);
				Assert.True(resultChargedInterest.Principal == testChargedInterest.Principal);
				Assert.True(resultChargedInterest.Pending == false);
				Assert.True(resultChargedInterest.TransactionId == testChargedInterest.TransactionIdentifier);
			}
		}

		[Fact]
		public void ReturnCorrectBillAndLienNoPayments()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
			var billCalculationService = Substitute.For<IBillCalculationService>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testName = "John Doe";
				string testOwnerName = "Billy Bob";
				int testAccountNumber = 999;
				int testAccountId = 1;
				string testAddress = "10 Noeth Main St";
				string testCity = "Bangor";
				string testState = "ME";
				string testZip = "02983";

				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					DateCreated = DateTime.Today.AddMonths(-6),
					Description = "Test Rate",
					Start = DateTime.Today.AddMonths(-12),
					End = DateTime.Today.AddMonths(-9),
					IntStart = DateTime.Today,
					RateType = "B",
					SIntRate = .1,
					WIntRate = .2,
				};

				var testRateKeyLien = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					DateCreated = DateTime.Today.AddMonths(-6),
					Description = "Test Lien Rate",
					Start = DateTime.Today.AddMonths(-12),
					End = DateTime.Today.AddMonths(-9),
					IntStart = DateTime.Today,
					RateType = "L",
					SIntRate = .1,
					WIntRate = .2,
				};

				utilityBillingContext.RateKeys.Add(testRateKey);
				utilityBillingContext.RateKeys.Add(testRateKeyLien);
				utilityBillingContext.SaveChanges();

				var testLien = new Lien
				{
					Book = 1,
					Page = 2,
					CostPaid = 0,
					Costs = 23.45,
					Interest = 10,
					IntAdded = -19.11,
					IntPaid = 0,
					IntPaidDate = null,
					MaturityFee = 0,
					PLIPaid = 0,
					PreviousInterestPaidDate = null,
					PrinPaid = 0,
					Principal = 100,
					RateKeyId = testRateKeyLien.ID,
					Status = "A",
					Tax = 14,
					PrintedLDN = false,
					TaxPaid = 0,
					Water = true,
				};

				utilityBillingContext.Liens.Add(testLien);
				utilityBillingContext.SaveChanges();

				var testBill = new Bill
				{
					AccountId = testAccountId,
					ActualAccountNumber = testAccountNumber,
					BAddress1 = testAddress,
					BAddress2 = "",
					BAddress3 = "",
					BCity = testCity,
					BState = testState,
					BZip = testZip,
					BZip4 = "",
					BName = testName,
					BName2 = "",
					BillNumber = testRateKey.ID,
					BillDate = testRateKey.BillDate,
					BillStatus = "A",
					Service = "W",
					OName = testOwnerName,
					WLienRecordNumber = testLien.ID,
					WIntPaidDate = null,
					WPreviousInterestPaidDate = null,
					WCostAdded = -10,
					WIntOwed = 15,
					WCostPaid = 10,
					WBillOwner = false,
					WCostOwed = 10,
					WIntAdded = -5,
					WIntPaid = 20,
					WPrinOwed = 200,
					WPrinPaid = 100,
					WTaxOwed = 50,
					WTaxPaid = 50,
				};

				utilityBillingContext.Bills.Add(testBill);
				utilityBillingContext.SaveChanges();

				testBill.WCombinationLienKey = testBill.ID;
				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new UBBillQueryHandler(trioContextFactory, billCalculationService);

				var result = service.ExecuteQuery(new UBBillSearchCriteria
				{
					AccountId = testAccountId,
					VoidedSelection = VoidedStatusSelection.NotVoided
				});

				var testResult = result.FirstOrDefault(x => x.IsLien == false);
				var testDetails = testResult.UtilityDetails.FirstOrDefault();

				var testLienResult = result.FirstOrDefault(x => x.IsLien);
				var testLienDetails = testLienResult.UtilityDetails.FirstOrDefault();

				Assert.True(result.Count() == 2);
				Assert.True(testResult.IsLien == false);
				Assert.True(testResult.BillPayments(UtilityType.All).Count == 0);
				Assert.True(testResult.UtilityDetails.Count == 1);

				Assert.True(testDetails.IsLiened == true);
				Assert.True(testDetails.LienMainBill == true);
				Assert.True(testDetails.LienId == testLien.ID);
				
				Assert.True(testLienResult.IsLien == true);
				Assert.True(testLienResult.Pending == false);
				Assert.True(testLienResult.ContainsService(UtilityType.Water) == true);
				Assert.True(testLienResult.BilledAddress1 == "");
				Assert.True(testLienResult.BilledAddress2 == "");
				Assert.True(testLienResult.BilledAddress3 == "");
				Assert.True(testLienResult.BilledCity == "");
				Assert.True(testLienResult.BilledState == "");
				Assert.True(testLienResult.BilledZip == "");
				Assert.True(testLienResult.BilledZip4 == "");
				Assert.True(testLienResult.BillDate == testRateKeyLien.BillDate);
				Assert.True(testLienResult.BillNumber == testRateKeyLien.ID);
				Assert.True(testLienResult.BillStatus == "A");
				Assert.True(testLienResult.Bname == "");
				Assert.True(testLienResult.Bname2 == "");
				Assert.True(testLienResult.Oname2 == "");
				Assert.True(testLienResult.Oname == "");
				Assert.True(testLienResult.BillPayments(UtilityType.All).Count == 0);
				Assert.True(testLienResult.UtilityDetails.Count == 1);

				Assert.True(testLienDetails.BillOwner == false);
				Assert.True(testLienDetails.IsIn30DayNoticeStatus == false);
				Assert.True(testLienDetails.IsLiened == false);
				Assert.True(testLienDetails.LienMainBill == false);
				Assert.True(testLienDetails.AbatementTotal == 0);
				Assert.True(testLienDetails.CostAdded == 0);
				Assert.True(testLienDetails.CostDue == ((decimal)testLien.Costs - (decimal)testLien.CostPaid));
				Assert.True(testLienDetails.CostOwed == (decimal)testLien.Costs);
				Assert.True(testLienDetails.CostPaid == (decimal)testLien.CostPaid);
				Assert.True(testLienDetails.CurrentInterest == 0);
				Assert.True(testLienDetails.DemandGroupId == 0);
				Assert.True(testLienDetails.IntAdded == (decimal)testLien.IntAdded);
				Assert.True(testLienDetails.IntOwed == (decimal)testLien.Interest);
				Assert.True(testLienDetails.IntPaid == (decimal)testLien.IntPaid);
				Assert.True(testLienDetails.InterestDue == (decimal)testLien.Interest - (decimal)testLien.IntAdded - (decimal)testLien.IntPaid);
				Assert.True(testLienDetails.LienId == 0);
				Assert.True(testLienDetails.LastInterestDate == testRateKeyLien.IntStart);
				Assert.True(testLienDetails.PreLienInterestPaid == 0);
				Assert.True(testLienDetails.MaturityFee == 0);
				Assert.True(testLienDetails.PendingChargedInterest == 0);
				Assert.True(testLienDetails.PendingCostAmount == 0);
				Assert.True(testLienDetails.PendingPrincipalAmount == 0);
				Assert.True(testLienDetails.PendingCurrentInterestAmount == 0);
				Assert.True(testLienDetails.PendingPreLienInterestAmount == 0);
				Assert.True(testLienDetails.PendingTaxAmount == 0);
				Assert.True(testLienDetails.PerDiem == 0);
				Assert.True(testLienDetails.PrincipalDue == (decimal)testLien.Principal - (decimal)testLien.PrinPaid);
				Assert.True(testLienDetails.PrincipalOwed == (decimal)testLien.Principal);
				Assert.True(testLienDetails.PrincipalPaid == (decimal)testLien.PrinPaid);
				Assert.True(testLienDetails.Payments.Count == 0);
				Assert.True(testLienDetails.PreviousInterestDate == null);
				Assert.True(testLienDetails.RateCreatedDate == testRateKeyLien.DateCreated);
				Assert.True(testLienDetails.RateEnd == testRateKeyLien.End);
				Assert.True(testLienDetails.RateStart == testRateKeyLien.Start);
				Assert.True(testLienDetails.SavedPreviousInterestDate == null);
				Assert.True(testLienDetails.Service == UtilityType.Water);
				Assert.True(testLienDetails.ServiceCode == "W");


			}
		}

		[Fact]
		public void ReturnCorrectBillAndLienPaymentWithChargedInterest()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
			var billCalculationService = Substitute.For<IBillCalculationService>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testName = "John Doe";
				string testOwnerName = "Billy Bob";
				int testAccountNumber = 999;
				int testAccountId = 1;
				string testAddress = "10 Noeth Main St";
				string testCity = "Bangor";
				string testState = "ME";
				string testZip = "02983";
				Guid testTransactionIdentifier = Guid.NewGuid();

				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					DateCreated = DateTime.Today.AddMonths(-6),
					Description = "Test Rate",
					Start = DateTime.Today.AddMonths(-12),
					End = DateTime.Today.AddMonths(-9),
					IntStart = DateTime.Today,
					RateType = "B",
					SIntRate = .1,
					WIntRate = .2,
				};

				var testRateKeyLien = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					DateCreated = DateTime.Today.AddMonths(-6),
					Description = "Test Lien Rate",
					Start = DateTime.Today.AddMonths(-12),
					End = DateTime.Today.AddMonths(-9),
					IntStart = DateTime.Today,
					RateType = "L",
					SIntRate = .1,
					WIntRate = .2,
				};

				utilityBillingContext.RateKeys.Add(testRateKey);
				utilityBillingContext.RateKeys.Add(testRateKeyLien);
				utilityBillingContext.SaveChanges();

				var testLien = new Lien
				{
					Book = 1,
					Page = 2,
					CostPaid = 0,
					Costs = 23.45,
					Interest = 10,
					IntAdded = -19.11,
					IntPaid = 0,
					IntPaidDate = null,
					MaturityFee = 0,
					PLIPaid = 0,
					PreviousInterestPaidDate = null,
					PrinPaid = 0,
					Principal = 100,
					RateKeyId = testRateKeyLien.ID,
					Status = "A",
					Tax = 14,
					PrintedLDN = false,
					TaxPaid = 0,
					Water = true,
				};

				utilityBillingContext.Liens.Add(testLien);
				utilityBillingContext.SaveChanges();

				var testBill = new Bill
				{
					AccountId = testAccountId,
					ActualAccountNumber = testAccountNumber,
					BAddress1 = testAddress,
					BAddress2 = "",
					BAddress3 = "",
					BCity = testCity,
					BState = testState,
					BZip = testZip,
					BZip4 = "",
					BName = testName,
					BName2 = "",
					BillNumber = testRateKey.ID,
					BillDate = testRateKey.BillDate,
					BillStatus = "A",
					Service = "W",
					OName = testOwnerName,
					WLienRecordNumber = testLien.ID,
					WIntPaidDate = null,
					WPreviousInterestPaidDate = null,
					WCostAdded = -10,
					WIntOwed = 15,
					WCostPaid = 10,
					WBillOwner = false,
					WCostOwed = 10,
					WIntAdded = -5,
					WIntPaid = 20,
					WPrinOwed = 200,
					WPrinPaid = 100,
					WTaxOwed = 50,
					WTaxPaid = 50,
				};

				utilityBillingContext.Bills.Add(testBill);
				utilityBillingContext.SaveChanges();

				testBill.WCombinationLienKey = testBill.ID;
				utilityBillingContext.SaveChanges();

				var testPayment = new PaymentRec
				{
					AccountKey = testAccountId,
					LienId = testLien.ID,
					ActualSystemDate = DateTime.Today,
					CashDrawer = "Y",
					Comments = "TEST COMMENT",
					Code = "P",
					EffectiveInterestDate = DateTime.Today,
					GeneralLedger = "Y",
					PreLienInterest = 10,
					CurrentInterest = 10,
					Tax = 50,
					Principal = 100,
					LienCost = 10,
					LienPayment = true,
					Service = "W",
					TransactionIdentifier = testTransactionIdentifier
				};

				var testChargedInterest = new PaymentRec
				{
					AccountKey = testAccountId,
					LienId = testLien.ID,
					ActualSystemDate = DateTime.Today,
					CashDrawer = "Y",
					Comments = "TEST COMMENT",
					Code = "I",
					EffectiveInterestDate = DateTime.Today,
					GeneralLedger = "Y",
					PreLienInterest = 0,
					CurrentInterest = -10,
					Reference = "CHGINT",
					CHGINTDate = DateTime.Today,
					Tax = 0,
					Principal = 0,
					LienCost = 0,
					LienPayment = true,
					Service = "W",
					TransactionIdentifier = testTransactionIdentifier
				};

				utilityBillingContext.PaymentRecs.Add(testPayment);
				utilityBillingContext.PaymentRecs.Add(testChargedInterest);
				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new UBBillQueryHandler(trioContextFactory, billCalculationService);

				var result = service.ExecuteQuery(new UBBillSearchCriteria
				{
					AccountId = testAccountId,
					VoidedSelection = VoidedStatusSelection.NotVoided
				});

				var testResult = result.FirstOrDefault(x => x.IsLien == false);
				var testDetails = testResult.UtilityDetails.FirstOrDefault();

				var testLienResult = result.FirstOrDefault(x => x.IsLien);
				var testLienDetails = testLienResult.UtilityDetails.FirstOrDefault();
				var resultPayment = testLienDetails.Payments.FirstOrDefault();
				var resultChargedInterest = resultPayment.ChargedInterestRecord;


				Assert.True(resultChargedInterest != null);

				Assert.True(resultChargedInterest.ActualSystemDate == testChargedInterest.ActualSystemDate);
				Assert.True(resultChargedInterest.BillId == testChargedInterest.LienId);
				Assert.True(resultChargedInterest.CashDrawer == testChargedInterest.CashDrawer);
				Assert.True(resultChargedInterest.ChargedInterestDate == testChargedInterest.CHGINTDate);
				Assert.True(resultChargedInterest.Code == testChargedInterest.Code);
				Assert.True(resultChargedInterest.Reference == testChargedInterest.Reference);
				Assert.True(resultChargedInterest.Comments == testChargedInterest.Comments);
				Assert.True(resultChargedInterest.Cost == testChargedInterest.LienCost);
				Assert.True(resultChargedInterest.CurrentInterest == testChargedInterest.CurrentInterest);
				Assert.True(resultChargedInterest.PreLienInterest == testChargedInterest.PreLienInterest);

				Assert.True(resultChargedInterest.EffectiveInterestDate == testChargedInterest.EffectiveInterestDate);
				Assert.True(resultChargedInterest.GeneralLedger == testChargedInterest.GeneralLedger);
				Assert.True(resultChargedInterest.InterestTotal == testChargedInterest.CurrentInterest + testChargedInterest.PreLienInterest);
				Assert.True(resultChargedInterest.Tax == testChargedInterest.Tax);
				Assert.True(resultChargedInterest.Principal == testChargedInterest.Principal);
				Assert.True(resultChargedInterest.Pending == false);
				Assert.True(resultChargedInterest.TransactionId == testChargedInterest.TransactionIdentifier);

				Assert.True(result.Count() == 2);
				Assert.True(testResult.IsLien == false);
				Assert.True(testResult.BillPayments(UtilityType.All).Count == 0);
				Assert.True(testResult.UtilityDetails.Count == 1);

				Assert.True(testDetails.IsLiened == true);
				Assert.True(testDetails.LienMainBill == true);
				Assert.True(testDetails.LienId == testLien.ID);

				Assert.True(testLienResult.BillPayments(UtilityType.All).Count == 1);
				Assert.True(testLienResult.UtilityDetails.Count == 1);

				Assert.True(testLienDetails.Payments.Count == 1);

				Assert.True(resultPayment.IsReversal == false);
				Assert.True(resultPayment.ActualSystemDate == testPayment.ActualSystemDate);
				Assert.True(resultPayment.BillId == testPayment.LienId);
				Assert.True(resultPayment.BudgetaryAccountNumber == testPayment.BudgetaryAccountNumber);
				Assert.True(resultPayment.CashDrawer == testPayment.CashDrawer);
				Assert.True(resultPayment.ChargedInterestDate == testPayment.CHGINTDate);
				Assert.True(resultPayment.Code == testPayment.Code);
				Assert.True(resultPayment.Comments == testPayment.Comments);
				Assert.True(resultPayment.Cost == testPayment.LienCost);
				Assert.True(resultPayment.CurrentInterest == testPayment.CurrentInterest);
				Assert.True(resultPayment.PreLienInterest == testPayment.PreLienInterest);

				Assert.True(resultPayment.EffectiveInterestDate == testPayment.EffectiveInterestDate);
				Assert.True(resultPayment.GeneralLedger == testPayment.GeneralLedger);
				Assert.True(resultPayment.InterestTotal == testPayment.CurrentInterest + testPayment.PreLienInterest);
				Assert.True(resultPayment.Tax == testPayment.Tax);
				Assert.True(resultPayment.Principal == testPayment.Principal);
				Assert.True(resultPayment.Pending == false);
				Assert.True(resultPayment.TransactionId == testPayment.TransactionIdentifier);

			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
