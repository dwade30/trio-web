﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Models;
using SharedDataAccess;
using SharedDataAccess.UtilityBilling;
using Xunit;

namespace SharedDataAccessTests.UtilityBilling
{
	public class CustomerPartyQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingName()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testName = "Will Wallace";
				int testAccountNumber = 999;

				utilityBillingContext.CustomerParties.Add(new CustomerParty
				{
					FullNameLF = testName,
					AccountNumber = testAccountNumber,
					Deleted = false
				});

				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new CustomerPartyQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerPartySearchCriteria
				{
					Name = testName
				}).FirstOrDefault();

				Assert.True(result.FullNameLF == testName);
				Assert.True(result.AccountNumber == testAccountNumber);
			}
		}

		[Fact]
		public void ReturnCorrectCustomerInfoForMatchingAddress()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var utilityBillingContext = new UtilityBillingContext(GetDBOptions<UtilityBillingContext>()))
			{
				string testName = "Will Wallace";
				string testAddress = "10 North Main St";
				int testAccountNumber = 999;

				utilityBillingContext.CustomerParties.Add(new CustomerParty
				{
					FullNameLF = testName,
					BilledAddress1 = testAddress,
					AccountNumber = testAccountNumber,
					Deleted = false,
					Comment = "",
					Zip = "",
					Address1 = "",
					Address2 = "",
					Address3 = "",
					Apt = "",
					BillMessage = "",
					Billed2NameLF = "",
					BilledAddress2 = "",
					BilledAddress3 = "",
					BilledCity = "",
					BilledCountry = "",
					BilledNameLF = "",
					BilledState = "",
					BilledZip = "",
					BillingPartyID = 0,
					Book = 0,
					BookPage = "",
					City = "",
					Code = 0,
					Country = "",
					DataEntry = "",
					DeedName1 = "",
					DeedName2 = "",
					Deposit = 0,
					Designation = "",
					Directions = "",
					Email = "",
					EmailBill = false,
					FinalBill = false,
					FirstName = "",
					FullName = "",
					ImpervSurfArea = 0,
					InBankruptcy = false,
					LastName = "",
					MapLot = "",
					MiddleName = "",
					NoBill = false,
					OverrideName = "",
					Owner2FullNameLF = "",
					OwnerPartyID = 0,
					Page = 0,
					REAccount = 0,
					RefAccountNumber = "",
					SewerAccount = "",
					State = "",
					StreetName = "",
					StreetNumber = "",
					Telephone = "",
					WaterAccount = "",
					WebAddress = ""
				});

				utilityBillingContext.SaveChanges();

				trioContextFactory.GetUtilityBillingContext().Returns(utilityBillingContext);

				var service = new CustomerPartyQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CustomerPartySearchCriteria
				{
					Address = "10 N"
				}).FirstOrDefault();

				Assert.True(result.FullNameLF == testName);
				Assert.True(result.BilledAddress1 == testAddress);
				Assert.True(result.AccountNumber == testAccountNumber);
			}
		}

		
		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
