﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedDataAccess;
using SharedDataAccess.AccountsReceivable;
using Xunit;

namespace SharedDataAccessTests
{
	public class ARBillQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectInformationForExistingBill()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
            var paymentService = Substitute.For<IPaymentService>();

			int testAccountNumber = 10;
			string testName = "Test Name";

			using (var accountsReceivableDataContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					FlatRate = 0,
					IntRate = .2,
					InterestMethod = "AP",
					IntStart = DateTime.Today.AddMonths(-2),
				};

				accountsReceivableDataContext.RateKeys.Add(testRateKey);

				accountsReceivableDataContext.CustomerMasters.Add(new CustomerMaster
				{
					CustomerId = testAccountNumber,
				});

				accountsReceivableDataContext.SaveChanges();

				var testPayment = new PaymentRec
				{
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					ActualAccountNumber = testAccountNumber,
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					Interest = 5,
					Tax = 50,
					Principal = 50,
					ReceiptNumber = 20,
					RecordedTransactionDate = DateTime.Today.AddMonths(-1),
					ActualSystemDate = DateTime.Today.AddMonths(-1)
				};

				var testBill = new Bill
				{
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					BillStatus = "A",
					Bname = testName,
					ActualAccountNumber = testAccountNumber,
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					IntAdded = -10,
					PrinOwed = 200,
					TaxOwed = 100,
					PrinPaid = 50,
					TaxPaid = 50,
					IntPaid = 5,
					IntPaidDate = DateTime.Today.AddMonths(-1),
					Payments = new List<PaymentRec>
					{
						testPayment
					}
				};

				accountsReceivableDataContext.Bills.Add(testBill);

				accountsReceivableDataContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableDataContext);

				var service = new ARBillQueryHandler(trioContextFactory, paymentService);

				var result = service.ExecuteQuery(new ARBillSearchCriteria
				{
					CustomerId = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id
				}).FirstOrDefault();

				Assert.True(result.BillDate == testRateKey.BillDate);
				Assert.True(result.BillNumber == testBill.BillNumber);
				Assert.True(result.BillStatus == testBill.BillStatus);
				Assert.True(result.Bname == testBill.Bname);
				Assert.True(result.CurrentInterest == 0);
				Assert.True(result.CurrentInterestDate == null);
				Assert.True(result.FlatRate == testRateKey.FlatRate);
				Assert.True(result.InterestRate == testRateKey.IntRate);
				Assert.True(result.InterestMethod == testRateKey.InterestMethod);
				Assert.True(result.IntAdded == testBill.IntAdded);
				Assert.True(result.IntPaid == testBill.IntPaid);
				Assert.True(result.PrinOwed == testBill.PrinOwed);
				Assert.True(result.PrinPaid == testBill.PrinPaid);
				Assert.True(result.TaxOwed == testBill.TaxOwed);
				Assert.True(result.TaxPaid == testBill.TaxPaid);
				Assert.True(result.IntPaid == testBill.IntPaid);
				Assert.True(result.IntPaidDate == testBill.IntPaidDate);
				Assert.True(result.LastInterestDate == testBill.IntPaidDate);
				Assert.True(result.PerDiem == 0);
				Assert.True(result.Payments.Count() == 1);
				Assert.True(result.Payments.FirstOrDefault().Interest == testPayment.Interest);
				Assert.True(result.Payments.FirstOrDefault().Principal == testPayment.Principal);
				Assert.True(result.Payments.FirstOrDefault().ReceiptId == testPayment.ReceiptNumber);
				Assert.True(result.Payments.FirstOrDefault().RecordedTransactionDate == testPayment.RecordedTransactionDate);
				Assert.True(result.Payments.FirstOrDefault().Tax == testPayment.Tax);
				Assert.True(result.Payments.FirstOrDefault().ActualSystemDate == testPayment.ActualSystemDate);
				
			}
		}

		[Fact]
		public void ReturnCorrectInformationForExistingBillWithChargedInterest()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
            var paymentService = Substitute.For<IPaymentService>();

			int testAccountNumber = 10;
			string testName = "Test Name";
			Guid testTransactionId = Guid.NewGuid();

			using (var accountsReceivableDataContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					FlatRate = 0,
					IntRate = .2,
					InterestMethod = "AP",
					IntStart = DateTime.Today.AddMonths(-2),
				};

				accountsReceivableDataContext.RateKeys.Add(testRateKey);

				accountsReceivableDataContext.CustomerMasters.Add(new CustomerMaster
				{
					CustomerId = testAccountNumber,
				});

				accountsReceivableDataContext.SaveChanges();

				var testPayment = new PaymentRec
				{
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					ActualAccountNumber = testAccountNumber,
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					Interest = 5,
					Tax = 50,
					Principal = 50,
					ReceiptNumber = 20,
					RecordedTransactionDate = DateTime.Today.AddMonths(-1),
					TransactionIdentifier = testTransactionId,
					ActualSystemDate = DateTime.Today.AddMonths(-1)
				};

				var testChargedInterest = new PaymentRec
				{
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					ActualAccountNumber = testAccountNumber,
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					Interest = -10,
					Tax = 0,
					Principal = 0,
					ReceiptNumber = 20,
					Code = "I",
					Reference = "CHGINT",
					RecordedTransactionDate = DateTime.Today.AddMonths(-1),
					TransactionIdentifier = testTransactionId,
					ActualSystemDate = DateTime.Today.AddMonths(-1),
					Chgintdate = DateTime.Today.AddMonths(-1)
				};

				var testBill = new Bill
				{
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					BillStatus = "A",
					Bname = testName,
					ActualAccountNumber = testAccountNumber,
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					IntAdded = -10,
					PrinOwed = 200,
					TaxOwed = 100,
					PrinPaid = 50,
					TaxPaid = 50,
					IntPaid = 5,
					IntPaidDate = DateTime.Today.AddMonths(-1),
					Payments = new List<PaymentRec>
					{
						testPayment,
						testChargedInterest
					}
				};

				accountsReceivableDataContext.Bills.Add(testBill);

				accountsReceivableDataContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableDataContext);

				var service = new ARBillQueryHandler(trioContextFactory, paymentService);

				var result = service.ExecuteQuery(new ARBillSearchCriteria
				{
					CustomerId = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id
				}).FirstOrDefault();

				Assert.True(result.BillDate == testRateKey.BillDate);
				Assert.True(result.BillNumber == testBill.BillNumber);
				Assert.True(result.BillStatus == testBill.BillStatus);
				Assert.True(result.Bname == testBill.Bname);
				Assert.True(result.CurrentInterest == 0);
				Assert.True(result.CurrentInterestDate == null);
				Assert.True(result.FlatRate == testRateKey.FlatRate);
				Assert.True(result.InterestRate == testRateKey.IntRate);
				Assert.True(result.InterestMethod == testRateKey.InterestMethod);
				Assert.True(result.IntAdded == testBill.IntAdded);
				Assert.True(result.IntPaid == testBill.IntPaid);
				Assert.True(result.PrinOwed == testBill.PrinOwed);
				Assert.True(result.PrinPaid == testBill.PrinPaid);
				Assert.True(result.TaxOwed == testBill.TaxOwed);
				Assert.True(result.TaxPaid == testBill.TaxPaid);
				Assert.True(result.IntPaid == testBill.IntPaid);
				Assert.True(result.IntPaidDate == testBill.IntPaidDate);
				Assert.True(result.LastInterestDate == testBill.IntPaidDate);
				Assert.True(result.PerDiem == 0);
				Assert.True(result.Payments.Count() == 1);
				Assert.True(result.Payments.FirstOrDefault().Interest == testPayment.Interest);
				Assert.True(result.Payments.FirstOrDefault().Principal == testPayment.Principal);
				Assert.True(result.Payments.FirstOrDefault().ReceiptId == testPayment.ReceiptNumber);
				Assert.True(result.Payments.FirstOrDefault().RecordedTransactionDate == testPayment.RecordedTransactionDate);
				Assert.True(result.Payments.FirstOrDefault().Tax == testPayment.Tax);
				Assert.True(result.Payments.FirstOrDefault().ActualSystemDate == testPayment.ActualSystemDate);
				Assert.True(result.Payments.FirstOrDefault().ChargedInterestRecord.Code == "I");
				Assert.True(result.Payments.FirstOrDefault().ChargedInterestRecord.Reference == "CHGINT");
				Assert.True(result.Payments.FirstOrDefault().ChargedInterestRecord.Interest == -10);
				Assert.True(result.Payments.FirstOrDefault().ChargedInterestRecord.ChargedInterestDate ==testChargedInterest.Chgintdate);

			}
		}

		[Fact]
		public void ReturnCorrectLastInterestDateForExistingBillNoIntPaid()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
            var paymentService = Substitute.For<IPaymentService>();

			int testAccountNumber = 10;
			string testName = "Test Name";

			using (var accountsReceivableDataContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				var testRateKey = new RateKey
				{
					BillDate = DateTime.Today.AddMonths(-6),
					FlatRate = 0,
					IntRate = .2,
					InterestMethod = "AP",
					IntStart = DateTime.Today.AddMonths(-2),
				};

				accountsReceivableDataContext.RateKeys.Add(testRateKey);

				accountsReceivableDataContext.CustomerMasters.Add(new CustomerMaster
				{
					CustomerId = testAccountNumber,
				});

				accountsReceivableDataContext.SaveChanges();

				var testPayment = new PaymentRec
				{
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					ActualAccountNumber = testAccountNumber,
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					Interest = 0,
					Tax = 50,
					Principal = 50,
					ReceiptNumber = 20,
					RecordedTransactionDate = DateTime.Today.AddMonths(-1),
					ActualSystemDate = DateTime.Today.AddMonths(-1)
				};

				var testBill = new Bill
				{
					BillNumber = accountsReceivableDataContext.RateKeys.FirstOrDefault().Id,
					BillStatus = "A",
					Bname = testName,
					ActualAccountNumber = testAccountNumber,
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					IntAdded = 0,
					PrinOwed = 200,
					TaxOwed = 100,
					PrinPaid = 50,
					TaxPaid = 50,
					IntPaid = 0,
					IntPaidDate = null,
					Payments = new List<PaymentRec>
					{
						testPayment
					}
				};

				accountsReceivableDataContext.Bills.Add(testBill);

				accountsReceivableDataContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableDataContext);

				var service = new ARBillQueryHandler(trioContextFactory, paymentService);

				var result = service.ExecuteQuery(new ARBillSearchCriteria
				{
					CustomerId = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id
				}).FirstOrDefault();

				Assert.True(result.LastInterestDate == testRateKey.IntStart);
			}
		}

		[Fact]
		public void ReturnCorrectInformationForExistingBillNoRateKey()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();
            var paymentService = Substitute.For<IPaymentService>();

			int testAccountNumber = 10;
			string testName = "Test Name";

			using (var accountsReceivableDataContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{

				accountsReceivableDataContext.CustomerMasters.Add(new CustomerMaster
				{
					CustomerId = testAccountNumber,
				});

				accountsReceivableDataContext.SaveChanges();

				var testPayment = new PaymentRec
				{
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					ActualAccountNumber = testAccountNumber,
					BillNumber = 0,
					Interest = 0,
					Tax = 50,
					Principal = 50,
					ReceiptNumber = 20,
					RecordedTransactionDate = DateTime.Today.AddMonths(-1),
					ActualSystemDate = DateTime.Today.AddMonths(-1)
				};

				var testBill = new Bill
				{
					BillNumber = 0,
					BillStatus = "A",
					Bname = testName,
					ActualAccountNumber = testAccountNumber,
					AccountKey = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id,
					IntAdded = 0,
					PrinOwed = 200,
					TaxOwed = 100,
					PrinPaid = 50,
					TaxPaid = 50,
					IntPaid = 0,
					IntPaidDate = null,
					Payments = new List<PaymentRec>
					{
						testPayment
					}
				};

				accountsReceivableDataContext.Bills.Add(testBill);

				accountsReceivableDataContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableDataContext);

				var service = new ARBillQueryHandler(trioContextFactory, paymentService);

				var result = service.ExecuteQuery(new ARBillSearchCriteria
				{
					CustomerId = accountsReceivableDataContext.CustomerMasters.FirstOrDefault().Id
				}).FirstOrDefault();

				Assert.True(result.BillDate == null);
				Assert.True(result.BillNumber == testBill.BillNumber);
				Assert.True(result.BillStatus == testBill.BillStatus);
				Assert.True(result.Bname == testBill.Bname);
				Assert.True(result.CurrentInterest == 0);
				Assert.True(result.CurrentInterestDate == null);
				Assert.True(result.FlatRate == 0);
				Assert.True(result.InterestRate == 0);
				Assert.True(result.InterestMethod == "");
				Assert.True(result.IntAdded == testBill.IntAdded);
				Assert.True(result.IntPaid == testBill.IntPaid);
				Assert.True(result.PrinOwed == testBill.PrinOwed);
				Assert.True(result.PrinPaid == testBill.PrinPaid);
				Assert.True(result.TaxOwed == testBill.TaxOwed);
				Assert.True(result.TaxPaid == testBill.TaxPaid);
				Assert.True(result.IntPaid == testBill.IntPaid);
				Assert.True(result.IntPaidDate == null);
				Assert.True(result.LastInterestDate == null);
				Assert.True(result.PerDiem == 0);
				Assert.True(result.Payments.Count() == 1);
				Assert.True(result.Payments.FirstOrDefault().Interest == testPayment.Interest);
				Assert.True(result.Payments.FirstOrDefault().Principal == testPayment.Principal);
				Assert.True(result.Payments.FirstOrDefault().ReceiptId == testPayment.ReceiptNumber);
				Assert.True(result.Payments.FirstOrDefault().RecordedTransactionDate == testPayment.RecordedTransactionDate);
				Assert.True(result.Payments.FirstOrDefault().Tax == testPayment.Tax);
				Assert.True(result.Payments.FirstOrDefault().ActualSystemDate == testPayment.ActualSystemDate);

			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
