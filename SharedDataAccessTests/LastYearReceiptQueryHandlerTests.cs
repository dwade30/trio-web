﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedDataAccess;
using SharedDataAccess.CashReceipts;
using Xunit;

namespace SharedDataAccessTests
{
	public class LastYearReceiptQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectInfoForExistingReceipt()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testReceiptId = 20;
			int testReceiptKey = 30;
			int testReceiptNumber = 100;

			using (var cashReceiptsDataContext = new CashReceiptsContext(GetDBOptions<CashReceiptsContext>()))
			{
				cashReceiptsDataContext.LastYearReceipts.Add(new LastYearReceipt
				{
					Id = testReceiptId,
					ReceiptKey = testReceiptKey,
					ReceiptNumber = testReceiptNumber,
					LastYearReceiptDate = DateTime.Today
				});

				cashReceiptsDataContext.SaveChanges();

				trioContextFactory.GetCashReceiptsContext().Returns(cashReceiptsDataContext);

				var service = new LastYearReceiptQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new LastYearReceiptSearchCriteria
				{
					ReceiptId = testReceiptId,
					ReceiptDate = DateTime.Today
				}).FirstOrDefault();

				Assert.True(result != null);
				Assert.True(result.ReceiptNumber == testReceiptNumber);
			}
		}

		[Fact]
		public void ReturnCorrectInfoForExistingReceiptNotSameDateWithNoDateCheck()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testReceiptId = 20;
			int testReceiptKey = 30;
			int testReceiptNumber = 100;

			using (var cashReceiptsDataContext = new CashReceiptsContext(GetDBOptions<CashReceiptsContext>()))
			{
				cashReceiptsDataContext.LastYearReceipts.Add(new LastYearReceipt
				{
					Id = testReceiptId,
					ReceiptKey = testReceiptKey,
					ReceiptNumber = testReceiptNumber,
					LastYearReceiptDate = DateTime.Today.AddDays(-10)
				});

				cashReceiptsDataContext.SaveChanges();

				trioContextFactory.GetCashReceiptsContext().Returns(cashReceiptsDataContext);

				var service = new LastYearReceiptQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new LastYearReceiptSearchCriteria
				{
					ReceiptId = testReceiptId,
				}).FirstOrDefault();

				Assert.True(result != null);
				Assert.True(result.ReceiptNumber == testReceiptNumber);
			}
		}

		[Fact]
		public void ReturnCorrectInfoForExistingReceiptNotSameIdNoIdCheck()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testReceiptId = 20;
			int testReceiptKey = 30;
			int testReceiptNumber = 100;

			using (var cashReceiptsDataContext = new CashReceiptsContext(GetDBOptions<CashReceiptsContext>()))
			{
				cashReceiptsDataContext.LastYearReceipts.Add(new LastYearReceipt
				{
					Id = testReceiptId,
					ReceiptKey = testReceiptKey,
					ReceiptNumber = testReceiptNumber,
					LastYearReceiptDate = DateTime.Today
				});

				cashReceiptsDataContext.SaveChanges();

				trioContextFactory.GetCashReceiptsContext().Returns(cashReceiptsDataContext);

				var service = new LastYearReceiptQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new LastYearReceiptSearchCriteria
				{
					ReceiptDate = DateTime.Today
				}).FirstOrDefault();

				Assert.True(result != null);
				Assert.True(result.ReceiptNumber == testReceiptNumber);
			}
		}

		[Fact]
		public void ReturnCorrectInfoForNoReceipt()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			int testReceiptId = 20;
			int testReceiptNumber = 100;

			using (var cashReceiptsDataContext = new CashReceiptsContext(GetDBOptions<CashReceiptsContext>()))
			{
				trioContextFactory.GetCashReceiptsContext().Returns(cashReceiptsDataContext);

				var service = new LastYearReceiptQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new LastYearReceiptSearchCriteria
				{
					ReceiptId = testReceiptId,
					ReceiptDate = DateTime.Today
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
