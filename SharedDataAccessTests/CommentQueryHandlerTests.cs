﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedDataAccess;
using SharedDataAccess.AccountsReceivable;
using Xunit;

namespace SharedDataAccessTests
{
	public class CommentQueryHandlerTests
	{
		[Fact]
		public void ReturnCorrectComment()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;

				accountsReceivableContext.Comments.Add(new Comment
				{
					Text = testComment,
					Account = testAccountNumber,
					Priority = testPriority
				});

				accountsReceivableContext.SaveChanges();

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CommentQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CommentSearchCriteria
				{
					CustomerId = testAccountNumber
				}).FirstOrDefault();

				Assert.True(result.Text == testComment);
				Assert.True(result.Account == testAccountNumber);
				Assert.True(result.Priority == testPriority);
			}
		}

		[Fact]
		public void ReturnCorrectNoComment()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var accountsReceivableContext = new AccountsReceivableContext(GetDBOptions<AccountsReceivableContext>()))
			{
				string testComment = "TEST COMMENT";
				int testAccountNumber = 999;
				int testPriority = 1;

				trioContextFactory.GetAccountsReceivableContext().Returns(accountsReceivableContext);

				var service = new CommentQueryHandler(trioContextFactory);

				var result = service.ExecuteQuery(new CommentSearchCriteria
				{
					CustomerId = testAccountNumber
				}).FirstOrDefault();

				Assert.True(result == null);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
