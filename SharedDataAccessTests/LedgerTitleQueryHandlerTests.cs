﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Models;
using SharedDataAccess;
using SharedDataAccess.Budgetary;
using Xunit;

namespace SharedDataAccessTests
{
	public class LedgerTitleQueryHandlerTests
	{
		[Fact]
		public void ExecuteQueryReturnsCorrectLedgerAccount()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.LedgerTitles.Add(new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "22",
					ShortDescription = "Fund Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new LedgerTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new LedgerTitleSearchCriteria
				{
					Fund = "10",
					Account = "100",
					Suffix = "22"
				});
				Assert.True(result.Count() == 1);
				Assert.True(result.First().ShortDescription == "Fund Short");
				Assert.True(result.First().Fund == "10");
				Assert.True(result.First().Account == "100");
				Assert.True(result.First().Year == "22");
			}
		}

		[Fact]
		public void ExecuteQueryReturnsAllWithNoCriteria()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.LedgerTitles.Add(new LedgerTitle
				{
					Fund = "10",
					Account = "000",
					Year = "",
					ShortDescription = "Fund Short"
				});

				budgetaryContext.LedgerTitles.Add(new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "99",
					ShortDescription = "Account Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new LedgerTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new LedgerTitleSearchCriteria
				{
					Fund = "",
					Account = ""
				});
				Assert.True(result.Count() == 2);
			}
		}

		[Fact]
		public void ExecuteQueryReturnsEmptyListOnNoMatch()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.LedgerTitles.Add(new LedgerTitle
				{
					Fund = "10",
					Account = "000",
					Year = "",
					ShortDescription = "Fund Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new LedgerTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new LedgerTitleSearchCriteria
				{
					Fund = "10",
					Account = "100"
				});
				Assert.True(result.Count() == 0);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
