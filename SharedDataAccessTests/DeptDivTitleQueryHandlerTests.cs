﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Models;
using SharedDataAccess;
using SharedDataAccess.Budgetary;
using Xunit;

namespace SharedDataAccessTests
{
	public class DeptDivTitleQueryHandlerTests
	{
		[Fact]
		public void ExecuteQueryReturnsCorrectExpenseAccount()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000",
					ShortDescription = "Dept Short"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100",
					ShortDescription = "Div Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new DeptDivTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new DeptDivTitleSearchCriteria
				{
					Department = "10",
					Division = "000"
				});
				Assert.True(result.Count() == 1);
				Assert.True(result.First().ShortDescription == "Dept Short");
				Assert.True(result.First().Department == "10");
				Assert.True(result.First().Division == "000");
			}
		}

		[Fact]
		public void ExecuteQueryReturnsEmptyListOnNoMatch()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000",
					ShortDescription = "Dept Short"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new DeptDivTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new DeptDivTitleSearchCriteria
				{
					Department = "10",
					Division = "100"
				});
				Assert.True(result.Count() == 0);
			}
		}

		[Fact]
		public void ExecuteQueryReturnsAllWithNoParameters()
		{
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000",
					ShortDescription = "Dept Short"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100",
					ShortDescription = "Div Short"
				});
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new DeptDivTitleQueryHandler(trioContextFactory);
				var result = service.ExecuteQuery(new DeptDivTitleSearchCriteria
				{
					Department = "",
					Division = ""
				});
				Assert.True(result.Count() == 2);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
