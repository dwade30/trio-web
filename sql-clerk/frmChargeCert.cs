﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmChargeCert : BaseForm
	{
		public frmChargeCert()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmChargeCert InstancePtr
		{
			get
			{
				return (frmChargeCert)Sys.GetInstance(typeof(frmChargeCert));
			}
		}

		protected frmChargeCert _InstancePtr = null;
		//=========================================================
		double dblFeeOrig;
		double dblFeeSubsequent;
		double dblFeeSecondCopies;
		double dblTotalFee;
		int intFirsts;
		int intSubsequents;
		int intSeconds;
		string strSecondDesc;
		string strSecondCopy;

		public double Init(double dblFeeFirst = 0, double dblFeeSubs = 0, int intNumFirsts = 1, int intNumSeconds = 0, string strSecondLabel = "Subsequent Copies (Same Day)", string strSecondCopyLabel = "", double dblFeeSecondCopy = 0, int intNumSecondCopies = 0)
		{
			double Init = 0;
			dblFeeOrig = dblFeeFirst;
			dblFeeSubsequent = dblFeeSubs;
			dblFeeSecondCopies = dblFeeSecondCopy;
			strSecondCopy = strSecondCopyLabel;
			intFirsts = intNumFirsts;
			intSubsequents = intNumSeconds;
			intSeconds = intNumSecondCopies;
			strSecondDesc = strSecondLabel;
			Init = 0;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = dblTotalFee;
			return Init;
		}

		private void frmChargeCert_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			txtFirst.Text = FCConvert.ToString(intFirsts);
			txtSubsequent.Text = FCConvert.ToString(intSubsequents);
			txtSecond.Text = FCConvert.ToString(intSeconds);
			lblFirst.Text = "X  " + Strings.Format(dblFeeOrig, "0.00");
			lblSubsequent.Text = "X  " + Strings.Format(dblFeeSubsequent, "0.00");
			lblSecond.Text = "X  " + Strings.Format(dblFeeSecondCopies, "0.00");
			lblSecondDesc.Text = strSecondDesc;
			lblSecondCopy.Text = strSecondCopy;
			if (strSecondCopy != string.Empty)
			{
				lblSecondCopy.Text = strSecondCopy;
				lblSecondCopy.Visible = true;
				txtSecond.Visible = true;
				lblSecond.Visible = true;
			}
			// Me.ZOrder 0  'this line crashes for some reason
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			dblTotalFee = dblFeeOrig * Conversion.Val(txtFirst.Text) + dblFeeSubsequent * Conversion.Val(txtSubsequent.Text) + dblFeeSecondCopies * Conversion.Val(txtSecond.Text);
			modCashReceiptingData.Statics.typeCRData.NumFirsts = FCConvert.ToInt32(Math.Round(Conversion.Val(txtFirst.Text)));
			modCashReceiptingData.Statics.typeCRData.NumSubsequents = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSubsequent.Text)));
			modCashReceiptingData.Statics.typeCRData.NumSeconds = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSecond.Text)));
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
