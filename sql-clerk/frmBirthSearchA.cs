//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using SharedApplication;
using SharedApplication.Clerk.Births;
using SharedApplication.Clerk.Commands;
using SharedApplication.Clerk.Enums;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCK0000
{
	public partial class frmBirthSearchA : BaseForm, IView<IBirthSearchViewModel>
    {
        private bool cancelling = true;
		public frmBirthSearchA()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmBirthSearchA(IBirthSearchViewModel viewModel) : this()
        {
            this.ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

			cmdSch = new System.Collections.Generic.List<FCButton>();
			cmdSch.AddControlArrayElement(cmdSch_1, 0);
			cmdSch.AddControlArrayElement(cmdSch_3, 1);
			cmdSch.AddControlArrayElement(cmdSch_4, 2);
			Text2 = new System.Collections.Generic.List<FCTextBox>();
			Text2.AddControlArrayElement(Text2_0, 0);
			Text2.AddControlArrayElement(Text2_1, 1);
			Text2.AddControlArrayElement(Text2_2, 2);
			Text2.AddControlArrayElement(Text2_3, 3);
			Check1 = new System.Collections.Generic.List<FCCheckBox>();
			Check1.AddControlArrayElement(Check1_0, 0);
			Check1.AddControlArrayElement(Check1_1, 1);
			Label2 = new System.Collections.Generic.List<FCLabel>();
            this.FormClosing += FrmBirthSearchA_FormClosing;
		}


        private void FrmBirthSearchA_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cancelling && ViewModel != null)
            {
                ViewModel.Cancel();
            }
        }
        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmBirthSearchA InstancePtr
		{
			get
			{
				return (frmBirthSearchA)Sys.GetInstance(typeof(frmBirthSearchA));
			}
		}

		protected frmBirthSearchA _InstancePtr = null;

		const int CNSTCHILDID = 0;
		const int CNSTCHILDLAST = 1;
		const int CNSTCHILDFIRST = 2;
		const int CNSTBIRTHPLACE = 3;
		const int CNSTFATHERLAST = 4;
		const int CNSTFATHERFIRST = 5;
		const int CNSTMOTHERLAST = 6;
		const int CNSTMOTHERFIRST = 7;
		const int CNSTMOTHERMAIDEN = 8;
		const int CNSTDESCRIPTION = 9;
		const int CNSTDEATHONFILE = 10;
		const int CNSTMARRIAGEONFILE = 11;
		clsDRWrapper rs = new clsDRWrapper();
		object temp;
		string lbl1 = "";
		string lbl2 = "";
		string lbl3 = "";
		string lbl4 = "";
		string lbl5 = "";
		string lbl6 = "";
		string lbl7 = "";
		string strSort = "";

		private void SetGridProperties()
		{
			lblLabel1.Text = lbl1;
			lblLabel2.Text = lbl2;
			lblLabel3.Text = lbl3;
			lblLabel4.Text = lbl4;
			lblLabel5.Text = lbl5;
			SearchGrid.Cols = 12;
			SearchGrid.ColHidden(CNSTCHILDID, true);
			// .ColWidth(cnstchildid) = 1
			SearchGrid.ColWidth(1, 2500);
			SearchGrid.ColWidth(2, 2000);
			SearchGrid.ColWidth(3, 2000);
			SearchGrid.ColWidth(4, 2000);
			SearchGrid.ColWidth(5, 2000);
			SearchGrid.ColWidth(6, 2000);
			SearchGrid.ColWidth(CNSTDEATHONFILE, 700);
			SearchGrid.ColWidth(CNSTMARRIAGEONFILE, 750);
			SearchGrid.ColDataType(CNSTDEATHONFILE, FCGrid.DataTypeSettings.flexDTBoolean);
			SearchGrid.ColDataType(CNSTMARRIAGEONFILE, FCGrid.DataTypeSettings.flexDTBoolean);
			//SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 11, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			SearchGrid.Rows = 1;
			SearchGrid.TextMatrix(0, CNSTCHILDID, FCConvert.ToString(1));
			SearchGrid.TextMatrix(0, CNSTCHILDLAST, "Last");
			SearchGrid.TextMatrix(0, CNSTCHILDFIRST, "First");
			SearchGrid.TextMatrix(0, CNSTFATHERFIRST, "F First");
			SearchGrid.TextMatrix(0, CNSTFATHERLAST, "Father Last");
			SearchGrid.TextMatrix(0, CNSTMOTHERLAST, "Mother Last");
			SearchGrid.TextMatrix(0, CNSTMOTHERFIRST, "M First");
			SearchGrid.TextMatrix(0, CNSTMOTHERMAIDEN, "Maiden");
			SearchGrid.TextMatrix(0, CNSTBIRTHPLACE, "Birthplace");
			// .TextMatrix(0, 1) = lbl1
			// .TextMatrix(0, 2) = lbl2
			// .TextMatrix(0, 3) = lbl3
			// .TextMatrix(0, 4) = lbl4
			// .TextMatrix(0, 5) = lbl5
			SearchGrid.TextMatrix(0, CNSTDESCRIPTION, "Type");
			SearchGrid.TextMatrix(0, CNSTDEATHONFILE, "Death");
			SearchGrid.TextMatrix(0, CNSTMARRIAGEONFILE, "Marriage");
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			int intResponse;
			modGNBas.Statics.Response = SearchGrid.TextMatrix(SearchGrid.Row, CNSTCHILDID);
			// Response is cleared out in the frmBirths.Show so I need
			// to have another variable
			intResponse = FCConvert.ToInt16(modGNBas.Statics.Response);
			if (fecherFoundation.Strings.UCase(SearchGrid.TextMatrix(SearchGrid.Row, CNSTDESCRIPTION)) == "LIVE BIRTHS")
			{
				if (SearchGrid.Row > 0)
				{
					modGNBas.Statics.gboolSearchPrint = true;
					modGNBas.Statics.BoolPrintFromSearch = true;
					//SearchGrid_DblClick(null, null);
                    if (SearchGrid.Row > 0)
                    {
                        Search(SearchGrid.Row);
                    }

                    // Call frmBirths.mnuPrintPreview_Click
					modGNBas.Statics.BoolPrintFromSearch = false;
					//frmBirths.InstancePtr.Unload();
				}
			}
			else if (fecherFoundation.Strings.UCase(SearchGrid.TextMatrix(SearchGrid.Row, CNSTDESCRIPTION)) == "DELAYED BIRTHS")
			{
				MessageBox.Show("Delayed Birth records cannot be printed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else if (fecherFoundation.Strings.UCase(SearchGrid.TextMatrix(SearchGrid.Row, CNSTDESCRIPTION)) == "FOREIGN BIRTHS")
			{
				MessageBox.Show("Foreign Birth records cannot be printed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("Birth record needs to be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void cmdSch_Click(int Index, object sender, System.EventArgs e)
		{
			object temp;
			int intID;
			switch (Index)
			{
				//case 0:
				//	{
				//		rs = null;
				//		frmBirthSearchA.InstancePtr.Close();
				//		break;
				//	}
				case 0:
					{
						SearchBth();
						break;
					}
				case 1:
					{
						clrForm();
						break;
					}
				case 2:
					{
						//SearchGrid_DblClick(null, null);
                        if (SearchGrid.Row > 0)
                        {
                            Search(SearchGrid.Row);
                        }
                        break;
					}
			}
			//end switch
		}

		private void cmdSch_Click(object sender, System.EventArgs e)
		{
			int index = cmdSch.GetIndex((FCButton)sender);
			cmdSch_Click(index, sender, e);
		}
		private void frmBirthSearchA_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
			this.Text2[0].Focus();
		}

		private void frmBirthSearchA_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmBirthSearchA_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				KeyAscii = KeyAscii - 32;
			}
			if (KeyAscii == Keys.Return)
				Support.SendKeys("{TAB}", false);
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmBirthSearchA_Load(object sender, System.EventArgs e)
		{

			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCMaskedTextBox)
				{
					(obj as FCMaskedTextBox).Mask = "##/##/####";
					(obj as FCMaskedTextBox).SelectionStart = 0;
				}
			}
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this, false);

			if (modCashReceiptingData.Statics.bolFromWindowsCR)
			{
				cmdPrint.Visible = false;
			}
			//if (modGNBas.Statics.gboolSearchType == "BIRTH")
			//{
				lbl1 = "Child's Name";
				lbl2 = "Birth Place";
				lbl3 = "Father's Name";
				lbl4 = "Mother's Name";
				lbl5 = "Maiden Name";
			//}
			SetGridProperties();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			
		}

		private void Check1_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			meBox1_0.Mask = "";
			meBox1_0.Text = "";
			meBox1_0.Mask = "##/##/####";
			meBox1_1.Mask = "";
			meBox1_1.Text = "";
			meBox1_1.Mask = "##/##/####";
			switch (Index)
			{
				case 0:
					{
						if (Check1[Index].CheckState == Wisej.Web.CheckState.Checked)
						{
							Check1[1].CheckState = Wisej.Web.CheckState.Unchecked;
							meBox1_0.Enabled = true;
							meBox1_1.Enabled = false;
						}
						else if (Check1[1].CheckState == Wisej.Web.CheckState.Checked)
						{
							Check1[Index].CheckState = Wisej.Web.CheckState.Unchecked;
							meBox1_0.Enabled = true;
							meBox1_1.Enabled = true;
						}
						else if (Check1[0].CheckState == Wisej.Web.CheckState.Unchecked && Check1[1].CheckState == Wisej.Web.CheckState.Unchecked)
						{
							meBox1_0.Enabled = false;
							meBox1_1.Enabled = false;
						}
						break;
					}
				case 1:
					{
						if (Check1[Index].CheckState == Wisej.Web.CheckState.Checked)
						{
							Check1[0].CheckState = Wisej.Web.CheckState.Unchecked;
							meBox1_0.Enabled = true;
							meBox1_1.Enabled = true;
						}
						else if (Check1[0].CheckState == Wisej.Web.CheckState.Checked)
						{
							Check1[Index].CheckState = Wisej.Web.CheckState.Unchecked;
							meBox1_0.Enabled = true;
							meBox1_1.Enabled = false;
						}
						else if (Check1[0].CheckState == Wisej.Web.CheckState.Unchecked && Check1[1].CheckState == Wisej.Web.CheckState.Unchecked)
						{
							meBox1_0.Enabled = false;
							meBox1_1.Enabled = false;
						}
						break;
					}
			}
			//end switch
			if (Check1[0].CheckState == CheckState.Checked | Check1[1].CheckState == Wisej.Web.CheckState.Checked)
				meBox1_0.Focus();
		}

		private void Check1_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = Check1.GetIndex((FCCheckBox)sender);
			Check1_CheckedChanged(index, sender, e);
		}

		private void SearchGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (SearchGrid.Rows > 2)
			{
				if (SearchGrid.MouseRow == 0)
				{
					// sort this grid
					SearchGrid.Select(1, SearchGrid.Col);
					if (strSort == "flexSortGenericAscending")
					{
						SearchGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						strSort = "flexSortGenericDescending";
					}
					else
					{
						SearchGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						strSort = "flexSortGenericAscending";
					}
				}
				else
				{
					if (SearchGrid.Rows > 1)
					{
						if (SearchGrid.Row > 0)
						{
							Text2[0].Text = SearchGrid.TextMatrix(SearchGrid.Row, CNSTCHILDLAST);
							txtChildFirstName.Text = SearchGrid.TextMatrix(SearchGrid.Row, CNSTCHILDFIRST);
							Text2[3].Text = SearchGrid.TextMatrix(SearchGrid.Row, CNSTBIRTHPLACE);
							txtFatherFirstName.Text = SearchGrid.TextMatrix(SearchGrid.Row, CNSTFATHERFIRST);
							Text2[1].Text = SearchGrid.TextMatrix(SearchGrid.Row, CNSTFATHERLAST);
							Text2[2].Text = SearchGrid.TextMatrix(SearchGrid.Row, CNSTMOTHERLAST);
							txtMotherFirstName.Text = SearchGrid.TextMatrix(SearchGrid.Row, CNSTMOTHERFIRST);
							txtMaidenName.Text = SearchGrid.TextMatrix(SearchGrid.Row, CNSTMOTHERMAIDEN);
						}
					}
				}
			}
		}

        private void Search(int row)
        {
            int intResponse;
            modGNBas.Statics.Response = SearchGrid.TextMatrix(row, CNSTCHILDID);
            // Response is cleared out in the frmBirths.Show so I need
            // to have another variable
            intResponse = FCConvert.ToInt32(modGNBas.Statics.Response);
            if (fecherFoundation.Strings.UCase(SearchGrid.TextMatrix(row, CNSTDESCRIPTION)) == "LIVE BIRTHS")
            {
                //FC:FINAL:AM:#2412 - Close first the search form
                modClerkGeneral.Statics.AddingBirthCertificate = false;
                CloseWithoutCancel();
                ViewModel.Select(intResponse);               
                modGNBas.Statics.BoolPrintFromSearch = false;
            }
            else if (fecherFoundation.Strings.UCase(SearchGrid.TextMatrix(row, CNSTDESCRIPTION)) == "DELAYED BIRTHS")
            {
                //FC:FINAL:AM:#2412 - Close first the search form
                Close();
                frmBirthDelayed.InstancePtr.Unload();
                frmBirthDelayed.InstancePtr.Init(intResponse);
            }
            else if (fecherFoundation.Strings.UCase(SearchGrid.TextMatrix(row, CNSTDESCRIPTION)) == "FOREIGN BIRTHS")
            {
                //FC:FINAL:AM:#2412 - Close first the search form
                Close();
                frmBirthForeign.InstancePtr.Unload();
                frmBirthForeign.InstancePtr.Init(intResponse);
            }
            else
            {
                MessageBox.Show("Birth record needs to be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
		private void SearchGrid_DblClick(object sender, System.EventArgs e)
		{
			Search(SearchGrid.MouseRow);
		}

		private void Text2_Enter(int Index, object sender, System.EventArgs e)
		{
			Text2[Index].SelectionStart = 0;
			Text2[Index].SelectionLength = Text2[Index].Text.Length;
		}

		private void Text2_Enter(object sender, System.EventArgs e)
		{
			int index = Text2.GetIndex((FCTextBox)sender);
			Text2_Enter(index, sender, e);
		}

		public void SearchBth()
		{
			string strSQL;
			string strDelayedSQL;
			string strForeignSQL;
			// vbPorter upgrade warning: I As int	OnWriteFCConvert.ToInt32(
			int I;
			// vbPorter upgrade warning: tmpString As object	OnWrite(string)
			object[] tmpString = new object[3 + 1];
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			string strTemp = "";
			for (I = 0; I <= 3; I++)
			{
				if (Text2[I].Text != "")
					tmpString[I] = modClerkGeneral.SortString2(Text2[I].Text);
			}
			// I
			strSQL = string.Empty;
			strDelayedSQL = string.Empty;
			strForeignSQL = string.Empty;
			if (fecherFoundation.Strings.Trim(Text2[0].Text) != string.Empty)
			{
				// childs lastname
				strTemp = fecherFoundation.Strings.Trim(Text2[0].Text);
				strTemp = FCConvert.ToString(modGlobalRoutines.FixQuote(strTemp));
				strSQL += " Lastname like '" + strTemp + "%' and ";
				strDelayedSQL += " childlastname like '" + strTemp + "%' and ";
				strForeignSQL += " childlastname like '" + strTemp + "%' and ";
			}
			if (fecherFoundation.Strings.Trim(txtChildFirstName.Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(txtChildFirstName.Text);
				strTemp = FCConvert.ToString(modGlobalRoutines.FixQuote(strTemp));
				strSQL += " Firstname like '" + strTemp + "%' and ";
				strDelayedSQL += " childfirstname like '" + strTemp + "%' and ";
				strForeignSQL += " childfirstname like '" + strTemp + "%' and ";
			}
			// If tmpString(0) <> "" Then strSQL = ChildsName(Split(tmpString(0), " ")) & " AND "
			// If tmpString(1) <> "" Then strSQL = strSQL & FathersName(Split(tmpString(1), " ")) & " AND "
			if (fecherFoundation.Strings.Trim(Text2[1].Text) != string.Empty)
			{
				// fathers last
				strTemp = fecherFoundation.Strings.Trim(Text2[1].Text);
				strTemp = FCConvert.ToString(modGlobalRoutines.FixQuote(strTemp));
				strSQL += " FathersLastName like '" + strTemp + "%' and ";
				strDelayedSQL += " FathersLastName like '" + strTemp + "%' and ";
				strForeignSQL += " FathersLastName like '" + strTemp + "%' and ";
			}
			if (fecherFoundation.Strings.Trim(txtFatherFirstName.Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(txtFatherFirstName.Text);
				strTemp = FCConvert.ToString(modGlobalRoutines.FixQuote(strTemp));
				strSQL += " FathersFirstName like '" + strTemp + "%' and ";
				strDelayedSQL += " FathersFirstName like '" + strTemp + "%' and ";
				strForeignSQL += " FathersFirstName like '" + strTemp + "%' and ";
			}
			// If tmpString(2) <> "" Then strSQL = strSQL & MothersName(Split(tmpString(2), " ")) & " AND "
			if (fecherFoundation.Strings.Trim(Text2[2].Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(Text2[2].Text);
				strTemp = FCConvert.ToString(modGlobalRoutines.FixQuote(strTemp));
				strSQL += " MothersLastName like '" + strTemp + "%' and ";
				strDelayedSQL += " MothersLastName like '" + strTemp + "%' and ";
				strForeignSQL += " MothersLastName like '" + strTemp + "%' and ";
			}
			if (fecherFoundation.Strings.Trim(txtMotherFirstName.Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(txtMotherFirstName.Text);
				strTemp = FCConvert.ToString(modGlobalRoutines.FixQuote(strTemp));
				strSQL += " MothersFirstName like '" + strTemp + "%' and ";
				strDelayedSQL += " Mothersfirstname like '" + strTemp + "%' and ";
				strForeignSQL += " MothersFirstName like '" + strTemp + "%' and ";
			}
			if (fecherFoundation.Strings.Trim(Text2[3].Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(Text2[3].Text);
				strTemp = FCConvert.ToString(modGlobalRoutines.FixQuote(strTemp));
				strSQL += " BirthPlace like '" + strTemp + "%' and ";
				strDelayedSQL += " Birthplace1 like '" + strTemp + "%' and ";
			}
			// If tmpString(3) <> "" Then strSQL = strSQL & "BirthPlace LIKE '" & Trim(Text2(3).Text) & "*' AND "
			if (txtMaidenName.Text != "")
			{
				strSQL += "MothersMaidenName LIKE '" + fecherFoundation.Strings.Trim(txtMaidenName.Text) + "%' AND ";
				strDelayedSQL += " MothersMaidenName like '" + fecherFoundation.Strings.Trim(txtMaidenName.Text) + "%' and ";
				strForeignSQL += " MOthersmaidenName like '" + fecherFoundation.Strings.Trim(txtMaidenName.Text) + "%' and ";
			}
			if (Check1[0].CheckState == Wisej.Web.CheckState.Checked)
			{
				if (Information.IsDate(meBox1_0.Text))
				{
					var result = StaticSettings.GlobalCommandDispatcher.Send(new GetBirthIdsBasedOnBirthDate
					{
						Type = BirthDateType.LiveBirth,
						StartDate = Convert.ToDateTime(meBox1_0.Text),
						EndDate = null
					}).Result;

					string ids = "(";
					foreach (var birthMatch in result)
					{
						ids += (birthMatch + ", ");
					}
					if (ids == "(")
					{
						ids += "0)";
					}
					else
					{
						ids = ids.Left(ids.Length - 2) + ")";
					}
					strSQL += "Id IN " + ids + " AND ";

				}
			}
			if (Check1[1].CheckState == Wisej.Web.CheckState.Checked)
			{
				if (Information.IsDate(meBox1_0.Text) && Information.IsDate(meBox1_1.Text))
				{
					var result = StaticSettings.GlobalCommandDispatcher.Send(new GetBirthIdsBasedOnBirthDate
					{
						Type = BirthDateType.LiveBirth,
						StartDate = Convert.ToDateTime(meBox1_0.Text),
						EndDate = Convert.ToDateTime(meBox1_1.Text)
					}).Result;

					string ids = "(";
					foreach (var birthMatch in result)
					{
						ids += (birthMatch + ", ");
					}
					if (ids == "(")
					{
						ids += "0)";
					}
					else
					{
						ids = ids.Left(ids.Length - 2) + ")";
					}
					strSQL += "Id IN " + ids + " AND ";
					modClerkGeneral.Statics.BSInfo.DateOfBirth = FCConvert.ToDateTime(meBox1_0.Text);
					modClerkGeneral.Statics.BSInfo.DOBubound = FCConvert.ToDateTime(meBox1_1.Text);
				}
			}
			if (Check2.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (strSQL.Length > 5)
				{
					strSQL = Strings.Left(strSQL, strSQL.Length - 5);
					rs.OpenRecordset("SELECT * FROM Births WHERE " + strSQL, modGNBas.DEFAULTDATABASE);
				}
				else
				{
					rs.OpenRecordset("SELECT  * FROM Births ORDER BY LastName,FirstName", modGNBas.DEFAULTDATABASE);
				}
				SearchGrid.Rows = 1;
				SearchGrid.Rows = rs.RecordCount() + 1;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					for (intCounter = 1; intCounter <= (rs.RecordCount()); intCounter++)
					{
						SearchGrid.TextMatrix(intCounter, CNSTCHILDID, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						SearchGrid.TextMatrix(intCounter, CNSTCHILDLAST, FCConvert.ToString(rs.Get_Fields_String("LastName")));
						SearchGrid.TextMatrix(intCounter, CNSTCHILDFIRST, FCConvert.ToString(rs.Get_Fields_String("FirstName")));
						SearchGrid.TextMatrix(intCounter, CNSTBIRTHPLACE, FCConvert.ToString(rs.Get_Fields_String("BirthPlace")));
						SearchGrid.TextMatrix(intCounter, CNSTFATHERLAST, FCConvert.ToString(rs.Get_Fields_String("FathersLastName")));
						SearchGrid.TextMatrix(intCounter, CNSTFATHERFIRST, FCConvert.ToString(rs.Get_Fields_String("FathersFirstName")));
						SearchGrid.TextMatrix(intCounter, CNSTMOTHERLAST, FCConvert.ToString(rs.Get_Fields_String("MothersLastName")));
						SearchGrid.TextMatrix(intCounter, CNSTMOTHERFIRST, FCConvert.ToString(rs.Get_Fields_String("MothersFirstName")));
						SearchGrid.TextMatrix(intCounter, CNSTMOTHERMAIDEN, FCConvert.ToString(rs.Get_Fields_String("MothersMaidenName")));
						SearchGrid.TextMatrix(intCounter, CNSTDESCRIPTION, "Live Births");
						SearchGrid.TextMatrix(intCounter, CNSTDEATHONFILE, FCConvert.ToString(rs.Get_Fields_Boolean("DeathRecordInOffice")));
						SearchGrid.TextMatrix(intCounter, CNSTMARRIAGEONFILE, FCConvert.ToString(rs.Get_Fields_Boolean("MarriageOnFile")));
						rs.MoveNext();
					}
				}
			}

			
			if (Check1[0].CheckState != Wisej.Web.CheckState.Checked && Check1[1].CheckState != Wisej.Web.CheckState.Checked)
			{
				if (Check3.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strDelayedSQL.Length > 5)
					{
						strDelayedSQL = Strings.Left(strDelayedSQL, strDelayedSQL.Length - 5) + ";";
						rs.OpenRecordset("SELECT * FROM BirthsDelayed WHERE " + strDelayedSQL, modGNBas.DEFAULTDATABASE);
					}
					else
					{
						// Call RS.OpenRecordset("SELECT TOP 25 * FROM BirthsDelayed ORDER BY ChildLastName,ChildFirstName", DEFAULTDATABASENAME)
						rs.OpenRecordset("SELECT  * FROM BirthsDelayed ORDER BY ChildLastName,ChildFirstName", modGNBas.DEFAULTDATABASE);
					}
					I = SearchGrid.Rows;
					SearchGrid.Rows += rs.RecordCount();
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						for (intCounter = I; intCounter <= (SearchGrid.Rows - 1); intCounter++)
						{
							SearchGrid.TextMatrix(intCounter, CNSTCHILDID, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
							SearchGrid.TextMatrix(intCounter, CNSTCHILDLAST, FCConvert.ToString(rs.Get_Fields_String("ChildLastName")));
							SearchGrid.TextMatrix(intCounter, CNSTCHILDFIRST, FCConvert.ToString(rs.Get_Fields_String("ChildFirstName")));
							SearchGrid.TextMatrix(intCounter, CNSTBIRTHPLACE, FCConvert.ToString(rs.Get_Fields_String("BirthPlace1")));
							SearchGrid.TextMatrix(intCounter, CNSTFATHERLAST, FCConvert.ToString(rs.Get_Fields_String("FathersLastName")));
							SearchGrid.TextMatrix(intCounter, CNSTFATHERFIRST, FCConvert.ToString(rs.Get_Fields_String("FathersFirstName")));
							SearchGrid.TextMatrix(intCounter, CNSTMOTHERLAST, FCConvert.ToString(rs.Get_Fields_String("MothersLastName")));
							SearchGrid.TextMatrix(intCounter, CNSTMOTHERFIRST, FCConvert.ToString(rs.Get_Fields_String("MothersFirstName")));
							SearchGrid.TextMatrix(intCounter, CNSTMOTHERMAIDEN, FCConvert.ToString(rs.Get_Fields_String("MothersMaidenName")));
							SearchGrid.TextMatrix(intCounter, CNSTDESCRIPTION, "Delayed Births");
							SearchGrid.TextMatrix(intCounter, CNSTDEATHONFILE, FCConvert.ToString(rs.Get_Fields_Boolean("DeathRecordInOffice")));
							SearchGrid.TextMatrix(intCounter, CNSTMARRIAGEONFILE, FCConvert.ToString(rs.Get_Fields_Boolean("MarriageOnFile")));
							SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 0, intCounter, 11, 0xFFFFC0);
							rs.MoveNext();
						}
					}
				}

				if (Check4.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strForeignSQL.Length > 5)
					{
						strForeignSQL = Strings.Left(strForeignSQL, strForeignSQL.Length - 5) + ";";
						rs.OpenRecordset("SELECT * FROM BirthsForeign WHERE " + strForeignSQL, modGNBas.DEFAULTDATABASE);
					}
					else
					{
						// Call RS.OpenRecordset("SELECT TOP 25 * FROM BirthsForeign ORDER BY ChildLastName,ChildFirstName", DEFAULTDATABASENAME)
						rs.OpenRecordset("SELECT  * FROM BirthsForeign ORDER BY ChildLastName,ChildFirstName", modGNBas.DEFAULTDATABASE);
					}
					I = SearchGrid.Rows;
					SearchGrid.Rows += rs.RecordCount();
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						for (intCounter = I; intCounter <= (SearchGrid.Rows - 1); intCounter++)
						{
							SearchGrid.TextMatrix(intCounter, CNSTCHILDID, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
							SearchGrid.TextMatrix(intCounter, CNSTCHILDLAST, FCConvert.ToString(rs.Get_Fields_String("ChildLastName")));
							SearchGrid.TextMatrix(intCounter, CNSTCHILDFIRST, FCConvert.ToString(rs.Get_Fields_String("ChildFirstName")));
							SearchGrid.TextMatrix(intCounter, CNSTBIRTHPLACE, "");
							SearchGrid.TextMatrix(intCounter, CNSTFATHERLAST, FCConvert.ToString(rs.Get_Fields_String("FathersLastName")));
							SearchGrid.TextMatrix(intCounter, CNSTFATHERFIRST, FCConvert.ToString(rs.Get_Fields_String("FathersFirstName")));
							SearchGrid.TextMatrix(intCounter, CNSTMOTHERLAST, FCConvert.ToString(rs.Get_Fields_String("MothersLastName")));
							SearchGrid.TextMatrix(intCounter, CNSTMOTHERFIRST, FCConvert.ToString(rs.Get_Fields_String("MothersFirstName")));
							SearchGrid.TextMatrix(intCounter, CNSTMOTHERMAIDEN, FCConvert.ToString(rs.Get_Fields_String("MothersMaidenName")));
							SearchGrid.TextMatrix(intCounter, CNSTDESCRIPTION, "Foreign Births");
							SearchGrid.TextMatrix(intCounter, CNSTDEATHONFILE, FCConvert.ToString(rs.Get_Fields_Boolean("DeathRecordInOffice")));
							SearchGrid.TextMatrix(intCounter, CNSTMARRIAGEONFILE, FCConvert.ToString(rs.Get_Fields_Boolean("MarriageOnFile")));
							SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 0, intCounter, 11, 0xC0FFFF);
							rs.MoveNext();
						}
					}
				}
			}
			if (SearchGrid.Rows == 1)
				MessageBox.Show("No records found", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			// sort this grid
			SearchGrid.Select(0, 1);
			SearchGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
			strSort = "flexSortGenericAscending";
			SearchGrid.Select(0, 0);
		}

	
		public void clrForm(bool boolClear = true)
		{
			object[] tmpString = null;
			// - "AutoDim"
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is FCMaskedTextBox)
				{
					(obj as FCMaskedTextBox).Mask = "";
					(obj as FCMaskedTextBox).Text = "";
					(obj as FCMaskedTextBox).Mask = "##/##/####";
				}
				if (obj is FCComboBox)
				{
					if (boolClear)
						(obj as FCMaskedTextBox).Clear();
				}
			}
			// obj
			Check1[0].CheckState = Wisej.Web.CheckState.Unchecked;
			Check1[1].CheckState = Wisej.Web.CheckState.Unchecked;
			SearchGrid.Rows = 1;
			tmpString = new object[3 + 1];
		}

		public void FillBoxes()
		{
			Text2[0].Text = rs.Get_Fields_String("FirstName") + "  " + (FCConvert.ToString(rs.Get_Fields("MiddleName ")) != "N/A" ? rs.Get_Fields("MiddleName ") : "") + " " + rs.Get_Fields_String("LastName");
		}

        public IBirthSearchViewModel ViewModel { get; set; }

		private void cmdNew_Click(object sender, EventArgs e)
		{
			modClerkGeneral.Statics.AddingBirthCertificate = true;
			CloseWithoutCancel();
			ViewModel.Select(0);
			modGNBas.Statics.BoolPrintFromSearch = false;
		}
	}
}
