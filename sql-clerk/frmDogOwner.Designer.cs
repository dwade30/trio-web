//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDogOwner.
	/// </summary>
	partial class frmDogOwner
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLabels;
		public fecherFoundation.FCTextBox txtCustomerNumber;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdEdit;
		public FCGrid gridTownCode;
		public fecherFoundation.FCCheckBox chkNotRequired;
		public fecherFoundation.FCGrid GridKennels;
		public fecherFoundation.FCGrid GridDogs;
		public fecherFoundation.FCTextBox txtStreet;
		public fecherFoundation.FCTextBox txtStreetNumber;
		public Global.T2KDateBox t2kTransactionDate;
		public fecherFoundation.FCFrame framAddDog;
		public fecherFoundation.FCButton cmdCancelAddDeleted;
		public FCGrid gridList;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel lblCustomerNumber;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel lblCustomerInfo;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel lblLabels_0;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label10;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteOwner;
		public fecherFoundation.FCToolStripMenuItem mnuAddDog;
		public fecherFoundation.FCToolStripMenuItem mnuAddKennel;
		public fecherFoundation.FCToolStripMenuItem mnuAddDeletedDog;
		public fecherFoundation.FCToolStripMenuItem mnuTransferDog;
		public fecherFoundation.FCToolStripMenuItem mnuComments;
		public fecherFoundation.FCToolStripMenuItem mnuDogHistory;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDogOwner));
            this.txtCustomerNumber = new fecherFoundation.FCTextBox();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.cmdEdit = new fecherFoundation.FCButton();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.chkNotRequired = new fecherFoundation.FCCheckBox();
            this.GridKennels = new fecherFoundation.FCGrid();
            this.GridDogs = new fecherFoundation.FCGrid();
            this.txtStreet = new fecherFoundation.FCTextBox();
            this.txtStreetNumber = new fecherFoundation.FCTextBox();
            this.t2kTransactionDate = new Global.T2KDateBox();
            this.framAddDog = new fecherFoundation.FCFrame();
            this.cmdCancelAddDeleted = new fecherFoundation.FCButton();
            this.gridList = new fecherFoundation.FCGrid();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.lblCustomerNumber = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.lblCustomerInfo = new fecherFoundation.FCLabel();
            this.Shape1 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.lblLabels_0 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuTransferDog = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDogHistory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteOwner = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddDeletedDog = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddDog = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddKennel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdAddNewDog = new fecherFoundation.FCButton();
            this.cmdAddNewKennel = new fecherFoundation.FCButton();
            this.cmdComments = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdDeleteOwner = new fecherFoundation.FCButton();
            this.cmdAddDeletedDog = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNotRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridKennels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framAddDog)).BeginInit();
            this.framAddDog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelAddDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNewDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNewKennel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddDeletedDog)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(820, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtCustomerNumber);
            this.ClientArea.Controls.Add(this.cmdSearch);
            this.ClientArea.Controls.Add(this.cmdEdit);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.chkNotRequired);
            this.ClientArea.Controls.Add(this.GridKennels);
            this.ClientArea.Controls.Add(this.GridDogs);
            this.ClientArea.Controls.Add(this.txtStreet);
            this.ClientArea.Controls.Add(this.txtStreetNumber);
            this.ClientArea.Controls.Add(this.t2kTransactionDate);
            this.ClientArea.Controls.Add(this.Image1);
            this.ClientArea.Controls.Add(this.lblCustomerNumber);
            this.ClientArea.Controls.Add(this.Label16);
            this.ClientArea.Controls.Add(this.lblCustomerInfo);
            this.ClientArea.Controls.Add(this.Shape1);
            this.ClientArea.Controls.Add(this.Label14);
            this.ClientArea.Controls.Add(this.lblLabels_0);
            this.ClientArea.Controls.Add(this.Label13);
            this.ClientArea.Controls.Add(this.Label12);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.framAddDog);
            this.ClientArea.Size = new System.Drawing.Size(820, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDeleteOwner);
            this.TopPanel.Controls.Add(this.cmdAddNewDog);
            this.TopPanel.Controls.Add(this.cmdAddNewKennel);
            this.TopPanel.Controls.Add(this.cmdAddDeletedDog);
            this.TopPanel.Controls.Add(this.cmdComments);
            this.TopPanel.Size = new System.Drawing.Size(820, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComments, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddDeletedDog, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddNewKennel, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddNewDog, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteOwner, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(136, 30);
            this.HeaderText.Text = "Dog Owner";
            // 
            // txtCustomerNumber
            // 
            this.txtCustomerNumber.Location = new System.Drawing.Point(176, 90);
            this.txtCustomerNumber.Name = "txtCustomerNumber";
            this.txtCustomerNumber.Size = new System.Drawing.Size(141, 40);
            this.txtCustomerNumber.TabIndex = 17;
            this.txtCustomerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustomerNumber_Validating);
            this.txtCustomerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCustomerNumber_KeyPress);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.Location = new System.Drawing.Point(327, 90);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(40, 40);
            this.cmdSearch.TabIndex = 16;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdEdit
            // 
            this.cmdEdit.AppearanceKey = "actionButton";
            this.cmdEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdEdit.Image")));
            this.cmdEdit.Location = new System.Drawing.Point(377, 90);
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Size = new System.Drawing.Size(40, 40);
            this.cmdEdit.TabIndex = 15;
            this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
            // 
            // gridTownCode
            // 
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.Location = new System.Drawing.Point(30, 30);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.ReadOnly = false;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.Size = new System.Drawing.Size(287, 40);
            this.gridTownCode.TabIndex = 13;
            this.gridTownCode.Tag = "land";
            this.gridTownCode.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.gridTownCode_ChangeEdit);
            // 
            // chkNotRequired
            // 
            this.chkNotRequired.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.chkNotRequired.Location = new System.Drawing.Point(337, 30);
            this.chkNotRequired.Name = "chkNotRequired";
            this.chkNotRequired.Size = new System.Drawing.Size(391, 24);
            this.chkNotRequired.Text = "Record contains older data.  Not all fields will be required";
            // 
            // GridKennels
            // 
            this.GridKennels.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridKennels.Cols = 5;
            this.GridKennels.ExtendLastCol = true;
            this.GridKennels.FixedCols = 0;
            this.GridKennels.Location = new System.Drawing.Point(30, 563);
            this.GridKennels.Name = "GridKennels";
            this.GridKennels.RowHeadersVisible = false;
            this.GridKennels.Rows = 1;
            this.GridKennels.Size = new System.Drawing.Size(728, 100);
            this.GridKennels.TabIndex = 5;
            this.GridKennels.DoubleClick += new System.EventHandler(this.GridKennels_DblClick);
            // 
            // GridDogs
            // 
            this.GridDogs.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridDogs.Cols = 6;
            this.GridDogs.ExtendLastCol = true;
            this.GridDogs.FixedCols = 0;
            this.GridDogs.Location = new System.Drawing.Point(30, 340);
            this.GridDogs.Name = "GridDogs";
            this.GridDogs.RowHeadersVisible = false;
            this.GridDogs.Rows = 1;
            this.GridDogs.Size = new System.Drawing.Size(728, 168);
            this.GridDogs.TabIndex = 4;
            this.GridDogs.DoubleClick += new System.EventHandler(this.GridDogs_DblClick);
            // 
            // txtStreet
            // 
            this.txtStreet.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreet.Location = new System.Drawing.Point(246, 245);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(221, 40);
            this.txtStreet.TabIndex = 2;
            // 
            // txtStreetNumber
            // 
            this.txtStreetNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetNumber.Location = new System.Drawing.Point(176, 245);
            this.txtStreetNumber.Name = "txtStreetNumber";
            this.txtStreetNumber.Size = new System.Drawing.Size(60, 40);
            this.txtStreetNumber.TabIndex = 1;
            // 
            // t2kTransactionDate
            // 
            this.t2kTransactionDate.Location = new System.Drawing.Point(487, 180);
            this.t2kTransactionDate.Name = "t2kTransactionDate";
            this.t2kTransactionDate.Size = new System.Drawing.Size(115, 22);
            this.t2kTransactionDate.TabIndex = 3;
            // 
            // framAddDog
            // 
            this.framAddDog.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framAddDog.AppearanceKey = "groupBoxNoBorders";
            this.framAddDog.BackColor = System.Drawing.Color.White;
            this.framAddDog.Controls.Add(this.cmdCancelAddDeleted);
            this.framAddDog.Controls.Add(this.gridList);
            this.framAddDog.Location = new System.Drawing.Point(10, 10);
            this.framAddDog.Name = "framAddDog";
            this.framAddDog.Size = new System.Drawing.Size(773, 485);
            this.framAddDog.TabIndex = 10;
            this.framAddDog.Visible = false;
            // 
            // cmdCancelAddDeleted
            // 
            this.cmdCancelAddDeleted.AppearanceKey = "actionButton";
            this.cmdCancelAddDeleted.Location = new System.Drawing.Point(20, 497);
            this.cmdCancelAddDeleted.Name = "cmdCancelAddDeleted";
            this.cmdCancelAddDeleted.Size = new System.Drawing.Size(100, 40);
            this.cmdCancelAddDeleted.TabIndex = 12;
            this.cmdCancelAddDeleted.Text = "Cancel";
            this.cmdCancelAddDeleted.Click += new System.EventHandler(this.cmdCancelAddDeleted_Click);
            // 
            // gridList
            // 
            this.gridList.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridList.Cols = 7;
            this.gridList.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.gridList.ExtendLastCol = true;
            this.gridList.Location = new System.Drawing.Point(20, 20);
            this.gridList.Name = "gridList";
            this.gridList.Rows = 1;
            this.gridList.Size = new System.Drawing.Size(728, 383);
            this.gridList.TabIndex = 11;
            this.gridList.DoubleClick += new System.EventHandler(this.gridList_DblClick);
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(427, 90);
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(40, 40);
            this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.Image1.TabIndex = 18;
            this.Image1.Visible = false;
            this.Image1.Click += new System.EventHandler(this.Image1_Click);
            // 
            // lblCustomerNumber
            // 
            this.lblCustomerNumber.Location = new System.Drawing.Point(30, 104);
            this.lblCustomerNumber.Name = "lblCustomerNumber";
            this.lblCustomerNumber.Size = new System.Drawing.Size(80, 18);
            this.lblCustomerNumber.TabIndex = 20;
            this.lblCustomerNumber.Text = "CUSTOMER #";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(30, 150);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(72, 18);
            this.Label16.TabIndex = 19;
            this.Label16.Text = "CUSTOMER";
            // 
            // lblCustomerInfo
            // 
            this.lblCustomerInfo.Location = new System.Drawing.Point(176, 150);
            this.lblCustomerInfo.Name = "lblCustomerInfo";
            this.lblCustomerInfo.Size = new System.Drawing.Size(291, 75);
            this.lblCustomerInfo.TabIndex = 18;
            // 
            // Shape1
            // 
            this.Shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Shape1.Location = new System.Drawing.Point(30, 305);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(11, 12);
            this.Shape1.TabIndex = 21;
            this.Shape1.Visible = false;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(50, 305);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(89, 15);
            this.Label14.TabIndex = 14;
            this.Label14.Text = "= DECEASED";
            this.Label14.Visible = false;
            // 
            // lblLabels_0
            // 
            this.lblLabels_0.Location = new System.Drawing.Point(487, 150);
            this.lblLabels_0.Name = "lblLabels_0";
            this.lblLabels_0.Size = new System.Drawing.Size(136, 15);
            this.lblLabels_0.TabIndex = 9;
            this.lblLabels_0.Text = "TRANSACTION DATE";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(340, 528);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(130, 15);
            this.Label13.TabIndex = 8;
            this.Label13.Text = "KENNEL LICENSES";
            this.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(362, 305);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(82, 15);
            this.Label12.TabIndex = 7;
            this.Label12.Text = "DOGS";
            this.Label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(30, 259);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(70, 19);
            this.Label10.TabIndex = 6;
            this.Label10.Text = "LOCATION";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuTransferDog,
            this.mnuDogHistory});
            this.MainMenu1.Name = null;
            // 
            // mnuTransferDog
            // 
            this.mnuTransferDog.Index = 0;
            this.mnuTransferDog.Name = "mnuTransferDog";
            this.mnuTransferDog.Text = "Transfer Dog from Other Owner";
            this.mnuTransferDog.Click += new System.EventHandler(this.mnuTransferDog_Click);
            // 
            // mnuDogHistory
            // 
            this.mnuDogHistory.Index = 1;
            this.mnuDogHistory.Name = "mnuDogHistory";
            this.mnuDogHistory.Text = "Dog History";
            this.mnuDogHistory.Click += new System.EventHandler(this.mnuDogHistory_Click);
            // 
            // mnuDeleteOwner
            // 
            this.mnuDeleteOwner.Index = -1;
            this.mnuDeleteOwner.Name = "mnuDeleteOwner";
            this.mnuDeleteOwner.Text = "Delete Owner";
            this.mnuDeleteOwner.Click += new System.EventHandler(this.mnuDeleteOwner_Click);
            // 
            // mnuAddDeletedDog
            // 
            this.mnuAddDeletedDog.Index = -1;
            this.mnuAddDeletedDog.Name = "mnuAddDeletedDog";
            this.mnuAddDeletedDog.Text = "Add Deleted Dog";
            this.mnuAddDeletedDog.Click += new System.EventHandler(this.mnuAddDeletedDog_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuAddDog
            // 
            this.mnuAddDog.Index = -1;
            this.mnuAddDog.Name = "mnuAddDog";
            this.mnuAddDog.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.mnuAddDog.Text = "Add New Dog";
            this.mnuAddDog.Click += new System.EventHandler(this.mnuAddDog_Click);
            // 
            // mnuAddKennel
            // 
            this.mnuAddKennel.Index = -1;
            this.mnuAddKennel.Name = "mnuAddKennel";
            this.mnuAddKennel.Shortcut = Wisej.Web.Shortcut.CtrlK;
            this.mnuAddKennel.Text = "Add New Kennel";
            this.mnuAddKennel.Click += new System.EventHandler(this.mnuAddKennel_Click);
            // 
            // mnuComments
            // 
            this.mnuComments.Index = -1;
            this.mnuComments.Name = "mnuComments";
            this.mnuComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComments.Text = "Comments";
            this.mnuComments.Click += new System.EventHandler(this.mnuComments_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = -1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddNewDog
            // 
            this.cmdAddNewDog.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddNewDog.Location = new System.Drawing.Point(330, 29);
            this.cmdAddNewDog.Name = "cmdAddNewDog";
            this.cmdAddNewDog.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.cmdAddNewDog.Size = new System.Drawing.Size(106, 24);
            this.cmdAddNewDog.TabIndex = 1;
            this.cmdAddNewDog.Text = "Add New Dog";
            this.cmdAddNewDog.Click += new System.EventHandler(this.mnuAddDog_Click);
            // 
            // cmdAddNewKennel
            // 
            this.cmdAddNewKennel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddNewKennel.Location = new System.Drawing.Point(442, 29);
            this.cmdAddNewKennel.Name = "cmdAddNewKennel";
            this.cmdAddNewKennel.Shortcut = Wisej.Web.Shortcut.CtrlK;
            this.cmdAddNewKennel.Size = new System.Drawing.Size(122, 24);
            this.cmdAddNewKennel.TabIndex = 2;
            this.cmdAddNewKennel.Text = "Add New Kennel";
            this.cmdAddNewKennel.Click += new System.EventHandler(this.mnuAddKennel_Click);
            // 
            // cmdComments
            // 
            this.cmdComments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComments.Location = new System.Drawing.Point(704, 29);
            this.cmdComments.Name = "cmdComments";
            this.cmdComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComments.Size = new System.Drawing.Size(88, 24);
            this.cmdComments.TabIndex = 3;
            this.cmdComments.Text = "Comments";
            this.cmdComments.Click += new System.EventHandler(this.mnuComments_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(406, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdDeleteOwner
            // 
            this.cmdDeleteOwner.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteOwner.Location = new System.Drawing.Point(216, 29);
            this.cmdDeleteOwner.Name = "cmdDeleteOwner";
            this.cmdDeleteOwner.Size = new System.Drawing.Size(108, 24);
            this.cmdDeleteOwner.TabIndex = 4;
            this.cmdDeleteOwner.Text = "Delete Owner";
            this.cmdDeleteOwner.Click += new System.EventHandler(this.mnuDeleteOwner_Click);
            // 
            // cmdAddDeletedDog
            // 
            this.cmdAddDeletedDog.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddDeletedDog.Location = new System.Drawing.Point(570, 29);
            this.cmdAddDeletedDog.Name = "cmdAddDeletedDog";
            this.cmdAddDeletedDog.Size = new System.Drawing.Size(128, 24);
            this.cmdAddDeletedDog.TabIndex = 5;
            this.cmdAddDeletedDog.Text = "Add Deleted Dog";
            this.cmdAddDeletedDog.Click += new System.EventHandler(this.mnuAddDeletedDog_Click);
            // 
            // frmDogOwner
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(820, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmDogOwner";
            this.Text = "Dog Owner";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDogOwner_Load);
            this.Resize += new System.EventHandler(this.frmDogOwner_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDogOwner_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNotRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridKennels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framAddDog)).EndInit();
            this.framAddDog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelAddDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNewDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNewKennel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddDeletedDog)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdAddNewDog;
		private FCButton cmdAddNewKennel;
		private FCButton cmdComments;
		private FCButton cmdSave;
        private FCButton cmdAddDeletedDog;
        private FCButton cmdDeleteOwner;
    }
}