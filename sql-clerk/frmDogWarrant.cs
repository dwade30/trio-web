//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDogWarrant : BaseForm
	{
		public frmDogWarrant()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDogWarrant InstancePtr
		{
			get
			{
				return (frmDogWarrant)Sys.GetInstance(typeof(frmDogWarrant));
			}
		}

		protected frmDogWarrant _InstancePtr = null;
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		public int intDataChanged;

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		public void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (modGNBas.Statics.boolWarrant)
			{
				// rptDogWarrant.Show 1
				rptDogWarrant.InstancePtr.Init(false, this.Modal);
				if (MessageBox.Show("Print Warrant Return with this same information?", "TRIO Software Dog Warrant Return", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rptWarrantReturn.InstancePtr.Init(this.Modal);
				}
				if (MessageBox.Show("Print Warrant Listing?", "TRIO Software Dog Warrant Listing", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					modGNBas.Statics.gstrReportOption = "Warrant";
					frmReportListing.InstancePtr.Show(App.MainForm);
					// gintYear = InputBox("Enter year to report on.", "TRIO Software", 2000)
					// If gintYear = 0 Then Exit Sub
					// 
					// rptWarrentListing.Show 1
					// rptWarrentListing.Init
					// If MsgBox("Update dog records for printed warrants?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
					// Call UpdateDogRecordsForWarrants
					// End If
				}
			}
			else
			{
				rptWarrantReturn.InstancePtr.Init(this.Modal);
				if (MessageBox.Show("Print Warrant with this same information?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rptDogWarrant.InstancePtr.Init(false, this.Modal);
				}
				if (MessageBox.Show("Print Warrant Listing?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					modGNBas.Statics.gintYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("Enter year to report on.", "TRIO Software", FCConvert.ToString(2000)))));
					if (modGNBas.Statics.gintYear == 0)
						return;
					// rptWarrentListing.Show 1
					rptWarrentListing.InstancePtr.Init(this.Modal);
					// If MsgBox("Update dog records for printed warrants?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
					// Call UpdateDogRecordsForWarrants
					// End If
				}
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				rs.OpenRecordset("SELECT * FROM WarrantParameters", modGNBas.DEFAULTDATABASE);
				if (rs.EndOfFile())
				{
					rs.AddNew();
				}
				else
				{
					rs.Edit();
				}
				rs.Set_Fields("TownCounty", txtTownCounty.Text + " ");
				rs.Set_Fields("TownName", txtTownName.Text + " ");
				rs.Set_Fields("ControlOfficer", txtControlOfficer.Text + " ");
				rs.Set_Fields("CourtCounty", txtCourtCounty.Text + " ");
				rs.Set_Fields("CourtTown", txtCourtTown.Text + " ");
				rs.Update();
				MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmDogWarrant_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDogWarrant_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDogWarrant_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDogWarrant properties;
			//frmDogWarrant.ScaleWidth	= 5940;
			//frmDogWarrant.ScaleHeight	= 4140;
			//frmDogWarrant.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this);
			// Set db = OpenDatabase(ClerkDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
			rs.OpenRecordset("SELECT * FROM WarrantParameters", modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				txtTownCounty.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("TownCounty") + " ");
				txtTownName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("TownName") + " ");
				txtControlOfficer.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ControlOfficer") + " ");
				txtCourtCounty.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("CourtCounty") + " ");
				txtCourtTown.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("CourtTown") + " ");
			}
			modDirtyForm.ClearDirtyControls(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGNBas.Statics.boolWarrant = false;
			intDataChanged = FCConvert.ToInt16(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "mnuSaveExit_Click");
			//MDIParent.InstancePtr.Show();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			/* Control ControlName = new Control(); */
			foreach (Control ControlName in this.GetAllControls())
			{
				if (ControlName is FCTextBox)
				{
					ControlName.Text = string.Empty;
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}
	}
}
