﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptWarrantReturn.
	/// </summary>
	partial class rptWarrantReturn
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptWarrantReturn));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtTownCounty1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtControlOfficer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCourtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCourtCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DLG1 = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			((System.ComponentModel.ISupportInitialize)(this.txtTownCounty1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtControlOfficer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCourtTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCourtCounty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTownCounty1,
				this.txtControlOfficer,
				this.txtTownName1,
				this.txtCourtTown,
				this.txtCourtCounty,
				this.txtTownName2,
				this.DLG1
			});
			this.PageHeader.Height = 3.791667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtTownCounty1
			// 
			this.txtTownCounty1.Height = 0.1875F;
			this.txtTownCounty1.Left = 0F;
			this.txtTownCounty1.Name = "txtTownCounty1";
			this.txtTownCounty1.Text = null;
			this.txtTownCounty1.Top = 0.8125F;
			this.txtTownCounty1.Width = 2.3125F;
			// 
			// txtControlOfficer
			// 
			this.txtControlOfficer.Height = 0.1875F;
			this.txtControlOfficer.Left = 0.1875F;
			this.txtControlOfficer.Name = "txtControlOfficer";
			this.txtControlOfficer.Text = null;
			this.txtControlOfficer.Top = 1.4375F;
			this.txtControlOfficer.Width = 2.4375F;
			// 
			// txtTownName1
			// 
			this.txtTownName1.Height = 0.1875F;
			this.txtTownName1.Left = 1.4375F;
			this.txtTownName1.Name = "txtTownName1";
			this.txtTownName1.Text = null;
			this.txtTownName1.Top = 1.125F;
			this.txtTownName1.Width = 1.9375F;
			// 
			// txtCourtTown
			// 
			this.txtCourtTown.Height = 0.1875F;
			this.txtCourtTown.Left = 2.875F;
			this.txtCourtTown.Name = "txtCourtTown";
			this.txtCourtTown.Text = null;
			this.txtCourtTown.Top = 3.125F;
			this.txtCourtTown.Width = 1.5625F;
			// 
			// txtCourtCounty
			// 
			this.txtCourtCounty.Height = 0.1875F;
			this.txtCourtCounty.Left = 0.625F;
			this.txtCourtCounty.Name = "txtCourtCounty";
			this.txtCourtCounty.Text = null;
			this.txtCourtCounty.Top = 3.125F;
			this.txtCourtCounty.Width = 1.5625F;
			// 
			// txtTownName2
			// 
			this.txtTownName2.Height = 0.1875F;
			this.txtTownName2.Left = 2.6875F;
			this.txtTownName2.Name = "txtTownName2";
			this.txtTownName2.Text = null;
			this.txtTownName2.Top = 1.75F;
			this.txtTownName2.Width = 1.5625F;
			// 
			// DLG1
			// 
			this.DLG1.Height = 0.3125F;
			this.DLG1.Left = 0F;
			this.DLG1.Name = "DLG1";
			this.DLG1.Top = 0F;
			this.DLG1.Visible = false;
			this.DLG1.Width = 0.875F;
			// 
			// rptWarrantReturn
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.072917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTownCounty1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtControlOfficer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCourtTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCourtCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownCounty1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtControlOfficer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCourtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCourtCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownName2;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl DLG1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
