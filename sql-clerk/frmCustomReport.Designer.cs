//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	partial class frmCustomReport
	{
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCFrame framEmailReminders;
		public Global.T2KDateBox T2KReminderDate;
		public fecherFoundation.FCCheckBox chkPrint;
		public fecherFoundation.FCCheckBox chkSendEmail;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCCheckBox chkLandscape;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCFrame fraReports;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCPanel Frame1;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCPictureBox Image1;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuEliminateDuplicates;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomReport));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbPrint = new fecherFoundation.FCComboBox();
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.framEmailReminders = new fecherFoundation.FCFrame();
            this.T2KReminderDate = new Global.T2KDateBox();
            this.chkPrint = new fecherFoundation.FCCheckBox();
            this.chkSendEmail = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.chkLandscape = new fecherFoundation.FCCheckBox();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.fraReports = new fecherFoundation.FCFrame();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.Line1 = new fecherFoundation.FCLine();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.cmdExit = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCPanel();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.Line2 = new fecherFoundation.FCLine();
            this.mnuEliminateDuplicates = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.btnPrintPreview = new fecherFoundation.FCButton();
            this.btnAddRow = new fecherFoundation.FCButton();
            this.btnAddColumn = new fecherFoundation.FCButton();
            this.btnDeleteRow = new fecherFoundation.FCButton();
            this.btnDeleteColumn = new fecherFoundation.FCButton();
            this.btnPrint = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framEmailReminders)).BeginInit();
            this.framEmailReminders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KReminderDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSendEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandscape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReports)).BeginInit();
            this.fraReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 482);
            this.BottomPanel.Size = new System.Drawing.Size(1134, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkLandscape);
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.fraReports);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Line2);
            this.ClientArea.Controls.Add(this.framEmailReminders);
            this.ClientArea.Size = new System.Drawing.Size(1134, 422);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Controls.Add(this.btnPrint);
            this.TopPanel.Controls.Add(this.btnDeleteColumn);
            this.TopPanel.Controls.Add(this.btnDeleteRow);
            this.TopPanel.Controls.Add(this.btnAddColumn);
            this.TopPanel.Controls.Add(this.btnAddRow);
            this.TopPanel.Size = new System.Drawing.Size(1134, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnAddRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnAddColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnDeleteRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnDeleteColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(177, 30);
            this.HeaderText.Text = "Custom Report";
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "All",
            "Only non-emailed"});
            this.cmbPrint.Location = new System.Drawing.Point(20, 104);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(278, 40);
            this.cmbPrint.TabIndex = 29;
            this.cmbPrint.Text = "All";
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(278, 40);
            this.cmbReport.TabIndex = 11;
            this.cmbReport.Text = "Create New Report";
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // framEmailReminders
            // 
            this.framEmailReminders.BackColor = System.Drawing.Color.White;
            this.framEmailReminders.Controls.Add(this.T2KReminderDate);
            this.framEmailReminders.Controls.Add(this.cmbPrint);
            this.framEmailReminders.Controls.Add(this.chkPrint);
            this.framEmailReminders.Controls.Add(this.chkSendEmail);
            this.framEmailReminders.Controls.Add(this.Label1);
            this.framEmailReminders.Location = new System.Drawing.Point(734, 190);
            this.framEmailReminders.Name = "framEmailReminders";
            this.framEmailReminders.Size = new System.Drawing.Size(318, 230);
            this.framEmailReminders.TabIndex = 23;
            this.framEmailReminders.Text = "Reminder Options";
            this.framEmailReminders.Visible = false;
            // 
            // T2KReminderDate
            // 
            this.T2KReminderDate.Location = new System.Drawing.Point(84, 154);
            this.T2KReminderDate.MaxLength = 10;
            this.T2KReminderDate.Name = "T2KReminderDate";
            this.T2KReminderDate.Size = new System.Drawing.Size(115, 40);
            this.T2KReminderDate.TabIndex = 28;
            // 
            // chkPrint
            // 
            this.chkPrint.Checked = true;
            this.chkPrint.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkPrint.Location = new System.Drawing.Point(20, 67);
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Size = new System.Drawing.Size(61, 27);
            this.chkPrint.TabIndex = 25;
            this.chkPrint.Text = "Print";
            // 
            // chkSendEmail
            // 
            this.chkSendEmail.Location = new System.Drawing.Point(20, 30);
            this.chkSendEmail.Name = "chkSendEmail";
            this.chkSendEmail.Size = new System.Drawing.Size(68, 27);
            this.chkSendEmail.TabIndex = 24;
            this.chkSendEmail.Text = "Email";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 168);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(40, 15);
            this.Label1.TabIndex = 29;
            this.Label1.Text = "DATE";
            // 
            // chkLandscape
            // 
            this.chkLandscape.Location = new System.Drawing.Point(1075, 190);
            this.chkLandscape.Name = "chkLandscape";
            this.chkLandscape.Size = new System.Drawing.Size(146, 27);
            this.chkLandscape.TabIndex = 22;
            this.chkLandscape.Text = "Print Landscape";
            this.chkLandscape.CheckedChanged += new System.EventHandler(this.chkLandscape_CheckedChanged);
            // 
            // fraWhere
            // 
            this.fraWhere.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.fraWhere.AppearanceKey = "groupBoxNoBorders";
            this.fraWhere.BackColor = System.Drawing.Color.White;
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 459);
            this.fraWhere.MinimumSize = new System.Drawing.Size(941, 205);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(1028, 205);
            this.fraWhere.TabIndex = 11;
            this.fraWhere.Text = "Select Search Criteria";
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.vsWhere.Cols = 10;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.ExtendLastCol = true;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(0, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.Size = new System.Drawing.Size(1022, 155);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 12;
            this.vsWhere.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEdit);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
            // 
            // fraReports
            // 
            this.fraReports.BackColor = System.Drawing.Color.White;
            this.fraReports.Controls.Add(this.cmdAdd);
            this.fraReports.Controls.Add(this.cmbReport);
            this.fraReports.Controls.Add(this.cboSavedReport);
            this.fraReports.Location = new System.Drawing.Point(734, 190);
            this.fraReports.Name = "fraReports";
            this.fraReports.Size = new System.Drawing.Size(318, 252);
            this.fraReports.TabIndex = 5;
            this.fraReports.Text = "Report";
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(20, 150);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(278, 40);
            this.cmdAdd.TabIndex = 10;
            this.cmdAdd.Text = "Add Custom Report to Library";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 90);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(278, 40);
            this.cboSavedReport.TabIndex = 8;
            this.cboSavedReport.Visible = false;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Line1.BorderWidth = ((short)(2));
            this.Line1.Location = new System.Drawing.Point(720, 0);
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(2, 140);
            //this.Line1.Y2 = 376F;
            // 
            // fraSort
            // 
            this.fraSort.BackColor = System.Drawing.Color.White;
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Location = new System.Drawing.Point(360, 190);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(354, 252);
            this.fraSort.TabIndex = 3;
            this.fraSort.Text = "Fields To Sort By";
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(314, 202);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 4;
            // 
            // cmdPrint
            // 
            this.cmdPrint.Location = new System.Drawing.Point(303, 512);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(96, 26);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // fraFields
            // 
            this.fraFields.BackColor = System.Drawing.Color.White;
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Location = new System.Drawing.Point(30, 190);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(310, 252);
            this.fraFields.TabIndex = 23;
            this.fraFields.Text = "Fields To Display On Report";
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(270, 203);
            this.lstFields.TabIndex = 2;
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // cmdExit
            // 
            this.cmdExit.Location = new System.Drawing.Point(36, 521);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(96, 26);
            this.cmdExit.TabIndex = 14;
            this.cmdExit.Text = "Exit";
            this.cmdExit.Visible = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AutoScroll = true;
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.Line1);
            this.Frame1.Controls.Add(this.fraMessage);
            this.Frame1.Controls.Add(this.Image1);
            this.Frame1.Controls.Add(this.vsLayout);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1069, 140);
            this.Frame1.ScrollBars = Wisej.Web.ScrollBars.Horizontal;
            this.Frame1.TabIndex = 16;
            // 
            // fraMessage
            // 
            this.fraMessage.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraMessage.AppearanceKey = "groupBoxNoBorders";
            this.fraMessage.BackColor = System.Drawing.Color.White;
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.Location = new System.Drawing.Point(0, 46);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(1048, 13);
            this.fraMessage.TabIndex = 20;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(0, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(700, 15);
            this.Label3.TabIndex = 21;
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.FillColor = 16777215;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(984, 22);
            this.Image1.TabIndex = 21;
            // 
            // vsLayout
            // 
            this.vsLayout.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
            this.vsLayout.AllowUserToResizeColumns = true;
            this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.Location = new System.Drawing.Point(0, 28);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.ReadOnly = false;
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            this.vsLayout.Rows = 1;
            this.vsLayout.Size = new System.Drawing.Size(984, 112);
            this.vsLayout.TabIndex = 19;
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            this.vsLayout.ColumnAdded += new Wisej.Web.DataGridViewColumnEventHandler(VsLayout_ColumnAdded);
            this.vsLayout.ColumnRemoved += new Wisej.Web.DataGridViewColumnEventHandler(VsLayout_ColumnRemoved);
            this.vsLayout.ColumnStateChanged += new Wisej.Web.DataGridViewColumnStateChangedEventHandler(VsLayout_ColumnStateChanged);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // mnuClear
            // 
            this.mnuClear.Index = -1;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // Line2
            // 
            this.Line2.Name = "Line2";
            this.Line2.Size = new System.Drawing.Size(1440, 1);
            this.Line2.Visible = false;
            this.Line2.X2 = 1440F;
            // 
            // mnuEliminateDuplicates
            // 
            this.mnuEliminateDuplicates.Index = -1;
            this.mnuEliminateDuplicates.Name = "mnuEliminateDuplicates";
            this.mnuEliminateDuplicates.Text = "Eliminate Duplicate Records";
            this.mnuEliminateDuplicates.Visible = false;
            this.mnuEliminateDuplicates.Click += new System.EventHandler(this.mnuEliminateDuplicates_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = -1;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row                            ";
            this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = -1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column         ";
            this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = -1;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row        ";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = -1;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column    ";
            this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = -1;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            this.mnuSepar4.Visible = false;
            // 
            // mnuSP21
            // 
            this.mnuSP21.Index = -1;
            this.mnuSP21.Name = "mnuSP21";
            this.mnuSP21.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = -1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = -1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrintPreview.Text = "Print/Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.AppearanceKey = "acceptButton";
            this.btnPrintPreview.Location = new System.Drawing.Point(419, 30);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnPrintPreview.Size = new System.Drawing.Size(148, 48);
            this.btnPrintPreview.Text = "Print Preview";
            this.btnPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // btnAddRow
            // 
            this.btnAddRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnAddRow.Location = new System.Drawing.Point(755, 29);
            this.btnAddRow.Name = "btnAddRow";
            this.btnAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.btnAddRow.Size = new System.Drawing.Size(70, 24);
            this.btnAddRow.TabIndex = 1;
            this.btnAddRow.Text = "Add Row";
            this.btnAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // btnAddColumn
            // 
            this.btnAddColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnAddColumn.Location = new System.Drawing.Point(831, 29);
            this.btnAddColumn.Name = "btnAddColumn";
            this.btnAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.btnAddColumn.Size = new System.Drawing.Size(90, 24);
            this.btnAddColumn.TabIndex = 2;
            this.btnAddColumn.Text = "Add Column";
            this.btnAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // btnDeleteRow
            // 
            this.btnDeleteRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnDeleteRow.Location = new System.Drawing.Point(927, 29);
            this.btnDeleteRow.Name = "btnDeleteRow";
            this.btnDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.btnDeleteRow.Size = new System.Drawing.Size(85, 24);
            this.btnDeleteRow.TabIndex = 3;
            this.btnDeleteRow.Text = "Delete Row";
            this.btnDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // btnDeleteColumn
            // 
            this.btnDeleteColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnDeleteColumn.Location = new System.Drawing.Point(1018, 29);
            this.btnDeleteColumn.Name = "btnDeleteColumn";
            this.btnDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.btnDeleteColumn.Size = new System.Drawing.Size(105, 24);
            this.btnDeleteColumn.TabIndex = 4;
            this.btnDeleteColumn.Text = "Delete Column";
            this.btnDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(1128, 29);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.btnPrint.Size = new System.Drawing.Size(45, 24);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(609, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(140, 24);
            this.cmdClear.TabIndex = 6;
            this.cmdClear.Text = "Clear Search Criteria";
            this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // frmCustomReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1134, 590);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomReport";
            this.Text = "Custom Report";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCustomReport_Load);
            this.Activated += new System.EventHandler(this.frmCustomReport_Activated);
            this.Resize += new System.EventHandler(this.frmCustomReport_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomReport_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framEmailReminders)).EndInit();
            this.framEmailReminders.ResumeLayout(false);
            this.framEmailReminders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KReminderDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSendEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandscape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReports)).EndInit();
            this.fraReports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            this.ResumeLayout(false);

		}

        
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton btnPrintPreview;
		private FCButton btnAddRow;
		private FCButton btnAddColumn;
		private FCButton btnDeleteRow;
		private FCButton btnDeleteColumn;
		private FCButton btnPrint;
		private FCButton cmdClear;
	}
}