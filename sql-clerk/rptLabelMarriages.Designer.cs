﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptLabelMarriages.
	/// </summary>
	partial class rptLabelMarriages
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLabelMarriages));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.L7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtF18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.L3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtF1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.L8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtF3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.L11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtF5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.L15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtF9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.L19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtF13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblIssued = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAttest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTown = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.L7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAttest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.L7,
				this.L5,
				this.L6,
				this.L1,
				this.L2,
				this.txtF18,
				this.txtF17,
				this.txtF19,
				this.L3,
				this.L4,
				this.txtF1,
				this.txtF2,
				this.L8,
				this.L9,
				this.L10,
				this.txtF3,
				this.txtF4,
				this.L11,
				this.L13,
				this.L12,
				this.L14,
				this.txtF5,
				this.txtF6,
				this.txtF7,
				this.txtF8,
				this.L15,
				this.L16,
				this.L17,
				this.L18,
				this.L20,
				this.L21,
				this.L22,
				this.txtF9,
				this.txtF10,
				this.txtF11,
				this.txtF12,
				this.txtF14,
				this.txtF15,
				this.txtF16,
				this.L19,
				this.txtF13,
				this.lblIssued,
				this.lblAttest,
				this.lblTown
			});
			this.Detail.Height = 10.48958F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// L7
			// 
			this.L7.Height = 0.2083333F;
			this.L7.HyperLink = null;
			this.L7.Left = 0.59375F;
			this.L7.Name = "L7";
			this.L7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L7.Text = "RESIDENCE";
			this.L7.Top = 3.75F;
			this.L7.Width = 1.3125F;
			// 
			// L5
			// 
			this.L5.Height = 0.2083333F;
			this.L5.HyperLink = null;
			this.L5.Left = 0.59375F;
			this.L5.Name = "L5";
			this.L5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L5.Text = "FULL NAME";
			this.L5.Top = 3.125F;
			this.L5.Width = 1.3125F;
			// 
			// L6
			// 
			this.L6.Height = 0.1875F;
			this.L6.HyperLink = null;
			this.L6.Left = 4.5F;
			this.L6.Name = "L6";
			this.L6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L6.Text = "FULL NAME";
			this.L6.Top = 3.125F;
			this.L6.Width = 1.3125F;
			// 
			// L1
			// 
			this.L1.Height = 0.1875F;
			this.L1.HyperLink = null;
			this.L1.Left = 0F;
			this.L1.MultiLine = false;
			this.L1.Name = "L1";
			this.L1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.L1.Text = "CERTIFIED ABSTRACT OF A CERTIFICATE OF MARRIAGE";
			this.L1.Top = 1.25F;
			this.L1.Width = 8.0625F;
			// 
			// L2
			// 
			this.L2.Height = 0.1875F;
			this.L2.HyperLink = null;
			this.L2.Left = 0F;
			this.L2.MultiLine = false;
			this.L2.Name = "L2";
			this.L2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.L2.Text = "DEPARTMENT OF HEALTH AND HUMAN SERVICES";
			this.L2.Top = 1.5F;
			this.L2.Width = 8.0625F;
			// 
			// txtF18
			// 
			this.txtF18.Height = 0.1875F;
			this.txtF18.Left = 2F;
			this.txtF18.Name = "txtF18";
			this.txtF18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF18.Text = "Attest";
			this.txtF18.Top = 10.1875F;
			this.txtF18.Width = 2.625F;
			// 
			// txtF17
			// 
			this.txtF17.Height = 0.1875F;
			this.txtF17.Left = 2F;
			this.txtF17.Name = "txtF17";
			this.txtF17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF17.Text = "Date Issued";
			this.txtF17.Top = 9.6875F;
			this.txtF17.Width = 2.604167F;
			// 
			// txtF19
			// 
			this.txtF19.Height = 0.1875F;
			this.txtF19.Left = 4.75F;
			this.txtF19.Name = "txtF19";
			this.txtF19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF19.Text = "Muni";
			this.txtF19.Top = 10.1875F;
			this.txtF19.Width = 2.6875F;
			// 
			// L3
			// 
			this.L3.Height = 0.1875F;
			this.L3.HyperLink = null;
			this.L3.Left = 2F;
			this.L3.Name = "L3";
			this.L3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L3.Text = "GROOM";
			this.L3.Top = 2.9375F;
			this.L3.Width = 1.3125F;
			// 
			// L4
			// 
			this.L4.Height = 0.1875F;
			this.L4.HyperLink = null;
			this.L4.Left = 6F;
			this.L4.Name = "L4";
			this.L4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L4.Text = "BRIDE";
			this.L4.Top = 2.9375F;
			this.L4.Width = 1.3125F;
			// 
			// txtF1
			// 
			this.txtF1.Height = 0.1875F;
			this.txtF1.Left = 2F;
			this.txtF1.Name = "txtF1";
			this.txtF1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF1.Text = null;
			this.txtF1.Top = 3.125F;
			this.txtF1.Width = 2.375F;
			// 
			// txtF2
			// 
			this.txtF2.Height = 0.1875F;
			this.txtF2.Left = 6F;
			this.txtF2.Name = "txtF2";
			this.txtF2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF2.Text = null;
			this.txtF2.Top = 3.125F;
			this.txtF2.Width = 2.125F;
			// 
			// L8
			// 
			this.L8.Height = 0.2083333F;
			this.L8.HyperLink = null;
			this.L8.Left = 2F;
			this.L8.Name = "L8";
			this.L8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L8.Text = "CITY / TOWN / LOCATION";
			this.L8.Top = 3.75F;
			this.L8.Width = 1.6875F;
			// 
			// L9
			// 
			this.L9.Height = 0.1875F;
			this.L9.HyperLink = null;
			this.L9.Left = 4.5F;
			this.L9.Name = "L9";
			this.L9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L9.Text = "RESIDENCE";
			this.L9.Top = 3.75F;
			this.L9.Width = 1.3125F;
			// 
			// L10
			// 
			this.L10.Height = 0.2083333F;
			this.L10.HyperLink = null;
			this.L10.Left = 6F;
			this.L10.Name = "L10";
			this.L10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L10.Text = "CITY / TOWN / LOCATION";
			this.L10.Top = 3.75F;
			this.L10.Width = 1.90625F;
			// 
			// txtF3
			// 
			this.txtF3.Height = 0.1875F;
			this.txtF3.Left = 2F;
			this.txtF3.Name = "txtF3";
			this.txtF3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF3.Text = "Residence Groom";
			this.txtF3.Top = 3.9375F;
			this.txtF3.Width = 2.375F;
			// 
			// txtF4
			// 
			this.txtF4.Height = 0.1875F;
			this.txtF4.Left = 6F;
			this.txtF4.Name = "txtF4";
			this.txtF4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF4.Text = "Residence Bride";
			this.txtF4.Top = 3.9375F;
			this.txtF4.Width = 2.125F;
			// 
			// L11
			// 
			this.L11.Height = 0.2083333F;
			this.L11.HyperLink = null;
			this.L11.Left = 0.59375F;
			this.L11.Name = "L11";
			this.L11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L11.Text = "AGE";
			this.L11.Top = 4.208333F;
			this.L11.Width = 0.8125F;
			// 
			// L13
			// 
			this.L13.Height = 0.1875F;
			this.L13.HyperLink = null;
			this.L13.Left = 4.5F;
			this.L13.Name = "L13";
			this.L13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L13.Text = "AGE";
			this.L13.Top = 4.1875F;
			this.L13.Width = 0.8125F;
			// 
			// L12
			// 
			this.L12.Height = 0.1666667F;
			this.L12.HyperLink = null;
			this.L12.Left = 2F;
			this.L12.Name = "L12";
			this.L12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L12.Text = "BIRTHPLACE";
			this.L12.Top = 4.208333F;
			this.L12.Width = 1.3125F;
			// 
			// L14
			// 
			this.L14.Height = 0.1666667F;
			this.L14.HyperLink = null;
			this.L14.Left = 6F;
			this.L14.Name = "L14";
			this.L14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L14.Text = "BIRTHPLACE";
			this.L14.Top = 4.208333F;
			this.L14.Width = 1.3125F;
			// 
			// txtF5
			// 
			this.txtF5.Height = 0.2083333F;
			this.txtF5.Left = 0.59375F;
			this.txtF5.Name = "txtF5";
			this.txtF5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF5.Text = "Age";
			this.txtF5.Top = 4.375F;
			this.txtF5.Width = 0.5F;
			// 
			// txtF6
			// 
			this.txtF6.Height = 0.2083333F;
			this.txtF6.Left = 2F;
			this.txtF6.Name = "txtF6";
			this.txtF6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF6.Text = "Town of Birth";
			this.txtF6.Top = 4.375F;
			this.txtF6.Width = 2.03125F;
			// 
			// txtF7
			// 
			this.txtF7.Height = 0.25F;
			this.txtF7.Left = 4.5F;
			this.txtF7.Name = "txtF7";
			this.txtF7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF7.Text = "Age";
			this.txtF7.Top = 4.375F;
			this.txtF7.Width = 0.5F;
			// 
			// txtF8
			// 
			this.txtF8.Height = 0.2083333F;
			this.txtF8.Left = 6F;
			this.txtF8.Name = "txtF8";
			this.txtF8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF8.Text = "Town of Birth";
			this.txtF8.Top = 4.375F;
			this.txtF8.Width = 1.96875F;
			// 
			// L15
			// 
			this.L15.Height = 0.2083333F;
			this.L15.HyperLink = null;
			this.L15.Left = 0.84375F;
			this.L15.Name = "L15";
			this.L15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L15.Text = "DATE INTENTIONS FILED";
			this.L15.Top = 5.5F;
			this.L15.Width = 2.25F;
			// 
			// L16
			// 
			this.L16.Height = 0.1875F;
			this.L16.HyperLink = null;
			this.L16.Left = 4.5F;
			this.L16.Name = "L16";
			this.L16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L16.Text = "DATE OF MARRIAGE";
			this.L16.Top = 5.5F;
			this.L16.Width = 1.9375F;
			// 
			// L17
			// 
			this.L17.Height = 0.2083333F;
			this.L17.HyperLink = null;
			this.L17.Left = 0.84375F;
			this.L17.Name = "L17";
			this.L17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L17.Text = "BY WHOM MARRIED";
			this.L17.Top = 6F;
			this.L17.Width = 1.9375F;
			// 
			// L18
			// 
			this.L18.Height = 0.1875F;
			this.L18.HyperLink = null;
			this.L18.Left = 4.5F;
			this.L18.Name = "L18";
			this.L18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L18.Text = "OFFICIAL TITLE";
			this.L18.Top = 6F;
			this.L18.Width = 1.9375F;
			// 
			// L20
			// 
			this.L20.Height = 0.2083333F;
			this.L20.HyperLink = null;
			this.L20.Left = 0.84375F;
			this.L20.Name = "L20";
			this.L20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L20.Text = "NAME OF RECORDING CLERK";
			this.L20.Top = 7.125F;
			this.L20.Width = 2.375F;
			// 
			// L21
			// 
			this.L21.Height = 0.2083333F;
			this.L21.HyperLink = null;
			this.L21.Left = 4.625F;
			this.L21.Name = "L21";
			this.L21.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L21.Text = "CITY / TOWN";
			this.L21.Top = 7.125F;
			this.L21.Width = 1.125F;
			// 
			// L22
			// 
			this.L22.Height = 0.2083333F;
			this.L22.HyperLink = null;
			this.L22.Left = 6.5F;
			this.L22.Name = "L22";
			this.L22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L22.Text = "DATE OF FILING";
			this.L22.Top = 7.125F;
			this.L22.Width = 1.21875F;
			// 
			// txtF9
			// 
			this.txtF9.Height = 0.1875F;
			this.txtF9.Left = 0.875F;
			this.txtF9.Name = "txtF9";
			this.txtF9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF9.Text = "Date of Intentions";
			this.txtF9.Top = 5.6875F;
			this.txtF9.Width = 2.125F;
			// 
			// txtF10
			// 
			this.txtF10.Height = 0.1875F;
			this.txtF10.Left = 4.5F;
			this.txtF10.Name = "txtF10";
			this.txtF10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF10.Text = "Date of Marriage";
			this.txtF10.Top = 5.6875F;
			this.txtF10.Width = 3.5F;
			// 
			// txtF11
			// 
			this.txtF11.Height = 0.1875F;
			this.txtF11.Left = 0.875F;
			this.txtF11.Name = "txtF11";
			this.txtF11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF11.Text = "Official Marrier";
			this.txtF11.Top = 6.125F;
			this.txtF11.Width = 2.4375F;
			// 
			// txtF12
			// 
			this.txtF12.Height = 0.1875F;
			this.txtF12.Left = 4.5F;
			this.txtF12.Name = "txtF12";
			this.txtF12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF12.Text = "Official Marrier\'s Title";
			this.txtF12.Top = 6.125F;
			this.txtF12.Width = 2.125F;
			// 
			// txtF14
			// 
			this.txtF14.Height = 0.1875F;
			this.txtF14.Left = 0.875F;
			this.txtF14.Name = "txtF14";
			this.txtF14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF14.Text = "Clerk Name";
			this.txtF14.Top = 7.3125F;
			this.txtF14.Width = 2.375F;
			// 
			// txtF15
			// 
			this.txtF15.Height = 0.1875F;
			this.txtF15.Left = 4.625F;
			this.txtF15.Name = "txtF15";
			this.txtF15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF15.Text = "CityName";
			this.txtF15.Top = 7.3125F;
			this.txtF15.Width = 1.5F;
			// 
			// txtF16
			// 
			this.txtF16.Height = 0.1875F;
			this.txtF16.Left = 6.5F;
			this.txtF16.Name = "txtF16";
			this.txtF16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF16.Text = "Date";
			this.txtF16.Top = 7.3125F;
			this.txtF16.Width = 1.625F;
			// 
			// L19
			// 
			this.L19.Height = 0.2083333F;
			this.L19.HyperLink = null;
			this.L19.Left = 0.875F;
			this.L19.Name = "L19";
			this.L19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L19.Text = "PLACE OF MARRIAGE";
			this.L19.Top = 6.375F;
			this.L19.Width = 1.9375F;
			// 
			// txtF13
			// 
			this.txtF13.Height = 0.25F;
			this.txtF13.Left = 0.875F;
			this.txtF13.Name = "txtF13";
			this.txtF13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtF13.Text = "Place of Marriage";
			this.txtF13.Top = 6.5625F;
			this.txtF13.Width = 2.125F;
			// 
			// lblIssued
			// 
			this.lblIssued.Height = 0.2083333F;
			this.lblIssued.HyperLink = null;
			this.lblIssued.Left = 2F;
			this.lblIssued.Name = "lblIssued";
			this.lblIssued.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblIssued.Text = "DATE ISSUED";
			this.lblIssued.Top = 9.5F;
			this.lblIssued.Width = 1.125F;
			// 
			// lblAttest
			// 
			this.lblAttest.Height = 0.2083333F;
			this.lblAttest.HyperLink = null;
			this.lblAttest.Left = 2F;
			this.lblAttest.Name = "lblAttest";
			this.lblAttest.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblAttest.Text = "ATTESTED BY";
			this.lblAttest.Top = 10F;
			this.lblAttest.Width = 1.125F;
			// 
			// lblTown
			// 
			this.lblTown.Height = 0.2083333F;
			this.lblTown.HyperLink = null;
			this.lblTown.Left = 4.71875F;
			this.lblTown.Name = "lblTown";
			this.lblTown.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblTown.Text = "CITY / TOWN";
			this.lblTown.Top = 10F;
			this.lblTown.Width = 1.125F;
			// 
			// rptLabelMarriages
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.194445F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.L7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAttest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label L7;
		private GrapeCity.ActiveReports.SectionReportModel.Label L5;
		private GrapeCity.ActiveReports.SectionReportModel.Label L6;
		private GrapeCity.ActiveReports.SectionReportModel.Label L1;
		private GrapeCity.ActiveReports.SectionReportModel.Label L2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF19;
		private GrapeCity.ActiveReports.SectionReportModel.Label L3;
		private GrapeCity.ActiveReports.SectionReportModel.Label L4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF2;
		private GrapeCity.ActiveReports.SectionReportModel.Label L8;
		private GrapeCity.ActiveReports.SectionReportModel.Label L9;
		private GrapeCity.ActiveReports.SectionReportModel.Label L10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF4;
		private GrapeCity.ActiveReports.SectionReportModel.Label L11;
		private GrapeCity.ActiveReports.SectionReportModel.Label L13;
		private GrapeCity.ActiveReports.SectionReportModel.Label L12;
		private GrapeCity.ActiveReports.SectionReportModel.Label L14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF8;
		private GrapeCity.ActiveReports.SectionReportModel.Label L15;
		private GrapeCity.ActiveReports.SectionReportModel.Label L16;
		private GrapeCity.ActiveReports.SectionReportModel.Label L17;
		private GrapeCity.ActiveReports.SectionReportModel.Label L18;
		private GrapeCity.ActiveReports.SectionReportModel.Label L20;
		private GrapeCity.ActiveReports.SectionReportModel.Label L21;
		private GrapeCity.ActiveReports.SectionReportModel.Label L22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF16;
		private GrapeCity.ActiveReports.SectionReportModel.Label L19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIssued;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAttest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTown;
	}
}
