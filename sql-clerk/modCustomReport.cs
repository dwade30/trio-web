﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public class modCustomReport
	{
		public const int GRIDTEXT = 1;
		public const int GRIDDATE = 2;
		public const int GRIDCOMBOIDTEXT = 3;
		public const int GRIDNUMRANGE = 4;
		public const int GRIDCOMBOIDNUM = 5;
		public const int GRIDCOMBOTEXT = 6;
		public const int GRIDTEXTRANGE = 7;
		public const int GRIDBOOLEAN = 8;
		public const string CUSTOMREPORTDATABASE = "Twck0000.vb1";

		public static object FixQuotes(string strValue)
		{
			object FixQuotes = null;
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void SetColumnCaptions(ref dynamic FormName)
		{
			// THIS ROUTINE GETS THE CAPTIONS THAT ARE TO BE DISPLAYED ON
			// THE REPORT ITSELF
			// THIS ARRAY (strFieldCaptions) HOLDS ONLY THE FIELD NAMES THAT ARE
			// TO BE DISPLAYED WHERE THE ARRAY (strCaptions) HOLDS ALL POSSIBLE
			// FIELD NAMES
			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				if (FormName.lstFields.Selected(intCounter))
				{
					Statics.strFieldCaptions[intCount] = Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}

		public static void GetNumberOfFields(string strSQL)
		{
			// THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
			// ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
			// TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
			// ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
			// vbPorter upgrade warning: strTemp As object	OnWrite(string())
			string[] strTemp;
			object strTemp2;
			int intCount;
			int intCounter;
			if (fecherFoundation.Strings.Trim(strSQL) == string.Empty)
			{
				Statics.intNumberOfSQLFields = -1;
				return;
			}
			strTemp = Strings.Split(strSQL, "from", -1, CompareConstants.vbBinaryCompare);
			strSQL = Strings.Mid(FCConvert.ToString(((object[])strTemp)[0]), 7, FCConvert.ToString(((object[])strTemp)[0]).Length - 6);
			strTemp = Strings.Split(strSQL, ",", -1, CompareConstants.vbBinaryCompare);
			Statics.intNumberOfSQLFields = Information.UBound(strTemp, 1);
			for (intCounter = 0; intCounter <= Statics.intNumberOfSQLFields; intCounter++)
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[intCounter])) != string.Empty)
				{
					// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
					Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
					// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
					// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
					for (intCount = 0; intCount <= Statics.intNumberOfSQLFields; intCount++)
					{
						if (Statics.strFields[intCount] == fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strTemp)[intCounter])))
						{
							Statics.strFieldCaptions[intCounter] = Statics.strCaptions[intCount];
							break;
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static string CheckForAS(string strFieldName, int intSegment = 2)
		{
			string CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// vbPorter upgrade warning: strTemp As object	OnWrite(string())
			string[] strTemp = null;
			strFieldName = Strings.Replace(strFieldName, " as ", " AS ", 1, -1, CompareConstants.vbTextCompare);
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(fecherFoundation.Strings.Trim(strFieldName), " AS ", -1, CompareConstants.vbBinaryCompare);
				// If UCase(strTemp(1)) = "AS" Then
				// RETURN THE SQL REFERENCE FIELD NAME
				if (intSegment == 1)
				{
					CheckForAS = strTemp[0];
				}
				else
				{
					CheckForAS = strTemp[1];
				}
				// Else
				// RETURN THE ACTUAL DATABASE FIELD NAME
				// CheckForAS = strTemp(0)
				// End If
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				// strTemp = Split(Trim(strFieldName), " ")
				// CheckForAS = Trim(strTemp(0))
				CheckForAS = strFieldName;
			}
			return CheckForAS;
		}

		public static void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 50; intCount++)
			{
				Statics.strComboList[intCount, 0] = string.Empty;
				Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomReport, frmCustomLabels)
		public static void SetFormFieldCaptions(FCForm FormName, string ReportType)
		{
			// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
			// 
			// ****************************************************************
			// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
			// TO ADD A NEW REPORT TYPE.
			// ****************************************************************
			Statics.strReportType = fecherFoundation.Strings.UCase(ReportType);
			// CLEAR THE COMBO LIST ARRAY
			ClearComboListArray();
			if (fecherFoundation.Strings.UCase(ReportType) == "DOGINVENTORY")
			{
				SetDogInventoryParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "DOGREMINDERS") || (fecherFoundation.Strings.UCase(ReportType) == "EXPIRINGDOGREMINDERS"))
			{
				SetDogReminderParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "DOGS") || (fecherFoundation.Strings.UCase(ReportType) == "DOGSLABELS"))
			{
				// THIS WILL ALLOW THE USER TO REPORT ON DOG INFORMATION
				SetDogParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "BURIAL") || (fecherFoundation.Strings.UCase(ReportType) == "BURIALLABELS"))
			{
				SetBurialParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "BIRTHS") || (fecherFoundation.Strings.UCase(ReportType) == "BIRTHLABELS"))
			{
				SetBirthParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "FOREIGNBIRTH") || (fecherFoundation.Strings.UCase(ReportType) == "FOREIGNBIRTHLABELS"))
			{
				SetForeignBirthParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "DELAYEDBIRTH") || (fecherFoundation.Strings.UCase(ReportType) == "DELAYEDBIRTHLABELS"))
			{
				SetDelayedBirthParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "DEATH") || (fecherFoundation.Strings.UCase(ReportType) == "DEATHLABELS"))
			{
				SetDeathParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "MARRIAGE") || (fecherFoundation.Strings.UCase(ReportType) == "MARRIAGELABELS"))
			{
				SetMarriageParameters(FormName);
			}
			else if ((fecherFoundation.Strings.UCase(ReportType) == "GROUP") || (fecherFoundation.Strings.UCase(ReportType) == "GROUPLABELS"))
			{
				SetGroupParameters(FormName);
			}
			// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
			LoadSortList(FormName);
			// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
			LoadWhereGrid(FormName);
			// LOAD THE SAVED REPORT COMBO ON THE FORM
			fecherFoundation.FCUtils.CallByName(FormName, "LoadCombo", CallType.Method);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetDeathParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            Statics.strCustomTitle = "**** Custom Death Report ****";
			FormName.lstFields.AddItem("Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Sex");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Date of Death");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Date Of Birth");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Place of Disposition");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Date of Disposition");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("File Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Correction");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Supplemental");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Fetal Death");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Amendments");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Mothers FirstName");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("Mothers MiddleName");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Mothers LastName");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("Fathers FirstName");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("Fathers MiddleName");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("Fathers LastName");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("Record had Older Data");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("City of Death");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			FormName.lstFields.AddItem("Age");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			Statics.strFields[0] = "LastName";
			Statics.strFields[1] = "FirstName";
			Statics.strFields[2] = "MiddleName";
			Statics.strFields[3] = "Sex";
			Statics.strFields[4] = "DateofDeath";
			Statics.strFields[5] = "DateOfBirth";
			Statics.strFields[6] = "PlaceOfDisposition";
			Statics.strFields[7] = "DateofDisposition";
			Statics.strFields[8] = "FileNumber";
			Statics.strFields[9] = "Correction";
			Statics.strFields[10] = "Supplemental";
			Statics.strFields[11] = "FetalDeath";
			Statics.strFields[12] = "Amendment";
			Statics.strFields[13] = "MotherFirstName";
			Statics.strFields[14] = "MotherMiddleName";
			Statics.strFields[15] = "MotherLastName";
			Statics.strFields[16] = "FatherFirstName";
			Statics.strFields[17] = "FatherMiddleName";
			Statics.strFields[18] = "FatherLastName";
			Statics.strFields[19] = "NoRequiredFields";
			Statics.strFields[20] = "CityOrTownOfDeath";
			Statics.strFields[21] = "Age";
			Statics.strCaptions[0] = "Last Name";
			Statics.strCaptions[1] = "First Name";
			Statics.strCaptions[2] = "Middle Name";
			Statics.strCaptions[3] = "Sex";
			Statics.strCaptions[4] = "Date of Death";
			Statics.strCaptions[5] = "Date Of Birth";
			Statics.strCaptions[6] = "Place of Disposition";
			Statics.strCaptions[7] = "Date of Disposition";
			Statics.strCaptions[8] = "File Number";
			Statics.strCaptions[9] = "Correction";
			Statics.strCaptions[10] = "Supplemental";
			Statics.strCaptions[11] = "Fetal Death";
			Statics.strCaptions[12] = "Amendment";
			Statics.strCaptions[13] = "Mothers FirstName";
			Statics.strCaptions[14] = "Mothers MiddleName";
			Statics.strCaptions[15] = "Mothers LastName";
			Statics.strCaptions[16] = "Fathers FirstName";
			Statics.strCaptions[17] = "Fathers MiddleName";
			Statics.strCaptions[18] = "Fathers LastName";
			Statics.strCaptions[19] = "Older Data";
			Statics.strCaptions[20] = "City of Death";
			Statics.strCaptions[21] = "Age";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[19] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[20] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[21] = FCConvert.ToString(GRIDNUMRANGE);
			LoadGridCellAsCombo(FormName, 3, "#0;M" + "\t" + "Male|#1;F" + "\t" + "Female");
			LoadGridCellAsCombo(FormName, 6, "Select * from DispositionLocation", "Name", "ID", "PlaceOfDisposition");
			LoadGridCellAsCombo(FormName, 9, "#0;T" + "\t" + "True|#1;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 10, "#0;T" + "\t" + "True|#1;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 11, "#0;T" + "\t" + "True|#1;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 12, "#0;T" + "\t" + "True|#1;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 19, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			// FormName.fraFields.Tag = "from Deaths INNER JOIN DispositionLocation ON DispositionLocation.ID = val(Deaths.PlaceOfDisposition)"
			FormName.fraFields.Tag = "from Deaths ";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetGroupParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            Statics.strCustomTitle = "**** Custom Group / Committee Report ****";
			FormName.lstFields.AddItem("Group's Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Group's Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Group's User Defined Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Member's Title");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Member's Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Member's Address 1");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Member's Address 2");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Member's City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Member's State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Member's Zip");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Member's Zip 4");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Member's Term Start Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Member's Term End Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Member's Email Address");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("Member's Work Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Member's Home Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("Member's Cell Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("Member's Other Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("Member's User Defined Field");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			Statics.strFields[0] = "GroupName";
			Statics.strFields[1] = "GroupDescription";
			Statics.strFields[2] = "UserDefinedDescription";
			Statics.strFields[3] = "Title";
			Statics.strFields[4] = "Name";
			Statics.strFields[5] = "Address1";
			Statics.strFields[6] = "Address2";
			Statics.strFields[7] = "City";
			Statics.strFields[8] = "State";
			Statics.strFields[9] = "Zip";
			Statics.strFields[10] = "Zip4";
			Statics.strFields[11] = "TermFrom";
			Statics.strFields[12] = "TermTo";
			Statics.strFields[13] = "Email";
			Statics.strFields[14] = "Telephone";
			Statics.strFields[15] = "HomeNumber";
			Statics.strFields[16] = "CellNumber";
			Statics.strFields[17] = "OtherNumber";
			Statics.strFields[18] = "UserDefined";
			Statics.strCaptions[0] = "Group Name";
			Statics.strCaptions[1] = "Group Description";
			Statics.strCaptions[2] = "Group User Descriptio";
			Statics.strCaptions[3] = "Title";
			Statics.strCaptions[4] = "Name";
			Statics.strCaptions[5] = "Address 1";
			Statics.strCaptions[6] = "Address 2";
			Statics.strCaptions[7] = "City";
			Statics.strCaptions[8] = "State";
			Statics.strCaptions[9] = "Zip";
			Statics.strCaptions[10] = "Zip 4";
			Statics.strCaptions[11] = "Term Start Date";
			Statics.strCaptions[12] = "Term End Date";
			Statics.strCaptions[13] = "Email Address";
			Statics.strCaptions[14] = "Work Number";
			Statics.strCaptions[15] = "Home Number";
			Statics.strCaptions[16] = "Cell Number";
			Statics.strCaptions[17] = "Other Number";
			Statics.strCaptions[18] = "User Defined";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDTEXT);
			FormName.fraFields.Tag = "from GroupList INNER JOIN GroupMembers ON GroupList.ID = GroupMembers.GroupID ";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetMarriageParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            Statics.strCustomTitle = "**** Custom Marriage Report ****";
			FormName.lstFields.AddItem("Groom's Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Groom's First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Groom's Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Groom's DOB");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Groom's Birthplace");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Groom's Street Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Groom's Steet Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Groom's City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Groom's County");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Groom's State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Groom's Zip");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Bride's Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Bride's First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Bride's Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("Bride's Maiden Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Bride's DOB");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("Bride's Birthplace");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("Bride's Street Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("Bride's Steet Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("Bride's City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("Bride's County");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			FormName.lstFields.AddItem("Bride's State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			FormName.lstFields.AddItem("Bride's Zip");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 22);
			FormName.lstFields.AddItem("Date Intentions Filed");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 23);
			FormName.lstFields.AddItem("Date License Issued");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 24);
			FormName.lstFields.AddItem("File Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 25);
			FormName.lstFields.AddItem("Ceremony Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 26);
			FormName.lstFields.AddItem("Record has Older Data");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 27);
			Statics.strFields[0] = "GroomsLastName";
			Statics.strFields[1] = "GroomsFirstName";
			Statics.strFields[2] = "GroomsMiddleName";
			Statics.strFields[3] = "GroomsDOB";
			Statics.strFields[4] = "GroomSBirthPlace";
			Statics.strFields[5] = "GroomsStreetAddress";
			Statics.strFields[6] = "GroomsStreetName";
			Statics.strFields[7] = "GroomsCity";
			Statics.strFields[8] = "GroomsCounty";
			Statics.strFields[9] = "GroomsState";
			Statics.strFields[10] = "GroomZip";
			Statics.strFields[11] = "BridesCurrentLastName";
			Statics.strFields[12] = "BridesFirstName";
			Statics.strFields[13] = "BridesMiddleName";
			Statics.strFields[14] = "BridesMaidenSurname";
			Statics.strFields[15] = "BridesDOB";
			Statics.strFields[16] = "BrideBirthPlace";
			Statics.strFields[17] = "BridesStreetNumber";
			Statics.strFields[18] = "BridesStreetName";
			Statics.strFields[19] = "BridesCity";
			Statics.strFields[20] = "BridesCounty";
			Statics.strFields[21] = "BridesState";
			Statics.strFields[22] = "BrideZip";
			Statics.strFields[23] = "DateIntentionsFiled";
			Statics.strFields[24] = "DateLicenseIssued";
			Statics.strFields[25] = "FileNumber";
			Statics.strFields[26] = "CeremonyDate";
			Statics.strFields[27] = "doginfo.NoRequiredFields";
			Statics.strCaptions[0] = "Grooms Last Name";
			Statics.strCaptions[1] = "Grooms First Name";
			Statics.strCaptions[2] = "Grooms Middle Name";
			Statics.strCaptions[3] = "Grooms DOB";
			Statics.strCaptions[4] = "Groom Birthplace";
			Statics.strCaptions[5] = "Grooms Street Number";
			Statics.strCaptions[6] = "Grooms Street Name";
			Statics.strCaptions[7] = "Grooms City";
			Statics.strCaptions[8] = "Grooms County";
			Statics.strCaptions[9] = "Grooms State";
			Statics.strCaptions[10] = "Groom Zip";
			Statics.strCaptions[11] = "Brides Current Last Name";
			Statics.strCaptions[12] = "Brides First Name";
			Statics.strCaptions[13] = "Brides Middle Name";
			Statics.strCaptions[14] = "Brides Maiden Surname";
			Statics.strCaptions[15] = "Brides DOB";
			Statics.strCaptions[16] = "Bride Birthplace";
			Statics.strCaptions[17] = "Brides Street Number";
			Statics.strCaptions[18] = "Brides Street Name";
			Statics.strCaptions[19] = "Brides City";
			Statics.strCaptions[20] = "Brides County";
			Statics.strCaptions[21] = "Brides State";
			Statics.strCaptions[22] = "Bride Zip";
			Statics.strCaptions[23] = "Date Intentions Filed";
			Statics.strCaptions[24] = "Date License Issued";
			Statics.strCaptions[25] = "File Number";
			Statics.strCaptions[26] = "Ceremony Date";
			Statics.strCaptions[27] = "Older Data";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[19] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[20] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[21] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[22] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[23] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[24] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[25] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[26] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[27] = FCConvert.ToString(GRIDBOOLEAN);
			// Call LoadGridCellAsCombo(FormName, 4, "Select * from States order by Description", "Description", "ID", "GroomsBirthPlace")
			// Call LoadGridCellAsCombo(FormName, 16, "Select * from States order by Description", "Description", "ID", "BrideBirthPlace")
			LoadGridCellAsCombo(FormName, 27, "#0;T " + "\t" + "True|#1;F" + "\t" + "False");
			// FormName.fraFields.Tag = "FROM (Marriages INNER JOIN States ON val(Marriages.GroomsBirthplace) = States.ID) INNER JOIN States AS States_1 ON val(Marriages.BridesBirthplace) = States_1.ID"
			FormName.fraFields.Tag = "FROM Marriages ";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetForeignBirthParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            Statics.strCustomTitle = "**** Custom Foreign Birth Report ****";
			FormName.lstFields.AddItem("Child Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Child First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Child Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Child Sex");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Date Of Birth");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Mothers Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Mothers First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Fathers Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Fathers First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("BOW");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("File Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Record has Older Data");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("City of Birth");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Mothers Maiden Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			Statics.strFields[0] = "ChildLastName";
			Statics.strFields[1] = "ChildFirstName";
			Statics.strFields[2] = "ChildMiddleName";
			Statics.strFields[3] = "ChildSex";
			Statics.strFields[4] = "ChildDOB";
			Statics.strFields[5] = "MothersLastName";
			Statics.strFields[6] = "MothersFirstName";
			Statics.strFields[7] = "FathersLastName";
			Statics.strFields[8] = "FathersFirstName";
			Statics.strFields[9] = "Legitimate";
			Statics.strFields[10] = "FileNumber";
			Statics.strFields[11] = "NoRequiredFields";
			Statics.strFields[12] = "ChildTown";
			Statics.strFields[13] = "MothersMaidenName";
			Statics.strCaptions[0] = "Child Last Name";
			Statics.strCaptions[1] = "Child First Name";
			Statics.strCaptions[2] = "Child Middle Name";
			Statics.strCaptions[3] = "Child Sex";
			Statics.strCaptions[4] = "Child Date Of Birth";
			Statics.strCaptions[5] = "Mothers Last Name";
			Statics.strCaptions[6] = "Mothers First Name";
			Statics.strCaptions[7] = "Fathers Last Name";
			Statics.strCaptions[8] = "Fathers First Name";
			Statics.strCaptions[9] = "Legitimate";
			Statics.strCaptions[10] = "File Number";
			Statics.strCaptions[11] = "Older Data";
			Statics.strCaptions[12] = "City of Birth";
			Statics.strCaptions[13] = "Mothers Maiden Name";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXTRANGE);
			LoadGridCellAsCombo(FormName, 3, "#0;Male|#1;Female");
			LoadGridCellAsCombo(FormName, 9, "#0;True|#-1;False");
			LoadGridCellAsCombo(FormName, 11, "#0;T " + "\t" + "True|#-1;F" + "\t" + "False");
			FormName.fraFields.Tag = "from BirthsForeign ";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetDelayedBirthParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            Statics.strCustomTitle = "**** Custom Delayed Birth Report ****";
			FormName.lstFields.AddItem("Child Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Child First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Child Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Child Sex");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Date Of Birth");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Mothers Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Mothers First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Fathers Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Fathers First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("BOW");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("File Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Record has Older Data");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Mothers Maiden Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			Statics.strFields[0] = "ChildLastName";
			Statics.strFields[1] = "ChildFirstName";
			Statics.strFields[2] = "ChildMiddleName";
			Statics.strFields[3] = "ChildSex";
			Statics.strFields[4] = "ChildDOB";
			Statics.strFields[5] = "MothersLastName";
			Statics.strFields[6] = "MothersFirstName";
			Statics.strFields[7] = "FathersLastName";
			Statics.strFields[8] = "FathersFirstName";
			Statics.strFields[9] = "Legitimate";
			Statics.strFields[10] = "FileNumber";
			Statics.strFields[11] = "NoRequiredFields";
			Statics.strFields[12] = "MothersMaidenName";
			Statics.strCaptions[0] = "Child Last Name";
			Statics.strCaptions[1] = "Child First Name";
			Statics.strCaptions[2] = "Child Middle Name";
			Statics.strCaptions[3] = "Child Sex";
			Statics.strCaptions[4] = "Child Date Of Birth";
			Statics.strCaptions[5] = "Mothers Last Name";
			Statics.strCaptions[6] = "Mothers First Name";
			Statics.strCaptions[7] = "Fathers Last Name";
			Statics.strCaptions[8] = "Fathers First Name";
			Statics.strCaptions[9] = "Legitimate";
			Statics.strCaptions[10] = "File Number";
			Statics.strCaptions[11] = "Older Data";
			Statics.strCaptions[12] = "Mothers Maiden Name";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDTEXTRANGE);
			LoadGridCellAsCombo(FormName, 3, "#0;Male|#1;Female");
			LoadGridCellAsCombo(FormName, 9, "#1;True|#0;False");
			LoadGridCellAsCombo(FormName, 11, "#1;T " + "\t" + "True|#0;F" + "\t" + "False");
			FormName.fraFields.Tag = "from BirthsDelayed ";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetBirthParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            Statics.strCustomTitle = "**** Custom Birth Report ****";
			FormName.lstFields.AddItem("Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Sex");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Birth Place");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Date Of Birth");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Mothers Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Mothers First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Fathers Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Fathers First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Date Of Filing");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("BOW");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("File Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Mothers Address");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("Mothers City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Mothers State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("Mothers Zip");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("Mothers Birthplace");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("Fathers Birthplace");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("Record has Older Data");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("Attest Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			FormName.lstFields.AddItem("Mothers Maiden Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			FormName.lstFields.AddItem("City of Birth");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 22);
			FormName.lstFields.AddItem("Attendants First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 23);
			FormName.lstFields.AddItem("Attendants Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 24);
			FormName.lstFields.AddItem("Attendants Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 25);
			FormName.lstFields.AddItem("Attendants Title");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 26);
			Statics.strFields[0] = "LastName";
			Statics.strFields[1] = "FirstName";
			Statics.strFields[2] = "MiddleName";
			Statics.strFields[3] = "Sex";
			Statics.strFields[4] = "BirthPlace";
			Statics.strFields[5] = "DateOfBirth";
			Statics.strFields[6] = "MothersLastName";
			Statics.strFields[7] = "MothersFirstName";
			Statics.strFields[8] = "FathersLastName";
			Statics.strFields[9] = "FathersFirstName";
			Statics.strFields[10] = "DateOfFiling";
			Statics.strFields[11] = "Legitimate";
			Statics.strFields[12] = "FileNumber";
			Statics.strFields[13] = "MothersAddress";
			Statics.strFields[14] = "MotherTown";
			Statics.strFields[15] = "MotherState";
			Statics.strFields[16] = "MothersZipCode";
			Statics.strFields[17] = "MothersBirthPlace";
			Statics.strFields[18] = "FathersBirthPlace";
			Statics.strFields[19] = "NoRequiredFields";
			Statics.strFields[20] = "AttestDate";
			Statics.strFields[21] = "MothersMaidenName";
			Statics.strFields[22] = "ChildTown";
			Statics.strFields[23] = "AttendantsFirstname";
			Statics.strFields[24] = "AttendantsMiddleInitial";
			Statics.strFields[25] = "AttendantsLastname";
			Statics.strFields[26] = "AttendantTitle";
			Statics.strCaptions[0] = "Last Name";
			Statics.strCaptions[1] = "First Name";
			Statics.strCaptions[2] = "Middle Name";
			Statics.strCaptions[3] = "Sex";
			Statics.strCaptions[4] = "Birth Place";
			Statics.strCaptions[5] = "Date Of Birth";
			Statics.strCaptions[6] = "Mothers Last Name";
			Statics.strCaptions[7] = "Mothers First Name";
			Statics.strCaptions[8] = "Fathers Last Name";
			Statics.strCaptions[9] = "Fathers First Name";
			Statics.strCaptions[10] = "Date Of Filing";
			Statics.strCaptions[11] = "Legitimate";
			Statics.strCaptions[12] = "File Number";
			Statics.strCaptions[13] = "Mothers Address";
			Statics.strCaptions[14] = "Mother City";
			Statics.strCaptions[15] = "Mother State";
			Statics.strCaptions[16] = "Mothers Zip";
			Statics.strCaptions[17] = "Mothers BirthPlace";
			Statics.strCaptions[18] = "Fathers BirthPlace";
			Statics.strCaptions[19] = "Older Data";
			Statics.strCaptions[20] = "Attest Date";
			Statics.strCaptions[21] = "Mothers Maiden name";
			Statics.strCaptions[22] = "City of Birth";
			Statics.strCaptions[23] = "Attendants First Name";
			Statics.strCaptions[24] = "Attendants Middle Name";
			Statics.strCaptions[25] = "Attendants Last Name";
			Statics.strCaptions[26] = "Attendants Title";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[19] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[20] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[21] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[22] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[23] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[24] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[25] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[26] = FCConvert.ToString(GRIDTEXTRANGE);
			LoadGridCellAsCombo(FormName, 3, "#0;Male|#1;Female");
			// Call LoadGridCellAsCombo(FormName, 4, "Select * from DefaultTowns", "Name", "ID", "BirthPlace")
			LoadGridCellAsCombo(FormName, 11, "#1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 14, "Select * from DefaultTowns", "Name", "ID", "BirthPlace");
			LoadGridCellAsCombo(FormName, 15, "Select * from States order by Description", "Description", "ID", "MotherState");
			LoadGridCellAsCombo(FormName, 17, "Select * from DefaultTowns", "Name", "ID", "BirthPlace");
			LoadGridCellAsCombo(FormName, 18, "Select * from DefaultTowns", "Name", "ID", "BirthPlace");
			LoadGridCellAsCombo(FormName, 19, "#0;T " + "\t" + "True|#1;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 22, "Select * from defaulttowns", "Name", "ID", "BirthPlace");
			FormName.fraFields.Tag = "from Births ";
			// FormName.fraFields.Tag = "from Births left JOIN DefaultTowns ON DefaultTowns.ID = val(Births.MotherTown)"
			// FormName.fraFields.Tag = "FROM (Births INNER JOIN States ON val(Births.MotherState) = States.ID) INNER JOIN DefaultTowns ON val(Births.MotherTown) = DefaultTowns.ID"
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetDogParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            clsDRWrapper rsTemp = new clsDRWrapper();
			// WHAT IS THE CAPTION TO SHOW ON THE TOP OF THE REPORT
			Statics.strCustomTitle = "**** CUSTOM DOG REPORT ****";
			// GIVE THE NAME TO SHOW IN THE LIST BOX
			FormName.lstFields.AddItem("Dog Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Date of Birth");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Sex");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Veterinarian");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Color");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Breed");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Dog Neuter");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Sticker Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Tag Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Rabies Expiration Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Rabies Tag Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Owner First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Owner Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Owner Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("Owner Address");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Owner City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("Owner State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("Owner Zip");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("Location Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("Location Street");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("Is Kennel?");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			FormName.lstFields.AddItem("Dog is Deceased");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			FormName.lstFields.AddItem("Dog Year");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 22);
			FormName.lstFields.AddItem("License Issue Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 23);
			FormName.lstFields.AddItem("Phone Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 24);
			FormName.lstFields.AddItem("Record has Older Data");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 25);
			FormName.lstFields.AddItem("Rabies Cert Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 26);
			FormName.lstFields.AddItem("Transaction Amount");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 27);
			FormName.lstFields.AddItem("Transaction Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 28);
			// FormName.lstFields.AddItem "Registered"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 29
			FormName.lstFields.AddItem("E-Mail");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 31);
			FormName.lstFields.AddItem("Has E-Mail");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 32);
			FormName.lstFields.AddItem("Service Dog");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 33);
			FormName.lstFields.AddItem("Hear/Guide");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 34);
            FormName.lstFields.AddItem("Nuisance Dog");
            FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 35);
            FormName.lstFields.AddItem("Dangerous Dog");
            FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 36);
			// THIS IS THE ACTUAL DATABASE FIELD NAME (NOTE THE PartyState ALIAS)
			Statics.strFields[0] = "DogName";
			Statics.strFields[1] = "DogDOB";
			Statics.strFields[2] = "DogSex";
			Statics.strFields[3] = "DogVet";
			Statics.strFields[4] = "dogColor";
			Statics.strFields[5] = "DogBreed";
			Statics.strFields[6] = "DogNeuter";
			Statics.strFields[7] = "convert(int, isnull(stickerlicnum, 0)) as stickerlic";
			Statics.strFields[8] = "TagLicNum";
			Statics.strFields[9] = "RabiesExpDate";
			Statics.strFields[10] = "RabiesTagNo";
			Statics.strFields[11] = "FirstName";
			Statics.strFields[12] = "MiddleName";
			Statics.strFields[13] = "LastName";
			Statics.strFields[14] = "Address1";
			Statics.strFields[15] = "City";
			Statics.strFields[16] = "State";
			Statics.strFields[17] = "Zip";
			Statics.strFields[18] = "LocationNum";
			Statics.strFields[19] = "LocationStr";
			Statics.strFields[20] = "KennelDog";
			Statics.strFields[21] = "DogDeceased";
			Statics.strFields[22] = "Year";
			Statics.strFields[23] = "RabStickerIssue";
			Statics.strFields[24] = "PhoneNumber";
			Statics.strFields[25] = "NOREQUIREDFIELDS";
			Statics.strFields[26] = "RabiesCertNum";
			Statics.strFields[27] = "Dogtransactions.amount as transactionamount";
			Statics.strFields[28] = "Dogtransactions.transactiondate as transactiondate";
			Statics.strFields[29] = "doginfo.ownernum";
			Statics.strFields[30] = "Year";
			Statics.strFields[31] = "EMail";
			Statics.strFields[32] = "HasEmail";
			Statics.strFields[33] = "SandR";
			Statics.strFields[34] = "HearGuide";
            Statics.strFields[35] = "IsNuisanceDog";
            Statics.strFields[36] = "IsDangerousDog";
			// THESE ARE THE CAPTIONS THAT YOU WANT THE USER TO SEE ON
			// THE REPORT IF THIS FIELD IS SELECTED.
			// NOTE:
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY. IF NOT THEN THE CAPTIONS
			// WILL NOT DISPLAY CORRECTLY ON THE REPORT
			Statics.strCaptions[0] = "Dog Name";
			Statics.strCaptions[1] = "DOB";
			Statics.strCaptions[2] = "Sex";
			Statics.strCaptions[3] = "Veterinarian";
			Statics.strCaptions[4] = "Color";
			Statics.strCaptions[5] = "Breed";
			Statics.strCaptions[6] = "Dog Neuter";
			Statics.strCaptions[7] = "Sticker Num";
			Statics.strCaptions[8] = "Tag Number";
			Statics.strCaptions[9] = "Rabies Expiration Date";
			Statics.strCaptions[10] = "Rabies Tag No";
			Statics.strCaptions[11] = "Owner First Name";
			Statics.strCaptions[12] = "Owner Middle Name";
			Statics.strCaptions[13] = "Owner Last Name";
			Statics.strCaptions[14] = "Owner Address";
			Statics.strCaptions[15] = "Owner City";
			Statics.strCaptions[16] = "Owner State";
			Statics.strCaptions[17] = "Owner Zip";
			Statics.strCaptions[18] = "Location Number";
			Statics.strCaptions[19] = "Location Street";
			Statics.strCaptions[20] = "Is Kennel?";
			Statics.strCaptions[21] = "Deceased";
			Statics.strCaptions[22] = "Dog Year";
			Statics.strCaptions[23] = "License Issue Date";
			Statics.strCaptions[24] = "Phone Number";
			Statics.strCaptions[25] = "Older Data";
			Statics.strCaptions[26] = "Rabies Cert Num";
			Statics.strCaptions[27] = "Amount";
			Statics.strCaptions[28] = "Transaction Date";
			Statics.strCaptions[29] = "Deleted";
			Statics.strCaptions[30] = "Registered";
			Statics.strCaptions[31] = "E-Mail";
			Statics.strCaptions[32] = "Has Email";
			Statics.strCaptions[33] = "Service Dog";
			Statics.strCaptions[34] = "Hear/Guide Dog";
            Statics.strCaptions[35] = "Nuisance Dog";
            Statics.strCaptions[36] = "Dangerous Dog";

			// need to indicate what table each field belongs to
			Statics.strTableNames[0] = "DogInfo";
			Statics.strTableNames[1] = "DogInfo";
			Statics.strTableNames[2] = "DogInfo";
			Statics.strTableNames[3] = "DogInfo";
			Statics.strTableNames[4] = "DogInfo";
			Statics.strTableNames[5] = "DogInfo";
			Statics.strTableNames[6] = "doginfo";
			Statics.strTableNames[7] = "doginfo";
			Statics.strTableNames[8] = "doginfo";
			Statics.strTableNames[9] = "doginfo";
			Statics.strTableNames[10] = "doginfo";
			Statics.strTableNames[11] = "dogowner";
			Statics.strTableNames[12] = "dogowner";
			Statics.strTableNames[13] = "dogowner";
			Statics.strTableNames[14] = "dogowner";
			Statics.strTableNames[15] = "dogowner";
			Statics.strTableNames[16] = "dogowner";
			Statics.strTableNames[17] = "dogowner";
			Statics.strTableNames[18] = "dogowner";
			Statics.strTableNames[19] = "dogowner";
			Statics.strTableNames[20] = "Doginfo";
			Statics.strTableNames[21] = "doginfo";
			Statics.strTableNames[22] = "doginfo";
			Statics.strTableNames[23] = "doginfo";
			Statics.strTableNames[24] = "dogowner";
			Statics.strTableNames[25] = "doginfo";
			Statics.strTableNames[26] = "doginfo";
			Statics.strTableNames[27] = "dogtransactions";
			Statics.strTableNames[28] = "dogtransactions";
			Statics.strTableNames[29] = "doginfo";
			Statics.strTableNames[30] = "doginfo";
			Statics.strTableNames[31] = "dogowner";
			Statics.strTableNames[32] = "dogowner";
			Statics.strTableNames[33] = "doginfo";
			Statics.strTableNames[34] = "doginfo";
            Statics.strTableNames[35] = "doginfo";
            Statics.strTableNames[36] = "doginfo";
			// MARK WHAT TYPE THIS FIELD IS SO THAT THE WHERE GRID
			// WILL KNOW WHAT TYPE OF DATA TO DISPLAY.
			// 
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY.
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[19] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[20] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[21] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[22] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[23] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[24] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[25] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[26] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[27] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[28] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[29] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[30] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[31] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[32] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[33] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[34] = FCConvert.ToString(GRIDBOOLEAN);
            Statics.strWhereType[35] = GRIDBOOLEAN.ToString();
            Statics.strWhereType[36] = GRIDBOOLEAN.ToString();
			// IF ONE OF THE FIELDS IS OF TYPE GRIDCOMBOIDTEXT THEN YOU NEED
			// TO CREATE A LIST TO SHOW WHEN THE USER CLICKS ON THAT CELL
			// IN THE GRID
			LoadGridCellAsCombo(FormName, 2, "#0;M" + "\t" + "Male|#1;F" + "\t" + "Female");
			LoadGridCellAsCombo(FormName, 6, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 20, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 21, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 25, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 29, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 30, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 32, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 33, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
			LoadGridCellAsCombo(FormName, 34, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
            LoadGridCellAsCombo(FormName, 35, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");
            LoadGridCellAsCombo(FormName, 36, "#-1;T " + "\t" + "True|#0;F" + "\t" + "False");

            // FINISH THE MID SECTION OF THE SQL STATEMENT
            // FormName.fraFields.Tag = ", kenneldog FROM (DogInfo left JOIN DogOwner ON (DogInfo.OwnerNum = dogowner.ID))  left join  dogtransactions on (doginfo.ID = dogtransactions.dognumber) CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p"
            FormName.fraFields.Tag = ", kenneldog FROM (DogInfo left JOIN DogOwner ON (DogInfo.OwnerNum = dogowner.ID)) left join " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView on (dogowner.PartyID = PartyAddressAndPhoneView.PartyID)  left join  dogtransactions on (doginfo.ID = dogtransactions.dognumber) ";
			// gstrWarrantSQL = "select " & rs.CurrentPrefix & "CentralParties.dbo.PartyAddressAndPhoneView.*,DogOwner.LocationNum, DogOwner.PartyID, DogOwner.LocationSTR, DogInfo.* from doginfo inner join dogowner on (doginfo.ownernum = dogowner.id) inner join " & rs.CurrentPrefix & "CentralParties.dbo.PartyAddressAndPhoneView on (dogowner.partyid = " & rs.CurrentPrefix & "CentralParties.dbo.PartyAddressAndPhoneView.PartyID) " & " where DogInfo.DogDeceased <> 1 AND DogInfo.Year < " & gintYear & gstrWhereClause & " Order by " & gstrSortField
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetDogReminderParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            // WHAT IS THE CAPTION TO SHOW ON THE TOP OF THE REPORT
            // strCustomTitle = "**** CUSTOM DOG REPORT ****"
            // GIVE THE NAME TO SHOW IN THE LIST BOX
            FormName.lstFields.AddItem("Owner First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Owner Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Owner Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Owner Address");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Owner City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Owner State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Owner Zip");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			// THIS IS THE ACTUAL DATABASE FIELD NAME (NOTE THE PartyState ALIAS)
			Statics.strFields[0] = "FirstName";
			Statics.strFields[1] = "MiddleName";
			// kk04172015 trocks-13  "MI"
			Statics.strFields[2] = "LastName";
			Statics.strFields[3] = "Address1";
			// "Address"
			Statics.strFields[4] = "City";
			Statics.strFields[5] = "p.State";
			// "PartyState"
			Statics.strFields[6] = "Zip";
			// THESE ARE THE CAPTIONS THAT YOU WANT THE USER TO SEE ON
			// THE REPORT IF THIS FIELD IS SELECTED.
			// NOTE:
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY. IF NOT THEN THE CAPTIONS
			// WILL NOT DISPLAY CORRECTLY ON THE REPORT
			Statics.strCaptions[0] = "Owner First Name";
			Statics.strCaptions[1] = "Owner Middle Name";
			Statics.strCaptions[2] = "Owner Last Name";
			Statics.strCaptions[3] = "Owner Address";
			Statics.strCaptions[4] = "Owner City";
			Statics.strCaptions[5] = "Owner State";
			Statics.strCaptions[6] = "Owner Zip";
			// MARK WHAT TYPE THIS FIELD IS SO THAT THE WHERE GRID
			// WILL KNOW WHAT TYPE OF DATA TO DISPLAY.
			// 
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY.
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXTRANGE);
			// IF ONE OF THE FIELDS IS OF TYPE GRIDCOMBOIDTEXT THEN YOU NEED
			// TO CREATE A LIST TO SHOW WHEN THE USER CLICKS ON THAT CELL
			// IN THE GRID
			// Call LoadGridCellAsCombo(FormName, 16, "Select * from States order by Description", "Description", "ID", "DogOwner.State")
			// FINISH THE MID SECTION OF THE SQL STATEMENT
			// FormName.fraFields.Tag = "FROM (((DogInfo INNER JOIN DefaultColors ON val(DogInfo.DogColor) = DefaultColors.ID) INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID) INNER JOIN DefaultBreeds ON val(DogInfo.DogBreed) = DefaultBreeds.ID)INNER JOIN States ON val(DogOwner.State) =States.ID"
			FormName.fraFields.Tag = " * FROM DogOwner  ";
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetDogInventoryParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            // WHAT IS THE CAPTION TO SHOW ON THE TOP OF THE REPORT
            Statics.strCustomTitle = "**** Custom Tag Inventory ****";
			// GIVE THE NAME TO SHOW IN THE LIST BOX
			FormName.lstFields.AddItem("Tag Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Year");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			// FormName.lstFields.AddItem "Type"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 2
			FormName.lstFields.AddItem("Status");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Date Received");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Operator ID");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			// THIS IS THE ACTUAL DATABASE FIELD NAME (NOTE THE ALIAS FOR #4)
			Statics.strFields[0] = "StickerNumber";
			Statics.strFields[1] = "Year";
			// strFields(2) = "Type"
			Statics.strFields[2] = "Status";
			Statics.strFields[3] = "DateReceived";
			Statics.strFields[4] = "ReceivedOPID";
			// THESE ARE THE CAPTIONS THAT YOU WANT THE USER TO SEE ON
			// THE REPORT IF THIS FIELD IS SELECTED.
			// NOTE:
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY. IF NOT THEN THE CAPTIONS
			// WILL NOT DISPLAY CORRECTLY ON THE REPORT
			Statics.strCaptions[0] = "Number";
			Statics.strCaptions[1] = "Year";
			// strCaptions(2) = "Type"
			Statics.strCaptions[2] = "Status";
			Statics.strCaptions[3] = "Received";
			Statics.strCaptions[4] = "Op ID";
			// MARK WHAT TYPE THIS FIELD IS SO THAT THE WHERE GRID
			// WILL KNOW WHAT TYPE OF DATA TO DISPLAY.
			// 
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY.
			Statics.strWhereType[0] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXT);
			// strWhereType(2) = GRIDCOMBOTEXT
			Statics.strWhereType[2] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXT);
			// IF ONE OF THE FIELDS IS OF TYPE GRIDCOMBOIDTEXT THEN YOU NEED
			// TO CREATE A LIST TO SHOW WHEN THE USER CLICKS ON THAT CELL
			// IN THE GRID
			// Call LoadGridCellAsCombo(FormName, 2, "#0;T" & vbTab & "Tag|#1;S" & vbTab & "Sticker")
			LoadGridCellAsCombo(FormName, 2, "#0;Active|#1;Used");
			// FINISH THE MID SECTION OF THE SQL STATEMENT
			FormName.fraFields.Tag = "from DogInventory";
		}

		public static void SetHeaderGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
			switch (fecherFoundation.Strings.UCase(Statics.strReportType))
			{
			// Case "DOGS"
			// 
			// 
			// Case "BIRTHS"
				default:
					{
						// GridName.BackColor = vbBlue
						// GridName.ForeColor = vbWhite
						break;
					}
			}
			//end switch
			// GridName.Select 0, 0, 0, GridName.Cols - 1
			// GridName.CellFontBold = True
		}

		public static void SetDataGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
			if (fecherFoundation.Strings.UCase(Statics.strReportType) == "DOGS")
			{
			}
			else if (fecherFoundation.Strings.UCase(Statics.strReportType) == "BIRTHS")
			{
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadSortList(dynamic FormName)
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				FormName.lstSort.AddItem(FormName.lstFields.List(intCounter));
				FormName.lstSort.ItemData(FormName.lstSort.NewIndex, FormName.lstFields.ItemData(intCounter));
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadWhereGrid(dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int intCounter;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				FormName.vsWhere.AddItem(FormName.lstFields.List(intCounter));
				// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
				// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
				// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
				if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
				{
					FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
			if (Statics.strReportType == "DOGS" || Statics.strReportType == "DOGSLABELS")
			{
				// add deleted option
				FormName.vsWhere.RowHidden(24, true);
				FormName.vsWhere.AddItem("Deleted", 29);
				FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 29, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				FormName.vsWhere.TextMatrix(29, 1, "F");
				FormName.vsWhere.AddItem("Registered", 30);
				FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 30, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 31, 2, FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 31, 1));
				FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 33, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 34, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// FormName.lstFields.AddItem "Registered"
				// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 29
			}
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			// THE LAST COLUMN IS THERE TO HOLD ID VALUES IF THE TYPE IS OF
			// A COMBO BOX BECAUSE THE LIST DOES NOT STAY WITH THAT CELL ONCE
			// THE USER MOVES TO A NEW ONE.
			FormName.vsWhere.Cols = 4;
			FormName.vsWhere.ColWidth(0, 1900);
			FormName.vsWhere.ColWidth(1, 1600);
			FormName.vsWhere.ColWidth(2, 1600);
			FormName.vsWhere.ColWidth(3, 0);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadGridCellAsCombo(FCForm FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
				while (!rsCombo.EndOfFile())
				{
					Statics.strComboList[intRowNumber, 0] += "|";
					Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
					Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
					rsCombo.MoveNext();
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static object IsValidDate(string DateToCheck)
		{
			object IsValidDate = null;
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							MessageBox.Show("Invalid Month on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							MessageBox.Show("Invalid day on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							MessageBox.Show("Invalid day on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetBurialParameters(dynamic FormName)
		{
            //FC:FINAL:SBE - #4290 - In VB6 form is closed when user goes a level up in the menu. On web the menu was replaced with a TreeView navigation, and forms are not clsed when user navigates to another level
            FormName.lstFields.Clear();
            // WHAT IS THE CAPTION TO SHOW ON THE TOP OF THE REPORT
            Statics.strCustomTitle = "**** CUSTOM BURIAL REPORT ****";
			modGNBas.Statics.strPreSetReport = "Burial";
			// GIVE THE NAME TO SHOW IN THE LIST BOX
			FormName.lstFields.AddItem("First Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Middle Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Last Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Designation");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Sex");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("DateOfDeath");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Race");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Age");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Place Of Death City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Place Of Death State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("License Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Name Of Funeral Establishmente");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Business Address");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Type Of Permit");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("authorization for permit");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Place Of Disposition");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("City Or Town");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("Name Of Clerk Or Subregistrar");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("Date Of Disposition");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("DateSigned");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("Disposition");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			FormName.lstFields.AddItem("DispositionDate");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			FormName.lstFields.AddItem("Location");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 22);
			FormName.lstFields.AddItem("NameOfCemetery");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 23);
			FormName.lstFields.AddItem("Burial");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 24);
			FormName.lstFields.AddItem("Cremation");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 25);
			FormName.lstFields.AddItem("Location State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 26);
			FormName.lstFields.AddItem("Business Address 2");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 27);
			FormName.lstFields.AddItem("Business City");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 28);
			FormName.lstFields.AddItem("Business State");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 29);
			FormName.lstFields.AddItem("BusinessZip");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 30);
			FormName.lstFields.AddItem("TownCode");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 31);
			FormName.lstFields.AddItem("InArmedForces");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 32);
			FormName.lstFields.AddItem("ClerkTown");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 33);
			// THIS IS THE ACTUAL DATABASE FIELD NAME (NOTE THE ALIAS FOR #4)
			Statics.strFields[0] = "FirstName";
			Statics.strFields[1] = "MiddleName";
			Statics.strFields[2] = "LastName";
			Statics.strFields[3] = "Designation";
			Statics.strFields[4] = "Sex";
			Statics.strFields[5] = "DateOfDeath";
			Statics.strFields[6] = "Race";
			Statics.strFields[7] = "Age";
			Statics.strFields[8] = "PlaceOfDeathCity";
			Statics.strFields[9] = "PlaceOfDeathState";
			Statics.strFields[10] = "LicenseNumber";
			Statics.strFields[11] = "NameOfFuneralEstablishment";
			Statics.strFields[12] = "BusinessAddress";
			Statics.strFields[13] = "TypeOfPermit";
			Statics.strFields[14] = "authorizationforpermit";
			Statics.strFields[15] = "PlaceOfDisposition";
			Statics.strFields[16] = "CityOrTown";
			Statics.strFields[17] = "NameOfClerkOrSubregistrar";
			Statics.strFields[18] = "DateOfDisposition";
			Statics.strFields[19] = "DateSigned";
			Statics.strFields[20] = "Disposition";
			Statics.strFields[21] = "DispositionDate";
			Statics.strFields[22] = "Location";
			Statics.strFields[23] = "NameOfCemetery";
			Statics.strFields[24] = "Burial";
			Statics.strFields[25] = "Cremation";
			Statics.strFields[26] = "LocationState";
			Statics.strFields[27] = "BusinessAddress2";
			Statics.strFields[28] = "BusinessCity";
			Statics.strFields[29] = "BusinessState";
			Statics.strFields[30] = "BusinessZip";
			Statics.strFields[31] = "TownCode";
			Statics.strFields[32] = "InArmedForces";
			Statics.strFields[33] = "ClerkTown";
			// THESE ARE THE CAPTIONS THAT YOU WANT THE USER TO SEE ON
			// THE REPORT IF THIS FIELD IS SELECTED.
			// NOTE:
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY. IF NOT THEN THE CAPTIONS
			// WILL NOT DISPLAY CORRECTLY ON THE REPORT
			Statics.strCaptions[0] = "FirstName";
			Statics.strCaptions[1] = "MiddleName";
			Statics.strCaptions[2] = "LastName";
			Statics.strCaptions[3] = "Designation";
			Statics.strCaptions[4] = "Sex";
			Statics.strCaptions[5] = "DateOfDeath";
			Statics.strCaptions[6] = "Race";
			Statics.strCaptions[7] = "Age";
			Statics.strCaptions[8] = "PlaceOfDeathCity";
			Statics.strCaptions[9] = "PlaceOfDeathState";
			Statics.strCaptions[10] = "LicenseNumber";
			Statics.strCaptions[11] = "NameOfFuneralEstablishment";
			Statics.strCaptions[12] = "BusinessAddress";
			Statics.strCaptions[13] = "TypeOfPermit";
			Statics.strCaptions[14] = "authorizationforpermit";
			Statics.strCaptions[15] = "PlaceOfDisposition";
			Statics.strCaptions[16] = "CityOrTown";
			Statics.strCaptions[17] = "NameOfClerkOrSubregistrar";
			Statics.strCaptions[18] = "DateOfDisposition";
			Statics.strCaptions[19] = "DateSigned";
			Statics.strCaptions[20] = "Disposition";
			Statics.strCaptions[21] = "DispositionDate";
			Statics.strCaptions[22] = "Location";
			Statics.strCaptions[23] = "NameOfCemetery";
			Statics.strCaptions[24] = "Burial";
			Statics.strCaptions[25] = "Cremation";
			Statics.strCaptions[26] = "LocationState";
			Statics.strCaptions[27] = "BusinessAddress2";
			Statics.strCaptions[28] = "BusinessCity";
			Statics.strCaptions[29] = "BusinessState";
			Statics.strCaptions[30] = "BusinessZip";
			Statics.strCaptions[31] = "TownCode";
			Statics.strCaptions[32] = "InArmedForces";
			Statics.strCaptions[33] = "ClerkTown";
			// need to indicate what table each field belongs to
			Statics.strTableNames[0] = "BurialPermitMaster";
			Statics.strTableNames[1] = "BurialPermitMaster";
			Statics.strTableNames[2] = "BurialPermitMaster";
			Statics.strTableNames[3] = "BurialPermitMaster";
			Statics.strTableNames[4] = "BurialPermitMaster";
			Statics.strTableNames[5] = "BurialPermitMaster";
			Statics.strTableNames[6] = "BurialPermitMaster";
			Statics.strTableNames[7] = "BurialPermitMaster";
			Statics.strTableNames[8] = "BurialPermitMaster";
			Statics.strTableNames[9] = "BurialPermitMaster";
			Statics.strTableNames[10] = "BurialPermitMaster";
			Statics.strTableNames[11] = "BurialPermitMaster";
			Statics.strTableNames[12] = "BurialPermitMaster";
			Statics.strTableNames[13] = "BurialPermitMaster";
			Statics.strTableNames[14] = "BurialPermitMaster";
			Statics.strTableNames[15] = "BurialPermitMaster";
			Statics.strTableNames[16] = "BurialPermitMaster";
			Statics.strTableNames[17] = "BurialPermitMaster";
			Statics.strTableNames[18] = "BurialPermitMaster";
			Statics.strTableNames[19] = "BurialPermitMaster";
			Statics.strTableNames[20] = "BurialPermitMaster";
			Statics.strTableNames[21] = "BurialPermitMaster";
			Statics.strTableNames[22] = "BurialPermitMaster";
			Statics.strTableNames[23] = "BurialPermitMaster";
			Statics.strTableNames[24] = "BurialPermitMaster";
			Statics.strTableNames[25] = "BurialPermitMaster";
			Statics.strTableNames[26] = "BurialPermitMaster";
			Statics.strTableNames[27] = "BurialPermitMaster";
			Statics.strTableNames[28] = "BurialPermitMaster";
			Statics.strTableNames[29] = "BurialPermitMaster";
			Statics.strTableNames[30] = "BurialPermitMaster";
			Statics.strTableNames[31] = "BurialPermitMaster";
			Statics.strTableNames[32] = "BurialPermitMaster";
			Statics.strTableNames[33] = "BurialPermitMaster";
			// MARK WHAT TYPE THIS FIELD IS SO THAT THE WHERE GRID
			// WILL KNOW WHAT TYPE OF DATA TO DISPLAY.
			// 
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY.
			Statics.strWhereType[0] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[19] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[20] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[21] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[22] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[23] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[24] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[25] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[26] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[27] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[28] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[29] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[30] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[31] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[32] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[33] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[34] = FCConvert.ToString(GRIDTEXT);
			// IF ONE OF THE FIELDS IS OF TYPE GRIDCOMBOIDTEXT THEN YOU NEED
			// TO CREATE A LIST TO SHOW WHEN THE USER CLICKS ON THAT CELL
			// IN THE GRID
			// Call LoadGridCellAsCombo(FormName, 4, "#0;M" & vbTab & "Male|#1;F" & vbTab & "Female")
			// FINISH THE MID SECTION OF THE SQL STATEMENT
			FormName.fraFields.Tag = "from BurialPermitMaster ";
		}

		public class StaticVariables
		{
			// vbPorter upgrade warning: intNumberOfSQLFields As int	OnWriteFCConvert.ToInt32(
			public int intNumberOfSQLFields;
			public int[] strFieldWidth = new int[50 + 1];
			public string strCustomSQL = "";
			public string strColumnCaptions = "";
			public string[] strFields = new string[50 + 1];
			public string[] strTableNames = new string[50 + 1];
			public string[] strFieldNames = new string[50 + 1];
			public string[] strFieldCaptions = new string[50 + 1];
			public string[] strCaptions = new string[50 + 1];
			public string strCustomTitle = string.Empty;
			public string strReportType = "";
			public string[,] strComboList = new string[50 + 1, 50 + 1];
			// vbPorter upgrade warning: strWhereType As string	OnWrite
			public string[] strWhereType = new string[60 + 1];
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
