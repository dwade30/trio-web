﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMarriageConsentWordDoc.
	/// </summary>
	partial class rptMarriageConsentWordDoc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMarriageConsentWordDoc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtIncarcerated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroom1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBride1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomClerk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWaiver = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBride2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroom2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBrideTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBrideClerk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBrideDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.shpBrideI = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.shpGroomI = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.shpBrideW = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.shpGroomW = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtHospitalized = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.shpBrideHospital = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.shpGroomHospital = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			((System.ComponentModel.ISupportInitialize)(this.txtIncarcerated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroom1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBride1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomClerk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWaiver)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBride2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroom2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideClerk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHospitalized)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.Name = "Detail";
			this.Detail.Visible = false;
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtIncarcerated,
				this.txtGroom1,
				this.txtBride1,
				this.txtGroomTown,
				this.txtGroomClerk,
				this.txtGroomDate,
				this.txtWaiver,
				this.txtBride2,
				this.txtGroom2,
				this.txtBrideTown,
				this.txtBrideClerk,
				this.txtBrideDate,
				this.shpBrideI,
				this.shpGroomI,
				this.shpBrideW,
				this.shpGroomW,
				this.txtHospitalized,
				this.shpBrideHospital,
				this.shpGroomHospital
			});
			this.PageHeader.Height = 8.979167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtIncarcerated
			// 
			this.txtIncarcerated.Height = 0.1875F;
			this.txtIncarcerated.Left = 0.4375F;
			this.txtIncarcerated.Name = "txtIncarcerated";
			this.txtIncarcerated.Style = "text-align: center";
			this.txtIncarcerated.Text = "X";
			this.txtIncarcerated.Top = 7.604167F;
			this.txtIncarcerated.Width = 0.25F;
			// 
			// txtGroom1
			// 
			this.txtGroom1.Height = 0.1875F;
			this.txtGroom1.Left = 0.5F;
			this.txtGroom1.Name = "txtGroom1";
			this.txtGroom1.Text = null;
			this.txtGroom1.Top = 3.604167F;
			this.txtGroom1.Width = 3.0625F;
			// 
			// txtBride1
			// 
			this.txtBride1.Height = 0.1875F;
			this.txtBride1.Left = 4.3125F;
			this.txtBride1.Name = "txtBride1";
			this.txtBride1.Text = null;
			this.txtBride1.Top = 3.604167F;
			this.txtBride1.Width = 3F;
			// 
			// txtGroomTown
			// 
			this.txtGroomTown.Height = 0.1875F;
			this.txtGroomTown.Left = 2.6875F;
			this.txtGroomTown.Name = "txtGroomTown";
			this.txtGroomTown.Text = null;
			this.txtGroomTown.Top = 4.840278F;
			this.txtGroomTown.Width = 1.3125F;
			// 
			// txtGroomClerk
			// 
			this.txtGroomClerk.Height = 0.1875F;
			this.txtGroomClerk.Left = 0.5F;
			this.txtGroomClerk.Name = "txtGroomClerk";
			this.txtGroomClerk.Text = null;
			this.txtGroomClerk.Top = 4.840278F;
			this.txtGroomClerk.Width = 2.0625F;
			// 
			// txtGroomDate
			// 
			this.txtGroomDate.Height = 0.1875F;
			this.txtGroomDate.Left = 2.6875F;
			this.txtGroomDate.Name = "txtGroomDate";
			this.txtGroomDate.Text = null;
			this.txtGroomDate.Top = 4.520833F;
			this.txtGroomDate.Width = 1.322917F;
			// 
			// txtWaiver
			// 
			this.txtWaiver.Height = 0.1875F;
			this.txtWaiver.Left = 0.4375F;
			this.txtWaiver.Name = "txtWaiver";
			this.txtWaiver.Style = "text-align: center";
			this.txtWaiver.Text = "X";
			this.txtWaiver.Top = 8.666667F;
			this.txtWaiver.Width = 0.25F;
			// 
			// txtBride2
			// 
			this.txtBride2.Height = 0.1875F;
			this.txtBride2.Left = 0.5F;
			this.txtBride2.Name = "txtBride2";
			this.txtBride2.Text = null;
			this.txtBride2.Top = 5.486111F;
			this.txtBride2.Width = 3.0625F;
			// 
			// txtGroom2
			// 
			this.txtGroom2.Height = 0.1875F;
			this.txtGroom2.Left = 4.3125F;
			this.txtGroom2.Name = "txtGroom2";
			this.txtGroom2.Text = null;
			this.txtGroom2.Top = 5.486111F;
			this.txtGroom2.Width = 2.375F;
			// 
			// txtBrideTown
			// 
			this.txtBrideTown.Height = 0.1875F;
			this.txtBrideTown.Left = 2.75F;
			this.txtBrideTown.Name = "txtBrideTown";
			this.txtBrideTown.Text = null;
			this.txtBrideTown.Top = 6.770833F;
			this.txtBrideTown.Width = 1.260417F;
			// 
			// txtBrideClerk
			// 
			this.txtBrideClerk.Height = 0.1875F;
			this.txtBrideClerk.Left = 0.4375F;
			this.txtBrideClerk.Name = "txtBrideClerk";
			this.txtBrideClerk.Text = null;
			this.txtBrideClerk.Top = 6.770833F;
			this.txtBrideClerk.Width = 2.1875F;
			// 
			// txtBrideDate
			// 
			this.txtBrideDate.Height = 0.1875F;
			this.txtBrideDate.Left = 2.739583F;
			this.txtBrideDate.Name = "txtBrideDate";
			this.txtBrideDate.Text = null;
			this.txtBrideDate.Top = 6.40625F;
			this.txtBrideDate.Width = 1.239583F;
			// 
			// shpBrideI
			// 
			this.shpBrideI.Height = 0.3125F;
			this.shpBrideI.Left = 0.6875F;
			this.shpBrideI.Name = "shpBrideI";
			this.shpBrideI.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shpBrideI.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Ellipse;
			this.shpBrideI.Top = 7.53125F;
			this.shpBrideI.Visible = false;
			this.shpBrideI.Width = 0.59375F;
			// 
			// shpGroomI
			// 
			this.shpGroomI.Height = 0.3229167F;
			this.shpGroomI.Left = 1.21875F;
			this.shpGroomI.Name = "shpGroomI";
			this.shpGroomI.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shpGroomI.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Ellipse;
			this.shpGroomI.Top = 7.520833F;
			this.shpGroomI.Visible = false;
			this.shpGroomI.Width = 0.6145833F;
			// 
			// shpBrideW
			// 
			this.shpBrideW.Height = 0.3020833F;
			this.shpBrideW.Left = 0.6875F;
			this.shpBrideW.Name = "shpBrideW";
			this.shpBrideW.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shpBrideW.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Ellipse;
			this.shpBrideW.Top = 8.604167F;
			this.shpBrideW.Visible = false;
			this.shpBrideW.Width = 0.59375F;
			// 
			// shpGroomW
			// 
			this.shpGroomW.Height = 0.3125F;
			this.shpGroomW.Left = 1.25F;
			this.shpGroomW.Name = "shpGroomW";
			this.shpGroomW.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shpGroomW.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Ellipse;
			this.shpGroomW.Top = 8.59375F;
			this.shpGroomW.Visible = false;
			this.shpGroomW.Width = 0.59375F;
			// 
			// txtHospitalized
			// 
			this.txtHospitalized.Height = 0.1875F;
			this.txtHospitalized.Left = 0.4375F;
			this.txtHospitalized.Name = "txtHospitalized";
			this.txtHospitalized.Style = "text-align: center";
			this.txtHospitalized.Text = "X";
			this.txtHospitalized.Top = 8.041667F;
			this.txtHospitalized.Visible = false;
			this.txtHospitalized.Width = 0.25F;
			// 
			// shpBrideHospital
			// 
			this.shpBrideHospital.Height = 0.3333333F;
			this.shpBrideHospital.Left = 0.6875F;
			this.shpBrideHospital.Name = "shpBrideHospital";
			this.shpBrideHospital.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shpBrideHospital.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Ellipse;
			this.shpBrideHospital.Top = 7.947917F;
			this.shpBrideHospital.Visible = false;
			this.shpBrideHospital.Width = 0.59375F;
			// 
			// shpGroomHospital
			// 
			this.shpGroomHospital.Height = 0.3333333F;
			this.shpGroomHospital.Left = 1.25F;
			this.shpGroomHospital.Name = "shpGroomHospital";
			this.shpGroomHospital.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.shpGroomHospital.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Ellipse;
			this.shpGroomHospital.Top = 7.947917F;
			this.shpGroomHospital.Visible = false;
			this.shpGroomHospital.Width = 0.59375F;
			// 
			// rptMarriageConsentWordDoc
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtIncarcerated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroom1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBride1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomClerk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWaiver)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBride2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroom2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideClerk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHospitalized)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIncarcerated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroom1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBride1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomClerk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWaiver;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBride2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroom2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideClerk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideDate;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shpBrideI;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shpGroomI;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shpBrideW;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shpGroomW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHospitalized;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shpBrideHospital;
		private GrapeCity.ActiveReports.SectionReportModel.Shape shpGroomHospital;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
