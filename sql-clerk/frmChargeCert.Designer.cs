//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmChargeCert.
	/// </summary>
	partial class frmChargeCert
	{
		public fecherFoundation.FCTextBox txtSecond;
		public fecherFoundation.FCTextBox txtSubsequent;
		public fecherFoundation.FCTextBox txtFirst;
		public fecherFoundation.FCLabel lblSecondCopy;
		public fecherFoundation.FCLabel lblSecond;
		public fecherFoundation.FCLabel lblSubsequent;
		public fecherFoundation.FCLabel lblSecondDesc;
		public fecherFoundation.FCLabel lblFirst;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtSecond = new fecherFoundation.FCTextBox();
			this.txtSubsequent = new fecherFoundation.FCTextBox();
			this.txtFirst = new fecherFoundation.FCTextBox();
			this.lblSecondCopy = new fecherFoundation.FCLabel();
			this.lblSecond = new fecherFoundation.FCLabel();
			this.lblSubsequent = new fecherFoundation.FCLabel();
			this.lblSecondDesc = new fecherFoundation.FCLabel();
			this.lblFirst = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 350);
			this.BottomPanel.Size = new System.Drawing.Size(311, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Controls.Add(this.txtSecond);
			this.ClientArea.Controls.Add(this.txtSubsequent);
			this.ClientArea.Controls.Add(this.txtFirst);
			this.ClientArea.Controls.Add(this.lblSecondCopy);
			this.ClientArea.Controls.Add(this.lblSecond);
			this.ClientArea.Controls.Add(this.lblSubsequent);
			this.ClientArea.Controls.Add(this.lblSecondDesc);
			this.ClientArea.Controls.Add(this.lblFirst);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(311, 290);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(311, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(271, 30);
			this.HeaderText.Text = "Transaction Information";
			// 
			// txtSecond
			// 
			this.txtSecond.BackColor = System.Drawing.SystemColors.Window;
			this.txtSecond.Location = new System.Drawing.Point(30, 159);
			this.txtSecond.Name = "txtSecond";
			this.txtSecond.Size = new System.Drawing.Size(41, 40);
			this.txtSecond.TabIndex = 6;
			this.txtSecond.Text = "0";
			this.txtSecond.Visible = false;
			// 
			// txtSubsequent
			// 
			this.txtSubsequent.BackColor = System.Drawing.SystemColors.Window;
			this.txtSubsequent.Location = new System.Drawing.Point(150, 72);
			this.txtSubsequent.Name = "txtSubsequent";
			this.txtSubsequent.Size = new System.Drawing.Size(41, 40);
			this.txtSubsequent.TabIndex = 3;
			this.txtSubsequent.Text = "0";
			// 
			// txtFirst
			// 
			this.txtFirst.BackColor = System.Drawing.SystemColors.Window;
			this.txtFirst.Location = new System.Drawing.Point(30, 72);
			this.txtFirst.Name = "txtFirst";
			this.txtFirst.Size = new System.Drawing.Size(41, 40);
			this.txtFirst.TabIndex = 7;
			this.txtFirst.Text = "0";
			// 
			// lblSecondCopy
			// 
			this.lblSecondCopy.Location = new System.Drawing.Point(30, 131);
			this.lblSecondCopy.Name = "lblSecondCopy";
			this.lblSecondCopy.Size = new System.Drawing.Size(80, 21);
			this.lblSecondCopy.TabIndex = 8;
			this.lblSecondCopy.Text = "2ND COPY";
			this.lblSecondCopy.Visible = false;
			// 
			// lblSecond
			// 
			this.lblSecond.Location = new System.Drawing.Point(91, 173);
			this.lblSecond.Name = "lblSecond";
			this.lblSecond.Size = new System.Drawing.Size(62, 16);
			this.lblSecond.TabIndex = 7;
			this.lblSecond.Text = "X  0.00";
			this.lblSecond.Visible = false;
			// 
			// lblSubsequent
			// 
			this.lblSubsequent.Location = new System.Drawing.Point(212, 86);
			this.lblSubsequent.Name = "lblSubsequent";
			this.lblSubsequent.Size = new System.Drawing.Size(66, 16);
			this.lblSubsequent.TabIndex = 5;
			this.lblSubsequent.Text = "X  0.00";
			// 
			// lblSecondDesc
			// 
			this.lblSecondDesc.Location = new System.Drawing.Point(150, 30);
			this.lblSecondDesc.Name = "lblSecondDesc";
			this.lblSecondDesc.Size = new System.Drawing.Size(138, 45);
			this.lblSecondDesc.TabIndex = 4;
			this.lblSecondDesc.Text = "SUBSEQUENT COPIES (SAME DAY)";
			this.lblSecondDesc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblFirst
			// 
			this.lblFirst.Location = new System.Drawing.Point(91, 86);
			this.lblFirst.Name = "lblFirst";
			this.lblFirst.Size = new System.Drawing.Size(60, 16);
			this.lblFirst.TabIndex = 2;
			this.lblFirst.Text = "X  0.00";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(74, 33);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "FIRST COPY";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(76, 222);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(159, 48);
			this.cmdProcess.TabIndex = 18;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// frmChargeCert
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(311, 350);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmChargeCert";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Transaction Information";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmChargeCert_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdProcess;
	}
}