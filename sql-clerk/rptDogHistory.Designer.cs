﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogHistory.
	/// </summary>
	partial class rptDogHistory
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDogHistory));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtTranDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDogYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRabiesTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtClerk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtDogName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBreed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSex = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtColor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVeterinarian = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSSR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHearing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHybrid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNuisanceDangerous = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblRescue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHearing = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblWolf = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNuisanceDangerous = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblUserFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtNeuter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblNeuter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtTranDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRabiesTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClerk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBreed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVeterinarian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHybrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceDangerous)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRescue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHearing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWolf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNuisanceDangerous)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUserFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNeuter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTranDate,
            this.txtDogYear,
            this.txtTag,
            this.txtRabiesTag,
            this.txtAmount,
            this.txtDescription,
            this.txtState,
            this.txtTown,
            this.txtClerk,
            this.txtLate,
            this.txtUser});
            this.Detail.Height = 0.21875F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtTranDate
            // 
            this.txtTranDate.Height = 0.2083333F;
            this.txtTranDate.Left = 0F;
            this.txtTranDate.Name = "txtTranDate";
            this.txtTranDate.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtTranDate.Text = "Field2";
            this.txtTranDate.Top = 0F;
            this.txtTranDate.Width = 0.6875F;
            // 
            // txtDogYear
            // 
            this.txtDogYear.Height = 0.2083333F;
            this.txtDogYear.Left = 0.75F;
            this.txtDogYear.Name = "txtDogYear";
            this.txtDogYear.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtDogYear.Text = "Field3";
            this.txtDogYear.Top = 0F;
            this.txtDogYear.Width = 0.46875F;
            // 
            // txtTag
            // 
            this.txtTag.Height = 0.2083333F;
            this.txtTag.Left = 1.25F;
            this.txtTag.Name = "txtTag";
            this.txtTag.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtTag.Text = "Field4";
            this.txtTag.Top = 0F;
            this.txtTag.Width = 0.71875F;
            // 
            // txtRabiesTag
            // 
            this.txtRabiesTag.Height = 0.2083333F;
            this.txtRabiesTag.Left = 2F;
            this.txtRabiesTag.Name = "txtRabiesTag";
            this.txtRabiesTag.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtRabiesTag.Text = "Field5";
            this.txtRabiesTag.Top = 0F;
            this.txtRabiesTag.Width = 0.90625F;
            // 
            // txtAmount
            // 
            this.txtAmount.Height = 0.2083333F;
            this.txtAmount.Left = 2.947917F;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
            this.txtAmount.Text = "Field6";
            this.txtAmount.Top = 0F;
            this.txtAmount.Width = 0.6875F;
            // 
            // txtDescription
            // 
            this.txtDescription.Height = 0.2083333F;
            this.txtDescription.Left = 3.65625F;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
            this.txtDescription.Text = "Field7";
            this.txtDescription.Top = 0F;
            this.txtDescription.Width = 1.145833F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.2083333F;
            this.txtState.Left = 4.84375F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
            this.txtState.Text = "Field8";
            this.txtState.Top = 0F;
            this.txtState.Width = 0.4375F;
            // 
            // txtTown
            // 
            this.txtTown.Height = 0.2083333F;
            this.txtTown.Left = 5.34375F;
            this.txtTown.Name = "txtTown";
            this.txtTown.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
            this.txtTown.Text = "Field9";
            this.txtTown.Top = 0F;
            this.txtTown.Width = 0.4375F;
            // 
            // txtClerk
            // 
            this.txtClerk.Height = 0.2083333F;
            this.txtClerk.Left = 5.84375F;
            this.txtClerk.Name = "txtClerk";
            this.txtClerk.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
            this.txtClerk.Text = "Field10";
            this.txtClerk.Top = 0F;
            this.txtClerk.Width = 0.4375F;
            // 
            // txtLate
            // 
            this.txtLate.Height = 0.2083333F;
            this.txtLate.Left = 6.34375F;
            this.txtLate.Name = "txtLate";
            this.txtLate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
            this.txtLate.Text = "Field11";
            this.txtLate.Top = 0F;
            this.txtLate.Width = 0.4375F;
            // 
            // txtUser
            // 
            this.txtUser.Height = 0.2083333F;
            this.txtUser.Left = 6.84375F;
            this.txtUser.Name = "txtUser";
            this.txtUser.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
            this.txtUser.Text = "Field12";
            this.txtUser.Top = 0F;
            this.txtUser.Width = 0.59375F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDate,
            this.txtPage,
            this.txtMuni,
            this.txtTime,
            this.Field1});
            this.PageHeader.Height = 0.6041667F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1666667F;
            this.txtDate.Left = 6.25F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "text-align: right";
            this.txtDate.Text = "Field1";
            this.txtDate.Top = 0.08333334F;
            this.txtDate.Width = 1.25F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1666667F;
            this.txtPage.Left = 6.25F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "text-align: right";
            this.txtPage.Text = "Field2";
            this.txtPage.Top = 0.25F;
            this.txtPage.Width = 1.25F;
            // 
            // txtMuni
            // 
            this.txtMuni.Height = 0.1666667F;
            this.txtMuni.Left = 0.08333334F;
            this.txtMuni.Name = "txtMuni";
            this.txtMuni.Text = "Field1";
            this.txtMuni.Top = 0.08333334F;
            this.txtMuni.Width = 2.5F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1666667F;
            this.txtTime.Left = 0.08333334F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Text = "Field1";
            this.txtTime.Top = 0.25F;
            this.txtTime.Width = 2F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.25F;
            this.Field1.Left = 2.5F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.Field1.Text = "Dog History";
            this.Field1.Top = 0.08333334F;
            this.Field1.Width = 2.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDogName,
            this.txtBreed,
            this.txtDOB,
            this.txtSex,
            this.txtColor,
            this.txtVeterinarian,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.txtOwnerName,
            this.Label1,
            this.txtSSR,
            this.txtHearing,
            this.txtHybrid,
            this.txtNuisanceDangerous,
            this.lblRescue,
            this.lblHearing,
            this.lblWolf,
            this.lblNuisanceDangerous,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label18,
            this.Label19,
            this.lblUserFee,
            this.txtNeuter,
            this.lblNeuter});
            this.GroupHeader1.DataField = "grpHeader";
            this.GroupHeader1.Height = 1.416667F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // txtDogName
            // 
            this.txtDogName.Height = 0.1875F;
            this.txtDogName.Left = 1.125F;
            this.txtDogName.Name = "txtDogName";
            this.txtDogName.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtDogName.Text = null;
            this.txtDogName.Top = 0.25F;
            this.txtDogName.Width = 1.5F;
            // 
            // txtBreed
            // 
            this.txtBreed.Height = 0.1875F;
            this.txtBreed.Left = 1.125F;
            this.txtBreed.Name = "txtBreed";
            this.txtBreed.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtBreed.Text = null;
            this.txtBreed.Top = 0.625F;
            this.txtBreed.Width = 3.083333F;
            // 
            // txtDOB
            // 
            this.txtDOB.Height = 0.1875F;
            this.txtDOB.Left = 3.3125F;
            this.txtDOB.Name = "txtDOB";
            this.txtDOB.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtDOB.Text = null;
            this.txtDOB.Top = 0.25F;
            this.txtDOB.Width = 1F;
            // 
            // txtSex
            // 
            this.txtSex.Height = 0.1875F;
            this.txtSex.Left = 4.6875F;
            this.txtSex.Name = "txtSex";
            this.txtSex.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtSex.Text = null;
            this.txtSex.Top = 0.25F;
            this.txtSex.Width = 0.25F;
            // 
            // txtColor
            // 
            this.txtColor.Height = 0.1875F;
            this.txtColor.Left = 1.125F;
            this.txtColor.Name = "txtColor";
            this.txtColor.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtColor.Text = null;
            this.txtColor.Top = 0.4375F;
            this.txtColor.Width = 3.052083F;
            // 
            // txtVeterinarian
            // 
            this.txtVeterinarian.Height = 0.1875F;
            this.txtVeterinarian.Left = 1.125F;
            this.txtVeterinarian.Name = "txtVeterinarian";
            this.txtVeterinarian.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtVeterinarian.Text = null;
            this.txtVeterinarian.Top = 0.7916667F;
            this.txtVeterinarian.Width = 3.104167F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label7.Text = "Dog Name";
            this.Label7.Top = 0.25F;
            this.Label7.Width = 0.6875F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label8.Text = "Color";
            this.Label8.Top = 0.4375F;
            this.Label8.Width = 0.4583333F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 3F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label9.Text = "DOB";
            this.Label9.Top = 0.25F;
            this.Label9.Width = 0.3125F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 4.375F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label10.Text = "Sex";
            this.Label10.Top = 0.25F;
            this.Label10.Width = 0.3125F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1875F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 0F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label11.Text = "Breed";
            this.Label11.Top = 0.625F;
            this.Label11.Width = 0.4375F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1875F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label12.Text = "Veterinarian";
            this.Label12.Top = 0.7916667F;
            this.Label12.Width = 0.8125F;
            // 
            // txtOwnerName
            // 
            this.txtOwnerName.Height = 0.1875F;
            this.txtOwnerName.Left = 1.125F;
            this.txtOwnerName.MultiLine = false;
            this.txtOwnerName.Name = "txtOwnerName";
            this.txtOwnerName.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtOwnerName.Text = null;
            this.txtOwnerName.Top = 0.0625F;
            this.txtOwnerName.Width = 3.3125F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label1.Text = "Owner/Keeper";
            this.Label1.Top = 0.0625F;
            this.Label1.Width = 1F;
            // 
            // txtSSR
            // 
            this.txtSSR.Height = 0.1875F;
            this.txtSSR.Left = 6.875F;
            this.txtSSR.Name = "txtSSR";
            this.txtSSR.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtSSR.Text = "No";
            this.txtSSR.Top = 0.25F;
            this.txtSSR.Width = 0.4375F;
            // 
            // txtHearing
            // 
            this.txtHearing.Height = 0.1875F;
            this.txtHearing.Left = 6.875F;
            this.txtHearing.Name = "txtHearing";
            this.txtHearing.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtHearing.Text = "No";
            this.txtHearing.Top = 0.4375F;
            this.txtHearing.Width = 0.4375F;
            // 
            // txtHybrid
            // 
            this.txtHybrid.Height = 0.1875F;
            this.txtHybrid.Left = 6.875F;
            this.txtHybrid.Name = "txtHybrid";
            this.txtHybrid.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtHybrid.Text = "No";
            this.txtHybrid.Top = 0.625F;
            this.txtHybrid.Width = 0.4375F;
            // 
            // txtNuisanceDangerous
            // 
            this.txtNuisanceDangerous.Height = 0.1875F;
            this.txtNuisanceDangerous.Left = 6.875F;
            this.txtNuisanceDangerous.Name = "txtNuisanceDangerous";
            this.txtNuisanceDangerous.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtNuisanceDangerous.Text = "No";
            this.txtNuisanceDangerous.Top = 0.7916667F;
            this.txtNuisanceDangerous.Width = 0.4375F;
            // 
            // lblRescue
            // 
            this.lblRescue.Height = 0.1875F;
            this.lblRescue.HyperLink = null;
            this.lblRescue.Left = 5.041667F;
            this.lblRescue.Name = "lblRescue";
            this.lblRescue.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
            this.lblRescue.Text = "Service/Search & Rescue";
            this.lblRescue.Top = 0.25F;
            this.lblRescue.Width = 1.770333F;
            // 
            // lblHearing
            // 
            this.lblHearing.Height = 0.1875F;
            this.lblHearing.HyperLink = null;
            this.lblHearing.Left = 5.041667F;
            this.lblHearing.Name = "lblHearing";
            this.lblHearing.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
            this.lblHearing.Text = "Hearing/Guide";
            this.lblHearing.Top = 0.4375F;
            this.lblHearing.Width = 1.75F;
            // 
            // lblWolf
            // 
            this.lblWolf.Height = 0.1875F;
            this.lblWolf.HyperLink = null;
            this.lblWolf.Left = 5.041667F;
            this.lblWolf.Name = "lblWolf";
            this.lblWolf.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
            this.lblWolf.Text = "Wolf/Hybrid";
            this.lblWolf.Top = 0.625F;
            this.lblWolf.Width = 1.28125F;
            // 
            // lblNuisanceDangerous
            // 
            this.lblNuisanceDangerous.Height = 0.1875F;
            this.lblNuisanceDangerous.HyperLink = null;
            this.lblNuisanceDangerous.Left = 5.041667F;
            this.lblNuisanceDangerous.Name = "lblNuisanceDangerous";
            this.lblNuisanceDangerous.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
            this.lblNuisanceDangerous.Text = "Nuisance";
            this.lblNuisanceDangerous.Top = 0.7916667F;
            this.lblNuisanceDangerous.Width = 1.166667F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.1875F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 0F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label13.Text = "Date";
            this.Label13.Top = 1.1875F;
            this.Label13.Width = 0.6875F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1875F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.75F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label14.Text = "Year";
            this.Label14.Top = 1.1875F;
            this.Label14.Width = 0.4375F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1875F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 1.25F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label15.Text = "Tag";
            this.Label15.Top = 1.1875F;
            this.Label15.Width = 0.6875F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 2F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label16.Text = "Rabies Tag";
            this.Label16.Top = 1.1875F;
            this.Label16.Width = 0.8020833F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 2.947917F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
            this.Label17.Text = "Amount";
            this.Label17.Top = 1.1875F;
            this.Label17.Width = 0.6875F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 4.875F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
            this.Label4.Text = "State";
            this.Label4.Top = 1.1875F;
            this.Label4.Width = 0.40625F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1875F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 5.375F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
            this.Label5.Text = "Town";
            this.Label5.Top = 1.1875F;
            this.Label5.Width = 0.40625F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 5.875F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
            this.Label6.Text = "Clerk";
            this.Label6.Top = 1.1875F;
            this.Label6.Width = 0.40625F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 6.375F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
            this.Label18.Text = "Late";
            this.Label18.Top = 1.1875F;
            this.Label18.Width = 0.40625F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 3.65625F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.Label19.Text = "Description";
            this.Label19.Top = 1.1875F;
            this.Label19.Width = 1.03125F;
            // 
            // lblUserFee
            // 
            this.lblUserFee.CanShrink = true;
            this.lblUserFee.Height = 0.1875F;
            this.lblUserFee.Left = 6.8125F;
            this.lblUserFee.Name = "lblUserFee";
            this.lblUserFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
            this.lblUserFee.Text = "User Fee";
            this.lblUserFee.Top = 1.1875F;
            this.lblUserFee.Width = 0.625F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // txtNeuter
            // 
            this.txtNeuter.Height = 0.1875F;
            this.txtNeuter.Left = 6.875366F;
            this.txtNeuter.Name = "txtNeuter";
            this.txtNeuter.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtNeuter.Text = "No";
            this.txtNeuter.Top = 0.062F;
            this.txtNeuter.Width = 0.4375F;
            // 
            // lblNeuter
            // 
            this.lblNeuter.Height = 0.1875F;
            this.lblNeuter.HyperLink = null;
            this.lblNeuter.Left = 5.042F;
            this.lblNeuter.Name = "lblNeuter";
            this.lblNeuter.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
            this.lblNeuter.Text = "Neuter/Spay";
            this.lblNeuter.Top = 0.062F;
            this.lblNeuter.Width = 1.166667F;
            // 
            // rptDogHistory
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtTranDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRabiesTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClerk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBreed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVeterinarian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHybrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceDangerous)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRescue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHearing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWolf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNuisanceDangerous)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUserFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNeuter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTranDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTag;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRabiesTag;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClerk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUser;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBreed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSex;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtColor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVeterinarian;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHybrid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNuisanceDangerous;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRescue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHearing;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWolf;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNuisanceDangerous;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblUserFee;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNeuter;
    }
}
