//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.CentralDocuments;
using System;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
    public class modGlobalRoutines
    {
        public static void EndApplication()
        {
            Application.Exit();
        }

        public static void UnloadAllForms()
        {
            // unload all forms
            foreach (FCForm frm in FCGlobal.Statics.Forms)
            {
                frm.Close();
            }
            // frm
        }

        public static Keys CheckForNumericKeyPress(Keys KeyAscii)
        {
            Keys CheckForNumericKeyPress = Keys.None;
            // let the backspace key go through
            if (KeyAscii == (Keys)8)
            {
                CheckForNumericKeyPress = KeyAscii;
                return CheckForNumericKeyPress;
            }
            if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
            {
                CheckForNumericKeyPress = 0;
            }
            else
            {
                CheckForNumericKeyPress = KeyAscii;
            }
            return CheckForNumericKeyPress;
        }
       
        public static object CheckForNA(string strValue)
        {
            object CheckForNA = null;
            if (Strings.Trim(strValue) == "N/A")
            {
                CheckForNA = "";
            }
            else
            {
                CheckForNA = Strings.Trim(strValue);
            }
            return CheckForNA;
        }
        // vbPorter upgrade warning: 'Return' As object	OnWrite(string)
        public static object CheckForNAReturnComma(string strValue)
        {
            object CheckForNAReturnComma = null;
            if (Strings.Trim(strValue) == "N/A")
            {
                CheckForNAReturnComma = "";
            }
            else
            {
                CheckForNAReturnComma = ", ";
            }
            return CheckForNAReturnComma;
        }
      
        public static object GetKennelLicenseNumber(int intValue)
        {
            object GetKennelLicenseNumber = null;
            clsDRWrapper rsD = new clsDRWrapper();
            rsD.OpenRecordset("SELECT * FROM DogInventory where ID = " + FCConvert.ToString(intValue), modGNBas.DEFAULTDATABASE);
            if (!rsD.EndOfFile())
            {
                GetKennelLicenseNumber = rsD.Get_Fields_Int32("StickerNumber");
            }
            return GetKennelLicenseNumber;
        }

        public static void ShowWaitMessage(FCForm FormName, int intSleepTime = 600)
        {
            modGNBas.Sleep(intSleepTime);
        }
      
        public static object FixQuote(string strValue)
        {
            object FixQuote = null;
            FixQuote = strValue.Replace("'", "''");
            return FixQuote;
        }       

        public static void GetAllRegistrar(ref FCComboBox ControlName)
        {
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                int i;
                clsDRWrapper rsRegistrar = new clsDRWrapper();
                rsRegistrar.OpenRecordset("SELECT * FROM DefaultRegistrarNames", modGNBas.DEFAULTDATABASE);
                ControlName.Clear();
                while (!rsRegistrar.EndOfFile())
                {
                    if (Strings.Trim(rsRegistrar.Get_Fields_String("Lastname") + " ").Length >= 12)
                    {
                        ControlName.AddItem(Strings.Trim(rsRegistrar.Get_Fields_String("Lastname") + " ") + "," + Strings.Left(Strings.Trim(rsRegistrar.Get_Fields_String("Lastname") + " "), 12) + Strings.Trim(rsRegistrar.Get_Fields_String("Firstname") + " ") + "," + Strings.Space(15 - Strings.Trim(rsRegistrar.Get_Fields_String("FirstName") + " ").Length) + Strings.Trim(rsRegistrar.Get_Fields_String("MI") + " "));
                    }
                    else
                    {
                        ControlName.AddItem(Strings.Trim(rsRegistrar.Get_Fields_String("Lastname") + " ") + "," + Strings.Space(12 - Strings.Trim(rsRegistrar.Get_Fields_String("Lastname") + " ").Length) + Strings.Trim(rsRegistrar.Get_Fields_String("Firstname") + " ") + "," + Strings.Space(15 - Strings.Trim(rsRegistrar.Get_Fields_String("FirstName") + " ").Length) + Strings.Trim(rsRegistrar.Get_Fields_String("MI") + " "));
                    }
                    ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsRegistrar.Get_Fields_Int32("ID")));
                    rsRegistrar.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
            }
        }

        public static void GetAllFacility(ref FCComboBox ControlName)
        {
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                int i;
                clsDRWrapper rsFacility = new clsDRWrapper();
                rsFacility.OpenRecordset("SELECT * FROM FacilityNames", modGNBas.DEFAULTDATABASE);
                ControlName.Clear();
                while (!rsFacility.EndOfFile())
                {
                    // corey 12/5/2005
                    // If Len(rsFacility.Fields("Lastname")) > 19 Then
                    // ControlName.AddItem rsFacility.Fields("Lastname") & "," '& Space(20 - Len(rsFacility.Fields("Lastname")))
                    // Else
                    // ControlName.AddItem rsFacility.Fields("Lastname") & "," & Space(20 - Len(rsFacility.Fields("Lastname")))
                    // End If
                    ControlName.AddItem(rsFacility.Get_Fields_String("Lastname"));
                    ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsFacility.Get_Fields_Int32("ID")));
                    rsFacility.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
            }
        }

        public static string GetListOfDogsByOwner(ref int lngOwnerNum)
        {
            string GetListOfDogsByOwner = "";
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strList;
            strList = "";
            if (lngOwnerNum > 0)
            {
                clsLoad.OpenRecordset("select ID from doginfo where ownernum = " + FCConvert.ToString(lngOwnerNum) + " and dogdeceased <> 1 order by ID", "twck0000.vb1");
                while (!clsLoad.EndOfFile())
                {
                    strList += clsLoad.Get_Fields_Int32("ID") + ",";
                    clsLoad.MoveNext();
                }
                if (strList.Length > 0)
                {
                    strList = Strings.Mid(strList, 1, strList.Length - 1);
                }
            }
            GetListOfDogsByOwner = strList;
            return GetListOfDogsByOwner;
        }

        public static string GetKennelLicenseByDog(int lngDogID)
        {
            string GetKennelLicenseByDog = "";
            GetKennelLicenseByDog = "";
            string strReturn;
            clsDRWrapper rsLoad = new clsDRWrapper();
            int lngOwner = 0;
            string[] strAry = null;
            // vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
            int X;
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                strReturn = "";
                rsLoad.OpenRecordset("select ownernum from doginfo where ID = " + FCConvert.ToString(lngDogID), "twck0000.vb1");
                if (!rsLoad.EndOfFile())
                {
                    lngOwner = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ownernum"));
                    rsLoad.OpenRecordset("select * from kennellicense where ownernum = " + FCConvert.ToString(lngOwner), "twck0000.vb1");
                    while (!rsLoad.EndOfFile())
                    {
                        if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("dognumbers"))) != String.Empty)
                        {
                            strAry = Strings.Split(Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("dognumbers"))), ",", -1, CompareConstants.vbTextCompare);
                            for (X = 0; X <= (Information.UBound(strAry, 1)); X++)
                            {
                                if (Conversion.Val(strAry[X]) == lngDogID)
                                {
                                    strReturn = FCConvert.ToString(GetKennelLicenseNumber(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("licenseid"))))));
                                    GetKennelLicenseByDog = strReturn;
                                    return GetKennelLicenseByDog;
                                }
                            }
                            // X
                        }
                        rsLoad.MoveNext();
                    }
                }
                GetKennelLicenseByDog = strReturn;
                return GetKennelLicenseByDog;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetKennelLicenseByDog", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetKennelLicenseByDog;
        }

        public static string[] SplitQuotedText(ref string strInput, string strDelim = ",")
        {
            string[] SplitQuotedText = null;
            string strNewInput = "";
            bool boolInQuote;
            int intindex;
            string strChar = "";
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                intindex = 1;
                boolInQuote = false;
                while (intindex <= strInput.Length)
                {
                    strChar = Strings.Mid(strInput, intindex, 1);
                    if (strChar == FCConvert.ToString(Convert.ToChar(34)))
                    {
                        boolInQuote = !boolInQuote;
                    }
                    else
                    {
                        if (boolInQuote)
                        {
                            strNewInput += strChar;
                        }
                        else
                        {
                            if (strChar == strDelim)
                            {
                                strNewInput += "|";
                            }
                            else
                            {
                                strNewInput += strChar;
                            }
                        }
                    }
                    intindex += 1;
                }
                SplitQuotedText = Strings.Split(strNewInput, "|", -1, CompareConstants.vbTextCompare);
                return SplitQuotedText;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SplitQuotedText", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return SplitQuotedText;
        }    

        //public class StaticVariables
        //{
        //    //=========================================================
        //    public clsDRWrapper WWK;
        //}

        //public static StaticVariables Statics
        //{
        //    get
        //    {
        //        return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
        //    }
        //}
    }
}
