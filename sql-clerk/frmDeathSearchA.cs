//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.Clerk.Deaths;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDeathSearchA : BaseForm, IView<IDeathSearchViewModel>
	{
		public frmDeathSearchA()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private bool cancelling = true;
        public frmDeathSearchA(IDeathSearchViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			if (_InstancePtr == null )
				_InstancePtr = this;
			Text2 = new System.Collections.Generic.List<FCTextBox>();
			Text2.AddControlArrayElement(Text2_0, 0);
			Text2.AddControlArrayElement(Text2_1, 1);
			Text2.AddControlArrayElement(Text2_4, 2);
			cmdSch = new System.Collections.Generic.List<FCButton>();
			cmdSch.AddControlArrayElement(cmdSch_1, 1);
			cmdSch.AddControlArrayElement(cmdSch_2, 2);
			cmdSch.AddControlArrayElement(cmdSch_3, 3);
			cmdSch.AddControlArrayElement(cmdSch_4, 4);
			Label2 = new System.Collections.Generic.List<FCLabel>();
			Label2.AddControlArrayElement(Label2_0, 0);
			Label2.AddControlArrayElement(Label2_1, 1);
			Label2.AddControlArrayElement(Label2_4, 2);
			Label2.AddControlArrayElement(Label2_5, 3);
            this.Closing += FrmDeathSearchA_Closing;
		}

        private void FrmDeathSearchA_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmDeathSearchA InstancePtr
		{
			get
			{
				return (frmDeathSearchA)Sys.GetInstance(typeof(frmDeathSearchA));
			}
		}

		protected frmDeathSearchA _InstancePtr = null;
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		//object temp;
		const int CNSTGRIDCOLID = 0;
		const int CNSTGRIDCOLLAST = 1;
		const int CNSTGRIDCOLFIRST = 2;
		const int CNSTGRIDCOLDATE = 3;

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			if (Grid.Row < 1)
				return;
			modGNBas.Statics.gboolSearchPrint = true;
			modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
			modClerkGeneral.Statics.utPrintInfo.TblName = "Deaths";
			strSQL = "Select * from Deaths where ID = " + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
			modGNBas.Statics.rsDeathCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			if (Conversion.Val(modGNBas.Statics.rsDeathCertificate.Get_Fields_Int16("CertifiedType")) == 0)
			{
				MessageBox.Show("This record cannot be printed as a Medical Examiner has been chosen. MUST COPY ORIGINAL DOCUMENT", "Cannot Print", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			this.Hide();
			frmPrintVitalRec.InstancePtr.Show(App.MainForm);
		}

		private void cmdSch_Click(int Index, object sender, System.EventArgs e)
		{
			object temp;
			switch (Index)
			{
				case 0:
					{
						rs = null;
						//frmDeathSearchA.InstancePtr.Close();
						break;
					}
				case 1:
					{
						SearchBth();
						break;
					}
				case 2:
					{
						break;
					}
				case 3:
					{
						clrForm();
						break;
					}
				case 4:
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.Response)) == string.Empty)
						{
							MessageBox.Show("Death record must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
						//frmDeaths.InstancePtr.Unload();
						modClerkGeneral.Statics.AddingDeathCertificate = false;
                        ViewModel.Select(Convert.ToInt32(modGNBas.Statics.Response));
						//frmDeaths.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(modGNBas.Statics.Response)));
						//clrForm();
						CloseWithoutCancel();
						break;
					}
			}
			//end switch
		}

		private void cmdSch_Click(object sender, System.EventArgs e)
		{
			int index = cmdSch.GetIndex((FCButton)sender);
			cmdSch_Click(index, sender, e);
		}

		private void frmDeathSearchA_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
		}

		private void frmDeathSearchA_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDeathSearchA_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// KeyAscii = KeyAscii - 32
			}
			if (KeyAscii == Keys.Return)
				Support.SendKeys("{TAB}", false);
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDeathSearchA_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this, false);
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCMaskedTextBox)
				{
					(obj as FCMaskedTextBox).Mask = "##/##/####";
					(obj as FCMaskedTextBox).SelectionStart = 0;
				}
			}
			SetupGrid();
		}

		private void frmDeathSearchA_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			
		}

		private void Grid_ClickEvent(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			rs.FindFirstRecord("ID", Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
			modGNBas.Statics.Response = rs.Get_Fields_Int32("ID");
			modGNBas.Statics.gintDeathNumber = FCConvert.ToInt16(modGNBas.Statics.Response);
			clrForm(false);
			if (FCConvert.ToString(rs.Get_Fields_String("LastName")) != "")
				Text2[0].Text = Text2[0].Text + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("LastName")));
			if (FCConvert.ToString(rs.Get_Fields_String("FirstName")) != "")
				Text2[0].Text = Text2[0].Text + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("FirstName")));
			if (FCConvert.ToString(rs.Get_Fields_String("MiddleName")) != "")
				Text2[0].Text = Text2[0].Text + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MiddleName")));
			Text2[0].Text = fecherFoundation.Strings.Trim(Text2[0].Text);
			Text2[1].Text = modDateRoutines.GetPlaceOfDeath((fecherFoundation.Strings.Trim(rs.Get_Fields_String("placeofdeath") + " ") == string.Empty ? "5" : fecherFoundation.Strings.Trim(rs.Get_Fields_String("placeofdeath") + " ")));
			meBox1_0.Mask = "";
			meBox1_0.Text = Strings.Format(rs.Get_Fields("DateofDeath"), "MM/dd/yyyy");
			meBox1_0.Mask = "##/##/####";
			chkBirthOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("BirthOnFile")) ? 1 : 0);
			chkMarriageOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("MarriageOnFile")) ? 1 : 0);
		}

		private void optDateofDeath_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 2)
			{
				meBox1_0.Enabled = false;
				meBox1_1.Enabled = false;
				cmbDateofDeath.Text = "Disable";
				cmdSch[1].Focus();
			}
			else
			{
				if (cmbDateofDeath.Text == "Exact Date of Death or")
				{
					meBox1_0.Enabled = true;
					meBox1_1.Enabled = false;
				}
				else if (cmbDateofDeath.Text == "Between Dates")
				{
					meBox1_0.Enabled = true;
					meBox1_1.Enabled = true;
				}
				meBox1_0.Focus();
			}
		}

		private void optDateofDeath_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbDateofDeath.SelectedIndex;
			optDateofDeath_CheckedChanged(index, sender, e);
		}

		private void Text2_Enter(int Index, object sender, System.EventArgs e)
		{
			Text2[Index].SelectionStart = 0;
			Text2[Index].SelectionLength = Text2[Index].Text.Length;
		}

		private void Text2_Enter(object sender, System.EventArgs e)
		{
			int index = Text2.GetIndex((FCTextBox)sender);
			Text2_Enter(index, sender, e);
		}

		public void SearchBth()
		{
			string strSQL = "";
			int I;
			// vbPorter upgrade warning: tmpString As object	OnWrite(string, int)
			object[] tmpString = new object[2 + 1];
			string strOrder;
			for (I = 0; I <= 0; I++)
			{
				if (Text2[I].Text != "")
					tmpString[I] = modClerkGeneral.SortString2(Text2[I].Text);
			}
			// I
			if (fecherFoundation.Strings.Trim(Text2[1].Text) != string.Empty)
			{
				tmpString[1] = modDateRoutines.GetPlaceOfDeathID(Text2[1].Text);
			}
			else
			{
				tmpString[1] = "";
			}
			strOrder = "";
			if (FCConvert.ToString(tmpString[0]) != "")
			{
				strSQL = DeceasedsName(Strings.Split(FCConvert.ToString(tmpString[0]), " ", -1, CompareConstants.vbBinaryCompare)) + " AND ";
				// strOrder = " order by lastname"
			}
			if (FCConvert.ToString(tmpString[1]) != "")
				strSQL += "PlaceofDeath LIKE '" + tmpString[1] + "' AND ";
			if (cmbDateofDeath.Text == "Exact Date of Death or")
			{
				if (!Information.IsDate(meBox1_0.Text))
				{
					MessageBox.Show("Invalid date for date range.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					meBox1_0.Focus();
					return;
				}
				else
				{
					strSQL += "(DateofDeath = '" + meBox1_0.Text + "' OR ActualDate = '" + meBox1_0.Text + "' OR (ISDATE(dateofdeathdescription) = 1 and convert(datetime,dateofdeathdescription) = '" + meBox1_0.Text + "')) AND ";
				}
			}
			else if (cmbDateofDeath.Text == "Between Dates")
			{
				if (!Information.IsDate(meBox1_0.Text))
				{
					MessageBox.Show("Invalid begin date for date range.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					meBox1_0.Focus();
					return;
				}
				else if (!Information.IsDate(meBox1_1.Text))
				{
					MessageBox.Show("Invalid end date for date range.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					meBox1_1.Focus();
					return;
				}
				else
				{
					strSQL += " (DateofDeath BETWEEN '" + meBox1_0.Text + "' AND '" + meBox1_1.Text + "' OR ActualDate BETWEEN '" + meBox1_0.Text + "' AND '" + meBox1_1.Text + "' OR (isdate(dateofdeathdescription) = 1 and convert(datetime,dateofdeathdescription) between '" + meBox1_0.Text + "' and '" + meBox1_1.Text + "')) AND ";
				}
			}
			if (strSQL == "")
			{
				strSQL = "SELECT  * FROM Deaths ORDER BY LastName";
			}
			else
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 5);
				strSQL = "SELECT  * FROM Deaths WHERE " + strSQL + " order by lastname,firstname ";
			}
			rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			Grid.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					Grid.Rows += 1;
					Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLLAST, FCConvert.ToString(rs.Get_Fields_String("lastname")));
					Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLFIRST, rs.Get_Fields_String("firstname") + " " + rs.Get_Fields_String("middlename"));
					Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLDATE, FCConvert.ToString(rs.Get_Fields("dateofbirth")));
					Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLID, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					rs.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("Nothing Found", "Nothing Found:", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		// vbPorter upgrade warning: CN As object	OnWrite(string())	OnRead(string)
		public string DeceasedsName(string[] CN)
		{
			string DeceasedsName = "";
			string strTemp1 = "";
			string strTemp2 = "";
			string strTemp3 = "";
			switch (Information.UBound(CN, 1) + 1)
			{
				case 1:
					{
						strTemp1 = FCConvert.ToString(((object[])CN)[0]);
						strTemp1 = modGlobalFunctions.EscapeQuotes(strTemp1);
						DeceasedsName = "LastName LIKE '" + strTemp1 + "%'";
						break;
					}
				case 2:
					{
						strTemp1 = FCConvert.ToString(((object[])CN)[0]);
						strTemp1 = modGlobalFunctions.EscapeQuotes(strTemp1);
						strTemp2 = FCConvert.ToString(((object[])CN)[1]);
						strTemp2 = modGlobalFunctions.EscapeQuotes(strTemp2);
						DeceasedsName = "LastName LIKE '" + strTemp1 + "%' AND FirstName LIKE '" + strTemp2 + "%'";
						break;
					}
				case 3:
					{
						strTemp1 = FCConvert.ToString(((object[])CN)[0]);
						strTemp1 = modGlobalFunctions.EscapeQuotes(strTemp1);
						strTemp2 = FCConvert.ToString(((object[])CN)[1]);
						strTemp2 = modGlobalFunctions.EscapeQuotes(strTemp2);
						strTemp3 = FCConvert.ToString(((object[])CN)[2]);
						strTemp3 = modGlobalFunctions.EscapeQuotes(strTemp3);
						DeceasedsName = "LastName LIKE '" + strTemp1 + "%' AND FirstName LIKE '" + strTemp2 + "*' AND MiddleName LIKE '" + strTemp3 + "%'";
						break;
					}
				default:
					{
						MessageBox.Show("Deceased's Name Error");
						break;
					}
			}
			//end switch
			return DeceasedsName;
		}

		public void clrForm(bool boolClear = true)
		{
			object[] tmpString = null;
			// - "AutoDim"
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is FCMaskedTextBox)
				{
					(obj as FCMaskedTextBox).Mask = "";
					(obj as FCMaskedTextBox).Text = "";
					(obj as FCMaskedTextBox).Mask = "##/##/####";
				}
			}
			// obj
			if (boolClear)
				Grid.Rows = 1;
			cmbDateofDeath.Text = "Disable";
			tmpString = new object[2 + 1];
		}

		public void FillBoxes()
		{
			Text2[0].Text = rs.Get_Fields_String("FirstName") + "  " + rs.Get_Fields_String("MiddleName") + " " + rs.Get_Fields_String("LastName");
		}

		private void SetupGrid()
		{
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLLAST, "Last");
			Grid.TextMatrix(0, CNSTGRIDCOLFIRST, "First Middle");
			Grid.TextMatrix(0, CNSTGRIDCOLDATE, "Birth Date");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLLAST, FCConvert.ToInt32(0.33 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLFIRST, FCConvert.ToInt32(0.45 * GridWidth));
		}

        public IDeathSearchViewModel ViewModel { get; set; }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

		private void cmdNew_Click(object sender, EventArgs e)
		{
			modClerkGeneral.Statics.AddingDeathCertificate = true;
			ViewModel.Select(0);
			CloseWithoutCancel();
		}
	}
}
