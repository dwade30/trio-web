﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Clerk.Commands;
using SharedApplication.Clerk.Enums;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmBirthReport : BaseForm
	{
		public frmBirthReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmBirthReport InstancePtr
		{
			get
			{
				return (frmBirthReport)Sys.GetInstance(typeof(frmBirthReport));
			}
		}

		protected frmBirthReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmBirthReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBirthReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBirthReport properties;
			//frmBirthReport.FillStyle	= 0;
			//frmBirthReport.ScaleWidth	= 5880;
			//frmBirthReport.ScaleHeight	= 4125;
			//frmBirthReport.LinkTopic	= "Form2";
			//frmBirthReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
		}

		private void frmBirthReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.27));
			Grid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.365));
			//Grid.Height = 4 * Grid.RowHeight(0) + 30;
		}

		private void SetupGrid()
		{
			Grid.TextMatrix(0, 0, "Last Name");
			Grid.TextMatrix(1, 0, "Birthdate");
			Grid.TextMatrix(2, 0, "Mother's Last");
			Grid.TextMatrix(3, 0, "Birthplace");
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (Grid.Row == 1)
			{
				string strD = "";
				if (fecherFoundation.Strings.Trim(Grid.EditText).Length == 8)
				{
					if (Information.IsNumeric(fecherFoundation.Strings.Trim(Grid.EditText)))
					{
						strD = fecherFoundation.Strings.Trim(Grid.EditText);
						strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
						if (Information.IsDate(strD))
						{
							Grid.EditText = strD;
						}
					}
				}
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{

			Grid.Row = -1;

            var reportData = StaticSettings.GlobalCommandDispatcher.Send(new GetBirthReportInfo
			{
				EndBirthDate = modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(1, 2).Trim()),
				StartBirthDate = modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(1, 1).Trim()),
				EndLastName = modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(0, 2).Trim()),
				StartLastName = modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(0, 1).Trim()),
				EndBirthPlace = modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(3, 2).Trim()),
				StartBirthPlace = modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(3, 1).Trim()),
				EndMothersLastName = modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(2, 2).Trim()),
				StartMothersLastName = modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(2, 1).Trim()),
				SortBy = cmbSort.Text == "Name" ? BirthReportSortOptions.Name 
					: cmbSort.Text == "Birthdate" ? BirthReportSortOptions.BirthDate 
					: cmbSort.Text == "Mother's name" ? BirthReportSortOptions.MothersName 
					: BirthReportSortOptions.Name
			}).Result;

			rptBirthReport.InstancePtr.Init(reportData.ToList());
		}
	}
}
