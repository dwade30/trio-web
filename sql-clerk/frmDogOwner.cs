//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	public partial class frmDogOwner : BaseForm
	{
		public frmDogOwner()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null && System.Windows.Forms.Application.OpenForms.Count == 0)
				_InstancePtr = this;
			lblLabels = new System.Collections.Generic.List<FCLabel>();
			lblLabels.AddControlArrayElement(lblLabels_0, 0);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDogOwner InstancePtr
		{
			get
			{
				return (frmDogOwner)Sys.GetInstance(typeof(frmDogOwner));
			}
		}

		protected frmDogOwner _InstancePtr = null;
		//=========================================================
		int lngOwnerNumber;
		const int COLDOGNUM = 0;
		const int COLDOGNAME = 1;
		const int COLDOGYEAR = 2;
		const int COLDOGTAG = 3;
		// Private Const COLDOGSTICKER = 4
		const int COLDOGBREED = 4;
		const int COLRABIESEXP = 5;
		const int COLKENNELLIC = 0;
		const int COLKENNELISSUE = 1;
		const int COLKENNELNUMDOGS = 2;
		const int COLKENNELINSPECTION = 3;
		const int COLKENNELLICID = 4;
		const int COLLISTDOGNUM = 0;
		const int COLLISTDOGNAME = 1;
		const int COLLISTDOGCOLOR = 3;
		const int COLLISTDOGBREED = 2;
		const int COLLISTPREVOWNERLAST = 4;
		const int COLLISTPREVOWNERFIRST = 5;
		const int COLLISTPREVOWNERNUM = 6;
		private modGNBas.typTransactionRecord curTrans = new modGNBas.typTransactionRecord();
		private bool boolCreatedTransaction;
		private modGNBas.typTransactionRecord CurKennelTrans = new modGNBas.typTransactionRecord();
		private bool boolCreatedKennelTransaction;
		private bool boolAlreadyChangedTransactionDate;
		private bool boolDataChanged;
		cPartyController pCont = new cPartyController();
		cParty pInfo;
		cPartyAddress pAdd;
		int lngCurrentPartyID;
		bool boolReadOnly;

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGNBas.Statics.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblregionS where townnumber > 0 order by townnumber", "CentralData");
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("townnumber") + ";" + clsLoad.Get_Fields_String("townname") + "\t" + clsLoad.Get_Fields("townnumber") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				strTemp = "0";
				gridTownCode.Visible = false;
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}

		private void ResizeGridDogs()
		{
			int GridWidth = 0;
			GridWidth = GridDogs.WidthOriginal;
			GridDogs.ColWidth(1, FCConvert.ToInt32(0.3 * GridWidth));
			GridDogs.ColWidth(2, FCConvert.ToInt32(0.1 * GridWidth));
			GridDogs.ColWidth(3, FCConvert.ToInt32(0.2 * GridWidth));
			GridDogs.ColWidth(4, FCConvert.ToInt32(0.2 * GridWidth));
			GridDogs.ColWidth(5, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private void ResizeGridKennels()
		{
			int GridWidth = 0;
			GridWidth = GridKennels.WidthOriginal;
			GridKennels.ColWidth(COLKENNELLIC, FCConvert.ToInt32(0.27 * GridWidth));
			GridKennels.ColWidth(COLKENNELNUMDOGS, FCConvert.ToInt32(0.15 * GridWidth));
			GridKennels.ColWidth(COLKENNELISSUE, FCConvert.ToInt32(0.27 * GridWidth));
		}

		private void ResizeGridList()
		{
			int GridWidth = 0;
			GridWidth = gridList.WidthOriginal;
			gridList.ColWidth(COLLISTDOGNAME, FCConvert.ToInt32(0.2 * GridWidth));
			gridList.ColWidth(COLLISTDOGBREED, FCConvert.ToInt32(0.2 * GridWidth));
			gridList.ColWidth(COLLISTDOGCOLOR, FCConvert.ToInt32(0.2 * GridWidth));
			gridList.ColWidth(COLLISTPREVOWNERLAST, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private void SetupGridList()
		{
			gridList.ColHidden(COLLISTDOGNUM, true);
			gridList.ColHidden(COLLISTPREVOWNERNUM, true);
			gridList.TextMatrix(0, COLLISTDOGNAME, "Name");
			gridList.TextMatrix(0, COLLISTDOGBREED, "Breed");
			gridList.TextMatrix(0, COLLISTDOGCOLOR, "Color");
			gridList.TextMatrix(0, COLLISTPREVOWNERLAST, "Last Name");
			gridList.TextMatrix(0, COLLISTPREVOWNERFIRST, "First Name");
		}

		private void SetupGridDogs()
		{
			GridDogs.ColHidden(COLDOGNUM, true);
			GridDogs.TextMatrix(0, COLDOGNAME, "Name");
			GridDogs.TextMatrix(0, COLDOGYEAR, "Year");
			GridDogs.TextMatrix(0, COLDOGTAG, "Tag Number");
			// .TextMatrix(0, COLDOGSTICKER) = "Sticker"
			GridDogs.TextMatrix(0, COLDOGBREED, "Breed");
			GridDogs.TextMatrix(0, COLRABIESEXP, "Rabies Exp");
		}

		private void SetupGridKennels()
		{
			GridKennels.ColHidden(COLKENNELLICID, true);
			GridKennels.TextMatrix(0, COLKENNELLIC, "License #");
			GridKennels.TextMatrix(0, COLKENNELINSPECTION, "Inspection Date");
			GridKennels.TextMatrix(0, COLKENNELISSUE, "Issue Date");
			GridKennels.TextMatrix(0, COLKENNELNUMDOGS, "# Dogs");
		}

		private void cmdCancelAddDeleted_Click(object sender, System.EventArgs e)
		{
			framAddDog.Visible = false;
		}

		private void frmDogOwner_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string[] strAry = null;
			string strTemp = "";
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						Support.SendKeys("{TAB}", false);
						//App.DoEvents();
						break;
					}
				case Keys.Insert:
					{
						KeyCode = (Keys)0;
						if (!(pAdd == null))
						{
							if (fecherFoundation.Strings.Trim(pAdd.Address1) != string.Empty)
							{
								if (Conversion.Val(pAdd.Address1) > 0)
								{
									strAry = Strings.Split(pAdd.Address1, " ", -1, CompareConstants.vbTextCompare);
									if (Information.UBound(strAry, 1) >= 0)
									{
										if (Conversion.Val(strAry[0]) > 0)
										{
											txtStreetNumber.Text = FCConvert.ToString(Conversion.Val(strAry[0]));
											strTemp = "";
											for (x = 1; x <= (Information.UBound(strAry, 1)); x++)
											{
												strTemp += " " + strAry[x];
											}
											// x
											strTemp = fecherFoundation.Strings.Trim(strTemp);
											txtStreet.Text = strTemp;
										}
										else
										{
											strTemp = "";
											for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
											{
												strTemp += " " + strAry[x];
											}
											// x
											strTemp = fecherFoundation.Strings.Trim(strTemp);
											txtStreet.Text = strTemp;
										}
									}
								}
								else
								{
									txtStreet.Text = pAdd.Address1;
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void frmDogOwner_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDogOwner properties;
			//frmDogOwner.ScaleWidth	= 8925;
			//frmDogOwner.ScaleHeight	= 7725;
			//frmDogOwner.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void frmDogOwner_Resize(object sender, System.EventArgs e)
		{
			ResizeGridDogs();
			ResizeGridKennels();
			ResizeGridList();
			ResizeGridTownCode();
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.HeightOriginal = gridTownCode.RowHeight(0) + 60;
		}
		// vbPorter upgrade warning: lngOwnerNum As int	OnWrite(int, string)
		public void Init(int lngOwnerNum)
		{
			// Owner number of zero means make a new one
			boolCreatedTransaction = false;
			boolCreatedKennelTransaction = false;
			lngOwnerNumber = lngOwnerNum;
			CheckPermissions();
			t2kTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			curTrans.TransactionDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
			curTrans.Description = "No Transaction";
			curTrans.Amount1 = 0;
			curTrans.Amount2 = 0;
			curTrans.Amount3 = 0;
			curTrans.Amount4 = 0;
			curTrans.Amount5 = 0;
			curTrans.Amount6 = 0;
			curTrans.TotalAmount = 0;
			modGNBas.Statics.typTransaction.TransactionDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
			SetupGridDogs();
			SetupGridKennels();
			SetupGridList();
			SetupGridTownCode();
			// FillStateCombo
			if (!LoadOwner())
			{
				MessageBox.Show("This owner is deleted, cannot continue loading", "Deleted Owner", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			GridDogs.Rows = 1;
			FillDogs();
			GridKennels.Rows = 1;
			FillKennels();
			boolDataChanged = false;
			this.Show(App.MainForm);
		}

		private bool LoadOwner()
		{
			bool LoadOwner = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x;
			string strTemp = "";
			LoadOwner = true;
			if (lngOwnerNumber > 0)
			{
				clsLoad.OpenRecordset("select * from dogowner where ID = " + FCConvert.ToString(lngOwnerNumber), "twck0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Convert.ToBoolean(clsLoad.Get_Fields_Boolean("deleted")))
					{
						LoadOwner = false;
						return LoadOwner;
					}
					if (Convert.ToBoolean(clsLoad.Get_Fields_Boolean("norequiredfields")))
					{
						chkNotRequired.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkNotRequired.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					txtCustomerNumber.Text = FCConvert.ToString(clsLoad.Get_Fields("PartyID"));
					lblCustomerInfo.Text = "";
					GetPartyInfo(clsLoad.Get_Fields_Int32("PartyID"));
					txtStreet.Text = FCConvert.ToString(clsLoad.Get_Fields_String("locationstr"));
					txtStreetNumber.Text = FCConvert.ToString(clsLoad.Get_Fields_String("locationnum"));
					if (Conversion.Val(clsLoad.Get_Fields_Int32("towncode")) > 0)
					{
						gridTownCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("towncode"))));
					}
					curTrans.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("towncode"))));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("comments"))) != string.Empty)
					{
						Image1.Visible = true;
					}
					else
					{
						Image1.Visible = false;
					}
				}
			}
			else
			{
				chkNotRequired.CheckState = Wisej.Web.CheckState.Unchecked;
				Image1.Visible = false;
				ClearPartyInfo();
				txtStreet.Text = "";
				txtStreetNumber.Text = "";
			}
			return LoadOwner;
		}

		private void FillDogs()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			if (lngOwnerNumber > 0)
			{
				clsLoad.OpenRecordset("select * from doginfo where ownernum = " + FCConvert.ToString(lngOwnerNumber) + " AND isnull(kenneldog,0) <> 1 order by dogname", "twck0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					GridDogs.Rows += 1;
					GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGNUM, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ID"))));
					GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGNAME, FCConvert.ToString(clsLoad.Get_Fields_String("DogName")));
					GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGYEAR, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("Year"))));
					// .TextMatrix(.Rows - 1, COLDOGSTICKER) = clsLoad.Fields("stickerlicnum")
					GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGBREED, FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed")));
					GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGTAG, FCConvert.ToString(clsLoad.Get_Fields_String("taglicnum")));
					if (Information.IsDate(clsLoad.Get_Fields("rabiesexpdate")))
					{
						if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("rabiesexpdate")).ToOADate() != 0)
						{
							GridDogs.TextMatrix(GridDogs.Rows - 1, COLRABIESEXP, FCConvert.ToString(clsLoad.Get_Fields_DateTime("rabiesexpdate")));
						}
					}
					else
					{
						GridDogs.TextMatrix(GridDogs.Rows - 1, COLRABIESEXP, "");
					}
					if (clsLoad.Get_Fields_Boolean("DogDeceased") == true)
					{
						// .Cell(FCGrid.CellPropertySettings.flexcpForeColor, .Rows - 1, 0, .Rows - 1, .Cols - 1) = TRIOCOLORRED
						GridDogs.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridDogs.Rows - 1, 0, GridDogs.Rows - 1, GridDogs.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
						Shape1.FillColor(ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED));
						Label14.Visible = true;
						Shape1.Visible = true;
					}
					clsLoad.MoveNext();
				}
			}
			else
			{
				GridDogs.Rows = 1;
			}
		}

		private void FillKennels()
		{
			// create separate table for kennels?
			// until then just load from owner record
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: strTemp As string	OnRead(DateTime)
			string strTemp = "";
			// vbPorter upgrade warning: dttemp As DateTime	OnWrite(string)
			DateTime dttemp;
			int x;
			GridKennels.Rows = 1;
			x = 0;
			if (lngOwnerNumber > 0)
			{
				clsLoad.OpenRecordset("select * from KENNELLICENSE where ownernum = " + FCConvert.ToString(lngOwnerNumber), "twck0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					// has a kennel license
					x += 1;
					// If Val(clsLoad.Fields("licenseid")) > 0 Then
					GridKennels.Rows += 1;
					GridKennels.TextMatrix(GridKennels.Rows - 1, COLKENNELLIC, FCConvert.ToString(modGlobalRoutines.GetKennelLicenseNumber(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("licenseid")))))));
					strTemp = FCConvert.ToString(clsLoad.Get_Fields_DateTime("reissuedate"));
					if (Information.IsDate(strTemp))
					{
						dttemp = FCConvert.ToDateTime(strTemp);
						if (dttemp.ToOADate() != 0)
						{
							GridKennels.TextMatrix(GridKennels.Rows - 1, COLKENNELISSUE, Strings.Format(dttemp, "MM/dd/yyyy"));
						}
						else
						{
							if (Information.IsDate(clsLoad.Get_Fields("originalissuedate")))
							{
								if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("originalissuedate")).ToOADate() != 0)
								{
									dttemp = (DateTime)clsLoad.Get_Fields_DateTime("originalissuedate");
									GridKennels.TextMatrix(GridKennels.Rows - 1, COLKENNELISSUE, Strings.Format(dttemp, "MM/dd/yyyy"));
								}
							}
							else
							{
							}
						}
					}
					else
					{
						if (Information.IsDate(clsLoad.Get_Fields("originalissuedate")))
						{
							if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("originalissuedate")).ToOADate() != 0)
							{
								dttemp = (DateTime)clsLoad.Get_Fields_DateTime("originalissuedate");
								GridKennels.TextMatrix(GridKennels.Rows - 1, COLKENNELISSUE, Strings.Format(dttemp, "MM/dd/yyyy"));
							}
						}
					}
					strTemp = FCConvert.ToString(clsLoad.Get_Fields_DateTime("inspectiondate"));
					if (Information.IsDate(strTemp))
					{
						dttemp = FCConvert.ToDateTime(strTemp);
						if (dttemp.ToOADate() != 0)
						{
							GridKennels.TextMatrix(GridKennels.Rows - 1, COLKENNELINSPECTION, Strings.Format(dttemp, "MM/dd/yyyy"));
						}
					}
					GridKennels.TextMatrix(GridKennels.Rows - 1, COLKENNELNUMDOGS, FCConvert.ToString(GetNumDogs(clsLoad.Get_Fields_String("dognumbers"))));
					// End If
					GridKennels.TextMatrix(GridKennels.Rows - 1, COLKENNELLICID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					clsLoad.MoveNext();
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToInt32(
		private int GetNumDogs(string strList)
		{
			int GetNumDogs = 0;
			string[] strAry = null;
			GetNumDogs = 0;
			if (strList == string.Empty)
			{
				return GetNumDogs;
			}
			strAry = Strings.Split(strList, ",", -1, CompareConstants.vbTextCompare);
			GetNumDogs = (Information.UBound(strAry, 1) + 1);
			return GetNumDogs;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (boolCreatedTransaction)
			{
				SaveCurTrans();
			}
			if (modCashReceiptingData.Statics.bolFromWindowsCR && boolCreatedTransaction)
			{
				TransferToCR();
				frmDogSearchResults.InstancePtr.Unload();
				frmNewDogSearch.InstancePtr.Unload();
				modGNBas.Statics.gboolEndApplication = true;
			}
		}

		private object UpdateCurTrans()
		{
			object UpdateCurTrans = null;
			clsDRWrapper clsSave = new clsDRWrapper();
			clsSave.OpenRecordset("select * from transactionTABLE where ID = " + FCConvert.ToString(curTrans.TransactionNumber), "twck0000.vb1");
			if (!clsSave.EndOfFile())
			{
				clsSave.Edit();
				clsSave.Set_Fields("amount1", curTrans.Amount1);
				clsSave.Set_Fields("amount2", curTrans.Amount2);
				clsSave.Set_Fields("amount3", curTrans.Amount3);
				clsSave.Set_Fields("amount4", curTrans.Amount4);
				clsSave.Set_Fields("amount5", curTrans.Amount5);
				clsSave.Set_Fields("amount6", curTrans.Amount6);
				clsSave.Set_Fields("TotalAmount", curTrans.TotalAmount);
				clsSave.Set_Fields("transactiondate", curTrans.TransactionDate);
				clsSave.Set_Fields("transactiontype", curTrans.TransactionType);
				clsSave.Set_Fields("userid", curTrans.UserID);
				clsSave.Set_Fields("Username", curTrans.UserName);
				clsSave.Set_Fields("description", curTrans.Description);
				clsSave.Set_Fields("TransactionPerson", curTrans.TransactionPerson);
				clsSave.Set_Fields("towncode", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				clsSave.Update();
			}
			return UpdateCurTrans;
		}

		private int SaveDogTransaction()
		{
			int SaveDogTransaction = 0;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngID = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsSave.OpenRecordset("Select top 1 * from dogtransactions", "twck0000.vb1");
				clsSave.AddNew();
				lngID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				clsSave.Set_Fields("transactionnumber", modGNBas.Statics.typDogTransaction.TransactionNumber);
				clsSave.Set_Fields("Amount", modGNBas.Statics.typDogTransaction.Amount);
				clsSave.Set_Fields("booladjustmentvoid", modGNBas.Statics.typDogTransaction.boolAdjustmentVoid);
				clsSave.Set_Fields("boolhearingguid", modGNBas.Statics.typDogTransaction.boolHearingGuid);
				clsSave.Set_Fields("boolkennel", modGNBas.Statics.typDogTransaction.boolKennel);
				clsSave.Set_Fields("boolLicense", modGNBas.Statics.typDogTransaction.boolLicense);
				clsSave.Set_Fields("boolreplacementlicense", modGNBas.Statics.typDogTransaction.boolReplacementLicense);
				clsSave.Set_Fields("boolreplacementsticker", modGNBas.Statics.typDogTransaction.boolReplacementSticker);
				clsSave.Set_Fields("boolreplacementtag", modGNBas.Statics.typDogTransaction.boolReplacementTag);
				clsSave.Set_Fields("boolsearchrescue", modGNBas.Statics.typDogTransaction.boolSearchRescue);
				clsSave.Set_Fields("boolspayneuter", modGNBas.Statics.typDogTransaction.boolSpayNeuter);
				clsSave.Set_Fields("booltemplicense", modGNBas.Statics.typDogTransaction.boolTempLicense);
				clsSave.Set_Fields("booltransfer", modGNBas.Statics.typDogTransaction.boolTransfer);
				clsSave.Set_Fields("clerkfee", modGNBas.Statics.typDogTransaction.ClerkFee);
				clsSave.Set_Fields("description", modGNBas.Statics.typDogTransaction.Description);
				clsSave.Set_Fields("dognumber", modGNBas.Statics.typDogTransaction.DogNumber);
				clsSave.Set_Fields("dogyear", modGNBas.Statics.typDogTransaction.DogYear);
                clsSave.Set_Fields("DangerousDog",modGNBas.Statics.typDogTransaction.IsDangerous);
                clsSave.Set_Fields("NuisanceDog", modGNBas.Statics.typDogTransaction.IsNuisance);
				clsSave.Set_Fields("kennellatefee", modGNBas.Statics.typDogTransaction.KennelLateFee);
				clsSave.Set_Fields("kennellic1fee", modGNBas.Statics.typDogTransaction.KennelLic1Fee);
				clsSave.Set_Fields("kennellic2fee", modGNBas.Statics.typDogTransaction.KennelLic2Fee);
				clsSave.Set_Fields("kennellic1num", modGNBas.Statics.typDogTransaction.KennelLic1Num);
				clsSave.Set_Fields("kennellic2num", modGNBas.Statics.typDogTransaction.KennelLic2Num);
				clsSave.Set_Fields("kennelwarrantfee", modGNBas.Statics.typDogTransaction.KennelWarrantFee);
				clsSave.Set_Fields("latefee", modGNBas.Statics.typDogTransaction.LateFee);
				clsSave.Set_Fields("ownernum", modGNBas.Statics.typDogTransaction.OwnerNum);
				clsSave.Set_Fields("rabiestagnumber", modGNBas.Statics.typDogTransaction.RabiesTagNumber);
				clsSave.Set_Fields("replacementlicensefee", modGNBas.Statics.typDogTransaction.ReplacementLicenseFee);
				clsSave.Set_Fields("replacementstickerfee", modGNBas.Statics.typDogTransaction.ReplacementStickerFee);
				clsSave.Set_Fields("replacementtagfee", modGNBas.Statics.typDogTransaction.ReplacementTagFee);
				clsSave.Set_Fields("spaynonspayamount", modGNBas.Statics.typDogTransaction.SpayNonSpayAmount);
				clsSave.Set_Fields("statefee", modGNBas.Statics.typDogTransaction.StateFee);
				clsSave.Set_Fields("stickernumber", modGNBas.Statics.typDogTransaction.StickerNumber);
				clsSave.Set_Fields("tagnumber", modGNBas.Statics.typDogTransaction.TAGNUMBER);
				clsSave.Set_Fields("templicensefee", modGNBas.Statics.typDogTransaction.TempLicenseFee);
				clsSave.Set_Fields("townfee", modGNBas.Statics.typDogTransaction.TownFee);
				clsSave.Set_Fields("transactiondate", modGNBas.Statics.typDogTransaction.TransactionDate);
				clsSave.Set_Fields("transferlicensefee", modGNBas.Statics.typDogTransaction.TransferLicenseFee);
				clsSave.Set_Fields("OldSticker", modGNBas.Statics.typDogTransaction.OldStickerNumber);
				clsSave.Set_Fields("OldTagNumber", modGNBas.Statics.typDogTransaction.OldTagNumber);
				clsSave.Set_Fields("OldYear", modGNBas.Statics.typDogTransaction.OldYear);
				clsSave.Set_Fields("OldIssueDate", modGNBas.Statics.typDogTransaction.OldIssueDate);
				clsSave.Set_Fields("animalcontrol", modGNBas.Statics.typDogTransaction.AnimalControl);
				clsSave.Set_Fields("kenneldogs", modGNBas.Statics.typDogTransaction.KennelDogs);
				clsSave.Set_Fields("kennellicenseID", modGNBas.Statics.typDogTransaction.KennelLicenseID);
				clsSave.Set_Fields("warrantfee", modGNBas.Statics.typDogTransaction.WarrantFee);
				clsSave.Set_Fields("timestamp", DateTime.Now);
				clsSave.Set_Fields("towncode", modGNBas.Statics.typDogTransaction.TownCode);
				clsSave.Update();
				SaveDogTransaction = lngID;
				return SaveDogTransaction;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveDogTransaction", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveDogTransaction;
		}

		private void CreateTransaction()
		{
			// This will create a transaction and put the trans number into the user type
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsSave.OpenRecordset("select top 1 * from transactiontable", "twck0000.vb1");
				clsSave.AddNew();
				curTrans.TransactionType = modGNBas.DOGTRANSACTION;
				curTrans.Description = "Dog transaction";
				curTrans.UserID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				curTrans.UserName = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserName());
				curTrans.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				clsSave.Update();
				curTrans.TransactionNumber = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				boolCreatedTransaction = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateTransaction", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridDogs_DblClick(object sender, System.EventArgs e)
		{
			bool boolReReg;
			string strDName;
			// if a dog is selected, edit it
			if (GridDogs.MouseRow < 1)
				return;
			if (Information.IsDate(t2kTransactionDate.Text))
			{
				modGNBas.Statics.typTransaction.TransactionDate = FCConvert.ToDateTime(t2kTransactionDate.Text);
				curTrans.TransactionDate = FCConvert.ToDateTime(t2kTransactionDate.Text);
			}
			else
			{
				MessageBox.Show("You must enter a valid transaction date before continuing", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (boolDataChanged && !boolReadOnly)
			{
				if (MessageBox.Show("Data has changed.  You must save before continuing." + "\r\n" + "Do you wish to do save now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveOwner())
					{
						return;
					}
				}
				else
				{
					return;
				}
			}
			boolReReg = false;
			if (!boolReadOnly)
			{
				if (Conversion.Val(GridDogs.TextMatrix(GridDogs.MouseRow, COLDOGYEAR)) < DateTime.Today.Year || (Conversion.Val(GridDogs.TextMatrix(GridDogs.Row, COLDOGYEAR)) == DateTime.Today.Year && DateTime.Today.Month >= 10))
				{
					if (MessageBox.Show("Open record as a re-registration?", "Re-Registration", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
						boolReReg = true;
					}
					else
					{
						boolReReg = false;
					}
				}
			}
			strDName = GridDogs.TextMatrix(GridDogs.MouseRow, COLDOGNAME);
            var result = frmNewDog.InstancePtr.Init(
                FCConvert.ToInt32(GridDogs.TextMatrix(GridDogs.MouseRow, COLDOGNUM)), lngOwnerNumber, boolReReg, false,
                0, !boolCreatedTransaction);
            if (result.outcome && !boolReadOnly)
			{
				// if true then made a transaction
				// update or create current transaction and save the dog transaction and the current transaction
				// save the current transaction since you don't want to lose info if you crash doing another dog transaction
				modGNBas.Statics.typDogTransaction.TransactionDate = curTrans.TransactionDate;
				if (!boolCreatedTransaction)
				{
					CreateTransaction();
					curTrans.Description = strDName;
					curTrans.TransactionDate = modGNBas.Statics.typTransaction.TransactionDate;
					CurKennelTrans.TransactionDate = curTrans.TransactionDate;
					curTrans.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
					CurKennelTrans.TownCode = curTrans.TownCode;
					t2kTransactionDate.Text = Strings.Format(curTrans.TransactionDate, "MM/dd/yyyy");
					t2kTransactionDate.Enabled = false;
				}
				else
				{
					curTrans.Description = "Multiple";
				}
				curTrans.Amount1 += modGNBas.Statics.typTransaction.Amount1;
				curTrans.Amount2 += modGNBas.Statics.typTransaction.Amount2;
				curTrans.Amount3 += modGNBas.Statics.typTransaction.Amount3;
				curTrans.Amount4 += modGNBas.Statics.typTransaction.Amount4;
				curTrans.Amount5 += modGNBas.Statics.typTransaction.Amount5;
				curTrans.Amount6 += modGNBas.Statics.typTransaction.Amount6;
				curTrans.TotalAmount += modGNBas.Statics.typTransaction.TotalAmount;
				modGNBas.Statics.typDogTransaction.TownCode = curTrans.TownCode;
				modGNBas.Statics.typDogTransaction.TransactionNumber = curTrans.TransactionNumber;
				SaveDogTransaction();
				UpdateCurTrans();
			}
			else
			{
				// if false, didn't make a transaction
			}
			// update the list in case one was deleted
			GridDogs.Rows = 1;
			FillDogs();
		}

		private void GridKennels_DblClick(object sender, System.EventArgs e)
		{
			bool boolReReg;
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
			DateTime dtDate;
			int lngYear = 0;
			clsDRWrapper rsKennelInfo = new clsDRWrapper();
			if (GridKennels.MouseRow < 1)
				return;
			if (Information.IsDate(t2kTransactionDate.Text))
			{
				modGNBas.Statics.typTransaction.TransactionDate = FCConvert.ToDateTime(t2kTransactionDate.Text);
				curTrans.TransactionDate = FCConvert.ToDateTime(t2kTransactionDate.Text);
			}
			else
			{
				MessageBox.Show("You must enter a valid transaction date before continuing", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (boolReadOnly)
			{
				if (boolDataChanged)
				{
					if (MessageBox.Show("Data has changed.  You must save before continuing." + "\r\n" + "Do you wish to do save now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (!SaveOwner())
						{
							return;
						}
					}
					else
					{
						return;
					}
				}
			}
			dtDate = FCConvert.ToDateTime(Strings.Format(t2kTransactionDate.Text, "MM/dd/yyyy"));
			rsKennelInfo.OpenRecordset("SELECT * FROM KennelLicense WHERE ID = " + FCConvert.ToString(Conversion.Val(GridKennels.TextMatrix(GridKennels.MouseRow, COLKENNELLICID))), "TWCK0000.vb1");
			if (rsKennelInfo.EndOfFile() != true)
			{
				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsKennelInfo.Get_Fields("Year"))));
				if (!boolReadOnly)
				{
					if (Conversion.Val(rsKennelInfo.Get_Fields("Year")) != 0)
					{
						lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsKennelInfo.Get_Fields("Year"))));
					}
					else
					{
						lngYear = DateTime.Today.Year;
						object temp = lngYear;
						frmInput.InstancePtr.Init(ref temp, "Enter Kennel Registration Year", "Please enter the last year this kennel has been registered for.", 1000, false, modGlobalConstants.InputDTypes.idtWholeNumber, FCConvert.ToString(DateTime.Today.Year));
						lngYear = FCConvert.ToInt32(temp);
					}
				}
			}
			else
			{
				lngYear = 0;
			}
			boolReReg = false;
			if (!boolReadOnly)
			{
				if ((dtDate.Month >= 10 && lngYear < dtDate.Year + 1) || (dtDate.Month < 10 && lngYear < dtDate.Year))
				{
					if (MessageBox.Show("Open record as a re-registration?", "Re-Registration", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
						boolReReg = true;
					}
					else
					{
						boolReReg = false;
					}
				}
			}
			if (boolReReg)
			{
				if (dtDate.Month >= 10)
				{
					if (lngYear < dtDate.Year)
					{
						if (MessageBox.Show("Is this license for the current year (" + FCConvert.ToString(dtDate.Year) + ")? Answer no if it is for next year (" + FCConvert.ToString(dtDate.Year + 1) + ".", "Current Year?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							lngYear = dtDate.Year;
						}
						else
						{
							lngYear = dtDate.Year + 1;
						}
					}
					else
					{
						lngYear = dtDate.Year + 1;
					}
				}
				else
				{
					lngYear = dtDate.Year;
				}
			}
			if (frmNewKennel.InstancePtr.Init(ref lngOwnerNumber, FCConvert.ToInt32(Conversion.Val(GridKennels.TextMatrix(GridKennels.MouseRow, COLKENNELLICID))), boolReReg, lngYear) && !boolReadOnly)
			{
				if (!boolCreatedTransaction)
				{
					CreateTransaction();
					curTrans.Description = "Kennel";
				}
				else
				{
					curTrans.Description = "Multiple";
				}
				t2kTransactionDate.Enabled = false;
				curTrans.Amount1 += modGNBas.Statics.typTransaction.Amount1;
				curTrans.Amount2 += modGNBas.Statics.typTransaction.Amount2;
				curTrans.Amount3 += modGNBas.Statics.typTransaction.Amount3;
				curTrans.Amount4 += modGNBas.Statics.typTransaction.Amount4;
				curTrans.Amount5 += modGNBas.Statics.typTransaction.Amount5;
				curTrans.Amount6 += modGNBas.Statics.typTransaction.Amount6;
				curTrans.TotalAmount += modGNBas.Statics.typTransaction.TotalAmount;
				curTrans.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				modGNBas.Statics.typDogTransaction.TransactionNumber = curTrans.TransactionNumber;
				modGNBas.Statics.typDogTransaction.TownCode = curTrans.TownCode;
				SaveDogTransaction();
				UpdateCurTrans();
			}
			else
			{
			}
			GridDogs.Rows = 1;
			FillDogs();
			FillKennels();
		}

		private void gridList_DblClick(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (gridList.MouseRow < 1)
					return;
				framAddDog.Visible = false;
				if (lngOwnerNumber == 0)
				{
					MessageBox.Show("This owner record does not exist yet.  You must enter and save the owner before adding a dog.", "Save Owner", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(gridList.Tag)) == "DELETED")
				{
					modGlobalFunctions.AddCYAEntry("CK", "Added deleted dog to owner " + FCConvert.ToString(lngOwnerNumber), "Dognum = " + gridList.TextMatrix(gridList.MouseRow, COLLISTDOGNUM), "", "", "");
				}
				else
				{
					modGlobalFunctions.AddCYAEntry("CK", "Transferred dog to owner " + FCConvert.ToString(lngOwnerNumber), "Dognum = " + gridList.TextMatrix(gridList.MouseRow, COLLISTDOGNUM), "From owner " + gridList.TextMatrix(gridList.MouseRow, COLLISTPREVOWNERNUM), "", "");
				}
				clsSave.Execute("update doginfo set kenneldog = 0, ownernum = " + FCConvert.ToString(lngOwnerNumber) + " where ID = " + FCConvert.ToString(Conversion.Val(gridList.TextMatrix(gridList.MouseRow, COLLISTDOGNUM))), "twck0000.vb1");
				if (Conversion.Val(gridList.TextMatrix(gridList.MouseRow, COLLISTPREVOWNERNUM)) > 0)
				{
					clsSave.OpenRecordset("select * from dogowner where ID = " + FCConvert.ToString(Conversion.Val(gridList.TextMatrix(gridList.MouseRow, COLLISTPREVOWNERNUM))), "twck0000.vb1");
					// remove it from the owners list of dogs
					if (!clsSave.EndOfFile())
					{
						strTemp = FCConvert.ToString(clsSave.Get_Fields_String("dognumbers"));
						if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
						{
							strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
							strTemp = "";
							for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
							{
								if (Conversion.Val(strAry[x]) != Conversion.Val(gridList.TextMatrix(gridList.MouseRow, COLLISTDOGNUM)))
								{
									strTemp = strAry[x] + ",";
								}
							}
							// x
							if (strTemp != string.Empty)
							{
								// get rid of last comma
								strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
							}
							clsSave.Edit();
							clsSave.Set_Fields("dognumbers", strTemp);
							clsSave.Update();
						}
					}
					clsSave.OpenRecordset("select * from kennellicense where ownernum = " + FCConvert.ToString(Conversion.Val(gridList.TextMatrix(gridList.MouseRow, COLLISTPREVOWNERNUM))), "twck0000.vb1");
					// remove it from any kennel licenses
					while (!clsSave.EndOfFile())
					{
						strTemp = FCConvert.ToString(clsSave.Get_Fields_String("dognumbers"));
						if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
						{
							strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
							strTemp = "";
							for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
							{
								if (Conversion.Val(strAry[x]) != Conversion.Val(gridList.TextMatrix(gridList.MouseRow, COLLISTDOGNUM)))
								{
									strTemp += strAry[x] + ",";
								}
							}
							// x
							if (strTemp != string.Empty)
							{
								// get rid of last comma
								strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
							}
							clsSave.Edit();
							clsSave.Set_Fields("dognumbers", strTemp);
							clsSave.Update();
						}
						clsSave.MoveNext();
					}
				}
				GridDogs.Rows = 1;
				FillDogs();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In gridList_DblClick", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void gridTownCode_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void Image1_Click(object sender, System.EventArgs e)
		{
			mnuComments_Click();
		}

		private void mnuAddDeletedDog_Click(object sender, System.EventArgs e)
		{
			LoadGridList();
			gridList.Tag = (System.Object)("Deleted");
			framAddDog.Visible = true;
			framAddDog.BringToFront();
		}

		private void LoadGridList(bool boolDeletedList = true)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strprefix;
			strprefix = clsLoad.CurrentPrefix + ".dbo.";
			string strDogInfo;
			string strDogOwner;
			string strParty;
			strDogInfo = clsLoad.Get_GetFullDBName("Clerk") + ".dbo.DogInfo";
			strDogOwner = clsLoad.Get_GetFullDBName("Clerk") + ".dbo.dogowner";
			strParty = clsLoad.Get_GetFullDBName("CentralParties") + ".dbo.parties";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (boolDeletedList)
				{
					// Call clsLoad.OpenRecordset("select * from " & strPrefix & "DOGINFO left join dogowner on (dogowner.ID = doginfo.previousowner) where doginfo.ownernum < 1 order by dogname", "twck0000.vb1")
					clsLoad.OpenRecordset("select * from " + strDogInfo + " left join " + strDogOwner + " on (" + strDogOwner + ".ID = " + strDogInfo + ".previousowner) " + " LEFT join " + strParty + " on (" + strDogOwner + ".partyid = " + strParty + ".id) where " + strDogInfo + ".ownernum < 1 order by dogname", "twck0000.vb1");
				}
				else
				{
					// Call clsLoad.OpenRecordset("select * from DOGINFO left join dogowner on (dogowner.ID = doginfo.ownernum) where doginfo.ownernum > 0 order by dogname", "twck0000.vb1")
					clsLoad.OpenRecordset("select * from " + strDogInfo + " left join " + strDogOwner + " on (" + strDogOwner + ".ID = " + strDogInfo + ".ownernum) " + " LEFT join " + strParty + " on (" + strDogOwner + ".partyid = " + strParty + ".id) where " + strDogInfo + ".ownernum > 0 order by dogname", "twck0000.vb1");
				}
				gridList.Rows = 1;
				while (!clsLoad.EndOfFile())
				{
					gridList.Rows += 1;
					gridList.TextMatrix(gridList.Rows - 1, COLLISTDOGNUM, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					gridList.TextMatrix(gridList.Rows - 1, COLLISTDOGNAME, FCConvert.ToString(clsLoad.Get_Fields_String("dogname")));
					gridList.TextMatrix(gridList.Rows - 1, COLLISTDOGBREED, FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed")));
					gridList.TextMatrix(gridList.Rows - 1, COLLISTDOGCOLOR, FCConvert.ToString(clsLoad.Get_Fields_String("dogcolor")));
					gridList.TextMatrix(gridList.Rows - 1, COLLISTPREVOWNERLAST, FCConvert.ToString(clsLoad.Get_Fields_String("lastname")));
					gridList.TextMatrix(gridList.Rows - 1, COLLISTPREVOWNERFIRST, FCConvert.ToString(clsLoad.Get_Fields_String("firstname")));
					gridList.TextMatrix(gridList.Rows - 1, COLLISTPREVOWNERNUM, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("dogowner.ID"))));
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridList", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuAddDog_Click(object sender, System.EventArgs e)
		{
			string strDName = "";
			if (Information.IsDate(t2kTransactionDate.Text))
			{
				modGNBas.Statics.typTransaction.TransactionDate = FCConvert.ToDateTime(t2kTransactionDate.Text);
				curTrans.TransactionDate = FCConvert.ToDateTime(t2kTransactionDate.Text);
			}
			else
			{
				MessageBox.Show("You must enter a valid transaction date before continuing", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (lngOwnerNumber == 0)
			{
				mnuSave_Click();
				// MsgBox "This owner record does not exist yet.  You must save the owner before adding a dog.", vbExclamation, "Save Owner"
				// Exit Sub
			}
			if (lngOwnerNumber == 0)
			{
				MessageBox.Show("This owner record does not exist yet.  You must enter and save the owner before adding a dog.", "Save Owner", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Convert.ToBoolean(frmNewDog.InstancePtr.Init(0, lngOwnerNumber, false, false, 0, !boolCreatedTransaction, strDName)))
			{
				if (!boolCreatedTransaction)
				{
					CreateTransaction();
					curTrans.Description = strDName;
					curTrans.TransactionDate = modGNBas.Statics.typTransaction.TransactionDate;
					CurKennelTrans.TransactionDate = curTrans.TransactionDate;
					curTrans.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
					CurKennelTrans.TownCode = curTrans.TownCode;
					t2kTransactionDate.Text = Strings.Format(curTrans.TransactionDate, "MM/dd/yyyy");
					t2kTransactionDate.Enabled = false;
				}
				else
				{
					curTrans.Description = "Multiple";
				}
				curTrans.Amount1 += modGNBas.Statics.typTransaction.Amount1;
				curTrans.Amount2 += modGNBas.Statics.typTransaction.Amount2;
				curTrans.Amount3 += modGNBas.Statics.typTransaction.Amount3;
				curTrans.Amount4 += modGNBas.Statics.typTransaction.Amount4;
				curTrans.Amount5 += modGNBas.Statics.typTransaction.Amount5;
				curTrans.Amount6 += modGNBas.Statics.typTransaction.Amount6;
				curTrans.TotalAmount += modGNBas.Statics.typTransaction.TotalAmount;
				curTrans.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				modGNBas.Statics.typDogTransaction.TownCode = curTrans.TownCode;
				modGNBas.Statics.typDogTransaction.TransactionNumber = curTrans.TransactionNumber;
				SaveDogTransaction();
				UpdateCurTrans();
			}
			GridDogs.Rows = 1;
			FillDogs();
		}

		private void mnuAddKennel_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
			DateTime dtDate;
			int lngYear = 0;
			if (Information.IsDate(t2kTransactionDate.Text))
			{
				modGNBas.Statics.typTransaction.TransactionDate = FCConvert.ToDateTime(t2kTransactionDate.Text);
				curTrans.TransactionDate = FCConvert.ToDateTime(t2kTransactionDate.Text);
			}
			else
			{
				MessageBox.Show("You must enter a valid transaction date before continuing", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (lngOwnerNumber == 0)
			{
				mnuSave_Click();
			}
			if (lngOwnerNumber == 0)
			{
				MessageBox.Show("This owner record does not exist yet.  You must enter and save the owner before adding a kennel license.", "Save Owner", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			dtDate = FCConvert.ToDateTime(Strings.Format(t2kTransactionDate.Text, "MM/dd/yyyy"));
			if (dtDate.Month >= 10)
			{
				if (MessageBox.Show("Is this license for the current year (" + FCConvert.ToString(dtDate.Year) + ")? Answer no if it is for next year (" + FCConvert.ToString(dtDate.Year + 1) + ".", "Current Year?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					lngYear = dtDate.Year;
				}
				else
				{
					lngYear = dtDate.Year + 1;
				}
			}
			else
			{
				lngYear = dtDate.Year;
			}
			if (frmNewKennel.InstancePtr.Init(ref lngOwnerNumber, 0, false, lngYear))
			{
				if (!boolCreatedTransaction)
				{
					CreateTransaction();
					curTrans.Description = "Kennel";
				}
				else
				{
					curTrans.Description = "Multiple";
				}
				t2kTransactionDate.Enabled = false;
				curTrans.Amount1 += modGNBas.Statics.typTransaction.Amount1;
				curTrans.Amount2 += modGNBas.Statics.typTransaction.Amount2;
				curTrans.Amount3 += modGNBas.Statics.typTransaction.Amount3;
				curTrans.Amount4 += modGNBas.Statics.typTransaction.Amount4;
				curTrans.Amount5 += modGNBas.Statics.typTransaction.Amount5;
				curTrans.Amount6 += modGNBas.Statics.typTransaction.Amount6;
				curTrans.TotalAmount += modGNBas.Statics.typTransaction.TotalAmount;
				curTrans.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				modGNBas.Statics.typDogTransaction.TransactionNumber = curTrans.TransactionNumber;
				modGNBas.Statics.typDogTransaction.TownCode = curTrans.TownCode;
				SaveDogTransaction();
				UpdateCurTrans();
			}
			else
			{
			}
			GridDogs.Rows = 1;
			FillDogs();
			FillKennels();
		}

		private void mnuComments_Click(object sender, System.EventArgs e)
		{
			if (lngOwnerNumber == 0)
			{
				MessageBox.Show("This owner does not exist yet.  You must save the information before you can tie comments to this record.", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (frmCommentOld.InstancePtr.Init("CK", "DogOwner", "Comments", "ID", lngOwnerNumber, boolNonEditable: false, boolModal: true))
			{
				Image1.Visible = false;
			}
			else
			{
				Image1.Visible = true;
			}
		}

		public void mnuComments_Click()
		{
			mnuComments_Click(mnuComments, new System.EventArgs());
		}

		private void mnuDeleteOwner_Click(object sender, System.EventArgs e)
		{
			bool boolReturnStickers = false;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngYear = 0;
			int lngStick = 0;
            string strTag = "";

			if (GridKennels.Rows > 1)
			{
				MessageBox.Show("Owner has at least one kennel license. You must delete this first to delete the owner.", "Kennel License", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (GridDogs.Rows > 1)
			{
				if (MessageBox.Show("This will delete the dogs as well. Do you want to continue?", "Delete Dogs?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					return;
				}
				if (MessageBox.Show("Return sticker(s) and tag(s) to inventory?", "Return Stickers and Tags?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					boolReturnStickers = true;
				}
				else
				{
					boolReturnStickers = false;
				}
				// 
				if (boolReturnStickers)
				{
					for (x = 1; x <= (GridDogs.Rows - 1); x++)
					{
						clsSave.OpenRecordset("select [year],stickerlicnum from doginfo where ID = " + FCConvert.ToString(Conversion.Val(GridDogs.TextMatrix(x, COLDOGNUM))), "twck0000.vb1");
						if (!clsSave.EndOfFile())
						{
							lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields("[year]"))));
							lngStick = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields_String("Stickerlicnum"))));
							clsSave.Execute("update doginventory set status = 'Active' where type = 'S' and [year] = " + FCConvert.ToString(lngYear) + " and convert(int, isnull(stickernumber, 0)) = " + FCConvert.ToString(lngStick), "twck0000.vb1");
						}
						clsSave.OpenRecordset("select [year],isdangerousdog,taglicnum from doginfo where ID = " + FCConvert.ToString(Conversion.Val(GridDogs.TextMatrix(x, COLDOGNUM))), "twck0000.vb1");
						if (!clsSave.EndOfFile())
						{
							lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields("[year]"))));
                            strTag = clsSave.Get_Fields_String("taglicnum");
                            if (clsSave.Get_Fields_Boolean("IsDangerousDog"))
                            {
                                clsSave.Execute(
                                    "update doginventory set status = 'Active' where [type] = 'D' and stickernumber = '" +
                                    strTag + "'", "Clerk");
                            }
                            else
                            {
                                clsSave.Execute(
                                    "update doginventory set status = 'Active' where [type] = 'T' and stickernumber = '" +
                                    strTag + "'", "Clerk");
                            }
                        }
					}
					// x
				}
				GridDogs.Rows = 1;
				clsSave.Execute("update doginfo set ownernum = -1,previousowner = " + FCConvert.ToString(lngOwnerNumber) + " where ownernum = " + FCConvert.ToString(lngOwnerNumber), "twck0000.vb1");
			}
			clsSave.Execute("update dogowner set deleted = 1 where id = " + FCConvert.ToString(lngOwnerNumber), "twck0000.vb1");
			modGlobalFunctions.AddCYAEntry("CK", "Deleted dogowner " + FCConvert.ToString(lngOwnerNumber), "", "", "", "");
			boolCreatedTransaction = false;
			mnuExit_Click();
		}

		private void mnuDogHistory_Click(object sender, System.EventArgs e)
		{
			rptDogHistory.InstancePtr.Init(1, lngOwnerNumber, 0, false, true);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
			if (modCashReceiptingData.Statics.bolFromWindowsCR && boolCreatedTransaction)
			{
				Application.Exit();
			}
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void TransferToCR()
		{
			modCashReceiptingData.Statics.typeCRData.ID = modClerkGeneral.Statics.dInfo.DogNumber;
			modCashReceiptingData.Statics.typeCRData.Type = "DOG";
			modCashReceiptingData.Statics.typeCRData.Name = pInfo.FullNameLastFirst;
			modCashReceiptingData.Statics.typeCRData.PartyID = pInfo.ID;
			// .Reference = Left(dInfo.DOGNAME, 10)
			modCashReceiptingData.Statics.typeCRData.Reference = Strings.Left(curTrans.Description, 10);
			// .Control1 = Left(dInfo.TagLicNum, 10)
			modCashReceiptingData.Statics.typeCRData.Control1 = curTrans.TransactionNumber.ToString();
			modCashReceiptingData.Statics.typeCRData.Control2 = " ";
			modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(curTrans.Amount1, "000000.00"));
			// vital
			modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(curTrans.Amount2, "000000.00"));
			// town
			modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(curTrans.Amount3, "000000.00"));
			// state
			modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(curTrans.Amount4, "000000.00"));
			// clerk
			modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(curTrans.Amount5, "000000.00"));
			// late
			modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(curTrans.Amount6, "000000.00"));
			// kennel
			if (boolCreatedTransaction)
			{
				modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
			}
			else
			{
				modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "N";
			}
			modCashReceiptingData.Statics.typeCRData.TownCode = curTrans.TownCode;
			modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
			// End
		}

		private void SaveCurTrans()
		{
			// save the current transaction
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsSave.OpenRecordset("select * from transactiontable where ID = " + FCConvert.ToString(curTrans.TransactionNumber), "twck0000.vb1");
				clsSave.Edit();
				clsSave.Set_Fields("amount1", curTrans.Amount1);
				clsSave.Set_Fields("amount2", curTrans.Amount2);
				clsSave.Set_Fields("amount3", curTrans.Amount3);
				clsSave.Set_Fields("amount4", curTrans.Amount4);
				clsSave.Set_Fields("amount5", curTrans.Amount5);
				clsSave.Set_Fields("amount6", curTrans.Amount6);
				clsSave.Set_Fields("description", curTrans.Description);
				clsSave.Set_Fields("totalamount", curTrans.TotalAmount);
				clsSave.Set_Fields("transactiondate", curTrans.TransactionDate);
				clsSave.Set_Fields("transactiontype", curTrans.TransactionType);
				clsSave.Set_Fields("userid", curTrans.UserID);
				clsSave.Set_Fields("username", curTrans.UserName);
				clsSave.Set_Fields("towncode", curTrans.TownCode);
				clsSave.Update();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveCurTrans", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveOwner();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveOwner())
			{
				mnuExit_Click();
			}
		}

		private bool SaveOwner()
		{
			bool SaveOwner = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp = "";
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveOwner = false;
				int tLong;
				tLong = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(txtCustomerNumber.Text))));
				pInfo = pCont.GetParty(tLong);
				if (!VerifyOwner())
				{
					return SaveOwner;
				}
				if (gridTownCode.Visible == true)
				{
					gridTownCode.Row = -1;
				}
				clsSave.OpenRecordset("select * from dogowner where ID = " + FCConvert.ToString(lngOwnerNumber), "twck0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.Set_DefaultValue("PartyID", 0);
					clsSave.AddNew();
				}
				if (chkNotRequired.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("NoRequiredFields", true);
				}
				else
				{
					clsSave.Set_Fields("NoRequiredFields", false);
				}
				clsSave.Set_Fields("TownCode", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				curTrans.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				clsSave.Set_Fields("PartyID", fecherFoundation.Strings.Trim(txtCustomerNumber.Text));
				if (!(pInfo == null))
				{
					curTrans.TransactionPerson = pInfo.FullName;
				}
				else
				{
					curTrans.TransactionPerson = "";
				}
				clsSave.Set_Fields("locationnum", fecherFoundation.Strings.Trim(txtStreetNumber.Text));
				clsSave.Set_Fields("locationstr", fecherFoundation.Strings.Trim(txtStreet.Text));
				clsSave.Set_Fields("lastupdated", DateTime.Now);
				clsSave.Update();
				lngOwnerNumber = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				clsSave.Set_Fields("dognumbers", modGlobalRoutines.GetListOfDogsByOwner(ref lngOwnerNumber));
				clsSave.Update();
				boolDataChanged = false;
				SaveOwner = true;
				MessageBox.Show("Save successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveOwner;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveOwner", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveOwner;
		}

		private bool VerifyOwner()
		{
			bool VerifyOwner = false;
			VerifyOwner = false;
			if (chkNotRequired.CheckState != Wisej.Web.CheckState.Checked)
			{
				if (!(pInfo == null))
				{
					// Set pAdd = pInfo.GetAddress("AR", lngCurrentPartyID)
					// If pInfo.FirstName = vbNullString Then
					// MsgBox "You must enter a first name", vbExclamation, "Missing Name"
					// Exit Function
					// End If
					// If pInfo.LastName = vbNullString Then
					// MsgBox "You must enter a last name", vbExclamation, "Missing Name"
					// Exit Function
					// End If
					// If Not pAdd Is Nothing Then
					// If pAdd.Address1 = vbNullString Then
					// MsgBox "You must enter a mailing address", vbExclamation, "Missing Address"
					// Exit Function
					// End If
					// If pAdd.City = vbNullString Then
					// MsgBox "You must enter a valid city", vbExclamation, "Missing City"
					// Exit Function
					// End If
					// If pAdd.State = vbNullString Then
					// MsgBox "You must select a state", vbExclamation, "Missing State"
					// Exit Function
					// End If
					// If pAdd.Zip = vbNullString Then
					// MsgBox "You must enter a zip code", vbExclamation, "Missing Zip"
					// Exit Function
					// End If
					// Else
					// MsgBox "Your party must have a valid address", vbExclamation, "Missing Address"
					// Exit Function
					// End If
				}
				else
				{
					MessageBox.Show("You must enter a valid party", "Missing Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return VerifyOwner;
				}
			}
			VerifyOwner = true;
			return VerifyOwner;
		}

		private void mnuTransferDog_Click(object sender, System.EventArgs e)
		{
			framAddDog.Visible = true;
			framAddDog.BringToFront();
			//App.DoEvents();
			LoadGridList(false);
			gridList.Tag = (System.Object)("Transfer");
		}

		private void txtCustomerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCustomerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtCustomerNumber.Text))
			{
				if (FCConvert.ToInt32(FCConvert.ToDouble(txtCustomerNumber.Text)) > 0)
				{
					GetPartyInfo(FCConvert.ToInt32(FCConvert.ToDouble(txtCustomerNumber.Text)));
				}
				else
				{
					ClearPartyInfo();
				}
			}
			else
			{
				ClearPartyInfo();
			}
		}

		public void ClearPartyInfo()
		{
			lngCurrentPartyID = 0;
			lblCustomerInfo.Text = "";
			Image1.Visible = false;
			cmdEdit.Enabled = false;
		}

		public void GetPartyInfo(int intPartyID)
		{
			FCCollection pComments = new FCCollection();
			pInfo = pCont.GetParty(intPartyID);
			if (!(pInfo == null))
			{
				lngCurrentPartyID = pInfo.ID;
				lblCustomerInfo.Text = pInfo.FullName;
				pAdd = pInfo.GetAddress("CK", intPartyID);
				// kk01062015 trouts-119    '.GetAddress("AR", intPartyID)
				if (pAdd == null)
				{
					lblCustomerInfo.Text = lblCustomerInfo.Text;
				}
				else
				{
					lblCustomerInfo.Text = lblCustomerInfo.Text + "\r\n" + pAdd.GetFormattedAddress();
				}
				// Set pComments = pInfo.GetModuleSpecificComments("CK")     'kk01062015 trouts-119   '.GetModuleSpecificComments("AR")
				// If pComments.Count > 0 Then
				// Image1.Visible = True
				// Else
				// Image1.Visible = False
				// End If
				if (!boolReadOnly)
				{
					if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(modCentralParties.CNSTCentralPartiesEdit, "CP") == "F")
					{
						cmdEdit.Enabled = true;
					}
				}
				curTrans.TransactionPerson = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(pInfo.FullName));
			}
		}

		private void cmdEdit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtCustomerNumber.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID);
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID);
			}
		}

		private void CheckPermissions()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTEDITOWNERSSCREEN)) != "F")
				{
					boolReadOnly = true;
					cmdSave.Enabled = false;
					chkNotRequired.Enabled = false;
					mnuAddDeletedDog.Enabled = false;
					cmdAddNewDog.Enabled = false;
					cmdAddNewKennel.Enabled = false;
					mnuDeleteOwner.Enabled = false;
					mnuTransferDog.Enabled = false;
					txtCustomerNumber.Enabled = false;
					cmdEdit.Enabled = false;
					cmdSearch.Enabled = false;
					t2kTransactionDate.Enabled = false;
					txtStreet.Enabled = false;
					txtStreetNumber.Enabled = false;
				}
				else
				{
					boolReadOnly = false;
					cmdSave.Enabled = true;
					mnuAddDeletedDog.Enabled = true;
					cmdAddNewDog.Enabled = true;
					cmdAddNewKennel.Enabled = true;
					mnuDeleteOwner.Enabled = true;
					mnuTransferDog.Enabled = true;
					txtCustomerNumber.Enabled = true;
					cmdEdit.Enabled = true;
					cmdSearch.Enabled = true;
					t2kTransactionDate.Enabled = true;
					txtStreet.Enabled = true;
					txtStreetNumber.Enabled = true;
				}
				if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(modCentralParties.CNSTCentralPartiesEdit, "CP") != "F")
				{
					cmdEdit.Enabled = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckPermissions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
