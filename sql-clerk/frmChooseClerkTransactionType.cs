﻿using System;
using System.Linq;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.Clerk;
using Wisej.Web;

namespace TWCK0000
{
    public partial class frmChooseClerkTransactionType : BaseForm, IView<IChooseClerkTransactionTypeViewModel>
    {
        private bool cancelling = true;
        public frmChooseClerkTransactionType()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmChooseClerkTransactionType(IChooseClerkTransactionTypeViewModel viewModel) : this()
        {
            modCashReceiptingData.Statics.bolFromWindowsCR = true;
            this.ViewModel = viewModel;
            AddChoices();
        }

        private void InitializeComponentEx()
        {
            this.Closing += FrmChooseClerkTransactionType_Closing;
        }

        private void FrmChooseClerkTransactionType_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }


        public IChooseClerkTransactionTypeViewModel ViewModel { get; set; }


        private void AddChoices()
        { 
            var tranTypes = ViewModel.GetAvailableTransactionTypes();
           
            foreach (var pair in tranTypes)
            {
	            var button = new FCButton() { Text = pair.Description, Tag = pair.ID, AppearanceKey = "actionButton", Height = 40 };
	            button.Click += Button_Click;
	            flexLayoutPanel1.Controls.Add(button);
            }
        }

        private void Button_Click(object sender, EventArgs e)
        {
            ViewModel.StartTransaction((ClerkTransactionType)((FCButton) sender).Tag);
            CloseWithoutCancel();
        }

		private void cmdCancel_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
