//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDeathSearchA.
	/// </summary>
	partial class frmDeathSearchA
	{
		public fecherFoundation.FCComboBox cmbDateofDeath;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> Text2;
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdSch;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid Grid;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkBirthOnFile;
		public fecherFoundation.FCCheckBox chkMarriageOnFile;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCTextBox Text2_1;
		public fecherFoundation.FCTextBox Text2_0;
		public fecherFoundation.FCTextBox Text2_4;
		public fecherFoundation.FCButton cmdSch_1;
		public fecherFoundation.FCButton cmdSch_3;
		public fecherFoundation.FCButton cmdSch_4;
		public Wisej.Web.MaskedTextBox meBox1_0;
		public Wisej.Web.MaskedTextBox meBox1_1;
		public fecherFoundation.FCButton cmdSch_2;
		public fecherFoundation.FCLabel Label2_4;
		public fecherFoundation.FCLabel Label2_5;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbDateofDeath = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.Grid = new fecherFoundation.FCGrid();
			this.cmdSch_1 = new fecherFoundation.FCButton();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.chkBirthOnFile = new fecherFoundation.FCCheckBox();
			this.chkMarriageOnFile = new fecherFoundation.FCCheckBox();
			this.Text2_1 = new fecherFoundation.FCTextBox();
			this.cmdSch_3 = new fecherFoundation.FCButton();
			this.Text2_0 = new fecherFoundation.FCTextBox();
			this.cmdSch_4 = new fecherFoundation.FCButton();
			this.meBox1_0 = new Wisej.Web.MaskedTextBox();
			this.meBox1_1 = new Wisej.Web.MaskedTextBox();
			this.Label2_4 = new fecherFoundation.FCLabel();
			this.Label2_5 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Text2_4 = new fecherFoundation.FCTextBox();
			this.cmdSch_2 = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBirthOnFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSch_4);
			this.BottomPanel.Location = new System.Drawing.Point(0, 573);
			this.BottomPanel.Size = new System.Drawing.Size(737, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmdSch_2);
			this.ClientArea.Size = new System.Drawing.Size(737, 513);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Size = new System.Drawing.Size(737, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(261, 30);
			this.HeaderText.Text = "Search Death Records";
			// 
			// cmbDateofDeath
			// 
			this.cmbDateofDeath.Items.AddRange(new object[] {
            "Exact Date of Death or",
            "Between Dates",
            "Disable"});
			this.cmbDateofDeath.Location = new System.Drawing.Point(20, 231);
			this.cmbDateofDeath.Name = "cmbDateofDeath";
			this.cmbDateofDeath.Size = new System.Drawing.Size(232, 40);
			this.cmbDateofDeath.TabIndex = 24;
			this.cmbDateofDeath.Text = "Disable";
			this.cmbDateofDeath.SelectedIndexChanged += new System.EventHandler(this.optDateofDeath_CheckedChanged);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cmdPrint);
			this.Frame1.Controls.Add(this.Grid);
			this.Frame1.Controls.Add(this.cmbDateofDeath);
			this.Frame1.Controls.Add(this.cmdSch_1);
			this.Frame1.Controls.Add(this.Frame2);
			this.Frame1.Controls.Add(this.Text2_1);
			this.Frame1.Controls.Add(this.cmdSch_3);
			this.Frame1.Controls.Add(this.Text2_0);
			this.Frame1.Controls.Add(this.meBox1_0);
			this.Frame1.Controls.Add(this.meBox1_1);
			this.Frame1.Controls.Add(this.Label2_4);
			this.Frame1.Controls.Add(this.Label2_5);
			this.Frame1.Controls.Add(this.Label2_1);
			this.Frame1.Controls.Add(this.Label2_0);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Controls.Add(this.Text2_4);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(679, 475);
			this.Frame1.TabIndex = 12;
			this.Frame1.Text = "Death Record Search:";
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "actionButton";
			this.cmdPrint.Enabled = false;
			this.cmdPrint.Location = new System.Drawing.Point(234, 415);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(77, 40);
			this.cmdPrint.TabIndex = 19;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// Grid
			// 
			this.Grid.Cols = 4;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.Location = new System.Drawing.Point(20, 291);
			this.Grid.Name = "Grid";
			this.Grid.RowHeadersVisible = false;
			this.Grid.Rows = 1;
			this.Grid.Size = new System.Drawing.Size(639, 104);
			this.Grid.TabIndex = 23;
			this.Grid.Click += new System.EventHandler(this.Grid_ClickEvent);
			// 
			// cmdSch_1
			// 
			this.cmdSch_1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSch_1.AppearanceKey = "actionButton";
			this.cmdSch_1.DialogResult = Wisej.Web.DialogResult.OK;
			this.cmdSch_1.Location = new System.Drawing.Point(124, 415);
			this.cmdSch_1.Name = "cmdSch_1";
			this.cmdSch_1.Size = new System.Drawing.Size(98, 40);
			this.cmdSch_1.TabIndex = 7;
			this.cmdSch_1.Text = "Search ";
			this.cmdSch_1.Click += new System.EventHandler(this.cmdSch_Click);
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxNoBorders";
			this.Frame2.Controls.Add(this.chkBirthOnFile);
			this.Frame2.Controls.Add(this.chkMarriageOnFile);
			this.Frame2.Enabled = false;
			this.Frame2.Location = new System.Drawing.Point(20, 184);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(391, 27);
			this.Frame2.TabIndex = 20;
			// 
			// chkBirthOnFile
			// 
			this.chkBirthOnFile.Location = new System.Drawing.Point(211, 0);
			this.chkBirthOnFile.Name = "chkBirthOnFile";
			this.chkBirthOnFile.Size = new System.Drawing.Size(134, 23);
			this.chkBirthOnFile.TabIndex = 22;
			this.chkBirthOnFile.Text = "Birth record on file";
			// 
			// chkMarriageOnFile
			// 
			this.chkMarriageOnFile.Name = "chkMarriageOnFile";
			this.chkMarriageOnFile.Size = new System.Drawing.Size(160, 23);
			this.chkMarriageOnFile.TabIndex = 21;
			this.chkMarriageOnFile.Text = "Marriage record on file";
			// 
			// Text2_1
			// 
			this.Text2_1.BackColor = System.Drawing.SystemColors.Window;
			this.Text2_1.Location = new System.Drawing.Point(195, 120);
			this.Text2_1.Name = "Text2_1";
			this.Text2_1.Size = new System.Drawing.Size(464, 40);
			this.Text2_1.TabIndex = 1;
			this.Text2_1.Enter += new System.EventHandler(this.Text2_Enter);
			// 
			// cmdSch_3
			// 
			this.cmdSch_3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSch_3.AppearanceKey = "actionButton";
			this.cmdSch_3.Location = new System.Drawing.Point(20, 415);
			this.cmdSch_3.Name = "cmdSch_3";
			this.cmdSch_3.Size = new System.Drawing.Size(84, 40);
			this.cmdSch_3.TabIndex = 10;
			this.cmdSch_3.Text = "Clear";
			this.cmdSch_3.Click += new System.EventHandler(this.cmdSch_Click);
			// 
			// Text2_0
			// 
			this.Text2_0.BackColor = System.Drawing.SystemColors.Window;
			this.Text2_0.Location = new System.Drawing.Point(195, 60);
			this.Text2_0.Name = "Text2_0";
			this.Text2_0.Size = new System.Drawing.Size(464, 40);
			this.Text2_0.TabIndex = 25;
			this.Text2_0.Enter += new System.EventHandler(this.Text2_Enter);
			// 
			// cmdSch_4
			// 
			this.cmdSch_4.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSch_4.AppearanceKey = "acceptButton";
			this.cmdSch_4.Location = new System.Drawing.Point(295, 37);
			this.cmdSch_4.Name = "cmdSch_4";
			this.cmdSch_4.Size = new System.Drawing.Size(146, 40);
			this.cmdSch_4.TabIndex = 8;
			this.cmdSch_4.Text = "Show Record";
			this.cmdSch_4.Click += new System.EventHandler(this.cmdSch_Click);
			// 
			// meBox1_0
			// 
			this.meBox1_0.Enabled = false;
			this.meBox1_0.Location = new System.Drawing.Point(272, 231);
			this.meBox1_0.Mask = "##/##/####";
			this.meBox1_0.MaxLength = 8;
			this.meBox1_0.Name = "meBox1_0";
			this.meBox1_0.Size = new System.Drawing.Size(160, 22);
			this.meBox1_0.TabIndex = 5;
			// 
			// meBox1_1
			// 
			this.meBox1_1.Enabled = false;
			this.meBox1_1.Location = new System.Drawing.Point(499, 231);
			this.meBox1_1.Mask = "##/##/####";
			this.meBox1_1.MaxLength = 8;
			this.meBox1_1.Name = "meBox1_1";
			this.meBox1_1.Size = new System.Drawing.Size(160, 22);
			this.meBox1_1.TabIndex = 6;
			// 
			// Label2_4
			// 
			this.Label2_4.Location = new System.Drawing.Point(284, 30);
			this.Label2_4.Name = "Label2_4";
			this.Label2_4.Size = new System.Drawing.Size(167, 15);
			this.Label2_4.TabIndex = 18;
			this.Label2_4.Text = "FIRST NAME (OPTIONAL)";
			// 
			// Label2_5
			// 
			this.Label2_5.Location = new System.Drawing.Point(195, 30);
			this.Label2_5.Name = "Label2_5";
			this.Label2_5.Size = new System.Drawing.Size(69, 15);
			this.Label2_5.TabIndex = 17;
			this.Label2_5.Text = "LAST NAME";
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(20, 134);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(122, 15);
			this.Label2_1.TabIndex = 16;
			this.Label2_1.Text = "PLACE OF DEATH";
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(20, 74);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(136, 15);
			this.Label2_0.TabIndex = 15;
			this.Label2_0.Text = "DECEASED\'S NAME";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(452, 245);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(27, 15);
			this.Label1.TabIndex = 14;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Text2_4
			// 
			this.Text2_4.BackColor = System.Drawing.SystemColors.Window;
			this.Text2_4.Location = new System.Drawing.Point(20, 352);
			this.Text2_4.Name = "Text2_4";
			this.Text2_4.Size = new System.Drawing.Size(213, 39);
			this.Text2_4.TabIndex = 13;
			this.Text2_4.Enter += new System.EventHandler(this.Text2_Enter);
			// 
			// cmdSch_2
			// 
			this.cmdSch_2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSch_2.AppearanceKey = "actionButton";
			this.cmdSch_2.Location = new System.Drawing.Point(292, 405);
			this.cmdSch_2.Name = "cmdSch_2";
			this.cmdSch_2.Size = new System.Drawing.Size(92, 40);
			this.cmdSch_2.TabIndex = 9;
			this.cmdSch_2.Text = "Long Search";
			this.cmdSch_2.Visible = false;
			this.cmdSch_2.Click += new System.EventHandler(this.cmdSch_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.Location = new System.Drawing.Point(662, 31);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(47, 24);
			this.cmdNew.TabIndex = 6;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// frmDeathSearchA
			// 
			this.AcceptButton = this.cmdSch_1;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(737, 681);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmDeathSearchA";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Search Death Records";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmDeathSearchA_Load);
			this.Activated += new System.EventHandler(this.frmDeathSearchA_Activated);
			this.Resize += new System.EventHandler(this.frmDeathSearchA_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDeathSearchA_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDeathSearchA_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBirthOnFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdNew;
	}
}
