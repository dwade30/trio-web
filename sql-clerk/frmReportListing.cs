﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	public partial class frmReportListing : BaseForm
	{
		public frmReportListing()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.txtYear.AllowOnlyNumericInput();
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmReportListing InstancePtr
		{
			get
			{
				return (frmReportListing)Sys.GetInstance(typeof(frmReportListing));
			}
		}

		protected frmReportListing _InstancePtr = null;
		//=========================================================
		const int REGISTRATION = 0;
		const int WARRANT = 1;
		const int OWNERNAME = 0;
		const int ADDRESS = 1;
		const int LOCATION = 2;
		const int DOGNAME = 3;
		const int TAGNUMBER = 4;
		const int COLOR = 5;
		const int BREED = 6;
		const int RABIESDATE = 7;
		const int YEARS = 8;
		// vbPorter upgrade warning: lngColor As int	OnWrite(Color)
		int lngColor;

		private void cmdExit_Click()
		{
			Close();
		}

		private void cmdPrint_Click()
		{
			bool boolUnregistered = false;
			bool boolIssueWarrant = false;
			GetSortField();
			GetWhereClause();
			if (cmbReport.Text == "Registration Listing")
			{
				// rptRegistrationListing.Show 
				rptRegistrationListing.InstancePtr.Init(txtYear1.Text, txtYear2.Text, this.Modal);
			}
			else if (cmbReport.Text == "Warrant Listing")
			{
				if (fecherFoundation.Strings.Trim(txtYear.Text) == string.Empty)
				{
					MessageBox.Show("You must enter year to report on.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtYear.Focus();
					return;
				}
				else
				{
					modGNBas.Statics.gintYear = FCConvert.ToInt32(fecherFoundation.Strings.Trim(txtYear.Text));
					// rptWarrentListing.Show 1
					if (chkUnregistered.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolUnregistered = true;
					}
					else
					{
						boolUnregistered = false;
					}
					if (chkServeWarrant.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolIssueWarrant = true;
					}
					else
					{
						boolIssueWarrant = false;
					}
					rptWarrentListing.InstancePtr.Init(this.Modal, boolUnregistered, boolIssueWarrant);
					// 
					// If MsgBox("Update dog records for printed warrants?", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
					// Call UpdateDogRecordsForWarrants
					// End If
				}
			}
		}

		public void GetWhereClause()
		{
			modGNBas.Statics.gboolConversion = false;
			modGNBas.Statics.gstrWhereClause = " AND ";
			if (fecherFoundation.Strings.Trim(txtLastName.Text) != string.Empty)
			{
				modGNBas.Statics.gstrWhereClause += "LastName LIKE '" + fecherFoundation.Strings.Trim(txtLastName.Text) + "%' AND ";
				// kk04172015   DogOwner.LastName   and   LIKE '...*'
			}
			if (fecherFoundation.Strings.Trim(txtAddress.Text) != string.Empty)
			{
				modGNBas.Statics.gstrWhereClause += "Address1 LIKE '%" + fecherFoundation.Strings.Trim(txtAddress.Text) + "%' AND ";
				// DogOwner.Address
			}
			if (fecherFoundation.Strings.Trim(txtLocation.Text) != string.Empty)
			{
				modGNBas.Statics.gstrWhereClause += "DogOwner.LocationSTR LIKE '" + fecherFoundation.Strings.Trim(txtLocation.Text) + "%' AND ";
			}
			if (fecherFoundation.Strings.Trim(txtDogName.Text) != string.Empty)
			{
				modGNBas.Statics.gstrWhereClause += "DogInfo.DogName LIKE '" + fecherFoundation.Strings.Trim(txtDogName.Text) + "%' AND ";
			}
			if (fecherFoundation.Strings.Trim(txtTagNumber1.Text) != string.Empty && fecherFoundation.Strings.Trim(txtTagNumber2.Text) != string.Empty)
			{
				modGNBas.Statics.gstrWhereClause += "val(DogInfo.TagLicNum) >= " + fecherFoundation.Strings.Trim(txtTagNumber1.Text) + " and val(DogInfo.TagLicNum) <= " + fecherFoundation.Strings.Trim(txtTagNumber2.Text) + " AND ";
			}
			if (fecherFoundation.Strings.Trim(txtColor.Text) != string.Empty)
			{
				modGNBas.Statics.gstrWhereClause += "dogcolor = '" + fecherFoundation.Strings.Trim(txtColor.Text) + "' AND ";
			}
			if (fecherFoundation.Strings.Trim(txtBreed.Text) != string.Empty)
			{
				modGNBas.Statics.gstrWhereClause += "dogbreed = '" + fecherFoundation.Strings.Trim(txtBreed.Text) + "' AND ";
			}
			if (fecherFoundation.Strings.Trim(txtRabiesDate1.Text) != string.Empty && fecherFoundation.Strings.Trim(txtRabiesDate2.Text) != string.Empty)
			{
				if (Information.IsDate(fecherFoundation.Strings.Trim(txtRabiesDate1.Text)) && Information.IsDate(fecherFoundation.Strings.Trim(txtRabiesDate2.Text)))
				{
					modGNBas.Statics.gstrWhereClause += "DogInfo.RabiesExpDate >=  '" + fecherFoundation.Strings.Trim(txtRabiesDate1.Text) + "' AND DogInfo.RabiesExpDate <= '" + fecherFoundation.Strings.Trim(txtRabiesDate2.Text) + "' AND ";
				}
			}
			if (fecherFoundation.Strings.Trim(txtYear1.Text) != string.Empty && fecherFoundation.Strings.Trim(txtYear2.Text) != string.Empty)
			{
				modGNBas.Statics.gstrWhereClause += "DogInfo.Year >= " + fecherFoundation.Strings.Trim(txtYear1.Text) + " and DogInfo.Year <= " + fecherFoundation.Strings.Trim(txtYear2.Text) + " AND ";
			}
			if (Strings.Right(modGNBas.Statics.gstrWhereClause, 5) == " AND ")
			{
				modGNBas.Statics.gstrWhereClause = Strings.Mid(modGNBas.Statics.gstrWhereClause, 1, modGNBas.Statics.gstrWhereClause.Length - 5);
			}
			if (modGNBas.Statics.gstrWhereClause == " AND ")
			{
				modGNBas.Statics.gstrWhereClause = string.Empty;
			}
			if (modGNBas.Statics.gstrWhereClause != string.Empty)
			{
				if (cmbReport.Text == "Registration Listing")
				{
					modGNBas.Statics.gstrWhereClause = "Where " + Strings.Mid(modGNBas.Statics.gstrWhereClause, 5, modGNBas.Statics.gstrWhereClause.Length - 4);
				}
			}
		}

		public void GetSortField()
		{
			if (cmbSort.Text == "Owners Name")
			{
				modGNBas.Statics.gstrSortField = "LastName, firstname , doginfo.dogname";
			}
			else if (cmbSort.Text == "Address")
			{
				modGNBas.Statics.gstrSortField = "Address1";
			}
			else if (cmbSort.Text == "Location")
			{
				modGNBas.Statics.gstrSortField = "DogOwner.LocationSTR, DogOwner.LocationNUM";
			}
			else if (cmbSort.Text == "Dog Name")
			{
				modGNBas.Statics.gstrSortField = "DogInfo.DogName";
			}
			else if (cmbSort.Text == "Tag Number")
			{
				modGNBas.Statics.gstrSortField = "doginfo.year ,DogInfo.TagLicNum,  doginfo.kenneldog ";
			}
			else if (cmbSort.Text == "Color")
			{
				modGNBas.Statics.gstrSortField = "dogcolor";
			}
			else if (cmbSort.Text == "Breed")
			{
				modGNBas.Statics.gstrSortField = "Dogbreed";
			}
			else if (cmbSort.Text == "Rabies Expiration Date")
			{
				modGNBas.Statics.gstrSortField = "DogInfo.RabiesExpDate";
			}
			else if (cmbSort.Text == "Registration Year")
			{
				modGNBas.Statics.gstrSortField = "DogInfo.Year , doginfo.kenneldog desc";
			}
		}

		private void frmReportListing_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				Close();
			}
		}

		private void frmReportListing_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmReportListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReportListing properties;
			//frmReportListing.ScaleWidth	= 9045;
			//frmReportListing.ScaleHeight	= 7815;
			//frmReportListing.LinkTopic	= "Form1";
			//frmReportListing.LockControls	= -1  'True;
			//End Unmaped Properties
			/* Control ctl = new Control(); */
			foreach (Control ctl in this.GetAllControls())
			{
				if (FCConvert.ToString(ctl.Tag) == "Required")
				{
					ctl.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
			}
			// ctl
			if (modGNBas.Statics.gstrReportOption == "Warrant")
			{
                if (cmbReport.Items.Contains("Registration Listing"))
                {
                    cmbReport.Items.Remove("Registration Listing");
                }
				cmbReport.SelectedIndex = 0;
				this.Text = "Municipal Warrant Listing";
				chkServeWarrant.Visible = true;
				chkPrintDate.Visible = false;
			}
			else
			{
				if (cmbReport.Items.Contains("Warrant Listing"))
				{
					cmbReport.Items.Remove("Warrant Listing");
				}
				cmbReport.SelectedIndex = 0;
				this.Text = "Registration Listing";
			}
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			txtYear.Text = FCConvert.ToString(DateTime.Today.Year);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void cmdClear_Click()
		{
			txtLastName.Text = string.Empty;
			txtAddress.Text = string.Empty;
			txtLocation.Text = string.Empty;
			txtDogName.Text = string.Empty;
			txtTagNumber1.Text = string.Empty;
			txtTagNumber2.Text = string.Empty;
			txtColor.Text = string.Empty;
			txtBreed.Text = string.Empty;
			txtRabiesDate1.Text = string.Empty;
			txtRabiesDate2.Text = string.Empty;
			txtYear1.Text = string.Empty;
			txtYear2.Text = string.Empty;
		}

		private void mnuClearSearch_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void optReport_CheckedChanged(string selectedItem, object sender, System.EventArgs e)
		{
			if (!cmbSort.Items.Contains("Location"))
			{
				cmbSort.Items.Insert(2, "Location");
			}
			if (selectedItem == "Registration Listing" || selectedItem == "Warrant Listing")
			{
				if (!cmbSort.Items.Contains("Owners Name"))
				{
					cmbSort.Items.Insert(0, "Owners Name");
				}
				if (!cmbSort.Items.Contains("Address"))
				{
					cmbSort.Items.Insert(1, "Address");
				}
				if (!cmbSort.Items.Contains("Dog Name"))
				{
					cmbSort.Items.Insert(3, "Dog Name");
				}
				if (!cmbSort.Items.Contains("Tag Number"))
				{
					cmbSort.Items.Insert(4, "Tag Number");
				}
			}
			else
			{
				if (cmbSort.Items.Contains("Owners Name"))
				{
					cmbSort.Items.Remove("Owners Name");
				}
				if (cmbSort.Items.Contains("Address"))
				{
					cmbSort.Items.Remove("Address");
				}
				if (cmbSort.Items.Contains("Dog Name"))
				{
					cmbSort.Items.Remove("Dog Name");
				}
				if (cmbSort.Items.Contains("Tag Number"))
				{
					cmbSort.Items.Remove("Tag Number");
				}
			}
			if (selectedItem == "Registration Listing")
			{
				if (!cmbSort.Items.Contains("Color"))
				{
					cmbSort.Items.Insert(5, "Color");
				}
				if (!cmbSort.Items.Contains("Breed"))
				{
					cmbSort.Items.Insert(6, "Breed");
				}
				if (!cmbSort.Items.Contains("Rabies Expiration Date"))
				{
					cmbSort.Items.Insert(7, "Rabies Expiration Date");
				}
				if (!cmbSort.Items.Contains("Registration Year"))
				{
					cmbSort.Items.Insert(8, "Registration Year");
				}
			}
            else
            {
                if (cmbSort.Items.Contains("Color"))
                {
                    cmbSort.Items.Remove("Color");
                }
                if (cmbSort.Items.Contains("Breed"))
                {
                    cmbSort.Items.Remove("Breed");
                }
                if (cmbSort.Items.Contains("Rabies Expiration Date"))
                {
                    cmbSort.Items.Remove("Rabies Expiration Date");
                }
                if (cmbSort.Items.Contains("Registration Year"))
                {
                    cmbSort.Items.Remove("Registration Year");
                }
            }
			txtLastName.Enabled = (selectedItem == "Registration Listing" || selectedItem == "Warrant Listing");
			txtAddress.Enabled = (selectedItem == "Registration Listing" || selectedItem == "Warrant Listing");
			// txtLocation.Enabled = Index = 1
			txtLocation.Enabled = true;
			txtDogName.Enabled = (selectedItem == "Registration Listing" || selectedItem == "Warrant Listing");
			txtTagNumber1.Enabled = (selectedItem == "Registration Listing" || selectedItem == "Warrant Listing");
			txtTagNumber2.Enabled = (selectedItem == "Registration Listing" || selectedItem == "Warrant Listing");
			txtColor.Enabled = selectedItem == "Registration Listing";
			txtBreed.Enabled = selectedItem == "Registration Listing";
			txtRabiesDate1.Enabled = selectedItem == "Registration Listing";
			txtRabiesDate2.Enabled = selectedItem == "Registration Listing";
			txtYear1.Enabled = selectedItem == "Registration Listing";
			txtYear2.Enabled = selectedItem == "Registration Listing";
			if (cmbReport.Text == "Warrant Listing")
			{
				//lngColor = ColorTranslator.ToOle(Color.White);
				// chkUnregistered.Visible = True
			}
			else
			{
				//lngColor = ColorTranslator.ToOle(this.BackColor);
				// chkUnregistered.Visible = False
			}
			// txtLocation.BackColor = lngColor
			if (cmbReport.Text == "Warrant Listing")
			{
				//lngColor = ColorTranslator.ToOle(this.BackColor);
			}
			else
			{
				//lngColor = ColorTranslator.ToOle(Color.White);
			}
			//txtColor.BackColor = ColorTranslator.FromOle(lngColor);
			//txtBreed.BackColor = ColorTranslator.FromOle(lngColor);
			//txtRabiesDate1.BackColor = ColorTranslator.FromOle(lngColor);
			//txtRabiesDate2.BackColor = ColorTranslator.FromOle(lngColor);
			//txtYear1.BackColor = ColorTranslator.FromOle(lngColor);
			//txtYear2.BackColor = ColorTranslator.FromOle(lngColor);
			txtYear.Visible = selectedItem == "Warrant Listing";
			lblYear.Visible = selectedItem == "Warrant Listing";
			cmdClear_Click();
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			string selectedItem = FCConvert.ToString(cmbReport.SelectedItem);
			optReport_CheckedChanged(selectedItem, sender, e);
		}

		private void txtYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Delete)
				return;
			if (KeyAscii == Keys.Back)
				return;
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

        //FC:FINAL:AM:#4076 - change type to T2KDateBox
        //private void txtRabiesDate1_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtRabiesDate1.Text.Length == 2 || txtRabiesDate1.Text.Length == 5) && Strings.Right(txtRabiesDate1.Text, 1) != "/")
        //	{
        //		txtRabiesDate1.Text = txtRabiesDate1.Text + "/";
        //		txtRabiesDate1.SelectionStart = txtRabiesDate1.Text.Length;
        //		txtRabiesDate1.SelectionLength = txtRabiesDate1.Text.Length;
        //	}
        //}

        //private void txtRabiesDate1_Enter(object sender, System.EventArgs e)
        //{
        //	txtRabiesDate1.SelectionStart = 0;
        //	txtRabiesDate1.SelectionLength = 10;
        //}

        //private void txtRabiesDate1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        //{
        //	Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
        //	if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Insert)
        //	{
        //		KeyAscii = (Keys)0;
        //	}
        //	if (fecherFoundation.Strings.Trim(txtRabiesDate1.Text).Length >= 8 && KeyAscii != Keys.Back)
        //	{
        //		KeyAscii = (Keys)0;
        //		return;
        //	}
        //	if (KeyAscii == Keys.Back && Strings.Right(txtRabiesDate1.Text, 1) == "/")
        //	{
        //		txtRabiesDate1.Text = Strings.Left(txtRabiesDate1.Text, txtRabiesDate1.Text.Length - 2);
        //		txtRabiesDate1.SelectionStart = txtRabiesDate1.Text.Length;
        //		txtRabiesDate1.SelectionLength = txtRabiesDate1.Text.Length;
        //		KeyAscii = (Keys)0;
        //	}
        //	e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        //}

        //private void txtRabiesDate2_TextChanged(object sender, System.EventArgs e)
        //{
        //	if ((txtRabiesDate2.Text.Length == 2 || txtRabiesDate2.Text.Length == 5) && Strings.Right(txtRabiesDate2.Text, 1) != "/")
        //	{
        //		txtRabiesDate2.Text = txtRabiesDate2.Text + "/";
        //		txtRabiesDate2.SelectionStart = txtRabiesDate2.Text.Length;
        //		txtRabiesDate2.SelectionLength = txtRabiesDate2.Text.Length;
        //	}
        //}

        //private void txtRabiesDate2_Enter(object sender, System.EventArgs e)
        //{
        //	txtRabiesDate2.SelectionStart = 0;
        //	txtRabiesDate2.SelectionLength = 10;
        //}

        //private void txtRabiesDate2_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        //{
        //	Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
        //	if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Insert)
        //	{
        //		KeyAscii = (Keys)0;
        //	}
        //	if (fecherFoundation.Strings.Trim(txtRabiesDate2.Text).Length >= 8 && KeyAscii != Keys.Back)
        //	{
        //		KeyAscii = (Keys)0;
        //		return;
        //	}
        //	if (KeyAscii == Keys.Back && Strings.Right(txtRabiesDate2.Text, 1) == "/")
        //	{
        //		txtRabiesDate2.Text = Strings.Left(txtRabiesDate2.Text, txtRabiesDate2.Text.Length - 2);
        //		txtRabiesDate2.SelectionStart = txtRabiesDate2.Text.Length;
        //		txtRabiesDate2.SelectionLength = txtRabiesDate2.Text.Length;
        //		KeyAscii = (Keys)0;
        //	}
        //	e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        //}

        private void cmdPrintPreview_Click(object sender, EventArgs e)
        {
            mnuPrintPreview_Click(cmdPrintPreview, EventArgs.Empty);
        }
    }
}
