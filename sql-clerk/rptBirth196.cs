//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBirth196.
	/// </summary>
	public partial class rptBirth196 : BaseSectionReport
	{
		public rptBirth196()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Birth Certificate ";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.PrintProgress += RptBirth196_PrintProgress;
		}

        private void RptBirth196_PrintProgress(object sender, EventArgs e)
        {
            boolPrinted = true;
        }

        public static rptBirth196 InstancePtr
		{
			get
			{
				return (rptBirth196)Sys.GetInstance(typeof(rptBirth196));
			}
		}

		protected rptBirth196 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData2.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBirth196	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private clsDRWrapper rsData2 = new clsDRWrapper();
		private bool boolPrintTest;
		private double dblLineAdjust;
		private int lngSignatureId;

		public void Init(bool boolTestPrint, double dblAdjust = 0, int intType = 0, bool boolShow = true)
		{
			boolPrintTest = boolTestPrint;
			lngSignatureId = 0;
			if (boolPrintTest)
			{
				dblLineAdjust = dblAdjust;
			}
			if (intType == 706)
			{
				Field14.Left -= 720 / 1440F;
				// Field14.Width = Field14.Width - 540
				Field16.Left -= 720 / 1440F;
				imgSig.Left -= 720 / 1440F;
			}
			if (boolShow)
			{
                //FC:FINAL:IPI - #i1705 - show the report viewer on web
                //this.Show(FCForm.FormShowEnum.Modal);
                frmReportViewer.InstancePtr.Init(this, showModal: true);
            }
			else
			{
				this.PrintReport(false);
			}
		}

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}


		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblLaserLineAdjustment3;
			// Dim rsData As New clsDRWrapper
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			//Application.DoEvents();
			// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
			// If rsData.EndOfFile Then
			// dblLaserLineAdjustment3 = 0
			// Else
			// dblLaserLineAdjustment3 = CDbl(Val(rsData.Fields("BirthAdjustment")))
			// End If
			dblLaserLineAdjustment3 = Conversion.Val(modRegistry.GetRegistryKey("BirthAdjustment", "CK"));
			if (boolPrintTest)
			{
				dblLaserLineAdjustment3 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment3) / 1440F;
			}
			if (!boolPrintTest)
			{
				rsData2.OpenRecordset("Select * from Amendments where Type = 'BIRTHS' and AmendmentID = '" + frmBirths.InstancePtr.lblBKey.Text + "'", modGNBas.DEFAULTCLERKDATABASE);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// If gboolFromDosTrio = True Or bolFromWindowsCR Then
            boolPrinted = true;
			if (boolPrinted)
			{

				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				//modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
			}
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */
			frmPrintVitalRec.InstancePtr.Hide();
			if (!boolPrintTest)
			{
				Field1.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FirstName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MiddleName"))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("LastName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("Designation")));
				// If IsDate(rsBirthCertificate.Fields("DateOfBirth")) Then
				// Field2 = Format(rsBirthCertificate.Fields("DateOfBirth"), "MM/dd/yyyy")
				// Else
				// Field2.Text = rsBirthCertificate.Fields("DateOfBirthDescription")
				// End If
				Field2.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields("dateofbirth");
				Field3.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("Sex");
				FileNumber.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FileNumber");
				// Field4 = GetDefaultTownName(rsBirthCertificate.Fields("BirthPlace"))
				Field4.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("ChildTown");
				Field5.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsFirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsMiddleInitial")))) + (fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsMiddleInitial")))) == string.Empty ? "" : " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsLastName"))) + (FCConvert.ToString(modGlobalRoutines.CheckForNAReturnComma(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantTitle") + ""))) + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantTitle")));
				Field6.Text = "";
				// CheckForNA(rsBirthCertificate.Fields("AttendantTitle"))
				Field7.Text = FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantAddress")));
				Field8.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersFirstName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersMiddleName"))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersMaidenName");
				// Field9 = GetDefaultTownName(Val(rsBirthCertificate.Fields("ResidenceOfMother")))
				if (fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherState") + "") == "")
				{
					Field9.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherTown") + "";
				}
				else
				{
					Field9.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherTown") + "" + ", " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherState") + "";
				}
				if (modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("Legitimate") == true || modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("aop") || modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("courtdeterminedpaternity"))
				{
					Field10.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersFirstName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersMiddleInitial"))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersLastName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersDesignation")));
				}
				else
				{
					Field10.Text = string.Empty;
				}
				if (FCConvert.ToString(modGlobalRoutines.CheckForNA(Field10.Text)) == string.Empty)
					Field10.Text = "- - -";
				// Field11 = rsBirthCertificate.Fields("RecordingClerksFirstName")
				// & " " & CheckForNA(rsBirthCertificate.Fields("RecordingClerksMiddleInitial"))
				// & " " & rsBirthCertificate.Fields("RecordingClerksLastName")
				// 
				Field11.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("RegistrarName");
				// Field12 = GetRegistrarTownName(rsBirthCertificate.Fields("RegistrarFirstName"), CheckForNA(rsBirthCertificate.Fields("RegistrarMiddleName")), rsBirthCertificate.Fields("RegistrarLastName"))
				Field12.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("RegTown");
				if (Information.IsDate(modGNBas.Statics.rsBirthCertificate.Get_Fields("RegistrarDateFiled")) && Convert.ToDateTime(modGNBas.Statics.rsBirthCertificate.Get_Fields_DateTime("RegistrarDateFiled")).ToOADate() != 0)
				{
					Field13.Text = Strings.Format(modGNBas.Statics.rsBirthCertificate.Get_Fields_DateTime("RegistrarDateFiled"), "MM/dd/yyyy");
				}
				else
				{
					Field13.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("registrardatefileddescription");
				}
				if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
				{
					Field14.Text = "";
				}
				else
				{
					Field14.Text = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				}
				Field15.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				Field16.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("TownOf") + " ");
				if (FCConvert.ToBoolean(modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("DeathRecordInOffice")))
				{
					txtDeceased.Text = txtDeceased.Tag + "  " + modGNBas.Statics.rsBirthCertificate.Get_Fields("DeceasedDate");
					txtDeceased.Visible = true;
				}
				else
				{
					txtDeceased.Visible = false;
				}
				if (FCConvert.ToBoolean(modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("SoleParent")))
				{
					txtSoleParent.Text = "Sole Legal Legitimate Parent";
				}
				else
				{
					txtSoleParent.Text = string.Empty;
				}
				txtAmended.Text = modAmendments.GetAmendments(rsData2);
			}
			else
			{
				Field2.Text = "Date of Birth";
				Field4.Text = "Town";
				Field12.Text = "Town Name";
				txtSoleParent.Text = "Sole Legal Legitimate Parent";
			}
		}

		
	}
}
