//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmPrintIntentions.
	/// </summary>
	partial class frmPrintIntentions
	{
		public fecherFoundation.FCComboBox cmbSafetyPaper;
		public fecherFoundation.FCLabel lblSafetyPaper;
		public fecherFoundation.FCComboBox cmbNew;
		public fecherFoundation.FCLabel lblNew;
		public fecherFoundation.FCComboBox cmbtion1;
		public fecherFoundation.FCComboBox cmbeck1;
		public fecherFoundation.FCLabel lbleck1;
		public fecherFoundation.FCCheckBox chkSplitFee;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdPrint;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbSafetyPaper = new fecherFoundation.FCComboBox();
            this.lblSafetyPaper = new fecherFoundation.FCLabel();
            this.cmbNew = new fecherFoundation.FCComboBox();
            this.lblNew = new fecherFoundation.FCLabel();
            this.cmbtion1 = new fecherFoundation.FCComboBox();
            this.cmbeck1 = new fecherFoundation.FCComboBox();
            this.lbleck1 = new fecherFoundation.FCLabel();
            this.chkSplitFee = new fecherFoundation.FCCheckBox();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.Frame3 = new Wisej.Web.Panel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSplitFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.Frame3.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 276);
            this.BottomPanel.Size = new System.Drawing.Size(726, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.cmbtion1);
            this.ClientArea.Controls.Add(this.chkSplitFee);
            this.ClientArea.Controls.Add(this.cmbNew);
            this.ClientArea.Controls.Add(this.lblNew);
            this.ClientArea.Controls.Add(this.cmdCancel);
            this.ClientArea.Controls.Add(this.cmbeck1);
            this.ClientArea.Controls.Add(this.lbleck1);
            this.ClientArea.Size = new System.Drawing.Size(726, 216);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(726, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(253, 30);
            this.HeaderText.Text = "Intentions Parameters";
            // 
            // cmbSafetyPaper
            // 
            this.cmbSafetyPaper.Items.AddRange(new object[] {
            "R196",
            "R0606"});
            this.cmbSafetyPaper.Location = new System.Drawing.Point(151, 0);
            this.cmbSafetyPaper.Name = "cmbSafetyPaper";
            this.cmbSafetyPaper.Size = new System.Drawing.Size(401, 40);
            this.cmbSafetyPaper.TabIndex = 2;
            this.cmbSafetyPaper.Text = "R0606";
            // 
            // lblSafetyPaper
            // 
            this.lblSafetyPaper.AutoSize = true;
            this.lblSafetyPaper.Location = new System.Drawing.Point(0, 14);
            this.lblSafetyPaper.Name = "lblSafetyPaper";
            this.lblSafetyPaper.Size = new System.Drawing.Size(101, 15);
            this.lblSafetyPaper.TabIndex = 3;
            this.lblSafetyPaper.Text = "SAFETY PAPER";
            // 
            // cmbNew
            // 
            this.cmbNew.Items.AddRange(new object[] {
            "R 01/2005",
            "R 08/2006",
            "R 05/2010",
            "Word Doc"});
            this.cmbNew.Location = new System.Drawing.Point(181, 150);
            this.cmbNew.Name = "cmbNew";
            this.cmbNew.Size = new System.Drawing.Size(401, 40);
            this.cmbNew.TabIndex = 11;
            this.cmbNew.Text = "Word Doc";
            // 
            // lblNew
            // 
            this.lblNew.AutoSize = true;
            this.lblNew.Location = new System.Drawing.Point(30, 164);
            this.lblNew.Name = "lblNew";
            this.lblNew.Size = new System.Drawing.Size(79, 15);
            this.lblNew.TabIndex = 12;
            this.lblNew.Text = "FORM TYPE";
            // 
            // cmbtion1
            // 
            this.cmbtion1.Items.AddRange(new object[] {
            "Certificate Information (Top of Marriage License form)",
            "Print Ceremony Section (Bottom of Marriage License form)",
            "Print Ceremony Section AND Certificate Information",
            "Print Intentions"});
            this.cmbtion1.Location = new System.Drawing.Point(181, 90);
            this.cmbtion1.Name = "cmbtion1";
            this.cmbtion1.Size = new System.Drawing.Size(401, 40);
            this.cmbtion1.TabIndex = 16;
            this.cmbtion1.Text = "Certificate Information (Top of Marriage License form)";
            this.cmbtion1.SelectedIndexChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // cmbeck1
            // 
            this.cmbeck1.Items.AddRange(new object[] {
            "State",
            "Place of Issuance Copy"});
            this.cmbeck1.Location = new System.Drawing.Point(181, 30);
            this.cmbeck1.Name = "cmbeck1";
            this.cmbeck1.Size = new System.Drawing.Size(401, 40);
            this.cmbeck1.TabIndex = 13;
            this.cmbeck1.Text = "State";
            // 
            // lbleck1
            // 
            this.lbleck1.AutoSize = true;
            this.lbleck1.Location = new System.Drawing.Point(30, 44);
            this.lbleck1.Name = "lbleck1";
            this.lbleck1.Size = new System.Drawing.Size(45, 15);
            this.lbleck1.TabIndex = 14;
            this.lbleck1.Text = "PRINT";
            // 
            // chkSplitFee
            // 
            this.chkSplitFee.Location = new System.Drawing.Point(30, 210);
            this.chkSplitFee.Name = "chkSplitFee";
            this.chkSplitFee.Size = new System.Drawing.Size(307, 27);
            this.chkSplitFee.TabIndex = 10;
            this.chkSplitFee.Text = "Separate fee for intentions and license";
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(602, 30);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(96, 40);
            this.cmdCancel.TabIndex = 2;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(318, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(80, 48);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.lblSafetyPaper);
            this.Frame3.Controls.Add(this.cmbSafetyPaper);
            this.Frame3.Location = new System.Drawing.Point(30, 257);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(576, 40);
            this.Frame3.TabIndex = 15;
            this.Frame3.TabStop = true;
            this.Frame3.Visible = false;
            // 
            // frmPrintIntentions
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(726, 384);
            this.FillColor = 0;
            this.Name = "frmPrintIntentions";
            this.Text = "Intentions Parameters";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmPrintIntentions_Load);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSplitFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            this.ResumeLayout(false);

		}
        #endregion

        private Panel Frame3;
    }
}