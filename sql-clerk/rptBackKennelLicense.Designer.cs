﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBackKennelLicense.
	/// </summary>
	partial class rptBackKennelLicense
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBackKennelLicense));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBreed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSex = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRabiesNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateGiven = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStickerNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLateFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBreed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRabiesNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateGiven)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStickerNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLateFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.txtBreed,
				this.txtSex,
				this.txtSN,
				this.txtRabiesNum,
				this.txtDateGiven,
				this.txtStickerNum,
				this.txtLateFee
			});
			this.Detail.Height = 0.34375F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Height = 0.4430556F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.6875F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0.15625F;
			this.txtName.Width = 1.25F;
			// 
			// txtBreed
			// 
			this.txtBreed.Height = 0.1875F;
			this.txtBreed.Left = 2.0625F;
			this.txtBreed.Name = "txtBreed";
			this.txtBreed.Text = "Field1";
			this.txtBreed.Top = 0.15625F;
			this.txtBreed.Width = 1.75F;
			// 
			// txtSex
			// 
			this.txtSex.Height = 0.1875F;
			this.txtSex.Left = 3.875F;
			this.txtSex.Name = "txtSex";
			this.txtSex.Text = "Field1";
			this.txtSex.Top = 0.15625F;
			this.txtSex.Width = 0.25F;
			// 
			// txtSN
			// 
			this.txtSN.Height = 0.1875F;
			this.txtSN.Left = 4.3125F;
			this.txtSN.Name = "txtSN";
			this.txtSN.Text = "Field1";
			this.txtSN.Top = 0.15625F;
			this.txtSN.Width = 0.1875F;
			// 
			// txtRabiesNum
			// 
			this.txtRabiesNum.Height = 0.1666667F;
			this.txtRabiesNum.Left = 4.78125F;
			this.txtRabiesNum.Name = "txtRabiesNum";
			this.txtRabiesNum.Text = "Field1";
			this.txtRabiesNum.Top = 0.1666667F;
			this.txtRabiesNum.Width = 0.96875F;
			// 
			// txtDateGiven
			// 
			this.txtDateGiven.Height = 0.1666667F;
			this.txtDateGiven.Left = 5.8125F;
			this.txtDateGiven.Name = "txtDateGiven";
			this.txtDateGiven.Text = "Field1";
			this.txtDateGiven.Top = 0.1666667F;
			this.txtDateGiven.Width = 0.75F;
			// 
			// txtStickerNum
			// 
			this.txtStickerNum.Height = 0.1666667F;
			this.txtStickerNum.Left = 6.59375F;
			this.txtStickerNum.Name = "txtStickerNum";
			this.txtStickerNum.Text = "Field1";
			this.txtStickerNum.Top = 0.1666667F;
			this.txtStickerNum.Width = 1.03125F;
			// 
			// txtLateFee
			// 
			this.txtLateFee.Height = 0.1666667F;
			this.txtLateFee.Left = 7.65625F;
			this.txtLateFee.Name = "txtLateFee";
			this.txtLateFee.Text = "Field1";
			this.txtLateFee.Top = 0.1666667F;
			this.txtLateFee.Width = 0.1875F;
			// 
			// rptBackKennelLicense
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.1736111F;
			this.PageSettings.Margins.Right = 0.3472222F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.885417F;
			this.Script = "\r\nSub ActiveReport_ReportStart\r\n\r\nEnd Sub\r\n";
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBreed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRabiesNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateGiven)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStickerNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLateFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBreed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSex;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRabiesNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateGiven;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickerNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLateFee;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
