﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogLicenseBackup.
	/// </summary>
	partial class rptDogLicenseBackup
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDogLicenseBackup));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTagNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDogName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSticker)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAmount,
				this.txtOwnerName,
				this.txtTagNumber,
				this.txtDatePaid,
				this.txtDogName,
				this.txtLine,
				this.txtSticker,
				this.txtDescription,
				this.Label1
			});
			this.Detail.Height = 0.40625F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field6,
				this.Field8,
				this.Field9,
				this.txtCaption,
				this.txtCaption2,
				this.txtDate,
				this.txtPage,
				this.txtTown,
				this.txtTime
			});
			this.PageHeader.Height = 0.8333333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.2083333F;
			this.Field1.Left = 0.40625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Date";
			this.Field1.Top = 0.625F;
			this.Field1.Width = 0.625F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.2083333F;
			this.Field2.Left = 1.15625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field2.Text = "Owners Name";
			this.Field2.Top = 0.625F;
			this.Field2.Width = 1F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.2083333F;
			this.Field3.Left = 2.75F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field3.Text = "Amount";
			this.Field3.Top = 0.625F;
			this.Field3.Width = 0.6875F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.2083333F;
			this.Field6.Left = 3.59375F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field6.Text = "Dog Name";
			this.Field6.Top = 0.625F;
			this.Field6.Width = 0.875F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.2083333F;
			this.Field8.Left = 5.625F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field8.Text = "Tag #";
			this.Field8.Top = 0.625F;
			this.Field8.Width = 0.5F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.2083333F;
			this.Field9.Left = 6.65625F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field9.Text = "Sticker #";
			this.Field9.Top = 0.625F;
			this.Field9.Width = 0.6875F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1666667F;
			this.txtCaption.Left = 0.09375F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = null;
			this.txtCaption.Top = 0.04166667F;
			this.txtCaption.Width = 7.21875F;
			// 
			// txtCaption2
			// 
			this.txtCaption2.Height = 0.1666667F;
			this.txtCaption2.Left = 0.09375F;
			this.txtCaption2.Name = "txtCaption2";
			this.txtCaption2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.txtCaption2.Text = null;
			this.txtCaption2.Top = 0.2083333F;
			this.txtCaption2.Width = 7.21875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 5.96875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 5.96875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.375F;
			// 
			// txtTown
			// 
			this.txtTown.Height = 0.1666667F;
			this.txtTown.Left = 0.03125F;
			this.txtTown.Name = "txtTown";
			this.txtTown.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTown.Text = null;
			this.txtTown.Top = 0.04166667F;
			this.txtTown.Width = 1.375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0.03125F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.375F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1666667F;
			this.txtAmount.Left = 2.6875F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.75F;
			// 
			// txtOwnerName
			// 
			this.txtOwnerName.Height = 0.1666667F;
			this.txtOwnerName.Left = 1.15625F;
			this.txtOwnerName.Name = "txtOwnerName";
			this.txtOwnerName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtOwnerName.Text = null;
			this.txtOwnerName.Top = 0F;
			this.txtOwnerName.Width = 1.5F;
			// 
			// txtTagNumber
			// 
			this.txtTagNumber.Height = 0.1666667F;
			this.txtTagNumber.Left = 5.53125F;
			this.txtTagNumber.Name = "txtTagNumber";
			this.txtTagNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTagNumber.Text = null;
			this.txtTagNumber.Top = 0F;
			this.txtTagNumber.Width = 0.96875F;
			// 
			// txtDatePaid
			// 
			this.txtDatePaid.Height = 0.1666667F;
			this.txtDatePaid.Left = 0.375F;
			this.txtDatePaid.Name = "txtDatePaid";
			this.txtDatePaid.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDatePaid.Text = null;
			this.txtDatePaid.Top = 0F;
			this.txtDatePaid.Width = 0.75F;
			// 
			// txtDogName
			// 
			this.txtDogName.CanShrink = true;
			this.txtDogName.Height = 0.25F;
			this.txtDogName.Left = 3.625F;
			this.txtDogName.Name = "txtDogName";
			this.txtDogName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDogName.Text = null;
			this.txtDogName.Top = 0F;
			this.txtDogName.Width = 1.8125F;
			// 
			// txtLine
			// 
			this.txtLine.CanGrow = false;
			this.txtLine.Height = 0.1666667F;
			this.txtLine.Left = 0.0625F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtLine.Text = null;
			this.txtLine.Top = 0F;
			this.txtLine.Width = 0.28125F;
			// 
			// txtSticker
			// 
			this.txtSticker.Height = 0.1666667F;
			this.txtSticker.Left = 6.5625F;
			this.txtSticker.Name = "txtSticker";
			this.txtSticker.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSticker.Text = null;
			this.txtSticker.Top = 0F;
			this.txtSticker.Width = 0.78125F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 1.1875F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.1666667F;
			this.txtDescription.Width = 2.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.3125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label1.Text = "Description";
			this.Label1.Top = 0.1666667F;
			this.Label1.Width = 0.8125F;
			// 
			// rptDogLicenseBackup
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.395833F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSticker)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
