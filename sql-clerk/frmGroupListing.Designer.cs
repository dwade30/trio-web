//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmGroupListing.
	/// </summary>
	partial class frmGroupListing
	{
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCComboBox cboSelectedGroup;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbAll = new fecherFoundation.FCComboBox();
            this.cboSelectedGroup = new fecherFoundation.FCComboBox();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 270);
            this.BottomPanel.Size = new System.Drawing.Size(521, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.cboSelectedGroup);
            this.ClientArea.Controls.Add(this.cmbAll);
            this.ClientArea.Size = new System.Drawing.Size(521, 210);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(521, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(159, 30);
            this.HeaderText.Text = "Group Listing";
            // 
            // cmbAll
            // 
            this.cmbAll.AutoSize = false;
            this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAll.FormattingEnabled = true;
            this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbAll.Location = new System.Drawing.Point(30, 30);
            this.cmbAll.Name = "cmbAll";
            this.cmbAll.Size = new System.Drawing.Size(219, 40);
            this.cmbAll.TabIndex = 0;
            this.cmbAll.Text = "All";
            this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
            // 
            // cboSelectedGroup
            // 
            this.cboSelectedGroup.AutoSize = false;
            this.cboSelectedGroup.BackColor = System.Drawing.SystemColors.Window;
            this.cboSelectedGroup.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboSelectedGroup.Enabled = false;
            this.cboSelectedGroup.FormattingEnabled = true;
            this.cboSelectedGroup.Location = new System.Drawing.Point(30, 90);
            this.cboSelectedGroup.Name = "cboSelectedGroup";
            this.cboSelectedGroup.Size = new System.Drawing.Size(462, 40);
            this.cboSelectedGroup.TabIndex = 3;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 150);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmGroupListing
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(521, 378);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmGroupListing";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Group Listing";
            this.Load += new System.EventHandler(this.frmGroupListing_Load);
            this.Activated += new System.EventHandler(this.frmGroupListing_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGroupListing_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}