//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmNewKennel.
	/// </summary>
	partial class frmNewKennel
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLabels;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCFrame framAddDogs;
		public fecherFoundation.FCButton cmdOKAddDogs;
		public FCGrid GridAddDogs;
		public fecherFoundation.FCComboBox cmbLicenseNumber;
		public Global.T2KDateBox t2kIssueDate;
		public fecherFoundation.FCCheckBox chkFees;
		public FCGrid GridDogs;
		public Global.T2KDateBox t2kInspectionDate;
		public fecherFoundation.FCFrame framFees;
		public fecherFoundation.FCTextBox txtAmount;
		public fecherFoundation.FCCheckBox chkLicense;
		public fecherFoundation.FCCheckBox chkWarrantFee;
		public fecherFoundation.FCCheckBox chkLateFee;
		public fecherFoundation.FCLabel lblLabels_1;
		public fecherFoundation.FCPictureBox imgDocuments;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel lblOwnerName;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteKennel;
		public fecherFoundation.FCToolStripMenuItem mnuAddNewDog;
		public fecherFoundation.FCToolStripMenuItem mnuAddExistingDog;
		public fecherFoundation.FCToolStripMenuItem mnuRemoveDog;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocs;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintBack;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewKennel));
            this.txtYear = new fecherFoundation.FCTextBox();
            this.framAddDogs = new fecherFoundation.FCFrame();
            this.cmdOKAddDogs = new fecherFoundation.FCButton();
            this.GridAddDogs = new fecherFoundation.FCGrid();
            this.cmbLicenseNumber = new fecherFoundation.FCComboBox();
            this.t2kIssueDate = new Global.T2KDateBox();
            this.chkFees = new fecherFoundation.FCCheckBox();
            this.GridDogs = new fecherFoundation.FCGrid();
            this.t2kInspectionDate = new Global.T2KDateBox();
            this.framFees = new fecherFoundation.FCFrame();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.chkLicense = new fecherFoundation.FCCheckBox();
            this.chkWarrantFee = new fecherFoundation.FCCheckBox();
            this.chkLateFee = new fecherFoundation.FCCheckBox();
            this.lblLabels_1 = new fecherFoundation.FCLabel();
            this.imgDocuments = new fecherFoundation.FCPictureBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.lblOwnerName = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuViewDocs = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintBack = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteKennel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddNewDog = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddExistingDog = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRemoveDog = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddNewDog = new fecherFoundation.FCButton();
            this.cmdDeleteKennelLicense = new fecherFoundation.FCButton();
            this.cmdAddExistingDog = new fecherFoundation.FCButton();
            this.cmdRemoveDogFromKennel = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framAddDogs)).BeginInit();
            this.framAddDogs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKAddDogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAddDogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInspectionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framFees)).BeginInit();
            this.framFees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWarrantFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLateFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNewDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteKennelLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddExistingDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveDogFromKennel)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(904, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtYear);
            this.ClientArea.Controls.Add(this.cmbLicenseNumber);
            this.ClientArea.Controls.Add(this.t2kIssueDate);
            this.ClientArea.Controls.Add(this.chkFees);
            this.ClientArea.Controls.Add(this.GridDogs);
            this.ClientArea.Controls.Add(this.t2kInspectionDate);
            this.ClientArea.Controls.Add(this.framFees);
            this.ClientArea.Controls.Add(this.imgDocuments);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label17);
            this.ClientArea.Controls.Add(this.lblOwnerName);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.framAddDogs);
            this.ClientArea.Size = new System.Drawing.Size(904, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdRemoveDogFromKennel);
            this.TopPanel.Controls.Add(this.cmdAddExistingDog);
            this.TopPanel.Controls.Add(this.cmdDeleteKennelLicense);
            this.TopPanel.Controls.Add(this.cmdAddNewDog);
            this.TopPanel.Size = new System.Drawing.Size(904, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddNewDog, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteKennelLicense, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddExistingDog, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRemoveDogFromKennel, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(181, 30);
            this.HeaderText.Text = "Kennel License";
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.Enabled = false;
            this.txtYear.Location = new System.Drawing.Point(582, 120);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(70, 40);
            this.txtYear.TabIndex = 20;
            this.txtYear.Text = "2020";
            // 
            // framAddDogs
            // 
            this.framAddDogs.BackColor = System.Drawing.Color.White;
            this.framAddDogs.Controls.Add(this.cmdOKAddDogs);
            this.framAddDogs.Controls.Add(this.GridAddDogs);
            this.framAddDogs.Location = new System.Drawing.Point(30, 30);
            this.framAddDogs.Name = "framAddDogs";
            this.framAddDogs.Size = new System.Drawing.Size(381, 490);
            this.framAddDogs.TabIndex = 16;
            this.framAddDogs.Text = "Dogs Not In Kennel(S)";
            this.framAddDogs.Visible = false;
            // 
            // cmdOKAddDogs
            // 
            this.cmdOKAddDogs.AppearanceKey = "actionButton";
            this.cmdOKAddDogs.Location = new System.Drawing.Point(20, 430);
            this.cmdOKAddDogs.Name = "cmdOKAddDogs";
            this.cmdOKAddDogs.Size = new System.Drawing.Size(65, 40);
            this.cmdOKAddDogs.TabIndex = 18;
            this.cmdOKAddDogs.Text = "OK";
            this.cmdOKAddDogs.Click += new System.EventHandler(this.cmdOKAddDogs_Click);
            // 
            // GridAddDogs
            // 
            this.GridAddDogs.Cols = 3;
            this.GridAddDogs.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridAddDogs.ExtendLastCol = true;
            this.GridAddDogs.FixedCols = 0;
            this.GridAddDogs.Location = new System.Drawing.Point(20, 30);
            this.GridAddDogs.Name = "GridAddDogs";
            this.GridAddDogs.ReadOnly = false;
            this.GridAddDogs.RowHeadersVisible = false;
            this.GridAddDogs.Rows = 1;
            this.GridAddDogs.Size = new System.Drawing.Size(340, 380);
            this.GridAddDogs.TabIndex = 17;
            this.GridAddDogs.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridAddDogs_BeforeEdit);
            // 
            // cmbLicenseNumber
            // 
            this.cmbLicenseNumber.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLicenseNumber.Location = new System.Drawing.Point(30, 120);
            this.cmbLicenseNumber.Name = "cmbLicenseNumber";
            this.cmbLicenseNumber.Size = new System.Drawing.Size(189, 40);
            this.cmbLicenseNumber.TabIndex = 21;
            this.cmbLicenseNumber.TextChanged += new System.EventHandler(this.cmbLicenseNumber_TextChanged);
            // 
            // t2kIssueDate
            // 
            this.t2kIssueDate.Location = new System.Drawing.Point(249, 120);
            this.t2kIssueDate.Name = "t2kIssueDate";
            this.t2kIssueDate.Size = new System.Drawing.Size(115, 40);
            this.t2kIssueDate.TabIndex = 1;
            this.t2kIssueDate.TextChanged += new System.EventHandler(this.t2kIssueDate_Change);
            // 
            // chkFees
            // 
            this.chkFees.Location = new System.Drawing.Point(30, 411);
            this.chkFees.Name = "chkFees";
            this.chkFees.Size = new System.Drawing.Size(120, 27);
            this.chkFees.TabIndex = 10;
            this.chkFees.Text = "Apply Fee(s)";
            this.chkFees.CheckedChanged += new System.EventHandler(this.chkFees_CheckedChanged);
            // 
            // GridDogs
            // 
            this.GridDogs.Cols = 10;
            this.GridDogs.FixedCols = 0;
            this.GridDogs.Location = new System.Drawing.Point(30, 180);
            this.GridDogs.Name = "GridDogs";
            this.GridDogs.RowHeadersVisible = false;
            this.GridDogs.Rows = 1;
            this.GridDogs.Size = new System.Drawing.Size(624, 211);
            this.GridDogs.TabIndex = 3;
            this.GridDogs.DoubleClick += new System.EventHandler(this.GridDogs_DblClick);
            // 
            // t2kInspectionDate
            // 
            this.t2kInspectionDate.Location = new System.Drawing.Point(392, 120);
            this.t2kInspectionDate.Name = "t2kInspectionDate";
            this.t2kInspectionDate.Size = new System.Drawing.Size(115, 40);
            this.t2kInspectionDate.TabIndex = 2;
            this.t2kInspectionDate.TextChanged += new System.EventHandler(this.t2kInspectionDate_Change);
            // 
            // framFees
            // 
            this.framFees.BackColor = System.Drawing.Color.White;
            this.framFees.Controls.Add(this.txtAmount);
            this.framFees.Controls.Add(this.chkLicense);
            this.framFees.Controls.Add(this.chkWarrantFee);
            this.framFees.Controls.Add(this.chkLateFee);
            this.framFees.Controls.Add(this.lblLabels_1);
            this.framFees.Location = new System.Drawing.Point(30, 458);
            this.framFees.Name = "framFees";
            this.framFees.Size = new System.Drawing.Size(624, 90);
            this.framFees.TabIndex = 7;
            this.framFees.Text = "Transaction Information";
            this.framFees.Visible = false;
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAmount.Location = new System.Drawing.Point(514, 30);
            this.txtAmount.LockedOriginal = true;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Size = new System.Drawing.Size(88, 40);
            this.txtAmount.TabIndex = 8;
            this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // chkLicense
            // 
            this.chkLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkLicense.Location = new System.Drawing.Point(20, 35);
            this.chkLicense.Name = "chkLicense";
            this.chkLicense.Size = new System.Drawing.Size(84, 27);
            this.chkLicense.TabIndex = 4;
            this.chkLicense.Text = "License";
            this.chkLicense.CheckedChanged += new System.EventHandler(this.chkLicense_CheckedChanged);
            // 
            // chkWarrantFee
            // 
            this.chkWarrantFee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkWarrantFee.Location = new System.Drawing.Point(232, 35);
            this.chkWarrantFee.Name = "chkWarrantFee";
            this.chkWarrantFee.Size = new System.Drawing.Size(118, 27);
            this.chkWarrantFee.TabIndex = 6;
            this.chkWarrantFee.Text = "Warrant Fee";
            this.chkWarrantFee.CheckedChanged += new System.EventHandler(this.chkWarrantFee_CheckedChanged);
            // 
            // chkLateFee
            // 
            this.chkLateFee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkLateFee.Location = new System.Drawing.Point(121, 35);
            this.chkLateFee.Name = "chkLateFee";
            this.chkLateFee.Size = new System.Drawing.Size(92, 27);
            this.chkLateFee.TabIndex = 5;
            this.chkLateFee.Text = "Late Fee";
            this.chkLateFee.CheckedChanged += new System.EventHandler(this.chkLateFee_CheckedChanged);
            // 
            // lblLabels_1
            // 
            this.lblLabels_1.Location = new System.Drawing.Point(368, 44);
            this.lblLabels_1.Name = "lblLabels_1";
            this.lblLabels_1.Size = new System.Drawing.Size(136, 17);
            this.lblLabels_1.TabIndex = 9;
            this.lblLabels_1.Text = "AMOUNT CHARGED";
            // 
            // imgDocuments
            // 
            this.imgDocuments.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDocuments.Image = ((System.Drawing.Image)(resources.GetObject("imgDocuments.Image")));
            this.imgDocuments.Location = new System.Drawing.Point(30, 30);
            this.imgDocuments.Name = "imgDocuments";
            this.imgDocuments.Size = new System.Drawing.Size(40, 40);
            this.imgDocuments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgDocuments, "One or more documents are attached to this record");
            this.imgDocuments.Click += new System.EventHandler(this.imgDocuments_Click);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(582, 96);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(58, 19);
            this.Label4.TabIndex = 19;
            this.Label4.Text = "YEAR";
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(30, 44);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(50, 15);
            this.Label17.TabIndex = 15;
            this.Label17.Text = "OWNER";
            // 
            // lblOwnerName
            // 
            this.lblOwnerName.Location = new System.Drawing.Point(91, 44);
            this.lblOwnerName.Name = "lblOwnerName";
            this.lblOwnerName.Size = new System.Drawing.Size(333, 15);
            this.lblOwnerName.TabIndex = 14;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(392, 96);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(160, 19);
            this.Label3.TabIndex = 13;
            this.Label3.Text = "KENNEL INSPECTION DATE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(249, 96);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(122, 19);
            this.Label2.TabIndex = 12;
            this.Label2.Text = "ISSUE / RENEW DATE";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 96);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(106, 19);
            this.Label1.TabIndex = 11;
            this.Label1.Text = "LICENSE NUMBER";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuViewDocs,
            this.mnuPrint,
            this.mnuPrintBack});
            this.MainMenu1.Name = null;
            // 
            // mnuViewDocs
            // 
            this.mnuViewDocs.Index = 0;
            this.mnuViewDocs.Name = "mnuViewDocs";
            this.mnuViewDocs.Text = "View Attached Documents";
            this.mnuViewDocs.Click += new System.EventHandler(this.mnuViewDocs_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print Preview Front of License";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintBack
            // 
            this.mnuPrintBack.Index = 2;
            this.mnuPrintBack.Name = "mnuPrintBack";
            this.mnuPrintBack.Text = "Print Preview Back of License";
            this.mnuPrintBack.Click += new System.EventHandler(this.mnuPrintBack_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuDeleteKennel
            // 
            this.mnuDeleteKennel.Index = -1;
            this.mnuDeleteKennel.Name = "mnuDeleteKennel";
            this.mnuDeleteKennel.Text = "Delete Kennel License";
            this.mnuDeleteKennel.Click += new System.EventHandler(this.mnuDeleteKennel_Click);
            // 
            // mnuAddNewDog
            // 
            this.mnuAddNewDog.Index = -1;
            this.mnuAddNewDog.Name = "mnuAddNewDog";
            this.mnuAddNewDog.Text = "Add New Dog";
            this.mnuAddNewDog.Click += new System.EventHandler(this.mnuAddNewDog_Click);
            // 
            // mnuAddExistingDog
            // 
            this.mnuAddExistingDog.Index = -1;
            this.mnuAddExistingDog.Name = "mnuAddExistingDog";
            this.mnuAddExistingDog.Text = "Add Existing Dog";
            this.mnuAddExistingDog.Click += new System.EventHandler(this.mnuAddExistingDog_Click);
            // 
            // mnuRemoveDog
            // 
            this.mnuRemoveDog.Index = -1;
            this.mnuRemoveDog.Name = "mnuRemoveDog";
            this.mnuRemoveDog.Text = "Remove Dog From Kennel";
            this.mnuRemoveDog.Click += new System.EventHandler(this.mnuRemoveDog_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = -1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(393, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddNewDog
            // 
            this.cmdAddNewDog.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddNewDog.Location = new System.Drawing.Point(451, 29);
            this.cmdAddNewDog.Name = "cmdAddNewDog";
            this.cmdAddNewDog.Size = new System.Drawing.Size(106, 24);
            this.cmdAddNewDog.TabIndex = 1;
            this.cmdAddNewDog.Text = "Add New Dog";
            this.cmdAddNewDog.Click += new System.EventHandler(this.mnuAddNewDog_Click);
            // 
            // cmdDeleteKennelLicense
            // 
            this.cmdDeleteKennelLicense.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteKennelLicense.Location = new System.Drawing.Point(289, 29);
            this.cmdDeleteKennelLicense.Name = "cmdDeleteKennelLicense";
            this.cmdDeleteKennelLicense.Size = new System.Drawing.Size(156, 24);
            this.cmdDeleteKennelLicense.TabIndex = 2;
            this.cmdDeleteKennelLicense.Text = "Delete Kennel License";
            this.cmdDeleteKennelLicense.Click += new System.EventHandler(this.mnuDeleteKennel_Click);
            // 
            // cmdAddExistingDog
            // 
            this.cmdAddExistingDog.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddExistingDog.Location = new System.Drawing.Point(563, 29);
            this.cmdAddExistingDog.Name = "cmdAddExistingDog";
            this.cmdAddExistingDog.Size = new System.Drawing.Size(126, 24);
            this.cmdAddExistingDog.TabIndex = 3;
            this.cmdAddExistingDog.Text = "Add Existing Dog";
            this.cmdAddExistingDog.Click += new System.EventHandler(this.mnuAddExistingDog_Click);
            // 
            // cmdRemoveDogFromKennel
            // 
            this.cmdRemoveDogFromKennel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRemoveDogFromKennel.Location = new System.Drawing.Point(695, 29);
            this.cmdRemoveDogFromKennel.Name = "cmdRemoveDogFromKennel";
            this.cmdRemoveDogFromKennel.Size = new System.Drawing.Size(181, 24);
            this.cmdRemoveDogFromKennel.TabIndex = 4;
            this.cmdRemoveDogFromKennel.Text = "Remove Dog From Kennel";
            this.cmdRemoveDogFromKennel.Click += new System.EventHandler(this.mnuRemoveDog_Click);
            // 
            // frmNewKennel
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(904, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmNewKennel";
            this.Text = "Kennel License";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmNewKennel_Load);
            this.Resize += new System.EventHandler(this.frmNewKennel_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewKennel_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framAddDogs)).EndInit();
            this.framAddDogs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKAddDogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAddDogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kInspectionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framFees)).EndInit();
            this.framFees.ResumeLayout(false);
            this.framFees.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWarrantFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLateFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNewDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteKennelLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddExistingDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveDogFromKennel)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdAddNewDog;
		private FCButton cmdRemoveDogFromKennel;
		private FCButton cmdAddExistingDog;
		private FCButton cmdDeleteKennelLicense;
	}
}