//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptStickerInventory.
	/// </summary>
	public partial class rptStickerInventory : BaseSectionReport
	{
		public rptStickerInventory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Detailed Tag Inventory";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptStickerInventory InstancePtr
		{
			get
			{
				return (rptStickerInventory)Sys.GetInstance(typeof(rptStickerInventory));
			}
		}

		protected rptStickerInventory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
				clsTransactions.Dispose();
				clsKennel.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptStickerInventory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		double dblTotal;
		clsDRWrapper rs = new clsDRWrapper();
		int intPageNumber;
		clsDRWrapper clsTransactions = new clsDRWrapper();
		clsDRWrapper clsKennel = new clsDRWrapper();
		int lngUsed;
		int lngActive;

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string[] strAry = null;
			string strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			clsDRWrapper rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                NextRecord: ;
                if (rs.EndOfFile())
                    return;
                if (Conversion.Val(rs.Get_Fields_Int32("StickerNumber")) > 0)
                {
                    txtTagNumber.Text =
                        fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields("StickerNumber")));
                    if (Conversion.Val(rs.Get_Fields_Int32("ownernum")) == -1)
                    {
                        if (Conversion.Val(rs.Get_Fields_Int32("previousowner")) > 0)
                        {
                            rsTemp.OpenRecordset(
                                "select * from dogowner c CROSS APPLY " + rsTemp.CurrentPrefix +
                                "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'CK', NULL) as p where ID = " +
                                rs.Get_Fields_Int32("previousowner"), "twck0000.vb1");
                            if (!rsTemp.EndOfFile())
                            {
                                txtOwnerName.Text = fecherFoundation.Strings.Trim(rsTemp.Get_Fields_String("FullName"));
                            }
                            else
                            {
                                txtOwnerName.Text = "";
                            }
                        }
                        else
                        {
                            txtOwnerName.Text = "";
                        }
                    }
                    else
                    {
                        txtOwnerName.Text = fecherFoundation.Strings.Trim(
                            rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("MiddleName") + " " +
                            rs.Get_Fields_String("LastName"));
                        // xxkk  "MI"
                    }

                    txtDogName.Text = rs.Get_Fields_String("DogName");
                    txtDate.Text =
                        FCConvert.ToString(Conversion.Val(rs.Get_Fields("Year")) == 0 ? "" : rs.Get_Fields("Year"));
                    txtStatus.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("Status"));
                    if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rs.Get_Fields_String("status"))) ==
                        "USED")
                    {
                        lngUsed += 1;
                    }
                    else if (fecherFoundation.Strings.UCase(
                        fecherFoundation.Strings.Trim(rs.Get_Fields_String("status"))) == "ACTIVE")
                    {
                        lngActive += 1;
                    }

                    if (Conversion.Val(rs.Get_Fields("year")) > 0)
                    {
                        clsTransactions.OpenRecordset(
                            "select * from dogtransactions where (boollicense = 1 or boolreplacementsticker = 1 or convert(int, isnull(latefee, 0)) > 0) and dognumber = " +
                            FCConvert.ToString(Conversion.Val(rs.Get_Fields("DogInfoID"))) +
                            " AND convert(int, isnull(TAGNUMBER, 0)) = " + rs.Get_Fields_Int32("stickernumber") +
                            " order by stickernumber,transactiondate desc", "twck0000.vb1");
                        // If clsTransactions.FindFirstRecord2("dognumber = " & Val(rs.Fields("ID")) & " and tagnumber = " & rs.Fields("stickernumber") & " and (boollicense or boolreplacementsticker or val(latefee & '') > 0)") Then
                        if (clsTransactions.EndOfFile() != true && clsTransactions.BeginningOfFile() != true)
                        {
                            txtFee.Text = Strings.Format(Conversion.Val(clsTransactions.Get_Fields("amount")),
                                "#,###,##0.00");
                            if (Conversion.Val(clsTransactions.Get_Fields("latefee")) > 0)
                            {
                                txtLateFee.Text = "Y";
                            }
                            else
                            {
                                txtLateFee.Text = "N";
                            }

                            dblTotal += Conversion.Val(clsTransactions.Get_Fields("amount"));
                        }
                        else
                        {
                            txtFee.Text = "";
                            txtLateFee.Text = "N";
                        }
                    }
                    else
                    {
                        txtFee.Text = "";
                        txtLateFee.Text = "N";
                    }

                    // txtTagNumber.Text = vbNullString
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_Boolean("kenneldog"))) !=
                        string.Empty)
                    {
                        if (FCConvert.CBool(rs.Get_Fields_Boolean("kenneldog")))
                        {
                            clsKennel.OpenRecordset(
                                "select * from kennellicense where ownernum = " +
                                FCConvert.ToString(rs.Get_Fields_Int32("ownernum")), "twck0000.vb1");
                            if (!clsKennel.EndOfFile())
                            {
                                while (!clsKennel.EndOfFile())
                                {
                                    strTemp = clsKennel.Get_Fields_String("dognumbers");
                                    if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
                                    {
                                        strAry = Strings.Split(strTemp, " ", -1, CompareConstants.vbBinaryCompare);
                                        for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
                                        {
                                            if (x == Conversion.Val(rs.Get_Fields("DogInfoID")))
                                            {
                                                strTemp = "K# " + FCConvert.ToString(
                                                    modGlobalRoutines.GetKennelLicenseNumber(
                                                        clsKennel.Get_Fields_Int32("licenseid")));
                                                txtTagNumber.Text = strTemp;
                                                break;
                                            }
                                        }

                                        // x
                                    }

                                    clsKennel.MoveNext();
                                }
                            }

                            // Else
                            // If Trim(RS.Fields("taglicnum")) <> vbNullString Then
                            // txtTagNumber.Text = RS.Fields("TagLicNum")
                            // End If
                            // 
                        }
                    }
                }

                if (!rs.EndOfFile())
                    rs.MoveNext();
                eArgs.EOF = false;
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " +
                    fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FetchData", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsTemp.Dispose();
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtTown.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "H:mm AM/PM");
			txtReportDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtCaption.Text = "Town Clerk's Report";
			txtCaption2.Text = "Detailed Tag Inventory - " + FCConvert.ToString(DateTime.Now.Year);
			intPageNumber = 1;
			lngUsed = 0;
			lngActive = 0;
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call RS.OpenRecordset("SELECT DogANDOwner.*, DogInventory.* FROM DogInventory LEFT JOIN DogANDOwner ON (DogInventory.Year = DogANDOwner.Year) AND (DogInventory.StickerNumber = val(DogANDOwner.StickerLicNum)) where Type = 'S' AND StickerNumber >= " & gstrBeginSticker & " and StickerNumber <= " & gstrEndSticker & " order by DogInventory.Year,StickerNumber", "TWCK0000.vb1")
			// Call SetPrintProperties(Me)
		}
		// Public Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports2.DDTool)
		// Call VerifyPrintToFile(Me, Tool)
		// End Sub
		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotal.Text = Strings.Format(dblTotal, "Currency");
			txtTotalActive.Text = Strings.Format(lngActive, "#,###,###,##0");
			txtTotalUsed.Text = Strings.Format(lngUsed, "#,###,###,##0");
		}
		// vbPorter upgrade warning: lngStickerStart As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: lngStickerEnd As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: lngYear As int	OnWrite(string)
		public void Init(int lngStickerStart, int lngStickerEnd, int lngYear)
		{
			string strSQL;
			string strQuery;
			// Call clsTransactions.OpenRecordset("select * from dogtransactions where boollicense = 1 and  convert(int, isnull(TAGNUMBER, 0)) > 0  and transactiondate between #1/1/" & lngYear & "# and #12/31/" & lngYear & "# order by stickernumber,transactiondate desc", "twck0000.vb1")
			// strQuery = "(select * from dogINFO LEFT join dogOWNER on (doginfo.ownernum = dogowner.ID) where DELETED <> 1  and convert(int, isnull(stickerlicnum, 0)) > 0 ) as DogQuery"
			// strQuery = "(select p.*, ownernum, previousowner, dogname, [year], doginfo.ID as DogInfoID, kenneldog, taglicnum from dogINFO LEFT join dogOWNER on (doginfo.ownernum = dogowner.ID) CROSS APPLY " & rs.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p where DELETED <> 1 and  convert(int, isnull(taglicnum, 0)) > 0 )  as DogQuery"
			strQuery = "(select p.*, ownernum, previousowner, dogname, [year], doginfo.ID as DogInfoID, kenneldog, taglicnum from dogINFO LEFT join dogOWNER on (doginfo.ownernum = dogowner.ID) LEFT JOIN " + rs.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON dogowner.PartyID = p.ID where DELETED <> 1 and  convert(int, isnull(taglicnum, 0)) > 0 )  as DogQuery";
			strSQL = "select * from doginventory left join " + strQuery + " on (doginventory.stickernumber = convert(int, isnull(dogquery.taglicnum, 0))) and (doginventory.year = dogquery.year) where doginventory.type = 'T' and convert(int, isnull(doginventory.stickernumber, 0)) > 0  and doginventory.stickernumber between " + FCConvert.ToString(lngStickerStart) + " and " + FCConvert.ToString(lngStickerEnd) + " and doginventory.year = " + FCConvert.ToString(lngYear) + " order by doginventory.year,doginventory.stickernumber";
			rs.OpenRecordset(strSQL, "Twck0000.vb1");
			if (rs.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "StickerInventory");
			// Me.Show 
		}

		
	}
}
