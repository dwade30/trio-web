//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	public class modAmendments
	{
		//=========================================================
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static string GetAmendments(clsDRWrapper rsData)
		{
			string GetAmendments = "";
			string strTemp = "";
			while (!rsData.EndOfFile())
			{
				if (rsData.Get_Fields_Boolean("PrintAmendment"))
				{
					string vbPorterVar = rsData.Get_Fields_String("Statement");
					if (vbPorterVar == "* See Attached")
					{
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields("Amendment")) + " -- ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Statement")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter1")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter2")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter3"));
						strTemp += "\r";
					}
					else if (vbPorterVar == "AMENDED")
					{
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields("Amendment")) + " -- ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Statement")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter1")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter2")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter3")) + " ";
						strTemp += "\r";
					}
					else if (vbPorterVar == "Baptismal")
					{
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields("Amendment")) + " -- ";
						strTemp += "Amended Item ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter1")) + ", ";
						strTemp += "baptismal record ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter2")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter3")) + " ";
						strTemp += "\r";
					}
					else if (vbPorterVar == "Affidavit")
					{
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields("Amendment")) + " -- ";
						strTemp += "Item ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter1")) + " ";
						strTemp += "personal affidavit ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter2")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter3")) + " ";
						strTemp += "\r";
					}
					else if (vbPorterVar == "OTHER")
					{
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields("Amendment")) + " -- ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter1")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter2")) + " ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter3")) + " ";
						strTemp += "\r";
					}
					else if (vbPorterVar == "Legal Name Change")
					{
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields("Amendment")) + " -- ";
						strTemp += "Legal Name Change (";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("Parameter1")) + ")";
						strTemp += ", Decree Dated ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("parameter2"));
						strTemp += ", Date Amended ";
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields_String("parameter3")) + " ";
						strTemp += "\r";
					}
					else
					{
						strTemp += fecherFoundation.Strings.Trim(rsData.Get_Fields("Amendment"));
						strTemp += "\r";
					}
				}
				rsData.MoveNext();
			}
			GetAmendments = strTemp;
			return GetAmendments;
		}
		// vbPorter upgrade warning: GridName As object	OnWrite(FCGridCtl.VSFlexGrid)
		// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
		public static void UpdateAmendmentTooltips(FCGrid GridName, int intRow, string EditText = "")
		{
            int row = intRow - GridName.FixedRows;
            if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "* SEE ATTACHED") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "0"))
			{
				//GridName.TextMatrix(intRow, 7, " ");
                GridName[4, row].ToolTipText = "";
                GridName[5, row].ToolTipText = "";
                GridName[6, row].ToolTipText = "";
            }
			else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "AMENDED") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "1"))
			{
				//GridName.TextMatrix(intRow, 7, "AMENDED (MM/DD/YYYY)");
                GridName[4, row].ToolTipText = "AMENDED (MM/DD/YYYY)";
                GridName[5, row].ToolTipText = "";
                GridName[6, row].ToolTipText = "";
            }
			else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "BAPTISMAL") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "2"))
			{
				//GridName.TextMatrix(intRow, 7, "Amended Item (#/#), baptismal record (MM/DD/YYYY)");
                GridName[4, row].ToolTipText = "Amended Item (#/#)";
                GridName[5, row].ToolTipText = "baptismal record (MM/DD/YYYY)";
                GridName[6, row].ToolTipText = "";
            }
			else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "AFFIDAVIT") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "3"))
			{
				//GridName.TextMatrix(intRow, 7, "Item (#/#), personal affidavit (MM/DD/YYYY)");
                GridName[4, row].ToolTipText = "Item (#/#)";
                GridName[5, row].ToolTipText = "personal affidavit (MM/DD/YYYY)";
                GridName[6, row].ToolTipText = "";
            }
			else if ((fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "LEGAL NAME CHANGE") || (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(EditText)) == "4"))
			{
                //GridName.TextMatrix(intRow, 7, "Court Name & Location, Decree Dated (MM/DD/YYYY), Date Amended (MM/DD/YYYY)");
                GridName[4, row].ToolTipText = "Court Name & Location";
                GridName[5, row].ToolTipText = "Decree Dated (MM/DD/YYYY)";
                GridName[6, row].ToolTipText = "Date Amended (MM/DD/YYYY)";
            }
			else
			{
                //GridName.TextMatrix(intRow, 7, " ");
                GridName[4, row].ToolTipText = "";
                GridName[5, row].ToolTipText = "";
                GridName[6, row].ToolTipText = "";
            }
		}
		// vbPorter upgrade warning: GridName As object	OnWrite(FCGridCtl.VSFlexGrid)
		// vbPorter upgrade warning: intVal As int	OnWriteFCConvert.ToInt32(
		public static void LoadAmendments(ref FCGrid GridName, int intVal, string strType)
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			SetGridParameters(ref GridName);
			rsData.OpenRecordset("Select * from Amendments where Type = '" + strType + "' and AmendmentID = '" + FCConvert.ToString(intVal) + "'", modGNBas.DEFAULTCLERKDATABASE);
			intCounter = 1;
			while (!rsData.EndOfFile())
			{
				GridName.TextMatrix(intCounter, 0, rsData.Get_Fields_Int32("ID"));
				GridName.TextMatrix(intCounter, 1, rsData.Get_Fields("Amendment"));
				GridName.TextMatrix(intCounter, 2, rsData.Get_Fields_Boolean("PrintAmendment"));
				GridName.TextMatrix(intCounter, 3, rsData.Get_Fields_String("Statement"));
				// GridName.TextMatrix(intCounter, 7) = rsData.Fields("statement")
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("statement")))) == "* SEE ATTACHED")
				{
					GridName.TextMatrix(intCounter, 7, " ");
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("statement")))) == "AMENDED")
				{
					GridName.TextMatrix(intCounter, 7, "AMENDED (MM/DD/YYYY)");
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("statement")))) == "BAPTISMAL")
				{
					GridName.TextMatrix(intCounter, 7, "Amended Item (#/#), baptismal record (MM/DD/YYYY)");
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("statement")))) == "AFFIDAVIT")
				{
					GridName.TextMatrix(intCounter, 7, "Item (#/#), personal affidavit (MM/DD/YYYY)");
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("statement")))) == "LEGAL NAME CHANGE")
				{
					GridName.TextMatrix(intCounter, 7, "Court Name & Location, Decree Dated (MM/DD/YYYY), Date Amended (MM/DD/YYYY)");
				}
				else
				{
					GridName.TextMatrix(intCounter, 7, " ");
				}
				GridName.TextMatrix(intCounter, 4, rsData.Get_Fields_String("Parameter1"));
				GridName.TextMatrix(intCounter, 5, rsData.Get_Fields_String("Parameter2"));
				GridName.TextMatrix(intCounter, 6, rsData.Get_Fields_String("Parameter3"));
				intCounter += 1;
				rsData.MoveNext();
			}
		}
		// vbPorter upgrade warning: GridName As object	OnWrite(FCGridCtl.VSFlexGrid)
		// vbPorter upgrade warning: intVal As int	OnWrite(int, string)
		public static void SaveAmendments(ref FCGrid GridName, int intVal, string strType)
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			GridName.Select(0, 0);
			// DELETE ALL RECORDS FOR THIS USER AND THIS TYPE
			rsData.Execute("Delete from Amendments where Type = '" + strType + "' and AmendmentID = '" + FCConvert.ToString(intVal) + "'", modGNBas.DEFAULTCLERKDATABASE);
			// OPEN UP THE RECORDSET FOR THIS USER AND THIS TYPE
			rsData.OpenRecordset("Select * from Amendments where Type = '" + strType + "' and AmendmentID = '" + FCConvert.ToString(intVal) + "'", modGNBas.DEFAULTCLERKDATABASE);
			for (intCounter = 1; intCounter <= GridName.Rows - 1; intCounter++)
			{
				if (GridName.TextMatrix(intCounter, 1) != string.Empty || GridName.TextMatrix(intCounter, 3) != string.Empty)
				{
					// CHECK TO SEE IF THERE IS AN ID FOR THIS RECORD
					if (Conversion.Val(GridName.TextMatrix(intCounter, 0)) == 0)
					{
						rsData.AddNew();
					}
					else
					{
						// CHECK TO SEE IF THIS IS A NEW RECORD OR AN EDIT
						rsData.FindFirstRecord("ID", Conversion.Val(GridName.TextMatrix(intCounter, 0)));
						if (rsData.NoMatch)
						{
							rsData.AddNew();
						}
						else
						{
							rsData.Edit();
						}
					}
					// UPDATE THE RECORDSET
					rsData.Set_Fields("Amendment", fecherFoundation.Strings.Trim(GridName.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)));
					if (fecherFoundation.Strings.Trim(GridName.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2)) == string.Empty)
						GridName.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2, false);
					rsData.Set_Fields("PrintAmendment", fecherFoundation.Strings.Trim(GridName.Cell(FCGrid.CellPropertySettings.flexcpText, intCounter, 2)));
					rsData.Set_Fields("Statement", fecherFoundation.Strings.Trim(GridName.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 3)));
					rsData.Set_Fields("Parameter1", fecherFoundation.Strings.Trim(GridName.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 4)));
					rsData.Set_Fields("Parameter2", fecherFoundation.Strings.Trim(GridName.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 5)));
					rsData.Set_Fields("Parameter3", fecherFoundation.Strings.Trim(GridName.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 6)));
					rsData.Set_Fields("AmendmentID", intVal);
					rsData.Set_Fields("Type", strType);
					// SAVE TO THE DATABASE
					rsData.Update();
				}
			}
		}

		public static void SetGridParameters(ref FCGrid GridName)
		{
			int GridWidth = 0;
			GridWidth = GridName.WidthOriginal;
			GridName.Cols = 8;
			GridName.ColHidden(0, true);
			GridName.ColHidden(7, true);
			GridName.TextMatrix(0, 1, "Amendment");
			// .ColWidth(1) = 3000
			GridName.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridName.TextMatrix(0, 2, "Print");
			GridName.ColDataType(2, FCGrid.DataTypeSettings.flexDTBoolean);
			// .ColWidth(2) = 500
			GridName.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridName.TextMatrix(0, 3, "Statement");
			// .ColWidth(3) = 1600
			GridName.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//GridName.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			GridName.ColComboList(3, "#0;* See Attached" + "\t" + "  |#1;AMENDED" + "\t" + "AMENDED (MM/DD/YYYY)|#2;Baptismal" + "\t" + "Amended Item (#/#), baptismal record (MM/DD/YYYY)|#3;Affidavit" + "\t" + "Item (#/#), personal affidavit (MM/DD/YYYY)|#5;Legal Name Change" + "\t" + "(Court Name & Location), Decree Dated (MM/DD/YYYY), Date Amended (MM/DD/YYYY)|#4;OTHER" + "\t" + "");
			GridName.ColComboList(7, "#0; |#1;AMENDED (MM/DD/YYYY)|#2;Amended Item (#/#), baptismal record (MM/DD/YYYY)|#3;Item (#/#), personal affidavit (MM/DD/YYYY)|#5;Legal Name Change (Court Name & Location), decree dated (MM/DD/YYYY), date amended (MM/DD/YYYY)|#4; ");
			// .ColComboList(3, "#0;* See Attached" & vbTab & "  |#1;AMENDED" & vbTab & "AMENDED (MM/DD/YYYY)|#2;Baptismal" & vbTab & "Amended Item (#/#), baptismal record (MM/DD/YYYY)|#3;Affidavit" & vbTab & "Item (#/#), personal affidavit (MM/DD/YYYY)|#4;OTHER" & vbTab & ""
			GridName.TextMatrix(0, 4, "Parameter 1");
			// .ColWidth(4) = 1000
			//GridName.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			GridName.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridName.TextMatrix(0, 5, "Parameter 2");
			// .ColWidth(5) = 1000
			//GridName.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			GridName.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridName.TextMatrix(0, 6, "Parameter 3");
			// .ColWidth(6) = 1000
			//GridName.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 6, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			GridName.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridWidth = GridName.WidthOriginal;
			GridName.ColWidth(1, FCConvert.ToInt32(0.26 * GridWidth));
			GridName.ColWidth(2, FCConvert.ToInt32(0.08 * GridWidth));
			GridName.ColWidth(3, FCConvert.ToInt32(0.21 * GridWidth));
			GridName.ColWidth(4, FCConvert.ToInt32(0.15 * GridWidth));
			GridName.ColWidth(5, FCConvert.ToInt32(0.15 * GridWidth));
			GridName.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}
	}
}
