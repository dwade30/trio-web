//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.ObjectModel;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication;
using SharedApplication.CentralDocuments;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCK0000
{
	public partial class frmNewDog : BaseForm
	{
		public frmNewDog()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			lblLabels = new System.Collections.Generic.List<FCLabel>();
			lblLabels.AddControlArrayElement(lblLabels_1, 0);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmNewDog InstancePtr
		{
			get
			{
				return (frmNewDog)Sys.GetInstance(typeof(frmNewDog));
			}
		}

		protected frmNewDog _InstancePtr = null;
		//=========================================================
		int lngDogNumber;
		int lngOwnerNumber;
		double dblTranAmount1;
		double dblTranAmount2;
		double dblTranAmount3;
		double dblTranAmount4;
		double dblTranAmount5;
		double dblTranAmount6;
		modGNBas.typDogTransactionRecord CurDogTrans = new modGNBas.typDogTransactionRecord();
		modGNBas.typTransactionRecord curTrans = new modGNBas.typTransactionRecord();
		bool boolMadeTrans;
		bool boolReRegistration;
		bool boolSaved;
		bool boolChangedData;
		bool boolOKToValidate;
		bool boolNewDog;
		// Dim lngOrigStick As Long
		bool boolKennelDog;
		bool boolPrinted;
		string strLogOutput = "";
		bool boolWarrant;
		bool boolChangedBreed;
		bool boolChangedVet;
		string strReturnName = "";
		bool boolReadOnly;

		private void imgDocuments_Click(object sender, System.EventArgs e)
		{
			if (lngDogNumber <= 0)
				return;
			if (imgDocuments.Visible == true)
			{
				ViewDocs();
			}
		}

		private void ViewDocs()
		{
			if (lngDogNumber <= 0) return;

   //         frmViewDocuments.InstancePtr.Unload();
			//frmViewDocuments.InstancePtr.Init(0,"",FCConvert.ToString(modGNBas.CNSTTYPEDOGS),lngDogNumber,"", "twck0000.vb1", FCForm.FormShowEnum.Modal, boolReadOnly);
			//if (modGlobalRoutines.DocListHasItems())
			//{
			//	imgDocuments.Visible = true;
			//	imgDocuments.Image = MDIParent.InstancePtr.ImageList2.Images[0];
			//}
			//else
			//{
			//	imgDocuments.Visible = false;
			//}
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer("", "Clerk", "Dog",
                lngDogNumber, "", true, boolReadOnly));
            var documentHeaders = StaticSettings.GlobalCommandDispatcher.Send(new GetHeadersByReferenceCommand()).Result;
            if (documentHeaders.Count > 0)
            {
                imgDocuments.Visible = true;
                imgDocuments.Image = MDIParent.InstancePtr.ImageList2.Images[0];
            }
            else
            {
                imgDocuments.Visible = false;
            }
        }
		// vbPorter upgrade warning: lngdognum As int	OnWrite(string, int)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool, int)
		public (bool outcome,int dogId) Init(int lngdognum, int lngOwnerNum, bool boolReReg = false, bool boolFromKennel = false, int lngdyear = 0, bool boolCanChangeIssueDate = true, string strDName = "")
		{
			bool Init = false;
			// returns true if a transaction was made
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				boolChangedBreed = false;
				boolChangedVet = false;
				if (lngdognum == 0)
					boolNewDog = true;
				boolWarrant = false;
				boolPrinted = false;
				// lngOrigStick = 0
				boolOKToValidate = false;
				boolReRegistration = boolReReg;
				boolKennelDog = boolFromKennel;
				boolSaved = false;
				boolMadeTrans = false;
				lngDogNumber = lngdognum;
				lngOwnerNumber = lngOwnerNum;
				//FC:FINAL:BB:#i1981 - fix checkbox, when form is not loaded no CheckedChanged event is thrown as in vb6
				if (!this.IsLoaded)
				{
					this.LoadForm();
				}
				CheckPermissions();
				clsLoad.OpenRecordset("select * from dogowner where ID = " + FCConvert.ToString(lngOwnerNum), "Clerk");
				if (!clsLoad.EndOfFile())
				{
					cPartyController pCont = new cPartyController();
					cParty part = new cParty();
					part = pCont.GetParty(clsLoad.Get_Fields_Int32("PartyID"), true);
					if (!(part == null))
					{
						lblOwnerName.Text = part.FullName;
					}
				}
				CurDogTrans.TransactionDate = modGNBas.Statics.typTransaction.TransactionDate;
				curTrans.TransactionDate = modGNBas.Statics.typTransaction.TransactionDate;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				UpdateBreeds();
				UpdateColors();
				UpdateVets();
				modDogs.GetDogFees();
				if (Conversion.Val(modDogs.Statics.DogFee.UserDefinedFee1) > 0)
				{
					chkUserDefinedFee.Visible = true;
					if (fecherFoundation.Strings.Trim(modDogs.Statics.DogFee.UserDefinedDescription1) != string.Empty)
					{
						chkUserDefinedFee.Text = fecherFoundation.Strings.Trim(modDogs.Statics.DogFee.UserDefinedDescription1);
					}
					lblLabels[0].LeftOriginal = chkTempLicense.LeftOriginal;
					txtAmount.LeftOriginal = lblLabels[0].LeftOriginal + lblLabels[0].WidthOriginal + 50;
				}
				else
				{
					chkUserDefinedFee.Visible = false;
				}
				if (lngDogNumber > 0)
				{
					LoadDogInfo();
				}
				else
				{
					// new dog
					ClearControls();
				}
				Init = false;
				if (boolKennelDog)
				{
					chkFees.CheckState = Wisej.Web.CheckState.Unchecked;
					chkFees.Enabled = false;
					boolChangedData = false;
					if (lngdyear > 0)
					{
						if (Conversion.Val(txtYear.Text) > 0)
						{
							if (Conversion.Val(txtYear.Text) != lngdyear)
							{
								// lngOrigStick = 0
								// txtSticker.Text = ""
								T2KIssueDate.Text = Strings.Format(modGNBas.Statics.typTransaction.TransactionDate, "MM/dd/yyyy");
								T2KIssueDate.Enabled = boolCanChangeIssueDate;
								boolChangedData = true;
							}
						}
						txtYear.Text = FCConvert.ToString(lngdyear);
						txtYear.Enabled = false;
					}
					if (boolReRegistration)
					{
						boolChangedData = true;
						T2KIssueDate.Text = Strings.Format(modGNBas.Statics.typTransaction.TransactionDate, "MM/dd/yyyy");
						T2KIssueDate.Enabled = false;
					}
					txtTag.Text = "";
					txtTag.Enabled = false;
					if (lngDogNumber == 0)
					{
						boolChangedData = true;
						T2KIssueDate.Text = Strings.Format(modGNBas.Statics.typTransaction.TransactionDate, "MM/dd/yyyy");
						T2KIssueDate.Enabled = false;
					}
				}
				else if (boolReRegistration)
				{
					chkNotRequired.CheckState = Wisej.Web.CheckState.Unchecked;
					boolChangedData = true;
					chkFees.CheckState = Wisej.Web.CheckState.Checked;
					chkFees.Enabled = false;
					T2KIssueDate.Text = Strings.Format(modGNBas.Statics.typTransaction.TransactionDate, "MM/dd/yyyy");
					T2KIssueDate.Enabled = boolCanChangeIssueDate;
					chkLicense.CheckState = Wisej.Web.CheckState.Checked;
					chkLicense.Enabled = false;
					ClearReRegFields();
					if (FCConvert.ToDateTime(T2KIssueDate.Text).Month > 10 || (FCConvert.ToDateTime(T2KIssueDate.Text).Month == 10 && FCConvert.ToDateTime(T2KIssueDate.Text).Day >= 15))
					{
						txtYear.Text = FCConvert.ToString(FCConvert.ToDateTime(T2KIssueDate.Text).Year + 1);
					}
					else
					{
						if (Conversion.Val(txtYear.Text) <= FCConvert.ToDateTime(T2KIssueDate.Text).Year)
						{
							if (FCConvert.ToDateTime(T2KIssueDate.Text).Month >= 2)
							{
								if (!boolWarrant)
								{
									chkLateFee.CheckState = Wisej.Web.CheckState.Checked;
								}
								else
								{
									chkWarrantFee.CheckState = Wisej.Web.CheckState.Checked;
								}
							}
						}
					}
				}
				else
				{
					chkFees.Enabled = true;
					if (lngDogNumber == 0)
					{
						boolChangedData = true;
						chkFees.CheckState = Wisej.Web.CheckState.Checked;
						chkLicense.CheckState = Wisej.Web.CheckState.Checked;
						T2KIssueDate.Text = Strings.Format(modGNBas.Statics.typTransaction.TransactionDate, "MM/dd/yyyy");
						T2KIssueDate.Enabled = boolCanChangeIssueDate;
						if (FCConvert.ToDateTime(T2KIssueDate.Text).Month > 10 || (FCConvert.ToDateTime(T2KIssueDate.Text).Month == 10 && FCConvert.ToDateTime(T2KIssueDate.Text).Day >= 15))
						{
							txtYear.Text = FCConvert.ToString(FCConvert.ToDateTime(T2KIssueDate.Text).Year + 1);
						}
					}
				}
				boolOKToValidate = true;
                this.Show(FCForm.FormShowEnum.Modal);
				strDName = strReturnName;
				return (outcome:boolMadeTrans && boolSaved,dogId:lngDogNumber);
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return (outcome:Init,dogId:lngDogNumber);
		}

		private void ClearReRegFields()
		{
			// vbPorter upgrade warning: dtTemp1 As DateTime	OnWrite(string)
			DateTime dtTemp1;
			// vbPorter upgrade warning: dtTemp2 As DateTime	OnWrite(string)
			DateTime dtTemp2;
			if (Conversion.Val(txtYear.Text) == 0)
			{
				txtYear.Text = FCConvert.ToString(FCConvert.ToDateTime(T2KIssueDate.Text).Year);
			}
			else
			{
				txtYear.Text = FCConvert.ToString(Conversion.Val(txtYear.Text) + 1);
				if (Conversion.Val(txtYear.Text) < FCConvert.ToDateTime(T2KIssueDate.Text).Year)
				{
					txtYear.Text = FCConvert.ToString(FCConvert.ToDateTime(T2KIssueDate.Text).Year);
				}
			}

            if (!chkDangerous.Checked)
            {
                txtPrevTag.Text = txtTag.Text;
                txtTag.Text = "";
            }

			if (Information.IsDate(t2kExpirationDate.Text))
			{
				dtTemp1 = FCConvert.ToDateTime(Strings.Format(t2kExpirationDate.Text, "MM/dd/yyyy"));
				dtTemp2 = FCConvert.ToDateTime(Strings.Format(T2KIssueDate.Text, "MM/dd/yyyy"));
				if (fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtTemp2) >= 0)
				{
					txtRabiesTag.Text = "";
					t2kExpirationDate.Text = "";
					T2KDateVaccinated.Text = "";
				}
			}
			else
			{
				txtRabiesTag.Text = "";
				t2kExpirationDate.Text = "";
				T2KDateVaccinated.Text = "";
			}
		}

		private void ClearControls()
		{
			ImgComment.Image = null;
			// lngOrigStick = 0
			txtAmount.Text = "";
			txtCertificateNumber.Text = "";
			txtDogName.Text = "";
			txtMarkings.Text = "";
			txtRabiesTag.Text = "";
			txtSex.Text = "";
			txtSpayNeuterNumber.Text = "";
			// txtSticker.Text = ""
			txtPrevTag.Text = "";
			txtTag.Text = "";
			cmbVet.SelectedIndex = -1;
			if (DateTime.Today.Month < 11)
			{
				txtYear.Text = FCConvert.ToString(DateTime.Today.Year);
			}
			else
			{
				txtYear.Text = FCConvert.ToString(DateTime.Today.Year + 1);
			}
			T2KDateVaccinated.Text = "";
			T2KDOB.Text = "";
			t2kExpirationDate.Text = "";
			T2KIssueDate.Text = "";
			cmbBreed.SelectedIndex = -1;
			cmbColor.SelectedIndex = -1;
			chkDeceased.CheckState = Wisej.Web.CheckState.Unchecked;
			chkHearingGuid.CheckState = Wisej.Web.CheckState.Unchecked;
			chkLateFee.CheckState = Wisej.Web.CheckState.Unchecked;
			chkLicense.CheckState = Wisej.Web.CheckState.Unchecked;
			chkNeuterSpay.CheckState = Wisej.Web.CheckState.Unchecked;
			chkNotRequired.CheckState = Wisej.Web.CheckState.Unchecked;
			chkReplacementLicense.CheckState = Wisej.Web.CheckState.Unchecked;
			chkReplacementSticker.CheckState = Wisej.Web.CheckState.Unchecked;
			chkReplacementTag.CheckState = Wisej.Web.CheckState.Unchecked;
			chkSearchRescue.CheckState = Wisej.Web.CheckState.Unchecked;
			chkTempLicense.CheckState = Wisej.Web.CheckState.Unchecked;
			chkTransferLicense.CheckState = Wisej.Web.CheckState.Unchecked;
			chkWarrantFee.CheckState = Wisej.Web.CheckState.Unchecked;
			chkWolfHybrid.CheckState = Wisej.Web.CheckState.Unchecked;
			ToggleBackGroundsNonRequired();
			boolChangedBreed = false;
			boolChangedVet = false;
            chkDangerous.CheckState = CheckState.Unchecked;
            chkNuisance.CheckState = CheckState.Unchecked;
        }

		private void LoadDogInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strReturnName = "";
				clsLoad.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("comments"))) != string.Empty)
					{
						ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
					}
					else
					{
						ImgComment.Image = null;
					}
					txtDogName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("dogname")));
					strReturnName = txtDogName.Text;
					txtSex.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("dogsex")));
					txtMarkings.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("dogmarkings")));
					cmbVet.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("dogvet")));
					cmbBreed.Text = FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed"));
					cmbColor.Text = FCConvert.ToString(clsLoad.Get_Fields_String("dogcolor"));
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("ExcludeFromWarrant")))
					{
						chkExcludeWarrant.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkExcludeWarrant.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("RabiesExempt")))
					{
						chkRabiesExempt.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkRabiesExempt.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					boolWarrant = false;
					if (Conversion.Val(clsLoad.Get_Fields_Int32("warrantyear")) == DateTime.Today.Year)
					{
						if (Conversion.Val(clsLoad.Get_Fields("Year")) < DateTime.Today.Year)
						{
							boolWarrant = true;
						}
					}
					txtYear.Text = FCConvert.ToString(clsLoad.Get_Fields("year"));
					txtSpayNeuterNumber.Text = FCConvert.ToString(clsLoad.Get_Fields_String("sncertnum"));
					txtCertificateNumber.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rabiescertnum"));
					txtRabiesTag.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rabiestagno"));
					// txtSticker.Text = clsLoad.Fields("stickerlicnum")
					txtPrevTag.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Previoustag"));
					// lngOrigStick = Val(clsLoad.Fields("stickerlicnum"))
					txtTag.Text = FCConvert.ToString(clsLoad.Get_Fields_String("taglicnum"));
					if (Information.IsDate(clsLoad.Get_Fields("dogdob")))
					{
						if (clsLoad.Get_Fields_DateTime("dogdob").ToOADate() != 0)
						{
							T2KDOB.Text = Strings.Format(clsLoad.Get_Fields_DateTime("dogdob"), "MM/dd/yyyy");
						}
						else
						{
							T2KDOB.Text = "";
						}
					}
					else
					{
						T2KDOB.Text = "";
					}
					if (Information.IsDate(clsLoad.Get_Fields("rabiesshotdate")))
					{
						if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("rabiesshotdate")).ToOADate() != 0)
						{
							T2KDateVaccinated.Text = Strings.Format(clsLoad.Get_Fields_DateTime("rabiesshotdate"), "MM/dd/yyyy");
						}
						else
						{
							T2KDateVaccinated.Text = "";
						}
					}
					else
					{
						T2KDateVaccinated.Text = "";
					}
					if (Information.IsDate(clsLoad.Get_Fields("rabstickerissue")))
					{
						if (clsLoad.Get_Fields_DateTime("rabstickerissue").ToOADate() != 0)
						{
							T2KIssueDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("rabstickerissue"), "MM/dd/yyyy");
						}
						else
						{
							T2KIssueDate.Text = "";
						}
					}
					else
					{
						T2KIssueDate.Text = "";
					}
					if (Information.IsDate(clsLoad.Get_Fields("rabiesexpdate")))
					{
						if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("rabiesexpdate")).ToOADate() != 0)
						{
							t2kExpirationDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("rabiesexpdate"), "MM/dd/yyyy");
						}
						else
						{
							t2kExpirationDate.Text = "";
						}
					}
					else
					{
						t2kExpirationDate.Text = "";
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("dogneuter")))
					{
						chkNeuterSpay.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkNeuterSpay.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("sandr")))
					{
						chkSearchRescue.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkSearchRescue.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("hearguide")))
					{
						chkHearingGuid.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkHearingGuid.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("wolfhybrid")))
					{
						chkWolfHybrid.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkWolfHybrid.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("DOGDECEASED")))
					{
						chkDeceased.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDeceased.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("norequiredfields")))
					{
						chkNotRequired.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkNotRequired.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (clsLoad.Get_Fields_Boolean("OutsideSource"))
					{
						chkOutsideSource.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkOutsideSource.CheckState = Wisej.Web.CheckState.Unchecked;
					}

                    if (clsLoad.Get_Fields_Boolean("IsDangerousDog"))
                    {
                        chkDangerous.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        chkDangerous.CheckState = CheckState.Unchecked;
                    }

                    if (clsLoad.Get_Fields_Boolean("IsNuisanceDog"))
                    {
                        chkNuisance.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        chkNuisance.CheckState = CheckState.Unchecked;
                    }
				}
				else
				{
					ClearControls();
				}
				boolChangedBreed = false;
				boolChangedVet = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadDogInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void chkDeceased_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkFees_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
			{
				framTransaction.Visible = true;
			}
			else
			{
				framTransaction.Visible = false;
				RecalcAmount();
			}
		}

		private void chkHearingGuid_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkLateFee_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
			if (lngDogNumber > 0)
			{
				if (chkLicense.CheckState == Wisej.Web.CheckState.Checked && !boolReRegistration)
				{
					if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
					{
						// check for a previous registration for this year
						clsDRWrapper rsLoad = new clsDRWrapper();
						int lngdyear = 0;
						switch (DateTime.Today.Month)
						{
							case 1:
							case 2:
							case 3:
							case 4:
							case 5:
							case 6:
							case 7:
							case 8:
							case 9:
								{
									lngdyear = DateTime.Today.Year;
									break;
								}
							case 10:
							case 11:
							case 12:
								{
									lngdyear = DateTime.Today.Year + 1;
									break;
								}
						}
						//end switch
						if (Conversion.Val(txtYear.Text) > lngdyear)
						{
							lngdyear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
						}
						rsLoad.OpenRecordset("select * from dogtransactions WHERE DOGNUMBER = " + FCConvert.ToString(lngDogNumber) + " and dogyear = " + FCConvert.ToString(lngdyear) + " and boollicense = 1", "twck0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							MessageBox.Show("There is already a transaction found licensing this dog for the year " + FCConvert.ToString(lngdyear), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
			}
		}

		private void chkNeuterSpay_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
			ToggleBackGroundsNonRequired();
		}

		private void chkNotRequired_CheckedChanged(object sender, System.EventArgs e)
		{
			// If chkNotRequired.Value = vbChecked Then
			// BackGroundsNonRequired (True)
			// Else
			// MakeBackGroundsNonRequired (False)
			// End If
			ToggleBackGroundsNonRequired();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkOutsideSource_CheckedChanged(object sender, System.EventArgs e)
		{
			ToggleBackGroundsNonRequired();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkReplacementLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkReplacementSticker_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkReplacementTag_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkSearchRescue_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkTempLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkTransferLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkUserDefinedFee_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkWarrantFee_CheckedChanged(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			RecalcAmount();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void chkWolfHybrid_CheckedChanged(object sender, System.EventArgs e)
		{
			ToggleBackGroundsNonRequired();
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void cmbVet_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
				boolChangedVet = true;
			}
		}

		private void cmbVet_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			boolChangedVet = false;
		}

		private void cmbBreed_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
				boolChangedBreed = true;
			}
		}

		private void cmbBreed_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			boolChangedBreed = false;
			// picked it from list
		}

		private void cmbVet_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (boolChangedVet)
			{
				boolChangedVet = false;
				clsDRWrapper clsSave = new clsDRWrapper();
				clsSave.OpenRecordset("select * from veterinarians where vetname = '" + modGlobalFunctions.EscapeQuotes(cmbVet.Text) + "'", "twck0000.vb1");
				if (clsSave.EndOfFile())
				{
					if (MessageBox.Show("This veterinarian is not found in your list of veterinarians" + "\r\n" + "Do you wish to add it?", "Add New Vet?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						clsSave.AddNew();
						clsSave.Set_Fields("VetName", cmbVet.Text);
						cmbVet.AddItem(cmbVet.Text);
						cmbVet.ItemData(cmbVet.NewIndex, FCConvert.ToInt32(clsSave.Get_Fields_Int32("id")));
						clsSave.Update();
					}
				}
			}
		}

		private void cmbBreed_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (boolChangedBreed)
			{
				boolChangedBreed = false;
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from defaultbreeds where name = '" + modGlobalFunctions.EscapeQuotes(cmbBreed.Text) + "'", "twck0000.vb1");
				if (rsLoad.EndOfFile())
				{
					if (MessageBox.Show("This breed is not found in your list of breeds" + "\r\n" + "Do you wish to add it?", "Add New Breed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						rsLoad.AddNew();
						rsLoad.Set_Fields("Name", cmbBreed.Text);
						rsLoad.Set_Fields("LastUpdated", DateTime.Now);
						cmbBreed.AddItem(cmbBreed.Text);
						cmbBreed.ItemData(cmbBreed.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id")));
						rsLoad.Update();
					}
				}
			}
		}

		private void cmbColor_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void cmdCancelSticker_Click(object sender, System.EventArgs e)
		{
			framStickerInventory.Visible = false;
		}

		private void frmNewDog_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
			cmbColor.SelectionLength = 0;
			cmbBreed.SelectionLength = 0;
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
        }

		private void frmNewDog_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						Support.SendKeys("{TAB}", false);
						//App.DoEvents();
						break;
					}
			}
			//end switch
		}

		private void frmNewDog_Load(object sender, System.EventArgs e)
		{

			foreach (Control ctl in this.GetAllControls())
			{
				if (FCConvert.ToString(ctl.Tag) == "Required")
				{
					ctl.BackColor = ColorTranslator.FromOle(StaticSettings.TrioColorSettings.TRIOCOLORHIGHLIGHT);
				}
			}
			// ctl
			if (lngDogNumber > 0)
            {
                mnuViewDocs.Enabled = true;
                imgDocuments.Visible = CheckForDocs(lngDogNumber, "Dog");
            }
			else
			{
				imgDocuments.Visible = false;
				mnuViewDocs.Enabled = false;
			}
        }

        public bool CheckForDocs(int referenceId, string referenceType)
        {
            var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
            var docList = new Collection<CentralDocumentHeader>();

            docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand { DataGroup = "Clerk", ReferenceId = referenceId, ReferenceType = referenceType });

            return docList.Count > 0;
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (!boolReadOnly)
			{
				if (boolChangedData)
				{
					if (MessageBox.Show("Data has been changed but not saved, do you wish to save now?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (!SaveData())
						{
							e.Cancel = true;
							return;
						}
					}
				}
			}
			if (modCashReceiptingData.Statics.bolFromWindowsCR && !boolMadeTrans)
			{
				if (MessageBox.Show("Clerk was entered from cash receipting but you have not charged any fees" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
				{
					e.Cancel = true;
					return;
				}
			}
		}

		private void GridStickerInventory_DblClick(object sender, System.EventArgs e)
		{
			if (GridStickerInventory.MouseRow >= 0)
			{
				txtTag.Text = GridStickerInventory.TextMatrix(GridStickerInventory.MouseRow, GridStickerInventory.MouseCol);
				framStickerInventory.Visible = false;
			}
		}

		private void lblComment_DblClick()
		{
			mnuComments_Click(null, null);
		}

		private void mnuBreeds_Click(object sender, System.EventArgs e)
		{
			int lngBreed = 0;
			frmDogInfo.InstancePtr.Init(true, 1);
			if (cmbBreed.SelectedIndex > -1)
			{
				lngBreed = cmbBreed.ItemData(cmbBreed.SelectedIndex);
				UpdateBreeds();
				AssignBreed(ref lngBreed);
			}
			else
			{
				UpdateBreeds();
			}
		}

		private void mnuVets_Click(object sender, System.EventArgs e)
		{

		}

		private void mnuColors_Click(object sender, System.EventArgs e)
		{
			int lngColor = 0;
			frmDogInfo.InstancePtr.Init(true, 0);
			if (cmbColor.SelectedIndex > -1)
			{
				lngColor = cmbColor.ItemData(cmbColor.SelectedIndex);
				UpdateColors();
				AssignColor(ref lngColor);
			}
			else
			{
				UpdateColors();
			}
		}

		private void mnuComments_Click(object sender, System.EventArgs e)
		{
			if (lngDogNumber == 0)
			{
				MessageBox.Show("This dog does not exist yet.  You must save before you can tie a comment to this record.", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (frmCommentOld.InstancePtr.Init("CK", "DogInfo", "Comments", "ID", lngDogNumber, boolNonEditable: boolReadOnly, boolModal: true))
			{
				ImgComment.Image = null;
			}
			else
			{
				ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
            string strTag = "";
			int lngYear = 0;
            var boolDangerous = false;

			if (MessageBox.Show("Are you sure you want to delete this dog?", "Delete Dog?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}
			clsSave.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
			if (!clsSave.EndOfFile())
			{
				strTag = clsSave.Get_Fields_String("taglicnum").Trim();
				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields("year"))));
                boolDangerous = clsSave.Get_Fields_Boolean("IsDangerousDog");
				if (!String.IsNullOrWhiteSpace(strTag))
				{
					if (MessageBox.Show("Return tag to inventory?", "Return Tag?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
                        if (!boolDangerous)
                        {
                            clsSave.Execute("update doginventory set status = 'Active' where type = 'T' and  isnull(stickernumber,'') = '" + strTag + "' and [year] = " + FCConvert.ToString(lngYear), "Clerk");
                        }
                        else
                        {
                            clsSave.Execute(
                                "update doginventory set status = 'Active' where type = 'D' and isnull(stickernumber,'') = '" +
                                strTag + "'", "Clerk");
                        }
						
					}
				}
			}
			clsSave.Execute("Update doginfo set previousowner = ownernum,ownernum = -1 where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
			modGlobalFunctions.AddCYAEntry("CK", "Deleted Dog " + FCConvert.ToString(lngDogNumber), "", "", "", "");
			boolChangedData = false;
			MessageBox.Show("Dog deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
			mnuExit_Click();
		}

		private void mnuDogHistory_Click(object sender, System.EventArgs e)
		{
			rptDogHistory.InstancePtr.Init(2, 0, lngDogNumber, false, true);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			if (!boolPrinted && boolSaved && (boolReRegistration || chkLicense.CheckState == Wisej.Web.CheckState.Checked) && !boolKennelDog)
			{
				if (MessageBox.Show("You have saved new license information but have not printed a license" + "\r\n" + "Do you wish to do so now?", "Print License?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					mnuPrintFront_Click();
					return;
				}
			}
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ToggleBackGroundsNonRequired()
		{
			if (chkNotRequired.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtDogName.BackColor = txtMarkings.BackColor;
				cmbVet.BackColor = txtMarkings.BackColor;
				txtYear.BackColor = txtMarkings.BackColor;
				cmbColor.BackColor = txtMarkings.BackColor;
				cmbBreed.BackColor = txtMarkings.BackColor;
				txtRabiesTag.BackColor = txtMarkings.BackColor;
				T2KDateVaccinated.BackColor = txtMarkings.BackColor;
				t2kExpirationDate.BackColor = txtMarkings.BackColor;
				txtTag.BackColor = txtMarkings.BackColor;
				// txtSticker.BackColor = txtMarkings.BackColor
				T2KIssueDate.BackColor = txtMarkings.BackColor;
				txtSex.BackColor = txtMarkings.BackColor;
				txtSpayNeuterNumber.BackColor = txtMarkings.BackColor;
			}
			else
            {
                var bColor = ColorTranslator.FromOle(StaticSettings.TrioColorSettings.TRIOCOLORHIGHLIGHT);
				txtDogName.BackColor = bColor;
				cmbVet.BackColor = bColor;
				txtYear.BackColor = bColor;
				cmbColor.BackColor = bColor;
				cmbBreed.BackColor = bColor;
				txtRabiesTag.BackColor = bColor;
				if (chkOutsideSource.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					t2kExpirationDate.BackColor = bColor;
					T2KDateVaccinated.BackColor = bColor;
				}
				else
				{
					t2kExpirationDate.BackColor = txtMarkings.BackColor;
					T2KDateVaccinated.BackColor = txtMarkings.BackColor;
				}
				if (chkWolfHybrid.CheckState == Wisej.Web.CheckState.Checked)
				{
					t2kExpirationDate.BackColor = txtMarkings.BackColor;
					T2KDateVaccinated.BackColor = txtMarkings.BackColor;
					txtRabiesTag.BackColor = txtMarkings.BackColor;
				}
				if (chkNeuterSpay.CheckState == Wisej.Web.CheckState.Checked)
				{
					txtSpayNeuterNumber.BackColor = bColor;
				}
				else
				{
					txtSpayNeuterNumber.BackColor = txtMarkings.BackColor;
				}
				txtTag.BackColor = bColor;
				T2KIssueDate.BackColor = bColor;
				txtSex.BackColor = bColor;
			}
		}

		private void UpdateBreeds()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbBreed.Clear();
			clsLoad.OpenRecordset("select * from DEFAULTbreeds order by name", "twck0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				cmbBreed.AddItem(clsLoad.Get_Fields_String("name"));
				cmbBreed.ItemData(cmbBreed.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id")));
				clsLoad.MoveNext();
			}
		}

		private void UpdateVets()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			cmbVet.Clear();
			rsLoad.OpenRecordset("select * from veterinarians order by VetName", "twck0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				cmbVet.AddItem(rsLoad.Get_Fields_String("VetName"));
				cmbVet.ItemData(cmbVet.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id")));
				rsLoad.MoveNext();
			}
		}

		private void UpdateColors()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbColor.Clear();
			clsLoad.OpenRecordset("select * from defaultcolors order by name", "twck0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				cmbColor.AddItem(clsLoad.Get_Fields_String("name"));
				cmbColor.ItemData(cmbColor.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id")));
				clsLoad.MoveNext();
			}
		}

		private void AssignBreed(ref int lngBreed)
		{
			int x;
			for (x = 0; x <= cmbBreed.Items.Count - 1; x++)
			{
				if (lngBreed == cmbBreed.ItemData(x))
				{
					cmbBreed.SelectedIndex = x;
					return;
				}
			}
			// x
			cmbBreed.SelectedIndex = -1;
		}

		private void AssignColor(ref int lngColor)
		{
			int x;
			for (x = 0; x <= cmbColor.Items.Count - 1; x++)
			{
				if (lngColor == cmbColor.ItemData(x))
				{
					cmbColor.SelectedIndex = x;
					return;
				}
			}
			// x
			cmbColor.SelectedIndex = -1;
		}

		private void RecalcAmount()
		{
			double dblAmount;
			double dblAmount1;
			double dblAmount2;
			double dblamount3;
			double dblamount4;
			double dblamount5;
			double dblamount6;
			double dblTemp;
			double dblState = 0;
			double dblTown = 0;
			double dblClerk = 0;
			double dblNeuterFee;
			double dblNonNeuterFee;
			double dblLateFee;
			double dblWarrantFee;
			double dblTransferFee;
			double dblReplacementFee;
			double dblReplacementTagFee;
			double dblReplacementStickerFee;
			double dblTempLicenseFee;
			double dblUserDefinedFee1;
            bool isDangerous = false;
            bool isNuisance = false;
            double nuisanceLateFee = 0;
            double dangerousLateFee = 0;

			modDogs.GetDogFees();
			dblNeuterFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Fixed_Fee);
			dblLateFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Late_Fee);
			dblWarrantFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Warrant_Fee);
			dblNonNeuterFee = FCConvert.ToDouble(modDogs.Statics.DogFee.UN_Fixed_FEE);
			dblTransferFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Transfer_Fee);
			dblReplacementFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Replacement_Fee);
			dblReplacementTagFee = FCConvert.ToDouble(modDogs.Statics.DogFee.DogTagFee);
			dblReplacementStickerFee = 0;
			dblTempLicenseFee = 0;
			dblUserDefinedFee1 = modDogs.Statics.DogFee.UserDefinedFee1;
            nuisanceLateFee = modDogs.Statics.DogFee.NuisanceLateFee;
            dangerousLateFee = modDogs.Statics.DogFee.DangerousLateFee;

			dblAmount = 0;
			dblAmount1 = 0;
			dblAmount2 = 0;
			dblamount3 = 0;
			dblamount4 = 0;
			dblamount5 = 0;
			dblamount6 = 0;

            isDangerous = chkDangerous.CheckState == CheckState.Checked;
            isNuisance = chkNuisance.CheckState == CheckState.Checked;

			// clear the values
			CurDogTrans.Amount = 0;
			CurDogTrans.ID = 0;
			CurDogTrans.boolAdjustmentVoid = false;
			CurDogTrans.boolHearingGuid = false;
			CurDogTrans.boolLicense = false;
			CurDogTrans.boolKennel = false;
			CurDogTrans.boolReplacementLicense = false;
			CurDogTrans.boolReplacementSticker = false;
			CurDogTrans.boolReplacementTag = false;
			CurDogTrans.boolSearchRescue = false;
			CurDogTrans.boolSpayNeuter = false;
			CurDogTrans.boolTempLicense = false;
			CurDogTrans.boolTransfer = false;
			CurDogTrans.ClerkFee = 0;
			CurDogTrans.Description = "";
			CurDogTrans.DogNumber = lngDogNumber;
			CurDogTrans.DogYear = txtYear.Text.ToIntegerValue();
            CurDogTrans.IsDangerous = isDangerous;
            CurDogTrans.IsNuisance = isNuisance;
			CurDogTrans.KennelDogs = "";
			CurDogTrans.KennelLateFee = 0;
			CurDogTrans.KennelLic1Fee = 0;
			CurDogTrans.KennelLic1Num = 0;
			CurDogTrans.KennelLic2Fee = 0;
			CurDogTrans.KennelLic2Num = 0;
			CurDogTrans.KennelLicenseID = 0;
			CurDogTrans.KennelWarrantFee = 0;
			CurDogTrans.LateFee = 0;
			CurDogTrans.OwnerNum = lngOwnerNumber;
			CurDogTrans.RabiesTagNumber = "";
			CurDogTrans.ReplacementLicenseFee = 0;
			CurDogTrans.ReplacementStickerFee = 0;
			CurDogTrans.ReplacementTagFee = 0;
			CurDogTrans.SpayNonSpayAmount = 0;
			CurDogTrans.StateFee = 0;
			CurDogTrans.StickerNumber = 0;
			CurDogTrans.TAGNUMBER = "";
			CurDogTrans.TempLicenseFee = 0;
			CurDogTrans.TownFee = 0;
			CurDogTrans.TransactionDate = modGNBas.Statics.typTransaction.TransactionDate;
			CurDogTrans.TransactionNumber = 0;
			CurDogTrans.TransferLicenseFee = 0;
			CurDogTrans.WarrantFee = 0;
			if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (!(chkHearingGuid.CheckState == Wisej.Web.CheckState.Checked || chkSearchRescue.CheckState == Wisej.Web.CheckState.Checked) || isDangerous || isNuisance)
				{
					if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						CurDogTrans.boolLicense = true;
						if (chkNeuterSpay.CheckState == CheckState.Checked)
						{
							dblClerk = modDogs.Statics.DogFee.FixedClerk;
							dblTemp = dblNeuterFee;
							// dblState = dblTemp - 3
							dblState = modDogs.Statics.DogFee.FixedState;
							CurDogTrans.boolSpayNeuter = true;
							CurDogTrans.SpayNonSpayAmount = dblNeuterFee;
							dblTown = modDogs.Statics.DogFee.FixedTown;
						}
						else
						{
							dblClerk = modDogs.Statics.DogFee.DogClerk;
							dblTemp = dblNonNeuterFee;
							// dblState = dblTemp - 1
							CurDogTrans.boolSpayNeuter = false;
							dblState = modDogs.Statics.DogFee.DogState;
							CurDogTrans.SpayNonSpayAmount = dblNonNeuterFee;
							dblTown = modDogs.Statics.DogFee.DogTown;
						}

                        if (isDangerous)
                        {
                            dblClerk = modDogs.Statics.DogFee.DangerousClerkFee;
                            dblState = modDogs.Statics.DogFee.DangerousStateFee;
                            dblTown = modDogs.Statics.DogFee.DangerousTownFee;
                            CurDogTrans.SpayNonSpayAmount = dblClerk + dblState + dblTown;
                            dblTemp = CurDogTrans.SpayNonSpayAmount;
                        }
                        else if(isNuisance)
                        {
                            dblClerk = modDogs.Statics.DogFee.NuisanceClerkFee;
                            dblState = modDogs.Statics.DogFee.NuisanceStateFee;
                            dblTown = modDogs.Statics.DogFee.NuisanceTownFee;
                            CurDogTrans.SpayNonSpayAmount = dblClerk + dblState + dblTown;
                            dblTemp = CurDogTrans.SpayNonSpayAmount;
                        }
						dblAmount2 += dblTown;
						dblamount3 += dblState;
						dblamount4 += dblClerk;
					}
					if (chkLateFee.CheckState == Wisej.Web.CheckState.Checked)
					{
                        if (isDangerous)
                        {
                            dblamount5 += modDogs.Statics.DogFee.DangerousLateFee;
                        }
                        else if (isNuisance)
                        {
                            dblamount5 += modDogs.Statics.DogFee.NuisanceLateFee;
                        }
                        else
                        {
                            dblamount5 += Convert.ToDouble(modDogs.Statics.DogFee.Late_Fee);
                        }
                    }
					if (chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked)
					{
						dblamount5 += FCConvert.ToDouble(modDogs.Statics.DogFee.Warrant_Fee);
						CurDogTrans.WarrantFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Warrant_Fee);
					}
					if (chkReplacementLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						CurDogTrans.boolReplacementLicense = true;
						CurDogTrans.ReplacementLicenseFee = dblReplacementFee;
						dblamount4 += dblReplacementFee;
						// goes to clerk fee
					}
					if (chkReplacementTag.CheckState == Wisej.Web.CheckState.Checked)
					{
						CurDogTrans.boolReplacementTag = true;
						CurDogTrans.ReplacementTagFee = dblReplacementTagFee;
						dblamount4 += dblReplacementTagFee;
					}
					if (chkReplacementSticker.CheckState == Wisej.Web.CheckState.Checked)
					{
						CurDogTrans.boolReplacementSticker = true;
						CurDogTrans.ReplacementStickerFee = dblReplacementStickerFee;
						dblamount4 += dblReplacementStickerFee;
					}
					if (chkTempLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						CurDogTrans.boolTempLicense = true;
						// don't currently charge for this but leave the code just in case
						CurDogTrans.TempLicenseFee = dblTempLicenseFee;
						dblamount4 += dblTempLicenseFee;
					}
					if (chkTransferLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						CurDogTrans.boolTransfer = true;
						CurDogTrans.TransferLicenseFee = dblTransferFee;
						dblamount4 += dblTransferFee;
					}
				}
				else
				{
					// kk07302015 trocks-9  Allow Late Fee on Search and Rescue and/or Hearing/Guide dogs
					if (modDogs.Statics.DogFee.LateFeeOnServiceDog)
					{
						if (chkLateFee.CheckState == Wisej.Web.CheckState.Checked)
						{
							dblamount5 += FCConvert.ToDouble(modDogs.Statics.DogFee.Late_Fee);
						}
					}
				}
				if (chkUserDefinedFee.CheckState == Wisej.Web.CheckState.Checked)
				{
					dblamount6 += dblUserDefinedFee1;
					CurDogTrans.AnimalControl = dblamount6;
				}
				if (chkNeuterSpay.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.boolSpayNeuter = true;
				}
				if (chkHearingGuid.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.boolHearingGuid = true;
				}
				if (chkReplacementLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.boolReplacementLicense = true;
				}
				if (chkReplacementSticker.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.boolReplacementSticker = true;
				}
				if (chkReplacementTag.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.boolReplacementTag = true;
				}
				if (chkSearchRescue.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.boolSearchRescue = true;
				}
				if (chkTempLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.boolTempLicense = true;
				}
				if (chkTransferLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.boolTransfer = true;
				}
				dblAmount = dblAmount1 + dblAmount2 + dblamount3 + dblamount4 + dblamount5 + dblamount6;
				curTrans.Amount1 = dblAmount1;
				curTrans.Amount2 = dblAmount2;
				curTrans.Amount3 = dblamount3;
				curTrans.Amount4 = dblamount4;
				curTrans.Amount5 = dblamount5;
				curTrans.Amount6 = dblamount6;
				CurDogTrans.Amount = dblAmount;
				CurDogTrans.ClerkFee = dblamount4;
				CurDogTrans.StateFee = dblamount3;
				CurDogTrans.LateFee = dblamount5;
				CurDogTrans.TownFee = dblAmount2;
				if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.Description = "Dog License";
				}
				else if (chkLateFee.CheckState == Wisej.Web.CheckState.Checked || chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.Description = "Dog Late or Warrant Fee";
				}
				else if (chkTempLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.Description = "Dog Temp License";
				}
				else if (chkTransferLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.Description = "Dog Transfer";
				}
				else if (chkReplacementTag.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.Description = "Dog Replacement Tag";
				}
				else if (chkReplacementLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.Description = "Dog Replacement License";
				}
				else if (chkReplacementSticker.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.Description = "Dog Replacement Sticker";
				}
				else if (chkUserDefinedFee.CheckState == Wisej.Web.CheckState.Checked)
				{
					CurDogTrans.Description = modDogs.Statics.DogFee.UserDefinedDescription1;
				}
				curTrans.Description = CurDogTrans.Description;
				CurDogTrans.TransactionDate = curTrans.TransactionDate;
				curTrans.TransactionType = modGNBas.DOGTRANSACTION;
				curTrans.TotalAmount = dblAmount;
				txtAmount.Text = Strings.Format(dblAmount, "0.00");
				if (dblAmount > 0)
				{
					boolMadeTrans = true;
				}
				else
				{
					if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else if (chkLateFee.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else if (chkReplacementLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else if (chkReplacementSticker.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else if (chkReplacementTag.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else if (chkTempLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else if (chkTransferLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else if (chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else if (chkUserDefinedFee.CheckState == Wisej.Web.CheckState.Checked)
					{
						boolMadeTrans = true;
					}
					else
					{
						boolMadeTrans = false;
					}
				}
			}
			else
			{
				boolMadeTrans = false;
			}
		}

		private void CopyToGlobalTrans()
		{
			modGNBas.Statics.typTransaction.Amount1 = curTrans.Amount1;
			modGNBas.Statics.typTransaction.Amount2 = curTrans.Amount2;
			modGNBas.Statics.typTransaction.Amount3 = curTrans.Amount3;
			modGNBas.Statics.typTransaction.Amount4 = curTrans.Amount4;
			modGNBas.Statics.typTransaction.Amount5 = curTrans.Amount5;
			modGNBas.Statics.typTransaction.Amount6 = curTrans.Amount6;
			modGNBas.Statics.typTransaction.Description = curTrans.Description;
			modGNBas.Statics.typTransaction.TotalAmount = curTrans.TotalAmount;
			modGNBas.Statics.typTransaction.TransactionDate = curTrans.TransactionDate;
			modGNBas.Statics.typTransaction.TransactionType = curTrans.TransactionType;
			modGNBas.Statics.typDogTransaction.Amount = CurDogTrans.Amount;
			modGNBas.Statics.typDogTransaction.boolAdjustmentVoid = CurDogTrans.boolAdjustmentVoid;
			modGNBas.Statics.typDogTransaction.boolHearingGuid = CurDogTrans.boolHearingGuid;
			modGNBas.Statics.typDogTransaction.boolKennel = CurDogTrans.boolKennel;
			modGNBas.Statics.typDogTransaction.boolLicense = CurDogTrans.boolLicense;
			modGNBas.Statics.typDogTransaction.boolReplacementLicense = CurDogTrans.boolReplacementLicense;
			modGNBas.Statics.typDogTransaction.boolReplacementSticker = CurDogTrans.boolReplacementSticker;
			modGNBas.Statics.typDogTransaction.boolReplacementTag = CurDogTrans.boolReplacementTag;
			modGNBas.Statics.typDogTransaction.boolSearchRescue = CurDogTrans.boolSearchRescue;
			modGNBas.Statics.typDogTransaction.boolSpayNeuter = CurDogTrans.boolSpayNeuter;
			modGNBas.Statics.typDogTransaction.boolTempLicense = CurDogTrans.boolTempLicense;
			modGNBas.Statics.typDogTransaction.boolTransfer = CurDogTrans.boolTransfer;
			modGNBas.Statics.typDogTransaction.ClerkFee = CurDogTrans.ClerkFee;
			modGNBas.Statics.typDogTransaction.Description = CurDogTrans.Description;
			modGNBas.Statics.typDogTransaction.DogNumber = CurDogTrans.DogNumber;
			modGNBas.Statics.typDogTransaction.DogYear = CurDogTrans.DogYear;
            modGNBas.Statics.typDogTransaction.IsDangerous = CurDogTrans.IsDangerous;
            modGNBas.Statics.typDogTransaction.IsNuisance = CurDogTrans.IsNuisance;
			modGNBas.Statics.typDogTransaction.KennelDogs = CurDogTrans.KennelDogs;
			modGNBas.Statics.typDogTransaction.KennelLateFee = CurDogTrans.KennelLateFee;
			modGNBas.Statics.typDogTransaction.KennelLic1Fee = CurDogTrans.KennelLic1Fee;
			modGNBas.Statics.typDogTransaction.KennelLic1Num = CurDogTrans.KennelLic1Num;
			modGNBas.Statics.typDogTransaction.KennelLic2Fee = CurDogTrans.KennelLic2Fee;
			modGNBas.Statics.typDogTransaction.KennelLic2Num = CurDogTrans.KennelLic2Num;
			modGNBas.Statics.typDogTransaction.KennelLicenseID = CurDogTrans.KennelLicenseID;
			modGNBas.Statics.typDogTransaction.KennelWarrantFee = CurDogTrans.KennelWarrantFee;
			modGNBas.Statics.typDogTransaction.LateFee = CurDogTrans.LateFee;
			modGNBas.Statics.typDogTransaction.OwnerNum = CurDogTrans.OwnerNum;
			modGNBas.Statics.typDogTransaction.RabiesTagNumber = CurDogTrans.RabiesTagNumber;
			modGNBas.Statics.typDogTransaction.ReplacementLicenseFee = CurDogTrans.ReplacementLicenseFee;
			modGNBas.Statics.typDogTransaction.ReplacementStickerFee = CurDogTrans.ReplacementStickerFee;
			modGNBas.Statics.typDogTransaction.ReplacementTagFee = CurDogTrans.ReplacementTagFee;
			modGNBas.Statics.typDogTransaction.SpayNonSpayAmount = CurDogTrans.SpayNonSpayAmount;
			modGNBas.Statics.typDogTransaction.StateFee = CurDogTrans.StateFee;
			modGNBas.Statics.typDogTransaction.StickerNumber = CurDogTrans.StickerNumber;
			modGNBas.Statics.typDogTransaction.TAGNUMBER = CurDogTrans.TAGNUMBER;
			modGNBas.Statics.typDogTransaction.TempLicenseFee = CurDogTrans.TempLicenseFee;
			modGNBas.Statics.typDogTransaction.TownFee = CurDogTrans.TownFee;
			modGNBas.Statics.typDogTransaction.TransactionDate = CurDogTrans.TransactionDate;
			modGNBas.Statics.typDogTransaction.TransactionNumber = CurDogTrans.TransactionNumber;
			modGNBas.Statics.typDogTransaction.TransferLicenseFee = CurDogTrans.TransferLicenseFee;
			modGNBas.Statics.typDogTransaction.OldIssueDate = CurDogTrans.OldIssueDate;
			modGNBas.Statics.typDogTransaction.OldYear = CurDogTrans.OldYear;
			modGNBas.Statics.typDogTransaction.OldStickerNumber = CurDogTrans.OldStickerNumber;
			modGNBas.Statics.typDogTransaction.OldTagNumber = CurDogTrans.OldTagNumber;
			modGNBas.Statics.typDogTransaction.AnimalControl = CurDogTrans.AnimalControl;
			modGNBas.Statics.typDogTransaction.WarrantFee = CurDogTrans.WarrantFee;
		}

		private void mnuFees_Click(object sender, System.EventArgs e)
		{
			frmDogInfo.InstancePtr.Init(true, 2);
		}

		private void mnuInventory_Click(object sender, System.EventArgs e)
		{
			frmDogInventory.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuPrintBack_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intLineToPrint;
			bool boolLookUpAmount = false;
			boolPrinted = true;
			if (boolChangedData)
			{
				if (!SaveData())
				{
					MessageBox.Show("Save failed. Cannot print license.", "Save Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			intLineToPrint = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("Enter Line to print on back of Dog License.", "TRIO Software", FCConvert.ToString(1)))));
			if (intLineToPrint == 0)
				return;
			if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolLookUpAmount = false;
			}
			else
			{
				boolLookUpAmount = true;
			}
			clsLoad.OpenRecordset("Select * from PrinterSettings", modGNBas.DEFAULTDATABASE);
			if (!clsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Int16("DogLicense")))
				{
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("PrintDogReRegOnFront")))
					{
						if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("PrintDogReRegExpiration")))
						{
							rptBackDogLicense.InstancePtr.Init(lngDogNumber, intLineToPrint, true, true, true, boolLookUpAmount);
						}
						else
						{
							rptBackDogLicense.InstancePtr.Init(lngDogNumber, intLineToPrint, true, false, true, boolLookUpAmount);
						}
					}
					else
					{
						if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("Printdogreregexpiration")))
						{
							rptBackDogLicense.InstancePtr.Init(lngDogNumber, intLineToPrint, false, true, true, boolLookUpAmount);
						}
						else
						{
							rptBackDogLicense.InstancePtr.Init(lngDogNumber, intLineToPrint, false, false, true, boolLookUpAmount);
						}
					}
				}
				else
				{
					rptBackDogLicense.InstancePtr.Init(lngDogNumber, intLineToPrint, false, boolLookUpAmount: boolLookUpAmount);
				}
			}
			else
			{
				rptBackDogLicense.InstancePtr.Init(lngDogNumber, intLineToPrint, false, boolLookUpAmount: boolLookUpAmount);
			}
		}

		private void mnuPrinterSettings_Click(object sender, System.EventArgs e)
		{
			frmPrinterSettings.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuPrintFront_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsLicense = new clsDRWrapper();
			boolPrinted = true;
			if (boolChangedData)
			{
				if (!SaveData())
				{
					MessageBox.Show("Save Failed. Cannot print license.", "Print Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			rsLicense.OpenRecordset("Select * from PrinterSettings", "twck0000.vb1");
			if (!rsLicense.EndOfFile())
			{
				if (FCConvert.ToInt32(rsLicense.Get_Fields_Int16("DogLicense")) == 1)
				{
					if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
					{
						rptDogLicenseLaser.InstancePtr.init(ref lngDogNumber);
					}
					else
					{
						rptDogLicenseLaser.InstancePtr.init(ref lngDogNumber, true);
					}
				}
				else
				{
					if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
					{
						rptDogLicense.InstancePtr.Init(ref lngDogNumber);
					}
					else
					{
						rptDogLicense.InstancePtr.Init(ref lngDogNumber, true);
					}
				}
			}
			else
			{
				if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
				{
					rptDogLicense.InstancePtr.Init(ref lngDogNumber);
				}
				else
				{
					rptDogLicense.InstancePtr.Init(ref lngDogNumber, true);
				}
			}
		}

		public void mnuPrintFront_Click()
		{
			mnuPrintFront_Click(mnuPrintFront, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void mnuStates_Click()
		{
			frmStateTownInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuViewDocs_Click(object sender, System.EventArgs e)
		{
			ViewDocs();
		}

		private void T2KDateVaccinated_Change(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void T2KDOB_Change(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void t2kExpirationDate_Change(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void T2KIssueDate_Change(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtCertificateNumber_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtDogName_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtMarkings_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtPrevTag_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtRabiesTag_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtSex_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtSpayNeuterNumber_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
				if (fecherFoundation.Strings.Trim(txtSpayNeuterNumber.Text) != string.Empty)
				{
					chkNeuterSpay.CheckState = Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void FillGridStickerInventory()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			GridStickerInventory.Rows = 0;
			if (Conversion.Val(txtYear.Text) > 0)
			{
				// Call clsLoad.OpenRecordset("select * from doginventory where type = 'S' and [year] = " & txtYear.Text & " and ((status <> 'Used') or stickernumber = " & lngOrigStick & ") order by stickernumber ", "twck0000.vb1")
				clsLoad.OpenRecordset("select * from doginventory where type = 'S' and [year] = " + txtYear.Text + " and status <> 'Used' order by stickernumber ", "twck0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					GridStickerInventory.AddItem(FCConvert.ToString(clsLoad.Get_Fields_Int32("stickernumber")));
					clsLoad.MoveNext();
				}
			}
		}

		private void FillTagInventory()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			GridStickerInventory.Rows = 0;
			clsLoad.OpenRecordset("select * from doginventory where type = 'T' and [year] = " + txtYear.Text + " and status <> 'Used' order by stickernumber", "twck0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				GridStickerInventory.AddItem(FCConvert.ToString(clsLoad.Get_Fields_Int32("stickernumber")));
				clsLoad.MoveNext();
			}
		}

		private void txtTag_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtTag_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F2:
					{
						KeyCode = (Keys)0;
                        if (chkDangerous.Checked)
                        {
                            FillDangerousTagInventory();
                        }
                        else
                        {
                            FillTagInventory();
                        }

                        GridStickerInventory.Tag = "Tag";
						framStickerInventory.Text = "Tag Inventory";
						framStickerInventory.Visible = true;
						framStickerInventory.BringToFront();
						break;
					}
			}
			//end switch
		}

        private void FillDangerousTagInventory()
        {
            var rsLoad = new clsDRWrapper();
            GridStickerInventory.Rows = 0;
            rsLoad.OpenRecordset(
                "select * from doginventory where type = 'D' and status <> 'Used' order by stickernumber", "Clerk");
            while (!rsLoad.EndOfFile())
            {
                GridStickerInventory.AddItem(rsLoad.Get_Fields_String("StickerNumber"));
                rsLoad.MoveNext();
            }
        }

        private void txtVet_Change()
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private void txtYear_TextChanged(object sender, System.EventArgs e)
		{
			if (boolOKToValidate)
			{
				boolChangedData = true;
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				fecherFoundation.Information.Err().Clear();
				strReturnName = txtDogName.Text;
				SaveData = false;
				if (!ValidateData())
				{
					return SaveData;
				}
				RecalcAmount();
				if (boolChangedBreed)
				{
					boolChangedBreed = false;
					clsSave.OpenRecordset("select * from defaultbreeds where name = '" + modGlobalFunctions.EscapeQuotes(cmbBreed.Text) + "'", "twck0000.vb1");
					if (clsSave.EndOfFile())
					{
						if (MessageBox.Show("This breed is not found in your list of breeds" + "\r\n" + "Do you wish to add it?", "Add New Breed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							clsSave.AddNew();
							clsSave.Set_Fields("Name", cmbBreed.Text);
							clsSave.Set_Fields("LastUpdated", DateTime.Now);
							cmbBreed.AddItem(cmbBreed.Text);
							cmbBreed.ItemData(cmbBreed.NewIndex, FCConvert.ToInt32(clsSave.Get_Fields_Int32("id")));
							clsSave.Update();
						}
					}
				}
				if (boolChangedVet)
				{
					boolChangedVet = false;
					clsSave.OpenRecordset("select * from veterinarians where vetname = '" + modGlobalFunctions.EscapeQuotes(cmbVet.Text) + "'", "twck0000.vb1");
					if (clsSave.EndOfFile())
					{
						if (MessageBox.Show("This veterinarian is not found in your list of veterinarians" + "\r\n" + "Do you wish to add it?", "Add New Vet?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							clsSave.AddNew();
							clsSave.Set_Fields("VetName", cmbVet.Text);
							cmbVet.AddItem(cmbVet.Text);
							clsSave.Update();
							cmbVet.ItemData(cmbVet.NewIndex, FCConvert.ToInt32(clsSave.Get_Fields_Int32("id")));
						}
					}
				}
				clsSave.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
				if (lngDogNumber == 0 || clsSave.EndOfFile())
				{
					boolNewDog = true;
					clsSave.AddNew();
					if (Information.IsDate(T2KIssueDate.Text))
					{
						curTrans.TransactionDate = FCConvert.ToDateTime(T2KIssueDate.Text);
					}
					if (boolKennelDog)
					{
						clsSave.Set_Fields("dogtokenneldate", Strings.Format(curTrans.TransactionDate, "MM/dd/yyyy"));
					}
					clsSave.Set_Fields("NewTagAddedDate", curTrans.TransactionDate);
					// ALREADY MADE THE new tag added date. Don't let them change the transaction date now.
					T2KIssueDate.Enabled = false;
					CurDogTrans.TransactionDate = curTrans.TransactionDate;
					CurDogTrans.DogNumber = lngDogNumber;
					CurDogTrans.OldIssueDate = DateTime.FromOADate(0);
					CurDogTrans.OldTagNumber = "";
					CurDogTrans.OldStickerNumber = 0;
					CurDogTrans.OldYear = 0;
				}
				else
				{
					clsSave.Edit();
					if (!boolNewDog)
					{
						if (FCConvert.ToString(clsSave.Get_Fields_DateTime("rabstickerissue")) == string.Empty)
						{
							CurDogTrans.OldIssueDate = DateTime.FromOADate(0);
						}
						else
						{
							CurDogTrans.OldIssueDate = (DateTime)clsSave.Get_Fields_DateTime("rabstickerissue");
						}
						CurDogTrans.OldTagNumber = clsSave.Get_Fields_String("taglicnum");
						CurDogTrans.OldStickerNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields_String("stickerlicnum"))));
						CurDogTrans.OldYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields("Year"))));
					}
				}
				if (Information.IsDate(T2KIssueDate.Text) && chkFees.CheckState == Wisej.Web.CheckState.Checked && chkLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					curTrans.TransactionDate = FCConvert.ToDateTime(T2KIssueDate.Text);
					CurDogTrans.TransactionDate = curTrans.TransactionDate;
				}
				if (!boolNewDog)
				{
					if (boolKennelDog)
					{
						clsSave.Set_Fields("kenneldog", true);
					}
					else
					{
						clsSave.Set_Fields("Kenneldog", false);
					}
				}
				else
				{
					clsSave.Set_Fields("KENNELDOG", false);
					// make the kennel license save first and add the dog in itself
				}
				clsSave.Set_Fields("Ownernum", lngOwnerNumber);
				clsSave.Set_Fields("dogname", fecherFoundation.Strings.Trim(txtDogName.Text));
				if (Information.IsDate(T2KDOB.Text))
				{
					clsSave.Set_Fields("dogdob", Strings.Format(T2KDOB.Text, "MM/dd/yyyy"));
				}
				else
				{
					clsSave.Set_Fields("dogdob", 0);
				}
				if (chkExcludeWarrant.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("ExcludeFromWarrant", true);
				}
				else
				{
					clsSave.Set_Fields("ExcludeFromWarrant", false);
				}
				clsSave.Set_Fields("dogsex", fecherFoundation.Strings.UCase(txtSex.Text));
				clsSave.Set_Fields("dogmarkings", fecherFoundation.Strings.Trim(txtMarkings.Text));
				clsSave.Set_Fields("dogvet", fecherFoundation.Strings.Trim(cmbVet.Text));
				clsSave.Set_Fields("dogneuter", chkNeuterSpay.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("sncertnum", fecherFoundation.Strings.Trim(txtSpayNeuterNumber.Text));
				clsSave.Set_Fields("Previoustag", fecherFoundation.Strings.Trim(txtPrevTag.Text));
                var strTag = txtTag.Text.Trim();                
				clsSave.Set_Fields("taglicnum", strTag);
				clsSave.Set_Fields("rabiestagno", txtRabiesTag.Text);
				CurDogTrans.TAGNUMBER = strTag;
				CurDogTrans.RabiesTagNumber = txtRabiesTag.Text;
				if (Information.IsDate(T2KDateVaccinated.Text))
				{
					clsSave.Set_Fields("rabiesshotdate", Strings.Format(T2KDateVaccinated.Text, "MM/dd/yyyy"));
				}
				else
				{
					clsSave.Set_Fields("rabiesshotdate", 0);
				}
				if (chkRabiesExempt.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("RabiesExempt", true);
				}
				else
				{
					clsSave.Set_Fields("RabiesExempt", false);
				}
				clsSave.Set_Fields("rabiescertnum", txtCertificateNumber.Text);
				if (Information.IsDate(T2KIssueDate.Text))
				{
					clsSave.Set_Fields("rabstickerissue", Strings.Format(T2KIssueDate.Text, "MM/dd/yyyy"));
				}
				else
				{
					clsSave.Set_Fields("rabstickerissue", 0);
				}
				if (Information.IsDate(t2kExpirationDate.Text))
				{
					clsSave.Set_Fields("RabiesExpDate", Strings.Format(t2kExpirationDate.Text, "MM/dd/yyyy"));
				}
				else
				{
					clsSave.Set_Fields("rabiesexpdate", 0);
				}
				clsSave.Set_Fields("sandr", chkSearchRescue.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("hearguide", chkHearingGuid.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("wolfhybrid", chkWolfHybrid.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("Year", FCConvert.ToString(Conversion.Val(txtYear.Text)));
				clsSave.Set_Fields("dogdeceased", chkDeceased.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("norequiredfields", chkNotRequired.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("OutsideSource", chkOutsideSource.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("dogbreed", cmbBreed.Text);
				clsSave.Set_Fields("dogcolor", cmbColor.Text);
                clsSave.Set_Fields("IsDangerousDog",chkDangerous.Checked);
                clsSave.Set_Fields("IsNuisanceDog",chkNuisance.Checked);
                CurDogTrans.IsDangerous = chkDangerous.Checked;
                CurDogTrans.IsNuisance = chkNuisance.Checked;
				clsSave.Update();
				lngDogNumber = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				if (!String.IsNullOrWhiteSpace(txtTag.Text))
				{
					if (!boolKennelDog)
					{
						if (!(chkOutsideSource.CheckState == Wisej.Web.CheckState.Checked))
						{
                            if (!chkDangerous.Checked)
                            {
                                clsSave.Execute("update doginventory set status = 'Used' where type = 'T' and stickernumber = '" + txtTag.Text + "' and [year] = " + txtYear.Text.ToIntegerValue(), "Clerk");
                            }
                            else
                            {
                                clsSave.Execute("update doginventory set status = 'Used' where type = 'D' and stickernumber = '" + txtTag.Text + "'", "Clerk");
                            }
							
						}
					}
				}
				boolChangedData = false;
				if (boolMadeTrans)
				{
					CopyToGlobalTrans();
				}
				boolSaved = true;
				SaveData = true;
				mnuViewDocs.Enabled = true;
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private bool ValidateData()
		{
			bool ValidateData = false;
			DateTime dtTemp1;
			DateTime dtTemp2;
			clsDRWrapper clsLoad = new clsDRWrapper();
            var strTemp = "";

			try
			{
				fecherFoundation.Information.Err().Clear();
				ValidateData = false;
				if (fecherFoundation.Strings.Trim(txtDogName.Text) == string.Empty)
				{
					MessageBox.Show("You must enter a valid dog name", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtDogName.Focus();
					return ValidateData;
				}
				if (!(chkNotRequired.CheckState == Wisej.Web.CheckState.Checked))
				{
					if (Conversion.Val(txtYear.Text) <= 0)
					{
						MessageBox.Show("You must enter a valid license year", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtYear.Focus();
						return ValidateData;
					}
					if (fecherFoundation.Strings.Trim(cmbVet.Text) == string.Empty)
					{
						MessageBox.Show("You must enter a valid veterinarian name", "Invalid Vet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cmbVet.Focus();
						return ValidateData;
					}
					if (chkNeuterSpay.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (fecherFoundation.Strings.Trim(txtSpayNeuterNumber.Text) == string.Empty)
						{
							MessageBox.Show("You must enter a spay/neuter certificate number", "Invalid Certificate Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtSpayNeuterNumber.Focus();
							return ValidateData;
						}
					}
					if (fecherFoundation.Strings.UCase(txtSex.Text) != "M" && fecherFoundation.Strings.UCase(txtSex.Text) != "F")
					{
						MessageBox.Show("You must enter a valid sex", "Invalid Sex", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtSex.Focus();
						return ValidateData;
					}
					if (cmbBreed.Text == "")
					{
						MessageBox.Show("You must enter a valid breed", "Invalid Breed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cmbBreed.Focus();
						return ValidateData;
					}
					if (cmbColor.Text == "")
					{
						MessageBox.Show("You must enter a valid color", "Invalid Color", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cmbColor.Focus();
						return ValidateData;
					}
					dtTemp1 = DateTime.FromOADate(0);
					dtTemp2 = DateTime.FromOADate(0);
					if (Information.IsDate(t2kExpirationDate.Text))
					{
						dtTemp1 = FCConvert.ToDateTime(t2kExpirationDate.Text);
					}
					bool boolExpWarning = false;
					boolExpWarning = false;
					if (!(chkOutsideSource.CheckState == Wisej.Web.CheckState.Checked || chkWolfHybrid.CheckState == Wisej.Web.CheckState.Checked))
					{
						if (!Information.IsDate(t2kExpirationDate.Text) && !(chkRabiesExempt.CheckState == Wisej.Web.CheckState.Checked))
						{
							if (!(MessageBox.Show("You must enter a valid rabies expiration date if this dog does not have a medical exemption" + "\r\n" + "Continue?", "Invalid Expiration Date", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK))
							{
								t2kExpirationDate.Focus();
								return ValidateData;
							}
							boolExpWarning = true;
						}
					}
					if (!Information.IsDate(T2KIssueDate.Text))
					{
						MessageBox.Show("You must enter a valid issue date", "Invalid Issue Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						T2KIssueDate.Focus();
						return ValidateData;
					}
					else
					{
						dtTemp2 = FCConvert.ToDateTime(T2KIssueDate.Text);
					}
					if (!boolExpWarning)
					{
						if (!(chkOutsideSource.CheckState == Wisej.Web.CheckState.Checked || chkWolfHybrid.CheckState == Wisej.Web.CheckState.Checked || chkRabiesExempt.CheckState == Wisej.Web.CheckState.Checked))
						{
							if (fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtTemp2) > 0)
							{
								if (!(MessageBox.Show("The rabies expiration date cannot expire before the issue date unless this dog has a medical exemption" + "\r\n" + "Continue?", "Rabies Shot Expired", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK))
								{
									t2kExpirationDate.Focus();
									return ValidateData;
								}
								boolExpWarning = true;
							}
						}
					}
					if ((boolReRegistration || boolNewDog) && !(chkOutsideSource.CheckState == Wisej.Web.CheckState.Checked))
					{
						if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(T2KIssueDate.Text), FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"))) > 90)
						{
							MessageBox.Show("You must enter a valid issue date greater then or equal to todays date.", "Invalid Issue Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
							T2KIssueDate.Focus();
							return ValidateData;
						}
					}
					if (!(chkOutsideSource.CheckState == Wisej.Web.CheckState.Checked) && !(chkWolfHybrid.CheckState == Wisej.Web.CheckState.Checked) && !(chkRabiesExempt.CheckState == Wisej.Web.CheckState.Checked))
					{
						if (!Information.IsDate(T2KDateVaccinated.Text) && !boolExpWarning)
						{
							if (MessageBox.Show("You must enter a valid rabies vaccination date unless this dog has a medical exemption", "Invalid Rabies Vaccination Date", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
							{
								T2KDateVaccinated.Focus();
								return ValidateData;
							}
							boolExpWarning = true;
						}
						else if (!boolExpWarning)
						{
							dtTemp2 = FCConvert.ToDateTime(T2KDateVaccinated.Text);
							if (fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtTemp2) > 0)
							{
								if (MessageBox.Show("The rabies expiration date cannot expire before the vaccination date unless this dog has a medical exemption", "Rabies Expiration", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
								{
									t2kExpirationDate.Focus();
									return ValidateData;
								}
								boolExpWarning = true;
							}
						}
					}
					if (fecherFoundation.Strings.Trim(txtRabiesTag.Text) == string.Empty && !(chkWolfHybrid.CheckState == Wisej.Web.CheckState.Checked) && !boolExpWarning && !(chkRabiesExempt.CheckState == Wisej.Web.CheckState.Checked))
					{
						if (MessageBox.Show("You must enter a valid rabies tag number unless this dog has a medical exemption", "Invalid Rabies Tag", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != DialogResult.OK)
						{
							txtRabiesTag.Focus();
							return ValidateData;
						}
						boolExpWarning = true;
					}
					if (!boolKennelDog)
					{
						if (String.IsNullOrWhiteSpace( txtTag.Text) )
						{
							MessageBox.Show("You must enter a valid city / town tag number", "Invalid City / Town Tag", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtTag.Focus();
							return ValidateData;
						}
					}
					if (!(chkOutsideSource.CheckState == Wisej.Web.CheckState.Checked) && !boolKennelDog)
                    {
                        var strTag = txtTag.Text.Trim();
                        var strTagType = "";
                        if (chkDangerous.Checked)
                        {
                            strTagType = "D";
                        }
                        else
                        {
                            strTagType = "T";
                        }
						if (!String.IsNullOrWhiteSpace(strTag))
						{
							// check if the tag number is in inventory and not used by a different dog
							if (!boolKennelDog)
							{
								clsLoad.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
								if (!clsLoad.EndOfFile())
								{
									if (clsLoad.Get_Fields_String("taglicnum") != strTag)
									{
										clsLoad.OpenRecordset("select * from doginventory where stickernumber = " + FCConvert.ToString(Conversion.Val(txtTag.Text)) + " and type = 'T' and ([YEAR] = " + FCConvert.ToString(Conversion.Val(txtYear.Text)) + " )", "twck0000.vb1");
										if (clsLoad.EndOfFile())
										{
                                            if (strTagType != "D")
                                            {
                                                strTemp =
                                                    "Tag is not in inventory. Do you want to add it now for year " +
                                                    txtYear.Text + "?";
                                            }
                                            else
                                            {
                                                strTemp = "Tag is not in inventory. Do you want to add it now?";
                                            }
											if (MessageBox.Show(strTemp, "Add To Inventory?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
											{
												modGlobalFunctions.AddCYAEntry("CK", "Added Tag from dog screen", "Tag " + txtTag.Text, "Year " + txtYear.Text, "Dog " + FCConvert.ToString(lngDogNumber), "");
												clsLoad.AddNew();
												clsLoad.Set_Fields("Type", strTagType);
												clsLoad.Set_Fields("StickerNumber", strTag);
												clsLoad.Set_Fields("Year", FCConvert.ToString(Conversion.Val(txtYear.Text)));
												clsLoad.Set_Fields("PermTemp", "N/A");
												clsLoad.Set_Fields("Status", "Used");
												clsLoad.Set_Fields("DateReceived", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
												clsLoad.Set_Fields("ReceivedOpid", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
												clsLoad.Update();
												MessageBox.Show("Tag added to inventory", "Tag Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
											}
											else
											{
												MessageBox.Show("Please enter a valid tag", "Invalid Tag", MessageBoxButtons.OK, MessageBoxIcon.Warning);
												txtTag.Focus();
												return ValidateData;
											}
										}
										else
										{
											if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("status"))) == "USED")
											{
												clsLoad.OpenRecordset("select * from doginfo where taglicnum = '" + strTag + "' and ownernum > 0 AND [YEAR] = " + FCConvert.ToString(Conversion.Val(txtYear.Text)), "twck0000.vb1");
												if (!clsLoad.EndOfFile())
												{
													if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID")) != lngDogNumber)
													{
														MessageBox.Show("Tag number already used by dog " + clsLoad.Get_Fields_String("dogname"), "Tag Used", MessageBoxButtons.OK, MessageBoxIcon.Warning);
														txtTag.Text = string.Empty;
														txtTag.Focus();
														return ValidateData;
													}
												}
											}
										}
									}
								}
								else
								{
									clsLoad.OpenRecordset("select * from doginventory where stickernumber = " + strTag + " and type = 'T' AND [YEAR] = " + FCConvert.ToString(Conversion.Val(txtYear.Text)), "twck0000.vb1");
									if (clsLoad.EndOfFile())
									{
										if (MessageBox.Show("Tag is not in inventory.  Do you want to add it now for year " + FCConvert.ToString(Conversion.Val(txtYear.Text)) + "?", "Add To Inventory?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
										{
											modGlobalFunctions.AddCYAEntry("CK", "Added Tag from dog screen", "Tag " + strTag, "Year " + txtYear.Text, "Dog " + FCConvert.ToString(lngDogNumber), "");
											clsLoad.AddNew();
											clsLoad.Set_Fields("Type", strTagType);
											clsLoad.Set_Fields("StickerNumber", strTag);
											clsLoad.Set_Fields("Year", FCConvert.ToString(Conversion.Val(txtYear.Text)));
											clsLoad.Set_Fields("PermTemp", "N/A");
											clsLoad.Set_Fields("Status", "Used");
											clsLoad.Set_Fields("DateReceived", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
											clsLoad.Set_Fields("ReceivedOpid", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
											clsLoad.Update();
											MessageBox.Show("Tag added to inventory", "Tag Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
										}
										else
										{
											MessageBox.Show("Please enter a valid tag", "Invalid Tag", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											txtTag.Focus();
											return ValidateData;
										}
									}
									else
									{
										if (fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("status"))) == "USED")
										{
											clsLoad.OpenRecordset("select * from doginfo where taglicnum = '" + strTag + "' and ownernum > 0 and [year] = " + FCConvert.ToString(Conversion.Val(txtYear.Text)), "twck0000.vb1");
											if (!clsLoad.EndOfFile())
											{
												if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID")) != lngDogNumber)
												{
													MessageBox.Show("Tag number already used by dog " + clsLoad.Get_Fields_String("dogname"), "Tag Used", MessageBoxButtons.OK, MessageBoxIcon.Warning);
													txtTag.Text = string.Empty;
													txtTag.Focus();
													return ValidateData;
												}
											}
										}
									}
								}
							}
						}
					}
				}
				ValidateData = true;
				return ValidateData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ValidateData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateData;
		}

		private void CheckPermissions()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTEDITTAGSTICKERINVENTORY)) != "F")
				{
					mnuInventory.Enabled = false;
				}
				else
				{
					mnuInventory.Enabled = true;
				}
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTEDITDOGSKENNELS)) != "F")
				{
					boolReadOnly = true;
					cmdDeleteDog.Enabled = false;
					cmdSave.Enabled = false;
					mnuSaveExit.Enabled = false;
					cmdFees.Enabled = false;
					mnuPrinterSettings.Enabled = false;
					Frame1.Enabled = false;
					chkNotRequired.Enabled = false;
					chkFees.Visible = false;
					framTransaction.Visible = false;
				}
				else
				{
					boolReadOnly = false;
					cmdDeleteDog.Enabled = true;
					cmdSave.Enabled = true;
					mnuSaveExit.Enabled = true;
					cmdFees.Enabled = true;
					mnuPrinterSettings.Enabled = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckPermissions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void chkExcludeWarrant_CheckedChanged(object sender, EventArgs e)
		{

		}

        private void chkDangerous_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDangerous.Checked)
            {
                chkNuisance.CheckState = CheckState.Unchecked;
            }
            RecalcAmount();
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
        }

        private void chkNuisance_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNuisance.Checked)
            {
                chkDangerous.CheckState = CheckState.Unchecked;
            }
            RecalcAmount();
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
        }
    }
}
