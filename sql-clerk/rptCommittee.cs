//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptCommittee.
	/// </summary>
	public partial class rptCommittee : BaseSectionReport
	{
		public rptCommittee()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Group Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCommittee InstancePtr
		{
			get
			{
				return (rptCommittee)Sys.GetInstance(typeof(rptCommittee));
			}
		}

		protected rptCommittee _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsGroups.Dispose();
				clsMembers.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCommittee	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsGroups = new clsDRWrapper();
		clsDRWrapper clsMembers = new clsDRWrapper();
		int lngCurrentGroup;
		int lngPage;

		public void Init(int lngGroupID = -1)
		{
			//FC:FINAL:DDU:#i2320 - grouping for report initialization of field
			this.Fields.Add("grpHeader");
			// -1 defaults to all groups
			lngPage = 1;
			if (lngGroupID > 0)
			{
				clsGroups.OpenRecordset("select * from grouplist where ID = " + FCConvert.ToString(lngGroupID), "twck0000.vb1");
			}
			else
			{
				clsGroups.OpenRecordset("select * from grouplist order by groupname", "twck0000.vb1");
			}
			if (clsGroups.EndOfFile())
			{
				MessageBox.Show("No group data", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			if (lngGroupID > 0)
			{
				clsMembers.OpenRecordset("select * from groupmembers where groupid = " + FCConvert.ToString(lngGroupID) + " order by name", "twck0000.vb1");
			}
			else
			{
				clsMembers.OpenRecordset("select * from groupmembers INNER join grouplist on (grouplist.ID = groupmembers.groupid) order by groupid,name", "twck0000.vb1");
			}
			if (clsMembers.EndOfFile())
			{
				MessageBox.Show("No members data", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			this.Fields["grpHeader"].Value = clsMembers.Get_Fields("groupid");
			lngCurrentGroup = FCConvert.ToInt32(clsMembers.Get_Fields("groupID"));
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Groups");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsMembers.EndOfFile();
			if (!eArgs.EOF)
			{
				this.Fields["grpHeader"].Value = clsMembers.Get_Fields("groupid");
				clsGroups.FindFirstRecord("ID", clsMembers.Get_Fields("groupid"));
				txtDescription.Text = clsGroups.Get_Fields_String("groupdescription");
				txtGroupTitle.Text = clsGroups.Get_Fields_String("groupname") + " Roster";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			if (!clsMembers.EndOfFile())
			{
				bool boolNumbers = false;
				boolNumbers = false;
				lblUserDefined.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsGroups.Get_Fields_String("USERDEFINEDdescription")));
				txtName.Text = clsMembers.Get_Fields_String("Name");
				txtAddress1.Text = clsMembers.Get_Fields_String("Address1");
				txtAddress2.Text = clsMembers.Get_Fields_String("Address2");
				txtCityStateZip.Text = fecherFoundation.Strings.Trim(clsMembers.Get_Fields_String("city") + " " + clsMembers.Get_Fields("State") + " " + clsMembers.Get_Fields_String("Zip") + " " + clsMembers.Get_Fields_String("zip4"));
				strTemp = Strings.Right(Strings.StrDup(10, "0") + clsMembers.Get_Fields_String("telephone"), 10);
				if (Strings.Replace(strTemp, "0", "", 1, -1, CompareConstants.vbTextCompare) != string.Empty)
				{
					txtPhone.Text = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
				}
				else
				{
					txtPhone.Text = "";
				}
				strTemp = Strings.Right(Strings.StrDup(10, "0") + clsMembers.Get_Fields_String("homenumber"), 10);
				if (Strings.Replace(strTemp, "0", "", 1, -1, CompareConstants.vbTextCompare) != string.Empty)
				{
					boolNumbers = true;
					txtHomePhone.Text = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
				}
				else
				{
					txtHomePhone.Text = "";
				}
				strTemp = Strings.Right(Strings.StrDup(10, "0") + clsMembers.Get_Fields_String("cellnumber"), 10);
				if (Strings.Replace(strTemp, "0", "", 1, -1, CompareConstants.vbTextCompare) != string.Empty)
				{
					boolNumbers = true;
					txtCell.Text = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
				}
				else
				{
					txtCell.Text = "";
				}
				strTemp = Strings.Right(Strings.StrDup(10, "0") + clsMembers.Get_Fields_String("othernumber"), 10);
				if (Strings.Replace(strTemp, "0", "", 1, -1, CompareConstants.vbTextCompare) != string.Empty)
				{
					boolNumbers = true;
					txtOther.Text = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
				}
				else
				{
					txtOther.Text = "";
				}
				if (!boolNumbers)
				{
					lblCell.Visible = false;
					txtCell.Visible = false;
					txtHomePhone.Visible = false;
					lblHome.Visible = false;
					lblOther.Visible = false;
					txtOther.Visible = false;
				}
				else
				{
					lblCell.Visible = true;
					txtCell.Visible = true;
					txtHomePhone.Visible = true;
					lblHome.Visible = true;
					lblOther.Visible = true;
					txtOther.Visible = true;
				}
				if (Information.IsDate(clsMembers.Get_Fields("termfrom") + ""))
				{
					if (clsMembers.Get_Fields_DateTime("termfrom").ToOADate() != 0)
					{
						lblTermFrom.Visible = true;
						txtTermFrom.Text = Strings.Format(clsMembers.Get_Fields_DateTime("termfrom"), "MM/dd/yyyy");
					}
					else
					{
						txtTermFrom.Text = "";
						lblTermFrom.Visible = false;
					}
				}
				else
				{
					txtTermFrom.Text = "";
					lblTermFrom.Visible = false;
				}
				if (Information.IsDate(clsMembers.Get_Fields("termto") + ""))
				{
					if (clsMembers.Get_Fields_DateTime("termto").ToOADate() != 0)
					{
						lblTermTo.Visible = true;
						txtTermTo.Text = Strings.Format(clsMembers.Get_Fields_DateTime("termto"), "MM/dd/yyyy");
					}
					else
					{
						txtTermTo.Text = "";
						lblTermTo.Visible = false;
					}
				}
				else
				{
					txtTermTo.Text = "";
					lblTermTo.Visible = false;
				}
				txtUserDefined.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsMembers.Get_Fields_String("userdefined")));
				txtEMail.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsMembers.Get_Fields_String("email")));
				txtTitle.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsMembers.Get_Fields("title")));
				clsMembers.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
			// txtDescription.Text = clsGroups.Fields("groupdescription")
			// txtGroupTitle.Text = clsGroups.Fields("groupname") & " Roster"
		}

		
	}
}
