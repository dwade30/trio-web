//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWCK0000
{
	public partial class frmOnlineDogs : BaseForm
	{
		public frmOnlineDogs()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			Label1 = new System.Collections.Generic.List<FCLabel>();
			Label1.AddControlArrayElement(Label1_0, 0);
			Label1.AddControlArrayElement(Label1_1, 1);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmOnlineDogs InstancePtr
		{
			get
			{
				return (frmOnlineDogs)Sys.GetInstance(typeof(frmOnlineDogs));
			}
		}

		protected frmOnlineDogs _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private struct TransactionInfo
		{
			public string strDogName;
			// vbPorter upgrade warning: strDOB As DateTime	OnWrite(string)
			public DateTime strDOB;
			// vbPorter upgrade warning: strTransDate As DateTime	OnWrite(string)
			public DateTime strTransDate;
			public string strSex;
			public string strBreed;
			public string strColor;
			public string strSpay;
			public string strVet;
			public string strRabiesCert;
			public string strTransType;
			public Decimal curTransFee;
			public string strTag;
			public string strRabiesDate;
			public string strSNCert;
		};

		private struct OwnerInfo
		{
			public int lngID;
			public string strOwner;
			public string strOwnerLast;
			public string strOwnerFirst;
			public string strOwnerMI;
			public string strPhone;
			public string strAddress1;
			public string strAddress2;
			public string strCity;
			public string strState;
			public string strZip;
			public string strLocation;
			public string strEmail;
		};

		const int CNSTGRIDCOLOWNERID = 0;
		const int CNSTGRIDCOLDOGID = 1;
		const int CNSTGRIDCOLDOGNAME = 2;
		const int CNSTGRIDCOLTRANSACTIONDATE = 3;
		const int CNSTGRIDCOLTRANSTYPE = 4;
		const int CNSTGRIDCOLTRANSFEE = 5;
		const int CNSTGRIDCOLOWNERFIRST = 6;
		const int CNSTGRIDCOLOWNERMIDDLE = 7;
		const int CNSTGRIDCOLOWNERLAST = 8;
		const int CNSTGRIDCOLBREED = 9;
		const int CNSTGRIDCOLSEX = 10;
		const int CNSTGRIDCOLCOLOR = 11;
		const int CNSTGRIDCOLNEUTER = 12;
		const int CNSTGRIDCOLVET = 13;
		const int CNSTGRIDCOLRABIESCERT = 14;
		// apparently is actually the tag
		const int CNSTGRIDCOLTAG = 15;
		const int CNSTGRIDCOLADDRESS1 = 16;
		const int CNSTGRIDCOLADDRESS2 = 17;
		const int CNSTGRIDCOLCITY = 18;
		const int CNSTGRIDCOLSTATE = 19;
		const int CNSTGRIDCOLZIP = 20;
		const int CNSTGRIDCOLLOCATION = 21;
		const int CNSTGRIDCOLPHONE = 22;
		const int CNSTGRIDCOLEMAIL = 23;
		const int CNSTGRIDCOLRABIESISSUEDATE = 24;
		const int CNSTGRIDCOLDOGDOB = 25;
		const int CNSTGRIDCOLNEUTERCERT = 26;
		const int CNSTGRIDCOLREASON = 27;
		const int CNSTGRIDCOLRABIESEXPIRATIONDATE = 28;
		private object[] lngAddedParties = null;
		private int intAddedPartyCounter;
		PartyUtil temp = new PartyUtil();

		private void frmOnlineDogs_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmOnlineDogs_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			if (!modGlobalConstants.Statics.gboolUse256Colors)
			{
				Shape1.FillColor(ColorTranslator.FromOle(Information.RGB(255, 175, 45)));
			}
			SetupGrids();
            LoadData();
		}

		private void SetupGrids()
		{
			Grid.Cols = 29;
			Grid.ColHidden(CNSTGRIDCOLDOGID, true);
			Grid.ColHidden(CNSTGRIDCOLOWNERID, true);
			Grid.ColHidden(CNSTGRIDCOLADDRESS2, true);
			Grid.ColHidden(CNSTGRIDCOLOWNERMIDDLE, true);
			Grid.ColHidden(CNSTGRIDCOLRABIESISSUEDATE, true);
			Grid.ColHidden(CNSTGRIDCOLTRANSTYPE, true);
			Grid.ColHidden(CNSTGRIDCOLDOGDOB, true);
			Grid.ColHidden(CNSTGRIDCOLNEUTERCERT, true);
			Grid.ColHidden(CNSTGRIDCOLREASON, true);
			Grid.ColHidden(CNSTGRIDCOLRABIESEXPIRATIONDATE, true);
			Grid.TextMatrix(0, CNSTGRIDCOLDOGNAME, "Dog");
			Grid.TextMatrix(0, CNSTGRIDCOLTRANSACTIONDATE, "Date");
			Grid.TextMatrix(0, CNSTGRIDCOLTRANSTYPE, "Type");
			Grid.TextMatrix(0, CNSTGRIDCOLTRANSFEE, "Fee");
			Grid.TextMatrix(0, CNSTGRIDCOLOWNERFIRST, "First");
			Grid.TextMatrix(0, CNSTGRIDCOLOWNERLAST, "Last");
			Grid.TextMatrix(0, CNSTGRIDCOLBREED, "Breed");
			Grid.TextMatrix(0, CNSTGRIDCOLSEX, "Sex");
			Grid.TextMatrix(0, CNSTGRIDCOLCOLOR, "Color");
			Grid.TextMatrix(0, CNSTGRIDCOLNEUTER, "Spay/Neuter");
			Grid.TextMatrix(0, CNSTGRIDCOLVET, "Vet");
			Grid.TextMatrix(0, CNSTGRIDCOLRABIESCERT, "Rabies Cert");
			Grid.TextMatrix(0, CNSTGRIDCOLTAG, "Tag");
			Grid.TextMatrix(0, CNSTGRIDCOLADDRESS1, "Address");
			Grid.TextMatrix(0, CNSTGRIDCOLADDRESS2, "Address");
			Grid.TextMatrix(0, CNSTGRIDCOLCITY, "City");
			Grid.TextMatrix(0, CNSTGRIDCOLSTATE, "State");
			Grid.TextMatrix(0, CNSTGRIDCOLZIP, "Zip");
			Grid.TextMatrix(0, CNSTGRIDCOLPHONE, "Phone");
			Grid.TextMatrix(0, CNSTGRIDCOLEMAIL, "EMail");
			Grid.TextMatrix(0, CNSTGRIDCOLRABIESISSUEDATE, "Rabies Date");
			GridNew.Cols = 29;
			GridNew.ColHidden(CNSTGRIDCOLNEUTERCERT, true);
			GridNew.ColHidden(CNSTGRIDCOLDOGID, true);
			GridNew.ColHidden(CNSTGRIDCOLOWNERID, true);
			GridNew.ColHidden(CNSTGRIDCOLADDRESS2, true);
			GridNew.ColHidden(CNSTGRIDCOLOWNERMIDDLE, true);
			GridNew.ColHidden(CNSTGRIDCOLRABIESISSUEDATE, true);
			GridNew.ColHidden(CNSTGRIDCOLTRANSTYPE, true);
			GridNew.ColHidden(CNSTGRIDCOLDOGDOB, true);
			GridNew.ColHidden(CNSTGRIDCOLREASON, true);
			GridNew.ColHidden(CNSTGRIDCOLRABIESEXPIRATIONDATE, true);
			GridNew.TextMatrix(0, CNSTGRIDCOLDOGNAME, "Dog");
			GridNew.TextMatrix(0, CNSTGRIDCOLTRANSACTIONDATE, "Date");
			GridNew.TextMatrix(0, CNSTGRIDCOLTRANSTYPE, "Type");
			GridNew.TextMatrix(0, CNSTGRIDCOLTRANSFEE, "Fee");
			GridNew.TextMatrix(0, CNSTGRIDCOLOWNERFIRST, "First");
			GridNew.TextMatrix(0, CNSTGRIDCOLOWNERLAST, "Last");
			GridNew.TextMatrix(0, CNSTGRIDCOLBREED, "Breed");
			GridNew.TextMatrix(0, CNSTGRIDCOLSEX, "Sex");
			GridNew.TextMatrix(0, CNSTGRIDCOLCOLOR, "Color");
			GridNew.TextMatrix(0, CNSTGRIDCOLNEUTER, "Spay/Neuter");
			GridNew.TextMatrix(0, CNSTGRIDCOLVET, "Vet");
			GridNew.TextMatrix(0, CNSTGRIDCOLRABIESCERT, "Rabies Tag");
			GridNew.TextMatrix(0, CNSTGRIDCOLTAG, "Tag");
			GridNew.TextMatrix(0, CNSTGRIDCOLADDRESS1, "Address");
			GridNew.TextMatrix(0, CNSTGRIDCOLADDRESS2, "Address");
			GridNew.TextMatrix(0, CNSTGRIDCOLCITY, "City");
			GridNew.TextMatrix(0, CNSTGRIDCOLSTATE, "State");
			GridNew.TextMatrix(0, CNSTGRIDCOLZIP, "Zip");
			GridNew.TextMatrix(0, CNSTGRIDCOLPHONE, "Phone");
			GridNew.TextMatrix(0, CNSTGRIDCOLEMAIL, "EMail");
			GridNew.TextMatrix(0, CNSTGRIDCOLRABIESISSUEDATE, "Rabies Date");
            //FC:FINAL:SBE - #2465 - autoresize all cells. GridNew.AutoSize() does not work if it is called just after grid was populated
            foreach (DataGridViewColumn col in Grid.Columns)
            {
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
            foreach (DataGridViewColumn col in GridNew.Columns)
            {
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
        }

		private void LoadData()
		{
			try
			{
				fecherFoundation.Information.Err().Clear();
				string strFileName;
                FCFileSystem fso = new FCFileSystem();
				StreamReader tsInfo;
				string strLine = "";
				string[] strData = null;
				int lngRow;
				OwnerInfo curOwner = new OwnerInfo();
				TransactionInfo curTrans = new TransactionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				bool boolMatch = false;
				int intMatch = 0;
				string strTemp = "";
				string strSQL = "";
				fecherFoundation.Information.Err().Clear();
				MDIParent.InstancePtr.CommonDialog1.Filter = "*.csv";
				MDIParent.InstancePtr.CommonDialog1.FileName = "*.csv";
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (FCFileSystem.FileExists(strFileName))
				{
					bool skipFirst = true;
					foreach (string line in File.ReadLines(strFileName))
					{
						if (skipFirst)
						{
							skipFirst = false;
							continue;
						}
						strLine = line;
						strLine = strLine.Replace("&amp;", "&");
						strData = modGlobalRoutines.SplitQuotedText(ref strLine, ",");
						curOwner.strOwnerFirst = strData[5];
						curOwner.strOwnerLast = strData[7];
						curOwner.strOwnerMI = strData[6];
						curOwner.strEmail = strData[12];
						curOwner.strAddress1 = fecherFoundation.Strings.Trim(strData[13]);
						curOwner.strAddress2 = fecherFoundation.Strings.Trim(strData[14]);
						curOwner.strCity = fecherFoundation.Strings.Trim(strData[15]);
						curOwner.strState = fecherFoundation.Strings.Trim(strData[16]);
						curOwner.strZip = Strings.Format(fecherFoundation.Strings.Trim(strData[17]), "00000");
						curOwner.strLocation = fecherFoundation.Strings.Trim(strData[18]);
						curOwner.strPhone = fecherFoundation.Strings.Trim(strData[11]).Replace("-", "");
						strTemp = fecherFoundation.Strings.Trim(strData[20].Replace("\"", ""));
						if (strTemp.Length == 6)
						{
							strTemp = Strings.Right(strTemp, 2) + "/1/" + Strings.Left(strTemp, 4);
						}
						if (Information.IsDate(strTemp))
						{
							curTrans.strDOB = FCConvert.ToDateTime(strTemp);
						}
						else
						{
							curTrans.strDOB = FCConvert.ToDateTime("");
						}
						curTrans.strDogName = fecherFoundation.Strings.Trim(strData[19]);
						curTrans.strRabiesCert = fecherFoundation.Strings.Trim(strData[37]);
						curTrans.strSex = fecherFoundation.Strings.Trim(strData[21]);
						curTrans.strSpay = fecherFoundation.Strings.Trim(strData[25]);
						curTrans.strBreed = fecherFoundation.Strings.Trim(strData[22]);
						curTrans.strColor = fecherFoundation.Strings.Trim(strData[23]);
						curTrans.strTransDate = FCConvert.ToDateTime(fecherFoundation.Strings.Trim(strData[29]));
						curTrans.curTransFee = FCConvert.ToDecimal(fecherFoundation.Strings.Trim(strData[26]));
						curTrans.strTransType = fecherFoundation.Strings.Trim(strData[2]);
						curTrans.strVet = fecherFoundation.Strings.Trim(strData[24]);
						curTrans.strTag = fecherFoundation.Strings.Trim(strData[3]);
						curTrans.strRabiesDate = fecherFoundation.Strings.Trim(strData[38]);
						curTrans.strSNCert = fecherFoundation.Strings.Trim(strData[30]);
						// now determine if we can match the record
						boolMatch = false;
						intMatch = 0;
						// strSQL = "select *, DogOwner.ID as DogOwnerID, DogInfo.ID as DogInfoID from doginfo inner join dogowner on (doginfo.ownernum = dogowner.ID) CROSS APPLY " & rsLoad.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddressAndPhone(PartyID, NULL, 'CK', NULL) as p where doginfo.ownernum > 0 and dogdeceased <> 1 and dogname = '" & EscapeQuotes(curTrans.strDogName) & "' order by ID,doginfo.ownernum"
						strSQL = "select *, DogOwner.ID as DogOwnerID, DogInfo.ID as DogInfoID from doginfo inner join dogowner on (doginfo.ownernum = dogowner.id) inner join " + rsLoad.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView on (dogowner.partyid = " + rsLoad.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView.PartyID) ";
						strSQL += " where doginfo.ownernum > 0 and isnull(dogdeceased,0) <> 1 and dogname = '" + modGlobalFunctions.EscapeQuotes(curTrans.strDogName) + "' order by " + rsLoad.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView.ID,doginfo.ownernum";
						rsLoad.OpenRecordset(strSQL, "twck0000.vb1");
						while (!rsLoad.EndOfFile())
						{
							if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(curOwner.strOwnerLast)) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("lastname")))))
							{
								intMatch = 1;
								if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(curOwner.strOwnerFirst)) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("firstname")))))
								{
									intMatch += 1;
									boolMatch = true;
									break;
								}
								else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curOwner.strLocation)) == fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("locationnum") + " " + rsLoad.Get_Fields_String("locationstr"))) && fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curOwner.strLocation)) != "")
								{
									intMatch += 1;
									boolMatch = true;
									break;
								}
							}
							if (!boolMatch)
							{
								if (curTrans.strTag != "" && curTrans.strTag == rsLoad.Get_Fields_String("taglicnum"))
								{
									intMatch += 1;
									boolMatch = true;
									break;
								}
								if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curOwner.strLocation)) == fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("locationnum") + " " + rsLoad.Get_Fields_String("locationstr"))) && fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curOwner.strLocation)) != "")
								{
									intMatch += 1;
									if (intMatch > 1)
									{
										boolMatch = true;
										break;
									}
								}
								if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curOwner.strAddress1)) != "" && fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curOwner.strAddress1)) == fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("address1")))))
								{
									intMatch += 1;
								}
								if (fecherFoundation.Strings.Trim(curOwner.strPhone) == fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("phone"))) && fecherFoundation.Strings.Trim(curOwner.strPhone) != "")
								{
									intMatch += 1;
								}
								if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curTrans.strRabiesCert)) != "" && fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curTrans.strRabiesCert)) == fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rabiescertnum")))))
								{
									intMatch += 1;
								}
							}
							if (intMatch >= 2)
							{
								boolMatch = true;
								break;
							}
							rsLoad.MoveNext();
						}
						if (boolMatch)
						{
							Grid.Rows += 1;
							lngRow = Grid.Rows - 1;
							Grid.RowData(lngRow, false);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERID, FCConvert.ToString(rsLoad.Get_Fields("dogownerID")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGID, FCConvert.ToString(rsLoad.Get_Fields("DogInfoID")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1, curOwner.strAddress1);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2, curOwner.strAddress2);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLBREED, curTrans.strBreed);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY, curOwner.strCity);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLCOLOR, curTrans.strColor);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME, curTrans.strDogName);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, curOwner.strEmail);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCATION, curOwner.strLocation);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTER, curTrans.strSpay);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST, curOwner.strOwnerFirst);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST, curOwner.strOwnerLast);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE, curOwner.strOwnerMI);
							if (curOwner.strPhone != string.Empty)
							{
								strTemp = Strings.Right(Strings.StrDup(10, "0") + curOwner.strPhone, 10);
								strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
							}
							else
							{
								strTemp = "(000)000-0000";
							}
							Grid.TextMatrix(lngRow, CNSTGRIDCOLPHONE, strTemp);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT, curTrans.strRabiesCert);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE, curTrans.strRabiesDate);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLSEX, curTrans.strSex);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE, curOwner.strState);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG, curTrans.strTag);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE, FCConvert.ToString(curTrans.strTransDate));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSFEE, Strings.Format(curTrans.curTransFee, "0.00"));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSTYPE, curTrans.strTransType);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLVET, curTrans.strVet);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP, curOwner.strZip);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGDOB, FCConvert.ToString(curTrans.strDOB));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTERCERT, curTrans.strSNCert);
						}
						else
						{
							GridNew.Rows += 1;
							lngRow = GridNew.Rows - 1;
							GridNew.RowData(lngRow, false);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERID, FCConvert.ToString(0));
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGID, FCConvert.ToString(0));
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1, curOwner.strAddress1);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2, curOwner.strAddress2);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLBREED, curTrans.strBreed);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLCITY, curOwner.strCity);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLCOLOR, curTrans.strColor);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME, curTrans.strDogName);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, curOwner.strEmail);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLLOCATION, curOwner.strLocation);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLNEUTER, curTrans.strSpay);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST, curOwner.strOwnerFirst);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST, curOwner.strOwnerLast);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE, curOwner.strOwnerMI);
							if (curOwner.strPhone != string.Empty)
							{
								strTemp = Strings.Right(Strings.StrDup(10, "0") + curOwner.strPhone, 10);
								strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
							}
							else
							{
								strTemp = "(000)000-0000";
							}
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLPHONE, strTemp);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT, curTrans.strRabiesCert);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE, curTrans.strRabiesDate);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLSEX, curTrans.strSex);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLSTATE, curOwner.strState);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLTAG, curTrans.strTag);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE, FCConvert.ToString(curTrans.strTransDate));
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSFEE, Strings.Format(curTrans.curTransFee, "0.00"));
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSTYPE, curTrans.strTransType);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLVET, curTrans.strVet);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLZIP, curOwner.strZip);
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGDOB, FCConvert.ToString(curTrans.strDOB));
							GridNew.TextMatrix(lngRow, CNSTGRIDCOLNEUTERCERT, curTrans.strSNCert);
							if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(curTrans.strTransType)) != "N")
							{
								GridNew.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridNew.Cols - 1, Shape1.FillColor());
							}
						}
					}
                    //GridNew.AutoResize = true;
                    //FC:FINAL:SBE - #2465 - autoresize all cells. GridNew.AutoSize() does not work if it is called just after grid was populated
                    //GridNew.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
                    //GridNew.AutoSize(0, GridNew.Cols - 1);
                    //Grid.AutoSize(0, Grid.Cols - 1);
                }
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void frmOnlineDogs_Resize(object sender, System.EventArgs e)
		{
			GridNew.AutoSize(0, GridNew.Cols - 1);
			Grid.AutoSize(0, Grid.Cols - 1);
		}

		private void Grid_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			//FC:FINAL:DDU:#2469 - removed functionality for changing popup menu with button
			//if (e.Button == MouseButtons.Right)
			//{
			//	int lngMouseRow = 0;
			//	lngMouseRow = Grid.MouseRow;
			//	//FC:FINAL:DDU:#i2337 - changed menu to toolbar button
			//	if (lngMouseRow > 0)
			//	{
			//		Grid.Row = lngMouseRow;
			//		//PopupMenu(mnuPopUp);
			//		cmdUnmatch.Visible = true;
			//	}
			//	else
			//	{
			//		cmdUnmatch.Visible = false;
			//	}
			//}
		}

		private void GridNew_DblClick(object sender, System.EventArgs e)
		{
			// VB6 Bad Scope Dim:
			int lngRow;
			int lngReturn;
			int lngCurRow;
			if (GridNew.MouseRow < 1)
				return;
			lngCurRow = GridNew.MouseRow;
			lngReturn = frmSelectDog.InstancePtr.Init();
			if (lngReturn > 0)
			{
				// copy it
				clsDRWrapper rsLoad = new clsDRWrapper();
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGID, FCConvert.ToString(lngReturn));
				rsLoad.OpenRecordset("select OWNERNUM from doginfo where ID = " + FCConvert.ToString(lngReturn), "twck0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ownernum")));
				}
				Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLADDRESS1));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLADDRESS2));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLBREED, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLBREED));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLCITY));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCOLOR, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLCOLOR));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGDOB, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLDOGDOB));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLDOGNAME));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLEMAIL));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCATION, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLLOCATION));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTER, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLNEUTER));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTERCERT, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLNEUTERCERT));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLOWNERFIRST));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLOWNERLAST));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLOWNERMIDDLE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLPHONE, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLPHONE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLRABIESCERT));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLRABIESISSUEDATE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLREASON, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLREASON));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLSEX, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLSEX));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLSTATE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLTAG));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLTRANSACTIONDATE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSFEE, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLTRANSFEE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSTYPE, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLTRANSTYPE));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLVET, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLVET));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP, GridNew.TextMatrix(lngCurRow, CNSTGRIDCOLZIP));
				Grid.RowData(lngRow, false);
				GridNew.RemoveItem(lngCurRow);
				Grid.AutoSize(0, Grid.Cols - 1);
				GridNew.AutoSize(0, GridNew.Cols - 1);
			}
		}

		private void GridNew_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Delete)
			{
				KeyCode = 0;
				if (GridNew.Row < 1)
					return;
				GridNew.RemoveItem(GridNew.Row);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPopUp_Click(object sender, System.EventArgs e)
		{
			// mnupopup.Visible = False
		}

		private void mnuPrintUnmatched_Click(object sender, System.EventArgs e)
		{
			rptUnmatchedRegistrations.InstancePtr.Init();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper conInfo = new clsDRWrapper();
			Process();

		}

		private void Process()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngRow;
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsDog = new clsDRWrapper();
				clsDRWrapper rsOwn = new clsDRWrapper();
				int lngDog = 0;
				int lngOwnerID = 0;
				string strTemp = "";
				string[] strAry = null;
				// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
				int intTemp = 0;
				int lngMRow;
				int lngYear = 0;
				clsDRWrapper rsParty = new clsDRWrapper();
				clsDRWrapper rsAddress = new clsDRWrapper();
				clsDRWrapper rsPhone = new clsDRWrapper();
				cCreateGUID clsGuid = new cCreateGUID();
				bool boolMatch = false;
				// create new records first
				frmWait.InstancePtr.Init("Processing Records");
                for (lngRow = GridNew.Rows - 1; lngRow >= 1; lngRow--)
                {
                    if (Conversion.Val(GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERID)) == 0 ||
                        Conversion.Val(GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGID)) == 0)
                    {
                        // And UCase(Trim(GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSTYPE))) = "N" Then
                        rsParty.OpenRecordset(
                            "SELECT * FROM Parties WHERE PartyType = 0 AND upper(FirstName) = '" +
                            fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(
                                modGlobalFunctions.EscapeQuotes(GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST)))) +
                            "' AND upper(isnull(LastName, '')) = '" +
                            fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(
                                modGlobalFunctions.EscapeQuotes(GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST)))) +
                            "' AND upper(isnull(MiddleName, '')) = '" +
                            fecherFoundation.Strings.UCase(
                                fecherFoundation.Strings.Trim(GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE))) +
                            "'", "CentralParties");
                        boolMatch = false;
                        while (!rsParty.EndOfFile() && !boolMatch)
                        {
                            //App.DoEvents();
                            if (fecherFoundation.Strings.Trim(GridNew.TextMatrix(lngRow, CNSTGRIDCOLEMAIL)) != "")
                            {
                                if (fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(rsParty.Get_Fields_String("email"))) != "")
                                {
                                    if (fecherFoundation.Strings.LCase(
                                            fecherFoundation.Strings.Trim(GridNew.TextMatrix(lngRow,
                                                CNSTGRIDCOLEMAIL))) ==
                                        fecherFoundation.Strings.LCase(
                                            fecherFoundation.Strings.Trim(
                                                FCConvert.ToString(rsParty.Get_Fields_String("email")))))
                                    {
                                        boolMatch = true;
                                    }
                                }
                            }

                            if (!boolMatch)
                            {
                                rsAddress.OpenRecordset(
                                    "select * from addresses where partyid = " + rsParty.Get_Fields_Int32("id"),
                                    "CentralParties");
                                if (fecherFoundation.Strings.Trim(GridNew.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1)) !=
                                    "")
                                {
                                    if (fecherFoundation.Strings.LCase(
                                            fecherFoundation.Strings.Trim(GridNew.TextMatrix(lngRow,
                                                CNSTGRIDCOLADDRESS1))) ==
                                        fecherFoundation.Strings.LCase(
                                            fecherFoundation.Strings.Trim(
                                                FCConvert.ToString(rsAddress.Get_Fields_String("Address1")))))
                                    {
                                        boolMatch = true;
                                    }
                                }
                            }

                            if (!boolMatch)
                                rsParty.MoveNext();
                        }

                        if (boolMatch)
                        {
                            if (fecherFoundation.Strings.Trim(GridNew.TextMatrix(lngRow, CNSTGRIDCOLEMAIL)) != "")
                            {
                                if (fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(rsParty.Get_Fields_String("Email"))) == "")
                                {
                                    rsParty.Edit();
                                    rsParty.Set_Fields("email", GridNew.TextMatrix(lngRow, CNSTGRIDCOLEMAIL));
                                    rsParty.Update();
                                }
                            }
                        }

                        if (!boolMatch)
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 0);
                            rsParty.Set_Fields("FirstName",
                                fecherFoundation.Strings.Trim(
                                    modGlobalFunctions.EscapeQuotes(GridNew.TextMatrix(lngRow,
                                        CNSTGRIDCOLOWNERFIRST))));
                            rsParty.Set_Fields("LastName",
                                fecherFoundation.Strings.Trim(
                                    modGlobalFunctions.EscapeQuotes(GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST))));
                            rsParty.Set_Fields("MiddleName",
                                fecherFoundation.Strings.Trim(
                                    modGlobalFunctions.EscapeQuotes(GridNew.TextMatrix(lngRow,
                                        CNSTGRIDCOLOWNERMIDDLE))));
                            rsParty.Set_Fields("Email", GridNew.TextMatrix(lngRow, CNSTGRIDCOLEMAIL));
                            rsParty.Set_Fields("Designation", "");
                            rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                            rsParty.Set_Fields("DateCreated", DateTime.Today);
                            rsParty.Set_Fields("CreatedBy",
                                modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Update();
                            Array.Resize(ref lngAddedParties, intAddedPartyCounter + 1);
                            lngAddedParties[intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            intAddedPartyCounter += 1;
                            rsAddress.OpenRecordset("SELECT * FROM Addresses", "CentralParties");
                            rsAddress.AddNew();
                            rsAddress.Set_Fields("PartyID", rsParty.Get_Fields_Int32("ID"));
                            rsAddress.Set_Fields("AddressType", "Primary");
                            rsAddress.Set_Fields("Address1", GridNew.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1));
                            rsAddress.Set_Fields("City", GridNew.TextMatrix(lngRow, CNSTGRIDCOLCITY));
                            rsAddress.Set_Fields("State", GridNew.TextMatrix(lngRow, CNSTGRIDCOLSTATE));
                            rsAddress.Set_Fields("Zip", GridNew.TextMatrix(lngRow, CNSTGRIDCOLZIP));
                            rsAddress.Update();
                            strTemp = GridNew.TextMatrix(lngRow, CNSTGRIDCOLPHONE);
                            strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
                            strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
                            strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
                            // rsSave.Fields("phone") = strTemp
                            if (strTemp != "")
                            {
                                if (strTemp.Length < 10)
                                {
                                    strTemp = Strings.StrDup(10 - strTemp.Length, "0") + strTemp;
                                }

                                rsPhone.OpenRecordset("SELECT * FROM CentralPhoneNumbers", "CentralParties");
                                rsPhone.AddNew();
                                rsPhone.Set_Fields("PartyID", rsParty.Get_Fields_Int32("ID"));
                                rsPhone.Set_Fields("PhoneNumber", strTemp);
                                rsPhone.Set_Fields("PhoneOrder", 1);
                                rsPhone.Set_Fields("Description", "Main");
                                rsPhone.Update();
                            }
                        }

                        //App.DoEvents();
                        rsSave.OpenRecordset("select * from dogowner where PartyID = " + rsParty.Get_Fields_Int32("ID"),
                            "twck0000.vb1");
                        if (rsSave.EndOfFile())
                        {
                            rsSave.AddNew();
                            rsSave.Set_Fields("PartyID", rsParty.Get_Fields_Int32("ID"));
                            strTemp = GridNew.TextMatrix(lngRow, CNSTGRIDCOLLOCATION);
                            if (strTemp != "")
                            {
                                intTemp = Strings.InStr(1, strTemp, " ", CompareConstants.vbTextCompare);
                                if (Conversion.Val(strTemp) > 0 && intTemp > 0)
                                {
                                    rsSave.Set_Fields("locationnum", Strings.Mid(strTemp, 1, intTemp - 1));
                                    rsSave.Set_Fields("locationstr", Strings.Mid(strTemp, intTemp + 1));
                                }
                                else
                                {
                                    rsSave.Set_Fields("locationnum", "");
                                    rsSave.Set_Fields("locationstr", strTemp);
                                }
                            }
                            else
                            {
                                rsSave.Set_Fields("locationnum", "");
                                rsSave.Set_Fields("locationstr", "");
                            }

                            rsSave.Set_Fields("lastupdated", DateTime.Now);
                            rsSave.Set_Fields("towncode", 0);
                        }

                        rsSave.Update();
                        lngOwnerID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
                        rsDog.OpenRecordset("select * from doginfo where ID = -10", "twck0000.vb1");
                        rsDog.AddNew();
                        GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERID, FCConvert.ToString(lngOwnerID));

                        rsDog.Set_Fields("ownernum", lngOwnerID);
                        rsDog.Set_Fields("dogname", GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME));
                        rsDog.Set_Fields("dogbreed", GridNew.TextMatrix(lngRow, CNSTGRIDCOLBREED));
                        rsDog.Set_Fields("dogcolor", GridNew.TextMatrix(lngRow, CNSTGRIDCOLCOLOR));
                        rsDog.Set_Fields("dogvet", GridNew.TextMatrix(lngRow, CNSTGRIDCOLVET));
                        rsDog.Set_Fields("sncertnum", GridNew.TextMatrix(lngRow, CNSTGRIDCOLNEUTERCERT));
                        if (fecherFoundation.Strings.Trim(GridNew.TextMatrix(lngRow, CNSTGRIDCOLNEUTER)) == "Y")
                        {
                            rsDog.Set_Fields("DogNeuter", true);
                        }
                        else
                        {
                            rsDog.Set_Fields("DogNeuter", false);
                        }

                        rsDog.Set_Fields("stickerlicnum", 0);
                        rsDog.Set_Fields("dogmarkings", "");
                        rsDog.Set_Fields("previoustag", rsDog.Get_Fields_String("Taglicnum"));
                        rsDog.Set_Fields("rabiestagno", GridNew.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT));
                        rsDog.Set_Fields("rabiescertnum", GridNew.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT));
                        rsDog.Set_Fields("rabstickernum", "");
                        rsDog.Set_Fields("kenneldog", false);
                        rsDog.Set_Fields("norequiredfields", false);
                        rsDog.Set_Fields("wolfhybrid", false);
                        rsDog.Set_Fields("dogdeceased", false);
                        rsDog.Set_Fields("hearguide", false);
                        rsDog.Set_Fields("sandr", false);
                        rsDog.Set_Fields("transfer", false);
                        rsDog.Set_Fields("replacement", false);
                        rsDog.Set_Fields("dogneuter", false);
                        rsDog.Set_Fields("dogsex", "");
                        rsDog.Set_Fields("PreviousOwner", 0);
                        rsDog.Set_Fields("Comments", "");
                        rsDog.Set_Fields("Warrantyear", 0);
                        rsDog.Set_Fields("ExcludeFromWarrant", false);
                        rsDog.Set_Fields("PreviousTag", "");
                        rsDog.Set_Fields("rabiesExempt", false);
                        if (Information.IsDate(GridNew.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE)))
                        {
                            rsDog.Set_Fields("rabiesexpdate", GridNew.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE));
                        }
                        else
                        {
                            rsDog.Set_Fields("rabiesexpdate", 0);
                        }

                        // rsDog.Fields ("dogdob")
                        if (Information.IsDate(GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGDOB)))
                        {
                            rsDog.Set_Fields("dogdob", GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGDOB));
                        }
                        else
                        {
                            rsDog.Set_Fields("dogdob", 0);
                        }

                        rsDog.Update();
                        lngDog = FCConvert.ToInt32(rsDog.Get_Fields_Int32("ID"));
                        GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGID, FCConvert.ToString(lngDog));
                        rsSave.Edit();
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsSave.Get_Fields_String("dognumbers"))) ==
                            string.Empty)
                        {
                            rsSave.Set_Fields("dognumbers", lngDog);
                        }
                        else
                        {
                            rsSave.Set_Fields("dognumbers",
                                rsSave.Get_Fields_String("dognumbers") + "," + FCConvert.ToString(lngDog));
                        }

                        rsSave.Update();
                        //App.DoEvents();
                        Grid.Rows += 1;
                        lngMRow = Grid.Rows - 1;
                        Grid.RowData(lngMRow, false);
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLADDRESS1, GridNew.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLADDRESS2, GridNew.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLBREED, GridNew.TextMatrix(lngRow, CNSTGRIDCOLBREED));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLCITY, GridNew.TextMatrix(lngRow, CNSTGRIDCOLCITY));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLCOLOR, GridNew.TextMatrix(lngRow, CNSTGRIDCOLCOLOR));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLDOGDOB, GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGDOB));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLDOGID, GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGID));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLDOGNAME, GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLEMAIL, GridNew.TextMatrix(lngRow, CNSTGRIDCOLEMAIL));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLLOCATION, GridNew.TextMatrix(lngRow, CNSTGRIDCOLLOCATION));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLNEUTER, GridNew.TextMatrix(lngRow, CNSTGRIDCOLNEUTER));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLOWNERFIRST,
                            GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLOWNERID, GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERID));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLOWNERLAST,
                            GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLOWNERMIDDLE,
                            GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLPHONE, GridNew.TextMatrix(lngRow, CNSTGRIDCOLPHONE));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLRABIESCERT,
                            GridNew.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLRABIESISSUEDATE,
                            GridNew.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLSEX, GridNew.TextMatrix(lngRow, CNSTGRIDCOLSEX));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLSTATE, GridNew.TextMatrix(lngRow, CNSTGRIDCOLSTATE));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLTAG, GridNew.TextMatrix(lngRow, CNSTGRIDCOLTAG));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLTRANSACTIONDATE,
                            GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLTRANSFEE, GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSFEE));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLTRANSTYPE,
                            GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSTYPE));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLVET, GridNew.TextMatrix(lngRow, CNSTGRIDCOLVET));
                        Grid.TextMatrix(lngMRow, CNSTGRIDCOLZIP, GridNew.TextMatrix(lngRow, CNSTGRIDCOLZIP));
                        GridNew.RemoveItem(lngRow);
                        //}
                    } // lngRow
                }

                Grid.AutoSize(0, Grid.Cols - 1);
						GridNew.AutoSize(0, GridNew.Cols - 1);
						// GridNew.Rows = 1
						// vbPorter upgrade warning: lngClerk As int	OnWrite(double, int)
						int lngClerk = 0;
						// vbPorter upgrade warning: lngState As int	OnWrite(double, int)
						int lngState = 0;
						// vbPorter upgrade warning: lngTown As int	OnWrite(double, int)
						int lngTown = 0;
						// vbPorter upgrade warning: lngLate As int	OnWrite(int, Decimal)
						int lngLate = 0;
						// vbPorter upgrade warning: lngWarrant As int	OnWrite(int, Decimal)
						int lngWarrant = 0;
						int lngAmount = 0;
						int lngLeft = 0;
						bool boolNeutered = false;
						bool boolProcess = false;
						int lngTranID = 0;
						int lngNewTag = 0;
						int lngLoadedTag = 0;
						modDogs.GetDogFees();
						rsDog.OpenRecordset("select * from settingsfees", "twck0000.vb1");
						for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
						{
							if (!FCConvert.ToBoolean(Grid.RowData(lngRow)))
							{
								boolProcess = true;
								lngYear = FCConvert.ToDateTime(Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE)).Year;
								if (FCConvert.ToDateTime(Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE)).Month >= 10)
								{
									lngYear += 1;
								}
								if (DateTime.Today.Month < 10)
								{
									if (lngYear > DateTime.Today.Year)
									{
										lngYear = DateTime.Today.Year;
									}
								}
								else
								{
									if (lngYear > DateTime.Today.Year + 1)
									{
										lngYear = DateTime.Today.Year + 1;
									}
								}
								lngLoadedTag = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG))));
								lngNewTag = 0;
								lngAmount = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSFEE))));
								lngLeft = lngAmount;
								if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTER))) == "Y")
								{
									boolNeutered = true;
								}
								else
								{
									boolNeutered = false;
								}
								if (boolNeutered)
								{
									if (lngAmount >= modDogs.Statics.DogFee.FixedClerk + modDogs.Statics.DogFee.FixedState + modDogs.Statics.DogFee.FixedTown)
									{
										boolProcess = true;
										lngTown = FCConvert.ToInt32(modDogs.Statics.DogFee.FixedTown);
										lngState = FCConvert.ToInt32(modDogs.Statics.DogFee.FixedState);
										lngClerk = FCConvert.ToInt32(modDogs.Statics.DogFee.FixedClerk);
										lngLeft = lngAmount - lngTown - lngState - lngClerk;
									}
									else
									{
										lngTown = 0;
										lngState = 0;
										lngClerk = 0;
										lngWarrant = 0;
										lngLate = 0;
									}
								}
								else
								{
									if (lngAmount >= modDogs.Statics.DogFee.DogClerk + modDogs.Statics.DogFee.DogState + modDogs.Statics.DogFee.DogTown)
									{
										boolProcess = true;
										lngTown = FCConvert.ToInt32(modDogs.Statics.DogFee.DogTown);
										lngState = FCConvert.ToInt32(modDogs.Statics.DogFee.DogState);
										lngClerk = FCConvert.ToInt32(modDogs.Statics.DogFee.DogClerk);
										lngLeft = lngAmount - lngTown - lngState - lngClerk;
									}
									else
									{
										lngTown = 0;
										lngState = 0;
										lngClerk = 0;
										lngWarrant = 0;
										lngLate = 0;
									}
								}
								lngLate = 0;
								lngWarrant = 0;
								if (lngLeft >= modDogs.Statics.DogFee.Late_Fee && lngYear == DateTime.Today.Year)
								{
									if (lngLeft >= modDogs.Statics.DogFee.Warrant_Fee && FCConvert.ToDateTime(Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE)).Month >= 2)
									{
										lngWarrant = FCConvert.ToInt32((modDogs.Statics.DogFee.Warrant_Fee));
									}
									else
									{
										lngLate = FCConvert.ToInt32((modDogs.Statics.DogFee.Late_Fee));
									}
								}
								lngLeft -= lngLate;
								lngLeft -= lngWarrant;
								if (boolProcess)
								{
									//App.DoEvents();
									rsSave.OpenRecordset("select * from dogtransactions where dognumber = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGID))) + " and dogyear = " + FCConvert.ToString(lngYear) + " and boollicense = 1", "twck0000.vb1");
									if (rsSave.EndOfFile())
									{
										if (lngLoadedTag > 0)
										{
											if (MatchTag(lngYear, lngLoadedTag))
											{
												lngNewTag = lngLoadedTag;
											}
										}
										rsDog.OpenRecordset("select * from doginfo where ID = " + Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGID), "twck0000.vb1");
										rsDog.Edit();
										rsSave.OpenRecordset("select * from transactiontable where ID = -1", "twck0000.vb1");
										rsSave.AddNew();
										
										rsSave.Set_Fields("transactiondate", Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE));
										rsSave.Set_Fields("transactiontype", modGNBas.DOGTRANSACTION);
										rsSave.Set_Fields("userid", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
										rsSave.Set_Fields("username", modGlobalConstants.Statics.clsSecurityClass.Get_UserName());
										rsSave.Set_Fields("description", "Dog License");
										rsSave.Set_Fields("towncode", 0);
										rsSave.Set_Fields("transactionperson", fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST) + " " + Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE)) + " " + Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST)));
										rsSave.Set_Fields("amount1", 0);
										rsSave.Set_Fields("amount2", lngTown);
										rsSave.Set_Fields("amount3", lngState);
										rsSave.Set_Fields("amount4", lngClerk);
										rsSave.Set_Fields("amount5", lngLate + lngWarrant);
										rsSave.Update();
                                        lngTranID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));

                                rsSave.OpenRecordset("select * from dogtransactions where ID = -1", "twck0000.vb1");
										rsSave.AddNew();
										rsSave.Set_Fields("transactionnumber", lngTranID);
										rsSave.Set_Fields("description", "Dog License");
										rsSave.Set_Fields("transactiondate", Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE));
										rsSave.Set_Fields("ownernum", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERID))));
										rsSave.Set_Fields("dognumber", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGID))));
										rsSave.Set_Fields("boollicense", true);
										rsSave.Set_Fields("amount", lngState + lngClerk + lngTown + lngLate + lngWarrant);
										rsSave.Set_Fields("statefee", lngState);
										rsSave.Set_Fields("townfee", lngTown);
										rsSave.Set_Fields("clerkfee", lngClerk);
										rsSave.Set_Fields("latefee", lngLate);
										rsSave.Set_Fields("warrantfee", lngWarrant);
										rsSave.Set_Fields("towncode", 0);
										rsSave.Set_Fields("online", true);
										rsSave.Set_Fields("dogyear", lngYear);
										rsSave.Set_Fields("spaynonspayamount", lngState + lngTown + lngClerk);
										rsSave.Set_Fields("boolspayneuter", boolNeutered);
										rsSave.Set_Fields("tagnumber", lngNewTag);
										rsSave.Set_Fields("stickernumber", FCConvert.ToString(Conversion.Val(rsDog.Get_Fields_String("stickerlicnum"))));
										rsSave.Set_Fields("rabiestagnumber", Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT));
										rsSave.Set_Fields("oldsticker", FCConvert.ToString(Conversion.Val(rsDog.Get_Fields_String("stickerlicnum"))));
										rsSave.Set_Fields("oldyear", FCConvert.ToString(Conversion.Val(rsDog.Get_Fields("year"))));
										rsSave.Set_Fields("oldtagnumber", FCConvert.ToString(Conversion.Val(rsDog.Get_Fields_String("taglicnum"))));
										rsDog.Set_Fields("Previoustag", rsDog.Get_Fields_String("taglicnum"));
										if (Information.IsDate(rsDog.Get_Fields("rabstickerissue")))
										{
											rsSave.Set_Fields("oldissuedate", rsDog.Get_Fields_DateTime("rabstickerissue"));
										}
										else
										{
											rsSave.Set_Fields("oldissuedate", 0);
										}
										rsDog.Set_Fields("year", lngYear);
										rsDog.Set_Fields("taglicnum", lngNewTag);
										Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG, FCConvert.ToString(lngNewTag));
										rsDog.Set_Fields("rabstickerissue", Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE));
										if (!(fecherFoundation.Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT)) == ""))
										{
											rsDog.Set_Fields("rabiescertnum", Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT));
										}
										if (Grid.TextMatrix(lngRow, CNSTGRIDCOLVET) != "")
										{
											rsDog.Set_Fields("dogvet", Grid.TextMatrix(lngRow, CNSTGRIDCOLVET));
										}
										if (Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTERCERT) != "")
										{
											rsDog.Set_Fields("sncertnum", Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTERCERT));
										}
										if (Information.IsDate(Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE)))
										{
											rsDog.Set_Fields("rabiesexpdate", Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE));
										}
										rsDog.Set_Fields("outsidesource", true);
										rsDog.Update();
										rsSave.Set_Fields("timestamp", DateTime.Now);
										rsSave.Update();
										Grid.RowData(lngRow, true);
										Grid.TextMatrix(lngRow, CNSTGRIDCOLREASON, "");
									}
									else
									{
										Grid.RowData(lngRow, false);
										Grid.TextMatrix(lngRow, CNSTGRIDCOLREASON, "Already Registered");
									}
								}
								else
								{
									Grid.RowData(lngRow, false);
									Grid.TextMatrix(lngRow, CNSTGRIDCOLREASON, "Error in Data");
								}
							}
							else
							{
								Grid.RowData(lngRow, false);
								Grid.TextMatrix(lngRow, CNSTGRIDCOLREASON, "Already Registered");
							}
						}
						// lngRow
						frmWait.InstancePtr.Unload();
						rptOnlineRegistration.InstancePtr.Init();
						return;
					//}
				//}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Process", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int GetTag(ref int lngYear)
		{
			int GetTag = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngReturn = 0;
			rsLoad.OpenRecordset("select * from doginventory where status = 'Active' and type = 'T' and year = [year] order by stickernumber", "twck0000.vb1");
			if (!rsLoad.EndOfFile())
			{
				lngReturn = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("stickernumber"));
				rsLoad.Edit();
				rsLoad.Set_Fields("status", "Used");
				rsLoad.Update();
			}
			else
			{
				lngReturn = 0;
			}
			GetTag = lngReturn;
			return GetTag;
		}
		// vbPorter upgrade warning: intYear As int	OnWriteFCConvert.ToInt32(
		private bool MatchTag(int intYear, int lngTag)
		{
			bool MatchTag = false;
			clsDRWrapper rsLoad = new clsDRWrapper();
			MatchTag = false;
			if (modGNBas.Statics.ClerkDefaults.OnlineDogImportOption != FCConvert.ToInt32(clsCustomize.OnlineDogImportOptionType.OnlineDogImportOptionNoTags))
			{
				rsLoad.OpenRecordset("select * from doginventory where type = 'T' and [year] = " + FCConvert.ToString(intYear) + " and stickernumber = " + FCConvert.ToString(lngTag), "twck0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					if (fecherFoundation.Strings.LCase(FCConvert.ToString(rsLoad.Get_Fields_String("status"))) == "active")
					{
						// tag exists and is available
						rsLoad.Edit();
						rsLoad.Set_Fields("status", "Used");
						rsLoad.Update();
						MatchTag = true;
					}
					else
					{
						// tag exists and is in use
					}
				}
				else
				{
					// tag doesn't exist
					if (modGNBas.Statics.ClerkDefaults.OnlineDogImportOption == FCConvert.ToInt32(clsCustomize.OnlineDogImportOptionType.OnlineDogImportOptionAddMissing))
					{
						rsLoad.AddNew();
						rsLoad.Set_Fields("status", "Used");
						rsLoad.Set_Fields("Year", intYear);
						rsLoad.Set_Fields("stickernumber", lngTag);
						rsLoad.Set_Fields("Type", "T");
						rsLoad.Set_Fields("DateReceived", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
						rsLoad.Update();
						MatchTag = true;
					}
				}
			}
			return MatchTag;
		}

		private void mnuUnmatch_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngURow;
			lngRow = Grid.Row;
			if (lngRow < 1)
				return;
			GridNew.Rows += 1;
			lngURow = GridNew.Rows - 1;
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLADDRESS1, Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLADDRESS2, Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLBREED, Grid.TextMatrix(lngRow, CNSTGRIDCOLBREED));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLCITY, Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLCOLOR, Grid.TextMatrix(lngRow, CNSTGRIDCOLCOLOR));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLDOGDOB, Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGDOB));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLDOGID, FCConvert.ToString(0));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLDOGNAME, Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLEMAIL, Grid.TextMatrix(lngRow, CNSTGRIDCOLEMAIL));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLLOCATION, Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCATION));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLNEUTER, Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTER));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLNEUTERCERT, Grid.TextMatrix(lngRow, CNSTGRIDCOLNEUTERCERT));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLOWNERFIRST, Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLOWNERID, FCConvert.ToString(0));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLOWNERLAST, Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLOWNERMIDDLE, Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLPHONE, Grid.TextMatrix(lngRow, CNSTGRIDCOLPHONE));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLRABIESCERT, Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESCERT));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLRABIESISSUEDATE, Grid.TextMatrix(lngRow, CNSTGRIDCOLRABIESISSUEDATE));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLREASON, "");
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLSEX, Grid.TextMatrix(lngRow, CNSTGRIDCOLSEX));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLSTATE, Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLTAG, Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLTRANSACTIONDATE, Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLTRANSFEE, Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSFEE));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLTRANSTYPE, Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSTYPE));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLVET, Grid.TextMatrix(lngRow, CNSTGRIDCOLVET));
			GridNew.TextMatrix(lngURow, CNSTGRIDCOLZIP, Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP));
			if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(GridNew.TextMatrix(lngURow, CNSTGRIDCOLTRANSTYPE))) != "N")
			{
				GridNew.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngURow, 0, lngURow, GridNew.Cols - 1, Shape1.BackColor);
			}
			GridNew.RowData(lngURow, false);
			Grid.RemoveItem(lngRow);
			Grid.AutoSize(0, Grid.Cols - 1);
			GridNew.AutoSize(0, GridNew.Cols - 1);
		}

		private void temp_CondensedPartiesHintsCompleted()
		{
			frmWait.InstancePtr.Unload();
		}

		private void temp_UpdateMessage(double dblProgress, string strMessage)
		{
			frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32(dblProgress);
			frmWait.InstancePtr.lblCounter.Text = strMessage;
			frmWait.InstancePtr.Refresh();
			//App.DoEvents();
		}
	}
}
