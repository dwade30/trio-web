//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmBirthDelayed.
	/// </summary>
	partial class frmBirthDelayed
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLabels;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblBirthCertNum;
		public fecherFoundation.FCCheckBox chkMarriageOnFile;
		public fecherFoundation.FCCheckBox chkDeceased;
		public fecherFoundation.FCTextBox txtAmendedInfo;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtCounty;
		public fecherFoundation.FCTextBox txtChildTown;
		public fecherFoundation.FCTextBox txtChildDOB;
		public fecherFoundation.FCTextBox txtChildSex;
		public fecherFoundation.FCTextBox txtChildFirstName;
		public fecherFoundation.FCTextBox txtChildLastName;
		public fecherFoundation.FCTextBox txtChildMI;
		public fecherFoundation.FCTextBox txtChildRace;
		public Global.T2KDateBox txtActualDate;
		public fecherFoundation.FCLabel Label70;
		public fecherFoundation.FCLabel lblLabels_5;
		public fecherFoundation.FCLabel lblLabels_15;
		public fecherFoundation.FCLabel lblLabels_16;
		public fecherFoundation.FCLabel lblLabels_17;
		public fecherFoundation.FCLabel lblLabels_25;
		public fecherFoundation.FCLabel lblLabels_8;
		public fecherFoundation.FCLabel lblLabels_35;
		public fecherFoundation.FCLabel lblLabels_36;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtMotherFirstName;
		public fecherFoundation.FCTextBox txtMotherLastName;
		public fecherFoundation.FCTextBox txtMotherMI;
		public fecherFoundation.FCTextBox txtMotherMaidenName;
		public fecherFoundation.FCTextBox txtMothersBirthPlace;
		public fecherFoundation.FCLabel lblLabels_18;
		public fecherFoundation.FCLabel lblLabels_19;
		public fecherFoundation.FCLabel lblLabels_20;
		public fecherFoundation.FCLabel lblLabels_28;
		public fecherFoundation.FCLabel lblLabels_29;
		public fecherFoundation.FCFrame Frame8;
		public fecherFoundation.FCTextBox txtFatherFirstName;
		public fecherFoundation.FCTextBox txtFatherLastName;
		public fecherFoundation.FCTextBox txtFatherMI;
		public fecherFoundation.FCTextBox txtFathersBirthPlace;
		public fecherFoundation.FCLabel lblLabels_12;
		public fecherFoundation.FCLabel lblLabels_13;
		public fecherFoundation.FCLabel lblLabels_14;
		public fecherFoundation.FCLabel lblLabels_30;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtRegistrantAddress;
		public fecherFoundation.FCLabel lblLabels_2;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCTextBox txtNotaryExpiresDate;
		public fecherFoundation.FCTextBox txtNotaryFirstName;
		public fecherFoundation.FCTextBox txtNotaryLastName;
		public fecherFoundation.FCTextBox txtNotaryMiddleName;
		public Global.T2KDateBox mebNotarySwornDate;
		public fecherFoundation.FCLabel lblLabels_1;
		public fecherFoundation.FCLabel lblLabels_0;
		public fecherFoundation.FCLabel lblLabels_21;
		public fecherFoundation.FCLabel lblLabels_22;
		public fecherFoundation.FCLabel lblLabels_23;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtClerkTown;
		public fecherFoundation.FCTextBox txtClerkMiddleName;
		public fecherFoundation.FCTextBox txtClerkLastName;
		public fecherFoundation.FCTextBox txtClerkFirstName;
		public Global.T2KDateBox mebClerkSwornDate;
		public fecherFoundation.FCLabel lblLabels_11;
		public fecherFoundation.FCLabel lblLabels_10;
		public fecherFoundation.FCLabel lblLabels_9;
		public fecherFoundation.FCLabel lblLabels_6;
		public fecherFoundation.FCLabel lblLabels_3;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtDateEntry1;
		public fecherFoundation.FCTextBox txtDateIssued1;
		public fecherFoundation.FCTextBox txtMotherFullName1;
		public fecherFoundation.FCTextBox txtFatherFullName1;
		public fecherFoundation.FCTextBox txtWhomSigned1;
		public fecherFoundation.FCTextBox txtBirthPlace1;
		public fecherFoundation.FCTextBox txtDocumentType1;
		public Global.T2KDateBox mebDOB1;
		public fecherFoundation.FCLabel lblLabels_38;
		public fecherFoundation.FCLabel lblLabels_37;
		public fecherFoundation.FCLabel lblLabels_34;
		public fecherFoundation.FCLabel lblLabels_33;
		public fecherFoundation.FCLabel lblLabels_32;
		public fecherFoundation.FCLabel lblLabels_31;
		public fecherFoundation.FCLabel lblLabels_26;
		public fecherFoundation.FCLabel lblLabels_4;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCTextBox txtDateEntry2;
		public fecherFoundation.FCTextBox txtDateIssued2;
		public fecherFoundation.FCTextBox txtDocumentType2;
		public fecherFoundation.FCTextBox txtBirthPlace2;
		public fecherFoundation.FCTextBox txtWhomSigned2;
		public fecherFoundation.FCTextBox txtFatherFullName2;
		public fecherFoundation.FCTextBox txtMotherFullName2;
		public Global.T2KDateBox mebDOB2;
		public fecherFoundation.FCLabel lblLabels_47;
		public fecherFoundation.FCLabel lblLabels_46;
		public fecherFoundation.FCLabel lblLabels_45;
		public fecherFoundation.FCLabel lblLabels_44;
		public fecherFoundation.FCLabel lblLabels_43;
		public fecherFoundation.FCLabel lblLabels_41;
		public fecherFoundation.FCLabel lblLabels_40;
		public fecherFoundation.FCLabel lblLabels_39;
		public fecherFoundation.FCFrame Frame10;
		public fecherFoundation.FCTextBox txtDateEntry3;
		public fecherFoundation.FCTextBox txtDateIssued3;
		public fecherFoundation.FCTextBox txtDocumentType3;
		public fecherFoundation.FCTextBox txtBirthPlace3;
		public fecherFoundation.FCTextBox txtWhomSigned3;
		public fecherFoundation.FCTextBox txtFatherFullName3;
		public fecherFoundation.FCTextBox txtMotherFullName3;
		public Global.T2KDateBox mebDOB3;
		public fecherFoundation.FCLabel lblLabels_55;
		public fecherFoundation.FCLabel lblLabels_54;
		public fecherFoundation.FCLabel lblLabels_53;
		public fecherFoundation.FCLabel lblLabels_52;
		public fecherFoundation.FCLabel lblLabels_51;
		public fecherFoundation.FCLabel lblLabels_50;
		public fecherFoundation.FCLabel lblLabels_49;
		public fecherFoundation.FCLabel lblLabels_48;
		public fecherFoundation.FCTabPage SSTab1_Page5;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtDateOfFiling;
		public fecherFoundation.FCTextBox txtRegistrarReviewed;
		public fecherFoundation.FCTextBox txtRegistrarName;
		public fecherFoundation.FCLabel lblLabels_27;
		public fecherFoundation.FCLabel lblLabels_24;
		public fecherFoundation.FCLabel lblLabels_7;
		public fecherFoundation.FCCheckBox chkRequired;
		public fecherFoundation.FCTextBox txtFileNumber;
		public fecherFoundation.FCCheckBox chkIllegitBirth;
		public FCCommonDialog cdlColor;
		public fecherFoundation.FCTextBox txtBirthCertNum;
		public Global.T2KDateBox mebDateDeceased;
		public FCGrid gridTownCode;
		public fecherFoundation.FCLabel lblBKey;
		public fecherFoundation.FCLabel lblBirthID;
		public fecherFoundation.FCLabel lblRecord;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessAddNewRecord;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDeleteRecord;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuSP22;
		public fecherFoundation.FCToolStripMenuItem mnuSetDefaultTowns;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		public fecherFoundation.FCLabel lblDeceased;
		public fecherFoundation.FCLabel lblLabels_42;
		public fecherFoundation.FCLabel lblBirthCertNum_28;
		public fecherFoundation.FCLabel lblFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.chkMarriageOnFile = new fecherFoundation.FCCheckBox();
			this.chkDeceased = new fecherFoundation.FCCheckBox();
			this.txtAmendedInfo = new fecherFoundation.FCTextBox();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtCounty = new fecherFoundation.FCTextBox();
			this.txtChildTown = new fecherFoundation.FCTextBox();
			this.txtChildDOB = new fecherFoundation.FCTextBox();
			this.txtChildSex = new fecherFoundation.FCTextBox();
			this.txtChildLastName = new fecherFoundation.FCTextBox();
			this.txtChildFirstName = new fecherFoundation.FCTextBox();
			this.txtChildMI = new fecherFoundation.FCTextBox();
			this.txtChildRace = new fecherFoundation.FCTextBox();
			this.txtActualDate = new Global.T2KDateBox();
			this.Label70 = new fecherFoundation.FCLabel();
			this.lblLabels_5 = new fecherFoundation.FCLabel();
			this.lblLabels_15 = new fecherFoundation.FCLabel();
			this.lblLabels_16 = new fecherFoundation.FCLabel();
			this.lblLabels_17 = new fecherFoundation.FCLabel();
			this.lblLabels_25 = new fecherFoundation.FCLabel();
			this.lblLabels_8 = new fecherFoundation.FCLabel();
			this.lblLabels_35 = new fecherFoundation.FCLabel();
			this.lblLabels_36 = new fecherFoundation.FCLabel();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.Frame7 = new fecherFoundation.FCFrame();
			this.txtMotherFirstName = new fecherFoundation.FCTextBox();
			this.txtMotherLastName = new fecherFoundation.FCTextBox();
			this.txtMotherMI = new fecherFoundation.FCTextBox();
			this.txtMotherMaidenName = new fecherFoundation.FCTextBox();
			this.txtMothersBirthPlace = new fecherFoundation.FCTextBox();
			this.lblLabels_18 = new fecherFoundation.FCLabel();
			this.lblLabels_19 = new fecherFoundation.FCLabel();
			this.lblLabels_20 = new fecherFoundation.FCLabel();
			this.lblLabels_28 = new fecherFoundation.FCLabel();
			this.lblLabels_29 = new fecherFoundation.FCLabel();
			this.Frame8 = new fecherFoundation.FCFrame();
			this.txtFatherFirstName = new fecherFoundation.FCTextBox();
			this.txtFatherLastName = new fecherFoundation.FCTextBox();
			this.txtFatherMI = new fecherFoundation.FCTextBox();
			this.txtFathersBirthPlace = new fecherFoundation.FCTextBox();
			this.lblLabels_12 = new fecherFoundation.FCLabel();
			this.lblLabels_13 = new fecherFoundation.FCLabel();
			this.lblLabels_14 = new fecherFoundation.FCLabel();
			this.lblLabels_30 = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtRegistrantAddress = new fecherFoundation.FCTextBox();
			this.lblLabels_2 = new fecherFoundation.FCLabel();
			this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
			this.Frame9 = new fecherFoundation.FCFrame();
			this.txtNotaryExpiresDate = new fecherFoundation.FCTextBox();
			this.txtNotaryFirstName = new fecherFoundation.FCTextBox();
			this.txtNotaryLastName = new fecherFoundation.FCTextBox();
			this.txtNotaryMiddleName = new fecherFoundation.FCTextBox();
			this.mebNotarySwornDate = new Global.T2KDateBox();
			this.lblLabels_1 = new fecherFoundation.FCLabel();
			this.lblLabels_0 = new fecherFoundation.FCLabel();
			this.lblLabels_21 = new fecherFoundation.FCLabel();
			this.lblLabels_22 = new fecherFoundation.FCLabel();
			this.lblLabels_23 = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtClerkTown = new fecherFoundation.FCTextBox();
			this.txtClerkMiddleName = new fecherFoundation.FCTextBox();
			this.txtClerkLastName = new fecherFoundation.FCTextBox();
			this.txtClerkFirstName = new fecherFoundation.FCTextBox();
			this.mebClerkSwornDate = new Global.T2KDateBox();
			this.lblLabels_11 = new fecherFoundation.FCLabel();
			this.lblLabels_10 = new fecherFoundation.FCLabel();
			this.lblLabels_9 = new fecherFoundation.FCLabel();
			this.lblLabels_6 = new fecherFoundation.FCLabel();
			this.lblLabels_3 = new fecherFoundation.FCLabel();
			this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.txtDateEntry1 = new fecherFoundation.FCTextBox();
			this.txtDateIssued1 = new fecherFoundation.FCTextBox();
			this.txtMotherFullName1 = new fecherFoundation.FCTextBox();
			this.txtFatherFullName1 = new fecherFoundation.FCTextBox();
			this.txtWhomSigned1 = new fecherFoundation.FCTextBox();
			this.txtBirthPlace1 = new fecherFoundation.FCTextBox();
			this.txtDocumentType1 = new fecherFoundation.FCTextBox();
			this.mebDOB1 = new Global.T2KDateBox();
			this.lblLabels_38 = new fecherFoundation.FCLabel();
			this.lblLabels_37 = new fecherFoundation.FCLabel();
			this.lblLabels_34 = new fecherFoundation.FCLabel();
			this.lblLabels_33 = new fecherFoundation.FCLabel();
			this.lblLabels_32 = new fecherFoundation.FCLabel();
			this.lblLabels_31 = new fecherFoundation.FCLabel();
			this.lblLabels_26 = new fecherFoundation.FCLabel();
			this.lblLabels_4 = new fecherFoundation.FCLabel();
			this.Frame6 = new fecherFoundation.FCFrame();
			this.txtDateEntry2 = new fecherFoundation.FCTextBox();
			this.txtDateIssued2 = new fecherFoundation.FCTextBox();
			this.txtDocumentType2 = new fecherFoundation.FCTextBox();
			this.txtBirthPlace2 = new fecherFoundation.FCTextBox();
			this.txtWhomSigned2 = new fecherFoundation.FCTextBox();
			this.txtFatherFullName2 = new fecherFoundation.FCTextBox();
			this.txtMotherFullName2 = new fecherFoundation.FCTextBox();
			this.mebDOB2 = new Global.T2KDateBox();
			this.lblLabels_47 = new fecherFoundation.FCLabel();
			this.lblLabels_46 = new fecherFoundation.FCLabel();
			this.lblLabels_45 = new fecherFoundation.FCLabel();
			this.lblLabels_44 = new fecherFoundation.FCLabel();
			this.lblLabels_43 = new fecherFoundation.FCLabel();
			this.lblLabels_41 = new fecherFoundation.FCLabel();
			this.lblLabels_40 = new fecherFoundation.FCLabel();
			this.lblLabels_39 = new fecherFoundation.FCLabel();
			this.Frame10 = new fecherFoundation.FCFrame();
			this.txtDateEntry3 = new fecherFoundation.FCTextBox();
			this.txtDateIssued3 = new fecherFoundation.FCTextBox();
			this.txtDocumentType3 = new fecherFoundation.FCTextBox();
			this.txtBirthPlace3 = new fecherFoundation.FCTextBox();
			this.txtWhomSigned3 = new fecherFoundation.FCTextBox();
			this.txtFatherFullName3 = new fecherFoundation.FCTextBox();
			this.txtMotherFullName3 = new fecherFoundation.FCTextBox();
			this.mebDOB3 = new Global.T2KDateBox();
			this.lblLabels_55 = new fecherFoundation.FCLabel();
			this.lblLabels_54 = new fecherFoundation.FCLabel();
			this.lblLabels_53 = new fecherFoundation.FCLabel();
			this.lblLabels_52 = new fecherFoundation.FCLabel();
			this.lblLabels_51 = new fecherFoundation.FCLabel();
			this.lblLabels_50 = new fecherFoundation.FCLabel();
			this.lblLabels_49 = new fecherFoundation.FCLabel();
			this.lblLabels_48 = new fecherFoundation.FCLabel();
			this.SSTab1_Page5 = new fecherFoundation.FCTabPage();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.txtDateOfFiling = new fecherFoundation.FCTextBox();
			this.txtRegistrarReviewed = new fecherFoundation.FCTextBox();
			this.txtRegistrarName = new fecherFoundation.FCTextBox();
			this.lblLabels_27 = new fecherFoundation.FCLabel();
			this.lblLabels_24 = new fecherFoundation.FCLabel();
			this.lblLabels_7 = new fecherFoundation.FCLabel();
			this.chkRequired = new fecherFoundation.FCCheckBox();
			this.txtFileNumber = new fecherFoundation.FCTextBox();
			this.chkIllegitBirth = new fecherFoundation.FCCheckBox();
			this.cdlColor = new fecherFoundation.FCCommonDialog();
			this.txtBirthCertNum = new fecherFoundation.FCTextBox();
			this.mebDateDeceased = new Global.T2KDateBox();
			this.gridTownCode = new fecherFoundation.FCGrid();
			this.lblBKey = new fecherFoundation.FCLabel();
			this.lblBirthID = new fecherFoundation.FCLabel();
			this.lblRecord = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessAddNewRecord = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessDeleteRecord = new fecherFoundation.FCToolStripMenuItem();
			this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP22 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSetDefaultTowns = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.lblDeceased = new fecherFoundation.FCLabel();
			this.lblLabels_42 = new fecherFoundation.FCLabel();
			this.lblBirthCertNum_28 = new fecherFoundation.FCLabel();
			this.lblFile = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.cmdComment = new fecherFoundation.FCButton();
			this.cmdProcessDeleteRecord = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).BeginInit();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).BeginInit();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
			this.Frame7.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame8)).BeginInit();
			this.Frame8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			this.SSTab1_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
			this.Frame9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebNotarySwornDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebClerkSwornDate)).BeginInit();
			this.SSTab1_Page4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
			this.Frame6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame10)).BeginInit();
			this.Frame10.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebDOB3)).BeginInit();
			this.SSTab1_Page5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkRequired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIllegitBirth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mebDateDeceased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessDeleteRecord)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 596);
			this.BottomPanel.Size = new System.Drawing.Size(1088, 104);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkMarriageOnFile);
			this.ClientArea.Controls.Add(this.chkDeceased);
			this.ClientArea.Controls.Add(this.txtAmendedInfo);
			this.ClientArea.Controls.Add(this.SSTab1);
			this.ClientArea.Controls.Add(this.chkRequired);
			this.ClientArea.Controls.Add(this.txtFileNumber);
			this.ClientArea.Controls.Add(this.chkIllegitBirth);
			this.ClientArea.Controls.Add(this.txtBirthCertNum);
			this.ClientArea.Controls.Add(this.mebDateDeceased);
			this.ClientArea.Controls.Add(this.gridTownCode);
			this.ClientArea.Controls.Add(this.lblBKey);
			this.ClientArea.Controls.Add(this.lblBirthID);
			this.ClientArea.Controls.Add(this.lblRecord);
			this.ClientArea.Controls.Add(this.lblDeceased);
			this.ClientArea.Controls.Add(this.lblLabels_42);
			this.ClientArea.Controls.Add(this.lblBirthCertNum_28);
			this.ClientArea.Controls.Add(this.lblFile);
			this.ClientArea.Size = new System.Drawing.Size(1088, 536);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdProcessDeleteRecord);
			this.TopPanel.Controls.Add(this.cmdComment);
			this.TopPanel.Size = new System.Drawing.Size(1088, 60);
			this.TopPanel.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessDeleteRecord, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(172, 30);
			this.HeaderText.Text = "Delayed Births ";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// chkMarriageOnFile
			// 
			this.chkMarriageOnFile.Location = new System.Drawing.Point(330, 141);
			this.chkMarriageOnFile.Name = "chkMarriageOnFile";
			this.chkMarriageOnFile.Size = new System.Drawing.Size(191, 27);
			this.chkMarriageOnFile.TabIndex = 6;
			this.chkMarriageOnFile.Text = "Marriage record on file";
			this.ToolTip1.SetToolTip(this.chkMarriageOnFile, null);
			// 
			// chkDeceased
			// 
			this.chkDeceased.Location = new System.Drawing.Point(330, 104);
			this.chkDeceased.Name = "chkDeceased";
			this.chkDeceased.Size = new System.Drawing.Size(264, 27);
			this.chkDeceased.TabIndex = 3;
			this.chkDeceased.Text = "Death record is filed in this office";
			this.ToolTip1.SetToolTip(this.chkDeceased, null);
			this.chkDeceased.CheckedChanged += new System.EventHandler(this.chkDeceased_CheckedChanged);
			// 
			// txtAmendedInfo
			// 
			this.txtAmendedInfo.BackColor = System.Drawing.SystemColors.Window;
			this.txtAmendedInfo.Location = new System.Drawing.Point(30, 211);
			this.txtAmendedInfo.Multiline = true;
			this.txtAmendedInfo.Name = "txtAmendedInfo";
			this.txtAmendedInfo.ScrollBars = Wisej.Web.ScrollBars.Vertical;
			this.txtAmendedInfo.Size = new System.Drawing.Size(1007, 65);
			this.txtAmendedInfo.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtAmendedInfo, null);
			// 
			// SSTab1
			// 
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Controls.Add(this.SSTab1_Page3);
			this.SSTab1.Controls.Add(this.SSTab1_Page4);
			this.SSTab1.Controls.Add(this.SSTab1_Page5);
			this.SSTab1.Location = new System.Drawing.Point(30, 303);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
			this.SSTab1.Size = new System.Drawing.Size(1008, 675);
			this.SSTab1.TabIndex = 15;
            this.SSTab1.TabStop = false;
			this.ToolTip1.SetToolTip(this.SSTab1, null);
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.Controls.Add(this.Frame1);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Text = "Child Information";
			this.ToolTip1.SetToolTip(this.SSTab1_Page1, null);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtCounty);
			this.Frame1.Controls.Add(this.txtChildTown);
			this.Frame1.Controls.Add(this.txtChildDOB);
			this.Frame1.Controls.Add(this.txtChildSex);
			this.Frame1.Controls.Add(this.txtChildLastName);
			this.Frame1.Controls.Add(this.txtChildFirstName);
			this.Frame1.Controls.Add(this.txtChildMI);
			this.Frame1.Controls.Add(this.txtChildRace);
			this.Frame1.Controls.Add(this.txtActualDate);
			this.Frame1.Controls.Add(this.Label70);
			this.Frame1.Controls.Add(this.lblLabels_5);
			this.Frame1.Controls.Add(this.lblLabels_15);
			this.Frame1.Controls.Add(this.lblLabels_16);
			this.Frame1.Controls.Add(this.lblLabels_17);
			this.Frame1.Controls.Add(this.lblLabels_25);
			this.Frame1.Controls.Add(this.lblLabels_8);
			this.Frame1.Controls.Add(this.lblLabels_35);
			this.Frame1.Controls.Add(this.lblLabels_36);
			this.Frame1.Location = new System.Drawing.Point(20, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(966, 180);
			this.Frame1.Text = "Child Information";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// txtCounty
			// 
			this.txtCounty.BackColor = System.Drawing.SystemColors.Window;
			this.txtCounty.Location = new System.Drawing.Point(618, 120);
			this.txtCounty.MaxLength = 20;
			this.txtCounty.Name = "txtCounty";
			this.txtCounty.Size = new System.Drawing.Size(328, 40);
			this.txtCounty.TabIndex = 17;
			this.txtCounty.Text = " ";
			this.ToolTip1.SetToolTip(this.txtCounty, null);
			this.txtCounty.DoubleClick += new System.EventHandler(this.txtCounty_DoubleClick);
			// 
			// txtChildTown
			// 
			this.txtChildTown.BackColor = System.Drawing.SystemColors.Window;
			this.txtChildTown.Location = new System.Drawing.Point(270, 120);
			this.txtChildTown.MaxLength = 20;
			this.txtChildTown.Name = "txtChildTown";
			this.txtChildTown.Size = new System.Drawing.Size(328, 40);
			this.txtChildTown.TabIndex = 15;
			this.txtChildTown.Text = " ";
			this.ToolTip1.SetToolTip(this.txtChildTown, null);
			this.txtChildTown.Enter += new System.EventHandler(this.txtChildTown_Enter);
			this.txtChildTown.DoubleClick += new System.EventHandler(this.txtChildTown_DoubleClick);
			// 
			// txtChildDOB
			// 
			this.txtChildDOB.Location = new System.Drawing.Point(652, 50);
			this.txtChildDOB.Name = "txtChildDOB";
			this.txtChildDOB.Size = new System.Drawing.Size(161, 40);
			this.txtChildDOB.TabIndex = 7;
			this.txtChildDOB.Tag = "Required";
			this.ToolTip1.SetToolTip(this.txtChildDOB, null);
			this.txtChildDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtChildDOB_Validating);
			// 
			// txtChildSex
			// 
			this.txtChildSex.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtChildSex.Location = new System.Drawing.Point(190, 120);
			this.txtChildSex.MaxLength = 1;
			this.txtChildSex.Name = "txtChildSex";
			this.txtChildSex.Size = new System.Drawing.Size(60, 40);
			this.txtChildSex.TabIndex = 13;
			this.txtChildSex.Tag = "Required";
			this.ToolTip1.SetToolTip(this.txtChildSex, null);
			this.txtChildSex.Validating += new System.ComponentModel.CancelEventHandler(this.txtChildSex_Validating);
			// 
			// txtChildLastName
			// 
			this.txtChildLastName.Location = new System.Drawing.Point(416, 50);
			this.txtChildLastName.Name = "txtChildLastName";
			this.txtChildLastName.Size = new System.Drawing.Size(216, 40);
			this.txtChildLastName.TabIndex = 5;
			this.txtChildLastName.Tag = "Required";
			this.ToolTip1.SetToolTip(this.txtChildLastName, null);
			// 
			// txtChildFirstName
			// 
			this.txtChildFirstName.Location = new System.Drawing.Point(20, 50);
			this.txtChildFirstName.Name = "txtChildFirstName";
			this.txtChildFirstName.Size = new System.Drawing.Size(216, 40);
			this.txtChildFirstName.TabIndex = 1;
			this.txtChildFirstName.Tag = "Required";
			this.ToolTip1.SetToolTip(this.txtChildFirstName, null);
			// 
			// txtChildMI
			// 
			this.txtChildMI.BackColor = System.Drawing.SystemColors.Window;
			this.txtChildMI.Location = new System.Drawing.Point(256, 50);
			this.txtChildMI.MaxLength = 20;
			this.txtChildMI.Name = "txtChildMI";
			this.txtChildMI.Size = new System.Drawing.Size(140, 40);
			this.txtChildMI.TabIndex = 3;
			this.txtChildMI.Text = " ";
			this.ToolTip1.SetToolTip(this.txtChildMI, null);
			// 
			// txtChildRace
			// 
			this.txtChildRace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtChildRace.Location = new System.Drawing.Point(20, 120);
			this.txtChildRace.Name = "txtChildRace";
			this.txtChildRace.Size = new System.Drawing.Size(150, 40);
			this.txtChildRace.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtChildRace, null);
			// 
			// txtActualDate
			// 
			this.txtActualDate.Location = new System.Drawing.Point(833, 50);
			this.txtActualDate.MaxLength = 10;
			this.txtActualDate.Name = "txtActualDate";
			this.txtActualDate.Size = new System.Drawing.Size(115, 40);
			this.txtActualDate.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtActualDate, null);
			// 
			// Label70
			// 
			this.Label70.Location = new System.Drawing.Point(833, 30);
			this.Label70.Name = "Label70";
			this.Label70.Size = new System.Drawing.Size(103, 16);
			this.Label70.TabIndex = 8;
			this.Label70.Text = "ACTUAL DOB";
			this.ToolTip1.SetToolTip(this.Label70, null);
			// 
			// lblLabels_5
			// 
			this.lblLabels_5.AutoSize = true;
			this.lblLabels_5.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_5.Location = new System.Drawing.Point(190, 100);
			this.lblLabels_5.Name = "lblLabels_5";
			this.lblLabels_5.Size = new System.Drawing.Size(32, 15);
			this.lblLabels_5.TabIndex = 12;
			this.lblLabels_5.Text = "SEX";
			this.ToolTip1.SetToolTip(this.lblLabels_5, null);
			// 
			// lblLabels_15
			// 
			this.lblLabels_15.AutoSize = true;
			this.lblLabels_15.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_15.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_15.Name = "lblLabels_15";
			this.lblLabels_15.Size = new System.Drawing.Size(82, 15);
			this.lblLabels_15.TabIndex = 18;
			this.lblLabels_15.Text = "FIRST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_15, null);
			// 
			// lblLabels_16
			// 
			this.lblLabels_16.AutoSize = true;
			this.lblLabels_16.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_16.Location = new System.Drawing.Point(416, 30);
			this.lblLabels_16.Name = "lblLabels_16";
			this.lblLabels_16.Size = new System.Drawing.Size(77, 15);
			this.lblLabels_16.TabIndex = 4;
			this.lblLabels_16.Text = "LAST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_16, null);
			// 
			// lblLabels_17
			// 
			this.lblLabels_17.AutoSize = true;
			this.lblLabels_17.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_17.Location = new System.Drawing.Point(256, 30);
			this.lblLabels_17.Name = "lblLabels_17";
			this.lblLabels_17.Size = new System.Drawing.Size(93, 15);
			this.lblLabels_17.TabIndex = 2;
			this.lblLabels_17.Text = "MIDDLE NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_17, null);
			// 
			// lblLabels_25
			// 
			this.lblLabels_25.AutoSize = true;
			this.lblLabels_25.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_25.Location = new System.Drawing.Point(20, 100);
			this.lblLabels_25.Name = "lblLabels_25";
			this.lblLabels_25.Size = new System.Drawing.Size(42, 15);
			this.lblLabels_25.TabIndex = 10;
			this.lblLabels_25.Text = "RACE";
			this.ToolTip1.SetToolTip(this.lblLabels_25, null);
			// 
			// lblLabels_8
			// 
			this.lblLabels_8.AutoSize = true;
			this.lblLabels_8.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_8.Location = new System.Drawing.Point(652, 30);
			this.lblLabels_8.Name = "lblLabels_8";
			this.lblLabels_8.Size = new System.Drawing.Size(140, 15);
			this.lblLabels_8.TabIndex = 6;
			this.lblLabels_8.Text = "DOB ON CERTIFICATE";
			this.ToolTip1.SetToolTip(this.lblLabels_8, null);
			// 
			// lblLabels_35
			// 
			this.lblLabels_35.AutoSize = true;
			this.lblLabels_35.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_35.Location = new System.Drawing.Point(618, 100);
			this.lblLabels_35.Name = "lblLabels_35";
			this.lblLabels_35.Size = new System.Drawing.Size(120, 15);
			this.lblLabels_35.TabIndex = 16;
			this.lblLabels_35.Text = "COUNTY OF BIRTH";
			this.ToolTip1.SetToolTip(this.lblLabels_35, null);
			// 
			// lblLabels_36
			// 
			this.lblLabels_36.AutoSize = true;
			this.lblLabels_36.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_36.Location = new System.Drawing.Point(270, 100);
			this.lblLabels_36.Name = "lblLabels_36";
			this.lblLabels_36.Size = new System.Drawing.Size(137, 15);
			this.lblLabels_36.TabIndex = 14;
			this.lblLabels_36.Text = "STATE OR PROVINCE";
			this.ToolTip1.SetToolTip(this.lblLabels_36, null);
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.Controls.Add(this.Frame7);
			this.SSTab1_Page2.Controls.Add(this.Frame8);
			this.SSTab1_Page2.Controls.Add(this.Frame2);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Text = "Parent Information";
			this.ToolTip1.SetToolTip(this.SSTab1_Page2, null);
			// 
			// Frame7
			// 
			this.Frame7.Controls.Add(this.txtMotherFirstName);
			this.Frame7.Controls.Add(this.txtMotherLastName);
			this.Frame7.Controls.Add(this.txtMotherMI);
			this.Frame7.Controls.Add(this.txtMotherMaidenName);
			this.Frame7.Controls.Add(this.txtMothersBirthPlace);
			this.Frame7.Controls.Add(this.lblLabels_18);
			this.Frame7.Controls.Add(this.lblLabels_19);
			this.Frame7.Controls.Add(this.lblLabels_20);
			this.Frame7.Controls.Add(this.lblLabels_28);
			this.Frame7.Controls.Add(this.lblLabels_29);
			this.Frame7.Location = new System.Drawing.Point(20, 170);
			this.Frame7.Name = "Frame7";
			this.Frame7.Size = new System.Drawing.Size(966, 110);
			this.Frame7.TabIndex = 1;
			this.Frame7.Text = "Mother\'s Information";
			this.ToolTip1.SetToolTip(this.Frame7, null);
			// 
			// txtMotherFirstName
			// 
			this.txtMotherFirstName.Location = new System.Drawing.Point(20, 50);
			this.txtMotherFirstName.Name = "txtMotherFirstName";
			this.txtMotherFirstName.Size = new System.Drawing.Size(168, 40);
			this.txtMotherFirstName.TabIndex = 1;
			this.txtMotherFirstName.Tag = "Required";
			this.ToolTip1.SetToolTip(this.txtMotherFirstName, null);
			this.txtMotherFirstName.Enter += new System.EventHandler(this.txtMotherFirstName_Enter);
			// 
			// txtMotherLastName
			// 
			this.txtMotherLastName.Location = new System.Drawing.Point(322, 50);
			this.txtMotherLastName.Name = "txtMotherLastName";
			this.txtMotherLastName.Size = new System.Drawing.Size(168, 40);
			this.txtMotherLastName.TabIndex = 5;
			this.txtMotherLastName.Tag = "Required";
			this.ToolTip1.SetToolTip(this.txtMotherLastName, null);
			// 
			// txtMotherMI
			// 
			this.txtMotherMI.BackColor = System.Drawing.SystemColors.Window;
			this.txtMotherMI.Location = new System.Drawing.Point(208, 50);
			this.txtMotherMI.MaxLength = 40;
			this.txtMotherMI.Name = "txtMotherMI";
			this.txtMotherMI.Size = new System.Drawing.Size(94, 40);
			this.txtMotherMI.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtMotherMI, null);
			// 
			// txtMotherMaidenName
			// 
			this.txtMotherMaidenName.Location = new System.Drawing.Point(510, 50);
			this.txtMotherMaidenName.Name = "txtMotherMaidenName";
			this.txtMotherMaidenName.Size = new System.Drawing.Size(168, 40);
			this.txtMotherMaidenName.TabIndex = 7;
			this.txtMotherMaidenName.Tag = "Required";
			this.ToolTip1.SetToolTip(this.txtMotherMaidenName, null);
			// 
			// txtMothersBirthPlace
			// 
			this.txtMothersBirthPlace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMothersBirthPlace.Location = new System.Drawing.Point(698, 50);
			this.txtMothersBirthPlace.Name = "txtMothersBirthPlace";
			this.txtMothersBirthPlace.Size = new System.Drawing.Size(248, 40);
			this.txtMothersBirthPlace.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtMothersBirthPlace, null);
			this.txtMothersBirthPlace.DoubleClick += new System.EventHandler(this.txtMothersBirthPlace_DoubleClick);
			// 
			// lblLabels_18
			// 
			this.lblLabels_18.AutoSize = true;
			this.lblLabels_18.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_18.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_18.Name = "lblLabels_18";
			this.lblLabels_18.Size = new System.Drawing.Size(82, 15);
			this.lblLabels_18.TabIndex = 10;
			this.lblLabels_18.Text = "FIRST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_18, null);
			// 
			// lblLabels_19
			// 
			this.lblLabels_19.AutoSize = true;
			this.lblLabels_19.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_19.Location = new System.Drawing.Point(322, 30);
			this.lblLabels_19.Name = "lblLabels_19";
			this.lblLabels_19.Size = new System.Drawing.Size(77, 15);
			this.lblLabels_19.TabIndex = 4;
			this.lblLabels_19.Text = "LAST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_19, null);
			// 
			// lblLabels_20
			// 
			this.lblLabels_20.AutoSize = true;
			this.lblLabels_20.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_20.Location = new System.Drawing.Point(208, 30);
			this.lblLabels_20.Name = "lblLabels_20";
			this.lblLabels_20.Size = new System.Drawing.Size(54, 15);
			this.lblLabels_20.TabIndex = 2;
			this.lblLabels_20.Text = "MIDDLE";
			this.ToolTip1.SetToolTip(this.lblLabels_20, null);
			// 
			// lblLabels_28
			// 
			this.lblLabels_28.AutoSize = true;
			this.lblLabels_28.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_28.Location = new System.Drawing.Point(510, 30);
			this.lblLabels_28.Name = "lblLabels_28";
			this.lblLabels_28.Size = new System.Drawing.Size(95, 15);
			this.lblLabels_28.TabIndex = 6;
			this.lblLabels_28.Text = "MAIDEN NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_28, null);
			// 
			// lblLabels_29
			// 
			this.lblLabels_29.AutoSize = true;
			this.lblLabels_29.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_29.Location = new System.Drawing.Point(698, 30);
			this.lblLabels_29.Name = "lblLabels_29";
			this.lblLabels_29.Size = new System.Drawing.Size(85, 15);
			this.lblLabels_29.TabIndex = 8;
			this.lblLabels_29.Text = "BIRTHPLACE";
			this.ToolTip1.SetToolTip(this.lblLabels_29, null);
			// 
			// Frame8
			// 
			this.Frame8.Controls.Add(this.txtFatherFirstName);
			this.Frame8.Controls.Add(this.txtFatherLastName);
			this.Frame8.Controls.Add(this.txtFatherMI);
			this.Frame8.Controls.Add(this.txtFathersBirthPlace);
			this.Frame8.Controls.Add(this.lblLabels_12);
			this.Frame8.Controls.Add(this.lblLabels_13);
			this.Frame8.Controls.Add(this.lblLabels_14);
			this.Frame8.Controls.Add(this.lblLabels_30);
			this.Frame8.Location = new System.Drawing.Point(20, 30);
			this.Frame8.Name = "Frame8";
			this.Frame8.Size = new System.Drawing.Size(966, 120);
			this.Frame8.TabIndex = 0;
			this.Frame8.Text = "Father\'s Information";
			this.ToolTip1.SetToolTip(this.Frame8, null);
			// 
			// txtFatherFirstName
			// 
			this.txtFatherFirstName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFatherFirstName.Location = new System.Drawing.Point(20, 60);
			this.txtFatherFirstName.Name = "txtFatherFirstName";
			this.txtFatherFirstName.Size = new System.Drawing.Size(248, 40);
			this.txtFatherFirstName.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtFatherFirstName, null);
			this.txtFatherFirstName.Enter += new System.EventHandler(this.txtFatherFirstName_Enter);
			// 
			// txtFatherLastName
			// 
			this.txtFatherLastName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFatherLastName.Location = new System.Drawing.Point(430, 60);
			this.txtFatherLastName.Name = "txtFatherLastName";
			this.txtFatherLastName.Size = new System.Drawing.Size(248, 40);
			this.txtFatherLastName.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtFatherLastName, null);
			// 
			// txtFatherMI
			// 
			this.txtFatherMI.BackColor = System.Drawing.SystemColors.Window;
			this.txtFatherMI.Location = new System.Drawing.Point(288, 60);
			this.txtFatherMI.MaxLength = 40;
			this.txtFatherMI.Name = "txtFatherMI";
			this.txtFatherMI.Size = new System.Drawing.Size(122, 40);
			this.txtFatherMI.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtFatherMI, null);
			// 
			// txtFathersBirthPlace
			// 
			this.txtFathersBirthPlace.BackColor = System.Drawing.SystemColors.Window;
			this.txtFathersBirthPlace.Location = new System.Drawing.Point(698, 60);
			this.txtFathersBirthPlace.Name = "txtFathersBirthPlace";
			this.txtFathersBirthPlace.Size = new System.Drawing.Size(248, 40);
			this.txtFathersBirthPlace.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtFathersBirthPlace, null);
			this.txtFathersBirthPlace.DoubleClick += new System.EventHandler(this.txtFathersBirthPlace_DoubleClick);
			// 
			// lblLabels_12
			// 
			this.lblLabels_12.AutoSize = true;
			this.lblLabels_12.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_12.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_12.Name = "lblLabels_12";
			this.lblLabels_12.Size = new System.Drawing.Size(82, 15);
			this.lblLabels_12.TabIndex = 8;
			this.lblLabels_12.Text = "FIRST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_12, null);
			// 
			// lblLabels_13
			// 
			this.lblLabels_13.AutoSize = true;
			this.lblLabels_13.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_13.Location = new System.Drawing.Point(430, 30);
			this.lblLabels_13.Name = "lblLabels_13";
			this.lblLabels_13.Size = new System.Drawing.Size(77, 15);
			this.lblLabels_13.TabIndex = 4;
			this.lblLabels_13.Text = "LAST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_13, null);
			// 
			// lblLabels_14
			// 
			this.lblLabels_14.AutoSize = true;
			this.lblLabels_14.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_14.Location = new System.Drawing.Point(288, 30);
			this.lblLabels_14.Name = "lblLabels_14";
			this.lblLabels_14.Size = new System.Drawing.Size(54, 15);
			this.lblLabels_14.TabIndex = 2;
			this.lblLabels_14.Text = "MIDDLE";
			this.ToolTip1.SetToolTip(this.lblLabels_14, null);
			// 
			// lblLabels_30
			// 
			this.lblLabels_30.AutoSize = true;
			this.lblLabels_30.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_30.Location = new System.Drawing.Point(698, 30);
			this.lblLabels_30.Name = "lblLabels_30";
			this.lblLabels_30.Size = new System.Drawing.Size(85, 15);
			this.lblLabels_30.TabIndex = 6;
			this.lblLabels_30.Text = "BIRTHPLACE";
			this.ToolTip1.SetToolTip(this.lblLabels_30, null);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.txtRegistrantAddress);
			this.Frame2.Controls.Add(this.lblLabels_2);
			this.Frame2.Location = new System.Drawing.Point(20, 300);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(966, 150);
			this.Frame2.TabIndex = 2;
			this.Frame2.Text = "Affidavit";
			this.ToolTip1.SetToolTip(this.Frame2, null);
			// 
			// txtRegistrantAddress
			// 
			this.txtRegistrantAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtRegistrantAddress.Location = new System.Drawing.Point(254, 30);
			this.txtRegistrantAddress.Multiline = true;
			this.txtRegistrantAddress.Name = "txtRegistrantAddress";
			this.txtRegistrantAddress.Size = new System.Drawing.Size(692, 100);
			this.txtRegistrantAddress.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtRegistrantAddress, null);
			this.txtRegistrantAddress.Enter += new System.EventHandler(this.txtRegistrantAddress_Enter);
			// 
			// lblLabels_2
			// 
			this.lblLabels_2.AutoSize = true;
			this.lblLabels_2.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_2.Location = new System.Drawing.Point(20, 31);
			this.lblLabels_2.Name = "lblLabels_2";
			this.lblLabels_2.Size = new System.Drawing.Size(232, 15);
			this.lblLabels_2.TabIndex = 2;
			this.lblLabels_2.Text = "PRESENT ADDRESS OF REGISTRANT";
			this.ToolTip1.SetToolTip(this.lblLabels_2, null);
			// 
			// SSTab1_Page3
			// 
			this.SSTab1_Page3.Controls.Add(this.Frame9);
			this.SSTab1_Page3.Controls.Add(this.Frame3);
			this.SSTab1_Page3.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page3.Name = "SSTab1_Page3";
			this.SSTab1_Page3.Text = "Clerk or Notary";
			this.ToolTip1.SetToolTip(this.SSTab1_Page3, null);
			// 
			// Frame9
			// 
			this.Frame9.Controls.Add(this.txtNotaryExpiresDate);
			this.Frame9.Controls.Add(this.txtNotaryFirstName);
			this.Frame9.Controls.Add(this.txtNotaryLastName);
			this.Frame9.Controls.Add(this.txtNotaryMiddleName);
			this.Frame9.Controls.Add(this.mebNotarySwornDate);
			this.Frame9.Controls.Add(this.lblLabels_1);
			this.Frame9.Controls.Add(this.lblLabels_0);
			this.Frame9.Controls.Add(this.lblLabels_21);
			this.Frame9.Controls.Add(this.lblLabels_22);
			this.Frame9.Controls.Add(this.lblLabels_23);
			this.Frame9.Location = new System.Drawing.Point(20, 30);
			this.Frame9.Name = "Frame9";
			this.Frame9.Size = new System.Drawing.Size(966, 110);
			this.Frame9.Text = "Notary";
			this.ToolTip1.SetToolTip(this.Frame9, null);
			// 
			// txtNotaryExpiresDate
			// 
			this.txtNotaryExpiresDate.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotaryExpiresDate.Location = new System.Drawing.Point(729, 50);
			this.txtNotaryExpiresDate.Name = "txtNotaryExpiresDate";
			this.txtNotaryExpiresDate.Size = new System.Drawing.Size(217, 40);
			this.txtNotaryExpiresDate.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtNotaryExpiresDate, null);
			this.txtNotaryExpiresDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtNotaryExpiresDate_Validating);
			// 
			// txtNotaryFirstName
			// 
			this.txtNotaryFirstName.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotaryFirstName.Location = new System.Drawing.Point(153, 50);
			this.txtNotaryFirstName.Name = "txtNotaryFirstName";
			this.txtNotaryFirstName.Size = new System.Drawing.Size(217, 40);
			this.txtNotaryFirstName.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtNotaryFirstName, null);
			// 
			// txtNotaryLastName
			// 
			this.txtNotaryLastName.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotaryLastName.Location = new System.Drawing.Point(492, 50);
			this.txtNotaryLastName.Name = "txtNotaryLastName";
			this.txtNotaryLastName.Size = new System.Drawing.Size(217, 40);
			this.txtNotaryLastName.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtNotaryLastName, null);
			// 
			// txtNotaryMiddleName
			// 
			this.txtNotaryMiddleName.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotaryMiddleName.Location = new System.Drawing.Point(390, 50);
			this.txtNotaryMiddleName.MaxLength = 40;
			this.txtNotaryMiddleName.Name = "txtNotaryMiddleName";
			this.txtNotaryMiddleName.Size = new System.Drawing.Size(82, 40);
			this.txtNotaryMiddleName.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtNotaryMiddleName, null);
			// 
			// mebNotarySwornDate
			// 
			this.mebNotarySwornDate.Location = new System.Drawing.Point(20, 50);
			this.mebNotarySwornDate.Name = "mebNotarySwornDate";
			this.mebNotarySwornDate.Size = new System.Drawing.Size(115, 40);
			this.mebNotarySwornDate.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.mebNotarySwornDate, null);
			this.mebNotarySwornDate.Enter += new System.EventHandler(this.mebNotarySwornDate_Enter);
			// 
			// lblLabels_1
			// 
			this.lblLabels_1.AutoSize = true;
			this.lblLabels_1.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_1.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_1.Name = "lblLabels_1";
			this.lblLabels_1.Size = new System.Drawing.Size(113, 15);
			this.lblLabels_1.TabIndex = 10;
			this.lblLabels_1.Text = "SWORN ON DATE";
			this.ToolTip1.SetToolTip(this.lblLabels_1, null);
			// 
			// lblLabels_0
			// 
			this.lblLabels_0.AutoSize = true;
			this.lblLabels_0.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_0.Location = new System.Drawing.Point(729, 30);
			this.lblLabels_0.Name = "lblLabels_0";
			this.lblLabels_0.Size = new System.Drawing.Size(145, 15);
			this.lblLabels_0.TabIndex = 4;
			this.lblLabels_0.Text = "COMMISSION EXPIRES";
			this.ToolTip1.SetToolTip(this.lblLabels_0, null);
			// 
			// lblLabels_21
			// 
			this.lblLabels_21.AutoSize = true;
			this.lblLabels_21.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_21.Location = new System.Drawing.Point(153, 30);
			this.lblLabels_21.Name = "lblLabels_21";
			this.lblLabels_21.Size = new System.Drawing.Size(82, 15);
			this.lblLabels_21.TabIndex = 1;
			this.lblLabels_21.Text = "FIRST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_21, null);
			// 
			// lblLabels_22
			// 
			this.lblLabels_22.AutoSize = true;
			this.lblLabels_22.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_22.Location = new System.Drawing.Point(492, 30);
			this.lblLabels_22.Name = "lblLabels_22";
			this.lblLabels_22.Size = new System.Drawing.Size(77, 15);
			this.lblLabels_22.TabIndex = 3;
			this.lblLabels_22.Text = "LAST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_22, null);
			// 
			// lblLabels_23
			// 
			this.lblLabels_23.AutoSize = true;
			this.lblLabels_23.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_23.Location = new System.Drawing.Point(390, 30);
			this.lblLabels_23.Name = "lblLabels_23";
			this.lblLabels_23.Size = new System.Drawing.Size(54, 15);
			this.lblLabels_23.TabIndex = 2;
			this.lblLabels_23.Text = "MIDDLE";
			this.ToolTip1.SetToolTip(this.lblLabels_23, null);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtClerkTown);
			this.Frame3.Controls.Add(this.txtClerkMiddleName);
			this.Frame3.Controls.Add(this.txtClerkLastName);
			this.Frame3.Controls.Add(this.txtClerkFirstName);
			this.Frame3.Controls.Add(this.mebClerkSwornDate);
			this.Frame3.Controls.Add(this.lblLabels_11);
			this.Frame3.Controls.Add(this.lblLabels_10);
			this.Frame3.Controls.Add(this.lblLabels_9);
			this.Frame3.Controls.Add(this.lblLabels_6);
			this.Frame3.Controls.Add(this.lblLabels_3);
			this.Frame3.Location = new System.Drawing.Point(20, 160);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(966, 110);
			this.Frame3.TabIndex = 1;
			this.Frame3.Text = "Clerk";
			this.ToolTip1.SetToolTip(this.Frame3, null);
			// 
			// txtClerkTown
			// 
			this.txtClerkTown.BackColor = System.Drawing.SystemColors.Window;
			this.txtClerkTown.Location = new System.Drawing.Point(729, 50);
			this.txtClerkTown.MaxLength = 40;
			this.txtClerkTown.Name = "txtClerkTown";
			this.txtClerkTown.Size = new System.Drawing.Size(217, 40);
			this.txtClerkTown.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtClerkTown, null);
			this.txtClerkTown.Enter += new System.EventHandler(this.txtClerkTown_Enter);
			this.txtClerkTown.DoubleClick += new System.EventHandler(this.txtClerkTown_DoubleClick);
			// 
			// txtClerkMiddleName
			// 
			this.txtClerkMiddleName.BackColor = System.Drawing.SystemColors.Window;
			this.txtClerkMiddleName.Location = new System.Drawing.Point(390, 50);
			this.txtClerkMiddleName.MaxLength = 40;
			this.txtClerkMiddleName.Name = "txtClerkMiddleName";
			this.txtClerkMiddleName.Size = new System.Drawing.Size(82, 40);
			this.txtClerkMiddleName.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtClerkMiddleName, null);
			// 
			// txtClerkLastName
			// 
			this.txtClerkLastName.BackColor = System.Drawing.SystemColors.Window;
			this.txtClerkLastName.Location = new System.Drawing.Point(492, 50);
			this.txtClerkLastName.Name = "txtClerkLastName";
			this.txtClerkLastName.Size = new System.Drawing.Size(217, 40);
			this.txtClerkLastName.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtClerkLastName, null);
			// 
			// txtClerkFirstName
			// 
			this.txtClerkFirstName.BackColor = System.Drawing.SystemColors.Window;
			this.txtClerkFirstName.Location = new System.Drawing.Point(153, 50);
			this.txtClerkFirstName.Name = "txtClerkFirstName";
			this.txtClerkFirstName.Size = new System.Drawing.Size(217, 40);
			this.txtClerkFirstName.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtClerkFirstName, null);
			// 
			// mebClerkSwornDate
			// 
			this.mebClerkSwornDate.Location = new System.Drawing.Point(20, 50);
			this.mebClerkSwornDate.Name = "mebClerkSwornDate";
			this.mebClerkSwornDate.Size = new System.Drawing.Size(115, 40);
			this.mebClerkSwornDate.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.mebClerkSwornDate, null);
			// 
			// lblLabels_11
			// 
			this.lblLabels_11.AutoSize = true;
			this.lblLabels_11.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_11.Location = new System.Drawing.Point(390, 30);
			this.lblLabels_11.Name = "lblLabels_11";
			this.lblLabels_11.Size = new System.Drawing.Size(54, 15);
			this.lblLabels_11.TabIndex = 2;
			this.lblLabels_11.Text = "MIDDLE";
			this.ToolTip1.SetToolTip(this.lblLabels_11, null);
			// 
			// lblLabels_10
			// 
			this.lblLabels_10.AutoSize = true;
			this.lblLabels_10.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_10.Location = new System.Drawing.Point(492, 30);
			this.lblLabels_10.Name = "lblLabels_10";
			this.lblLabels_10.Size = new System.Drawing.Size(77, 15);
			this.lblLabels_10.TabIndex = 3;
			this.lblLabels_10.Text = "LAST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_10, null);
			// 
			// lblLabels_9
			// 
			this.lblLabels_9.AutoSize = true;
			this.lblLabels_9.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_9.Location = new System.Drawing.Point(153, 30);
			this.lblLabels_9.Name = "lblLabels_9";
			this.lblLabels_9.Size = new System.Drawing.Size(82, 15);
			this.lblLabels_9.TabIndex = 1;
			this.lblLabels_9.Text = "FIRST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_9, null);
			// 
			// lblLabels_6
			// 
			this.lblLabels_6.AutoSize = true;
			this.lblLabels_6.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_6.Location = new System.Drawing.Point(729, 30);
			this.lblLabels_6.Name = "lblLabels_6";
			this.lblLabels_6.Size = new System.Drawing.Size(99, 15);
			this.lblLabels_6.TabIndex = 4;
			this.lblLabels_6.Text = "CITY OR TOWN";
			this.ToolTip1.SetToolTip(this.lblLabels_6, null);
			// 
			// lblLabels_3
			// 
			this.lblLabels_3.AutoSize = true;
			this.lblLabels_3.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_3.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_3.Name = "lblLabels_3";
			this.lblLabels_3.Size = new System.Drawing.Size(113, 15);
			this.lblLabels_3.TabIndex = 10;
			this.lblLabels_3.Text = "SWORN ON DATE";
			this.ToolTip1.SetToolTip(this.lblLabels_3, null);
			// 
			// SSTab1_Page4
			// 
			this.SSTab1_Page4.Controls.Add(this.Frame5);
			this.SSTab1_Page4.Controls.Add(this.Frame6);
			this.SSTab1_Page4.Controls.Add(this.Frame10);
			this.SSTab1_Page4.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page4.Name = "SSTab1_Page4";
			this.SSTab1_Page4.Text = "Documentary Evidence";
			this.ToolTip1.SetToolTip(this.SSTab1_Page4, null);
			// 
			// Frame5
			// 
			this.Frame5.Controls.Add(this.txtDateEntry1);
			this.Frame5.Controls.Add(this.txtDateIssued1);
			this.Frame5.Controls.Add(this.txtMotherFullName1);
			this.Frame5.Controls.Add(this.txtFatherFullName1);
			this.Frame5.Controls.Add(this.txtWhomSigned1);
			this.Frame5.Controls.Add(this.txtBirthPlace1);
			this.Frame5.Controls.Add(this.txtDocumentType1);
			this.Frame5.Controls.Add(this.mebDOB1);
			this.Frame5.Controls.Add(this.lblLabels_38);
			this.Frame5.Controls.Add(this.lblLabels_37);
			this.Frame5.Controls.Add(this.lblLabels_34);
			this.Frame5.Controls.Add(this.lblLabels_33);
			this.Frame5.Controls.Add(this.lblLabels_32);
			this.Frame5.Controls.Add(this.lblLabels_31);
			this.Frame5.Controls.Add(this.lblLabels_26);
			this.Frame5.Controls.Add(this.lblLabels_4);
			this.Frame5.Location = new System.Drawing.Point(20, 30);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(966, 180);
			this.Frame5.Text = "Evidence #1";
			this.ToolTip1.SetToolTip(this.Frame5, null);
			// 
			// txtDateEntry1
			// 
			this.txtDateEntry1.BackColor = System.Drawing.SystemColors.Window;
			this.txtDateEntry1.Location = new System.Drawing.Point(763, 50);
			this.txtDateEntry1.MaxLength = 40;
			this.txtDateEntry1.Name = "txtDateEntry1";
			this.txtDateEntry1.Size = new System.Drawing.Size(183, 40);
			this.txtDateEntry1.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtDateEntry1, null);
			this.txtDateEntry1.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateEntry1_Validating);
			// 
			// txtDateIssued1
			// 
			this.txtDateIssued1.BackColor = System.Drawing.SystemColors.Window;
			this.txtDateIssued1.Location = new System.Drawing.Point(560, 50);
			this.txtDateIssued1.MaxLength = 40;
			this.txtDateIssued1.Name = "txtDateIssued1";
			this.txtDateIssued1.Size = new System.Drawing.Size(183, 40);
			this.txtDateIssued1.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtDateIssued1, null);
			this.txtDateIssued1.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateIssued1_Validating);
			// 
			// txtMotherFullName1
			// 
			this.txtMotherFullName1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMotherFullName1.Location = new System.Drawing.Point(671, 120);
			this.txtMotherFullName1.Name = "txtMotherFullName1";
			this.txtMotherFullName1.Size = new System.Drawing.Size(275, 40);
			this.txtMotherFullName1.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.txtMotherFullName1, null);
			// 
			// txtFatherFullName1
			// 
			this.txtFatherFullName1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtFatherFullName1.Location = new System.Drawing.Point(376, 120);
			this.txtFatherFullName1.Name = "txtFatherFullName1";
			this.txtFatherFullName1.Size = new System.Drawing.Size(275, 40);
			this.txtFatherFullName1.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtFatherFullName1, null);
			// 
			// txtWhomSigned1
			// 
			this.txtWhomSigned1.BackColor = System.Drawing.SystemColors.Window;
			this.txtWhomSigned1.Location = new System.Drawing.Point(290, 50);
			this.txtWhomSigned1.MaxLength = 40;
			this.txtWhomSigned1.Name = "txtWhomSigned1";
			this.txtWhomSigned1.Size = new System.Drawing.Size(250, 40);
			this.txtWhomSigned1.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtWhomSigned1, null);
			// 
			// txtBirthPlace1
			// 
			this.txtBirthPlace1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtBirthPlace1.Location = new System.Drawing.Point(153, 120);
			this.txtBirthPlace1.Name = "txtBirthPlace1";
			this.txtBirthPlace1.Size = new System.Drawing.Size(203, 40);
			this.txtBirthPlace1.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtBirthPlace1, null);
			this.txtBirthPlace1.DoubleClick += new System.EventHandler(this.txtBirthPlace1_DoubleClick);
			// 
			// txtDocumentType1
			// 
			this.txtDocumentType1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtDocumentType1.Location = new System.Drawing.Point(20, 50);
			this.txtDocumentType1.Name = "txtDocumentType1";
			this.txtDocumentType1.Size = new System.Drawing.Size(250, 40);
			this.txtDocumentType1.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtDocumentType1, null);
			this.txtDocumentType1.Enter += new System.EventHandler(this.txtDocumentType1_Enter);
			// 
			// mebDOB1
			// 
			this.mebDOB1.Location = new System.Drawing.Point(20, 120);
			this.mebDOB1.Name = "mebDOB1";
			this.mebDOB1.Size = new System.Drawing.Size(115, 40);
			this.mebDOB1.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.mebDOB1, null);
			// 
			// lblLabels_38
			// 
			this.lblLabels_38.AutoSize = true;
			this.lblLabels_38.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_38.Location = new System.Drawing.Point(671, 100);
			this.lblLabels_38.Name = "lblLabels_38";
			this.lblLabels_38.Size = new System.Drawing.Size(154, 15);
			this.lblLabels_38.TabIndex = 14;
			this.lblLabels_38.Text = "FULL NAME OF MOTHER";
			this.ToolTip1.SetToolTip(this.lblLabels_38, null);
			// 
			// lblLabels_37
			// 
			this.lblLabels_37.AutoSize = true;
			this.lblLabels_37.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_37.Location = new System.Drawing.Point(376, 100);
			this.lblLabels_37.Name = "lblLabels_37";
			this.lblLabels_37.Size = new System.Drawing.Size(150, 15);
			this.lblLabels_37.TabIndex = 12;
			this.lblLabels_37.Text = "FULL NAME OF FATHER";
			this.ToolTip1.SetToolTip(this.lblLabels_37, null);
			// 
			// lblLabels_34
			// 
			this.lblLabels_34.AutoSize = true;
			this.lblLabels_34.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_34.Location = new System.Drawing.Point(20, 100);
			this.lblLabels_34.Name = "lblLabels_34";
			this.lblLabels_34.Size = new System.Drawing.Size(101, 15);
			this.lblLabels_34.TabIndex = 8;
			this.lblLabels_34.Text = "DATE OF BIRTH";
			this.ToolTip1.SetToolTip(this.lblLabels_34, null);
			// 
			// lblLabels_33
			// 
			this.lblLabels_33.AutoSize = true;
			this.lblLabels_33.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_33.Location = new System.Drawing.Point(290, 30);
			this.lblLabels_33.Name = "lblLabels_33";
			this.lblLabels_33.Size = new System.Drawing.Size(118, 15);
			this.lblLabels_33.TabIndex = 2;
			this.lblLabels_33.Text = "BY WHOM SIGNED";
			this.ToolTip1.SetToolTip(this.lblLabels_33, null);
			// 
			// lblLabels_32
			// 
			this.lblLabels_32.AutoSize = true;
			this.lblLabels_32.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_32.Location = new System.Drawing.Point(153, 100);
			this.lblLabels_32.Name = "lblLabels_32";
			this.lblLabels_32.Size = new System.Drawing.Size(85, 15);
			this.lblLabels_32.TabIndex = 10;
			this.lblLabels_32.Text = "BIRTHPLACE";
			this.ToolTip1.SetToolTip(this.lblLabels_32, null);
			// 
			// lblLabels_31
			// 
			this.lblLabels_31.AutoSize = true;
			this.lblLabels_31.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_31.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_31.Name = "lblLabels_31";
			this.lblLabels_31.Size = new System.Drawing.Size(135, 15);
			this.lblLabels_31.TabIndex = 16;
			this.lblLabels_31.Text = "TYPE OF DOCUMENT";
			this.ToolTip1.SetToolTip(this.lblLabels_31, null);
			// 
			// lblLabels_26
			// 
			this.lblLabels_26.AutoSize = true;
			this.lblLabels_26.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_26.Location = new System.Drawing.Point(763, 30);
			this.lblLabels_26.Name = "lblLabels_26";
			this.lblLabels_26.Size = new System.Drawing.Size(141, 15);
			this.lblLabels_26.TabIndex = 6;
			this.lblLabels_26.Text = "DATE OF ORIG ENTRY";
			this.ToolTip1.SetToolTip(this.lblLabels_26, null);
			// 
			// lblLabels_4
			// 
			this.lblLabels_4.AutoSize = true;
			this.lblLabels_4.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_4.Location = new System.Drawing.Point(560, 30);
			this.lblLabels_4.Name = "lblLabels_4";
			this.lblLabels_4.Size = new System.Drawing.Size(90, 15);
			this.lblLabels_4.TabIndex = 4;
			this.lblLabels_4.Text = "DATE ISSUED ";
			this.ToolTip1.SetToolTip(this.lblLabels_4, null);
			// 
			// Frame6
			// 
			this.Frame6.Controls.Add(this.txtDateEntry2);
			this.Frame6.Controls.Add(this.txtDateIssued2);
			this.Frame6.Controls.Add(this.txtDocumentType2);
			this.Frame6.Controls.Add(this.txtBirthPlace2);
			this.Frame6.Controls.Add(this.txtWhomSigned2);
			this.Frame6.Controls.Add(this.txtFatherFullName2);
			this.Frame6.Controls.Add(this.txtMotherFullName2);
			this.Frame6.Controls.Add(this.mebDOB2);
			this.Frame6.Controls.Add(this.lblLabels_47);
			this.Frame6.Controls.Add(this.lblLabels_46);
			this.Frame6.Controls.Add(this.lblLabels_45);
			this.Frame6.Controls.Add(this.lblLabels_44);
			this.Frame6.Controls.Add(this.lblLabels_43);
			this.Frame6.Controls.Add(this.lblLabels_41);
			this.Frame6.Controls.Add(this.lblLabels_40);
			this.Frame6.Controls.Add(this.lblLabels_39);
			this.Frame6.Location = new System.Drawing.Point(20, 230);
			this.Frame6.Name = "Frame6";
			this.Frame6.Size = new System.Drawing.Size(966, 180);
			this.Frame6.TabIndex = 1;
			this.Frame6.Text = "Evidence #2";
			this.ToolTip1.SetToolTip(this.Frame6, null);
			// 
			// txtDateEntry2
			// 
			this.txtDateEntry2.BackColor = System.Drawing.SystemColors.Window;
			this.txtDateEntry2.Location = new System.Drawing.Point(763, 50);
			this.txtDateEntry2.MaxLength = 40;
			this.txtDateEntry2.Name = "txtDateEntry2";
			this.txtDateEntry2.Size = new System.Drawing.Size(183, 40);
			this.txtDateEntry2.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtDateEntry2, null);
			this.txtDateEntry2.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateEntry2_Validating);
			// 
			// txtDateIssued2
			// 
			this.txtDateIssued2.BackColor = System.Drawing.SystemColors.Window;
			this.txtDateIssued2.Location = new System.Drawing.Point(560, 50);
			this.txtDateIssued2.MaxLength = 40;
			this.txtDateIssued2.Name = "txtDateIssued2";
			this.txtDateIssued2.Size = new System.Drawing.Size(183, 40);
			this.txtDateIssued2.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtDateIssued2, null);
			this.txtDateIssued2.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateIssued2_Validating);
			// 
			// txtDocumentType2
			// 
			this.txtDocumentType2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtDocumentType2.Location = new System.Drawing.Point(20, 50);
			this.txtDocumentType2.Name = "txtDocumentType2";
			this.txtDocumentType2.Size = new System.Drawing.Size(250, 40);
			this.txtDocumentType2.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtDocumentType2, null);
			// 
			// txtBirthPlace2
			// 
			this.txtBirthPlace2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtBirthPlace2.Location = new System.Drawing.Point(153, 120);
			this.txtBirthPlace2.Name = "txtBirthPlace2";
			this.txtBirthPlace2.Size = new System.Drawing.Size(203, 40);
			this.txtBirthPlace2.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtBirthPlace2, null);
			this.txtBirthPlace2.DoubleClick += new System.EventHandler(this.txtBirthPlace2_DoubleClick);
			// 
			// txtWhomSigned2
			// 
			this.txtWhomSigned2.BackColor = System.Drawing.SystemColors.Window;
			this.txtWhomSigned2.Location = new System.Drawing.Point(290, 50);
			this.txtWhomSigned2.MaxLength = 40;
			this.txtWhomSigned2.Name = "txtWhomSigned2";
			this.txtWhomSigned2.Size = new System.Drawing.Size(250, 40);
			this.txtWhomSigned2.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtWhomSigned2, null);
			// 
			// txtFatherFullName2
			// 
			this.txtFatherFullName2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtFatherFullName2.Location = new System.Drawing.Point(376, 120);
			this.txtFatherFullName2.Name = "txtFatherFullName2";
			this.txtFatherFullName2.Size = new System.Drawing.Size(275, 40);
			this.txtFatherFullName2.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtFatherFullName2, null);
			// 
			// txtMotherFullName2
			// 
			this.txtMotherFullName2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMotherFullName2.Location = new System.Drawing.Point(671, 120);
			this.txtMotherFullName2.Name = "txtMotherFullName2";
			this.txtMotherFullName2.Size = new System.Drawing.Size(275, 40);
			this.txtMotherFullName2.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.txtMotherFullName2, null);
			// 
			// mebDOB2
			// 
			this.mebDOB2.Location = new System.Drawing.Point(20, 120);
			this.mebDOB2.Name = "mebDOB2";
			this.mebDOB2.Size = new System.Drawing.Size(115, 40);
			this.mebDOB2.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.mebDOB2, null);
			// 
			// lblLabels_47
			// 
			this.lblLabels_47.AutoSize = true;
			this.lblLabels_47.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_47.Location = new System.Drawing.Point(560, 30);
			this.lblLabels_47.Name = "lblLabels_47";
			this.lblLabels_47.Size = new System.Drawing.Size(90, 15);
			this.lblLabels_47.TabIndex = 4;
			this.lblLabels_47.Text = "DATE ISSUED ";
			this.ToolTip1.SetToolTip(this.lblLabels_47, null);
			// 
			// lblLabels_46
			// 
			this.lblLabels_46.AutoSize = true;
			this.lblLabels_46.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_46.Location = new System.Drawing.Point(763, 30);
			this.lblLabels_46.Name = "lblLabels_46";
			this.lblLabels_46.Size = new System.Drawing.Size(141, 15);
			this.lblLabels_46.TabIndex = 6;
			this.lblLabels_46.Text = "DATE OF ORIG ENTRY";
			this.ToolTip1.SetToolTip(this.lblLabels_46, null);
			// 
			// lblLabels_45
			// 
			this.lblLabels_45.AutoSize = true;
			this.lblLabels_45.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_45.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_45.Name = "lblLabels_45";
			this.lblLabels_45.Size = new System.Drawing.Size(135, 15);
			this.lblLabels_45.TabIndex = 16;
			this.lblLabels_45.Text = "TYPE OF DOCUMENT";
			this.ToolTip1.SetToolTip(this.lblLabels_45, null);
			// 
			// lblLabels_44
			// 
			this.lblLabels_44.AutoSize = true;
			this.lblLabels_44.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_44.Location = new System.Drawing.Point(153, 100);
			this.lblLabels_44.Name = "lblLabels_44";
			this.lblLabels_44.Size = new System.Drawing.Size(85, 15);
			this.lblLabels_44.TabIndex = 10;
			this.lblLabels_44.Text = "BIRTHPLACE";
			this.ToolTip1.SetToolTip(this.lblLabels_44, null);
			// 
			// lblLabels_43
			// 
			this.lblLabels_43.AutoSize = true;
			this.lblLabels_43.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_43.Location = new System.Drawing.Point(290, 30);
			this.lblLabels_43.Name = "lblLabels_43";
			this.lblLabels_43.Size = new System.Drawing.Size(118, 15);
			this.lblLabels_43.TabIndex = 2;
			this.lblLabels_43.Text = "BY WHOM SIGNED";
			this.ToolTip1.SetToolTip(this.lblLabels_43, null);
			// 
			// lblLabels_41
			// 
			this.lblLabels_41.AutoSize = true;
			this.lblLabels_41.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_41.Location = new System.Drawing.Point(20, 100);
			this.lblLabels_41.Name = "lblLabels_41";
			this.lblLabels_41.Size = new System.Drawing.Size(101, 15);
			this.lblLabels_41.TabIndex = 8;
			this.lblLabels_41.Text = "DATE OF BIRTH";
			this.ToolTip1.SetToolTip(this.lblLabels_41, null);
			// 
			// lblLabels_40
			// 
			this.lblLabels_40.AutoSize = true;
			this.lblLabels_40.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_40.Location = new System.Drawing.Point(376, 100);
			this.lblLabels_40.Name = "lblLabels_40";
			this.lblLabels_40.Size = new System.Drawing.Size(150, 15);
			this.lblLabels_40.TabIndex = 12;
			this.lblLabels_40.Text = "FULL NAME OF FATHER";
			this.ToolTip1.SetToolTip(this.lblLabels_40, null);
			// 
			// lblLabels_39
			// 
			this.lblLabels_39.AutoSize = true;
			this.lblLabels_39.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_39.Location = new System.Drawing.Point(671, 100);
			this.lblLabels_39.Name = "lblLabels_39";
			this.lblLabels_39.Size = new System.Drawing.Size(154, 15);
			this.lblLabels_39.TabIndex = 14;
			this.lblLabels_39.Text = "FULL NAME OF MOTHER";
			this.ToolTip1.SetToolTip(this.lblLabels_39, null);
			// 
			// Frame10
			// 
			this.Frame10.Controls.Add(this.txtDateEntry3);
			this.Frame10.Controls.Add(this.txtDateIssued3);
			this.Frame10.Controls.Add(this.txtDocumentType3);
			this.Frame10.Controls.Add(this.txtBirthPlace3);
			this.Frame10.Controls.Add(this.txtWhomSigned3);
			this.Frame10.Controls.Add(this.txtFatherFullName3);
			this.Frame10.Controls.Add(this.txtMotherFullName3);
			this.Frame10.Controls.Add(this.mebDOB3);
			this.Frame10.Controls.Add(this.lblLabels_55);
			this.Frame10.Controls.Add(this.lblLabels_54);
			this.Frame10.Controls.Add(this.lblLabels_53);
			this.Frame10.Controls.Add(this.lblLabels_52);
			this.Frame10.Controls.Add(this.lblLabels_51);
			this.Frame10.Controls.Add(this.lblLabels_50);
			this.Frame10.Controls.Add(this.lblLabels_49);
			this.Frame10.Controls.Add(this.lblLabels_48);
			this.Frame10.Location = new System.Drawing.Point(20, 430);
			this.Frame10.Name = "Frame10";
			this.Frame10.Size = new System.Drawing.Size(966, 180);
			this.Frame10.TabIndex = 2;
			this.Frame10.Text = "Evidence #3";
			this.ToolTip1.SetToolTip(this.Frame10, null);
			// 
			// txtDateEntry3
			// 
			this.txtDateEntry3.BackColor = System.Drawing.SystemColors.Window;
			this.txtDateEntry3.Location = new System.Drawing.Point(763, 50);
			this.txtDateEntry3.MaxLength = 40;
			this.txtDateEntry3.Name = "txtDateEntry3";
			this.txtDateEntry3.Size = new System.Drawing.Size(183, 40);
			this.txtDateEntry3.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtDateEntry3, null);
			this.txtDateEntry3.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateEntry3_Validating);
			// 
			// txtDateIssued3
			// 
			this.txtDateIssued3.BackColor = System.Drawing.SystemColors.Window;
			this.txtDateIssued3.Location = new System.Drawing.Point(560, 50);
			this.txtDateIssued3.MaxLength = 40;
			this.txtDateIssued3.Name = "txtDateIssued3";
			this.txtDateIssued3.Size = new System.Drawing.Size(183, 40);
			this.txtDateIssued3.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtDateIssued3, null);
			this.txtDateIssued3.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateIssued3_Validating);
			// 
			// txtDocumentType3
			// 
			this.txtDocumentType3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtDocumentType3.Location = new System.Drawing.Point(20, 50);
			this.txtDocumentType3.Name = "txtDocumentType3";
			this.txtDocumentType3.Size = new System.Drawing.Size(250, 40);
			this.txtDocumentType3.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtDocumentType3, null);
			// 
			// txtBirthPlace3
			// 
			this.txtBirthPlace3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtBirthPlace3.Location = new System.Drawing.Point(153, 120);
			this.txtBirthPlace3.Name = "txtBirthPlace3";
			this.txtBirthPlace3.Size = new System.Drawing.Size(203, 40);
			this.txtBirthPlace3.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtBirthPlace3, null);
			this.txtBirthPlace3.DoubleClick += new System.EventHandler(this.txtBirthPlace3_DoubleClick);
			// 
			// txtWhomSigned3
			// 
			this.txtWhomSigned3.BackColor = System.Drawing.SystemColors.Window;
			this.txtWhomSigned3.Location = new System.Drawing.Point(290, 50);
			this.txtWhomSigned3.MaxLength = 40;
			this.txtWhomSigned3.Name = "txtWhomSigned3";
			this.txtWhomSigned3.Size = new System.Drawing.Size(250, 40);
			this.txtWhomSigned3.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtWhomSigned3, null);
			// 
			// txtFatherFullName3
			// 
			this.txtFatherFullName3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtFatherFullName3.Location = new System.Drawing.Point(376, 120);
			this.txtFatherFullName3.Name = "txtFatherFullName3";
			this.txtFatherFullName3.Size = new System.Drawing.Size(275, 40);
			this.txtFatherFullName3.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtFatherFullName3, null);
			// 
			// txtMotherFullName3
			// 
			this.txtMotherFullName3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMotherFullName3.Location = new System.Drawing.Point(671, 120);
			this.txtMotherFullName3.Name = "txtMotherFullName3";
			this.txtMotherFullName3.Size = new System.Drawing.Size(275, 40);
			this.txtMotherFullName3.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.txtMotherFullName3, null);
			this.txtMotherFullName3.Enter += new System.EventHandler(this.txtMotherFullName3_Enter);
			// 
			// mebDOB3
			// 
			this.mebDOB3.Location = new System.Drawing.Point(20, 120);
			this.mebDOB3.Name = "mebDOB3";
			this.mebDOB3.Size = new System.Drawing.Size(115, 40);
			this.mebDOB3.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.mebDOB3, null);
			// 
			// lblLabels_55
			// 
			this.lblLabels_55.AutoSize = true;
			this.lblLabels_55.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_55.Location = new System.Drawing.Point(560, 30);
			this.lblLabels_55.Name = "lblLabels_55";
			this.lblLabels_55.Size = new System.Drawing.Size(90, 15);
			this.lblLabels_55.TabIndex = 4;
			this.lblLabels_55.Text = "DATE ISSUED ";
			this.ToolTip1.SetToolTip(this.lblLabels_55, null);
			// 
			// lblLabels_54
			// 
			this.lblLabels_54.AutoSize = true;
			this.lblLabels_54.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_54.Location = new System.Drawing.Point(763, 30);
			this.lblLabels_54.Name = "lblLabels_54";
			this.lblLabels_54.Size = new System.Drawing.Size(141, 15);
			this.lblLabels_54.TabIndex = 6;
			this.lblLabels_54.Text = "DATE OF ORIG ENTRY";
			this.ToolTip1.SetToolTip(this.lblLabels_54, null);
			// 
			// lblLabels_53
			// 
			this.lblLabels_53.AutoSize = true;
			this.lblLabels_53.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_53.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_53.Name = "lblLabels_53";
			this.lblLabels_53.Size = new System.Drawing.Size(135, 15);
			this.lblLabels_53.TabIndex = 16;
			this.lblLabels_53.Text = "TYPE OF DOCUMENT";
			this.ToolTip1.SetToolTip(this.lblLabels_53, null);
			// 
			// lblLabels_52
			// 
			this.lblLabels_52.AutoSize = true;
			this.lblLabels_52.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_52.Location = new System.Drawing.Point(153, 100);
			this.lblLabels_52.Name = "lblLabels_52";
			this.lblLabels_52.Size = new System.Drawing.Size(85, 15);
			this.lblLabels_52.TabIndex = 10;
			this.lblLabels_52.Text = "BIRTHPLACE";
			this.ToolTip1.SetToolTip(this.lblLabels_52, null);
			// 
			// lblLabels_51
			// 
			this.lblLabels_51.AutoSize = true;
			this.lblLabels_51.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_51.Location = new System.Drawing.Point(290, 30);
			this.lblLabels_51.Name = "lblLabels_51";
			this.lblLabels_51.Size = new System.Drawing.Size(118, 15);
			this.lblLabels_51.TabIndex = 2;
			this.lblLabels_51.Text = "BY WHOM SIGNED";
			this.ToolTip1.SetToolTip(this.lblLabels_51, null);
			// 
			// lblLabels_50
			// 
			this.lblLabels_50.AutoSize = true;
			this.lblLabels_50.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_50.Location = new System.Drawing.Point(20, 100);
			this.lblLabels_50.Name = "lblLabels_50";
			this.lblLabels_50.Size = new System.Drawing.Size(101, 15);
			this.lblLabels_50.TabIndex = 8;
			this.lblLabels_50.Text = "DATE OF BIRTH";
			this.ToolTip1.SetToolTip(this.lblLabels_50, null);
			// 
			// lblLabels_49
			// 
			this.lblLabels_49.AutoSize = true;
			this.lblLabels_49.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_49.Location = new System.Drawing.Point(376, 100);
			this.lblLabels_49.Name = "lblLabels_49";
			this.lblLabels_49.Size = new System.Drawing.Size(150, 15);
			this.lblLabels_49.TabIndex = 12;
			this.lblLabels_49.Text = "FULL NAME OF FATHER";
			this.ToolTip1.SetToolTip(this.lblLabels_49, null);
			// 
			// lblLabels_48
			// 
			this.lblLabels_48.AutoSize = true;
			this.lblLabels_48.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_48.Location = new System.Drawing.Point(671, 100);
			this.lblLabels_48.Name = "lblLabels_48";
			this.lblLabels_48.Size = new System.Drawing.Size(154, 15);
			this.lblLabels_48.TabIndex = 14;
			this.lblLabels_48.Text = "FULL NAME OF MOTHER";
			this.ToolTip1.SetToolTip(this.lblLabels_48, null);
			// 
			// SSTab1_Page5
			// 
			this.SSTab1_Page5.Controls.Add(this.Frame4);
			this.SSTab1_Page5.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page5.Name = "SSTab1_Page5";
			this.SSTab1_Page5.Text = "State Registrar";
			this.ToolTip1.SetToolTip(this.SSTab1_Page5, null);
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.txtDateOfFiling);
			this.Frame4.Controls.Add(this.txtRegistrarReviewed);
			this.Frame4.Controls.Add(this.txtRegistrarName);
			this.Frame4.Controls.Add(this.lblLabels_27);
			this.Frame4.Controls.Add(this.lblLabels_24);
			this.Frame4.Controls.Add(this.lblLabels_7);
			this.Frame4.Location = new System.Drawing.Point(20, 30);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(966, 110);
			this.Frame4.Text = "State Registrar";
			this.ToolTip1.SetToolTip(this.Frame4, null);
			// 
			// txtDateOfFiling
			// 
			this.txtDateOfFiling.BackColor = System.Drawing.SystemColors.Window;
			this.txtDateOfFiling.Location = new System.Drawing.Point(760, 50);
			this.txtDateOfFiling.MaxLength = 40;
			this.txtDateOfFiling.Name = "txtDateOfFiling";
			this.txtDateOfFiling.Size = new System.Drawing.Size(186, 40);
			this.txtDateOfFiling.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtDateOfFiling, null);
			this.txtDateOfFiling.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateOfFiling_Validating);
			// 
			// txtRegistrarReviewed
			// 
			this.txtRegistrarReviewed.BackColor = System.Drawing.SystemColors.Window;
			this.txtRegistrarReviewed.Location = new System.Drawing.Point(390, 50);
			this.txtRegistrarReviewed.MaxLength = 40;
			this.txtRegistrarReviewed.Name = "txtRegistrarReviewed";
			this.txtRegistrarReviewed.Size = new System.Drawing.Size(350, 40);
			this.txtRegistrarReviewed.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtRegistrarReviewed, null);
			// 
			// txtRegistrarName
			// 
			this.txtRegistrarName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtRegistrarName.Location = new System.Drawing.Point(20, 50);
			this.txtRegistrarName.Name = "txtRegistrarName";
			this.txtRegistrarName.Size = new System.Drawing.Size(350, 40);
			this.txtRegistrarName.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtRegistrarName, null);
			this.txtRegistrarName.Enter += new System.EventHandler(this.txtRegistrarName_Enter);
			// 
			// lblLabels_27
			// 
			this.lblLabels_27.AutoSize = true;
			this.lblLabels_27.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_27.Location = new System.Drawing.Point(390, 30);
			this.lblLabels_27.Name = "lblLabels_27";
			this.lblLabels_27.Size = new System.Drawing.Size(160, 15);
			this.lblLabels_27.TabIndex = 2;
			this.lblLabels_27.Text = "EVIDENCE REVIEWED BY";
			this.ToolTip1.SetToolTip(this.lblLabels_27, null);
			// 
			// lblLabels_24
			// 
			this.lblLabels_24.AutoSize = true;
			this.lblLabels_24.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_24.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_24.Name = "lblLabels_24";
			this.lblLabels_24.Size = new System.Drawing.Size(119, 15);
			this.lblLabels_24.TabIndex = 6;
			this.lblLabels_24.Text = "REGISTRAR NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_24, null);
			// 
			// lblLabels_7
			// 
			this.lblLabels_7.AutoSize = true;
			this.lblLabels_7.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_7.Location = new System.Drawing.Point(760, 30);
			this.lblLabels_7.Name = "lblLabels_7";
			this.lblLabels_7.Size = new System.Drawing.Size(104, 15);
			this.lblLabels_7.TabIndex = 4;
			this.lblLabels_7.Text = "DATE OF FILING";
			this.ToolTip1.SetToolTip(this.lblLabels_7, null);
			// 
			// chkRequired
			// 
			this.chkRequired.Location = new System.Drawing.Point(330, 67);
			this.chkRequired.Name = "chkRequired";
			this.chkRequired.Size = new System.Drawing.Size(437, 27);
			this.chkRequired.TabIndex = 2;
			this.chkRequired.Text = "Record contains older data. Not all fields will be required";
			this.ToolTip1.SetToolTip(this.chkRequired, null);
			// 
			// txtFileNumber
			// 
			this.txtFileNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtFileNumber.Location = new System.Drawing.Point(100, 30);
			this.txtFileNumber.Name = "txtFileNumber";
			this.txtFileNumber.Size = new System.Drawing.Size(200, 40);
			this.txtFileNumber.TabIndex = 70;
			this.ToolTip1.SetToolTip(this.txtFileNumber, null);
			// 
			// chkIllegitBirth
			// 
			this.chkIllegitBirth.Location = new System.Drawing.Point(330, 30);
			this.chkIllegitBirth.Name = "chkIllegitBirth";
			this.chkIllegitBirth.Size = new System.Drawing.Size(291, 27);
			this.chkIllegitBirth.TabIndex = 1;
			this.chkIllegitBirth.Text = "Check this box if this birth was BOW";
			this.ToolTip1.SetToolTip(this.chkIllegitBirth, null);
			this.chkIllegitBirth.CheckedChanged += new System.EventHandler(this.chkIllegitBirth_CheckedChanged);
			// 
			// cdlColor
			// 
			this.cdlColor.Name = "cdlColor";
			this.cdlColor.Size = new System.Drawing.Size(0, 0);
			this.ToolTip1.SetToolTip(this.cdlColor, null);
			// 
			// txtBirthCertNum
			// 
			this.txtBirthCertNum.Appearance = 0;
			this.txtBirthCertNum.BackColor = System.Drawing.SystemColors.Info;
			this.txtBirthCertNum.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtBirthCertNum.Location = new System.Drawing.Point(493, 332);
			this.txtBirthCertNum.Name = "txtBirthCertNum";
			this.txtBirthCertNum.Size = new System.Drawing.Size(112, 40);
			this.txtBirthCertNum.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtBirthCertNum, null);
			this.txtBirthCertNum.Visible = false;
			// 
			// mebDateDeceased
			// 
			this.mebDateDeceased.Location = new System.Drawing.Point(681, 106);
			this.mebDateDeceased.Name = "mebDateDeceased";
			this.mebDateDeceased.Size = new System.Drawing.Size(115, 40);
			this.mebDateDeceased.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.mebDateDeceased, null);
			this.mebDateDeceased.Visible = false;
			// 
			// gridTownCode
			// 
			this.gridTownCode.ColumnHeadersVisible = false;
			this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTownCode.ExtendLastCol = true;
			this.gridTownCode.FixedCols = 0;
			this.gridTownCode.FixedRows = 0;
			this.gridTownCode.Location = new System.Drawing.Point(30, 100);
			this.gridTownCode.Name = "gridTownCode";
			this.gridTownCode.ReadOnly = false;
			this.gridTownCode.RowHeadersVisible = false;
			this.gridTownCode.Rows = 1;
			this.gridTownCode.Size = new System.Drawing.Size(257, 41);
			this.gridTownCode.TabIndex = 7;
			this.gridTownCode.Tag = "land";
			this.ToolTip1.SetToolTip(this.gridTownCode, null);
			// 
			// lblBKey
			// 
			this.lblBKey.Location = new System.Drawing.Point(98, 573);
			this.lblBKey.Name = "lblBKey";
			this.lblBKey.Size = new System.Drawing.Size(112, 17);
			this.lblBKey.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.lblBKey, null);
			this.lblBKey.Visible = false;
			// 
			// lblBirthID
			// 
			this.lblBirthID.Location = new System.Drawing.Point(61, 573);
			this.lblBirthID.Name = "lblBirthID";
			this.lblBirthID.Size = new System.Drawing.Size(65, 17);
			this.lblBirthID.TabIndex = 13;
			this.lblBirthID.Text = "BIRTH ID";
			this.lblBirthID.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblBirthID, null);
			this.lblBirthID.Visible = false;
			// 
			// lblRecord
			// 
			this.lblRecord.Location = new System.Drawing.Point(518, 327);
			this.lblRecord.Name = "lblRecord";
			this.lblRecord.Size = new System.Drawing.Size(79, 13);
			this.lblRecord.TabIndex = 12;
			this.lblRecord.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRecord, null);
			this.lblRecord.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessAddNewRecord,
            this.mnuProcessDeleteRecord,
            this.mnuComment,
            this.mnuSP22,
            this.mnuSetDefaultTowns,
            this.mnuSP2,
            this.mnuProcessSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessAddNewRecord
			// 
			this.mnuProcessAddNewRecord.Index = 0;
			this.mnuProcessAddNewRecord.Name = "mnuProcessAddNewRecord";
			this.mnuProcessAddNewRecord.Text = "New";
			this.mnuProcessAddNewRecord.Visible = false;
			this.mnuProcessAddNewRecord.Click += new System.EventHandler(this.mnuProcessAddNewRecord_Click);
			// 
			// mnuProcessDeleteRecord
			// 
			this.mnuProcessDeleteRecord.Index = 1;
			this.mnuProcessDeleteRecord.Name = "mnuProcessDeleteRecord";
			this.mnuProcessDeleteRecord.Text = "Delete";
			this.mnuProcessDeleteRecord.Click += new System.EventHandler(this.mnuProcessDeleteRecord_Click);
			// 
			// mnuComment
			// 
			this.mnuComment.Index = 2;
			this.mnuComment.Name = "mnuComment";
			this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuComment.Text = "Comment";
			this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
			// 
			// mnuSP22
			// 
			this.mnuSP22.Index = 3;
			this.mnuSP22.Name = "mnuSP22";
			this.mnuSP22.Text = "-";
			// 
			// mnuSetDefaultTowns
			// 
			this.mnuSetDefaultTowns.Index = 4;
			this.mnuSetDefaultTowns.Name = "mnuSetDefaultTowns";
			this.mnuSetDefaultTowns.Text = "Set Default Towns";
			this.mnuSetDefaultTowns.Visible = false;
			this.mnuSetDefaultTowns.Click += new System.EventHandler(this.mnuSetDefaultTowns_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 5;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			this.mnuSP2.Visible = false;
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 6;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuProcessSave.Text = "Save";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 7;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit                         ";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 8;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 9;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// lblDeceased
			// 
			this.lblDeceased.AutoSize = true;
			this.lblDeceased.BackColor = System.Drawing.Color.Transparent;
			this.lblDeceased.Location = new System.Drawing.Point(607, 120);
			this.lblDeceased.Name = "lblDeceased";
			this.lblDeceased.Size = new System.Drawing.Size(40, 15);
			this.lblDeceased.TabIndex = 4;
			this.lblDeceased.Text = "DATE ";
			this.ToolTip1.SetToolTip(this.lblDeceased, null);
			this.lblDeceased.Visible = false;
			// 
			// lblLabels_42
			// 
			this.lblLabels_42.AutoSize = true;
			this.lblLabels_42.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_42.Location = new System.Drawing.Point(30, 186);
			this.lblLabels_42.Name = "lblLabels_42";
			this.lblLabels_42.Size = new System.Drawing.Size(159, 15);
			this.lblLabels_42.TabIndex = 8;
			this.lblLabels_42.Text = "AMENDED INFORMATION";
			this.ToolTip1.SetToolTip(this.lblLabels_42, null);
			// 
			// lblBirthCertNum_28
			// 
			this.lblBirthCertNum_28.AutoSize = true;
			this.lblBirthCertNum_28.BackColor = System.Drawing.Color.Transparent;
			this.lblBirthCertNum_28.Location = new System.Drawing.Point(494, 320);
			this.lblBirthCertNum_28.Name = "lblBirthCertNum_28";
			this.lblBirthCertNum_28.Size = new System.Drawing.Size(138, 15);
			this.lblBirthCertNum_28.TabIndex = 10;
			this.lblBirthCertNum_28.Text = "BIRTH CERT NUMBER";
			this.ToolTip1.SetToolTip(this.lblBirthCertNum_28, null);
			this.lblBirthCertNum_28.Visible = false;
			// 
			// lblFile
			// 
			this.lblFile.AutoSize = true;
			this.lblFile.BackColor = System.Drawing.Color.Transparent;
			this.lblFile.Location = new System.Drawing.Point(30, 44);
			this.lblFile.Name = "lblFile";
			this.lblFile.Size = new System.Drawing.Size(44, 15);
			this.lblFile.TabIndex = 71;
			this.lblFile.Text = "FILE #";
			this.ToolTip1.SetToolTip(this.lblFile, null);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(234, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(81, 48);
			this.cmdProcessSave.Text = "Save";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// cmdComment
			// 
			this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdComment.Location = new System.Drawing.Point(972, 29);
			this.cmdComment.Name = "cmdComment";
			this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdComment.Size = new System.Drawing.Size(78, 24);
			this.cmdComment.TabIndex = 2;
			this.cmdComment.Text = "Comment";
			this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
			// 
			// cmdProcessDeleteRecord
			// 
			this.cmdProcessDeleteRecord.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessDeleteRecord.Location = new System.Drawing.Point(906, 29);
			this.cmdProcessDeleteRecord.Name = "cmdProcessDeleteRecord";
			this.cmdProcessDeleteRecord.Size = new System.Drawing.Size(60, 24);
			this.cmdProcessDeleteRecord.TabIndex = 1;
			this.cmdProcessDeleteRecord.Text = "Delete";
			this.cmdProcessDeleteRecord.Click += new System.EventHandler(this.mnuProcessDeleteRecord_Click);
			// 
			// frmBirthDelayed
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1088, 700);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmBirthDelayed";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Delayed Births ";
			this.ToolTip1.SetToolTip(this, null);
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmBirthDelayed_Load);
			this.Activated += new System.EventHandler(this.frmBirthDelayed_Activated);
			this.Resize += new System.EventHandler(this.frmBirthDelayed_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBirthDelayed_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBirthDelayed_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).EndInit();
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).EndInit();
			this.SSTab1_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
			this.Frame7.ResumeLayout(false);
			this.Frame7.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame8)).EndInit();
			this.Frame8.ResumeLayout(false);
			this.Frame8.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			this.SSTab1_Page3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
			this.Frame9.ResumeLayout(false);
			this.Frame9.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebNotarySwornDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebClerkSwornDate)).EndInit();
			this.SSTab1_Page4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			this.Frame5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
			this.Frame6.ResumeLayout(false);
			this.Frame6.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame10)).EndInit();
			this.Frame10.ResumeLayout(false);
			this.Frame10.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.mebDOB3)).EndInit();
			this.SSTab1_Page5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkRequired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIllegitBirth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mebDateDeceased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessDeleteRecord)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdProcessSave;
		private System.ComponentModel.IContainer components;
		private FCButton cmdComment;
        private FCButton cmdProcessDeleteRecord;
    }
}
