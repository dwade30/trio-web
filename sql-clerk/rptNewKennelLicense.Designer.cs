﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptKennelLicense.
	/// </summary>
	partial class rptKennelLicense
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptKennelLicense));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLicenseNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLicenseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIssuedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssuedBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtOwnerName,
				this.txtPhone,
				this.txtAddress,
				this.txtLicenseNumber,
				this.txtLocation,
				this.txtLicenseDate,
				this.txtYear,
				this.txtIssuedBy,
				this.txtMuni2,
				this.txtStreet,
				this.txtCity,
				this.txtZip
			});
			this.PageHeader.Height = 4.020833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1666667F;
			this.txtMuni.Left = 1.666667F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.5520833F;
			this.txtMuni.Width = 3.166667F;
			// 
			// txtOwnerName
			// 
			this.txtOwnerName.Height = 0.1875F;
			this.txtOwnerName.Left = 1.020833F;
			this.txtOwnerName.Name = "txtOwnerName";
			this.txtOwnerName.Text = null;
			this.txtOwnerName.Top = 1.40625F;
			this.txtOwnerName.Width = 3.125F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1875F;
			this.txtPhone.Left = 1.020833F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Text = null;
			this.txtPhone.Top = 2.072917F;
			this.txtPhone.Width = 3.125F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1875F;
			this.txtAddress.Left = 1.020833F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 1.65625F;
			this.txtAddress.Width = 3.125F;
			// 
			// txtLicenseNumber
			// 
			this.txtLicenseNumber.Height = 0.1875F;
			this.txtLicenseNumber.Left = 6.541667F;
			this.txtLicenseNumber.Name = "txtLicenseNumber";
			this.txtLicenseNumber.Text = null;
			this.txtLicenseNumber.Top = 0.125F;
			this.txtLicenseNumber.Width = 0.9375F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 1.020833F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1.875F;
			this.txtLocation.Width = 3.125F;
			// 
			// txtLicenseDate
			// 
			this.txtLicenseDate.Height = 0.1875F;
			this.txtLicenseDate.Left = 1.020833F;
			this.txtLicenseDate.Name = "txtLicenseDate";
			this.txtLicenseDate.Text = null;
			this.txtLicenseDate.Top = 2.322917F;
			this.txtLicenseDate.Width = 1F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1875F;
			this.txtYear.Left = 4.145833F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Text = null;
			this.txtYear.Top = 0.875F;
			this.txtYear.Width = 1F;
			// 
			// txtIssuedBy
			// 
			this.txtIssuedBy.Height = 0.1875F;
			this.txtIssuedBy.Left = 0.5208333F;
			this.txtIssuedBy.Name = "txtIssuedBy";
			this.txtIssuedBy.Text = null;
			this.txtIssuedBy.Top = 3.427083F;
			this.txtIssuedBy.Width = 2F;
			// 
			// txtMuni2
			// 
			this.txtMuni2.Height = 0.1666667F;
			this.txtMuni2.Left = 4.520833F;
			this.txtMuni2.Name = "txtMuni2";
			this.txtMuni2.Text = null;
			this.txtMuni2.Top = 3.427083F;
			this.txtMuni2.Width = 2.416667F;
			// 
			// txtStreet
			// 
			this.txtStreet.Height = 0.1875F;
			this.txtStreet.Left = 0.5208333F;
			this.txtStreet.Name = "txtStreet";
			this.txtStreet.Text = null;
			this.txtStreet.Top = 3.697917F;
			this.txtStreet.Width = 2F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.1875F;
			this.txtCity.Left = 3.270833F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Text = null;
			this.txtCity.Top = 3.697917F;
			this.txtCity.Width = 1.25F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.1875F;
			this.txtZip.Left = 5.4375F;
			this.txtZip.Name = "txtZip";
			this.txtZip.Text = null;
			this.txtZip.Top = 3.697917F;
			this.txtZip.Width = 1F;
			// 
			// rptKennelLicense
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.3472222F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.1388889F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.FetchData += new FetchEventHandler(this.ActiveReport_FetchData);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssuedBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssuedBy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
