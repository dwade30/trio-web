//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmPrintMarriageConsent.
	/// </summary>
	partial class frmPrintMarriageConsent
	{
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCCheckBox chkGroomHospitalized;
		public fecherFoundation.FCCheckBox chkBrideHospitalized;
		public fecherFoundation.FCFrame fraWaiver;
		public fecherFoundation.FCCheckBox chkWaiverBride;
		public fecherFoundation.FCCheckBox chkWaiverGroom;
		public fecherFoundation.FCFrame fraIncarcerated;
		public fecherFoundation.FCCheckBox chkIncarceratedBride;
		public fecherFoundation.FCCheckBox chkIncarceratedGroom;
		public fecherFoundation.FCTextBox txtTown;
		public fecherFoundation.FCTextBox txtClerk;
		public Global.T2KDateBox mebToDate;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdPrint = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.chkGroomHospitalized = new fecherFoundation.FCCheckBox();
            this.chkBrideHospitalized = new fecherFoundation.FCCheckBox();
            this.fraWaiver = new fecherFoundation.FCFrame();
            this.chkWaiverBride = new fecherFoundation.FCCheckBox();
            this.chkWaiverGroom = new fecherFoundation.FCCheckBox();
            this.fraIncarcerated = new fecherFoundation.FCFrame();
            this.chkIncarceratedBride = new fecherFoundation.FCCheckBox();
            this.chkIncarceratedGroom = new fecherFoundation.FCCheckBox();
            this.txtTown = new fecherFoundation.FCTextBox();
            this.txtClerk = new fecherFoundation.FCTextBox();
            this.mebToDate = new Global.T2KDateBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroomHospitalized)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBrideHospitalized)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWaiver)).BeginInit();
            this.fraWaiver.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkWaiverBride)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWaiverGroom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraIncarcerated)).BeginInit();
            this.fraIncarcerated.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncarceratedBride)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncarceratedGroom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebToDate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 489);
            this.BottomPanel.Size = new System.Drawing.Size(560, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(560, 429);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(560, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(338, 30);
            this.HeaderText.Text = "Marriage Consent Information";
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(205, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(159, 48);
            this.cmdPrint.TabIndex = 12;
            this.cmdPrint.Text = "Print / Preview";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxLeftBorder";
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.Controls.Add(this.fraWaiver);
            this.Frame1.Controls.Add(this.fraIncarcerated);
            this.Frame1.Controls.Add(this.txtTown);
            this.Frame1.Controls.Add(this.txtClerk);
            this.Frame1.Controls.Add(this.mebToDate);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(502, 394);
            this.Frame1.TabIndex = 9;
            this.Frame1.Text = "Print Parameters";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.chkGroomHospitalized);
            this.Frame3.Controls.Add(this.chkBrideHospitalized);
            this.Frame3.Location = new System.Drawing.Point(20, 122);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(261, 72);
            this.Frame3.TabIndex = 19;
            this.Frame3.Text = "Hospitalized , Death Imminent";
            // 
            // chkGroomHospitalized
            // 
            this.chkGroomHospitalized.Location = new System.Drawing.Point(104, 30);
            this.chkGroomHospitalized.Name = "chkGroomHospitalized";
            this.chkGroomHospitalized.Size = new System.Drawing.Size(77, 27);
            this.chkGroomHospitalized.TabIndex = 5;
            this.chkGroomHospitalized.Text = "Groom";
            // 
            // chkBrideHospitalized
            // 
            this.chkBrideHospitalized.Location = new System.Drawing.Point(20, 30);
            this.chkBrideHospitalized.Name = "chkBrideHospitalized";
            this.chkBrideHospitalized.Size = new System.Drawing.Size(65, 27);
            this.chkBrideHospitalized.TabIndex = 4;
            this.chkBrideHospitalized.Text = "Bride";
            // 
            // fraWaiver
            // 
            this.fraWaiver.Controls.Add(this.chkWaiverBride);
            this.fraWaiver.Controls.Add(this.chkWaiverGroom);
            this.fraWaiver.Location = new System.Drawing.Point(301, 30);
            this.fraWaiver.Name = "fraWaiver";
            this.fraWaiver.Size = new System.Drawing.Size(201, 72);
            this.fraWaiver.TabIndex = 18;
            this.fraWaiver.Text = "Certified Waiver";
            // 
            // chkWaiverBride
            // 
            this.chkWaiverBride.Location = new System.Drawing.Point(20, 30);
            this.chkWaiverBride.Name = "chkWaiverBride";
            this.chkWaiverBride.Size = new System.Drawing.Size(65, 27);
            this.chkWaiverBride.TabIndex = 2;
            this.chkWaiverBride.Text = "Bride";
            // 
            // chkWaiverGroom
            // 
            this.chkWaiverGroom.Location = new System.Drawing.Point(104, 30);
            this.chkWaiverGroom.Name = "chkWaiverGroom";
            this.chkWaiverGroom.Size = new System.Drawing.Size(77, 27);
            this.chkWaiverGroom.TabIndex = 3;
            this.chkWaiverGroom.Text = "Groom";
            // 
            // fraIncarcerated
            // 
            this.fraIncarcerated.Controls.Add(this.chkIncarceratedBride);
            this.fraIncarcerated.Controls.Add(this.chkIncarceratedGroom);
            this.fraIncarcerated.Location = new System.Drawing.Point(20, 30);
            this.fraIncarcerated.Name = "fraIncarcerated";
            this.fraIncarcerated.Size = new System.Drawing.Size(261, 72);
            this.fraIncarcerated.TabIndex = 17;
            this.fraIncarcerated.Text = "Incarcerated";
            // 
            // chkIncarceratedBride
            // 
            this.chkIncarceratedBride.Location = new System.Drawing.Point(20, 30);
            this.chkIncarceratedBride.Name = "chkIncarceratedBride";
            this.chkIncarceratedBride.Size = new System.Drawing.Size(65, 27);
            this.chkIncarceratedBride.TabIndex = 0;
            this.chkIncarceratedBride.Text = "Bride";
            // 
            // chkIncarceratedGroom
            // 
            this.chkIncarceratedGroom.Location = new System.Drawing.Point(104, 30);
            this.chkIncarceratedGroom.Name = "chkIncarceratedGroom";
            this.chkIncarceratedGroom.Size = new System.Drawing.Size(77, 27);
            this.chkIncarceratedGroom.TabIndex = 1;
            this.chkIncarceratedGroom.Text = "Groom";
            // 
            // txtTown
            // 
            this.txtTown.AutoSize = false;
            this.txtTown.BackColor = System.Drawing.SystemColors.Window;
            this.txtTown.LinkItem = null;
            this.txtTown.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTown.LinkTopic = null;
            this.txtTown.Location = new System.Drawing.Point(247, 274);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(255, 40);
            this.txtTown.TabIndex = 7;
            this.txtTown.TabStop = false;
            // 
            // txtClerk
            // 
            this.txtClerk.AutoSize = false;
            this.txtClerk.BackColor = System.Drawing.SystemColors.Window;
            this.txtClerk.LinkItem = null;
            this.txtClerk.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtClerk.LinkTopic = null;
            this.txtClerk.Location = new System.Drawing.Point(247, 214);
            this.txtClerk.Name = "txtClerk";
            this.txtClerk.Size = new System.Drawing.Size(255, 40);
            this.txtClerk.TabIndex = 6;
            this.txtClerk.TabStop = false;
            // 
            // mebToDate
            // 
            this.mebToDate.Location = new System.Drawing.Point(247, 334);
            this.mebToDate.Mask = "00/00/0000";
            this.mebToDate.Name = "mebToDate";
            this.mebToDate.Size = new System.Drawing.Size(115, 40);
            this.mebToDate.TabIndex = 8;
            this.mebToDate.Text = "  /  /";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 348);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(170, 15);
            this.Label7.TabIndex = 16;
            this.Label7.Text = "DATE COMMISSION EXPIRES";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 288);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(170, 15);
            this.Label3.TabIndex = 11;
            this.Label3.Text = "COUNTY OF NOTARY";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 228);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(170, 15);
            this.Label2.TabIndex = 10;
            this.Label2.Text = "NAME OF NOTARY";
            // 
            // frmPrintMarriageConsent
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(560, 597);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPrintMarriageConsent";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Marriage Consent Information";
            this.Load += new System.EventHandler(this.frmPrintMarriageConsent_Load);
            this.Activated += new System.EventHandler(this.frmPrintMarriageConsent_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintMarriageConsent_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintMarriageConsent_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroomHospitalized)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBrideHospitalized)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWaiver)).EndInit();
            this.fraWaiver.ResumeLayout(false);
            this.fraWaiver.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkWaiverBride)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWaiverGroom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraIncarcerated)).EndInit();
            this.fraIncarcerated.ResumeLayout(false);
            this.fraIncarcerated.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncarceratedBride)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncarceratedGroom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebToDate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}