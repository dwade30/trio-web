﻿using Global;

namespace TWCK0000
{
    partial class frmChooseClerkTransactionType : BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.cmdCancel = new fecherFoundation.FCButton();
			this.flexLayoutPanel1 = new Wisej.Web.FlexLayoutPanel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			this.SuspendLayout();
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.flexLayoutPanel1);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdCancel);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdCancel, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(294, 30);
			this.HeaderText.Text = "Choose Transaction Type";
			// 
			// cmdCancel
			// 
			this.cmdCancel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdCancel.Location = new System.Drawing.Point(893, 32);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdCancel.Size = new System.Drawing.Size(88, 24);
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// flexLayoutPanel1
			// 
			this.flexLayoutPanel1.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
			this.flexLayoutPanel1.Location = new System.Drawing.Point(362, 74);
			this.flexLayoutPanel1.Name = "flexLayoutPanel1";
			this.flexLayoutPanel1.Size = new System.Drawing.Size(283, 287);
			this.flexLayoutPanel1.TabIndex = 3;
			this.flexLayoutPanel1.TabStop = true;
			// 
			// frmChooseClerkTransactionType
			// 
			this.Name = "frmChooseClerkTransactionType";
			this.Text = "Choose Transaction Type";
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCButton cmdCancel;
        private Wisej.Web.FlexLayoutPanel flexLayoutPanel1;
    }
}