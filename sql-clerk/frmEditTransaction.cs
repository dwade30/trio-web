//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmEditTransaction : BaseForm
	{
		public frmEditTransaction()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			lblLabels = new System.Collections.Generic.List<FCLabel>();
			lblLabels.AddControlArrayElement(lblLabels_1, 0);
            chkDangerous.CheckedChanged += ChkDangerous_CheckedChanged;
            chkNuisance.CheckedChanged += ChkNuisance_CheckedChanged;
		}

        private void ChkNuisance_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNuisance.Checked)
            {
                chkDangerous.Value = FCCheckBox.ValueSettings.Unchecked;
            }
            RecalcAmount();
        }

        private void ChkDangerous_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDangerous.Checked)
            {
                chkNuisance.Value = FCCheckBox.ValueSettings.Unchecked;
            }
            RecalcAmount();
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmEditTransaction InstancePtr
		{
			get
			{
				return (frmEditTransaction)Sys.GetInstance(typeof(frmEditTransaction));
			}
		}

		protected frmEditTransaction _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngTransactionID;
		private bool boolKennel;
		private bool boolLoading;
		private modGNBas.typDogTransactionRecord CurDogTrans = new modGNBas.typDogTransactionRecord();

		public void Init(ref int lngID)
		{
			lngTransactionID = lngID;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void chkHearingGuid_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkLateFee_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkNeuterSpay_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkReplacementLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkReplacementSticker_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkReplacementTag_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkSearchRescue_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkTempLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkTransferLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkUserDefinedFee_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void chkWarrantFee_CheckedChanged(object sender, System.EventArgs e)
		{
			RecalcAmount();
		}

		private void frmEditTransaction_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEditTransaction_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			modDogs.GetDogFees();
			if (modDogs.Statics.DogFee.UserDefinedDescription1 != string.Empty)
			{
				chkUserDefinedFee.Text = modDogs.Statics.DogFee.UserDefinedDescription1;
				lblUserDefinedFee.Text = modDogs.Statics.DogFee.UserDefinedDescription1;
			}
			boolLoading = true;
			LoadInfo();
			boolLoading = false;
		}

		private void LoadInfo()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL = "";
			int lngOwner = 0;
			int lngDog = 0;
			string strDogs = "";
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CurDogTrans.Amount = 0;
				CurDogTrans.ID = 0;
				CurDogTrans.boolAdjustmentVoid = false;
				CurDogTrans.boolHearingGuid = false;
				CurDogTrans.boolLicense = false;
				CurDogTrans.boolKennel = false;
				CurDogTrans.boolReplacementLicense = false;
				CurDogTrans.boolReplacementSticker = false;
				CurDogTrans.boolReplacementTag = false;
				CurDogTrans.boolSearchRescue = false;
				CurDogTrans.boolSpayNeuter = false;
				CurDogTrans.boolTempLicense = false;
				CurDogTrans.boolTransfer = false;
				CurDogTrans.ClerkFee = 0;
				CurDogTrans.Description = "";
                CurDogTrans.IsDangerous = false;
                CurDogTrans.IsNuisance = false;
				CurDogTrans.KennelDogs = "";
				CurDogTrans.KennelLateFee = 0;
				CurDogTrans.KennelLic1Fee = 0;
				CurDogTrans.KennelLic1Num = 0;
				CurDogTrans.KennelLic2Fee = 0;
				CurDogTrans.KennelLic2Num = 0;
				CurDogTrans.KennelLicenseID = 0;
				CurDogTrans.KennelWarrantFee = 0;
				CurDogTrans.LateFee = 0;
				CurDogTrans.RabiesTagNumber = "";
				CurDogTrans.ReplacementLicenseFee = 0;
				CurDogTrans.ReplacementStickerFee = 0;
				CurDogTrans.ReplacementTagFee = 0;
				CurDogTrans.SpayNonSpayAmount = 0;
				CurDogTrans.StateFee = 0;
				CurDogTrans.StickerNumber = 0;
				CurDogTrans.TAGNUMBER = "";
				CurDogTrans.TempLicenseFee = 0;
				CurDogTrans.TownFee = 0;
				CurDogTrans.TransactionNumber = 0;
				CurDogTrans.TransferLicenseFee = 0;
				CurDogTrans.WarrantFee = 0;
				CurDogTrans.DogYear = 0;
				rsLoad.OpenRecordset("select * from dogtransactions where ID = " + FCConvert.ToString(lngTransactionID), "twck0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					t2kTransactionDate.Text = Strings.Format(rsLoad.Get_Fields("transactiondate"), "MM/dd/yyyy");
					if (!rsLoad.Get_Fields_Boolean("boolkennel"))
					{
						txtDogYear.Text = FCConvert.ToString(rsLoad.Get_Fields("dogyear"));
						if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("boollicense")))
						{
							chkLicense.CheckState = Wisej.Web.CheckState.Checked;
							if (rsLoad.Get_Fields_String("tagnumber") != rsLoad.Get_Fields_String("oldtagnumber") && (rsLoad.Get_Fields_String("oldtagnumber") == "" || rsLoad.Get_Fields_String("oldtagnumber") == "0") && !rsLoad.Get_Fields_Boolean("boolReplacementTag"))
							{
								chkNewTag.CheckState = Wisej.Web.CheckState.Checked;
							}
							else
							{
								chkNewTag.CheckState = Wisej.Web.CheckState.Unchecked;
							}
							lblLicenseAmount.Text = rsLoad.Get_Fields_Double("spaynonspayamount").FormatAsMoney();
							CurDogTrans.SpayNonSpayAmount = rsLoad.Get_Fields_Double("spaynonspayamount");
						}
						else
						{
							chkLicense.CheckState = Wisej.Web.CheckState.Unchecked;
							chkNewTag.CheckState = Wisej.Web.CheckState.Unchecked;
							lblLicenseAmount.Text = "0.00";
						}
						if (rsLoad.Get_Fields_Boolean("boolspayneuter"))
						{
							chkNeuterSpay.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkNeuterSpay.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (rsLoad.Get_Fields_Boolean("boolsearchrescue"))
						{
							chkSearchRescue.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkSearchRescue.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (rsLoad.Get_Fields_Boolean("boolHearingGuid"))
						{
							chkHearingGuid.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkHearingGuid.CheckState = Wisej.Web.CheckState.Unchecked;
						}

                        if (rsLoad.Get_Fields_Boolean("NuisanceDog"))
                        {
                            chkNuisance.CheckState = CheckState.Checked;
                        }
                        else
                        {
                            chkNuisance.CheckState = CheckState.Unchecked;
                        }

                        if (rsLoad.Get_Fields_Boolean("DangerousDog"))
                        {
                            chkDangerous.CheckState = CheckState.Checked;
                        }
                        else
                        {
                            chkDangerous.CheckState = CheckState.Unchecked;
                        }
						if (rsLoad.Get_Fields_Double("latefee") > 0)
						{
							if (rsLoad.Get_Fields_Double("Warrantfee") > 0)
							{
								lblWarrantAmount.Text = rsLoad.Get_Fields_Double("warrantfee").FormatAsMoney();
								chkWarrantFee.CheckState = Wisej.Web.CheckState.Checked;
								CurDogTrans.WarrantFee = rsLoad.Get_Fields_Double("warrantfee");
								if (rsLoad.Get_Fields_Double("latefee") > rsLoad.Get_Fields_Double("warrantfee"))
								{
									chkLateFee.CheckState = Wisej.Web.CheckState.Checked;
									lblLateBreakdownAmount.Text = (rsLoad.Get_Fields_Double("latefee") - rsLoad.Get_Fields_Double("warrantfee")).FormatAsMoney();
								}
								else
								{
									chkLateFee.CheckState = Wisej.Web.CheckState.Unchecked;
									lblLateBreakdownAmount.Text = "0.00";
								}
							}
							else
							{
								chkWarrantFee.CheckState = Wisej.Web.CheckState.Unchecked;
								chkLateFee.CheckState = Wisej.Web.CheckState.Checked;
                                lblLateBreakdownAmount.Text = rsLoad.Get_Fields_Double("latefee").FormatAsMoney();
                            }

                            lblLateAmount.Text = rsLoad.Get_Fields_Double("latefee").FormatAsMoney();
                        }
						else
						{
							chkLateFee.CheckState = Wisej.Web.CheckState.Unchecked;
							chkWarrantFee.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (rsLoad.Get_Fields_Boolean("boolReplacementTag"))
						{
							chkReplacementTag.CheckState = Wisej.Web.CheckState.Checked;
							lblReplTagAmount.Text = rsLoad.Get_Fields_Double("replacementtagfee").FormatAsMoney();
							CurDogTrans.ReplacementTagFee = rsLoad.Get_Fields_Double("replacementtagfee");
						}
						else
						{
							chkReplacementTag.CheckState = Wisej.Web.CheckState.Unchecked;
							lblReplTagAmount.Text = "0.00";
						}
						if (rsLoad.Get_Fields_Boolean("boolReplacementSticker"))
						{
							chkReplacementSticker.CheckState = Wisej.Web.CheckState.Checked;
							lblReplacementStickerAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields("replacementstickerfee")), "0.00");
							CurDogTrans.ReplacementStickerFee = Conversion.Val(rsLoad.Get_Fields("replacementstickerfee"));
						}
						else
						{
							chkReplacementSticker.CheckState = Wisej.Web.CheckState.Unchecked;
							lblReplacementStickerAmount.Text = "0.00";
						}
						if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("boolReplacementLicense")))
						{
							chkReplacementLicense.CheckState = Wisej.Web.CheckState.Checked;
							lblReplLicenseAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("replacementlicensefee")), "0.00");
							CurDogTrans.ReplacementLicenseFee = Conversion.Val(rsLoad.Get_Fields_Double("replacementlicensefee"));
						}
						else
						{
							chkReplacementLicense.CheckState = Wisej.Web.CheckState.Unchecked;
							lblReplLicenseAmount.Text = "0.00";
						}
						if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("boolTransfer")))
						{
							chkTransferLicense.CheckState = Wisej.Web.CheckState.Checked;
							lblTransferAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("transferlicensefee")), "0.00");
							CurDogTrans.TransferLicenseFee = Conversion.Val(rsLoad.Get_Fields_Double("transferlicensefee"));
						}
						else
						{
							chkTransferLicense.CheckState = Wisej.Web.CheckState.Unchecked;
							lblTransferAmount.Text = "0.00";
						}
						if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("boolTempLicense")))
						{
							chkTempLicense.CheckState = Wisej.Web.CheckState.Checked;
							lblTempLicenseAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("templicensefee")), "0.00");
							CurDogTrans.TempLicenseFee = Conversion.Val(rsLoad.Get_Fields_Double("templicensefee"));
						}
						else
						{
							chkTempLicense.CheckState = Wisej.Web.CheckState.Unchecked;
							lblTempLicenseAmount.Text = "0.00";
						}
						if (Conversion.Val(rsLoad.Get_Fields_Double("AnimalControl")) > 0)
						{
							chkUserDefinedFee.CheckState = Wisej.Web.CheckState.Checked;
							lblUserDefinedAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("animalcontrol")), "0.00");
							CurDogTrans.AnimalControl = Conversion.Val(rsLoad.Get_Fields_Double("animalcontrol"));
						}
						else
						{
							chkUserDefinedFee.CheckState = Wisej.Web.CheckState.Unchecked;
							lblUserDefinedAmount.Text = "0.00";
						}
						lblStateAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("statefee")), "0.00");
						lblClerkAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("clerkfee")), "0.00");
						lblTownAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("townfee")), "0.00");
						lblLateAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields("latefee")), "0.00");
						txtAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields("amount")), "0.00");
						CurDogTrans.DogYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("dogyear"))));
						CurDogTrans.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
						CurDogTrans.StateFee = Conversion.Val(rsLoad.Get_Fields_Double("statefee"));
						CurDogTrans.ClerkFee = Conversion.Val(rsLoad.Get_Fields_Double("clerkfee"));
						CurDogTrans.TownFee = Conversion.Val(rsLoad.Get_Fields_Double("townfee"));
						CurDogTrans.LateFee = Conversion.Val(rsLoad.Get_Fields("latefee"));
						lngOwner = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ownernum"))));
						lngDog = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("dognumber"))));
						CurDogTrans.OwnerNum = lngOwner;
						CurDogTrans.DogNumber = lngDog;
						rsLoad.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDog), "twck0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							lblDogs.Text = FCConvert.ToString(rsLoad.Get_Fields_String("dogname"));
						}
						else
						{
							lblDogs.Text = "";
						}
					}
					else
					{
						txtDogYear.Enabled = false;
						txtDogYear.Text = "";
						Label4.Enabled = false;
						chkHearingGuid.Enabled = false;
						chkNeuterSpay.Enabled = false;
						chkNewTag.Enabled = false;
						chkReplacementLicense.Enabled = false;
						chkReplacementSticker.Enabled = false;
						chkReplacementTag.Enabled = false;
						chkSearchRescue.Enabled = false;
						chkTempLicense.Enabled = false;
						chkTransferLicense.Enabled = false;
						chkUserDefinedFee.Enabled = false;
						lblUserDefinedFee.Enabled = false;
						lblUserDefinedAmount.Enabled = false;
						lblReplacementLicense.Enabled = false;
						lblReplacementSticker.Enabled = false;
						lblReplacementStickerAmount.Enabled = false;
						lblReplacementTag.Enabled = false;
						lblReplLicenseAmount.Enabled = false;
						lblReplTagAmount.Enabled = false;
						lblTempLicense.Enabled = false;
						lblTempLicenseAmount.Enabled = false;
						lblTransferAmount.Enabled = false;
						lblTransferLicense.Enabled = false;
						boolKennel = true;
						lblStateAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("statefee")), "0.00");
						lblClerkAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("clerkfee")), "0.00");
						lblTownAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("townfee")), "0.00");
						lblLateAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields("latefee")), "0.00");
						txtAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields("amount")), "0.00");
						CurDogTrans.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
						CurDogTrans.StateFee = Conversion.Val(rsLoad.Get_Fields_Double("statefee"));
						CurDogTrans.ClerkFee = Conversion.Val(rsLoad.Get_Fields_Double("clerkfee"));
						CurDogTrans.TownFee = Conversion.Val(rsLoad.Get_Fields_Double("townfee"));
						CurDogTrans.LateFee = Conversion.Val(rsLoad.Get_Fields("latefee"));
						if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("boolkennel")))
						{
							chkLicense.CheckState = Wisej.Web.CheckState.Checked;
							lblLicenseAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("kennellic1fee")), "0.00");
						}
						else
						{
							chkLicense.CheckState = Wisej.Web.CheckState.Unchecked;
							lblLicenseAmount.Text = "0.00";
						}
						if (Conversion.Val(rsLoad.Get_Fields_Double("kennelwarrantfee")) > 0)
						{
							chkWarrantFee.CheckState = Wisej.Web.CheckState.Checked;
							lblWarrantAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("kennelwarrantfee")), "0.00");
						}
						else
						{
							chkWarrantFee.CheckState = Wisej.Web.CheckState.Unchecked;
							lblWarrantAmount.Text = "0.00";
						}
						if (Conversion.Val(rsLoad.Get_Fields_Double("kennellatefee")) > 0)
						{
							chkLateFee.CheckState = Wisej.Web.CheckState.Checked;
							lblLateBreakdownAmount.Text = Strings.Format(Conversion.Val(rsLoad.Get_Fields_Double("kennellatefee")), "0.00");
						}
						else
						{
							chkLateFee.CheckState = Wisej.Web.CheckState.Unchecked;
							lblLateBreakdownAmount.Text = "0.00";
						}
						lngOwner = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ownernum"))));
						CurDogTrans.OwnerNum = lngOwner;
						strDogs = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("kenneldogs")));
						if (strDogs != string.Empty)
						{
							strAry = Strings.Split(strDogs, ",", -1, CompareConstants.vbTextCompare);
							strDogs = "";
							for (lngDog = 0; lngDog <= Information.UBound(strAry, 1); lngDog++)
							{
								rsLoad.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[lngDog])), "twck0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									strDogs += rsLoad.Get_Fields_String("dogname") + ", ";
								}
							}
							// lngDog
							if (strDogs != string.Empty)
							{
								// take off the last ", "
								strDogs = Strings.Mid(strDogs, 1, strDogs.Length - 2);
							}
							lblDogs.Text = strDogs;
						}
						else
						{
							lblDogs.Text = "";
						}
					}
					rsLoad.OpenRecordset("select * from dogowner where ID = " + FCConvert.ToString(lngOwner), "twck0000.vb1");
					if (!rsLoad.EndOfFile())
					{
						lblName.Text = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("firstname") + " " + rsLoad.Get_Fields_String("lastname") + " " + rsLoad.Get_Fields_String("desig"));
					}
					else
					{
						lblName.Text = "";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void RecalcAmount()
		{
			double dblAmount;
			double dblAmount1;
			double dblAmount2;
			double dblamount3;
			double dblamount4;
			double dblamount5;
			double dblamount6;
			double dblTemp;
			double dblState = 0;
			double dblTown = 0;
			double dblClerk = 0;
			double dblNeuterFee;
			double dblNonNeuterFee;
			double dblLateFee;
			double dblWarrantFee;
			double dblTransferFee;
			double dblReplacementFee;
			double dblReplacementTagFee;
			double dblReplacementStickerFee;
			double dblTempLicenseFee;
			double dblUserDefinedFee1;
            bool isDangerous = false;
            bool isNuisance = false;

			if (boolLoading)
				return;
			dblNeuterFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Fixed_Fee);
			dblLateFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Late_Fee);
			dblWarrantFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Warrant_Fee);
			dblNonNeuterFee = FCConvert.ToDouble(modDogs.Statics.DogFee.UN_Fixed_FEE);
			dblTransferFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Transfer_Fee);
			dblReplacementFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Replacement_Fee);
			dblReplacementTagFee = FCConvert.ToDouble(modDogs.Statics.DogFee.DogTagFee);
			dblReplacementStickerFee = 0;
			dblTempLicenseFee = 0;
			dblUserDefinedFee1 = modDogs.Statics.DogFee.UserDefinedFee1;
			dblAmount = 0;
			dblAmount1 = 0;
			dblAmount2 = 0;
			dblamount3 = 0;
			dblamount4 = 0;
			dblamount5 = 0;
			dblamount6 = 0;

            isNuisance = chkNuisance.CheckState == CheckState.Checked;
            isDangerous = chkDangerous.CheckState == CheckState.Checked;

			CurDogTrans.Amount = 0;
			CurDogTrans.ID = 0;
			CurDogTrans.boolAdjustmentVoid = false;
			CurDogTrans.boolHearingGuid = false;
			CurDogTrans.boolLicense = false;
			CurDogTrans.boolKennel = false;
			CurDogTrans.boolReplacementLicense = false;
			CurDogTrans.boolReplacementSticker = false;
			CurDogTrans.boolReplacementTag = false;
			CurDogTrans.boolSearchRescue = false;
			CurDogTrans.boolSpayNeuter = false;
			CurDogTrans.boolTempLicense = false;
			CurDogTrans.boolTransfer = false;
			CurDogTrans.ClerkFee = 0;
			CurDogTrans.Description = "";
            CurDogTrans.IsDangerous = isDangerous;
            CurDogTrans.IsNuisance = isNuisance;
			CurDogTrans.KennelDogs = "";
			CurDogTrans.KennelLateFee = 0;
			CurDogTrans.KennelLic1Fee = 0;
			CurDogTrans.KennelLic1Num = 0;
			CurDogTrans.KennelLic2Fee = 0;
			CurDogTrans.KennelLic2Num = 0;
			CurDogTrans.KennelLicenseID = 0;
			CurDogTrans.KennelWarrantFee = 0;
			CurDogTrans.LateFee = 0;
			CurDogTrans.RabiesTagNumber = "";
			CurDogTrans.ReplacementLicenseFee = 0;
			CurDogTrans.ReplacementStickerFee = 0;
			CurDogTrans.ReplacementTagFee = 0;
			CurDogTrans.SpayNonSpayAmount = 0;
			CurDogTrans.StateFee = 0;
			CurDogTrans.StickerNumber = 0;
			CurDogTrans.TAGNUMBER = "";
			CurDogTrans.TempLicenseFee = 0;
			CurDogTrans.TownFee = 0;
			CurDogTrans.TransactionNumber = 0;
			CurDogTrans.TransferLicenseFee = 0;
			CurDogTrans.WarrantFee = 0;
			CurDogTrans.DogYear = 0;
			// clear the values
			//if (boolKennel!=1) {
			if (true)
			{
				CurDogTrans.DogYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtDogYear.Text)));
				if (!(chkHearingGuid.CheckState == Wisej.Web.CheckState.Checked || chkSearchRescue.CheckState == Wisej.Web.CheckState.Checked) || isNuisance || isDangerous )
				{
					if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (chkNeuterSpay.CheckState == CheckState.Checked)
						{
							dblClerk = modDogs.Statics.DogFee.FixedClerk;
							dblTemp = dblNeuterFee;
							dblState = modDogs.Statics.DogFee.FixedState;
							lblLicenseAmount.Text = Strings.Format(dblNeuterFee, "0.00");
							CurDogTrans.SpayNonSpayAmount = dblNeuterFee;
							dblTown = modDogs.Statics.DogFee.FixedTown;
						}
						else
						{
							dblClerk = modDogs.Statics.DogFee.DogClerk;
							dblTemp = dblNonNeuterFee;
							dblState = modDogs.Statics.DogFee.DogState;
							lblLicenseAmount.Text = Strings.Format(dblNonNeuterFee, "0.00");
							CurDogTrans.SpayNonSpayAmount = dblNonNeuterFee;
							dblTown = modDogs.Statics.DogFee.DogTown;
						}

                        if (isDangerous)
                        {
                            dblClerk = modDogs.Statics.DogFee.DangerousClerkFee;
                            dblState = modDogs.Statics.DogFee.DangerousStateFee;
                            dblTown = modDogs.Statics.DogFee.DangerousTownFee;
                            dblTemp = dblClerk + dblState + dblTown;
                            lblLicenseAmount.Text = dblTemp.FormatAsMoney();
                            CurDogTrans.SpayNonSpayAmount = dblTemp;
                        }
                        else if (isNuisance)
                        {
                            dblClerk = modDogs.Statics.DogFee.NuisanceClerkFee;
                            dblState = modDogs.Statics.DogFee.NuisanceStateFee;
                            dblTown = modDogs.Statics.DogFee.NuisanceTownFee;
                            dblTemp = dblClerk + dblState + dblTown;
                            lblLicenseAmount.Text = dblTemp.FormatAsMoney();
                            CurDogTrans.SpayNonSpayAmount = dblTemp;
                        }
						dblAmount2 += dblTown;
						dblamount3 += dblState;
						dblamount4 += dblClerk;
					}
					else
					{
						lblLicenseAmount.Text = "0.00";
					}
					if (chkLateFee.CheckState == Wisej.Web.CheckState.Checked)
					{
                        if (isDangerous)
                        {
                            dblamount5 = dblamount5 + modDogs.Statics.DogFee.DangerousLateFee;
                            lblLateBreakdownAmount.Text = modDogs.Statics.DogFee.DangerousLateFee.FormatAsMoney();
                        }
                        else if (isNuisance)
                        {
                            dblamount5 = dblamount5 + modDogs.Statics.DogFee.NuisanceLateFee;
                            lblLateBreakdownAmount.Text = modDogs.Statics.DogFee.NuisanceLateFee.FormatAsMoney();
                        }
                        else
                        {
                            dblamount5 += FCConvert.ToDouble(modDogs.Statics.DogFee.Late_Fee);
                            lblLateBreakdownAmount.Text = modDogs.Statics.DogFee.Late_Fee.FormatAsMoney();
                        }
                    }
					else
					{
						lblLateBreakdownAmount.Text = "0.00";
					}
					if (chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked)
					{
						dblamount5 += FCConvert.ToDouble(modDogs.Statics.DogFee.Warrant_Fee);
						lblWarrantAmount.Text = Strings.Format(modDogs.Statics.DogFee.Warrant_Fee, "0.00");
						CurDogTrans.WarrantFee = FCConvert.ToDouble(modDogs.Statics.DogFee.Warrant_Fee);
					}
					else
					{
						lblWarrantAmount.Text = "0.00";
					}
					if (chkReplacementLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						dblamount4 += dblReplacementFee;
						// goes to clerk fee
						lblReplLicenseAmount.Text = Strings.Format(dblReplacementFee, "0.00");
						CurDogTrans.ReplacementLicenseFee = dblReplacementFee;
					}
					else
					{
						lblReplLicenseAmount.Text = "0.00";
					}
					if (chkReplacementTag.CheckState == Wisej.Web.CheckState.Checked)
					{
						dblamount4 += dblReplacementTagFee;
						lblReplTagAmount.Text = Strings.Format(dblReplacementTagFee, "0.00");
						CurDogTrans.ReplacementTagFee = dblReplacementTagFee;
					}
					else
					{
						lblReplTagAmount.Text = "0.00";
					}
					if (chkReplacementSticker.CheckState == Wisej.Web.CheckState.Checked)
					{
						dblamount4 += dblReplacementStickerFee;
						lblReplacementStickerAmount.Text = Strings.Format(dblReplacementStickerFee, "0.00");
						CurDogTrans.ReplacementLicenseFee = dblReplacementStickerFee;
					}
					else
					{
						lblReplacementStickerAmount.Text = "0.00";
					}
					if (chkTempLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						dblamount4 += dblTempLicenseFee;
						lblTempLicenseAmount.Text = Strings.Format(dblTempLicenseFee, "0.00");
						CurDogTrans.TempLicenseFee = dblTempLicenseFee;
					}
					else
					{
						lblTempLicenseAmount.Text = "0.00";
					}
					if (chkTransferLicense.CheckState == Wisej.Web.CheckState.Checked)
					{
						dblamount4 += dblTransferFee;
						lblTransferAmount.Text = Strings.Format(dblTransferFee, "0.00");
						CurDogTrans.TransferLicenseFee = dblTransferFee;
					}
					else
					{
						lblTransferAmount.Text = "0.00";
					}
				}
				else
				{
					lblLicenseAmount.Text = "0.00";
					lblLateBreakdownAmount.Text = "0.00";
					lblWarrantAmount.Text = "0.00";
					lblReplLicenseAmount.Text = "0.00";
					lblReplacementStickerAmount.Text = "0.00";
					lblReplTagAmount.Text = "0.00";
					lblReplLicenseAmount.Text = "0.00";
					lblTempLicenseAmount.Text = "0.00";
					lblTransferAmount.Text = "0.00";
				}
			}
			else
			{
				clsDRWrapper clsKennel = new clsDRWrapper();
				double dblClerkFee = 0;
				double dblStateFee = 0;
				double dblTownFee = 0;
				double dblKennelFee = 0;
				double dblKennelWarrantFee = 0;
				double dblKennelLateFee = 0;
				dblClerkFee = 0;
				dblStateFee = 0;
				dblTownFee = 0;
				dblLateFee = 0;
				lblLicenseAmount.Text = "0.00";
				lblLateBreakdownAmount.Text = "0.00";
				lblWarrantAmount.Text = "0.00";
				lblReplLicenseAmount.Text = "0.00";
				lblReplacementStickerAmount.Text = "0.00";
				lblReplTagAmount.Text = "0.00";
				lblReplLicenseAmount.Text = "0.00";
				lblTempLicenseAmount.Text = "0.00";
				lblTransferAmount.Text = "0.00";
				modDogs.GetDogFees();
				dblKennelFee = modDogs.Statics.DogFee.KennelClerk + modDogs.Statics.DogFee.KennelState + modDogs.Statics.DogFee.KennelTown;
				dblKennelWarrantFee = modDogs.Statics.DogFee.KennelWarrantFee;
				dblKennelLateFee = modDogs.Statics.DogFee.KennelLateFee;
				if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
				{
					dblClerkFee = modDogs.Statics.DogFee.KennelClerk;
					dblStateFee = modDogs.Statics.DogFee.KennelState;
					dblTownFee = modDogs.Statics.DogFee.KennelTown;
					CurDogTrans.KennelLic1Fee = dblKennelFee;
					CurDogTrans.boolLicense = true;
					lblLicenseAmount.Text = Strings.Format(dblKennelFee, "0.00");
				}
				else
				{
					lblLicenseAmount.Text = "0.00";
				}
				if (chkLateFee.CheckState == Wisej.Web.CheckState.Checked)
				{
					dblLateFee += dblKennelLateFee;
					CurDogTrans.KennelLateFee = dblKennelLateFee;
					lblLateBreakdownAmount.Text = Strings.Format(dblKennelLateFee, "0.00");
				}
				else
				{
					lblLateBreakdownAmount.Text = "0.00";
				}
				if (chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked)
				{
					dblLateFee += dblKennelWarrantFee;
					CurDogTrans.KennelWarrantFee = dblKennelWarrantFee;
					lblWarrantAmount.Text = Strings.Format(dblKennelWarrantFee, "0.00");
				}
				else
				{
					lblWarrantAmount.Text = "0.00";
				}
				dblAmount2 = dblTownFee;
				dblamount3 = dblStateFee;
				dblamount4 = dblClerkFee;
				dblamount5 = dblLateFee;
				dblAmount = dblAmount1 + dblAmount2 + dblamount3 + dblamount4 + dblamount5 + dblamount6;
				// CurTrans.Amount1 = dblAmount1
				// CurTrans.Amount2 = dblAmount2
				// CurTrans.Amount3 = dblamount3
				// CurTrans.Amount4 = dblamount4
				// CurTrans.Amount5 = dblamount5
				// CurTrans.Amount6 = dblamount6
				// CurTrans.TotalAmount = dblAmount
				CurDogTrans.Amount = dblAmount;
				CurDogTrans.ClerkFee = dblamount4;
				CurDogTrans.StateFee = dblamount3;
				CurDogTrans.TownFee = dblAmount2;
				CurDogTrans.LateFee = dblamount5;
				CurDogTrans.Amount = dblAmount;
			}
			if (chkUserDefinedFee.CheckState == Wisej.Web.CheckState.Checked)
			{
				dblamount6 += dblUserDefinedFee1;
			}
			dblAmount = dblAmount1 + dblAmount2 + dblamount3 + dblamount4 + dblamount5 + dblamount6;
			CurDogTrans.Amount = dblAmount;
			CurDogTrans.AnimalControl = dblamount6;
			CurDogTrans.ClerkFee = dblamount4;
			CurDogTrans.TownFee = dblAmount2;
			CurDogTrans.LateFee = dblamount5;
			CurDogTrans.StateFee = dblamount3;
			lblClerkAmount.Text = Strings.Format(dblamount4, "0.00");
			lblStateAmount.Text = Strings.Format(dblamount3, "0.00");
			lblTownAmount.Text = Strings.Format(dblAmount2, "0.00");
			lblLateAmount.Text = Strings.Format(dblamount5, "0.00");
			lblUserDefinedAmount.Text = Strings.Format(dblamount6, "0.00");
			txtAmount.Text = Strings.Format(dblAmount, "0.00");
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			// VB6 Bad Scope Dim:
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				rsSave.OpenRecordset("select * from dogtransactions where ID = " + FCConvert.ToString(lngTransactionID), "twck0000.vb1");
				if (!rsSave.EndOfFile())
				{
					modGlobalFunctions.AddCYAEntry("CK", "Edited dog transaction", "ID " + FCConvert.ToString(lngTransactionID), "", "", "");
					rsSave.Edit();
					//if (boolKennel!=1) {
					if (!boolKennel)
					{
						if (chkHearingGuid.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("BOOLHearingGuid", true);
						}
						else
						{
							rsSave.Set_Fields("boolHearingGuid", false);
						}
						if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolLicense", true);
						}
						else
						{
							rsSave.Set_Fields("boolLicense", false);
						}
						if (chkNeuterSpay.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolSpayNeuter", true);
						}
						else
						{
							rsSave.Set_Fields("boolSpayNeuter", false);
						}
						if (chkNewTag.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("oldTagNumber", 0);
						}
						else
						{
							if (Conversion.Val(rsSave.Get_Fields_Int32("oldtagnumber")) == 0)
							{
								rsSave.Set_Fields("oldtagnumber", rsSave.Get_Fields("tagnumber"));
							}
						}
						if (chkReplacementLicense.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolReplacementLicense", true);
						}
						else
						{
							rsSave.Set_Fields("boolReplacementLicense", false);
						}
						if (chkReplacementSticker.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolReplacementSticker", true);
						}
						else
						{
							rsSave.Set_Fields("boolReplacementSticker", false);
						}
						if (chkReplacementTag.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolreplacementtag", true);
						}
						else
						{
							rsSave.Set_Fields("boolReplacementtag", false);
						}
						if (chkSearchRescue.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolsearchrescue", true);
						}
						else
						{
							rsSave.Set_Fields("boolsearchrescue", false);
						}
						if (chkTempLicense.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolTempLicense", true);
						}
						else
						{
							rsSave.Set_Fields("boolTempLicense", false);
						}
						if (chkTransferLicense.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolTransfer", true);
						}
						else
						{
							rsSave.Set_Fields("boolTransfer", false);
						}
                        rsSave.Set_Fields("DangerousDog",chkDangerous.CheckState == CheckState.Checked);
                        rsSave.Set_Fields("NuisanceDog", chkNuisance.CheckState == CheckState.Checked);
					}
					else
					{
						if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsSave.Set_Fields("boolLicense", true);
						}
						else
						{
							rsSave.Set_Fields("boolLicense", false);
						}
					}
					rsSave.Set_Fields("dogyear", FCConvert.ToString(Conversion.Val(txtDogYear.Text)));
					rsSave.Set_Fields("amount", CurDogTrans.Amount);
					rsSave.Set_Fields("townfee", CurDogTrans.TownFee);
					rsSave.Set_Fields("clerkfee", CurDogTrans.ClerkFee);
					rsSave.Set_Fields("statefee", CurDogTrans.StateFee);
					rsSave.Set_Fields("latefee", CurDogTrans.LateFee);
					rsSave.Set_Fields("animalcontrol", CurDogTrans.AnimalControl);
					rsSave.Set_Fields("ReplacementLicenseFee", CurDogTrans.ReplacementLicenseFee);
					rsSave.Set_Fields("replacementStickerFee", CurDogTrans.ReplacementStickerFee);
					rsSave.Set_Fields("replacementtagfee", CurDogTrans.ReplacementTagFee);
					rsSave.Set_Fields("SpayNonSpayamount", CurDogTrans.SpayNonSpayAmount);
					rsSave.Set_Fields("TempLicenseFee", CurDogTrans.TempLicenseFee);
					rsSave.Set_Fields("transferlicensefee", CurDogTrans.TransferLicenseFee);
					rsSave.Set_Fields("warrantfee", CurDogTrans.WarrantFee);
					rsSave.Set_Fields("KennelLateFee", CurDogTrans.KennelLateFee);
					rsSave.Set_Fields("KennelLic1Fee", CurDogTrans.KennelLic1Fee);
					rsSave.Set_Fields("KennelWarrantFee", CurDogTrans.KennelWarrantFee);
					if (Information.IsDate(t2kTransactionDate.Text))
					{
						rsSave.Set_Fields("transactiondate", t2kTransactionDate.Text);
						//if (boolKennel!=1) {
						if (true)
						{
							if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
							{
								rsLoad.OpenRecordset("Select rabstickerissue from doginfo where ID = " + FCConvert.ToString(CurDogTrans.DogNumber), "twck0000.vb1");
								if (!rsLoad.EndOfFile())
								{
									if (Information.IsDate(rsLoad.Get_Fields("rabstickerissue")))
									{
										if (fecherFoundation.DateAndTime.DateDiff("d", (DateTime)rsLoad.Get_Fields_DateTime("rabstickerissue"), FCConvert.ToDateTime(t2kTransactionDate.Text)) != 0)
										{
											if (MessageBox.Show("Transaction date is different than the issue date in the dog record" + "\r\n" + "Do you want to make the issue date in the dog record match?", "Update Issue Date?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
											{
												rsLoad.Edit();
												rsLoad.Set_Fields("rabstickerissue", t2kTransactionDate.Text);
												rsLoad.Update();
											}
										}
									}
								}
							}
						}
						rsLoad.OpenRecordset("select * from transactiontable where ID = " + rsSave.Get_Fields_Int32("transactionnumber"), "twck0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							rsLoad.Edit();
							rsLoad.Set_Fields("transactiondate", t2kTransactionDate.Text);
							rsLoad.Update();
						}
					}
					rsSave.Update();
				}
				SaveInfo = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}
    }
}
