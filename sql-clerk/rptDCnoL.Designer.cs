﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDCnoL.
	/// </summary>
	partial class rptDCnoL
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDCnoL));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fCauseA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fCauseB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fCauseC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fCauseD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAmended = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDecedentStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDecedentCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedentStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedentCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Field17,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Field18,
				this.Field19,
				this.Field21,
				this.Field22,
				this.fCauseA,
				this.fCauseB,
				this.fCauseC,
				this.fCauseD,
				this.Label2,
				this.Label3,
				this.Label4,
				this.txtAmended,
				this.txtDecedentStreet,
				this.txtDecedentCity,
				this.Field23,
				this.Field24,
				this.Image1,
				this.Field20
			});
			this.Detail.Height = 10.41667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.875F;
			this.Label1.MultiLine = false;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label1.Text = "a.";
			this.Label1.Top = 5.930555F;
			this.Label1.Width = 0.3125F;
			// 
			// Field17
			// 
			this.Field17.CanGrow = false;
			this.Field17.Height = 0.2916667F;
			this.Field17.Left = 2.1875F;
			this.Field17.Name = "Field17";
			this.Field17.Text = "Field17";
			this.Field17.Top = 8.75F;
			this.Field17.Width = 2.625F;
			// 
			// Field1
			// 
			this.Field1.CanGrow = false;
			this.Field1.Height = 0.1666667F;
			this.Field1.Left = 2F;
			this.Field1.Name = "Field1";
			this.Field1.Text = null;
			this.Field1.Top = 2.8125F;
			this.Field1.Width = 3.25F;
			// 
			// Field2
			// 
			this.Field2.CanGrow = false;
			this.Field2.Height = 0.1666667F;
			this.Field2.Left = 6F;
			this.Field2.Name = "Field2";
			this.Field2.Text = null;
			this.Field2.Top = 2.8125F;
			this.Field2.Width = 2.0625F;
			// 
			// Field3
			// 
			this.Field3.CanGrow = false;
			this.Field3.Height = 0.2083333F;
			this.Field3.Left = 2F;
			this.Field3.Name = "Field3";
			this.Field3.Text = null;
			this.Field3.Top = 3.25F;
			this.Field3.Width = 3.25F;
			// 
			// Field4
			// 
			this.Field4.CanGrow = false;
			this.Field4.Height = 0.2083333F;
			this.Field4.Left = 6F;
			this.Field4.Name = "Field4";
			this.Field4.Text = null;
			this.Field4.Top = 3.25F;
			this.Field4.Width = 2.0625F;
			// 
			// Field5
			// 
			this.Field5.CanGrow = false;
			this.Field5.Height = 0.2083333F;
			this.Field5.Left = 2F;
			this.Field5.Name = "Field5";
			this.Field5.Text = null;
			this.Field5.Top = 3.75F;
			this.Field5.Width = 3.25F;
			// 
			// Field6
			// 
			this.Field6.CanGrow = false;
			this.Field6.Height = 0.2083333F;
			this.Field6.Left = 6F;
			this.Field6.Name = "Field6";
			this.Field6.Text = null;
			this.Field6.Top = 3.75F;
			this.Field6.Width = 2F;
			// 
			// Field7
			// 
			this.Field7.CanGrow = false;
			this.Field7.Height = 0.375F;
			this.Field7.Left = 2.0625F;
			this.Field7.Name = "Field7";
			this.Field7.Text = null;
			this.Field7.Top = 4.25F;
			this.Field7.Width = 2.625F;
			// 
			// Field8
			// 
			this.Field8.CanGrow = false;
			this.Field8.Height = 0.375F;
			this.Field8.Left = 4.8125F;
			this.Field8.Name = "Field8";
			this.Field8.Text = null;
			this.Field8.Top = 4.25F;
			this.Field8.Width = 3.1875F;
			// 
			// Field9
			// 
			this.Field9.CanGrow = false;
			this.Field9.Height = 0.3229167F;
			this.Field9.Left = 2.0625F;
			this.Field9.Name = "Field9";
			this.Field9.Text = null;
			this.Field9.Top = 5.083333F;
			this.Field9.Width = 2.125F;
			// 
			// Field10
			// 
			this.Field10.CanGrow = false;
			this.Field10.Height = 0.3229167F;
			this.Field10.Left = 4.375F;
			this.Field10.Name = "Field10";
			this.Field10.Text = null;
			this.Field10.Top = 5.083333F;
			this.Field10.Width = 3.6875F;
			// 
			// Field12
			// 
			this.Field12.CanGrow = false;
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 2.1875F;
			this.Field12.Name = "Field12";
			this.Field12.Text = "Field12";
			this.Field12.Top = 7.5625F;
			this.Field12.Width = 3.75F;
			// 
			// Field13
			// 
			this.Field13.CanGrow = false;
			this.Field13.Height = 0.3333333F;
			this.Field13.Left = 6.34375F;
			this.Field13.Name = "Field13";
			this.Field13.Text = "Field13";
			this.Field13.Top = 7.583333F;
			this.Field13.Width = 1.625F;
			// 
			// Field14
			// 
			this.Field14.CanGrow = false;
			this.Field14.Height = 0.3333333F;
			this.Field14.Left = 2.1875F;
			this.Field14.Name = "Field14";
			this.Field14.Text = "Field14";
			this.Field14.Top = 8.25F;
			this.Field14.Width = 2.625F;
			// 
			// Field15
			// 
			this.Field15.CanGrow = false;
			this.Field15.Height = 0.3333333F;
			this.Field15.Left = 4.9375F;
			this.Field15.Name = "Field15";
			this.Field15.Text = "Field15";
			this.Field15.Top = 8.25F;
			this.Field15.Width = 1.3125F;
			// 
			// Field16
			// 
			this.Field16.CanGrow = false;
			this.Field16.Height = 0.3333333F;
			this.Field16.Left = 6.25F;
			this.Field16.Name = "Field16";
			this.Field16.Text = "Field16";
			this.Field16.Top = 8.25F;
			this.Field16.Width = 1.8125F;
			// 
			// Field18
			// 
			this.Field18.CanGrow = false;
			this.Field18.Height = 0.2916667F;
			this.Field18.Left = 4.9375F;
			this.Field18.Name = "Field18";
			this.Field18.Text = "Field18";
			this.Field18.Top = 8.75F;
			this.Field18.Width = 1.4375F;
			// 
			// Field19
			// 
			this.Field19.CanGrow = false;
			this.Field19.Height = 0.2916667F;
			this.Field19.Left = 6.4375F;
			this.Field19.Name = "Field19";
			this.Field19.Text = "Field19";
			this.Field19.Top = 8.75F;
			this.Field19.Width = 1.625F;
			// 
			// Field21
			// 
			this.Field21.CanGrow = false;
			this.Field21.Height = 0.3333333F;
			this.Field21.Left = 2.125F;
			this.Field21.Name = "Field21";
			this.Field21.Text = "Field21";
			this.Field21.Top = 9.652778F;
			this.Field21.Width = 2.8125F;
			// 
			// Field22
			// 
			this.Field22.CanGrow = false;
			this.Field22.Height = 0.2083333F;
			this.Field22.Left = 5.53125F;
			this.Field22.Name = "Field22";
			this.Field22.Text = "Field22";
			this.Field22.Top = 9.947917F;
			this.Field22.Width = 2.28125F;
			// 
			// fCauseA
			// 
			this.fCauseA.CanGrow = false;
			this.fCauseA.Height = 0.2083333F;
			this.fCauseA.Left = 2.1875F;
			this.fCauseA.Name = "fCauseA";
			this.fCauseA.Text = "a";
			this.fCauseA.Top = 5.930555F;
			this.fCauseA.Width = 5.4375F;
			// 
			// fCauseB
			// 
			this.fCauseB.CanGrow = false;
			this.fCauseB.Height = 0.1666667F;
			this.fCauseB.Left = 2.1875F;
			this.fCauseB.Name = "fCauseB";
			this.fCauseB.Text = "b";
			this.fCauseB.Top = 6.263889F;
			this.fCauseB.Width = 5.4375F;
			// 
			// fCauseC
			// 
			this.fCauseC.CanGrow = false;
			this.fCauseC.Height = 0.2083333F;
			this.fCauseC.Left = 2.1875F;
			this.fCauseC.Name = "fCauseC";
			this.fCauseC.Text = "c";
			this.fCauseC.Top = 6.555555F;
			this.fCauseC.Width = 5.4375F;
			// 
			// fCauseD
			// 
			this.fCauseD.CanGrow = false;
			this.fCauseD.Height = 0.1666667F;
			this.fCauseD.Left = 2.1875F;
			this.fCauseD.Name = "fCauseD";
			this.fCauseD.Text = "d";
			this.fCauseD.Top = 6.888889F;
			this.fCauseD.Width = 5.4375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.875F;
			this.Label2.MultiLine = false;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label2.Text = "b.";
			this.Label2.Top = 6.263889F;
			this.Label2.Width = 0.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2083333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.875F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label3.Text = "c.";
			this.Label3.Top = 6.555555F;
			this.Label3.Width = 0.3125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.875F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.Label4.Text = "d.";
			this.Label4.Top = 6.888889F;
			this.Label4.Width = 0.3125F;
			// 
			// txtAmended
			// 
			this.txtAmended.CanGrow = false;
			this.txtAmended.Height = 0.625F;
			this.txtAmended.Left = 0.5F;
			this.txtAmended.Name = "txtAmended";
			this.txtAmended.Style = "font-size: 8pt";
			this.txtAmended.Text = null;
			this.txtAmended.Top = 1.875F;
			this.txtAmended.Width = 6.8125F;
			// 
			// txtDecedentStreet
			// 
			this.txtDecedentStreet.CanGrow = false;
			this.txtDecedentStreet.Height = 0.1666667F;
			this.txtDecedentStreet.Left = 2.083333F;
			this.txtDecedentStreet.Name = "txtDecedentStreet";
			this.txtDecedentStreet.Text = "Field23";
			this.txtDecedentStreet.Top = 5.583333F;
			this.txtDecedentStreet.Width = 2.083333F;
			// 
			// txtDecedentCity
			// 
			this.txtDecedentCity.CanGrow = false;
			this.txtDecedentCity.Height = 0.1666667F;
			this.txtDecedentCity.Left = 4.416667F;
			this.txtDecedentCity.Name = "txtDecedentCity";
			this.txtDecedentCity.Text = "Field23";
			this.txtDecedentCity.Top = 5.583333F;
			this.txtDecedentCity.Width = 3.666667F;
			// 
			// Field23
			// 
			this.Field23.CanGrow = false;
			this.Field23.Height = 0.1875F;
			this.Field23.Left = 2.1875F;
			this.Field23.Name = "Field23";
			this.Field23.Text = "Field23";
			this.Field23.Top = 7.75F;
			this.Field23.Width = 3.75F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1875F;
			this.Field24.Left = 7F;
			this.Field24.Name = "Field24";
			this.Field24.Text = "Field24";
			this.Field24.Top = 1.527778F;
			this.Field24.Width = 0.9375F;
			// 
			// Image1
			// 
			this.Image1.Height = 0.25F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = null;
			this.Image1.Left = 5.0625F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.Top = 9.6875F;
			this.Image1.Width = 1.625F;
			// 
			// Field20
			// 
			this.Field20.CanGrow = false;
			this.Field20.Height = 0.2083333F;
			this.Field20.Left = 2.40625F;
			this.Field20.Name = "Field20";
			this.Field20.Text = "Field20";
			this.Field20.Top = 9.951389F;
			this.Field20.Width = 2.875F;
			// 
			// rptDCnoL
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.DefaultPaperSize = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.497222F;
			this.PrintWidth = 8.194445F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedentStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedentCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fCauseA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fCauseB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fCauseC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fCauseD;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDecedentStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDecedentCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
	}
}
