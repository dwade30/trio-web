﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmVoidDog.
	/// </summary>
	partial class frmVoidDog
	{
		public fecherFoundation.FCCheckBox chkRevertInfo;
		public fecherFoundation.FCFrame fraTransactionDate;
		public fecherFoundation.FCButton cmdProcessSave;
		public Global.T2KDateBox txtTransactionDate;
		public fecherFoundation.FCGrid vsTransactions;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.chkRevertInfo = new fecherFoundation.FCCheckBox();
            this.fraTransactionDate = new fecherFoundation.FCFrame();
            this.txtTransactionDate = new Global.T2KDateBox();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.vsTransactions = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRevertInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTransactionDate)).BeginInit();
            this.fraTransactionDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsTransactions)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(610, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraTransactionDate);
            this.ClientArea.Controls.Add(this.chkRevertInfo);
            this.ClientArea.Controls.Add(this.vsTransactions);
            this.ClientArea.Size = new System.Drawing.Size(610, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(610, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(249, 30);
            this.HeaderText.Text = "Void Dog Transaction";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // chkRevertInfo
            // 
            this.chkRevertInfo.Location = new System.Drawing.Point(30, 30);
            this.chkRevertInfo.Name = "chkRevertInfo";
            this.chkRevertInfo.Size = new System.Drawing.Size(145, 24);
            this.chkRevertInfo.TabIndex = 5;
            this.chkRevertInfo.Text = "Revert Information";
            this.ToolTip1.SetToolTip(this.chkRevertInfo, "Will revert registration info (stickers/dates etc) to values prior to this transa" +
        "ction");
            this.chkRevertInfo.Visible = false;
            // 
            // fraTransactionDate
            // 
            this.fraTransactionDate.Controls.Add(this.txtTransactionDate);
            this.fraTransactionDate.Location = new System.Drawing.Point(30, 30);
            this.fraTransactionDate.Name = "fraTransactionDate";
            this.fraTransactionDate.Size = new System.Drawing.Size(243, 90);
            this.fraTransactionDate.Text = "Enter The Transaction Date";
            this.ToolTip1.SetToolTip(this.fraTransactionDate, null);
            // 
            // txtTransactionDate
            // 
            this.txtTransactionDate.Location = new System.Drawing.Point(20, 30);
            this.txtTransactionDate.MaxLength = 10;
            this.txtTransactionDate.Name = "txtTransactionDate";
            this.txtTransactionDate.Size = new System.Drawing.Size(115, 22);
            this.txtTransactionDate.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtTransactionDate, null);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(245, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(103, 48);
            this.cmdProcessSave.TabIndex = 2;
            this.cmdProcessSave.Text = "Process";
            this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // vsTransactions
            // 
            this.vsTransactions.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsTransactions.Cols = 5;
            this.vsTransactions.ExtendLastCol = true;
            this.vsTransactions.Location = new System.Drawing.Point(30, 77);
            this.vsTransactions.Name = "vsTransactions";
            this.vsTransactions.Rows = 1;
            this.vsTransactions.Size = new System.Drawing.Size(552, 420);
            this.vsTransactions.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.vsTransactions, null);
            this.vsTransactions.Visible = false;
            this.vsTransactions.CurrentCellChanged += new System.EventHandler(this.vsTransactions_RowColChange);
            this.vsTransactions.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsTransactions_MouseMoveEvent);
            this.vsTransactions.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vsTransactions_AfterCollapse);
            this.vsTransactions.DoubleClick += new System.EventHandler(this.vsTransactions_DblClick);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmVoidDog
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(610, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmVoidDog";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Void Dog Transaction";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmVoidDog_Load);
            this.Activated += new System.EventHandler(this.frmVoidDog_Activated);
            this.Resize += new System.EventHandler(this.frmVoidDog_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVoidDog_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRevertInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTransactionDate)).EndInit();
            this.fraTransactionDate.ResumeLayout(false);
            this.fraTransactionDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsTransactions)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
    }
}
