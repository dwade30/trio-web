//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmPrintOldVitals : BaseForm
	{
		public frmPrintOldVitals()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintOldVitals InstancePtr
		{
			get
			{
				return (frmPrintOldVitals)Sys.GetInstance(typeof(frmPrintOldVitals));
			}
		}

		protected frmPrintOldVitals _InstancePtr = null;
		//=========================================================
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
			// CallByName MDIParent, "Grid_GotFocus", VbMethod
		}

		private void cmdFind_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp = "";
			modGNBas.Statics.boolGetClerk = true;
			frmListRegistrar.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrRegistrar) == string.Empty)
				return;
			rsTemp.OpenRecordset("Select * from DefaultRegistrarNames where ID = " + modGNBas.Statics.gstrRegistrar, modGNBas.DEFAULTDATABASE);
			if (!rsTemp.EndOfFile())
			{
				strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("FirstName"))) + " ";
				strTemp += (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("MI"))) == string.Empty ? " " : fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("MI"))) + ". ");
				strTemp += fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("LastName"))) + " ";
				strTemp = Strings.Replace(strTemp, "..", ".", 1, -1, CompareConstants.vbTextCompare);
				txtAttest.Text = strTemp;
			}
			txtAttest.Focus();
			modGNBas.Statics.boolGetClerk = false;
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.typOldVitalsReport.AttestBy = txtAttest.Text;
			modGNBas.Statics.typOldVitalsReport.FileNumber = txtFileNum.Text;
			modGNBas.Statics.typOldVitalsReport.pageNumber = txtPageNum.Text;
			modGNBas.Statics.typOldVitalsReport.NameOfClerk1 = txtClerk.Text;
			modGNBas.Statics.typOldVitalsReport.NameOfClerk2 = txtClerk.Text;
			modGNBas.Statics.typOldVitalsReport.TownOfClerk1 = txtTown.Text;
			modGNBas.Statics.typOldVitalsReport.TownOfClerk2 = txtAttestTown.Text;
			modGNBas.Statics.typOldVitalsReport.DateTo = mebToDate.Text;
			modGNBas.Statics.typOldVitalsReport.DateFrom = mebFromDate.Text;
			if (modClerkGeneral.Statics.utPrintInfo.PreView)
			{
				frmReportViewer.InstancePtr.Init(rptOldVitalReport.InstancePtr, showModal: this.Modal);
				//rptOldVitalReport.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			}
			else
			{
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
				rptOldVitalReport.InstancePtr.PrintReport(false);
			}
		}

		private void frmPrintOldVitals_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
			//App.DoEvents();
			//Support.ZOrder(this, 0);
		}

		private void frmPrintOldVitals_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmPrintOldVitals_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPrintOldVitals_Load(object sender, System.EventArgs e)
		{

			string strTemp = "";
			txtAttest.Text = modGNBas.Statics.typOldVitalsReport.AttestBy;
			if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGNBas.Statics.typOldVitalsReport.AttestBy)) == "ADMIN" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGNBas.Statics.typOldVitalsReport.AttestBy)) == "TRIO")
			{
				strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("CKLastClerkName"));
				if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
				{
					txtAttest.Text = strTemp;
					modGNBas.Statics.typOldVitalsReport.AttestBy = strTemp;
				}
				else
				{
					txtAttest.Text = "";
					modGNBas.Statics.typOldVitalsReport.AttestBy = "";
				}
			}
			txtClerk.Text = modGNBas.Statics.typOldVitalsReport.NameOfClerk1;
			txtTown.Text = modGNBas.Statics.typOldVitalsReport.TownOfClerk1;
			txtAttestTown.Text = modGNBas.Statics.typOldVitalsReport.TownOfClerk2;
			txtFileNum.Text = modGNBas.Statics.typOldVitalsReport.FileNumber;
			txtPageNum.Text = modGNBas.Statics.typOldVitalsReport.pageNumber;
			if (modGNBas.Statics.typOldVitalsReport.DateFrom != string.Empty)
			{
				mebFromDate.Text = modGNBas.Statics.typOldVitalsReport.DateFrom;
			}
			mebToDate.Text = modGNBas.Statics.typOldVitalsReport.DateTo;
			//txtNotes.Text = modGNBas.Statics.typOldVitalsReport.Notes;
			modGlobalFunctions.SetFixedSize(this, 1);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGNBas.Statics.gboolPrintPreview = false;
			// CallByName MDIParent, "Grid_GotFocus", VbMethod
		}
	}
}
