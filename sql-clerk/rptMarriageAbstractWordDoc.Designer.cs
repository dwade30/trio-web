﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMarriageAbstractWordDoc.
	/// </summary>
	partial class rptMarriageAbstractWordDoc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMarriageAbstractWordDoc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtF18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtF13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmended = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.FileNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.txtF18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FileNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtF18,
				this.txtF17,
				this.txtF19,
				this.txtF1,
				this.txtF2,
				this.txtF3,
				this.txtF4,
				this.txtF5,
				this.txtF6,
				this.txtF7,
				this.txtF8,
				this.txtF9,
				this.txtF10,
				this.txtF11,
				this.txtF12,
				this.txtF14,
				this.txtF15,
				this.txtF16,
				this.txtF13,
				this.txtAmended,
				this.FileNumber,
				this.SubReport1
			});
			this.Detail.Height = 9.90625F;
			this.Detail.Name = "Detail";
			// 
			// txtF18
			// 
			this.txtF18.CanGrow = false;
			this.txtF18.Height = 0.2083333F;
			this.txtF18.Left = 1.21875F;
			this.txtF18.Name = "txtF18";
			this.txtF18.Text = "Attest";
			this.txtF18.Top = 9.645833F;
			this.txtF18.Visible = false;
			this.txtF18.Width = 2.78125F;
			// 
			// txtF17
			// 
			this.txtF17.CanGrow = false;
			this.txtF17.Height = 0.2083333F;
			this.txtF17.Left = 1.21875F;
			this.txtF17.Name = "txtF17";
			this.txtF17.Text = "Date Issued";
			this.txtF17.Top = 9.104167F;
			this.txtF17.Visible = false;
			this.txtF17.Width = 2.625F;
			// 
			// txtF19
			// 
			this.txtF19.CanGrow = false;
			this.txtF19.Height = 0.2083333F;
			this.txtF19.Left = 4.1875F;
			this.txtF19.Name = "txtF19";
			this.txtF19.Text = "Muni";
			this.txtF19.Top = 9.645833F;
			this.txtF19.Visible = false;
			this.txtF19.Width = 1.96875F;
			// 
			// txtF1
			// 
			this.txtF1.CanGrow = false;
			this.txtF1.Height = 0.3333333F;
			this.txtF1.Left = 0.8125F;
			this.txtF1.Name = "txtF1";
			this.txtF1.Text = "Groom";
			this.txtF1.Top = 2.5625F;
			this.txtF1.Width = 2.375F;
			// 
			// txtF2
			// 
			this.txtF2.CanGrow = false;
			this.txtF2.Height = 0.3333333F;
			this.txtF2.Left = 3.916667F;
			this.txtF2.Name = "txtF2";
			this.txtF2.Text = "Bride";
			this.txtF2.Top = 2.5625F;
			this.txtF2.Width = 2.5625F;
			// 
			// txtF3
			// 
			this.txtF3.CanGrow = false;
			this.txtF3.Height = 0.3333333F;
			this.txtF3.Left = 0.8375F;
			this.txtF3.Name = "txtF3";
			this.txtF3.Text = "Residence Groom";
			this.txtF3.Top = 3.166667F;
			this.txtF3.Width = 2.375F;
			// 
			// txtF4
			// 
			this.txtF4.CanGrow = false;
			this.txtF4.Height = 0.3333333F;
			this.txtF4.Left = 3.916667F;
			this.txtF4.Name = "txtF4";
			this.txtF4.Text = "Residence Bride";
			this.txtF4.Top = 3.166667F;
			this.txtF4.Width = 2F;
			// 
			// txtF5
			// 
			this.txtF5.CanGrow = false;
			this.txtF5.Height = 0.1875F;
			this.txtF5.Left = 0.8125F;
			this.txtF5.Name = "txtF5";
			this.txtF5.Text = "Age";
			this.txtF5.Top = 3.729167F;
			this.txtF5.Width = 0.5F;
			// 
			// txtF6
			// 
			this.txtF6.CanGrow = false;
			this.txtF6.Height = 0.5416667F;
			this.txtF6.Left = 2.1875F;
			this.txtF6.Name = "txtF6";
			this.txtF6.Text = "Town of Birth";
			this.txtF6.Top = 3.729167F;
			this.txtF6.Width = 1.5625F;
			// 
			// txtF7
			// 
			this.txtF7.CanGrow = false;
			this.txtF7.Height = 0.1875F;
			this.txtF7.Left = 3.916667F;
			this.txtF7.Name = "txtF7";
			this.txtF7.Text = "Age";
			this.txtF7.Top = 3.729167F;
			this.txtF7.Width = 0.5F;
			// 
			// txtF8
			// 
			this.txtF8.CanGrow = false;
			this.txtF8.Height = 0.5416667F;
			this.txtF8.Left = 4.9375F;
			this.txtF8.Name = "txtF8";
			this.txtF8.Text = "Town of Birth";
			this.txtF8.Top = 3.729167F;
			this.txtF8.Width = 2.375F;
			// 
			// txtF9
			// 
			this.txtF9.CanGrow = false;
			this.txtF9.Height = 0.2083333F;
			this.txtF9.Left = 0.8125F;
			this.txtF9.Name = "txtF9";
			this.txtF9.Text = "Date of Intentions";
			this.txtF9.Top = 4.604167F;
			this.txtF9.Width = 2.833333F;
			// 
			// txtF10
			// 
			this.txtF10.CanGrow = false;
			this.txtF10.Height = 0.3333333F;
			this.txtF10.Left = 3.916667F;
			this.txtF10.Name = "txtF10";
			this.txtF10.Text = "Date of Marriage";
			this.txtF10.Top = 4.604167F;
			this.txtF10.Width = 2.78125F;
			// 
			// txtF11
			// 
			this.txtF11.CanGrow = false;
			this.txtF11.Height = 0.2083333F;
			this.txtF11.Left = 0.84375F;
			this.txtF11.Name = "txtF11";
			this.txtF11.Text = "Official Marrier";
			this.txtF11.Top = 5.125F;
			this.txtF11.Width = 2.4375F;
			// 
			// txtF12
			// 
			this.txtF12.CanGrow = false;
			this.txtF12.Height = 0.4583333F;
			this.txtF12.Left = 3.916667F;
			this.txtF12.Name = "txtF12";
			this.txtF12.Text = "Official Marrier\'s Title";
			this.txtF12.Top = 5.104167F;
			this.txtF12.Width = 3.364583F;
			// 
			// txtF14
			// 
			this.txtF14.CanGrow = false;
			this.txtF14.Height = 0.5416667F;
			this.txtF14.Left = 0.78125F;
			this.txtF14.Name = "txtF14";
			this.txtF14.Text = "Clerk Name";
			this.txtF14.Top = 6.583333F;
			this.txtF14.Width = 2.96875F;
			// 
			// txtF15
			// 
			this.txtF15.CanGrow = false;
			this.txtF15.Height = 0.5416667F;
			this.txtF15.Left = 3.90625F;
			this.txtF15.Name = "txtF15";
			this.txtF15.Text = "CityName";
			this.txtF15.Top = 6.583333F;
			this.txtF15.Width = 1.40625F;
			// 
			// txtF16
			// 
			this.txtF16.CanGrow = false;
			this.txtF16.Height = 0.5416667F;
			this.txtF16.Left = 5.40625F;
			this.txtF16.Name = "txtF16";
			this.txtF16.Text = "Date";
			this.txtF16.Top = 6.583333F;
			this.txtF16.Width = 1.604167F;
			// 
			// txtF13
			// 
			this.txtF13.CanGrow = false;
			this.txtF13.Height = 0.5F;
			this.txtF13.Left = 0.8125F;
			this.txtF13.Name = "txtF13";
			this.txtF13.Text = "Place of Marriage";
			this.txtF13.Top = 5.666667F;
			this.txtF13.Width = 2.875F;
			// 
			// txtAmended
			// 
			this.txtAmended.CanGrow = false;
			this.txtAmended.Height = 0.4375F;
			this.txtAmended.Left = 0.5F;
			this.txtAmended.Name = "txtAmended";
			this.txtAmended.Style = "font-size: 8pt";
			this.txtAmended.Text = "Amended Info";
			this.txtAmended.Top = 1.3125F;
			this.txtAmended.Width = 6.8125F;
			// 
			// FileNumber
			// 
			this.FileNumber.CanGrow = false;
			this.FileNumber.DataField = "FileNumber";
			this.FileNumber.Height = 0.1875F;
			this.FileNumber.Left = 5.875F;
			this.FileNumber.Name = "FileNumber";
			this.FileNumber.OutputFormat = resources.GetString("FileNumber.OutputFormat");
			this.FileNumber.Text = "File Number";
			this.FileNumber.Top = 0.75F;
			this.FileNumber.Width = 1.0625F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0.25F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 9.104167F;
			this.SubReport1.Width = 7F;
			// 
			// rptMarriageAbstractWordDoc
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtF18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtF13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FileNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtF13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox FileNumber;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
	}
}
