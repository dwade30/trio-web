﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptOldVitalReport.
	/// </summary>
	public partial class rptOldVitalReport : BaseSectionReport
	{
		public rptOldVitalReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Old Vital Records";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptOldVitalReport InstancePtr
		{
			get
			{
				return (rptOldVitalReport)Sys.GetInstance(typeof(rptOldVitalReport));
			}
		}

		protected rptOldVitalReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOldVitalReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted = this.Document.Printer.DeviceCopies;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (boolPrinted)
			{
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
			}
			else
			{
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 0;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtDate.Text = FCConvert.ToString(modDateRoutines.LongDate(DateTime.Today));
			txtFileNumber.Text = modGNBas.Statics.typOldVitalsReport.FileNumber;
			txtPageNumber.Text = modGNBas.Statics.typOldVitalsReport.pageNumber;
			txtFamilyName.Text = modGNBas.Statics.typOldVitalsReport.FamilyName;
			txtChild1.Text = modGNBas.Statics.typOldVitalsReport.ChildName1;
			txtChild2.Text = modGNBas.Statics.typOldVitalsReport.ChildName2;
			txtChild3.Text = modGNBas.Statics.typOldVitalsReport.ChildName3;
			txtDateOfEvent.Text = modGNBas.Statics.typOldVitalsReport.DateOfEvent;
			txtParents1.Text = modGNBas.Statics.typOldVitalsReport.Parent1;
			txtParents2.Text = modGNBas.Statics.typOldVitalsReport.Parent2;
			txtParents3.Text = modGNBas.Statics.typOldVitalsReport.Parent3;
			txtParents4.Text = modGNBas.Statics.typOldVitalsReport.Parent4;
			txtPlaceOfEvent.Text = modGNBas.Statics.typOldVitalsReport.PlaceOfEvent;
			txtPlaceOfResidence.Text = modGNBas.Statics.typOldVitalsReport.PlaceOfResidence;
			txtNameOfClerk.Text = modGNBas.Statics.typOldVitalsReport.NameOfClerk1;
			// txtNameOfClerk2 = .NameOfClerk2
			txtNameOfClerk2.Text = modGNBas.Statics.typOldVitalsReport.AttestBy;
			txtTownOfClerk2.Text = modGNBas.Statics.typOldVitalsReport.TownOfClerk2;
			txtTownOfClerk1.Text = modGNBas.Statics.typOldVitalsReport.TownOfClerk1;
			txtToDate.Text = modGNBas.Statics.typOldVitalsReport.DateTo;
			txtFromDate.Text = modGNBas.Statics.typOldVitalsReport.DateFrom;
		}

		
	}
}
