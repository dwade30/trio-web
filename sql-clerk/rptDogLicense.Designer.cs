﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogLicense.
	/// </summary>
	partial class rptDogLicense
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDogLicense));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDogName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBreed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTagNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMF = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSex = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtColor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVeterinarian = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLicenseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCertNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNeuter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHearing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHybrid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRabiesCertNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRabiesExpiration = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNeuterValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBreed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtColor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVeterinarian)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHearing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHybrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSticker)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRabiesCertNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRabiesExpiration)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuterValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtOwnerName,
				this.txtPhone,
				this.txtAddress,
				this.txtDogName,
				this.txtBreed,
				this.txtTagNumber,
				this.txtMF,
				this.txtDOB,
				this.txtSex,
				this.txtColor,
				this.txtVeterinarian,
				this.txtLocation,
				this.txtLicenseDate,
				this.txtCertNumber,
				this.txtNeuter,
				this.txtSSR,
				this.txtHearing,
				this.txtHybrid,
				this.txtSticker,
				this.txtRabiesCertNum,
				this.Label17,
				this.txtRabiesExpiration,
				this.Label18,
				this.txtYear,
				this.txtNeuterValue
			});
			this.PageHeader.Height = 2.229167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 1.0625F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 1.375F;
			this.txtMuni.Width = 1.3125F;
			// 
			// txtOwnerName
			// 
			this.txtOwnerName.Height = 0.1875F;
			this.txtOwnerName.Left = 1.0625F;
			this.txtOwnerName.MultiLine = false;
			this.txtOwnerName.Name = "txtOwnerName";
			this.txtOwnerName.Text = null;
			this.txtOwnerName.Top = 0.5F;
			this.txtOwnerName.Width = 3.3125F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1875F;
			this.txtPhone.Left = 1.0625F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Text = null;
			this.txtPhone.Top = 1.0625F;
			this.txtPhone.Width = 2.25F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1875F;
			this.txtAddress.Left = 1.0625F;
			this.txtAddress.MultiLine = false;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 0.6875F;
			this.txtAddress.Width = 3.3125F;
			// 
			// txtDogName
			// 
			this.txtDogName.Height = 0.1875F;
			this.txtDogName.Left = 0.75F;
			this.txtDogName.Name = "txtDogName";
			this.txtDogName.Text = null;
			this.txtDogName.Top = 1.8125F;
			this.txtDogName.Width = 1.5F;
			// 
			// txtBreed
			// 
			this.txtBreed.Height = 0.1875F;
			this.txtBreed.Left = 4.3125F;
			this.txtBreed.Name = "txtBreed";
			this.txtBreed.Text = null;
			this.txtBreed.Top = 1.8125F;
			this.txtBreed.Width = 1.375F;
			// 
			// txtTagNumber
			// 
			this.txtTagNumber.Height = 0.1875F;
			this.txtTagNumber.Left = 6.25F;
			this.txtTagNumber.Name = "txtTagNumber";
			this.txtTagNumber.Text = null;
			this.txtTagNumber.Top = 0.375F;
			this.txtTagNumber.Width = 0.875F;
			// 
			// txtMF
			// 
			this.txtMF.Height = 0.1875F;
			this.txtMF.Left = 7F;
			this.txtMF.Name = "txtMF";
			this.txtMF.Text = "X";
			this.txtMF.Top = 0.6875F;
			this.txtMF.Visible = false;
			this.txtMF.Width = 0.1875F;
			// 
			// txtDOB
			// 
			this.txtDOB.Height = 0.1875F;
			this.txtDOB.Left = 2.4375F;
			this.txtDOB.Name = "txtDOB";
			this.txtDOB.Text = null;
			this.txtDOB.Top = 1.8125F;
			this.txtDOB.Width = 0.875F;
			// 
			// txtSex
			// 
			this.txtSex.Height = 0.1875F;
			this.txtSex.Left = 3.5F;
			this.txtSex.Name = "txtSex";
			this.txtSex.Text = null;
			this.txtSex.Top = 1.8125F;
			this.txtSex.Width = 0.1875F;
			// 
			// txtColor
			// 
			this.txtColor.Height = 0.1875F;
			this.txtColor.Left = 1.0625F;
			this.txtColor.Name = "txtColor";
			this.txtColor.Text = null;
			this.txtColor.Top = 2F;
			this.txtColor.Width = 1.375F;
			// 
			// txtVeterinarian
			// 
			this.txtVeterinarian.Height = 0.1875F;
			this.txtVeterinarian.Left = 5.375F;
			this.txtVeterinarian.Name = "txtVeterinarian";
			this.txtVeterinarian.Text = null;
			this.txtVeterinarian.Top = 2F;
			this.txtVeterinarian.Width = 1.375F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 1.0625F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.875F;
			this.txtLocation.Width = 3.3125F;
			// 
			// txtLicenseDate
			// 
			this.txtLicenseDate.Height = 0.1875F;
			this.txtLicenseDate.Left = 4F;
			this.txtLicenseDate.Name = "txtLicenseDate";
			this.txtLicenseDate.Text = null;
			this.txtLicenseDate.Top = 1.0625F;
			this.txtLicenseDate.Width = 0.8125F;
			// 
			// txtCertNumber
			// 
			this.txtCertNumber.Height = 0.1875F;
			this.txtCertNumber.Left = 4F;
			this.txtCertNumber.Name = "txtCertNumber";
			this.txtCertNumber.Text = null;
			this.txtCertNumber.Top = 1.375F;
			this.txtCertNumber.Width = 0.8125F;
			// 
			// txtNeuter
			// 
			this.txtNeuter.Height = 0.1875F;
			this.txtNeuter.Left = 7F;
			this.txtNeuter.Name = "txtNeuter";
			this.txtNeuter.Text = "X";
			this.txtNeuter.Top = 0.875F;
			this.txtNeuter.Visible = false;
			this.txtNeuter.Width = 0.1875F;
			// 
			// txtSSR
			// 
			this.txtSSR.Height = 0.1875F;
			this.txtSSR.Left = 7F;
			this.txtSSR.Name = "txtSSR";
			this.txtSSR.Text = "X";
			this.txtSSR.Top = 1.0625F;
			this.txtSSR.Visible = false;
			this.txtSSR.Width = 0.1875F;
			// 
			// txtHearing
			// 
			this.txtHearing.Height = 0.1875F;
			this.txtHearing.Left = 7F;
			this.txtHearing.Name = "txtHearing";
			this.txtHearing.Text = "X";
			this.txtHearing.Top = 1.3125F;
			this.txtHearing.Visible = false;
			this.txtHearing.Width = 0.1875F;
			// 
			// txtHybrid
			// 
			this.txtHybrid.Height = 0.1875F;
			this.txtHybrid.Left = 7F;
			this.txtHybrid.Name = "txtHybrid";
			this.txtHybrid.Text = "X";
			this.txtHybrid.Top = 1.5F;
			this.txtHybrid.Visible = false;
			this.txtHybrid.Width = 0.1875F;
			// 
			// txtSticker
			// 
			this.txtSticker.Height = 0.1875F;
			this.txtSticker.Left = 4.375F;
			this.txtSticker.Name = "txtSticker";
			this.txtSticker.Text = null;
			this.txtSticker.Top = 0.5F;
			this.txtSticker.Width = 1.8125F;
			// 
			// txtRabiesCertNum
			// 
			this.txtRabiesCertNum.Height = 0.1875F;
			this.txtRabiesCertNum.Left = 4.6875F;
			this.txtRabiesCertNum.Name = "txtRabiesCertNum";
			this.txtRabiesCertNum.Text = null;
			this.txtRabiesCertNum.Top = 1.625F;
			this.txtRabiesCertNum.Width = 1.5F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Courier\'; font-weight: bold; text-align: right";
			this.Label17.Text = "Cert #";
			this.Label17.Top = 1.625F;
			this.Label17.Width = 0.6875F;
			// 
			// txtRabiesExpiration
			// 
			this.txtRabiesExpiration.Height = 0.1875F;
			this.txtRabiesExpiration.Left = 1.4375F;
			this.txtRabiesExpiration.Name = "txtRabiesExpiration";
			this.txtRabiesExpiration.Text = null;
			this.txtRabiesExpiration.Top = 1.625F;
			this.txtRabiesExpiration.Width = 0.9375F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.1875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Courier\'; font-weight: bold";
			this.Label18.Text = "Rabies Exp.";
			this.Label18.Top = 1.625F;
			this.Label18.Width = 1.25F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1875F;
			this.txtYear.Left = 4.4375F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Text = null;
			this.txtYear.Top = 0.1875F;
			this.txtYear.Width = 0.875F;
			// 
			// txtNeuterValue
			// 
			this.txtNeuterValue.Height = 0.1875F;
			this.txtNeuterValue.Left = 5.5625F;
			this.txtNeuterValue.Name = "txtNeuterValue";
			this.txtNeuterValue.Style = "text-align: right";
			this.txtNeuterValue.Text = null;
			this.txtNeuterValue.Top = 0.8125F;
			this.txtNeuterValue.Width = 0.875F;
			// 
			// rptDogLicense
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.3472222F;
			this.PageSettings.Margins.Left = 0.1388889F;
			this.PageSettings.Margins.Right = 0.1388889F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.638889F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBreed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtColor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVeterinarian)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHearing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHybrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSticker)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRabiesCertNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRabiesExpiration)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuterValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBreed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMF;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSex;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtColor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVeterinarian;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCertNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHybrid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRabiesCertNum;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRabiesExpiration;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuterValue;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
