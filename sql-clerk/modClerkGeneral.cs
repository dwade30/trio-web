﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Runtime.InteropServices;

namespace TWCK0000
{
	public class modClerkGeneral
	{
		

		public const int lngMAX_CHAR_PER_LINE = 80;
		public const int lngGETLINE = 196;
		public const int lngGETLINECOUNT = 186;

		
		// *****
		// *****  NAS 18 DEC 00
		// *****  User defined type for printing Vital Records
		// *****
		// *****
		public const string FULL_ACCESS = "F";
		public const string VIEW_ONLY = "V";
		public const string NO_ACCESS = "N";

		public struct Security
		{
			public FCFixedString Entry_Clerk;
			public FCFixedString PrintVi_Rec;
			public FCFixedString Dog_License;
			public FCFixedString RV_reg_IFaW;
			public FCFixedString IFW_License;
			public FCFixedString RV_Inv_IFW;
			public FCFixedString LC_Inv_IFW;
			public FCFixedString VW_I_Birth;
			public FCFixedString VW_C_Death;
		};

		public struct PrintInfo
		{
			public string CertNumber;
			// **** Vital Record Certificate Number
			// vbPorter upgrade warning: tblKeyNum As int	OnWrite(int, string)
			public int tblKeyNum;
			// **** Primary Key for the table
			public string TblName;
			// **** Table Name
			public string AttestedBy;
			// **** Clerk or Registrar issuing the Certificate
			public DateTime PrintDate;
			// **** Date printed
			// vbPorter upgrade warning: PreView As bool	OnWrite(bool, TriState)
			public bool PreView;
			public bool GeneralForm;
			public bool DontShowBottomHeaders;
		};

		public struct DogInfo
		{
			public int OwnerNum;
			public int DogNumber;
			public string DOGNAME;
			public string DogDOB;
			public string DogSex;
			public string DogColor;
			public string DogBreed;
			public string DogMarkings;
			public string DogVet;
			public bool DogDeceased;
			public bool TempLicense;
			public bool DogNeuter;
			public string SNCertNum;
			public string RabiesTagNo;
			public string RabiesCertNum;
			public string RabiesShotDate;
			public string RabStickerIssue;
			public string DateVaccinated;
			public string RabStickerNum;
			public string RabiesExpDate;
			public string TagLicNum;
			public string StickerLicNum;
			public string LicStickI_Date;
			public bool LateFee;
			public bool ReplacementTagFee;
			public bool ReplacementStickerFee;
			public bool WarrantFee;
			public bool Replacement;
			public bool Transfer;
			public bool SandR;
			public bool HearGuide;
			public bool WolfHybrid;
			public int Year;
			public Decimal AmountC;
			public string ArchInfo;
			public DateTime ReplacementTagDate;
			public DateTime ReplacementStickerDate;
			public DateTime ReplacementLicenseDate;
			public DateTime TransferLicenseDate;
		};

		public struct DogOwner
		{
			public int OwnerNum;
			public int NewONum;
			public int OldONum;
			public string OldODogNums;
			public string FirstName;
			public string MI;
			public string LastName;
			public string Desig;
			public string ADDRESS;
			public string City;
			public string State;
			public string Zip;
			public string Phone;
			public string LocationNum;
			public string LocationSTR;
			public string K10num;
			public DateTime K10Date;
			public string K20num;
			public DateTime K20Date;
			public bool KennelLic;
			public string DogNumbers;
			public DateTime KennelInspectionDate;
		};

		public struct dogArch
		{
			public int KeyNum;
			public int DogNumber;
			public int OwnerNumber;
			public int NewOwnerNum;
			public DateTime Year;
			public bool Deceased;
			public string RabiesTagNo;
			public string RabiesStickerNo;
			public DateTime RabiesStickerIssue;
			public DateTime RabiesShotDate;
			public string RabiesCertNum;
			public DateTime RabiesExpDate;
			public DateTime ThisRecDate;
			public string transaction;
		};

		public struct SearchInfo
		{
			public int ID;
			public object LastName;
			public object MiddleName;
			public object FirstName;
			public object Designation;
			// vbPorter upgrade warning: DateOfBirth As DateTime	OnWrite(string)
			public DateTime DateOfBirth;
			// vbPorter upgrade warning: DOBubound As DateTime	OnWrite(string)
			public DateTime DOBubound;
			public object Sex;
			public object BirthPlace;
			public object AttendantsLastName;
			public object AttendantsFirstName;
			public object AttendantsMiddleInitial;
			public object AttendantTitle;
			public object MothersLastName;
			public object MothersFirstName;
			// vbPorter upgrade warning: MothersMiddleName As object	OnWrite(string)
			public object MothersMiddleName;
			public object MothersMaidenName;
			public object ResidenceOfMother;
			public object FathersLastName;
			public object FathersFirstName;
			// vbPorter upgrade warning: FathersMiddleInitial As object	OnWrite(string)
			public object FathersMiddleInitial;
			public object FathersDesignation;
			public object RecordingClerksLastName;
			public object RecordingClerksFirstName;
			public object RecordingClerksMiddleInitial;
			public object CityOrTown;
			public DateTime DateOfFiling;
			public DateTime AttestDate;
			public object StateRegistrarMunicipalClerk;
			public object TownOf;
			public bool Legitimate;
			public object BirthCertNum;
		};

		
		// vbPorter upgrade warning: strInput As string	OnWrite(string, VB.TextBox)
		public static string SortString(ref string strInput)
		{
			string SortString = "";
			// vbPorter upgrade warning: myName As object	OnWrite(string())	OnRead(string)
			string[] myName;
			object tempName;
			bool Period;
			bool Comma;
			int Num;
			string strTemp = "";
			string F_Name;
			string m_Name;
			string L_Name;
			int i;
			F_Name = "";
			m_Name = "";
			L_Name = "";
			Num = 0;
			Comma = false;
			Period = false;
			// *********End Def >>>>
			if (Strings.InStr(1, strInput, ",", CompareConstants.vbTextCompare) != 0)
				Comma = true;
			if (Strings.InStr(1, strInput, ".", CompareConstants.vbTextCompare) != 0)
				Period = true;
			strInput = fecherFoundation.Strings.Trim(strInput);
			myName = Strings.Split(strInput, " ", -1, CompareConstants.vbBinaryCompare);
			if (Information.UBound(myName, 1) < 0)
				return SortString;
			switch (Information.UBound(myName, 1) + 1)
			{
				case 0:
					{
						L_Name = FCConvert.ToString(myName[0]);
						break;
					}
				case 1:
					{
						L_Name = FCConvert.ToString(myName[0]);
						break;
					}
				case 2:
					{
						if (Comma == true)
						{
							L_Name = Strings.Left(FCConvert.ToString(myName[0]), (FCConvert.ToString(myName[0]).Length - 1)) + " ";
							F_Name = myName[1] + " ";
						}
						else if (Period == true)
						{
							F_Name = Strings.Left(FCConvert.ToString(myName[0]), (FCConvert.ToString(myName[0]).Length - 1)) + " ";
							L_Name = myName[1] + " ";
						}
						else
						{
							F_Name = myName[0] + " ";
							L_Name = myName[1] + " ";
						}
						break;
					}
				case 3:
					{
						if (Comma == true)
						{
							L_Name = Strings.Left(FCConvert.ToString(myName[0]), (FCConvert.ToString(myName[0]).Length - 1)) + " ";
							F_Name = myName[1] + " ";
							if (Period == true)
							{
								m_Name = Strings.Left(FCConvert.ToString(myName[2]), (FCConvert.ToString(myName[2]).Length - 1)) + " ";
							}
							else
							{
								m_Name = myName[2] + " ";
							}
						}
						else
						{
							F_Name = myName[0] + " ";
							if (Period == true)
							{
								m_Name = Strings.Left(FCConvert.ToString(myName[1]), (FCConvert.ToString(myName[1]).Length - 1)) + " ";
							}
							else
							{
								m_Name = myName[1] + " ";
							}
							L_Name = myName[2] + " ";
						}
						break;
					}
				default:
					{
						// MsgBox "it failed because of Num Select"
						break;
					}
			}
			//end switch
			// ***** if no first must be a last name else set first name
			if (F_Name != "")
			{
				SortString = fecherFoundation.Strings.Trim(F_Name);
			}
			else
			{
				SortString = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(L_Name));
				return SortString;
			}
			// ***** Have first check middle and then add last
			if (m_Name != "")
			{
				SortString = SortString + " " + fecherFoundation.Strings.Trim(m_Name) + " " + fecherFoundation.Strings.Trim(L_Name);
			}
			else
			{
				SortString = SortString + " " + fecherFoundation.Strings.Trim(L_Name);
			}
			SortString = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(SortString));
			return SortString;
		}

		public static string SortString2(string strInput)
		{
			return SortString(ref strInput);
		}

		public class StaticVariables
		{
			public string User = string.Empty;
			public string ClerkName = string.Empty;
			// ***** This is the Clerk's Name
			public bool AddingBirthCertificate;
			public bool SearchBirthCertificate;
			public bool PrintBirthCertificate;
			public bool AddingMarriageCertificate;
			public bool SearchMarriageCertificate;
			public bool Searching;
			public bool FirstMarriageFormLoaded;
			public bool SecondMarriageFormLoaded;
			public bool MarriageUpdateCancelled;
			public bool Quitting;
			public bool AddingDeathCertificate;
			public bool SearchDeathCertificate;
			public bool SearchDogCertificate;
			public bool AddingRec;
			public bool EditRec;
			public bool bolEditDefaults;
			// used in the recreation vechicle registration screen.
			public int lngID;
			// vbPorter upgrade warning: SearchResponse As int	OnWrite(int, string)
			public int SearchResponse;
			public int SearchRecordNumber;
			
			
			public int intPerSticker;
			
			
			public SearchInfo BSInfo = new SearchInfo();
			public DogInfo dInfo = new DogInfo();
			public DogOwner dOwner = new DogOwner();
			public dogArch dArchive = new dogArch();
			public Security utCkSec = new Security();
			public PrintInfo utPrintInfo = new PrintInfo();
			public string strReturnString = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
