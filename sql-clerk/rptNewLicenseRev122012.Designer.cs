﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptNewMarriageLicenseRev122012.
	/// </summary>
	partial class rptNewLicenseRev122012
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewLicenseRev122012));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtGroomFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateIntenstionsFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCeremonyDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCountyMarried = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPersonPerformingCeremony = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCityMarried = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateLicenseIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsDesig = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsFathersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMothersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMothersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesSurName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFathersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMothersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMothersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMarriageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMarriageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMarriageEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCityofIssue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLicenseValidUntil = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleOfPersonPerforming = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateOfCommision = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtResidenceOfPersonPerforming = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddressOfPersonPerforming = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWitnessOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWitnessTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSignitureWit2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSignitureWit1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSigniturePerson = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomDomesticPartnerYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomDomesticPartnersNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideDomesticPartnerYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideDomesticPartnerNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtstate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIssuance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMarriage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.FileNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomYearDomesticPartner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBrideYearDomesticPartner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyABirthName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBDesig = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyAMarriageEndedDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyAMarriageEndedDivorce = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyAMarriageEndedAnnulment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMarriageEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBMarriageEndedDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBMarriageEndedDivorce = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBMarriageEndedAnnulment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyABride = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyAGroom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyASpouse = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBBride = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBGroom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBSpouse = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBFemale = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBMale = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyAFemale = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyAMale = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyACourt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPartyBCourt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateIntenstionsFiled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCeremonyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountyMarried)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonPerformingCeremony)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityMarried)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateLicenseIssued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDesig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesSurName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEnded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityofIssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenseValidUntil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleOfPersonPerforming)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateOfCommision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResidenceOfPersonPerforming)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddressOfPersonPerforming)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitnessOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitnessTwo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignitureWit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignitureWit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSigniturePerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnerYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssuance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarriage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FileNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomYearDomesticPartner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideYearDomesticPartner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyABirthName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBDesig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAMarriageEndedDeath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAMarriageEndedDivorce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAMarriageEndedAnnulment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEnded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBMarriageEndedDeath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBMarriageEndedDivorce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBMarriageEndedAnnulment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyABride)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAGroom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyASpouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBBride)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBGroom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBSpouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBFemale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBMale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAFemale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAMale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyACourt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBCourt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroomFirst,
            this.txtGroomStreet,
            this.txtGroomAge,
            this.txtGroomBirthplace,
            this.txtDateIntenstionsFiled,
            this.txtCeremonyDate,
            this.txtCountyMarried,
            this.txtPersonPerformingCeremony,
            this.txtCityMarried,
            this.txtDateLicenseIssued,
            this.txtGroomMiddle,
            this.txtGroomLast,
            this.txtGroomsDesig,
            this.txtGroomsState,
            this.txtGroomsCounty,
            this.txtGroomsCity,
            this.txtGroomsDOB,
            this.txtGroomsFathersName,
            this.txtGroomsFathersBirthplace,
            this.txtGroomsMothersName,
            this.txtGroomsMothersBirthplace,
            this.txtBridesFirst,
            this.txtBridesStreet,
            this.txtBridesAge,
            this.txtBridesBirthplace,
            this.txtBridesMiddle,
            this.txtBridesSurName,
            this.txtBridesState,
            this.txtBridesCounty,
            this.txtBridesCity,
            this.txtBridesFathersName,
            this.txtBridesFathersBirthplace,
            this.txtBridesMothersName,
            this.txtBridesDOB,
            this.txtBridesMothersBirthplace,
            this.txtBridesLast,
            this.txtGroomsMarriageNumber,
            this.txtBridesMarriageNumber,
            this.txtBridesMarriageEnded,
            this.txtCityofIssue,
            this.txtLicenseValidUntil,
            this.txtTitleOfPersonPerforming,
            this.txtDateOfCommision,
            this.txtResidenceOfPersonPerforming,
            this.txtAddressOfPersonPerforming,
            this.txtWitnessOne,
            this.txtWitnessTwo,
            this.txtDateFiled,
            this.txtSignitureWit2,
            this.txtSignitureWit1,
            this.txtSigniturePerson,
            this.fldGroomDomesticPartnerYes,
            this.fldGroomDomesticPartnersNo,
            this.fldBrideDomesticPartnerYes,
            this.fldBrideDomesticPartnerNo,
            this.txtstate,
            this.txtIssuance,
            this.txtMarriage,
            this.FileNumber,
            this.txtGroomYearDomesticPartner,
            this.txtBrideYearDomesticPartner,
            this.txtPartyABirthName,
            this.txtPartyBDesig,
            this.txtPartyAMarriageEndedDeath,
            this.txtPartyAMarriageEndedDivorce,
            this.txtPartyAMarriageEndedAnnulment,
            this.txtGroomsMarriageEnded,
            this.txtPartyBMarriageEndedDeath,
            this.txtPartyBMarriageEndedDivorce,
            this.txtPartyBMarriageEndedAnnulment,
            this.txtPartyABride,
            this.txtPartyAGroom,
            this.txtPartyASpouse,
            this.txtPartyBBride,
            this.txtPartyBGroom,
            this.txtPartyBSpouse,
            this.txtPartyBFemale,
            this.txtPartyBMale,
            this.txtPartyAFemale,
            this.txtPartyAMale,
            this.txtPartyACourt,
            this.txtPartyBCourt});
            this.Detail.Height = 9.770833F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtGroomFirst
            // 
            this.txtGroomFirst.CanGrow = false;
            this.txtGroomFirst.Height = 0.1875F;
            this.txtGroomFirst.Left = 0.0625F;
            this.txtGroomFirst.Name = "txtGroomFirst";
            this.txtGroomFirst.Text = null;
            this.txtGroomFirst.Top = 0.6458333F;
            this.txtGroomFirst.Width = 1.625F;
            // 
            // txtGroomStreet
            // 
            this.txtGroomStreet.CanGrow = false;
            this.txtGroomStreet.Height = 0.1666667F;
            this.txtGroomStreet.Left = 4.3125F;
            this.txtGroomStreet.Name = "txtGroomStreet";
            this.txtGroomStreet.Text = null;
            this.txtGroomStreet.Top = 1.291667F;
            this.txtGroomStreet.Width = 2.5625F;
            // 
            // txtGroomAge
            // 
            this.txtGroomAge.CanGrow = false;
            this.txtGroomAge.Height = 0.1875F;
            this.txtGroomAge.Left = 0.0625F;
            this.txtGroomAge.Name = "txtGroomAge";
            this.txtGroomAge.Text = null;
            this.txtGroomAge.Top = 0.9791667F;
            this.txtGroomAge.Width = 0.8125F;
            // 
            // txtGroomBirthplace
            // 
            this.txtGroomBirthplace.CanGrow = false;
            this.txtGroomBirthplace.Height = 0.19F;
            this.txtGroomBirthplace.Left = 2.1875F;
            this.txtGroomBirthplace.Name = "txtGroomBirthplace";
            this.txtGroomBirthplace.Text = null;
            this.txtGroomBirthplace.Top = 0.9791667F;
            this.txtGroomBirthplace.Width = 1.6875F;
            // 
            // txtDateIntenstionsFiled
            // 
            this.txtDateIntenstionsFiled.CanGrow = false;
            this.txtDateIntenstionsFiled.Height = 0.19F;
            this.txtDateIntenstionsFiled.Left = 1.6875F;
            this.txtDateIntenstionsFiled.Name = "txtDateIntenstionsFiled";
            this.txtDateIntenstionsFiled.Text = null;
            this.txtDateIntenstionsFiled.Top = 5.53125F;
            this.txtDateIntenstionsFiled.Width = 1.25F;
            // 
            // txtCeremonyDate
            // 
            this.txtCeremonyDate.CanGrow = false;
            this.txtCeremonyDate.Height = 0.19F;
            this.txtCeremonyDate.Left = 1F;
            this.txtCeremonyDate.Name = "txtCeremonyDate";
            this.txtCeremonyDate.Text = null;
            this.txtCeremonyDate.Top = 6.8125F;
            this.txtCeremonyDate.Visible = false;
            this.txtCeremonyDate.Width = 1.5625F;
            // 
            // txtCountyMarried
            // 
            this.txtCountyMarried.CanGrow = false;
            this.txtCountyMarried.Height = 0.19F;
            this.txtCountyMarried.Left = 5.375F;
            this.txtCountyMarried.Name = "txtCountyMarried";
            this.txtCountyMarried.Text = null;
            this.txtCountyMarried.Top = 6.8125F;
            this.txtCountyMarried.Visible = false;
            this.txtCountyMarried.Width = 1.5625F;
            // 
            // txtPersonPerformingCeremony
            // 
            this.txtPersonPerformingCeremony.CanGrow = false;
            this.txtPersonPerformingCeremony.Height = 0.19F;
            this.txtPersonPerformingCeremony.Left = 3.5625F;
            this.txtPersonPerformingCeremony.Name = "txtPersonPerformingCeremony";
            this.txtPersonPerformingCeremony.Text = null;
            this.txtPersonPerformingCeremony.Top = 7.111111F;
            this.txtPersonPerformingCeremony.Visible = false;
            this.txtPersonPerformingCeremony.Width = 2.3125F;
            // 
            // txtCityMarried
            // 
            this.txtCityMarried.CanGrow = false;
            this.txtCityMarried.Height = 0.19F;
            this.txtCityMarried.Left = 2.6875F;
            this.txtCityMarried.Name = "txtCityMarried";
            this.txtCityMarried.Text = null;
            this.txtCityMarried.Top = 6.8125F;
            this.txtCityMarried.Visible = false;
            this.txtCityMarried.Width = 2.4375F;
            // 
            // txtDateLicenseIssued
            // 
            this.txtDateLicenseIssued.CanGrow = false;
            this.txtDateLicenseIssued.Height = 0.19F;
            this.txtDateLicenseIssued.Left = 3.9375F;
            this.txtDateLicenseIssued.Name = "txtDateLicenseIssued";
            this.txtDateLicenseIssued.Text = null;
            this.txtDateLicenseIssued.Top = 5.53125F;
            this.txtDateLicenseIssued.Width = 1.125F;
            // 
            // txtGroomMiddle
            // 
            this.txtGroomMiddle.CanGrow = false;
            this.txtGroomMiddle.Height = 0.1875F;
            this.txtGroomMiddle.Left = 1.75F;
            this.txtGroomMiddle.Name = "txtGroomMiddle";
            this.txtGroomMiddle.Text = null;
            this.txtGroomMiddle.Top = 0.6458333F;
            this.txtGroomMiddle.Width = 1.5625F;
            // 
            // txtGroomLast
            // 
            this.txtGroomLast.CanGrow = false;
            this.txtGroomLast.Height = 0.1875F;
            this.txtGroomLast.Left = 5.125F;
            this.txtGroomLast.Name = "txtGroomLast";
            this.txtGroomLast.Text = null;
            this.txtGroomLast.Top = 0.6458333F;
            this.txtGroomLast.Width = 1.625F;
            // 
            // txtGroomsDesig
            // 
            this.txtGroomsDesig.CanGrow = false;
            this.txtGroomsDesig.Height = 0.1875F;
            this.txtGroomsDesig.Left = 6.9375F;
            this.txtGroomsDesig.Name = "txtGroomsDesig";
            this.txtGroomsDesig.Text = null;
            this.txtGroomsDesig.Top = 0.6458333F;
            this.txtGroomsDesig.Width = 0.4375F;
            // 
            // txtGroomsState
            // 
            this.txtGroomsState.CanGrow = false;
            this.txtGroomsState.Height = 0.1666667F;
            this.txtGroomsState.Left = 5.125F;
            this.txtGroomsState.Name = "txtGroomsState";
            this.txtGroomsState.Text = null;
            this.txtGroomsState.Top = 0.9791667F;
            this.txtGroomsState.Width = 0.8125F;
            // 
            // txtGroomsCounty
            // 
            this.txtGroomsCounty.CanGrow = false;
            this.txtGroomsCounty.Height = 0.1666667F;
            this.txtGroomsCounty.Left = 0.0625F;
            this.txtGroomsCounty.Name = "txtGroomsCounty";
            this.txtGroomsCounty.Text = null;
            this.txtGroomsCounty.Top = 1.291667F;
            this.txtGroomsCounty.Width = 1.375F;
            // 
            // txtGroomsCity
            // 
            this.txtGroomsCity.CanGrow = false;
            this.txtGroomsCity.Height = 0.1666667F;
            this.txtGroomsCity.Left = 2.1875F;
            this.txtGroomsCity.Name = "txtGroomsCity";
            this.txtGroomsCity.Text = null;
            this.txtGroomsCity.Top = 1.291667F;
            this.txtGroomsCity.Width = 2.0625F;
            // 
            // txtGroomsDOB
            // 
            this.txtGroomsDOB.CanGrow = false;
            this.txtGroomsDOB.Height = 0.1666667F;
            this.txtGroomsDOB.Left = 0.9375F;
            this.txtGroomsDOB.Name = "txtGroomsDOB";
            this.txtGroomsDOB.Text = null;
            this.txtGroomsDOB.Top = 0.9791667F;
            this.txtGroomsDOB.Width = 1.072917F;
            // 
            // txtGroomsFathersName
            // 
            this.txtGroomsFathersName.CanGrow = false;
            this.txtGroomsFathersName.Height = 0.1875F;
            this.txtGroomsFathersName.Left = 0.0625F;
            this.txtGroomsFathersName.Name = "txtGroomsFathersName";
            this.txtGroomsFathersName.Text = null;
            this.txtGroomsFathersName.Top = 1.59375F;
            this.txtGroomsFathersName.Width = 2.0625F;
            // 
            // txtGroomsFathersBirthplace
            // 
            this.txtGroomsFathersBirthplace.CanGrow = false;
            this.txtGroomsFathersBirthplace.Height = 0.1875F;
            this.txtGroomsFathersBirthplace.Left = 2.1875F;
            this.txtGroomsFathersBirthplace.Name = "txtGroomsFathersBirthplace";
            this.txtGroomsFathersBirthplace.Text = null;
            this.txtGroomsFathersBirthplace.Top = 1.59375F;
            this.txtGroomsFathersBirthplace.Width = 1.5F;
            // 
            // txtGroomsMothersName
            // 
            this.txtGroomsMothersName.CanGrow = false;
            this.txtGroomsMothersName.Height = 0.1875F;
            this.txtGroomsMothersName.Left = 3.75F;
            this.txtGroomsMothersName.Name = "txtGroomsMothersName";
            this.txtGroomsMothersName.Text = null;
            this.txtGroomsMothersName.Top = 1.59375F;
            this.txtGroomsMothersName.Width = 2.15625F;
            // 
            // txtGroomsMothersBirthplace
            // 
            this.txtGroomsMothersBirthplace.CanGrow = false;
            this.txtGroomsMothersBirthplace.Height = 0.1875F;
            this.txtGroomsMothersBirthplace.Left = 6.125F;
            this.txtGroomsMothersBirthplace.Name = "txtGroomsMothersBirthplace";
            this.txtGroomsMothersBirthplace.Text = null;
            this.txtGroomsMothersBirthplace.Top = 1.59375F;
            this.txtGroomsMothersBirthplace.Width = 1.3125F;
            // 
            // txtBridesFirst
            // 
            this.txtBridesFirst.CanGrow = false;
            this.txtBridesFirst.Height = 0.19F;
            this.txtBridesFirst.Left = 0.0625F;
            this.txtBridesFirst.Name = "txtBridesFirst";
            this.txtBridesFirst.Text = null;
            this.txtBridesFirst.Top = 2.1875F;
            this.txtBridesFirst.Width = 1.625F;
            // 
            // txtBridesStreet
            // 
            this.txtBridesStreet.CanGrow = false;
            this.txtBridesStreet.Height = 0.1666667F;
            this.txtBridesStreet.Left = 4.3125F;
            this.txtBridesStreet.Name = "txtBridesStreet";
            this.txtBridesStreet.Text = null;
            this.txtBridesStreet.Top = 2.875F;
            this.txtBridesStreet.Width = 2.5625F;
            // 
            // txtBridesAge
            // 
            this.txtBridesAge.CanGrow = false;
            this.txtBridesAge.Height = 0.19F;
            this.txtBridesAge.Left = 0.0625F;
            this.txtBridesAge.Name = "txtBridesAge";
            this.txtBridesAge.Text = null;
            this.txtBridesAge.Top = 2.5F;
            this.txtBridesAge.Width = 0.625F;
            // 
            // txtBridesBirthplace
            // 
            this.txtBridesBirthplace.CanGrow = false;
            this.txtBridesBirthplace.Height = 0.1666667F;
            this.txtBridesBirthplace.Left = 2.1875F;
            this.txtBridesBirthplace.Name = "txtBridesBirthplace";
            this.txtBridesBirthplace.Text = null;
            this.txtBridesBirthplace.Top = 2.5F;
            this.txtBridesBirthplace.Width = 1.9375F;
            // 
            // txtBridesMiddle
            // 
            this.txtBridesMiddle.CanGrow = false;
            this.txtBridesMiddle.Height = 0.19F;
            this.txtBridesMiddle.Left = 1.75F;
            this.txtBridesMiddle.Name = "txtBridesMiddle";
            this.txtBridesMiddle.Text = null;
            this.txtBridesMiddle.Top = 2.1875F;
            this.txtBridesMiddle.Width = 1.5625F;
            // 
            // txtBridesSurName
            // 
            this.txtBridesSurName.CanGrow = false;
            this.txtBridesSurName.Height = 0.19F;
            this.txtBridesSurName.Left = 3.4375F;
            this.txtBridesSurName.Name = "txtBridesSurName";
            this.txtBridesSurName.Text = null;
            this.txtBridesSurName.Top = 2.1875F;
            this.txtBridesSurName.Width = 1.625F;
            // 
            // txtBridesState
            // 
            this.txtBridesState.CanGrow = false;
            this.txtBridesState.Height = 0.1666667F;
            this.txtBridesState.Left = 5.125F;
            this.txtBridesState.Name = "txtBridesState";
            this.txtBridesState.Text = null;
            this.txtBridesState.Top = 2.5F;
            this.txtBridesState.Width = 0.8125F;
            // 
            // txtBridesCounty
            // 
            this.txtBridesCounty.CanGrow = false;
            this.txtBridesCounty.Height = 0.19F;
            this.txtBridesCounty.Left = 0.0625F;
            this.txtBridesCounty.Name = "txtBridesCounty";
            this.txtBridesCounty.Text = null;
            this.txtBridesCounty.Top = 2.875F;
            this.txtBridesCounty.Width = 1.375F;
            // 
            // txtBridesCity
            // 
            this.txtBridesCity.CanGrow = false;
            this.txtBridesCity.Height = 0.1666667F;
            this.txtBridesCity.Left = 2.1875F;
            this.txtBridesCity.Name = "txtBridesCity";
            this.txtBridesCity.Text = null;
            this.txtBridesCity.Top = 2.875F;
            this.txtBridesCity.Width = 2.0625F;
            // 
            // txtBridesFathersName
            // 
            this.txtBridesFathersName.CanGrow = false;
            this.txtBridesFathersName.Height = 0.1875F;
            this.txtBridesFathersName.Left = 0.0625F;
            this.txtBridesFathersName.Name = "txtBridesFathersName";
            this.txtBridesFathersName.Text = null;
            this.txtBridesFathersName.Top = 3.15625F;
            this.txtBridesFathersName.Width = 2.0625F;
            // 
            // txtBridesFathersBirthplace
            // 
            this.txtBridesFathersBirthplace.CanGrow = false;
            this.txtBridesFathersBirthplace.Height = 0.1875F;
            this.txtBridesFathersBirthplace.Left = 2.1875F;
            this.txtBridesFathersBirthplace.Name = "txtBridesFathersBirthplace";
            this.txtBridesFathersBirthplace.Text = null;
            this.txtBridesFathersBirthplace.Top = 3.15625F;
            this.txtBridesFathersBirthplace.Width = 1.5F;
            // 
            // txtBridesMothersName
            // 
            this.txtBridesMothersName.CanGrow = false;
            this.txtBridesMothersName.Height = 0.1875F;
            this.txtBridesMothersName.Left = 3.75F;
            this.txtBridesMothersName.Name = "txtBridesMothersName";
            this.txtBridesMothersName.Text = null;
            this.txtBridesMothersName.Top = 3.15625F;
            this.txtBridesMothersName.Width = 2.15625F;
            // 
            // txtBridesDOB
            // 
            this.txtBridesDOB.CanGrow = false;
            this.txtBridesDOB.Height = 0.1666667F;
            this.txtBridesDOB.Left = 0.9375F;
            this.txtBridesDOB.Name = "txtBridesDOB";
            this.txtBridesDOB.Text = null;
            this.txtBridesDOB.Top = 2.5F;
            this.txtBridesDOB.Width = 1.072917F;
            // 
            // txtBridesMothersBirthplace
            // 
            this.txtBridesMothersBirthplace.CanGrow = false;
            this.txtBridesMothersBirthplace.Height = 0.1875F;
            this.txtBridesMothersBirthplace.Left = 6.125F;
            this.txtBridesMothersBirthplace.Name = "txtBridesMothersBirthplace";
            this.txtBridesMothersBirthplace.Text = null;
            this.txtBridesMothersBirthplace.Top = 3.15625F;
            this.txtBridesMothersBirthplace.Width = 1.3125F;
            // 
            // txtBridesLast
            // 
            this.txtBridesLast.CanGrow = false;
            this.txtBridesLast.Height = 0.19F;
            this.txtBridesLast.Left = 5.125F;
            this.txtBridesLast.Name = "txtBridesLast";
            this.txtBridesLast.Text = null;
            this.txtBridesLast.Top = 2.1875F;
            this.txtBridesLast.Width = 1.625F;
            // 
            // txtGroomsMarriageNumber
            // 
            this.txtGroomsMarriageNumber.CanGrow = false;
            this.txtGroomsMarriageNumber.Height = 0.19F;
            this.txtGroomsMarriageNumber.Left = 0.0625F;
            this.txtGroomsMarriageNumber.Name = "txtGroomsMarriageNumber";
            this.txtGroomsMarriageNumber.Text = null;
            this.txtGroomsMarriageNumber.Top = 4.083333F;
            this.txtGroomsMarriageNumber.Width = 0.625F;
            // 
            // txtBridesMarriageNumber
            // 
            this.txtBridesMarriageNumber.CanGrow = false;
            this.txtBridesMarriageNumber.Height = 0.19F;
            this.txtBridesMarriageNumber.Left = 3.75F;
            this.txtBridesMarriageNumber.Name = "txtBridesMarriageNumber";
            this.txtBridesMarriageNumber.Text = null;
            this.txtBridesMarriageNumber.Top = 4.083333F;
            this.txtBridesMarriageNumber.Width = 0.625F;
            // 
            // txtBridesMarriageEnded
            // 
            this.txtBridesMarriageEnded.CanGrow = false;
            this.txtBridesMarriageEnded.Height = 0.19F;
            this.txtBridesMarriageEnded.Left = 6.25F;
            this.txtBridesMarriageEnded.Name = "txtBridesMarriageEnded";
            this.txtBridesMarriageEnded.Text = null;
            this.txtBridesMarriageEnded.Top = 4.239583F;
            this.txtBridesMarriageEnded.Width = 1.145833F;
            // 
            // txtCityofIssue
            // 
            this.txtCityofIssue.CanGrow = false;
            this.txtCityofIssue.Height = 0.1875F;
            this.txtCityofIssue.Left = 4.34375F;
            this.txtCityofIssue.Name = "txtCityofIssue";
            this.txtCityofIssue.Text = null;
            this.txtCityofIssue.Top = 6.013889F;
            this.txtCityofIssue.Width = 2.75F;
            // 
            // txtLicenseValidUntil
            // 
            this.txtLicenseValidUntil.CanGrow = false;
            this.txtLicenseValidUntil.Height = 0.19F;
            this.txtLicenseValidUntil.Left = 6.125F;
            this.txtLicenseValidUntil.Name = "txtLicenseValidUntil";
            this.txtLicenseValidUntil.Text = null;
            this.txtLicenseValidUntil.Top = 5.53125F;
            this.txtLicenseValidUntil.Width = 1.125F;
            // 
            // txtTitleOfPersonPerforming
            // 
            this.txtTitleOfPersonPerforming.CanGrow = false;
            this.txtTitleOfPersonPerforming.Height = 0.19F;
            this.txtTitleOfPersonPerforming.Left = 5.9375F;
            this.txtTitleOfPersonPerforming.Name = "txtTitleOfPersonPerforming";
            this.txtTitleOfPersonPerforming.Text = null;
            this.txtTitleOfPersonPerforming.Top = 7.111111F;
            this.txtTitleOfPersonPerforming.Visible = false;
            this.txtTitleOfPersonPerforming.Width = 1.1875F;
            // 
            // txtDateOfCommision
            // 
            this.txtDateOfCommision.CanGrow = false;
            this.txtDateOfCommision.Height = 0.19F;
            this.txtDateOfCommision.Left = 5.875F;
            this.txtDateOfCommision.Name = "txtDateOfCommision";
            this.txtDateOfCommision.Text = null;
            this.txtDateOfCommision.Top = 7.486111F;
            this.txtDateOfCommision.Visible = false;
            this.txtDateOfCommision.Width = 1.1875F;
            // 
            // txtResidenceOfPersonPerforming
            // 
            this.txtResidenceOfPersonPerforming.CanGrow = false;
            this.txtResidenceOfPersonPerforming.Height = 0.19F;
            this.txtResidenceOfPersonPerforming.Left = 0.125F;
            this.txtResidenceOfPersonPerforming.Name = "txtResidenceOfPersonPerforming";
            this.txtResidenceOfPersonPerforming.Text = null;
            this.txtResidenceOfPersonPerforming.Top = 7.486111F;
            this.txtResidenceOfPersonPerforming.Visible = false;
            this.txtResidenceOfPersonPerforming.Width = 2.625F;
            // 
            // txtAddressOfPersonPerforming
            // 
            this.txtAddressOfPersonPerforming.CanGrow = false;
            this.txtAddressOfPersonPerforming.Height = 0.19F;
            this.txtAddressOfPersonPerforming.Left = 0.125F;
            this.txtAddressOfPersonPerforming.Name = "txtAddressOfPersonPerforming";
            this.txtAddressOfPersonPerforming.Text = null;
            this.txtAddressOfPersonPerforming.Top = 7.8125F;
            this.txtAddressOfPersonPerforming.Visible = false;
            this.txtAddressOfPersonPerforming.Width = 7.125F;
            // 
            // txtWitnessOne
            // 
            this.txtWitnessOne.CanGrow = false;
            this.txtWitnessOne.Height = 0.19F;
            this.txtWitnessOne.Left = 3.635417F;
            this.txtWitnessOne.Name = "txtWitnessOne";
            this.txtWitnessOne.Text = null;
            this.txtWitnessOne.Top = 8.34375F;
            this.txtWitnessOne.Visible = false;
            this.txtWitnessOne.Width = 3.4375F;
            // 
            // txtWitnessTwo
            // 
            this.txtWitnessTwo.CanGrow = false;
            this.txtWitnessTwo.Height = 0.19F;
            this.txtWitnessTwo.Left = 3.635417F;
            this.txtWitnessTwo.Name = "txtWitnessTwo";
            this.txtWitnessTwo.Text = null;
            this.txtWitnessTwo.Top = 8.71875F;
            this.txtWitnessTwo.Visible = false;
            this.txtWitnessTwo.Width = 3.4375F;
            // 
            // txtDateFiled
            // 
            this.txtDateFiled.CanGrow = false;
            this.txtDateFiled.Height = 0.1875F;
            this.txtDateFiled.Left = 3.635417F;
            this.txtDateFiled.Name = "txtDateFiled";
            this.txtDateFiled.Text = null;
            this.txtDateFiled.Top = 9.291667F;
            this.txtDateFiled.Visible = false;
            this.txtDateFiled.Width = 2.46875F;
            // 
            // txtSignitureWit2
            // 
            this.txtSignitureWit2.CanGrow = false;
            this.txtSignitureWit2.Height = 0.19F;
            this.txtSignitureWit2.Left = 0.1354167F;
            this.txtSignitureWit2.Name = "txtSignitureWit2";
            this.txtSignitureWit2.Text = null;
            this.txtSignitureWit2.Top = 8.71875F;
            this.txtSignitureWit2.Visible = false;
            this.txtSignitureWit2.Width = 3.4375F;
            // 
            // txtSignitureWit1
            // 
            this.txtSignitureWit1.CanGrow = false;
            this.txtSignitureWit1.Height = 0.19F;
            this.txtSignitureWit1.Left = 0.1666667F;
            this.txtSignitureWit1.Name = "txtSignitureWit1";
            this.txtSignitureWit1.Text = null;
            this.txtSignitureWit1.Top = 8.34375F;
            this.txtSignitureWit1.Visible = false;
            this.txtSignitureWit1.Width = 3.4375F;
            // 
            // txtSigniturePerson
            // 
            this.txtSigniturePerson.CanGrow = false;
            this.txtSigniturePerson.Height = 0.19F;
            this.txtSigniturePerson.Left = 0.09375F;
            this.txtSigniturePerson.Name = "txtSigniturePerson";
            this.txtSigniturePerson.Text = null;
            this.txtSigniturePerson.Top = 7.111111F;
            this.txtSigniturePerson.Visible = false;
            this.txtSigniturePerson.Width = 3.15625F;
            // 
            // fldGroomDomesticPartnerYes
            // 
            this.fldGroomDomesticPartnerYes.Height = 0.21875F;
            this.fldGroomDomesticPartnerYes.Left = 2.947917F;
            this.fldGroomDomesticPartnerYes.Name = "fldGroomDomesticPartnerYes";
            this.fldGroomDomesticPartnerYes.Style = "font-size: 9pt";
            this.fldGroomDomesticPartnerYes.Text = "X";
            this.fldGroomDomesticPartnerYes.Top = 4.59375F;
            this.fldGroomDomesticPartnerYes.Width = 0.1875F;
            // 
            // fldGroomDomesticPartnersNo
            // 
            this.fldGroomDomesticPartnersNo.Height = 0.21875F;
            this.fldGroomDomesticPartnersNo.Left = 3.25F;
            this.fldGroomDomesticPartnersNo.Name = "fldGroomDomesticPartnersNo";
            this.fldGroomDomesticPartnersNo.Style = "font-size: 9pt";
            this.fldGroomDomesticPartnersNo.Text = "X";
            this.fldGroomDomesticPartnersNo.Top = 4.59375F;
            this.fldGroomDomesticPartnersNo.Width = 0.1875F;
            // 
            // fldBrideDomesticPartnerYes
            // 
            this.fldBrideDomesticPartnerYes.Height = 0.21875F;
            this.fldBrideDomesticPartnerYes.Left = 6.802083F;
            this.fldBrideDomesticPartnerYes.Name = "fldBrideDomesticPartnerYes";
            this.fldBrideDomesticPartnerYes.Text = "X";
            this.fldBrideDomesticPartnerYes.Top = 4.59375F;
            this.fldBrideDomesticPartnerYes.Width = 0.1875F;
            // 
            // fldBrideDomesticPartnerNo
            // 
            this.fldBrideDomesticPartnerNo.Height = 0.21875F;
            this.fldBrideDomesticPartnerNo.Left = 7.090278F;
            this.fldBrideDomesticPartnerNo.Name = "fldBrideDomesticPartnerNo";
            this.fldBrideDomesticPartnerNo.Text = "X";
            this.fldBrideDomesticPartnerNo.Top = 4.59375F;
            this.fldBrideDomesticPartnerNo.Width = 0.1875F;
            // 
            // txtstate
            // 
            this.txtstate.Height = 0.2083333F;
            this.txtstate.Left = 0.25F;
            this.txtstate.Name = "txtstate";
            this.txtstate.Text = "X";
            this.txtstate.Top = 0.04166667F;
            this.txtstate.Visible = false;
            this.txtstate.Width = 0.1875F;
            // 
            // txtIssuance
            // 
            this.txtIssuance.Height = 0.2083333F;
            this.txtIssuance.Left = 0.9375F;
            this.txtIssuance.Name = "txtIssuance";
            this.txtIssuance.Text = "X";
            this.txtIssuance.Top = 0.1666667F;
            this.txtIssuance.Visible = false;
            this.txtIssuance.Width = 0.1875F;
            // 
            // txtMarriage
            // 
            this.txtMarriage.Height = 0.2083333F;
            this.txtMarriage.Left = 1.0625F;
            this.txtMarriage.Name = "txtMarriage";
            this.txtMarriage.Text = "X";
            this.txtMarriage.Top = 0.2916667F;
            this.txtMarriage.Visible = false;
            this.txtMarriage.Width = 0.1875F;
            // 
            // FileNumber
            // 
            this.FileNumber.CanGrow = false;
            this.FileNumber.DataField = "FileNumber";
            this.FileNumber.Height = 0.1875F;
            this.FileNumber.Left = 6.333333F;
            this.FileNumber.Name = "FileNumber";
            this.FileNumber.OutputFormat = resources.GetString("FileNumber.OutputFormat");
            this.FileNumber.Text = null;
            this.FileNumber.Top = 0.2708333F;
            this.FileNumber.Width = 1.0625F;
            // 
            // txtGroomYearDomesticPartner
            // 
            this.txtGroomYearDomesticPartner.Height = 0.1666667F;
            this.txtGroomYearDomesticPartner.Left = 2.270833F;
            this.txtGroomYearDomesticPartner.Name = "txtGroomYearDomesticPartner";
            this.txtGroomYearDomesticPartner.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtGroomYearDomesticPartner.Text = null;
            this.txtGroomYearDomesticPartner.Top = 4.739583F;
            this.txtGroomYearDomesticPartner.Width = 0.625F;
            // 
            // txtBrideYearDomesticPartner
            // 
            this.txtBrideYearDomesticPartner.Height = 0.1666667F;
            this.txtBrideYearDomesticPartner.Left = 5.3125F;
            this.txtBrideYearDomesticPartner.Name = "txtBrideYearDomesticPartner";
            this.txtBrideYearDomesticPartner.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtBrideYearDomesticPartner.Text = null;
            this.txtBrideYearDomesticPartner.Top = 4.739583F;
            this.txtBrideYearDomesticPartner.Width = 0.625F;
            // 
            // txtPartyABirthName
            // 
            this.txtPartyABirthName.CanGrow = false;
            this.txtPartyABirthName.Height = 0.1875F;
            this.txtPartyABirthName.Left = 3.4375F;
            this.txtPartyABirthName.Name = "txtPartyABirthName";
            this.txtPartyABirthName.Text = null;
            this.txtPartyABirthName.Top = 0.6458333F;
            this.txtPartyABirthName.Width = 1.625F;
            // 
            // txtPartyBDesig
            // 
            this.txtPartyBDesig.CanGrow = false;
            this.txtPartyBDesig.Height = 0.1666667F;
            this.txtPartyBDesig.Left = 6.9375F;
            this.txtPartyBDesig.Name = "txtPartyBDesig";
            this.txtPartyBDesig.Text = null;
            this.txtPartyBDesig.Top = 2.1875F;
            this.txtPartyBDesig.Width = 0.4375F;
            // 
            // txtPartyAMarriageEndedDeath
            // 
            this.txtPartyAMarriageEndedDeath.Height = 0.21875F;
            this.txtPartyAMarriageEndedDeath.Left = 1.527778F;
            this.txtPartyAMarriageEndedDeath.Name = "txtPartyAMarriageEndedDeath";
            this.txtPartyAMarriageEndedDeath.Text = "X";
            this.txtPartyAMarriageEndedDeath.Top = 4.027778F;
            this.txtPartyAMarriageEndedDeath.Width = 0.1875F;
            // 
            // txtPartyAMarriageEndedDivorce
            // 
            this.txtPartyAMarriageEndedDivorce.Height = 0.21875F;
            this.txtPartyAMarriageEndedDivorce.Left = 2.076389F;
            this.txtPartyAMarriageEndedDivorce.Name = "txtPartyAMarriageEndedDivorce";
            this.txtPartyAMarriageEndedDivorce.Text = "X";
            this.txtPartyAMarriageEndedDivorce.Top = 4.027778F;
            this.txtPartyAMarriageEndedDivorce.Width = 0.1875F;
            // 
            // txtPartyAMarriageEndedAnnulment
            // 
            this.txtPartyAMarriageEndedAnnulment.Height = 0.21875F;
            this.txtPartyAMarriageEndedAnnulment.Left = 2.6875F;
            this.txtPartyAMarriageEndedAnnulment.Name = "txtPartyAMarriageEndedAnnulment";
            this.txtPartyAMarriageEndedAnnulment.Text = "X";
            this.txtPartyAMarriageEndedAnnulment.Top = 4.027778F;
            this.txtPartyAMarriageEndedAnnulment.Width = 0.1875F;
            // 
            // txtGroomsMarriageEnded
            // 
            this.txtGroomsMarriageEnded.CanGrow = false;
            this.txtGroomsMarriageEnded.Height = 0.19F;
            this.txtGroomsMarriageEnded.Left = 2.5F;
            this.txtGroomsMarriageEnded.Name = "txtGroomsMarriageEnded";
            this.txtGroomsMarriageEnded.Text = null;
            this.txtGroomsMarriageEnded.Top = 4.239583F;
            this.txtGroomsMarriageEnded.Width = 1F;
            // 
            // txtPartyBMarriageEndedDeath
            // 
            this.txtPartyBMarriageEndedDeath.Height = 0.21875F;
            this.txtPartyBMarriageEndedDeath.Left = 5.25F;
            this.txtPartyBMarriageEndedDeath.Name = "txtPartyBMarriageEndedDeath";
            this.txtPartyBMarriageEndedDeath.Text = "X";
            this.txtPartyBMarriageEndedDeath.Top = 4.027778F;
            this.txtPartyBMarriageEndedDeath.Width = 0.1875F;
            // 
            // txtPartyBMarriageEndedDivorce
            // 
            this.txtPartyBMarriageEndedDivorce.Height = 0.21875F;
            this.txtPartyBMarriageEndedDivorce.Left = 5.826389F;
            this.txtPartyBMarriageEndedDivorce.Name = "txtPartyBMarriageEndedDivorce";
            this.txtPartyBMarriageEndedDivorce.Text = "X";
            this.txtPartyBMarriageEndedDivorce.Top = 4.027778F;
            this.txtPartyBMarriageEndedDivorce.Width = 0.1875F;
            // 
            // txtPartyBMarriageEndedAnnulment
            // 
            this.txtPartyBMarriageEndedAnnulment.Height = 0.21875F;
            this.txtPartyBMarriageEndedAnnulment.Left = 6.458333F;
            this.txtPartyBMarriageEndedAnnulment.Name = "txtPartyBMarriageEndedAnnulment";
            this.txtPartyBMarriageEndedAnnulment.Text = "X";
            this.txtPartyBMarriageEndedAnnulment.Top = 4.027778F;
            this.txtPartyBMarriageEndedAnnulment.Width = 0.1875F;
            // 
            // txtPartyABride
            // 
            this.txtPartyABride.Height = 0.2083333F;
            this.txtPartyABride.Left = 2.173611F;
            this.txtPartyABride.Name = "txtPartyABride";
            this.txtPartyABride.Text = "X";
            this.txtPartyABride.Top = 0.2708333F;
            this.txtPartyABride.Visible = false;
            this.txtPartyABride.Width = 0.1875F;
            // 
            // txtPartyAGroom
            // 
            this.txtPartyAGroom.Height = 0.2083333F;
            this.txtPartyAGroom.Left = 3.173611F;
            this.txtPartyAGroom.Name = "txtPartyAGroom";
            this.txtPartyAGroom.Text = "X";
            this.txtPartyAGroom.Top = 0.2708333F;
            this.txtPartyAGroom.Visible = false;
            this.txtPartyAGroom.Width = 0.1875F;
            // 
            // txtPartyASpouse
            // 
            this.txtPartyASpouse.Height = 0.2083333F;
            this.txtPartyASpouse.Left = 4.229167F;
            this.txtPartyASpouse.Name = "txtPartyASpouse";
            this.txtPartyASpouse.Text = "X";
            this.txtPartyASpouse.Top = 0.2708333F;
            this.txtPartyASpouse.Visible = false;
            this.txtPartyASpouse.Width = 0.1875F;
            // 
            // txtPartyBBride
            // 
            this.txtPartyBBride.Height = 0.2083333F;
            this.txtPartyBBride.Left = 2.173611F;
            this.txtPartyBBride.Name = "txtPartyBBride";
            this.txtPartyBBride.Text = "X";
            this.txtPartyBBride.Top = 1.8125F;
            this.txtPartyBBride.Visible = false;
            this.txtPartyBBride.Width = 0.1875F;
            // 
            // txtPartyBGroom
            // 
            this.txtPartyBGroom.Height = 0.2083333F;
            this.txtPartyBGroom.Left = 3.152778F;
            this.txtPartyBGroom.Name = "txtPartyBGroom";
            this.txtPartyBGroom.Text = "X";
            this.txtPartyBGroom.Top = 1.8125F;
            this.txtPartyBGroom.Visible = false;
            this.txtPartyBGroom.Width = 0.1875F;
            // 
            // txtPartyBSpouse
            // 
            this.txtPartyBSpouse.Height = 0.2083333F;
            this.txtPartyBSpouse.Left = 4.208333F;
            this.txtPartyBSpouse.Name = "txtPartyBSpouse";
            this.txtPartyBSpouse.Text = "X";
            this.txtPartyBSpouse.Top = 1.8125F;
            this.txtPartyBSpouse.Visible = false;
            this.txtPartyBSpouse.Width = 0.1875F;
            // 
            // txtPartyBFemale
            // 
            this.txtPartyBFemale.Height = 0.21875F;
            this.txtPartyBFemale.Left = 4.135417F;
            this.txtPartyBFemale.Name = "txtPartyBFemale";
            this.txtPartyBFemale.Style = "font-size: 9pt";
            this.txtPartyBFemale.Text = "X";
            this.txtPartyBFemale.Top = 2.527778F;
            this.txtPartyBFemale.Width = 0.1875F;
            // 
            // txtPartyBMale
            // 
            this.txtPartyBMale.Height = 0.21875F;
            this.txtPartyBMale.Left = 4.625F;
            this.txtPartyBMale.Name = "txtPartyBMale";
            this.txtPartyBMale.Style = "font-size: 9pt";
            this.txtPartyBMale.Text = "X";
            this.txtPartyBMale.Top = 2.527778F;
            this.txtPartyBMale.Width = 0.1875F;
            // 
            // txtPartyAFemale
            // 
            this.txtPartyAFemale.Height = 0.21875F;
            this.txtPartyAFemale.Left = 4.135417F;
            this.txtPartyAFemale.Name = "txtPartyAFemale";
            this.txtPartyAFemale.Style = "font-size: 9pt";
            this.txtPartyAFemale.Text = "X";
            this.txtPartyAFemale.Top = 0.9513889F;
            this.txtPartyAFemale.Width = 0.1875F;
            // 
            // txtPartyAMale
            // 
            this.txtPartyAMale.Height = 0.21875F;
            this.txtPartyAMale.Left = 4.625F;
            this.txtPartyAMale.Name = "txtPartyAMale";
            this.txtPartyAMale.Style = "font-size: 9pt";
            this.txtPartyAMale.Text = "X";
            this.txtPartyAMale.Top = 0.9513889F;
            this.txtPartyAMale.Width = 0.1875F;
            // 
            // txtPartyACourt
            // 
            this.txtPartyACourt.CanGrow = false;
            this.txtPartyACourt.Height = 0.19F;
            this.txtPartyACourt.Left = 1.208333F;
            this.txtPartyACourt.Name = "txtPartyACourt";
            this.txtPartyACourt.Text = null;
            this.txtPartyACourt.Top = 4.427083F;
            this.txtPartyACourt.Width = 2.375F;
            // 
            // txtPartyBCourt
            // 
            this.txtPartyBCourt.CanGrow = false;
            this.txtPartyBCourt.Height = 0.19F;
            this.txtPartyBCourt.Left = 5.0625F;
            this.txtPartyBCourt.Name = "txtPartyBCourt";
            this.txtPartyBCourt.Text = null;
            this.txtPartyBCourt.Top = 4.427083F;
            this.txtPartyBCourt.Width = 2.25F;
            // 
            // rptNewLicenseRev122012
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.625F;
            this.PageSettings.Margins.Right = 0.375F;
            this.PageSettings.Margins.Top = 0.4166667F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.499306F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_Terminate);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
            this.PrintProgress += new System.EventHandler(this.ActiveReport_PrintProgress);
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateIntenstionsFiled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCeremonyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountyMarried)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonPerformingCeremony)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityMarried)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateLicenseIssued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDesig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesSurName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEnded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityofIssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenseValidUntil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleOfPersonPerforming)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateOfCommision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResidenceOfPersonPerforming)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddressOfPersonPerforming)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitnessOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitnessTwo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignitureWit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignitureWit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSigniturePerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnerYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssuance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarriage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FileNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomYearDomesticPartner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideYearDomesticPartner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyABirthName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBDesig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAMarriageEndedDeath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAMarriageEndedDivorce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAMarriageEndedAnnulment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEnded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBMarriageEndedDeath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBMarriageEndedDivorce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBMarriageEndedAnnulment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyABride)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAGroom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyASpouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBBride)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBGroom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBSpouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBFemale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBMale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAFemale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyAMale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyACourt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyBCourt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateIntenstionsFiled;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCeremonyDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountyMarried;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPersonPerformingCeremony;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityMarried;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateLicenseIssued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsDesig;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsFathersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMothersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMothersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesSurName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFathersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMothersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMothersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityofIssue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseValidUntil;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleOfPersonPerforming;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateOfCommision;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtResidenceOfPersonPerforming;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddressOfPersonPerforming;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWitnessOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWitnessTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateFiled;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSignitureWit2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSignitureWit1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSigniturePerson;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomDomesticPartnerYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomDomesticPartnersNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideDomesticPartnerYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideDomesticPartnerNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtstate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssuance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarriage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox FileNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomYearDomesticPartner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideYearDomesticPartner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyABirthName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBDesig;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyAMarriageEndedDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyAMarriageEndedDivorce;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyAMarriageEndedAnnulment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBMarriageEndedDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBMarriageEndedDivorce;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBMarriageEndedAnnulment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyABride;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyAGroom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyASpouse;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBBride;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBGroom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBSpouse;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBFemale;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBMale;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyAFemale;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyAMale;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyACourt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPartyBCourt;
	}
}
