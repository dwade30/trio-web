﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public class modLookUpCombo
	{
		//=========================================================
		// *********************************************
		// * Submitted by Mike Shaffer
		// *********************************************
		const int CB_ERR = -1;
		const int CB_FINDSTRING = 0x14C;

		public static bool ComboTextFind(ref Control ControlName, int KeyAscii)
		{
			bool ComboTextFind = false;
			ComboTextFind = IncrLookup(ref ControlName, ref KeyAscii);
			return ComboTextFind;
		}
		// vbPorter upgrade warning: ControlName As Control	OnRead(FCComboBox)
		public static bool IncrLookup(ref Control ControlName, ref int ky)
		{
			bool IncrLookup = false;
			// vbPorter upgrade warning: pboolValidChange As object	OnWrite(bool)
			object pboolValidChange;
			// - "AutoDim"
			// 
			// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
			// :::                                                                   :::'
			// :::   Performs incremental matching during ComboBox keyin             :::'
			// :::                                                                   :::'
			// :::   To use, place this line in the combo box KeyPress event:        :::'
			// :::                                                                   :::'
			// :::         IncrLookup {comboname}, KeyAscii                          :::'
			// :::                                                                   :::'
			// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
			// :::                                                                   :::'
			// :::  Passed:                                                          :::'
			// :::                                                                   :::'
			// :::     ky        :=     the ascii keystroke value                    :::'
			// :::                                                                   :::'
			// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
			// 
			string s = "";
			int l = 0;
			// vbPorter upgrade warning: c As FCComboBox	OnWrite(Control)
			FCComboBox c;
			// 
			pboolValidChange = true;
			if (ControlName is FCComboBox)
			{
				c = ControlName as FCComboBox;
				if ((FCConvert.ToInt32(c.DropDownStyle) < 2))
				{
					// Build the search string
					s = Strings.Left(c.Text, c.SelectionStart) + FCConvert.ToString(Convert.ToChar(ky));
					// 
					// Now search the combobox for the string
					IntPtr x = (IntPtr)FCConvert.ToInt32(s);
					l = modAPIsConst.SendMessage((c.Handle.ToInt32()), CB_FINDSTRING, -1, x);
					// 
					// Was the string found in the list?
					if (l != CB_ERR)
					{
						// Yes, so we set the ListIndex and highlight the text
						c.SelectedIndex = l;
						c.Text = c.Items[l].ToString();
						c.SelectionStart = s.Length;
						c.SelectionLength = c.Text.Length;
						ky = 0;
					}
					else
					{
						IncrLookup = false;
						pboolValidChange = false;
						return IncrLookup;
					}
					// 
				}
			}
			IncrLookup = true;
			pboolValidChange = false;
			return IncrLookup;
		}
	}
}
