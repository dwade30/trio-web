//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmTagInventory : BaseForm
	{
		public frmTagInventory()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTagInventory InstancePtr
		{
			get
			{
				return (frmTagInventory)Sys.GetInstance(typeof(frmTagInventory));
			}
		}

		protected frmTagInventory _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillFromTo();
		}

		private void frmTagInventory_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmTagInventory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTagInventory properties;
			//frmTagInventory.FillStyle	= 0;
			//frmTagInventory.ScaleWidth	= 3885;
			//frmTagInventory.ScaleHeight	= 2385;
			//frmTagInventory.LinkTopic	= "Form2";
			//frmTagInventory.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			FillCombo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillCombo()
		{
			int lngYear;
			lngYear = DateTime.Today.Year;
			cmbYear.AddItem(FCConvert.ToString(lngYear + 1));
			cmbYear.AddItem(FCConvert.ToString(lngYear));
			cmbYear.AddItem(FCConvert.ToString(lngYear - 1));
			cmbYear.AddItem(FCConvert.ToString(lngYear - 2));
			cmbYear.SelectedIndex = 0;
			FillFromTo();
		}

		private void FillFromTo()
		{
			int lngYear;
			lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbYear.Text)));
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from doginventory where [year] = " + FCConvert.ToString(lngYear) + " and type = 'T'  and isnull(stickernumber,'') <> '' order by stickernumber", "Clerk");
			cmbFromTag.Clear();
			cmbToTag.Clear();
			cmbFromTag.AddItem("0");
			cmbToTag.AddItem("0");
			while (!rsLoad.EndOfFile())
			{
				cmbFromTag.AddItem(FCConvert.ToString(rsLoad.Get_Fields_Int32("stickernumber")));
				cmbToTag.AddItem(FCConvert.ToString(rsLoad.Get_Fields_Int32("stickernumber")));
				rsLoad.MoveNext();
			}
			cmbFromTag.SelectedIndex = 0;
			cmbToTag.SelectedIndex = 0;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(cmbFromTag.Text) > Conversion.Val(cmbToTag.Text))
			{
				MessageBox.Show("Invalid Range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Conversion.Val(cmbToTag.Text) == 0)
			{
				MessageBox.Show("You did not pick a range of tags", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rptStickerInventory.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(cmbFromTag.Text)), FCConvert.ToInt32(Conversion.Val(cmbToTag.Text)), FCConvert.ToInt32(cmbYear.Text));
			Close();
		}
	}
}
