//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.Clerk.Marriages;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmMarriageSearchA : BaseForm, IView<IMarriageSearchViewModel>
    {
        private bool cancelling = true;

		public frmMarriageSearchA()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmMarriageSearchA(IMarriageSearchViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			cmdOwnerS = new System.Collections.Generic.List<FCButton>();
			cmdOwnerS.AddControlArrayElement(cmdOwnerS_0, 0);
            //FC:FINAL:AKV Exit button click
            //cmdOwnerS.AddControlArrayElement(cmdOwnerS_1, 1);
            cmdOwnerS.AddControlArrayElement(cmdOwnerS_2, 2);
			cmdOwnerS.AddControlArrayElement(cmdOwnerS_3, 3);
			txtFName = new System.Collections.Generic.List<FCTextBox>();
			txtFName.AddControlArrayElement(txtFName_0, 0);
			txtFName.AddControlArrayElement(txtFName_1, 1);
			txtDSearch = new System.Collections.Generic.List<FCTextBox>();
			txtDSearch.AddControlArrayElement(txtDSearch_0, 0);
			txtDSearch.AddControlArrayElement(txtDSearch_1, 1);
			txtDSearch.AddControlArrayElement(txtDSearch_2, 2);
			Label2 = new System.Collections.Generic.List<FCLabel>();
			Label2.AddControlArrayElement(Label2_0, 0);
			Label2.AddControlArrayElement(Label2_1, 1);
			Label2.AddControlArrayElement(Label2_2, 2);
			Label2.AddControlArrayElement(Label2_4, 4);
			Label2.AddControlArrayElement(Label2_5, 5);
            this.Closing += FrmMarriageSearchA_Closing;
		}

        private void FrmMarriageSearchA_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                if (ViewModel != null)
                {
                    ViewModel.Cancel();
                }
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmMarriageSearchA InstancePtr
		{
			get
			{
				return (frmMarriageSearchA)Sys.GetInstance(typeof(frmMarriageSearchA));
			}
		}

		protected frmMarriageSearchA _InstancePtr = null;
		//=========================================================
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsOSch = new clsDRWrapper();
			public clsDRWrapper rsOSch_AutoInitialized = null;
			public clsDRWrapper rsOSch
			{
				get
				{
					if ( rsOSch_AutoInitialized == null)
					{
						 rsOSch_AutoInitialized = new clsDRWrapper();
					}
					return rsOSch_AutoInitialized;
				}
				set
				{
					 rsOSch_AutoInitialized = value;
				}
			}
		public int ID;
		const int CNSTGRIDCOLID = 0;
		const int CNSTGRIDCOLGROOMLAST = 1;
		const int CNSTGRIDCOLGROOMFIRST = 2;
		const int CNSTGRIDCOLBRIDELAST = 3;
		const int CNSTGRIDCOLBRIDEFIRST = 4;

		private void chkBirthOnFile_CheckedChanged(object sender, System.EventArgs e)
		{
			// chkBirthOnFile.Value = IIf(rsOSch.Fields("BirthOnFile"), 1, 0)
		}

		private void chkDeathOnFile_CheckedChanged(object sender, System.EventArgs e)
		{
			// chkDeathOnFile.Value = IIf(rsOSch.Fields("DeathOnFile"), 1, 0)
		}

		private void chkGroomBirthOnFile_CheckedChanged(object sender, System.EventArgs e)
		{
			// chkGroomBirthOnFile.Value = IIf(rsOSch.Fields("GroomBirthOnFile"), 1, 0)
		}

		private void chkGroomDeathOnFile_CheckedChanged(object sender, System.EventArgs e)
		{
			// chkGroomDeathOnFile.Value = IIf(rsOSch.Fields("GroomDeathOnFile"), 1, 0)
		}

		private void cmdOwnerS_Click(int I, object sender, System.EventArgs e)
		{
			switch (I)
			{
				case 0:
					{
						Search();
						break;
					}
				case 1:
					{
						Close();
						break;
					}
				case 2:
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(modClerkGeneral.Statics.SearchResponse)) == string.Empty || modClerkGeneral.Statics.SearchResponse == 0)
						{
							MessageBox.Show("Marriage record must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
						modClerkGeneral.Statics.AddingMarriageCertificate = false;
						ViewModel.Select(modClerkGeneral.Statics.SearchResponse);
                        CloseWithoutCancel();
						//frmMarriages.InstancePtr.Unload();
						//frmMarriages.InstancePtr.Show(App.MainForm);
						//// End If
						//Close();
						break;
					}
				case 3:
					{
						clrSearch();
						break;
					}
			}
			//end switch
		}

		private void cmdOwnerS_Click(object sender, System.EventArgs e)
		{
			int index = cmdOwnerS.GetIndex((FCButton)sender);
			cmdOwnerS_Click(index, sender, e);
		}

		public void clrSearch()
		{
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCComboBox)
					(obj as FCComboBox).Clear();
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is FCCheckBox)
					(obj as FCCheckBox).Value = FCCheckBox.ValueSettings.Unchecked;
				if (obj is FCMaskedTextBox)
				{
					(obj as FCMaskedTextBox).Mask = "";
					(obj as FCMaskedTextBox).Text = "";
					(obj as FCMaskedTextBox).Mask = "###-####";
				}
			}
		}

		public void Search()
		{
			int I;
			string strSQLtemp;
			string strSQL;
			object tempOname;
			string strTemp = "";
			I = 0;
			strSQLtemp = "";
			// vbPorter upgrade warning: SA As object	OnWrite(string)
			object[] SA = new object[6 + 1];
			if (fecherFoundation.Strings.Trim(txtDSearch[0].Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(txtDSearch[0].Text);
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				SA[0] = "(GroomsLastName like '" + strTemp + "%' or partyabirthname like '" + strTemp + "%')";
			}
			else
			{
				SA[0] = "";
			}
			if (fecherFoundation.Strings.Trim(txtFName[0].Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(txtFName[0].Text);
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				SA[1] = "GroomsFirstName like '" + strTemp + "%'";
			}
			else
			{
				SA[1] = "";
			}
			if (fecherFoundation.Strings.Trim(txtDSearch[1].Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(txtDSearch[1].Text);
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				SA[2] = "BridesMaidenSurName like '" + strTemp + "%'";
			}
			else
			{
				SA[2] = "";
			}
			if (fecherFoundation.Strings.Trim(txtFName[1].Text) != string.Empty)
			{
				strTemp = fecherFoundation.Strings.Trim(txtFName[1].Text);
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				SA[3] = "BridesFirstName like '" + strTemp + "%'";
			}
			else
			{
				SA[3] = "";
			}
			if (fecherFoundation.Strings.Trim(txtDSearch[2].Text) != string.Empty)
			{
				strTemp = txtDSearch[2].Text;
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				SA[4] = "CityMarried like '" + strTemp + "%'";
			}
			else
			{
				SA[4] = "";
			}
			strSQL = "SELECT * FROM Marriages WHERE ";
			for (I = 0; I <= 4; I++)
			{
				if (FCConvert.ToString(SA[I]) != "")
					strSQLtemp += SA[I] + " AND ";
			}
			// I
			if (this.chkBirthOnFile.CheckState == CheckState.Checked)
				strSQLtemp += "BirthOnFile = 1 AND ";
			if (this.chkDeathOnFile.CheckState == CheckState.Checked)
				strSQLtemp += "DeathOnFile = 1 AND ";
			if (this.chkGroomBirthOnFile.CheckState == CheckState.Checked)
				strSQLtemp += "GroomBirthOnFile = 1 AND ";
			if (this.chkGroomDeathOnFile.CheckState == CheckState.Checked)
				strSQLtemp += "GroomDeathOnFile = 1 AND ";
			if (strSQLtemp == "")
			{
				strSQL = "SELECT * FROM Marriages ORDER BY  GroomsLastName, GroomsFirstName ";
			}
			else
			{
				strSQL += strSQLtemp;
				strSQL = Strings.Left(strSQL, (strSQL.Length - 5));
				// ****  This is the search string
				// strSQL = strSQL & ";"
			}
			rsOSch.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			int lngRow;
			if (rsOSch.EndOfFile() != true && rsOSch.BeginningOfFile() != true)
			{
				rsOSch.MoveLast();
				rsOSch.MoveFirst();
				while (!rsOSch.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLID, FCConvert.ToString(rsOSch.Get_Fields_Int32("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLGROOMLAST, FCConvert.ToString(rsOSch.Get_Fields_String("Groomslastname")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLGROOMFIRST, FCConvert.ToString(rsOSch.Get_Fields_String("GroomsFirstName")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLBRIDELAST, FCConvert.ToString(rsOSch.Get_Fields_String("bridesMaidensurname")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLBRIDEFIRST, FCConvert.ToString(rsOSch.Get_Fields_String("bridesfirstname")));
					rsOSch.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("No Matching Records ", "Nothing Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			if (Grid.Rows < 1)
				return;
			modGNBas.Statics.gboolSearchPrint = true;
			modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
			modClerkGeneral.Statics.utPrintInfo.TblName = "Marriages";
			strSQL = "Select * from Marriages where ID = " + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
			modGNBas.Statics.rsMarriageCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			this.Hide();
			frmPrintVitalRec.InstancePtr.Show(App.MainForm);
		}

		private void frmMarriageSearchA_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
		}

		private void frmMarriageSearchA_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmMarriageSearchA_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
            //FC:FINAL:AM:#3909 - code not reached in VB6
			//else if (KeyAscii == Keys.Return)
			//{
			//	KeyAscii = (Keys)0;
			//	Support.SendKeys("{TAB}", false);
			//}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmMarriageSearchA_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetupGrid();
		}

		private void frmMarriageSearchA_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void Grid_ClickEvent(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			if (rsOSch.EndOfFile() != true && rsOSch.BeginningOfFile() != true)
			{
				rsOSch.MoveLast();
				rsOSch.MoveFirst();
			}
			modClerkGeneral.Statics.SearchResponse = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
			rsOSch.FindFirstRecord("ID", Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
			if (rsOSch.NoMatch == true)
				return;
			txtDSearch[0].Text = FCConvert.ToString(rsOSch.Get_Fields_String("GroomsLastName"));
			txtFName[0].Text = FCConvert.ToString(rsOSch.Get_Fields_String("GroomsFirstName"));
			txtDSearch[1].Text = FCConvert.ToString(rsOSch.Get_Fields_String("BridesMaidenSurname"));
			txtFName[1].Text = FCConvert.ToString(rsOSch.Get_Fields_String("BridesFirstName"));
			txtDSearch[2].Text = FCConvert.ToString(rsOSch.Get_Fields_String("CityMarried"));
			chkBirthOnFile.CheckState = (CheckState)((rsOSch.Get_Fields_Boolean("BirthOnFile")) ? 1 : 0);
			chkDeathOnFile.CheckState = (CheckState)((rsOSch.Get_Fields_Boolean("DeathOnFile")) ? 1 : 0);
			chkGroomBirthOnFile.CheckState = (CheckState)((rsOSch.Get_Fields_Boolean("GroomBirthOnFile")) ? 1 : 0);
			chkGroomDeathOnFile.CheckState = (CheckState)((rsOSch.Get_Fields_Boolean("GroomDeathOnFile")) ? 1 : 0);
		}

		private void txtDSearch_Enter(int I, object sender, System.EventArgs e)
		{
			txtDSearch[I].SelectionStart = 0;
			txtDSearch[I].SelectionLength = txtDSearch[I].Text.Length;
		}

		private void txtDSearch_Enter(object sender, System.EventArgs e)
		{
			int index = txtDSearch.GetIndex((FCTextBox)sender);
			txtDSearch_Enter(index, sender, e);
		}

		private void SetupGrid()
		{
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLGROOMLAST, "Party A Last");
			Grid.TextMatrix(0, CNSTGRIDCOLGROOMFIRST, "Party A First");
			Grid.TextMatrix(0, CNSTGRIDCOLBRIDELAST, "Party B Last");
			Grid.TextMatrix(0, CNSTGRIDCOLBRIDEFIRST, "Party B First");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLGROOMLAST, FCConvert.ToInt32(0.23 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLGROOMFIRST, FCConvert.ToInt32(0.23 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLBRIDELAST, FCConvert.ToInt32(0.23 * GridWidth));
		}

        public IMarriageSearchViewModel ViewModel { get; set; }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

		private void cmdNew_Click(object sender, EventArgs e)
		{
			modClerkGeneral.Statics.AddingMarriageCertificate = true;
			ViewModel.Select(0);
			CloseWithoutCancel();
		}
	}
}
