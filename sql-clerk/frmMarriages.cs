//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.ObjectModel;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication;
using SharedApplication.CentralDocuments;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Marriages;
using TWSharedLibrary;

namespace TWCK0000
{
	public partial class frmMarriages : BaseForm, IView<IMarriageViewModel>
    {
        private bool cancelling = true;
		public frmMarriages()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmMarriages(IMarriageViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.Closing += FrmMarriages_Closing;
		}

        private void FrmMarriages_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmMarriages InstancePtr
		{
			get
			{
				return (frmMarriages)Sys.GetInstance(typeof(frmMarriages));
			}
		}

		protected frmMarriages _InstancePtr = null;
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool MarriageFormLoaded;
		bool OKtoSave;
		bool EdittingRecord;
		int intDataChanged;
		int lngMarriageID;
		// 09/24/2003 State said no more all caps so we commented out code that does so
		const int CNSTPRINTINTENTIONS = 1;
		const int CNSTPRINTLICENSE = 0;
		private string strDefCounty = "";

		private void gridTownCode_ChangeEdit(object sender, System.EventArgs e)
		{
			intDataChanged = 1;
		}

		private void imgDocuments_Click(object sender, System.EventArgs e)
		{
			if (lngMarriageID <= 0)
				return;
			if (imgDocuments.Visible == true)
			{
				ViewDocs();
			}
		}

        private void ViewDocs()
        {
            if (lngMarriageID <= 0) return;

            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer("Marriages", "Clerk", "Marriage",
                lngMarriageID, "", true, false));
            var documentHeaders = StaticSettings.GlobalCommandDispatcher.Send(new GetHeadersByReferenceCommand()).Result;
            imgDocuments.Visible = documentHeaders.Count > 0;
        }

        private void SetupMarriageNumberCombos()
		{
			cboGroomMarriageNumber.Clear();
			cboGroomMarriageNumber.AddItem("");
			cboGroomMarriageNumber.AddItem("First");
			cboGroomMarriageNumber.AddItem("Second");
			cboGroomMarriageNumber.AddItem("Third");
			cboGroomMarriageNumber.AddItem("Fourth");
			cboGroomMarriageNumber.AddItem("Fifth");
			cboGroomMarriageNumber.AddItem("Sixth");
			cboGroomMarriageNumber.AddItem("Seventh");
			cboGroomMarriageNumber.AddItem("Eighth");
			cboGroomMarriageNumber.AddItem("Ninth");
			cboGroomMarriageNumber.AddItem("Tenth");
			cboGroomMarriageNumber.AddItem("Eleventh");
			cboGroomMarriageNumber.AddItem("Twelfth");
			cboGroomMarriageNumber.AddItem("Thirteenth");
			cboGroomMarriageNumber.AddItem("Fourteenth");
			cboGroomMarriageNumber.AddItem("Fifteenth");
			cboGroomMarriageNumber.AddItem("Sixteenth");
			cboGroomMarriageNumber.AddItem("Seventeenth");
			cboGroomMarriageNumber.AddItem("Eighteenth");
			cboGroomMarriageNumber.AddItem("Nineteenth");
			cboGroomMarriageNumber.AddItem("Twentieth");
			cboBridesMarriageNumber.Clear();
			cboBridesMarriageNumber.AddItem("");
			cboBridesMarriageNumber.AddItem("First");
			cboBridesMarriageNumber.AddItem("Second");
			cboBridesMarriageNumber.AddItem("Third");
			cboBridesMarriageNumber.AddItem("Fourth");
			cboBridesMarriageNumber.AddItem("Fifth");
			cboBridesMarriageNumber.AddItem("Sixth");
			cboBridesMarriageNumber.AddItem("Seventh");
			cboBridesMarriageNumber.AddItem("Eighth");
			cboBridesMarriageNumber.AddItem("Ninth");
			cboBridesMarriageNumber.AddItem("Tenth");
			cboBridesMarriageNumber.AddItem("Eleventh");
			cboBridesMarriageNumber.AddItem("Twelfth");
			cboBridesMarriageNumber.AddItem("Thirteenth");
			cboBridesMarriageNumber.AddItem("Fourteenth");
			cboBridesMarriageNumber.AddItem("Fifteenth");
			cboBridesMarriageNumber.AddItem("Sixteenth");
			cboBridesMarriageNumber.AddItem("Seventeenth");
			cboBridesMarriageNumber.AddItem("Eighteenth");
			cboBridesMarriageNumber.AddItem("Nineteenth");
			cboBridesMarriageNumber.AddItem("Twentieth");
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGNBas.Statics.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblregionS where townnumber > 0 order by townnumber", "CentralData");
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("townnumber") + ";" + clsLoad.Get_Fields_String("townname") + "\t" + clsLoad.Get_Fields("townnumber") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				strTemp = "0";
				gridTownCode.Visible = false;
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}

		private void chkGroomDomesticPartner_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkGroomDomesticPartner.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblGroomDomesticPartnerYearRegistered.Enabled = true;
				txtGroomDomesticPartnerYearRegistered.Enabled = true;
			}
			else
			{
				lblGroomDomesticPartnerYearRegistered.Enabled = false;
				txtGroomDomesticPartnerYearRegistered.Enabled = false;
			}
		}

		private void chkBrideDomesticPartner_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkBrideDomesticPartner.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblBrideDomesticPartnerYearRegistered.Enabled = true;
				txtBrideDomesticPartnerYearRegistered.Enabled = true;
			}
			else
			{
				lblBrideDomesticPartnerYearRegistered.Enabled = false;
				txtBrideDomesticPartnerYearRegistered.Enabled = false;
			}
		}

		private void SetUpPartyTypeCombos()
		{
			cmbPartyAType.Clear();
			cmbPartyBType.Clear();
			cmbPartyAType.AddItem("Spouse");
			cmbPartyAType.ItemData(cmbPartyAType.NewIndex, modGNBas.CNSTMARRIAGEPARTYTYPESPOUSE);
			cmbPartyBType.AddItem("Spouse");
			cmbPartyBType.ItemData(cmbPartyBType.NewIndex, modGNBas.CNSTMARRIAGEPARTYTYPESPOUSE);
			cmbPartyAType.AddItem("Bride");
			cmbPartyAType.ItemData(cmbPartyAType.NewIndex, modGNBas.CNSTMARRIAGEPARTYTYPEBRIDE);
			cmbPartyBType.AddItem("Bride");
			cmbPartyBType.ItemData(cmbPartyBType.NewIndex, modGNBas.CNSTMARRIAGEPARTYTYPEBRIDE);
			cmbPartyAType.AddItem("Groom");
			cmbPartyAType.ItemData(cmbPartyAType.NewIndex, modGNBas.CNSTMARRIAGEPARTYTYPEGROOM);
			cmbPartyBType.AddItem("Groom");
			cmbPartyBType.ItemData(cmbPartyBType.NewIndex, modGNBas.CNSTMARRIAGEPARTYTYPEGROOM);
		}

		private void SetupPartyGenderCombos()
		{
			cmbPartyAGender.Clear();
			cmbPartyBGender.Clear();
			cmbPartyAGender.AddItem(" ");
			cmbPartyAGender.AddItem("M");
			cmbPartyAGender.AddItem("F");
			cmbPartyBGender.AddItem(" ");
			cmbPartyBGender.AddItem("M");
			cmbPartyBGender.AddItem("F");
		}

		private void lblCopyGroomsInfo_Click(object sender, System.EventArgs e)
		{
			txtBridesState.Text = txtGroomsState.Text;
			txtBridesCity.Text = txtGroomsCity.Text;
			txtBridesStreetAddress.Text = txtGroomsStreetName.Text;
			txtBridesStreetNumber.Text = txtGroomsStreetNumber.Text;
			txtBridesCounty.Text = txtGroomsCounty.Text;
			txtBrideZip.Text = txtGroomZip.Text;
			txtState2.Focus();
		}

		private void mnuEditTitles_Click(object sender, System.EventArgs e)
		{
			string strTemp;
			strTemp = txtTitleOfPersonPerforming.Text;
			// save because setting up will clear the combo
			frmTitles.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			SetupTitleCombo();
			txtTitleOfPersonPerforming.Text = strTemp;
		}

		private void mnuEditTown_Click(object sender, System.EventArgs e)
		{
			frmStateTownInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuViewDocs_Click(object sender, System.EventArgs e)
		{
			ViewDocs();
		}

		private void txtBrideCourtName_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtBrideFormerSpouseName_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtBridesCity_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtBridesCity.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtBridesCounty_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "C";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtBridesCounty.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtBridesDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtBridesDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtBridesDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtBridesDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtBridesDOB.Text = strD;
						if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Now, FCConvert.ToDateTime(strD)) > 0)
						{
							MessageBox.Show("This birth date is in the future and should be changed", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
			}
			else if (Information.IsDate(fecherFoundation.Strings.Trim(txtBridesDOB.Text)))
			{
				if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Now, FCConvert.ToDateTime(fecherFoundation.Strings.Trim(txtBridesDOB.Text))) > 0)
				{
					MessageBox.Show("This birth date is in the future and should be changed", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void txtBridesFathersBirthplace_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboBridesMarriageNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboBridesMarriageNumber.SelectedIndex > 1)
			{
				lstBridesMarriageEndedHow.Enabled = true;
				txtBridesMarriageEndedWhen.Enabled = true;
				lstBridesMarriageEndedHow.TabStop = true;
				txtBridesMarriageEndedWhen.TabStop = true;
				Label49.Enabled = true;
				Label50.Enabled = true;
				Label51.Enabled = true;
				lstBridesMarriageEndedHow.SelectedIndex = 1;
				Label66.Enabled = true;
				Label67.Enabled = true;
				txtBrideCourtName.Enabled = true;
				txtBrideFormerSpouseName.Enabled = true;
			}
			else
			{
				lstBridesMarriageEndedHow.Enabled = false;
				txtBridesMarriageEndedWhen.Enabled = false;
				lstBridesMarriageEndedHow.TabStop = false;
				txtBridesMarriageEndedWhen.TabStop = false;
				Label49.Enabled = false;
				Label50.Enabled = false;
				Label51.Enabled = false;
				lstBridesMarriageEndedHow.SelectedIndex = 0;
				txtBridesMarriageEndedWhen.Text = string.Empty;
				Label66.Enabled = false;
				Label67.Enabled = false;
				txtBrideCourtName.Enabled = false;
				txtBrideFormerSpouseName.Enabled = false;
				txtBrideCourtName.Text = "";
				txtBrideFormerSpouseName.Text = "";
			}
			intDataChanged += 1;
		}

		private void txtBridesFathersBirthplace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtBridesFathersBirthplace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtBridesMarriageEndedWhen_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtBridesMarriageEndedWhen.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtBridesMarriageEndedWhen.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtBridesMarriageEndedWhen.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtBridesMarriageEndedWhen.Text = strD;
					}
				}
			}
		}

		private void txtBridesMothersBirthplace_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cboGroomMarriageNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboGroomMarriageNumber.SelectedIndex > 1)
			{
				Label54.Enabled = true;
				Label55.Enabled = true;
				Label53.Enabled = true;
				Label64.Enabled = true;
				Label65.Enabled = true;
				lstGroomsMarriageEndedHow.Enabled = true;
				txtGroomsMarriageEndedWhen.Enabled = true;
				lstGroomsMarriageEndedHow.TabStop = true;
				txtGroomsMarriageEndedWhen.TabStop = true;
				lstGroomsMarriageEndedHow.SelectedIndex = 1;
				txtGroomCourtName.Enabled = true;
				txtGroomFormerSpouseName.Enabled = true;
			}
			else
			{
				lstGroomsMarriageEndedHow.Enabled = false;
				txtGroomsMarriageEndedWhen.Enabled = false;
				lstGroomsMarriageEndedHow.TabStop = false;
				txtGroomsMarriageEndedWhen.TabStop = false;
				Label54.Enabled = false;
				Label55.Enabled = false;
				Label53.Enabled = false;
				Label64.Enabled = false;
				Label65.Enabled = false;
				txtGroomCourtName.Enabled = false;
				txtGroomFormerSpouseName.Enabled = false;
				txtGroomCourtName.Text = "";
				txtGroomFormerSpouseName.Text = "";
				lstGroomsMarriageEndedHow.SelectedIndex = 0;
				txtGroomsMarriageEndedWhen.Text = string.Empty;
			}
			intDataChanged += 1;
		}

		private void cboGroomMarriageNumber_Enter(object sender, System.EventArgs e)
		{
			tabMarriages.SelectedIndex = 1;
		}

		private void txtBridesMothersBirthplace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtBridesMothersBirthplace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtCityMarried_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCityMarried.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtCityofIssue_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCityofIssue.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtCountyMarried_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "C";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCountyMarried.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtDateClerkFiled_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateClerkFiled.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateClerkFiled.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateClerkFiled.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateClerkFiled.Text = strD;
					}
				}
			}
		}

		private void txtDateIntentionsFiled_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateIntentionsFiled.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateIntentionsFiled.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateIntentionsFiled.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateIntentionsFiled.Text = strD;
					}
				}
			}
            //FC:FINAL:AM:#3905 - moved code from Leave
            DateTime XX;
            if (Information.IsDate(txtDateIntentionsFiled.Text))
            {
                XX = fecherFoundation.DateAndTime.DateAdd("d", 90, fecherFoundation.DateAndTime.DateValue(txtDateIntentionsFiled.Text));
                txtDateLicenseValid.Text = Strings.Format(XX, "MM/dd/yyyy");
            }
        }

		private void txtDateLicenseIssued_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateLicenseIssued.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateLicenseIssued.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateLicenseIssued.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateLicenseIssued.Text = strD;
					}
				}
			}
		}

		private void txtDateOfCommision_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateOfCommision.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateOfCommision.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateOfCommision.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateOfCommision.Text = strD;
					}
				}
			}
		}

		private void txtGroomCourtName_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtGroomFormerSpouseName_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtGroomsCity_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtGroomsCity.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtGroomsCounty_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "C";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtGroomsCounty.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtGroomsDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtGroomsDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtGroomsDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtGroomsDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtGroomsDOB.Text = strD;
						if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Now, FCConvert.ToDateTime(strD)) > 0)
						{
							MessageBox.Show("This birth date is in the future and should be changed", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
			}
			else if (Information.IsDate(fecherFoundation.Strings.Trim(txtGroomsDOB.Text)))
			{
				if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Now, FCConvert.ToDateTime(fecherFoundation.Strings.Trim(txtGroomsDOB.Text))) > 0)
				{
					MessageBox.Show("This birth date is in the future and should be changed", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void txtGroomsFathersBirthplace_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtGroomsFathersBirthplace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtGroomsFathersBirthplace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtGroomsMarriageEndedWhen_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtGroomsMarriageEndedWhen.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtGroomsMarriageEndedWhen.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtGroomsMarriageEndedWhen.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtGroomsMarriageEndedWhen.Text = strD;
					}
				}
			}
		}

		private void txtGroomsMothersBirthplace_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtGroomsMothersBirthplace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtGroomsMothersBirthplace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtResidenceofPersonPerforming_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtResidenceofPersonPerforming.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtState_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtState_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtState.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtState2_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkVoided_CheckedChanged(object sender, System.EventArgs e)
		{
			mebVoided.Visible = chkVoided.CheckState == Wisej.Web.CheckState.Checked;
			if (chkVoided.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				mebVoided.Text = "";
			}
		}

		private void cmdFind_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			modGNBas.Statics.boolMarriageClerkFind = true;
			frmListRegistrar.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrRegistrar) == string.Empty)
				return;
			rs.OpenRecordset("Select * from DefaultRegistrarNames where ID = " + modGNBas.Statics.gstrRegistrar, modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				txtNameOfClerk.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MI"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("LastName")));
				modRegistry.SaveRegistryKey("CKLastClerkName", txtNameOfClerk.Text);
			}
		}

		private void cmdMoveNext_Click()
		{
			//FC:TODO
			//if (!FCConvert.ToBoolean(datMarriage.Recordset.Fields["EndOfFile"].Value)) {
			//	datMarriage.Recordset.MoveNext();
			//} else {
			//	datMarriage.Recordset.MoveFirst();
			//}
		}
		// vbPorter upgrade warning: ControlName As MaskEdBox	OnWrite(MSMask.MaskEdBox)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object CheckForValidDate(T2KDateBox ControlName, int intTab, string strName)
		{
			object CheckForValidDate = null;
			string strTemp;
			strTemp = ControlName.Text;
            //FC:FINAL:AM:#2434 - check for 00/00/0000            
            if (strTemp == "00/00/0000")
            {
                strTemp = string.Empty;
            }
            else
            {
                strTemp = Strings.Replace(strTemp, "/", "", 1, -1, CompareConstants.vbTextCompare);
                strTemp = fecherFoundation.Strings.Trim(strTemp);
            }
            if (strTemp != string.Empty)
			{
				if (Information.IsDate(Strings.Format(ControlName.Text.Replace("/", ""), "##/##/####")) == true)
				{
					OKtoSave = true;
				}
				else
				{
					if (ControlName.BackColor == SystemColors.Info/*0x80000018*/)
					{
						MessageBox.Show("Invalid " + strName + " Date. Please enter a valid date.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("Invalid " + strName + " Date. Please enter a valid date or no date or all.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					OKtoSave = false;
					tabMarriages.SelectedIndex = intTab;
					ControlName.Focus();
					CheckForValidDate = true;
				}
			}
			return CheckForValidDate;
		}

		public void OKToSaveData()
		{
			if (chkRequired.CheckState == CheckState.Unchecked)
			{
				OKtoSave = true;
				if (fecherFoundation.Strings.Trim(txtGroomsFirstName.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party A First Name.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtGroomsFirstName.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(txtGroomsLastName.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party A Last Name.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtGroomsLastName.Focus();
					goto TheTag;
				}
				if (FCConvert.ToBoolean(CheckForValidDate(mebVoided, 1, "Voided")))
					goto TheTag;
				if (fecherFoundation.Strings.Trim(txtState.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party A Birthplace.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtState.Focus();
					goto TheTag;
				}
				if (Conversion.Val(txtGroomsAge.Text) != 0)
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party A Age.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtGroomsAge.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(txtGroomsCity.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party A City.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtGroomsCity.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(txtGroomsStreetName.Text) == "")
				{
					MessageBox.Show("Please enter a valid street name for Party A.", "Invalid Street Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtGroomsStreetName.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(txtGroomsStreetNumber.Text) == "")
				{
					MessageBox.Show("Please enter a valid street number for Party A", "Invalid Street Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtGroomsStreetNumber.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(this.txtGroomsState.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party A State.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtGroomsState.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(this.txtGroomsCounty.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					// MsgBox "Please enter a valid Groom's County.", vbOKOnly, "Data Error"
					if (MessageBox.Show("There is no county for Party A.  If this is not an international address, a county is required" + "\r\n" + "Is this an international address?", "No County", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
					}
					else
					{
						OKtoSave = false;
						tabMarriages.SelectedIndex = 0;
						txtGroomsCounty.Focus();
						goto TheTag;
					}
				}
				if (fecherFoundation.Strings.Trim(txtBridesFirstName.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party B First Name.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtBridesFirstName.Focus();
					goto TheTag;
				}
				// If Trim(txtBridesMaidenSurname.Text) <> "" Then
				// OKtoSave = True
				// Else
				// MsgBox "Please enter a valid Party B Birth Name.", vbOKOnly, "Data Error"
				// OKtoSave = False
				// tabMarriages.Tab = 0
				// txtBridesMaidenSurname.SetFocus
				// GoTo TheTag
				// End If
				if (fecherFoundation.Strings.Trim(txtBridesCurrentLastName.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party B Current Last Name.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtBridesCurrentLastName.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(txtState2.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Party B BirthPlace.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtState2.Focus();
					goto TheTag;
				}
				if (Conversion.Val(txtBridesAge.Text) != 0)
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Bride's Age.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtBridesAge.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(txtBridesCity.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Bride's City.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtBridesCity.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(txtBridesStreetAddress.Text) == "")
				{
					MessageBox.Show("Please enter a valid bride's street name.", "Invalid Street Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtBridesStreetNumber.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(txtBridesStreetNumber.Text) == "")
				{
					MessageBox.Show("Please enter a valid bride's street number.", "Invalid Street Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtBridesStreetNumber.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(this.txtBridesState.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					MessageBox.Show("Please enter a valid Bride's State.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 0;
					txtBridesState.Focus();
					goto TheTag;
				}
				if (fecherFoundation.Strings.Trim(this.txtBridesCounty.Text) != "")
				{
					OKtoSave = true;
				}
				else
				{
					if (MessageBox.Show("There is no Bride's County.  If this is not an international address, a county is required" + "\r\n" + "Is this an international address?", "No County", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
					}
					else
					{
						// MsgBox "Please enter a valid Bride's County.", vbOKOnly, "Data Error"
						OKtoSave = false;
						tabMarriages.SelectedIndex = 0;
						txtBridesCounty.Focus();
						goto TheTag;
					}
				}
				if (cboGroomMarriageNumber.SelectedIndex > 0)
				{
					if (cboGroomMarriageNumber.SelectedIndex > 1)
					{
						if (lstGroomsMarriageEndedHow.Text == string.Empty)
						{
							MessageBox.Show("Enter how Groom's marriage ended.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							OKtoSave = false;
							tabMarriages.SelectedIndex = 1;
							lstGroomsMarriageEndedHow.Focus();
							goto TheTag;
						}
					}
				}
				else
				{
					MessageBox.Show("Please enter a valid Groom's marriage number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 1;
					cboGroomMarriageNumber.Focus();
					goto TheTag;
				}
				if (cboBridesMarriageNumber.SelectedIndex > 0)
				{
					if (cboBridesMarriageNumber.SelectedIndex > 1)
					{
						if (lstBridesMarriageEndedHow.Text == string.Empty)
						{
							MessageBox.Show("Enter how Bride's marriage ended.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							OKtoSave = false;
							tabMarriages.SelectedIndex = 1;
							lstBridesMarriageEndedHow.Focus();
							goto TheTag;
						}
					}
				}
				else
				{
					MessageBox.Show("Please enter a valid Bride's marriage number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 1;
					cboBridesMarriageNumber.Focus();
					goto TheTag;
				}
				if (txtDateIntentionsFiled.Text == "")
				{
					MessageBox.Show("Please eneter a date for intentions filed.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					OKtoSave = false;
					tabMarriages.SelectedIndex = 1;
					txtDateIntentionsFiled.Focus();
					goto TheTag;
				}
				// If IsDate(Format(txtDateClerkFiled.Text, "##/##/####")) = True Then
				// OKtoSave = True
				// Else
				// MsgBox "Please enter a valid Date Filed", vbOKOnly, "Data Error"
				// OKtoSave = False
				// tabMarriages.Tab = 1
				// txtDateClerkFiled.SetFocus
				// GoTo TheTag
				// End If
				// 
				// If Trim(Me.txtNameOfClerk.Text) <> "" Then
				// OKtoSave = True
				// Else
				// MsgBox "Please enter a valid Clerk Name.", vbOKOnly, "Data Error"
				// OKtoSave = False
				// tabMarriages.Tab = 1
				// txtNameOfClerk.SetFocus
				// GoTo TheTag
				// End If
			}
			else
			{
				OKtoSave = true;
			}
			if (modGNBas.Statics.ClerkDefaults.boolFileDuplicateWarning && fecherFoundation.Strings.Trim(txtFileNumber.Text) != string.Empty)
			{
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsLoad.OpenRecordset("select ID FROM MARRIAGES WHERE filenumber = '" + txtFileNumber.Text + "' and ID <> " + FCConvert.ToString(lngMarriageID), "twCK0000.VB1");
				if (!clsLoad.EndOfFile())
				{
					if (MessageBox.Show("There is already another marriage record with this file number" + "\r\n" + "Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						OKtoSave = false;
					}
				}
			}
			TheTag:
			;
		}

		private void frmMarriages_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (EdittingRecord == false)
			{
				if (KeyCode == Keys.Back)
				{
					// backspace
					EdittingRecord = true;
				}
				else if (KeyCode >= Keys.D0 && KeyCode <= Keys.NumPad9)
				{
					EdittingRecord = true;
				}
				else if (KeyCode == (Keys)190)
				{
					// period
					EdittingRecord = true;
				}
				else
				{
					EdittingRecord = false;
				}
			}
			if (KeyCode == Keys.Escape)
				mnuQuit_Click();
		}

		private void frmMarriages_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmMarriages_Resize(object sender, System.EventArgs e)
		{
			ResizeVsFlexGrid1();
			ResizeGridTownCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			MarriageFormLoaded = false;
			modGNBas.Statics.boolMarriageOpen = false;
			intDataChanged = FCConvert.ToInt16(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "mnuSave_Click");
			//if (modCashReceiptingData.Statics.bolFromWindowsCR)
			//{
			//	modGNBas.Statics.gboolEndApplication = true;
			//}
			//MDIParent.InstancePtr.Show();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void lstBridesMarriageEndedHow_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void lstGroomsMarriageEndedHow_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtBridesDOB_Enter(object sender, System.EventArgs e)
		{
			txtBridesDOB.SelectionStart = 0;
		}

		private void txtBridesMarriageEndedWhen_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtBridesMarriageEndedWhen_Enter(object sender, System.EventArgs e)
		{
			txtBridesMarriageEndedWhen.SelectionStart = 0;
		}

		private void txtCeremonyDate_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtCeremonyDate_Enter(object sender, System.EventArgs e)
		{
			txtCeremonyDate.SelectionStart = 0;
		}

		private void txtCeremonyDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtCeremonyDate.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtCeremonyDate.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtCeremonyDate.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtCeremonyDate.Text = strD;
					}
				}
			}
			if (Information.IsDate(txtCeremonyDate.Text))
			{
				if (DateAndTime.DateValue(txtCeremonyDate.Text).ToOADate() < FCConvert.ToDateTime("1/1/1892").ToOADate())
				{
					chkRequired.CheckState = Wisej.Web.CheckState.Checked;
				}
				txtActualDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateValue(txtCeremonyDate.Text), "MM/dd/yyyy");
			}
		}

		private void txtDateClerkFiled_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtDateClerkFiled_Enter(object sender, System.EventArgs e)
		{
			txtDateClerkFiled.SelectionStart = 0;
		}

		private void txtDateIntentionsFiled_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtDateIntentionsFiled_Enter(object sender, System.EventArgs e)
		{
			txtDateIntentionsFiled.SelectionStart = 0;
		}

		//private void txtDateIntentionsFiled_Leave(object sender, System.EventArgs e)
		//{
		//	DateTime XX;
		//	if (Information.IsDate(txtDateIntentionsFiled.Text))
		//	{
		//		XX = fecherFoundation.DateAndTime.DateAdd("d", 90, fecherFoundation.DateAndTime.DateValue(txtDateIntentionsFiled.Text));
		//		txtDateLicenseValid.Text = Strings.Format(XX, "MM/dd/yyyy");
		//	}
		//}

		private void txtDateLicenseIssued_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtDateLicenseIssued_Enter(object sender, System.EventArgs e)
		{
			txtDateLicenseIssued.SelectionStart = 0;
		}

		private void txtDateLicenseValid_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtDateLicenseValid_Enter(object sender, System.EventArgs e)
		{
			txtDateLicenseValid.SelectionStart = 0;
		}

		private void txtDateLicenseValid_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateLicenseValid.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateLicenseValid.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateLicenseValid.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateLicenseValid.Text = strD;
					}
				}
			}
			if (txtDateIntentionsFiled.Text == string.Empty)
				return;
			if (!Information.IsDate(txtDateIntentionsFiled.Text))
				return;
			if (!Information.IsDate(txtDateLicenseValid.Text))
				return;
			// If DateDiff("d", ConvertDateToHaveSlashes(txtDateIntentionsFiled), txtDateLicenseValid) > 90 Then
			if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(Strings.Format(txtDateIntentionsFiled.Text, "MM/dd/yyyy")), FCConvert.ToDateTime(Strings.Format(txtDateLicenseValid.Text, "MM/dd/yyyy"))) > 90)
			{
				if (MessageBox.Show("License Valid Until date is greater than 90 days. Continue?", "Marriages", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					e.Cancel = true;
				}
			}
		}

		private void txtDateOfCommision_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtDateOfCommision_Enter(object sender, System.EventArgs e)
		{
			txtDateOfCommision.SelectionStart = 0;
		}

		private void txtGroomsDOB_Enter(object sender, System.EventArgs e)
		{
			txtGroomsDOB.SelectionStart = 0;
		}

		private void FillScreen()
		{
			string strTemp = "";
			int x;
			lngMarriageID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) > 0)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("towncode"))));
			}
			txtGroomSSN.Text = FCConvert.ToString(rs.Get_Fields_String("groomssn"));
			txtBrideSSN.Text = FCConvert.ToString(rs.Get_Fields_String("bridessn"));
			chkRequired.CheckState = (CheckState)((rs.Get_Fields_Boolean("NoRequiredFields")) ? 1 : 0);
			chkConsent.CheckState = (CheckState)((rs.Get_Fields_Boolean("ConsentInfo")) ? 1 : 0);
			chkSignitureOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("SignatureOnFile")) ? 1 : 0);
			chkBirthOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("BirthOnFile")) ? 1 : 0);
			chkVerificationOfForeignBride.CheckState = (CheckState)((rs.Get_Fields_Boolean("VerifyForeignBride")) ? 1 : 0);
			chkVerificationOfForeignGroom.CheckState = (CheckState)((rs.Get_Fields_Boolean("VerifyForeignGroom")) ? 1 : 0);
			chkDeathOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("DeathOnFile")) ? 1 : 0);
			chkGroomBirthOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("GroomBirthOnFile")) ? 1 : 0);
			chkGroomDeathOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("GroomDeathOnFile")) ? 1 : 0);
			chkVoided.CheckState = (CheckState)((rs.Get_Fields_Boolean("Voided")) ? 1 : 0);
			modGNBas.Statics.intRecordNumber = FCConvert.ToInt16(rs.Get_Fields_Int32("ID"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FileNumber")))
				txtFileNumber.Text = FCConvert.ToString(rs.Get_Fields_String("FileNumber"));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsFirstName")) == false)
				txtGroomsFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsFirstName"));
			else
				txtGroomsFirstName.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsMiddleName")) == false)
				txtGroomsMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsMiddleName"));
			else
				txtGroomsMiddleName.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsLastName")) == false)
				txtGroomsLastName.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsLastName"));
			else
				txtGroomsLastName.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsDesignation")) == false)
				txtGroomsDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsDesignation"));
			else
				txtGroomsDesignation.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int16("groomsage")) == false)
				txtGroomsAge.Text = FCConvert.ToString(rs.Get_Fields_Int16("groomsage"));
			else
				txtGroomsAge.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsStreetName")) == false)
				txtGroomsStreetName.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsStreetName"));
			else
				txtGroomsStreetName.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsStreetAddress")) == false)
				txtGroomsStreetNumber.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsStreetAddress"));
			else
				txtGroomsStreetNumber.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsCity")) == false)
				txtGroomsCity.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsCity"));
			else
				txtGroomsCity.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsCounty")) == false)
				txtGroomsCounty.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsCounty"));
			else
				txtGroomsCounty.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsState")) == false)
				txtGroomsState.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsState"));
			else
				txtGroomsState.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomZip")) == false)
				txtGroomZip.Text = FCConvert.ToString(rs.Get_Fields_String("GroomZip"));
			else
				txtGroomZip.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BrideZip")) == false)
				txtBrideZip.Text = FCConvert.ToString(rs.Get_Fields_String("BrideZip"));
			else
				txtBrideZip.Text = "";
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("PartyAGender")))
			{
				cmbPartyAGender.Text = FCConvert.ToString(rs.Get_Fields_String("partyagender"));
			}
			else
			{
				cmbPartyAGender.SelectedIndex = 0;
				// blank
			}
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("PartyBGender")))
			{
				cmbPartyBGender.Text = FCConvert.ToString(rs.Get_Fields_String("PartyBGender"));
			}
			else
			{
				cmbPartyBGender.SelectedIndex = 0;
			}
			txtPartyABirthName.Text = FCConvert.ToString(rs.Get_Fields_String("PartyABirthName"));
			txtPartyBDesig.Text = FCConvert.ToString(rs.Get_Fields_String("PartyBDesignation"));
			cmbPartyAType.SelectedIndex = 0;
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("PartyAType")))
			{
				for (x = 0; x <= cmbPartyAType.Items.Count - 1; x++)
				{
					if (cmbPartyAType.ItemData(x) == Conversion.Val(rs.Get_Fields_Int32("PartyAType")))
					{
						cmbPartyAType.SelectedIndex = x;
						break;
					}
				}
				// x
			}
			cmbPartyBType.SelectedIndex = 0;
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("partybtype")))
			{
				for (x = 0; x <= cmbPartyBType.Items.Count - 1; x++)
				{
					if (cmbPartyBType.ItemData(x) == Conversion.Val(rs.Get_Fields_Int32("PartyBType")))
					{
						cmbPartyBType.SelectedIndex = x;
						break;
					}
				}
				// x
			}
			txtState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("GroomsBirthplace")));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsDOB")) == false)
				txtGroomsDOB.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("GroomsDOB")));
			else
				txtGroomsDOB.Text = "";
			if (fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("VoidedDate")) == false)
				mebVoided.Text = Strings.Format(rs.Get_Fields("VoidedDate"), "MM/dd/yyyy");
			else
				mebVoided.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsFathersName")) == false)
				txtGroomsFathersName.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsFathersName"));
			else
				txtGroomsFathersName.Text = "";
			txtGroomsFathersBirthplace.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("GroomsFathersBirthplace")));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsMothersName")) == false)
				txtGroomsMothersName.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsMothersName"));
			else
				txtGroomsMothersName.Text = "";
			txtGroomsMothersBirthplace.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("GroomsMothersBirthplace")));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesFirstName")) == false)
				txtBridesFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("BridesFirstName"));
			else
				txtBridesFirstName.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesMiddleName")) == false)
				txtBridesMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("BridesMiddleName"));
			else
				txtBridesMiddleName.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesMaidenSurname")) == false)
				txtBridesMaidenSurname.Text = FCConvert.ToString(rs.Get_Fields_String("BridesMaidenSurname"));
			else
				txtBridesMaidenSurname.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesCurrentLastName")) == false)
				txtBridesCurrentLastName.Text = FCConvert.ToString(rs.Get_Fields_String("BridesCurrentLastName"));
			else
				txtBridesCurrentLastName.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int16("bridesage")) == false)
				txtBridesAge.Text = FCConvert.ToString(rs.Get_Fields_Int16("bridesage"));
			else
				txtBridesAge.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesStreetName")) == false)
				txtBridesStreetAddress.Text = FCConvert.ToString(rs.Get_Fields_String("BridesStreetName"));
			else
				txtBridesStreetAddress.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesStreetNumber")) == false)
				txtBridesStreetNumber.Text = FCConvert.ToString(rs.Get_Fields_String("BridesStreetNumber"));
			else
				txtBridesStreetNumber.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesCity")) == false)
				txtBridesCity.Text = FCConvert.ToString(rs.Get_Fields_String("BridesCity"));
			else
				txtBridesCity.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesCounty")) == false)
				txtBridesCounty.Text = FCConvert.ToString(rs.Get_Fields_String("BridesCounty"));
			else
				txtBridesCounty.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesState")) == false)
				txtBridesState.Text = FCConvert.ToString(rs.Get_Fields_String("BridesState"));
			else
				txtBridesState.Text = "";
			txtState2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("bridesbirthplace")));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesDOB")) == false)
				txtBridesDOB.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("BridesDOB")));
			else
				txtBridesDOB.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesFathersName")) == false)
				txtBridesFathersName.Text = FCConvert.ToString(rs.Get_Fields_String("BridesFathersName"));
			else
				txtBridesFathersName.Text = "";
			txtBridesFathersBirthplace.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("BridesFathersBirthplace")));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesMothersName")) == false)
				txtBridesMothersName.Text = FCConvert.ToString(rs.Get_Fields_String("BridesMothersName"));
			else
				txtBridesMothersName.Text = "";
			txtBridesMothersBirthplace.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("BridesMothersBirthplace")));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsMarriageNumber")) == false && FCConvert.ToString(rs.Get_Fields_String("GroomsMarriageNumber")) != "")
			{
				if (Conversion.Val(Strings.Left(FCConvert.ToString(rs.Get_Fields_String("groomsmarriagenumber")), 1)) > 0)
				{
					if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 1)
					{
						cboGroomMarriageNumber.Text = "First";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 2)
					{
						cboGroomMarriageNumber.Text = "Second";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 3)
					{
						cboGroomMarriageNumber.Text = "Third";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 4)
					{
						cboGroomMarriageNumber.Text = "Fourth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 5)
					{
						cboGroomMarriageNumber.Text = "Fifth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 6)
					{
						cboGroomMarriageNumber.Text = "Sixth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 7)
					{
						cboGroomMarriageNumber.Text = "Seventh";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 8)
					{
						cboGroomMarriageNumber.Text = "Eighth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 9)
					{
						cboGroomMarriageNumber.Text = "Ninth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 10)
					{
						cboGroomMarriageNumber.Text = "Tenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 11)
					{
						cboGroomMarriageNumber.Text = "Eleventh";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 12)
					{
						cboGroomMarriageNumber.Text = "Twelfth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 13)
					{
						cboGroomMarriageNumber.Text = "Thirteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) ==14)
					{
						cboGroomMarriageNumber.Text = "Fourteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 15)
					{
						cboGroomMarriageNumber.Text = "Fifteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 16)
					{
						cboGroomMarriageNumber.Text = "Sixteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 17)
					{
						cboGroomMarriageNumber.Text = "Seventeenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 18)
					{
						cboGroomMarriageNumber.Text = "Eigthteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 19)
					{
						cboGroomMarriageNumber.Text = "Nineteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("groomsmarriagenumber") + "  ", 2)) == 20)
					{
						cboGroomMarriageNumber.Text = "Twentieth";
					}
				}
				else
				{
					cboGroomMarriageNumber.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("GroomsMarriageNumber")));
				}
			}
			else
			{
				cboGroomMarriageNumber.SelectedIndex = 0;
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsPreviousMarriageEnded")) == false)
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("groomspreviousmarriageended"))) != string.Empty)
				{
					if (lstGroomsMarriageEndedHow.Text != string.Empty)
					{
						lstGroomsMarriageEndedHow.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsPreviousMarriageEnded"));
					}
				}
				else
				{
					lstGroomsMarriageEndedHow.SelectedIndex = 0;
				}
			}
			else
			{
				lstGroomsMarriageEndedHow.SelectedIndex = 0;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("GroomDomesticPartner")))
			{
				chkGroomDomesticPartner.CheckState = Wisej.Web.CheckState.Checked;
				if (Conversion.Val(rs.Get_Fields_Int32("GroomDomesticPartnerYearRegistered")) != 0)
				{
					txtGroomDomesticPartnerYearRegistered.Text = FCConvert.ToString(rs.Get_Fields_Int32("GroomDomesticPartnerYearRegistered"));
				}
				else
				{
					txtGroomDomesticPartnerYearRegistered.Text = "";
				}
			}
			else
			{
				chkGroomDomesticPartner.CheckState = Wisej.Web.CheckState.Unchecked;
				txtGroomDomesticPartnerYearRegistered.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("GroomsPrevMarriageEndDate")) == false)
			{
				if (FCConvert.ToString(rs.Get_Fields_String("groomsprevmarriageenddate")) == "N/A")
				{
					txtGroomsMarriageEndedWhen.Text = "";
				}
				else
				{
					txtGroomsMarriageEndedWhen.Text = FCConvert.ToString(rs.Get_Fields_String("groomsprevmarriageenddate"));
				}
				txtGroomCourtName.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsCourtName"));
				txtGroomFormerSpouseName.Text = FCConvert.ToString(rs.Get_Fields_String("GroomsFormerSpouseName"));
			}
			else
			{
				txtGroomsMarriageEndedWhen.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesMarriageNumber")) == false && FCConvert.ToString(rs.Get_Fields_String("BridesMarriageNumber")) != "")
			{
				if (Conversion.Val(Strings.Left(FCConvert.ToString(rs.Get_Fields_String("bridesmarriagenumber")), 1)) > 0)
				{
					if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 1)
					{
						cboBridesMarriageNumber.Text = "First";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 2)
					{
						cboBridesMarriageNumber.Text = "Second";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 3)
					{
						cboBridesMarriageNumber.Text = "Third";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 4)
					{
						cboBridesMarriageNumber.Text = "Fourth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 5)
					{
						cboBridesMarriageNumber.Text = "Fifth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 6)
					{
						cboBridesMarriageNumber.Text = "Sixth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 7)
					{
						cboBridesMarriageNumber.Text = "Seventh";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 8)
					{
						cboBridesMarriageNumber.Text = "Eighth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 9)
					{
						cboBridesMarriageNumber.Text = "Ninth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 10)
					{
						cboBridesMarriageNumber.Text = "Tenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 11)
					{
						cboBridesMarriageNumber.Text = "Eleventh";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 12)
					{
						cboBridesMarriageNumber.Text = "Twelfth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 13)
					{
						cboBridesMarriageNumber.Text = "Thirteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 14)
					{
						cboBridesMarriageNumber.Text = "Fourteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 15)
					{
						cboBridesMarriageNumber.Text = "Fifteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 16)
					{
						cboBridesMarriageNumber.Text = "Sixteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 17)
					{
						cboBridesMarriageNumber.Text = "Seventeenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 18)
					{
						cboBridesMarriageNumber.Text = "Eigthteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 19)
					{
						cboBridesMarriageNumber.Text = "Nineteenth";
					}
					else if (Conversion.Val(Strings.Left(rs.Get_Fields_String("bridesmarriagenumber") + "  ", 2)) == 20)
					{
						cboBridesMarriageNumber.Text = "Twentieth";
					}
				}
				else
				{
					cboBridesMarriageNumber.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("BridesMarriageNumber")));
				}
			}
			else
			{
				cboBridesMarriageNumber.SelectedIndex = 0;
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesPreviousMarriageEnded")) == false)
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("bridespreviousmarriageended"))) != string.Empty)
				{
					if (lstBridesMarriageEndedHow.Text != string.Empty)
					{
						lstBridesMarriageEndedHow.Text = FCConvert.ToString(rs.Get_Fields_String("BridesPreviousMarriageEnded"));
					}
				}
				else
				{
					lstBridesMarriageEndedHow.SelectedIndex = 0;
				}
			}
			else
			{
				lstBridesMarriageEndedHow.SelectedIndex = 0;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BrideDomesticPartner")))
			{
				chkBrideDomesticPartner.CheckState = Wisej.Web.CheckState.Checked;
				if (Conversion.Val(rs.Get_Fields_Int32("BrideDomesticPartnerYearRegistered")) != 0)
				{
					txtBrideDomesticPartnerYearRegistered.Text = FCConvert.ToString(rs.Get_Fields_Int32("BrideDomesticPartnerYearRegistered"));
				}
				else
				{
					txtBrideDomesticPartnerYearRegistered.Text = "";
				}
			}
			else
			{
				chkBrideDomesticPartner.CheckState = Wisej.Web.CheckState.Unchecked;
				txtBrideDomesticPartnerYearRegistered.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BridesPrevMarriageEndDate")) == false)
			{
				txtBridesMarriageEndedWhen.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("BridesPrevMarriageEndDate")));
				txtBrideCourtName.Text = FCConvert.ToString(rs.Get_Fields_String("BridesCourtName"));
				txtBrideFormerSpouseName.Text = FCConvert.ToString(rs.Get_Fields_String("BridesFormerSpouseName"));
			}
			else
			{
				txtBridesMarriageEndedWhen.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("DateIntentionsFiled")) == false)
			{
				txtDateIntentionsFiled.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("DateIntentionsFiled")));
			}
			else
			{
				txtDateIntentionsFiled.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("DateLicenseIssued")) == false)
			{
				txtDateLicenseIssued.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("DateLicenseIssued")));
			}
			else
			{
				txtDateLicenseIssued.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("LicenseValidUntil")) == false && FCConvert.ToString(rs.Get_Fields_String("LicenseValidUntil")) != "N/A")
			{
				if (FCConvert.ToInt32(rs.Get_Fields_String("LicenseValidUntil")) != 0)
					txtDateLicenseValid.Text = FCConvert.ToString(rs.Get_Fields_String("LicenseValidUntil"));
			}
			else
			{
				txtDateLicenseValid.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CityOfIssue")) == false)
				txtCityofIssue.Text = FCConvert.ToString(rs.Get_Fields_String("CityOfIssue"));
			else
				txtCityofIssue.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CeremonyDate")) == false)
			{
				txtCeremonyDate.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CeremonyDate")));
			}
			else
			{
				txtCeremonyDate.Text = "";
			}
			if (Information.IsDate(rs.Get_Fields("ActualDate")) && Convert.ToDateTime(rs.Get_Fields_DateTime("ActualDate")).ToOADate() != 0)
			{
				txtActualDate.Text = Strings.Format(rs.Get_Fields_DateTime("ActualDate"), "MM/dd/yyyy");
			}
			else
			{
				txtActualDate.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CityMarried")) == false)
				txtCityMarried.Text = FCConvert.ToString(rs.Get_Fields_String("CityMarried"));
			else
				txtCityMarried.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CountyMarried")) == false)
				txtCountyMarried.Text = FCConvert.ToString(rs.Get_Fields_String("CountyMarried"));
			else
				txtCountyMarried.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("PersonPerformingCeremony")) == false)
				txtPersonPerformingCeremony.Text = FCConvert.ToString(rs.Get_Fields_String("PersonPerformingCeremony"));
			else
				txtPersonPerformingCeremony.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("TitleofPersonPerforming")) == false)
				txtTitleOfPersonPerforming.Text = FCConvert.ToString(rs.Get_Fields_String("TitleofPersonPerforming"));
			else
				txtTitleOfPersonPerforming.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("DateOfLicenseForPerson")) == false)
				txtDateOfCommision.Text = FCConvert.ToString(rs.Get_Fields_String("DateOfLicenseForPerson"));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ResidenceOfPersonPerformingCeremony")) == false)
				txtResidenceofPersonPerforming.Text = FCConvert.ToString(rs.Get_Fields_String("ResidenceOfPersonPerformingCeremony"));
			else
				txtResidenceofPersonPerforming.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MailingAddressOfPersonPerformingCeremony")) == false)
				txtMailingAddressofPersonPerforming.Text = FCConvert.ToString(rs.Get_Fields_String("MailingAddressOfPersonPerformingCeremony"));
			else
				txtMailingAddressofPersonPerforming.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameOfWitnessOne")) == false)
				txtNameofWitnessOne.Text = FCConvert.ToString(rs.Get_Fields_String("NameOfWitnessOne"));
			else
				txtNameofWitnessOne.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameOfWitnessTwo")) == false)
				txtNameofWitnessTwo.Text = FCConvert.ToString(rs.Get_Fields_String("NameOfWitnessTwo"));
			else
				txtNameofWitnessTwo.Text = "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("DateClerkFiled")) == false)
			{
				txtDateClerkFiled.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("DateClerkFiled")));
			}
			else
			{
				txtDateClerkFiled.Text = "";
			}
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameOfClerk")) == false)
				txtNameOfClerk.Text = FCConvert.ToString(rs.Get_Fields_String("NameOfClerk"));
			else
				txtNameOfClerk.Text = "";
			// If Trim(txtNameOfClerk.Text) = vbNullString Then
			// Call SaveRegistryKey("CKLastClerkName", txtNameOfClerk.Text)
			// End If
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("commentS"))) != string.Empty)
			{
				ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[1 - 1];
			}
			else
			{
				ImgComment.Image = null;
			}
			// If txtBridesCounty = vbNullString Then txtBridesCounty = ClerkDefaults.county
			// If txtCountyMarried = vbNullString Then txtCountyMarried = ClerkDefaults.county
			modDirtyForm.ClearDirtyControls(this);
			intDataChanged = 0;
		}

		public void ClearScreen()
		{
			string strTemp;
			strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(modRegistry.GetRegistryKey("CKLastClerkName")));
			if (strTemp != string.Empty)
			{
				txtNameOfClerk.Text = strTemp;
			}
			lngMarriageID = 0;
			modGNBas.Statics.intRecordNumber = 0;
			txtGroomZip.Text = string.Empty;
			txtBrideZip.Text = string.Empty;
			chkRequired.CheckState = Wisej.Web.CheckState.Unchecked;
			chkConsent.CheckState = Wisej.Web.CheckState.Unchecked;
			chkSignitureOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkBirthOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkVerificationOfForeignBride.CheckState = Wisej.Web.CheckState.Unchecked;
			chkVerificationOfForeignGroom.CheckState = Wisej.Web.CheckState.Unchecked;
			chkDeathOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkGroomBirthOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkGroomDeathOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkVoided.CheckState = Wisej.Web.CheckState.Unchecked;
			txtGroomsFirstName.Text = "";
			txtGroomsMiddleName.Text = "";
			txtGroomsLastName.Text = "";
			txtGroomsDesignation.Text = "";
			txtGroomsAge.Text = "";
			txtGroomsStreetName.Text = "";
			txtGroomsStreetNumber.Text = "";
			txtGroomsCity.Text = "";
			txtGroomsCounty.Text = "";
			txtGroomsState.Text = "";
			txtGroomsDOB.Text = "";
			mebVoided.Text = "";
			txtGroomsFathersName.Text = "";
			txtGroomsMothersName.Text = "";
			txtBridesFirstName.Text = "";
			txtBridesMiddleName.Text = "";
			txtBridesMaidenSurname.Text = "";
			txtBridesCurrentLastName.Text = "";
			txtBridesAge.Text = "";
			txtBridesStreetAddress.Text = "";
			txtBridesStreetNumber.Text = "";
			txtBridesCity.Text = "";
			txtBridesCounty.Text = "";
			txtBridesState.Text = "";
			txtBridesDOB.Text = "";
			txtBridesFathersName.Text = "";
			txtBridesMothersName.Text = "";
			cboGroomMarriageNumber.SelectedIndex = 0;
			txtGroomsMarriageEndedWhen.Text = "";
			cboBridesMarriageNumber.SelectedIndex = 0;
			txtBridesMarriageEndedWhen.Text = "";
			txtGroomCourtName.Text = "";
			txtGroomFormerSpouseName.Text = "";
			txtBrideCourtName.Text = "";
			txtBrideFormerSpouseName.Text = "";
			txtDateIntentionsFiled.Text = "";
			txtDateLicenseIssued.Text = "";
			txtDateLicenseValid.Text = "";
			txtCityofIssue.Text = FCConvert.ToString(fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase));
			txtCeremonyDate.Text = "";
			txtActualDate.Text = "";
			txtCityMarried.Text = "";
			txtCountyMarried.Text = "";
			txtPersonPerformingCeremony.Text = "";
			txtTitleOfPersonPerforming.Text = "";
			txtDateOfCommision.Text = "";
			txtResidenceofPersonPerforming.Text = "";
			txtMailingAddressofPersonPerforming.Text = "";
			txtNameofWitnessOne.Text = "";
			txtNameofWitnessTwo.Text = "";
			txtDateClerkFiled.Text = "";
			cmbPartyAType.SelectedIndex = 0;
			cmbPartyBType.SelectedIndex = 0;
			cmbPartyAGender.SelectedIndex = 0;
			cmbPartyBGender.SelectedIndex = 0;
			txtPartyABirthName.Text = "";
			txtPartyBDesig.Text = "";
			intDataChanged = 0;
			modDirtyForm.ClearDirtyControls(this);
			VSFlexGrid1.Clear();
		}

		private void txtGroomsMarriageEndedWhen_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtGroomsMarriageEndedWhen_Enter(object sender, System.EventArgs e)
		{
			txtGroomsMarriageEndedWhen.SelectionStart = 0;
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			ClearScreen();
			modClerkGeneral.Statics.AddingMarriageCertificate = true;
			txtGroomsFirstName.Focus();
		}

		private void mnuComment_Click(object sender, System.EventArgs e)
		{
			if (lngMarriageID == 0)
			{
				MessageBox.Show("This record doesn't exist yet.  You must save this record before it can be tied to a comment.", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (frmCommentOld.InstancePtr.Init("CK", "Marriages", "Comments", "ID", lngMarriageID, boolModal: true))
			{
				ImgComment.Image = null;
			}
			else
			{
				ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[1 - 1];
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (modGNBas.Statics.intRecordNumber == 0)
					return;
				if (MessageBox.Show("This will delete the current marriage information. Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.Execute("Delete from Marriages where ID = " + FCConvert.ToString(modGNBas.Statics.intRecordNumber), modGNBas.DEFAULTDATABASE);
					MessageBox.Show("Record deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					Close();
					return;
				}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
			}
		}

		private void mnuMoveNext_Click()
		{
			modClerkGeneral.Statics.AddingMarriageCertificate = false;
			rs.MoveNext();
			if (rs.EndOfFile() == true)
				rs.MoveFirst();
			ClearScreen();
			FillScreen();
		}

		private void mnuMovePrevious_Click()
		{
			modClerkGeneral.Statics.AddingMarriageCertificate = false;
			rs.MovePrevious();
			if (rs.BeginningOfFile() == true)
				rs.MoveLast();
			ClearScreen();
			FillScreen();
		}

		private void PrintCertificate()
		{
			string strSQL = "";
			// vbPorter upgrade warning: dblAmt1 As double	OnWriteFCConvert.ToDecimal(
			double dblAmt1 = 0;
			// vbPorter upgrade warning: dblAmt2 As double	OnWriteFCConvert.ToDecimal(
			double dblAmt2 = 0;
			modDogs.GetClerkFees();
			// THIS WILL FILL A USER DEFINED TYPE TO TRACK IF THE CONSENT
			// FORM NEEDS TO BE PRINTED
			FillUserType();
			if (modGNBas.Statics.typMarriageConsent.NeedConsent)
			{
				if (chkConsent.CheckState != Wisej.Web.CheckState.Checked)
				{
					if (chkRequired.CheckState == Wisej.Web.CheckState.Unchecked)
					{
						MessageBox.Show(modGNBas.Statics.typMarriageConsent.Notes + "\r" + "Cannot print record without consent.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
			}
			modCashReceiptingData.Statics.typeCRData.Type = "MAR";
			modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(frmMarriages.InstancePtr.txtBridesCurrentLastName.Text + ", " + frmMarriages.InstancePtr.txtBridesFirstName.Text, 30);
			modCashReceiptingData.Statics.typeCRData.Reference = (fecherFoundation.Strings.Trim(frmMarriages.InstancePtr.txtFileNumber.Text) + " ");
			modCashReceiptingData.Statics.typeCRData.Control1 = fecherFoundation.Strings.Trim(frmMarriages.InstancePtr.txtDateIntentionsFiled.Text);
			modCashReceiptingData.Statics.typeCRData.Control2 = fecherFoundation.Strings.Trim(frmMarriages.InstancePtr.txtDateLicenseIssued.Text);
			// DO WE NEED TO SHOW THE MESSAGE?
			// IF THIS RECORD IS SHOWING AS THE CONSENT HAS BEEN RECEIVED THE
			// DO NOT SHOW THIS INFORMATION.
			if (modGNBas.Statics.typMarriageConsent.NeedConsent && chkConsent.CheckState == CheckState.Unchecked)
			{
				if (chkRequired.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					MessageBox.Show(modGNBas.Statics.typMarriageConsent.Notes);
				}
			}
			if (Information.IsDate(txtCeremonyDate.Text))
			{
				// If ConvertDateToHaveSlashes(txtCeremonyDate.Text) < "01/01/1892" Then
				// If DateDiff("d", "01/01/1892", ConvertDateToHaveSlashes(txtCeremonyDate.Text)) <= 0 Then
				if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime("01/01/1892"), FCConvert.ToDateTime(txtCeremonyDate.Text)) <= 0)
				{
					modGNBas.Statics.typOldVitalsReport.AttestBy = modClerkGeneral.Statics.ClerkName;
					modGNBas.Statics.typOldVitalsReport.FileNumber = txtFileNumber.Text;
					modGNBas.Statics.typOldVitalsReport.pageNumber = string.Empty;
					modGNBas.Statics.typOldVitalsReport.FamilyName = string.Empty;
					modGNBas.Statics.typOldVitalsReport.ChildName1 = "Groom:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsMiddleName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsLastName.Text));
					modGNBas.Statics.typOldVitalsReport.ChildName2 = "Bride:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesMiddleName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesMaidenSurname.Text));
					modGNBas.Statics.typOldVitalsReport.ChildName3 = string.Empty;
					modGNBas.Statics.typOldVitalsReport.DateOfEvent = txtCeremonyDate.Text;
					modGNBas.Statics.typOldVitalsReport.Parent1 = "Grooms Father:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsFathersName.Text));
					modGNBas.Statics.typOldVitalsReport.Parent2 = "Grooms Mother:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsMothersName.Text));
					modGNBas.Statics.typOldVitalsReport.Parent3 = "Brides Father:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesFathersName.Text));
					modGNBas.Statics.typOldVitalsReport.Parent4 = "Brides Mother:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesMothersName.Text));
					modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = txtCityMarried.Text;
					modGNBas.Statics.typOldVitalsReport.PlaceOfResidence = string.Empty;
					modGNBas.Statics.typOldVitalsReport.NameOfClerk1 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtNameOfClerk.Text));
					modGNBas.Statics.typOldVitalsReport.NameOfClerk2 = modGNBas.Statics.typOldVitalsReport.NameOfClerk1;
					modGNBas.Statics.typOldVitalsReport.TownOfClerk1 = modGlobalConstants.Statics.MuniName;
					modGNBas.Statics.typOldVitalsReport.TownOfClerk2 = modGlobalConstants.Statics.MuniName;
					modGNBas.Statics.typOldVitalsReport.DateTo = FCConvert.ToString(modDateRoutines.LongDate(DateTime.Today));
					modGNBas.Statics.typOldVitalsReport.DateFrom = string.Empty;
					modGNBas.Statics.typOldVitalsReport.Notes = "Record is dated prior to January 1, 1892. This record must be printed on Town letterhead. Please load letterhead into printer now.";
					modCashReceiptingData.Statics.typeCRData.NumFirsts = 0;
					modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
					frmPrintOldVitals.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					if (ViewModel.ChargeFees)
					{
						if (modCashReceiptingData.Statics.typeCRData.NumFirsts + modCashReceiptingData.Statics.typeCRData.NumSubsequents > 0)
						{
							modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(frmChargeCert.InstancePtr.Init(FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewMarriageCert + modDogs.Statics.typClerkFees.MarriageCertStateFee)), FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementMarriageCert + modDogs.Statics.typClerkFees.ReplacementMarriageCertStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents), "000000.00"));
							dblAmt1 = FCConvert.ToDouble(((modDogs.Statics.typClerkFees.NewMarriageCert * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementMarriageCert * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
							dblAmt2 = FCConvert.ToDouble(((modDogs.Statics.typClerkFees.MarriageCertStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementMarriageCertStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
							modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(dblAmt1, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(dblAmt2, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
							modCashReceiptingData.Statics.typeCRData.PartyID = 0;
							modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
							Application.Exit();
						}
					}
				}
				else
				{
					intDataChanged += FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this));
					if (intDataChanged > 0)
					{
						if (MessageBox.Show("Save current Marriage information and proceed to Marriage Certificate?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							if (FCConvert.ToBoolean(AddedValidation()))
							{
								OKToSaveData();
								if (OKtoSave)
								{
									mnuSave_Click();
									// With RS
									// utPrintInfo.tblKeyNum = .Fields("ID")
									// End With
									modClerkGeneral.Statics.utPrintInfo.TblName = "Marriages";
									strSQL = "Select * from Marriages where ID = " + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
									modGNBas.Statics.rsMarriageCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
									frmMarriages.InstancePtr.Hide();
									// frmPrintVitalRec.Show 
									frmPrintVitalRec.InstancePtr.Init(false);
								}
								else
								{
									return;
								}
							}
							else
							{
								return;
							}
						}
						else
						{
							return;
						}
					}
					else
					{
						modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
						modClerkGeneral.Statics.utPrintInfo.TblName = "Marriages";
						strSQL = "Select * from Marriages where ID = " + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
						modGNBas.Statics.rsMarriageCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
						frmMarriages.InstancePtr.Hide();
						// frmPrintVitalRec.Show 
						frmPrintVitalRec.InstancePtr.Init(false);
					}
				}
			}
			else
			{
				intDataChanged += FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this));
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Save current Marriage information and proceed to Marriage Certificate?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (FCConvert.ToBoolean(AddedValidation()))
						{
							OKToSaveData();
							if (OKtoSave)
							{
								mnuSave_Click();
								// With RS
								// utPrintInfo.tblKeyNum = .Fields("ID")
								// End With
								modClerkGeneral.Statics.utPrintInfo.TblName = "Marriages";
								strSQL = "Select * from Marriages where ID = " + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
								modGNBas.Statics.rsMarriageCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
								frmMarriages.InstancePtr.Hide();
								// frmPrintVitalRec.Show 
								frmPrintVitalRec.InstancePtr.Init(false);
							}
							else
							{
								return;
							}
						}
						else
						{
							return;
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					modClerkGeneral.Statics.utPrintInfo.TblName = "Marriages";
					strSQL = "Select * from Marriages where ID = " + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
					modGNBas.Statics.rsMarriageCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
					frmMarriages.InstancePtr.Hide();
					// frmPrintVitalRec.Show 
					frmPrintVitalRec.InstancePtr.Init(false);
				}
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			if (!FCConvert.ToBoolean(AddedValidation()))
				return;
			if (intDataChanged + Conversion.Val(modDirtyForm.NumberDirtyControls(this)) > 0)
			{
				if (MessageBox.Show("Data has changed.  You must save before printing the certificate or license." + "\r\n" + "Do you wish to do so now?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					mnuSave_Click();
				}
				else
				{
					return;
				}
			}
			FillUserType();
			modClerkGeneral.Statics.utPrintInfo.PreView = false;
			PrintCertificate();

		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		private object AddedValidation()
		{
			object AddedValidation = null;
			if (fecherFoundation.Strings.Trim(txtNameOfClerk.Text) != "")
			{
				AddedValidation = true;
			}
			else
			{
				MessageBox.Show("Please enter a valid Name of Filing Clerk (second screen).", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				AddedValidation = false;
				tabMarriages.SelectedIndex = 1;
				goto TheTag;
			}
			if (fecherFoundation.Strings.Trim(txtCityofIssue.Text) != "")
			{
				AddedValidation = true;
			}
			else
			{
				MessageBox.Show("Please enter a valid Town of Issue (second screen).", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				AddedValidation = false;
				tabMarriages.SelectedIndex = 1;
				goto TheTag;
			}
			if (fecherFoundation.Strings.Trim(txtPersonPerformingCeremony.Text) != "")
			{
				AddedValidation = true;
			}
			else
			{
				MessageBox.Show("Please enter a valid Name for Person Performing the Ceremony (second screen).", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				AddedValidation = false;
				tabMarriages.SelectedIndex = 1;
				goto TheTag;
			}
			if (fecherFoundation.Strings.Trim(txtTitleOfPersonPerforming.Text) != "")
			{
				AddedValidation = true;
			}
			else
			{
				MessageBox.Show("Please enter a valid Title of Person Performing Ceremony", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				AddedValidation = false;
				tabMarriages.SelectedIndex = 1;
				goto TheTag;
			}
			TheTag:
			;
			return AddedValidation;
		}

		private void FillUserType()
		{
			bool boolGroom16 = false;
			bool boolBride16 = false;
			modGNBas.Statics.typMarriageConsent.ClerkName = modClerkGeneral.Statics.ClerkName;
			modGNBas.Statics.typMarriageConsent.DateOfConsent = FCConvert.ToString(modDateRoutines.LongDate(DateTime.Today));
			modGNBas.Statics.typMarriageConsent.GroomName = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsMiddleName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsLastName.Text));
			modGNBas.Statics.typMarriageConsent.BrideName = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesMiddleName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesMaidenSurname.Text));
			modGNBas.Statics.typMarriageConsent.TownName = modGlobalConstants.Statics.MuniName;
			modGNBas.Statics.typMarriageConsent.NeedConsent = false;
			modGNBas.Statics.typMarriageConsent.Groom = false;
			modGNBas.Statics.typMarriageConsent.Bride = false;
			modGNBas.Statics.typMarriageConsent.Notes = string.Empty;
			modGNBas.Statics.typMarriageConsent.MarriageDate = txtCeremonyDate.Text;
			modGNBas.Statics.typMarriageConsent.CountyOfCommission = modGNBas.Statics.ClerkDefaults.County;
			if (txtGroomsAge.Text != string.Empty)
			{
				if (FCConvert.ToDouble(txtGroomsAge.Text) < 16)
				{
					modGNBas.Statics.typMarriageConsent.NeedConsent = true;
					modGNBas.Statics.typMarriageConsent.Notes += "Grooms age is less than 16. Be sure to get Consent of Parents or Guardians form completed. Remember to send letter to Judge of Probate per Title 19-A. Do NOT issue license until written consent to issue license is received." + "\r\n" + "\r\n";
					modGNBas.Statics.typMarriageConsent.Groom = true;
					boolGroom16 = true;
				}
			}
			if (txtBridesAge.Text != string.Empty)
			{
				if (FCConvert.ToDouble(txtBridesAge.Text) < 16)
				{
					modGNBas.Statics.typMarriageConsent.NeedConsent = true;
					modGNBas.Statics.typMarriageConsent.Notes += "Brides age is less than 16. Be sure to get Consent of Parents or Guardians form completed. Remember to send letter to Judge of Probate per Title 19-A. Do NOT issue license until written consent to issue license is received." + "\r\n" + "\r\n";
					modGNBas.Statics.typMarriageConsent.Bride = true;
					boolBride16 = true;
				}
			}
			if (txtGroomsAge.Text != string.Empty)
			{
				if (FCConvert.ToDouble(txtGroomsAge.Text) < 18 && boolGroom16 == false)
				{
					modGNBas.Statics.typMarriageConsent.NeedConsent = true;
					modGNBas.Statics.typMarriageConsent.Notes += "Grooms age is less then 18. Be sure to get Consent of Parents or Guardians form completed." + "\r\n" + "\r\n";
					modGNBas.Statics.typMarriageConsent.Groom = true;
				}
			}
			if (txtBridesAge.Text != string.Empty)
			{
				if (FCConvert.ToDouble(txtBridesAge.Text) < 18 && boolBride16 == false)
				{
					modGNBas.Statics.typMarriageConsent.NeedConsent = true;
					modGNBas.Statics.typMarriageConsent.Notes += "Brides age is less then 18. Be sure to get Consent of Parents or Guardians form completed." + "\r\n" + "\r\n";
					modGNBas.Statics.typMarriageConsent.Bride = true;
				}
			}
			if (Information.IsDate(txtGroomsMarriageEndedWhen.Text))
			{
				if (!FCConvert.ToBoolean(rs.Get_Fields_Boolean("groomoutofstatedivorce")))
				{
					if (Information.IsDate(txtActualDate.Text))
					{
						if (fecherFoundation.DateAndTime.DateValue(txtGroomsMarriageEndedWhen.Text).ToOADate() > fecherFoundation.DateAndTime.DateAdd("d", -21, fecherFoundation.DateAndTime.DateValue(txtActualDate.Text)).ToOADate())
						{
							MessageBox.Show("Groom's last marriage ended within 21 days from this marriage." + "\r\n" + "If this is not an out of state divorce a waiver is required", "Waiver may be required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else if (Information.IsDate(txtCeremonyDate.Text))
					{
						if (fecherFoundation.DateAndTime.DateValue(txtGroomsMarriageEndedWhen.Text).ToOADate() > fecherFoundation.DateAndTime.DateAdd("d", -21, fecherFoundation.DateAndTime.DateValue(txtCeremonyDate.Text)).ToOADate())
						{
							MessageBox.Show("Groom's last marriage ended within 21 days from this marriage." + "\r\n" + "If this is not an out of state divorce a waiver is required", "Waiver may be required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else if (Information.IsDate(txtDateIntentionsFiled.Text))
					{
						// If IsDate(txtDateIntentionsFiled.Text) Then
						if (fecherFoundation.DateAndTime.DateValue(txtGroomsMarriageEndedWhen.Text).ToOADate() > fecherFoundation.DateAndTime.DateAdd("d", -21, fecherFoundation.DateAndTime.DateValue(txtDateIntentionsFiled.Text)).ToOADate())
						{
							MessageBox.Show("Groom's last marriage ended within 21 days from intentions filed." + "\r\n" + "If this is not an out of state divorce and the ceremony will also" + "\r\n" + "be performed within the 21 days, a waiver is required", "Waiver may be required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
			}
			// If IsDate(txtGroomsMarriageEndedWhen) And IsDate(txtDateIntentionsFiled) Then
			// If CDate(txtGroomsMarriageEndedWhen) < CDate(txtDateIntentionsFiled) Then
			// If CDate(txtGroomsMarriageEndedWhen) > DateAdd("d", -21, CDate(txtDateIntentionsFiled)) Then
			// If Not RS.Fields("groomoutofstatedivorce") Then
			// If MsgBox("Grooms last marriage ended within 21 days from intentions filed.  Is this an out of state divorce?", vbYesNo + vbQuestion, "Out of State Divorce?") = vbYes Then
			// RS.Edit
			// RS.Fields("groomoutofstatedivorce") = True
			// RS.Update
			// Else
			// .NeedConsent = True
			// .Notes = .Notes & "Grooms last marriage ended within 21 days from intentions filed. A 21 day waiting period waiver is required." & vbCrLf & vbCrLf
			// .Groom = True
			// End If
			// End If
			// End If
			// End If
			// End If
			if (Information.IsDate(txtBridesMarriageEndedWhen.Text))
			{
				if (!FCConvert.ToBoolean(rs.Get_Fields_Boolean("brideoutofstatedivorce")))
				{
					if (Information.IsDate(txtActualDate.Text))
					{
						if (fecherFoundation.DateAndTime.DateValue(txtBridesMarriageEndedWhen.Text).ToOADate() > fecherFoundation.DateAndTime.DateAdd("d", -21, fecherFoundation.DateAndTime.DateValue(txtActualDate.Text)).ToOADate())
						{
							MessageBox.Show("Bride's last marriage ended within 21 days from this marriage." + "\r\n" + "If this is not an out of state divorce a waiver is required", "Waiver may be required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else if (Information.IsDate(txtCeremonyDate.Text))
					{
						if (fecherFoundation.DateAndTime.DateValue(txtBridesMarriageEndedWhen.Text).ToOADate() > fecherFoundation.DateAndTime.DateAdd("d", -21, fecherFoundation.DateAndTime.DateValue(txtCeremonyDate.Text)).ToOADate())
						{
							MessageBox.Show("Bride's last marriage ended within 21 days from this marriage." + "\r\n" + "If this is not an out of state divorce a waiver is required", "Waiver may be required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else if (Information.IsDate(txtDateIntentionsFiled.Text))
					{
						if (fecherFoundation.DateAndTime.DateValue(txtBridesMarriageEndedWhen.Text).ToOADate() > fecherFoundation.DateAndTime.DateAdd("d", -21, fecherFoundation.DateAndTime.DateValue(txtDateIntentionsFiled.Text)).ToOADate())
						{
							MessageBox.Show("Bride's last marriage ended within 21 days from intentions filed." + "\r\n" + "If this is not an out of state divorce and the ceremony will also" + "\r\n" + "be performed within the 21 days, a waiver is required", "Waiver may be required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
			}
			// If IsDate(txtBridesMarriageEndedWhen) And IsDate(txtDateIntentionsFiled) Then
			// If CDate(txtBridesMarriageEndedWhen) < CDate(txtDateIntentionsFiled) Then
			// If CDate(txtBridesMarriageEndedWhen) > DateAdd("d", -21, CDate(txtDateIntentionsFiled)) Then
			// If Not RS.Fields("brideoutofstatedivorce") Then
			// If MsgBox("Bride's last marriage ended within 21 days from intentions filed.  Is this an out of state divorce?", vbYesNo + vbQuestion, "Out of State Divorce?") = vbYes Then
			// RS.Edit
			// RS.Fields("brideoutofstatedivorce") = True
			// RS.Update
			// Else
			// .NeedConsent = True
			// .Notes = .Notes & "Brides last marrriage ended 21 days from intentions filed. A 21 day waiting period waiver is required." & vbCrLf & vbCrLf
			// .Bride = True
			// End If
			// End If
			// End If
			// End If
			// End If
		}

		private void mnuPrintIntentions_Click(object sender, System.EventArgs e)
		{
			modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			modClerkGeneral.Statics.utPrintInfo.PreView = true;
			PrintLicense(CNSTPRINTINTENTIONS);
		}

		private void PrintLicense(int lngPrintType)
		{
			string strSQL;
			// vbPorter upgrade warning: dblAmt1 As double	OnWriteFCConvert.ToDecimal(
			double dblAmt1 = 0;
			// vbPorter upgrade warning: dblAmt2 As double	OnWriteFCConvert.ToDecimal(
			double dblAmt2 = 0;
			FillUserType();
			modDogs.GetClerkFees();
			if (modGNBas.Statics.typMarriageConsent.NeedConsent)
			{
				if (chkConsent.CheckState != Wisej.Web.CheckState.Checked)
				{
					if (chkRequired.CheckState == Wisej.Web.CheckState.Unchecked)
					{
						MessageBox.Show(modGNBas.Statics.typMarriageConsent.Notes + "\r" + "Cannot print record without consent.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
			}
			if (Information.IsDate(txtCeremonyDate.Text))
			{
				// If ConvertDateToHaveSlashes(txtCeremonyDate.Text) < "01/01/1892" Then
				// If DateDiff("d", "01/01/1892", ConvertDateToHaveSlashes(txtCeremonyDate.Text)) <= 0 Then
				if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime("01/01/1892"), FCConvert.ToDateTime(txtCeremonyDate.Text)) <= 0)
				{
					modGNBas.Statics.typOldVitalsReport.AttestBy = modClerkGeneral.Statics.ClerkName;
					modGNBas.Statics.typOldVitalsReport.FileNumber = txtFileNumber.Text;
					modGNBas.Statics.typOldVitalsReport.pageNumber = string.Empty;
					modGNBas.Statics.typOldVitalsReport.FamilyName = string.Empty;
					modGNBas.Statics.typOldVitalsReport.ChildName1 = "Groom:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsMiddleName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsLastName.Text));
					modGNBas.Statics.typOldVitalsReport.ChildName2 = "Bride:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesMiddleName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesMaidenSurname.Text));
					modGNBas.Statics.typOldVitalsReport.ChildName3 = string.Empty;
					modGNBas.Statics.typOldVitalsReport.DateOfEvent = txtCeremonyDate.Text;
					modGNBas.Statics.typOldVitalsReport.Parent1 = "Grooms Father:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsFathersName.Text));
					modGNBas.Statics.typOldVitalsReport.Parent2 = "Grooms Mother:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtGroomsMothersName.Text));
					modGNBas.Statics.typOldVitalsReport.Parent3 = "Brides Father:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesFathersName.Text));
					modGNBas.Statics.typOldVitalsReport.Parent4 = "Brides Mother:  " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtBridesMothersName.Text));
					modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = txtCityMarried.Text;
					modGNBas.Statics.typOldVitalsReport.PlaceOfResidence = string.Empty;
					modGNBas.Statics.typOldVitalsReport.NameOfClerk1 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtNameOfClerk.Text));
					modGNBas.Statics.typOldVitalsReport.NameOfClerk2 = modGNBas.Statics.typOldVitalsReport.NameOfClerk1;
					modGNBas.Statics.typOldVitalsReport.TownOfClerk1 = modGlobalConstants.Statics.MuniName;
					modGNBas.Statics.typOldVitalsReport.TownOfClerk2 = modGlobalConstants.Statics.MuniName;
					modGNBas.Statics.typOldVitalsReport.DateTo = FCConvert.ToString(modDateRoutines.LongDate(DateTime.Today));
					modGNBas.Statics.typOldVitalsReport.DateFrom = string.Empty;
					modGNBas.Statics.typOldVitalsReport.Notes = "Record is dated prior to January 1, 1892. This record must be printed on Town letterhead. Please load letterhead into printer now.";
					modCashReceiptingData.Statics.typeCRData.NumFirsts = 0;
					modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
					frmPrintOldVitals.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					if (ViewModel.ChargeFees)
					{
						if (modCashReceiptingData.Statics.typeCRData.NumFirsts + modCashReceiptingData.Statics.typeCRData.NumSubsequents > 0)
						{
							// .Amount1 = Format(frmChargeCert.Init(CDbl(typClerkFees.NewMarriageCert), CDbl(typClerkFees.ReplacementMarriageCert), .NumFirsts, .NumSubsequents), "000000.00")
							frmChargeCert.InstancePtr.Init(FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewMarriageCert + modDogs.Statics.typClerkFees.MarriageLicenseStateFee)), FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementMarriageCert + modDogs.Statics.typClerkFees.MarriageLicenseReplacementStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents);
							dblAmt1 = FCConvert.ToDouble(((modDogs.Statics.typClerkFees.NewMarriageLicense * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementMarriageLicense * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
							dblAmt2 = FCConvert.ToDouble(((modDogs.Statics.typClerkFees.MarriageLicenseStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.MarriageLicenseReplacementStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
							modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(dblAmt2, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(dblAmt2, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
							modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
							modCashReceiptingData.Statics.typeCRData.PartyID = 0;
							modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
							Application.Exit();
							return;
						}
					}
				}
			}
			intDataChanged += FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this));
			if (intDataChanged > 0)
			{
				if (MessageBox.Show("Save current Marriage information and proceed to print?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					OKToSaveData();
					if (!OKtoSave)
					{
						return;
					}
					else
					{
						mnuSave_Click();
					}
				}
			}
			if (!frmPrintIntentions.InstancePtr.Init(lngPrintType))
			{
				return;
			}
			strSQL = "Select * from Marriages where ID = " + FCConvert.ToString(modClerkGeneral.Statics.SearchResponse);
			modGNBas.Statics.rsMarriageCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			if (modGNBas.Statics.gboolStateIntentions)
			{
				modGNBas.Statics.gstrActiveLicense = "STATE";
			}
			else if (modGNBas.Statics.gboolIssuanceIntentions)
			{
				modGNBas.Statics.gstrActiveLicense = "ISSUANCE";
			}
			else if (modGNBas.Statics.gboolMarriageIntentions)
			{
				modGNBas.Statics.gstrActiveLicense = "MARRIAGE";
			}
			else
			{
				modGNBas.Statics.gstrActiveLicense = "OTHER";
			}
			if (modGNBas.Statics.gboolNewMarriageForms)
			{
				if (modGNBas.Statics.gintMarriageConfirmation < 3)
				{
					switch (modGNBas.Statics.gintMarriageForm)
					{
						case 1:
							{
								rptNewLicense.InstancePtr.Init(modGNBas.Statics.gintMarriageForm, true);
								break;
							}
						case 2:
							{
								rptNewLicenseRev082006.InstancePtr.Init(modGNBas.Statics.gintMarriageForm, true);
								break;
							}
						case 3:
							{
								rptNewLicenseRev082006.InstancePtr.Init(modGNBas.Statics.gintMarriageForm, true);
								break;
							}
						case 4:
							{
								// Call rptMarriageLicenseWordDoc.Init(gintMarriageForm)
								rptNewLicenseRev122012.InstancePtr.Init(modGNBas.Statics.gintMarriageForm, true);
								break;
							}
						default:
							{
								break;
							}
					}
					
				}
				else
				{
					switch (modGNBas.Statics.gintMarriageForm)
					{
						case 1:
							{
								frmReportViewer.InstancePtr.Init(rptNewIntentions.InstancePtr, showModal: true);
								//rptNewIntentions.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								break;
							}
						case 2:
							{
								frmReportViewer.InstancePtr.Init(rptNewIntentionsRev082006.InstancePtr, showModal: true);
								//rptNewIntentionsRev082006.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								break;
							}
						case 3:
							{
								frmReportViewer.InstancePtr.Init(rptMarriageIntentionsRev052010.InstancePtr, showModal: true);
								//rptMarriageIntentionsRev052010.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								break;
							}
						default:
							{
								// rptMarriageIntentionsWordDoc.Show 1
								frmReportViewer.InstancePtr.Init(rptNewIntentionsRev122012.InstancePtr, showModal: true);
								//rptNewIntentionsRev122012.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								break;
							}
					}

				}
			}
			else
			{
				frmReportViewer.InstancePtr.Init(rptLicense.InstancePtr, showModal: true);
				//rptLicense.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			}
			// now record transaction stuff
			// don't charge for the intentions.  Intentions & license together are $20
			// If (gboolFromDosTrio = True Or bolFromWindowsCR) And (gintMarriageConfirmation < 3 Or gboolSplitMarriageFee) Then
			if ( ViewModel.ChargeFees)
			{
				modCashReceiptingData.Statics.typeCRData.Type = "MAR";
				modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(frmMarriages.InstancePtr.txtBridesCurrentLastName.Text + ", " + frmMarriages.InstancePtr.txtBridesFirstName.Text, 30);
				modCashReceiptingData.Statics.typeCRData.Reference = frmMarriages.InstancePtr.txtFileNumber.Text;
				modCashReceiptingData.Statics.typeCRData.Control1 = fecherFoundation.Strings.Trim(txtDateIntentionsFiled.Text);
				modCashReceiptingData.Statics.typeCRData.Control2 = fecherFoundation.Strings.Trim(txtDateLicenseIssued.Text);
				// vbPorter upgrade warning: dblTemp As double	OnWriteFCConvert.ToDecimal(
				double dblTemp = 0;
				// If Not gboolSplitMarriageFee Then
				dblTemp = FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewMarriageLicense + modDogs.Statics.typClerkFees.MarriageLicenseStateFee));

				if (modGNBas.Statics.gintMarriageConfirmation == 3)
				{					
					frmChargeCert.InstancePtr.Init(dblTemp, FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementMarriageLicense)) + FCConvert.ToDouble((modDogs.Statics.typClerkFees.MarriageLicenseReplacementStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents, "Replacement" + "\r\n" + "Licenses");
					dblAmt1 = FCConvert.ToDouble(((modDogs.Statics.typClerkFees.NewMarriageLicense * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementMarriageLicense * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
					dblAmt2 = FCConvert.ToDouble(((modDogs.Statics.typClerkFees.MarriageLicenseStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.MarriageLicenseReplacementStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
				}
				else
				{
					// don't default to charge for the first license since they already paid for it when they did the intentions
					// .Amount1 = Val(.Amount1) + Format(frmChargeCert.Init(dblTemp, CDbl(typClerkFees.ReplacementMarriageLicense), 0, typeCRData.NumSubsequents, "Replacement" & vbCrLf & "Licenses"), "000000.00")
					frmChargeCert.InstancePtr.Init(dblTemp, FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementMarriageLicense)) + FCConvert.ToDouble((modDogs.Statics.typClerkFees.MarriageLicenseReplacementStateFee)), 0, modCashReceiptingData.Statics.typeCRData.NumSubsequents, "Replacement" + "\r\n" + "Licenses");
					dblAmt1 = FCConvert.ToDouble(((modDogs.Statics.typClerkFees.NewMarriageLicense * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementMarriageLicense * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
					dblAmt2 = FCConvert.ToDouble(((modDogs.Statics.typClerkFees.MarriageLicenseStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.MarriageLicenseReplacementStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
				}

                var transaction = new VitalTransaction();
               
				modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(Conversion.Val(modCashReceiptingData.Statics.typeCRData.Amount1) + dblAmt1, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(Conversion.Val(modCashReceiptingData.Statics.typeCRData.Amount2) + dblAmt2, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
				modCashReceiptingData.Statics.typeCRData.PartyID = 0;
				modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
                transaction.VitalAmount = Convert.ToDecimal(modCashReceiptingData.Statics.typeCRData.Amount1);
                transaction.StateFee = Convert.ToDecimal(modCashReceiptingData.Statics.typeCRData.Amount2);
                transaction.TypeCode = "MAR";
                transaction.Name = Strings.Left(txtBridesCurrentLastName.Text + ", " + txtBridesFirstName.Text, 30);
                transaction.Reference = txtFileNumber.Text;
                transaction.Control1 = txtDateIntentionsFiled.Text.Trim();
                transaction.Control2 = txtDateLicenseIssued.Text.Trim();
                ViewModel.UpdateTransaction(transaction);
                if (modGNBas.Statics.gintMarriageConfirmation < 3)
				{
					// don't end if intentions, or this will kick them out before printing the license
					
                    ViewModel.CompleteTransaction();
                    CloseWithoutCancel();
				}
			}
		}

		private void mnuPrintPreviewCertificate_Click(object sender, System.EventArgs e)
		{
			modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			modClerkGeneral.Statics.utPrintInfo.PreView = true;
			if (FCConvert.ToBoolean(AddedValidation()))
				PrintCertificate();
		}

		private void mnuPrintPreviewLicense_Click(object sender, System.EventArgs e)
		{
			modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			modClerkGeneral.Statics.utPrintInfo.PreView = true;
			PrintLicense(CNSTPRINTLICENSE);
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			modClerkGeneral.Statics.Quitting = true;
			Close();
		}

		public void mnuQuit_Click()
		{
			mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void mnuRegistrarNames_Click(object sender, System.EventArgs e)
		{
			frmRegistrarNames.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		public void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsClerkInfo = new clsDRWrapper();
			bool blnUpdateFile = false;
			OKToSaveData();
			if (OKtoSave == true)
			{
				Support.SendKeys("{TAB}", false);
				//App.DoEvents();
				NEWRECORD:
				;
				if (modClerkGeneral.Statics.AddingMarriageCertificate == true)
				{
					blnUpdateFile = true;
					EdittingRecord = false;
					rs.AddNew();
					modClerkGeneral.Statics.SearchResponse = SaveData();
					rs.Update();
					rs.OpenRecordset("SELECT * FROM Marriages WHERE ID = " + FCConvert.ToString(modClerkGeneral.Statics.SearchResponse), "twck0000.vb1");
					// get the last added ID and put it in the variable that
					// will be read by the Marriage license report
					// Dim RSTemp As New clsDRWrapper
					// Call RSTemp.OpenRecordset("Select Max(ID) as MaxID from Marriages", DEFAULTDATABASENAME)
					// If Not RSTemp.EndOfFile Then
					// SearchResponse = Val(RSTemp.Fields("MaxID"))
					// Else
					// SearchResponse = 0
					// End If
					modClerkGeneral.Statics.utPrintInfo.tblKeyNum = modClerkGeneral.Statics.SearchResponse;
					// Set RSTemp = Nothing
				}
				else
				{
					if (rs.EndOfFile())
					{
						modClerkGeneral.Statics.AddingMarriageCertificate = true;
						goto NEWRECORD;
					}
					if (lngMarriageID == 0)
					{
						modClerkGeneral.Statics.AddingMarriageCertificate = true;
						goto NEWRECORD;
					}
					modClerkGeneral.Statics.AddingMarriageCertificate = false;
					rs.OpenRecordset("select * from marriages where ID = " + FCConvert.ToString(lngMarriageID), "twck0000.vb1");
					if (rs.EndOfFile())
					{
						modClerkGeneral.Statics.AddingMarriageCertificate = true;
						goto NEWRECORD;
					}
					// @DJW 1/2/2008 The boolean was always getting reset back to false after it was getting set to true in the certain cases
					if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != string.Empty && fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("filenumber"))) == string.Empty)
					{
						blnUpdateFile = true;
					}
					else
					{
						blnUpdateFile = false;
					}
					rs.Edit();
					SaveData();
					rs.Update();
					EdittingRecord = false;
				}
				if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != "" && blnUpdateFile)
				{
					rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
					if (rsClerkInfo.BeginningOfFile() != true && rsClerkInfo.EndOfFile() != true)
					{
						rsClerkInfo.Edit();
					}
					else
					{
						rsClerkInfo.AddNew();
					}
					rsClerkInfo.Set_Fields("LastMarriageFileNumber", fecherFoundation.Strings.Trim(txtFileNumber.Text));
					rsClerkInfo.Update();
					ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + fecherFoundation.Strings.Trim(txtFileNumber.Text));
					ToolTip1.SetToolTip(lblFile, ToolTip1.GetToolTip(txtFileNumber));
				}
				// Dim answer As String
				// pass in the ID number of this marriage record so that we
				// can have an identifier in the amendments table
				modAmendments.SaveAmendments(ref VSFlexGrid1, modGNBas.Statics.gintMarriageNumber, "MARRIAGES");
				// If AddingMarriageCertificate Then
				// answer = MsgBox("Save successful. Would you like to add another record?", vbYesNo, "Add Another Record?")
				// If answer = vbYes Then
				// AddingMarriageCertificate = True
				// EdittingRecord = False
				// ClearScreen
				// tabMarriages.Tab = 0
				// txtGroomsFirstName.SetFocus
				// Else
				modClerkGeneral.Statics.AddingMarriageCertificate = false;
				// Unload Me
				// End If
				// Else
				MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// End If
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			if (OKtoSave)
				mnuQuit_Click();
		}

		private void mnuSaveExitNoValidation_Click(object sender, System.EventArgs e)
		{
			NEWRECORD:
			;
			if (modClerkGeneral.Statics.AddingMarriageCertificate == true)
			{
				EdittingRecord = false;
				rs.AddNew();
				modClerkGeneral.Statics.SearchResponse = SaveData();
				rs.Update();
				modClerkGeneral.Statics.utPrintInfo.tblKeyNum = modClerkGeneral.Statics.SearchResponse;
			}
			else
			{
				if (rs.EndOfFile())
				{
					modClerkGeneral.Statics.AddingMarriageCertificate = true;
					goto NEWRECORD;
				}
				if (lngMarriageID == 0)
				{
					modClerkGeneral.Statics.AddingMarriageCertificate = true;
					goto NEWRECORD;
				}
				modClerkGeneral.Statics.AddingMarriageCertificate = false;
				rs.OpenRecordset("select * from marriages where ID = " + FCConvert.ToString(lngMarriageID), "twck0000.vb1");
				if (rs.EndOfFile())
				{
					modClerkGeneral.Statics.AddingMarriageCertificate = true;
					goto NEWRECORD;
				}
				rs.Edit();
				SaveData();
				rs.Update();
				EdittingRecord = false;
			}
			// Dim answer As String
			// pass in the ID number of this marriage record so that we
			// can have an identifier in the amendments table
			modAmendments.SaveAmendments(ref VSFlexGrid1, modGNBas.Statics.gintMarriageNumber, "MARRIAGES");
			modClerkGeneral.Statics.AddingMarriageCertificate = false;
			MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			mnuQuit_Click();
		}

		private void txtBridesMothersBirthplace_Leave(object sender, System.EventArgs e)
		{
			tabMarriages.SelectedIndex = 1;
		}

		public int SaveData(bool boolShow = true)
		{
			int SaveData = 0;
			lngMarriageID = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("ID"))));
			SaveData = lngMarriageID;
			rs.Set_Fields("TownCode", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			rs.Set_Fields("GroomSSN", txtGroomSSN.Text);
			rs.Set_Fields("BrideSSN", txtBrideSSN.Text);
			rs.Set_Fields("NoRequiredFields", chkRequired.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("ConsentInfo", chkConsent.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("SignatureOnFile", chkSignitureOnFile.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("BirthOnFile", chkBirthOnFile.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("DeathOnFile", chkDeathOnFile.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("GroomBirthOnFile", chkGroomBirthOnFile.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("GroomDeathOnFile", chkGroomDeathOnFile.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("VerifyForeignBride", chkVerificationOfForeignBride.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("VerifyForeignGroom", chkVerificationOfForeignGroom.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("Voided", chkVoided.CheckState == Wisej.Web.CheckState.Checked);
			// If Trim(txtFileNumber.Text) <> vbNullString And Trim(.Fields("filenumber")) = vbNullString Then
			// blnUpdateFile = True
			// End If
			rs.Set_Fields("FileNumber", txtFileNumber.Text);
			rs.Set_Fields("GroomsFirstName", txtGroomsFirstName.Text);
			rs.Set_Fields("GroomsMiddleName", txtGroomsMiddleName.Text);
			rs.Set_Fields("PartyABirthName", txtPartyABirthName.Text);
			rs.Set_Fields("GroomsLastName", txtGroomsLastName.Text);
			rs.Set_Fields("GroomsDesignation", txtGroomsDesignation.Text);
			rs.Set_Fields("groomsage", FCConvert.ToString(Conversion.Val(txtGroomsAge.Text)));
			rs.Set_Fields("GroomsStreetName", txtGroomsStreetName.Text);
			rs.Set_Fields("GroomsStreetAddress", txtGroomsStreetNumber.Text);
			rs.Set_Fields("GroomsCity", txtGroomsCity.Text);
			rs.Set_Fields("GroomsCounty", txtGroomsCounty.Text);
			rs.Set_Fields("GroomsState", txtGroomsState.Text);
			rs.Set_Fields("GroomZip", txtGroomZip.Text);
			rs.Set_Fields("BrideZip", txtBrideZip.Text);
			rs.Set_Fields("GroomsBirthplace", fecherFoundation.Strings.Trim(txtState.Text));
			rs.Set_Fields("bridesbirthplace", fecherFoundation.Strings.Trim(txtState2.Text));
			rs.Set_Fields("GroomsFathersBirthplace", fecherFoundation.Strings.Trim(txtGroomsFathersBirthplace.Text));
			rs.Set_Fields("GroomsMothersBirthplace", fecherFoundation.Strings.Trim(txtGroomsMothersBirthplace.Text));
			rs.Set_Fields("BridesFathersBirthplace", fecherFoundation.Strings.Trim(txtBridesFathersBirthplace.Text));
			rs.Set_Fields("BridesMothersBirthplace", fecherFoundation.Strings.Trim(txtBridesMothersBirthplace.Text));
			rs.Set_Fields("GroomsDOB", fecherFoundation.Strings.Trim(txtGroomsDOB.Text));
			rs.Set_Fields("PartyAGender", cmbPartyAGender.Text);
			rs.Set_Fields("PartyBGender", cmbPartyBGender.Text);
			rs.Set_Fields("PartyAType", cmbPartyAType.ItemData(cmbPartyAType.SelectedIndex));
			rs.Set_Fields("PartyBType", cmbPartyBType.ItemData(cmbPartyBType.SelectedIndex));
			if (mebVoided.Text == string.Empty || !Information.IsDate(mebVoided.Text))
			{
				rs.Set_Fields("VoidedDate", null);
			}
			else
			{
				rs.Set_Fields("VoidedDate", Strings.Format(mebVoided.Text, "MM/dd/yyyy"));
			}
			rs.Set_Fields("GroomsFathersName", txtGroomsFathersName.Text);
			rs.Set_Fields("GroomsMothersName", txtGroomsMothersName.Text);
			rs.Set_Fields("BridesFirstName", txtBridesFirstName.Text);
			rs.Set_Fields("BridesMiddleName", txtBridesMiddleName.Text);
			rs.Set_Fields("BridesMaidenSurname", txtBridesMaidenSurname.Text);
			rs.Set_Fields("BridesCurrentLastName", txtBridesCurrentLastName.Text);
			rs.Set_Fields("PartyBDesignation", txtPartyBDesig.Text);
			rs.Set_Fields("bridesage", FCConvert.ToString(Conversion.Val(txtBridesAge.Text)));
			rs.Set_Fields("BridesStreetName", txtBridesStreetAddress.Text);
			rs.Set_Fields("BridesStreetNumber", txtBridesStreetNumber.Text);
			rs.Set_Fields("BridesCity", txtBridesCity.Text);
			rs.Set_Fields("BridesCounty", txtBridesCounty.Text);
			rs.Set_Fields("BridesState", txtBridesState.Text);
			rs.Set_Fields("BridesDOB", fecherFoundation.Strings.Trim(txtBridesDOB.Text));
			rs.Set_Fields("BridesFathersName", txtBridesFathersName.Text);
			rs.Set_Fields("BridesMothersName", txtBridesMothersName.Text);
			rs.Set_Fields("GroomsMarriageNumber", cboGroomMarriageNumber.Text);
			rs.Set_Fields("GroomsPreviousMarriageEnded", lstGroomsMarriageEndedHow.Text);
			rs.Set_Fields("GroomsPrevMarriageEndDate", fecherFoundation.Strings.Trim(txtGroomsMarriageEndedWhen.Text));
			rs.Set_Fields("GroomDomesticPartner", chkGroomDomesticPartner.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("GroomDomesticPartnerYearRegistered", FCConvert.ToString(Conversion.Val(txtGroomDomesticPartnerYearRegistered.Text)));
			rs.Set_Fields("GroomsCourtName", fecherFoundation.Strings.Trim(txtGroomCourtName.Text));
			rs.Set_Fields("GroomsFormerSpouseName", fecherFoundation.Strings.Trim(txtGroomFormerSpouseName.Text));
			rs.Set_Fields("BridesCourtName", fecherFoundation.Strings.Trim(txtBrideCourtName.Text));
			rs.Set_Fields("BridesFormerSpouseName", fecherFoundation.Strings.Trim(txtBrideFormerSpouseName.Text));
			rs.Set_Fields("BridesMarriageNumber", cboBridesMarriageNumber.Text);
			rs.Set_Fields("BridesPreviousMarriageEnded", lstBridesMarriageEndedHow.Text);
			rs.Set_Fields("BridesPrevMarriageEndDate", fecherFoundation.Strings.Trim(txtBridesMarriageEndedWhen.Text));
			rs.Set_Fields("BrideDomesticPartner", chkBrideDomesticPartner.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("BrideDomesticPartnerYearRegistered", FCConvert.ToString(Conversion.Val(txtBrideDomesticPartnerYearRegistered.Text)));
			rs.Set_Fields("DateIntentionsFiled", fecherFoundation.Strings.Trim(txtDateIntentionsFiled.Text));
			rs.Set_Fields("DateLicenseIssued", fecherFoundation.Strings.Trim(txtDateLicenseIssued.Text));
			rs.Set_Fields("NameOfClerk", txtNameOfClerk.Text);
			if (fecherFoundation.Strings.Trim(txtNameOfClerk.Text) != string.Empty)
			{
				modRegistry.SaveRegistryKey("CKLastClerkName", txtNameOfClerk.Text);
			}
			rs.Set_Fields("LicenseValidUntil", txtDateLicenseValid.Text);
			rs.Set_Fields("CityOfIssue", txtCityofIssue.Text);
			rs.Set_Fields("CeremonyDate", fecherFoundation.Strings.Trim(txtCeremonyDate.Text));
			if (Information.IsDate(txtActualDate.Text))
			{
				rs.Set_Fields("ActualDate", fecherFoundation.DateAndTime.DateValue(txtActualDate.Text));
			}
			else
			{
				rs.Set_Fields("ActualDate", null);
			}
			rs.Set_Fields("CityMarried", txtCityMarried.Text);
			rs.Set_Fields("CountyMarried", txtCountyMarried.Text);
			rs.Set_Fields("PersonPerformingCeremony", txtPersonPerformingCeremony.Text);
			rs.Set_Fields("DateOfLicenseForPerson", txtDateOfCommision.Text);
			rs.Set_Fields("ResidenceOfPersonPerformingCeremony", txtResidenceofPersonPerforming.Text);
			rs.Set_Fields("MailingAddressOfPersonPerformingCeremony", txtMailingAddressofPersonPerforming.Text);
			rs.Set_Fields("NameOfWitnessOne", txtNameofWitnessOne.Text);
			rs.Set_Fields("NameOfWitnessTwo", txtNameofWitnessTwo.Text);
			rs.Set_Fields("TitleofPersonPerforming", txtTitleOfPersonPerforming.Text);
			rs.Set_Fields("DateClerkFiled", fecherFoundation.Strings.Trim(txtDateClerkFiled.Text));
			mnuViewDocs.Enabled = true;
			modDirtyForm.ClearDirtyControls(this);
			intDataChanged = 0;
			return SaveData;
		}

		private void frmMarriages_Load(object sender, System.EventArgs e)
		{
            modCashReceiptingData.Statics.typeCRData.Amount1 = 0;
            modCashReceiptingData.Statics.typeCRData.Amount2 = 0;
            string strSQL = "";
			clsDRWrapper rsClerkInfo = new clsDRWrapper();
			/* Control ctl = new Control(); */
			foreach (Control ctl in this.GetAllControls())
			{
				if (FCConvert.ToString(ctl.Tag) == "Required")
				{
					ctl.BackColor = ColorTranslator.FromOle(StaticSettings.TrioColorSettings.TRIOCOLORHIGHLIGHT);
				}
			}
			// ctl
			SetUpPartyTypeCombos();
			SetupPartyGenderCombos();
			SetupMarriageNumberCombos();
			lngMarriageID = 0;
			modGNBas.Statics.gboolSplitMarriageFee = false;
			SetupGridTownCode();
			SetupTitleCombo();
			//if (modClerkGeneral.Statics.SearchResponse == 0)
            if (ViewModel.VitalId == 0)
            {
				if (MarriageFormLoaded == false)
				{
					MarriageFormLoaded = true;
					strSQL = "SELECT * FROM Marriages order by GroomsLastName";
					rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
					if (rs.EndOfFile() == true && rs.BeginningOfFile() == true)
					{
						// Do Nothing
					}
					else
					{
						rs.MoveLast();
						rs.MoveFirst();
					}
					ClearScreen();
				}
			}
			else
			{
				ClearScreen();
				strSQL = "SELECT * FROM Marriages order by GroomsLastName";
				rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
				//rs.FindFirstRecord("ID", modClerkGeneral.Statics.SearchResponse);
                rs.FindFirstRecord("ID", ViewModel.VitalId);
                if (rs.NoMatch == true)
				{
					MessageBox.Show("No Match:");
				}
				else
				{
					FillScreen();
					modClerkGeneral.Statics.AddingMarriageCertificate = false;
				}
			}
			modGNBas.Statics.gintMarriageNumber = ViewModel.VitalId;
			modAmendments.LoadAmendments(ref VSFlexGrid1, modClerkGeneral.Statics.SearchResponse, "MARRIAGES");
			rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
			if (rsClerkInfo.EndOfFile() != true && rsClerkInfo.BeginningOfFile() != true)
			{
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + rsClerkInfo.Get_Fields_String("LastMarriageFileNumber"));
			}
			else
			{
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: UNKNOWN");
			}
			ToolTip1.SetToolTip(lblFile, ToolTip1.GetToolTip(txtFileNumber));
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this, false);
			if (lngMarriageID > 0)
            {
                mnuViewDocs.Enabled = true;
                imgDocuments.Visible = CheckForDocs(lngMarriageID, "Marriage");
            }
			else
			{
				imgDocuments.Visible = false;
				mnuViewDocs.Enabled = false;
			}
			lblCopyGroomsInfo.ForeColor = ColorTranslator.FromOle(StaticSettings.TrioColorSettings.TRIOCOLORBLUE);
			tabMarriages.SelectedIndex = 0;
		}

        public bool CheckForDocs(int referenceId, string referenceType)
        {
            var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
            var docList = new Collection<CentralDocumentHeader>();

            docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand { DataGroup = "Clerk", ReferenceId = referenceId, ReferenceType = referenceType });

            return docList.Count > 0;
        }

        private void txtBridesAge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
				KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtBridesCity_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				txtBridesCity.Text = modGNBas.Statics.ClerkDefaults.City;
			}
		}

		private void txtBridesCounty_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				txtBridesCounty.Text = modGNBas.Statics.ClerkDefaults.County;
				if (fecherFoundation.Strings.Trim(txtBrideZip.Text) == string.Empty)
					txtBrideZip.Text = modGNBas.Statics.ClerkDefaults.Zip;
				if (fecherFoundation.Strings.Trim(txtBridesCity.Text) == string.Empty)
					txtBridesCity.Text = modGNBas.Statics.ClerkDefaults.City;
			}
		}

		private void txtBridesState_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				txtBridesState.Text = modGNBas.Statics.ClerkDefaults.State;
				if (fecherFoundation.Strings.Trim(txtBridesCounty.Text) == string.Empty)
					txtBridesCounty.Text = modGNBas.Statics.ClerkDefaults.County;
				if (fecherFoundation.Strings.Trim(txtBrideZip.Text) == string.Empty)
					txtBrideZip.Text = modGNBas.Statics.ClerkDefaults.Zip;
				if (fecherFoundation.Strings.Trim(txtBridesCity.Text) == string.Empty)
					txtBridesCity.Text = modGNBas.Statics.ClerkDefaults.City;
			}
		}

		private void txtBrideZip_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				txtBrideZip.Text = modGNBas.Statics.ClerkDefaults.Zip;
				if (fecherFoundation.Strings.Trim(txtBridesCity.Text) == string.Empty)
					txtBridesCity.Text = modGNBas.Statics.ClerkDefaults.City;
			}
		}

		private void txtGroomsAge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
				KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtGroomsCity_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				txtGroomsCity.Text = modGNBas.Statics.ClerkDefaults.City;
			}
		}

		private void txtGroomsCounty_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				txtGroomsCounty.Text = modGNBas.Statics.ClerkDefaults.County;
				if (fecherFoundation.Strings.Trim(txtGroomZip.Text) == string.Empty)
					txtGroomZip.Text = modGNBas.Statics.ClerkDefaults.Zip;
				if (fecherFoundation.Strings.Trim(txtGroomsCity.Text) == string.Empty)
					txtGroomsCity.Text = modGNBas.Statics.ClerkDefaults.City;
			}
		}

		private void txtGroomsState_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				txtGroomsState.Text = modGNBas.Statics.ClerkDefaults.State;
				if (fecherFoundation.Strings.Trim(txtGroomsCounty.Text) == string.Empty)
					txtGroomsCounty.Text = modGNBas.Statics.ClerkDefaults.County;
				if (fecherFoundation.Strings.Trim(txtGroomZip.Text) == string.Empty)
					txtGroomZip.Text = modGNBas.Statics.ClerkDefaults.Zip;
				if (fecherFoundation.Strings.Trim(txtGroomsCity.Text) == string.Empty)
					txtGroomsCity.Text = modGNBas.Statics.ClerkDefaults.City;
			}
		}

		private void txtGroomZip_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				txtGroomZip.Text = modGNBas.Statics.ClerkDefaults.Zip;
				if (fecherFoundation.Strings.Trim(txtGroomsCity.Text) == string.Empty)
					txtGroomsCity.Text = modGNBas.Statics.ClerkDefaults.City;
			}
		}

		private void txtState2_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtState2.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void VSFlexGrid1_Enter(object sender, System.EventArgs e)
		{
			tabMarriages.SelectedIndex = 2;
		}
		// Private Sub VSFlexGrid1_DblClick()
		// Dim counter As Integer
		// Dim blnEmpty As Boolean
		//
		// blnEmpty = True
		// For counter = 0 To VSFlexGrid1.Cols - 1
		// If VSFlexGrid1.TextMatrix(VSFlexGrid1.Row, counter) <> "" Then
		// blnEmpty = False
		// End If
		// Next
		// If Not blnEmpty Then
		// If MsgBox("Delete current record", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
		// Call VSFlexGrid1.RemoveItem(VSFlexGrid1.Row)
		// VSFlexGrid1.AddItem vbNullString
		// End If
		// Else
		// Call VSFlexGrid1.RemoveItem(VSFlexGrid1.Row)
		// VSFlexGrid1.AddItem vbNullString
		// End If
		// End Sub
		private void VSFlexGrid1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			bool blnEmpty = false;
			if (KeyCode == (Keys)46)
			{
				blnEmpty = true;
				for (counter = 0; counter <= (VSFlexGrid1.Cols - 1); counter++)
				{
					if (VSFlexGrid1.TextMatrix(VSFlexGrid1.Row, counter) != "")
					{
						blnEmpty = false;
					}
				}
				if (!blnEmpty)
				{
					if (MessageBox.Show("Delete current record", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						VSFlexGrid1.RemoveItem(VSFlexGrid1.Row);
						VSFlexGrid1.AddItem(string.Empty);
					}
				}
				else
				{
					VSFlexGrid1.RemoveItem(VSFlexGrid1.Row);
					VSFlexGrid1.AddItem(string.Empty);
				}
			}
		}
		// Private Sub VSFlexGrid1_KeyDownEdit(ByVal Row As Long, ByVal Col As Long, KeyCode As Integer, ByVal Shift As Integer)
		// If KeyCode = 46 Then
		// If MsgBox("Delete current record", vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
		// Call VSFlexGrid1.RemoveItem(VSFlexGrid1.Row)
		// VSFlexGrid1.AddItem vbNullString
		// End If
		// End If
		// End Sub
		private void VSFlexGrid1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = VSFlexGrid1[e.ColumnIndex, e.RowIndex];
            int lngRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			lngRow = VSFlexGrid1.GetFlexRowIndex(e.RowIndex);
			intCol = VSFlexGrid1.GetFlexColIndex(e.ColumnIndex);
			if (intCol == 2 || lngRow < 1)
			{
                //ToolTip1.SetToolTip(VSFlexGrid1, "");
                cell.ToolTipText = "";
			}
			else
			{
				//ToolTip1.SetToolTip(VSFlexGrid1, FCConvert.ToString(VSFlexGrid1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, intCol)));
				cell.ToolTipText = FCConvert.ToString(VSFlexGrid1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, intCol));
			}
		}

		private void ResizeVsFlexGrid1()
		{
			int GridWidth = 0;
			GridWidth = VSFlexGrid1.WidthOriginal;
			VSFlexGrid1.ColWidth(1, FCConvert.ToInt32(0.26 * GridWidth));
			VSFlexGrid1.ColWidth(2, FCConvert.ToInt32(0.08 * GridWidth));
			VSFlexGrid1.ColWidth(3, FCConvert.ToInt32(0.21 * GridWidth));
			VSFlexGrid1.ColWidth(4, FCConvert.ToInt32(0.15 * GridWidth));
			VSFlexGrid1.ColWidth(5, FCConvert.ToInt32(0.15 * GridWidth));
		}

		private void SetupTitleCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from titles order by description", "twck0000.vb1");
			txtTitleOfPersonPerforming.Clear();
			while (!clsLoad.EndOfFile())
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
				{
					txtTitleOfPersonPerforming.AddItem(clsLoad.Get_Fields_String("description"));
				}
				clsLoad.MoveNext();
			}
		}

        public IMarriageViewModel ViewModel { get; set; }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }
    }
}
