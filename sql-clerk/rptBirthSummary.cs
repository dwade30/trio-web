//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Clerk.Commands;
using SharedApplication.Clerk.Enums;
using SharedApplication.Extensions;
using TWSharedLibrary;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBirthSummary.
	/// </summary>
	public partial class rptBirthSummary : BaseSectionReport
	{
		public rptBirthSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Birth Vital Statistics";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBirthSummary InstancePtr
		{
			get
			{
				return (rptBirthSummary)Sys.GetInstance(typeof(rptBirthSummary));
			}
		}

		protected rptBirthSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBirthSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		int intPageNumber;
		string strStartDate = "";
		string strEndDate = "";

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			do
			{
				if (Information.IsDate(rs.Get_Fields_String("dateofbirth")) &&
				    Convert.ToDateTime(rs.Get_Fields_String("dateofbirth")) >= Convert.ToDateTime(strStartDate) &&
				    Convert.ToDateTime(rs.Get_Fields_String("dateofbirth")) <= Convert.ToDateTime(strEndDate))
				{
					break;
				}
				else if (rs.Get_Fields_DateTime("ActualDate") >= Convert.ToDateTime(strStartDate) &&
				         rs.Get_Fields_DateTime("ActualDate") <= Convert.ToDateTime(strEndDate))
				{
					break;
				}

				rs.MoveNext();
			} while (!rs.EndOfFile());

			if (rs.EndOfFile())
				return;
			txtName.Text = rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("MiddleName") + " " + rs.Get_Fields_String("LastName");
			txtBorn.Text = rs.Get_Fields_String("dateofbirth");
			if (!rs.EndOfFile())
				rs.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtTown.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "H:mm AM/PM");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtCaption.Text = "Town Clerk's Report";
			txtCaption2.Text = "Births - " + strStartDate + " to " + strEndDate;
			intPageNumber = 1;

			var result = StaticSettings.GlobalCommandDispatcher.Send(new GetBirthIdsBasedOnBirthDate
			{
				Type =  BirthDateType.LiveBirth,
				StartDate = Convert.ToDateTime(strStartDate),
				EndDate = Convert.ToDateTime(strEndDate)
			}).Result;

			string ids = "(";
			foreach (var birthMatch in result)
			{
				ids += (birthMatch + ", ");
			}

			if (ids == "(")
			{
				ids += "0)";
			}
			else
			{
				ids = ids.Left(ids.Length - 2) + ")";
			}

			rs.OpenRecordset("Select firstname, middlename, lastname, dateofbirth, ActualDate from Births WHERE Id IN " + ids, modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				rs.MoveLast();
				rs.MoveFirst();
			}

			if (result.Count() == 0)
			{
				txtTotal.Text = string.Empty;
			}
			else if (result.Count() == 1)
			{
				txtTotal.Text = "There was a total of " + FCConvert.ToString(result.Count()) + " Birth";
			}
			else
			{
				txtTotal.Text = "There were a total of " + FCConvert.ToString(result.Count()) + " Births";
			}
		}

		public void Init(ref string strRange)
		{
			string[] strTemp;
			strTemp = Strings.Split(strRange, ";", -1, CompareConstants.vbTextCompare);
			strStartDate = strTemp[0];
			strEndDate = strTemp[1];
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "BirthSummary");
		}
	}
}
