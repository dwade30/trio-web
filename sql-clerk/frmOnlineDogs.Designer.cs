//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmOnlineDogs.
	/// </summary>
	partial class frmOnlineDogs
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public FCGrid Grid;
		public FCGrid GridNew;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuPrintUnmatched;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuPopUp;
		public fecherFoundation.FCToolStripMenuItem mnuUnmatch;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuExitPopUp;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.Grid = new fecherFoundation.FCGrid();
			this.GridNew = new fecherFoundation.FCGrid();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.mnuPopUp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUnmatch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExitPopUp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintUnmatched = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdPrintUnmatched = new fecherFoundation.FCButton();
			this.cmdUnmatch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintUnmatched)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUnmatch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(682, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.GridNew);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Shape1);
			this.ClientArea.Controls.Add(this.Label1_1);
			this.ClientArea.Controls.Add(this.Label1_0);
			this.ClientArea.Size = new System.Drawing.Size(682, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdUnmatch);
			this.TopPanel.Controls.Add(this.cmdPrintUnmatched);
			this.TopPanel.Size = new System.Drawing.Size(682, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintUnmatched, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdUnmatch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(286, 30);
			this.HeaderText.Text = "Online Dog Registrations";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 57);
			this.Grid.Name = "Grid";
			this.Grid.OutlineCol = 0;
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(624, 254);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.Grid, "Right Click to unmatch entries");
			this.Grid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.Grid_MouseDownEvent);
			// 
			// GridNew
			// 
			this.GridNew.AllowSelection = false;
			this.GridNew.AllowUserToResizeColumns = false;
			this.GridNew.AllowUserToResizeRows = false;
			this.GridNew.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridNew.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridNew.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridNew.BackColorBkg = System.Drawing.Color.Empty;
			this.GridNew.BackColorFixed = System.Drawing.Color.Empty;
			this.GridNew.BackColorSel = System.Drawing.Color.Empty;
			this.GridNew.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridNew.Cols = 10;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridNew.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridNew.ColumnHeadersHeight = 30;
			this.GridNew.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridNew.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridNew.DragIcon = null;
			this.GridNew.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridNew.FixedCols = 0;
			this.GridNew.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridNew.FrozenCols = 0;
			this.GridNew.GridColor = System.Drawing.Color.Empty;
			this.GridNew.GridColorFixed = System.Drawing.Color.Empty;
			this.GridNew.Location = new System.Drawing.Point(30, 358);
			this.GridNew.Name = "GridNew";
			this.GridNew.OutlineCol = 0;
			this.GridNew.ReadOnly = true;
			this.GridNew.RowHeadersVisible = false;
			this.GridNew.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridNew.RowHeightMin = 0;
			this.GridNew.Rows = 1;
			this.GridNew.ScrollTipText = null;
			this.GridNew.ShowColumnVisibilityMenu = false;
			this.GridNew.Size = new System.Drawing.Size(624, 216);
			this.GridNew.StandardTab = true;
			this.GridNew.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridNew.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridNew.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.GridNew, "Use Delete to remove unmatchable transactions from list");
			this.GridNew.KeyDown += new Wisej.Web.KeyEventHandler(this.GridNew_KeyDownEvent);
			this.GridNew.DoubleClick += new System.EventHandler(this.GridNew_DblClick);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(259, 331);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(200, 15);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "UNMATCHED RE-REGISTRATIONS";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.Shape1.Location = new System.Drawing.Point(241, 331);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(10, 12);
			this.Shape1.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.Shape1, null);
			// 
			// Label1_1
			// 
			this.Label1_1.Location = new System.Drawing.Point(30, 331);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(180, 15);
			this.Label1_1.TabIndex = 3;
			this.Label1_1.Text = "UNMATCHED TRANSACTIONS";
			this.ToolTip1.SetToolTip(this.Label1_1, null);
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(30, 30);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(180, 15);
			this.Label1_0.TabIndex = 2;
			this.Label1_0.Text = "MATCHED TRANSACTIONS";
			this.ToolTip1.SetToolTip(this.Label1_0, null);
			// 
			// mnuPopUp
			// 
			this.mnuPopUp.Index = -1;
			this.mnuPopUp.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuUnmatch,
            this.mnuSepar3,
            this.mnuExitPopUp});
			this.mnuPopUp.Name = "mnuPopUp";
			this.mnuPopUp.Text = "PopUp";
			this.mnuPopUp.Visible = false;
			this.mnuPopUp.Click += new System.EventHandler(this.mnuPopUp_Click);
			// 
			// mnuUnmatch
			// 
			this.mnuUnmatch.Index = 0;
			this.mnuUnmatch.Name = "mnuUnmatch";
			this.mnuUnmatch.Text = "Move to Unmatched List";
			this.mnuUnmatch.Click += new System.EventHandler(this.mnuUnmatch_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 1;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuExitPopUp
			// 
			this.mnuExitPopUp.Index = 2;
			this.mnuExitPopUp.Name = "mnuExitPopUp";
			this.mnuExitPopUp.Text = "Exit";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintUnmatched,
            this.mnuSepar2,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuPrintUnmatched
			// 
			this.mnuPrintUnmatched.Index = 0;
			this.mnuPrintUnmatched.Name = "mnuPrintUnmatched";
			this.mnuPrintUnmatched.Text = "Print Unmatched Transactions";
			this.mnuPrintUnmatched.Click += new System.EventHandler(this.mnuPrintUnmatched_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 2;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Continue";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 3;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(257, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(174, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save & Continue";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// cmdPrintUnmatched
			// 
			this.cmdPrintUnmatched.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintUnmatched.AppearanceKey = "toolbarButton";
			this.cmdPrintUnmatched.Location = new System.Drawing.Point(454, 29);
			this.cmdPrintUnmatched.Name = "cmdPrintUnmatched";
			this.cmdPrintUnmatched.Size = new System.Drawing.Size(200, 24);
			this.cmdPrintUnmatched.TabIndex = 1;
			this.cmdPrintUnmatched.Text = "Print Unmatched Transactions";
			this.ToolTip1.SetToolTip(this.cmdPrintUnmatched, null);
			this.cmdPrintUnmatched.Click += new System.EventHandler(this.mnuPrintUnmatched_Click);
			// 
			// cmdUnmatch
			// 
			this.cmdUnmatch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdUnmatch.AppearanceKey = "toolbarButton";
			this.cmdUnmatch.Location = new System.Drawing.Point(288, 29);
			this.cmdUnmatch.Name = "cmdUnmatch";
			this.cmdUnmatch.Size = new System.Drawing.Size(160, 24);
			this.cmdUnmatch.TabIndex = 2;
			this.cmdUnmatch.Text = "Move to Unmatched List";
			this.ToolTip1.SetToolTip(this.cmdUnmatch, null);
			this.cmdUnmatch.Click += new EventHandler(this.mnuUnmatch_Click);
			// 
			// frmOnlineDogs
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(682, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmOnlineDogs";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Online Dog Registrations";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmOnlineDogs_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmOnlineDogs_KeyDown);
			this.Resize += new System.EventHandler(this.frmOnlineDogs_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintUnmatched)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUnmatch)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdPrintUnmatched;
		private FCButton cmdUnmatch;
	}
}