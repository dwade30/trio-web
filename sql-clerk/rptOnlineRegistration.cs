﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptOnlineRegistration.
	/// </summary>
	public partial class rptOnlineRegistration : BaseSectionReport
	{
		public rptOnlineRegistration()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Online Dog Registration";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptOnlineRegistration InstancePtr
		{
			get
			{
				return (rptOnlineRegistration)Sys.GetInstance(typeof(rptOnlineRegistration));
			}
		}

		protected rptOnlineRegistration _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOnlineRegistration	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngRow;
		const int CNSTGRIDCOLOWNERID = 0;
		const int CNSTGRIDCOLDOGID = 1;
		const int CNSTGRIDCOLDOGNAME = 2;
		const int CNSTGRIDCOLTRANSACTIONDATE = 3;
		const int CNSTGRIDCOLTRANSTYPE = 4;
		const int CNSTGRIDCOLTRANSFEE = 5;
		const int CNSTGRIDCOLOWNERFIRST = 6;
		const int CNSTGRIDCOLOWNERMIDDLE = 7;
		const int CNSTGRIDCOLOWNERLAST = 8;
		const int CNSTGRIDCOLBREED = 9;
		const int CNSTGRIDCOLSEX = 10;
		const int CNSTGRIDCOLCOLOR = 11;
		const int CNSTGRIDCOLNEUTER = 12;
		const int CNSTGRIDCOLVET = 13;
		const int CNSTGRIDCOLRABIESCERT = 14;
		const int CNSTGRIDCOLTAG = 15;
		const int CNSTGRIDCOLADDRESS1 = 16;
		const int CNSTGRIDCOLADDRESS2 = 17;
		const int CNSTGRIDCOLCITY = 18;
		const int CNSTGRIDCOLSTATE = 19;
		const int CNSTGRIDCOLZIP = 20;
		const int CNSTGRIDCOLLOCATION = 21;
		const int CNSTGRIDCOLPHONE = 22;
		const int CNSTGRIDCOLEMAIL = 23;
		const int CNSTGRIDCOLRABIESISSUEDATE = 24;
		const int CNSTGRIDCOLDOGDOB = 25;
		const int CNSTGRIDCOLNEUTERCERT = 26;
		const int CNSTGRIDCOLREASON = 27;

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "OnlineRegistrations");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngRow >= frmOnlineDogs.InstancePtr.Grid.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:dd AMPM");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngRow = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow < frmOnlineDogs.InstancePtr.Grid.Rows)
			{
				txtOwner.Text = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST) + " " + frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE)) + " " + frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST));
				txtDog.Text = frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME);
				txtAddress.Text = frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1);
				txtCityStateZip.Text = frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY) + " " + frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE) + "  " + frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP);
				txtTransactionDate.Text = frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE);
				txtFee.Text = frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSFEE);
				if (FCConvert.ToBoolean(frmOnlineDogs.InstancePtr.Grid.RowData(lngRow)))
				{
					txtProcess.Text = "Processed";
					txtNewTag.Text = frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG);
				}
				else
				{
					txtProcess.Text = frmOnlineDogs.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLREASON);
					txtNewTag.Text = "";
				}
			}
			lngRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
