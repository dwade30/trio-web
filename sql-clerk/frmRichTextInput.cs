//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmRichTextInput : BaseForm
	{
		public frmRichTextInput()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmRichTextInput InstancePtr
		{
			get
			{
				return (frmRichTextInput)Sys.GetInstance(typeof(frmRichTextInput));
			}
		}

		protected frmRichTextInput _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By Corey Gray
		// Date 1/4/2005
		// ********************************************************
		string strField;
		string strTable;
		string strDatabase;

		public void Init(string strFieldName, string strTableName, string strDatabaseName, string strCaption, bool boolModal)
		{
			this.Text = strCaption;
            //FC:FINAL:AM:#2508 - set the header text too
            this.HeaderText.Text = strCaption;
			strField = strFieldName;
			strTable = strTableName;
			strDatabase = strDatabaseName;
			LoadText();
			if (boolModal)
			{
				this.Show(FCForm.FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void frmRichTextInput_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmRichTextInput_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRichTextInput properties;
			//frmRichTextInput.FillStyle	= 0;
			//frmRichTextInput.ScaleWidth	= 9045;
			//frmRichTextInput.ScaleHeight	= 7410;
			//frmRichTextInput.LinkTopic	= "Form2";
			//frmRichTextInput.LockControls	= -1  'True;
			//frmRichTextInput.PaletteMode	= 1  'UseZOrder;
			//RichTextBox2 properties;
			//RichTextBox2.ScrollBars	= 2;
			//RichTextBox2.TextRTF	= $"frmRichTextInput.frx":058A;
			//RichTextBox1 properties;
			//RichTextBox1.TextRTF	= $"frmRichTextInput.frx":060E;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
            //FC:FINAL:SBE - #2499 - force cursor position at position 0
            RichTextBox1.Select(0, 0);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveText())
			{
				Close();
			}
		}

		private void LoadText()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				RichTextBox1.Text = "";
				clsLoad.OpenRecordset("select * from " + strTable, strDatabase);
				if (!clsLoad.EndOfFile())
				{
					RichTextBox1.Text = FCConvert.ToString(clsLoad.Get_Fields(strField));
					RichTextBox2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("UnlicensedSignature"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadText", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveText()
		{
			bool SaveText = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveText = false;
				clsSave.OpenRecordset("select * from " + strTable, strDatabase);
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				clsSave.Set_Fields(strField, RichTextBox1.Text);
				clsSave.Set_Fields("unlicensedSignature", RichTextBox2.Text);
				clsSave.Update();
				MessageBox.Show("Save complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveText = true;
				return SaveText;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveText", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveText;
		}
	}
}
