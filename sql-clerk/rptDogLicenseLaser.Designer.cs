﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogLicenseLaser.
	/// </summary>
	partial class rptDogLicenseLaser
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDogLicenseLaser));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDogName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBreed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTagNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMF = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSex = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtColor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtVeterinarian = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLicenseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCertNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNeuter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSSR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHearing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHybrid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMF = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNeuter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRescue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHearing = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblWolf = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtRabiesCertNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDateVaccinated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtVaccineExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmountC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtRabiesTagNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtNuisanceDangerous = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblNuisanceDangerous = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBreed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVeterinarian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCertNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHybrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNeuter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRescue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHearing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWolf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRabiesCertNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateVaccinated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVaccineExpires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRabiesTagNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceDangerous)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNuisanceDangerous)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape1,
            this.Shape3,
            this.txtMuni,
            this.txtOwnerName,
            this.txtPhone,
            this.txtAddress,
            this.txtDogName,
            this.txtBreed,
            this.txtTagNumber,
            this.txtMF,
            this.txtDOB,
            this.txtSex,
            this.txtColor,
            this.txtVeterinarian,
            this.txtLocation,
            this.txtLicenseDate,
            this.txtCertNumber,
            this.txtNeuter,
            this.txtSSR,
            this.txtHearing,
            this.txtHybrid,
            this.txtSticker,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label16,
            this.lblMF,
            this.lblNeuter,
            this.lblRescue,
            this.lblHearing,
            this.lblWolf,
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field5,
            this.Field6,
            this.Field7,
            this.Field8,
            this.Field9,
            this.Field10,
            this.Shape2,
            this.txtRabiesCertNum,
            this.Label17,
            this.txtDateVaccinated,
            this.Label18,
            this.txtVaccineExpires,
            this.Label19,
            this.txtYear,
            this.Field11,
            this.txtAmountC,
            this.Label20,
            this.Label21,
            this.Line3,
            this.txtRabiesTagNum,
            this.Label22,
            this.txtNuisanceDangerous,
            this.lblNuisanceDangerous,
            this.Line2});
            this.PageHeader.Height = 5.09375F;
            this.PageHeader.Name = "PageHeader";
            // 
            // Shape1
            // 
            this.Shape1.Height = 1.625F;
            this.Shape1.Left = 0.4375F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape1.Top = 3.0625F;
            this.Shape1.Width = 2.4375F;
            // 
            // Shape3
            // 
            this.Shape3.Height = 1.625F;
            this.Shape3.Left = 4.875F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape3.Top = 3.0625F;
            this.Shape3.Width = 2.625F;
            // 
            // txtMuni
            // 
            this.txtMuni.Height = 0.1875F;
            this.txtMuni.Left = 1.4375F;
            this.txtMuni.Name = "txtMuni";
            this.txtMuni.Text = null;
            this.txtMuni.Top = 1.75F;
            this.txtMuni.Width = 1.6875F;
            // 
            // txtOwnerName
            // 
            this.txtOwnerName.Height = 0.1875F;
            this.txtOwnerName.Left = 1.4375F;
            this.txtOwnerName.MultiLine = false;
            this.txtOwnerName.Name = "txtOwnerName";
            this.txtOwnerName.Text = null;
            this.txtOwnerName.Top = 0.75F;
            this.txtOwnerName.Width = 3.3125F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.1875F;
            this.txtPhone.Left = 1.4375F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Text = null;
            this.txtPhone.Top = 1.3125F;
            this.txtPhone.Width = 2.25F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1875F;
            this.txtAddress.Left = 1.4375F;
            this.txtAddress.MultiLine = false;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 0.9375F;
            this.txtAddress.Width = 4.6875F;
            // 
            // txtDogName
            // 
            this.txtDogName.Height = 0.1875F;
            this.txtDogName.Left = 1.125F;
            this.txtDogName.Name = "txtDogName";
            this.txtDogName.Text = null;
            this.txtDogName.Top = 2.56F;
            this.txtDogName.Width = 1.5F;
            // 
            // txtBreed
            // 
            this.txtBreed.Height = 0.1875F;
            this.txtBreed.Left = 5.5F;
            this.txtBreed.Name = "txtBreed";
            this.txtBreed.Text = null;
            this.txtBreed.Top = 2.56F;
            this.txtBreed.Width = 1.5F;
            // 
            // txtTagNumber
            // 
            this.txtTagNumber.Height = 0.1875F;
            this.txtTagNumber.Left = 6.5F;
            this.txtTagNumber.Name = "txtTagNumber";
            this.txtTagNumber.Text = null;
            this.txtTagNumber.Top = 0.625F;
            this.txtTagNumber.Width = 0.875F;
            // 
            // txtMF
            // 
            this.txtMF.Height = 0.1875F;
            this.txtMF.Left = 7.312F;
            this.txtMF.Name = "txtMF";
            this.txtMF.Text = "X";
            this.txtMF.Top = 0.897F;
            this.txtMF.Visible = false;
            this.txtMF.Width = 0.1875F;
            // 
            // txtDOB
            // 
            this.txtDOB.Height = 0.1875F;
            this.txtDOB.Left = 3.3125F;
            this.txtDOB.Name = "txtDOB";
            this.txtDOB.Text = null;
            this.txtDOB.Top = 2.56F;
            this.txtDOB.Width = 1F;
            // 
            // txtSex
            // 
            this.txtSex.Height = 0.1875F;
            this.txtSex.Left = 4.6875F;
            this.txtSex.Name = "txtSex";
            this.txtSex.Text = null;
            this.txtSex.Top = 2.56F;
            this.txtSex.Width = 0.25F;
            // 
            // txtColor
            // 
            this.txtColor.Height = 0.1875F;
            this.txtColor.Left = 1.5F;
            this.txtColor.Name = "txtColor";
            this.txtColor.Text = null;
            this.txtColor.Top = 2.7475F;
            this.txtColor.Width = 1.375F;
            // 
            // txtVeterinarian
            // 
            this.txtVeterinarian.Height = 0.1875F;
            this.txtVeterinarian.Left = 3.8125F;
            this.txtVeterinarian.Name = "txtVeterinarian";
            this.txtVeterinarian.Text = null;
            this.txtVeterinarian.Top = 2.7475F;
            this.txtVeterinarian.Width = 3.1875F;
            // 
            // txtLocation
            // 
            this.txtLocation.Height = 0.1875F;
            this.txtLocation.Left = 1.4375F;
            this.txtLocation.MultiLine = false;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Text = null;
            this.txtLocation.Top = 1.125F;
            this.txtLocation.Width = 3.3125F;
            // 
            // txtLicenseDate
            // 
            this.txtLicenseDate.Height = 0.1875F;
            this.txtLicenseDate.Left = 4.5F;
            this.txtLicenseDate.Name = "txtLicenseDate";
            this.txtLicenseDate.Text = null;
            this.txtLicenseDate.Top = 1.5625F;
            this.txtLicenseDate.Width = 0.8125F;
            // 
            // txtCertNumber
            // 
            this.txtCertNumber.Height = 0.1875F;
            this.txtCertNumber.Left = 4.75F;
            this.txtCertNumber.Name = "txtCertNumber";
            this.txtCertNumber.Text = null;
            this.txtCertNumber.Top = 1.75F;
            this.txtCertNumber.Width = 0.8125F;
            // 
            // txtNeuter
            // 
            this.txtNeuter.Height = 0.1875F;
            this.txtNeuter.Left = 7.312F;
            this.txtNeuter.Name = "txtNeuter";
            this.txtNeuter.Text = "X";
            this.txtNeuter.Top = 1.0845F;
            this.txtNeuter.Visible = false;
            this.txtNeuter.Width = 0.1875F;
            // 
            // txtSSR
            // 
            this.txtSSR.Height = 0.1875F;
            this.txtSSR.Left = 7.312F;
            this.txtSSR.Name = "txtSSR";
            this.txtSSR.Text = "X";
            this.txtSSR.Top = 1.272F;
            this.txtSSR.Visible = false;
            this.txtSSR.Width = 0.1875F;
            // 
            // txtHearing
            // 
            this.txtHearing.Height = 0.1875F;
            this.txtHearing.Left = 7.31225F;
            this.txtHearing.Name = "txtHearing";
            this.txtHearing.Text = "X";
            this.txtHearing.Top = 1.448167F;
            this.txtHearing.Visible = false;
            this.txtHearing.Width = 0.1875F;
            // 
            // txtHybrid
            // 
            this.txtHybrid.Height = 0.1875F;
            this.txtHybrid.Left = 7.31225F;
            this.txtHybrid.Name = "txtHybrid";
            this.txtHybrid.Text = "X";
            this.txtHybrid.Top = 1.635667F;
            this.txtHybrid.Visible = false;
            this.txtHybrid.Width = 0.1875F;
            // 
            // txtSticker
            // 
            this.txtSticker.Height = 0.1875F;
            this.txtSticker.Left = 4.875F;
            this.txtSticker.Name = "txtSticker";
            this.txtSticker.Text = null;
            this.txtSticker.Top = 0.625F;
            this.txtSticker.Width = 1.5625F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.4375F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label1.Text = "Owner/Keeper";
            this.Label1.Top = 0.75F;
            this.Label1.Width = 1F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 2.625F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label2.Text = "EXPIRES  DECEMBER";
            this.Label2.Top = 0.125F;
            this.Label2.Width = 1.625F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.4375F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label3.Text = "Mailing Address";
            this.Label3.Top = 0.9375F;
            this.Label3.Width = 1F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.4375F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label4.Text = "Location";
            this.Label4.Top = 1.125F;
            this.Label4.Width = 1F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1875F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0.4375F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label5.Text = "Telephone";
            this.Label5.Top = 1.3125F;
            this.Label5.Width = 1F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.4375F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label6.Text = "Municipality";
            this.Label6.Top = 1.75F;
            this.Label6.Width = 1F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.4375F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label7.Text = "Dog Name";
            this.Label7.Top = 2.56F;
            this.Label7.Width = 0.6875F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.4375F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label8.Text = "Color Description";
            this.Label8.Top = 2.7475F;
            this.Label8.Width = 1.0625F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 3F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label9.Text = "DOB";
            this.Label9.Top = 2.56F;
            this.Label9.Width = 0.3125F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 4.375F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label10.Text = "Sex";
            this.Label10.Top = 2.56F;
            this.Label10.Width = 0.3125F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1875F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 5.0625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label11.Text = "Breed";
            this.Label11.Top = 2.56F;
            this.Label11.Width = 0.4375F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1875F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 3F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label12.Text = "Veterinarian";
            this.Label12.Top = 2.7475F;
            this.Label12.Width = 0.8125F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.1875F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 3.8125F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label13.Text = "Issue Date";
            this.Label13.Top = 1.5625F;
            this.Label13.Width = 0.6875F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1875F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 3.8125F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label14.Text = "Neuter/Spay #";
            this.Label14.Top = 1.75F;
            this.Label14.Width = 0.9375F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.3125F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 6.5F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label16.Text = "Municipality Tag Number";
            this.Label16.Top = 0.3125F;
            this.Label16.Width = 0.9375F;
            // 
            // lblMF
            // 
            this.lblMF.Height = 0.1666667F;
            this.lblMF.HyperLink = null;
            this.lblMF.Left = 6.28075F;
            this.lblMF.Name = "lblMF";
            this.lblMF.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
            this.lblMF.Text = "Male/Female";
            this.lblMF.Top = 0.9178333F;
            this.lblMF.Width = 0.9375F;
            // 
            // lblNeuter
            // 
            this.lblNeuter.Height = 0.1666667F;
            this.lblNeuter.HyperLink = null;
            this.lblNeuter.Left = 6.28075F;
            this.lblNeuter.Name = "lblNeuter";
            this.lblNeuter.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
            this.lblNeuter.Text = "Neuter/Spay";
            this.lblNeuter.Top = 1.0845F;
            this.lblNeuter.Width = 0.9375F;
            // 
            // lblRescue
            // 
            this.lblRescue.Height = 0.1666667F;
            this.lblRescue.HyperLink = null;
            this.lblRescue.Left = 5.656F;
            this.lblRescue.Name = "lblRescue";
            this.lblRescue.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
            this.lblRescue.Text = "Service/Search & Rescue";
            this.lblRescue.Top = 1.282F;
            this.lblRescue.Width = 1.5625F;
            // 
            // lblHearing
            // 
            this.lblHearing.Height = 0.1666667F;
            this.lblHearing.HyperLink = null;
            this.lblHearing.Left = 6.281F;
            this.lblHearing.Name = "lblHearing";
            this.lblHearing.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
            this.lblHearing.Text = "Hearing/Guide";
            this.lblHearing.Top = 1.469F;
            this.lblHearing.Width = 0.9375F;
            // 
            // lblWolf
            // 
            this.lblWolf.Height = 0.167F;
            this.lblWolf.HyperLink = null;
            this.lblWolf.Left = 6.281F;
            this.lblWolf.Name = "lblWolf";
            this.lblWolf.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
            this.lblWolf.Text = "Wolf/Hybrid";
            this.lblWolf.Top = 1.635667F;
            this.lblWolf.Width = 0.9375F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.1875F;
            this.Field1.Left = 0.5F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "text-align: center";
            this.Field1.Text = "Maine Department of Agriculture";
            this.Field1.Top = 3.125F;
            this.Field1.Width = 2.3125F;
            // 
            // Field2
            // 
            this.Field2.Height = 0.5625F;
            this.Field2.Left = 4.9375F;
            this.Field2.Name = "Field2";
            this.Field2.Text = "Renewal for dog licensing is by January 1st of each year. A late fee will be appl" +
    "ied after January 31st.";
            this.Field2.Top = 3.6875F;
            this.Field2.Width = 2.5F;
            // 
            // Field3
            // 
            this.Field3.Height = 0.5F;
            this.Field3.Left = 4.9375F;
            this.Field3.Name = "Field3";
            this.Field3.Text = "The State of Maine requires dogs to be licensed at 6 months of age or within 10 d" +
    "ays of ownership.                                                               " +
    "                    ";
            this.Field3.Top = 3.125F;
            this.Field3.Width = 2.5F;
            // 
            // Field4
            // 
            this.Field4.Height = 0.1875F;
            this.Field4.Left = 2.9375F;
            this.Field4.Name = "Field4";
            this.Field4.Style = "text-align: center";
            this.Field4.Text = "STATE OF MAINE";
            this.Field4.Top = 3.125F;
            this.Field4.Width = 1.875F;
            // 
            // Field5
            // 
            this.Field5.Height = 0.1875F;
            this.Field5.Left = 2.9375F;
            this.Field5.Name = "Field5";
            this.Field5.Style = "text-align: center";
            this.Field5.Text = "DOG LICENSE";
            this.Field5.Top = 3.5F;
            this.Field5.Width = 1.875F;
            // 
            // Field6
            // 
            this.Field6.Height = 0.1875F;
            this.Field6.Left = 0.5F;
            this.Field6.Name = "Field6";
            this.Field6.Style = "text-align: center";
            this.Field6.Text = "Division of Animal Health & Industry";
            this.Field6.Top = 3.3125F;
            this.Field6.Width = 2.3125F;
            // 
            // Field7
            // 
            this.Field7.Height = 0.1875F;
            this.Field7.Left = 0.5F;
            this.Field7.Name = "Field7";
            this.Field7.Style = "text-align: center";
            this.Field7.Text = "Animal Welfare Program";
            this.Field7.Top = 3.5F;
            this.Field7.Width = 2.3125F;
            // 
            // Field8
            // 
            this.Field8.Height = 0.1875F;
            this.Field8.Left = 0.5F;
            this.Field8.Name = "Field8";
            this.Field8.Style = "text-align: center";
            this.Field8.Text = "28 State House Station";
            this.Field8.Top = 3.6875F;
            this.Field8.Width = 2.3125F;
            // 
            // Field9
            // 
            this.Field9.Height = 0.1875F;
            this.Field9.Left = 0.5F;
            this.Field9.Name = "Field9";
            this.Field9.Style = "text-align: center";
            this.Field9.Text = "Augusta, ME 04333-0028";
            this.Field9.Top = 3.875F;
            this.Field9.Width = 2.3125F;
            // 
            // Field10
            // 
            this.Field10.Height = 0.1875F;
            this.Field10.Left = 0.5F;
            this.Field10.Name = "Field10";
            this.Field10.Style = "text-align: center";
            this.Field10.Text = "(207) 287-3846";
            this.Field10.Top = 4.0625F;
            this.Field10.Width = 2.3125F;
            // 
            // Shape2
            // 
            this.Shape2.Height = 1.625F;
            this.Shape2.Left = 2.875F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape2.Top = 3.0625F;
            this.Shape2.Width = 2F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.4375F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 2.435F;
            this.Line2.Width = 7.0625F;
            this.Line2.X1 = 0.4375F;
            this.Line2.X2 = 7.5F;
            this.Line2.Y1 = 2.435F;
            this.Line2.Y2 = 2.435F;
            // 
            // txtRabiesCertNum
            // 
            this.txtRabiesCertNum.Height = 0.1875F;
            this.txtRabiesCertNum.Left = 1.6875F;
            this.txtRabiesCertNum.Name = "txtRabiesCertNum";
            this.txtRabiesCertNum.Text = null;
            this.txtRabiesCertNum.Top = 2.06F;
            this.txtRabiesCertNum.Width = 1.375F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 0.4375F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label17.Text = "Rabies Certificate #";
            this.Label17.Top = 2.06F;
            this.Label17.Width = 1.3125F;
            // 
            // txtDateVaccinated
            // 
            this.txtDateVaccinated.Height = 0.1875F;
            this.txtDateVaccinated.Left = 4.1875F;
            this.txtDateVaccinated.Name = "txtDateVaccinated";
            this.txtDateVaccinated.Text = null;
            this.txtDateVaccinated.Top = 2.06F;
            this.txtDateVaccinated.Width = 1.0625F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 3.1875F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label18.Text = "Date Vaccinated";
            this.Label18.Top = 2.06F;
            this.Label18.Width = 1F;
            // 
            // txtVaccineExpires
            // 
            this.txtVaccineExpires.Height = 0.1875F;
            this.txtVaccineExpires.Left = 6.4375F;
            this.txtVaccineExpires.Name = "txtVaccineExpires";
            this.txtVaccineExpires.Text = null;
            this.txtVaccineExpires.Top = 2.06F;
            this.txtVaccineExpires.Width = 1.0625F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.2083333F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 5.4375F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label19.Text = "Vaccine Expires";
            this.Label19.Top = 2.06F;
            this.Label19.Width = 0.96875F;
            // 
            // txtYear
            // 
            this.txtYear.Height = 0.1875F;
            this.txtYear.Left = 4.5F;
            this.txtYear.Name = "txtYear";
            this.txtYear.Text = null;
            this.txtYear.Top = 0.125F;
            this.txtYear.Width = 0.875F;
            // 
            // Field11
            // 
            this.Field11.Height = 0.1875F;
            this.Field11.Left = 4.1875F;
            this.Field11.Name = "Field11";
            this.Field11.Style = "font-weight: bold";
            this.Field11.Text = "31,";
            this.Field11.Top = 0.125F;
            this.Field11.Width = 0.25F;
            // 
            // txtAmountC
            // 
            this.txtAmountC.Height = 0.1666667F;
            this.txtAmountC.Left = 6.4375F;
            this.txtAmountC.Name = "txtAmountC";
            this.txtAmountC.Style = "text-align: left";
            this.txtAmountC.Text = null;
            this.txtAmountC.Top = 2.226667F;
            this.txtAmountC.Width = 1.03125F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1666667F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 5.4375F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label20.Text = "Total Charged";
            this.Label20.Top = 2.226667F;
            this.Label20.Width = 1F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.1666667F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 1.166667F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-size: 8.5pt";
            this.Label21.Text = "Owner / Keeper Signature";
            this.Label21.Top = 4.916667F;
            this.Label21.Width = 1.75F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 3F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 5.083333F;
            this.Line3.Width = 3F;
            this.Line3.X1 = 3F;
            this.Line3.X2 = 6F;
            this.Line3.Y1 = 5.083333F;
            this.Line3.Y2 = 5.083333F;
            // 
            // txtRabiesTagNum
            // 
            this.txtRabiesTagNum.Height = 0.1875F;
            this.txtRabiesTagNum.Left = 1.6875F;
            this.txtRabiesTagNum.Name = "txtRabiesTagNum";
            this.txtRabiesTagNum.Text = null;
            this.txtRabiesTagNum.Top = 2.2475F;
            this.txtRabiesTagNum.Width = 1.375F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.1875F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 0.4375F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label22.Text = "Rabies Tag #";
            this.Label22.Top = 2.2475F;
            this.Label22.Width = 1.3125F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtNuisanceDangerous
            // 
            this.txtNuisanceDangerous.Height = 0.1875F;
            this.txtNuisanceDangerous.Left = 7.312249F;
            this.txtNuisanceDangerous.Name = "txtNuisanceDangerous";
            this.txtNuisanceDangerous.Text = "X";
            this.txtNuisanceDangerous.Top = 1.833F;
            this.txtNuisanceDangerous.Visible = false;
            this.txtNuisanceDangerous.Width = 0.1875F;
            // 
            // lblNuisanceDangerous
            // 
            this.lblNuisanceDangerous.Height = 0.167F;
            this.lblNuisanceDangerous.HyperLink = null;
            this.lblNuisanceDangerous.Left = 6.281F;
            this.lblNuisanceDangerous.Name = "lblNuisanceDangerous";
            this.lblNuisanceDangerous.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
            this.lblNuisanceDangerous.Text = "Nuisance";
            this.lblNuisanceDangerous.Top = 1.833F;
            this.lblNuisanceDangerous.Width = 0.9375F;
            // 
            // rptDogLicenseLaser
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.1388889F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.604167F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBreed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVeterinarian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCertNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHybrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNeuter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRescue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHearing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWolf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRabiesCertNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateVaccinated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVaccineExpires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRabiesTagNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceDangerous)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNuisanceDangerous)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBreed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMF;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSex;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtColor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVeterinarian;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCertNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHybrid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSticker;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMF;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNeuter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRescue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHearing;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWolf;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRabiesCertNum;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateVaccinated;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVaccineExpires;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountC;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRabiesTagNum;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNuisanceDangerous;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNuisanceDangerous;
    }
}
