//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDeletedInventory.
	/// </summary>
	public partial class rptDeletedInventory : BaseSectionReport
	{
		public rptDeletedInventory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Deleted Inventory";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDeletedInventory InstancePtr
		{
			get
			{
				return (rptDeletedInventory)Sys.GetInstance(typeof(rptDeletedInventory));
			}
		}

		protected rptDeletedInventory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDeletedInventory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();

		public void Init()
		{
			rsReport.OpenRecordset("select * from cya where DESCRIPTION1 = 'Deleted Tags' or description1 = 'Deleted Kennel Licenses' order by CYADate desc", "twck0000.vb1");
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this, strAttachmentName: "Deleted Inventory");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtPage.Text = "Page 1";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				txtCYADate.Text = Strings.Format(rsReport.Get_Fields("cyadate"), "MM/dd/yyyy");
				txtFrom.Text = rsReport.Get_Fields_String("description3");
				txtTo.Text = rsReport.Get_Fields_String("description4");
				txtReason.Text = rsReport.Get_Fields_String("description5");
				txtUser.Text = rsReport.Get_Fields_String("UserID");
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsReport.Get_Fields_String("description1"))) == "DELETED TAGS")
				{
					txtType.Text = "Tag";
					if (Conversion.Val(rsReport.Get_Fields_String("description2")) > 0)
					{
						txtYear.Text = FCConvert.ToString(rsReport.Get_Fields_String("description2"));
					}
					else
					{
						txtYear.Text = "";
					}
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsReport.Get_Fields_String("description1"))) == "DELETED KENNEL LICENSES")
				{
					txtType.Text = "Kennel License";
					txtYear.Text = "";
				}
				else
				{
					txtType.Text = "Tag";
					if (Conversion.Val(rsReport.Get_Fields_String("description2")) > 0)
					{
						txtYear.Text = FCConvert.ToString(rsReport.Get_Fields_String("description2"));
					}
					else
					{
						txtYear.Text = "";
					}
				}
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
