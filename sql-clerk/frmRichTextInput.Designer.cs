//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmRichTextInput.
	/// </summary>
	partial class frmRichTextInput
	{
		public fecherFoundation.FCRichTextBox RichTextBox2;
		public fecherFoundation.FCRichTextBox RichTextBox1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.RichTextBox2 = new fecherFoundation.FCRichTextBox();
            this.RichTextBox1 = new fecherFoundation.FCRichTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RichTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(798, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.RichTextBox2);
            this.ClientArea.Controls.Add(this.RichTextBox1);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(798, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(798, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(300, 30);
            this.HeaderText.Text = "Unlicensed Dog Reminder";
            // 
            // RichTextBox2
            // 
            this.RichTextBox2.Location = new System.Drawing.Point(30, 457);
            this.RichTextBox2.Multiline = true;
            this.RichTextBox2.Name = "RichTextBox2";
            this.RichTextBox2.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
            this.RichTextBox2.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
            this.RichTextBox2.SelTabCount = null;
            this.RichTextBox2.Size = new System.Drawing.Size(322, 60);
            this.RichTextBox2.TabIndex = 4;
            // 
            // RichTextBox1
            // 
            this.RichTextBox1.Location = new System.Drawing.Point(30, 82);
            this.RichTextBox1.Multiline = true;
            this.RichTextBox1.Name = "RichTextBox1";
            this.RichTextBox1.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
            this.RichTextBox1.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
            this.RichTextBox1.SelTabCount = null;
            this.RichTextBox1.Size = new System.Drawing.Size(740, 339);
            this.RichTextBox1.TabIndex = 0;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 431);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(139, 15);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "VERY TRULY YOURS,";
            this.Label3.ToolTipText = null;
            this.Label3.Visible = false;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(187, 15);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "DEAR DOG OWNER/KEEPER";
            this.Label2.ToolTipText = null;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 56);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(595, 15);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "OUR RECORDS INDICATE THAT YOU ARE THE OWNER OR KEEPER OF X DOG(S) OVER SIX MONTHS" +
    " OF AGE";
            this.Label1.ToolTipText = null;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 0;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(272, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.ToolTipText = null;
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmRichTextInput
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(798, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmRichTextInput";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Unlicensed Dog Reminder";
            this.Load += new System.EventHandler(this.frmRichTextInput_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRichTextInput_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RichTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}