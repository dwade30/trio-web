﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for srptBulkEmailList.
	/// </summary>
	public partial class srptBulkEmailList : FCSectionReport
	{
		public srptBulkEmailList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptBulkEmailList InstancePtr
		{
			get
			{
				return (srptBulkEmailList)Sys.GetInstance(typeof(srptBulkEmailList));
			}
		}

		protected srptBulkEmailList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBulkEmailList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsBulkEmail lstEmails = new clsBulkEmail();

		public clsBulkEmail EmailList
		{
			set
			{
				lstEmails = value;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !lstEmails.IsCurrent();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// If Not Me.Tag Is Nothing Then
			// Set lstEmails = Me.Tag
			// Else
			// Set lstEmails = New clsBulkEmail
			// End If
			lstEmails.MoveFirst();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lstEmails.IsCurrent())
			{
				clsEmailObject tMail;
				tMail = lstEmails.GetCurrent();
				if (!(tMail == null))
				{
					txtAddress.Text = tMail.EmailAddress;
					txtTag.Text = tMail.Tag;
				}
				lstEmails.MoveNext();
			}
		}

		
	}
}
