﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDeathReport : BaseForm
	{
		public frmDeathReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDeathReport InstancePtr
		{
			get
			{
				return (frmDeathReport)Sys.GetInstance(typeof(frmDeathReport));
			}
		}

		protected frmDeathReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmDeathReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDeathReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDeathReport properties;
			//frmDeathReport.FillStyle	= 0;
			//frmDeathReport.ScaleWidth	= 5880;
			//frmDeathReport.ScaleHeight	= 4020;
			//frmDeathReport.LinkTopic	= "Form2";
			//frmDeathReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
		}

		private void SetupGrid()
		{
			Grid.TextMatrix(0, 0, "Last Name");
			Grid.TextMatrix(1, 0, "Date of Death");
			Grid.TextMatrix(2, 0, "Age");
			Grid.TextMatrix(3, 0, "Place of Disposition");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.30));
			Grid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.35));
			//Grid.HeightOriginal = 4 * Grid.RowHeight(0) + 30;
		}

		private void frmDeathReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (Grid.Row == 1)
			{
				string strD = "";
				if (fecherFoundation.Strings.Trim(Grid.EditText).Length == 8)
				{
					if (Information.IsNumeric(fecherFoundation.Strings.Trim(Grid.EditText)))
					{
						strD = fecherFoundation.Strings.Trim(Grid.EditText);
						strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
						if (Information.IsDate(strD))
						{
							Grid.EditText = strD;
						}
					}
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			string strAnd;
			string strWhere;
			string strOrder = "";
			Grid.Row = -1;
			//App.DoEvents();
			if (cmbSort.Text == "Name")
			{
				strOrder = " order by lastname,firstname ";
			}
			else if (cmbSort.Text == "Date of Death")
			{
				strOrder = " order by dateofdeath ";
			}
			else if (cmbSort.Text == "Age")
			{
				strOrder = " order by val(age & '') ";
			}
			else
			{
				strOrder = " order by placeofdisposition";
			}
			strWhere = "";
			strAnd = "";
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(0, 1)) != string.Empty)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(0, 2)) != string.Empty)
				{
					strWhere = " lastname >= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(0, 1)) + "' and lastname <= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(0, 2)) + "zzz' ";
				}
				else
				{
					strWhere = " lastname = '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(0, 1)) + "' ";
				}
				strAnd = " and ";
			}
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(1, 1)) != string.Empty)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(1, 2)) != string.Empty)
				{
					if (Information.IsDate(Grid.TextMatrix(1, 1)) && Information.IsDate(Grid.TextMatrix(1, 2)))
					{
						strWhere += strAnd + " (((isdate(dateofdeath) = 1 and convert(datetime,dateofdeath) between '" + Grid.TextMatrix(1, 1) + "' and '" + Grid.TextMatrix(1, 2) + "') or (isdate(dateofdeathdescription, 1 and convert(datetime,dateofdeathdescription) between '" + Grid.TextMatrix(1, 1) + "' and '" + Grid.TextMatrix(1, 2) + "' )) OR (ActualDate between '" + Grid.TextMatrix(1, 1) + "' and '" + Grid.TextMatrix(1, 2) + "'))";
					}
					else
					{
						strWhere += strAnd + " dateofdeathdescription between '" + Grid.TextMatrix(1, 1) + "' and '" + Grid.TextMatrix(1, 2) + "' ";
					}
				}
				else
				{
					if (Information.IsDate(Grid.TextMatrix(1, 1)))
					{
						strWhere += strAnd + " (((isdate(dateofdeath) = 1 and convert(datetime,dateofdeath) = '" + Grid.TextMatrix(1, 1) + "') or (isdate(dateofdeathdescription, 1 and convert(datetime,dateofdeathdescription, '" + Grid.TextMatrix(1, 1) + "')) OR (ActualDate = '" + Grid.TextMatrix(1, 1) + "'))";
					}
					else
					{
						strWhere += strAnd + " dateofdeathdescription = '" + Grid.TextMatrix(1, 1) + "' ";
					}
				}
				strAnd = " and ";
			}
			else if (cmbSort.Text == "Date of Death")
			{
				strWhere += strAnd + " isdate(dateofdeath) = 1 ";
				strAnd = " and ";
			}
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(2, 1)) != string.Empty)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(2, 2)) != string.Empty)
				{
					strWhere += strAnd + " age >= " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(2, 1))) + " and age <= " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(2, 2)));
				}
				else
				{
					strWhere += strAnd + " age = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(2, 1)));
				}
				strAnd = " and ";
			}
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(3, 1)) != string.Empty)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(3, 2)) != string.Empty)
				{
					strWhere += strAnd + " placeofdisposition >= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(3, 1)) + "' and placeofdisposition <= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(3, 2)) + "zzz' ";
				}
				else
				{
					strWhere += strAnd + " placeofdisposition = '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(3, 1)) + "' ";
				}
				strAnd = " and ";
			}
			if (strWhere != string.Empty)
			{
				strWhere = " where " + strWhere;
			}
			strSQL = "select * from deaths " + strWhere + strOrder;
			rptDeathReport.InstancePtr.Init(ref strSQL);
		}
	}
}
