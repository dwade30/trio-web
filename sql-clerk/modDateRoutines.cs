﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public class modDateRoutines
	{
		//=========================================================
		// vbPorter upgrade warning: MyDate As object	OnWrite(DateTime)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public static object LongDate(object MyDate)
		{
			object LongDate = null;
			if (FCConvert.ToString(MyDate).Length == 8)
			{
				if (Strings.InStr(1, FCConvert.ToString(MyDate), "/", CompareConstants.vbBinaryCompare) > 0)
				{
					LongDate = Strings.Format(MyDate, "MM/dd/yyyy");
				}
				else
				{
					LongDate = MyDate;
					return LongDate;
				}
			}
			else
			{
				LongDate = Strings.Format(MyDate, "MM/dd/yyyy");
			}
			return LongDate;
		}

		public static string GetPlaceOfDeath(string intValue)
		{
			string GetPlaceOfDeath = "";
			if (intValue == "0")
			{
				GetPlaceOfDeath = "DOA";
			}
			else if (intValue == "1")
			{
				GetPlaceOfDeath = "Inpatient";
			}
			else if (intValue == "2")
			{
				GetPlaceOfDeath = "ER/Outpatient";
			}
			else if (intValue == "3")
			{
				GetPlaceOfDeath = "Nursing Home";
			}
			else if (intValue == "4")
			{
				GetPlaceOfDeath = "Residence";
			}
			else if (intValue == "5")
			{
				GetPlaceOfDeath = "Other";
			}
			return GetPlaceOfDeath;
		}

		public static int GetPlaceOfDeathID(string strValue)
		{
			int GetPlaceOfDeathID = 0;
			if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strValue)) == "DOA")
			{
				GetPlaceOfDeathID = 0;
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strValue)) == "INPATIENT")
			{
				GetPlaceOfDeathID = 1;
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strValue)) == "ER/OUTPATIENT")
			{
				GetPlaceOfDeathID = 2;
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strValue)) == "NURSING HOME")
			{
				GetPlaceOfDeathID = 3;
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strValue)) == "RESIDENCE")
			{
				GetPlaceOfDeathID = 4;
			}
			else
			{
				GetPlaceOfDeathID = 5;
			}
			return GetPlaceOfDeathID;
		}
		// vbPorter upgrade warning: MyDate As object	OnWrite(string, object)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object StripDateSlashes(object MyDate)
		{
			object StripDateSlashes = null;
			// strips out the dash marks if the number came from a masked
			// edit box such as a sub-account number
			// vbPorter upgrade warning: i As int	OnWriteFCConvert.ToInt32(
			int i;
			string strTempNumber;
			strTempNumber = string.Empty;
			for (i = 0; i <= (FCConvert.ToString(MyDate).Length); i++)
			{
				if (Strings.Left(FCConvert.ToString(MyDate), 1) != "/" && Strings.Left(FCConvert.ToString(MyDate), 1) != "_")
				{
					strTempNumber += Strings.Left(FCConvert.ToString(MyDate), 1);
				}
				if (!(FCConvert.ToString(MyDate) == string.Empty))
				{
					MyDate = Strings.Mid(FCConvert.ToString(MyDate), 2, FCConvert.ToString(MyDate).Length - 1);
				}
			}
			// i
			// return the new number
			StripDateSlashes = fecherFoundation.Strings.Trim(strTempNumber);
			return StripDateSlashes;
		}
		// vbPorter upgrade warning: ControlName As object	OnWrite(string)
		public static void DateMask(T2KDateBox ControlName)
		{
			string TempDate;
			TempDate = fecherFoundation.Strings.Trim(FCConvert.ToString(StripDateSlashes(ControlName)));
			if (TempDate != string.Empty)
			{
				if (TempDate.Length == 1)
				{
					if (FCConvert.ToDouble(TempDate) > 1)
					{
						ControlName.Text = "0" + TempDate;
						ControlName.SelStart = 3;
					}
				}
				else if (TempDate.Length == 2)
				{
					if (FCConvert.ToDouble(Strings.Left(TempDate, 1)) == 1 && FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 2)
					{
						ControlName.Text = Strings.Left(TempDate, 1);
						ControlName.SelStart = 1;
					}
				}
				else if (TempDate.Length == 3)
				{
					if (FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 3)
					{
						ControlName.Text = Strings.Left(TempDate, 2) + "0" + Strings.Right(TempDate, 1);
						ControlName.SelStart = 6;
					}
				}
				else if (TempDate.Length == 4)
				{
					if (FCConvert.ToDouble(Strings.Mid(TempDate, 3, 1)) == 3 && FCConvert.ToDouble(Strings.Right(TempDate, 1)) > 1)
					{
						ControlName.Text = Strings.Left(TempDate, 3);
						ControlName.SelStart = 4;
					}
					// No need to check 5th digit
				}
				else if (TempDate.Length == 6)
				{
					if ((FCConvert.ToDouble(Strings.Right(TempDate, 2)) != 19) && (FCConvert.ToDouble(Strings.Right(TempDate, 2)) != 20))
					{
						if (FCConvert.ToDouble(Strings.Right(TempDate, 2)) > 60)
						{
							ControlName.Text = Strings.Left(TempDate, 4) + "19" + Strings.Right(TempDate, 2);
						}
						else
						{
							ControlName.Text = Strings.Left(TempDate, 4) + "20" + Strings.Right(TempDate, 2);
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: DateValue As string	OnWrite(string, MSMask.MaskEdBox)
		public static string ConvertDateToHaveSlashes(string DateValue)
		{
			string ConvertDateToHaveSlashes = "";
			string str_date;
			if (DateValue == string.Empty)
			{
				DateValue = CurrentDate();
			}
			str_date = fecherFoundation.Strings.Trim(DateValue);
			if (str_date.Length == 10)
			{
				ConvertDateToHaveSlashes = str_date;
			}
			else
			{
				ConvertDateToHaveSlashes = Strings.Left(str_date, 2) + "/" + Strings.Mid(str_date, 3, 2) + "/" + Strings.Right(str_date, 4);
			}
			return ConvertDateToHaveSlashes;
		}

		public static string ConvertDateToHaveSlashes2(string DateValue)
		{
			string ConvertDateToHaveSlashes2 = "";
			string str_date;
			if (DateValue == string.Empty)
			{
				DateValue = CurrentDate();
			}
			str_date = fecherFoundation.Strings.Trim(DateValue);
			ConvertDateToHaveSlashes2 = Strings.Mid(str_date, 3, 2) + "/" + Strings.Left(str_date, 2) + "/" + Strings.Right(str_date, 4);
			return ConvertDateToHaveSlashes2;
		}

		public static string ConvertDate(string DateValue)
		{
			string ConvertDate = "";
			string str_date;
			if (DateValue == string.Empty)
			{
				return ConvertDate;
			}
			str_date = fecherFoundation.Strings.Trim(DateValue);
			ConvertDate = Strings.Left(str_date, 2) + "/" + Strings.Mid(str_date, 3, 2) + "/" + Strings.Right(str_date, 4);
			return ConvertDate;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object MakeFullDate(string DateValue)
		{
			object MakeFullDate = null;
			MakeFullDate = Strings.Format(DateValue, "MM/dd/yyyy");
			return MakeFullDate;
		}

		public static string CurrentDate()
		{
			string CurrentDate = "";
			string TempDate;
			TempDate = FCConvert.ToString(Conversion.Val(Strings.Format(DateTime.Now, "MMddyyyy")));
			if (TempDate.Length == 7)
			{
				CurrentDate = 0 + TempDate;
			}
			else
			{
				CurrentDate = TempDate;
			}
			return CurrentDate;
		}

		public static bool InvalidMaskedDate(ref Wisej.Web.MaskedTextBox mskTbx, ref string strFieldName)
		{
			bool InvalidMaskedDate = false;
			try
			{
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: strDate As object	OnWrite(string)
				object strDate;
				InvalidMaskedDate = false;
				// Anticipate no problems
				strDate = ConvertDateToHaveSlashes(mskTbx.Text);
				if (!(Information.IsDate(strDate)) || ((DateTime)strDate).Year < 1800 || ((DateTime)strDate).Year > 2200)
				{
					InvalidMaskedDate = true;
					// Problem.Fields("
					MessageBox.Show("Invalid Entry for " + strFieldName, null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					mskTbx.Focus();
					mskTbx.SelectionStart = 0;
					mskTbx.SelectionLength = 10;
				}
				return InvalidMaskedDate;
			}
			catch (Exception ex)
			{
			}
			return InvalidMaskedDate;
		}

		public static bool InvalidMaskedDate2(ref Wisej.Web.MaskedTextBox mskTbx, ref string strFieldName)
		{
			bool InvalidMaskedDate2 = false;
			try
			{
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: strDate As object	OnWrite(string)
				object strDate;
				InvalidMaskedDate2 = false;
				// Anticipate no problems
				strDate = ConvertDateToHaveSlashes(mskTbx.Text);
				if (FCConvert.ToDouble(Strings.Left(FCConvert.ToString(strDate), 2)) > 13 || ((DateTime)strDate).Year < 1800 || ((DateTime)strDate).Year > 2200 || FCConvert.ToDouble(Strings.Mid(FCConvert.ToString(strDate), 4, 2)) > 32)
				{
					InvalidMaskedDate2 = true;
					// Problem.Fields("
					MessageBox.Show("Invalid Entry for " + strFieldName, null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					mskTbx.Focus();
					mskTbx.SelectionStart = 0;
					mskTbx.SelectionLength = 10;
				}
				return InvalidMaskedDate2;
			}
			catch (Exception ex)
			{
			}
			return InvalidMaskedDate2;
		}
		// Routine For Selecting a Text Box value for Change
		public static void Reselect(ref FCTextBox txtTbx)
		{
			txtTbx.SelectionStart = 0;
			txtTbx.SelectionLength = txtTbx.Text.Length;
		}
	}
}
