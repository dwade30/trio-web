//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogLicenseSummary.
	/// </summary>
	public partial class rptDogLicenseSummary : BaseSectionReport
	{
		public rptDogLicenseSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Dog Summary";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDogLicenseSummary InstancePtr
		{
			get
			{
				return (rptDogLicenseSummary)Sys.GetInstance(typeof(rptDogLicenseSummary));
			}
		}

		protected rptDogLicenseSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDogLicenseSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngYearToUse;
		bool boolJustSummary;
		DateTime dtStart;
		DateTime dtEnd;
		bool boolBothYears;
		clsDRWrapper rsReport = new clsDRWrapper();

		public void Init(ref int lngYear, ref bool boolSummary)
		{
			int X;
			string strSQL;
			boolJustSummary = boolSummary;
			lngYearToUse = lngYear;
			lblYear.Text = FCConvert.ToString(lngYear);
			boolBothYears = false;
			if (lngYear < DateTime.Today.Year || DateTime.Today.Month > 9)
			{
				// do both years
				boolBothYears = true;
			}
			strSQL = "select month(transactiondate) as tmonth from dogtransactions where year(transactiondate) = " + FCConvert.ToString(lngYearToUse) + " group by month(transactiondate) order by month(transactiondate)";
			rsReport.OpenRecordset(strSQL, "twck0000.vb1");
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No transactions found", "No Transactions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "DogSummary");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblMuni.Text = modGlobalConstants.Statics.MuniName;
			lblStickerYear1.Text = FCConvert.ToString(lngYearToUse);
			lblStickerYear2.Text = FCConvert.ToString(lngYearToUse + 1);
			modGNBas.Statics.typDogSummary.lngAddedToKennel1 = 0;
			modGNBas.Statics.typDogSummary.lngAddedToKennel2 = 0;
			modGNBas.Statics.typDogSummary.lngHearingGuide1 = 0;
			modGNBas.Statics.typDogSummary.lngHearingGuide2 = 0;
			modGNBas.Statics.typDogSummary.lngKennelLicenses = 0;
			modGNBas.Statics.typDogSummary.lngKennelSticker1 = 0;
			modGNBas.Statics.typDogSummary.lngKennelSticker2 = 0;
			modGNBas.Statics.typDogSummary.lngMF1 = 0;
			modGNBas.Statics.typDogSummary.lngMF2 = 0;
			modGNBas.Statics.typDogSummary.lngNeuter1 = 0;
			modGNBas.Statics.typDogSummary.lngNeuter2 = 0;
			modGNBas.Statics.typDogSummary.lngNewTag = 0;
			modGNBas.Statics.typDogSummary.lngOtherTag = 0;
			modGNBas.Statics.typDogSummary.lngReplacement1 = 0;
			modGNBas.Statics.typDogSummary.lngReplacement2 = 0;
			modGNBas.Statics.typDogSummary.lngReplacementTag = 0;
			modGNBas.Statics.typDogSummary.lngSearchRescue1 = 0;
			modGNBas.Statics.typDogSummary.lngSearchRescue2 = 0;
			modGNBas.Statics.typDogSummary.lngTotalSticker1 = 0;
			modGNBas.Statics.typDogSummary.lngTotalSticker2 = 0;
			modGNBas.Statics.typDogSummary.lngTotalTag = 0;
			modGNBas.Statics.typDogSummary.lngTransfer1 = 0;
			modGNBas.Statics.typDogSummary.lngTransfer2 = 0;
			SubReport1.Report = new srptDogSummary();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				SubReport1.Report.UserData = rsReport.Get_Fields("tmonth") + "," + FCConvert.ToString(lngYearToUse) + "," + FCConvert.ToString(boolJustSummary);
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtKennel1.Text = modGNBas.Statics.typDogSummary.lngKennelSticker1.ToString();
			txtKennel2.Text = modGNBas.Statics.typDogSummary.lngKennelSticker2.ToString();
			txtHearingGuide1.Text = modGNBas.Statics.typDogSummary.lngHearingGuide1.ToString();
			txtHearingGuide2.Text = modGNBas.Statics.typDogSummary.lngHearingGuide2.ToString();
			txtDogKennel1.Text = modGNBas.Statics.typDogSummary.lngAddedToKennel1.ToString();
			txtDogKennel2.Text = modGNBas.Statics.typDogSummary.lngAddedToKennel2.ToString();
			txtMF1.Text = modGNBas.Statics.typDogSummary.lngMF1.ToString();
			txtMF2.Text = modGNBas.Statics.typDogSummary.lngMF2.ToString();
			txtNeuter1.Text = modGNBas.Statics.typDogSummary.lngNeuter1.ToString();
			txtNeuter2.Text = modGNBas.Statics.typDogSummary.lngNeuter2.ToString();
            txtDangerous1.Text = modGNBas.Statics.typDogSummary.lngDangerous1.ToString();
            txtDangerous2.Text = modGNBas.Statics.typDogSummary.lngDangerous2.ToString();
            txtNuisance1.Text = modGNBas.Statics.typDogSummary.lngNuisance1.ToString();
            txtNuisance2.Text = modGNBas.Statics.typDogSummary.lngNuisance2.ToString();
			txtNewTag.Text = modGNBas.Statics.typDogSummary.lngNewTag.ToString();
			txtReplacement1.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngReplacement1);
			txtReplacement2.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngReplacement2);
			txtReplacementTag.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngReplacementTag);
			txtKennelTotal.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngKennelLicenses);
			txtSandR1.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngSearchRescue1);
			txtSandR2.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngSearchRescue2);
			txtTagOther.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngOtherTag);
			modGNBas.Statics.typDogSummary.lngTotalTag = modGNBas.Statics.typDogSummary.lngOtherTag + modGNBas.Statics.typDogSummary.lngNewTag + modGNBas.Statics.typDogSummary.lngReplacementTag;
			txtTagTotal.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngTotalTag);
			txtTransfers1.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngTransfer1);
			txtTransfers2.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngTransfer2);
			modGNBas.Statics.typDogSummary.lngTotalSticker1 = modGNBas.Statics.typDogSummary.lngMF1 + modGNBas.Statics.typDogSummary.lngNeuter1 + modGNBas.Statics.typDogSummary.lngReplacement1 + modGNBas.Statics.typDogSummary.lngSearchRescue1 + modGNBas.Statics.typDogSummary.lngTransfer1 + modGNBas.Statics.typDogSummary.lngHearingGuide1 + modGNBas.Statics.typDogSummary.lngKennelSticker1;
			modGNBas.Statics.typDogSummary.lngTotalSticker2 = modGNBas.Statics.typDogSummary.lngMF2 + modGNBas.Statics.typDogSummary.lngNeuter2 + modGNBas.Statics.typDogSummary.lngReplacement2 + modGNBas.Statics.typDogSummary.lngSearchRescue2 + modGNBas.Statics.typDogSummary.lngTransfer2 + modGNBas.Statics.typDogSummary.lngHearingGuide2 + modGNBas.Statics.typDogSummary.lngKennelSticker2;
			txtDogTotal1.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngTotalSticker1);
			txtDogTotal2.Text = FCConvert.ToString(modGNBas.Statics.typDogSummary.lngTotalSticker2);
		}

		
	}
}
