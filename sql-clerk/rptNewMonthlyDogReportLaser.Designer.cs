﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptNewMonthlyDogReportLaser.
	/// </summary>
	partial class rptNewMonthlyDogReportLaser
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewMonthlyDogReportLaser));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtStickerStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStickerEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblThru = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAttestedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMF1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMF2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNeuter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNeuter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMFTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNeuterTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKennel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKennel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKennelTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKennelSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNeuterSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMFSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtReportTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdjustments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtReason = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtReplacement1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtReplacement2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSandR1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSandR2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHearingGuide1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHearingGuide2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTransfers1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTransfers2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDogKennel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDogKennel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblStickerYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblStickerYear2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtLastEndingBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStickersIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEndingBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStickerRangeStart1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStickerRangeEnd1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStickerRangeStart2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStickerRangeEnd2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtOnline1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOnline2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOnlineTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtReplacementTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNewTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTagOnline = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTagTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDangerous1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDangerous2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNuisance1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNuisance2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDangerousTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNuisanceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNuisanceSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDangerousSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDangerousFee = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNuisanceFee = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtReportDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblThru)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttestedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMFTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuterTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennelTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennelSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuterSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMFSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdjustments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacement1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacement2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSandR1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSandR2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransfers1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransfers2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogKennel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogKennel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStickerYear1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStickerYear2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastEndingBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickersIssued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndingBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerRangeStart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerRangeEnd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerRangeStart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerRangeEnd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOnline1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOnline2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOnlineTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacementTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagOnline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerous1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerous2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisance1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisance2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerousTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerousSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDangerousFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNuisanceFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMonth,
            this.txtAttestedBy,
            this.txtMuniName,
            this.txtMF1,
            this.txtMF2,
            this.txtNeuter1,
            this.txtNeuter2,
            this.txtMFTotal,
            this.txtNeuterTotal,
            this.txtKennel1,
            this.txtKennel2,
            this.txtKennelTotal,
            this.txtKennelSummary,
            this.txtNeuterSummary,
            this.txtMFSummary,
            this.txtAddress1,
            this.txtPhone,
            this.txtAddress2,
            this.txtReportTotal,
            this.txtAdjustments,
            this.txtReason,
            this.txtReplacement1,
            this.txtReplacement2,
            this.txtSandR1,
            this.txtSandR2,
            this.txtHearingGuide1,
            this.txtHearingGuide2,
            this.txtTransfers1,
            this.txtTransfers2,
            this.txtDogKennel1,
            this.txtDogKennel2,
            this.Label1,
            this.lblStickerYear1,
            this.lblStickerYear2,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Label32,
            this.Label33,
            this.Label34,
            this.Label35,
            this.lblYear1,
            this.lblYear2,
            this.Label38,
            this.Label39,
            this.Label40,
            this.Label41,
            this.Line1,
            this.Label42,
            this.Line2,
            this.Line3,
            this.Line5,
            this.Line6,
            this.Line7,
            this.Label43,
            this.Line8,
            this.Label44,
            this.Label46,
            this.Label47,
            this.Line12,
            this.Label48,
            this.Line13,
            this.Label49,
            this.Line14,
            this.Label50,
            this.Line15,
            this.Line16,
            this.Line18,
            this.Line19,
            this.Label51,
            this.Line20,
            this.Line23,
            this.Label52,
            this.Label53,
            this.Label54,
            this.Label55,
            this.Label56,
            this.Line24,
            this.Label57,
            this.Label58,
            this.Label59,
            this.Label60,
            this.Line25,
            this.Line26,
            this.Line27,
            this.Line28,
            this.Line29,
            this.Line30,
            this.Line31,
            this.Line32,
            this.Line33,
            this.Line34,
            this.Line35,
            this.Line36,
            this.Line37,
            this.Line38,
            this.Line39,
            this.Line40,
            this.Line41,
            this.Line42,
            this.Line43,
            this.Line44,
            this.Line45,
            this.Line46,
            this.Line47,
            this.Line48,
            this.Line49,
            this.Line50,
            this.Line51,
            this.Line53,
            this.Line55,
            this.Label61,
            this.Label62,
            this.Label63,
            this.Label64,
            this.Shape1,
            this.Label65,
            this.Line56,
            this.Line57,
            this.Line58,
            this.Line59,
            this.Line60,
            this.Line62,
            this.txtLastEndingBalance,
            this.txtStickersIssued,
            this.txtEndingBalance,
            this.txtStickerRangeStart1,
            this.txtStickerRangeEnd1,
            this.txtStickerRangeStart2,
            this.txtStickerRangeEnd2,
            this.Label67,
            this.Label68,
            this.txtOnline1,
            this.txtOnline2,
            this.txtOnlineTotal,
            this.Label70,
            this.Label71,
            this.Line64,
            this.Line65,
            this.Line66,
            this.Label72,
            this.Line67,
            this.txtReplacementTag,
            this.txtNewTag,
            this.Field1,
            this.txtTagOnline,
            this.txtTagTotal,
            this.txtDangerous1,
            this.txtDangerous2,
            this.txtNuisance1,
            this.txtNuisance2,
            this.txtDangerousTotal,
            this.txtNuisanceTotal,
            this.txtNuisanceSummary,
            this.txtDangerousSummary,
            this.Label73,
            this.Label74,
            this.Label75,
            this.Label76,
            this.Label77,
            this.Label78,
            this.lblDangerousFee,
            this.lblNuisanceFee,
            this.Line68,
            this.Line69,
            this.Line70,
            this.Line71,
            this.Line72,
            this.Line73,
            this.Line74,
            this.Line75,
            this.txtReportDate,
            this.Line4});
            this.Detail.Height = 7.021F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label66,
            this.txtStickerStart,
            this.txtStickerEnd,
            this.lblThru});
            this.ReportFooter.Height = 4.791667F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.ReportFooter.Visible = false;
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // Label66
            // 
            this.Label66.Height = 0.2083333F;
            this.Label66.HyperLink = null;
            this.Label66.Left = 0.4791667F;
            this.Label66.Name = "Label66";
            this.Label66.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label66.Text = "Sticker serial number series issued:";
            this.Label66.Top = 0.1354167F;
            this.Label66.Width = 2.5625F;
            // 
            // txtStickerStart
            // 
            this.txtStickerStart.Height = 0.1666667F;
            this.txtStickerStart.Left = 0.34375F;
            this.txtStickerStart.Name = "txtStickerStart";
            this.txtStickerStart.Text = null;
            this.txtStickerStart.Top = 0.3541667F;
            this.txtStickerStart.Width = 0.8020833F;
            // 
            // txtStickerEnd
            // 
            this.txtStickerEnd.Height = 0.1666667F;
            this.txtStickerEnd.Left = 2.104167F;
            this.txtStickerEnd.Name = "txtStickerEnd";
            this.txtStickerEnd.Text = null;
            this.txtStickerEnd.Top = 0.3541667F;
            this.txtStickerEnd.Width = 0.8020833F;
            // 
            // lblThru
            // 
            this.lblThru.Height = 0.1666667F;
            this.lblThru.Left = 1.229167F;
            this.lblThru.Name = "lblThru";
            this.lblThru.Text = null;
            this.lblThru.Top = 0.34375F;
            this.lblThru.Width = 0.8020833F;
            // 
            // txtMonth
            // 
            this.txtMonth.Height = 0.2083333F;
            this.txtMonth.Left = 3.698F;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Text = null;
            this.txtMonth.Top = 0.587F;
            this.txtMonth.Visible = false;
            this.txtMonth.Width = 0.3125F;
            // 
            // txtAttestedBy
            // 
            this.txtAttestedBy.Height = 0.1666667F;
            this.txtAttestedBy.Left = 5.16675F;
            this.txtAttestedBy.Name = "txtAttestedBy";
            this.txtAttestedBy.Text = null;
            this.txtAttestedBy.Top = 1.045333F;
            this.txtAttestedBy.Width = 2.25F;
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.2083333F;
            this.txtMuniName.Left = 5.16675F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Text = null;
            this.txtMuniName.Top = 0.837F;
            this.txtMuniName.Width = 2.25F;
            // 
            // txtMF1
            // 
            this.txtMF1.Height = 0.1666667F;
            this.txtMF1.Left = 2.72925F;
            this.txtMF1.Name = "txtMF1";
            this.txtMF1.Text = null;
            this.txtMF1.Top = 2.566167F;
            this.txtMF1.Width = 0.4375F;
            // 
            // txtMF2
            // 
            this.txtMF2.Height = 0.1666667F;
            this.txtMF2.Left = 3.22925F;
            this.txtMF2.Name = "txtMF2";
            this.txtMF2.Text = null;
            this.txtMF2.Top = 2.566167F;
            this.txtMF2.Width = 0.4375F;
            // 
            // txtNeuter1
            // 
            this.txtNeuter1.Height = 0.2083333F;
            this.txtNeuter1.Left = 2.72925F;
            this.txtNeuter1.Name = "txtNeuter1";
            this.txtNeuter1.Text = null;
            this.txtNeuter1.Top = 2.781445F;
            this.txtNeuter1.Width = 0.4375F;
            // 
            // txtNeuter2
            // 
            this.txtNeuter2.Height = 0.2083333F;
            this.txtNeuter2.Left = 3.22925F;
            this.txtNeuter2.Name = "txtNeuter2";
            this.txtNeuter2.Text = null;
            this.txtNeuter2.Top = 2.7745F;
            this.txtNeuter2.Width = 0.4375F;
            // 
            // txtMFTotal
            // 
            this.txtMFTotal.Height = 0.1666667F;
            this.txtMFTotal.Left = 4.16675F;
            this.txtMFTotal.Name = "txtMFTotal";
            this.txtMFTotal.Text = null;
            this.txtMFTotal.Top = 2.566167F;
            this.txtMFTotal.Width = 0.75F;
            // 
            // txtNeuterTotal
            // 
            this.txtNeuterTotal.Height = 0.2083333F;
            this.txtNeuterTotal.Left = 4.16675F;
            this.txtNeuterTotal.Name = "txtNeuterTotal";
            this.txtNeuterTotal.Text = null;
            this.txtNeuterTotal.Top = 2.7745F;
            this.txtNeuterTotal.Width = 0.75F;
            // 
            // txtKennel1
            // 
            this.txtKennel1.Height = 0.2083333F;
            this.txtKennel1.Left = 2.72925F;
            this.txtKennel1.Name = "txtKennel1";
            this.txtKennel1.Text = null;
            this.txtKennel1.Top = 3.732833F;
            this.txtKennel1.Width = 0.4375F;
            // 
            // txtKennel2
            // 
            this.txtKennel2.Height = 0.2083333F;
            this.txtKennel2.Left = 3.22925F;
            this.txtKennel2.Name = "txtKennel2";
            this.txtKennel2.Text = null;
            this.txtKennel2.Top = 3.732833F;
            this.txtKennel2.Width = 0.4375F;
            // 
            // txtKennelTotal
            // 
            this.txtKennelTotal.Height = 0.2083333F;
            this.txtKennelTotal.Left = 4.16675F;
            this.txtKennelTotal.Name = "txtKennelTotal";
            this.txtKennelTotal.Text = null;
            this.txtKennelTotal.Top = 3.732833F;
            this.txtKennelTotal.Width = 0.75F;
            // 
            // txtKennelSummary
            // 
            this.txtKennelSummary.Height = 0.2083333F;
            this.txtKennelSummary.Left = 6.72925F;
            this.txtKennelSummary.Name = "txtKennelSummary";
            this.txtKennelSummary.Text = null;
            this.txtKennelSummary.Top = 3.732833F;
            this.txtKennelSummary.Width = 0.65625F;
            // 
            // txtNeuterSummary
            // 
            this.txtNeuterSummary.Height = 0.2083333F;
            this.txtNeuterSummary.Left = 6.72925F;
            this.txtNeuterSummary.Name = "txtNeuterSummary";
            this.txtNeuterSummary.Text = null;
            this.txtNeuterSummary.Top = 2.7745F;
            this.txtNeuterSummary.Width = 0.65625F;
            // 
            // txtMFSummary
            // 
            this.txtMFSummary.Height = 0.1666667F;
            this.txtMFSummary.Left = 6.72925F;
            this.txtMFSummary.Name = "txtMFSummary";
            this.txtMFSummary.Text = null;
            this.txtMFSummary.Top = 2.566167F;
            this.txtMFSummary.Width = 0.65625F;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Height = 0.2083333F;
            this.txtAddress1.Left = 5.16675F;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Text = null;
            this.txtAddress1.Top = 1.212F;
            this.txtAddress1.Width = 2.25F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.2083333F;
            this.txtPhone.Left = 5.16675F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Text = null;
            this.txtPhone.Top = 1.587F;
            this.txtPhone.Width = 2.25F;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Height = 0.1666667F;
            this.txtAddress2.Left = 5.16675F;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Text = null;
            this.txtAddress2.Top = 1.420333F;
            this.txtAddress2.Width = 2.25F;
            // 
            // txtReportTotal
            // 
            this.txtReportTotal.Height = 0.2083333F;
            this.txtReportTotal.Left = 6.448F;
            this.txtReportTotal.Name = "txtReportTotal";
            this.txtReportTotal.Text = null;
            this.txtReportTotal.Top = 4.857834F;
            this.txtReportTotal.Width = 0.9375F;
            // 
            // txtAdjustments
            // 
            this.txtAdjustments.Height = 0.1666667F;
            this.txtAdjustments.Left = 6.10425F;
            this.txtAdjustments.Name = "txtAdjustments";
            this.txtAdjustments.Text = null;
            this.txtAdjustments.Top = 4.378668F;
            this.txtAdjustments.Width = 1.28125F;
            // 
            // txtReason
            // 
            this.txtReason.Height = 0.1875F;
            this.txtReason.Left = 0.9480004F;
            this.txtReason.Name = "txtReason";
            this.txtReason.Text = null;
            this.txtReason.Top = 4.368252F;
            this.txtReason.Width = 3.75F;
            // 
            // txtReplacement1
            // 
            this.txtReplacement1.Height = 0.1666667F;
            this.txtReplacement1.Left = 2.29175F;
            this.txtReplacement1.Name = "txtReplacement1";
            this.txtReplacement1.Text = null;
            this.txtReplacement1.Top = 5.003668F;
            this.txtReplacement1.Width = 0.4375F;
            // 
            // txtReplacement2
            // 
            this.txtReplacement2.Height = 0.1666667F;
            this.txtReplacement2.Left = 2.79175F;
            this.txtReplacement2.Name = "txtReplacement2";
            this.txtReplacement2.Text = null;
            this.txtReplacement2.Top = 5.003668F;
            this.txtReplacement2.Width = 0.4375F;
            // 
            // txtSandR1
            // 
            this.txtSandR1.Height = 0.2083333F;
            this.txtSandR1.Left = 2.29175F;
            this.txtSandR1.Name = "txtSandR1";
            this.txtSandR1.Text = null;
            this.txtSandR1.Top = 5.170334F;
            this.txtSandR1.Width = 0.4375F;
            // 
            // txtSandR2
            // 
            this.txtSandR2.Height = 0.2083333F;
            this.txtSandR2.Left = 2.79175F;
            this.txtSandR2.Name = "txtSandR2";
            this.txtSandR2.Text = null;
            this.txtSandR2.Top = 5.170334F;
            this.txtSandR2.Width = 0.4375F;
            // 
            // txtHearingGuide1
            // 
            this.txtHearingGuide1.Height = 0.1666667F;
            this.txtHearingGuide1.Left = 2.29175F;
            this.txtHearingGuide1.Name = "txtHearingGuide1";
            this.txtHearingGuide1.Text = null;
            this.txtHearingGuide1.Top = 5.378668F;
            this.txtHearingGuide1.Width = 0.4375F;
            // 
            // txtHearingGuide2
            // 
            this.txtHearingGuide2.Height = 0.1666667F;
            this.txtHearingGuide2.Left = 2.79175F;
            this.txtHearingGuide2.Name = "txtHearingGuide2";
            this.txtHearingGuide2.Text = null;
            this.txtHearingGuide2.Top = 5.378668F;
            this.txtHearingGuide2.Width = 0.4375F;
            // 
            // txtTransfers1
            // 
            this.txtTransfers1.Height = 0.2083333F;
            this.txtTransfers1.Left = 2.29175F;
            this.txtTransfers1.Name = "txtTransfers1";
            this.txtTransfers1.Text = null;
            this.txtTransfers1.Top = 5.545334F;
            this.txtTransfers1.Width = 0.4375F;
            // 
            // txtTransfers2
            // 
            this.txtTransfers2.Height = 0.2083333F;
            this.txtTransfers2.Left = 2.79175F;
            this.txtTransfers2.Name = "txtTransfers2";
            this.txtTransfers2.Text = null;
            this.txtTransfers2.Top = 5.545334F;
            this.txtTransfers2.Width = 0.4375F;
            // 
            // txtDogKennel1
            // 
            this.txtDogKennel1.Height = 0.2083333F;
            this.txtDogKennel1.Left = 2.29175F;
            this.txtDogKennel1.Name = "txtDogKennel1";
            this.txtDogKennel1.Text = null;
            this.txtDogKennel1.Top = 6.003668F;
            this.txtDogKennel1.Visible = false;
            this.txtDogKennel1.Width = 0.4375F;
            // 
            // txtDogKennel2
            // 
            this.txtDogKennel2.Height = 0.2083333F;
            this.txtDogKennel2.Left = 2.79175F;
            this.txtDogKennel2.Name = "txtDogKennel2";
            this.txtDogKennel2.Text = null;
            this.txtDogKennel2.Top = 6.003668F;
            this.txtDogKennel2.Visible = false;
            this.txtDogKennel2.Width = 0.4375F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1666667F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2.22925F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Arial\'; font-size: 10pt; font-style: italic; font-weight: bold";
            this.Label1.Text = "Tags";
            this.Label1.Top = 2.316167F;
            this.Label1.Width = 0.5625F;
            // 
            // lblStickerYear1
            // 
            this.lblStickerYear1.Height = 0.2083333F;
            this.lblStickerYear1.HyperLink = null;
            this.lblStickerYear1.Left = 2.72925F;
            this.lblStickerYear1.Name = "lblStickerYear1";
            this.lblStickerYear1.Style = "font-family: \'Courier\'; font-weight: bold";
            this.lblStickerYear1.Text = null;
            this.lblStickerYear1.Top = 2.295333F;
            this.lblStickerYear1.Width = 0.5F;
            // 
            // lblStickerYear2
            // 
            this.lblStickerYear2.Height = 0.2083333F;
            this.lblStickerYear2.HyperLink = null;
            this.lblStickerYear2.Left = 3.2605F;
            this.lblStickerYear2.Name = "lblStickerYear2";
            this.lblStickerYear2.Style = "font-family: \'Courier\'; font-weight: bold";
            this.lblStickerYear2.Text = null;
            this.lblStickerYear2.Top = 2.295333F;
            this.lblStickerYear2.Width = 0.5F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1666667F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 3.04175F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label2.Text = "+";
            this.Label2.Top = 2.566167F;
            this.Label2.Width = 0.125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2083333F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 3.04175F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label3.Text = "+";
            this.Label3.Top = 2.795333F;
            this.Label3.Width = 0.125F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1666667F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 3.04175F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label4.Text = "+";
            this.Label4.Top = 3.732833F;
            this.Label4.Width = 0.1875F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2083333F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 0.4792505F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label13.Text = null;
            this.Label13.Top = 6.003668F;
            this.Label13.Width = 1.75F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.2083333F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.4792505F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label14.Text = "Transfers:";
            this.Label14.Top = 5.587002F;
            this.Label14.Width = 1.75F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1666667F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 0.4792505F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label15.Text = "Replacement Tags:";
            this.Label15.Top = 5.003668F;
            this.Label15.Width = 1.75F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.2083333F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 0.4792505F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label16.Text = "Service/Search/Rescue:";
            this.Label16.Top = 5.170334F;
            this.Label16.Width = 1.75F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1666667F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 0.4792505F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label17.Text = "Hearing/Guide:";
            this.Label17.Top = 5.378668F;
            this.Label17.Width = 1.75F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 0.07300042F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label19.Text = "Reason";
            this.Label19.Top = 4.368252F;
            this.Label19.Width = 0.8125F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1666667F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 2.198F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label20.Text = "Kennel";
            this.Label20.Top = 3.732833F;
            this.Label20.Width = 0.5F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.2083333F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 1.8855F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label21.Text = "Neuter/Spay";
            this.Label21.Top = 2.7745F;
            this.Label21.Width = 0.84375F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.1666667F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 1.91675F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label22.Text = "Male/Female";
            this.Label22.Top = 2.566167F;
            this.Label22.Width = 0.8125F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.1666667F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 3.85425F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label24.Text = "=";
            this.Label24.Top = 2.566167F;
            this.Label24.Width = 0.125F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.2083333F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 3.85425F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label25.Text = "=";
            this.Label25.Top = 2.7745F;
            this.Label25.Width = 0.125F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1875F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 5.073F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.Label26.Text = "   X    $   6.50    =    $";
            this.Label26.Top = 2.55575F;
            this.Label26.Width = 1.5F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.21875F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 5.073F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.Label27.Text = "   X    $   1.00    =    $";
            this.Label27.Top = 2.7745F;
            this.Label27.Width = 1.5F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.1875F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 5.073F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.Label28.Text = "   X    $   30.00    =    $";
            this.Label28.Top = 3.74325F;
            this.Label28.Width = 1.5F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.1875F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 5.0105F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label29.Text = "Adjustments:   $";
            this.Label29.Top = 4.368252F;
            this.Label29.Width = 1F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.2083333F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 5.323F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: \'Arial\'; font-size: 10pt; font-weight: bold";
            this.Label30.Text = "Report Total:   $";
            this.Label30.Top = 4.857834F;
            this.Label30.Width = 1.125F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.2083333F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 3.97925F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label31.Text = "Telephone (207):";
            this.Label31.Top = 1.587F;
            this.Label31.Width = 1.125F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.2083333F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 4.04175F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label32.Text = "Mailing Address:";
            this.Label32.Top = 1.212F;
            this.Label32.Width = 1.0625F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.1666667F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 4.10425F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label33.Text = "Prepared By:";
            this.Label33.Top = 1.045333F;
            this.Label33.Width = 1F;
            // 
            // Label34
            // 
            this.Label34.Height = 0.2083333F;
            this.Label34.HyperLink = null;
            this.Label34.Left = 4.10425F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label34.Text = "Municipality:";
            this.Label34.Top = 0.837F;
            this.Label34.Width = 1F;
            // 
            // Label35
            // 
            this.Label35.Height = 0.2083333F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 2.5105F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
            this.Label35.Text = "For the Month of:";
            this.Label35.Top = 0.587F;
            this.Label35.Width = 1.125F;
            // 
            // lblYear1
            // 
            this.lblYear1.Height = 0.2083333F;
            this.lblYear1.HyperLink = null;
            this.lblYear1.Left = 4.073F;
            this.lblYear1.Name = "lblYear1";
            this.lblYear1.Style = "font-family: \'Courier\'; font-weight: bold";
            this.lblYear1.Text = null;
            this.lblYear1.Top = 0.587F;
            this.lblYear1.Visible = false;
            this.lblYear1.Width = 0.4375F;
            // 
            // lblYear2
            // 
            this.lblYear2.Height = 0.2083333F;
            this.lblYear2.HyperLink = null;
            this.lblYear2.Left = 4.698F;
            this.lblYear2.Name = "lblYear2";
            this.lblYear2.Style = "font-family: \'Courier\'; font-weight: bold";
            this.lblYear2.Text = null;
            this.lblYear2.Top = 0.587F;
            this.lblYear2.Visible = false;
            this.lblYear2.Width = 0.4375F;
            // 
            // Label38
            // 
            this.Label38.Height = 0.2083333F;
            this.Label38.HyperLink = null;
            this.Label38.Left = 0.1667504F;
            this.Label38.Name = "Label38";
            this.Label38.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label38.Text = "Animal Welfare Program";
            this.Label38.Top = 1.232833F;
            this.Label38.Width = 1.8125F;
            // 
            // Label39
            // 
            this.Label39.Height = 0.1666667F;
            this.Label39.HyperLink = null;
            this.Label39.Left = 0.1667504F;
            this.Label39.Name = "Label39";
            this.Label39.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label39.Text = "28 State House Station";
            this.Label39.Top = 1.441167F;
            this.Label39.Width = 1.8125F;
            // 
            // Label40
            // 
            this.Label40.Height = 0.2083333F;
            this.Label40.HyperLink = null;
            this.Label40.Left = 0.1667504F;
            this.Label40.Name = "Label40";
            this.Label40.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label40.Text = "Augusta, Maine 04333-0028";
            this.Label40.Top = 1.607833F;
            this.Label40.Width = 1.8125F;
            // 
            // Label41
            // 
            this.Label41.Height = 0.1666667F;
            this.Label41.HyperLink = null;
            this.Label41.Left = 0.1667504F;
            this.Label41.Name = "Label41";
            this.Label41.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label41.Text = "Forestry";
            this.Label41.Top = 1.045333F;
            this.Label41.Width = 1.8125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.07994488F;
            this.Line1.LineWeight = 3F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 2.045333F;
            this.Line1.Width = 7.274305F;
            this.Line1.X1 = 0.07994488F;
            this.Line1.X2 = 7.35425F;
            this.Line1.Y1 = 2.045333F;
            this.Line1.Y2 = 2.045333F;
            // 
            // Label42
            // 
            this.Label42.Height = 0.1666667F;
            this.Label42.HyperLink = null;
            this.Label42.Left = 4.66675F;
            this.Label42.Name = "Label42";
            this.Label42.Style = "font-family: \'Arial\'; font-size: 10pt; font-style: italic; font-weight: bold";
            this.Label42.Text = "FEES";
            this.Label42.Top = 2.316167F;
            this.Label42.Width = 0.5F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.07994488F;
            this.Line2.LineWeight = 3F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 4.253668F;
            this.Line2.Width = 7.274305F;
            this.Line2.X1 = 0.07994488F;
            this.Line2.X2 = 7.35425F;
            this.Line2.Y1 = 4.253668F;
            this.Line2.Y2 = 4.253668F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.1667504F;
            this.Line3.LineWeight = 3F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 4.691168F;
            this.Line3.Width = 7.1875F;
            this.Line3.X1 = 0.1667504F;
            this.Line3.X2 = 7.354251F;
            this.Line3.Y1 = 4.691168F;
            this.Line3.Y2 = 4.691168F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0.07994488F;
            this.Line5.LineWeight = 3F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 6.732834F;
            this.Line5.Width = 4.649305F;
            this.Line5.X1 = 0.07994488F;
            this.Line5.X2 = 4.72925F;
            this.Line5.Y1 = 6.732834F;
            this.Line5.Y2 = 6.732834F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 4.72925F;
            this.Line6.LineWeight = 3F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 5.482834F;
            this.Line6.Width = 2.625F;
            this.Line6.X1 = 4.72925F;
            this.Line6.X2 = 7.35425F;
            this.Line6.Y1 = 5.482834F;
            this.Line6.Y2 = 5.482834F;
            // 
            // Line7
            // 
            this.Line7.Height = 2.291666F;
            this.Line7.Left = 4.72925F;
            this.Line7.LineWeight = 3F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 4.691168F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 4.72925F;
            this.Line7.X2 = 4.72925F;
            this.Line7.Y1 = 4.691168F;
            this.Line7.Y2 = 6.982834F;
            // 
            // Label43
            // 
            this.Label43.Height = 0.1875F;
            this.Label43.HyperLink = null;
            this.Label43.Left = 5.198F;
            this.Label43.Name = "Label43";
            this.Label43.Style = "font-family: \'Arial\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.Label43.Text = "Check   #:";
            this.Label43.Top = 5.180752F;
            this.Label43.Width = 1.25F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 6.448F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 5.274502F;
            this.Line8.Width = 0.9375F;
            this.Line8.X1 = 6.448F;
            this.Line8.X2 = 7.3855F;
            this.Line8.Y1 = 5.274502F;
            this.Line8.Y2 = 5.274502F;
            // 
            // Label44
            // 
            this.Label44.Height = 0.2083333F;
            this.Label44.HyperLink = null;
            this.Label44.Left = 4.85425F;
            this.Label44.Name = "Label44";
            this.Label44.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.Label44.Text = "License #\'s Issued";
            this.Label44.Top = 5.816168F;
            this.Label44.Width = 2.5625F;
            // 
            // Label46
            // 
            this.Label46.Height = 0.2083333F;
            this.Label46.HyperLink = null;
            this.Label46.Left = 6.10425F;
            this.Label46.Name = "Label46";
            this.Label46.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label46.Text = "thru";
            this.Label46.Top = 6.191168F;
            this.Label46.Visible = false;
            this.Label46.Width = 0.3125F;
            // 
            // Label47
            // 
            this.Label47.Height = 0.1666667F;
            this.Label47.HyperLink = null;
            this.Label47.Left = 4.85425F;
            this.Label47.Name = "Label47";
            this.Label47.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.Label47.Text = "Last month\'s ending balance";
            this.Label47.Top = 6.399501F;
            this.Label47.Width = 1.75F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 6.72925F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 6.524501F;
            this.Line12.Width = 0.625F;
            this.Line12.X1 = 6.72925F;
            this.Line12.X2 = 7.35425F;
            this.Line12.Y1 = 6.524501F;
            this.Line12.Y2 = 6.524501F;
            // 
            // Label48
            // 
            this.Label48.Height = 0.2256944F;
            this.Label48.HyperLink = null;
            this.Label48.Left = 4.85425F;
            this.Label48.Name = "Label48";
            this.Label48.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.Label48.Text = "Minus stickers issued for month";
            this.Label48.Top = 6.566168F;
            this.Label48.Width = 1.96875F;
            // 
            // Line13
            // 
            this.Line13.Height = 0F;
            this.Line13.Left = 6.91675F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 6.691168F;
            this.Line13.Width = 0.4375F;
            this.Line13.X1 = 6.91675F;
            this.Line13.X2 = 7.35425F;
            this.Line13.Y1 = 6.691168F;
            this.Line13.Y2 = 6.691168F;
            // 
            // Label49
            // 
            this.Label49.Height = 0.1666667F;
            this.Label49.HyperLink = null;
            this.Label49.Left = 4.85425F;
            this.Label49.Name = "Label49";
            this.Label49.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.Label49.Text = "Equal this month\'s ending balance";
            this.Label49.Top = 6.774501F;
            this.Label49.Width = 2.125F;
            // 
            // Line14
            // 
            this.Line14.Height = 0F;
            this.Line14.Left = 6.91675F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 6.899501F;
            this.Line14.Width = 0.4375F;
            this.Line14.X1 = 6.91675F;
            this.Line14.X2 = 7.35425F;
            this.Line14.Y1 = 6.899501F;
            this.Line14.Y2 = 6.899501F;
            // 
            // Label50
            // 
            this.Label50.Height = 0.1666667F;
            this.Label50.HyperLink = null;
            this.Label50.Left = 6.10425F;
            this.Label50.Name = "Label50";
            this.Label50.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label50.Text = "thru";
            this.Label50.Top = 6.024501F;
            this.Label50.Width = 0.3125F;
            // 
            // Line15
            // 
            this.Line15.Height = 0F;
            this.Line15.Left = 4.97925F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 6.149501F;
            this.Line15.Width = 0.8125F;
            this.Line15.X1 = 4.97925F;
            this.Line15.X2 = 5.79175F;
            this.Line15.Y1 = 6.149501F;
            this.Line15.Y2 = 6.149501F;
            // 
            // Line16
            // 
            this.Line16.Height = 0F;
            this.Line16.Left = 4.97925F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 6.316168F;
            this.Line16.Width = 0.8125F;
            this.Line16.X1 = 4.97925F;
            this.Line16.X2 = 5.79175F;
            this.Line16.Y1 = 6.316168F;
            this.Line16.Y2 = 6.316168F;
            // 
            // Line18
            // 
            this.Line18.Height = 0F;
            this.Line18.Left = 6.60425F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 6.149501F;
            this.Line18.Width = 0.6875F;
            this.Line18.X1 = 6.60425F;
            this.Line18.X2 = 7.29175F;
            this.Line18.Y1 = 6.149501F;
            this.Line18.Y2 = 6.149501F;
            // 
            // Line19
            // 
            this.Line19.Height = 0F;
            this.Line19.Left = 6.60425F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 6.316168F;
            this.Line19.Width = 0.6875F;
            this.Line19.X1 = 6.60425F;
            this.Line19.X2 = 7.29175F;
            this.Line19.Y1 = 6.316168F;
            this.Line19.Y2 = 6.316168F;
            // 
            // Label51
            // 
            this.Label51.Height = 0.2083333F;
            this.Label51.HyperLink = null;
            this.Label51.Left = 4.5105F;
            this.Label51.Name = "Label51";
            this.Label51.Style = "font-family: \'Courier\'; font-size: 11pt; font-weight: bold";
            this.Label51.Text = "/";
            this.Label51.Top = 0.587F;
            this.Label51.Visible = false;
            this.Label51.Width = 0.125F;
            // 
            // Line20
            // 
            this.Line20.Height = 0F;
            this.Line20.Left = 0.9480004F;
            this.Line20.LineWeight = 1F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 4.524502F;
            this.Line20.Width = 3.75F;
            this.Line20.X1 = 0.9480004F;
            this.Line20.X2 = 4.698F;
            this.Line20.Y1 = 4.524502F;
            this.Line20.Y2 = 4.524502F;
            // 
            // Line23
            // 
            this.Line23.Height = 0F;
            this.Line23.Left = 6.073F;
            this.Line23.LineWeight = 1F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 4.524502F;
            this.Line23.Width = 1.1875F;
            this.Line23.X1 = 6.073F;
            this.Line23.X2 = 7.2605F;
            this.Line23.Y1 = 4.524502F;
            this.Line23.Y2 = 4.524502F;
            // 
            // Label52
            // 
            this.Label52.Height = 0.1666667F;
            this.Label52.HyperLink = null;
            this.Label52.Left = 0.07300042F;
            this.Label52.Name = "Label52";
            this.Label52.Style = "font-family: \'Arial\'; font-size: 10pt; font-style: italic; font-weight: bold";
            this.Label52.Text = "TAGS ISSUED";
            this.Label52.Top = 2.087F;
            this.Label52.Width = 1.15625F;
            // 
            // Label53
            // 
            this.Label53.Height = 0.1666667F;
            this.Label53.HyperLink = null;
            this.Label53.Left = 0.07300042F;
            this.Label53.Name = "Label53";
            this.Label53.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.Label53.Text = "New:";
            this.Label53.Top = 2.316167F;
            this.Label53.Width = 0.5F;
            // 
            // Label54
            // 
            this.Label54.Height = 0.1666667F;
            this.Label54.HyperLink = null;
            this.Label54.Left = 0.07300042F;
            this.Label54.Name = "Label54";
            this.Label54.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.Label54.Text = "Replacement:";
            this.Label54.Top = 2.566167F;
            this.Label54.Width = 0.90625F;
            // 
            // Label55
            // 
            this.Label55.Height = 0.1666667F;
            this.Label55.HyperLink = null;
            this.Label55.Left = 0.07300042F;
            this.Label55.Name = "Label55";
            this.Label55.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.Label55.Text = "Online:";
            this.Label55.Top = 3.659917F;
            this.Label55.Width = 0.6875F;
            // 
            // Label56
            // 
            this.Label56.Height = 0.1666667F;
            this.Label56.HyperLink = null;
            this.Label56.Left = 0.07300042F;
            this.Label56.Name = "Label56";
            this.Label56.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.Label56.Text = "Total:";
            this.Label56.Top = 3.920333F;
            this.Label56.Width = 0.5F;
            // 
            // Line24
            // 
            this.Line24.Height = 2.208335F;
            this.Line24.Left = 1.79175F;
            this.Line24.LineWeight = 3F;
            this.Line24.Name = "Line24";
            this.Line24.Top = 2.045333F;
            this.Line24.Width = 0F;
            this.Line24.X1 = 1.79175F;
            this.Line24.X2 = 1.79175F;
            this.Line24.Y1 = 2.045333F;
            this.Line24.Y2 = 4.253668F;
            // 
            // Label57
            // 
            this.Label57.Height = 0.2083333F;
            this.Label57.HyperLink = null;
            this.Label57.Left = 0.1667504F;
            this.Label57.Name = "Label57";
            this.Label57.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label57.Text = "Maine Department of Agriculture, Conservation and";
            this.Label57.Top = 0.837F;
            this.Label57.Width = 3.3125F;
            // 
            // Label58
            // 
            this.Label58.Height = 0.2083333F;
            this.Label58.HyperLink = null;
            this.Label58.Left = 2.43725F;
            this.Label58.Name = "Label58";
            this.Label58.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.Label58.Text = "MONTHLY DOG LICENSE REPORT";
            this.Label58.Top = 0.1700001F;
            this.Label58.Width = 2.5F;
            // 
            // Label59
            // 
            this.Label59.Height = 0.2083333F;
            this.Label59.HyperLink = null;
            this.Label59.Left = 4.2605F;
            this.Label59.Name = "Label59";
            this.Label59.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label59.Text = "Make Checks Payable to: Treasurer, State of Maine";
            this.Label59.Top = 1.837F;
            this.Label59.Width = 3.09375F;
            // 
            // Label60
            // 
            this.Label60.Height = 0.1666667F;
            this.Label60.HyperLink = null;
            this.Label60.Left = 1.97925F;
            this.Label60.Name = "Label60";
            this.Label60.Style = "font-family: \'Arial\'; font-size: 10pt; font-style: italic; font-weight: bold";
            this.Label60.Text = "DOG LICENSES SOLD";
            this.Label60.Top = 2.087F;
            this.Label60.Width = 1.96875F;
            // 
            // Line25
            // 
            this.Line25.Height = 0F;
            this.Line25.Left = 2.72925F;
            this.Line25.LineWeight = 1F;
            this.Line25.Name = "Line25";
            this.Line25.Top = 2.732833F;
            this.Line25.Width = 0.3125F;
            this.Line25.X1 = 2.72925F;
            this.Line25.X2 = 3.04175F;
            this.Line25.Y1 = 2.732833F;
            this.Line25.Y2 = 2.732833F;
            // 
            // Line26
            // 
            this.Line26.Height = 0F;
            this.Line26.Left = 3.22925F;
            this.Line26.LineWeight = 1F;
            this.Line26.Name = "Line26";
            this.Line26.Top = 2.732833F;
            this.Line26.Width = 0.4375F;
            this.Line26.X1 = 3.22925F;
            this.Line26.X2 = 3.66675F;
            this.Line26.Y1 = 2.732833F;
            this.Line26.Y2 = 2.732833F;
            // 
            // Line27
            // 
            this.Line27.Height = 0F;
            this.Line27.Left = 2.72925F;
            this.Line27.LineWeight = 1F;
            this.Line27.Name = "Line27";
            this.Line27.Top = 2.975889F;
            this.Line27.Width = 0.3125F;
            this.Line27.X1 = 2.72925F;
            this.Line27.X2 = 3.04175F;
            this.Line27.Y1 = 2.975889F;
            this.Line27.Y2 = 2.975889F;
            // 
            // Line28
            // 
            this.Line28.Height = 0F;
            this.Line28.Left = 3.22925F;
            this.Line28.LineWeight = 1F;
            this.Line28.Name = "Line28";
            this.Line28.Top = 2.975889F;
            this.Line28.Width = 0.4375F;
            this.Line28.X1 = 3.22925F;
            this.Line28.X2 = 3.66675F;
            this.Line28.Y1 = 2.975889F;
            this.Line28.Y2 = 2.975889F;
            // 
            // Line29
            // 
            this.Line29.Height = 0F;
            this.Line29.Left = 2.72925F;
            this.Line29.LineWeight = 1F;
            this.Line29.Name = "Line29";
            this.Line29.Top = 3.8995F;
            this.Line29.Width = 0.3125F;
            this.Line29.X1 = 2.72925F;
            this.Line29.X2 = 3.04175F;
            this.Line29.Y1 = 3.8995F;
            this.Line29.Y2 = 3.8995F;
            // 
            // Line30
            // 
            this.Line30.Height = 0F;
            this.Line30.Left = 3.22925F;
            this.Line30.LineWeight = 1F;
            this.Line30.Name = "Line30";
            this.Line30.Top = 3.8995F;
            this.Line30.Width = 0.4375F;
            this.Line30.X1 = 3.22925F;
            this.Line30.X2 = 3.66675F;
            this.Line30.Y1 = 3.8995F;
            this.Line30.Y2 = 3.8995F;
            // 
            // Line31
            // 
            this.Line31.Height = 0F;
            this.Line31.Left = 1.0105F;
            this.Line31.LineWeight = 1F;
            this.Line31.Name = "Line31";
            this.Line31.Top = 2.482833F;
            this.Line31.Width = 0.5624999F;
            this.Line31.X1 = 1.0105F;
            this.Line31.X2 = 1.573F;
            this.Line31.Y1 = 2.482833F;
            this.Line31.Y2 = 2.482833F;
            // 
            // Line32
            // 
            this.Line32.Height = 0F;
            this.Line32.Left = 1.0105F;
            this.Line32.LineWeight = 1F;
            this.Line32.Name = "Line32";
            this.Line32.Top = 2.732833F;
            this.Line32.Width = 0.5624999F;
            this.Line32.X1 = 1.0105F;
            this.Line32.X2 = 1.573F;
            this.Line32.Y1 = 2.732833F;
            this.Line32.Y2 = 2.732833F;
            // 
            // Line33
            // 
            this.Line33.Height = 0F;
            this.Line33.Left = 1.0105F;
            this.Line33.LineWeight = 1F;
            this.Line33.Name = "Line33";
            this.Line33.Top = 3.847417F;
            this.Line33.Width = 0.5624999F;
            this.Line33.X1 = 1.0105F;
            this.Line33.X2 = 1.573F;
            this.Line33.Y1 = 3.847417F;
            this.Line33.Y2 = 3.847417F;
            // 
            // Line34
            // 
            this.Line34.Height = 0F;
            this.Line34.Left = 1.0105F;
            this.Line34.LineWeight = 1F;
            this.Line34.Name = "Line34";
            this.Line34.Top = 4.087002F;
            this.Line34.Width = 0.5624999F;
            this.Line34.X1 = 1.0105F;
            this.Line34.X2 = 1.573F;
            this.Line34.Y1 = 4.087002F;
            this.Line34.Y2 = 4.087002F;
            // 
            // Line35
            // 
            this.Line35.Height = 0F;
            this.Line35.Left = 4.16675F;
            this.Line35.LineWeight = 1F;
            this.Line35.Name = "Line35";
            this.Line35.Top = 2.732833F;
            this.Line35.Width = 0.75F;
            this.Line35.X1 = 4.16675F;
            this.Line35.X2 = 4.91675F;
            this.Line35.Y1 = 2.732833F;
            this.Line35.Y2 = 2.732833F;
            // 
            // Line36
            // 
            this.Line36.Height = 0F;
            this.Line36.Left = 4.16675F;
            this.Line36.LineWeight = 1F;
            this.Line36.Name = "Line36";
            this.Line36.Top = 2.941167F;
            this.Line36.Width = 0.75F;
            this.Line36.X1 = 4.16675F;
            this.Line36.X2 = 4.91675F;
            this.Line36.Y1 = 2.941167F;
            this.Line36.Y2 = 2.941167F;
            // 
            // Line37
            // 
            this.Line37.Height = 0F;
            this.Line37.Left = 4.16675F;
            this.Line37.LineWeight = 1F;
            this.Line37.Name = "Line37";
            this.Line37.Top = 3.8995F;
            this.Line37.Width = 0.75F;
            this.Line37.X1 = 4.16675F;
            this.Line37.X2 = 4.91675F;
            this.Line37.Y1 = 3.8995F;
            this.Line37.Y2 = 3.8995F;
            // 
            // Line38
            // 
            this.Line38.Height = 0F;
            this.Line38.Left = 6.72925F;
            this.Line38.LineWeight = 1F;
            this.Line38.Name = "Line38";
            this.Line38.Top = 2.732833F;
            this.Line38.Width = 0.65625F;
            this.Line38.X1 = 6.72925F;
            this.Line38.X2 = 7.3855F;
            this.Line38.Y1 = 2.732833F;
            this.Line38.Y2 = 2.732833F;
            // 
            // Line39
            // 
            this.Line39.Height = 0F;
            this.Line39.Left = 6.72925F;
            this.Line39.LineWeight = 1F;
            this.Line39.Name = "Line39";
            this.Line39.Top = 2.941167F;
            this.Line39.Width = 0.65625F;
            this.Line39.X1 = 6.72925F;
            this.Line39.X2 = 7.3855F;
            this.Line39.Y1 = 2.941167F;
            this.Line39.Y2 = 2.941167F;
            // 
            // Line40
            // 
            this.Line40.Height = 0F;
            this.Line40.Left = 6.72925F;
            this.Line40.LineWeight = 1F;
            this.Line40.Name = "Line40";
            this.Line40.Top = 3.8995F;
            this.Line40.Width = 0.65625F;
            this.Line40.X1 = 6.72925F;
            this.Line40.X2 = 7.3855F;
            this.Line40.Y1 = 3.8995F;
            this.Line40.Y2 = 3.8995F;
            // 
            // Line41
            // 
            this.Line41.Height = 0F;
            this.Line41.Left = 6.448F;
            this.Line41.LineWeight = 1F;
            this.Line41.Name = "Line41";
            this.Line41.Top = 5.066168F;
            this.Line41.Width = 0.9375F;
            this.Line41.X1 = 6.448F;
            this.Line41.X2 = 7.3855F;
            this.Line41.Y1 = 5.066168F;
            this.Line41.Y2 = 5.066168F;
            // 
            // Line42
            // 
            this.Line42.Height = 0F;
            this.Line42.Left = 2.29175F;
            this.Line42.LineWeight = 1F;
            this.Line42.Name = "Line42";
            this.Line42.Top = 5.170334F;
            this.Line42.Width = 0.3125F;
            this.Line42.X1 = 2.29175F;
            this.Line42.X2 = 2.60425F;
            this.Line42.Y1 = 5.170334F;
            this.Line42.Y2 = 5.170334F;
            // 
            // Line43
            // 
            this.Line43.Height = 0F;
            this.Line43.Left = 2.29175F;
            this.Line43.LineWeight = 1F;
            this.Line43.Name = "Line43";
            this.Line43.Top = 5.337002F;
            this.Line43.Width = 0.3125F;
            this.Line43.X1 = 2.29175F;
            this.Line43.X2 = 2.60425F;
            this.Line43.Y1 = 5.337002F;
            this.Line43.Y2 = 5.337002F;
            // 
            // Line44
            // 
            this.Line44.Height = 0F;
            this.Line44.Left = 2.29175F;
            this.Line44.LineWeight = 1F;
            this.Line44.Name = "Line44";
            this.Line44.Top = 5.545334F;
            this.Line44.Width = 0.3125F;
            this.Line44.X1 = 2.29175F;
            this.Line44.X2 = 2.60425F;
            this.Line44.Y1 = 5.545334F;
            this.Line44.Y2 = 5.545334F;
            // 
            // Line45
            // 
            this.Line45.Height = 0F;
            this.Line45.Left = 2.29175F;
            this.Line45.LineWeight = 1F;
            this.Line45.Name = "Line45";
            this.Line45.Top = 5.753668F;
            this.Line45.Width = 0.3125F;
            this.Line45.X1 = 2.29175F;
            this.Line45.X2 = 2.60425F;
            this.Line45.Y1 = 5.753668F;
            this.Line45.Y2 = 5.753668F;
            // 
            // Line46
            // 
            this.Line46.Height = 0F;
            this.Line46.Left = 2.79175F;
            this.Line46.LineWeight = 1F;
            this.Line46.Name = "Line46";
            this.Line46.Top = 5.170334F;
            this.Line46.Width = 0.4375F;
            this.Line46.X1 = 2.79175F;
            this.Line46.X2 = 3.22925F;
            this.Line46.Y1 = 5.170334F;
            this.Line46.Y2 = 5.170334F;
            // 
            // Line47
            // 
            this.Line47.Height = 0F;
            this.Line47.Left = 2.79175F;
            this.Line47.LineWeight = 1F;
            this.Line47.Name = "Line47";
            this.Line47.Top = 5.337002F;
            this.Line47.Width = 0.4375F;
            this.Line47.X1 = 2.79175F;
            this.Line47.X2 = 3.22925F;
            this.Line47.Y1 = 5.337002F;
            this.Line47.Y2 = 5.337002F;
            // 
            // Line48
            // 
            this.Line48.Height = 0F;
            this.Line48.Left = 2.79175F;
            this.Line48.LineWeight = 1F;
            this.Line48.Name = "Line48";
            this.Line48.Top = 5.545334F;
            this.Line48.Width = 0.4375F;
            this.Line48.X1 = 2.79175F;
            this.Line48.X2 = 3.22925F;
            this.Line48.Y1 = 5.545334F;
            this.Line48.Y2 = 5.545334F;
            // 
            // Line49
            // 
            this.Line49.Height = 0F;
            this.Line49.Left = 2.79175F;
            this.Line49.LineWeight = 1F;
            this.Line49.Name = "Line49";
            this.Line49.Top = 5.753668F;
            this.Line49.Width = 0.4375F;
            this.Line49.X1 = 2.79175F;
            this.Line49.X2 = 3.22925F;
            this.Line49.Y1 = 5.753668F;
            this.Line49.Y2 = 5.753668F;
            // 
            // Line50
            // 
            this.Line50.Height = 0F;
            this.Line50.Left = 2.79175F;
            this.Line50.LineWeight = 1F;
            this.Line50.Name = "Line50";
            this.Line50.Top = 6.212001F;
            this.Line50.Width = 0.4375F;
            this.Line50.X1 = 2.79175F;
            this.Line50.X2 = 3.22925F;
            this.Line50.Y1 = 6.212001F;
            this.Line50.Y2 = 6.212001F;
            // 
            // Line51
            // 
            this.Line51.Height = 0F;
            this.Line51.Left = 2.29175F;
            this.Line51.LineWeight = 1F;
            this.Line51.Name = "Line51";
            this.Line51.Top = 5.962001F;
            this.Line51.Width = 0.3125F;
            this.Line51.X1 = 2.29175F;
            this.Line51.X2 = 2.60425F;
            this.Line51.Y1 = 5.962001F;
            this.Line51.Y2 = 5.962001F;
            // 
            // Line53
            // 
            this.Line53.Height = 0F;
            this.Line53.Left = 2.29175F;
            this.Line53.LineWeight = 1F;
            this.Line53.Name = "Line53";
            this.Line53.Top = 6.212001F;
            this.Line53.Width = 0.3125F;
            this.Line53.X1 = 2.29175F;
            this.Line53.X2 = 2.60425F;
            this.Line53.Y1 = 6.212001F;
            this.Line53.Y2 = 6.212001F;
            // 
            // Line55
            // 
            this.Line55.Height = 0F;
            this.Line55.Left = 2.79175F;
            this.Line55.LineWeight = 1F;
            this.Line55.Name = "Line55";
            this.Line55.Top = 5.962001F;
            this.Line55.Width = 0.4375F;
            this.Line55.X1 = 2.79175F;
            this.Line55.X2 = 3.22925F;
            this.Line55.Y1 = 5.962001F;
            this.Line55.Y2 = 5.962001F;
            // 
            // Label61
            // 
            this.Label61.Height = 0.2083333F;
            this.Label61.HyperLink = null;
            this.Label61.Left = 0.4792505F;
            this.Label61.Name = "Label61";
            this.Label61.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label61.Text = "Adjustments/Voids:";
            this.Label61.Top = 5.795334F;
            this.Label61.Width = 1.75F;
            // 
            // Label62
            // 
            this.Label62.Height = 0.2083333F;
            this.Label62.HyperLink = null;
            this.Label62.Left = 4.85425F;
            this.Label62.Name = "Label62";
            this.Label62.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
            this.Label62.Text = "CLERKS USE ONLY (OPTIONAL)";
            this.Label62.Top = 5.524502F;
            this.Label62.Width = 2.3125F;
            // 
            // Label63
            // 
            this.Label63.Height = 0.1666667F;
            this.Label63.HyperLink = null;
            this.Label63.Left = 0.1355004F;
            this.Label63.Name = "Label63";
            this.Label63.Style = "font-family: \'Arial\'; font-size: 10pt; font-style: italic; font-weight: bold";
            this.Label63.Text = "DOG LICENSES AT NO CHARGE";
            this.Label63.Top = 4.753668F;
            this.Label63.Width = 2.8125F;
            // 
            // Label64
            // 
            this.Label64.Height = 0.25F;
            this.Label64.HyperLink = null;
            this.Label64.Left = 0.04175043F;
            this.Label64.Name = "Label64";
            this.Label64.Style = "font-family: \'Arial\'; font-size: 8.5pt; text-align: left";
            this.Label64.Text = "**WOLF HYBRID**Please attach a copy of any license issued for a wolf hybrid.";
            this.Label64.Top = 6.378668F;
            this.Label64.Width = 4.09375F;
            // 
            // Shape1
            // 
            this.Shape1.Height = 0.125F;
            this.Shape1.Left = 5.3855F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape1.Top = 2.337F;
            this.Shape1.Width = 0.125F;
            // 
            // Label65
            // 
            this.Label65.Height = 0.1666667F;
            this.Label65.HyperLink = null;
            this.Label65.Left = 5.54175F;
            this.Label65.Name = "Label65";
            this.Label65.Style = "font-family: \'Arial\'; font-size: 10pt";
            this.Label65.Text = "No Sales This Month";
            this.Label65.Top = 2.337F;
            this.Label65.Width = 1.375F;
            // 
            // Line56
            // 
            this.Line56.Height = 0.125F;
            this.Line56.Left = 2.698F;
            this.Line56.LineWeight = 2F;
            this.Line56.Name = "Line56";
            this.Line56.Top = 5.212002F;
            this.Line56.Width = 0F;
            this.Line56.X1 = 2.698F;
            this.Line56.X2 = 2.698F;
            this.Line56.Y1 = 5.212002F;
            this.Line56.Y2 = 5.337002F;
            // 
            // Line57
            // 
            this.Line57.Height = 0.125F;
            this.Line57.Left = 2.698F;
            this.Line57.LineWeight = 2F;
            this.Line57.Name = "Line57";
            this.Line57.Top = 5.024502F;
            this.Line57.Width = 0F;
            this.Line57.X1 = 2.698F;
            this.Line57.X2 = 2.698F;
            this.Line57.Y1 = 5.024502F;
            this.Line57.Y2 = 5.149502F;
            // 
            // Line58
            // 
            this.Line58.Height = 0.125F;
            this.Line58.Left = 2.698F;
            this.Line58.LineWeight = 2F;
            this.Line58.Name = "Line58";
            this.Line58.Top = 5.590474F;
            this.Line58.Width = 0F;
            this.Line58.X1 = 2.698F;
            this.Line58.X2 = 2.698F;
            this.Line58.Y1 = 5.590474F;
            this.Line58.Y2 = 5.715474F;
            // 
            // Line59
            // 
            this.Line59.Height = 0.125F;
            this.Line59.Left = 2.698F;
            this.Line59.LineWeight = 2F;
            this.Line59.Name = "Line59";
            this.Line59.Top = 5.399502F;
            this.Line59.Width = 0F;
            this.Line59.X1 = 2.698F;
            this.Line59.X2 = 2.698F;
            this.Line59.Y1 = 5.399502F;
            this.Line59.Y2 = 5.524502F;
            // 
            // Line60
            // 
            this.Line60.Height = 0.125F;
            this.Line60.Left = 2.698F;
            this.Line60.LineWeight = 2F;
            this.Line60.Name = "Line60";
            this.Line60.Top = 6.024501F;
            this.Line60.Width = 0F;
            this.Line60.X1 = 2.698F;
            this.Line60.X2 = 2.698F;
            this.Line60.Y1 = 6.024501F;
            this.Line60.Y2 = 6.149501F;
            // 
            // Line62
            // 
            this.Line62.Height = 0.125F;
            this.Line62.Left = 2.698F;
            this.Line62.LineWeight = 2F;
            this.Line62.Name = "Line62";
            this.Line62.Top = 5.837001F;
            this.Line62.Width = 0F;
            this.Line62.X1 = 2.698F;
            this.Line62.X2 = 2.698F;
            this.Line62.Y1 = 5.837001F;
            this.Line62.Y2 = 5.962001F;
            // 
            // txtLastEndingBalance
            // 
            this.txtLastEndingBalance.Height = 0.1666667F;
            this.txtLastEndingBalance.Left = 6.7605F;
            this.txtLastEndingBalance.Name = "txtLastEndingBalance";
            this.txtLastEndingBalance.Style = "text-align: right";
            this.txtLastEndingBalance.Text = null;
            this.txtLastEndingBalance.Top = 6.399501F;
            this.txtLastEndingBalance.Width = 0.5833333F;
            // 
            // txtStickersIssued
            // 
            this.txtStickersIssued.Height = 0.1666667F;
            this.txtStickersIssued.Left = 6.927167F;
            this.txtStickersIssued.Name = "txtStickersIssued";
            this.txtStickersIssued.Style = "text-align: right";
            this.txtStickersIssued.Text = null;
            this.txtStickersIssued.Top = 6.566168F;
            this.txtStickersIssued.Width = 0.4166667F;
            // 
            // txtEndingBalance
            // 
            this.txtEndingBalance.Height = 0.1666667F;
            this.txtEndingBalance.Left = 6.927167F;
            this.txtEndingBalance.Name = "txtEndingBalance";
            this.txtEndingBalance.Style = "text-align: right";
            this.txtEndingBalance.Text = null;
            this.txtEndingBalance.Top = 6.732834F;
            this.txtEndingBalance.Width = 0.4166667F;
            // 
            // txtStickerRangeStart1
            // 
            this.txtStickerRangeStart1.Height = 0.1666667F;
            this.txtStickerRangeStart1.Left = 5.020917F;
            this.txtStickerRangeStart1.Name = "txtStickerRangeStart1";
            this.txtStickerRangeStart1.Text = null;
            this.txtStickerRangeStart1.Top = 6.003668F;
            this.txtStickerRangeStart1.Width = 0.8020833F;
            // 
            // txtStickerRangeEnd1
            // 
            this.txtStickerRangeEnd1.Height = 0.1666667F;
            this.txtStickerRangeEnd1.Left = 6.614667F;
            this.txtStickerRangeEnd1.Name = "txtStickerRangeEnd1";
            this.txtStickerRangeEnd1.Text = null;
            this.txtStickerRangeEnd1.Top = 6.003668F;
            this.txtStickerRangeEnd1.Width = 0.8020833F;
            // 
            // txtStickerRangeStart2
            // 
            this.txtStickerRangeStart2.Height = 0.1666667F;
            this.txtStickerRangeStart2.Left = 5.020917F;
            this.txtStickerRangeStart2.Name = "txtStickerRangeStart2";
            this.txtStickerRangeStart2.Text = null;
            this.txtStickerRangeStart2.Top = 6.180751F;
            this.txtStickerRangeStart2.Visible = false;
            this.txtStickerRangeStart2.Width = 0.8020833F;
            // 
            // txtStickerRangeEnd2
            // 
            this.txtStickerRangeEnd2.Height = 0.1666667F;
            this.txtStickerRangeEnd2.Left = 6.614667F;
            this.txtStickerRangeEnd2.Name = "txtStickerRangeEnd2";
            this.txtStickerRangeEnd2.Text = null;
            this.txtStickerRangeEnd2.Top = 6.180751F;
            this.txtStickerRangeEnd2.Visible = false;
            this.txtStickerRangeEnd2.Width = 0.8020833F;
            // 
            // Label67
            // 
            this.Label67.Height = 0.1666667F;
            this.Label67.HyperLink = null;
            this.Label67.Left = 0.1667504F;
            this.Label67.Name = "Label67";
            this.Label67.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.Label67.Text = "Toll Free 1-877-269-9200  or  (207)287-3846";
            this.Label67.Top = 1.795333F;
            this.Label67.Width = 2.625F;
            // 
            // Label68
            // 
            this.Label68.Height = 0.2083333F;
            this.Label68.HyperLink = null;
            this.Label68.Left = 3.85425F;
            this.Label68.Name = "Label68";
            this.Label68.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label68.Text = "=";
            this.Label68.Top = 3.732833F;
            this.Label68.Width = 0.125F;
            // 
            // txtOnline1
            // 
            this.txtOnline1.Height = 0.2083333F;
            this.txtOnline1.Left = 2.72925F;
            this.txtOnline1.Name = "txtOnline1";
            this.txtOnline1.Text = null;
            this.txtOnline1.Top = 3.475889F;
            this.txtOnline1.Width = 0.4375F;
            // 
            // txtOnline2
            // 
            this.txtOnline2.Height = 0.2083333F;
            this.txtOnline2.Left = 3.22925F;
            this.txtOnline2.Name = "txtOnline2";
            this.txtOnline2.Text = null;
            this.txtOnline2.Top = 3.475889F;
            this.txtOnline2.Width = 0.4375F;
            // 
            // txtOnlineTotal
            // 
            this.txtOnlineTotal.Height = 0.2083333F;
            this.txtOnlineTotal.Left = 4.16675F;
            this.txtOnlineTotal.Name = "txtOnlineTotal";
            this.txtOnlineTotal.Text = null;
            this.txtOnlineTotal.Top = 3.475889F;
            this.txtOnlineTotal.Width = 0.75F;
            // 
            // Label70
            // 
            this.Label70.Height = 0.2083333F;
            this.Label70.HyperLink = null;
            this.Label70.Left = 3.04175F;
            this.Label70.Name = "Label70";
            this.Label70.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label70.Text = "+";
            this.Label70.Top = 3.475889F;
            this.Label70.Width = 0.125F;
            // 
            // Label71
            // 
            this.Label71.Height = 0.2083333F;
            this.Label71.HyperLink = null;
            this.Label71.Left = 3.85425F;
            this.Label71.Name = "Label71";
            this.Label71.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label71.Text = "=";
            this.Label71.Top = 3.475889F;
            this.Label71.Width = 0.125F;
            // 
            // Line64
            // 
            this.Line64.Height = 0F;
            this.Line64.Left = 2.72925F;
            this.Line64.LineWeight = 1F;
            this.Line64.Name = "Line64";
            this.Line64.Top = 3.656445F;
            this.Line64.Width = 0.3125F;
            this.Line64.X1 = 2.72925F;
            this.Line64.X2 = 3.04175F;
            this.Line64.Y1 = 3.656445F;
            this.Line64.Y2 = 3.656445F;
            // 
            // Line65
            // 
            this.Line65.Height = 0F;
            this.Line65.Left = 3.22925F;
            this.Line65.LineWeight = 1F;
            this.Line65.Name = "Line65";
            this.Line65.Top = 3.656445F;
            this.Line65.Width = 0.4375F;
            this.Line65.X1 = 3.22925F;
            this.Line65.X2 = 3.66675F;
            this.Line65.Y1 = 3.656445F;
            this.Line65.Y2 = 3.656445F;
            // 
            // Line66
            // 
            this.Line66.Height = 0F;
            this.Line66.Left = 4.16675F;
            this.Line66.LineWeight = 1F;
            this.Line66.Name = "Line66";
            this.Line66.Top = 3.656445F;
            this.Line66.Width = 0.75F;
            this.Line66.X1 = 4.16675F;
            this.Line66.X2 = 4.91675F;
            this.Line66.Y1 = 3.656445F;
            this.Line66.Y2 = 3.656445F;
            // 
            // Label72
            // 
            this.Label72.Height = 0.1666667F;
            this.Label72.HyperLink = null;
            this.Label72.Left = 0.07300042F;
            this.Label72.Name = "Label72";
            this.Label72.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
            this.Label72.Text = "Other:";
            this.Label72.Top = 2.816167F;
            this.Label72.Width = 0.53125F;
            // 
            // Line67
            // 
            this.Line67.Height = 0F;
            this.Line67.Left = 1.0105F;
            this.Line67.LineWeight = 1F;
            this.Line67.Name = "Line67";
            this.Line67.Top = 2.982833F;
            this.Line67.Width = 0.5624999F;
            this.Line67.X1 = 1.0105F;
            this.Line67.X2 = 1.573F;
            this.Line67.Y1 = 2.982833F;
            this.Line67.Y2 = 2.982833F;
            // 
            // txtReplacementTag
            // 
            this.txtReplacementTag.Height = 0.2083333F;
            this.txtReplacementTag.Left = 0.9480004F;
            this.txtReplacementTag.Name = "txtReplacementTag";
            this.txtReplacementTag.Style = "font-family: \'Arial\'; font-size: 10pt";
            this.txtReplacementTag.Text = null;
            this.txtReplacementTag.Top = 2.566167F;
            this.txtReplacementTag.Width = 0.6875F;
            // 
            // txtNewTag
            // 
            this.txtNewTag.Height = 0.2083333F;
            this.txtNewTag.Left = 0.9480004F;
            this.txtNewTag.Name = "txtNewTag";
            this.txtNewTag.Style = "font-family: \'Arial\'; font-size: 10pt";
            this.txtNewTag.Text = null;
            this.txtNewTag.Top = 2.316167F;
            this.txtNewTag.Width = 0.6875F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.2083333F;
            this.Field1.Left = 0.9480004F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Arial\'; font-size: 10pt";
            this.Field1.Text = null;
            this.Field1.Top = 2.816167F;
            this.Field1.Width = 0.6875F;
            // 
            // txtTagOnline
            // 
            this.txtTagOnline.Height = 0.2083333F;
            this.txtTagOnline.Left = 0.9480004F;
            this.txtTagOnline.Name = "txtTagOnline";
            this.txtTagOnline.Style = "font-family: \'Arial\'; font-size: 10pt";
            this.txtTagOnline.Text = null;
            this.txtTagOnline.Top = 3.663389F;
            this.txtTagOnline.Width = 0.6875F;
            // 
            // txtTagTotal
            // 
            this.txtTagTotal.Height = 0.2083333F;
            this.txtTagTotal.Left = 0.9480004F;
            this.txtTagTotal.Name = "txtTagTotal";
            this.txtTagTotal.Style = "font-family: \'Arial\'; font-size: 10pt";
            this.txtTagTotal.Text = null;
            this.txtTagTotal.Top = 3.920333F;
            this.txtTagTotal.Width = 0.6875F;
            // 
            // txtDangerous1
            // 
            this.txtDangerous1.Height = 0.1666667F;
            this.txtDangerous1.Left = 2.72925F;
            this.txtDangerous1.Name = "txtDangerous1";
            this.txtDangerous1.Text = null;
            this.txtDangerous1.Top = 3.003667F;
            this.txtDangerous1.Width = 0.4375F;
            // 
            // txtDangerous2
            // 
            this.txtDangerous2.Height = 0.1666667F;
            this.txtDangerous2.Left = 3.22925F;
            this.txtDangerous2.Name = "txtDangerous2";
            this.txtDangerous2.Text = null;
            this.txtDangerous2.Top = 3.003667F;
            this.txtDangerous2.Width = 0.4375F;
            // 
            // txtNuisance1
            // 
            this.txtNuisance1.Height = 0.2083333F;
            this.txtNuisance1.Left = 2.72925F;
            this.txtNuisance1.Name = "txtNuisance1";
            this.txtNuisance1.Text = null;
            this.txtNuisance1.Top = 3.218945F;
            this.txtNuisance1.Width = 0.4375F;
            // 
            // txtNuisance2
            // 
            this.txtNuisance2.Height = 0.2083333F;
            this.txtNuisance2.Left = 3.22925F;
            this.txtNuisance2.Name = "txtNuisance2";
            this.txtNuisance2.Text = null;
            this.txtNuisance2.Top = 3.212F;
            this.txtNuisance2.Width = 0.4375F;
            // 
            // txtDangerousTotal
            // 
            this.txtDangerousTotal.Height = 0.1666667F;
            this.txtDangerousTotal.Left = 4.16675F;
            this.txtDangerousTotal.Name = "txtDangerousTotal";
            this.txtDangerousTotal.Text = null;
            this.txtDangerousTotal.Top = 3.003667F;
            this.txtDangerousTotal.Width = 0.75F;
            // 
            // txtNuisanceTotal
            // 
            this.txtNuisanceTotal.Height = 0.2083333F;
            this.txtNuisanceTotal.Left = 4.16675F;
            this.txtNuisanceTotal.Name = "txtNuisanceTotal";
            this.txtNuisanceTotal.Text = null;
            this.txtNuisanceTotal.Top = 3.212F;
            this.txtNuisanceTotal.Width = 0.75F;
            // 
            // txtNuisanceSummary
            // 
            this.txtNuisanceSummary.Height = 0.2083333F;
            this.txtNuisanceSummary.Left = 6.72925F;
            this.txtNuisanceSummary.Name = "txtNuisanceSummary";
            this.txtNuisanceSummary.Text = null;
            this.txtNuisanceSummary.Top = 3.212F;
            this.txtNuisanceSummary.Width = 0.65625F;
            // 
            // txtDangerousSummary
            // 
            this.txtDangerousSummary.Height = 0.1666667F;
            this.txtDangerousSummary.Left = 6.72925F;
            this.txtDangerousSummary.Name = "txtDangerousSummary";
            this.txtDangerousSummary.Text = null;
            this.txtDangerousSummary.Top = 3.003667F;
            this.txtDangerousSummary.Width = 0.65625F;
            // 
            // Label73
            // 
            this.Label73.Height = 0.1666667F;
            this.Label73.HyperLink = null;
            this.Label73.Left = 3.04175F;
            this.Label73.Name = "Label73";
            this.Label73.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label73.Text = "+";
            this.Label73.Top = 3.003667F;
            this.Label73.Width = 0.125F;
            // 
            // Label74
            // 
            this.Label74.Height = 0.2083333F;
            this.Label74.HyperLink = null;
            this.Label74.Left = 3.04175F;
            this.Label74.Name = "Label74";
            this.Label74.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label74.Text = "+";
            this.Label74.Top = 3.232833F;
            this.Label74.Width = 0.125F;
            // 
            // Label75
            // 
            this.Label75.Height = 0.2083333F;
            this.Label75.HyperLink = null;
            this.Label75.Left = 1.8855F;
            this.Label75.Name = "Label75";
            this.Label75.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label75.Text = "Nuisance Dog";
            this.Label75.Top = 3.212F;
            this.Label75.Width = 0.84375F;
            // 
            // Label76
            // 
            this.Label76.Height = 0.1666667F;
            this.Label76.HyperLink = null;
            this.Label76.Left = 1.91675F;
            this.Label76.Name = "Label76";
            this.Label76.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
            this.Label76.Text = "Dangerous Dog";
            this.Label76.Top = 3.003667F;
            this.Label76.Width = 0.8125F;
            // 
            // Label77
            // 
            this.Label77.Height = 0.1666667F;
            this.Label77.HyperLink = null;
            this.Label77.Left = 3.85425F;
            this.Label77.Name = "Label77";
            this.Label77.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label77.Text = "=";
            this.Label77.Top = 3.003667F;
            this.Label77.Width = 0.125F;
            // 
            // Label78
            // 
            this.Label78.Height = 0.2083333F;
            this.Label78.HyperLink = null;
            this.Label78.Left = 3.85425F;
            this.Label78.Name = "Label78";
            this.Label78.Style = "font-family: \'Courier\'; font-weight: bold";
            this.Label78.Text = "=";
            this.Label78.Top = 3.212F;
            this.Label78.Width = 0.125F;
            // 
            // lblDangerousFee
            // 
            this.lblDangerousFee.Height = 0.1875F;
            this.lblDangerousFee.HyperLink = null;
            this.lblDangerousFee.Left = 5.073F;
            this.lblDangerousFee.Name = "lblDangerousFee";
            this.lblDangerousFee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.lblDangerousFee.Text = "   X    $   1.00    =    $";
            this.lblDangerousFee.Top = 2.99325F;
            this.lblDangerousFee.Width = 1.5F;
            // 
            // lblNuisanceFee
            // 
            this.lblNuisanceFee.Height = 0.21875F;
            this.lblNuisanceFee.HyperLink = null;
            this.lblNuisanceFee.Left = 5.073F;
            this.lblNuisanceFee.Name = "lblNuisanceFee";
            this.lblNuisanceFee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.lblNuisanceFee.Text = "   X    $   1.00    =    $";
            this.lblNuisanceFee.Top = 3.212F;
            this.lblNuisanceFee.Width = 1.5F;
            // 
            // Line68
            // 
            this.Line68.Height = 0F;
            this.Line68.Left = 2.72925F;
            this.Line68.LineWeight = 1F;
            this.Line68.Name = "Line68";
            this.Line68.Top = 3.170333F;
            this.Line68.Width = 0.3125F;
            this.Line68.X1 = 2.72925F;
            this.Line68.X2 = 3.04175F;
            this.Line68.Y1 = 3.170333F;
            this.Line68.Y2 = 3.170333F;
            // 
            // Line69
            // 
            this.Line69.Height = 0F;
            this.Line69.Left = 3.22925F;
            this.Line69.LineWeight = 1F;
            this.Line69.Name = "Line69";
            this.Line69.Top = 3.170333F;
            this.Line69.Width = 0.4375F;
            this.Line69.X1 = 3.22925F;
            this.Line69.X2 = 3.66675F;
            this.Line69.Y1 = 3.170333F;
            this.Line69.Y2 = 3.170333F;
            // 
            // Line70
            // 
            this.Line70.Height = 0F;
            this.Line70.Left = 2.72925F;
            this.Line70.LineWeight = 1F;
            this.Line70.Name = "Line70";
            this.Line70.Top = 3.413389F;
            this.Line70.Width = 0.3125F;
            this.Line70.X1 = 2.72925F;
            this.Line70.X2 = 3.04175F;
            this.Line70.Y1 = 3.413389F;
            this.Line70.Y2 = 3.413389F;
            // 
            // Line71
            // 
            this.Line71.Height = 0F;
            this.Line71.Left = 3.22925F;
            this.Line71.LineWeight = 1F;
            this.Line71.Name = "Line71";
            this.Line71.Top = 3.413389F;
            this.Line71.Width = 0.4375F;
            this.Line71.X1 = 3.22925F;
            this.Line71.X2 = 3.66675F;
            this.Line71.Y1 = 3.413389F;
            this.Line71.Y2 = 3.413389F;
            // 
            // Line72
            // 
            this.Line72.Height = 0F;
            this.Line72.Left = 4.16675F;
            this.Line72.LineWeight = 1F;
            this.Line72.Name = "Line72";
            this.Line72.Top = 3.170333F;
            this.Line72.Width = 0.75F;
            this.Line72.X1 = 4.16675F;
            this.Line72.X2 = 4.91675F;
            this.Line72.Y1 = 3.170333F;
            this.Line72.Y2 = 3.170333F;
            // 
            // Line73
            // 
            this.Line73.Height = 0F;
            this.Line73.Left = 4.16675F;
            this.Line73.LineWeight = 1F;
            this.Line73.Name = "Line73";
            this.Line73.Top = 3.378667F;
            this.Line73.Width = 0.75F;
            this.Line73.X1 = 4.16675F;
            this.Line73.X2 = 4.91675F;
            this.Line73.Y1 = 3.378667F;
            this.Line73.Y2 = 3.378667F;
            // 
            // Line74
            // 
            this.Line74.Height = 0F;
            this.Line74.Left = 6.72925F;
            this.Line74.LineWeight = 1F;
            this.Line74.Name = "Line74";
            this.Line74.Top = 3.170333F;
            this.Line74.Width = 0.65625F;
            this.Line74.X1 = 6.72925F;
            this.Line74.X2 = 7.3855F;
            this.Line74.Y1 = 3.170333F;
            this.Line74.Y2 = 3.170333F;
            // 
            // Line75
            // 
            this.Line75.Height = 0F;
            this.Line75.Left = 6.72925F;
            this.Line75.LineWeight = 1F;
            this.Line75.Name = "Line75";
            this.Line75.Top = 3.378667F;
            this.Line75.Width = 0.65625F;
            this.Line75.X1 = 6.72925F;
            this.Line75.X2 = 7.3855F;
            this.Line75.Y1 = 3.378667F;
            this.Line75.Y2 = 3.378667F;
            // 
            // txtReportDate
            // 
            this.txtReportDate.Height = 0.2083333F;
            this.txtReportDate.Left = 3.708417F;
            this.txtReportDate.Name = "txtReportDate";
            this.txtReportDate.Text = null;
            this.txtReportDate.Top = 0.587F;
            this.txtReportDate.Width = 3.385417F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0.1110004F;
            this.Line4.LineWeight = 3F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 6.991F;
            this.Line4.Width = 7.274305F;
            this.Line4.X1 = 0.1110004F;
            this.Line4.X2 = 7.385305F;
            this.Line4.Y1 = 6.991F;
            this.Line4.Y2 = 6.991F;
            // 
            // rptNewMonthlyDogReportLaser
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.375F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.458F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblThru)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttestedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMFTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuterTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennelTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennelSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuterSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMFSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdjustments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacement1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacement2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSandR1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSandR2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransfers1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransfers2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogKennel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogKennel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStickerYear1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStickerYear2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastEndingBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickersIssued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndingBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerRangeStart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerRangeEnd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerRangeStart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerRangeEnd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOnline1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOnline2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOnlineTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacementTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagOnline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerous1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerous2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisance1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisance2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerousTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerousSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDangerousFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNuisanceFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickerStart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickerEnd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblThru;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttestedBy;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMF1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMF2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMFTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuterTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennel1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennel2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennelTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennelSummary;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuterSummary;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMFSummary;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReportTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAdjustments;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReason;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacement1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacement2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSandR1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSandR2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearingGuide1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearingGuide2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransfers1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransfers2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogKennel1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogKennel2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblStickerYear1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblStickerYear2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLastEndingBalance;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickersIssued;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEndingBalance;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickerRangeStart1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickerRangeEnd1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickerRangeStart2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickerRangeEnd2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOnline1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOnline2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOnlineTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line64;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line65;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line66;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line67;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacementTag;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewTag;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagOnline;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDangerous1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDangerous2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNuisance1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNuisance2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDangerousTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNuisanceTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNuisanceSummary;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDangerousSummary;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDangerousFee;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNuisanceFee;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line68;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line69;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line70;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line71;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line72;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line73;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line74;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line75;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReportDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
    }
}
