//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public class clsCustomize
	{
		//=========================================================
		public bool boolFileDuplicateWarning;
		public string City = "";
		public string Zip = "";
		public string LegalResidence = "";
		public string AgentNumber = "";
		public string Address1 = "";
		public string Address2 = "";
		public string Phone = "";
		public string County = "";
		public string State = "";
		public string BirthDefaultCounty = "";
		public string BirthDefaultTown = "";
		public bool boolDontPrintAttested;
		public string MotherDefaultCounty = "";
		public string MotherDefaultState = "";
		public string MotherDefaultCity = "";
		private bool boolDefaultAttestedBy;
		// vbPorter upgrade warning: intOnlineDogImportOption As int	OnWrite(int, OnlineDogImportOptionType)
		private int intOnlineDogImportOption;

		public enum OnlineDogImportOptionType
		{
			OnlineDogImportOptionNoTags = 0,
			OnlineDogImportOptionExisting = 1,
			OnlineDogImportOptionAddMissing = 2,
		}

		public int OnlineDogImportOption
		{
			set
			{
				intOnlineDogImportOption = value;
			}
			get
			{
				int OnlineDogImportOption = 0;
				OnlineDogImportOption = intOnlineDogImportOption;
				return OnlineDogImportOption;
			}
		}

		public bool DefaultAttestedBy
		{
			set
			{
				boolDefaultAttestedBy = value;
			}
			get
			{
				bool DefaultAttestedBy = false;
				DefaultAttestedBy = boolDefaultAttestedBy;
				return DefaultAttestedBy;
			}
		}

		public void LoadCustomizeInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from clerkdefaults", "twck0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				Address1 = FCConvert.ToString(clsLoad.Get_Fields_String("address1"));
				Address2 = FCConvert.ToString(clsLoad.Get_Fields_String("address2"));
				AgentNumber = FCConvert.ToString(clsLoad.Get_Fields_String("agentnumber"));
				boolFileDuplicateWarning = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("boolFileDuplicateWarning"));
				City = FCConvert.ToString(clsLoad.Get_Fields_String("city"));
				County = FCConvert.ToString(clsLoad.Get_Fields("County"));
				LegalResidence = FCConvert.ToString(clsLoad.Get_Fields_String("legalresidence"));
				Phone = FCConvert.ToString(clsLoad.Get_Fields_String("Phone"));
				State = FCConvert.ToString(clsLoad.Get_Fields_String("statename"));
				Zip = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
				BirthDefaultCounty = FCConvert.ToString(clsLoad.Get_Fields_String("BirthDefaultCounty"));
				BirthDefaultTown = FCConvert.ToString(clsLoad.Get_Fields_String("BirthDefaultTown"));
				boolDontPrintAttested = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("DontPrintAttested"));
				MotherDefaultCounty = FCConvert.ToString(clsLoad.Get_Fields_String("MotherDefaultCounty"));
				MotherDefaultState = FCConvert.ToString(clsLoad.Get_Fields_String("MotherDefaultState"));
				MotherDefaultCity = FCConvert.ToString(clsLoad.Get_Fields_String("MotherDefaultCity"));
				boolDefaultAttestedBy = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("DefaultAttestedBy"));
				intOnlineDogImportOption = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("OnlineDogImportOption"))));
			}
		}

		public bool SaveCustomizeInfo()
		{
			bool SaveCustomizeInfo = false;
			try
			{
				clsDRWrapper clsSave = new clsDRWrapper();
				SaveCustomizeInfo = false;
				clsSave.OpenRecordset("select * from clerkdefaults", "twck0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				clsSave.Set_Fields("address1", Address1);
				clsSave.Set_Fields("address2", Address2);
				clsSave.Set_Fields("agentnumber", AgentNumber);
				clsSave.Set_Fields("boolfileDuplicateWarning", boolFileDuplicateWarning);
				clsSave.Set_Fields("city", City);
				clsSave.Set_Fields("county", County);
				clsSave.Set_Fields("legalresidence", LegalResidence);
				clsSave.Set_Fields("Phone", Phone);
				clsSave.Set_Fields("StateName", State);
				clsSave.Set_Fields("zip", Zip);
				clsSave.Set_Fields("DontPrintAttested", boolDontPrintAttested);
				clsSave.Set_Fields("BirthDefaultCounty", BirthDefaultCounty);
				clsSave.Set_Fields("BirthDefaultTown", BirthDefaultTown);
				clsSave.Set_Fields("MotherDefaultCounty", MotherDefaultCounty);
				clsSave.Set_Fields("MotherDefaultState", MotherDefaultState);
				clsSave.Set_Fields("MotherDefaultCity", MotherDefaultCity);
				clsSave.Set_Fields("DefaultAttestedBy", boolDefaultAttestedBy);
				clsSave.Set_Fields("OnlineDogImportOption", intOnlineDogImportOption);
				clsSave.Update();
				SaveCustomizeInfo = true;
				return SaveCustomizeInfo;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveCustomizeInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return SaveCustomizeInfo;
			}
		}

		public clsCustomize() : base()
		{
			intOnlineDogImportOption = FCConvert.ToInt32(OnlineDogImportOptionType.OnlineDogImportOptionNoTags);
		}
	}
}
