//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.ObjectModel;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using SharedApplication;
using SharedApplication.CentralDocuments;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Births;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCK0000
{
	public partial class frmBirths : BaseForm, IView<IBirthViewModel>
    {
        private bool cancelling = true;
		public frmBirths()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmBirths(IBirthViewModel viewModel) : this()
        {
            ViewModel = viewModel;
            intInitChanges = 0;
        }
		private void InitializeComponentEx()
		{
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
                _InstancePtr = this;
            lblLabels = new System.Collections.Generic.List<FCLabel>();
			lblLabels.AddControlArrayElement(lblLabels_0, 0);
			lblLabels.AddControlArrayElement(lblLabels_1, 1);
			lblLabels.AddControlArrayElement(lblLabels_2, 2);
			lblLabels.AddControlArrayElement(lblLabels_3, 3);
			lblLabels.AddControlArrayElement(lblLabels_4, 4);
			lblLabels.AddControlArrayElement(lblLabels_5, 5);
			lblLabels.AddControlArrayElement(lblLabels_6, 6);
			lblLabels.AddControlArrayElement(lblLabels_7, 7);
			lblLabels.AddControlArrayElement(lblLabels_8, 8);
			lblLabels.AddControlArrayElement(lblLabels_9, 9);
			lblLabels.AddControlArrayElement(lblLabels_10, 10);
			lblLabels.AddControlArrayElement(lblLabels_11, 11);
			lblLabels.AddControlArrayElement(lblLabels_12, 12);
			lblLabels.AddControlArrayElement(lblLabels_13, 13);
			lblLabels.AddControlArrayElement(lblLabels_14, 14);
			lblLabels.AddControlArrayElement(lblLabels_15, 15);
			lblLabels.AddControlArrayElement(lblLabels_16, 16);
			lblLabels.AddControlArrayElement(lblLabels_17, 17);
			lblLabels.AddControlArrayElement(lblLabels_18, 18);
			lblLabels.AddControlArrayElement(lblLabels_19, 19);
			lblLabels.AddControlArrayElement(lblLabels_20, 20);
			lblLabels.AddControlArrayElement(lblLabels_21, 21);
			lblLabels.AddControlArrayElement(lblLabels_22, 22);
			lblLabels.AddControlArrayElement(lblLabels_23, 23);
			lblLabels.AddControlArrayElement(lblLabels_24, 24);
			lblLabels.AddControlArrayElement(lblLabels_25, 25);
			lblLabels.AddControlArrayElement(lblLabels_26, 26);
			lblLabels.AddControlArrayElement(lblLabels_27, 27);
			lblLabels.AddControlArrayElement(lblLabels_28, 28);
			lblLabels.AddControlArrayElement(lblLabels_29, 29);
			lblLabels.AddControlArrayElement(lblLabels_30, 30);
			lblLabels.AddControlArrayElement(lblLabels_31, 31);
			lblLabels.AddControlArrayElement(lblLabels_32, 32);
			lblLabels.AddControlArrayElement(lblLabels_33, 33);
			lblLabels.AddControlArrayElement(lblLabels_34, 34);
			lblLabels.AddControlArrayElement(lblLabels_35, 35);
			lblLabels.AddControlArrayElement(lblLabels_36, 36);
			lblLabels.AddControlArrayElement(lblLabels_37, 37);
			lblLabels.AddControlArrayElement(lblLabels_38, 38);
			lblLabels.AddControlArrayElement(lblLabels_39, 39);
			lblLabels.AddControlArrayElement(lblLabels_40, 40);
			lblLabels.AddControlArrayElement(lblLabels_41, 41);
			lblLabels.AddControlArrayElement(lblLabels_42, 42);
			lblLabels.AddControlArrayElement(lblLabels_43, 43);
			lblLabels.AddControlArrayElement(lblLabels_44, 44);
			lblLabels.AddControlArrayElement(lblLabels_45, 45);
			lblLabels.AddControlArrayElement(lblLabels_47, 46);
			lblLabels.AddControlArrayElement(lblLabels_48, 47);
			lbltown = new System.Collections.Generic.List<FCLabel>();
			lbltown.AddControlArrayElement(lbltown_42, 0);
			lblBirthCertNum = new System.Collections.Generic.List<FCLabel>();
			lblBirthCertNum.AddControlArrayElement(lblBirthCertNum_28, 0);
            this.Closing += FrmBirths_Closing;
		}

        private void FrmBirths_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
            else
            {
               //ViewModel.CompleteTransaction();
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmBirths InstancePtr
        {
            get
            {
                return (frmBirths)Sys.GetInstance(typeof(frmBirths));
            }
        }

        protected frmBirths _InstancePtr = null;
        public clsDRWrapper rs_AutoInitialized = null;
        public clsDRWrapper rs
        {
            get
            {
                if (rs_AutoInitialized == null)
                {
                    rs_AutoInitialized = new clsDRWrapper();
                }
                return rs_AutoInitialized;
            }
            set
            {
                rs_AutoInitialized = value;
            }
        }
        public string strSQL = "";
		public bool Popped;
		public bool boolFormLoading;
		public bool Adding;
		public bool FormActivated;
		public bool GoodValidation;
		public bool boolSaveSuccessfully;
		public object BirthCertNum;
		private int intDataChanged;
		private int intFrameValue;
		private bool boolLoadRun;
		private int intInitChanges;
		int lngBirthID;

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGNBas.Statics.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblregionS where townnumber > 0 order by townnumber", "CentralData");
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("townnumber") + ";" + clsLoad.Get_Fields_String("townname") + "\t" + clsLoad.Get_Fields("townnumber") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				strTemp = "0";
				gridTownCode.Visible = false;
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = VSFlexGrid1.WidthOriginal;
			VSFlexGrid1.ColWidth(1, FCConvert.ToInt32(0.26 * GridWidth));
			VSFlexGrid1.ColWidth(2, FCConvert.ToInt32(0.08 * GridWidth));
			VSFlexGrid1.ColWidth(3, FCConvert.ToInt32(0.21 * GridWidth));
			VSFlexGrid1.ColWidth(4, FCConvert.ToInt32(0.15 * GridWidth));
			VSFlexGrid1.ColWidth(5, FCConvert.ToInt32(0.15 * GridWidth));
		}

		private void cboMothersResidence_Click()
		{
			intDataChanged += 1;
		}

		private void chkDeceased_CheckedChanged(object sender, System.EventArgs e)
		{
			lblDeceased.Visible = 0 != (chkDeceased.CheckState);
			mebDateDeceased.Visible = 0 != chkDeceased.CheckState;
		}

		private void cmdFind_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			modGNBas.Statics.boolGetClerk = true;
			frmListRegistrar.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrRegistrar) == string.Empty)
				return;
			rsTemp.OpenRecordset("Select * from DefaultRegistrarNames where ID = " + modGNBas.Statics.gstrRegistrar, modGNBas.DEFAULTDATABASE);
			if (!rsTemp.EndOfFile())
			{
				txtClerkFirstName.Text = FCConvert.ToString(rsTemp.Get_Fields_String("FirstName"));
				txtClerkLastName.Text = FCConvert.ToString(rsTemp.Get_Fields_String("LastName"));
				txtClerkMI.Text = FCConvert.ToString(rsTemp.Get_Fields_String("MI"));
				txtClerkCityTown.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("ClerkTown")));
			}
			txtClerkFirstName.Focus();
			modGNBas.Statics.boolGetClerk = false;
		}

		public void LoadData()
        {
            lngBirthID = ViewModel.VitalId;
			// LOAD THE DATA FROM THE DATABASE
			if (modGNBas.Statics.boolSetDefaultTowns)
			{
				modGNBas.Statics.boolSetDefaultTowns = false;
				return;
			}
			else
            {
                if (modGNBas.Statics.boolGetClerk)
                {
                    modGNBas.Statics.boolGetClerk = false;
                    return;
                }
            }

            if (FormActivated == false)
			{
                strSQL = "SELECT * FROM Births where ID = " + FCConvert.ToString(lngBirthID) + " order by LastName,FirstName ";
                rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
                FormActivated = true;
			}
			if (lngBirthID != 0)
			{
				modAmendments.LoadAmendments(ref VSFlexGrid1, lngBirthID, "BIRTHS");
				Adding = false;
				modGNBas.Statics.Response = "";
			}
			if (lngBirthID == 0)
			{
				modClerkGeneral.Statics.AddingBirthCertificate = false;
				Adding = true;
				// clear the controls
				ClearBoxes();
				modAmendments.LoadAmendments(ref VSFlexGrid1, 0, "BIRTHS");
				// Dave 11/2/2006
				txtTown.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.ClerkDefaults.BirthDefaultTown);
				txtCounty.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.ClerkDefaults.BirthDefaultCounty);
			}
			else
			{
				// fill the boxes with the correct data
				FillBoxes();
			}
			MenuCheck();
			modDirtyForm.ClearDirtyControls(this);
			intDataChanged = intInitChanges;
			intInitChanges = 0;
		}

		private void cmdFind_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 2;
		}

		private void cmdFindClerk_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: strName As object	OnWrite(string())
			object strName;
			clsDRWrapper rsFacility = new clsDRWrapper();
			frmListFacility.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrFacility) == string.Empty)
				return;
			strName = Strings.Split(modGNBas.Statics.gstrFacility, ",", -1, CompareConstants.vbBinaryCompare);
			txtFacilityName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strName)[1])) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])strName)[0]));
			// & vbCrLf
			txtFacilityName.Text = fecherFoundation.Strings.Trim(txtFacilityName.Text);

		}

		private void cmdFindMunicipalClerk_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			modGNBas.Statics.boolGetClerk = true;
			frmListRegistrar.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrRegistrar) == string.Empty)
				return;
			rsTemp.OpenRecordset("Select * from DefaultRegistrarNames where ID = " + modGNBas.Statics.gstrRegistrar, modGNBas.DEFAULTDATABASE);
			if (!rsTemp.EndOfFile())
			{
				txtStateRegistrar.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("FirstName"))) + " ";
				txtStateRegistrar.Text = txtStateRegistrar.Text + (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("MI"))) == string.Empty ? " " : rsTemp.Get_Fields_String("MI") + ". ");
				txtStateRegistrar.Text = txtStateRegistrar.Text + fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("LastName"))) + " ";
				txtTownOf.Text = FCConvert.ToString(rsTemp.Get_Fields_String("ClerkTown"));
				modRegistry.SaveRegistryKey("CKLastClerkName", txtStateRegistrar.Text);
				modRegistry.SaveRegistryKey("CKLastClerkTown", txtTownOf.Text);
			}
			txtStateRegistrar.Focus();
			modGNBas.Statics.boolGetClerk = false;
		}

		private void cmdRegistrar_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			modGNBas.Statics.boolGetClerk = true;
			frmListRegistrar.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrRegistrar) == string.Empty)
				return;
			rsTemp.OpenRecordset("Select * from DefaultRegistrarNames where ID = " + modGNBas.Statics.gstrRegistrar, modGNBas.DEFAULTDATABASE);
			if (!rsTemp.EndOfFile())
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("MI"))) != "")
				{
					txtRegistrarName.Text = rsTemp.Get_Fields_String("FirstName") + " " + rsTemp.Get_Fields_String("MI") + " " + rsTemp.Get_Fields_String("LastName");
				}
				else
				{
					txtRegistrarName.Text = rsTemp.Get_Fields_String("FirstName") + " " + rsTemp.Get_Fields_String("LastName");
				}
				txtRegTown.Text = FCConvert.ToString(rsTemp.Get_Fields_String("ClerkTown"));
			}
			// txtRegistrarFirstName.SetFocus
			mebRegistrarDateFiling.Focus();
			modGNBas.Statics.boolGetClerk = false;
		}

		private void cmdRegistrar_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 1;
		}

		public void frmBirths_Activated(object sender, System.EventArgs e)
		{
			//App.DoEvents();
			if (!boolLoadRun)
				Form_Load();
		}

		private void ClearBoxes()
		{
			/* object obj; */
			string strTemp;
			// Dim rsDefaults As New clsDRWrapper
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
			}
			lngBirthID = 0;
			chkIllegitBirth.CheckState = Wisej.Web.CheckState.Unchecked;
			chkDeceased.CheckState = Wisej.Web.CheckState.Unchecked;
			chkCourtDetermination.CheckState = Wisej.Web.CheckState.Unchecked;
			chkMarriageOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkRequired.CheckState = Wisej.Web.CheckState.Unchecked;
			chkSoleParent.CheckState = Wisej.Web.CheckState.Unchecked;
			txtAttestDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			mebDateDeceased.Text = string.Empty;
			strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("CKLastClerkName"));
			if (fecherFoundation.Strings.Trim(strTemp) == string.Empty)
			{
				txtStateRegistrar.Text = "Municipal Clerk";
			}
			else
			{
				txtStateRegistrar.Text = strTemp;
			}
			strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("CKLastClerkTown"));
			if (fecherFoundation.Strings.Trim(strTemp) == string.Empty)
			{
				txtTownOf.Text = "";
			}
			else
			{
				txtTownOf.Text = strTemp;
			}
			// rsDefaults.OpenRecordset "SELECT * FROM ClerkDefaults", "TWCK0000.vb1"
			if (txtTownOf.Text == "")
			{
				// txtTownOf.Text = StrConv(ClerkDefaults.City, vbProperCase)
				txtTownOf.Text = modGNBas.Statics.ClerkDefaults.City;
			}
			// txtMothersTown.Text = StrConv(ClerkDefaults.MotherDefaultCity, vbProperCase)
			txtMothersTown.Text = modGNBas.Statics.ClerkDefaults.MotherDefaultCity;
			// txtMotherCounty.Text = StrConv(ClerkDefaults.MotherDefaultCounty, vbProperCase)
			txtMotherCounty.Text = modGNBas.Statics.ClerkDefaults.MotherDefaultCounty;
			// txtMotherState.Text = StrConv(ClerkDefaults.MotherDefaultState, vbProperCase)
			txtMotherState.Text = modGNBas.Statics.ClerkDefaults.MotherDefaultState;
			txtChildBirthPlace.Text = "Hospital";
			lblRecord.Text = "";
			lblBKey.Text = "";
			txtChildMI.Text = "";
			txtChildDOB.Text = "";
			txtActualDate.Text = "";
			txtRegTown.Text = "";
			txtClerkDateOfFiling.Text = "";
			txtMotherDOB.Text = string.Empty;
			txtFathersDOB.Text = string.Empty;
			mebRegistrarDateFiling.Text = string.Empty;
			VSFlexGrid1.Clear();
			chkAOP.CheckState = Wisej.Web.CheckState.Unchecked;
		}

		private void FillBoxes()
		{
			if (rs.EndOfFile())
				return;
			lngBirthID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FileNumber")))
				txtFileNumber.Text = FCConvert.ToString(rs.Get_Fields_String("FileNumber"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("LastName")))
				txtChildLastName.Text = FCConvert.ToString(rs.Get_Fields_String("LastName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FirstName")))
				txtChildFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FirstName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MiddleName")))
				txtChildMI.Text = FCConvert.ToString(rs.Get_Fields_String("MiddleName"));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Designation")) == false)
				txtChildDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("Designation"));

			txtChildDOB.Text = FCConvert.ToString(rs.Get_Fields("dateofbirth"));
			if (Information.IsDate(rs.Get_Fields("ActualDate")) && !fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("ActualDate")))
			{
				txtActualDate.Text = Strings.Format(rs.Get_Fields_DateTime("ActualDate"), "MM/dd/yyyy");
			}
			else
			{
				txtActualDate.Text = "";
			}
			txtMothersBirthPlace.Text = FCConvert.ToString(rs.Get_Fields_String("MothersBirthPlace"));
			txtFathersBirthPlace.Text = FCConvert.ToString(rs.Get_Fields_String("FathersBirthPlace"));
			mebAmendedDate.Text = Strings.Format(rs.Get_Fields("DateAmended"), "MM/dd/yyyy");
			mebDateDeceased.Text = Strings.Format(rs.Get_Fields("DeceasedDate"), "MM/dd/yyyy");
			txtAmended.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Amended")));
			txtCourt.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("court")));
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) > 0)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("towncode"))));
			}
			if (fecherFoundation.Strings.UCase(Strings.Mid(rs.Get_Fields_String("sex") + " ", 1, 1)) == "M")
			{
				txtChildSex.Text = "M";
			}
			else
			{
				if (fecherFoundation.Strings.UCase(Strings.Mid(rs.Get_Fields_String("sex") + " ", 1, 1)) == "F")
				{
					txtChildSex.Text = "F";
				}
				else
				{
					txtChildSex.Text = "";
				}
			}
			if (FCConvert.ToString(rs.Get_Fields_String("BirthPlace")) != "N/A")
			{
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BirthPlace")))
					txtChildBirthPlace.Text = FCConvert.ToString(rs.Get_Fields_String("BirthPlace"));
			}
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("AttendantsLastName")))
				txtAttendantLastName.Text = FCConvert.ToString(rs.Get_Fields_String("AttendantsLastName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("AttendantsFirstName")))
				txtAttendantFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("AttendantsFirstName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("AttendantsMiddleInitial")))
				txtAttendantMI.Text = FCConvert.ToString(rs.Get_Fields_String("AttendantsMiddleInitial"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("AttendantTitle")))
				txtAttendantTitle.Text = FCConvert.ToString(rs.Get_Fields_String("AttendantTitle"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("AttendantAddress")))
				txtAttendantAddress.Text = FCConvert.ToString(rs.Get_Fields_String("AttendantAddress")) ;
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersLastName")))
				txtMotherLastName.Text = FCConvert.ToString(rs.Get_Fields_String("MothersLastName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersFirstName")))
				txtMotherFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("MothersFirstName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersMiddleName")))
				txtMotherMI.Text = rs.Get_Fields_String("MothersMiddleName") + "";
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersMaidenName")))
				txtMotherMaidenName.Text = rs.Get_Fields_String("MothersMaidenName") + "";
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersDesignation")) == false)
				txtFatherDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("FathersDesignation"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("RecordingClerksLastName")))
				txtClerkLastName.Text = FCConvert.ToString(rs.Get_Fields_String("RecordingClerksLastName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("RecordingClerksFirstName")))
				txtClerkFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("RecordingClerksFirstName"));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("RecordingClerksMiddleInitial")) == false)
				txtClerkMI.Text = FCConvert.ToString(rs.Get_Fields_String("RecordingClerksMiddleInitial"));
			// If Not IsNull(.Fields("ChildTown")) Then
			txtTown.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("childtown")));
			// End If
			txtRegTown.Text = FCConvert.ToString(rs.Get_Fields_String("regtown"));
			if (txtRegTown.Text == string.Empty)
			{
				intInitChanges = 1;
				txtRegTown.Text = txtTown.Text;
			}

			txtTime.Text = FCConvert.ToString(rs.Get_Fields_String("ChildTime"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ChildCounty")) && FCConvert.ToString(rs.Get_Fields_String("ChildCounty")) != string.Empty)
				txtCounty.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildCounty") + " ");
			txtFacilityName.Text = FCConvert.ToString(rs.Get_Fields_String("FacilityName"));

			txtMotherDOB.Text = FCConvert.ToString(rs.Get_Fields_String("MothersDOB"));
			// End If
			// End If
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MotherState")))
				txtMotherState.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MotherState") + " ");
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MotherCounty")))
				txtMotherCounty.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MotherCounty") + " ");
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MotherTown")))
				txtMothersTown.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MotherTown") + " ");
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersAddress")))
				txtMothersAddress.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MothersAddress") + " ");
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersZipCode")))
				txtMothersZipCode.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MothersZipCode") + " ");
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersYears")))
				txtYears.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MothersYears") + " ");

			txtFathersDOB.Text = FCConvert.ToString(rs.Get_Fields_String("fathersdob"));

			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CityOrTown")))
				txtClerkCityTown.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CityOrTown")));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("RegistrarName")))
				txtRegistrarName.Text = FCConvert.ToString(rs.Get_Fields_String("RegistrarName"));
			// If Not IsNull(.Fields("RegistrarDateFiled")) Then mebRegistrarDateFiling.Text = Format(.Fields("RegistrarDateFiled"), "MM/dd/yyyy")
			if (Information.IsDate(rs.Get_Fields("registrardatefiled")))
			{
				mebRegistrarDateFiling.Text = Strings.Format(rs.Get_Fields_DateTime("registrardatefiled"), "MM/dd/yyyy");
			}
			else
			{
				mebRegistrarDateFiling.Text = FCConvert.ToString(rs.Get_Fields_String("registrardatefileddescription"));
			}
			if (!fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("DateOfFiling")))
				txtClerkDateOfFiling.Text = Strings.Format(rs.Get_Fields("DateOfFiling"), "MM/dd/yyyy");
			if (!fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("AttestDate")))
				txtAttestDate.Text = Strings.Format(rs.Get_Fields("AttestDate"), "MM/dd/yyyy");
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("StateRegistrarMunicipalClerk")))
				txtStateRegistrar.Text = FCConvert.ToString(rs.Get_Fields_String("StateRegistrarMunicipalClerk"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("TownOf")))
				txtTownOf.Text = FCConvert.ToString(rs.Get_Fields_String("TownOf"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("ID")))
				lblBKey.Text = FCConvert.ToString(rs.Get_Fields_Int32("ID"));
			if (rs.Get_Fields_Boolean("Legitimate") == true)
			{
				chkIllegitBirth.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			else
			{
				chkIllegitBirth.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("aop")))
			{
				chkAOP.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkAOP.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("courtdeterminedpaternity")))
			{
				chkCourtDetermination.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkCourtDetermination.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersLastName")))
				txtFatherLastName.Text = FCConvert.ToString(rs.Get_Fields_String("FathersLastName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersFirstName")))
				txtFatherFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FathersFirstName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersMiddleInitial")))
				txtFatherMI.Text = FCConvert.ToString(rs.Get_Fields_String("FathersMiddleInitial"));
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersDesignation")) == false)
				txtFatherDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("FathersDesignation"));
			chkRequired.CheckState = (CheckState)((rs.Get_Fields_Boolean("NoRequiredFields")) ? 1 : 0);
			chkDeceased.CheckState = (CheckState)((rs.Get_Fields_Boolean("DeathRecordInOffice")) ? 1 : 0);
			chkMarriageOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("MarriageOnFile")) ? 1 : 0);
			chkSoleParent.CheckState = (CheckState)((rs.Get_Fields_Boolean("SoleParent")) ? 1 : 0);
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("commentS"))) != string.Empty)
			{
				ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
			}
			else
			{
				ImgComment.Image = null;
			}
		}

		private void frmBirths_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				mnuProcessQuit_Click();
		}

		private void frmBirths_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (FCGlobal.Screen.ActiveControl.Name != txtAttendantAddress.Name)
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this)) > 0)
			{
				if (MessageBox.Show("Current changes have not been saved. Do so now?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveData())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmBirths_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridTownCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FormActivated = false;
			//modGNBas.Statics.boolBirthOpen = false;
		}

		private void imgDocuments_Click(object sender, System.EventArgs e)
		{
			if (lngBirthID <= 0)
				return;
			if (imgDocuments.Visible == true)
			{
				ViewDocs();
			}
		}

		private void ViewDocs()
		{
			if (lngBirthID <= 0)
				return;

   //         frmViewDocuments.InstancePtr.Unload();
			//frmViewDocuments.InstancePtr.Init(0,"Births",  modGNBas.CNSTTYPEBIRTHS.ToString(), lngBirthID, "", "twck0000.vb1", FCForm.FormShowEnum.Modal);
			//imgDocuments.Visible = frmViewDocuments.InstancePtr.docList?.Count>0;
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer("Births", "Clerk", "Birth",
                lngBirthID, "", true, false));
            var documentHeaders = StaticSettings.GlobalCommandDispatcher.Send(new GetHeadersByReferenceCommand()).Result;
            imgDocuments.Visible = documentHeaders.Count > 0;
        }

		private void mebRegistrarDateFiling_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
            if (fecherFoundation.Strings.Trim(mebRegistrarDateFiling.Text).Length != 8 || !Information.IsNumeric(fecherFoundation.Strings.Trim(mebRegistrarDateFiling.Text))) return;

            var strD = fecherFoundation.Strings.Trim(mebRegistrarDateFiling.Text);
            strD = $"{Strings.Mid(strD, 1, 2)}/{Strings.Mid(strD, 3, 2)}/{Strings.Mid(strD, 5)}";
            if (Information.IsDate(strD))
            {
                mebRegistrarDateFiling.Text = strD;
            }
        }

		private void mnuComment_Click(object sender, System.EventArgs e)
        {
            if (lngBirthID == 0)
			{
				MessageBox.Show("This record doesn't exist yet.  You must save before this record can be tied to a comment.", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

            ImgComment.Image = frmCommentOld.InstancePtr.Init("Clerk", modGNBas.CNSTTYPEBIRTHS.ToString(), "Comments", "ID", lngBirthID, boolModal: true) 
                ? null 
                : MDIParent.InstancePtr.ImageList2.Images[0];
        }

		private void mnuGotoFirstRecord_Click()
		{
			rs.MoveFirst();
			FillBoxes();
		}

		private void mnuGotoLastRecord_Click()
		{
			rs.MoveLast();
			FillBoxes();
		}

		private void mnuGotoNextRecord_Click()
		{
			rs.MoveNext();
			if (rs.EndOfFile() == true)
				rs.MoveFirst();
			ClearBoxes();
			FillBoxes();
		}

		private void mnuGotoPreviousRecord_Click()
		{
			rs.MovePrevious();
			if (rs.BeginningOfFile() == true)
				rs.MoveLast();
			ClearBoxes();
			FillBoxes();
		}

		private void mnuGotoSpecificRecord_Click()
		{
			string hold;
			redotag:
			;
			hold = "";
			hold = Interaction.InputBox("Enter Child's Last Name.", "Update Record", null);
			if (hold == "")
				return;
			rs.MoveLast();
			rs.MoveFirst();
			rs.FindFirstRecord("LastName", "'" + hold + "'");
			if (rs.NoMatch)
			{
				MessageBox.Show("There is no match for the Name entered.");
				goto redotag;
			}
			FillBoxes();
		}

		//public void mnuPrintBirthCertificate_Click()
		//{
		//	bool boolOld;
		//	modClerkGeneral.Statics.utPrintInfo.PreView = false;
		//	if (chkIllegitBirth.CheckState == CheckState.Checked)
		//	{
		//		MessageBox.Show("ONLY PERSONS LISTED ON CERTIFICATE CAN RECEIVE A COPY OF BIRTH RECORDS.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//	}
		//	boolOld = false;
		//	if (Conversion.Val(txtChildDOB.Text) != 0)
		//	{
		//		if (FCConvert.ToDateTime(txtChildDOB.Text).Year < 1892)
		//		{
		//			boolOld = true;
		//		}
		//	}
		//	modCashReceiptingData.Statics.typeCRData.Type = "BIR";
		//	modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(txtChildLastName.Text + " " + txtChildFirstName.Text, 30);
		//	modCashReceiptingData.Statics.typeCRData.Reference = txtFileNumber.Text;
		//	modCashReceiptingData.Statics.typeCRData.Control1 = txtChildDOB.Text;
		//	modCashReceiptingData.Statics.typeCRData.Control2 = txtChildBirthPlace.Text;
		//	if (boolOld || (chkRequired.CheckState == Wisej.Web.CheckState.Checked && Conversion.Val(txtChildDOB.Text) == 0))
		//	{
		//		// If CompareDates(StripDateSlashes(txtChildDOB.Text), "01011892", "<") Then
		//		modGNBas.Statics.typOldVitalsReport.AttestBy = modClerkGeneral.Statics.ClerkName;
		//		modGNBas.Statics.typOldVitalsReport.FileNumber = txtFileNumber.Text;
		//		modGNBas.Statics.typOldVitalsReport.pageNumber = string.Empty;
		//		modGNBas.Statics.typOldVitalsReport.FamilyName = string.Empty;
		//		modGNBas.Statics.typOldVitalsReport.ChildName1 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtChildFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtChildMI.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtChildLastName.Text));
		//		modGNBas.Statics.typOldVitalsReport.ChildName2 = string.Empty;
		//		modGNBas.Statics.typOldVitalsReport.ChildName3 = string.Empty;
		//		modGNBas.Statics.typOldVitalsReport.DateOfEvent = txtAttestDate.Text;
		//		modGNBas.Statics.typOldVitalsReport.Parent1 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtMotherFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtMotherMI.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtMotherLastName.Text));
		//		modGNBas.Statics.typOldVitalsReport.Parent2 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtFatherFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtFatherMI.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtFatherLastName.Text));
		//		modGNBas.Statics.typOldVitalsReport.Parent3 = string.Empty;
		//		modGNBas.Statics.typOldVitalsReport.Parent4 = string.Empty;
		//		modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = txtChildBirthPlace.Text;
		//		modGNBas.Statics.typOldVitalsReport.PlaceOfResidence = string.Empty;
		//		modGNBas.Statics.typOldVitalsReport.NameOfClerk1 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtClerkFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtClerkMI.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtClerkLastName.Text));
		//		modGNBas.Statics.typOldVitalsReport.NameOfClerk2 = modGNBas.Statics.typOldVitalsReport.NameOfClerk1;
		//		modGNBas.Statics.typOldVitalsReport.TownOfClerk1 = modGlobalConstants.Statics.MuniName;
		//		modGNBas.Statics.typOldVitalsReport.TownOfClerk2 = modGlobalConstants.Statics.MuniName;
		//		modGNBas.Statics.typOldVitalsReport.DateTo = FCConvert.ToString(modDateRoutines.LongDate(DateTime.Today));
		//		modGNBas.Statics.typOldVitalsReport.DateFrom = string.Empty;
		//		modGNBas.Statics.typOldVitalsReport.Notes = "Record is dated prior to January 1, 1892. This record must be printed on Town letterhead. Please load letterhead into printer now.";
		//		frmPrintOldVitals.InstancePtr.Show(App.MainForm);
		//	}
		//	else
		//	{
		//		intDataChanged += FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this));
		//		if (intDataChanged > 0)
		//		{
		//			if (MessageBox.Show("Save current birth information and proceed to Certificate?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
		//			{
		//				ValidateEntry();
		//				if (GoodValidation)
		//				{
		//					SaveData();
		//					if (boolSaveSuccessfully)
		//					{
		//						strSQL = "Select * from Births where ID = " + lblBKey.Text;
		//						modGNBas.Statics.rsBirthCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
		//						modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(lblBKey.Text);
		//						modClerkGeneral.Statics.utPrintInfo.TblName = "Births";
		//						Hide();
		//						// frmPrintVitalRec.Show 
		//						frmPrintVitalRec.InstancePtr.Init(false);
		//					}
		//				}
		//				else
		//				{
		//				}
		//			}
		//		}
		//		else
		//		{
		//			strSQL = "Select * from Births where ID = " + lblBKey.Text;
		//			modGNBas.Statics.rsBirthCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
		//			modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(lblBKey.Text);
		//			modClerkGeneral.Statics.utPrintInfo.TblName = "Births";
		//			frmBirths.InstancePtr.Hide();
		//			// frmPrintVitalRec.Show 
		//			frmPrintVitalRec.InstancePtr.Init(false);
		//		}
		//	}
		//	if (modCashReceiptingData.Statics.bolFromWindowsCR)
		//	{
		//		modCashReceiptingData.Statics.typeCRData.Type = "BIR";
		//		modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(frmBirths.InstancePtr.txtChildLastName.Text + " " + frmBirths.InstancePtr.txtChildFirstName.Text, 30);
		//		modCashReceiptingData.Statics.typeCRData.Reference = frmBirths.InstancePtr.txtFileNumber.Text;
		//		modCashReceiptingData.Statics.typeCRData.Control1 = frmBirths.InstancePtr.txtChildDOB.Text;
		//		modCashReceiptingData.Statics.typeCRData.Control2 = frmBirths.InstancePtr.txtChildBirthPlace.Text;
		//		modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(modDogs.Statics.typClerkFees.NewBirth, "000000.00"));
		//		modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(modDogs.Statics.typClerkFees.BirthStateFee, "000000.00"));
		//		modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
		//		modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
		//		modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
		//		modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
		//		modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
		//		modCashReceiptingData.Statics.typeCRData.PartyID = 0;
		//		modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
               
  //          }

  //          UpdateTransaction();
  //      }

        private void UpdateTransaction()
        {
            cancelling = false;
            var transaction = new VitalTransaction();
            transaction.TypeCode = "BIR";
            transaction.Reference = txtFileNumber.Text;
            transaction.Control1 = txtChildDOB.Text;
            transaction.Control2 = txtChildBirthPlace.Text;
            transaction.VitalAmount = modDogs.Statics.typClerkFees.NewBirth;
            transaction.Name = (txtChildLastName.Text + " " + txtChildFirstName.Text).Trim();
            transaction.TownCode = gridTownCode.TextMatrix(0, 0).ToString().ToIntegerValue();
            ViewModel.UpdateTransaction(transaction);
        }
		public void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			bool boolOld;
			if (lngBirthID < 1)
			{
				MessageBox.Show("The record has not been created yet" + "\r\n" + "You must save the record before you can print", "No Record", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			modClerkGeneral.Statics.utPrintInfo.PreView = true;
			if (chkIllegitBirth.CheckState == CheckState.Checked)
			{
				MessageBox.Show("ONLY PERSONS LISTED ON CERTIFICATE CAN RECEIVE A COPY OF BIRTH RECORDS.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			modCashReceiptingData.Statics.typeCRData.Type = "BIR";
			modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(txtChildLastName.Text + " " + txtChildFirstName.Text, 30);
			modCashReceiptingData.Statics.typeCRData.Reference = txtFileNumber.Text;
			modCashReceiptingData.Statics.typeCRData.Control1 = txtChildDOB.Text;
			modCashReceiptingData.Statics.typeCRData.Control2 = txtChildBirthPlace.Text;
			boolOld = false;
			if (Information.IsDate(txtChildDOB.Text))
			{
				if (FCConvert.ToDateTime(txtChildDOB.Text).Year < 1892)
				{
					boolOld = true;
				}
			}
			else if (chkRequired.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (MessageBox.Show("Is this record from before 1892?", "Prior to 1892?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					boolOld = true;
				}
			}

            
			if (boolOld)
			{
				// If CompareDates(StripDateSlashes(txtChildDOB.Text), "01011892", "<") Then
				modGNBas.Statics.typOldVitalsReport.AttestBy = modClerkGeneral.Statics.ClerkName;
				modGNBas.Statics.typOldVitalsReport.FileNumber = txtFileNumber.Text;
				modGNBas.Statics.typOldVitalsReport.pageNumber = string.Empty;
				modGNBas.Statics.typOldVitalsReport.FamilyName = string.Empty;
				modGNBas.Statics.typOldVitalsReport.ChildName1 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtChildFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtChildMI.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtChildLastName.Text));
				modGNBas.Statics.typOldVitalsReport.ChildName2 = string.Empty;
				modGNBas.Statics.typOldVitalsReport.ChildName3 = string.Empty;
				// .DateOfEvent = txtAttestDate.Text
				modGNBas.Statics.typOldVitalsReport.DateOfEvent = txtChildDOB.Text;
				modGNBas.Statics.typOldVitalsReport.Parent1 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtMotherFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtMotherMI.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtMotherLastName.Text));
				modGNBas.Statics.typOldVitalsReport.Parent2 = FCConvert.ToString(modGlobalRoutines.CheckForNA(txtFatherFirstName.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtFatherMI.Text)) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(txtFatherLastName.Text));
				modGNBas.Statics.typOldVitalsReport.Parent3 = string.Empty;
				modGNBas.Statics.typOldVitalsReport.Parent4 = string.Empty;
				modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = txtChildBirthPlace.Text;
				modGNBas.Statics.typOldVitalsReport.PlaceOfResidence = string.Empty;
				// .NameOfClerk1 = CheckForNA(txtClerkFirstName) & " " & CheckForNA(txtClerkMI) & " " & CheckForNA(txtClerkLastName)
				modGNBas.Statics.typOldVitalsReport.NameOfClerk1 = fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNA(txtRegistrarName.Text)));
				modGNBas.Statics.typOldVitalsReport.NameOfClerk2 = modGNBas.Statics.typOldVitalsReport.AttestBy;
				// .TownOfClerk1 = MuniName
				modGNBas.Statics.typOldVitalsReport.TownOfClerk1 = txtRegTown.Text;
				modGNBas.Statics.typOldVitalsReport.TownOfClerk2 = modGlobalConstants.Statics.MuniName;
				modGNBas.Statics.typOldVitalsReport.DateTo = FCConvert.ToString(modDateRoutines.LongDate(DateTime.Today));
				modGNBas.Statics.typOldVitalsReport.DateFrom = string.Empty;
				modGNBas.Statics.typOldVitalsReport.Notes = "Record is dated prior to January 1, 1892. This record must be printed on Town letterhead. Please load letterhead into printer now.";
				frmPrintOldVitals.InstancePtr.Show(App.MainForm);
			}
			else
			{
				intDataChanged += FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this));
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Save current birth information and proceed to Certificate?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						ValidateEntry();
						if (GoodValidation)
						{
							SaveData();
							if (boolSaveSuccessfully)
							{
								strSQL = "Select * from Births where ID = " + lblBKey.Text;
								modGNBas.Statics.rsBirthCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
								modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(lblBKey.Text);
								modClerkGeneral.Statics.utPrintInfo.TblName = "Births";
                                UpdateTransaction();
                                ViewModel.PrintVital();
                                CloseWithoutCancel();
                            }
						}
						else
						{
						}
					}
				}
				else
				{
					strSQL = "Select * from Births where ID = " + lblBKey.Text;
					modGNBas.Statics.rsBirthCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
					modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(lblBKey.Text);
					modClerkGeneral.Statics.utPrintInfo.TblName = "Births";
                    UpdateTransaction();
                    ViewModel.PrintVital();
                    CloseWithoutCancel();
                }
			}

           // UpdateTransaction();
		}

		private void mnuProcessAddNewRecord_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				Adding = true;
				lngBirthID = 0;
				ClearBoxes();
				MenuCheck();
				SSTab1.SelectedIndex = 0;
				txtFileNumber.Focus();
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " has been encountered." + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				return;
			}
		}

		private void mnuProcessDeleteRecord_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
			DialogResult answer;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				answer = MessageBox.Show("Are you sure you want to permanently delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					if (lngBirthID > 0)
					{
						rs.Execute("Delete from Births where ID = " + FCConvert.ToString(lngBirthID), modGNBas.DEFAULTCLERKDATABASE);
						MessageBox.Show("Record deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						Close();
					}
					else
					{
						MessageBox.Show("There is no active record or record has not been saved. Delete cannot be completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " has been encountered." + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				return;
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{            
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		public void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			object Ans;
			SaveData();
			if (GoodValidation == false)
				return;
			if (Adding == true)
			{

				Adding = false;
				MenuCheck();

			}
		}

		private void chkIllegitBirth_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkIllegitBirth.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkIllegitBirth.ForeColor = Color.Red;

			}
			else
			{
				chkIllegitBirth.ForeColor = System.Drawing.Color.FromName("@windowText");
                modGNBas.Statics.boolIllegitbirth = false;
				// fraFather.Enabled = True
			}
			intDataChanged += 1;
		}

		private bool SaveData(bool boolValidate = true)
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsClerkInfo = new clsDRWrapper();
				bool blnUpdateFile = false;
				SaveData = false;
				GoodValidation = false;
				modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				if (boolValidate)
				{
					ValidateEntry();
					if (GoodValidation == false)
					{
						// MsgBox "Unable to Update.", vbOKOnly, "Error in Save"
						return SaveData;
					}
				}
				boolSaveSuccessfully = false;
				if (modGNBas.Statics.ClerkDefaults.boolFileDuplicateWarning && fecherFoundation.Strings.Trim(txtFileNumber.Text) != string.Empty)
				{
					clsDRWrapper clsLoad = new clsDRWrapper();
					clsLoad.OpenRecordset("select * from births where ID <> " + FCConvert.ToString(lngBirthID) + " and FILENUMBER = '" + txtFileNumber.Text + "'", "twck0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						if (MessageBox.Show("There is already another birth record with this file number" + "\r\n" + "Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
						{
							GoodValidation = false;
							return SaveData;
						}
					}
				}
				// If comment has   ***  in front of the field name then that
				// field is required and the validation routine will not allow the
				// user to get this far if that field does not have data in it.
				// user to get
				rs.OpenRecordset("Select * from Births where ID = " + FCConvert.ToString(lngBirthID), modGNBas.DEFAULTCLERKDATABASE);
				if (rs.EndOfFile())
				{
					rs.AddNew();
					blnUpdateFile = true;
				}
				else
				{
					rs.Edit();
					blnUpdateFile = false;
				}
				// ***File Number
				if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != string.Empty && fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("filenumber"))) == string.Empty)
				{
					blnUpdateFile = true;
				}
				rs.Set_Fields("FileNumber", txtFileNumber.Text);
				// ***LastName
				rs.Set_Fields("LastName", txtChildLastName.Text);
				// ***FirstName
				rs.Set_Fields("FirstName", txtChildFirstName.Text);
				// Middle Initial
				rs.Set_Fields("MiddleName", txtChildMI.Text);
				// Designation
				rs.Set_Fields("Designation", txtChildDesignation.Text);

				rs.Set_Fields("dateofbirth", txtChildDOB.Text);
				if (Information.IsDate(txtActualDate.Text))
				{
					rs.Set_Fields("ActualDate", fecherFoundation.DateAndTime.DateValue(txtActualDate.Text));
				}
				else
				{
					rs.Set_Fields("ActualDate", null);
				}
				rs.Set_Fields("MothersAddress", fecherFoundation.Strings.Trim(txtMothersAddress.Text));
				rs.Set_Fields("MothersZipCode", fecherFoundation.Strings.Trim(txtMothersZipCode.Text));
				rs.Set_Fields("MothersYears", fecherFoundation.Strings.Trim(txtYears.Text));
				rs.Set_Fields("MothersBirthPlace", fecherFoundation.Strings.Trim(txtMothersBirthPlace.Text));
				rs.Set_Fields("FathersBirthPlace", fecherFoundation.Strings.Trim(txtFathersBirthPlace.Text));
				rs.Set_Fields("towncode", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				if (txtRegTown.Text != string.Empty)
				{
					rs.Set_Fields("RegTown", txtRegTown.Text);
				}
				else
				{
					rs.Set_Fields("regtown", txtTown.Text);
					txtRegTown.Text = txtTown.Text;
				}
				if (fecherFoundation.FCUtils.IsNull(mebAmendedDate.Text) == true || mebAmendedDate.IsEmpty)
				{
					rs.Set_Fields("DateAmended", null);
				}
				else
				{
					rs.Set_Fields("DateAmended", mebAmendedDate.Text);
				}
				if (fecherFoundation.FCUtils.IsNull(mebDateDeceased.Text) == true || mebDateDeceased.IsEmpty)
				{
					rs.Set_Fields("DeceasedDate", null);
				}
				else
				{
					rs.Set_Fields("DeceasedDate", mebDateDeceased.Text);
				}
				rs.Set_Fields("Amended", txtAmended.Text);
				// .Fields("AmendedInfo") = txtAmendedInfo.Text
				rs.Set_Fields("Court", txtCourt.Text);
				// ***Sex
				if (txtChildSex.Text == "M" || txtChildSex.Text == "m")
				{
					rs.Set_Fields("Sex", "Male");
				}
				else if (txtChildSex.Text == "F" || txtChildSex.Text == "f")
				{
					rs.Set_Fields("Sex", "Female");
				}
				// ***Birth Place
				if (fecherFoundation.FCUtils.IsNull(txtChildBirthPlace.Text) == false)
					rs.Set_Fields("BirthPlace", txtChildBirthPlace.Text);
				// ***Birth Certificate Number
				// If IsNull(txtBirthCertNum.Text) = False Then .Fields("Birthplace = txtBirthCertNum.Text
				// ***Attendants Last Name
				if (fecherFoundation.FCUtils.IsNull(txtAttendantLastName.Text) == false)
					rs.Set_Fields("AttendantsLastName", txtAttendantLastName.Text);
				// ***Attendants First Name
				if (fecherFoundation.FCUtils.IsNull(txtAttendantFirstName.Text) == false)
					rs.Set_Fields("AttendantsFirstName", txtAttendantFirstName.Text);
				// Attendants Middle Initial
				rs.Set_Fields("AttendantsMiddleInitial", txtAttendantMI.Text);
				// Attendants Title
				rs.Set_Fields("AttendantTitle", txtAttendantTitle.Text);
				// ATTENDANTS ADDRESS
				rs.Set_Fields("AttendantAddress", txtAttendantAddress.Text);
				// ***Mothers Last Name
				if (fecherFoundation.FCUtils.IsNull(txtMotherLastName.Text) == false)
					rs.Set_Fields("MothersLastName", txtMotherLastName.Text);
				// ***Mothers Maiden Name
				if (fecherFoundation.FCUtils.IsNull(txtMotherMaidenName.Text) == false)
					rs.Set_Fields("MothersMaidenName", txtMotherMaidenName.Text);
				// ***Mothers First Name
				if (fecherFoundation.FCUtils.IsNull(txtMotherFirstName.Text) == false)
					rs.Set_Fields("MothersFirstName", txtMotherFirstName.Text);
				// Mothers Middle Initial
				rs.Set_Fields("MothersMiddleName", txtMotherMI.Text);
				if (fecherFoundation.Strings.Trim(txtTown.Text) != "")
				{
					rs.Set_Fields("ChildTown", fecherFoundation.Strings.Trim(txtTown.Text));
				}
				if (fecherFoundation.Strings.Trim(txtCounty.Text) != "")
				{
					rs.Set_Fields("ChildCounty", fecherFoundation.Strings.Trim(txtCounty.Text));
				}
				rs.Set_Fields("ChildTime", txtTime.Text);
				rs.Set_Fields("FacilityName", txtFacilityName.Text);
				if (txtMotherDOB.Text == "00/00/0000" || txtMotherDOB.Text == string.Empty)
				{
					rs.Set_Fields("MothersDOB", null);
				}
				else
				{
					rs.Set_Fields("MothersDOB", txtMotherDOB.Text);
				}
				if (txtFathersDOB.Text == "00/00/0000" || txtFathersDOB.Text == string.Empty)
				{
					rs.Set_Fields("FathersDOB", null);
				}
				else
				{
					rs.Set_Fields("FathersDOB", txtFathersDOB.Text);
				}
				if (fecherFoundation.Strings.Trim(txtMotherState.Text) != "")
				{
					rs.Set_Fields("MotherState", fecherFoundation.Strings.Trim(txtMotherState.Text));
				}
				if (fecherFoundation.Strings.Trim(txtMotherCounty.Text) != "")
				{
					rs.Set_Fields("MotherCounty", fecherFoundation.Strings.Trim(txtMotherCounty.Text));
				}
				if (fecherFoundation.Strings.Trim(txtMothersTown.Text) != "")
				{
					rs.Set_Fields("MotherTown", fecherFoundation.Strings.Trim(txtMothersTown.Text));
				}
				// Fathers Last Name
				if (fecherFoundation.Strings.Trim(txtFatherLastName.Text) != "")
				{
					rs.Set_Fields("FathersLastName", txtFatherLastName.Text);
				}
				else
				{
					rs.Set_Fields("FathersLastName", "");
				}
				// Fathers First Name
				if (fecherFoundation.Strings.Trim(txtFatherFirstName.Text) != "")
				{
					rs.Set_Fields("FathersFirstName", txtFatherFirstName.Text);
				}
				else
				{
					rs.Set_Fields("FathersFirstName", "");
				}
				// Fathers Middle Initial
				if (fecherFoundation.Strings.Trim(txtFatherMI.Text) != "")
				{
					rs.Set_Fields("FathersMiddleInitial", txtFatherMI.Text);
				}
				else
				{
					rs.Set_Fields("FathersMiddleInitial", "");
				}
				// Fathers Designation
				rs.Set_Fields("FathersDesignation", txtFatherDesignation.Text);
				// ***Clerks Last Name
				if (fecherFoundation.FCUtils.IsNull(txtClerkLastName.Text) == false)
					rs.Set_Fields("RecordingClerksLastName", txtClerkLastName.Text);
				// ***Clerks First Name
				if (fecherFoundation.FCUtils.IsNull(txtClerkFirstName.Text) == false)
					rs.Set_Fields("RecordingClerksFirstName", txtClerkFirstName.Text);
				// Clerks Middle Initial
				rs.Set_Fields("RecordingClerksMiddleInitial", txtClerkMI.Text);
				// ***Clerk City or Town
				rs.Set_Fields("CityOrTown", fecherFoundation.Strings.Trim(txtClerkCityTown.Text));
				// ***Date of Filing
				if (txtClerkDateOfFiling.IsEmpty)
				{
					rs.Set_Fields("DateOfFiling", null);
				}
				else
				{
					rs.Set_Fields("DateOfFiling", txtClerkDateOfFiling.Text);
				}
				rs.Set_Fields("RegistrarName", fecherFoundation.Strings.Trim(txtRegistrarName.Text));

				if (Conversion.Val(mebRegistrarDateFiling.Text) != 0 && Information.IsDate(mebRegistrarDateFiling.Text))
				{
					rs.Set_Fields("RegistrarDateFiled", Strings.Format(mebRegistrarDateFiling.Text, "MM/dd/yyyy"));
					rs.Set_Fields("RegistrarDateFiledDescription", "");
				}
				else
				{
					rs.Set_Fields("registrardatefiled", null);
					rs.Set_Fields("RegistrarDateFiledDescription", mebRegistrarDateFiling.Text);
				}
				// End If
				// ***Attest Date
				if (txtAttestDate.IsEmpty)
				{
					rs.Set_Fields("AttestDate", null);
				}
				else
				{
					rs.Set_Fields("AttestDate", txtAttestDate.Text);
				}
				// If IsNull(txtAttestDate.Text) = False Then .Fields("AttestDate") = txtAttestDate.Text
				// ***STATE REGISTRAR OR MUNI CLERK
				if (fecherFoundation.FCUtils.IsNull(txtStateRegistrar.Text) == false)
					rs.Set_Fields("StateRegistrarMunicipalClerk", txtStateRegistrar.Text);
				if (fecherFoundation.Strings.Trim(txtStateRegistrar.Text) != string.Empty)
				{
					modRegistry.SaveRegistryKey("CKLastClerkName", txtStateRegistrar.Text);
				}
				// ***TOWN OF
				if (fecherFoundation.FCUtils.IsNull(txtTownOf.Text) == false)
					rs.Set_Fields("TownOf", txtTownOf.Text);
				// ***LEGITIMATE BIRTH
				if (chkIllegitBirth.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs.Set_Fields("Legitimate", false);
				}
				else
				{
					rs.Set_Fields("Legitimate", true);
				}
				if (chkAOP.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs.Set_Fields("aop", true);
				}
				else
				{
					rs.Set_Fields("aop", false);
				}
				rs.Set_Fields("NoRequiredFields", chkRequired.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("DeathRecordInOffice", chkDeceased.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("MarriageOnFile", chkMarriageOnFile.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("SoleParent", chkSoleParent.CheckState == Wisej.Web.CheckState.Checked);
				if (chkCourtDetermination.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs.Set_Fields("CourtDeterminedPaternity", true);
				}
				else
				{
					rs.Set_Fields("CourtDeterminedPaternity", false);
				}
				rs.Update();
				lngBirthID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lblBKey.Text = FCConvert.ToString(rs.Get_Fields("ID"));
				if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != "" && blnUpdateFile)
				{
					rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
					if (rsClerkInfo.BeginningOfFile() != true && rsClerkInfo.EndOfFile() != true)
					{
						rsClerkInfo.Edit();
					}
					else
					{
						rsClerkInfo.AddNew();
					}
					rsClerkInfo.Set_Fields("LastBirthFileNumber", fecherFoundation.Strings.Trim(txtFileNumber.Text));
					rsClerkInfo.Update();
					ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + fecherFoundation.Strings.Trim(txtFileNumber.Text));
					ToolTip1.SetToolTip(lblFile, ToolTip1.GetToolTip(txtFileNumber));
				}
				modAmendments.SaveAmendments(ref VSFlexGrid1, FCConvert.ToInt32(lblBKey.Text), "BIRTHS");
				intDataChanged = 0;
				modDirtyForm.ClearDirtyControls(this);
				boolSaveSuccessfully = true;
				SaveData = true;
				mnuViewDocuments.Enabled = true;
				MessageBox.Show("Save Completed Successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
			}
			return SaveData;
		}

		private void ValidateEntry()
		{
			if (chkRequired.CheckState == CheckState.Unchecked)
			{
				if (fecherFoundation.Strings.Trim(txtChildFirstName.Text) == "")
				{
					MessageBox.Show("You must enter a First Name for the Child.");
					txtChildFirstName.Focus();
					SSTab1.SelectedIndex = 0;
				}
				else if (fecherFoundation.Strings.Trim(txtChildLastName.Text) == "")
				{
					MessageBox.Show("You must enter a Last Name for the Child.");
					txtChildLastName.Focus();
					SSTab1.SelectedIndex = 0;
				}
				else if (fecherFoundation.Strings.Trim(txtChildDOB.Text) == string.Empty)
				{
					MessageBox.Show("You must enter a Date of Birth for the Child.");
					txtChildDOB.Focus();
					SSTab1.SelectedIndex = 0;
				}
				else if (fecherFoundation.Strings.UCase(txtChildSex.Text) != "M" && fecherFoundation.Strings.UCase(txtChildSex.Text) != "F")
				{
					MessageBox.Show("You must Enter the Sex of the Child.");
					txtChildSex.Focus();
					SSTab1.SelectedIndex = 0;
				}
				else if (fecherFoundation.Strings.Trim(txtChildBirthPlace.Text) == "")
				{
					MessageBox.Show("You must enter the Birth Place for the Child.");
					txtChildBirthPlace.Focus();
					SSTab1.SelectedIndex = 0;
					SSTab1.SelectedIndex = 0;
				}
				else if (fecherFoundation.Strings.Trim(txtAttendantFirstName.Text) == "")
				{
					MessageBox.Show("You must enter a First Name for the Attendant.");
					txtAttendantFirstName.Focus();
					SSTab1.SelectedIndex = 0;
				}
				else if (fecherFoundation.Strings.Trim(txtAttendantLastName.Text) == "")
				{
					MessageBox.Show("You must enter a Last Name for the Attendant.");
					txtAttendantLastName.Focus();
					SSTab1.SelectedIndex = 0;
				}
				else if (fecherFoundation.Strings.Trim(txtMotherFirstName.Text) == "")
				{
					MessageBox.Show("You must enter First Name for the Mother.");
					txtMotherFirstName.Focus();
					SSTab1.SelectedIndex = 1;
				}
				else if (fecherFoundation.Strings.Trim(txtMotherMaidenName.Text) == "")
				{
					MessageBox.Show("You must enter Maiden Name for the Mother.");
					txtMotherMaidenName.Focus();
					SSTab1.SelectedIndex = 1;
				}
				else if (fecherFoundation.Strings.Trim(txtMotherLastName.Text) == "")
				{
					MessageBox.Show("You must enter Last Name for the Mother.");
					txtMotherLastName.Focus();
					SSTab1.SelectedIndex = 1;
				}
				else if (fecherFoundation.Strings.Trim(txtClerkFirstName.Text) == "")
				{
					MessageBox.Show("You must enter First Name for the Informant.");
					txtClerkFirstName.Focus();
					SSTab1.SelectedIndex = 2;
				}
				else if (fecherFoundation.Strings.Trim(txtClerkLastName.Text) == "")
				{
					MessageBox.Show("You must enter Last Name for the Informant.");
					txtClerkLastName.Focus();
					SSTab1.SelectedIndex = 2;
				}
				else if (fecherFoundation.Strings.Trim(txtMothersTown.Text) == "")
				{
					MessageBox.Show("You must enter the City or Town of the Mothers Residence.");
					txtMothersTown.Focus();
					SSTab1.SelectedIndex = 1;
				}
				else if (fecherFoundation.Strings.Trim(txtMotherState.Text) == "")
				{
					MessageBox.Show("You must enter the State of the Mothers Residence.");
					txtMotherState.Focus();
					SSTab1.SelectedIndex = 1;
				}
				else if (Information.IsDate(txtAttestDate.Text) == false)
				{
					MessageBox.Show("You must enter a valid Attest Date.");
					txtAttestDate.Focus();
					SSTab1.SelectedIndex = 2;
				}
				else if (fecherFoundation.Strings.Trim(txtStateRegistrar.Text) == "")
				{
					MessageBox.Show("You must enter a valid response for State Registrar/Municipal Clerk.");
					txtStateRegistrar.Focus();
					SSTab1.SelectedIndex = 2;
				}
				else if (fecherFoundation.Strings.Trim(txtTownOf.Text) == "")
				{
					MessageBox.Show("You must enter the Town Name.");
					txtTownOf.Focus();
					SSTab1.SelectedIndex = 2;
				}
				else
				{
					GoodValidation = true;
				}
				if (GoodValidation == true)
				{

					if (!txtClerkDateOfFiling.IsEmpty)
					{
						// CHECK THE FILING DATE
						if (FCConvert.ToDateTime(txtClerkDateOfFiling.Text).Year > DateTime.Today.Year)
						{
							MessageBox.Show("You cannot enter a Filing Date greater than today.");
							GoodValidation = false;
							return;
						}
						else if (FCConvert.ToDateTime(txtClerkDateOfFiling.Text).Year == DateTime.Today.Year)
						{
							if (FCConvert.ToDateTime(txtClerkDateOfFiling.Text).Month > DateTime.Today.Month)
							{
								MessageBox.Show("You cannot enter a Filing Date greater than today.");
								GoodValidation = false;
								return;
							}
							else if (FCConvert.ToDateTime(txtClerkDateOfFiling.Text).Month == DateTime.Today.Month)
							{
								if (FCConvert.ToDateTime(txtClerkDateOfFiling.Text).Day > DateTime.Today.Day)
									MessageBox.Show("You cannot enter a Filing Date greater than today.");
								GoodValidation = false;
								return;
							}
						}
					}

					if (FCConvert.ToDateTime(txtAttestDate.Text).Year > DateTime.Today.Year)
					{
						MessageBox.Show("You cannot enter an Attest Date greater than today.");
						GoodValidation = false;
						return;
					}
					else if (FCConvert.ToDateTime(txtAttestDate.Text).Year == DateTime.Today.Year)
					{
						if (FCConvert.ToDateTime(txtAttestDate.Text).Month > DateTime.Today.Month)
						{
							MessageBox.Show("You cannot enter an Attest Date greater than today.");
							GoodValidation = false;
							return;
						}
						else if (FCConvert.ToDateTime(txtAttestDate.Text).Month == DateTime.Today.Month)
						{
                            if (FCConvert.ToDateTime(txtAttestDate.Text).Day > DateTime.Today.Day)
                            {
                                MessageBox.Show("You cannot enter an Attest Date greater than today.");
                                GoodValidation = false;
                                return;
                            }
						}
					}
				}
			}
			else
			{
				GoodValidation = true;
			}
		}

		private void mnuRegistrarNames_Click(object sender, System.EventArgs e)
		{
			frmRegistrarNames.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveData();
			if (GoodValidation)
				mnuProcessQuit_Click();
		}

		private void mnuSaveExitNoValidation_Click(object sender, System.EventArgs e)
		{
			SaveData(false);
			mnuProcessQuit_Click();
		}

		private void mnuSetDefaultTowns_Click(object sender, System.EventArgs e)
		{
			frmStateTownInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuViewDocuments_Click(object sender, System.EventArgs e)
		{
			ViewDocs();
		}

		private void SSTab1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lngBirthID == 0)
			{
				if (fecherFoundation.Strings.Trim(txtClerkFirstName.Text) == string.Empty && fecherFoundation.Strings.Trim(txtClerkLastName.Text) == string.Empty && fecherFoundation.Strings.Trim(txtClerkMI.Text) == string.Empty)
				{
					txtClerkLastName.Text = txtMotherLastName.Text;
					txtClerkMI.Text = txtMotherMI.Text;
					txtClerkFirstName.Text = txtMotherFirstName.Text;
				}
				if (SSTab1.PreviousTab == 1)
				{
					if (txtClerkCityTown.Text == string.Empty)
					{
						txtClerkCityTown.Text = txtMothersTown.Text;
					}
				}
			}
		}

		private void txtActualDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsDate(txtActualDate.Text))
			{
				if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Now, FCConvert.ToDateTime(txtActualDate.Text)) > 0)
				{
					MessageBox.Show("This birth date is in the future and should be changed", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void txtAttendantAddress_Leave(object sender, System.EventArgs e)
		{
			txtAttendantAddress.SelectionStart = 0;
		}

		private void txtAttendantFirstName_Leave(object sender, System.EventArgs e)
		{
			txtAttendantFirstName.SelectionStart = 0;
		}

		private void txtChildBirthPlace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtChildBirthPlace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtChildDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int Days;
			// days before or after today
			int Ans;

			string strD = "";
			if (fecherFoundation.Strings.Trim(txtChildDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtChildDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtChildDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtChildDOB.Text = strD;
					}
				}
			}
			if (Conversion.Val(txtChildDOB.Text) != 0 && Information.IsDate(txtChildDOB.Text))
			{
				if (FCConvert.ToDateTime(txtChildDOB.Text).Year < 1892)
				{
					chkRequired.CheckState = Wisej.Web.CheckState.Checked;
				}
				txtActualDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateValue(txtChildDOB.Text), "MM/dd/yyyy");
			}
		}

		private void MenuCheck()
		{
			if (Adding == true)
			{
				// mnuPrintBirthCertificate.Enabled = False
				cmdNew.Enabled = false;
				cmdDelete.Enabled = false;
				// mnuGoto.Enabled = False
			}
			else
			{
				// mnuPrintBirthCertificate.Enabled = True
				cmdNew.Enabled = true;
				cmdDelete.Enabled = true;
				// mnuGoto.Enabled = True
			}
		}

		public void Init(int lngID)
		{
			intInitChanges = 0;
			lngBirthID = lngID;
			this.Show(App.MainForm);
		}

		public void frmBirths_Load(object sender, System.EventArgs e)
		{

			clsDRWrapper rsClerkInfo = new clsDRWrapper();
			/* Control ctl = new Control(); */
			foreach (var ctl in this.GetAllControls().Where(ctl => FCConvert.ToString(ctl.Tag) == "Required"))
            {
                ctl.BackColor = ColorTranslator.FromOle(StaticSettings.TrioColorSettings.TRIOCOLORHIGHLIGHT);
            }
			boolFormLoading = true;
			//modGNBas.Statics.boolBirthOpen = true;
			SetupGridTownCode();
			LoadData();
			rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");

            if (rsClerkInfo.EndOfFile() == true || rsClerkInfo.BeginningOfFile() == true)
            {
                ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: UNKNOWN");
            }
            else
            {
                ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + rsClerkInfo.Get_Fields_String("LastBirthFileNumber"));
            }

            ToolTip1.SetToolTip(lblFile, ToolTip1.GetToolTip(txtFileNumber));
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this, false);
			if (chkIllegitBirth.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkIllegitBirth.ForeColor = Color.Red;
			}
			//imgDocuments.Visible = frmViewDocuments.InstancePtr.docList?.Count>0;
            imgDocuments.Visible = CheckForDocs(lngBirthID, "Birth");
            mnuViewDocuments.Enabled = lngBirthID > 0;
			boolLoadRun = true;
			boolFormLoading = false;
		}

        public bool CheckForDocs(int referenceId, string referenceType)
        {
            var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
            var docList = new Collection<CentralDocumentHeader>();

            docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand { DataGroup = "Clerk", ReferenceId = referenceId, ReferenceType = referenceType });

            return docList.Count > 0;
        }

        public void Form_Load()
		{
			frmBirths_Load(this, new System.EventArgs());
		}

        private void txtChildSex_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtChildSex.Modified)
            {
                string text = txtChildSex.Text;
                if (text != "M" && text != "F")
                {
                    MessageBox.Show("Must enter 'M' or 'F'.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
        }

        private void txtClerkCityTown_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtClerkCityTown.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtClerkFirstName_Enter(object sender, System.EventArgs e)
		{
			txtClerkFirstName.SelectionStart = 0;
			txtClerkFirstName.SelectionLength = 50;
			this.SSTab1.SelectedIndex = 2;
		}

		private void txtClerkCityTown_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtCounty_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "C";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCounty.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtFathersBirthPlace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtFathersBirthPlace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtFathersDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtFathersDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtFathersDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtFathersDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtFathersDOB.Text = strD;
					}
				}
			}
		}

		private void txtMotherCounty_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "C";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtMotherCounty.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtMotherDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtMotherDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtMotherDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtMotherDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtMotherDOB.Text = strD;
					}
				}
			}
		}

		private void txtMothersBirthPlace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtMothersBirthPlace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtMotherState_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtMotherState.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtMothersTown_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtMothersTown.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtRegTown_DoubleClick(object sender, System.EventArgs e)
		{
			txtRegTown.Text = frmSelectTownStateCounty.InstancePtr.Init("T", txtRegTown.Text);
		}

		private void txtRegTown_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtTown_DoubleClick(object sender, System.EventArgs e)
		{
			txtTown.Text = frmSelectTownStateCounty.InstancePtr.Init("T", txtTown.Text);
		}

		private void txtTown_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtRegTown.Text == string.Empty)
			{
				txtRegTown.Text = txtTown.Text;
			}
		}

		private void txtTownOf_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtTownOf.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtTownOf_Enter(object sender, System.EventArgs e)
		{
			txtTownOf.SelectionLength = txtTownOf.Text.Length;
		}

		private void txtTownOf_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			chkIllegitBirth.Focus();
			//e.KeepFocus = false;
		}

		private void VSFlexGrid1_ComboCloseUp(object sender, EventArgs e)
		{
			if (VSFlexGrid1.Col == 3)
			{
				modAmendments.UpdateAmendmentTooltips(VSFlexGrid1, VSFlexGrid1.Row, VSFlexGrid1.EditText);
			}
		}

		private void VSFlexGrid1_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 3;
		}

		private void VSFlexGrid1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == (Keys)46)
			{
				if (MessageBox.Show("Delete current record", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					VSFlexGrid1.RemoveItem(VSFlexGrid1.Row);
					VSFlexGrid1.AddItem(string.Empty);
				}
			}
		}

		private void VSFlexGrid1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = VSFlexGrid1[e.ColumnIndex, e.RowIndex];
            int lngRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			string[] strAry = null;
			lngRow = VSFlexGrid1.GetFlexRowIndex(e.RowIndex);
			intCol = VSFlexGrid1.GetFlexColIndex(e.ColumnIndex);
			if (intCol == 2 || lngRow < 1)
			{
                //ToolTip1.SetToolTip(VSFlexGrid1, "");
                cell.ToolTipText = "";
			}
			else if (intCol > 3 && intCol < 7)
			{
				//strAry = Strings.Split(" " + VSFlexGrid1.TextMatrix(lngRow, 7), ",", -1, CompareConstants.vbTextCompare);
				//if (Information.UBound(strAry, 1) >= intCol - 4)
				//{
				//	//ToolTip1.SetToolTip(VSFlexGrid1, fecherFoundation.Strings.Trim(strAry[intCol - 4]));
				//	cell.ToolTipText = fecherFoundation.Strings.Trim(strAry[intCol - 4]);
				//}
				//else
				//{
    //                //ToolTip1.SetToolTip(VSFlexGrid1, "");
    //                cell.ToolTipText = "";
				//}
			}
			else
			{
				//ToolTip1.SetToolTip(VSFlexGrid1, FCConvert.ToString(VSFlexGrid1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, intCol)));
				cell.ToolTipText = FCConvert.ToString(VSFlexGrid1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, intCol));
			}
		}

        public IBirthViewModel ViewModel { get; set; }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }
    }
}
