﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMarriageIntentionsWordDoc.
	/// </summary>
	partial class rptMarriageIntentionsWordDoc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMarriageIntentionsWordDoc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtGroomFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsDesig = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsFathersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsMothersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsMothersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesSurName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesFathersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesMothersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesMothersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsMarriageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsMarriageEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesMarriageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesMarriageEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroomFormerSpouse = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroomCourtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBrideFormerSpouse = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBrideCourtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroomDomesticPartnerYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroomDomesticPartnersNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBrideDomesticPartnerYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBrideDomesticPartnerNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtstate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIssuance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMarriage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroomDomesticPartnersYearRegistered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBrideDomesticPartnersYearRegistered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsMarriageEndedDay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomsMarriageEndedYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesMarriageEndedDay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBridesMarriageEndedYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomEndedDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomEndedDivorce = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroomEndedAnnulment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBrideEndedDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBrideEndedDivorce = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBrideEndedAnnulment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomFirst)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomAge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomLast)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsDesig)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersBirthplace)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersBirthplace)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesFirst)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesAge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesSurName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersBirthplace)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersBirthplace)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesLast)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEnded)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEnded)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomFormerSpouse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomCourtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideFormerSpouse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideCourtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnerYes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerYes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtstate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssuance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMarriage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersYearRegistered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnersYearRegistered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEndedDay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEndedYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEndedDay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEndedYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedDivorce)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedAnnulment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedDivorce)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedAnnulment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroomFirst,
				this.txtGroomAge,
				this.txtGroomMiddle,
				this.txtGroomLast,
				this.txtGroomsDesig,
				this.txtGroomsState,
				this.txtGroomsFathersName,
				this.txtGroomsFathersBirthplace,
				this.txtGroomsMothersName,
				this.txtGroomsMothersBirthplace,
				this.txtBridesFirst,
				this.txtBridesAge,
				this.txtBridesMiddle,
				this.txtBridesSurName,
				this.txtBridesState,
				this.txtBridesFathersName,
				this.txtBridesFathersBirthplace,
				this.txtBridesMothersName,
				this.txtBridesMothersBirthplace,
				this.txtBridesLast,
				this.txtGroomsMarriageNumber,
				this.txtGroomsMarriageEnded,
				this.txtBridesMarriageNumber,
				this.txtBridesMarriageEnded,
				this.fldGroomFormerSpouse,
				this.fldGroomCourtName,
				this.fldBrideFormerSpouse,
				this.fldBrideCourtName,
				this.fldGroomDomesticPartnerYes,
				this.fldGroomDomesticPartnersNo,
				this.fldBrideDomesticPartnerYes,
				this.fldBrideDomesticPartnerNo,
				this.txtstate,
				this.txtIssuance,
				this.txtMarriage,
				this.fldGroomDomesticPartnersYearRegistered,
				this.fldBrideDomesticPartnersYearRegistered,
				this.txtGroomsMarriageEndedDay,
				this.txtGroomsMarriageEndedYear,
				this.txtBridesMarriageEndedDay,
				this.txtBridesMarriageEndedYear,
				this.txtGroomEndedDeath,
				this.txtGroomEndedDivorce,
				this.txtGroomEndedAnnulment,
				this.txtBrideEndedDeath,
				this.txtBrideEndedDivorce,
				this.txtBrideEndedAnnulment
			});
			this.Detail.Height = 5.395833F;
			this.Detail.Name = "Detail";
			// 
			// txtGroomFirst
			// 
			this.txtGroomFirst.CanGrow = false;
			this.txtGroomFirst.Height = 0.1666667F;
			this.txtGroomFirst.Left = 0.0625F;
			this.txtGroomFirst.Name = "txtGroomFirst";
			this.txtGroomFirst.Text = null;
			this.txtGroomFirst.Top = 0.8541667F;
			this.txtGroomFirst.Width = 1.59375F;
			// 
			// txtGroomAge
			// 
			this.txtGroomAge.CanGrow = false;
			this.txtGroomAge.Height = 0.1875F;
			this.txtGroomAge.Left = 0.0625F;
			this.txtGroomAge.Name = "txtGroomAge";
			this.txtGroomAge.Text = null;
			this.txtGroomAge.Top = 1.159722F;
			this.txtGroomAge.Width = 0.8125F;
			// 
			// txtGroomMiddle
			// 
			this.txtGroomMiddle.CanGrow = false;
			this.txtGroomMiddle.Height = 0.1666667F;
			this.txtGroomMiddle.Left = 1.875F;
			this.txtGroomMiddle.Name = "txtGroomMiddle";
			this.txtGroomMiddle.Text = null;
			this.txtGroomMiddle.Top = 0.8541667F;
			this.txtGroomMiddle.Width = 1.875F;
			// 
			// txtGroomLast
			// 
			this.txtGroomLast.CanGrow = false;
			this.txtGroomLast.Height = 0.1666667F;
			this.txtGroomLast.Left = 4F;
			this.txtGroomLast.Name = "txtGroomLast";
			this.txtGroomLast.Text = null;
			this.txtGroomLast.Top = 0.8541667F;
			this.txtGroomLast.Width = 2.5625F;
			// 
			// txtGroomsDesig
			// 
			this.txtGroomsDesig.CanGrow = false;
			this.txtGroomsDesig.Height = 0.1666667F;
			this.txtGroomsDesig.Left = 6.75F;
			this.txtGroomsDesig.Name = "txtGroomsDesig";
			this.txtGroomsDesig.Text = null;
			this.txtGroomsDesig.Top = 0.8541667F;
			this.txtGroomsDesig.Width = 0.4375F;
			// 
			// txtGroomsState
			// 
			this.txtGroomsState.CanGrow = false;
			this.txtGroomsState.Height = 0.1666667F;
			this.txtGroomsState.Left = 1F;
			this.txtGroomsState.Name = "txtGroomsState";
			this.txtGroomsState.Text = null;
			this.txtGroomsState.Top = 1.159722F;
			this.txtGroomsState.Width = 1.25F;
			// 
			// txtGroomsFathersName
			// 
			this.txtGroomsFathersName.CanGrow = false;
			this.txtGroomsFathersName.Height = 0.1666667F;
			this.txtGroomsFathersName.Left = 2.5F;
			this.txtGroomsFathersName.Name = "txtGroomsFathersName";
			this.txtGroomsFathersName.Text = null;
			this.txtGroomsFathersName.Top = 1.15625F;
			this.txtGroomsFathersName.Width = 2.770833F;
			// 
			// txtGroomsFathersBirthplace
			// 
			this.txtGroomsFathersBirthplace.CanGrow = false;
			this.txtGroomsFathersBirthplace.Height = 0.1666667F;
			this.txtGroomsFathersBirthplace.Left = 5.822917F;
			this.txtGroomsFathersBirthplace.Name = "txtGroomsFathersBirthplace";
			this.txtGroomsFathersBirthplace.Text = null;
			this.txtGroomsFathersBirthplace.Top = 1.15625F;
			this.txtGroomsFathersBirthplace.Width = 1.229167F;
			// 
			// txtGroomsMothersName
			// 
			this.txtGroomsMothersName.CanGrow = false;
			this.txtGroomsMothersName.Height = 0.1666667F;
			this.txtGroomsMothersName.Left = 0.0625F;
			this.txtGroomsMothersName.Name = "txtGroomsMothersName";
			this.txtGroomsMothersName.Text = null;
			this.txtGroomsMothersName.Top = 1.479167F;
			this.txtGroomsMothersName.Width = 3.34375F;
			// 
			// txtGroomsMothersBirthplace
			// 
			this.txtGroomsMothersBirthplace.CanGrow = false;
			this.txtGroomsMothersBirthplace.Height = 0.1666667F;
			this.txtGroomsMothersBirthplace.Left = 3.96875F;
			this.txtGroomsMothersBirthplace.Name = "txtGroomsMothersBirthplace";
			this.txtGroomsMothersBirthplace.Text = null;
			this.txtGroomsMothersBirthplace.Top = 1.479167F;
			this.txtGroomsMothersBirthplace.Width = 3.0625F;
			// 
			// txtBridesFirst
			// 
			this.txtBridesFirst.CanGrow = false;
			this.txtBridesFirst.Height = 0.19F;
			this.txtBridesFirst.Left = 0.0625F;
			this.txtBridesFirst.Name = "txtBridesFirst";
			this.txtBridesFirst.Text = null;
			this.txtBridesFirst.Top = 2.055556F;
			this.txtBridesFirst.Width = 2.125F;
			// 
			// txtBridesAge
			// 
			this.txtBridesAge.CanGrow = false;
			this.txtBridesAge.Height = 0.19F;
			this.txtBridesAge.Left = 0.0625F;
			this.txtBridesAge.Name = "txtBridesAge";
			this.txtBridesAge.Text = null;
			this.txtBridesAge.Top = 2.388889F;
			this.txtBridesAge.Width = 0.625F;
			// 
			// txtBridesMiddle
			// 
			this.txtBridesMiddle.CanGrow = false;
			this.txtBridesMiddle.Height = 0.19F;
			this.txtBridesMiddle.Left = 2.3125F;
			this.txtBridesMiddle.Name = "txtBridesMiddle";
			this.txtBridesMiddle.Text = null;
			this.txtBridesMiddle.Top = 2.055556F;
			this.txtBridesMiddle.Width = 1.5625F;
			// 
			// txtBridesSurName
			// 
			this.txtBridesSurName.CanGrow = false;
			this.txtBridesSurName.Height = 0.19F;
			this.txtBridesSurName.Left = 4F;
			this.txtBridesSurName.Name = "txtBridesSurName";
			this.txtBridesSurName.Text = null;
			this.txtBridesSurName.Top = 2.055556F;
			this.txtBridesSurName.Width = 1.75F;
			// 
			// txtBridesState
			// 
			this.txtBridesState.CanGrow = false;
			this.txtBridesState.Height = 0.19F;
			this.txtBridesState.Left = 1.6875F;
			this.txtBridesState.Name = "txtBridesState";
			this.txtBridesState.Text = null;
			this.txtBridesState.Top = 2.388889F;
			this.txtBridesState.Width = 0.625F;
			// 
			// txtBridesFathersName
			// 
			this.txtBridesFathersName.CanGrow = false;
			this.txtBridesFathersName.Height = 0.1736111F;
			this.txtBridesFathersName.Left = 2.5F;
			this.txtBridesFathersName.Name = "txtBridesFathersName";
			this.txtBridesFathersName.Text = null;
			this.txtBridesFathersName.Top = 2.388889F;
			this.txtBridesFathersName.Width = 2.875F;
			// 
			// txtBridesFathersBirthplace
			// 
			this.txtBridesFathersBirthplace.CanGrow = false;
			this.txtBridesFathersBirthplace.Height = 0.1875F;
			this.txtBridesFathersBirthplace.Left = 5.8125F;
			this.txtBridesFathersBirthplace.Name = "txtBridesFathersBirthplace";
			this.txtBridesFathersBirthplace.Text = null;
			this.txtBridesFathersBirthplace.Top = 2.375F;
			this.txtBridesFathersBirthplace.Width = 1.625F;
			// 
			// txtBridesMothersName
			// 
			this.txtBridesMothersName.CanGrow = false;
			this.txtBridesMothersName.Height = 0.1666667F;
			this.txtBridesMothersName.Left = 0.0625F;
			this.txtBridesMothersName.Name = "txtBridesMothersName";
			this.txtBridesMothersName.Text = null;
			this.txtBridesMothersName.Top = 2.680556F;
			this.txtBridesMothersName.Width = 3.25F;
			// 
			// txtBridesMothersBirthplace
			// 
			this.txtBridesMothersBirthplace.CanGrow = false;
			this.txtBridesMothersBirthplace.Height = 0.1666667F;
			this.txtBridesMothersBirthplace.Left = 3.96875F;
			this.txtBridesMothersBirthplace.Name = "txtBridesMothersBirthplace";
			this.txtBridesMothersBirthplace.Text = null;
			this.txtBridesMothersBirthplace.Top = 2.680556F;
			this.txtBridesMothersBirthplace.Width = 3F;
			// 
			// txtBridesLast
			// 
			this.txtBridesLast.CanGrow = false;
			this.txtBridesLast.Height = 0.19F;
			this.txtBridesLast.Left = 5.8125F;
			this.txtBridesLast.Name = "txtBridesLast";
			this.txtBridesLast.Text = null;
			this.txtBridesLast.Top = 2.055556F;
			this.txtBridesLast.Width = 1.25F;
			// 
			// txtGroomsMarriageNumber
			// 
			this.txtGroomsMarriageNumber.CanGrow = false;
			this.txtGroomsMarriageNumber.Height = 0.19F;
			this.txtGroomsMarriageNumber.Left = 0.0625F;
			this.txtGroomsMarriageNumber.Name = "txtGroomsMarriageNumber";
			this.txtGroomsMarriageNumber.Text = null;
			this.txtGroomsMarriageNumber.Top = 3.833333F;
			this.txtGroomsMarriageNumber.Width = 0.625F;
			// 
			// txtGroomsMarriageEnded
			// 
			this.txtGroomsMarriageEnded.CanGrow = false;
			this.txtGroomsMarriageEnded.Height = 0.1666667F;
			this.txtGroomsMarriageEnded.Left = 1.90625F;
			this.txtGroomsMarriageEnded.Name = "txtGroomsMarriageEnded";
			this.txtGroomsMarriageEnded.Text = null;
			this.txtGroomsMarriageEnded.Top = 3.774306F;
			this.txtGroomsMarriageEnded.Width = 0.3958333F;
			// 
			// txtBridesMarriageNumber
			// 
			this.txtBridesMarriageNumber.CanGrow = false;
			this.txtBridesMarriageNumber.Height = 0.1666667F;
			this.txtBridesMarriageNumber.Left = 3.75F;
			this.txtBridesMarriageNumber.Name = "txtBridesMarriageNumber";
			this.txtBridesMarriageNumber.Text = null;
			this.txtBridesMarriageNumber.Top = 3.833333F;
			this.txtBridesMarriageNumber.Width = 0.625F;
			// 
			// txtBridesMarriageEnded
			// 
			this.txtBridesMarriageEnded.CanGrow = false;
			this.txtBridesMarriageEnded.Height = 0.1666667F;
			this.txtBridesMarriageEnded.Left = 5.8125F;
			this.txtBridesMarriageEnded.Name = "txtBridesMarriageEnded";
			this.txtBridesMarriageEnded.Text = null;
			this.txtBridesMarriageEnded.Top = 3.767361F;
			this.txtBridesMarriageEnded.Width = 0.3541667F;
			// 
			// fldGroomFormerSpouse
			// 
			this.fldGroomFormerSpouse.CanGrow = false;
			this.fldGroomFormerSpouse.Height = 0.1666667F;
			this.fldGroomFormerSpouse.Left = 1.75F;
			this.fldGroomFormerSpouse.Name = "fldGroomFormerSpouse";
			this.fldGroomFormerSpouse.Text = null;
			this.fldGroomFormerSpouse.Top = 3.989583F;
			this.fldGroomFormerSpouse.Width = 1.875F;
			// 
			// fldGroomCourtName
			// 
			this.fldGroomCourtName.CanGrow = false;
			this.fldGroomCourtName.Height = 0.1666667F;
			this.fldGroomCourtName.Left = 0F;
			this.fldGroomCourtName.Name = "fldGroomCourtName";
			this.fldGroomCourtName.Text = null;
			this.fldGroomCourtName.Top = 4.583333F;
			this.fldGroomCourtName.Width = 2.65625F;
			// 
			// fldBrideFormerSpouse
			// 
			this.fldBrideFormerSpouse.CanGrow = false;
			this.fldBrideFormerSpouse.Height = 0.1666667F;
			this.fldBrideFormerSpouse.Left = 5.59375F;
			this.fldBrideFormerSpouse.Name = "fldBrideFormerSpouse";
			this.fldBrideFormerSpouse.Text = null;
			this.fldBrideFormerSpouse.Top = 3.979167F;
			this.fldBrideFormerSpouse.Width = 1.875F;
			// 
			// fldBrideCourtName
			// 
			this.fldBrideCourtName.CanGrow = false;
			this.fldBrideCourtName.Height = 0.1666667F;
			this.fldBrideCourtName.Left = 3.75F;
			this.fldBrideCourtName.Name = "fldBrideCourtName";
			this.fldBrideCourtName.Text = null;
			this.fldBrideCourtName.Top = 4.583333F;
			this.fldBrideCourtName.Width = 2.65625F;
			// 
			// fldGroomDomesticPartnerYes
			// 
			this.fldGroomDomesticPartnerYes.Height = 0.21875F;
			this.fldGroomDomesticPartnerYes.Left = 0.625F;
			this.fldGroomDomesticPartnerYes.Name = "fldGroomDomesticPartnerYes";
			this.fldGroomDomesticPartnerYes.Style = "text-align: right";
			this.fldGroomDomesticPartnerYes.Text = "X";
			this.fldGroomDomesticPartnerYes.Top = 4.270833F;
			this.fldGroomDomesticPartnerYes.Width = 0.1875F;
			// 
			// fldGroomDomesticPartnersNo
			// 
			this.fldGroomDomesticPartnersNo.Height = 0.21875F;
			this.fldGroomDomesticPartnersNo.Left = 1.125F;
			this.fldGroomDomesticPartnersNo.Name = "fldGroomDomesticPartnersNo";
			this.fldGroomDomesticPartnersNo.Style = "text-align: right";
			this.fldGroomDomesticPartnersNo.Text = "X";
			this.fldGroomDomesticPartnersNo.Top = 4.270833F;
			this.fldGroomDomesticPartnersNo.Width = 0.1875F;
			// 
			// fldBrideDomesticPartnerYes
			// 
			this.fldBrideDomesticPartnerYes.Height = 0.21875F;
			this.fldBrideDomesticPartnerYes.Left = 4.677083F;
			this.fldBrideDomesticPartnerYes.Name = "fldBrideDomesticPartnerYes";
			this.fldBrideDomesticPartnerYes.Style = "text-align: right";
			this.fldBrideDomesticPartnerYes.Text = "X";
			this.fldBrideDomesticPartnerYes.Top = 4.270833F;
			this.fldBrideDomesticPartnerYes.Width = 0.1875F;
			// 
			// fldBrideDomesticPartnerNo
			// 
			this.fldBrideDomesticPartnerNo.Height = 0.21875F;
			this.fldBrideDomesticPartnerNo.Left = 5.166667F;
			this.fldBrideDomesticPartnerNo.Name = "fldBrideDomesticPartnerNo";
			this.fldBrideDomesticPartnerNo.Style = "text-align: right";
			this.fldBrideDomesticPartnerNo.Text = "X";
			this.fldBrideDomesticPartnerNo.Top = 4.270833F;
			this.fldBrideDomesticPartnerNo.Width = 0.1875F;
			// 
			// txtstate
			// 
			this.txtstate.Height = 0.2083333F;
			this.txtstate.Left = 0.25F;
			this.txtstate.Name = "txtstate";
			this.txtstate.Text = "X";
			this.txtstate.Top = 0.2916667F;
			this.txtstate.Visible = false;
			this.txtstate.Width = 0.1875F;
			// 
			// txtIssuance
			// 
			this.txtIssuance.Height = 0.2083333F;
			this.txtIssuance.Left = 0.9375F;
			this.txtIssuance.Name = "txtIssuance";
			this.txtIssuance.Text = "X";
			this.txtIssuance.Top = 0.4166667F;
			this.txtIssuance.Visible = false;
			this.txtIssuance.Width = 0.1875F;
			// 
			// txtMarriage
			// 
			this.txtMarriage.Height = 0.2083333F;
			this.txtMarriage.Left = 1.0625F;
			this.txtMarriage.Name = "txtMarriage";
			this.txtMarriage.Text = "X";
			this.txtMarriage.Top = 0.5416667F;
			this.txtMarriage.Visible = false;
			this.txtMarriage.Width = 0.1875F;
			// 
			// fldGroomDomesticPartnersYearRegistered
			// 
			this.fldGroomDomesticPartnersYearRegistered.CanGrow = false;
			this.fldGroomDomesticPartnersYearRegistered.Height = 0.19F;
			this.fldGroomDomesticPartnersYearRegistered.Left = 2.90625F;
			this.fldGroomDomesticPartnersYearRegistered.Name = "fldGroomDomesticPartnersYearRegistered";
			this.fldGroomDomesticPartnersYearRegistered.Text = null;
			this.fldGroomDomesticPartnersYearRegistered.Top = 4.302083F;
			this.fldGroomDomesticPartnersYearRegistered.Width = 0.59375F;
			// 
			// fldBrideDomesticPartnersYearRegistered
			// 
			this.fldBrideDomesticPartnersYearRegistered.CanGrow = false;
			this.fldBrideDomesticPartnersYearRegistered.Height = 0.19F;
			this.fldBrideDomesticPartnersYearRegistered.Left = 6.90625F;
			this.fldBrideDomesticPartnersYearRegistered.Name = "fldBrideDomesticPartnersYearRegistered";
			this.fldBrideDomesticPartnersYearRegistered.Text = null;
			this.fldBrideDomesticPartnersYearRegistered.Top = 4.302083F;
			this.fldBrideDomesticPartnersYearRegistered.Width = 0.53125F;
			// 
			// txtGroomsMarriageEndedDay
			// 
			this.txtGroomsMarriageEndedDay.CanGrow = false;
			this.txtGroomsMarriageEndedDay.Height = 0.1666667F;
			this.txtGroomsMarriageEndedDay.Left = 2.375F;
			this.txtGroomsMarriageEndedDay.Name = "txtGroomsMarriageEndedDay";
			this.txtGroomsMarriageEndedDay.Text = null;
			this.txtGroomsMarriageEndedDay.Top = 3.774306F;
			this.txtGroomsMarriageEndedDay.Width = 0.3958333F;
			// 
			// txtGroomsMarriageEndedYear
			// 
			this.txtGroomsMarriageEndedYear.CanGrow = false;
			this.txtGroomsMarriageEndedYear.Height = 0.1666667F;
			this.txtGroomsMarriageEndedYear.Left = 2.8125F;
			this.txtGroomsMarriageEndedYear.Name = "txtGroomsMarriageEndedYear";
			this.txtGroomsMarriageEndedYear.Text = null;
			this.txtGroomsMarriageEndedYear.Top = 3.774306F;
			this.txtGroomsMarriageEndedYear.Width = 0.3958333F;
			// 
			// txtBridesMarriageEndedDay
			// 
			this.txtBridesMarriageEndedDay.CanGrow = false;
			this.txtBridesMarriageEndedDay.Height = 0.1666667F;
			this.txtBridesMarriageEndedDay.Left = 6.25F;
			this.txtBridesMarriageEndedDay.Name = "txtBridesMarriageEndedDay";
			this.txtBridesMarriageEndedDay.Text = null;
			this.txtBridesMarriageEndedDay.Top = 3.767361F;
			this.txtBridesMarriageEndedDay.Width = 0.3958333F;
			// 
			// txtBridesMarriageEndedYear
			// 
			this.txtBridesMarriageEndedYear.CanGrow = false;
			this.txtBridesMarriageEndedYear.Height = 0.1666667F;
			this.txtBridesMarriageEndedYear.Left = 6.6875F;
			this.txtBridesMarriageEndedYear.Name = "txtBridesMarriageEndedYear";
			this.txtBridesMarriageEndedYear.Text = null;
			this.txtBridesMarriageEndedYear.Top = 3.767361F;
			this.txtBridesMarriageEndedYear.Width = 0.3958333F;
			// 
			// txtGroomEndedDeath
			// 
			this.txtGroomEndedDeath.Height = 0.21875F;
			this.txtGroomEndedDeath.Left = 0.875F;
			this.txtGroomEndedDeath.Name = "txtGroomEndedDeath";
			this.txtGroomEndedDeath.Text = "X";
			this.txtGroomEndedDeath.Top = 3.552083F;
			this.txtGroomEndedDeath.Visible = false;
			this.txtGroomEndedDeath.Width = 0.1875F;
			// 
			// txtGroomEndedDivorce
			// 
			this.txtGroomEndedDivorce.Height = 0.21875F;
			this.txtGroomEndedDivorce.Left = 1.75F;
			this.txtGroomEndedDivorce.Name = "txtGroomEndedDivorce";
			this.txtGroomEndedDivorce.Style = "text-align: center";
			this.txtGroomEndedDivorce.Text = "X";
			this.txtGroomEndedDivorce.Top = 3.552083F;
			this.txtGroomEndedDivorce.Visible = false;
			this.txtGroomEndedDivorce.Width = 0.1875F;
			// 
			// txtGroomEndedAnnulment
			// 
			this.txtGroomEndedAnnulment.Height = 0.21875F;
			this.txtGroomEndedAnnulment.Left = 2.625F;
			this.txtGroomEndedAnnulment.Name = "txtGroomEndedAnnulment";
			this.txtGroomEndedAnnulment.Style = "text-align: right";
			this.txtGroomEndedAnnulment.Text = "X";
			this.txtGroomEndedAnnulment.Top = 3.552083F;
			this.txtGroomEndedAnnulment.Visible = false;
			this.txtGroomEndedAnnulment.Width = 0.1875F;
			// 
			// txtBrideEndedDeath
			// 
			this.txtBrideEndedDeath.Height = 0.21875F;
			this.txtBrideEndedDeath.Left = 4.6875F;
			this.txtBrideEndedDeath.Name = "txtBrideEndedDeath";
			this.txtBrideEndedDeath.Style = "text-align: center";
			this.txtBrideEndedDeath.Text = "X";
			this.txtBrideEndedDeath.Top = 3.552083F;
			this.txtBrideEndedDeath.Visible = false;
			this.txtBrideEndedDeath.Width = 0.1875F;
			// 
			// txtBrideEndedDivorce
			// 
			this.txtBrideEndedDivorce.Height = 0.21875F;
			this.txtBrideEndedDivorce.Left = 5.5625F;
			this.txtBrideEndedDivorce.Name = "txtBrideEndedDivorce";
			this.txtBrideEndedDivorce.Style = "text-align: right";
			this.txtBrideEndedDivorce.Text = "X";
			this.txtBrideEndedDivorce.Top = 3.552083F;
			this.txtBrideEndedDivorce.Visible = false;
			this.txtBrideEndedDivorce.Width = 0.1875F;
			// 
			// txtBrideEndedAnnulment
			// 
			this.txtBrideEndedAnnulment.Height = 0.21875F;
			this.txtBrideEndedAnnulment.Left = 6.4375F;
			this.txtBrideEndedAnnulment.Name = "txtBrideEndedAnnulment";
			this.txtBrideEndedAnnulment.Style = "text-align: right";
			this.txtBrideEndedAnnulment.Text = "X";
			this.txtBrideEndedAnnulment.Top = 3.552083F;
			this.txtBrideEndedAnnulment.Visible = false;
			this.txtBrideEndedAnnulment.Width = 0.1875F;
			// 
			// rptMarriageIntentionsWordDoc
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.3472222F;
			this.PageSettings.Margins.Left = 0.625F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.3680556F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtGroomFirst)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomAge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomLast)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsDesig)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersBirthplace)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersBirthplace)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesFirst)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesAge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesSurName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersBirthplace)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersBirthplace)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesLast)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEnded)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEnded)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomFormerSpouse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomCourtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideFormerSpouse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideCourtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnerYes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerYes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtstate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssuance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMarriage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersYearRegistered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnersYearRegistered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEndedDay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEndedYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEndedDay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEndedYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedDivorce)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedAnnulment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedDivorce)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedAnnulment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsDesig;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsFathersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMothersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMothersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesSurName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFathersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMothersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMothersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomFormerSpouse;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomCourtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideFormerSpouse;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideCourtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomDomesticPartnerYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomDomesticPartnersNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideDomesticPartnerYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideDomesticPartnerNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtstate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssuance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarriage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomDomesticPartnersYearRegistered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideDomesticPartnersYearRegistered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageEndedDay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageEndedYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageEndedDay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageEndedYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomEndedDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomEndedDivorce;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomEndedAnnulment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideEndedDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideEndedDivorce;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideEndedAnnulment;
	}
}
