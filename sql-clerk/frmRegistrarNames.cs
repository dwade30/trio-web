//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmRegistrarNames : BaseForm
	{
		public frmRegistrarNames()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			Label1 = new System.Collections.Generic.List<FCLabel>();
			Label1.AddControlArrayElement(Label1_0, 0);
			Label1.AddControlArrayElement(Label1_1, 1);
			Label1.AddControlArrayElement(Label1_2, 2);
			Label1.AddControlArrayElement(Label1_3, 3);
			Label1.AddControlArrayElement(Label1_4, 4);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmRegistrarNames InstancePtr
		{
			get
			{
				return (frmRegistrarNames)Sys.GetInstance(typeof(frmRegistrarNames));
			}
		}

		protected frmRegistrarNames _InstancePtr = null;
		//=========================================================
		object strName;
		clsDRWrapper rsRegistrar = new clsDRWrapper();
		private int intDataChanged;

		private void cboRegistrar_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			if (cboRegistrar.SelectedIndex < 0)
				return;
			cboRegistrar.Tag = cboRegistrar.ItemData(cboRegistrar.SelectedIndex);
			rs.OpenRecordset("Select * from DefaultRegistrarNames where ID = " + cboRegistrar.Tag, modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				this.txtFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FirstName"));
				this.txtLastName.Text = FCConvert.ToString(rs.Get_Fields_String("LastName"));
				this.txtMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("MI"));
				this.txtTown.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ClerkTown")));
			}
			modDirtyForm.ClearDirtyControls(this);
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToInt32(cboRegistrar.Tag) > 0)
			{
				if (MessageBox.Show("Are you sure you wish to delete this Clerk?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
					rsRegistrar.Execute("Delete from DefaultRegistrarNames where ID = " + cboRegistrar.Tag, modGNBas.DEFAULTDATABASE);
					modGlobalRoutines.ShowWaitMessage(this);
					modGlobalRoutines.GetAllRegistrar(ref cboRegistrar);
					if (cboRegistrar.Items.Count > 0)
						cboRegistrar.SelectedIndex = 0;
					cboRegistrar.Focus();
					modDirtyForm.ClearDirtyControls(this);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				}
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			txtFirstName.Text = string.Empty;
			txtLastName.Text = string.Empty;
			txtMiddleName.Text = string.Empty;
			cboRegistrar.Tag = 0;
			txtFirstName.Focus();
			modDirtyForm.ClearDirtyControls(this);
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		public void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtFirstName.Text) == "" && fecherFoundation.Strings.Trim(txtLastName.Text) == "" && fecherFoundation.Strings.Trim(txtMiddleName.Text) == "" && fecherFoundation.Strings.Trim(txtTown.Text) == "")
			{
				MessageBox.Show("You must enter information before you may save.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (Conversion.Val(cboRegistrar.Tag) == 0)
			{
				rsRegistrar.Execute("Insert into DefaultRegistrarNames (FirstName,LastName,MI,ClerkTown) VALUES ('" + modGlobalFunctions.EscapeQuotes(txtFirstName.Text) + "','" + modGlobalFunctions.EscapeQuotes(txtLastName.Text) + "','" + modGlobalFunctions.EscapeQuotes(txtMiddleName.Text) + "','" + fecherFoundation.Strings.Trim(txtTown.Text) + "')", modGNBas.DEFAULTDATABASE);
			}
			else
			{
				rsRegistrar.Execute("Update DefaultRegistrarNames Set FirstName = '" + modGlobalFunctions.EscapeQuotes(txtFirstName.Text) + "',LastName ='" + modGlobalFunctions.EscapeQuotes(txtLastName.Text) + "',MI = '" + modGlobalFunctions.EscapeQuotes(txtMiddleName.Text) + "', ClerkTown = '" + fecherFoundation.Strings.Trim(txtTown.Text) + "' where ID = " + cboRegistrar.Tag, modGNBas.DEFAULTDATABASE);
			}
			txtFirstName.Text = string.Empty;
			txtLastName.Text = string.Empty;
			txtMiddleName.Text = string.Empty;
			txtTown.Text = string.Empty;
			cboRegistrar.Focus();
			modGlobalRoutines.ShowWaitMessage(this);
			modGlobalRoutines.GetAllRegistrar(ref cboRegistrar);
			if (cboRegistrar.Items.Count > 0)
				cboRegistrar.SelectedIndex = 0;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmRegistrarNames_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmRegistrarNames_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmRegistrarNames_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRegistrarNames properties;
			//frmRegistrarNames.ScaleWidth	= 5940;
			//frmRegistrarNames.ScaleHeight	= 3900;
			//frmRegistrarNames.LinkTopic	= "Form1";
			//End Unmaped Properties
			// used in frmBirths activate event
			//modGNBas.Statics.boolSetDefaultRegistrar = true;
			modGlobalRoutines.GetAllRegistrar(ref cboRegistrar);
			if (cboRegistrar.Items.Count > 0)
				cboRegistrar.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			intDataChanged = FCConvert.ToInt16(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "cmdSave_Click");
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
			cmdExit_Click();
		}

		private void txtFirstName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtFirstName.Text = FCConvert.ToString(modGlobalRoutines.FixQuote(txtFirstName.Text));
		}

		private void txtLastName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtLastName.Text = FCConvert.ToString(modGlobalRoutines.FixQuote(txtLastName.Text));
		}

		private void txtMiddleName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtMiddleName.Text = FCConvert.ToString(modGlobalRoutines.FixQuote(txtMiddleName.Text));
		}

		private void txtTown_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtTown.Text = FCConvert.ToString(modGlobalRoutines.FixQuote(txtTown.Text));
		}
	}
}
