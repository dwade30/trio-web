﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmFees.
	/// </summary>
	partial class frmFees
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtAdditionalFeeDescription;
		public System.Collections.Generic.List<T2KBackFillDecimal> mebAdditionalFee;
		public System.Collections.Generic.List<T2KBackFillDecimal> mebFees;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblAdditionalFee;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtAdditionalFeeDescription_0;
		public fecherFoundation.FCTextBox txtAdditionalFeeDescription_3;
		public fecherFoundation.FCTextBox txtAdditionalFeeDescription_2;
		public fecherFoundation.FCTextBox txtAdditionalFeeDescription_1;
		public Global.T2KBackFillDecimal mebAdditionalFee_1;
		public Global.T2KBackFillDecimal mebAdditionalFee_2;
		public Global.T2KBackFillDecimal mebAdditionalFee_3;
		public Global.T2KBackFillDecimal mebAdditionalFee_0;
		public fecherFoundation.FCFrame Frame1;
		public Global.T2KBackFillDecimal mebFees_1;
		public Global.T2KBackFillDecimal mebFees_2;
		public Global.T2KBackFillDecimal mebFees_3;
		public Global.T2KBackFillDecimal mebFees_4;
		public Global.T2KBackFillDecimal mebFees_5;
		public Global.T2KBackFillDecimal mebFees_6;
		public Global.T2KBackFillDecimal mebFees_0;
		public Global.T2KBackFillDecimal mebFees_7;
		public Global.T2KBackFillDecimal mebFees_8;
		public Global.T2KBackFillDecimal mebAdditionalFee_4;
		public Global.T2KBackFillDecimal mebBirthSubState;
		public Global.T2KBackFillDecimal mebBurialState;
		public Global.T2KBackFillDecimal mebDeathState;
		public Global.T2KBackFillDecimal mebDeathSubState;
		public Global.T2KBackFillDecimal mebMarriageCertState;
		public Global.T2KBackFillDecimal mebMarriageCertSubState;
		public Global.T2KBackFillDecimal mebBirthState;
		public Global.T2KBackFillDecimal mebMarriageLicState;
		public Global.T2KBackFillDecimal mebMarriageLicSubState;
		public Global.T2KBackFillDecimal mebVeteranState;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblAdditionalFee_4;
		public fecherFoundation.FCLabel Label1_15;
		public fecherFoundation.FCLabel Label1_14;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_6;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtAdditionalFeeDescription_0 = new fecherFoundation.FCTextBox();
            this.txtAdditionalFeeDescription_3 = new fecherFoundation.FCTextBox();
            this.txtAdditionalFeeDescription_2 = new fecherFoundation.FCTextBox();
            this.txtAdditionalFeeDescription_1 = new fecherFoundation.FCTextBox();
            this.mebAdditionalFee_1 = new Global.T2KBackFillDecimal();
            this.mebAdditionalFee_2 = new Global.T2KBackFillDecimal();
            this.mebAdditionalFee_3 = new Global.T2KBackFillDecimal();
            this.mebAdditionalFee_0 = new Global.T2KBackFillDecimal();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.mebFees_1 = new Global.T2KBackFillDecimal();
            this.mebFees_2 = new Global.T2KBackFillDecimal();
            this.mebFees_3 = new Global.T2KBackFillDecimal();
            this.mebFees_4 = new Global.T2KBackFillDecimal();
            this.mebFees_5 = new Global.T2KBackFillDecimal();
            this.mebFees_6 = new Global.T2KBackFillDecimal();
            this.mebFees_0 = new Global.T2KBackFillDecimal();
            this.mebFees_7 = new Global.T2KBackFillDecimal();
            this.mebFees_8 = new Global.T2KBackFillDecimal();
            this.mebAdditionalFee_4 = new Global.T2KBackFillDecimal();
            this.mebBirthSubState = new Global.T2KBackFillDecimal();
            this.mebBurialState = new Global.T2KBackFillDecimal();
            this.mebDeathState = new Global.T2KBackFillDecimal();
            this.mebDeathSubState = new Global.T2KBackFillDecimal();
            this.mebMarriageCertState = new Global.T2KBackFillDecimal();
            this.mebMarriageCertSubState = new Global.T2KBackFillDecimal();
            this.mebBirthState = new Global.T2KBackFillDecimal();
            this.mebMarriageLicState = new Global.T2KBackFillDecimal();
            this.mebMarriageLicSubState = new Global.T2KBackFillDecimal();
            this.mebVeteranState = new Global.T2KBackFillDecimal();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblAdditionalFee_4 = new fecherFoundation.FCLabel();
            this.Label1_15 = new fecherFoundation.FCLabel();
            this.Label1_14 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebBirthSubState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebBurialState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDeathState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDeathSubState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebMarriageCertState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebMarriageCertSubState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebBirthState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebMarriageLicState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebMarriageLicSubState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebVeteranState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(1078, 520);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(131, 30);
            this.HeaderText.Text = "Clerk Fees";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtAdditionalFeeDescription_0);
            this.Frame2.Controls.Add(this.txtAdditionalFeeDescription_3);
            this.Frame2.Controls.Add(this.txtAdditionalFeeDescription_2);
            this.Frame2.Controls.Add(this.txtAdditionalFeeDescription_1);
            this.Frame2.Controls.Add(this.mebAdditionalFee_1);
            this.Frame2.Controls.Add(this.mebAdditionalFee_2);
            this.Frame2.Controls.Add(this.mebAdditionalFee_3);
            this.Frame2.Controls.Add(this.mebAdditionalFee_0);
            this.Frame2.Location = new System.Drawing.Point(30, 605);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(362, 240);
            this.Frame2.TabIndex = 36;
            this.Frame2.Text = "Additional Fees";
            // 
            // txtAdditionalFeeDescription_0
            // 
            this.txtAdditionalFeeDescription_0.AutoSize = false;
            this.txtAdditionalFeeDescription_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdditionalFeeDescription_0.LinkItem = null;
            this.txtAdditionalFeeDescription_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAdditionalFeeDescription_0.LinkTopic = null;
            this.txtAdditionalFeeDescription_0.Location = new System.Drawing.Point(20, 30);
            this.txtAdditionalFeeDescription_0.Name = "txtAdditionalFeeDescription_0";
            this.txtAdditionalFeeDescription_0.Size = new System.Drawing.Size(151, 40);
            this.txtAdditionalFeeDescription_0.TabIndex = 24;
            this.txtAdditionalFeeDescription_0.Text = "Additional Fee 1";
            // 
            // txtAdditionalFeeDescription_3
            // 
            this.txtAdditionalFeeDescription_3.AutoSize = false;
            this.txtAdditionalFeeDescription_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdditionalFeeDescription_3.LinkItem = null;
            this.txtAdditionalFeeDescription_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAdditionalFeeDescription_3.LinkTopic = null;
            this.txtAdditionalFeeDescription_3.Location = new System.Drawing.Point(20, 180);
            this.txtAdditionalFeeDescription_3.Name = "txtAdditionalFeeDescription_3";
            this.txtAdditionalFeeDescription_3.Size = new System.Drawing.Size(151, 40);
            this.txtAdditionalFeeDescription_3.TabIndex = 27;
            this.txtAdditionalFeeDescription_3.Text = "Additional Fee 4";
            // 
            // txtAdditionalFeeDescription_2
            // 
            this.txtAdditionalFeeDescription_2.AutoSize = false;
            this.txtAdditionalFeeDescription_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdditionalFeeDescription_2.LinkItem = null;
            this.txtAdditionalFeeDescription_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAdditionalFeeDescription_2.LinkTopic = null;
            this.txtAdditionalFeeDescription_2.Location = new System.Drawing.Point(20, 130);
            this.txtAdditionalFeeDescription_2.Name = "txtAdditionalFeeDescription_2";
            this.txtAdditionalFeeDescription_2.Size = new System.Drawing.Size(151, 40);
            this.txtAdditionalFeeDescription_2.TabIndex = 26;
            this.txtAdditionalFeeDescription_2.Text = "Additional Fee 3";
            // 
            // txtAdditionalFeeDescription_1
            // 
            this.txtAdditionalFeeDescription_1.AutoSize = false;
            this.txtAdditionalFeeDescription_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdditionalFeeDescription_1.LinkItem = null;
            this.txtAdditionalFeeDescription_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAdditionalFeeDescription_1.LinkTopic = null;
            this.txtAdditionalFeeDescription_1.Location = new System.Drawing.Point(20, 80);
            this.txtAdditionalFeeDescription_1.Name = "txtAdditionalFeeDescription_1";
            this.txtAdditionalFeeDescription_1.Size = new System.Drawing.Size(151, 40);
            this.txtAdditionalFeeDescription_1.TabIndex = 25;
            this.txtAdditionalFeeDescription_1.Text = "Additional Fee 2";
            // 
            // mebAdditionalFee_1
            // 
            this.mebAdditionalFee_1.Location = new System.Drawing.Point(191, 80);
            this.mebAdditionalFee_1.MaxLength = 8;
            this.mebAdditionalFee_1.Name = "mebAdditionalFee_1";
            this.mebAdditionalFee_1.Size = new System.Drawing.Size(151, 40);
            this.mebAdditionalFee_1.TabIndex = 21;
            this.mebAdditionalFee_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebAdditionalFee_2
            // 
            this.mebAdditionalFee_2.Location = new System.Drawing.Point(191, 130);
            this.mebAdditionalFee_2.MaxLength = 8;
            this.mebAdditionalFee_2.Name = "mebAdditionalFee_2";
            this.mebAdditionalFee_2.Size = new System.Drawing.Size(151, 40);
            this.mebAdditionalFee_2.TabIndex = 22;
            this.mebAdditionalFee_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebAdditionalFee_3
            // 
            this.mebAdditionalFee_3.Location = new System.Drawing.Point(191, 180);
            this.mebAdditionalFee_3.MaxLength = 8;
            this.mebAdditionalFee_3.Name = "mebAdditionalFee_3";
            this.mebAdditionalFee_3.Size = new System.Drawing.Size(151, 40);
            this.mebAdditionalFee_3.TabIndex = 23;
            this.mebAdditionalFee_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebAdditionalFee_0
            // 
            this.mebAdditionalFee_0.Location = new System.Drawing.Point(191, 30);
            this.mebAdditionalFee_0.MaxLength = 8;
            this.mebAdditionalFee_0.Name = "mebAdditionalFee_0";
            this.mebAdditionalFee_0.Size = new System.Drawing.Size(151, 40);
            this.mebAdditionalFee_0.TabIndex = 20;
            this.mebAdditionalFee_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.mebFees_1);
            this.Frame1.Controls.Add(this.mebFees_2);
            this.Frame1.Controls.Add(this.mebFees_3);
            this.Frame1.Controls.Add(this.mebFees_4);
            this.Frame1.Controls.Add(this.mebFees_5);
            this.Frame1.Controls.Add(this.mebFees_6);
            this.Frame1.Controls.Add(this.mebFees_0);
            this.Frame1.Controls.Add(this.mebFees_7);
            this.Frame1.Controls.Add(this.mebFees_8);
            this.Frame1.Controls.Add(this.mebAdditionalFee_4);
            this.Frame1.Controls.Add(this.mebBirthSubState);
            this.Frame1.Controls.Add(this.mebBurialState);
            this.Frame1.Controls.Add(this.mebDeathState);
            this.Frame1.Controls.Add(this.mebDeathSubState);
            this.Frame1.Controls.Add(this.mebMarriageCertState);
            this.Frame1.Controls.Add(this.mebMarriageCertSubState);
            this.Frame1.Controls.Add(this.mebBirthState);
            this.Frame1.Controls.Add(this.mebMarriageLicState);
            this.Frame1.Controls.Add(this.mebMarriageLicSubState);
            this.Frame1.Controls.Add(this.mebVeteranState);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.lblAdditionalFee_4);
            this.Frame1.Controls.Add(this.Label1_15);
            this.Frame1.Controls.Add(this.Label1_14);
            this.Frame1.Controls.Add(this.Label1_0);
            this.Frame1.Controls.Add(this.Label1_1);
            this.Frame1.Controls.Add(this.Label1_2);
            this.Frame1.Controls.Add(this.Label1_3);
            this.Frame1.Controls.Add(this.Label1_4);
            this.Frame1.Controls.Add(this.Label1_5);
            this.Frame1.Controls.Add(this.Label1_6);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(618, 565);
            this.Frame1.TabIndex = 28;
            this.Frame1.Text = "Fees";
            // 
            // mebFees_1
            // 
            this.mebFees_1.Location = new System.Drawing.Point(276, 105);
            this.mebFees_1.MaxLength = 8;
            this.mebFees_1.Name = "mebFees_1";
            this.mebFees_1.Size = new System.Drawing.Size(151, 40);
            this.mebFees_1.TabIndex = 2;
            this.mebFees_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_1.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebFees_2
            // 
            this.mebFees_2.Location = new System.Drawing.Point(276, 155);
            this.mebFees_2.MaxLength = 8;
            this.mebFees_2.Name = "mebFees_2";
            this.mebFees_2.Size = new System.Drawing.Size(151, 40);
            this.mebFees_2.TabIndex = 4;
            this.mebFees_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_2.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebFees_3
            // 
            this.mebFees_3.Location = new System.Drawing.Point(276, 205);
            this.mebFees_3.MaxLength = 8;
            this.mebFees_3.Name = "mebFees_3";
            this.mebFees_3.Size = new System.Drawing.Size(151, 40);
            this.mebFees_3.TabIndex = 6;
            this.mebFees_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_3.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebFees_4
            // 
            this.mebFees_4.Location = new System.Drawing.Point(276, 255);
            this.mebFees_4.MaxLength = 8;
            this.mebFees_4.Name = "mebFees_4";
            this.mebFees_4.Size = new System.Drawing.Size(151, 40);
            this.mebFees_4.TabIndex = 8;
            this.mebFees_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_4.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebFees_5
            // 
            this.mebFees_5.Location = new System.Drawing.Point(276, 355);
            this.mebFees_5.MaxLength = 8;
            this.mebFees_5.Name = "mebFees_5";
            this.mebFees_5.Size = new System.Drawing.Size(151, 40);
            this.mebFees_5.TabIndex = 12;
            this.mebFees_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_5.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebFees_6
            // 
            this.mebFees_6.Location = new System.Drawing.Point(276, 405);
            this.mebFees_6.MaxLength = 8;
            this.mebFees_6.Name = "mebFees_6";
            this.mebFees_6.Size = new System.Drawing.Size(151, 40);
            this.mebFees_6.TabIndex = 14;
            this.mebFees_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_6.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebFees_0
            // 
            this.mebFees_0.Location = new System.Drawing.Point(276, 55);
            this.mebFees_0.MaxLength = 8;
            this.mebFees_0.Name = "mebFees_0";
            this.mebFees_0.Size = new System.Drawing.Size(151, 40);
            this.mebFees_0.TabIndex = 0;
            this.mebFees_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_0.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebFees_7
            // 
            this.mebFees_7.Location = new System.Drawing.Point(276, 455);
            this.mebFees_7.MaxLength = 8;
            this.mebFees_7.Name = "mebFees_7";
            this.mebFees_7.Size = new System.Drawing.Size(151, 40);
            this.mebFees_7.TabIndex = 16;
            this.mebFees_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_7.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebFees_8
            // 
            this.mebFees_8.Location = new System.Drawing.Point(276, 505);
            this.mebFees_8.MaxLength = 8;
            this.mebFees_8.Name = "mebFees_8";
            this.mebFees_8.Size = new System.Drawing.Size(151, 40);
            this.mebFees_8.TabIndex = 18;
            this.mebFees_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.mebFees_8.TextChanged += new System.EventHandler(this.mebFees_Change);
            // 
            // mebAdditionalFee_4
            // 
            this.mebAdditionalFee_4.Location = new System.Drawing.Point(276, 305);
            this.mebAdditionalFee_4.MaxLength = 8;
            this.mebAdditionalFee_4.Name = "mebAdditionalFee_4";
            this.mebAdditionalFee_4.Size = new System.Drawing.Size(151, 40);
            this.mebAdditionalFee_4.TabIndex = 10;
            this.mebAdditionalFee_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebBirthSubState
            // 
            this.mebBirthSubState.Location = new System.Drawing.Point(447, 105);
            this.mebBirthSubState.MaxLength = 8;
            this.mebBirthSubState.Name = "mebBirthSubState";
            this.mebBirthSubState.Size = new System.Drawing.Size(151, 40);
            this.mebBirthSubState.TabIndex = 3;
            this.mebBirthSubState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebBurialState
            // 
            this.mebBurialState.Location = new System.Drawing.Point(447, 155);
            this.mebBurialState.MaxLength = 8;
            this.mebBurialState.Name = "mebBurialState";
            this.mebBurialState.Size = new System.Drawing.Size(151, 40);
            this.mebBurialState.TabIndex = 5;
            this.mebBurialState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebDeathState
            // 
            this.mebDeathState.Location = new System.Drawing.Point(447, 205);
            this.mebDeathState.MaxLength = 8;
            this.mebDeathState.Name = "mebDeathState";
            this.mebDeathState.Size = new System.Drawing.Size(151, 40);
            this.mebDeathState.TabIndex = 7;
            this.mebDeathState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebDeathSubState
            // 
            this.mebDeathSubState.Location = new System.Drawing.Point(447, 255);
            this.mebDeathSubState.MaxLength = 8;
            this.mebDeathSubState.Name = "mebDeathSubState";
            this.mebDeathSubState.Size = new System.Drawing.Size(151, 40);
            this.mebDeathSubState.TabIndex = 9;
            this.mebDeathSubState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebMarriageCertState
            // 
            this.mebMarriageCertState.Location = new System.Drawing.Point(447, 355);
            this.mebMarriageCertState.MaxLength = 8;
            this.mebMarriageCertState.Name = "mebMarriageCertState";
            this.mebMarriageCertState.Size = new System.Drawing.Size(151, 40);
            this.mebMarriageCertState.TabIndex = 13;
            this.mebMarriageCertState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebMarriageCertSubState
            // 
            this.mebMarriageCertSubState.Location = new System.Drawing.Point(447, 405);
            this.mebMarriageCertSubState.MaxLength = 8;
            this.mebMarriageCertSubState.Name = "mebMarriageCertSubState";
            this.mebMarriageCertSubState.Size = new System.Drawing.Size(151, 40);
            this.mebMarriageCertSubState.TabIndex = 15;
            this.mebMarriageCertSubState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebBirthState
            // 
            this.mebBirthState.Location = new System.Drawing.Point(447, 55);
            this.mebBirthState.MaxLength = 8;
            this.mebBirthState.Name = "mebBirthState";
            this.mebBirthState.Size = new System.Drawing.Size(151, 40);
            this.mebBirthState.TabIndex = 1;
            this.mebBirthState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebMarriageLicState
            // 
            this.mebMarriageLicState.Location = new System.Drawing.Point(447, 455);
            this.mebMarriageLicState.MaxLength = 8;
            this.mebMarriageLicState.Name = "mebMarriageLicState";
            this.mebMarriageLicState.Size = new System.Drawing.Size(151, 40);
            this.mebMarriageLicState.TabIndex = 17;
            this.mebMarriageLicState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebMarriageLicSubState
            // 
            this.mebMarriageLicSubState.Location = new System.Drawing.Point(447, 505);
            this.mebMarriageLicSubState.MaxLength = 8;
            this.mebMarriageLicSubState.Name = "mebMarriageLicSubState";
            this.mebMarriageLicSubState.Size = new System.Drawing.Size(151, 40);
            this.mebMarriageLicSubState.TabIndex = 19;
            this.mebMarriageLicSubState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // mebVeteranState
            // 
            this.mebVeteranState.Location = new System.Drawing.Point(447, 305);
            this.mebVeteranState.MaxLength = 8;
            this.mebVeteranState.Name = "mebVeteranState";
            this.mebVeteranState.Size = new System.Drawing.Size(151, 40);
            this.mebVeteranState.TabIndex = 11;
            this.mebVeteranState.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(447, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(39, 15);
            this.Label3.TabIndex = 41;
            this.Label3.Text = "STATE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(277, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(65, 15);
            this.Label2.TabIndex = 40;
            this.Label2.Text = "MUNICIPAL";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblAdditionalFee_4
            // 
            this.lblAdditionalFee_4.Location = new System.Drawing.Point(20, 319);
            this.lblAdditionalFee_4.Name = "lblAdditionalFee_4";
            this.lblAdditionalFee_4.Size = new System.Drawing.Size(97, 17);
            this.lblAdditionalFee_4.TabIndex = 39;
            this.lblAdditionalFee_4.Text = "VETERANS COPY";
            // 
            // Label1_15
            // 
            this.Label1_15.Location = new System.Drawing.Point(20, 519);
            this.Label1_15.Name = "Label1_15";
            this.Label1_15.Size = new System.Drawing.Size(223, 17);
            this.Label1_15.TabIndex = 38;
            this.Label1_15.Text = "REPLACEMENT MARRIAGE LICENSE FEE";
            // 
            // Label1_14
            // 
            this.Label1_14.Location = new System.Drawing.Point(20, 469);
            this.Label1_14.Name = "Label1_14";
            this.Label1_14.Size = new System.Drawing.Size(169, 17);
            this.Label1_14.TabIndex = 37;
            this.Label1_14.Text = "NEW MARRIAGE LICENSE FEE";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(20, 69);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(181, 17);
            this.Label1_0.TabIndex = 29;
            this.Label1_0.Text = "NEW BIRTH CERTIFICATE FEE";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(20, 119);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(239, 17);
            this.Label1_1.TabIndex = 30;
            this.Label1_1.Text = "MULTI COPIES BIRTH FEE (SAME DAY)";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(20, 169);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(110, 17);
            this.Label1_2.TabIndex = 31;
            this.Label1_2.Text = "BURIAL PERMIT FEE";
            this.Label1_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(20, 219);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(213, 17);
            this.Label1_3.TabIndex = 32;
            this.Label1_3.Text = "NEW DEATH CERTIFICATE FEE";
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(20, 269);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(221, 17);
            this.Label1_4.TabIndex = 33;
            this.Label1_4.Text = "MULTI COPIES DEATH FEE (SAME DAY) ";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 369);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(223, 17);
            this.Label1_5.TabIndex = 34;
            this.Label1_5.Text = "NEW MARRIAGE CERTIFICATE FEE";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(20, 419);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(213, 17);
            this.Label1_6.TabIndex = 35;
            this.Label1_6.Text = "MULTI COPIES CERT. FEE (SAME DAY)";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Fields";
            this.mnuClear.Visible = false;
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            this.mnuSP2.Visible = false;
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 2;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                   ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 3;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit             ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 4;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(307, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmFees
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(683, 688);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFees";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Clerk Fees";
            this.Load += new System.EventHandler(this.frmFees_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmFees_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFees_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFees_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAdditionalFee_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebBirthSubState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebBurialState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDeathState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDeathSubState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebMarriageCertState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebMarriageCertSubState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebBirthState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebMarriageLicState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebMarriageLicSubState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebVeteranState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
