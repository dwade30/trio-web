﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDeathCertWordDoc.
	/// </summary>
	partial class rptDeathCertWordDoc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDeathCertWordDoc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtFullName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateOfDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCertNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlaceOfDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateofBirth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSSN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSex = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMothersMaidenName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNameOfInformant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetAndNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCityTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCause1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCause2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCause3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysician = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePhysicianSigned = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCemetery = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClerk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCemeteryCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClerkCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCemeteryState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClerkState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.txtFullName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateofBirth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFathersName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMothersMaidenName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameOfInformant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetAndNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCause1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCause2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCause3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysician)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePhysicianSigned)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCemetery)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCemeteryCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerkCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCemeteryState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerkState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtFullName,
				this.txtDateOfDeath,
				this.txtCertNumber,
				this.txtPlaceOfDeath,
				this.txtDateofBirth,
				this.txtSSN,
				this.txtSex,
				this.txtFathersName,
				this.txtMothersMaidenName,
				this.txtNameOfInformant,
				this.txtAddress,
				this.txtStreetAndNumber,
				this.txtCityTown,
				this.txtCause1,
				this.txtCause2,
				this.txtCause3,
				this.txtPhysician,
				this.txtDatePhysicianSigned,
				this.txtCemetery,
				this.txtClerk,
				this.txtCemeteryCity,
				this.txtClerkCity,
				this.txtCemeteryState,
				this.txtClerkState,
				this.SubReport1
			});
			this.Detail.Height = 9.96875F;
			this.Detail.Name = "Detail";
			// 
			// txtFullName
			// 
			this.txtFullName.Height = 0.1979167F;
			this.txtFullName.Left = 1.15625F;
			this.txtFullName.Name = "txtFullName";
			this.txtFullName.Text = "Full Name";
			this.txtFullName.Top = 2.03125F;
			this.txtFullName.Width = 2.770833F;
			// 
			// txtDateOfDeath
			// 
			this.txtDateOfDeath.Height = 0.1979167F;
			this.txtDateOfDeath.Left = 4.65625F;
			this.txtDateOfDeath.Name = "txtDateOfDeath";
			this.txtDateOfDeath.Text = "00/00/0000";
			this.txtDateOfDeath.Top = 2.03125F;
			this.txtDateOfDeath.Width = 1.958333F;
			// 
			// txtCertNumber
			// 
			this.txtCertNumber.Height = 0.1979167F;
			this.txtCertNumber.Left = 6.020833F;
			this.txtCertNumber.Name = "txtCertNumber";
			this.txtCertNumber.Text = "Cert Number";
			this.txtCertNumber.Top = 1.135417F;
			this.txtCertNumber.Width = 1.541667F;
			// 
			// txtPlaceOfDeath
			// 
			this.txtPlaceOfDeath.Height = 0.1979167F;
			this.txtPlaceOfDeath.Left = 1.15625F;
			this.txtPlaceOfDeath.Name = "txtPlaceOfDeath";
			this.txtPlaceOfDeath.Text = "Place of death";
			this.txtPlaceOfDeath.Top = 2.604167F;
			this.txtPlaceOfDeath.Width = 2.770833F;
			// 
			// txtDateOfBirth
			// 
			this.txtDateofBirth.Height = 0.1979167F;
			this.txtDateofBirth.Left = 4.65625F;
			this.txtDateofBirth.Name = "txtDateOfBirth";
			this.txtDateofBirth.Text = "00/00/0000";
			this.txtDateofBirth.Top = 2.604167F;
			this.txtDateofBirth.Width = 1.958333F;
			// 
			// txtSSN
			// 
			this.txtSSN.Height = 0.1979167F;
			this.txtSSN.Left = 1.15625F;
			this.txtSSN.Name = "txtSSN";
			this.txtSSN.Text = "000-00-0000";
			this.txtSSN.Top = 3.177083F;
			this.txtSSN.Width = 2.770833F;
			// 
			// txtSex
			// 
			this.txtSex.Height = 0.1979167F;
			this.txtSex.Left = 4.65625F;
			this.txtSex.Name = "txtSex";
			this.txtSex.Text = "Sex";
			this.txtSex.Top = 3.177083F;
			this.txtSex.Width = 1.958333F;
			// 
			// txtFathersName
			// 
			this.txtFathersName.Height = 0.1979167F;
			this.txtFathersName.Left = 1.15625F;
			this.txtFathersName.Name = "txtFathersName";
			this.txtFathersName.Text = "Father\'s Name";
			this.txtFathersName.Top = 3.625F;
			this.txtFathersName.Width = 2.770833F;
			// 
			// txtMothersMaidenName
			// 
			this.txtMothersMaidenName.Height = 0.1979167F;
			this.txtMothersMaidenName.Left = 4.65625F;
			this.txtMothersMaidenName.Name = "txtMothersMaidenName";
			this.txtMothersMaidenName.Text = "Mother\'s maiden name";
			this.txtMothersMaidenName.Top = 3.625F;
			this.txtMothersMaidenName.Width = 3.020833F;
			// 
			// txtNameOfInformant
			// 
			this.txtNameOfInformant.Height = 0.1979167F;
			this.txtNameOfInformant.Left = 1.15625F;
			this.txtNameOfInformant.Name = "txtNameOfInformant";
			this.txtNameOfInformant.Text = "Informant";
			this.txtNameOfInformant.Top = 4.197917F;
			this.txtNameOfInformant.Width = 2.770833F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.4479167F;
			this.txtAddress.Left = 4.65625F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Text = "Address";
			this.txtAddress.Top = 4.197917F;
			this.txtAddress.Width = 3.020833F;
			// 
			// txtStreetAndNumber
			// 
			this.txtStreetAndNumber.Height = 0.1979167F;
			this.txtStreetAndNumber.Left = 1.15625F;
			this.txtStreetAndNumber.Name = "txtStreetAndNumber";
			this.txtStreetAndNumber.Text = "Street";
			this.txtStreetAndNumber.Top = 5.020833F;
			this.txtStreetAndNumber.Width = 2.770833F;
			// 
			// txtCityTown
			// 
			this.txtCityTown.Height = 0.1979167F;
			this.txtCityTown.Left = 4.65625F;
			this.txtCityTown.Name = "txtCityTown";
			this.txtCityTown.Text = "City";
			this.txtCityTown.Top = 5.020833F;
			this.txtCityTown.Width = 3.0625F;
			// 
			// txtCause1
			// 
			this.txtCause1.Height = 0.1979167F;
			this.txtCause1.Left = 1.15625F;
			this.txtCause1.Name = "txtCause1";
			this.txtCause1.Text = "Cause number 1";
			this.txtCause1.Top = 5.28125F;
			this.txtCause1.Width = 5.708333F;
			// 
			// txtCause2
			// 
			this.txtCause2.Height = 0.1979167F;
			this.txtCause2.Left = 1.15625F;
			this.txtCause2.Name = "txtCause2";
			this.txtCause2.Text = "cause number 2";
			this.txtCause2.Top = 5.604167F;
			this.txtCause2.Width = 5.708333F;
			// 
			// txtCause3
			// 
			this.txtCause3.Height = 0.1979167F;
			this.txtCause3.Left = 1.15625F;
			this.txtCause3.Name = "txtCause3";
			this.txtCause3.Text = "cause number 3";
			this.txtCause3.Top = 5.927083F;
			this.txtCause3.Width = 5.708333F;
			// 
			// txtPhysician
			// 
			this.txtPhysician.Height = 0.1979167F;
			this.txtPhysician.Left = 1.15625F;
			this.txtPhysician.Name = "txtPhysician";
			this.txtPhysician.Text = "Physician";
			this.txtPhysician.Top = 6.75F;
			this.txtPhysician.Width = 4.333333F;
			// 
			// txtDatePhysicianSigned
			// 
			this.txtDatePhysicianSigned.Height = 0.1979167F;
			this.txtDatePhysicianSigned.Left = 6.5F;
			this.txtDatePhysicianSigned.Name = "txtDatePhysicianSigned";
			this.txtDatePhysicianSigned.Text = "00/00/0000";
			this.txtDatePhysicianSigned.Top = 6.75F;
			this.txtDatePhysicianSigned.Width = 1.020833F;
			// 
			// txtCemetery
			// 
			this.txtCemetery.Height = 0.1979167F;
			this.txtCemetery.Left = 1.15625F;
			this.txtCemetery.Name = "txtCemetery";
			this.txtCemetery.Text = "Cemetery";
			this.txtCemetery.Top = 7.510417F;
			this.txtCemetery.Width = 2.770833F;
			// 
			// txtClerk
			// 
			this.txtClerk.Height = 0.1979167F;
			this.txtClerk.Left = 1.15625F;
			this.txtClerk.Name = "txtClerk";
			this.txtClerk.Text = "Clerk";
			this.txtClerk.Top = 8.208333F;
			this.txtClerk.Width = 2.770833F;
			// 
			// txtCemeteryCity
			// 
			this.txtCemeteryCity.Height = 0.1979167F;
			this.txtCemeteryCity.Left = 4.65625F;
			this.txtCemeteryCity.Name = "txtCemeteryCity";
			this.txtCemeteryCity.Text = "City";
			this.txtCemeteryCity.Top = 7.541667F;
			this.txtCemeteryCity.Width = 1.770833F;
			// 
			// txtClerkCity
			// 
			this.txtClerkCity.Height = 0.1979167F;
			this.txtClerkCity.Left = 4.65625F;
			this.txtClerkCity.Name = "txtClerkCity";
			this.txtClerkCity.Text = "City";
			this.txtClerkCity.Top = 8.239583F;
			this.txtClerkCity.Width = 1.770833F;
			// 
			// txtCemeteryState
			// 
			this.txtCemeteryState.Height = 0.3854167F;
			this.txtCemeteryState.Left = 6.5F;
			this.txtCemeteryState.Name = "txtCemeteryState";
			this.txtCemeteryState.Text = "State";
			this.txtCemeteryState.Top = 7.541667F;
			this.txtCemeteryState.Width = 1.1875F;
			// 
			// txtClerkState
			// 
			this.txtClerkState.Height = 0.3854167F;
			this.txtClerkState.Left = 6.5F;
			this.txtClerkState.Name = "txtClerkState";
			this.txtClerkState.Text = "State";
			this.txtClerkState.Top = 8.239583F;
			this.txtClerkState.Width = 1.1875F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0.25F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 9.105556F;
			this.SubReport1.Width = 7F;
			// 
			// rptDeathCertWordDoc
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.739583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtFullName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateofBirth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSSN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFathersName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMothersMaidenName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameOfInformant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetAndNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCause1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCause2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCause3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysician)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePhysicianSigned)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCemetery)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCemeteryCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerkCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCemeteryState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerkState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateOfDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCertNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlaceOfDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateofBirth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSSN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSex;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMothersMaidenName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNameOfInformant;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetAndNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCause1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCause2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCause3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysician;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePhysicianSigned;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCemetery;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClerk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCemeteryCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClerkCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCemeteryState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClerkState;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
	}
}
