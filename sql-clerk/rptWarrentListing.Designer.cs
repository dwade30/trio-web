﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptWarrentListing.
	/// </summary>
	partial class rptWarrentListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptWarrentListing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DLG1 = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTagNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDogName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLocation,
				this.txtOwnerName,
				this.txtTagNumber,
				this.txtDatePaid,
				this.txtAddress,
				this.txtDogName,
				this.txtLine
			});
			this.Detail.Height = 0.375F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Field2,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field8,
				this.DLG1,
				this.txtCaption,
				this.txtCaption2,
				this.txtTown,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Line1
			});
			this.PageHeader.Height = 0.7916667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.2083333F;
			this.Field1.Left = 0.40625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field1.Text = "Date PD";
			this.Field1.Top = 0.5833333F;
			this.Field1.Visible = false;
			this.Field1.Width = 0.625F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1666667F;
			this.Field2.Left = 0.9375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field2.Text = "Owners Name";
			this.Field2.Top = 0.5833333F;
			this.Field2.Width = 1.3125F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.2083333F;
			this.Field4.Left = 2.25F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field4.Text = "Address/Phone";
			this.Field4.Top = 0.5833333F;
			this.Field4.Width = 1.25F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.2083333F;
			this.Field5.Left = 5.0625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field5.Text = "Location";
			this.Field5.Top = 0.5833333F;
			this.Field5.Width = 0.875F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.2083333F;
			this.Field6.Left = 6.4375F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field6.Text = "Dog Name";
			this.Field6.Top = 0.5833333F;
			this.Field6.Width = 0.8125F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.2083333F;
			this.Field8.Left = 7.28125F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field8.Text = "Tag #";
			this.Field8.Top = 0.5833333F;
			this.Field8.Width = 0.5F;
			// 
			// DLG1
			// 
			this.DLG1.Height = 0.3125F;
			this.DLG1.Left = 0F;
			this.DLG1.Name = "DLG1";
			this.DLG1.Top = 0F;
			this.DLG1.Visible = false;
			this.DLG1.Width = 0.875F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1666667F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = null;
			this.txtCaption.Top = 0.04166667F;
			this.txtCaption.Width = 7.78125F;
			// 
			// txtCaption2
			// 
			this.txtCaption2.Height = 0.1666667F;
			this.txtCaption2.Left = 0.09375F;
			this.txtCaption2.Name = "txtCaption2";
			this.txtCaption2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.txtCaption2.Text = null;
			this.txtCaption2.Top = 0.25F;
			this.txtCaption2.Width = 7.75F;
			// 
			// txtTown
			// 
			this.txtTown.Height = 0.1666667F;
			this.txtTown.Left = 0.03125F;
			this.txtTown.Name = "txtTown";
			this.txtTown.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTown.Text = null;
			this.txtTown.Top = 0.04166667F;
			this.txtTown.Width = 1.375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0.03125F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 6.5F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 6.5F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.09375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.75F;
			this.Line1.Width = 7.75F;
			this.Line1.X1 = 0.09375F;
			this.Line1.X2 = 7.84375F;
			this.Line1.Y1 = 0.75F;
			this.Line1.Y2 = 0.75F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.2083333F;
			this.txtLocation.Left = 5.0625F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.1666667F;
			this.txtLocation.Width = 1.3125F;
			// 
			// txtOwnerName
			// 
			this.txtOwnerName.Height = 0.2083333F;
			this.txtOwnerName.Left = 0.9375F;
			this.txtOwnerName.Name = "txtOwnerName";
			this.txtOwnerName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtOwnerName.Text = null;
			this.txtOwnerName.Top = 0.1666667F;
			this.txtOwnerName.Width = 1.25F;
			// 
			// txtTagNumber
			// 
			this.txtTagNumber.Height = 0.2083333F;
			this.txtTagNumber.Left = 7.28125F;
			this.txtTagNumber.Name = "txtTagNumber";
			this.txtTagNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTagNumber.Text = null;
			this.txtTagNumber.Top = 0.1666667F;
			this.txtTagNumber.Width = 0.59375F;
			// 
			// txtDatePaid
			// 
			this.txtDatePaid.Height = 0.2083333F;
			this.txtDatePaid.Left = 0.40625F;
			this.txtDatePaid.Name = "txtDatePaid";
			this.txtDatePaid.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDatePaid.Text = null;
			this.txtDatePaid.Top = 0.1666667F;
			this.txtDatePaid.Width = 0.4375F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.2083333F;
			this.txtAddress.Left = 2.25F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 0.1666667F;
			this.txtAddress.Width = 2.75F;
			// 
			// txtDogName
			// 
			this.txtDogName.Height = 0.2083333F;
			this.txtDogName.Left = 6.4375F;
			this.txtDogName.Name = "txtDogName";
			this.txtDogName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDogName.Text = null;
			this.txtDogName.Top = 0.1666667F;
			this.txtDogName.Width = 0.75F;
			// 
			// txtLine
			// 
			this.txtLine.Height = 0.2083333F;
			this.txtLine.Left = 0.125F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtLine.Text = null;
			this.txtLine.Top = 0.1666667F;
			this.txtLine.Width = 0.25F;
			// 
			// rptWarrentListing
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.2777778F;
			this.PageSettings.Margins.Right = 0.1388889F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.916667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl DLG1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
