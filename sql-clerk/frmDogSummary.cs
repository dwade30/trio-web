﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDogSummary : BaseForm
	{
		public frmDogSummary()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDogSummary InstancePtr
		{
			get
			{
				return (frmDogSummary)Sys.GetInstance(typeof(frmDogSummary));
			}
		}

		protected frmDogSummary _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmDogSummary_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDogSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDogSummary properties;
			//frmDogSummary.FillStyle	= 0;
			//frmDogSummary.ScaleWidth	= 3885;
			//frmDogSummary.ScaleHeight	= 2235;
			//frmDogSummary.LinkTopic	= "Form2";
			//frmDogSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			FillCombo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillCombo()
		{
			int lngYear;
			// vbPorter upgrade warning: intMonth As int	OnWriteFCConvert.ToInt32(
			int intMonth;
			lngYear = DateTime.Today.Year;
			intMonth = DateTime.Today.Month;
			cmbYear.AddItem(FCConvert.ToString(lngYear));
			cmbYear.AddItem(FCConvert.ToString(lngYear - 1));
			cmbYear.AddItem(FCConvert.ToString(lngYear - 2));
			cmbYear.SelectedIndex = 0;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			bool boolSummary = false;
			int lngYear;
			lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbYear.Text)));
			if (cmbDetail.Text == "Detail")
			{
				boolSummary = false;
			}
			else
			{
				boolSummary = true;
			}
			rptDogLicenseSummary.InstancePtr.Init(ref lngYear, ref boolSummary);
		}
	}
}
