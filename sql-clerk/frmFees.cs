//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmFees : BaseForm
	{
		public frmFees()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			txtAdditionalFeeDescription = new System.Collections.Generic.List<FCTextBox>();
			txtAdditionalFeeDescription.AddControlArrayElement(txtAdditionalFeeDescription_0, 0);
			txtAdditionalFeeDescription.AddControlArrayElement(txtAdditionalFeeDescription_1, 1);
			txtAdditionalFeeDescription.AddControlArrayElement(txtAdditionalFeeDescription_2, 2);
			txtAdditionalFeeDescription.AddControlArrayElement(txtAdditionalFeeDescription_3, 3);
			lblAdditionalFee = new System.Collections.Generic.List<FCLabel>();
			lblAdditionalFee.AddControlArrayElement(lblAdditionalFee_4, 0);
			Label1 = new System.Collections.Generic.List<FCLabel>();
			Label1.AddControlArrayElement(Label1_0, 0);
			Label1.AddControlArrayElement(Label1_1, 1);
			Label1.AddControlArrayElement(Label1_2, 2);
			Label1.AddControlArrayElement(Label1_3, 3);
			Label1.AddControlArrayElement(Label1_4, 4);
			Label1.AddControlArrayElement(Label1_5, 5);
			Label1.AddControlArrayElement(Label1_6, 6);
			Label1.AddControlArrayElement(Label1_14, 7);
			Label1.AddControlArrayElement(Label1_15, 8);
            mebAdditionalFee = new System.Collections.Generic.List<T2KBackFillDecimal>();
            mebAdditionalFee.AddControlArrayElement(mebAdditionalFee_0, 0);
            mebAdditionalFee.AddControlArrayElement(mebAdditionalFee_1, 1);
            mebAdditionalFee.AddControlArrayElement(mebAdditionalFee_2, 2);
            mebAdditionalFee.AddControlArrayElement(mebAdditionalFee_3, 3);
            mebAdditionalFee.AddControlArrayElement(mebAdditionalFee_4, 4);
            mebFees = new System.Collections.Generic.List<T2KBackFillDecimal>();
            mebFees.AddControlArrayElement(mebFees_0, 0);
            mebFees.AddControlArrayElement(mebFees_1, 1);
            mebFees.AddControlArrayElement(mebFees_2, 2);
            mebFees.AddControlArrayElement(mebFees_3, 3);
            mebFees.AddControlArrayElement(mebFees_4, 4);
            mebFees.AddControlArrayElement(mebFees_5, 5);
            mebFees.AddControlArrayElement(mebFees_6, 6);
            mebFees.AddControlArrayElement(mebFees_7, 7);
            mebFees.AddControlArrayElement(mebFees_8, 8);
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFees InstancePtr
		{
			get
			{
				return (frmFees)Sys.GetInstance(typeof(frmFees));
			}
		}

		protected frmFees _InstancePtr = null;
		//=========================================================
		public int intDataChanged;

		private void frmFees_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
			if (KeyCode == Keys.Return)
				Support.SendKeys("{TAB}", false);
		}

		private void frmFees_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFees_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFees properties;
			//frmFees.ScaleWidth	= 9075;
			//frmFees.ScaleHeight	= 7095;
			//frmFees.LinkTopic	= "Form1";
			//frmFees.LockControls	= -1  'True;
			//End Unmaped Properties
			FillFees();
			intDataChanged = 0;
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modDirtyForm.SaveChanges(intDataChanged, this, "SaveFees");
			//MDIParent.InstancePtr.Show();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void FillFees()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "FillFees";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsFees = new clsDRWrapper();
				rsFees.OpenRecordset("SELECT * FROM ClerkFees;", modGNBas.DEFAULTDATABASE);
				if (rsFees.EndOfFile())
					return;
				mebFees[0].SelectedText = Strings.Format(rsFees.Get_Fields_Double("NewBirthFee"), "##0.00");
				mebFees[1].SelectedText = Strings.Format(rsFees.Get_Fields_Double("ReplacementBirthFee"), "##0.00");
				mebFees[3].SelectedText = Strings.Format(rsFees.Get_Fields_Double("NewDeathFee"), "##0.00");
				mebFees[4].SelectedText = Strings.Format(rsFees.Get_Fields_Double("ReplacementDeathFee"), "##0.00");
				mebFees[2].SelectedText = Strings.Format(rsFees.Get_Fields_Double("BurialPermitFee"), "##0.00");
				mebFees[5].SelectedText = Strings.Format(rsFees.Get_Fields_Double("NewMarriageCertificateFee"), "##0.00");
				mebFees[6].SelectedText = Strings.Format(rsFees.Get_Fields_Double("ReplacementMarriageCertificateFee"), "##0.00");
				mebFees[7].SelectedText = Strings.Format(rsFees.Get_Fields_Double("NewMarriageLicenseFee"), "##0.00");
				mebFees[8].SelectedText = Strings.Format(rsFees.Get_Fields_Double("ReplacementMarriageLicenseFee"), "##0.00");
				mebBirthState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("BirthStateFee"))), "##0.00");
				mebBirthSubState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("BirthReplacementStateFee"))), "##0.00");
				mebDeathState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("DeathStateFee"))), "##0.00");
				mebDeathSubState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("DeathReplacementStateFee"))), "##0.00");
				mebBurialState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("DispositionStateFee"))), "##0.00");
				mebMarriageCertState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("MarriageCertStateFee"))), "##0.00");
				mebMarriageCertSubState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("MarriageCertReplacementStateFee"))), "##0.00");
				mebMarriageLicState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("MarriageLicStateFee"))), "##0.00");
				mebMarriageLicSubState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("MarriageLicReplacementStateFee"))), "##0.00");
				mebVeteranState.SelectedText = Strings.Format(FCConvert.ToString(Conversion.Val(rsFees.Get_Fields_Double("VetDeathStateFee"))), "##0.00");
				if (fecherFoundation.Strings.Trim(rsFees.Get_Fields_String("AdditionalFeeCaption1") + " ") != string.Empty)
				{
					txtAdditionalFeeDescription[0].Text = FCConvert.ToString(rsFees.Get_Fields_String("AdditionalFeeCaption1"));
					txtAdditionalFeeDescription[1].Text = FCConvert.ToString(rsFees.Get_Fields_String("AdditionalFeeCaption2"));
					txtAdditionalFeeDescription[2].Text = FCConvert.ToString(rsFees.Get_Fields_String("AdditionalFeeCaption3"));
					txtAdditionalFeeDescription[3].Text = FCConvert.ToString(rsFees.Get_Fields_String("AdditionalFeeCaption4"));
				}
				mebAdditionalFee[0].SelectedText = Strings.Format(rsFees.Get_Fields_Double("AdditionalFee1"), "##0.00");
				mebAdditionalFee[1].SelectedText = Strings.Format(rsFees.Get_Fields_Double("AdditionalFee2"), "##0.00");
				mebAdditionalFee[2].SelectedText = Strings.Format(rsFees.Get_Fields_Double("AdditionalFee3"), "##0.00");
				mebAdditionalFee[3].SelectedText = Strings.Format(rsFees.Get_Fields_Double("AdditionalFee4"), "##0.00");
				mebAdditionalFee[4].SelectedText = Strings.Format(rsFees.Get_Fields_Double("AdditionalFee5"), "##0.00");
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		public void SaveFees()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveFees";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				clsDRWrapper rsFees = new clsDRWrapper();
				rsFees.OpenRecordset("SELECT * FROM ClerkFees;", modGNBas.DEFAULTDATABASE);
				if (rsFees.EndOfFile())
				{
					rsFees.AddNew();
				}
				else
				{
					rsFees.Edit();
				}
				rsFees.Set_Fields("NewBirthFee", FCConvert.ToString(Conversion.Val(mebFees[0].Text)));
				rsFees.Set_Fields("ReplacementBirthFee", FCConvert.ToString(Conversion.Val(mebFees[1].Text)));
				rsFees.Set_Fields("NewDeathFee", FCConvert.ToString(Conversion.Val(mebFees[3].Text)));
				rsFees.Set_Fields("ReplacementDeathFee", FCConvert.ToString(Conversion.Val(mebFees[4].Text)));
				rsFees.Set_Fields("BurialPermitFee", FCConvert.ToString(Conversion.Val(mebFees[2].Text)));
				rsFees.Set_Fields("NewMarriageCertificateFee", FCConvert.ToString(Conversion.Val(mebFees[5].Text)));
				rsFees.Set_Fields("ReplacementMarriageCertificateFee", FCConvert.ToString(Conversion.Val(mebFees[6].Text)));
				rsFees.Set_Fields("NewMarriageLicenseFee", FCConvert.ToString(Conversion.Val(mebFees[7].Text)));
				rsFees.Set_Fields("ReplacementMarriageLicenseFee", FCConvert.ToString(Conversion.Val(mebFees[8].Text)));
				rsFees.Set_Fields("BirthStateFee", FCConvert.ToString(Conversion.Val(mebBirthState.Text)));
				rsFees.Set_Fields("BirthReplacementStateFee", FCConvert.ToString(Conversion.Val(mebBirthSubState.Text)));
				rsFees.Set_Fields("DeathStateFee", FCConvert.ToString(Conversion.Val(mebDeathState.Text)));
				rsFees.Set_Fields("DeathReplacementStateFee", FCConvert.ToString(Conversion.Val(mebDeathSubState.Text)));
				rsFees.Set_Fields("DispositionStateFee", FCConvert.ToString(Conversion.Val(mebBurialState.Text)));
				rsFees.Set_Fields("MarriageCertStateFee", FCConvert.ToString(Conversion.Val(mebMarriageCertState.Text)));
				rsFees.Set_Fields("MarriageCertReplacementStateFee", FCConvert.ToString(Conversion.Val(mebMarriageCertSubState.Text)));
				rsFees.Set_Fields("MarriageLicStateFee", FCConvert.ToString(Conversion.Val(mebMarriageLicState.Text)));
				rsFees.Set_Fields("MarriageLicReplacementStateFee", FCConvert.ToString(Conversion.Val(mebMarriageLicSubState.Text)));
				rsFees.Set_Fields("VetDeathStateFee", FCConvert.ToString(Conversion.Val(mebVeteranState.Text)));
				rsFees.Set_Fields("AdditionalFeeCaption1", txtAdditionalFeeDescription[0].Text);
				rsFees.Set_Fields("AdditionalFeeCaption2", txtAdditionalFeeDescription[1].Text);
				rsFees.Set_Fields("AdditionalFeeCaption3", txtAdditionalFeeDescription[2].Text);
				rsFees.Set_Fields("AdditionalFeeCaption4", txtAdditionalFeeDescription[3].Text);
				rsFees.Set_Fields("AdditionalFee1", FCConvert.ToString(Conversion.Val(mebAdditionalFee[0].Text)));
				rsFees.Set_Fields("AdditionalFee2", FCConvert.ToString(Conversion.Val(mebAdditionalFee[1].Text)));
				rsFees.Set_Fields("AdditionalFee3", FCConvert.ToString(Conversion.Val(mebAdditionalFee[2].Text)));
				rsFees.Set_Fields("AdditionalFee4", FCConvert.ToString(Conversion.Val(mebAdditionalFee[3].Text)));
				rsFees.Set_Fields("AdditionalFee5", FCConvert.ToString(Conversion.Val(mebAdditionalFee[4].Text)));
				rsFees.Update();
				modDogs.GetClerkFees();
				intDataChanged = 0;
				MessageBox.Show("Records Saved successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mebAdditionalFee_ClickEvent(int Index, object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void mebAdditionalFee_ClickEvent(object sender, System.EventArgs e)
		{
			int index = mebAdditionalFee.GetIndex((Global.T2KBackFillDecimal)sender);
			mebAdditionalFee_ClickEvent(index, sender, e);
		}

		private void mebFees_Change(int Index, object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void mebFees_Change(object sender, System.EventArgs e)
		{
			int index = mebFees.GetIndex((Global.T2KBackFillDecimal)sender);
			mebFees_Change(index, sender, e);
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			/* Control ControlName = new Control(); */
			foreach (Control ControlName in this.GetAllControls())
			{
				if (ControlName is Global.T2KBackFillDecimal)
				{
					ControlName.Text = "0.00";
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveFees();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveFees();
			Close();
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(cmdSave, EventArgs.Empty);
		}
	}
}
