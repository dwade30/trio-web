﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmMarriageReport : BaseForm
	{
		public frmMarriageReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMarriageReport InstancePtr
		{
			get
			{
				return (frmMarriageReport)Sys.GetInstance(typeof(frmMarriageReport));
			}
		}

		protected frmMarriageReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmMarriageReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmMarriageReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMarriageReport properties;
			//frmMarriageReport.FillStyle	= 0;
			//frmMarriageReport.ScaleWidth	= 5880;
			//frmMarriageReport.ScaleHeight	= 4080;
			//frmMarriageReport.LinkTopic	= "Form2";
			//frmMarriageReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
		}

		private void SetupGrid()
		{
			Grid.TextMatrix(0, 0, "Groom's Last");
			Grid.TextMatrix(1, 0, "Bride's Last");
			Grid.TextMatrix(2, 0, "Date Married");
			Grid.TextMatrix(3, 0, "City");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.31));
			Grid.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.345));
			//Grid.Height = 4 * Grid.RowHeight(0) + 30;
		}

		private void frmMarriageReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (Grid.Row == 2)
			{
				string strD = "";
				if (fecherFoundation.Strings.Trim(Grid.EditText).Length == 8)
				{
					if (Information.IsNumeric(fecherFoundation.Strings.Trim(Grid.EditText)))
					{
						strD = fecherFoundation.Strings.Trim(Grid.EditText);
						strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
						if (Information.IsDate(strD))
						{
							Grid.EditText = strD;
						}
					}
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			string strAnd;
			string strWhere;
			string strOrder = "";
			Grid.Row = -1;
			//App.DoEvents();
			if (cmbSort.Text == "Groom")
			{
				strOrder = " order by groomslastname,groomsfirstname ";
			}
			else if (cmbSort.Text == "Bride")
			{
				strOrder = " order by brideslastname,bridesfirstname ";
			}
			else if (cmbSort.Text == "Date Married")
			{
				strOrder = " order by cdate(ceremonydate & '') ";
			}
			else
			{
				strOrder = " order by citymarried ";
			}
			strWhere = "";
			strAnd = "";
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(0, 1)) != string.Empty)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(0, 2)) != string.Empty)
				{
					strWhere = " groomslastname >= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(0, 1)) + "' and groomslastname <= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(0, 2)) + "zzz' ";
				}
				else
				{
					strWhere = " groomslastname = '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(0, 1)) + "' ";
				}
				strAnd = " and ";
			}
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(1, 1)) != string.Empty)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(1, 2)) != string.Empty)
				{
					strWhere += strAnd + " bridescurrentlastname >= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(1, 1)) + "' and bridescurrentlastname <= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(1, 2)) + "zzz' ";
				}
				else
				{
					strWhere += strAnd + " bridescurrentlastname = '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(1, 1)) + "' ";
				}
				strAnd = " and ";
			}
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(2, 1)) != string.Empty)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(2, 2)) != string.Empty)
				{
					if (Information.IsDate(Grid.TextMatrix(2, 1)) && Information.IsDate(Grid.TextMatrix(2, 2)))
					{
						strWhere += strAnd + " ((isdate(ceremonydate) = 1 and convert(datetime,ceremonydate) between '" + Grid.TextMatrix(2, 1) + "' and '" + Grid.TextMatrix(2, 2) + "') OR (ActualDate between '" + Grid.TextMatrix(2, 1) + "' and '" + Grid.TextMatrix(2, 2) + "')) ";
					}
					else
					{
						strWhere += strAnd + " ceremonydate >= '" + Grid.TextMatrix(2, 1) + "' and ceremonydate <= '" + Grid.TextMatrix(2, 2) + "' ";
					}
				}
				else
				{
					if (Information.IsDate(Grid.TextMatrix(2, 1)))
					{
						strWhere += strAnd + " ((isdate(ceremonydate) = 1 and cconvert(datetime,ceremonydate) = '" + Grid.TextMatrix(2, 1) + "') OR (ActualDate = '" + Grid.TextMatrix(2, 1) + "')) ";
					}
					else
					{
						strWhere += strAnd + " ceremonydate = '" + Grid.TextMatrix(2, 1) + "' ";
					}
				}
				strAnd = " and ";
			}
			else if (cmbSort.Text == "Date Married")
			{
				strWhere += strAnd + " isdate(ceremonydate) = 1 ";
				strAnd = " and ";
			}
			if (fecherFoundation.Strings.Trim(Grid.TextMatrix(3, 1)) != string.Empty)
			{
				if (fecherFoundation.Strings.Trim(Grid.TextMatrix(3, 2)) != string.Empty)
				{
					strWhere += strAnd + " citymarried >= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(3, 1)) + "' and citymarried <= '" + modGlobalFunctions.EscapeQuotes(Grid.TextMatrix(3, 2)) + "zzz' ";
				}
				else
				{
					strWhere += strAnd + " CITYmarried = '" + Grid.TextMatrix(3, 1) + "' ";
				}
				strAnd = " and ";
			}
			if (fecherFoundation.Strings.Trim(strWhere) != string.Empty)
			{
				strWhere = " where " + strWhere;
			}
			strSQL = "select * from marriages " + strWhere + strOrder;
			rptMarriageReport.InstancePtr.Init(ref strSQL);
		}

		private void cmdSaveContinue_Click(object sender, EventArgs e)
		{
			mnuSaveContinue_Click(cmdSaveContinue, EventArgs.Empty);
		}
	}
}
