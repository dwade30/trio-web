﻿using Autofac;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Births;
using SharedApplication.Clerk.Burials;
using SharedApplication.Clerk.Deaths;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Marriages;
using SharedApplication.Clerk.Receipting;
using TWCK0000.Dogs;

namespace TWCK0000.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmChooseClerkTransactionType>().As<IView<IChooseClerkTransactionTypeViewModel>>();
            builder.RegisterType<frmSearchDog>().As<IView<IDogOwnerSearchViewModel>>();
            builder.RegisterType<frmNewDogOwner>().As<IView<IDogOwnerViewModel>>();
            builder.RegisterType<frmBirthSearchA>().As<IView<IBirthSearchViewModel>>();
            builder.RegisterType<frmBirths>().As<IView<IBirthViewModel>>();
            builder.RegisterType<frmDeathSearchA>().As<IView<IDeathSearchViewModel>>();
            builder.RegisterType<frmPrintVitalRec>().As<IView<IPrintVitalsViewModel>>();
            builder.RegisterType<frmDeaths>().As<IView<IDeathViewModel>>();
            builder.RegisterType<frmMarriages>().As<IView<IMarriageViewModel>>();
            builder.RegisterType<frmMarriageSearchA>().As<IView<IMarriageSearchViewModel>>();
            builder.RegisterType<frmBurialSearch>().As<IView<IBurialSearchViewModel>>();
            builder.RegisterType<frmBurialPermit>().As<IView<IBurialViewModel>>();
            builder.RegisterType<frmDogAuditOptions>().As<IModalView<IDogAuditOptionsViewModel>>();
        }

    }
}