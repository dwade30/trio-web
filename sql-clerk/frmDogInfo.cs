//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDogInfo : BaseForm
	{
		public frmDogInfo()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            mebDogFees = new System.Collections.Generic.List<T2KBackFillDecimal>();
            mebDogFees.AddControlArrayElement(mebDogFees_0,0);
            mebDogFees.AddControlArrayElement(mebDogFees_1, 1);
            mebDogFees.AddControlArrayElement(mebDogFees_2, 2);
            mebDogFees.AddControlArrayElement(mebDogFees_3, 3);
            mebDogFees.AddControlArrayElement(mebDogFees_4, 4);

            mebKennelFees= new System.Collections.Generic.List<T2KBackFillDecimal>();
            mebKennelFees.AddControlArrayElement(mebKennelFees_0, 0);
            mebKennelFees.AddControlArrayElement(mebKennelFees_1, 1);
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
			Label1 = new System.Collections.Generic.List<FCLabel>();
			Label1.AddControlArrayElement(Label1_0, 0);
			Label1.AddControlArrayElement(Label1_1, 1);
			Label1.AddControlArrayElement(Label1_2, 2);
			Label1.AddControlArrayElement(Label1_3, 3);
			Label1.AddControlArrayElement(Label1_6, 4);
			Label1.AddControlArrayElement(Label1_9, 5);
			Label1.AddControlArrayElement(Label1_10, 6);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDogInfo InstancePtr
		{
			get
			{
				return (frmDogInfo)Sys.GetInstance(typeof(frmDogInfo));
			}
		}

		protected frmDogInfo _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsState = new clsDRWrapper();
		const int NEWRECORD = 1;
		const int MODIFYRECORD = 2;
		const int COLORSTATUSCOL = 2;
		const int BREEDSTATUSCOL = 2;
		bool boolShownModally;
		int intTabToStart;
		public int intColorDataChanged;
		public int intBreedDataChanged;
		public int intFeesDataChanged;
		public int intVetsDataChanged;
		const int CNSTGRIDCOLDESC = 0;
		const int CNSTGRIDCOLSTATE = 1;
		const int CNSTGRIDCOLTOWN = 2;
		const int CNSTGRIDCOLCLERK = 3;

		private void BreedGrid_ChangeEdit(object sender, System.EventArgs e)
		{
			intBreedDataChanged += 1;
            if (FCConvert.ToDouble(BreedGrid.TextMatrix(BreedGrid.Row, BREEDSTATUSCOL)) != 1)
                BreedGrid.TextMatrix(BreedGrid.Row, BREEDSTATUSCOL, FCConvert.ToString(2));
        }

		private void BreedGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (BreedGrid.Rows - 1); counter++)
			{
				if (counter != BreedGrid.Row)
				{
					if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(BreedGrid.TextMatrix(counter, 1))) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(BreedGrid.EditText)))
					{
						MessageBox.Show("The Breed you have entered is a duplicate of a Breed already entered.", "Duplicate Breed", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void VetGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (fecherFoundation.Strings.Trim(VetGrid.EditText) == "")
				return;
			for (counter = 1; counter <= (VetGrid.Rows - 1); counter++)
			{
				if (counter != VetGrid.Row)
				{
					if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(VetGrid.TextMatrix(counter, 1))) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(VetGrid.EditText)))
					{
						MessageBox.Show("The veterinarian you have entered is a duplicate of one already entered.", "Duplicate Vet", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
					}
				}
			}
			// counter
		}

		private void ColorGrid_ChangeEdit(object sender, System.EventArgs e)
		{
			intColorDataChanged += 1;
            if (FCConvert.ToDouble(ColorGrid.TextMatrix(ColorGrid.Row, COLORSTATUSCOL)) != 1)
                ColorGrid.TextMatrix(ColorGrid.Row, COLORSTATUSCOL, FCConvert.ToString(2));
        }

		private void ColorGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (ColorGrid.Rows - 1); counter++)
			{
				if (counter != ColorGrid.Row)
				{
					//FC:FINAL:DDU:#2447 - check only if edit isn't empty string
					if (fecherFoundation.Strings.Trim(ColorGrid.EditText) != "" && fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(ColorGrid.TextMatrix(counter, 1))) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(ColorGrid.EditText)))
					{
						MessageBox.Show("The color you have entered is a duplicate of a color already entered.", "Duplicate Color", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void frmDogInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDogInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// Select Case SSTab1.Tab
				// Case 0
				// If ColorGrid.Col <> 2 Then KeyAscii = KeyAscii - 32
				// Case 1
				// If BreedGrid.Col <> 2 Then KeyAscii = KeyAscii - 32
				// Case 2
				// 
				// End Select
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDogInfo_Load(object sender, System.EventArgs e)
		{
			SetupGrid();
			LoadColors();
			LoadBreeds();
			LoadFees();
			LoadVets();
			intBreedDataChanged = 0;
			intColorDataChanged = 0;
			intFeesDataChanged = 0;
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			SSTab1.SelectedTab = SSTab1.TabPages[intTabToStart];
		}

		private void frmDogInfo_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// If Not boolShownModally Then
			// MDIParent.Show
			// End If
			//FC:FINAL:DDU:#i2033 - call methods correct
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						modDirtyForm.SaveChanges(intColorDataChanged, this, "mnuSaveExit_Click", null, null);
						break;
					}
				case 1:
					{
						modDirtyForm.SaveChanges(intBreedDataChanged, this, "mnuSaveExit_Click", null, null);
						break;
					}
				case 2:
					{
						modDirtyForm.SaveChanges(intFeesDataChanged, this, "mnuSaveExit_Click", null, null);
						break;
					}
			}
			//end switch
		}

		private void Grid_ChangeEdit(object sender, System.EventArgs e)
		{
			intFeesDataChanged += 1;
		}

		private void mebDogFees_Change(int Index, object sender, System.EventArgs e)
		{
			intFeesDataChanged += 1;
		}

		private void mebDogFees_Change(object sender, System.EventArgs e)
		{
			int index = mebDogFees.GetIndex((Global.T2KBackFillDecimal)sender);
			mebDogFees_Change(index, sender, e);
		}

		private void mebKennelFees_ClickEvent(int Index, object sender, System.EventArgs e)
		{
			intFeesDataChanged += 1;
		}

		private void mebKennelFees_ClickEvent(object sender, System.EventArgs e)
		{
			int index = mebKennelFees.GetIndex((Global.T2KBackFillDecimal)sender);
			mebKennelFees_ClickEvent(index, sender, e);
		}

		private void chkLateFeeOnServiceDog_CheckedChanged(object sender, System.EventArgs e)
		{
			intFeesDataChanged += 1;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			//Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedTab.Text.Trim())
			{
				case "Colors":
					{
						DeleteColor();
						break;
					}
				case "Breeds":
					{
						DeleteBreed();
						break;
					}
				case "Fees":
					{
						DeleteFees();
						break;
					}
				case "Veterinarians":
					{
						DeleteVet();
						break;
					}
			}
			//end switch
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedTab.Text.Trim())
			{
				case "Colors":
					{
						NewColor();
						break;
					}
				case "Breeds":
					{
						NewBreed();
						break;
					}
				case "Fees":
					{
						if (MessageBox.Show("This option will clear all existing fees. Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							NewFees();
						}
						break;
					}
				case "Veterinarians":
					{
						NewVet();
						break;
					}
			}
			//end switch
		}

		public void mnuSave_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedTab.Text.Trim())
			{
				case "Colors":
					{
						SaveColors();
						break;
					}
				case "Breeds":
					{
						SaveBreeds();
						break;
					}
				case "Fees":
					{
						SaveFees();
						break;
					}
				case "Veterinarians":
					{
						SaveVets();
						break;
					}
			}
			//end switch
		}

		public void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedTab.Text.Trim())
			{
				case "Colors":
					{
						if (SaveColors() == false)
							return;
						break;
					}
				case "Breeds":
					{
						if (SaveBreeds() == false)
							return;
						break;
					}
				case "Fees":
					{
						if (SaveFees() == false)
							return;
						break;
					}
				case "Veterinarians":
					{
						if (SaveVets() == false)
							return;
						break;
					}
			}
			//end switch
			mnuExit_Click();
		}
        // Private Sub SSTab1_Click(PreviousTab As Integer)
        // On Error Resume Next
        // Select Case SSTab1.Tab
        // Case 0
        // ColorGrid.SetFocus
        // Case 1
        // BreedGrid.SetFocus
        // Case 2
        // mebDogFees(0).SetFocus
        // End Select
        // End Sub
        // *********************************************
        //FC:FINAL:AM:#2448 - moved code to ChangeEdit
  //      private void ColorGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		//{
		//	if (FCConvert.ToDouble(ColorGrid.TextMatrix(ColorGrid.Row, COLORSTATUSCOL)) != 1)
		//		ColorGrid.TextMatrix(ColorGrid.Row, COLORSTATUSCOL, FCConvert.ToString(2));
		//}

		private void NewColor()
		{
			ColorGrid.AddItem("", ColorGrid.Rows);
			ColorGrid.TextMatrix(ColorGrid.Rows - 1, COLORSTATUSCOL, FCConvert.ToString(1));
			ColorGrid.TopRow = ColorGrid.Rows - 1;
			ColorGrid.Row = ColorGrid.Rows - 1;
			ColorGrid.Col = 1;
		}

		private void LoadColors()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from DefaultColors ORDER BY Name", modGNBas.DEFAULTCLERKDATABASE);
			ColorGrid.Rows = rsData.RecordCount() + 1;
			ColorGrid.Cols = 3;
			ColorGrid.TextMatrix(0, 0, "ID");
			ColorGrid.TextMatrix(0, 1, "Color");
			ColorGrid.ColHidden(0, true);
			ColorGrid.ColHidden(COLORSTATUSCOL, true);
			//ColorGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			ColorGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, ColorGrid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// ColorGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1) = True
			// ColorGrid.ColWidth(1) = 7550
			ColorGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				ColorGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				ColorGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Name")));
				ColorGrid.TextMatrix(intCounter, COLORSTATUSCOL, FCConvert.ToString(0));
				rsData.MoveNext();
			}
            rsData.DisposeOf();
			ColorGrid.TopRow = 1;
			ColorGrid.Row = 1;
			ColorGrid.Col = 1;
		}

		private void DeleteColor()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (ColorGrid.Row != 0)
				{
					// If MsgBox("Are you sure you wish to delete this Color?   " & ColorGrid.TextMatrix(ColorGrid.Row, 1), vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
					if (FCConvert.ToDouble(ColorGrid.TextMatrix(ColorGrid.Row, COLORSTATUSCOL)) == 1)
					{
						ColorGrid.RemoveItem(ColorGrid.Row);
					}
					else
					{
						rsState.Execute("Delete from DefaultColors where ID = " + ColorGrid.TextMatrix(ColorGrid.Row, 0), modGNBas.DEFAULTCLERKDATABASE);
						ColorGrid.RemoveItem(ColorGrid.Row);
					}
					// End If
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveColors()
		{
			bool SaveColors = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				ColorGrid.Row = 0;
				for (intCounter = 1; intCounter <= (ColorGrid.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(ColorGrid.TextMatrix(intCounter, 1)).Length <= 0)
					{
						MessageBox.Show("Color name must be entered.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						ColorGrid.Row = intCounter;
						ColorGrid.TopRow = intCounter;
						ColorGrid.Col = 1;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveColors;
					}
					if (FCConvert.ToDouble(ColorGrid.TextMatrix(intCounter, COLORSTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into DefaultColors (Name) VALUES ('" + ColorGrid.TextMatrix(intCounter, 1) + "')", modGNBas.DEFAULTCLERKDATABASE);
						ColorGrid.TextMatrix(intCounter, COLORSTATUSCOL, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(ColorGrid.TextMatrix(intCounter, COLORSTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update DefaultColors Set Name = '" + fecherFoundation.Strings.Trim(ColorGrid.TextMatrix(intCounter, 1)) + "' where ID = " + ColorGrid.TextMatrix(intCounter, 0), modGNBas.DEFAULTCLERKDATABASE);
						ColorGrid.TextMatrix(intCounter, COLORSTATUSCOL, FCConvert.ToString(0));
					}
				}
				LoadColors();
				SaveColors = true;
                intColorDataChanged = 0;
				MessageBox.Show("Save of Color information completed successfully", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveColors;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveColors;
		}
        // *********************************************
        //FC:FINAL:AM:#2448 - moved code to ChangeEdit
  //      private void BreedGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		//{
		//	if (FCConvert.ToDouble(BreedGrid.TextMatrix(BreedGrid.Row, BREEDSTATUSCOL)) != 1)
		//		BreedGrid.TextMatrix(BreedGrid.Row, BREEDSTATUSCOL, FCConvert.ToString(2));
		//}

		private void NewBreed()
		{
			BreedGrid.AddItem("", BreedGrid.Rows);
			BreedGrid.TextMatrix(BreedGrid.Rows - 1, BREEDSTATUSCOL, FCConvert.ToString(1));
			BreedGrid.TopRow = BreedGrid.Rows - 1;
			BreedGrid.Row = BreedGrid.Rows - 1;
			BreedGrid.Col = 1;
		}

		private void NewVet()
		{
			VetGrid.Rows += 1;
			VetGrid.TopRow = VetGrid.Rows - 1;
			VetGrid.TextMatrix(VetGrid.Rows - 1, 0, FCConvert.ToString(0));
			VetGrid.Row = VetGrid.Rows - 1;
			VetGrid.Col = 1;
		}

		private void LoadVets()
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			clsDRWrapper rsLoad = new clsDRWrapper();
			VetGrid.Rows = 1;
			rsLoad.OpenRecordset("Select * from veterinarians order by vetname", modGNBas.DEFAULTCLERKDATABASE);
			VetGrid.TextMatrix(0, 0, "ID");
			VetGrid.TextMatrix(0, 1, "Veterinarian");
			VetGrid.ColHidden(0, true);
			VetGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			while (!rsLoad.EndOfFile())
			{
				VetGrid.Rows += 1;
				intRow = (VetGrid.Rows - 1);
				VetGrid.TextMatrix(intRow, 0, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
				VetGrid.TextMatrix(intRow, 1, FCConvert.ToString(rsLoad.Get_Fields_String("VetName")));
				rsLoad.MoveNext();
			}
            rsLoad.DisposeOf();
			if (VetGrid.Rows > 1)
			{
				VetGrid.TopRow = 1;
				VetGrid.Row = 1;
			}
			VetGrid.Col = 1;
		}

		private void LoadBreeds()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from DefaultBreeds ORDER BY Name", modGNBas.DEFAULTCLERKDATABASE);
			BreedGrid.Rows = rsData.RecordCount() + 1;
			BreedGrid.Cols = 3;
			BreedGrid.TextMatrix(0, 0, "ID");
			BreedGrid.TextMatrix(0, 1, "Breed");
			BreedGrid.ColHidden(0, true);
			BreedGrid.ColHidden(BREEDSTATUSCOL, true);
			//BreedGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			BreedGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, BreedGrid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// BreedGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1) = True
			// BreedGrid.ColWidth(1) = 7550
			BreedGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				BreedGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				BreedGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Name")));
				BreedGrid.TextMatrix(intCounter, BREEDSTATUSCOL, FCConvert.ToString(0));
				rsData.MoveNext();
			}
			BreedGrid.TopRow = 1;
			BreedGrid.Row = 1;
			BreedGrid.Col = 1;
		}

		private void DeleteBreed()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// CHECK TO SEE IF THIS IS A VALID ROW
				if (BreedGrid.Row != 0)
				{
					// If MsgBox("Are you sure you wish to delete this Breed?   " & BreedGrid.TextMatrix(BreedGrid.Row, 1), vbQuestion + vbYesNo, "TRIO Software") = vbYes Then
					if (FCConvert.ToDouble(BreedGrid.TextMatrix(BreedGrid.Row, BREEDSTATUSCOL)) == 1)
					{
						// THIS ROW HAS BEEN ADDED BUT IT WAS NOT SAVED INTO
						// THE DATABASE SO JUST REMOVE IT FROM THE GRID.
						BreedGrid.RemoveItem(BreedGrid.Row);
					}
					else
					{
						// THIS RECORD WAS ADDED TO THE DATABASE SO WE NEED TO
						// REMOVE IT FROM THE DATABASE AND THE GRID.
						rsState.Execute("Delete from DefaultBreeds where ID = " + BreedGrid.TextMatrix(BreedGrid.Row, 0), modGNBas.DEFAULTCLERKDATABASE);
						BreedGrid.RemoveItem(BreedGrid.Row);
					}
					// End If
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private void DeleteVet()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (VetGrid.Row != 0)
				{
					if (Conversion.Val(VetGrid.TextMatrix(VetGrid.Row, 0)) == 0)
					{
						VetGrid.RemoveItem(VetGrid.Row);
					}
					else
					{
						clsDRWrapper rsSave = new clsDRWrapper();
						rsSave.Execute("Delete from Veterinarians where id = " + FCConvert.ToString(Conversion.Val(VetGrid.TextMatrix(VetGrid.Row, 0))), modGNBas.DEFAULTCLERKDATABASE);
						VetGrid.RemoveItem(VetGrid.Row);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveVets()
		{
			bool SaveVets = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				VetGrid.Row = 0;
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from veterinarians", modGNBas.DEFAULTCLERKDATABASE);
				for (intCounter = 1; intCounter <= (VetGrid.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(VetGrid.TextMatrix(intCounter, 1)) == "")
					{
						MessageBox.Show("Veterinarian name must be entered.", "Blank Vet Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						VetGrid.Row = intCounter;
						VetGrid.TopRow = intCounter;
						VetGrid.Col = 1;
						return SaveVets;
					}
					if (Conversion.Val(VetGrid.TextMatrix(intCounter, 0)) > 0)
					{
						if (rsSave.FindFirstRecord("id", Conversion.Val(VetGrid.TextMatrix(intCounter, 0))))
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
					}
					else
					{
						// new
						rsSave.AddNew();
					}
					rsSave.Set_Fields("VetName", fecherFoundation.Strings.Trim(VetGrid.TextMatrix(intCounter, 1)));
					rsSave.Update();
					VetGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsSave.Get_Fields_Int32("id")));
				}
				//FC:FINAL:DDU:#2450 - added prompt message
				FCMessageBox.Show("Save of Veterinarian information completed successfully", MsgBoxStyle.OkOnly, "TRIO Software");
				// intCounter
				SaveVets = true;
                intVetsDataChanged = 0;
				return SaveVets;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveVets;
		}

		private bool SaveBreeds()
		{
			bool SaveBreeds = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				BreedGrid.Row = 0;
				for (intCounter = 1; intCounter <= (BreedGrid.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(BreedGrid.TextMatrix(intCounter, 1)).Length <= 0)
					{
						MessageBox.Show("Breed name must be entered.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						BreedGrid.Row = intCounter;
						BreedGrid.TopRow = intCounter;
						BreedGrid.Col = 1;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveBreeds;
					}
					if (FCConvert.ToDouble(BreedGrid.TextMatrix(intCounter, BREEDSTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into DefaultBreeds (Name) VALUES ('" + modGlobalFunctions.EscapeQuotes(BreedGrid.TextMatrix(intCounter, 1)) + "')", modGNBas.DEFAULTCLERKDATABASE);
						BreedGrid.TextMatrix(intCounter, BREEDSTATUSCOL, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(BreedGrid.TextMatrix(intCounter, BREEDSTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update DefaultBreeds Set Name = '" + modGlobalFunctions.EscapeQuotes(fecherFoundation.Strings.Trim(BreedGrid.TextMatrix(intCounter, 1))) + "' where ID = " + BreedGrid.TextMatrix(intCounter, 0), modGNBas.DEFAULTCLERKDATABASE);
						BreedGrid.TextMatrix(intCounter, BREEDSTATUSCOL, FCConvert.ToString(0));
					}
				}
				LoadBreeds();
				SaveBreeds = true;
                intBreedDataChanged = 0;
				MessageBox.Show("Save of Breed information completed successfully", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveBreeds;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveBreeds;
		}

		private void NewFees()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				mebDogFees[0].Text = "0.00";
				mebDogFees[1].Text = "0.00";
				mebDogFees[2].Text = "0.00";
				mebDogFees[3].Text = "0.00";
				mebDogFees[4].Text = "0.00";
				//FC:FINAL:DDU:#i2203 - avoided no control error
				//mebDogFees[5].Text = "0.00";
				//mebDogFees[6].Text = "0.00";
				mebKennelFees[0].Text = "0.00";
				mebKennelFees[1].Text = "0.00";
				//FC:FINAL:DDU:#i2203 - avoided no control error
				//mebKennelFees[2].Text = "0.00";
				//mebKennelFees[3].Text = "0.00";
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private void LoadFees()
		{
			int I;
			object tempMask;
			modDogs.GetDogFees();
			if (modDogs.Statics.DogFee.LateFeeOnServiceDog)
			{
				// kk07302015 trocks-9
				chkLateFeeOnServiceDog.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkLateFeeOnServiceDog.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			mebDogFees[0].SelectedText = modDogs.Statics.DogFee.Late_Fee.FormatAsMoney();
			mebDogFees[1].SelectedText = modDogs.Statics.DogFee.Warrant_Fee.FormatAsMoney();
			mebDogFees[2].SelectedText = modDogs.Statics.DogFee.Replacement_Fee.FormatAsMoney();
			mebDogFees[3].SelectedText = modDogs.Statics.DogFee.Transfer_Fee.FormatAsMoney();
			mebDogFees[4].SelectedText = modDogs.Statics.DogFee.DogTagFee.FormatAsMoney();
			txtCustomFeeAmount.Text = modDogs.Statics.DogFee.UserDefinedFee1.FormatAsMoney();
			txtCustomFeeDesc.Text = modDogs.Statics.DogFee.UserDefinedDescription1;
            txtDangerousLateFee.Text = modDogs.Statics.DogFee.DangerousLateFee.FormatAsMoney();
            txtNuisanceLateFee.Text = modDogs.Statics.DogFee.NuisanceLateFee.FormatAsMoney();
			Grid.TextMatrix(1, CNSTGRIDCOLSTATE, Strings.Format(modDogs.Statics.DogFee.FixedState, "0.00"));
			Grid.TextMatrix(1, CNSTGRIDCOLTOWN, Strings.Format(modDogs.Statics.DogFee.FixedTown, "0.00"));
			Grid.TextMatrix(1, CNSTGRIDCOLCLERK, Strings.Format(modDogs.Statics.DogFee.FixedClerk, "0.00"));
			Grid.TextMatrix(2, CNSTGRIDCOLSTATE, Strings.Format(modDogs.Statics.DogFee.DogState, "0.00"));
			Grid.TextMatrix(2, CNSTGRIDCOLTOWN, Strings.Format(modDogs.Statics.DogFee.DogTown, "0.00"));
			Grid.TextMatrix(2, CNSTGRIDCOLCLERK, Strings.Format(modDogs.Statics.DogFee.DogClerk, "0.00"));
			Grid.TextMatrix(3, CNSTGRIDCOLSTATE, Strings.Format(modDogs.Statics.DogFee.KennelState, "0.00"));
			Grid.TextMatrix(3, CNSTGRIDCOLTOWN, Strings.Format(modDogs.Statics.DogFee.KennelTown, "0.00"));
			Grid.TextMatrix(3, CNSTGRIDCOLCLERK, Strings.Format(modDogs.Statics.DogFee.KennelClerk, "0.00"));
            Grid.TextMatrix(4, CNSTGRIDCOLSTATE, Strings.Format(modDogs.Statics.DogFee.DangerousStateFee, "0.00"));
            Grid.TextMatrix(4, CNSTGRIDCOLTOWN, Strings.Format(modDogs.Statics.DogFee.DangerousTownFee, "0.00"));
            Grid.TextMatrix(4, CNSTGRIDCOLCLERK, Strings.Format(modDogs.Statics.DogFee.DangerousClerkFee, "0.00"));
            Grid.TextMatrix(5, CNSTGRIDCOLSTATE, Strings.Format(modDogs.Statics.DogFee.NuisanceStateFee, "0.00"));
            Grid.TextMatrix(5, CNSTGRIDCOLTOWN, Strings.Format(modDogs.Statics.DogFee.NuisanceTownFee, "0.00"));
            Grid.TextMatrix(5, CNSTGRIDCOLCLERK, Strings.Format(modDogs.Statics.DogFee.NuisanceClerkFee, "0.00"));
			mebKennelFees[0].SelectedText = Strings.Format(modDogs.Statics.DogFee.KennelWarrantFee, "0.00");
			mebKennelFees[1].SelectedText = Strings.Format(modDogs.Statics.DogFee.KennelLateFee, "0.00");
		}

		private void SetupGrid()
		{
			Grid.TextMatrix(0, 1, "State");
			Grid.TextMatrix(0, 2, "Town");
			Grid.TextMatrix(0, 3, "Clerk");
			Grid.TextMatrix(1, 0, "Neuter / Spay");
			Grid.TextMatrix(2, 0, "Male / Female");
			Grid.TextMatrix(3, 0, "Kennel");
            Grid.TextMatrix(4, 0, "Dangerous");
            Grid.TextMatrix(5, 0, "Nuisance");
        }

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLDESC, FCConvert.ToInt32(0.34 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSTATE, FCConvert.ToInt32(0.22 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLTOWN, FCConvert.ToInt32(0.22 * GridWidth));
			//Grid.HeightOriginal = Grid.RowHeight(0) * 4 + 30;
		}

		private void DeleteFees()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				mebDogFees[0].Text = "0.00";
				mebDogFees[1].Text = "0.00";
				mebDogFees[2].Text = "0.00";
				mebDogFees[3].Text = "0.00";
				mebDogFees[4].Text = "0.00";
				mebKennelFees[0].Text = "0.00";
				mebKennelFees[1].Text = "0.00";
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveFees()
		{
			bool SaveFees = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsKennel = new clsDRWrapper();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				Grid.Row = 0;
				//App.DoEvents();
				modDogs.Statics.DogFee.LateFeeOnServiceDog = FCConvert.CBool(chkLateFeeOnServiceDog.CheckState == Wisej.Web.CheckState.Checked);
				// kk07302015 trocks-9
				modDogs.Statics.DogFee.Late_Fee = FCConvert.ToDecimal(mebDogFees[0].Text);
				modDogs.Statics.DogFee.Warrant_Fee = FCConvert.ToDecimal(mebDogFees[1].Text);
				modDogs.Statics.DogFee.Replacement_Fee = FCConvert.ToDecimal(mebDogFees[2].Text);
				modDogs.Statics.DogFee.Transfer_Fee = FCConvert.ToDecimal(mebDogFees[3].Text);
				modDogs.Statics.DogFee.DogTagFee = FCConvert.ToDecimal(mebDogFees[4].Text);
				modDogs.Statics.DogFee.UserDefinedFee1 = Conversion.Val(txtCustomFeeAmount.Text);
				modDogs.Statics.DogFee.UserDefinedDescription1 = fecherFoundation.Strings.Trim(txtCustomFeeDesc.Text);
				modDogs.Statics.DogFee.KennelWarrantFee = Conversion.Val(mebKennelFees[0].Text);
				modDogs.Statics.DogFee.KennelLateFee = Conversion.Val(mebKennelFees[1].Text);
				modDogs.Statics.DogFee.DogClerk = Conversion.Val(Grid.TextMatrix(2, CNSTGRIDCOLCLERK));
				modDogs.Statics.DogFee.DogState = Conversion.Val(Grid.TextMatrix(2, CNSTGRIDCOLSTATE));
				modDogs.Statics.DogFee.DogTown = Conversion.Val(Grid.TextMatrix(2, CNSTGRIDCOLTOWN));
				modDogs.Statics.DogFee.FixedClerk = Conversion.Val(Grid.TextMatrix(1, CNSTGRIDCOLCLERK));
				modDogs.Statics.DogFee.FixedState = Conversion.Val(Grid.TextMatrix(1, CNSTGRIDCOLSTATE));
				modDogs.Statics.DogFee.FixedTown = Conversion.Val(Grid.TextMatrix(1, CNSTGRIDCOLTOWN));
				modDogs.Statics.DogFee.KennelClerk = Conversion.Val(Grid.TextMatrix(3, CNSTGRIDCOLCLERK));
				modDogs.Statics.DogFee.KennelState = Conversion.Val(Grid.TextMatrix(3, CNSTGRIDCOLSTATE));
				modDogs.Statics.DogFee.KennelTown = Conversion.Val(Grid.TextMatrix(3, CNSTGRIDCOLTOWN));
                modDogs.Statics.DogFee.DangerousClerkFee = Conversion.Val(Grid.TextMatrix(4,CNSTGRIDCOLCLERK));
                modDogs.Statics.DogFee.DangerousStateFee = Conversion.Val(Grid.TextMatrix(4, CNSTGRIDCOLSTATE));
                modDogs.Statics.DogFee.DangerousTownFee = Conversion.Val(Grid.TextMatrix(4, CNSTGRIDCOLTOWN));
                modDogs.Statics.DogFee.NuisanceClerkFee = Conversion.Val(Grid.TextMatrix(5, CNSTGRIDCOLCLERK));
                modDogs.Statics.DogFee.NuisanceStateFee = Conversion.Val(Grid.TextMatrix(5, CNSTGRIDCOLSTATE));
                modDogs.Statics.DogFee.NuisanceTownFee = Conversion.Val(Grid.TextMatrix(5, CNSTGRIDCOLTOWN));
                modDogs.Statics.DogFee.NuisanceLateFee = Conversion.Val(txtNuisanceLateFee.Text);
                modDogs.Statics.DogFee.DangerousLateFee = Conversion.Val(txtDangerousLateFee.Text);
                modDogs.Statics.DogFee.Fixed_Fee = FCConvert.ToDecimal(modDogs.Statics.DogFee.FixedClerk + modDogs.Statics.DogFee.FixedState + modDogs.Statics.DogFee.FixedTown);
				modDogs.Statics.DogFee.UN_Fixed_FEE = FCConvert.ToDecimal(modDogs.Statics.DogFee.DogClerk + modDogs.Statics.DogFee.DogState + modDogs.Statics.DogFee.DogTown);
				modDogs.SetDogFees();
				SaveFees = true;
                intFeesDataChanged = 0;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveFees;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveFees", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveFees;
		}

		public void Init(bool boolModal, int intTab = 0)
		{
			boolShownModally = boolModal;
			intTabToStart = intTab;
			if (boolModal)
			{
				int x;
				for (x = 0; x <= 3; x++)
				{
					if (x != intTab)
					{
						SSTab1.TabPages[x].Visible = false;
					}
				}
				// x
				this.Show(FCForm.FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void txtCustomFeeAmount_TextChanged(object sender, System.EventArgs e)
		{
			intFeesDataChanged += 1;
		}

		private void txtCustomFeeDesc_TextChanged(object sender, System.EventArgs e)
		{
			intFeesDataChanged += 1;
		}
	}
}
