//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using TWSharedLibrary.Data;
using Wisej.Web;
using clsDRWrapper = Global.clsDRWrapper;

namespace TWCK0000
{
	public class modStartup
	{
		//=========================================================
		public const int CKLEVELFULLCLERK = 0;
		// Levels are ored together.  6 would be vitals only population level2
		public const int CKLEVELPOPLEVEL1 = 1;
		// POP 4000
		public const int CKLEVELPOPLEVEL2 = 2;
		// POP 10000
		public const int CKLEVELVITALSONLY = 4;
		public const int CKLEVELDOGSONLY = 8;

		public static bool UpdateDatabaseStructure()
		{
			bool UpdateDatabaseStructure = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				bool boolOutcome;
				frmWait.InstancePtr.Init("Checking Database Structure", true);
				boolOutcome = false;
				UpdateDatabaseStructure = false;
				CheckVersion(true);
				UpdateDatabaseStructure = boolOutcome;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Check Database Structure Complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return UpdateDatabaseStructure;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UpdateDatabaseStructure", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateDatabaseStructure;
		}

		private static bool CheckVersion(bool boolAll = false)
		{
			bool CheckVersion = false;
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			clsDRWrapper rs = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CheckVersion = false;
				cClerk tClerk = new cClerk();
				// kk07302015 trocks-9
				CheckVersion = tClerk.CheckVersion();
				cCentralParties tPDB = new cCentralParties();
				tPDB.CheckVersion();
				// 
				return CheckVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckVersion", "error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckVersion;
		}

		public static void Main(bool addExitMenu, CommandDispatcher commandDispatcher)
		{
			
			//if (!FCConvert.ToBoolean(modGlobalFunctions.LoadSQLConfig("TWCK0000.VB1")))
			//{
			//	MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			//	return;
			//}

			try
			{
                modCashReceiptingData.Statics.bolFromWindowsCR = false;
                InitializeClerkStatics();
    //            // On Error GoTo ErrorHandler
    //            fecherFoundation.Information.Err().Clear();
				//// Debug.Print "Start Main   " & Time
				//Statics.boolError = false;
				
				//modReplaceWorkFiles.GetGlobalVariables();
                //modGlobalFunctions.GetGlobalRegistrySetting();
				modReplaceWorkFiles.EntryFlagFile(boolUseRegistry: true);

			    if (modGlobalConstants.Statics.gstrEntryFlag == "CR")
				{
					modCashReceiptingData.Statics.bolFromWindowsCR = true;
					modCashReceiptingData.Statics.typeCRData.ID = 0;
					modCashReceiptingData.Statics.typeCRData.Type = "D";
					modCashReceiptingData.Statics.typeCRData.Name = "";
					modCashReceiptingData.Statics.typeCRData.PartyID = 0;
					// .Reference = Left(dInfo.DOGNAME, 10)
					modCashReceiptingData.Statics.typeCRData.Reference = "";
					// .Control1 = Left(dInfo.TagLicNum, 10)
					modCashReceiptingData.Statics.typeCRData.Control1 = "";
					modCashReceiptingData.Statics.typeCRData.Control2 = "";
					modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble("0.00");
					modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble("0.00");
					modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble("0.00");
					modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble("0.00");
					modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble("0.00");
					modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "N";
					modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
					/*? 30 */
				}

				modReplaceWorkFiles.EntryFlagFile(true, "  ");
				// THIS IS THE NETWORK
				modReplaceWorkFiles.Statics.gstrNetworkFlag = (FCConvert.ToString(modRegistry.GetRegistryKey("NetworkFlag")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "Y");
			//	modReplaceWorkFiles.SetDatabase();
			//	modReplaceWorkFiles.Statics.gintSecurityID = FCConvert.ToInt32((FCConvert.ToString(modRegistry.GetRegistryKey("SecurityID")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "0"));
				// create a new instance of the class trio security
				//modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
				//if (modReplaceWorkFiles.Statics.gintSecurityID == 0)
				//{
				//	// no user has been chosen Need to run the
				//	// change user routine this is to show
				//	frmTrioSplash.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				//}
				//modGlobalConstants.Statics.MuniName = fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName);
				//modGNBas.Statics.TrioSDrive = fecherFoundation.Strings.Trim(modGNBas.Statics.TrioSDrive);
				//modGlobalFunctions.UpdateUsedModules();
				if (!modGlobalConstants.Statics.gboolCK)
				{
					MessageBox.Show("Clerk has expired or is not enabled", "Cannot Load", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Application.Exit();
					return;
				}
				//modReplaceWorkFiles.Statics.secRS = null;
				//Statics.gintClerkLevel = 0;
				//modReplaceWorkFiles.Statics.secRS.OpenRecordset("select cklevel from modules", "SystemSettings");
				//if (!modReplaceWorkFiles.Statics.secRS.EndOfFile())
				//{
				//	Statics.gintClerkLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.secRS.Get_Fields_Int16("cklevel"))));
				//	if (Statics.gintClerkLevel < 0)
				//		Statics.gintClerkLevel = 0;
				//}
				//modReplaceWorkFiles.Statics.secRS.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				//if (!modReplaceWorkFiles.Statics.secRS.EndOfFile())
				//{
				//	// get the type of installation that this town is
				//	// Town or City
				//	Statics.CityTown = FCConvert.ToString(modReplaceWorkFiles.Statics.secRS.Get_Fields_String("CityTown"));
				//}
				//modGNBas.Statics.boolRegionalTown = modRegionalTown.IsRegionalTown();
				//GetWorkRecords();
				//modGlobalConstants.Statics.clsSecurityClass.Init("CK");
				//modClerkGeneral.Statics.ClerkName = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserName());
				//// assign a ROBO HELP file to this application and allow the F1 key to work
				//App.HelpFile = App.Path + "\\twck0000.hlp";
				//CheckVersion(false);
				
				modReplaceWorkFiles.EntryFlagFile(true, "CK");

				//Statics.ClerkDatabase = modGNBas.DEFAULTDATABASE;
				//modDogs.GetClerkFees();
				//modGNBas.Statics.ClerkDefaults.LoadCustomizeInfo();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MDIParent.InstancePtr.Init(addExitMenu,commandDispatcher);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (fecherFoundation.Information.Err(ex).Number == 3078)
				{
					MessageBox.Show("Invalid General Entry database structure.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Main Line " + fecherFoundation.Information.Erl());
				}
			}
		}

		public class StaticVariables
		{
            public bool moduleInitialized = false;
            public string CityTown = "";
			public string ClerkDatabase = string.Empty;
			public int gintClerkLevel;
			public bool boolError;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}

        public static void InitializeClerkStatics()
        {
            if (TWSharedLibrary.Data.DataReader.Statics.ConnectionPools == null ||
                String.IsNullOrWhiteSpace(TWSharedLibrary.Data.DataReader.Statics.ConnectionPools.DefaultMegaGroup))
            {
                DataConfigurator.LoadSQLConfig();
            }
            if (string.IsNullOrWhiteSpace(modGlobalConstants.Statics.MuniName ))
            {
                try
                {

                    modCashReceiptingData.Statics.bolFromWindowsCR = false;
                    modReplaceWorkFiles.GetGlobalVariables();
                    modGlobalFunctions.GetGlobalRegistrySetting();



                    modReplaceWorkFiles.SetDatabase();
                    modReplaceWorkFiles.Statics.gintSecurityID =
                        (modRegistry.GetRegistryKey("SecurityID") != string.Empty
                            ? modReplaceWorkFiles.Statics.gstrReturn
                            : "0").ToIntegerValue();
                    modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();

                    modGlobalConstants.Statics.MuniName =
                        fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName);
                    modGNBas.Statics.TrioSDrive = fecherFoundation.Strings.Trim(modGNBas.Statics.TrioSDrive);
                    modGlobalFunctions.UpdateUsedModules();

                    modReplaceWorkFiles.Statics.secRS = null;
                    Statics.gintClerkLevel = 0;
                    modReplaceWorkFiles.Statics.secRS.OpenRecordset("select cklevel from modules", "SystemSettings");
                    if (!modReplaceWorkFiles.Statics.secRS.EndOfFile())
                    {
                        Statics.gintClerkLevel =
                            FCConvert.ToInt32(Math.Round(
                                Conversion.Val(modReplaceWorkFiles.Statics.secRS.Get_Fields_Int16("cklevel"))));
                        if (Statics.gintClerkLevel < 0)
                            Statics.gintClerkLevel = 0;
                    }

                    modReplaceWorkFiles.Statics.secRS.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
                    if (!modReplaceWorkFiles.Statics.secRS.EndOfFile())
                    {
                        // get the type of installation that this town is
                        // Town or City
                        Statics.CityTown =
                            FCConvert.ToString(modReplaceWorkFiles.Statics.secRS.Get_Fields_String("CityTown"));
                    }

                    modGNBas.Statics.boolRegionalTown = modRegionalTown.IsRegionalTown();
                    modGlobalConstants.Statics.clsSecurityClass.Init("CK");
                    modClerkGeneral.Statics.ClerkName =
                        FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserName());
                    CheckVersion(false);
                    Statics.ClerkDatabase = modGNBas.DEFAULTDATABASE;
                    modDogs.GetClerkFees();
                    modGNBas.Statics.ClerkDefaults.LoadCustomizeInfo();
                }
                catch
                {

                }
            }
        }
	}
}
