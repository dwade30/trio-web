//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmTitles : BaseForm
	{
		public frmTitles()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTitles InstancePtr
		{
			get
			{
				return (frmTitles)Sys.GetInstance(typeof(frmTitles));
			}
		}

		protected frmTitles _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLID = 0;
		const int CNSTGRIDCOLDESCRIPTION = 1;
		private string strReturn = string.Empty;
		private bool boolReturnOption;

		public string Init()
		{
			string Init = "";
			Init = "";
			strReturn = "";
			boolReturnOption = true;
			this.Show(FCForm.FormShowEnum.Modal);
			return Init;
		}

		private void frmTitles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmTitles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTitles properties;
			//frmTitles.FillStyle	= 0;
			//frmTitles.ScaleWidth	= 5880;
			//frmTitles.ScaleHeight	= 4410;
			//frmTitles.LinkTopic	= "Form2";
			//frmTitles.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void LoadGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			Grid.Rows = 1;
			clsLoad.OpenRecordset("select * from titles order by description", "twck0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				clsLoad.MoveNext();
			}
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Title");
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow < 1)
				return;
			if (boolReturnOption)
			{
				strReturn = Grid.TextMatrix(Grid.MouseRow, CNSTGRIDCOLDESCRIPTION);
				Close();
			}
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						mnuDelete_Click();
						break;
					}
				case Keys.Insert:
					{
						KeyCode = 0;
						mnuNew_Click();
						break;
					}
			}
			//end switch
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngDRow;
			if (Grid.Row < 1)
				return;
			GridDeleted.Rows += 1;
			lngDRow = GridDeleted.Rows - 1;
			lngRow = Grid.Row;
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLID)) > 0)
			{
				GridDeleted.TextMatrix(lngDRow, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLID))));
			}
			Grid.RemoveItem(lngRow);
		}

		public void mnuDelete_Click()
		{
			mnuDelete_Click(mnuDelete, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			Grid.Rows += 1;
			Grid.TopRow = Grid.Rows - 1;
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				Grid.Row = 0;
				SaveInfo = false;
				for (x = 0; x <= (GridDeleted.Rows - 1); x++)
				{
					clsSave.Execute("delete from titles where ID = " + FCConvert.ToString(Conversion.Val(GridDeleted.TextMatrix(x, 0))), "twck0000.vb1");
				}
				// x
				GridDeleted.Rows = 0;
				for (x = 1; x <= (Grid.Rows - 1); x++)
				{
					clsSave.OpenRecordset("select * from titles where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLID))), "twck0000.vb1");
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
					}
					else
					{
						clsSave.AddNew();
						Grid.TextMatrix(x, CNSTGRIDCOLID, FCConvert.ToString(clsSave.Get_Fields_Int32("ID")));
					}
					clsSave.Set_Fields("description", Grid.TextMatrix(x, CNSTGRIDCOLDESCRIPTION));
					clsSave.Update();
				}
				// x
				SaveInfo = true;
				MessageBox.Show("Save successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mnuSaveContinue_Click(cmdSave, EventArgs.Empty);
        }
    }
}
