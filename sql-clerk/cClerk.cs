//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports.Data;
using Wisej.Web;

namespace TWCK0000
{
	public class cClerk
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", "twck0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("Build"));
					tVer.Major = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("Major"));
					tVer.Minor = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("Minor"));
					tVer.Revision = FCConvert.ToInt16(rsLoad.Get_Fields_Int32("Revision"));
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
				lngLastError = fecherFoundation.Information.Err(ex).Number;
			}
			return GetVersion;
		}

		public void SetVersion(cVersionInfo nVersion)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", "twck0000.vb1");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
				lngLastError = fecherFoundation.Information.Err(ex).Number;
			}
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;
				cVer = GetVersion();
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1;
				
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 0;
				tVer.Build = 0;
				if (tVer.IsNewer( cVer))
				{
					if (AddLateFeeOnServiceDogOption())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return false;
					}
				}

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer( cVer))
                {
                    if (AddNuisanceFields())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AlterTagFields())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
	                if (AddLastUpdatedToDogInfoTable())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 4;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (UpdateOwnerPartyViews())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 5;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (CorrectDogTransactionTownCode())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                CheckVersion = true;
				return CheckVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = fecherFoundation.Information.Err(ex).Description;
				lngLastError = fecherFoundation.Information.Err(ex).Number;
			}
			return CheckVersion;
		}

        private bool CorrectDogTransactionTownCode()
        {
            if (!modGNBas.Statics.boolRegionalTown)
            {
                try
                {
                    fecherFoundation.Information.Err().Clear();
                    clsDRWrapper rsTemp = new clsDRWrapper();
                    rsTemp.Execute("UPDATE DogTransactions SET TownCode = 0", "twck0000.vb1");
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return true;
        }

        private bool AddLastUpdatedToDogInfoTable()
		{
			try
			{
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsTemp.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'LastUpdated' AND TABLE_NAME = 'DogInfo'", "twck0000.vb1");
				if (rsTemp.EndOfFile())
				{
					rsTemp.Execute("ALTER TABLE [dbo].[DogInfo] ADD [LastUpdated] datetime NULL", "twck0000.vb1");
				}
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool AlterTagFields()
        {
            try
            {
                var updateInventory = false;
                var updateTransactionTag = false;
                var updateTransactionOldTag = false;

                if (GetDataType("DogInventory", "StickerNumber", "Clerk") == "int")
                {
                    updateInventory = true;
                }

                if (GetDataType("DogTransactions", "TagNumber", "Clerk") == "int")
                {
                    updateTransactionTag = true;
                }

                if (GetDataType("DogTransactions", "OldTagNumber", "Clerk") == "int")
                {
                    updateTransactionOldTag = true;
                }

                string strSQL = "";
                var rsSave = new clsDRWrapper();

                if (updateInventory)
                {
                    strSQL = "Alter table DogInventory alter column StickerNumber varchar(50) null";
                    rsSave.Execute(strSQL, "Clerk");
                }

                if (updateTransactionTag)
                {
                    strSQL = "alter table DogTransactions alter column TagNumber varchar(50) null";
                    rsSave.Execute(strSQL, "Clerk");
                }

                if (updateTransactionOldTag)
                {
                    strSQL = "alter table DogTransactions alter column OldTagNumber varchar(50) null";
                    rsSave.Execute(strSQL, "Clerk");
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddLateFeeOnServiceDogOption()
		{
			bool AddLateFeeOnServiceDogOption = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsTemp = new clsDRWrapper();
				// kk07302015 trocks-9  Add Allow Late Fee on Service and Guide Dog option
				rsTemp.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'LateFeeOnServiceDog' AND TABLE_NAME = 'SettingsFees'", "twck0000.vb1");
				if (rsTemp.EndOfFile())
				{
					rsTemp.Execute("ALTER TABLE [dbo].[SettingsFees] ADD [LateFeeOnServiceDog] bit NULL", "twck0000.vb1");
				}
				AddLateFeeOnServiceDogOption = true;
				return AddLateFeeOnServiceDogOption;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				AddLateFeeOnServiceDogOption = false;
			}
			return AddLateFeeOnServiceDogOption;
		}

        private bool AddNuisanceFields()
        {
            try
            {
                var rsTemp = new clsDRWrapper();

                if (!FieldExists("DogTransactions", "DangerousDog"))
                {
                    rsTemp.Execute("alter table [DogTransactions] add [DangerousDog] bit NULL", "Clerk");
                    rsTemp.Execute("update dogtransactions set DangerousDog = 0", "Clerk");
                }

                if (!FieldExists("DogTransactions", "NuisanceDog"))
                {
                    rsTemp.Execute("alter table [DogTransactions] add [NuisanceDog] bit NULL", "Clerk");
                    rsTemp.Execute("update dogtransactions set NuisanceDog = 0", "Clerk");
                }

                if (!FieldExists("SettingsFees", "DangerousClerkFee"))
                {
                    rsTemp.Execute("alter table [SettingsFees] add DangerousClerkFee Decimal (5,2) NULL", "Clerk");
                    rsTemp.Execute("update settingsfees set DangerousClerkFee = 1", "Clerk");
                }

                if (!FieldExists("SettingsFees", "DangerousStateFee"))
                {
                    rsTemp.Execute("alter table [SettingsFees] add DangerousStateFee Decimal (5,2) NULL", "Clerk");
                    rsTemp.Execute("update settingsfees set DangerousStateFee = 1", "Clerk");
                }

                if (!FieldExists("SettingsFees", "DangerousTownFee"))
                {
                    rsTemp.Execute("alter table [SettingsFees] add DangerousTownFee Decimal (5,2) NULL", "Clerk");
                    rsTemp.Execute("update settingsfees set DangerousTownFee = 98", "Clerk");
                }

                if (!FieldExists("SettingsFees", "DangerousLateFee"))
                {
                    rsTemp.Execute("alter table [SettingsFees] add DangerousLateFee Decimal (5,2) NULL", "Clerk");
                    rsTemp.Execute("update settingsfees set DangerousLateFee = 140", "Clerk");
                }

                if (!FieldExists("SettingsFees", "NuisanceClerkFee"))
                {
                    rsTemp.Execute("alter table [SettingsFees] add NuisanceClerkFee Decimal (5,2) NULL", "Clerk");
                    rsTemp.Execute("update settingsfees set NuisanceClerkFee = 1", "Clerk");
                }

                if (!FieldExists("SettingsFees", "NuisanceStateFee"))
                {
                    rsTemp.Execute("alter table [SettingsFees] add NuisanceStateFee  Decimal (5,2) NULL", "Clerk");
                    rsTemp.Execute("update settingsfees set NuisanceStateFee = 1", "Clerk");
                }

                if (!FieldExists("SettingsFees", "NuisanceTownFee"))
                {
                    rsTemp.Execute("alter table [SettingsFees] add NuisanceTownFee Decimal (5,2) NULL", "Clerk");
                    rsTemp.Execute("update settingsfees set NuisanceTownFee = 28", "Clerk");
                }

                if (!FieldExists("SettingsFees", "NuisanceLateFee"))
                {
                    rsTemp.Execute("alter table [SettingsFees] add NuisanceLateFee Decimal (5,2) NULL", "Clerk");
                    rsTemp.Execute("update settingsfees set NuisanceLateFee = 70", "Clerk");
                }

                if (!FieldExists("DogInfo", "IsDangerousDog"))
                {
                    rsTemp.Execute("alter table [DogInfo] add [IsDangerousDog] bit NULL", "Clerk");
                    rsTemp.Execute("update Doginfo set IsDangerousDog = 0", "Clerk");
                }

                if (!FieldExists("DogInfo", "IsNuisanceDog"))
                {
                    rsTemp.Execute("alter table [DogInfo] add [IsNuisanceDog] bit NULL", "Clerk");
                    rsTemp.Execute("update DogInfo set IsNuisanceDog = 0", "Clerk");
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool FieldExists(string strTable, string strField)
        {
            var rsTemp = new clsDRWrapper();
            rsTemp.OpenRecordset(
                "Select Column_Name from Information_Schema.Columns where column_name = '" + strField +
                "' and table_name = '" + strTable + "'", "Clerk");
            if (!rsTemp.EndOfFile())
            {
                return true;
            }

            return false;
        }

        private string GetDataType(string strTableName, string strFieldName, string strDB)
        {
            var rsLoad = new clsDRWrapper();
            var strSQL = "select data_type from information_schema.columns where table_name = '" + strTableName + "' and column_name = '" + strFieldName + "'";
            rsLoad.OpenRecordset(strSQL, strDB);
            if (!rsLoad.EndOfFile())
            {
                return rsLoad.Get_Fields_String("data_type");
            }

            return "";

        }

        private bool UpdateOwnerPartyViews()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();

            try
            {
                rsUpdate.OpenRecordset("select* FROM sys.views where name = 'DogOwnerPartyView'", "Clerk");
                if (!rsUpdate.EndOfFile())
                {
                    rsUpdate.Execute("Drop View [dbo].[DogOwnerPartyView]", "Clerk");
                }

                var sql = "Create View [dbo].[DogOwnerPartyView] as "
                          + "select dogowner.*, parties.PartyGuid,parties.PartyType,parties.FirstName,parties.MiddleName,parties.LastName, parties.Designation,parties.Email,parties.WebAddress"
                          + " from dbo.DogOwner Inner join "
                          + rsUpdate.Get_GetFullDBName("CentralParties")
                          + ".dbo.Parties on DogOwner.PartyID = Parties.id";
                rsUpdate.Execute(sql, "Clerk");

                sql = "Create View [dbo].[DogAndOwnerPartyView] as "
                          + "select tbl1.* , parties.PartyGuid,parties.PartyType,parties.FirstName,parties.MiddleName,parties.LastName, parties.Designation,parties.Email,parties.WebAddress"
                          + " from (select DogInfo.*, dogowner.PartyId, dogOwner.dogNUMBERs, dogOwner.LocationNum, dogOwner.LocationStr, dogOwner.Deleted as OwnerDeleted"
                          + ", dogOwner.Comments as OwnerComments, dogOwner.TownCode "
                          + " from dbo.doginfo inner join dbo.DogOwner on "
                          + " DogInfo.OwnerNum = dogowner.ID) tbl1 left join "
                          + rsUpdate.Get_GetFullDBName("CentralParties")
                          + ".dbo.Parties "
                          + " on tbl1.PartyID = Parties.id";
                rsUpdate.Execute(sql, "Clerk");
                return true;
            }
            catch
            {
                return false;
            }
        }

    }

}

