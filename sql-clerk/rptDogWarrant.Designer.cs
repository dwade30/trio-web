﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogWarrant.
	/// </summary>
	partial class rptDogWarrant
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDogWarrant));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtTownCounty1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtControlOfficer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownCounty2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownName4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownName3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtTownCounty1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtControlOfficer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownCounty2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTownCounty1,
				this.txtControlOfficer,
				this.txtTownName1,
				this.txtTownName2,
				this.txtTownCounty2,
				this.txtTownName4,
				this.txtTownName3
			});
			this.PageHeader.Height = 8.166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtTownCounty1
			// 
			this.txtTownCounty1.Height = 0.1875F;
			this.txtTownCounty1.Left = 0F;
			this.txtTownCounty1.Name = "txtTownCounty1";
			this.txtTownCounty1.Text = null;
			this.txtTownCounty1.Top = 1.75F;
			this.txtTownCounty1.Width = 2.3125F;
			// 
			// txtControlOfficer
			// 
			this.txtControlOfficer.Height = 0.1875F;
			this.txtControlOfficer.Left = 0.1875F;
			this.txtControlOfficer.Name = "txtControlOfficer";
			this.txtControlOfficer.Text = null;
			this.txtControlOfficer.Top = 2.0625F;
			this.txtControlOfficer.Width = 2.4375F;
			// 
			// txtTownName1
			// 
			this.txtTownName1.Height = 0.1875F;
			this.txtTownName1.Left = 1.25F;
			this.txtTownName1.Name = "txtTownName1";
			this.txtTownName1.Text = null;
			this.txtTownName1.Top = 2.375F;
			this.txtTownName1.Width = 1.9375F;
			// 
			// txtTownName2
			// 
			this.txtTownName2.Height = 0.1875F;
			this.txtTownName2.Left = 5.1875F;
			this.txtTownName2.Name = "txtTownName2";
			this.txtTownName2.Text = null;
			this.txtTownName2.Top = 6.875F;
			this.txtTownName2.Width = 1.5625F;
			// 
			// txtTownCounty2
			// 
			this.txtTownCounty2.Height = 0.1875F;
			this.txtTownCounty2.Left = 4.125F;
			this.txtTownCounty2.Name = "txtTownCounty2";
			this.txtTownCounty2.Text = null;
			this.txtTownCounty2.Top = 6.25F;
			this.txtTownCounty2.Width = 1.5625F;
			// 
			// txtTownName4
			// 
			this.txtTownName4.Height = 0.1875F;
			this.txtTownName4.Left = 1.4375F;
			this.txtTownName4.Name = "txtTownName4";
			this.txtTownName4.Text = null;
			this.txtTownName4.Top = 6.25F;
			this.txtTownName4.Width = 1.5625F;
			// 
			// txtTownName3
			// 
			this.txtTownName3.Height = 0.1875F;
			this.txtTownName3.Left = 0.25F;
			this.txtTownName3.Name = "txtTownName3";
			this.txtTownName3.Text = null;
			this.txtTownName3.Top = 5.9375F;
			this.txtTownName3.Width = 1.5625F;
			// 
			// rptDogWarrant
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.927083F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTownCounty1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtControlOfficer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownCounty2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownCounty1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtControlOfficer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownCounty2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownName4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownName3;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
