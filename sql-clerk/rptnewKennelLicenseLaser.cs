//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptNewKennelLicenseLaser.
	/// </summary>
	public partial class rptNewKennelLicenseLaser : BaseSectionReport
	{
		public rptNewKennelLicenseLaser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNewKennelLicenseLaser InstancePtr
		{
			get
			{
				return (rptNewKennelLicenseLaser)Sys.GetInstance(typeof(rptNewKennelLicenseLaser));
			}
		}

		protected rptNewKennelLicenseLaser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewKennelLicenseLaser	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		int lngLicense;
		// vbPorter upgrade warning: dtIssueDate As DateTime	OnWrite(string)
		DateTime dtIssueDate;
		// Public Sub Init(lngLicenseNumber As Long, dtLicDate As Date)
		public void Init(int lngLicenseID, int lngOwnerNum)
		{
			rs.OpenRecordset("select * from kennellicense where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
			if (!rs.EndOfFile())
			{
				lngLicense = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("licenseid"))));
				if (Information.IsDate(rs.Get_Fields("reissuedate")))
				{
					dtIssueDate = (DateTime)rs.Get_Fields_DateTime("reissuedate");
					if (dtIssueDate.ToOADate() == 0)
					{
						dtIssueDate = FCConvert.ToDateTime(Strings.Format(rs.Get_Fields_DateTime("originalissuedate"), "MM/dd/yyyy"));
					}
					else
					{
						dtIssueDate = FCConvert.ToDateTime(Strings.Format(rs.Get_Fields_DateTime("reissuedate"), "MM/dd/yyyy"));
					}
				}
				txtExpYear.Text = rs.Get_Fields_String("year");
				rs.OpenRecordset("select * from doginventory where ID = " + FCConvert.ToString(lngLicense), "twck0000.vb1");
				if (!rs.EndOfFile())
				{
					lngLicense = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("stickernumber"))));
				}
				else
				{
					MessageBox.Show("Kennel license not found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("Kennel license not found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
				return;
			}
			rs.OpenRecordset("select * from dogowner inner join " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView as p on dogowner.partyid = p.Id where dogOwner.ID = " + FCConvert.ToString(lngOwnerNum), "twck0000.vb1");
            // lngLicense = lngLicenseNumber
            // dtIssueDate = dtLicDate
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			/*? On Error Resume Next  */
			if (rs.EndOfFile())
				return;
            txtOwnerName.Text = rs.Get_Fields_String("FullName"); //rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("MI") + " " + rs.Get_Fields_String("LastName");
            var phoneNumber = new string(rs.Get_Fields_String("PhoneNumber").Where(char.IsDigit).ToArray());

            txtPhone.Text = phoneNumber.Length == 7 
                ? $"(207) {Strings.Left(phoneNumber, 3)}-{Strings.Right(phoneNumber, 4)}" 
                : Strings.Format(phoneNumber, "(###) ###-####");

			// add the information to the required con
			// txtAddress = RS.Fields("Address") & "  " & RS.Fields("City") & ", " & GetDefaultStateName(Val(RS.Fields("State"))) & " " & IIf(Len(RS.Fields("Zip")) > 5, Left(RS.Fields("Zip"), 5) & "-" & Mid(RS.Fields("Zip"), 6, Len(RS.Fields("Zip")) - 5), RS.Fields("Zip"))
			txtAddress.Text = rs.Get_Fields_String("Address1") + "  " + rs.Get_Fields_String("City") + ", " + rs.Get_Fields("State") + " " + (FCConvert.ToString(rs.Get_Fields_String("Zip")).Length > 5 ? Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Zip")), 5) + "-" + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("Zip")), 6, FCConvert.ToString(rs.Get_Fields_String("Zip")).Length - 5) : rs.Get_Fields_String("Zip"));
			// txtLicenseNumber = GetKennelLicenseNumber(RS.Fields("K10Num"))
			txtLicenseNumber.Text = FCConvert.ToString(lngLicense);
			txtLocation.Text = rs.Get_Fields_String("LocationNum") + " " + rs.Get_Fields_String("LocationSTR");
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) == 0)
			{
				txtMuni.Text = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				txtMuni.Text = modRegionalTown.GetTownKeyName_2(rs.Get_Fields_Int32("towncode"));
			}
			// txtLicenseDate = Format(Date, "MM/dd/yyyy")
			txtLicenseDate.Text = Strings.Format(dtIssueDate, "MM/dd/yyyy");
			// txtDOB = RS.Fields("DOB")
			txtMuni2.Text = modGlobalConstants.Statics.MuniName;
			txtTownAddress.Text = modGNBas.Statics.ClerkDefaults.Address1 + "  " + modGNBas.Statics.ClerkDefaults.City + "  " + modGNBas.Statics.ClerkDefaults.Zip;
			// if this is not the end of the recordset
			// then move to the next record.
			if (!rs.EndOfFile())
				rs.MoveNext();
			// set the EOF flag for the report.
			eArgs.EOF = rs.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
		
		}
	}
}
