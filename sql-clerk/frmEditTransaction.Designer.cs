//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmEditTransaction.
	/// </summary>
	partial class frmEditTransaction
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLabels;
		public fecherFoundation.FCTextBox txtDogYear;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel lblTempLicenseAmount;
		public fecherFoundation.FCLabel lblTransferAmount;
		public fecherFoundation.FCLabel lblReplTagAmount;
		public fecherFoundation.FCLabel lblReplLicenseAmount;
		public fecherFoundation.FCLabel lblReplacementStickerAmount;
		public fecherFoundation.FCLabel lblLicenseAmount;
		public fecherFoundation.FCLabel lblWarrantAmount;
		public fecherFoundation.FCLabel lblLateBreakdownAmount;
		public fecherFoundation.FCLabel lblUserDefinedAmount;
		public fecherFoundation.FCLabel lblLateAmount;
		public fecherFoundation.FCLabel lblTownAmount;
		public fecherFoundation.FCLabel lblClerkAmount;
		public fecherFoundation.FCLabel lblStateAmount;
		public fecherFoundation.FCLabel lblTempLicense;
		public fecherFoundation.FCLabel lblTransferLicense;
		public fecherFoundation.FCLabel lblReplacementTag;
		public fecherFoundation.FCLabel lblReplacementLicense;
		public fecherFoundation.FCLabel lblReplacementSticker;
		public fecherFoundation.FCLabel lblLicense;
		public fecherFoundation.FCLabel lblWarrant;
		public fecherFoundation.FCLabel lblLateBreakdown;
		public fecherFoundation.FCLabel lblUserDefinedFee;
		public fecherFoundation.FCLabel lblLateFee;
		public fecherFoundation.FCLabel lblTownFee;
		public fecherFoundation.FCLabel lblClerkFee;
		public fecherFoundation.FCLabel lblStateFee;
		public fecherFoundation.FCCheckBox chkNewTag;
		public fecherFoundation.FCCheckBox chkSearchRescue;
		public fecherFoundation.FCCheckBox chkHearingGuid;
		public fecherFoundation.FCCheckBox chkNeuterSpay;
		public fecherFoundation.FCFrame framTransaction;
		public fecherFoundation.FCCheckBox chkTempLicense;
		public fecherFoundation.FCCheckBox chkReplacementTag;
		public fecherFoundation.FCCheckBox chkLateFee;
		public fecherFoundation.FCCheckBox chkWarrantFee;
		public fecherFoundation.FCCheckBox chkTransferLicense;
		public fecherFoundation.FCCheckBox chkReplacementLicense;
		public fecherFoundation.FCCheckBox chkReplacementSticker;
		public fecherFoundation.FCCheckBox chkLicense;
		public fecherFoundation.FCTextBox txtAmount;
		public fecherFoundation.FCCheckBox chkUserDefinedFee;
		public fecherFoundation.FCLabel lblLabels_1;
		public Global.T2KDateBox t2kTransactionDate;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblDogs;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.txtDogYear = new fecherFoundation.FCTextBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.lblTempLicenseAmount = new fecherFoundation.FCLabel();
            this.lblTransferAmount = new fecherFoundation.FCLabel();
            this.lblReplTagAmount = new fecherFoundation.FCLabel();
            this.lblReplLicenseAmount = new fecherFoundation.FCLabel();
            this.lblReplacementStickerAmount = new fecherFoundation.FCLabel();
            this.lblLicenseAmount = new fecherFoundation.FCLabel();
            this.lblWarrantAmount = new fecherFoundation.FCLabel();
            this.lblLateBreakdownAmount = new fecherFoundation.FCLabel();
            this.lblUserDefinedAmount = new fecherFoundation.FCLabel();
            this.lblLateAmount = new fecherFoundation.FCLabel();
            this.lblTownAmount = new fecherFoundation.FCLabel();
            this.lblClerkAmount = new fecherFoundation.FCLabel();
            this.lblStateAmount = new fecherFoundation.FCLabel();
            this.lblTempLicense = new fecherFoundation.FCLabel();
            this.lblTransferLicense = new fecherFoundation.FCLabel();
            this.lblReplacementTag = new fecherFoundation.FCLabel();
            this.lblReplacementLicense = new fecherFoundation.FCLabel();
            this.lblReplacementSticker = new fecherFoundation.FCLabel();
            this.lblLicense = new fecherFoundation.FCLabel();
            this.lblWarrant = new fecherFoundation.FCLabel();
            this.lblLateBreakdown = new fecherFoundation.FCLabel();
            this.lblUserDefinedFee = new fecherFoundation.FCLabel();
            this.lblLateFee = new fecherFoundation.FCLabel();
            this.lblTownFee = new fecherFoundation.FCLabel();
            this.lblClerkFee = new fecherFoundation.FCLabel();
            this.lblStateFee = new fecherFoundation.FCLabel();
            this.chkNewTag = new fecherFoundation.FCCheckBox();
            this.chkSearchRescue = new fecherFoundation.FCCheckBox();
            this.chkHearingGuid = new fecherFoundation.FCCheckBox();
            this.chkNeuterSpay = new fecherFoundation.FCCheckBox();
            this.framTransaction = new fecherFoundation.FCFrame();
            this.chkTempLicense = new fecherFoundation.FCCheckBox();
            this.chkReplacementTag = new fecherFoundation.FCCheckBox();
            this.chkLateFee = new fecherFoundation.FCCheckBox();
            this.chkWarrantFee = new fecherFoundation.FCCheckBox();
            this.chkTransferLicense = new fecherFoundation.FCCheckBox();
            this.chkReplacementLicense = new fecherFoundation.FCCheckBox();
            this.chkReplacementSticker = new fecherFoundation.FCCheckBox();
            this.chkLicense = new fecherFoundation.FCCheckBox();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.chkUserDefinedFee = new fecherFoundation.FCCheckBox();
            this.lblLabels_1 = new fecherFoundation.FCLabel();
            this.t2kTransactionDate = new Global.T2KDateBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblDogs = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.chkDangerous = new fecherFoundation.FCCheckBox();
            this.chkNuisance = new fecherFoundation.FCCheckBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSearchRescue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHearingGuid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNeuterSpay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framTransaction)).BeginInit();
            this.framTransaction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTempLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLateFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWarrantFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTransferLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserDefinedFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDangerous)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNuisance)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 568);
            this.BottomPanel.Size = new System.Drawing.Size(635, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkDangerous);
            this.ClientArea.Controls.Add(this.chkNuisance);
            this.ClientArea.Controls.Add(this.txtDogYear);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.chkNewTag);
            this.ClientArea.Controls.Add(this.chkSearchRescue);
            this.ClientArea.Controls.Add(this.chkHearingGuid);
            this.ClientArea.Controls.Add(this.chkNeuterSpay);
            this.ClientArea.Controls.Add(this.framTransaction);
            this.ClientArea.Controls.Add(this.t2kTransactionDate);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblDogs);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(635, 508);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(635, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(190, 30);
            this.HeaderText.Text = "Edit Transaction";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // txtDogYear
            // 
            this.txtDogYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtDogYear.Location = new System.Drawing.Point(180, 105);
            this.txtDogYear.Name = "txtDogYear";
            this.txtDogYear.Size = new System.Drawing.Size(113, 40);
            this.txtDogYear.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtDogYear, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.lblTempLicenseAmount);
            this.Frame1.Controls.Add(this.lblTransferAmount);
            this.Frame1.Controls.Add(this.lblReplTagAmount);
            this.Frame1.Controls.Add(this.lblReplLicenseAmount);
            this.Frame1.Controls.Add(this.lblReplacementStickerAmount);
            this.Frame1.Controls.Add(this.lblLicenseAmount);
            this.Frame1.Controls.Add(this.lblWarrantAmount);
            this.Frame1.Controls.Add(this.lblLateBreakdownAmount);
            this.Frame1.Controls.Add(this.lblUserDefinedAmount);
            this.Frame1.Controls.Add(this.lblLateAmount);
            this.Frame1.Controls.Add(this.lblTownAmount);
            this.Frame1.Controls.Add(this.lblClerkAmount);
            this.Frame1.Controls.Add(this.lblStateAmount);
            this.Frame1.Controls.Add(this.lblTempLicense);
            this.Frame1.Controls.Add(this.lblTransferLicense);
            this.Frame1.Controls.Add(this.lblReplacementTag);
            this.Frame1.Controls.Add(this.lblReplacementLicense);
            this.Frame1.Controls.Add(this.lblReplacementSticker);
            this.Frame1.Controls.Add(this.lblLicense);
            this.Frame1.Controls.Add(this.lblWarrant);
            this.Frame1.Controls.Add(this.lblLateBreakdown);
            this.Frame1.Controls.Add(this.lblUserDefinedFee);
            this.Frame1.Controls.Add(this.lblLateFee);
            this.Frame1.Controls.Add(this.lblTownFee);
            this.Frame1.Controls.Add(this.lblClerkFee);
            this.Frame1.Controls.Add(this.lblStateFee);
            this.Frame1.Location = new System.Drawing.Point(25, 551);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(575, 240);
            this.Frame1.TabIndex = 18;
            this.Frame1.Text = "Breakdown";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // lblTempLicenseAmount
            // 
            this.lblTempLicenseAmount.Location = new System.Drawing.Point(471, 205);
            this.lblTempLicenseAmount.Name = "lblTempLicenseAmount";
            this.lblTempLicenseAmount.Size = new System.Drawing.Size(70, 15);
            this.lblTempLicenseAmount.TabIndex = 44;
            this.lblTempLicenseAmount.Text = "0";
            this.lblTempLicenseAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblTempLicenseAmount, null);
            // 
            // lblTransferAmount
            // 
            this.lblTransferAmount.Location = new System.Drawing.Point(471, 180);
            this.lblTransferAmount.Name = "lblTransferAmount";
            this.lblTransferAmount.Size = new System.Drawing.Size(70, 15);
            this.lblTransferAmount.TabIndex = 43;
            this.lblTransferAmount.Text = "0";
            this.lblTransferAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblTransferAmount, null);
            // 
            // lblReplTagAmount
            // 
            this.lblReplTagAmount.Location = new System.Drawing.Point(471, 155);
            this.lblReplTagAmount.Name = "lblReplTagAmount";
            this.lblReplTagAmount.Size = new System.Drawing.Size(70, 15);
            this.lblReplTagAmount.TabIndex = 42;
            this.lblReplTagAmount.Text = "0";
            this.lblReplTagAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblReplTagAmount, null);
            // 
            // lblReplLicenseAmount
            // 
            this.lblReplLicenseAmount.Location = new System.Drawing.Point(471, 130);
            this.lblReplLicenseAmount.Name = "lblReplLicenseAmount";
            this.lblReplLicenseAmount.Size = new System.Drawing.Size(70, 15);
            this.lblReplLicenseAmount.TabIndex = 41;
            this.lblReplLicenseAmount.Text = "0";
            this.lblReplLicenseAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblReplLicenseAmount, null);
            // 
            // lblReplacementStickerAmount
            // 
            this.lblReplacementStickerAmount.Location = new System.Drawing.Point(471, 105);
            this.lblReplacementStickerAmount.Name = "lblReplacementStickerAmount";
            this.lblReplacementStickerAmount.Size = new System.Drawing.Size(70, 15);
            this.lblReplacementStickerAmount.TabIndex = 40;
            this.lblReplacementStickerAmount.Text = "0";
            this.lblReplacementStickerAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblReplacementStickerAmount, null);
            // 
            // lblLicenseAmount
            // 
            this.lblLicenseAmount.Location = new System.Drawing.Point(471, 80);
            this.lblLicenseAmount.Name = "lblLicenseAmount";
            this.lblLicenseAmount.Size = new System.Drawing.Size(70, 15);
            this.lblLicenseAmount.TabIndex = 39;
            this.lblLicenseAmount.Text = "0";
            this.lblLicenseAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblLicenseAmount, null);
            // 
            // lblWarrantAmount
            // 
            this.lblWarrantAmount.Location = new System.Drawing.Point(471, 55);
            this.lblWarrantAmount.Name = "lblWarrantAmount";
            this.lblWarrantAmount.Size = new System.Drawing.Size(70, 15);
            this.lblWarrantAmount.TabIndex = 38;
            this.lblWarrantAmount.Text = "0";
            this.lblWarrantAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblWarrantAmount, null);
            // 
            // lblLateBreakdownAmount
            // 
            this.lblLateBreakdownAmount.Location = new System.Drawing.Point(471, 30);
            this.lblLateBreakdownAmount.Name = "lblLateBreakdownAmount";
            this.lblLateBreakdownAmount.Size = new System.Drawing.Size(70, 15);
            this.lblLateBreakdownAmount.TabIndex = 37;
            this.lblLateBreakdownAmount.Text = "0";
            this.lblLateBreakdownAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblLateBreakdownAmount, null);
            // 
            // lblUserDefinedAmount
            // 
            this.lblUserDefinedAmount.Location = new System.Drawing.Point(154, 130);
            this.lblUserDefinedAmount.Name = "lblUserDefinedAmount";
            this.lblUserDefinedAmount.Size = new System.Drawing.Size(70, 15);
            this.lblUserDefinedAmount.TabIndex = 36;
            this.lblUserDefinedAmount.Text = "0";
            this.lblUserDefinedAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblUserDefinedAmount, null);
            // 
            // lblLateAmount
            // 
            this.lblLateAmount.Location = new System.Drawing.Point(154, 105);
            this.lblLateAmount.Name = "lblLateAmount";
            this.lblLateAmount.Size = new System.Drawing.Size(70, 15);
            this.lblLateAmount.TabIndex = 35;
            this.lblLateAmount.Text = "0";
            this.lblLateAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblLateAmount, null);
            // 
            // lblTownAmount
            // 
            this.lblTownAmount.Location = new System.Drawing.Point(154, 80);
            this.lblTownAmount.Name = "lblTownAmount";
            this.lblTownAmount.Size = new System.Drawing.Size(70, 15);
            this.lblTownAmount.TabIndex = 34;
            this.lblTownAmount.Text = "0";
            this.lblTownAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblTownAmount, null);
            // 
            // lblClerkAmount
            // 
            this.lblClerkAmount.Location = new System.Drawing.Point(154, 55);
            this.lblClerkAmount.Name = "lblClerkAmount";
            this.lblClerkAmount.Size = new System.Drawing.Size(70, 15);
            this.lblClerkAmount.TabIndex = 33;
            this.lblClerkAmount.Text = "0";
            this.lblClerkAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblClerkAmount, null);
            // 
            // lblStateAmount
            // 
            this.lblStateAmount.Location = new System.Drawing.Point(154, 30);
            this.lblStateAmount.Name = "lblStateAmount";
            this.lblStateAmount.Size = new System.Drawing.Size(70, 15);
            this.lblStateAmount.TabIndex = 32;
            this.lblStateAmount.Text = "0";
            this.lblStateAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblStateAmount, null);
            // 
            // lblTempLicense
            // 
            this.lblTempLicense.Location = new System.Drawing.Point(303, 205);
            this.lblTempLicense.Name = "lblTempLicense";
            this.lblTempLicense.Size = new System.Drawing.Size(128, 15);
            this.lblTempLicense.TabIndex = 31;
            this.lblTempLicense.Text = "TEMP LICENSE";
            this.ToolTip1.SetToolTip(this.lblTempLicense, null);
            // 
            // lblTransferLicense
            // 
            this.lblTransferLicense.Location = new System.Drawing.Point(303, 180);
            this.lblTransferLicense.Name = "lblTransferLicense";
            this.lblTransferLicense.Size = new System.Drawing.Size(128, 15);
            this.lblTransferLicense.TabIndex = 30;
            this.lblTransferLicense.Text = "TRANSFER LICENSE";
            this.ToolTip1.SetToolTip(this.lblTransferLicense, null);
            // 
            // lblReplacementTag
            // 
            this.lblReplacementTag.Location = new System.Drawing.Point(303, 155);
            this.lblReplacementTag.Name = "lblReplacementTag";
            this.lblReplacementTag.Size = new System.Drawing.Size(128, 15);
            this.lblReplacementTag.TabIndex = 29;
            this.lblReplacementTag.Text = "REPLACEMENT TAG";
            this.ToolTip1.SetToolTip(this.lblReplacementTag, null);
            // 
            // lblReplacementLicense
            // 
            this.lblReplacementLicense.Location = new System.Drawing.Point(303, 130);
            this.lblReplacementLicense.Name = "lblReplacementLicense";
            this.lblReplacementLicense.Size = new System.Drawing.Size(140, 15);
            this.lblReplacementLicense.TabIndex = 28;
            this.lblReplacementLicense.Text = "REPLACEMENT LICENSE";
            this.ToolTip1.SetToolTip(this.lblReplacementLicense, null);
            // 
            // lblReplacementSticker
            // 
            this.lblReplacementSticker.Location = new System.Drawing.Point(303, 105);
            this.lblReplacementSticker.Name = "lblReplacementSticker";
            this.lblReplacementSticker.Size = new System.Drawing.Size(160, 15);
            this.lblReplacementSticker.TabIndex = 27;
            this.lblReplacementSticker.Text = "REPLACEMENT STICKER FEE";
            this.ToolTip1.SetToolTip(this.lblReplacementSticker, null);
            // 
            // lblLicense
            // 
            this.lblLicense.Location = new System.Drawing.Point(303, 80);
            this.lblLicense.Name = "lblLicense";
            this.lblLicense.Size = new System.Drawing.Size(128, 15);
            this.lblLicense.TabIndex = 26;
            this.lblLicense.Text = "LICENSE";
            this.ToolTip1.SetToolTip(this.lblLicense, null);
            // 
            // lblWarrant
            // 
            this.lblWarrant.Location = new System.Drawing.Point(303, 55);
            this.lblWarrant.Name = "lblWarrant";
            this.lblWarrant.Size = new System.Drawing.Size(128, 15);
            this.lblWarrant.TabIndex = 25;
            this.lblWarrant.Text = "WARRANT";
            this.ToolTip1.SetToolTip(this.lblWarrant, null);
            // 
            // lblLateBreakdown
            // 
            this.lblLateBreakdown.Location = new System.Drawing.Point(303, 30);
            this.lblLateBreakdown.Name = "lblLateBreakdown";
            this.lblLateBreakdown.Size = new System.Drawing.Size(128, 15);
            this.lblLateBreakdown.TabIndex = 24;
            this.lblLateBreakdown.Text = "LATE";
            this.ToolTip1.SetToolTip(this.lblLateBreakdown, null);
            // 
            // lblUserDefinedFee
            // 
            this.lblUserDefinedFee.Location = new System.Drawing.Point(20, 130);
            this.lblUserDefinedFee.Name = "lblUserDefinedFee";
            this.lblUserDefinedFee.Size = new System.Drawing.Size(180, 30);
            this.lblUserDefinedFee.TabIndex = 23;
            this.lblUserDefinedFee.Text = "USER DEFINED FEE";
            this.ToolTip1.SetToolTip(this.lblUserDefinedFee, null);
            // 
            // lblLateFee
            // 
            this.lblLateFee.Location = new System.Drawing.Point(20, 105);
            this.lblLateFee.Name = "lblLateFee";
            this.lblLateFee.Size = new System.Drawing.Size(128, 15);
            this.lblLateFee.TabIndex = 22;
            this.lblLateFee.Text = "LATE FEE";
            this.ToolTip1.SetToolTip(this.lblLateFee, null);
            // 
            // lblTownFee
            // 
            this.lblTownFee.Location = new System.Drawing.Point(20, 80);
            this.lblTownFee.Name = "lblTownFee";
            this.lblTownFee.Size = new System.Drawing.Size(128, 15);
            this.lblTownFee.TabIndex = 21;
            this.lblTownFee.Text = "TOWN FEE";
            this.ToolTip1.SetToolTip(this.lblTownFee, null);
            // 
            // lblClerkFee
            // 
            this.lblClerkFee.Location = new System.Drawing.Point(20, 55);
            this.lblClerkFee.Name = "lblClerkFee";
            this.lblClerkFee.Size = new System.Drawing.Size(128, 15);
            this.lblClerkFee.TabIndex = 20;
            this.lblClerkFee.Text = "CLERK FEE";
            this.ToolTip1.SetToolTip(this.lblClerkFee, null);
            // 
            // lblStateFee
            // 
            this.lblStateFee.Location = new System.Drawing.Point(20, 30);
            this.lblStateFee.Name = "lblStateFee";
            this.lblStateFee.Size = new System.Drawing.Size(128, 15);
            this.lblStateFee.TabIndex = 19;
            this.lblStateFee.Text = "STATE FEE";
            this.ToolTip1.SetToolTip(this.lblStateFee, null);
            // 
            // chkNewTag
            // 
            this.chkNewTag.Location = new System.Drawing.Point(30, 155);
            this.chkNewTag.Name = "chkNewTag";
            this.chkNewTag.Size = new System.Drawing.Size(142, 24);
            this.chkNewTag.TabIndex = 2;
            this.chkNewTag.Text = "New Tag / License";
            this.ToolTip1.SetToolTip(this.chkNewTag, "Do not check this for re-registrations or replacement tags");
            // 
            // chkSearchRescue
            // 
            this.chkSearchRescue.Location = new System.Drawing.Point(30, 229);
            this.chkSearchRescue.Name = "chkSearchRescue";
            this.chkSearchRescue.Size = new System.Drawing.Size(213, 24);
            this.chkSearchRescue.TabIndex = 4;
            this.chkSearchRescue.Text = "Service - Search and  Rescue";
            this.ToolTip1.SetToolTip(this.chkSearchRescue, null);
            this.chkSearchRescue.CheckedChanged += new System.EventHandler(this.chkSearchRescue_CheckedChanged);
            // 
            // chkHearingGuid
            // 
            this.chkHearingGuid.Location = new System.Drawing.Point(30, 266);
            this.chkHearingGuid.Name = "chkHearingGuid";
            this.chkHearingGuid.Size = new System.Drawing.Size(125, 24);
            this.chkHearingGuid.TabIndex = 5;
            this.chkHearingGuid.Text = "Hearing / Guide";
            this.ToolTip1.SetToolTip(this.chkHearingGuid, null);
            this.chkHearingGuid.CheckedChanged += new System.EventHandler(this.chkHearingGuid_CheckedChanged);
            // 
            // chkNeuterSpay
            // 
            this.chkNeuterSpay.Location = new System.Drawing.Point(30, 192);
            this.chkNeuterSpay.Name = "chkNeuterSpay";
            this.chkNeuterSpay.Size = new System.Drawing.Size(127, 24);
            this.chkNeuterSpay.TabIndex = 3;
            this.chkNeuterSpay.Text = "Neuter / Spayed";
            this.ToolTip1.SetToolTip(this.chkNeuterSpay, null);
            this.chkNeuterSpay.CheckedChanged += new System.EventHandler(this.chkNeuterSpay_CheckedChanged);
            // 
            // framTransaction
            // 
            this.framTransaction.Controls.Add(this.chkTempLicense);
            this.framTransaction.Controls.Add(this.chkReplacementTag);
            this.framTransaction.Controls.Add(this.chkLateFee);
            this.framTransaction.Controls.Add(this.chkWarrantFee);
            this.framTransaction.Controls.Add(this.chkTransferLicense);
            this.framTransaction.Controls.Add(this.chkReplacementLicense);
            this.framTransaction.Controls.Add(this.chkReplacementSticker);
            this.framTransaction.Controls.Add(this.chkLicense);
            this.framTransaction.Controls.Add(this.txtAmount);
            this.framTransaction.Controls.Add(this.chkUserDefinedFee);
            this.framTransaction.Controls.Add(this.lblLabels_1);
            this.framTransaction.Location = new System.Drawing.Point(25, 303);
            this.framTransaction.Name = "framTransaction";
            this.framTransaction.Size = new System.Drawing.Size(574, 238);
            this.framTransaction.TabIndex = 16;
            this.framTransaction.Text = "Transaction Information";
            this.ToolTip1.SetToolTip(this.framTransaction, null);
            // 
            // chkTempLicense
            // 
            this.chkTempLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkTempLicense.Location = new System.Drawing.Point(311, 141);
            this.chkTempLicense.Name = "chkTempLicense";
            this.chkTempLicense.Size = new System.Drawing.Size(212, 24);
            this.chkTempLicense.TabIndex = 14;
            this.chkTempLicense.Text = "Temporary License (10 Days)";
            this.ToolTip1.SetToolTip(this.chkTempLicense, null);
            this.chkTempLicense.CheckedChanged += new System.EventHandler(this.chkTempLicense_CheckedChanged);
            // 
            // chkReplacementTag
            // 
            this.chkReplacementTag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkReplacementTag.Location = new System.Drawing.Point(20, 141);
            this.chkReplacementTag.Name = "chkReplacementTag";
            this.chkReplacementTag.Size = new System.Drawing.Size(164, 24);
            this.chkReplacementTag.TabIndex = 9;
            this.chkReplacementTag.Text = "Replacement Tag Fee";
            this.ToolTip1.SetToolTip(this.chkReplacementTag, null);
            this.chkReplacementTag.CheckedChanged += new System.EventHandler(this.chkReplacementTag_CheckedChanged);
            // 
            // chkLateFee
            // 
            this.chkLateFee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkLateFee.Location = new System.Drawing.Point(20, 67);
            this.chkLateFee.Name = "chkLateFee";
            this.chkLateFee.Size = new System.Drawing.Size(80, 24);
            this.chkLateFee.TabIndex = 7;
            this.chkLateFee.Text = "Late Fee";
            this.ToolTip1.SetToolTip(this.chkLateFee, null);
            this.chkLateFee.CheckedChanged += new System.EventHandler(this.chkLateFee_CheckedChanged);
            // 
            // chkWarrantFee
            // 
            this.chkWarrantFee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkWarrantFee.Location = new System.Drawing.Point(20, 104);
            this.chkWarrantFee.Name = "chkWarrantFee";
            this.chkWarrantFee.Size = new System.Drawing.Size(103, 24);
            this.chkWarrantFee.TabIndex = 8;
            this.chkWarrantFee.Text = "Warrant Fee";
            this.ToolTip1.SetToolTip(this.chkWarrantFee, null);
            this.chkWarrantFee.CheckedChanged += new System.EventHandler(this.chkWarrantFee_CheckedChanged);
            // 
            // chkTransferLicense
            // 
            this.chkTransferLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkTransferLicense.Location = new System.Drawing.Point(311, 104);
            this.chkTransferLicense.Name = "chkTransferLicense";
            this.chkTransferLicense.Size = new System.Drawing.Size(133, 24);
            this.chkTransferLicense.TabIndex = 13;
            this.chkTransferLicense.Text = "Transfer License";
            this.ToolTip1.SetToolTip(this.chkTransferLicense, null);
            this.chkTransferLicense.CheckedChanged += new System.EventHandler(this.chkTransferLicense_CheckedChanged);
            // 
            // chkReplacementLicense
            // 
            this.chkReplacementLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkReplacementLicense.Location = new System.Drawing.Point(311, 67);
            this.chkReplacementLicense.Name = "chkReplacementLicense";
            this.chkReplacementLicense.Size = new System.Drawing.Size(163, 24);
            this.chkReplacementLicense.TabIndex = 12;
            this.chkReplacementLicense.Text = "Replacement License";
            this.ToolTip1.SetToolTip(this.chkReplacementLicense, null);
            this.chkReplacementLicense.CheckedChanged += new System.EventHandler(this.chkReplacementLicense_CheckedChanged);
            // 
            // chkReplacementSticker
            // 
            this.chkReplacementSticker.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkReplacementSticker.Location = new System.Drawing.Point(311, 30);
            this.chkReplacementSticker.Name = "chkReplacementSticker";
            this.chkReplacementSticker.Size = new System.Drawing.Size(158, 24);
            this.chkReplacementSticker.TabIndex = 11;
            this.chkReplacementSticker.Text = "Replacement Sticker";
            this.ToolTip1.SetToolTip(this.chkReplacementSticker, null);
            this.chkReplacementSticker.CheckedChanged += new System.EventHandler(this.chkReplacementSticker_CheckedChanged);
            // 
            // chkLicense
            // 
            this.chkLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkLicense.Location = new System.Drawing.Point(20, 30);
            this.chkLicense.Name = "chkLicense";
            this.chkLicense.Size = new System.Drawing.Size(76, 24);
            this.chkLicense.TabIndex = 6;
            this.chkLicense.Text = "License";
            this.ToolTip1.SetToolTip(this.chkLicense, null);
            this.chkLicense.CheckedChanged += new System.EventHandler(this.chkLicense_CheckedChanged);
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAmount.Location = new System.Drawing.Point(456, 178);
            this.txtAmount.LockedOriginal = true;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Size = new System.Drawing.Size(98, 40);
            this.txtAmount.TabIndex = 15;
            this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtAmount, null);
            // 
            // chkUserDefinedFee
            // 
            this.chkUserDefinedFee.AutoSize = false;
            this.chkUserDefinedFee.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.chkUserDefinedFee.Location = new System.Drawing.Point(20, 178);
            this.chkUserDefinedFee.Name = "chkUserDefinedFee";
            this.chkUserDefinedFee.Size = new System.Drawing.Size(220, 50);
            this.chkUserDefinedFee.TabIndex = 10;
            this.chkUserDefinedFee.Text = "User Defined Fee";
            this.chkUserDefinedFee.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ToolTip1.SetToolTip(this.chkUserDefinedFee, null);
            this.chkUserDefinedFee.CheckedChanged += new System.EventHandler(this.chkUserDefinedFee_CheckedChanged);
            // 
            // lblLabels_1
            // 
            this.lblLabels_1.Location = new System.Drawing.Point(311, 192);
            this.lblLabels_1.Name = "lblLabels_1";
            this.lblLabels_1.Size = new System.Drawing.Size(136, 15);
            this.lblLabels_1.TabIndex = 17;
            this.lblLabels_1.Text = "AMOUNT CHARGED";
            this.ToolTip1.SetToolTip(this.lblLabels_1, null);
            // 
            // t2kTransactionDate
            // 
            this.t2kTransactionDate.Location = new System.Drawing.Point(180, 55);
            this.t2kTransactionDate.Name = "t2kTransactionDate";
            this.t2kTransactionDate.Size = new System.Drawing.Size(115, 22);
            this.ToolTip1.SetToolTip(this.t2kTransactionDate, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 119);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(114, 15);
            this.Label4.TabIndex = 50;
            this.Label4.Text = "DOG YEAR";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 69);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(125, 15);
            this.Label3.TabIndex = 49;
            this.Label3.Text = "TRANSACTION DATE";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // lblDogs
            // 
            this.lblDogs.Location = new System.Drawing.Point(390, 30);
            this.lblDogs.Name = "lblDogs";
            this.lblDogs.Size = new System.Drawing.Size(209, 52);
            this.lblDogs.TabIndex = 48;
            this.ToolTip1.SetToolTip(this.lblDogs, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(313, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(50, 18);
            this.Label2.TabIndex = 47;
            this.Label2.Text = "DOG(S)";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(94, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(195, 15);
            this.lblName.TabIndex = 46;
            this.ToolTip1.SetToolTip(this.lblName, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(50, 15);
            this.Label1.TabIndex = 45;
            this.Label1.Text = "OWNER";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(261, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // chkDangerous
            // 
            this.chkDangerous.Location = new System.Drawing.Point(313, 155);
            this.chkDangerous.Name = "chkDangerous";
            this.chkDangerous.Size = new System.Drawing.Size(96, 24);
            this.chkDangerous.TabIndex = 6;
            this.chkDangerous.Text = "Dangerous";
            // 
            // chkNuisance
            // 
            this.chkNuisance.Location = new System.Drawing.Point(313, 192);
            this.chkNuisance.Name = "chkNuisance";
            this.chkNuisance.Size = new System.Drawing.Size(85, 24);
            this.chkNuisance.TabIndex = 7;
            this.chkNuisance.Text = "Nuisance";
            // 
            // frmEditTransaction
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(635, 676);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEditTransaction";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit Transaction";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmEditTransaction_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditTransaction_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkNewTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSearchRescue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHearingGuid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNeuterSpay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framTransaction)).EndInit();
            this.framTransaction.ResumeLayout(false);
            this.framTransaction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTempLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLateFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWarrantFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTransferLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserDefinedFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDangerous)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNuisance)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private System.ComponentModel.IContainer components;
        public FCCheckBox chkDangerous;
        public FCCheckBox chkNuisance;
    }
}