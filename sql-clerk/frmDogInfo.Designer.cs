//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDogInfo.
	/// </summary>
	partial class frmDogInfo
	{
		public System.Collections.Generic.List<T2KBackFillDecimal> mebDogFees;
		public System.Collections.Generic.List<T2KBackFillDecimal> mebKennelFees;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCGrid ColorGrid;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCGrid BreedGrid;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkLateFeeOnServiceDog;
		public FCGrid Grid;
		public Global.T2KBackFillDecimal mebDogFees_1;
		public Global.T2KBackFillDecimal mebDogFees_2;
		public Global.T2KBackFillDecimal mebDogFees_3;
		public Global.T2KBackFillDecimal mebDogFees_4;
		public Global.T2KBackFillDecimal mebDogFees_0;
		public Global.T2KBackFillDecimal mebKennelFees_0;
		public Global.T2KBackFillDecimal mebKennelFees_1;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtCustomFeeDesc;
		public fecherFoundation.FCTextBox txtCustomFeeAmount;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCGrid VetGrid;
		public fecherFoundation.FCLabel lblNotes;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDogInfo));
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.ColorGrid = new fecherFoundation.FCGrid();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.BreedGrid = new fecherFoundation.FCGrid();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtCustomFeeDesc = new fecherFoundation.FCTextBox();
            this.txtCustomFeeAmount = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtDangerousLateFee = new Global.T2KBackFillDecimal();
            this.txtNuisanceLateFee = new Global.T2KBackFillDecimal();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.chkLateFeeOnServiceDog = new fecherFoundation.FCCheckBox();
            this.Grid = new fecherFoundation.FCGrid();
            this.mebDogFees_1 = new Global.T2KBackFillDecimal();
            this.mebDogFees_2 = new Global.T2KBackFillDecimal();
            this.mebDogFees_3 = new Global.T2KBackFillDecimal();
            this.mebDogFees_4 = new Global.T2KBackFillDecimal();
            this.mebDogFees_0 = new Global.T2KBackFillDecimal();
            this.mebKennelFees_0 = new Global.T2KBackFillDecimal();
            this.mebKennelFees_1 = new Global.T2KBackFillDecimal();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
            this.VetGrid = new fecherFoundation.FCGrid();
            this.lblNotes = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ColorGrid)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BreedGrid)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerousLateFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceLateFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLateFeeOnServiceDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebKennelFees_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebKennelFees_1)).BeginInit();
            this.SSTab1_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VetGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 482);
            this.BottomPanel.Size = new System.Drawing.Size(998, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblNotes);
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Size = new System.Drawing.Size(998, 422);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Size = new System.Drawing.Size(998, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(187, 30);
            this.HeaderText.Text = "Dog Information";
            // 
            // SSTab1
            // 
            this.SSTab1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Controls.Add(this.SSTab1_Page4);
            this.SSTab1.Location = new System.Drawing.Point(20, 55);
            this.SSTab1.MinimumSize = new System.Drawing.Size(900, 455);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.SSTab1.Size = new System.Drawing.Size(941, 673);
            this.SSTab1.TabIndex = 12;
            this.SSTab1.TabStop = false;
            this.SSTab1.Text = "Veterinarians";
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.ColorGrid);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Size = new System.Drawing.Size(939, 625);
            this.SSTab1_Page1.Text = "Colors";
            // 
            // ColorGrid
            // 
            this.ColorGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.ColorGrid.Cols = 10;
            this.ColorGrid.ExtendLastCol = true;
            this.ColorGrid.Location = new System.Drawing.Point(20, 20);
            this.ColorGrid.Name = "ColorGrid";
            this.ColorGrid.Rows = 50;
            this.ColorGrid.Size = new System.Drawing.Size(800, 582);
            this.ColorGrid.StandardTab = false;
            this.ColorGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.ColorGrid.TabIndex = 11;
            this.ColorGrid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.ColorGrid_ChangeEdit);
            this.ColorGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.ColorGrid_ValidateEdit);
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.BreedGrid);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Size = new System.Drawing.Size(939, 625);
            this.SSTab1_Page2.Text = "Breeds";
            // 
            // BreedGrid
            // 
            this.BreedGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.BreedGrid.Cols = 10;
            this.BreedGrid.ExtendLastCol = true;
            this.BreedGrid.Location = new System.Drawing.Point(20, 20);
            this.BreedGrid.Name = "BreedGrid";
            this.BreedGrid.Rows = 50;
            this.BreedGrid.Size = new System.Drawing.Size(800, 588);
            this.BreedGrid.StandardTab = false;
            this.BreedGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.BreedGrid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.BreedGrid_ChangeEdit);
            this.BreedGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.BreedGrid_ValidateEdit);
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.Frame1);
            this.SSTab1_Page3.Controls.Add(this.Frame2);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Size = new System.Drawing.Size(939, 625);
            this.SSTab1_Page3.Text = "Fees";
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.txtCustomFeeDesc);
            this.Frame1.Controls.Add(this.txtCustomFeeAmount);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Location = new System.Drawing.Point(20, 515);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(459, 107);
            this.Frame1.TabIndex = 20;
            this.Frame1.Text = "User Defined Dog Fee";
            // 
            // txtCustomFeeDesc
            // 
            this.txtCustomFeeDesc.BackColor = System.Drawing.SystemColors.Window;
            this.txtCustomFeeDesc.Location = new System.Drawing.Point(20, 50);
            this.txtCustomFeeDesc.Name = "txtCustomFeeDesc";
            this.txtCustomFeeDesc.Size = new System.Drawing.Size(267, 40);
            this.txtCustomFeeDesc.TabIndex = 8;
            this.txtCustomFeeDesc.TextChanged += new System.EventHandler(this.txtCustomFeeDesc_TextChanged);
            // 
            // txtCustomFeeAmount
            // 
            this.txtCustomFeeAmount.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtCustomFeeAmount, resources.GetString("txtCustomFeeAmount.JavaScript"));
            this.txtCustomFeeAmount.Location = new System.Drawing.Point(307, 50);
            this.txtCustomFeeAmount.Name = "txtCustomFeeAmount";
            this.txtCustomFeeAmount.Size = new System.Drawing.Size(134, 40);
            this.txtCustomFeeAmount.TabIndex = 9;
            this.txtCustomFeeAmount.TextChanged += new System.EventHandler(this.txtCustomFeeAmount_TextChanged);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(307, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(99, 15);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "AMOUNT";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(267, 15);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "DESCRIPTION";
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.White;
            this.Frame2.Controls.Add(this.txtDangerousLateFee);
            this.Frame2.Controls.Add(this.txtNuisanceLateFee);
            this.Frame2.Controls.Add(this.fcLabel1);
            this.Frame2.Controls.Add(this.fcLabel2);
            this.Frame2.Controls.Add(this.chkLateFeeOnServiceDog);
            this.Frame2.Controls.Add(this.Grid);
            this.Frame2.Controls.Add(this.mebDogFees_1);
            this.Frame2.Controls.Add(this.mebDogFees_2);
            this.Frame2.Controls.Add(this.mebDogFees_3);
            this.Frame2.Controls.Add(this.mebDogFees_4);
            this.Frame2.Controls.Add(this.mebDogFees_0);
            this.Frame2.Controls.Add(this.mebKennelFees_0);
            this.Frame2.Controls.Add(this.mebKennelFees_1);
            this.Frame2.Controls.Add(this.Label1_9);
            this.Frame2.Controls.Add(this.Label1_10);
            this.Frame2.Controls.Add(this.Label1_6);
            this.Frame2.Controls.Add(this.Label1_3);
            this.Frame2.Controls.Add(this.Label1_2);
            this.Frame2.Controls.Add(this.Label1_1);
            this.Frame2.Controls.Add(this.Label1_0);
            this.Frame2.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
            this.Frame2.Location = new System.Drawing.Point(20, 20);
            this.Frame2.MinimumSize = new System.Drawing.Size(699, 390);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(899, 480);
            this.Frame2.TabIndex = 13;
            this.Frame2.Text = "Dog Fees";
            // 
            // txtDangerousLateFee
            // 
            this.txtDangerousLateFee.Location = new System.Drawing.Point(205, 379);
            this.txtDangerousLateFee.MaxLength = 6;
            this.txtDangerousLateFee.Name = "txtDangerousLateFee";
            this.txtDangerousLateFee.Size = new System.Drawing.Size(138, 40);
            this.txtDangerousLateFee.TabIndex = 8;
            // 
            // txtNuisanceLateFee
            // 
            this.txtNuisanceLateFee.Location = new System.Drawing.Point(205, 429);
            this.txtNuisanceLateFee.MaxLength = 6;
            this.txtNuisanceLateFee.Name = "txtNuisanceLateFee";
            this.txtNuisanceLateFee.Size = new System.Drawing.Size(138, 40);
            this.txtNuisanceLateFee.TabIndex = 9;
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(20, 393);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(161, 17);
            this.fcLabel1.TabIndex = 30;
            this.fcLabel1.Text = "DANGEROUS LATE FEE";
            // 
            // fcLabel2
            // 
            this.fcLabel2.Location = new System.Drawing.Point(20, 443);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(143, 17);
            this.fcLabel2.TabIndex = 29;
            this.fcLabel2.Text = "NUISANCE LATE FEE";
            // 
            // chkLateFeeOnServiceDog
            // 
            this.chkLateFeeOnServiceDog.Location = new System.Drawing.Point(363, 30);
            this.chkLateFeeOnServiceDog.Name = "chkLateFeeOnServiceDog";
            this.chkLateFeeOnServiceDog.Size = new System.Drawing.Size(333, 27);
            this.chkLateFeeOnServiceDog.TabIndex = 26;
            this.chkLateFeeOnServiceDog.Text = "Allow Late Fee on Service and Guide Dog";
            this.chkLateFeeOnServiceDog.CheckedChanged += new System.EventHandler(this.chkLateFeeOnServiceDog_CheckedChanged);
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 4;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.ExtendLastCol = true;
            this.Grid.Location = new System.Drawing.Point(366, 78);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = false;
            this.Grid.Rows = 6;
            this.Grid.Size = new System.Drawing.Size(479, 262);
            this.Grid.StandardTab = false;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid.TabIndex = 10;
            this.Grid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_ChangeEdit);
            // 
            // mebDogFees_1
            // 
            this.mebDogFees_1.Location = new System.Drawing.Point(205, 80);
            this.mebDogFees_1.MaxLength = 8;
            this.mebDogFees_1.Name = "mebDogFees_1";
            this.mebDogFees_1.Size = new System.Drawing.Size(138, 40);
            this.mebDogFees_1.TabIndex = 2;
            this.mebDogFees_1.TextChanged += new System.EventHandler(this.mebDogFees_Change);
            // 
            // mebDogFees_2
            // 
            this.mebDogFees_2.Location = new System.Drawing.Point(205, 130);
            this.mebDogFees_2.MaxLength = 8;
            this.mebDogFees_2.Name = "mebDogFees_2";
            this.mebDogFees_2.Size = new System.Drawing.Size(138, 40);
            this.mebDogFees_2.TabIndex = 3;
            this.mebDogFees_2.TextChanged += new System.EventHandler(this.mebDogFees_Change);
            // 
            // mebDogFees_3
            // 
            this.mebDogFees_3.Location = new System.Drawing.Point(205, 180);
            this.mebDogFees_3.MaxLength = 8;
            this.mebDogFees_3.Name = "mebDogFees_3";
            this.mebDogFees_3.Size = new System.Drawing.Size(138, 40);
            this.mebDogFees_3.TabIndex = 4;
            this.mebDogFees_3.TextChanged += new System.EventHandler(this.mebDogFees_Change);
            // 
            // mebDogFees_4
            // 
            this.mebDogFees_4.Location = new System.Drawing.Point(205, 230);
            this.mebDogFees_4.MaxLength = 8;
            this.mebDogFees_4.Name = "mebDogFees_4";
            this.mebDogFees_4.Size = new System.Drawing.Size(138, 40);
            this.mebDogFees_4.TabIndex = 5;
            this.mebDogFees_4.TextChanged += new System.EventHandler(this.mebDogFees_Change);
            // 
            // mebDogFees_0
            // 
            this.mebDogFees_0.Location = new System.Drawing.Point(205, 30);
            this.mebDogFees_0.MaxLength = 8;
            this.mebDogFees_0.Name = "mebDogFees_0";
            this.mebDogFees_0.Size = new System.Drawing.Size(138, 40);
            this.mebDogFees_0.TabIndex = 1;
            this.mebDogFees_0.TextChanged += new System.EventHandler(this.mebDogFees_Change);
            // 
            // mebKennelFees_0
            // 
            this.mebKennelFees_0.Location = new System.Drawing.Point(205, 280);
            this.mebKennelFees_0.MaxLength = 6;
            this.mebKennelFees_0.Name = "mebKennelFees_0";
            this.mebKennelFees_0.Size = new System.Drawing.Size(138, 40);
            this.mebKennelFees_0.TabIndex = 6;
            // 
            // mebKennelFees_1
            // 
            this.mebKennelFees_1.Location = new System.Drawing.Point(205, 330);
            this.mebKennelFees_1.MaxLength = 6;
            this.mebKennelFees_1.Name = "mebKennelFees_1";
            this.mebKennelFees_1.Size = new System.Drawing.Size(138, 40);
            this.mebKennelFees_1.TabIndex = 7;
            // 
            // Label1_9
            // 
            this.Label1_9.Location = new System.Drawing.Point(20, 294);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(161, 17);
            this.Label1_9.TabIndex = 24;
            this.Label1_9.Text = "KENNEL WARRANT FEE";
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(20, 344);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(143, 17);
            this.Label1_10.TabIndex = 23;
            this.Label1_10.Text = "KENNEL LATE FEE";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(20, 244);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(161, 17);
            this.Label1_6.TabIndex = 18;
            this.Label1_6.Text = "REPLACEMENT TAG FEE";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(20, 194);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(143, 17);
            this.Label1_3.TabIndex = 17;
            this.Label1_3.Text = "TRANSFER";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(20, 144);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(143, 17);
            this.Label1_2.TabIndex = 16;
            this.Label1_2.Text = "REPLACEMENT";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(20, 94);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(143, 17);
            this.Label1_1.TabIndex = 15;
            this.Label1_1.Text = "WARRANT FEE";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(20, 44);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(143, 17);
            this.Label1_0.TabIndex = 14;
            this.Label1_0.Text = "LATE FEE";
            // 
            // SSTab1_Page4
            // 
            this.SSTab1_Page4.Controls.Add(this.VetGrid);
            this.SSTab1_Page4.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page4.Name = "SSTab1_Page4";
            this.SSTab1_Page4.Size = new System.Drawing.Size(939, 625);
            this.SSTab1_Page4.Text = "Veterinarians";
            // 
            // VetGrid
            // 
            this.VetGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.VetGrid.ExtendLastCol = true;
            this.VetGrid.Location = new System.Drawing.Point(20, 20);
            this.VetGrid.Name = "VetGrid";
            this.VetGrid.Rows = 50;
            this.VetGrid.Size = new System.Drawing.Size(800, 582);
            this.VetGrid.StandardTab = false;
            this.VetGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.VetGrid.TabIndex = 25;
            this.VetGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.VetGrid_ValidateEdit);
            // 
            // lblNotes
            // 
            this.lblNotes.Location = new System.Drawing.Point(30, 30);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(660, 15);
            this.lblNotes.TabIndex = 19;
            this.lblNotes.Text = "MENU OPTIONS SUCH AS NEW, SAVE AND DELETE WILL ONLY WORK FOR THE ACTIVE VISIBLE T" +
    "AB";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 2;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                  ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit        ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 5;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(335, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(876, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(45, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(927, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // frmDogInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(998, 590);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDogInfo";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Dog Information";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDogInfo_Load);
            this.Resize += new System.EventHandler(this.frmDogInfo_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDogInfo_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDogInfo_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ColorGrid)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BreedGrid)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerousLateFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisanceLateFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLateFeeOnServiceDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDogFees_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebKennelFees_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebKennelFees_1)).EndInit();
            this.SSTab1_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VetGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdNew;
		private FCButton cmdDelete;
        public T2KBackFillDecimal txtDangerousLateFee;
        public T2KBackFillDecimal txtNuisanceLateFee;
        public FCLabel fcLabel1;
        public FCLabel fcLabel2;
        private JavaScript javaScript1;
        private System.ComponentModel.IContainer components;
    }
}