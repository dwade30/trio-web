//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmGroupListing : BaseForm
	{
		public frmGroupListing()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmGroupListing InstancePtr
		{
			get
			{
				return (frmGroupListing)Sys.GetInstance(typeof(frmGroupListing));
			}
		}

		protected frmGroupListing _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmGroupListing_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmGroupListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGroupListing properties;
			//frmGroupListing.FillStyle	= 0;
			//frmGroupListing.ScaleWidth	= 3885;
			//frmGroupListing.ScaleHeight	= 1995;
			//frmGroupListing.LinkTopic	= "Form2";
			//frmGroupListing.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			LoadGroupCombo();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmGroupListing_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadGroupCombo()
		{
			clsDRWrapper rs = new clsDRWrapper();
			cboSelectedGroup.Clear();
			rs.OpenRecordset("select * from grouplist order by groupname", "twck0000.vb1");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					cboSelectedGroup.AddItem(rs.Get_Fields_String("GroupName"));
					cboSelectedGroup.ItemData(cboSelectedGroup.NewIndex, FCConvert.ToInt32(rs.Get_Fields_Int32("ID")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				rptCommittee.InstancePtr.Init();
			}
			else
			{
				rptCommittee.InstancePtr.Init(cboSelectedGroup.ItemData(cboSelectedGroup.SelectedIndex));
			}
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				cboSelectedGroup.SelectedIndex = -1;
				cboSelectedGroup.Enabled = false;
			}
		}

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAll.Text == "Selected")
			{
				cboSelectedGroup.Enabled = true;
				if (cboSelectedGroup.Items.Count > 0)
				{
					cboSelectedGroup.SelectedIndex = 0;
				}
			}
		}

		private void cmbAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Selected")
			{
				optSelected_CheckedChanged(sender, e);
			}
		}
	}
}
