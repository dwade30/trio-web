//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDeathReport.
	/// </summary>
	public partial class rptDeathReport : BaseSectionReport
	{
		public rptDeathReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Death Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDeathReport InstancePtr
		{
			get
			{
				return (rptDeathReport)Sys.GetInstance(typeof(rptDeathReport));
			}
		}

		protected rptDeathReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDeathReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();

		public void Init(ref string strSQL)
		{
			rsReport.OpenRecordset(strSQL, "twck0000.vb1");
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(this, strAttachmentName: "DeathReport");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Information.IsDate(rsReport.Get_Fields("dateofdeath")))
			{
				if (Convert.ToDateTime(rsReport.Get_Fields("dateofdeath")).ToOADate() != 0)
				{
					txtDeathDate.Text = Strings.Format(rsReport.Get_Fields("dateofdeath"), "MM/dd/yyyy");
				}
				else
				{
					txtDeathDate.Text = rsReport.Get_Fields_String("dateofdeathdescription");
				}
			}
			else
			{
				txtDeathDate.Text = rsReport.Get_Fields_String("dateofdeathdescription");
			}
			string strTemp;
			strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("middlename")));
			if (strTemp != string.Empty)
				strTemp = Strings.Left(strTemp, 1);
			txtName.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("firstname") + " " + strTemp) + " " + rsReport.Get_Fields_String("Lastname");
			txtAge.Text = rsReport.Get_Fields_String("age");
			txtSex.Text = rsReport.Get_Fields_String("sex");
			txtDisposition.Text = rsReport.Get_Fields_String("Placeofdisposition");
			rsReport.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
