//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmPrinterSettings : BaseForm
	{

		public List<fecherFoundation.FCButton> cmdPreview;
		public frmPrinterSettings()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			cmdPreview = new System.Collections.Generic.List<FCButton>();
			cmdPreview.AddControlArrayElement(cmdPreview_0, 0);
			cmdPreview.AddControlArrayElement(cmdPreview_1, 1);
			cmdPreview.AddControlArrayElement(cmdPreview_2, 2);
			cmdPreview.AddControlArrayElement(cmdPreview_3, 3);
			cmdPreview.AddControlArrayElement(cmdPreview_4, 4);
			cmdPreview.AddControlArrayElement(cmdPreview_5, 5);
			cmdPreview.AddControlArrayElement(cmdPreview_6, 6);
			cmdPreview.AddControlArrayElement(cmdPreview_7, 7);
			cmdPreview.AddControlArrayElement(cmdPreview_8, 8);
			cmdPreview.AddControlArrayElement(cmdPreview_9, 9);
			cmdPreview.AddControlArrayElement(cmdPreview_10, 10);
			//FC:FINAL:DDU:#2451 - allow only numerics
			this.txtYear.AllowOnlyNumericInput();
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrinterSettings InstancePtr
		{
			get
			{
				return (frmPrinterSettings)Sys.GetInstance(typeof(frmPrinterSettings));
			}
		}

		protected frmPrinterSettings _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsPrinter = new clsDRWrapper();
		bool boolSavingAndExiting;
		private int intDataChanged;

		private void LoadData()
		{
			// GET THE DATA FROM THE TABLE PRINTERSETTINGS
			rsPrinter.OpenRecordset("Select * from PrinterSettings", modGNBas.DEFAULTDATABASE);
			if (!rsPrinter.EndOfFile())
			{
				cmbDogLicense.SelectedIndex = FCConvert.ToInt32(rsPrinter.Get_Fields_Int16("DogLicense"));
				cmbKennelLicense.SelectedIndex = FCConvert.ToInt32(rsPrinter.Get_Fields_Int16("KennelLicense"));
				cmbMonthlyReport.SelectedIndex = FCConvert.ToInt32(rsPrinter.Get_Fields_Int16("MonthlyLicense"));
                txtBirthAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("BirthAdjustment", "CK")));
				txtDeathAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("DeathAdjustment", "CK")));
				txtNCDeath.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("NonConfidentialDeathAdjustment", "CK")));
				txtMarriageAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("MarriageAdjustment", "CK")));
				txtMarriageLicenseAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("MarriageLicenseAdjustment", "CK")));
				txtMarriageIntentionsAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("MarriageIntentionsAdjustment", "CK")));
				txtDogAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("DogAdjustment", "CK")));
				txtBurialAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("BurialAdjustment", "CK")));
				txtKennelAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("KennelAdjustment", "CK")));
				txtDogReminder.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("DogRemindersAdjustment", "CK")));
				// txtBirthAdjustment.Text = rsPrinter.Fields("BirthAdjustment")
				// txtDeathAdjustment.Text = rsPrinter.Fields("DeathAdjustment")
				// txtMarriageAdjustment.Text = rsPrinter.Fields("MarriageAdjustment")
				// txtMarriageLicenseAdjustment.Text = rsPrinter.Fields("MarriageLicenseAdjustment")
				// txtDogAdjustment.Text = rsPrinter.Fields("DogAdjustment")
				// txtBurialAdjustment.Text = rsPrinter.Fields("BurialAdjustment")
				// txtKennelAdjustment.Text = rsPrinter.Fields("KennelAdjustment")
				// txtDogReminder.Text = Val(rsPrinter.Fields("dogremindersadjustment"))
				if (rsPrinter.Get_Fields_Boolean("PrintDogReRegOnFront") && cmbDogLicense.Text == "Blank Form")
				{
					chkPrintOnFront.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkPrintOnFront.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (rsPrinter.Get_Fields_Boolean("PrintDogReRegExpiration") && cmbDogLicense.Text == "Blank Form")
				{
					chkPrintExpiration.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkPrintExpiration.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsPrinter.Get_Fields_Boolean("PrintMonthlyDogOptionalSection")))
				{
					chkPrintOptionalSection.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkPrintOptionalSection.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtYear.Text = FCConvert.ToString(rsPrinter.Get_Fields_String("LicenseYear"));
				txtDate.Text = Strings.Format(rsPrinter.Get_Fields("DateVaccinated"), "MM/dd/yyyy");
			}
			txtMarriageHorizontal.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("MarriageLicenseHorizontalAdjustment", "CK")));
		}

		private void cmdPreview_Click(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// birth
						modClerkGeneral.Statics.utPrintInfo.TblName = "Births";
						frmPrintVitalRec.InstancePtr.Init(true, Conversion.Val(txtBirthAdjustment.Text));
						break;
					}
				case 1:
					{
						// marriage cert
						modClerkGeneral.Statics.utPrintInfo.TblName = "Marriages";
						frmPrintVitalRec.InstancePtr.Init(true, Conversion.Val(txtMarriageAdjustment.Text));
						break;
					}
				case 2:
				case 8:
					{
						// marriage lic
						modRegistry.SaveRegistryKey("MarriageLicenseHorizontalAdjustment", FCConvert.ToString(Conversion.Val(txtMarriageHorizontal.Text)), "CK");
						// If frmPrintIntentions.Init Then
						// If gboolStateIntentions Then
						// gstrActiveLicense = "STATE"
						// ElseIf gboolIssuanceIntentions Then
						// gstrActiveLicense = "ISSUANCE"
						// ElseIf gboolMarriageIntentions Then
						// gstrActiveLicense = "MARRIAGE"
						// Else
						// gstrActiveLicense = "OTHER"
						// End If
						// 
						// If gboolNewMarriageForms Then
						// If gintMarriageConfirmation < 3 Then
						// If gintMarriageForm = 2 Then
						// Unload rptNewMarriageLicenseRev082006
						// Call rptNewMarriageLicenseRev082006.Init(gintMarriageForm, True, Val(txtMarriageLicenseAdjustment.Text))
						// 
						// Else
						// Call rptNewMarriageLicense.Init(gintMarriageForm, True, Val(txtMarriageLicenseAdjustment.Text))
						// End If
						// Else
						// If gintMarriageForm = 2 Then
						// Call rptNewMarriageIntentionsRev082006.Init(True, Val(txtMarriageLicenseAdjustment.Text))
						// Else
						// Call rptNewMarriageIntentions.Init(True, Val(txtMarriageLicenseAdjustment.Text))
						// End If
						// End If
						// Else
						// Call rptMarriageLicense.Init(True, Val(txtMarriageLicenseAdjustment.Text))
						// End If
						// End If
						// Unload rptMarriageLicenseWordDoc
						// Call rptMarriageLicenseWordDoc.Init(gintMarriageForm, True, Val(txtMarriageLicenseAdjustment.Text))
						rptNewLicenseRev122012.InstancePtr.Unload();
						rptNewLicenseRev122012.InstancePtr.Init(modGNBas.Statics.gintMarriageForm, this.Modal, true, Conversion.Val(txtMarriageLicenseAdjustment.Text));
						break;
					}
				case 3:
					{
						// death
						modClerkGeneral.Statics.utPrintInfo.TblName = "Deaths";
						frmPrintVitalRec.InstancePtr.Init(true, Conversion.Val(txtDeathAdjustment.Text));
						break;
					}
				case 9:
					{
						// non-confidential death (vs-30A)
						modClerkGeneral.Statics.utPrintInfo.TblName = "NCDeath";
						frmPrintVitalRec.InstancePtr.Init(true, Conversion.Val(txtNCDeath.Text));
						break;
					}
				case 4:
					{
						// burial permit
						// Call rptBurialPermit.Init(True, Val(txtBurialAdjustment.Text))
						rptBurialPermitWordDoc.InstancePtr.Init(true, this.Modal, Conversion.Val(txtBurialAdjustment.Text));
						break;
					}
				case 5:
					{
						// dog
						rptDogWarrant.InstancePtr.Init(true, this.Modal, Conversion.Val(txtDogAdjustment.Text));
						break;
					}
				case 6:
					{
						// back kennel
						rptBackKennelLicense.InstancePtr.Init(1, false, true, Conversion.Val(txtKennelAdjustment.Text));
						break;
					}
				case 7:
					{
						// dog reminders
						rptDogReminders.InstancePtr.Init(true, this.Modal, Conversion.Val(txtDogReminder.Text));
						break;
					}
				case 10:
					{
						// intentions
						// Unload rptNewMarriageIntentionsRev082006
						// Call rptNewMarriageIntentionsRev082006.Init(True, Val(txtMarriageIntentionsAdjustment.Text))
						// Unload rptMarriageIntentionsRev052010
						// Call rptMarriageIntentionsRev052010.Init(True, Val(txtMarriageIntentionsAdjustment.Text))
						// Unload rptMarriageIntentionsWordDoc
						// Call rptMarriageIntentionsWordDoc.init(True, Val(txtMarriageIntentionsAdjustment.Text))
						rptNewIntentionsRev122012.InstancePtr.Unload();
						rptNewIntentionsRev122012.InstancePtr.Init(this.Modal, true, Conversion.Val(txtMarriageIntentionsAdjustment.Text));
						break;
					}
			}
			//end switch
		}

		private void cmdPreview_Click(object sender, System.EventArgs e)
		{
			int index = cmdPreview.GetIndex((FCButton)sender);
			cmdPreview_Click(index, sender, e);
		}

		private void frmPrinterSettings_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrinterSettings properties;
			//frmPrinterSettings.ScaleWidth	= 9045;
			//frmPrinterSettings.ScaleHeight	= 7380;
			//frmPrinterSettings.LinkTopic	= "Form1";
			//frmPrinterSettings.LockControls	= -1  'True;
			//End Unmaped Properties
			// SET THE SIZE OF THE FORM TO FIT INTO THE MDIPARENT
			boolSavingAndExiting = false;
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			LoadData();
			modDirtyForm.ClearDirtyControls(this);
		}

		private void frmPrinterSettings_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmPrinterSettings_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			frmPrintVitalRec.InstancePtr.Unload();
			intDataChanged = FCConvert.ToInt16(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "mnuSaveExit_Click");
			//Support.ZOrder(MDIParent.InstancePtr, 0);
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			int intValue = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// CHECKT TO SEE IF THIS IS AND UPDATE OR AN EDIT
				rsPrinter.OpenRecordset("Select * from PrinterSettings", modGNBas.DEFAULTDATABASE);
				if (rsPrinter.EndOfFile())
				{
					rsPrinter.AddNew();
				}
				else
				{
					rsPrinter.Edit();
				}
				if (cmbDogLicense.Text == "Pre-Printed")
					intValue = 0;
				if (cmbDogLicense.Text == "Blank Form")
					intValue = 1;
				rsPrinter.Set_Fields("DogLicense", intValue);
				if (chkPrintOnFront.CheckState == Wisej.Web.CheckState.Checked && cmbDogLicense.Text == "Blank Form")
				{
					rsPrinter.Set_Fields("printdogreregonfront", true);
				}
				else
				{
					rsPrinter.Set_Fields("PrintDogReRegOnFront", false);
				}
				if (chkPrintOptionalSection.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsPrinter.Set_Fields("PrintMonthlyDogOptionalSection", true);
				}
				else
				{
					rsPrinter.Set_Fields("PrintMonthlyDogOptionalSection", false);
				}
				if (chkPrintExpiration.CheckState == Wisej.Web.CheckState.Checked && cmbDogLicense.Text == "Blank Form")
				{
					rsPrinter.Set_Fields("PrintDogReRegExpiration", true);
				}
				else
				{
					rsPrinter.Set_Fields("PrintDogReRegExpiration", false);
				}
				modRegistry.SaveRegistryKey("MarriageLicenseHorizontalAdjustment", FCConvert.ToString(Conversion.Val(txtMarriageHorizontal.Text)), "CK");
				if (cmbKennelLicense.Text == "Pre-Printed")
					intValue = 0;
				if (cmbKennelLicense.Text == "Blank Form")
					intValue = 1;
				rsPrinter.Set_Fields("KennelLicense", intValue);
				if (cmbMonthlyReport.Text == "Pre-Printed")
					intValue = 0;
				if (cmbMonthlyReport.Text == "Blank Form")
					intValue = 1;
				rsPrinter.Set_Fields("MonthlyLicense", intValue);
				if (Information.IsDate(modDateRoutines.ConvertDateToHaveSlashes(txtDate.Text)))
				{
					rsPrinter.Set_Fields("DateVaccinated", modDateRoutines.ConvertDateToHaveSlashes(txtDate.Text));
				}
				if (txtYear.Text == string.Empty)
					txtYear.Text = FCConvert.ToString(DateTime.Today.Year);
				rsPrinter.Set_Fields("LicenseYear", txtYear.Text);
				modRegistry.SaveRegistryKey("BirthAdjustment", FCConvert.ToString(Conversion.Val(txtBirthAdjustment.Text)), "CK");
				modRegistry.SaveRegistryKey("DeathAdjustment", FCConvert.ToString(Conversion.Val(txtDeathAdjustment.Text)), "CK");
				modRegistry.SaveRegistryKey("NonConfidentialDeathAdjustment", FCConvert.ToString(Conversion.Val(txtNCDeath.Text)), "CK");
				modRegistry.SaveRegistryKey("MarriageAdjustment", FCConvert.ToString(Conversion.Val(txtMarriageAdjustment.Text)), "CK");
				modRegistry.SaveRegistryKey("MarriageLicenseAdjustment", FCConvert.ToString(Conversion.Val(txtMarriageLicenseAdjustment.Text)), "CK");
				modRegistry.SaveRegistryKey("MarriageIntentionsAdjustment", FCConvert.ToString(Conversion.Val(txtMarriageIntentionsAdjustment.Text)), "CK");
				modRegistry.SaveRegistryKey("DogAdjustment", FCConvert.ToString(Conversion.Val(txtDogAdjustment.Text)), "CK");
				modRegistry.SaveRegistryKey("BurialAdjustment", FCConvert.ToString(Conversion.Val(txtBurialAdjustment.Text)), "CK");
				modRegistry.SaveRegistryKey("KennelAdjustment", FCConvert.ToString(Conversion.Val(txtKennelAdjustment.Text)), "CK");
				modRegistry.SaveRegistryKey("DogRemindersAdjustment", FCConvert.ToString(Conversion.Val(txtDogReminder.Text)), "CK");
				// rsPrinter.Fields("BirthAdjustment") = Val(txtBirthAdjustment.Text)
				// rsPrinter.Fields("DeathAdjustment") = Val(txtDeathAdjustment.Text)
				// rsPrinter.Fields("MarriageAdjustment") = Val(txtMarriageAdjustment.Text)
				// rsPrinter.Fields("MarriageLicenseAdjustment") = Val(txtMarriageLicenseAdjustment.Text)
				// rsPrinter.Fields("DogAdjustment") = Val(txtDogAdjustment.Text)
				// rsPrinter.Fields("BurialAdjustment") = Val(txtBurialAdjustment.Text)
				// rsPrinter.Fields("KennelAdjustment") = Val(txtKennelAdjustment.Text)
				// rsPrinter.Fields("DogRemindersAdjustment") = Val(txtDogReminder.Text)
				rsPrinter.Update();
				MessageBox.Show("Update completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				modDirtyForm.ClearDirtyControls(this);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r" + "Unable to complete update.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		public void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			boolSavingAndExiting = true;
			mnuSave_Click();
			Form_Unload(sender, new FCFormClosingEventArgs(FCCloseReason.FormCode, false));
			// MDIParent.Show
		}

		private void optDogLicense_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// pre-printed
						chkPrintOnFront.CheckState = Wisej.Web.CheckState.Unchecked;
						chkPrintOnFront.Enabled = false;
						chkPrintExpiration.Visible = false;
						break;
					}
				case 1:
					{
						// blank
						chkPrintOnFront.Enabled = true;
						chkPrintExpiration.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optDogLicense_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbDogLicense.SelectedIndex;
			optDogLicense_CheckedChanged(index, sender, e);
		}

		private void optMonthlyReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				chkPrintOptionalSection.CheckState = Wisej.Web.CheckState.Unchecked;
				chkPrintOptionalSection.Enabled = false;
			}
			else
			{
				chkPrintOptionalSection.Enabled = true;
			}
		}

		private void optMonthlyReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbMonthlyReport.SelectedIndex;
			optMonthlyReport_CheckedChanged(index, sender, e);
		}

		private void txtYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (txtYear.Text.Length >= 4)
			{
				if (KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
				{
					KeyAscii = (Keys)0;
				}
			}
			else
			{
				KeyAscii = modGlobalRoutines.CheckForNumericKeyPress(KeyAscii);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
