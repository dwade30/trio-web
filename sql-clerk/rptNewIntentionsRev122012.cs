//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptNewMarriageIntentionsRev122012.
	/// </summary>
	public partial class rptNewIntentionsRev122012 : BaseSectionReport
	{
		public rptNewIntentionsRev122012()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.Name = "Marriage License";
        }

		public static rptNewIntentionsRev122012 InstancePtr
		{
			get
			{
				return (rptNewIntentionsRev122012)Sys.GetInstance(typeof(rptNewIntentionsRev122012));
			}
		}

		protected rptNewIntentionsRev122012 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewMarriageIntentionsRev122012	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private int intMarriageForm;
		private bool boolPrintTest;
		private double dblLineAdjust;

		public void Init(bool modalDialog, bool boolTestPrint = false, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			if (boolPrintTest)
			{
				dblLineAdjust = dblAdjust;
			}
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(object sender, EventArgs e)
		{
			boolPrinted = true;
			//intPrinted = this.Document.Printer.DeviceCopies;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// If gboolFromDosTrio = True Or bolFromWindowsCR Then
			if (boolPrinted)
			{
				// With typeCRData
				// .Type = "MAR"
				// .Name = Left(frmMarriages.txtBridesCurrentLastName.Text & ", " & frmMarriages.txtBridesFirstName.Text, 30)
				// .Reference = frmMarriages.txtFileNumber.Text
				// .Control1 = Format(frmMarriages.mebDateIntentionsFiled.Text, "##/##/####")
				// .Control2 = Format(frmMarriages.mebDateLicenseIssued.Text, "##/##/####")
				// 
				// 
				// .Amount1 = Format((typClerkFees.NewMarriageLicense + ((intPrinted - 1) * typClerkFees.ReplacementMarriageLicense)), "000000.00")
				// .Amount2 = Format(0, "000000.00")
				// .Amount3 = Format(0, "000000.00")
				// .Amount4 = Format(0, "000000.00")
				// .Amount5 = Format(0, "000000.00")
				// .Amount6 = Format(0, "000000.00")
				// .ProcessReceipt = "Y"
				// End With
				// 
				// Call WriteCashReceiptingData(typeCRData)
				// End
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
			}
			// End If
			if (!boolPrintTest)
			{
				if (modGNBas.Statics.typMarriageConsent.NeedConsent)
				{
					if (Information.IsDate(modGNBas.Statics.typMarriageConsent.MarriageDate))
					{
						if (fecherFoundation.DateAndTime.DateValue(modGNBas.Statics.typMarriageConsent.MarriageDate).ToOADate() >= DateTime.Today.ToOADate())
						{
							frmPrintMarriageConsent.InstancePtr.Show(FCForm.FormShowEnum.Modal);
						}
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			double dblLaserLineAdjustment2 = 0;
			double dblHAdjust;
			// vbPorter upgrade warning: lngAdjust As int	OnWriteFCConvert.ToDouble(
			float lngAdjust;
			dblHAdjust = Conversion.Val(modRegistry.GetRegistryKey("MarriageLicenseHorizontalAdjustment", "CK"));
			lngAdjust = FCConvert.ToSingle(dblHAdjust * 144) / 1440F;
			if (lngAdjust < 0)
			{
				if (this.PageSettings.Margins.Left - lngAdjust < 0)
				{
					this.PageSettings.Margins.Left = 0;
				}
				else
				{
					this.PageSettings.Margins.Left += lngAdjust;
				}
			}
			else
			{
				if (lngAdjust > this.PageSettings.Margins.Right)
					lngAdjust = FCConvert.ToInt32(this.PageSettings.Margins.Right);
				this.PageSettings.Margins.Right -= lngAdjust;
				this.PageSettings.Margins.Left += lngAdjust;
			}
			//Application.DoEvents();
			if (!boolPrintTest)
			{
				// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
				// If rsData.EndOfFile Then
				// dblLaserLineAdjustment2 = 0
				// Else
				// dblLaserLineAdjustment2 = CDbl(Val(rsData.Fields("MarriageLicenseAdjustment")))
				// End If
				dblLaserLineAdjustment2 = Conversion.Val(modRegistry.GetRegistryKey("MarriageIntentionsAdjustment", "CK"));
			}
			else
			{
				dblLaserLineAdjustment2 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment2) / 1440F;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtPartyABride.Text = "";
			txtPartyAGroom.Text = "";
			txtPartyASpouse.Text = "";
			txtPartyBBride.Text = "";
			txtPartyBGroom.Text = "";
			txtPartyBSpouse.Text = "";
			txtPartyAFemale.Text = "";
			txtPartyAMale.Text = "";
			txtPartyBFemale.Text = "";
			txtPartyBMale.Text = "";
			txtPartyBDesig.Text = "";
			if (!boolPrintTest)
			{
				if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("PartyAType")) == modGNBas.CNSTMARRIAGEPARTYTYPEBRIDE)
				{
					txtPartyABride.Text = "X";
				}
				else if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("PartyAType")) == modGNBas.CNSTMARRIAGEPARTYTYPEGROOM)
				{
					txtPartyAGroom.Text = "X";
				}
				else
				{
					txtPartyASpouse.Text = "X";
				}
				if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("PartyBType")) == modGNBas.CNSTMARRIAGEPARTYTYPEBRIDE)
				{
					txtPartyBBride.Text = "X";
				}
				else if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("PartyBType")) == modGNBas.CNSTMARRIAGEPARTYTYPEGROOM)
				{
					txtPartyBGroom.Text = "X";
				}
				else if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("PartyBType")) == modGNBas.CNSTMARRIAGEPARTYTYPESPOUSE)
				{
					txtPartyBSpouse.Text = "X";
				}
				if (fecherFoundation.Strings.LCase(FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("PartyAGender"))) == "m")
				{
					txtPartyAMale.Text = "X";
				}
				else if (fecherFoundation.Strings.LCase(FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("PartyAGender"))) == "f")
				{
					txtPartyAFemale.Text = "X";
				}
				else
				{
				}
				if (fecherFoundation.Strings.LCase(FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("PartyBGender"))) == "m")
				{
					txtPartyBMale.Text = "X";
				}
				else if (fecherFoundation.Strings.LCase(FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("PartyBGender"))) == "f")
				{
					txtPartyBFemale.Text = "X";
				}
				else
				{
				}
				txtGroomFirst.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFirstName");
				txtGroomMiddle.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMiddleName");
				txtGroomLast.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsLastName");
				txtGroomAge.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int16("GroomsAge"));
				txtGroomStreet.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsStreetAddress") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsStreetName");
				txtGroomBirthplace.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsBirthplace");
				txtGroomsDesig.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsDesignation");
				txtGroomsState.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsState");
				txtGroomsCounty.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsCounty");
				txtGroomsCity.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsCity");
				txtPartyABirthName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("partyabirthname");
				txtPartyBDesig.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("Partybdesignation");
				// txtGroomsDOB = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("GroomsDOB"))))
				txtGroomsDOB.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("groomsdob");
				txtGroomsFathersName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFathersName");
				txtGroomsFathersBirthplace.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFathersBirthplace");
				txtGroomsMothersName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMothersName");
				txtGroomsMothersBirthplace.Text = fecherFoundation.Strings.Trim(Strings.Left(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMothersBirthplace") + Strings.StrDup(20, " "), 20));
				txtBridesFirst.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFirstName");
				txtBridesMiddle.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMiddleName");
				txtBridesSurName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMaidenSurName");
				txtBridesLast.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCurrentLastName");
				txtBridesAge.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int16("BridesAge"));
				txtBridesStreet.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesStreetNumber") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesStreetName");
				txtBridesBirthplace.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesBirthplace");
				txtBridesState.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesState");
				txtBridesCounty.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCounty");
				txtBridesCity.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCity");
				// txtBridesDOB = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("BridesDOB"))))
				txtBridesDOB.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("bridesdob");
				txtBridesFathersName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFathersName");
				txtBridesFathersBirthplace.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFathersBirthplace");
				txtBridesMothersName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMothersName");
				txtBridesMothersBirthplace.Text = fecherFoundation.Strings.Trim(Strings.Left(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMothersBirthplace") + Strings.StrDup(20, " "), 20));
				txtGroomsMarriageNumber.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMarriageNumber");
				txtPartyAMarriageEndedAnnulment.Text = "";
				txtPartyAMarriageEndedDeath.Text = "";
				txtPartyAMarriageEndedDivorce.Text = "";
				// This places the xx's over the
				if (FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsPrevMarriageEndDate")) != string.Empty)
				{
					// txtGroomsMarriageEnded = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("GroomsPrevMarriageEndDate"))))
					txtGroomsMarriageEnded.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("groomsprevmarriageenddate");
					// txtGroomsMarriageEnded.Visible = (gintMarriageConfirmation = 0 Or gintMarriageConfirmation = 2)
					if (FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsPreviousMarriageEnded")) == "Death")
					{
						// txtGroomEndedWhy = "XX"
						txtPartyAMarriageEndedDeath.Text = "X";
					}
					else if (modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsPreviousMarriageEnded") == "Divorce")
					{
						// txtGroomEndedWhy = "                        XX"
						txtPartyAMarriageEndedDivorce.Text = "X";
					}
					else if (modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsPreviousMarriageEnded") == "Annulment")
					{
						// txtGroomEndedWhy = "                                               XX"
						txtPartyAMarriageEndedAnnulment.Text = "X";
					}
					else
					{
						// txtGroomEndedWhy = vbNullString
					}
					txtPartyACourt.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsCourtName");
					txtPartyAFormerSpouse.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFormerSpouseName");
				}
				// txtGroomEndedWhy.Visible = (gintMarriageConfirmation = 0 Or gintMarriageConfirmation = 2)
				if (modGNBas.Statics.gintMarriageConfirmation == 0 || modGNBas.Statics.gintMarriageConfirmation == 2)
				{
					if (FCConvert.ToBoolean(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Boolean("GroomDomesticPartner")))
					{
						fldGroomDomesticPartnersNo.Text = "";
						fldGroomDomesticPartnerYes.Text = "X";
						if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("GroomDomesticPartnerYearRegistered")) != 0)
						{
							txtGroomYearDomesticPartner.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("GroomDomesticPartnerYearRegistered"));
						}
						else
						{
							txtGroomYearDomesticPartner.Text = "";
						}
					}
					else
					{
						fldGroomDomesticPartnersNo.Text = "X";
						fldGroomDomesticPartnerYes.Text = "";
						txtGroomYearDomesticPartner.Text = "";
					}
					if (FCConvert.ToBoolean(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Boolean("BrideDomesticPartner")))
					{
						fldBrideDomesticPartnerNo.Text = "";
						fldBrideDomesticPartnerYes.Text = "X";
						if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("BrideDomesticPartnerYearRegistered")) != 0)
						{
							txtBrideYearDomesticPartner.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("BrideDomesticPartnerYearRegistered"));
						}
						else
						{
							txtBrideYearDomesticPartner.Text = "";
						}
					}
					else
					{
						fldBrideDomesticPartnerNo.Text = "X";
						fldBrideDomesticPartnerYes.Text = "";
					}
				}
				else
				{
					fldGroomDomesticPartnersNo.Text = "";
					fldGroomDomesticPartnerYes.Text = "";
					fldBrideDomesticPartnerNo.Text = "";
					fldBrideDomesticPartnerYes.Text = "";
				}
				txtBridesMarriageNumber.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMarriageNumber");
				txtPartyBMarriageEndedAnnulment.Text = "";
				txtPartyBMarriageEndedDeath.Text = "";
				txtPartyBMarriageEndedDivorce.Text = "";
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("bridesmarriagenumber"))) != "FIRST")
				{
					// If StripDateSlashes(LongDate(rsMarriageCertificate.Fields("BridesPrevMarriageEndDate"))) <> vbNullString Then
					if (FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("bridesprevmarriageenddate")) != string.Empty)
					{
						// txtBridesMarriageEnded = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("BridesPrevMarriageEndDate"))))
						txtBridesMarriageEnded.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("bridesprevmarriageenddate");
						txtBridesMarriageEnded.Visible = (modGNBas.Statics.gintMarriageConfirmation == 0 || modGNBas.Statics.gintMarriageConfirmation == 2);
						if (FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesPreviousMarriageEnded")) == "Death")
						{
							// txtBrideEndedWhy = "XX"
							txtPartyBMarriageEndedDeath.Text = "X";
						}
						else if (modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesPreviousMarriageEnded") == "Divorce")
						{
							// txtBrideEndedWhy = "                         XX"
							txtPartyBMarriageEndedDivorce.Text = "X";
						}
						else if (modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesPreviousMarriageEnded") == "Annulment")
						{
							// txtBrideEndedWhy = "                                                XX"
							txtPartyBMarriageEndedAnnulment.Text = "X";
						}
						else
						{
							// txtBrideEndedWhy = vbNullString
						}
						txtPartyBCourt.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCourtName");
						txtPartyBFormerSpouse.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFormerSpouseName");
					}
				}
			}
			else
			{
				txtGroomFirst.Text = "First";
				txtGroomMiddle.Text = "M";
				txtGroomLast.Text = "Last";
				txtPartyABirthName.Text = "Birth Name";
				txtGroomAge.Text = "99";
				txtGroomStreet.Text = "0 Street";
				txtGroomBirthplace.Text = "Birth Place";
				txtGroomsDesig.Text = "Sr";
				txtGroomsState.Text = "ME";
				txtGroomsCounty.Text = "County";
				txtGroomsCity.Text = "City";
				// txtGroomsDOB = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("GroomsDOB"))))
				txtGroomsDOB.Text = "00/00/00";
				txtGroomsFathersName.Text = "Father's Name";
				txtGroomsFathersBirthplace.Text = "Birth Place";
				txtGroomsMothersName.Text = "Mother's Name";
				txtGroomsMothersBirthplace.Text = "Birth Place";
				txtBridesFirst.Text = "First";
				txtBridesMiddle.Text = "M";
				txtBridesSurName.Text = "Birth Name";
				txtBridesLast.Text = "Last";
				txtBridesAge.Text = "99";
				txtBridesStreet.Text = "0 Street";
				txtBridesBirthplace.Text = "Place of Birth";
				txtBridesState.Text = "ME";
				txtBridesCounty.Text = "County";
				txtBridesCity.Text = "City";
				// txtBridesDOB = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("BridesDOB"))))
				txtBridesDOB.Text = "00/00/00";
				txtBridesFathersName.Text = "Father's Name";
				txtBridesFathersBirthplace.Text = "Place of Birth";
				txtBridesMothersName.Text = "Mother's Name";
				txtBridesMothersBirthplace.Text = "Place of Birth";
				txtGroomsMarriageNumber.Text = "1";
				txtBridesMarriageNumber.Text = "1";
				txtGroomsMarriageEnded.Text = "00/00/0000";
				txtPartyAMarriageEndedAnnulment.Text = "X";
				txtPartyAMarriageEndedDeath.Text = "X";
				txtPartyAMarriageEndedDivorce.Text = "X";
				txtPartyBMarriageEndedAnnulment.Text = "X";
				txtPartyBMarriageEndedDeath.Text = "X";
				txtPartyBMarriageEndedDivorce.Text = "X";
				txtBridesMarriageEnded.Text = "00/00/0000";
				txtPartyABride.Text = "X";
				txtPartyAGroom.Text = "X";
				txtPartyASpouse.Text = "X";
				txtPartyBBride.Text = "X";
				txtPartyBGroom.Text = "X";
				txtPartyBSpouse.Text = "X";
				txtPartyAFemale.Text = "X";
				txtPartyAMale.Text = "X";
				txtPartyBFemale.Text = "X";
				txtPartyBMale.Text = "X";
			}
		}

		
	}
}
