//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWCK0000
{
	public partial class frmBirthDelayed : BaseForm
	{
		public frmBirthDelayed()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			lblLabels = new System.Collections.Generic.List<FCLabel>();
			lblLabels.AddControlArrayElement(lblLabels_0, 0);
			lblLabels.AddControlArrayElement(lblLabels_1, 1);
			lblLabels.AddControlArrayElement(lblLabels_2, 2);
			lblLabels.AddControlArrayElement(lblLabels_3, 3);
			lblLabels.AddControlArrayElement(lblLabels_4, 4);
			lblLabels.AddControlArrayElement(lblLabels_5, 5);
			lblLabels.AddControlArrayElement(lblLabels_6, 6);
			lblLabels.AddControlArrayElement(lblLabels_7, 7);
			lblLabels.AddControlArrayElement(lblLabels_8, 8);
			lblLabels.AddControlArrayElement(lblLabels_9, 9);
			lblLabels.AddControlArrayElement(lblLabels_10, 10);
			lblLabels.AddControlArrayElement(lblLabels_11, 11);
			lblLabels.AddControlArrayElement(lblLabels_12, 12);
			lblLabels.AddControlArrayElement(lblLabels_13, 13);
			lblLabels.AddControlArrayElement(lblLabels_14, 14);
			lblLabels.AddControlArrayElement(lblLabels_15, 15);
			lblLabels.AddControlArrayElement(lblLabels_16, 16);
			lblLabels.AddControlArrayElement(lblLabels_17, 17);
			lblLabels.AddControlArrayElement(lblLabels_18, 18);
			lblLabels.AddControlArrayElement(lblLabels_19, 19);
			lblLabels.AddControlArrayElement(lblLabels_20, 20);
			lblLabels.AddControlArrayElement(lblLabels_21, 21);
			lblLabels.AddControlArrayElement(lblLabels_22, 22);
			lblLabels.AddControlArrayElement(lblLabels_23, 23);
			lblLabels.AddControlArrayElement(lblLabels_24, 24);
			lblLabels.AddControlArrayElement(lblLabels_25, 25);
			lblLabels.AddControlArrayElement(lblLabels_26, 26);
			lblLabels.AddControlArrayElement(lblLabels_27, 27);
			lblLabels.AddControlArrayElement(lblLabels_28, 28);
			lblLabels.AddControlArrayElement(lblLabels_29, 29);
			lblLabels.AddControlArrayElement(lblLabels_30, 30);
			lblLabels.AddControlArrayElement(lblLabels_31, 31);
			lblLabels.AddControlArrayElement(lblLabels_32, 32);
			lblLabels.AddControlArrayElement(lblLabels_33, 33);
			lblLabels.AddControlArrayElement(lblLabels_34, 34);
			lblLabels.AddControlArrayElement(lblLabels_35, 35);
			lblLabels.AddControlArrayElement(lblLabels_36, 36);
			lblLabels.AddControlArrayElement(lblLabels_37, 37);
			lblLabels.AddControlArrayElement(lblLabels_38, 38);
			lblLabels.AddControlArrayElement(lblLabels_39, 39);
			lblLabels.AddControlArrayElement(lblLabels_40, 40);
			lblLabels.AddControlArrayElement(lblLabels_41, 41);
			lblLabels.AddControlArrayElement(lblLabels_42, 42);
			lblLabels.AddControlArrayElement(lblLabels_43, 43);
			lblLabels.AddControlArrayElement(lblLabels_44, 44);
			lblLabels.AddControlArrayElement(lblLabels_45, 45);
			lblLabels.AddControlArrayElement(lblLabels_46, 46);
			lblLabels.AddControlArrayElement(lblLabels_47, 47);
			lblLabels.AddControlArrayElement(lblLabels_48, 48);
			lblLabels.AddControlArrayElement(lblLabels_49, 49);
			lblLabels.AddControlArrayElement(lblLabels_50, 50);
			lblLabels.AddControlArrayElement(lblLabels_51, 51);
			lblLabels.AddControlArrayElement(lblLabels_52, 52);
			lblLabels.AddControlArrayElement(lblLabels_53, 53);
			lblLabels.AddControlArrayElement(lblLabels_54, 54);
			lblLabels.AddControlArrayElement(lblLabels_55, 55);
			lblBirthCertNum = new System.Collections.Generic.List<FCLabel>();
			lblBirthCertNum.AddControlArrayElement(lblBirthCertNum_28, 0);

		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmBirthDelayed InstancePtr
		{
			get
			{
				return (frmBirthDelayed)Sys.GetInstance(typeof(frmBirthDelayed));
			}
		}

		protected frmBirthDelayed _InstancePtr = null;
		//=========================================================
		// *************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 17,2002
		//
		// MODIFIED BY:
		//
		// NOTES: THIS MODULE WAS CREATED BY REQUEST FROM STATE AND SOME
		// OTHER TOWNS. IT IS BASED ON THE FORM VS-6 R-8/87. THIS DATA
		// IS JUST STORED AND CANNOT BE PRINTED.
		// **************************************************
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rs = new clsDRWrapper();
			public clsDRWrapper rs_AutoInitialized = null;
			public clsDRWrapper rs
			{
				get
				{
					if ( rs_AutoInitialized == null)
					{
						 rs_AutoInitialized = new clsDRWrapper();
					}
					return rs_AutoInitialized;
				}
				set
				{
					 rs_AutoInitialized = value;
				}
			}
		public string strSQL = "";
		public bool Popped;
		public bool Adding;
		public bool FormActivated;
		public bool GoodValidation;
		public bool boolSaveSuccessfully;
		public object BirthCertNum;
		private int intDataChanged;
		private int intFrameValue;
		private int intRecordID;

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void mnuComment_Click(object sender, System.EventArgs e)
		{
			if (intRecordID == 0)
			{
				MessageBox.Show("This record doesn't exist yet.  You must save before this record can be tied to a comment.", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (frmCommentOld.InstancePtr.Init("CK", "BirthsDelayed", "Comments", "ID", intRecordID, boolModal: true))
			{
				// This is supposed to make the yellow paper clip show up when there is a comment i think but i cant seem
				// to find the object anywhere
				// Set ImgComment.Picture = Nothing
			}
			else
			{
				// Set ImgComment.Picture = MDIParent.ImageList2.ListImages(1).Picture
			}
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGNBas.Statics.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblregionS where townnumber > 0 order by townnumber", "CentralData");
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("townnumber") + ";" + clsLoad.Get_Fields_String("townname") + "\t" + clsLoad.Get_Fields("townnumber") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				strTemp = "0";
				gridTownCode.Visible = false;
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}

		public void Init(int lngID)
		{
			intRecordID = lngID;
			this.Show(App.MainForm);
		}

		public void SetCurrentRecordID(int intID)
		{
			intRecordID = intID;
		}

		private void frmBirthDelayed_Resize(object sender, System.EventArgs e)
		{
			//SSTab1.Height = FCConvert.ToInt32(0.086 * SSTab1.HeightOriginal);
			ResizeGridTownCode();
		}

		private void txtBirthPlace1_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtBirthPlace1.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtBirthPlace2_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtBirthPlace2.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtBirthPlace3_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtBirthPlace3.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtChildTown_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtChildTown.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtChildTown_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 0;
		}

		private void cboClerkCityTown_Click()
		{
			intDataChanged += 1;
		}

		private void cboFathersState_GotFocus()
		{
			SSTab1.SelectedIndex = 1;
		}

		private void txtClerkTown_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtClerkTown.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtClerkTown_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 2;
		}

		private void chkDeceased_CheckedChanged(object sender, System.EventArgs e)
		{
			lblDeceased.Visible = 0 != (chkDeceased.CheckState);
			mebDateDeceased.Visible = 0 != chkDeceased.CheckState;
		}

		public void LoadData()
		{
			if (modGNBas.Statics.boolSetDefaultTowns)
			{
				modGNBas.Statics.boolSetDefaultTowns = false;
				return;
			}
			else if (modGNBas.Statics.boolGetClerk)
			{
				modGNBas.Statics.boolGetClerk = false;
				return;
			}
			if (FormActivated == false)
			{
				// lblWMuniName.Text = "- " & MuniName & " -"
				// lblScreenTitle.Text = "Delayed Birth Certificate"
				// lblDate.Text = Format(Date, "MM/dd/yyyy")
				strSQL = "SELECT * FROM BirthsDelayed where id = " + FCConvert.ToString(intRecordID) + " order by ChildLastName,ChildFirstName ";
				rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
				FormActivated = true;
			}
			
			if (intRecordID == 0)
			{
				modClerkGeneral.Statics.AddingBirthCertificate = false;
				Adding = true;
				ClearBoxes();
				// Dave 11/2/2006
				txtChildTown.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.ClerkDefaults.BirthDefaultTown);
				txtCounty.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.ClerkDefaults.BirthDefaultCounty);
			}
			else
			{
				FillBoxes();
			}
			MenuCheck();
			modDirtyForm.ClearDirtyControls(this);
			intDataChanged = 0;
		}

		private void cmdFind_GotFocus()
		{
			SSTab1.SelectedIndex = 2;
		}

		public void frmBirthDelayed_Activated(object sender, System.EventArgs e)
		{
			//App.DoEvents();
		}

		private void ClearBoxes()
		{
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
			}
			chkIllegitBirth.CheckState = Wisej.Web.CheckState.Unchecked;
			chkDeceased.CheckState = Wisej.Web.CheckState.Unchecked;
			chkMarriageOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkRequired.CheckState = Wisej.Web.CheckState.Unchecked;
			mebDateDeceased.Text = string.Empty;
			// Call SetComboByName(cboMothersTown, UCase(MuniName))
			txtChildMI.Text = "";
			txtChildDOB.Text = "";
			txtActualDate.Text = "";
		}

		private void FillBoxes()
		{
			if (rs.EndOfFile())
				return;
			txtFileNumber.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FileNumber") + "");
			mebDateDeceased.Text = Strings.Format(rs.Get_Fields("DeceasedDate"), "MM/dd/yyyy");
			if (rs.Get_Fields_Boolean("Legitimate") == true)
			{
				chkIllegitBirth.CheckState = Wisej.Web.CheckState.Unchecked;
				txtFatherLastName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FathersLastName") + "");
				txtFatherFirstName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FathersFirstName") + "");
				txtFatherMI.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FathersMiddleName") + "");
			}
			else
			{
				chkIllegitBirth.CheckState = Wisej.Web.CheckState.Checked;
			}
			txtAmendedInfo.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("AmendedInfo") + "");
			chkRequired.CheckState = (CheckState)((rs.Get_Fields_Boolean("NoRequiredFields")) ? 1 : 0);
			chkDeceased.CheckState = (CheckState)((rs.Get_Fields_Boolean("DeathRecordInOffice")) ? 1 : 0);
			chkMarriageOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("MarriageOnFile")) ? 1 : 0);
			txtChildLastName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildLastName") + "");
			txtChildFirstName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildFirstName") + "");
			txtChildMI.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildMiddleName") + "");
			txtChildSex.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildSex") + "");
			if (Information.IsDate(rs.Get_Fields_String("childdob")))
			{
				txtChildDOB.Text = Strings.Format(fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildDOB") + ""), "MM/dd/yyyy");
			}
			else
			{
				txtChildDOB.Text = FCConvert.ToString(rs.Get_Fields_String("ChildDOBDescription"));
			}
			if (Information.IsDate(rs.Get_Fields("ActualDate")) && !rs.Get_Fields_DateTime("ActualDate").IsEmptyDate())
			{
				txtActualDate.Text = Strings.Format(rs.Get_Fields_DateTime("ActualDate"), "MM/dd/yyyy");
			}
			else
			{
				txtActualDate.Text = "";
			}
			txtCounty.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildCounty") + "");
			txtChildTown.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildTown") + "");
			txtChildFirstName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildFirstName") + "");
			txtChildMI.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildMiddleName") + "");
			txtChildLastName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildLastName") + "");
			txtChildRace.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildRace") + "");
			txtMotherFirstName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MothersFirstName") + "");
			txtMotherMI.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MothersMiddleName") + "");
			txtMotherLastName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MothersLastName") + "");
			txtMotherMaidenName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MothersMaidenName") + "");
			txtMothersBirthPlace.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("MothersBirthPlace") + "");
			txtFatherLastName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FathersLastName") + "");
			txtFatherFirstName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FathersFirstName") + "");
			txtFatherMI.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FathersMiddleName") + "");
			txtFathersBirthPlace.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FathersBirthPlace") + "");
			txtRegistrantAddress.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ChildCurrentAddress") + "");
			mebNotarySwornDate.Text = Strings.Format(fecherFoundation.Strings.Trim(rs.Get_Fields("NotarySwornDate") + ""), "MM/dd/yyyy");
			txtNotaryFirstName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("NotaryFirstName") + "");
			txtNotaryMiddleName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("NotaryMiddleName") + "");
			txtNotaryLastName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("NotaryLastName") + "");
			txtNotaryExpiresDate.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("NotaryCommissionExpires") + "");
			mebClerkSwornDate.Text = Strings.Format(fecherFoundation.Strings.Trim(rs.Get_Fields("ClerkSwornDate") + ""), "MM/dd/yyyy");
			txtClerkFirstName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ClerkFirstName") + "");
			txtClerkMiddleName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ClerkMiddleName") + "");
			txtClerkLastName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ClerkLastName") + "");
			txtClerkTown.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("ClerkTown") + "");
			txtDocumentType1.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("TypeDocument1") + "");
			txtWhomSigned1.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("WhomSigned1") + "");
			txtDateIssued1.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("DateIssued1") + "");
			txtDateEntry1.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("DateOrigEntry1") + "");
			mebDOB1.Text = Strings.Format(fecherFoundation.Strings.Trim(rs.Get_Fields("DOB1") + ""), "MM/dd/yyyy");
			txtBirthPlace1.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("BirthPlace1") + "");
			txtFatherFullName1.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FullNameFather1") + "");
			txtMotherFullName1.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FullNameMother1") + "");
			txtDocumentType2.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("TypeDocument2") + "");
			txtWhomSigned2.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("WhomSigned2") + "");
			txtDateIssued2.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("DateIssued2") + "");
			txtDateEntry2.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("DateOrigEntry2") + "");
			mebDOB2.Text = Strings.Format(fecherFoundation.Strings.Trim(rs.Get_Fields("DOB2") + ""), "MM/dd/yyyy");
			txtBirthPlace2.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("BirthPlace2") + "");
			txtFatherFullName2.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FullNameFather2") + "");
			txtMotherFullName2.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FullNameMother2") + "");
			txtDocumentType3.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("TypeDocument3") + "");
			txtWhomSigned3.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("WhomSigned3") + "");
			txtDateIssued3.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("DateIssued3") + "");
			txtDateEntry3.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("DateOrigEntry3") + "");
			mebDOB3.Text = Strings.Format(fecherFoundation.Strings.Trim(rs.Get_Fields("DOB3") + ""), "MM/dd/yyyy");
			txtBirthPlace3.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("BirthPlace3") + "");
			txtFatherFullName3.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FullNameFather3") + "");
			txtMotherFullName3.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("FullNameMother3") + "");
			txtRegistrarName.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("StateRegistrar") + "");
			txtRegistrarReviewed.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("EvidenceReviewedBy") + "");
			txtDateOfFiling.Text = fecherFoundation.Strings.Trim(rs.Get_Fields("DateOfFiling") + "");
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) > 0)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("towncode"))));
			}
		}

		private void frmBirthDelayed_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				mnuProcessQuit_Click();
			// If KeyCode = vbKeyF10 Then
			// Call mnuProcessSave_Click
			// If GoodValidation Then Call mnuProcessQuit_Click
			// End If
			// 
			// If KeyCode = vbKeyF9 Then Call mnuProcessSave_Click
		}

		private void frmBirthDelayed_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != txtRegistrantAddress.Name)
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FormActivated = false;
			//modGNBas.Statics.boolBirthOpen = false;
			intDataChanged = FCConvert.ToInt16(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "mnuProcessSave_Click");
			//MDIParent.InstancePtr.Show();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mebNotarySwornDate_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 2;
		}

		private void mnuProcessAddNewRecord_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				Adding = true;
				intRecordID = 0;
				ClearBoxes();
				MenuCheck();
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " has been encountered." + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				return;
			}
		}

		private void mnuProcessDeleteRecord_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
			DialogResult answer;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				answer = MessageBox.Show("Are you sure you want to permanently delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					if (intRecordID > 0)
					{
						modGlobalFunctions.AddCYAEntry("CK", "Deleted delayed birth record", "", "", "", "");
						rs.Execute("Delete from BirthsDelayed where ID = " + FCConvert.ToString(intRecordID), modGNBas.DEFAULTCLERKDATABASE);
						MessageBox.Show("Record deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						Close();
					}
					else
					{
						MessageBox.Show("There is no active record or record has not been saved. Delete cannot be completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " has been encountered." + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				return;
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		public void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			object Ans;
			SaveData();
			// If GoodValidation = False Then Exit Sub
			// If Adding = True Then
			// Ans = MsgBox("Would you like to add another Birth Certificate?", vbYesNo)
			// If Ans = vbYes Then
			// Call ClearBoxes
			// Adding = True
			// intRecordID = 0
			// Call MenuCheck
			// Else
			// Adding = False
			// Unload Me
			// MDIParent.Show
			// End If
			// End If
		}

		private void chkIllegitBirth_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkIllegitBirth.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkIllegitBirth.ForeColor = Color.Red;
				modGNBas.Statics.boolIllegitbirth = true;
			}
			else
			{
				chkIllegitBirth.ForeColor = Color.Black;
				modGNBas.Statics.boolIllegitbirth = false;
			}
			intDataChanged += 1;
		}

		private bool SaveData()
		{
			bool SaveData = false;
			clsDRWrapper rsClerkInfo = new clsDRWrapper();
			bool blnUpdateFile = false;
			GoodValidation = false;
			SaveData = false;
			ValidateEntry();
			if (GoodValidation == false)
			{
				// MsgBox "Unable to Update.", vbOKOnly, "Error in Save"
				return SaveData;
			}
			boolSaveSuccessfully = false;
			// If comment has   ***  in front of the field name then that
			// field is required and the validation routine will not allow the
			// user to get this far if that field does not have data in it.
			rs.OpenRecordset("Select * from BirthsDelayed where ID = " + FCConvert.ToString(intRecordID), modGNBas.DEFAULTCLERKDATABASE);
			if (rs.EndOfFile())
			{
				rs.AddNew();
				blnUpdateFile = true;
			}
			else
			{
				rs.Edit();
				blnUpdateFile = false;
			}
			rs.Set_Fields("FileNumber", txtFileNumber.Text);
			if (fecherFoundation.FCUtils.IsNull(mebDateDeceased.Text) == true || mebDateDeceased.IsEmpty)
			{
				rs.Set_Fields("DeceasedDate", null);
			}
			else
			{
				rs.Set_Fields("DeceasedDate", mebDateDeceased.Text);
			}
			rs.Set_Fields("AmendedInfo", txtAmendedInfo.Text);
			rs.Set_Fields("Legitimate", chkIllegitBirth.CheckState == Wisej.Web.CheckState.Unchecked);
			rs.Set_Fields("NoRequiredFields", chkRequired.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("DeathRecordInOffice", chkDeceased.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("MarriageOnFile", chkMarriageOnFile.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("ChildFirstName", txtChildFirstName.Text);
			rs.Set_Fields("ChildMiddleName", txtChildMI.Text);
			rs.Set_Fields("ChildLastName", txtChildLastName.Text);
			// If IsNull(txtChildDOB.Text) = True Or txtChildDOB.Text = "00/00/0000" Or txtChildDOB.Text = vbNullString Then
			// .Fields("ChildDOB") = Null
			// Else
			// .Fields("ChildDOB") = txtChildDOB.Text
			// End If
			if (Conversion.Val(txtChildDOB.Text) != 0 && Information.IsDate(txtChildDOB.Text))
			{
				rs.Set_Fields("Childdob", Strings.Format(txtChildDOB.Text, "MM/dd/yyyy"));
				rs.Set_Fields("Childdobdescription", "");
			}
			else
			{
				rs.Set_Fields("ChildDOB", null);
				rs.Set_Fields("ChildDobDescription", txtChildDOB.Text);
			}
			if (Information.IsDate(txtActualDate.Text))
			{
				rs.Set_Fields("ActualDate", fecherFoundation.DateAndTime.DateValue(txtActualDate.Text));
			}
			else
			{
				rs.Set_Fields("ActualDate", null);
			}
			rs.Set_Fields("ChildRace", txtChildRace.Text);
			rs.Set_Fields("ChildSex", txtChildSex.Text);
			rs.Set_Fields("ChildTown", fecherFoundation.Strings.Trim(txtChildTown.Text));
			rs.Set_Fields("ChildCounty", fecherFoundation.Strings.Trim(txtCounty.Text));
			rs.Set_Fields("MothersFirstName", txtMotherFirstName.Text);
			rs.Set_Fields("MothersMiddleName", txtMotherMI.Text);
			rs.Set_Fields("MothersLastName", txtMotherLastName.Text);
			rs.Set_Fields("MothersMaidenName", txtMotherMaidenName.Text);
			rs.Set_Fields("MothersBirthPlace", txtMothersBirthPlace.Text);
			rs.Set_Fields("FathersLastName", txtFatherLastName.Text);
			rs.Set_Fields("FathersFirstName", txtFatherFirstName.Text);
			rs.Set_Fields("FathersMiddleName", txtFatherMI.Text);
			rs.Set_Fields("FathersBirthPlace", txtFathersBirthPlace.Text);
			rs.Set_Fields("ChildCurrentAddress", txtRegistrantAddress.Text);
			if (mebNotarySwornDate.IsEmpty)
			{
				rs.Set_Fields("NotarySwornDate", null);
			}
			else
			{
				rs.Set_Fields("NotarySwornDate", mebNotarySwornDate.Text);
			}
			rs.Set_Fields("NotaryFirstName", txtNotaryFirstName.Text);
			rs.Set_Fields("NotaryMiddleName", txtNotaryMiddleName.Text);
			rs.Set_Fields("NotaryLastName", txtNotaryLastName.Text);
			rs.Set_Fields("NotaryCommissionExpires", txtNotaryExpiresDate.Text);
			if (mebClerkSwornDate.IsEmpty)
			{
				rs.Set_Fields("ClerkSwornDate", null);
			}
			else
			{
				rs.Set_Fields("ClerkSwornDate", mebClerkSwornDate.Text);
			}
			rs.Set_Fields("ClerkFirstName", txtClerkFirstName.Text);
			rs.Set_Fields("ClerkMiddleName", txtClerkMiddleName.Text);
			rs.Set_Fields("ClerkLastName", txtClerkLastName.Text);
			rs.Set_Fields("ClerkTown", fecherFoundation.Strings.Trim(txtClerkTown.Text));
			rs.Set_Fields("TypeDocument1", txtDocumentType1.Text);
			rs.Set_Fields("WhomSigned1", txtWhomSigned1.Text);
			rs.Set_Fields("DateIssued1", txtDateIssued1.Text);
			rs.Set_Fields("DateOrigEntry1", txtDateEntry1.Text);
			if (mebDOB1.IsEmpty)
			{
				rs.Set_Fields("DOB1", null);
			}
			else
			{
				rs.Set_Fields("DOB1", mebDOB1.Text);
			}
			rs.Set_Fields("BirthPlace1", txtBirthPlace1.Text);
			rs.Set_Fields("FullNameFather1", txtFatherFullName1.Text);
			rs.Set_Fields("FullNameMother1", txtMotherFullName1.Text);
			rs.Set_Fields("TypeDocument2", txtDocumentType2.Text);
			rs.Set_Fields("WhomSigned2", txtWhomSigned2.Text);
			rs.Set_Fields("DateIssued2", txtDateIssued2.Text);
			rs.Set_Fields("DateOrigEntry2", txtDateEntry2.Text);
			if (mebDOB2.IsEmpty)
			{
				rs.Set_Fields("DOB2", null);
			}
			else
			{
				rs.Set_Fields("DOB2", mebDOB2.Text);
			}
			rs.Set_Fields("BirthPlace2", txtBirthPlace2.Text);
			rs.Set_Fields("FullNameFather2", txtFatherFullName2.Text);
			rs.Set_Fields("FullNameMother2", txtMotherFullName2.Text);
			rs.Set_Fields("TypeDocument3", txtDocumentType3.Text);
			rs.Set_Fields("WhomSigned3", txtWhomSigned3.Text);
			rs.Set_Fields("DateIssued3", txtDateIssued3.Text);
			rs.Set_Fields("DateOrigEntry3", txtDateEntry3.Text);
			if (mebDOB3.IsEmpty)
			{
				rs.Set_Fields("DOB3", null);
			}
			else
			{
				rs.Set_Fields("DOB3", mebDOB3.Text);
			}
			rs.Set_Fields("BirthPlace3", txtBirthPlace3.Text);
			rs.Set_Fields("FullNameFather3", txtFatherFullName3.Text);
			rs.Set_Fields("FullNameMother3", txtMotherFullName3.Text);
			rs.Set_Fields("StateRegistrar", txtRegistrarName.Text);
			rs.Set_Fields("EvidenceReviewedBy", txtRegistrarReviewed.Text);
			rs.Set_Fields("DateOfFiling", txtDateOfFiling.Text);
			rs.Set_Fields("towncode", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			rs.Update();
			intRecordID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != "" && blnUpdateFile)
			{
				rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
				if (rsClerkInfo.BeginningOfFile() != true && rsClerkInfo.EndOfFile() != true)
				{
					rsClerkInfo.Edit();
				}
				else
				{
					rsClerkInfo.AddNew();
				}
				rsClerkInfo.Set_Fields("LastBirthFileNumber", fecherFoundation.Strings.Trim(txtFileNumber.Text));
				rsClerkInfo.Update();
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + fecherFoundation.Strings.Trim(txtFileNumber.Text));
			}
			intDataChanged = 0;
			modDirtyForm.ClearDirtyControls(this);
			boolSaveSuccessfully = true;
			SaveData = true;
			MessageBox.Show("Save Completed Successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			return SaveData;
		}

		private void ValidateEntry()
		{
			if (chkRequired.CheckState == CheckState.Unchecked)
			{
				if (fecherFoundation.Strings.Trim(txtChildFirstName.Text) == "")
				{
					MessageBox.Show("You must enter a First Name for the Child");
					this.SSTab1.SelectedIndex = 0;
					txtChildFirstName.Focus();
				}
				else if (fecherFoundation.Strings.Trim(txtChildLastName.Text) == "")
				{
					MessageBox.Show("You must enter a Last Name for the Child");
					this.SSTab1.SelectedIndex = 0;
					txtChildLastName.Focus();
				}
				else if (fecherFoundation.Strings.Trim(txtChildDOB.Text) == string.Empty)
				{
					MessageBox.Show("You must enter a  Date of Birth for the Child");
					this.SSTab1.SelectedIndex = 0;
					txtChildDOB.Focus();
				}
				else if (fecherFoundation.Strings.UCase(txtChildSex.Text) != "M" && fecherFoundation.Strings.UCase(txtChildSex.Text) != "F")
				{
					MessageBox.Show("You must Enter the Sex of the Child");
					this.SSTab1.SelectedIndex = 0;
					txtChildSex.Focus();
				}
				else if (fecherFoundation.Strings.Trim(txtMotherFirstName.Text) == "")
				{
					MessageBox.Show("You must enter First Name for the Mother");
                    this.SSTab1.SelectedIndex = 1;
					txtMotherFirstName.Focus();
                }
				else if (fecherFoundation.Strings.Trim(txtMotherMaidenName.Text) == "")
				{
					MessageBox.Show("You must enter Maiden Name for the Mother");
                    this.SSTab1.SelectedIndex = 1;
					txtMotherMaidenName.Focus();
                }
				else if (fecherFoundation.Strings.Trim(txtMotherLastName.Text) == "")
				{
					MessageBox.Show("You must enter Last Name for the Mother");
                    this.SSTab1.SelectedIndex = 1;
					txtMotherLastName.Focus();
                    // ElseIf Trim(txtClerkFirstName.Text) = "" Then
                    // MsgBox "You must enter First Name for the Clerk."
                    // txtClerkFirstName.SetFocus
                    // Me.SSTab1.Tab = 2
                    // ElseIf Trim(txtClerkLastName.Text) = "" Then
                    // MsgBox "You must enter Last Name for the Clerk."
                    // txtClerkLastName.SetFocus
                }
				else
				{
					GoodValidation = true;
				}
				if (GoodValidation == true)
				{
					// Check Date of Birth
					// If Year(txtChildDOB.Text) > Year(Date) Then
					// BadDateBirth:
					// MsgBox "You cannot enter a Date of Birth greater than today."
					// GoodValidation = False
					// Exit Sub
					// ElseIf Year(txtChildDOB.Text) = Year(Date) Then
					// If Month(txtChildDOB.Text) > Month(Date) Then
					// GoTo BadDateBirth
					// ElseIf Month(txtChildDOB.Text) = Month(Date) Then
					// If Day(txtChildDOB.Text) > Day(Date) Then GoTo BadDateBirth
					// End If
					// End If
					// Check Attest Date
					// If Year(mebAttestDate.Text) > Year(Date) Then
					// BadDateAttest:
					// MsgBox "You cannot enter an Attest Date greater than today."
					// GoodValidation = False
					// Exit Sub
					// ElseIf Year(mebAttestDate.Text) = Year(Date) Then
					// If Month(mebAttestDate.Text) > Month(Date) Then
					// GoTo BadDateAttest
					// ElseIf Month(mebAttestDate.Text) = Month(Date) Then
					// If Day(mebAttestDate.Text) > Day(Date) Then GoTo BadDateAttest
					// End If
					// End If
				}
			}
			else
			{
				GoodValidation = true;
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
				mnuProcessQuit_Click();
		}

		private void mnuSetDefaultTowns_Click(object sender, System.EventArgs e)
		{
			frmStateTownInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void txtChildCounty_GotFocus()
		{
			SSTab1.SelectedIndex = 0;
		}

		private void txtChildDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int Days;
			// days before or after today
			int Ans;
			// changed from vbYesNo to vbOk
			// If Days > 0 Then
			// Cancel = True
			// Ans = MsgBox("The Child's Date of Birth is greater than today?" & vbNewLine &
			// "Your system date is " & Format(Now, "MM/dd/yyyy") & ".",
			// vbOKOnly, "Invalid Date Of Birth")
			// txtChildDOB.Text = Format(Now, "MM/dd/yyyy")
			// txtChildDOB.SetFocus
			// Else: If Days = 0 Then Cancel = False
			// MsgBox "todays date"
			// End If
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtChildDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtChildDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtChildDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtChildDOB.Text = strD;
					}
				}
			}
			if (Information.IsDate(txtChildDOB.Text))
			{
				txtActualDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateValue(txtChildDOB.Text), "MM/dd/yyyy");
			}
		}

		private void MenuCheck()
		{
			if (Adding == true)
			{
				mnuProcessAddNewRecord.Enabled = false;
				cmdProcessDeleteRecord.Enabled = false;
			}
			else
			{
				// mnuProcessAddNewRecord.Enabled = True
				cmdProcessDeleteRecord.Enabled = true;
			}
		}

		private void frmBirthDelayed_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBirthDelayed properties;
			//frmBirthDelayed.ScaleWidth	= 9030;
			//frmBirthDelayed.ScaleHeight	= 7425;
			//frmBirthDelayed.LinkTopic	= "Form1";
			//frmBirthDelayed.LockControls	= -1  'True;
			//End Unmaped Properties
			/* Control ctl = new Control(); */
			clsDRWrapper rsClerkInfo = new clsDRWrapper();
			foreach (Control ctl in this.GetAllControls())
			{
				if (FCConvert.ToString(ctl.Tag) == "Required")
				{
					ctl.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
			}
			// ctl
			//modGNBas.Statics.boolBirthOpen = true;
			SetupGridTownCode();
			LoadData();
			rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
			if (rsClerkInfo.EndOfFile() != true && rsClerkInfo.BeginningOfFile() != true)
			{
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + rsClerkInfo.Get_Fields_String("LastBirthFileNumber"));
			}
			else
			{
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: UNKNOWN");
			}
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
		}

        //FC:FINAL:AM: Use Validating event
        //private void txtChildSex_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        //{
        //	Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
        //	// if this is the sex field
        //	if (KeyAscii != Keys.F && KeyAscii != Keys.M && KeyAscii != Keys.NumPad6 && KeyAscii != Keys.Subtract && KeyAscii != Keys.Back)
        //	{
        //		KeyAscii = (Keys)0;
        //		MessageBox.Show("Must enter 'M' or 'F'.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //	}
        //	e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        //}

        private void txtChildSex_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtChildSex.Modified)
            {
                string text = txtChildSex.Text;
                if (text != "M" && text != "F")
                {
                    MessageBox.Show("Must enter 'M' or 'F'.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
        }

        private void txtCounty_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "C";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCounty.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtDateEntry1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateEntry1.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateEntry1.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateEntry1.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateEntry1.Text = strD;
					}
				}
			}
		}

		private void txtDateEntry2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateEntry2.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateEntry2.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateEntry2.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateEntry2.Text = strD;
					}
				}
			}
		}

		private void txtDateEntry3_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateEntry3.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateEntry3.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateEntry3.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateEntry3.Text = strD;
					}
				}
			}
		}

		private void txtDateIssued1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateIssued1.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateIssued1.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateIssued1.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateIssued1.Text = strD;
					}
				}
			}
		}

		private void txtDateIssued2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateIssued2.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateIssued2.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateIssued2.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateIssued2.Text = strD;
					}
				}
			}
		}

		private void txtDateIssued3_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateIssued3.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateIssued3.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateIssued3.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateIssued3.Text = strD;
					}
				}
			}
		}

		private void txtDateOfFiling_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateOfFiling.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateOfFiling.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateOfFiling.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateOfFiling.Text = strD;
					}
				}
			}
		}

		private void txtDocumentType1_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 3;
		}

		private void txtFatherFirstName_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 1;
		}

		private void txtFathersBirthPlace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtFathersBirthPlace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtMotherFirstName_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 1;
		}

		private void txtMotherFullName3_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 3;
		}

		private void txtMothersBirthPlace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtMothersBirthPlace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtNotaryExpiresDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtNotaryExpiresDate.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtNotaryExpiresDate.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtNotaryExpiresDate.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtNotaryExpiresDate.Text = strD;
					}
				}
			}
		}

		private void txtRegistrantAddress_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 1;
		}

		private void txtRegistrarName_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 4;
		}
	}
}
