﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBurialPermit.
	/// </summary>
	partial class rptBurialPermit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBurialPermit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtFullName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSex = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFacility = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateOfDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlaceOfDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLicenseNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRemovalState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTempStorage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBurialSea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBurial = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMedicalScience = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCremation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDisinterment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeathCertificate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReportDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReleaseCreamation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCourtOrder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTNameVault = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTX = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRX1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRX2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRX3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRX4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRX5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRX6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.CremName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.CremLoc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.CremDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.CremBuried = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.CremScattered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.CremFamily = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDX = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateSigned = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtArmedForcesYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtArmedForcesNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClerkTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMausoleum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtFullName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFacility)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemovalState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTempStorage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBurialSea)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBurial)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicalScience)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCremation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDisinterment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeathCertificate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReportDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReleaseCreamation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCourtOrder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTNameVault)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CremName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CremLoc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CremDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CremBuried)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CremScattered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CremFamily)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateSigned)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArmedForcesYes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArmedForcesNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerkTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMausoleum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtFullName,
				this.txtAge,
				this.txtSex,
				this.txtFacility,
				this.txtDateOfDeath,
				this.txtPlaceOfDeath,
				this.txtLicenseNumber,
				this.txtRemovalState,
				this.txtTempStorage,
				this.txtBurialSea,
				this.txtBurial,
				this.txtMedicalScience,
				this.txtCremation,
				this.txtDisinterment,
				this.txtDeathCertificate,
				this.txtReportDeath,
				this.txtReleaseCreamation,
				this.txtCourtOrder,
				this.txtTNameVault,
				this.txtTLocation,
				this.txtTDate,
				this.txtTX,
				this.txtRName,
				this.txtRLocation,
				this.txtRDate,
				this.txtRX1,
				this.txtRX2,
				this.txtRX3,
				this.txtRX4,
				this.txtRX5,
				this.txtRX6,
				this.CremName,
				this.CremLoc,
				this.CremDate,
				this.CremBuried,
				this.CremScattered,
				this.CremFamily,
				this.txtDName,
				this.txtDLocation,
				this.txtDDate,
				this.txtDX,
				this.txtDateSigned,
				this.txtArmedForcesYes,
				this.txtArmedForcesNo,
				this.txtClerkTown,
				this.txtMausoleum
			});
			this.PageHeader.Height = 9.104167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtFullName
			// 
			this.txtFullName.CanGrow = false;
			this.txtFullName.Height = 0.1875F;
			this.txtFullName.Left = 0.125F;
			this.txtFullName.Name = "txtFullName";
			this.txtFullName.Text = null;
			this.txtFullName.Top = 1.625F;
			this.txtFullName.Width = 2.3125F;
			// 
			// txtAge
			// 
			this.txtAge.Height = 0.1875F;
			this.txtAge.Left = 0.75F;
			this.txtAge.Name = "txtAge";
			this.txtAge.Text = null;
			this.txtAge.Top = 2.0625F;
			this.txtAge.Width = 0.4375F;
			// 
			// txtSex
			// 
			this.txtSex.Height = 0.1875F;
			this.txtSex.Left = 0F;
			this.txtSex.Name = "txtSex";
			this.txtSex.Text = null;
			this.txtSex.Top = 2.0625F;
			this.txtSex.Width = 0.6875F;
			// 
			// txtFacility
			// 
			this.txtFacility.Height = 0.1875F;
			this.txtFacility.Left = 0F;
			this.txtFacility.Name = "txtFacility";
			this.txtFacility.Text = null;
			this.txtFacility.Top = 2.4375F;
			this.txtFacility.Width = 5.4375F;
			// 
			// txtDateOfDeath
			// 
			this.txtDateOfDeath.CanGrow = false;
			this.txtDateOfDeath.Height = 0.1875F;
			this.txtDateOfDeath.Left = 5.625F;
			this.txtDateOfDeath.Name = "txtDateOfDeath";
			this.txtDateOfDeath.Text = null;
			this.txtDateOfDeath.Top = 1.625F;
			this.txtDateOfDeath.Width = 1.1875F;
			// 
			// txtPlaceOfDeath
			// 
			this.txtPlaceOfDeath.Height = 0.1875F;
			this.txtPlaceOfDeath.Left = 2.833333F;
			this.txtPlaceOfDeath.Name = "txtPlaceOfDeath";
			this.txtPlaceOfDeath.Text = null;
			this.txtPlaceOfDeath.Top = 2.0625F;
			this.txtPlaceOfDeath.Width = 4.479167F;
			// 
			// txtLicenseNumber
			// 
			this.txtLicenseNumber.Height = 0.1875F;
			this.txtLicenseNumber.Left = 6.041667F;
			this.txtLicenseNumber.Name = "txtLicenseNumber";
			this.txtLicenseNumber.Style = "text-align: right";
			this.txtLicenseNumber.Text = null;
			this.txtLicenseNumber.Top = 2.375F;
			this.txtLicenseNumber.Width = 1.3125F;
			// 
			// txtRemovalState
			// 
			this.txtRemovalState.Height = 0.1875F;
			this.txtRemovalState.Left = 1.5F;
			this.txtRemovalState.Name = "txtRemovalState";
			this.txtRemovalState.Text = "XX";
			this.txtRemovalState.Top = 2.833333F;
			this.txtRemovalState.Width = 0.3125F;
			// 
			// txtTempStorage
			// 
			this.txtTempStorage.Height = 0.1875F;
			this.txtTempStorage.Left = 2.979167F;
			this.txtTempStorage.Name = "txtTempStorage";
			this.txtTempStorage.Text = "XX";
			this.txtTempStorage.Top = 2.645833F;
			this.txtTempStorage.Width = 0.3125F;
			// 
			// txtBurialSea
			// 
			this.txtBurialSea.Height = 0.1875F;
			this.txtBurialSea.Left = 2.979167F;
			this.txtBurialSea.Name = "txtBurialSea";
			this.txtBurialSea.Text = "XX";
			this.txtBurialSea.Top = 2.833333F;
			this.txtBurialSea.Width = 0.3125F;
			// 
			// txtBurial
			// 
			this.txtBurial.Height = 0.1875F;
			this.txtBurial.Left = 4.354167F;
			this.txtBurial.Name = "txtBurial";
			this.txtBurial.Text = "XX";
			this.txtBurial.Top = 2.645833F;
			this.txtBurial.Width = 0.3125F;
			// 
			// txtMedicalScience
			// 
			this.txtMedicalScience.Height = 0.1875F;
			this.txtMedicalScience.Left = 4.354167F;
			this.txtMedicalScience.Name = "txtMedicalScience";
			this.txtMedicalScience.Text = "XX";
			this.txtMedicalScience.Top = 2.833333F;
			this.txtMedicalScience.Width = 0.3125F;
			// 
			// txtCremation
			// 
			this.txtCremation.Height = 0.1875F;
			this.txtCremation.Left = 5.354167F;
			this.txtCremation.Name = "txtCremation";
			this.txtCremation.Text = "XX";
			this.txtCremation.Top = 2.645833F;
			this.txtCremation.Width = 0.3125F;
			// 
			// txtDisinterment
			// 
			this.txtDisinterment.Height = 0.1875F;
			this.txtDisinterment.Left = 6.3125F;
			this.txtDisinterment.Name = "txtDisinterment";
			this.txtDisinterment.Text = "XX";
			this.txtDisinterment.Top = 2.833333F;
			this.txtDisinterment.Width = 0.3125F;
			// 
			// txtDeathCertificate
			// 
			this.txtDeathCertificate.Height = 0.1875F;
			this.txtDeathCertificate.Left = 1.197917F;
			this.txtDeathCertificate.Name = "txtDeathCertificate";
			this.txtDeathCertificate.Text = "XX";
			this.txtDeathCertificate.Top = 3.083333F;
			this.txtDeathCertificate.Width = 0.3125F;
			// 
			// txtReportDeath
			// 
			this.txtReportDeath.Height = 0.1875F;
			this.txtReportDeath.Left = 2.364583F;
			this.txtReportDeath.Name = "txtReportDeath";
			this.txtReportDeath.Text = "XX";
			this.txtReportDeath.Top = 3.083333F;
			this.txtReportDeath.Width = 0.3125F;
			// 
			// txtReleaseCreamation
			// 
			this.txtReleaseCreamation.Height = 0.1875F;
			this.txtReleaseCreamation.Left = 3.770833F;
			this.txtReleaseCreamation.Name = "txtReleaseCreamation";
			this.txtReleaseCreamation.Text = "XX";
			this.txtReleaseCreamation.Top = 3.083333F;
			this.txtReleaseCreamation.Width = 0.3125F;
			// 
			// txtCourtOrder
			// 
			this.txtCourtOrder.Height = 0.1875F;
			this.txtCourtOrder.Left = 6.114583F;
			this.txtCourtOrder.Name = "txtCourtOrder";
			this.txtCourtOrder.Text = "XX";
			this.txtCourtOrder.Top = 3.083333F;
			this.txtCourtOrder.Width = 0.3125F;
			// 
			// txtTNameVault
			// 
			this.txtTNameVault.CanGrow = false;
			this.txtTNameVault.Height = 0.1875F;
			this.txtTNameVault.Left = 1.395833F;
			this.txtTNameVault.Name = "txtTNameVault";
			this.txtTNameVault.Text = null;
			this.txtTNameVault.Top = 4.916667F;
			this.txtTNameVault.Width = 3.375F;
			// 
			// txtTLocation
			// 
			this.txtTLocation.CanGrow = false;
			this.txtTLocation.Height = 0.1875F;
			this.txtTLocation.Left = 4.895833F;
			this.txtTLocation.Name = "txtTLocation";
			this.txtTLocation.Text = null;
			this.txtTLocation.Top = 4.916667F;
			this.txtTLocation.Width = 2F;
			// 
			// txtTDate
			// 
			this.txtTDate.CanGrow = false;
			this.txtTDate.Height = 0.1875F;
			this.txtTDate.Left = 5.583333F;
			this.txtTDate.Name = "txtTDate";
			this.txtTDate.Style = "text-align: right";
			this.txtTDate.Text = null;
			this.txtTDate.Top = 5.479167F;
			this.txtTDate.Width = 1.4375F;
			// 
			// txtTX
			// 
			this.txtTX.Height = 0.1875F;
			this.txtTX.Left = 0F;
			this.txtTX.Name = "txtTX";
			this.txtTX.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtTX.Text = "X";
			this.txtTX.Top = 4.916667F;
			this.txtTX.Width = 0.1875F;
			// 
			// txtRName
			// 
			this.txtRName.CanGrow = false;
			this.txtRName.Height = 0.1875F;
			this.txtRName.Left = 1.395833F;
			this.txtRName.Name = "txtRName";
			this.txtRName.Text = null;
			this.txtRName.Top = 6.104167F;
			this.txtRName.Width = 3.375F;
			// 
			// txtRLocation
			// 
			this.txtRLocation.CanGrow = false;
			this.txtRLocation.Height = 0.1875F;
			this.txtRLocation.Left = 4.895833F;
			this.txtRLocation.Name = "txtRLocation";
			this.txtRLocation.Text = null;
			this.txtRLocation.Top = 6.104167F;
			this.txtRLocation.Width = 2F;
			// 
			// txtRDate
			// 
			this.txtRDate.CanGrow = false;
			this.txtRDate.Height = 0.1875F;
			this.txtRDate.Left = 5.583333F;
			this.txtRDate.Name = "txtRDate";
			this.txtRDate.Style = "text-align: right";
			this.txtRDate.Text = null;
			this.txtRDate.Top = 6.666667F;
			this.txtRDate.Width = 1.4375F;
			// 
			// txtRX1
			// 
			this.txtRX1.Height = 0.1875F;
			this.txtRX1.Left = 0F;
			this.txtRX1.Name = "txtRX1";
			this.txtRX1.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtRX1.Text = "X";
			this.txtRX1.Top = 5.979167F;
			this.txtRX1.Width = 0.1875F;
			// 
			// txtRX2
			// 
			this.txtRX2.Height = 0.1875F;
			this.txtRX2.Left = 0F;
			this.txtRX2.Name = "txtRX2";
			this.txtRX2.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtRX2.Text = "X";
			this.txtRX2.Top = 6.131945F;
			this.txtRX2.Width = 0.1875F;
			// 
			// txtRX3
			// 
			this.txtRX3.Height = 0.1875F;
			this.txtRX3.Left = 0F;
			this.txtRX3.Name = "txtRX3";
			this.txtRX3.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtRX3.Text = "X";
			this.txtRX3.Top = 6.302083F;
			this.txtRX3.Width = 0.1875F;
			// 
			// txtRX4
			// 
			this.txtRX4.Height = 0.1875F;
			this.txtRX4.Left = 0F;
			this.txtRX4.Name = "txtRX4";
			this.txtRX4.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtRX4.Text = "X";
			this.txtRX4.Top = 6.458333F;
			this.txtRX4.Width = 0.1875F;
			// 
			// txtRX5
			// 
			this.txtRX5.Height = 0.1875F;
			this.txtRX5.Left = 0F;
			this.txtRX5.Name = "txtRX5";
			this.txtRX5.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtRX5.Text = "X";
			this.txtRX5.Top = 6.625F;
			this.txtRX5.Width = 0.1875F;
			// 
			// txtRX6
			// 
			this.txtRX6.Height = 0.1875F;
			this.txtRX6.Left = 0F;
			this.txtRX6.Name = "txtRX6";
			this.txtRX6.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtRX6.Text = "X";
			this.txtRX6.Top = 6.791667F;
			this.txtRX6.Width = 0.1875F;
			// 
			// CremName
			// 
			this.CremName.CanGrow = false;
			this.CremName.Height = 0.1875F;
			this.CremName.Left = 1.395833F;
			this.CremName.Name = "CremName";
			this.CremName.Text = null;
			this.CremName.Top = 7.166667F;
			this.CremName.Width = 3.375F;
			// 
			// CremLoc
			// 
			this.CremLoc.CanGrow = false;
			this.CremLoc.Height = 0.1875F;
			this.CremLoc.Left = 4.895833F;
			this.CremLoc.Name = "CremLoc";
			this.CremLoc.Text = null;
			this.CremLoc.Top = 7.166667F;
			this.CremLoc.Width = 2F;
			// 
			// CremDate
			// 
			this.CremDate.CanGrow = false;
			this.CremDate.Height = 0.1875F;
			this.CremDate.Left = 5.583333F;
			this.CremDate.Name = "CremDate";
			this.CremDate.Style = "text-align: right";
			this.CremDate.Text = null;
			this.CremDate.Top = 7.729167F;
			this.CremDate.Width = 1.4375F;
			// 
			// CremBuried
			// 
			this.CremBuried.Height = 0.1875F;
			this.CremBuried.Left = 0F;
			this.CremBuried.Name = "CremBuried";
			this.CremBuried.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.CremBuried.Text = "X";
			this.CremBuried.Top = 7.395833F;
			this.CremBuried.Width = 0.1875F;
			// 
			// CremScattered
			// 
			this.CremScattered.Height = 0.1875F;
			this.CremScattered.Left = 0F;
			this.CremScattered.Name = "CremScattered";
			this.CremScattered.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.CremScattered.Text = "X";
			this.CremScattered.Top = 7.572917F;
			this.CremScattered.Width = 0.1875F;
			// 
			// CremFamily
			// 
			this.CremFamily.Height = 0.1875F;
			this.CremFamily.Left = 0F;
			this.CremFamily.Name = "CremFamily";
			this.CremFamily.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.CremFamily.Text = "X";
			this.CremFamily.Top = 7.770833F;
			this.CremFamily.Width = 0.1875F;
			// 
			// txtDName
			// 
			this.txtDName.CanGrow = false;
			this.txtDName.Height = 0.1875F;
			this.txtDName.Left = 1.395833F;
			this.txtDName.Name = "txtDName";
			this.txtDName.Text = null;
			this.txtDName.Top = 8.354167F;
			this.txtDName.Width = 3.375F;
			// 
			// txtDLocation
			// 
			this.txtDLocation.CanGrow = false;
			this.txtDLocation.Height = 0.1875F;
			this.txtDLocation.Left = 4.895833F;
			this.txtDLocation.Name = "txtDLocation";
			this.txtDLocation.Text = null;
			this.txtDLocation.Top = 8.354167F;
			this.txtDLocation.Width = 2F;
			// 
			// txtDDate
			// 
			this.txtDDate.CanGrow = false;
			this.txtDDate.Height = 0.1875F;
			this.txtDDate.Left = 5.583333F;
			this.txtDDate.Name = "txtDDate";
			this.txtDDate.Style = "text-align: right";
			this.txtDDate.Text = null;
			this.txtDDate.Top = 8.916667F;
			this.txtDDate.Width = 1.4375F;
			// 
			// txtDX
			// 
			this.txtDX.Height = 0.1875F;
			this.txtDX.Left = 0F;
			this.txtDX.Name = "txtDX";
			this.txtDX.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtDX.Text = "X";
			this.txtDX.Top = 8.479167F;
			this.txtDX.Width = 0.1875F;
			// 
			// txtDateSigned
			// 
			this.txtDateSigned.Height = 0.2083333F;
			this.txtDateSigned.Left = 5.760417F;
			this.txtDateSigned.Name = "txtDateSigned";
			this.txtDateSigned.Text = null;
			this.txtDateSigned.Top = 4.197917F;
			this.txtDateSigned.Width = 1.697917F;
			// 
			// txtArmedForcesYes
			// 
			this.txtArmedForcesYes.Height = 0.1875F;
			this.txtArmedForcesYes.Left = 2.583333F;
			this.txtArmedForcesYes.Name = "txtArmedForcesYes";
			this.txtArmedForcesYes.Text = "X";
			this.txtArmedForcesYes.Top = 1.8125F;
			this.txtArmedForcesYes.Width = 0.3125F;
			// 
			// txtArmedForcesNo
			// 
			this.txtArmedForcesNo.Height = 0.1875F;
			this.txtArmedForcesNo.Left = 2.583333F;
			this.txtArmedForcesNo.Name = "txtArmedForcesNo";
			this.txtArmedForcesNo.Text = "X";
			this.txtArmedForcesNo.Top = 2F;
			this.txtArmedForcesNo.Width = 0.2291667F;
			// 
			// txtClerkTown
			// 
			this.txtClerkTown.Height = 0.2083333F;
			this.txtClerkTown.Left = 3.583333F;
			this.txtClerkTown.Name = "txtClerkTown";
			this.txtClerkTown.Text = null;
			this.txtClerkTown.Top = 4.208333F;
			this.txtClerkTown.Width = 1.822917F;
			// 
			// txtMausoleum
			// 
			this.txtMausoleum.Height = 0.1875F;
			this.txtMausoleum.Left = 6.3125F;
			this.txtMausoleum.Name = "txtMausoleum";
			this.txtMausoleum.Text = "XX";
			this.txtMausoleum.Top = 2.645833F;
			this.txtMausoleum.Width = 0.3125F;
			// 
			// rptBurialPermit
			//
			// 
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.3472222F;
			this.PageSettings.Margins.Left = 0.625F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.4166667F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.59375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtFullName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFacility)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemovalState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTempStorage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBurialSea)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBurial)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMedicalScience)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCremation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDisinterment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeathCertificate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReportDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReleaseCreamation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCourtOrder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTNameVault)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRX6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CremName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CremLoc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CremDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CremBuried)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CremScattered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CremFamily)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateSigned)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArmedForcesYes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtArmedForcesNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerkTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMausoleum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSex;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFacility;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateOfDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlaceOfDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRemovalState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTempStorage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBurialSea;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBurial;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedicalScience;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCremation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDisinterment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeathCertificate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReportDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReleaseCreamation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCourtOrder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTNameVault;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTX;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRX1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRX2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRX3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRX4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRX5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRX6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox CremName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox CremLoc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox CremDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox CremBuried;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox CremScattered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox CremFamily;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDX;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateSigned;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtArmedForcesYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtArmedForcesNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClerkTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMausoleum;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
