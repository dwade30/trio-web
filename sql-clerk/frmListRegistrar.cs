//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmListRegistrar : BaseForm
	{
		public frmListRegistrar()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmListRegistrar InstancePtr
		{
			get
			{
				return (frmListRegistrar)Sys.GetInstance(typeof(frmListRegistrar));
			}
		}

		protected frmListRegistrar _InstancePtr = null;
		//=========================================================
		int NameCol;
		int TownCol;
		int KeyCol;

		private void cmdCancel_Click()
		{
			Close();
		}

		private void frmListRegistrar_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmListRegistrar_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				vsClerks_DblClick(null, null);
			}
			else if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmListRegistrar_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmListRegistrar properties;
			//frmListRegistrar.ScaleWidth	= 5910;
			//frmListRegistrar.ScaleHeight	= 3990;
			//frmListRegistrar.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this);
			KeyCol = 0;
			NameCol = 1;
			TownCol = 2;
			vsClerks.TextMatrix(0, NameCol, "Name");
			vsClerks.TextMatrix(0, TownCol, "Town");
			vsClerks.ColHidden(KeyCol, true);
			vsClerks.ColWidth(NameCol, FCConvert.ToInt32(vsClerks.WidthOriginal * 0.7));
			LoadData();
		}

		private void LoadData()
		{
			modGNBas.Statics.gstrRegistrar = string.Empty;
			modGNBas.Statics.gstrRegistrarTown = string.Empty;
			string strTemp = "";
			clsDRWrapper rsRegistrar = new clsDRWrapper();
			rsRegistrar.OpenRecordset("SELECT * FROM DefaultRegistrarNames order by lastname asc", modGNBas.DEFAULTDATABASE);
			vsClerks.Rows = 1;
			while (!rsRegistrar.EndOfFile())
			{
				vsClerks.Rows += 1;
				vsClerks.TextMatrix(vsClerks.Rows - 1, KeyCol, FCConvert.ToString(rsRegistrar.Get_Fields_Int32("ID")));
				vsClerks.TextMatrix(vsClerks.Rows - 1, NameCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsRegistrar.Get_Fields_String("Lastname"))) + ", " + fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(rsRegistrar.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRegistrar.Get_Fields_String("MI")))));
				vsClerks.TextMatrix(vsClerks.Rows - 1, TownCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsRegistrar.Get_Fields_String("ClerkTown"))));
				rsRegistrar.MoveNext();
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuEdit_Click(object sender, System.EventArgs e)
		{
			frmRegistrarNames.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			LoadData();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsClerks_DblClick(object sender, System.EventArgs e)
		{
			if (vsClerks.MouseRow == 0)
			{
				MessageBox.Show("A valid entry must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			modGNBas.Statics.gstrRegistrar = vsClerks.TextMatrix(vsClerks.MouseRow, KeyCol);
			Close();
		}
	}
}
