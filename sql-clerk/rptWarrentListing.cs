//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptWarrentListing.
	/// </summary>
	public partial class rptWarrentListing : BaseSectionReport
	{
		public rptWarrentListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Warrant Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptWarrentListing InstancePtr
		{
			get
			{
				return (rptWarrentListing)Sys.GetInstance(typeof(rptWarrentListing));
			}
		}

		protected rptWarrentListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
				clsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWarrentListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strPhone = "";
		int intLine;
		clsDRWrapper rs = new clsDRWrapper();
		int intPageNumber;
		clsDRWrapper clsTemp = new clsDRWrapper();
		private bool boolUpdateWarrant;
		private bool boolUnregistered;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strTemp = "";
			string[] strAry = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			clsDRWrapper rsSave = new clsDRWrapper();
			/*? On Error Resume Next  */
			NextDog:
			;
			if (rs.EndOfFile())
				return;
			txtDogName.Text = rs.Get_Fields_String("DogName");
			txtOwnerName.Text = rs.Get_Fields_String("FullName");
			txtAddress.Text = rs.Get_Fields_String("Address1") + " " + rs.Get_Fields_String("City") + ", " + rs.Get_Fields("State") + " " + rs.Get_Fields_String("Zip");
			// kk04172015  rs.Fields("Address") and  rs.Fields("PartyState")
			txtLocation.Text = rs.Get_Fields_String("LocationNum") + " " + rs.Get_Fields_String("LocationSTR");
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Phonenumber"))).Length == 7)
			{
				strPhone = "(207) " + Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Phonenumber")), 3) + "-" + Strings.Right(FCConvert.ToString(rs.Get_Fields_String("Phonenumber")), 4);
			}
			else if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Phonenumber"))).Length == 10 && Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Phonenumber"))), 3) == "000")
			{
				strPhone = "(207) " + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("Phonenumber")), 4, 3) + "-" + Strings.Right(FCConvert.ToString(rs.Get_Fields_String("Phonenumber")), 4);
			}
			else
			{
				strPhone = Strings.Format(rs.Get_Fields_String("Phonenumber"), "(###) ###-####");
			}
			if (fecherFoundation.Strings.Trim(strPhone) != "() -")
			{
				txtAddress.Text = txtAddress.Text + "\r" + strPhone;
			}
			txtTagNumber.Text = "";
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("KennelDog")))
			{
				clsTemp.OpenRecordset("select * from kennellicense where ownernum = " + rs.Get_Fields_Int32("ownernum"), "twck0000.vb1");
				while (!clsTemp.EndOfFile())
				{
					strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("dognumbers"));
					if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
					{
						strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
						{
							if (Conversion.Val(strAry[x]) == Conversion.Val(rs.Get_Fields_Int32("dognumber")))
							{
								txtTagNumber.Text = "K# " + FCConvert.ToString(modGlobalRoutines.GetKennelLicenseNumber(clsTemp.Get_Fields_Int32("licenseid")));
								if (clsTemp.Get_Fields("year") >= modGNBas.Statics.gintYear)
								{
									rs.MoveNext();
									goto NextDog;
								}
							}
						}
						// x
					}
					clsTemp.MoveNext();
				}
			}
			else
			{
				txtTagNumber.Text = rs.Get_Fields_String("TagLicNum");
			}
			intLine += 1;
			txtLine.Text = intLine.ToString();
			if (boolUpdateWarrant)
			{
				if (modGNBas.Statics.gintYear == DateTime.Today.Year)
				{
					rsSave.Execute("update doginfo set warrantyear = " + FCConvert.ToString(DateTime.Today.Year) + " where ID = " + rs.Get_Fields_Int32("dognumber"), "twck0000.vb1");
				}
			}
			// If RS.Fields("TagLicNum") = vbNullString Then
			// If RS.Fields("KennelDog") Then
			// If Val(RS.Fields("K20Num")) = 0 Then
			// txtTagNumber = "K# " & GetKennelLicenseNumber(RS.Fields("K10Num"))
			// Else
			// txtTagNumber = "K# " & GetKennelLicenseNumber(RS.Fields("K10Num")) & "/" & GetKennelLicenseNumber(RS.Fields("K20Num"))
			// End If
			// End If
			// Else
			// txtTagNumber = RS.Fields("TagLicNum")
			// End If
			if (!rs.EndOfFile())
				rs.MoveNext();
			eArgs.EOF = false;
			rsSave.Dispose();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			txtTown.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "H:mm AM/PM");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtCaption.Text = "Town Clerk's Report";
			intPageNumber = 1;
			modGNBas.Statics.gstrWhereClause += " and DogDeceased <> 1";
			if (!boolUnregistered)
			{
				modGNBas.Statics.gstrWhereClause += " and ExcludeFromWarrant <> 1";
			}
			if (modGNBas.Statics.gstrSortField == "LastName")
				modGNBas.Statics.gstrSortField = "LastName,FirstName";
			if (modGNBas.Statics.gstrSortField == string.Empty)
			{
				// gstrWarrantSQL = "SELECT States.Description, DogOwner.*, DogInfo.* FROM (DogInfo INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID) INNER JOIN States ON States.ID = val(DogOwner.State) where DogInfo.DogDeceased <> 1 AND DogInfo.Year < " & gintYear & gstrWhereClause
				// gstrWarrantSQL = "SELECT  * FROM DogInfo INNER JOIN DogOwner ON (DogInfo.OwnerNum = dogowner.ID) CROSS APPLY " & rs.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddressAndPhone(PartyID, NULL, 'CK', NULL) as p where DogInfo.DogDeceased <> 1 AND DogInfo.Year < " & gintYear & gstrWhereClause
				modGNBas.Statics.gstrWarrantSQL = "select " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView.*,DogOwner.LocationNum, DogOwner.PartyID, DogOwner.LocationSTR, DogInfo.* from doginfo inner join dogowner on (doginfo.ownernum = dogowner.id) inner join " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView on (dogowner.partyid = " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView.PartyID) " + " where DogInfo.DogDeceased <> 1 AND DogInfo.Year < " + FCConvert.ToString(modGNBas.Statics.gintYear) + modGNBas.Statics.gstrWhereClause;
			}
			else
			{
				// gstrWarrantSQL = "SELECT States.Description, DogOwner.*, DogInfo.* FROM (DogInfo INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID) INNER JOIN States ON States.ID = val(DogOwner.State) where DogInfo.DogDeceased <> 1 AND DogInfo.Year < " & gintYear & gstrWhereClause & " Order by " & gstrSortField
				// gstrWarrantSQL = "SELECT  * FROM DogInfo INNER JOIN DogOwner ON (DogInfo.OwnerNum = dogowner.ID) CROSS APPLY " & rs.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddressANDpHONE(PartyID, NULL, 'CK', NULL) as p where DogInfo.DogDeceased <> 1 AND DogInfo.Year < " & gintYear & gstrWhereClause & " Order by " & gstrSortField
				modGNBas.Statics.gstrWarrantSQL = "select " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView.*,DogOwner.LocationNum, DogOwner.PartyID, DogOwner.LocationSTR, DogInfo.* from doginfo inner join dogowner on (doginfo.ownernum = dogowner.id) inner join " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView on (dogowner.partyid = " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView.PartyID) " + " where DogInfo.DogDeceased <> 1 AND DogInfo.Year < " + FCConvert.ToString(modGNBas.Statics.gintYear) + modGNBas.Statics.gstrWhereClause + " Order by " + modGNBas.Statics.gstrSortField;
			}
			rs.OpenRecordset(modGNBas.Statics.gstrWarrantSQL, modGNBas.DEFAULTDATABASE);
			intLine = 0;
			// txtDate = Format(Date, "MM/dd/yyyy") & "  " & Format(Time, "HH:MM AMPM")
			// Call SetPrintProperties(Me)
		}

		public void Init(bool modalDialog, bool boolUnregisteredReport = false, bool boolServeWarrant = false)
		{
			boolUpdateWarrant = boolServeWarrant;
			string strCap = "";
			boolUnregistered = boolUnregisteredReport;
			if (boolUnregisteredReport)
			{
				txtCaption2.Text = "Unregistered Dogs - " + FCConvert.ToString(modGNBas.Statics.gintYear);
				strCap = "UnregisteredDogs";
			}
			else
			{
				txtCaption2.Text = "Municipal Warrant Listing - " + FCConvert.ToString(modGNBas.Statics.gintYear);
				strCap = "WarrantListing";
			}
			frmReportViewer.InstancePtr.Init(this, intModal: 1, boolAllowEmail: true, strAttachmentName: strCap, showModal: modalDialog);
		}

		
	}
}
