//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogLicenseLaser.
	/// </summary>
	public partial class rptDogLicenseLaser : BaseSectionReport
	{
		public rptDogLicenseLaser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Dog License Laser";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDogLicenseLaser InstancePtr
		{
			get
			{
				return (rptDogLicenseLaser)Sys.GetInstance(typeof(rptDogLicenseLaser));
			}
		}

		protected rptDogLicenseLaser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDogLicenseLaser	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		private bool boolPrinted;
		private int intPrinted;
		bool boolLookUpLast;

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted = this.Printer.DeviceCopies;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			/*? On Error Resume Next  */
			clsDRWrapper clsTemp = new clsDRWrapper();
			string[] strAry = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngTemp = 0;
			cParty tPart;
			cPartyController tPartCont = new cPartyController();
			cPartyPhoneNumber tPhone = new cPartyPhoneNumber();
			if (rs.EndOfFile())
				return;
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) == 0)
			{
				txtMuni.Text = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				txtMuni.Text = modRegionalTown.GetTownKeyName_2(rs.Get_Fields_Int32("towncode"));
			}
			if (rs.Get_Fields_DateTime("dogdob").ToOADate() != 0)
			{
				txtDOB.Text = FCConvert.ToString(rs.Get_Fields("DogDOB"));
			}
			txtSex.Text = rs.Get_Fields_String("DogSex");
			txtDogName.Text = rs.Get_Fields_String("DogName");
			txtVeterinarian.Text = rs.Get_Fields_String("DogVet");
			txtColor.Text = rs.Get_Fields_String("DogColor");			
			txtOwnerName.Text = rs.Get_Fields_String("FullName");
			tPart = tPartCont.GetParty(rs.Get_Fields_Int32("partyid"));
			if (!(tPart == null))
			{
				if (tPart.PhoneNumbers.Count > 0)
				{
					tPhone = tPart.PhoneNumbers[1];
				}
			}
			txtAddress.Text = rs.Get_Fields_String("Address1") + "  " + rs.Get_Fields_String("City") + ", " + rs.Get_Fields("State") + " " + (FCConvert.ToString(rs.Get_Fields_String("Zip")).Length > 5 ? Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Zip")), 5) + "-" + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("Zip")), 6, FCConvert.ToString(rs.Get_Fields_String("Zip")).Length - 5) : rs.Get_Fields_String("Zip"));
			txtLocation.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("LocationNum") + " " + rs.Get_Fields_String("LocationSTR"));
			// txtSticker = "Sticker #" & rs.Fields("StickerLicNum")
			txtBreed.Text = rs.Get_Fields_String("DogBreed");
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("KennelDog")))
			{
				// txtTagNumber = "K " & RS.Fields("TagLicNum")
				txtTagNumber.Text = "";
				clsTemp.OpenRecordset("select * from kennellicense where ownernum = " + rs.Get_Fields_Int32("ownernum"), "twck0000.vb1");
				while (!clsTemp.EndOfFile())
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("Dognumbers"))) != string.Empty)
					{
						strAry = Strings.Split(FCConvert.ToString(clsTemp.Get_Fields_String("dognumbers")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
						{
							if (Conversion.Val(strAry[x]) == rs.Get_Fields_Int32("dognumber"))
							{
								lngTemp = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("licenseid"));
								clsTemp.OpenRecordset("select * from doginventory where ID = " + FCConvert.ToString(lngTemp), "twck0000.vb1");
								if (!clsTemp.EndOfFile())
								{
									txtTagNumber.Text = "K " + clsTemp.Get_Fields_Int32("stickernumber");
								}
								break;
							}
						}
						// x
					}
					clsTemp.MoveNext();
				}
			}
			else
			{
				txtTagNumber.Text = rs.Get_Fields_String("TagLicNum");
			}
			// txtAmountC = Format(RS.Fields("AmountC"), "#,##0.00")
			if (FCConvert.ToBoolean(Conversion.Val(modGNBas.Statics.typDogTransaction.Amount > 0)) | !boolLookUpLast)
			{
				txtAmountC.Text = Strings.Format(Conversion.Val(modGNBas.Statics.typDogTransaction.Amount), "#,##0.00");
			}
			else
			{
				clsTemp.OpenRecordset("select * from dogtransactions where boollicense = 1 and dognumber = " + FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("dognumber"))) + " order by transactiondate desc", "twck0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					txtAmountC.Text = Strings.Format(Conversion.Val(clsTemp.Get_Fields("amount")), "#,##0.00");
				}
				else
				{
					txtAmountC.Text = Strings.Format(Conversion.Val(modGNBas.Statics.typDogTransaction.Amount), "#,##0.00");
				}
			}
			txtRabiesCertNum.Text = rs.Get_Fields_String("RabiesCertNum");
			txtRabiesTagNum.Text = rs.Get_Fields_String("RabiesTagNo");
			if (Information.IsDate(rs.Get_Fields("rabiesexpdate")))
			{
				if (rs.Get_Fields_DateTime("rabiesexpdate").ToOADate() != 0)
				{
					if (fecherFoundation.DateAndTime.DateDiff("d", (DateTime)rs.Get_Fields_DateTime("rabiesexpdate"), DateTime.Today) <= 0)
					{
						txtVaccineExpires.Text = FCConvert.ToString(rs.Get_Fields_DateTime("RabiesExpDate"));
					}
					else
					{
						if (!FCConvert.ToBoolean(rs.Get_Fields_Boolean("RabiesExempt")))
						{
							txtVaccineExpires.Text = "Expired";
						}
						else
						{
							txtVaccineExpires.Text = "EXEMPT";
						}
					}
				}
				else
				{
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("RabiesExempt")))
					{
						txtVaccineExpires.Text = "EXEMPT";
					}
					else
					{
						txtVaccineExpires.Text = "";
					}
				}
			}
			else
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("RabiesExempt")))
				{
					txtVaccineExpires.Text = "EXEMPT";
				}
				else
				{
					txtVaccineExpires.Text = "";
				}
			}
			// txtYear = gintLaserReportYear
			txtYear.Text = FCConvert.ToString(rs.Get_Fields("year"));
			// txtDateVaccinated = RS.Fields("DateVaccinated")
			if (Information.IsDate(rs.Get_Fields("rabiesshotdate")))
			{
				if (Convert.ToDateTime(rs.Get_Fields_DateTime("rabiesshotdate")).ToOADate() != 0)
				{
					txtDateVaccinated.Text = Strings.Format(rs.Get_Fields_DateTime("rabiesshotdate"), "MM/dd/yyyy");
				}
				else
				{
					txtDateVaccinated.Text = "";
				}
			}
			else
			{
				txtDateVaccinated.Text = "";
			}
			txtMF.Visible = true;
			txtMF.Text = rs.Get_Fields_String("DogSex");
			txtNeuter.Visible = rs.Get_Fields_Boolean("DogNeuter");
			txtSSR.Visible = rs.Get_Fields_Boolean("SandR");
			txtHearing.Visible = rs.Get_Fields_Boolean("HearGuide");
			txtHybrid.Visible = rs.Get_Fields_Boolean("WolfHybrid");
            txtNuisanceDangerous.Visible =
                rs.Get_Fields_Boolean("IsDangerousDog") || rs.Get_Fields_Boolean("IsNuisanceDog");

			lblWolf.Visible = txtHybrid.Visible;
			lblHearing.Visible = txtHearing.Visible;
			lblRescue.Visible = txtSSR.Visible;
			lblNeuter.Visible = txtNeuter.Visible;
			lblMF.Visible = txtMF.Visible;
            lblNuisanceDangerous.Visible = txtNuisanceDangerous.Visible;
            if (rs.Get_Fields_Boolean("IsDangerousDog"))
            {
                lblNuisanceDangerous.Text = "Dangerous";
            }
            else
            {
                lblNuisanceDangerous.Text = "Nuisance";
            }
			txtLicenseDate.Text = FCConvert.ToString(rs.Get_Fields("RabStickerIssue"));
			txtCertNumber.Text = rs.Get_Fields_String("SNCertNum");
			if (!(tPhone == null))
			{
				txtPhone.Text = tPhone.PhoneNumber;
				// If Len(Trim(rs.Fields("Phone"))) = 7 Then
				// txtPhone = "(207) " & Left(rs.Fields("Phone"), 3) & "-" & Right(rs.Fields("Phone"), 4)
				// Else
				// txtPhone = Format(rs.Fields("Phone"), "(###) ###-####")
				// End If
			}
			if (!rs.EndOfFile())
				rs.MoveNext();
			eArgs.EOF = rs.EndOfFile();
			clsTemp.Dispose();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call RS.OpenRecordset("SELECT DogOwner.Address,DogOwner.city, DogOwner.state, DogOwner.zip, DogOwner.Phone,DogOwner.FirstName, DogOwner.MI, DogOwner.LastName, DogOwner.LocationNum, DogOwner.Desig, DogOwner.LocationSTR, DogInfo.* FROM DogInfo INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID where DogInfo.DogNumber = " & dInfo.DogNumber, DEFAULTDATABASENAME)
			// Call SetPrintProperties(Me)
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	// Call VerifyPrintToFile(Me, Tool)
		//}
		public void init(ref int lngdognum, bool boolLookUpAmount = false)
		{
			string strSQL;
			// Call rs.OpenRecordset("SELECT DogOwner.Address,DogOwner.city, DogOwner.state, DogOwner.zip, DogOwner.Phone,DogOwner.FirstName, DogOwner.MI, DogOwner.LastName, DogOwner.LocationNum, DogOwner.Desig, DogOwner.LocationSTR,DOGowner.towncode, DogInfo.* FROM DogInfo INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID where DogInfo.ID = " & lngdognum, "twck0000.vb1")
			strSQL = "Select dogowner.partyid,FullName,Address1,address2,address3,city,state,zip,dogowner.locationnum, DogOwner.LocationSTR,DOGowner.towncode, DogInfo.* ,doginfo.id as dognumber from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID) inner join " + rs.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView on (dogowner.partyid = " + rs.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView.Id)  where DogInfo.ID = " + FCConvert.ToString(lngdognum);
			rs.OpenRecordset(strSQL, "Clerk");
			// Call rs.OpenRecordset("SELECT DogOwner.Address,DogOwner.city, DogOwner.state, DogOwner.zip, DogOwner.Phone,DogOwner.FirstName, DogOwner.MI, DogOwner.LastName, DogOwner.LocationNum, DogOwner.Desig, DogOwner.LocationSTR,DOGowner.towncode, DogInfo.* FROM DogInfo INNER JOIN (DogOwner inner join ON DogInfo.OwnerNum = dogowner.ID where DogInfo.ID = " & lngdognum, "twck0000.vb1")
			// Me.Show vbModal
			boolLookUpLast = boolLookUpAmount;
            //FC:FINAL:SBE - #i1996 - show report as modal dialog
            //frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: false);
            frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: false, showModal: true);
        }

		
	}
}
