﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMarriageLicense.
	/// </summary>
	partial class rptLicense
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLicense));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtGroomFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateIntenstionsFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCeremonyDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCountyMarried = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPersonPerformingCeremony = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCityMarried = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateLicenseIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsDesig = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsFathersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMothersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMothersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesSurName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFathersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMothersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMothersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMarriageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMarriageEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMarriageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMarriageEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCityofIssue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLicenseValidUntil = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitleOfPersonPerforming = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateOfCommision = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtResidenceOfPersonPerforming = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddressOfPersonPerforming = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomEndedWhy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBrideEndedWhy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWitnessOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWitnessTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSignitureWit2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSignitureWit1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSigniturePerson = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomFormerSpouse = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomCourtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideFormerSpouse = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideCourtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtstate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIssuance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMarriage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateIntenstionsFiled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCeremonyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountyMarried)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonPerformingCeremony)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityMarried)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateLicenseIssued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDesig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesSurName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEnded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEnded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityofIssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenseValidUntil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleOfPersonPerforming)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateOfCommision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResidenceOfPersonPerforming)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddressOfPersonPerforming)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedWhy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedWhy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitnessOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitnessTwo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignitureWit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignitureWit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSigniturePerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomFormerSpouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomCourtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideFormerSpouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideCourtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssuance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarriage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroomFirst,
            this.txtGroomStreet,
            this.txtGroomAge,
            this.txtGroomBirthplace,
            this.txtDateIntenstionsFiled,
            this.txtCeremonyDate,
            this.txtCountyMarried,
            this.txtPersonPerformingCeremony,
            this.txtCityMarried,
            this.txtDateLicenseIssued,
            this.txtGroomMiddle,
            this.txtGroomLast,
            this.txtGroomsDesig,
            this.txtGroomsState,
            this.txtGroomsCounty,
            this.txtGroomsCity,
            this.txtGroomsDOB,
            this.txtGroomsFathersName,
            this.txtGroomsFathersBirthplace,
            this.txtGroomsMothersName,
            this.txtGroomsMothersBirthplace,
            this.txtBridesFirst,
            this.txtBridesStreet,
            this.txtBridesAge,
            this.txtBridesBirthplace,
            this.txtBridesMiddle,
            this.txtBridesSurName,
            this.txtBridesState,
            this.txtBridesCounty,
            this.txtBridesCity,
            this.txtBridesFathersName,
            this.txtBridesFathersBirthplace,
            this.txtBridesMothersName,
            this.txtBridesDOB,
            this.txtBridesMothersBirthplace,
            this.txtBridesLast,
            this.txtGroomsMarriageNumber,
            this.txtGroomsMarriageEnded,
            this.txtBridesMarriageNumber,
            this.txtBridesMarriageEnded,
            this.txtCityofIssue,
            this.txtLicenseValidUntil,
            this.txtTitleOfPersonPerforming,
            this.txtDateOfCommision,
            this.txtResidenceOfPersonPerforming,
            this.txtAddressOfPersonPerforming,
            this.txtGroomEndedWhy,
            this.txtBrideEndedWhy,
            this.txtWitnessOne,
            this.txtWitnessTwo,
            this.txtDateFiled,
            this.txtSignitureWit2,
            this.txtSignitureWit1,
            this.txtSigniturePerson,
            this.fldGroomFormerSpouse,
            this.fldGroomCourtName,
            this.fldBrideFormerSpouse,
            this.fldBrideCourtName});
            this.Detail.Height = 9.833333F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtGroomFirst
            // 
            this.txtGroomFirst.CanGrow = false;
            this.txtGroomFirst.Height = 0.2083333F;
            this.txtGroomFirst.Left = 0.0625F;
            this.txtGroomFirst.Name = "txtGroomFirst";
            this.txtGroomFirst.Text = null;
            this.txtGroomFirst.Top = 0.5833333F;
            this.txtGroomFirst.Width = 2.1875F;
            // 
            // txtGroomStreet
            // 
            this.txtGroomStreet.CanGrow = false;
            this.txtGroomStreet.Height = 0.1666667F;
            this.txtGroomStreet.Left = 0.0625F;
            this.txtGroomStreet.Name = "txtGroomStreet";
            this.txtGroomStreet.Text = null;
            this.txtGroomStreet.Top = 1.333333F;
            this.txtGroomStreet.Width = 3.6875F;
            // 
            // txtGroomAge
            // 
            this.txtGroomAge.CanGrow = false;
            this.txtGroomAge.Height = 0.2083333F;
            this.txtGroomAge.Left = 0.0625F;
            this.txtGroomAge.Name = "txtGroomAge";
            this.txtGroomAge.Text = null;
            this.txtGroomAge.Top = 0.9583333F;
            this.txtGroomAge.Width = 0.8125F;
            // 
            // txtGroomBirthplace
            // 
            this.txtGroomBirthplace.CanGrow = false;
            this.txtGroomBirthplace.Height = 0.1666667F;
            this.txtGroomBirthplace.Left = 3.9375F;
            this.txtGroomBirthplace.Name = "txtGroomBirthplace";
            this.txtGroomBirthplace.Text = null;
            this.txtGroomBirthplace.Top = 1.333333F;
            this.txtGroomBirthplace.Width = 2.125F;
            // 
            // txtDateIntenstionsFiled
            // 
            this.txtDateIntenstionsFiled.CanGrow = false;
            this.txtDateIntenstionsFiled.Height = 0.1666667F;
            this.txtDateIntenstionsFiled.Left = 1.6875F;
            this.txtDateIntenstionsFiled.Name = "txtDateIntenstionsFiled";
            this.txtDateIntenstionsFiled.Text = null;
            this.txtDateIntenstionsFiled.Top = 5.583333F;
            this.txtDateIntenstionsFiled.Width = 1.25F;
            // 
            // txtCeremonyDate
            // 
            this.txtCeremonyDate.CanGrow = false;
            this.txtCeremonyDate.Height = 0.1666667F;
            this.txtCeremonyDate.Left = 1F;
            this.txtCeremonyDate.Name = "txtCeremonyDate";
            this.txtCeremonyDate.Text = null;
            this.txtCeremonyDate.Top = 7F;
            this.txtCeremonyDate.Visible = false;
            this.txtCeremonyDate.Width = 1.5625F;
            // 
            // txtCountyMarried
            // 
            this.txtCountyMarried.CanGrow = false;
            this.txtCountyMarried.Height = 0.1666667F;
            this.txtCountyMarried.Left = 5.875F;
            this.txtCountyMarried.Name = "txtCountyMarried";
            this.txtCountyMarried.Text = null;
            this.txtCountyMarried.Top = 7F;
            this.txtCountyMarried.Visible = false;
            this.txtCountyMarried.Width = 1.1875F;
            // 
            // txtPersonPerformingCeremony
            // 
            this.txtPersonPerformingCeremony.CanGrow = false;
            this.txtPersonPerformingCeremony.Height = 0.1666667F;
            this.txtPersonPerformingCeremony.Left = 3.5625F;
            this.txtPersonPerformingCeremony.Name = "txtPersonPerformingCeremony";
            this.txtPersonPerformingCeremony.Text = null;
            this.txtPersonPerformingCeremony.Top = 7.375F;
            this.txtPersonPerformingCeremony.Visible = false;
            this.txtPersonPerformingCeremony.Width = 2.5F;
            // 
            // txtCityMarried
            // 
            this.txtCityMarried.CanGrow = false;
            this.txtCityMarried.Height = 0.1666667F;
            this.txtCityMarried.Left = 2.625F;
            this.txtCityMarried.Name = "txtCityMarried";
            this.txtCityMarried.Text = null;
            this.txtCityMarried.Top = 7F;
            this.txtCityMarried.Visible = false;
            this.txtCityMarried.Width = 2.4375F;
            // 
            // txtDateLicenseIssued
            // 
            this.txtDateLicenseIssued.CanGrow = false;
            this.txtDateLicenseIssued.Height = 0.1666667F;
            this.txtDateLicenseIssued.Left = 3.9375F;
            this.txtDateLicenseIssued.Name = "txtDateLicenseIssued";
            this.txtDateLicenseIssued.Text = null;
            this.txtDateLicenseIssued.Top = 5.583333F;
            this.txtDateLicenseIssued.Width = 1.125F;
            // 
            // txtGroomMiddle
            // 
            this.txtGroomMiddle.CanGrow = false;
            this.txtGroomMiddle.Height = 0.2083333F;
            this.txtGroomMiddle.Left = 2.3125F;
            this.txtGroomMiddle.Name = "txtGroomMiddle";
            this.txtGroomMiddle.Text = null;
            this.txtGroomMiddle.Top = 0.5833333F;
            this.txtGroomMiddle.Width = 1.5625F;
            // 
            // txtGroomLast
            // 
            this.txtGroomLast.CanGrow = false;
            this.txtGroomLast.Height = 0.2083333F;
            this.txtGroomLast.Left = 4F;
            this.txtGroomLast.Name = "txtGroomLast";
            this.txtGroomLast.Text = null;
            this.txtGroomLast.Top = 0.5833333F;
            this.txtGroomLast.Width = 2.5625F;
            // 
            // txtGroomsDesig
            // 
            this.txtGroomsDesig.CanGrow = false;
            this.txtGroomsDesig.Height = 0.2083333F;
            this.txtGroomsDesig.Left = 6.625F;
            this.txtGroomsDesig.Name = "txtGroomsDesig";
            this.txtGroomsDesig.Text = null;
            this.txtGroomsDesig.Top = 0.5833333F;
            this.txtGroomsDesig.Width = 0.4375F;
            // 
            // txtGroomsState
            // 
            this.txtGroomsState.CanGrow = false;
            this.txtGroomsState.Height = 0.2083333F;
            this.txtGroomsState.Left = 1F;
            this.txtGroomsState.Name = "txtGroomsState";
            this.txtGroomsState.Text = null;
            this.txtGroomsState.Top = 0.9583333F;
            this.txtGroomsState.Width = 0.8125F;
            // 
            // txtGroomsCounty
            // 
            this.txtGroomsCounty.CanGrow = false;
            this.txtGroomsCounty.Height = 0.2083333F;
            this.txtGroomsCounty.Left = 1.875F;
            this.txtGroomsCounty.Name = "txtGroomsCounty";
            this.txtGroomsCounty.Text = null;
            this.txtGroomsCounty.Top = 0.9583333F;
            this.txtGroomsCounty.Width = 1.375F;
            // 
            // txtGroomsCity
            // 
            this.txtGroomsCity.CanGrow = false;
            this.txtGroomsCity.Height = 0.2083333F;
            this.txtGroomsCity.Left = 3.9375F;
            this.txtGroomsCity.Name = "txtGroomsCity";
            this.txtGroomsCity.Text = null;
            this.txtGroomsCity.Top = 0.9583333F;
            this.txtGroomsCity.Width = 3.125F;
            // 
            // txtGroomsDOB
            // 
            this.txtGroomsDOB.CanGrow = false;
            this.txtGroomsDOB.Height = 0.1666667F;
            this.txtGroomsDOB.Left = 6.125F;
            this.txtGroomsDOB.Name = "txtGroomsDOB";
            this.txtGroomsDOB.Text = null;
            this.txtGroomsDOB.Top = 1.333333F;
            this.txtGroomsDOB.Width = 1.333333F;
            // 
            // txtGroomsFathersName
            // 
            this.txtGroomsFathersName.CanGrow = false;
            this.txtGroomsFathersName.Height = 0.1770833F;
            this.txtGroomsFathersName.Left = 0.0625F;
            this.txtGroomsFathersName.Name = "txtGroomsFathersName";
            this.txtGroomsFathersName.Text = null;
            this.txtGroomsFathersName.Top = 1.666667F;
            this.txtGroomsFathersName.Width = 2.125F;
            // 
            // txtGroomsFathersBirthplace
            // 
            this.txtGroomsFathersBirthplace.CanGrow = false;
            this.txtGroomsFathersBirthplace.Height = 0.1875F;
            this.txtGroomsFathersBirthplace.Left = 2.25F;
            this.txtGroomsFathersBirthplace.Name = "txtGroomsFathersBirthplace";
            this.txtGroomsFathersBirthplace.Text = null;
            this.txtGroomsFathersBirthplace.Top = 1.65625F;
            this.txtGroomsFathersBirthplace.Width = 1.625F;
            // 
            // txtGroomsMothersName
            // 
            this.txtGroomsMothersName.CanGrow = false;
            this.txtGroomsMothersName.Height = 0.1666667F;
            this.txtGroomsMothersName.Left = 3.9375F;
            this.txtGroomsMothersName.Name = "txtGroomsMothersName";
            this.txtGroomsMothersName.Text = null;
            this.txtGroomsMothersName.Top = 1.666667F;
            this.txtGroomsMothersName.Width = 2.03125F;
            // 
            // txtGroomsMothersBirthplace
            // 
            this.txtGroomsMothersBirthplace.CanGrow = false;
            this.txtGroomsMothersBirthplace.Height = 0.1666667F;
            this.txtGroomsMothersBirthplace.Left = 6.0625F;
            this.txtGroomsMothersBirthplace.Name = "txtGroomsMothersBirthplace";
            this.txtGroomsMothersBirthplace.Text = null;
            this.txtGroomsMothersBirthplace.Top = 1.666667F;
            this.txtGroomsMothersBirthplace.Width = 1.375F;
            // 
            // txtBridesFirst
            // 
            this.txtBridesFirst.CanGrow = false;
            this.txtBridesFirst.Height = 0.1666667F;
            this.txtBridesFirst.Left = 0.0625F;
            this.txtBridesFirst.Name = "txtBridesFirst";
            this.txtBridesFirst.Text = null;
            this.txtBridesFirst.Top = 2.25F;
            this.txtBridesFirst.Width = 2.125F;
            // 
            // txtBridesStreet
            // 
            this.txtBridesStreet.CanGrow = false;
            this.txtBridesStreet.Height = 0.2083333F;
            this.txtBridesStreet.Left = 0.0625F;
            this.txtBridesStreet.Name = "txtBridesStreet";
            this.txtBridesStreet.Text = null;
            this.txtBridesStreet.Top = 2.958333F;
            this.txtBridesStreet.Width = 3.125F;
            // 
            // txtBridesAge
            // 
            this.txtBridesAge.CanGrow = false;
            this.txtBridesAge.Height = 0.1666667F;
            this.txtBridesAge.Left = 0.0625F;
            this.txtBridesAge.Name = "txtBridesAge";
            this.txtBridesAge.Text = null;
            this.txtBridesAge.Top = 2.625F;
            this.txtBridesAge.Width = 0.625F;
            // 
            // txtBridesBirthplace
            // 
            this.txtBridesBirthplace.CanGrow = false;
            this.txtBridesBirthplace.Height = 0.2083333F;
            this.txtBridesBirthplace.Left = 3.9375F;
            this.txtBridesBirthplace.Name = "txtBridesBirthplace";
            this.txtBridesBirthplace.Text = null;
            this.txtBridesBirthplace.Top = 2.958333F;
            this.txtBridesBirthplace.Width = 2.0625F;
            // 
            // txtBridesMiddle
            // 
            this.txtBridesMiddle.CanGrow = false;
            this.txtBridesMiddle.Height = 0.1666667F;
            this.txtBridesMiddle.Left = 2.3125F;
            this.txtBridesMiddle.Name = "txtBridesMiddle";
            this.txtBridesMiddle.Text = null;
            this.txtBridesMiddle.Top = 2.25F;
            this.txtBridesMiddle.Width = 1.5625F;
            // 
            // txtBridesSurName
            // 
            this.txtBridesSurName.CanGrow = false;
            this.txtBridesSurName.Height = 0.1666667F;
            this.txtBridesSurName.Left = 4F;
            this.txtBridesSurName.Name = "txtBridesSurName";
            this.txtBridesSurName.Text = null;
            this.txtBridesSurName.Top = 2.25F;
            this.txtBridesSurName.Width = 1.75F;
            // 
            // txtBridesState
            // 
            this.txtBridesState.CanGrow = false;
            this.txtBridesState.Height = 0.1666667F;
            this.txtBridesState.Left = 1F;
            this.txtBridesState.Name = "txtBridesState";
            this.txtBridesState.Text = null;
            this.txtBridesState.Top = 2.625F;
            this.txtBridesState.Width = 0.625F;
            // 
            // txtBridesCounty
            // 
            this.txtBridesCounty.CanGrow = false;
            this.txtBridesCounty.Height = 0.1666667F;
            this.txtBridesCounty.Left = 1.875F;
            this.txtBridesCounty.Name = "txtBridesCounty";
            this.txtBridesCounty.Text = null;
            this.txtBridesCounty.Top = 2.625F;
            this.txtBridesCounty.Width = 1.9375F;
            // 
            // txtBridesCity
            // 
            this.txtBridesCity.CanGrow = false;
            this.txtBridesCity.Height = 0.1666667F;
            this.txtBridesCity.Left = 3.9375F;
            this.txtBridesCity.Name = "txtBridesCity";
            this.txtBridesCity.Text = null;
            this.txtBridesCity.Top = 2.625F;
            this.txtBridesCity.Width = 3.125F;
            // 
            // txtBridesFathersName
            // 
            this.txtBridesFathersName.CanGrow = false;
            this.txtBridesFathersName.Height = 0.1979167F;
            this.txtBridesFathersName.Left = 0.0625F;
            this.txtBridesFathersName.Name = "txtBridesFathersName";
            this.txtBridesFathersName.Text = null;
            this.txtBridesFathersName.Top = 3.333333F;
            this.txtBridesFathersName.Width = 2.125F;
            // 
            // txtBridesFathersBirthplace
            // 
            this.txtBridesFathersBirthplace.CanGrow = false;
            this.txtBridesFathersBirthplace.Height = 0.1875F;
            this.txtBridesFathersBirthplace.Left = 2.25F;
            this.txtBridesFathersBirthplace.Name = "txtBridesFathersBirthplace";
            this.txtBridesFathersBirthplace.Text = null;
            this.txtBridesFathersBirthplace.Top = 3.34375F;
            this.txtBridesFathersBirthplace.Width = 1.625F;
            // 
            // txtBridesMothersName
            // 
            this.txtBridesMothersName.CanGrow = false;
            this.txtBridesMothersName.Height = 0.2083333F;
            this.txtBridesMothersName.Left = 3.9375F;
            this.txtBridesMothersName.Name = "txtBridesMothersName";
            this.txtBridesMothersName.Text = null;
            this.txtBridesMothersName.Top = 3.333333F;
            this.txtBridesMothersName.Width = 2.0625F;
            // 
            // txtBridesDOB
            // 
            this.txtBridesDOB.CanGrow = false;
            this.txtBridesDOB.Height = 0.2083333F;
            this.txtBridesDOB.Left = 6.125F;
            this.txtBridesDOB.Name = "txtBridesDOB";
            this.txtBridesDOB.Text = null;
            this.txtBridesDOB.Top = 2.958333F;
            this.txtBridesDOB.Width = 1.333333F;
            // 
            // txtBridesMothersBirthplace
            // 
            this.txtBridesMothersBirthplace.CanGrow = false;
            this.txtBridesMothersBirthplace.Height = 0.2083333F;
            this.txtBridesMothersBirthplace.Left = 6.0625F;
            this.txtBridesMothersBirthplace.Name = "txtBridesMothersBirthplace";
            this.txtBridesMothersBirthplace.Text = null;
            this.txtBridesMothersBirthplace.Top = 3.333333F;
            this.txtBridesMothersBirthplace.Width = 1.375F;
            // 
            // txtBridesLast
            // 
            this.txtBridesLast.CanGrow = false;
            this.txtBridesLast.Height = 0.1666667F;
            this.txtBridesLast.Left = 5.8125F;
            this.txtBridesLast.Name = "txtBridesLast";
            this.txtBridesLast.Text = null;
            this.txtBridesLast.Top = 2.25F;
            this.txtBridesLast.Width = 1.25F;
            // 
            // txtGroomsMarriageNumber
            // 
            this.txtGroomsMarriageNumber.CanGrow = false;
            this.txtGroomsMarriageNumber.Height = 0.1666667F;
            this.txtGroomsMarriageNumber.Left = 0.0625F;
            this.txtGroomsMarriageNumber.Name = "txtGroomsMarriageNumber";
            this.txtGroomsMarriageNumber.Text = null;
            this.txtGroomsMarriageNumber.Top = 4.5F;
            this.txtGroomsMarriageNumber.Width = 0.625F;
            // 
            // txtGroomsMarriageEnded
            // 
            this.txtGroomsMarriageEnded.CanGrow = false;
            this.txtGroomsMarriageEnded.Height = 0.19F;
            this.txtGroomsMarriageEnded.Left = 1.75F;
            this.txtGroomsMarriageEnded.Name = "txtGroomsMarriageEnded";
            this.txtGroomsMarriageEnded.Text = null;
            this.txtGroomsMarriageEnded.Top = 4.46875F;
            this.txtGroomsMarriageEnded.Width = 1.75F;
            // 
            // txtBridesMarriageNumber
            // 
            this.txtBridesMarriageNumber.CanGrow = false;
            this.txtBridesMarriageNumber.Height = 0.1666667F;
            this.txtBridesMarriageNumber.Left = 3.75F;
            this.txtBridesMarriageNumber.Name = "txtBridesMarriageNumber";
            this.txtBridesMarriageNumber.Text = null;
            this.txtBridesMarriageNumber.Top = 4.5F;
            this.txtBridesMarriageNumber.Width = 0.625F;
            // 
            // txtBridesMarriageEnded
            // 
            this.txtBridesMarriageEnded.CanGrow = false;
            this.txtBridesMarriageEnded.Height = 0.19F;
            this.txtBridesMarriageEnded.Left = 5.3125F;
            this.txtBridesMarriageEnded.Name = "txtBridesMarriageEnded";
            this.txtBridesMarriageEnded.Text = null;
            this.txtBridesMarriageEnded.Top = 4.46875F;
            this.txtBridesMarriageEnded.Width = 1.854167F;
            // 
            // txtCityofIssue
            // 
            this.txtCityofIssue.CanGrow = false;
            this.txtCityofIssue.Height = 0.2083333F;
            this.txtCityofIssue.Left = 4.34375F;
            this.txtCityofIssue.Name = "txtCityofIssue";
            this.txtCityofIssue.Text = null;
            this.txtCityofIssue.Top = 6.083333F;
            this.txtCityofIssue.Width = 2.75F;
            // 
            // txtLicenseValidUntil
            // 
            this.txtLicenseValidUntil.CanGrow = false;
            this.txtLicenseValidUntil.Height = 0.1666667F;
            this.txtLicenseValidUntil.Left = 5.3125F;
            this.txtLicenseValidUntil.Name = "txtLicenseValidUntil";
            this.txtLicenseValidUntil.Text = null;
            this.txtLicenseValidUntil.Top = 5.583333F;
            this.txtLicenseValidUntil.Width = 1.125F;
            // 
            // txtTitleOfPersonPerforming
            // 
            this.txtTitleOfPersonPerforming.CanGrow = false;
            this.txtTitleOfPersonPerforming.Height = 0.19F;
            this.txtTitleOfPersonPerforming.Left = 6.0625F;
            this.txtTitleOfPersonPerforming.Name = "txtTitleOfPersonPerforming";
            this.txtTitleOfPersonPerforming.Text = null;
            this.txtTitleOfPersonPerforming.Top = 7.375F;
            this.txtTitleOfPersonPerforming.Visible = false;
            this.txtTitleOfPersonPerforming.Width = 1.1875F;
            // 
            // txtDateOfCommision
            // 
            this.txtDateOfCommision.CanGrow = false;
            this.txtDateOfCommision.Height = 0.1666667F;
            this.txtDateOfCommision.Left = 5.875F;
            this.txtDateOfCommision.Name = "txtDateOfCommision";
            this.txtDateOfCommision.Text = null;
            this.txtDateOfCommision.Top = 7.75F;
            this.txtDateOfCommision.Visible = false;
            this.txtDateOfCommision.Width = 1.1875F;
            // 
            // txtResidenceOfPersonPerforming
            // 
            this.txtResidenceOfPersonPerforming.CanGrow = false;
            this.txtResidenceOfPersonPerforming.Height = 0.1666667F;
            this.txtResidenceOfPersonPerforming.Left = 1F;
            this.txtResidenceOfPersonPerforming.Name = "txtResidenceOfPersonPerforming";
            this.txtResidenceOfPersonPerforming.Text = null;
            this.txtResidenceOfPersonPerforming.Top = 7.75F;
            this.txtResidenceOfPersonPerforming.Visible = false;
            this.txtResidenceOfPersonPerforming.Width = 2.625F;
            // 
            // txtAddressOfPersonPerforming
            // 
            this.txtAddressOfPersonPerforming.CanGrow = false;
            this.txtAddressOfPersonPerforming.Height = 0.19F;
            this.txtAddressOfPersonPerforming.Left = 1F;
            this.txtAddressOfPersonPerforming.Name = "txtAddressOfPersonPerforming";
            this.txtAddressOfPersonPerforming.Text = null;
            this.txtAddressOfPersonPerforming.Top = 8.125F;
            this.txtAddressOfPersonPerforming.Visible = false;
            this.txtAddressOfPersonPerforming.Width = 5.625F;
            // 
            // txtGroomEndedWhy
            // 
            this.txtGroomEndedWhy.CanGrow = false;
            this.txtGroomEndedWhy.Height = 0.2083333F;
            this.txtGroomEndedWhy.Left = 0.8125F;
            this.txtGroomEndedWhy.Name = "txtGroomEndedWhy";
            this.txtGroomEndedWhy.Text = null;
            this.txtGroomEndedWhy.Top = 4.25F;
            this.txtGroomEndedWhy.Width = 2.9375F;
            // 
            // txtBrideEndedWhy
            // 
            this.txtBrideEndedWhy.CanGrow = false;
            this.txtBrideEndedWhy.Height = 0.2083333F;
            this.txtBrideEndedWhy.Left = 4.5F;
            this.txtBrideEndedWhy.Name = "txtBrideEndedWhy";
            this.txtBrideEndedWhy.Text = null;
            this.txtBrideEndedWhy.Top = 4.25F;
            this.txtBrideEndedWhy.Width = 2.625F;
            // 
            // txtWitnessOne
            // 
            this.txtWitnessOne.CanGrow = false;
            this.txtWitnessOne.Height = 0.19F;
            this.txtWitnessOne.Left = 3.625F;
            this.txtWitnessOne.Name = "txtWitnessOne";
            this.txtWitnessOne.Text = null;
            this.txtWitnessOne.Top = 8.5F;
            this.txtWitnessOne.Visible = false;
            this.txtWitnessOne.Width = 3.4375F;
            // 
            // txtWitnessTwo
            // 
            this.txtWitnessTwo.CanGrow = false;
            this.txtWitnessTwo.Height = 0.19F;
            this.txtWitnessTwo.Left = 3.625F;
            this.txtWitnessTwo.Name = "txtWitnessTwo";
            this.txtWitnessTwo.Text = null;
            this.txtWitnessTwo.Top = 8.875F;
            this.txtWitnessTwo.Visible = false;
            this.txtWitnessTwo.Width = 3.4375F;
            // 
            // txtDateFiled
            // 
            this.txtDateFiled.CanGrow = false;
            this.txtDateFiled.Height = 0.2083333F;
            this.txtDateFiled.Left = 4.625F;
            this.txtDateFiled.Name = "txtDateFiled";
            this.txtDateFiled.Text = null;
            this.txtDateFiled.Top = 9.458333F;
            this.txtDateFiled.Visible = false;
            this.txtDateFiled.Width = 2.46875F;
            // 
            // txtSignitureWit2
            // 
            this.txtSignitureWit2.CanGrow = false;
            this.txtSignitureWit2.Height = 0.1666667F;
            this.txtSignitureWit2.Left = 0.125F;
            this.txtSignitureWit2.Name = "txtSignitureWit2";
            this.txtSignitureWit2.Text = null;
            this.txtSignitureWit2.Top = 8.875F;
            this.txtSignitureWit2.Visible = false;
            this.txtSignitureWit2.Width = 3.4375F;
            // 
            // txtSignitureWit1
            // 
            this.txtSignitureWit1.CanGrow = false;
            this.txtSignitureWit1.Height = 0.1666667F;
            this.txtSignitureWit1.Left = 0.15625F;
            this.txtSignitureWit1.Name = "txtSignitureWit1";
            this.txtSignitureWit1.Text = null;
            this.txtSignitureWit1.Top = 8.5F;
            this.txtSignitureWit1.Visible = false;
            this.txtSignitureWit1.Width = 3.4375F;
            // 
            // txtSigniturePerson
            // 
            this.txtSigniturePerson.CanGrow = false;
            this.txtSigniturePerson.Height = 0.1666667F;
            this.txtSigniturePerson.Left = 0.09375F;
            this.txtSigniturePerson.Name = "txtSigniturePerson";
            this.txtSigniturePerson.Text = null;
            this.txtSigniturePerson.Top = 7.375F;
            this.txtSigniturePerson.Visible = false;
            this.txtSigniturePerson.Width = 3.15625F;
            // 
            // fldGroomFormerSpouse
            // 
            this.fldGroomFormerSpouse.CanGrow = false;
            this.fldGroomFormerSpouse.Height = 0.1875F;
            this.fldGroomFormerSpouse.Left = 1.75F;
            this.fldGroomFormerSpouse.Name = "fldGroomFormerSpouse";
            this.fldGroomFormerSpouse.Text = null;
            this.fldGroomFormerSpouse.Top = 4.75F;
            this.fldGroomFormerSpouse.Width = 1.875F;
            // 
            // fldGroomCourtName
            // 
            this.fldGroomCourtName.CanGrow = false;
            this.fldGroomCourtName.Height = 0.1875F;
            this.fldGroomCourtName.Left = 0F;
            this.fldGroomCourtName.Name = "fldGroomCourtName";
            this.fldGroomCourtName.Text = null;
            this.fldGroomCourtName.Top = 5.03125F;
            this.fldGroomCourtName.Width = 2.65625F;
            // 
            // fldBrideFormerSpouse
            // 
            this.fldBrideFormerSpouse.CanGrow = false;
            this.fldBrideFormerSpouse.Height = 0.1875F;
            this.fldBrideFormerSpouse.Left = 5.3125F;
            this.fldBrideFormerSpouse.Name = "fldBrideFormerSpouse";
            this.fldBrideFormerSpouse.Text = null;
            this.fldBrideFormerSpouse.Top = 4.75F;
            this.fldBrideFormerSpouse.Width = 1.875F;
            // 
            // fldBrideCourtName
            // 
            this.fldBrideCourtName.CanGrow = false;
            this.fldBrideCourtName.Height = 0.1875F;
            this.fldBrideCourtName.Left = 3.75F;
            this.fldBrideCourtName.Name = "fldBrideCourtName";
            this.fldBrideCourtName.Text = null;
            this.fldBrideCourtName.Top = 5.03125F;
            this.fldBrideCourtName.Width = 2.65625F;
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtstate,
            this.txtIssuance,
            this.txtMarriage});
            this.PageHeader.Height = 0.75F;
            this.PageHeader.Name = "PageHeader";
            // 
            // txtstate
            // 
            this.txtstate.Height = 0.2083333F;
            this.txtstate.Left = 0.25F;
            this.txtstate.Name = "txtstate";
            this.txtstate.Text = "X";
            this.txtstate.Top = 0.2916667F;
            this.txtstate.Visible = false;
            this.txtstate.Width = 0.1875F;
            // 
            // txtIssuance
            // 
            this.txtIssuance.Height = 0.2083333F;
            this.txtIssuance.Left = 0.9375F;
            this.txtIssuance.Name = "txtIssuance";
            this.txtIssuance.Text = "X";
            this.txtIssuance.Top = 0.4166667F;
            this.txtIssuance.Visible = false;
            this.txtIssuance.Width = 0.1875F;
            // 
            // txtMarriage
            // 
            this.txtMarriage.Height = 0.2083333F;
            this.txtMarriage.Left = 1.0625F;
            this.txtMarriage.Name = "txtMarriage";
            this.txtMarriage.Text = "X";
            this.txtMarriage.Top = 0.5416667F;
            this.txtMarriage.Visible = false;
            this.txtMarriage.Width = 0.1875F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptLicense
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.2083333F;
            this.PageSettings.Margins.Left = 0.625F;
            this.PageSettings.Margins.Right = 0.375F;
            this.PageSettings.Margins.Top = 0.2083333F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.499306F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_Terminate);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
            this.PrintProgress += new System.EventHandler(this.ActiveReport_PrintProgress);
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateIntenstionsFiled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCeremonyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountyMarried)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonPerformingCeremony)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityMarried)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateLicenseIssued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDesig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesSurName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEnded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEnded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityofIssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenseValidUntil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleOfPersonPerforming)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateOfCommision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResidenceOfPersonPerforming)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddressOfPersonPerforming)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedWhy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedWhy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitnessOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitnessTwo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignitureWit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignitureWit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSigniturePerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomFormerSpouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomCourtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideFormerSpouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideCourtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssuance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarriage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateIntenstionsFiled;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCeremonyDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountyMarried;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPersonPerformingCeremony;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityMarried;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateLicenseIssued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsDesig;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsFathersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMothersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMothersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesSurName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFathersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMothersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMothersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityofIssue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseValidUntil;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitleOfPersonPerforming;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateOfCommision;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddressOfPersonPerforming;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomEndedWhy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideEndedWhy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWitnessOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWitnessTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateFiled;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSignitureWit2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSignitureWit1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSigniturePerson;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomFormerSpouse;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomCourtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideFormerSpouse;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideCourtName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssuance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarriage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtResidenceOfPersonPerforming;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtstate;
    }
}
