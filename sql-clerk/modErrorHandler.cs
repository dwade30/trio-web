﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;

namespace TWCK0000
{
	public class modErrorHandler
	{
		//=========================================================
		public class StaticVariables
		{
			// ********************************************************
			// Last Updated   :               11/17/2002              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// Date           :                                       *
			// WRITTEN BY     :               Matthew Larrabee        *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			public string gstrStack = "";
			public string gstrCurrentRoutine = "";
			public string gstrFormName = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}

		public static void SetErrorHandler(Exception ex)
		{
			clsErrorHandlers clsErrorHandler;
			clsErrorHandler = new clsErrorHandlers();
			clsErrorHandler.SetErrorHandler(ex);
			clsErrorHandler = null;
		}
	}
}
