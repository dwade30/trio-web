﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for srptDogSummary.
	/// </summary>
	partial class srptDogSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptDogSummary));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtMF1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMF2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNeuter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNeuter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKennelTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblStickerYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblStickerYear2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtReplacementTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNewTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTagOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTagTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMonth = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtKennel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKennel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtReplacement1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtReplacement2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSandR1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSandR2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHearingGuide1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHearingGuide2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTransfers1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTransfers2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDogKennel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDogKennel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDogTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDogTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDangerous2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDangerous1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNuisance2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNuisance1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennelTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStickerYear1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStickerYear2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacementTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacement1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacement2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSandR1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSandR2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransfers1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransfers2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogKennel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogKennel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogTotal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogTotal2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerous2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerous1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisance2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisance1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMF1,
            this.txtMF2,
            this.txtNeuter1,
            this.txtNeuter2,
            this.txtKennelTotal,
            this.lblStickerYear1,
            this.lblStickerYear2,
            this.Label21,
            this.Label22,
            this.Label23,
            this.txtReplacementTag,
            this.txtNewTag,
            this.txtTagOther,
            this.txtTagTotal,
            this.Label53,
            this.Label54,
            this.Label55,
            this.Label56,
            this.Label57,
            this.Label58,
            this.lblMonth,
            this.txtKennel1,
            this.txtKennel2,
            this.Label20,
            this.txtReplacement1,
            this.txtReplacement2,
            this.txtSandR1,
            this.txtSandR2,
            this.txtHearingGuide1,
            this.txtHearingGuide2,
            this.txtTransfers1,
            this.txtTransfers2,
            this.txtDogKennel1,
            this.txtDogKennel2,
            this.txtDogTotal1,
            this.txtDogTotal2,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.txtDangerous2,
            this.txtDangerous1,
            this.label2,
            this.txtNuisance2,
            this.txtNuisance1,
            this.label3});
            this.Detail.Height = 2.827F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // txtMF1
            // 
            this.txtMF1.Height = 0.18F;
            this.txtMF1.Left = 1.969F;
            this.txtMF1.Name = "txtMF1";
            this.txtMF1.Style = "text-align: right";
            this.txtMF1.Text = null;
            this.txtMF1.Top = 0.7220001F;
            this.txtMF1.Width = 0.438F;
            // 
            // txtMF2
            // 
            this.txtMF2.Height = 0.18F;
            this.txtMF2.Left = 2.469F;
            this.txtMF2.Name = "txtMF2";
            this.txtMF2.Style = "text-align: right";
            this.txtMF2.Tag = "YEAR2";
            this.txtMF2.Text = null;
            this.txtMF2.Top = 0.7220001F;
            this.txtMF2.Width = 0.438F;
            // 
            // txtNeuter1
            // 
            this.txtNeuter1.Height = 0.18F;
            this.txtNeuter1.Left = 1.969F;
            this.txtNeuter1.Name = "txtNeuter1";
            this.txtNeuter1.Style = "text-align: right";
            this.txtNeuter1.Text = null;
            this.txtNeuter1.Top = 0.9020001F;
            this.txtNeuter1.Width = 0.438F;
            // 
            // txtNeuter2
            // 
            this.txtNeuter2.Height = 0.18F;
            this.txtNeuter2.Left = 2.469F;
            this.txtNeuter2.Name = "txtNeuter2";
            this.txtNeuter2.Style = "text-align: right";
            this.txtNeuter2.Tag = "YEAR2";
            this.txtNeuter2.Text = null;
            this.txtNeuter2.Top = 0.9020001F;
            this.txtNeuter2.Width = 0.438F;
            // 
            // txtKennelTotal
            // 
            this.txtKennelTotal.Height = 0.18F;
            this.txtKennelTotal.Left = 3.781F;
            this.txtKennelTotal.Name = "txtKennelTotal";
            this.txtKennelTotal.Text = null;
            this.txtKennelTotal.Top = 1.082F;
            this.txtKennelTotal.Width = 0.375F;
            // 
            // lblStickerYear1
            // 
            this.lblStickerYear1.Height = 0.18F;
            this.lblStickerYear1.HyperLink = null;
            this.lblStickerYear1.Left = 1.96875F;
            this.lblStickerYear1.Name = "lblStickerYear1";
            this.lblStickerYear1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblStickerYear1.Text = null;
            this.lblStickerYear1.Top = 0.5418333F;
            this.lblStickerYear1.Width = 0.438F;
            // 
            // lblStickerYear2
            // 
            this.lblStickerYear2.Height = 0.18F;
            this.lblStickerYear2.HyperLink = null;
            this.lblStickerYear2.Left = 2.46875F;
            this.lblStickerYear2.Name = "lblStickerYear2";
            this.lblStickerYear2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblStickerYear2.Tag = "YEAR2";
            this.lblStickerYear2.Text = null;
            this.lblStickerYear2.Top = 0.5418333F;
            this.lblStickerYear2.Width = 0.438F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.18F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 0.09400022F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label21.Text = "Neuter/Spay";
            this.Label21.Top = 0.9020001F;
            this.Label21.Width = 1.75F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.18F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 0.09400022F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label22.Text = "Male/Female";
            this.Label22.Top = 0.7220001F;
            this.Label22.Width = 1.75F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.18F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 2.969F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.Label23.Text = "Licenses";
            this.Label23.Top = 1.082F;
            this.Label23.Width = 0.813F;
            // 
            // txtReplacementTag
            // 
            this.txtReplacementTag.Height = 0.18F;
            this.txtReplacementTag.Left = 5.844F;
            this.txtReplacementTag.Name = "txtReplacementTag";
            this.txtReplacementTag.Style = "font-family: \'Arial\'; font-size: 10pt; text-align: right";
            this.txtReplacementTag.Tag = "TAGS";
            this.txtReplacementTag.Text = null;
            this.txtReplacementTag.Top = 0.7220001F;
            this.txtReplacementTag.Width = 0.313F;
            // 
            // txtNewTag
            // 
            this.txtNewTag.Height = 0.18F;
            this.txtNewTag.Left = 5.84375F;
            this.txtNewTag.Name = "txtNewTag";
            this.txtNewTag.Style = "font-family: \'Arial\'; font-size: 10pt; text-align: right";
            this.txtNewTag.Tag = "TAGS";
            this.txtNewTag.Text = null;
            this.txtNewTag.Top = 0.5418333F;
            this.txtNewTag.Width = 0.313F;
            // 
            // txtTagOther
            // 
            this.txtTagOther.Height = 0.18F;
            this.txtTagOther.Left = 5.844F;
            this.txtTagOther.Name = "txtTagOther";
            this.txtTagOther.Style = "font-family: \'Arial\'; font-size: 10pt; text-align: right";
            this.txtTagOther.Tag = "TAGS";
            this.txtTagOther.Text = null;
            this.txtTagOther.Top = 0.9020001F;
            this.txtTagOther.Width = 0.313F;
            // 
            // txtTagTotal
            // 
            this.txtTagTotal.Height = 0.18F;
            this.txtTagTotal.Left = 5.844F;
            this.txtTagTotal.Name = "txtTagTotal";
            this.txtTagTotal.Style = "font-family: \'Arial\'; font-size: 10pt; text-align: right";
            this.txtTagTotal.Tag = "TAGS";
            this.txtTagTotal.Text = null;
            this.txtTagTotal.Top = 1.082F;
            this.txtTagTotal.Width = 0.313F;
            // 
            // Label53
            // 
            this.Label53.Height = 0.18F;
            this.Label53.HyperLink = null;
            this.Label53.Left = 4.65625F;
            this.Label53.Name = "Label53";
            this.Label53.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label53.Tag = "TAGS";
            this.Label53.Text = "New";
            this.Label53.Top = 0.5418333F;
            this.Label53.Width = 0.5F;
            // 
            // Label54
            // 
            this.Label54.Height = 0.18F;
            this.Label54.HyperLink = null;
            this.Label54.Left = 4.6565F;
            this.Label54.Name = "Label54";
            this.Label54.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label54.Tag = "TAGS";
            this.Label54.Text = "Replacement";
            this.Label54.Top = 0.7220001F;
            this.Label54.Width = 1.094F;
            // 
            // Label55
            // 
            this.Label55.Height = 0.18F;
            this.Label55.HyperLink = null;
            this.Label55.Left = 4.6565F;
            this.Label55.Name = "Label55";
            this.Label55.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label55.Tag = "TAGS";
            this.Label55.Text = "Other";
            this.Label55.Top = 0.9020001F;
            this.Label55.Width = 0.5F;
            // 
            // Label56
            // 
            this.Label56.Height = 0.18F;
            this.Label56.HyperLink = null;
            this.Label56.Left = 4.6565F;
            this.Label56.Name = "Label56";
            this.Label56.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label56.Tag = "TAGS";
            this.Label56.Text = "Total";
            this.Label56.Top = 1.082F;
            this.Label56.Width = 0.5F;
            // 
            // Label57
            // 
            this.Label57.Height = 0.2291667F;
            this.Label57.HyperLink = null;
            this.Label57.Left = 5.71875F;
            this.Label57.Name = "Label57";
            this.Label57.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.Label57.Tag = "TAGS";
            this.Label57.Text = "Tags";
            this.Label57.Top = 0.3125F;
            this.Label57.Width = 0.4375F;
            // 
            // Label58
            // 
            this.Label58.Height = 0.18F;
            this.Label58.HyperLink = null;
            this.Label58.Left = 2F;
            this.Label58.Name = "Label58";
            this.Label58.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
            this.Label58.Text = "Stickers";
            this.Label58.Top = 0.3125F;
            this.Label58.Width = 0.875F;
            // 
            // lblMonth
            // 
            this.lblMonth.Height = 0.1979167F;
            this.lblMonth.HyperLink = null;
            this.lblMonth.Left = 3.53125F;
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Style = "";
            this.lblMonth.Text = null;
            this.lblMonth.Top = 0.05208333F;
            this.lblMonth.Width = 1.041667F;
            // 
            // txtKennel1
            // 
            this.txtKennel1.Height = 0.18F;
            this.txtKennel1.Left = 1.969F;
            this.txtKennel1.Name = "txtKennel1";
            this.txtKennel1.Style = "text-align: right";
            this.txtKennel1.Text = null;
            this.txtKennel1.Top = 1.442F;
            this.txtKennel1.Width = 0.438F;
            // 
            // txtKennel2
            // 
            this.txtKennel2.Height = 0.18F;
            this.txtKennel2.Left = 2.469F;
            this.txtKennel2.Name = "txtKennel2";
            this.txtKennel2.Style = "text-align: right";
            this.txtKennel2.Tag = "YEAR2";
            this.txtKennel2.Text = null;
            this.txtKennel2.Top = 1.442F;
            this.txtKennel2.Width = 0.438F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.18F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 0.1040001F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label20.Text = "Kennel";
            this.Label20.Top = 1.442F;
            this.Label20.Width = 1.812F;
            // 
            // txtReplacement1
            // 
            this.txtReplacement1.Height = 0.18F;
            this.txtReplacement1.Left = 1.969F;
            this.txtReplacement1.Name = "txtReplacement1";
            this.txtReplacement1.Style = "text-align: right";
            this.txtReplacement1.Text = null;
            this.txtReplacement1.Top = 1.622F;
            this.txtReplacement1.Width = 0.438F;
            // 
            // txtReplacement2
            // 
            this.txtReplacement2.Height = 0.18F;
            this.txtReplacement2.Left = 2.469F;
            this.txtReplacement2.Name = "txtReplacement2";
            this.txtReplacement2.Style = "text-align: right";
            this.txtReplacement2.Tag = "YEAR2";
            this.txtReplacement2.Text = null;
            this.txtReplacement2.Top = 1.622F;
            this.txtReplacement2.Width = 0.438F;
            // 
            // txtSandR1
            // 
            this.txtSandR1.Height = 0.18F;
            this.txtSandR1.Left = 1.969F;
            this.txtSandR1.Name = "txtSandR1";
            this.txtSandR1.Style = "text-align: right";
            this.txtSandR1.Text = null;
            this.txtSandR1.Top = 1.802F;
            this.txtSandR1.Width = 0.438F;
            // 
            // txtSandR2
            // 
            this.txtSandR2.Height = 0.18F;
            this.txtSandR2.Left = 2.469F;
            this.txtSandR2.Name = "txtSandR2";
            this.txtSandR2.Style = "text-align: right";
            this.txtSandR2.Tag = "YEAR2";
            this.txtSandR2.Text = null;
            this.txtSandR2.Top = 1.802F;
            this.txtSandR2.Width = 0.438F;
            // 
            // txtHearingGuide1
            // 
            this.txtHearingGuide1.Height = 0.18F;
            this.txtHearingGuide1.Left = 1.969F;
            this.txtHearingGuide1.Name = "txtHearingGuide1";
            this.txtHearingGuide1.Style = "text-align: right";
            this.txtHearingGuide1.Text = null;
            this.txtHearingGuide1.Top = 1.982F;
            this.txtHearingGuide1.Width = 0.438F;
            // 
            // txtHearingGuide2
            // 
            this.txtHearingGuide2.Height = 0.18F;
            this.txtHearingGuide2.Left = 2.469F;
            this.txtHearingGuide2.Name = "txtHearingGuide2";
            this.txtHearingGuide2.Style = "text-align: right";
            this.txtHearingGuide2.Tag = "YEAR2";
            this.txtHearingGuide2.Text = null;
            this.txtHearingGuide2.Top = 1.982F;
            this.txtHearingGuide2.Width = 0.438F;
            // 
            // txtTransfers1
            // 
            this.txtTransfers1.Height = 0.18F;
            this.txtTransfers1.Left = 1.969F;
            this.txtTransfers1.Name = "txtTransfers1";
            this.txtTransfers1.Style = "text-align: right";
            this.txtTransfers1.Text = null;
            this.txtTransfers1.Top = 2.162F;
            this.txtTransfers1.Width = 0.438F;
            // 
            // txtTransfers2
            // 
            this.txtTransfers2.Height = 0.18F;
            this.txtTransfers2.Left = 2.469F;
            this.txtTransfers2.Name = "txtTransfers2";
            this.txtTransfers2.Style = "text-align: right";
            this.txtTransfers2.Tag = "YEAR2";
            this.txtTransfers2.Text = null;
            this.txtTransfers2.Top = 2.162F;
            this.txtTransfers2.Width = 0.438F;
            // 
            // txtDogKennel1
            // 
            this.txtDogKennel1.Height = 0.18F;
            this.txtDogKennel1.Left = 1.969F;
            this.txtDogKennel1.Name = "txtDogKennel1";
            this.txtDogKennel1.Style = "text-align: right";
            this.txtDogKennel1.Text = null;
            this.txtDogKennel1.Top = 2.342F;
            this.txtDogKennel1.Width = 0.438F;
            // 
            // txtDogKennel2
            // 
            this.txtDogKennel2.Height = 0.18F;
            this.txtDogKennel2.Left = 2.469F;
            this.txtDogKennel2.Name = "txtDogKennel2";
            this.txtDogKennel2.Style = "text-align: right";
            this.txtDogKennel2.Tag = "YEAR2";
            this.txtDogKennel2.Text = null;
            this.txtDogKennel2.Top = 2.342F;
            this.txtDogKennel2.Width = 0.438F;
            // 
            // txtDogTotal1
            // 
            this.txtDogTotal1.Height = 0.18F;
            this.txtDogTotal1.Left = 1.969F;
            this.txtDogTotal1.Name = "txtDogTotal1";
            this.txtDogTotal1.Style = "text-align: right";
            this.txtDogTotal1.Text = null;
            this.txtDogTotal1.Top = 2.522F;
            this.txtDogTotal1.Width = 0.438F;
            // 
            // txtDogTotal2
            // 
            this.txtDogTotal2.Height = 0.18F;
            this.txtDogTotal2.Left = 2.469F;
            this.txtDogTotal2.Name = "txtDogTotal2";
            this.txtDogTotal2.Style = "text-align: right";
            this.txtDogTotal2.Tag = "YEAR2";
            this.txtDogTotal2.Text = null;
            this.txtDogTotal2.Top = 2.522F;
            this.txtDogTotal2.Width = 0.438F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.18F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0.1040001F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label12.Text = "Total";
            this.Label12.Top = 2.522F;
            this.Label12.Width = 1.812F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.18F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 0.1040001F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label13.Text = "*Dog(s) added to Kennel";
            this.Label13.Top = 2.342F;
            this.Label13.Width = 1.812F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.18F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.1040001F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label14.Text = "Transfers";
            this.Label14.Top = 2.162F;
            this.Label14.Width = 1.812F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.18F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 0.1040001F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label15.Text = "Replacement";
            this.Label15.Top = 1.622F;
            this.Label15.Width = 1.812F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.18F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 0.1040001F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label16.Text = "Service/Search/Rescue";
            this.Label16.Top = 1.802F;
            this.Label16.Width = 1.812F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.18F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 0.1040001F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.Label17.Text = "Hearing/Guide";
            this.Label17.Top = 1.982F;
            this.Label17.Width = 1.812F;
            // 
            // txtDangerous2
            // 
            this.txtDangerous2.Height = 0.18F;
            this.txtDangerous2.Left = 2.472F;
            this.txtDangerous2.Name = "txtDangerous2";
            this.txtDangerous2.Style = "text-align: right";
            this.txtDangerous2.Text = null;
            this.txtDangerous2.Top = 1.082F;
            this.txtDangerous2.Width = 0.438F;
            // 
            // txtDangerous1
            // 
            this.txtDangerous1.Height = 0.18F;
            this.txtDangerous1.Left = 1.972F;
            this.txtDangerous1.Name = "txtDangerous1";
            this.txtDangerous1.Style = "text-align: right";
            this.txtDangerous1.Tag = "YEAR2";
            this.txtDangerous1.Text = null;
            this.txtDangerous1.Top = 1.082F;
            this.txtDangerous1.Width = 0.438F;
            // 
            // label2
            // 
            this.label2.Height = 0.18F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.09699988F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.label2.Text = "Dangerous Dog";
            this.label2.Top = 1.082F;
            this.label2.Width = 1.812F;
            // 
            // txtNuisance2
            // 
            this.txtNuisance2.Height = 0.18F;
            this.txtNuisance2.Left = 2.472F;
            this.txtNuisance2.Name = "txtNuisance2";
            this.txtNuisance2.Style = "text-align: right";
            this.txtNuisance2.Text = null;
            this.txtNuisance2.Top = 1.262F;
            this.txtNuisance2.Width = 0.438F;
            // 
            // txtNuisance1
            // 
            this.txtNuisance1.Height = 0.18F;
            this.txtNuisance1.Left = 1.972F;
            this.txtNuisance1.Name = "txtNuisance1";
            this.txtNuisance1.Style = "text-align: right";
            this.txtNuisance1.Tag = "YEAR2";
            this.txtNuisance1.Text = null;
            this.txtNuisance1.Top = 1.262F;
            this.txtNuisance1.Width = 0.438F;
            // 
            // label3
            // 
            this.label3.Height = 0.18F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.09699988F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.label3.Text = "Nuisance Dog";
            this.label3.Top = 1.262F;
            this.label3.Width = 1.812F;
            // 
            // srptDogSummary
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.447917F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtMF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMF2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNeuter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennelTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStickerYear1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStickerYear2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacementTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTagTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKennel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacement1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacement2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSandR1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSandR2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransfers1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransfers2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogKennel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogKennel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogTotal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDogTotal2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerous2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDangerous1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisance2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuisance1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMF1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMF2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennelTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStickerYear1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStickerYear2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacementTag;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewTag;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMonth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennel1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennel2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacement1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacement2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSandR1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSandR2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearingGuide1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearingGuide2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransfers1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransfers2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogKennel1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogKennel2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogTotal1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogTotal2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDangerous2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDangerous1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNuisance2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNuisance1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
    }
}
