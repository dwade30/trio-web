﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMarriageConsentWordDoc.
	/// </summary>
	public partial class rptMarriageConsentWordDoc : BaseSectionReport
	{
		public rptMarriageConsentWordDoc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Marriage Consent";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMarriageConsentWordDoc InstancePtr
		{
			get
			{
				return (rptMarriageConsentWordDoc)Sys.GetInstance(typeof(rptMarriageConsentWordDoc));
			}
		}

		protected rptMarriageConsentWordDoc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMarriageConsentWordDoc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			double dblLaserLineAdjustment7;
			// Dim rsData As New clsDRWrapper
			//Application.DoEvents();
			// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
			// If rsData.EndOfFile Then
			// dblLaserLineAdjustment7 = 0
			// Else
			// dblLaserLineAdjustment7 = CDbl(Val(rsData.Fields("MarriageAdjustment")))
			// End If
			dblLaserLineAdjustment7 = Conversion.Val(modRegistry.GetRegistryKey("MarriageAdjustment", "CK"));
			if (Conversion.Val(dblLaserLineAdjustment7) != 0)
			{
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					ControlName.Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment7) / 1440F;
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			if (modGNBas.Statics.typMarriageConsent.IncarceratedBride || modGNBas.Statics.typMarriageConsent.IncarceratedGroom)
			{
				txtIncarcerated.Visible = true;
				if (modGNBas.Statics.typMarriageConsent.IncarceratedBride)
				{
					shpBrideI.Visible = true;
				}
				if (modGNBas.Statics.typMarriageConsent.IncarceratedGroom)
				{
					shpGroomI.Visible = true;
				}
			}
			if (modGNBas.Statics.typMarriageConsent.WaiverBride || modGNBas.Statics.typMarriageConsent.WaiverGroom)
			{
				txtWaiver.Visible = true;
				if (modGNBas.Statics.typMarriageConsent.WaiverBride)
				{
					shpBrideW.Visible = true;
				}
				if (modGNBas.Statics.typMarriageConsent.WaiverGroom)
				{
					shpGroomW.Visible = true;
				}
			}
			if (modGNBas.Statics.typMarriageConsent.BrideHospitalized || modGNBas.Statics.typMarriageConsent.GroomHospitalized)
			{
				txtHospitalized.Visible = true;
				if (modGNBas.Statics.typMarriageConsent.BrideHospitalized)
				{
					shpBrideHospital.Visible = true;
				}
				if (modGNBas.Statics.typMarriageConsent.GroomHospitalized)
				{
					shpGroomHospital.Visible = true;
				}
			}
			if (modGNBas.Statics.typMarriageConsent.Groom)
			{
				txtGroom1.Text = modGNBas.Statics.typMarriageConsent.GroomName;
				txtBride1.Text = modGNBas.Statics.typMarriageConsent.BrideName;
				txtGroomDate.Text = modGNBas.Statics.typMarriageConsent.DateCommissionExpires;
				txtGroomClerk.Text = modGNBas.Statics.typMarriageConsent.ClerkName;
				txtGroomTown.Text = modGNBas.Statics.typMarriageConsent.CountyOfCommission;
			}
			else
			{
				txtGroom1.Text = string.Empty;
				txtBride1.Text = string.Empty;
				txtGroomDate.Text = string.Empty;
				txtGroomClerk.Text = string.Empty;
				txtGroomTown.Text = string.Empty;
			}
			if (modGNBas.Statics.typMarriageConsent.Bride)
			{
				txtGroom2.Text = modGNBas.Statics.typMarriageConsent.GroomName;
				txtBride2.Text = modGNBas.Statics.typMarriageConsent.BrideName;
				txtBrideDate.Text = modGNBas.Statics.typMarriageConsent.DateCommissionExpires;
				txtBrideClerk.Text = modGNBas.Statics.typMarriageConsent.ClerkName;
				txtBrideTown.Text = modGNBas.Statics.typMarriageConsent.CountyOfCommission;
			}
			else
			{
				txtGroom2.Text = string.Empty;
				txtBride2.Text = string.Empty;
				txtBrideDate.Text = string.Empty;
				txtBrideClerk.Text = string.Empty;
				txtBrideTown.Text = string.Empty;
			}
		}

		
	}
}
