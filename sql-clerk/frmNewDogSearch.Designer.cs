//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmNewDogSearch.
	/// </summary>
	partial class frmNewDogSearch
	{
		public fecherFoundation.FCComboBox cmbSearchBy;
		public fecherFoundation.FCLabel lblSearchBy;
		public fecherFoundation.FCComboBox cmbCriteria;
		public fecherFoundation.FCLabel lblCriteria;
		public fecherFoundation.FCComboBox cmbTypes;
		public fecherFoundation.FCLabel lblTypes;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCButton cmdClearSearch;
		public FCGrid GridCriteria;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCCheckBox chkIncludeDeleted;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNewOwner;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbSearchBy = new fecherFoundation.FCComboBox();
            this.lblSearchBy = new fecherFoundation.FCLabel();
            this.cmbCriteria = new fecherFoundation.FCComboBox();
            this.lblCriteria = new fecherFoundation.FCLabel();
            this.cmbTypes = new fecherFoundation.FCComboBox();
            this.lblTypes = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.cmdClearSearch = new fecherFoundation.FCButton();
            this.GridCriteria = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.chkIncludeDeleted = new fecherFoundation.FCCheckBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNewOwner = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdNewOwner = new fecherFoundation.FCButton();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCriteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 513);
            this.BottomPanel.Size = new System.Drawing.Size(560, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.cmbTypes);
            this.ClientArea.Controls.Add(this.lblTypes);
            this.ClientArea.Size = new System.Drawing.Size(560, 453);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNewOwner);
            this.TopPanel.Size = new System.Drawing.Size(560, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewOwner, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(155, 30);
            this.HeaderText.Text = "Dog Records";
            // 
            // cmbSearchBy
            // 
            this.cmbSearchBy.Items.AddRange(new object[] {
            "Owner Name",
            "City or Town",
            "Location",
            "Kennel License Number *",
            "Dog Name",
            "Veterinarian",
            "Rabies Tag Number *",
            "License Tag Number *",
            "License Sticker Number *",
            "Breed"});
            this.cmbSearchBy.Location = new System.Drawing.Point(192, 30);
            this.cmbSearchBy.Name = "cmbSearchBy";
            this.cmbSearchBy.Size = new System.Drawing.Size(279, 40);
            this.cmbSearchBy.TabIndex = 29;
            this.cmbSearchBy.Text = "Owner Name";
            this.cmbSearchBy.SelectedIndexChanged += new System.EventHandler(this.optSearchBy_CheckedChanged);
            // 
            // lblSearchBy
            // 
            this.lblSearchBy.AutoSize = true;
            this.lblSearchBy.Location = new System.Drawing.Point(20, 44);
            this.lblSearchBy.Name = "lblSearchBy";
            this.lblSearchBy.Size = new System.Drawing.Size(87, 17);
            this.lblSearchBy.TabIndex = 30;
            this.lblSearchBy.Text = "SEARCH BY";
            // 
            // cmbCriteria
            // 
            this.cmbCriteria.Items.AddRange(new object[] {
            "Contain Search Criteria",
            "Start With Search Criteria",
            "End With Search Criteria"});
            this.cmbCriteria.Location = new System.Drawing.Point(192, 108);
            this.cmbCriteria.Name = "cmbCriteria";
            this.cmbCriteria.Size = new System.Drawing.Size(279, 40);
            this.cmbCriteria.TabIndex = 19;
            this.cmbCriteria.Text = "Start With Search Criteria";
            // 
            // lblCriteria
            // 
            this.lblCriteria.AutoSize = true;
            this.lblCriteria.Location = new System.Drawing.Point(20, 122);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(176, 17);
            this.lblCriteria.TabIndex = 20;
            this.lblCriteria.Text = "DISPLAY RECORDS THAT";
            // 
            // cmbTypes
            // 
            this.cmbTypes.Items.AddRange(new object[] {
            "Dogs",
            "Dogs in Kennels",
            "All"});
            this.cmbTypes.Location = new System.Drawing.Point(175, 30);
            this.cmbTypes.Name = "cmbTypes";
            this.cmbTypes.Size = new System.Drawing.Size(194, 40);
            this.cmbTypes.TabIndex = 23;
            this.cmbTypes.Text = "All";
            // 
            // lblTypes
            // 
            this.lblTypes.AutoSize = true;
            this.lblTypes.Location = new System.Drawing.Point(30, 44);
            this.lblTypes.Name = "lblTypes";
            this.lblTypes.Size = new System.Drawing.Size(145, 17);
            this.lblTypes.TabIndex = 24;
            this.lblTypes.Text = "TYPES OF RECORDS";
            // 
            // Frame5
            // 
            this.Frame5.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame5.Controls.Add(this.Label3);
            this.Frame5.Controls.Add(this.cmdClearSearch);
            this.Frame5.Controls.Add(this.cmbSearchBy);
            this.Frame5.Controls.Add(this.lblSearchBy);
            this.Frame5.Controls.Add(this.cmbCriteria);
            this.Frame5.Controls.Add(this.lblCriteria);
            this.Frame5.Controls.Add(this.GridCriteria);
            this.Frame5.Controls.Add(this.Label2);
            this.Frame5.Location = new System.Drawing.Point(30, 162);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(290, 457);
            this.Frame5.TabIndex = 22;
            this.Frame5.Text = "Search";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 80);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(195, 15);
            this.Label3.TabIndex = 28;
            this.Label3.Text = "*  PERFORMS EXACT MATCH ONLY";
            // 
            // cmdClearSearch
            // 
            this.cmdClearSearch.AppearanceKey = "actionButton";
            this.cmdClearSearch.Location = new System.Drawing.Point(20, 397);
            this.cmdClearSearch.Name = "cmdClearSearch";
            this.cmdClearSearch.Size = new System.Drawing.Size(142, 40);
            this.cmdClearSearch.TabIndex = 18;
            this.cmdClearSearch.Text = "Clear Search";
            // 
            // GridCriteria
            // 
            this.GridCriteria.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridCriteria.Cols = 1;
            this.GridCriteria.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridCriteria.ExtendLastCol = true;
            this.GridCriteria.FixedCols = 0;
            this.GridCriteria.Location = new System.Drawing.Point(20, 184);
            this.GridCriteria.Name = "GridCriteria";
            this.GridCriteria.ReadOnly = false;
            this.GridCriteria.RowHeadersVisible = false;
            this.GridCriteria.Size = new System.Drawing.Size(231, 203);
            this.GridCriteria.TabIndex = 17;
            this.GridCriteria.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridCriteria_KeyDownEdit);
            this.GridCriteria.KeyDown += new Wisej.Web.KeyEventHandler(this.GridCriteria_KeyDownEvent);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 158);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(200, 15);
            this.Label2.TabIndex = 27;
            this.Label2.Text = "ENTER SEARCH CRITERIA";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.chkIncludeDeleted);
            this.Frame3.Location = new System.Drawing.Point(30, 80);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(247, 72);
            this.Frame3.TabIndex = 21;
            this.Frame3.Text = "Include";
            // 
            // chkIncludeDeleted
            // 
            this.chkIncludeDeleted.Location = new System.Drawing.Point(20, 30);
            this.chkIncludeDeleted.Name = "chkIncludeDeleted";
            this.chkIncludeDeleted.Size = new System.Drawing.Size(181, 24);
            this.chkIncludeDeleted.TabIndex = 3;
            this.chkIncludeDeleted.Text = "Deleted Dogs or Owners";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNewOwner,
            this.mnuSepar2,
            this.mnuSearch,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNewOwner
            // 
            this.mnuNewOwner.Index = 0;
            this.mnuNewOwner.Name = "mnuNewOwner";
            this.mnuNewOwner.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.mnuNewOwner.Text = "New Owner";
            this.mnuNewOwner.Click += new System.EventHandler(this.mnuNewOwner_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSearch
            // 
            this.mnuSearch.Index = 2;
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSearch.Text = "Search";
            this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 3;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdNewOwner
            // 
            this.cmdNewOwner.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewOwner.Location = new System.Drawing.Point(434, 29);
            this.cmdNewOwner.Name = "cmdNewOwner";
            this.cmdNewOwner.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.cmdNewOwner.Size = new System.Drawing.Size(90, 24);
            this.cmdNewOwner.TabIndex = 1;
            this.cmdNewOwner.Text = "New Owner";
            this.cmdNewOwner.Click += new System.EventHandler(this.mnuNewOwner_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(235, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(96, 48);
            this.cmdProcess.Text = "Search";
            this.cmdProcess.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // frmNewDogSearch
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(560, 621);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmNewDogSearch";
            this.Text = "Dog Records";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmNewDogSearch_Load);
            this.Resize += new System.EventHandler(this.frmNewDogSearch_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewDogSearch_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridCriteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdNewOwner;
		private FCButton cmdProcess;
	}
}