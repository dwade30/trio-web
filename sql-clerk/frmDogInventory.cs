//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using SharedApplication.Extensions;
using System;
using Wisej.Web;

namespace TWCK0000
{
    public partial class frmDogInventory : BaseForm
	{
		public frmDogInventory()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDogInventory InstancePtr
		{
			get
			{
				return (frmDogInventory)Sys.GetInstance(typeof(frmDogInventory));
			}
		}

		protected frmDogInventory _InstancePtr = null;
        //=========================================================
        clsDRWrapper rsAutoInitialized = null;
        clsDRWrapper rs
        {
            get
            {
                if (rsAutoInitialized == null)
                {
                    rsAutoInitialized = new clsDRWrapper();
                }
                return rsAutoInitialized;
            }
            set
            {
                rsAutoInitialized = value;
            }
        }
		string strSQL = "";
		bool boolFormLoading;
		bool boolOKToSave;

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				fecherFoundation.Information.Err().Clear();
				int lngCounter;
				string strType;
				int intYear;
				string strPT = "";
				bool boolShowMsg;
				DialogResult intReturn = 0;
				clsDRWrapper clsTemp = new clsDRWrapper();
				string strTypeDesc = "";
                string strLow = "";
                string strHigh = "";

				boolOKToSave = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (cmbType.Text == "Tag Number")
				{
					strTypeDesc = "tag";
				}
				else if (cmbType.Text == "Sticker Number")
				{
					strTypeDesc = "sticker";
				}
				else if (cmbType.Text == "Kennel License Number")
				{
					strTypeDesc = "kennel license";
				}
                else if (cmbType.Text == "Dangerous Dog")
                {
                    strTypeDesc = "dangerous dog";
                }
				TryOverTag:
				;
                strLow = txtLow.Text.Trim();
                strHigh = txtHigh.Text.Trim();
                strLow = strLow.Replace("DD", "").Replace("dd", "");
                strHigh = strHigh.Replace("DD", "").Replace("dd", "");

				if (strLow != "" && strHigh != "")
				{
					if (Conversion.Val(strLow) != 0 && Conversion.Val(strHigh) != 0)
					{
						if (Conversion.Val(strLow) <= Conversion.Val(strHigh))
						{
						}
						else
						{
							MessageBox.Show("You have entered an invalid High or Low Range of " + strTypeDesc + " numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							return;
						}
					}
					else
					{
						MessageBox.Show("You have entered an invalid High or Low Range of " + strTypeDesc + " numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return;
					}
				}
				else
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("You have entered an invalid High or Low Range of " + strTypeDesc + " numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				intYear = 0;
				strType = "";
				if (cmbType.Text == "Sticker Number")
				{
					if (fecherFoundation.Strings.Trim(txtYear.Text) == string.Empty)
					{
						strType = Interaction.InputBox("Enter the year of these stickers.", "Sticker Year", null);
						if (fecherFoundation.Strings.Trim(strType) == string.Empty)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							return;
						}
					}
					else
					{
						strType = txtYear.Text;
					}
					intYear = FCConvert.ToInt32(strType);
					if (Information.IsDate("12/31/" + FCConvert.ToString(intYear)) == false)
					{
						MessageBox.Show("Invalid year, please re-enter.");
						goto TryOverTag;
					}
				}
				else if (cmbType.Text == "Tag Number")
				{
					if (Conversion.Val(txtYear.Text) == 0)
					{
						object temp = intYear;
						if (!frmInput.InstancePtr.Init(ref temp, "Tag Year", "Enter the year of these tags", 825, false, modGlobalConstants.InputDTypes.idtWholeNumber, FCConvert.ToString(DateTime.Today.Year), true))
						{
							strType = "";
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							return;
						}
						intYear = FCConvert.ToInt32(temp);
					}
					else
					{
						intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
						strType = txtYear.Text;
					}
					if (intYear < (DateTime.Today.Year - 2) || intYear > (DateTime.Today.Year + 2))
					{
						MessageBox.Show("Invalid year, please re-enter");
						goto TryOverTag;
					}
				}
				strType = "";
				boolShowMsg = true;
				if (cmbType.Text == "Tag Number")
					strType = "T";
				if (cmbType.Text == "Sticker Number")
					strType = "S";
				if (cmbType.Text == "Kennel License Number")
					strType = "K";
                if (cmbType.Text == "Dangerous Dog")
                {
                    strType = "D";
                }
				if (strType == "")
				{
					MessageBox.Show("You must choose an inventory type.", "Choose a Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
				bool boolMsgShown;
				boolMsgShown = false;
				if (strLow != "" && strHigh != "")
				{
					if (strLow.ToIntegerValue() != 0 && strHigh.ToIntegerValue() != 0)
					{
						if (strLow.ToIntegerValue() <= strHigh.ToIntegerValue())
						{
							if (cmbType.Text == "Sticker Number" || cmbType.Text == "Tag Number")
							{
								strSQL = "SELECT * FROM DogInventory where Year = " + FCConvert.ToString(intYear) + " and Type = '" + strType + "'";
							}
							else
							{
								strSQL = "SELECT * FROM DogInventory where Type = '" + strType + "'";
							}
							rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
							if (rs.EndOfFile())
							{
								for (lngCounter = strLow.ToIntegerValue(); lngCounter <= strHigh.ToIntegerValue(); lngCounter++)
								{
									rs.AddNew();
                                    if (strType != "D")
                                    {
                                        rs.Set_Fields("StickerNumber", lngCounter);
                                        rs.Set_Fields("Year", intYear);
                                    }
                                    else
                                    {
                                        rs.Set_Fields("StickerNumber","DD" + lngCounter);
                                        rs.Set_Fields("Year",0);
                                    }
									rs.Set_Fields("Type", strType);
									rs.Set_Fields("Status", "Active");
									rs.Set_Fields("DateReceived", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
									rs.Set_Fields("ReceivedOPID", modClerkGeneral.Statics.User + " ");
									rs.Update();
								}
								// lngCounter
							}
							else
							{
								for (lngCounter =strLow.ToIntegerValue(); lngCounter <= strHigh.ToIntegerValue(); lngCounter++)
								{
                                    if (strType != "D")
                                    {
                                        rs.FindFirst("StickerNumber = '" + lngCounter + "'");
                                    }
                                    else
                                    {
                                        rs.FindFirst("StickerNumber = 'DD" + lngCounter + "'");
                                    }									
									if (rs.NoMatch == true)
									{
										rs.AddNew();
                                        if (strType != "D")
                                        {
                                            rs.Set_Fields("StickerNumber", lngCounter);
                                        }
                                        else
                                        {
                                            rs.Set_Fields("StickerNumber","DD" + lngCounter);
                                        }
										
										rs.Set_Fields("Year", intYear);
										rs.Set_Fields("Type", strType);
										rs.Set_Fields("Status", "Active");
										rs.Set_Fields("DateReceived", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
										rs.Set_Fields("ReceivedOPID", modClerkGeneral.Statics.User + " ");
										rs.Update();
									}
									else
									{
										// check if it is still used
										if (FCConvert.ToString(rs.Get_Fields_String("Status")) == "Active")
										{
											if (boolShowMsg)
											{
												MessageBox.Show("Inventory numbers that have already been issued will not be added to inventory.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
												boolShowMsg = false;
												// Screen.MousePointer = vbDefault
												// Exit Sub
											}
										}
										else
										{
											// search for a dog that has the sticker
											if (cmbType.Text == "Sticker Number")
											{
												clsTemp.OpenRecordset("select ownernum from DOGINFO where  isnull(stickerlicnum, '')) = '" + lngCounter + "'", "Clerk");
											}
											else if (cmbType.Text == "Tag Number")
											{
												clsTemp.OpenRecordset("select ownernum from doginfo where isnull(taglicnum, '') = '" + lngCounter + "' and [year] = " + intYear, "Clerk");
											}
											else if (cmbType.Text == "Kennel License Number")
											{
												clsTemp.OpenRecordset("select OWNERnum from kennellicense where licenseid = " + rs.Get_Fields_Int32("ID"), "Clerk");
											}
                                            else if (cmbType.Text == "Dangerous Dog")
                                            {
                                                clsTemp.OpenRecordset(
                                                    "select ownernum from doginfo where taglicnum + '' = 'DD" +
                                                    lngCounter + "'", "Clerk");
                                            }
											if (clsTemp.EndOfFile())
											{
												if (cmbType.Text != "Kennel License Number")
												{
													intReturn = MessageBox.Show("This " + strTypeDesc + " has been issued but no dog has this " + strTypeDesc + "." + "\r\n" + "Do you wish to return the " + strTypeDesc + "?", fecherFoundation.Strings.StrConv(strTypeDesc, VbStrConv.ProperCase) + " Issued", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
												}
												else
												{
													intReturn = MessageBox.Show("This " + strTypeDesc + " has been issued but no " + strTypeDesc + " has this number." + "\r\n" + "Do you wish to return the " + strTypeDesc + "?", fecherFoundation.Strings.StrConv(strTypeDesc, VbStrConv.ProperCase) + " Issued", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
												}
												if (intReturn == DialogResult.Yes)
												{
													rs.Edit();
													rs.Set_Fields("Status", "Active");
													rs.Update();
												}
											}
											else
											{
												if (clsTemp.Get_Fields_Int32("ownernum") < 0)
												{
													if (cmbType.Text != "Kennel License Number")
													{
														intReturn = MessageBox.Show("This " + strTypeDesc + " has been issued but no dog has this " + strTypeDesc + "." + "\r\n" + "Do you wish to return the " + strTypeDesc + "?", fecherFoundation.Strings.StrConv(strTypeDesc, VbStrConv.ProperCase) + " Issued", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
													}
													else
													{
														intReturn = MessageBox.Show("This " + strTypeDesc + " has been issued but no " + strTypeDesc + " has this number." + "\r\n" + "Do you wish to return the " + strTypeDesc + "?", fecherFoundation.Strings.StrConv(strTypeDesc, VbStrConv.ProperCase) + " Issued", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
													}
													if (intReturn == DialogResult.Yes)
													{
														rs.Edit();
														rs.Set_Fields("Status", "Active");
														rs.Update();
													}
													// Screen.MousePointer = vbDefault
													// Call cmdView_Click
													// Exit Sub
												}
												else
												{
													if (boolShowMsg && !boolMsgShown)
													{
														boolMsgShown = true;
														MessageBox.Show("Inventory numbers that have already been issued will not be added to inventory.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
														boolShowMsg = false;
														// Screen.MousePointer = vbDefault
														// Exit Sub
													}
												}
											}
										}
									}
								}
								// lngCounter
							}
						}
						else
						{
							MessageBox.Show("You have entered an invalid High or Low Range of numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							return;
						}
					}
					else
					{
						MessageBox.Show("You have entered an invalid High or Low Range of numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return;
					}
				}
				else
				{
					MessageBox.Show("You have entered an invalid High or Low Range of numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
				cmdView_Click();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Inventory update successful.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtHigh.Text = string.Empty;
				txtLow.Text = string.Empty;
				boolOKToSave = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdAdd_Click()
		{
			cmdAdd_Click(cmdAdd, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdQuit_Click()
		{		
		}

		private void cmdRemove_Click(object sender, System.EventArgs e)
		{
			int lngCounter;
			string strType = "";
			object intYear;
			string strPT = "";
			bool boolDeletedOne;
            string strLow = "";
            string strHigh = "";

			TryOverTag:
			;
            strLow = txtLow.Text;
            strHigh = txtHigh.Text;
            strLow = strLow.Replace("DD", "").Replace("dd", "");
            strHigh = strHigh.Replace("DD", "").Replace("dd", "");
            if (txtLow.Text != "" && txtHigh.Text != "")
			{
				if (strLow.ToIntegerValue() != 0 && strHigh.ToIntegerValue() != 0)
				{
					if (strLow.ToIntegerValue() <= strHigh.ToIntegerValue())
					{
					}
					else
					{
						MessageBox.Show("You have entered an invalid range of numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return;
					}
				}
				else
				{
					MessageBox.Show("You have entered an invalid range of numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
			}
			else
			{
				MessageBox.Show("You have entered an invalid range of numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
			if (cmbType.Text == "Sticker Number")
			{
				strType = "";
				if (FCConvert.ToInt32(intYear) == 0)
				{
					strType = Interaction.InputBox("Enter the year of these stickers.", "Sticker Year", null);
					if (strType == "")
					{
						return;
					}
					else
					{
						intYear = FCConvert.ToInt32(strType);
						if (Information.IsDate("12/31/" + FCConvert.ToString(intYear)) == false)
						{
							MessageBox.Show("Invalid year, please re-enter.");
							goto TryOverTag;
						}
					}
				}
			}
			else if (cmbType.Text == "Tag Number")
			{
				strType = "";
				if (intYear.ToString() == "0")
				{
					if (!frmInput.InstancePtr.Init(ref intYear, "Tag Year", "Enter the year of these tags", 825, false, modGlobalConstants.InputDTypes.idtWholeNumber, "", true))
					{
						return;
					}
				}
			}
			strType = "";
			if (cmbType.Text == "Tag Number")
				strType = "T";
			if (cmbType.Text == "Sticker Number")
				strType = "S";
			if (cmbType.Text == "Kennel License Number")
				strType = "K";
            if (cmbType.Text == "Dangerous Dog")
            {
                strType = "D";
            }
			if (strType == "")
			{
				MessageBox.Show("You must choose an inventory type", "Choose a Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			boolDeletedOne = false;
			if (strLow != "" && strHigh != "")
			{
				if (strLow.ToIntegerValue() != 0 && strHigh.ToIntegerValue() != 0)
				{
					if (strLow.ToIntegerValue() <= strHigh.ToIntegerValue())
					{
						if (strType == "K" || strType == "D")
						{
							strSQL = "SELECT * FROM DogInventory WHERE Type = '" + strType + "'";
						}
						else
						{
							strSQL = "SELECT * FROM DogInventory WHERE Year = " + FCConvert.ToString(intYear) + " And Type = '" + strType + "'";
						}
						rs.OpenRecordset(strSQL, "Clerk");
						for (lngCounter = strLow.ToIntegerValue(); lngCounter <= strHigh.ToIntegerValue(); lngCounter++)
						{
                            if (strType !=  "D")
                            {
                                rs.FindFirst("StickerNumber = '" + lngCounter + "'");
                            }
                            else
                            {
                                rs.FindFirst("StickerNumber = 'DD" + lngCounter + "'");
                            }
                            if (rs.NoMatch == false)
							{
								boolDeletedOne = true;
								rs.Delete();
                                rs.Update();
								rs.MoveNext();
							}
						}
						// lngCounter
						if (boolDeletedOne)
						{
							object strReason = "";
							if (!frmInput.InstancePtr.Init(ref strReason, "Reason", "Reason for deletion", 4320, false, modGlobalConstants.InputDTypes.idtString))
							{
								strReason = "";
							}
							if (strType.ToUpper() == "T")
							{
								modGlobalFunctions.AddCYAEntry("CK", "Deleted Tags", intYear.ToString(), strLow, strHigh, strReason.ToString());
							}
							else if (strType.ToUpper() == "K")
							{
								modGlobalFunctions.AddCYAEntry("CK", "Deleted Kennel Licenses", "0", strLow, strHigh, strReason.ToString());
							}
                            else if (strType.ToUpper() == "D")
                            {
                                modGlobalFunctions.AddCYAEntry("CK", "Deleted Dangerous Tags","0","DD" + strLow, "DD" + strHigh,strReason.ToString());
                            }
						}
						cmdView_Click();
					}
					else
					{
						MessageBox.Show("You have entered an invalid range of sticker numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return;
					}
				}
				else
				{
					MessageBox.Show("You have entered an invalid range of numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
			}
			else
			{
				MessageBox.Show("You have entered an invalid range of numbers.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			cmdView_Click();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			if (boolDeletedOne)
			{
				MessageBox.Show("Inventory deletion successful.", "Items removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				if (strType == "K")
				{
					MessageBox.Show("No matches found" + "\r\n" + "Nothing deleted", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show("No matches were found for the numbers and year entered" + "\r\n" + "No items were removed from inventory", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			txtHigh.Text = string.Empty;
			txtLow.Text = string.Empty;
		}

		public void cmdRemove_Click()
		{
			cmdRemove_Click(cmdRemove, new System.EventArgs());
		}

		private void cmdView_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngFirstNumber = 0;
			// vbPorter upgrade warning: lngLastNumber As int	OnWrite(int, double)
			int lngLastNumber = 0;
			int intYear = 0;
			string strType = "";
			string strPT = "";
            string strFirstNumber = "";
            string strLastNumber = "";
            string strTag = "";
            string strStrippedTag = "";

			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (cmbType.Text == "Tag Number")
				strType = "T";
			if (cmbType.Text == "Sticker Number")
				strType = "S";
			if (cmbType.Text == "Kennel License Number")
				strType = "K";
            if (cmbType.Text == "Dangerous Dog")
            {
                strType = "D";
            }
			if (strType == "")
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("You must choose a dog inventory type (Tag,Sticker,Kennel).", "Choose a Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			strSQL = "SELECT * FROM DogInventory WHERE [Type] = '" + strType + "' and status = 'Active' ORDER BY Year, [Type],StickerNumber";
			rs.OpenRecordset(strSQL, "Clerk");
            //FC:FINAL:SBE - #i2333 - use BeginUpdate/EndUpdate to avoid ApplicationException when cells are updated on client side.
            vs1.BeginUpdate();
            vs1.Clear();
			LoadHeaders();
			lngRow = 0;
            vs1.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				CheckFirstAgain:
				;
                strTag = rs.Get_Fields_String("StickerNumber");
                strStrippedTag = strTag.Replace("DD", "").Replace("dd", "");
				if (FCConvert.ToString(rs.Get_Fields_String("Status")) == "Active")
                {
                    lngFirstNumber = strStrippedTag.ToIntegerValue();
					lngLastNumber = lngFirstNumber;
				}
				else
				{
					rs.MoveNext();
					if (rs.EndOfFile() != true)
					{
						goto CheckFirstAgain;
					}
					else
					{
                        vs1.EndUpdate();
						return;
					}
				}
				intYear = FCConvert.ToInt16(rs.Get_Fields("Year"));
				while (!rs.EndOfFile())
                {
                    strTag = rs.Get_Fields_String("StickerNumber");
                    strStrippedTag = strTag.Replace("DD", "").Replace("dd", "");
					if (rs.Get_Fields_String("Status") == "Active" && strTag != string.Empty)
					{
						if (strStrippedTag.ToIntegerValue() == lngLastNumber && (strType == "D" ||intYear == rs.Get_Fields_Int32("Year")))
						{
							lngLastNumber += 1;
						}
						else
						{
							lngRow += 1;
                            vs1.Rows = vs1.Rows + 1;
							vs1.TextMatrix(lngRow, 0,intYear.ToString());
							if (strType == "T")
							{
								vs1.TextMatrix(lngRow, 1, "TAG");
								vs1.ColHidden(0, false);
							}
							else if (strType == "S")
							{
								vs1.TextMatrix(lngRow, 1, "STICKER");
								vs1.ColHidden(0, false);
							}
							else if (strType == "K")
							{
								vs1.TextMatrix(lngRow, 1, "KENNEL");
								vs1.ColHidden(0, true);
							}
                            else if (strType == "D")
                            {
                                vs1.TextMatrix(lngRow, 1, "DANGEROUS");
                                vs1.ColHidden(0, true);
                            }
							vs1.TextMatrix(lngRow, 2, FCConvert.ToString(lngFirstNumber));
							vs1.TextMatrix(lngRow, 3, FCConvert.ToString(lngLastNumber - 1));
							intYear = FCConvert.ToInt16(rs.Get_Fields("Year"));
                            lngFirstNumber = strStrippedTag.ToIntegerValue();
                            lngLastNumber = strStrippedTag.ToIntegerValue() + 1;
						}
					}
					rs.MoveNext();
				}
				if (lngFirstNumber != lngLastNumber)
                {
                    vs1.Rows = vs1.Rows + 1;
					vs1.TextMatrix(lngRow + 1, 0, intYear.ToString());
					if (strType == "T")
					{
						vs1.TextMatrix(lngRow + 1, 1, "TAG");
						vs1.ColHidden(0, false);
					}
					else if (strType == "S")
					{
						vs1.TextMatrix(lngRow + 1, 1, "STICKER");
						vs1.ColHidden(0, false);
					}
					else if (strType == "K")
					{
						vs1.TextMatrix(lngRow + 1, 1, "KENNEL");
						vs1.ColHidden(0, true);
					}
                    else if (strType == "D")
                    {
                        vs1.TextMatrix(lngRow + 1, 1, "DANGEROUS");
                        vs1.ColHidden(0, true);
                    }
					vs1.TextMatrix(lngRow + 1, 2, FCConvert.ToString(lngFirstNumber));
					vs1.TextMatrix(lngRow + 1, 3, FCConvert.ToString(lngLastNumber - 1));
				}
			}
			else
            {
                vs1.Rows = vs1.Rows + 1;
				if (strType == "T")
				{
					vs1.TextMatrix(lngRow + 1, 1, "TAG");
					vs1.ColHidden(0, false);
				}
				else if (strType == "S")
				{
					vs1.TextMatrix(lngRow + 1, 1, "STICKER");
					vs1.ColHidden(0, false);
				}
				else if (strType == "K")
				{
					vs1.TextMatrix(lngRow + 1, 1, "KENNEL");
					vs1.ColHidden(0, true);
				}
                else if (strType == "D")
                {
                    vs1.TextMatrix(lngRow + 1, 1, "DANGEROUS");
                    vs1.ColHidden(0, true);
                }
			}
			rs = null;
			if (vs1.Rows > 1)
			{
				for (lngRow = vs1.Rows - 1; lngRow >= 1; lngRow--)
				{
					if (vs1.TextMatrix(lngRow, 1) == string.Empty)
					{
						vs1.RemoveItem(lngRow);
					}
				}
				// lngRow
			}
            //FC:FINAL:SBE - #i2333 - use BeginUpdate/EndUpdate to avoid ApplicationException when cells are updated on client side.
            vs1.EndUpdate();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		public void cmdView_Click()
		{
			cmdView_Click(cmdView, new System.EventArgs());
		}

		private void frmDogInventory_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
		}

		private void frmDogInventory_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDogInventory_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				// enter key
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// CallByName MDIParent, "Grid_GotFocus", VbMethod
		}

		private void frmDogInventory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDogInventory properties;
			//frmDogInventory.ScaleWidth	= 9045;
			//frmDogInventory.ScaleHeight	= 7365;
			//frmDogInventory.LinkTopic	= "Form1";
			//frmDogInventory.LockControls	= -1  'True;
			//End Unmaped Properties
			boolFormLoading = true;
			txtYear.Text = FCConvert.ToString(DateTime.Now.Year);
			LoadHeaders();
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void LoadHeaders()
		{
			vs1.TextMatrix(0, 0, "Year");
			vs1.TextMatrix(0, 1, "Desc");
			vs1.TextMatrix(0, 2, "Low");
			vs1.TextMatrix(0, 3, "High");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			cmdAdd_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuRemove_Click(object sender, System.EventArgs e)
		{
			cmdRemove_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdAdd_Click();
			if (boolOKToSave)
				Close();
		}

		private void mnuView_Click(object sender, System.EventArgs e)
		{
			cmdView_Click();
		}

		private void optType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			cmdView_Click();
			txtYear.Visible = cmbType.Text != "Kennel License Number" && cmbType.Text != "Dangerous Dog";
			this.Label4.Visible = cmbType.Text != "Kennel License Number" && cmbType.Text != "Dangerous Dog";
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void optType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbType.SelectedIndex;
			optType_CheckedChanged(index, sender, e);
		}

		private void txtYear_Enter(object sender, System.EventArgs e)
		{
			txtYear.SelectionStart = 0;
			txtYear.SelectionLength = txtYear.Text.Length;
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			string strType;
			string strPT = "";
			int intYear = 0;
			if (vs1.MouseRow < 1)
				return;
			if (vs1.Row < 1)
				return;
			strType = string.Empty;
			if (cmbType.Text == "Tag Number")
				strType = "T";
			if (cmbType.Text == "Sticker Number")
				strType = "S";
			if (cmbType.Text == "Kennel License Number")
				strType = "K";
            if (cmbType.Text == "Dangerous Dog")
            {
                strType = "D";
            }
			if (FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 0)) == string.Empty || FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 2)) == string.Empty || FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 3)) == string.Empty)
			{
			}
			else
			{
				intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				if (strType == "")
				{
					MessageBox.Show("You must choose an inventory type", "Choose a Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				object strReason = "";
				if (intYear == 0)
				{
					// there is no year so don't show it on the message
					if (MessageBox.Show("Delete inventory from " + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 2) + " to " + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 3) + "?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (!frmInput.InstancePtr.Init(ref strReason, "Reason", "Reason for deletion", 4320, false, modGlobalConstants.InputDTypes.idtString))
						{
							return;
						}
						if (fecherFoundation.Strings.UCase(strType) == "T")
						{
							modGlobalFunctions.AddCYAEntry("CK", "Deleted Tags", intYear.ToString(), FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 2))), FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 3))), strReason.ToString());
						}
						else if (fecherFoundation.Strings.UCase(strType) == "K")
						{
							modGlobalFunctions.AddCYAEntry("CK", "Deleted Kennel Licenses", FCConvert.ToString(0), FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 2))), FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 3))), strReason.ToString());
						}
                        else if (strType.ToUpper() == "D")
                        {
                            modGlobalFunctions.AddCYAEntry("CK", "Deleted Dangerous Dog Tags", FCConvert.ToString(0), FCConvert.ToString(vs1.TextMatrix(vs1.Row, 2).ToIntegerValue()), vs1.TextMatrix(vs1.Row, 3), strReason.ToString());
                        }

                        if (strType != "D")
                        {
                            rs.Execute("Delete from DogInventory where Year = " + FCConvert.ToString(Conversion.Val(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 0))) + " and StickerNumber >='" + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 2) + "' and StickerNumber <= '" + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 3) + "' and Type = '" + strType + "'", "Clerk");
                        }
                        else
                        {
                            rs.Execute("Delete from DogInventory where  StickerNumber >= 'DD" + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 2) + "' and StickerNumber <= 'DD" + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 3) + "' and Type = '" + strType + "'", "Clerk");
                        }
						
						cmdView_Click();
					}
				}
				else
				{
					if (MessageBox.Show("Delete inventory from " + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 2) + " to " + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 3) + " for the year " + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 0) + "?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (!frmInput.InstancePtr.Init(ref strReason, "Reason", "Reason for deletion", 4320, false, modGlobalConstants.InputDTypes.idtString))
						{
							return;
						}
						if (strType.ToUpper() == "T")
						{
							modGlobalFunctions.AddCYAEntry("CK", "Deleted Tags", intYear.ToString(), FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 2))), FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 3))), strReason.ToString());
						}
						else if (strType.ToUpper() == "K")
						{
							modGlobalFunctions.AddCYAEntry("CK", "Deleted Kennel Licenses", FCConvert.ToString(0), FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 2))), FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 3))), strReason.ToString());
						}
                        else if (strType.ToUpper() == "D")
                        {
                            modGlobalFunctions.AddCYAEntry("CK", "Deleted Dangerous Dog Tags", "0", "DD" + vs1.TextMatrix(vs1.Row, 2), "DD" + vs1.TextMatrix(vs1.Row, 3), strReason.ToString());
                        }

                        if (strType != "D")
                        {
                            rs.Execute(
                                "Delete from DogInventory where Year = " + FCConvert.ToString(intYear) +
                                " and StickerNumber >= '" +
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 2) +
                                "' and StickerNumber <= '" +
                                vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 3) + "' and Type = '" +
                                strType + "'", modGNBas.DEFAULTDATABASE);
                        }
                        else
                        {
                            rs.Execute("delete from DogInventory where StickerNumber >= 'DD" 
                                       + vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 2) +
                                       "' and StickerNumber <= 'DD" +
                                       vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, 3) + "' and Type = '" +
                                       strType + "'",
                                "Clerk");                                       
                        }

                        cmdView_Click();
					}
				}
			}
		}
	}
}
