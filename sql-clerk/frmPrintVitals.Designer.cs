//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmPrintVitalRec.
	/// </summary>
	partial class frmPrintVitalRec
	{
		public fecherFoundation.FCComboBox cmbSafetyPaper;
		public fecherFoundation.FCLabel lblSafetyPaper;
		public fecherFoundation.FCComboBox cmbtion2;
		public fecherFoundation.FCLabel lbltion2;
		public fecherFoundation.FCComboBox cmbtion1;
		public System.Collections.Generic.List<fecherFoundation.FCButton> txtPrint;
		public fecherFoundation.FCButton txtPrint_0;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkDontShowBottomHeadings;
		public fecherFoundation.FCCheckBox chkPrint;
		public fecherFoundation.FCTextBox txtAttest;
		public fecherFoundation.FCTextBox txtVitalCertNum;
		public fecherFoundation.FCCheckBox Check1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblEvent;
		public fecherFoundation.FCLabel lbVitalCertNum;
		public fecherFoundation.FCButton txtPrint_2;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbSafetyPaper = new fecherFoundation.FCComboBox();
            this.lblSafetyPaper = new fecherFoundation.FCLabel();
            this.cmbtion2 = new fecherFoundation.FCComboBox();
            this.lbltion2 = new fecherFoundation.FCLabel();
            this.cmbtion1 = new fecherFoundation.FCComboBox();
            this.txtPrint_0 = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkDontShowBottomHeadings = new fecherFoundation.FCCheckBox();
            this.chkPrint = new fecherFoundation.FCCheckBox();
            this.txtAttest = new fecherFoundation.FCTextBox();
            this.txtVitalCertNum = new fecherFoundation.FCTextBox();
            this.Check1 = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblEvent = new fecherFoundation.FCLabel();
            this.lbVitalCertNum = new fecherFoundation.FCLabel();
            this.txtPrint_2 = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrint_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDontShowBottomHeadings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrint_2)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.txtPrint_2);
            this.BottomPanel.Location = new System.Drawing.Point(0, 410);
            this.BottomPanel.Size = new System.Drawing.Size(497, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.txtPrint_0);
            this.ClientArea.Size = new System.Drawing.Size(497, 350);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(497, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(197, 30);
            this.HeaderText.Text = "Print Parameters";
            // 
            // cmbSafetyPaper
            // 
            this.cmbSafetyPaper.Items.AddRange(new object[] {
            "R196",
            "R0606",
            "VS-31"});
            this.cmbSafetyPaper.Location = new System.Drawing.Point(204, 90);
            this.cmbSafetyPaper.Name = "cmbSafetyPaper";
            this.cmbSafetyPaper.Size = new System.Drawing.Size(215, 40);
            this.cmbSafetyPaper.Text = "R196";
            // 
            // lblSafetyPaper
            // 
            this.lblSafetyPaper.AutoSize = true;
            this.lblSafetyPaper.Location = new System.Drawing.Point(20, 104);
            this.lblSafetyPaper.Name = "lblSafetyPaper";
            this.lblSafetyPaper.Size = new System.Drawing.Size(111, 17);
            this.lblSafetyPaper.TabIndex = 1;
            this.lblSafetyPaper.Text = "SAFETY PAPER";
            // 
            // cmbtion2
            // 
            this.cmbtion2.Items.AddRange(new object[] {
            "Municipal Clerk",
            "State Registrar",
            "Assistant Clerk",
            "Deputy Clerk"});
            this.cmbtion2.Location = new System.Drawing.Point(204, 257);
            this.cmbtion2.Name = "cmbtion2";
            this.cmbtion2.Size = new System.Drawing.Size(215, 40);
            this.cmbtion2.TabIndex = 17;
            this.cmbtion2.Text = "Municipal Clerk";
            // 
            // lbltion2
            // 
            this.lbltion2.AutoSize = true;
            this.lbltion2.Location = new System.Drawing.Point(20, 271);
            this.lbltion2.Name = "lbltion2";
            this.lbltion2.Size = new System.Drawing.Size(148, 17);
            this.lbltion2.TabIndex = 18;
            this.lbltion2.Text = "TITLE OF RECORDER";
            // 
            // cmbtion1
            // 
            this.cmbtion1.Items.AddRange(new object[] {
            "",
            "",
            "",
            ""});
            this.cmbtion1.Location = new System.Drawing.Point(20, 30);
            this.cmbtion1.Name = "cmbtion1";
            this.cmbtion1.Size = new System.Drawing.Size(230, 40);
            this.cmbtion1.TabIndex = 19;
            this.cmbtion1.SelectedIndexChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // txtPrint_0
            // 
            this.txtPrint_0.AppearanceKey = "actionButton";
            this.txtPrint_0.Location = new System.Drawing.Point(30, 307);
            this.txtPrint_0.Name = "txtPrint_0";
            this.txtPrint_0.Size = new System.Drawing.Size(64, 40);
            this.txtPrint_0.TabIndex = 18;
            this.txtPrint_0.Text = "Print";
            this.txtPrint_0.Visible = false;
            this.txtPrint_0.Click += new System.EventHandler(this.txtPrint_Click);
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.cmbSafetyPaper);
            this.Frame1.Controls.Add(this.lblSafetyPaper);
            this.Frame1.Controls.Add(this.chkDontShowBottomHeadings);
            this.Frame1.Controls.Add(this.cmbtion2);
            this.Frame1.Controls.Add(this.lbltion2);
            this.Frame1.Controls.Add(this.cmbtion1);
            this.Frame1.Controls.Add(this.chkPrint);
            this.Frame1.Controls.Add(this.txtAttest);
            this.Frame1.Controls.Add(this.txtVitalCertNum);
            this.Frame1.Controls.Add(this.Check1);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.lblEvent);
            this.Frame1.Controls.Add(this.lbVitalCertNum);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(439, 317);
            this.Frame1.TabIndex = 4;
            this.Frame1.Text = "Select Form";
            // 
            // chkDontShowBottomHeadings
            // 
            this.chkDontShowBottomHeadings.Location = new System.Drawing.Point(20, 90);
            this.chkDontShowBottomHeadings.Name = "chkDontShowBottomHeadings";
            this.chkDontShowBottomHeadings.Size = new System.Drawing.Size(209, 24);
            this.chkDontShowBottomHeadings.TabIndex = 16;
            this.chkDontShowBottomHeadings.Text = "Don\'t show bottom headings";
            this.chkDontShowBottomHeadings.Visible = false;
            // 
            // chkPrint
            // 
            this.chkPrint.Checked = true;
            this.chkPrint.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkPrint.Location = new System.Drawing.Point(20, 210);
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Size = new System.Drawing.Size(73, 24);
            this.chkPrint.TabIndex = 12;
            this.chkPrint.Text = "Check2";
            // 
            // txtAttest
            // 
            this.txtAttest.BackColor = System.Drawing.SystemColors.Window;
            this.txtAttest.Location = new System.Drawing.Point(204, 150);
            this.txtAttest.Name = "txtAttest";
            this.txtAttest.Size = new System.Drawing.Size(215, 40);
            this.txtAttest.TabIndex = 1;
            this.txtAttest.TabStop = false;
            // 
            // txtVitalCertNum
            // 
            this.txtVitalCertNum.BackColor = System.Drawing.SystemColors.Window;
            this.txtVitalCertNum.Location = new System.Drawing.Point(20, 30);
            this.txtVitalCertNum.Name = "txtVitalCertNum";
            this.txtVitalCertNum.Size = new System.Drawing.Size(215, 40);
            this.txtVitalCertNum.TabIndex = 20;
            this.txtVitalCertNum.Visible = false;
            this.txtVitalCertNum.Validating += new System.ComponentModel.CancelEventHandler(this.txtVitalCertNum_Validating);
            // 
            // Check1
            // 
            this.Check1.Location = new System.Drawing.Point(20, 257);
            this.Check1.Name = "Check1";
            this.Check1.Size = new System.Drawing.Size(88, 24);
            this.Check1.TabIndex = 11;
            this.Check1.Text = "Test Print ";
            this.Check1.Visible = false;
            this.Check1.CheckedChanged += new System.EventHandler(this.Check1_CheckedChanged);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 164);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(101, 15);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "ATTESTED BY";
            // 
            // lblEvent
            // 
            this.lblEvent.Location = new System.Drawing.Point(119, 271);
            this.lblEvent.Name = "lblEvent";
            this.lblEvent.Size = new System.Drawing.Size(109, 15);
            this.lblEvent.TabIndex = 6;
            this.lblEvent.Text = "G";
            this.lblEvent.Visible = false;
            // 
            // lbVitalCertNum
            // 
            this.lbVitalCertNum.Location = new System.Drawing.Point(20, 30);
            this.lbVitalCertNum.Name = "lbVitalCertNum";
            this.lbVitalCertNum.Size = new System.Drawing.Size(220, 16);
            this.lbVitalCertNum.TabIndex = 5;
            this.lbVitalCertNum.Text = "VITAL RECORD CERTIFICATE NUMBER";
            this.lbVitalCertNum.Visible = false;
            // 
            // txtPrint_2
            // 
            this.txtPrint_2.AppearanceKey = "acceptButton";
            this.txtPrint_2.Location = new System.Drawing.Point(171, 30);
            this.txtPrint_2.Name = "txtPrint_2";
            this.txtPrint_2.Shortcut = Wisej.Web.Shortcut.F12;
            this.txtPrint_2.Size = new System.Drawing.Size(159, 48);
            this.txtPrint_2.TabIndex = 17;
            this.txtPrint_2.Text = "Print / Preview";
            this.txtPrint_2.Click += new System.EventHandler(this.txtPrint_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintPreview,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = 0;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmPrintVitalRec
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(497, 518);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.Name = "frmPrintVitalRec";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Print Parameters";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPrintVitalRec_Load);
            this.Activated += new System.EventHandler(this.frmPrintVitalRec_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintVitalRec_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrint_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDontShowBottomHeadings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Check1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrint_2)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}