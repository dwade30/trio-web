//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDeathNonConfidential.
	/// </summary>
	public partial class rptDeathNonConfidential : BaseSectionReport
	{
		public rptDeathNonConfidential()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Non-Confidential Record Of Death";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDeathNonConfidential InstancePtr
		{
			get
			{
				return (rptDeathNonConfidential)Sys.GetInstance(typeof(rptDeathNonConfidential));
			}
		}

		protected rptDeathNonConfidential _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDeathNonConfidential	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrintTest;
		private double dblLineAdjust;
		private bool boolPrinted;
		private int intPrinted;

		public void Init(bool boolTestPrint, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			dblLineAdjust = dblAdjust;
			if (modClerkGeneral.Statics.utPrintInfo.PreView)
			{
				frmPrintVitalRec.InstancePtr.Hide();
				if (!boolPrintTest)
				{
					frmDeaths.InstancePtr.Hide();
				}
                //FC:FINAL:IPI - #i1705 - show the report viewer on web
                //this.Show(FCForm.FormShowEnum.Modal);
                frmReportViewer.InstancePtr.Init(this, showModal: true);
            }
			else
			{
				this.PrintReport(false);
			}
		}

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted += this.Document.Printer.DeviceCopies;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblLaserLineAdjustment4 = 0;
			// Dim rsData                  As New clsDRWrapper
			int lngID;
			double dblRatio;
			int lngWidth;
			int lngHeight;

			if (!boolPrintTest)
			{
				dblLaserLineAdjustment4 = Conversion.Val(modRegistry.GetRegistryKey("NonConfidentialDeathAdjustment", "CK"));
			}
			else
			{
				dblLaserLineAdjustment4 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment4) / 1440F;
			}
			if (dblLaserLineAdjustment4 > 0)
			{
				Detail.Height += FCConvert.ToSingle(200 * dblLaserLineAdjustment4) / 1440F;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!boolPrintTest)
			{
				txtDecedent.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("LastName") + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("Designation");
				if (Information.IsDate(modGNBas.Statics.rsDeathCertificate.Get_Fields("DateOfDeath")))
				{
					txtDateOfDeath.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields("DateofDeath"), "MM/dd/yyyy");
				}
				else
				{
					txtDateOfDeath.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateOfDeathDescription");
				}
				txtLocationOfDeath.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CityorTownofDeath");
				if (Conversion.Val(modGNBas.Statics.rsDeathCertificate.Get_Fields("age")) > 0)
				{
					txtAge.Text = FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields("age"));
				}
				else
				{
					txtAge.Text = "";
				}
				txtDateIssued.Text = Strings.Format(DateTime.Now, "MMMM dd, yyyy");
				if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
				{
					txtAttest.Text = "";
				}
				else
				{
					txtAttest.Text = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				}
				if (Conversion.Val(modGNBas.Statics.rsDeathCertificate.Get_Fields_Int32("towncode")) == 0)
				{
					txtTownOf.Text = fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase);
				}
				else
				{
					txtTownOf.Text = fecherFoundation.Strings.StrConv(modRegionalTown.GetTownKeyName_2(modGNBas.Statics.rsDeathCertificate.Get_Fields_Int32("towncode")), VbStrConv.ProperCase);
				}
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			int lngNumCopies;
			double dblTemp;
			if (modCashReceiptingData.Statics.gboolFromDosTrio == true || modCashReceiptingData.Statics.bolFromWindowsCR)
			{
				if (boolPrinted)
				{
					modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
					modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
				}
			}
		}

		
	}
}
