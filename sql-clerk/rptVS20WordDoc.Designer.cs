﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptVS20WordDoc.
	/// </summary>
	partial class rptVS20WordDoc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptVS20WordDoc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.RichEdit1 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeceased = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.FileNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmended = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoleParent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeceased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FileNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoleParent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.RichEdit1,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12,
				this.Field13,
				this.txtDeceased,
				this.FileNumber,
				this.txtAmended,
				this.txtSoleParent,
				this.SubReport1
			});
			this.Detail.Height = 8.760417F;
			this.Detail.Name = "Detail";
			// 
			// RichEdit1
			// 
			this.RichEdit1.CanGrow = false;
			//this.RichEdit1.Font = new System.Drawing.Font("Arial", 10F);
			this.RichEdit1.Height = 6.6875F;
			this.RichEdit1.Left = 0.5277778F;
			this.RichEdit1.Name = "RichEdit1";
			this.RichEdit1.RTF = resources.GetString("RichEdit1.RTF");
			this.RichEdit1.Top = 0F;
			this.RichEdit1.Visible = false;
			this.RichEdit1.Width = 6.375F;
			// 
			// Field1
			// 
			this.Field1.CanGrow = false;
			this.Field1.Height = 0.3125F;
			this.Field1.Left = 1F;
			this.Field1.Name = "Field1";
			this.Field1.Text = "Name of Child";
			this.Field1.Top = 2.201389F;
			this.Field1.Width = 3.375F;
			// 
			// Field2
			// 
			this.Field2.CanGrow = false;
			this.Field2.Height = 0.3125F;
			this.Field2.Left = 4.5F;
			this.Field2.Name = "Field2";
			this.Field2.Text = null;
			this.Field2.Top = 2.201389F;
			this.Field2.Width = 2F;
			// 
			// Field3
			// 
			this.Field3.CanGrow = false;
			this.Field3.Height = 0.4375F;
			this.Field3.Left = 1F;
			this.Field3.Name = "Field3";
			this.Field3.Text = "Sex";
			this.Field3.Top = 2.708333F;
			this.Field3.Width = 0.9375F;
			// 
			// Field4
			// 
			this.Field4.CanGrow = false;
			this.Field4.Height = 0.4375F;
			this.Field4.Left = 4.5F;
			this.Field4.Name = "Field4";
			this.Field4.Text = null;
			this.Field4.Top = 2.708333F;
			this.Field4.Width = 2.3125F;
			// 
			// Field5
			// 
			this.Field5.CanGrow = false;
			this.Field5.Height = 0.4375F;
			this.Field5.Left = 1F;
			this.Field5.Name = "Field5";
			this.Field5.Text = "Attendant\'s Name";
			this.Field5.Top = 3.4375F;
			this.Field5.Width = 2.5F;
			// 
			// Field6
			// 
			this.Field6.CanGrow = false;
			this.Field6.Height = 0.5F;
			this.Field6.Left = 3.5625F;
			this.Field6.Name = "Field6";
			this.Field6.Text = "Attendant\'s Title";
			this.Field6.Top = 3.4375F;
			this.Field6.Width = 0.875F;
			// 
			// Field7
			// 
			this.Field7.CanGrow = false;
			this.Field7.Height = 0.625F;
			this.Field7.Left = 4.5F;
			this.Field7.Name = "Field7";
			this.Field7.Text = "Attendant\'s Address";
			this.Field7.Top = 3.4375F;
			this.Field7.Width = 2.1875F;
			// 
			// Field8
			// 
			this.Field8.CanGrow = false;
			this.Field8.Height = 0.4375F;
			this.Field8.Left = 1F;
			this.Field8.Name = "Field8";
			this.Field8.Text = "Mother\'s Maiden Name";
			this.Field8.Top = 4.375F;
			this.Field8.Width = 2.5625F;
			// 
			// Field9
			// 
			this.Field9.CanGrow = false;
			this.Field9.Height = 0.5F;
			this.Field9.Left = 4.5F;
			this.Field9.Name = "Field9";
			this.Field9.Text = "Mother\'s residence";
			this.Field9.Top = 4.375F;
			this.Field9.Width = 2.375F;
			// 
			// Field10
			// 
			this.Field10.CanGrow = false;
			this.Field10.Height = 0.5F;
			this.Field10.Left = 1F;
			this.Field10.Name = "Field10";
			this.Field10.Text = "Father\'s Name";
			this.Field10.Top = 5.3125F;
			this.Field10.Width = 2.5625F;
			// 
			// Field11
			// 
			this.Field11.CanGrow = false;
			this.Field11.Height = 0.5625F;
			this.Field11.Left = 1F;
			this.Field11.Name = "Field11";
			this.Field11.Text = "Clerk of Record";
			this.Field11.Top = 6.25F;
			this.Field11.Width = 2.5F;
			// 
			// Field12
			// 
			this.Field12.CanGrow = false;
			this.Field12.Height = 0.5F;
			this.Field12.Left = 3.6875F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "text-align: center";
			this.Field12.Text = null;
			this.Field12.Top = 6.25F;
			this.Field12.Width = 1.5F;
			// 
			// Field13
			// 
			this.Field13.CanGrow = false;
			this.Field13.Height = 0.5F;
			this.Field13.Left = 5.3125F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "text-align: left";
			this.Field13.Text = "Date of Filing";
			this.Field13.Top = 6.25F;
			this.Field13.Width = 1.8125F;
			// 
			// txtDeceased
			// 
			this.txtDeceased.Height = 0.1875F;
			this.txtDeceased.Left = 1.9375F;
			this.txtDeceased.Name = "txtDeceased";
			this.txtDeceased.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold";
			this.txtDeceased.Tag = "DECEASED";
			this.txtDeceased.Text = "Deceased";
			this.txtDeceased.Top = 1.1875F;
			this.txtDeceased.Width = 2.5F;
			// 
			// FileNumber
			// 
			this.FileNumber.CanGrow = false;
			this.FileNumber.DataField = "FileNumber";
			this.FileNumber.Height = 0.1875F;
			this.FileNumber.Left = 5F;
			this.FileNumber.Name = "FileNumber";
			this.FileNumber.OutputFormat = resources.GetString("FileNumber.OutputFormat");
			this.FileNumber.Style = "font-family: \'Tahoma\'";
			this.FileNumber.Text = "FileNumber";
			this.FileNumber.Top = 1.25F;
			this.FileNumber.Width = 1F;
			// 
			// txtAmended
			// 
			this.txtAmended.CanGrow = false;
			this.txtAmended.Height = 0.625F;
			this.txtAmended.Left = 0.125F;
			this.txtAmended.Name = "txtAmended";
			this.txtAmended.Style = "font-size: 8pt";
			this.txtAmended.Text = null;
			this.txtAmended.Top = 7.375F;
			this.txtAmended.Width = 6.8125F;
			// 
			// txtSoleParent
			// 
			this.txtSoleParent.CanGrow = false;
			this.txtSoleParent.Height = 0.1875F;
			this.txtSoleParent.Left = 2.125F;
			this.txtSoleParent.Name = "txtSoleParent";
			this.txtSoleParent.Text = null;
			this.txtSoleParent.Top = 7.125F;
			this.txtSoleParent.Width = 3.5F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 8.5625F;
			this.SubReport1.Width = 7F;
			// 
			// rptVS20WordDoc
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.75F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.21875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeceased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FileNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoleParent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeceased;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox FileNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoleParent;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
	}
}
