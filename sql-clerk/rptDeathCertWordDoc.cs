//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDeathCertWordDoc.
	/// </summary>
	public partial class rptDeathCertWordDoc : BaseSectionReport
	{
		public rptDeathCertWordDoc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Death Certificate";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDeathCertWordDoc InstancePtr
		{
			get
			{
				return (rptDeathCertWordDoc)Sys.GetInstance(typeof(rptDeathCertWordDoc));
			}
		}

		protected rptDeathCertWordDoc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDeathCertWordDoc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private clsDRWrapper rsData = new clsDRWrapper();
		private bool boolPrintTest;
		private double dblLineAdjust;

		public void Init(bool boolTestPrint, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			if (boolPrintTest)
			{
				dblLineAdjust = dblAdjust;
			}
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_Error(int Number, String Description, int Scode, string Source, string HelpFile, int HelpContext)
		{
			MessageBox.Show("Error Number " + FCConvert.ToString(Number) + " " + Description + "\r\n" + "Source: " + FCConvert.ToString(Scode) + " " + Source, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolPrinted = true;
				// intPrinted = Me.Printer.DeviceCopies
				//intPrinted = this.Document.Printer.Copies;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In PrintProgress", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblLaserLineAdjustment4 = 0;
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			//Application.DoEvents();
			if (!boolPrintTest)
			{
				dblLaserLineAdjustment4 = Conversion.Val(modRegistry.GetRegistryKey("DeathAdjustment", "CK"));
				rsData.OpenRecordset("Select * from Amendments where Type = 'DEATHS' and AmendmentID = '" + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum) + "'", modGNBas.DEFAULTCLERKDATABASE);
			}
			else
			{
				dblLaserLineAdjustment4 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment4) / 1440F;
			}
			SubReport1.Report = new rptSafetyPaperAttest();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			int lngNumCopies;
			double dblTemp;
			if (modCashReceiptingData.Statics.gboolFromDosTrio == true || modCashReceiptingData.Statics.bolFromWindowsCR)
			{
				if (boolPrinted)
				{
					modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
					modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngID;
			double dblRatio;
			/*? On Error Resume Next  */
			//clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			string strMuni = "";
			string strAttest = "";
			string strDate = "";
			frmPrintVitalRec.InstancePtr.Hide();
			if (!boolPrintTest)
			{
				frmDeaths.InstancePtr.Hide();
				txtFullName.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("LastName") + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("Designation");
				if (Information.IsDate(modGNBas.Statics.rsDeathCertificate.Get_Fields("DateOfDeath")))
				{
					txtDateOfDeath.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields("DateofDeath"), "MM/dd/yyyy");
				}
				else
				{
					txtDateOfDeath.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateOfDeathDescription");
				}
				txtPlaceOfDeath.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CityorTownofDeath");
				txtDateofBirth.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields("DateofBirth");
				if (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("socialsecuritynumber")).Length <= 9)
				{
					txtSSN.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("SocialSecurityNumber"), "000-00-0000");
				}
				else
				{
					strTemp = FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("Socialsecuritynumber"));
					strTemp = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Mid(strTemp, 6);
					txtSSN.Text = strTemp;
				}
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("sex"))) == "M")
				{
					txtSex.Text = "Male";
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("sex"))) == "F")
				{
					txtSex.Text = "Female";
				}
				else
				{
					txtSex.Text = string.Empty;
				}
				// Field6 = rsDeathCertificate.Fields("sex")
				txtFathersName.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherFirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherMiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherMiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherLastName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherDesignation")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherDesignation"));
				txtMothersMaidenName.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherFirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherMiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherMiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherLastName");
				txtNameOfInformant.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("InformantName");
				txtAddress.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("InformantMailingAddress");
				// state added this to form 1/29/2004
				txtStreetAndNumber.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ResidenceStreet");
				txtCityTown.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("residencecityortown");
				// Call clsTemp.OpenRecordset("select * from defaulttowns where id = " & Val(rsDeathCertificate.Fields("residencecityortown")), "TWCK0000.vb1")
				// If Not clsTemp.EndOfFile Then
				// txtDecedentCity.Text = clsTemp.Fields("name")
				// Else
				// txtDecedentCity.Text = ""
				// End If
				if (frmPrintVitalRec.InstancePtr.chkPrint.CheckState == CheckState.Checked)
				{
					txtCause1.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseA");
					txtCause2.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseB");
					txtCause3.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseC");
					// fCauseD = rsDeathCertificate.Fields("CauseD")
				}
				else
				{
					txtCause1.Text = "XXXXXXXXXXXXXXXXXXXX";
					txtCause2.Text = "XXXXXXXXXXXXXXXXXXXX";
					txtCause3.Text = "XXXXXXXXXXXXXXXXXXXX";
					// fCauseD = "XXXXXXXXXXXXXXXXXXXX"
				}
				txtPhysician.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("NameofPhysician");
				// Field23 = rsDeathCertificate.Fields("NameofAttendingPhysician")
				txtDatePhysicianSigned.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateSigned"), "MM/dd/yyyy");
				txtCemetery.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceofDisposition")));
				txtCemeteryCity.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceOfDispositionCity");
				txtCemeteryState.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceOfDispositionState");
				txtClerk.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ClerkofRecordName");
				txtClerkCity.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ClerkofRecordCity");
				txtClerkState.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateOriginalFiling");
				strDate = Strings.Format(DateTime.Now, "MMMM dd, yyyy");
				// If ClerkDefaults.boolDontPrintAttested Then
				// Field21 = ""
				// Else
				strAttest = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				// End If
				if (Conversion.Val(modGNBas.Statics.rsDeathCertificate.Get_Fields_Int32("towncode")) == 0)
				{
					strMuni = fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase);
				}
				else
				{
					strMuni = fecherFoundation.Strings.StrConv(modRegionalTown.GetTownKeyName_2(modGNBas.Statics.rsDeathCertificate.Get_Fields_Int32("towncode")), VbStrConv.ProperCase);
				}
				txtCertNumber.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FileNumber");
				// txtAmended = GetAmendments(rsData2)
			}
			else
			{
				// Field1.Text = "Name"
				// Field2.Text = "00/00/0000"
				// Field3.Text = "city"
				// Field4.Text = "00/00/0000"
				// Field5.Text = "000-00-0000"
				// Field6.Text = "Male"
				// Field7.Text = "Father's Name"
				// Field8.Text = "Mother's Name"
				// Field9.Text = "Informant"
				// Field10.Text = "Address"
				// txtDecedentStreet.Text = "Street"
				// txtDecedentCity.Text = "Residence"
				// fCauseA.Text = "XXXXXXXXXXXXXXXXXXXX"
				// fCauseB.Text = "XXXXXXXXXXXXXXXXXXXX"
				// fCauseC.Text = "XXXXXXXXXXXXXXXXXXXX"
				// fCauseD.Text = "XXXXXXXXXXXXXXXXXXXX"
				// Field12.Text = "Physician"
				// Field13.Text = "00/00/0000"
				// Field14.Text = "Place"
				// Field15.Text = "City"
				// Field16.Text = "State"
				// Field17.Text = "Clerk Name"
				// Field18.Text = "City"
				// Field19.Text = "00/00/0000"
				strDate = Strings.Format(DateTime.Now, "MMMM dd, yyyy");
				strMuni = fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase);
				strAttest = "Attested By";
			}
			SubReport1.Report.UserData = strMuni + "|" + strAttest + "|" + strDate;
		}

		
	}
}
