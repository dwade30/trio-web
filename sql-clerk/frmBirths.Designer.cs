//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmBirths.
	/// </summary>
	partial class frmBirths
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLabels;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lbltown;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblBirthCertNum;
		public fecherFoundation.FCCheckBox chkCourtDetermination;
		public fecherFoundation.FCCheckBox chkAOP;
		public fecherFoundation.FCCheckBox chkMarriageOnFile;
		public fecherFoundation.FCCheckBox chkDeceased;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtCounty;
		public fecherFoundation.FCTextBox txtTown;
		public fecherFoundation.FCTextBox txtChildDOB;
		public fecherFoundation.FCTextBox txtChildBirthPlace;
		public fecherFoundation.FCTextBox txtChildDesignation;
		public fecherFoundation.FCTextBox txtChildFirstName;
		public fecherFoundation.FCTextBox txtChildLastName;
		public fecherFoundation.FCTextBox txtChildMI;
		public fecherFoundation.FCTextBox txtChildSex;
		public fecherFoundation.FCTextBox txtTime;
		public fecherFoundation.FCTextBox txtFacilityName;
		public fecherFoundation.FCButton cmdFindClerk;
		public Global.T2KDateBox txtActualDate;
		public fecherFoundation.FCLabel lblLabels_42;
		public fecherFoundation.FCLabel lblLabels_10;
		public fecherFoundation.FCLabel lblLabels_15;
		public fecherFoundation.FCLabel lblLabels_16;
		public fecherFoundation.FCLabel lblLabels_17;
		public fecherFoundation.FCLabel lblLabels_25;
		public fecherFoundation.FCLabel lblLabels_6;
		public fecherFoundation.FCLabel lblLabels_8;
		public fecherFoundation.FCLabel lblLabels_34;
		public fecherFoundation.FCLabel lblLabels_35;
		public fecherFoundation.FCLabel lblLabels_36;
		public fecherFoundation.FCLabel lblLabels_37;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCTextBox txtAttendantMI;
		public fecherFoundation.FCTextBox txtAttendantLastName;
		public fecherFoundation.FCTextBox txtAttendantFirstName;
		public fecherFoundation.FCTextBox txtAttendantTitle;
		public fecherFoundation.FCTextBox txtAttendantAddress;
		public fecherFoundation.FCLabel lblLabels_4;
		public fecherFoundation.FCLabel lblLabels_3;
		public fecherFoundation.FCLabel lblLabels_2;
		public fecherFoundation.FCLabel lblLabels_1;
		public fecherFoundation.FCLabel lblLabels_0;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtMotherDOB;
		public fecherFoundation.FCTextBox txtMothersTown;
		public fecherFoundation.FCTextBox txtMotherCounty;
		public fecherFoundation.FCTextBox txtMotherState;
		public fecherFoundation.FCTextBox txtYears;
		public fecherFoundation.FCTextBox txtMothersAddress;
		public fecherFoundation.FCTextBox txtMothersZipCode;
		public fecherFoundation.FCTextBox txtMotherFirstName;
		public fecherFoundation.FCTextBox txtMotherLastName;
		public fecherFoundation.FCTextBox txtMotherMI;
		public fecherFoundation.FCTextBox txtMotherMaidenName;
		public fecherFoundation.FCTextBox txtMothersBirthPlace;
		public fecherFoundation.FCLabel lblLabels_45;
		public fecherFoundation.FCLabel lblLabels_44;
		public fecherFoundation.FCLabel lblLabels_43;
		public fecherFoundation.FCLabel lblLabels_18;
		public fecherFoundation.FCLabel lblLabels_19;
		public fecherFoundation.FCLabel lblLabels_20;
		public fecherFoundation.FCLabel lblLabels_28;
		public fecherFoundation.FCLabel lblLabels_29;
		public fecherFoundation.FCLabel lblLabels_24;
		public fecherFoundation.FCLabel lblLabels_38;
		public fecherFoundation.FCLabel lblLabels_39;
		public fecherFoundation.FCLabel lblLabels_40;
		public fecherFoundation.FCFrame fraFather;
		public fecherFoundation.FCTextBox txtFathersDOB;
		public fecherFoundation.FCTextBox txtFatherDesignation;
		public fecherFoundation.FCTextBox txtFatherFirstName;
		public fecherFoundation.FCTextBox txtFatherLastName;
		public fecherFoundation.FCTextBox txtFatherMI;
		public fecherFoundation.FCTextBox txtFathersBirthPlace;
		public fecherFoundation.FCLabel lblLabels_11;
		public fecherFoundation.FCLabel lblLabels_12;
		public fecherFoundation.FCLabel lblLabels_13;
		public fecherFoundation.FCLabel lblLabels_14;
		public fecherFoundation.FCLabel lblLabels_30;
		public fecherFoundation.FCLabel lblLabels_41;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtRegTown;
		public fecherFoundation.FCTextBox mebRegistrarDateFiling;
		public fecherFoundation.FCButton cmdRegistrar;
		public fecherFoundation.FCTextBox txtRegistrarName;
		public fecherFoundation.FCLabel lbltown_42;
		public fecherFoundation.FCLabel lblLabels_48;
		public fecherFoundation.FCLabel lblLabels_47;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdFindMunicipalClerk;
		public fecherFoundation.FCTextBox txtCourt;
		public fecherFoundation.FCTextBox txtAmended;
		public fecherFoundation.FCTextBox txtStateRegistrar;
		public fecherFoundation.FCTextBox txtTownOf;
		public Global.T2KDateBox txtAttestDate;
		public Global.T2KDateBox mebAmendedDate;
		public fecherFoundation.FCLabel lblLabels_33;
		public fecherFoundation.FCLabel lblLabels_32;
		public fecherFoundation.FCLabel lblLabels_31;
		public fecherFoundation.FCLabel lblLabels_27;
		public fecherFoundation.FCLabel lblLabels_26;
		public fecherFoundation.FCLabel lblLabels_5;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCTextBox txtClerkCityTown;
		public fecherFoundation.FCTextBox txtClerkFirstName;
		public fecherFoundation.FCTextBox txtClerkLastName;
		public fecherFoundation.FCTextBox txtClerkMI;
		public fecherFoundation.FCButton cmdFind;
		public Global.T2KDateBox txtClerkDateOfFiling;
		public fecherFoundation.FCLabel lblLabels_7;
		public fecherFoundation.FCLabel lblLabels_9;
		public fecherFoundation.FCLabel lblLabels_21;
		public fecherFoundation.FCLabel lblLabels_22;
		public fecherFoundation.FCLabel lblLabels_23;
		public fecherFoundation.FCCheckBox chkSoleParent;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCGrid VSFlexGrid1;
		public fecherFoundation.FCCheckBox chkRequired;
		public fecherFoundation.FCTextBox txtFileNumber;
		public fecherFoundation.FCCheckBox chkIllegitBirth;
		public FCCommonDialog cdlColor;
		public fecherFoundation.FCTextBox txtBirthCertNum;
		public Global.T2KDateBox mebDateDeceased;
		public FCGrid gridTownCode;
		public fecherFoundation.FCPictureBox imgDocuments;
		public fecherFoundation.FCPictureBox ImgComment;
		public fecherFoundation.FCLabel lblBKey;
		public fecherFoundation.FCLabel lblBirthID;
		public fecherFoundation.FCLabel lblRecord;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessAddNewRecord;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDeleteRecord;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSetDefaultTowns;
		public fecherFoundation.FCToolStripMenuItem mnuRegistrarNames;
		public fecherFoundation.FCToolStripMenuItem mnuDefaultCounties;
		public fecherFoundation.FCToolStripMenuItem mnuDefaultStates;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocuments;
		public fecherFoundation.FCToolStripMenuItem ProcessSeparator1;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExitNoValidation;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		public fecherFoundation.FCLabel lblDeceased;
		public fecherFoundation.FCLabel lblBirthCertNum_28;
		public fecherFoundation.FCLabel lblFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBirths));
            this.chkCourtDetermination = new fecherFoundation.FCCheckBox();
            this.chkAOP = new fecherFoundation.FCCheckBox();
            this.chkMarriageOnFile = new fecherFoundation.FCCheckBox();
            this.chkDeceased = new fecherFoundation.FCCheckBox();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtCounty = new fecherFoundation.FCTextBox();
            this.txtTown = new fecherFoundation.FCTextBox();
            this.txtChildDOB = new fecherFoundation.FCTextBox();
            this.txtChildBirthPlace = new fecherFoundation.FCTextBox();
            this.txtChildDesignation = new fecherFoundation.FCTextBox();
            this.txtChildFirstName = new fecherFoundation.FCTextBox();
            this.txtChildLastName = new fecherFoundation.FCTextBox();
            this.txtChildMI = new fecherFoundation.FCTextBox();
            this.txtChildSex = new fecherFoundation.FCTextBox();
            this.txtTime = new fecherFoundation.FCTextBox();
            this.txtFacilityName = new fecherFoundation.FCTextBox();
            this.cmdFindClerk = new fecherFoundation.FCButton();
            this.txtActualDate = new Global.T2KDateBox();
            this.lblLabels_42 = new fecherFoundation.FCLabel();
            this.lblLabels_10 = new fecherFoundation.FCLabel();
            this.lblLabels_15 = new fecherFoundation.FCLabel();
            this.lblLabels_16 = new fecherFoundation.FCLabel();
            this.lblLabels_17 = new fecherFoundation.FCLabel();
            this.lblLabels_25 = new fecherFoundation.FCLabel();
            this.lblLabels_6 = new fecherFoundation.FCLabel();
            this.lblLabels_8 = new fecherFoundation.FCLabel();
            this.lblLabels_34 = new fecherFoundation.FCLabel();
            this.lblLabels_35 = new fecherFoundation.FCLabel();
            this.lblLabels_36 = new fecherFoundation.FCLabel();
            this.lblLabels_37 = new fecherFoundation.FCLabel();
            this.Frame6 = new fecherFoundation.FCFrame();
            this.txtAttendantMI = new fecherFoundation.FCTextBox();
            this.txtAttendantLastName = new fecherFoundation.FCTextBox();
            this.txtAttendantFirstName = new fecherFoundation.FCTextBox();
            this.txtAttendantTitle = new fecherFoundation.FCTextBox();
            this.txtAttendantAddress = new fecherFoundation.FCTextBox();
            this.lblLabels_4 = new fecherFoundation.FCLabel();
            this.lblLabels_3 = new fecherFoundation.FCLabel();
            this.lblLabels_2 = new fecherFoundation.FCLabel();
            this.lblLabels_1 = new fecherFoundation.FCLabel();
            this.lblLabels_0 = new fecherFoundation.FCLabel();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtMotherDOB = new fecherFoundation.FCTextBox();
            this.txtMothersTown = new fecherFoundation.FCTextBox();
            this.txtMotherCounty = new fecherFoundation.FCTextBox();
            this.txtMotherState = new fecherFoundation.FCTextBox();
            this.txtYears = new fecherFoundation.FCTextBox();
            this.txtMothersAddress = new fecherFoundation.FCTextBox();
            this.txtMothersZipCode = new fecherFoundation.FCTextBox();
            this.txtMotherFirstName = new fecherFoundation.FCTextBox();
            this.txtMotherLastName = new fecherFoundation.FCTextBox();
            this.txtMotherMI = new fecherFoundation.FCTextBox();
            this.txtMotherMaidenName = new fecherFoundation.FCTextBox();
            this.txtMothersBirthPlace = new fecherFoundation.FCTextBox();
            this.lblLabels_45 = new fecherFoundation.FCLabel();
            this.lblLabels_44 = new fecherFoundation.FCLabel();
            this.lblLabels_43 = new fecherFoundation.FCLabel();
            this.lblLabels_18 = new fecherFoundation.FCLabel();
            this.lblLabels_19 = new fecherFoundation.FCLabel();
            this.lblLabels_20 = new fecherFoundation.FCLabel();
            this.lblLabels_28 = new fecherFoundation.FCLabel();
            this.lblLabels_29 = new fecherFoundation.FCLabel();
            this.lblLabels_24 = new fecherFoundation.FCLabel();
            this.lblLabels_38 = new fecherFoundation.FCLabel();
            this.lblLabels_39 = new fecherFoundation.FCLabel();
            this.lblLabels_40 = new fecherFoundation.FCLabel();
            this.fraFather = new fecherFoundation.FCFrame();
            this.txtFathersDOB = new fecherFoundation.FCTextBox();
            this.txtFatherDesignation = new fecherFoundation.FCTextBox();
            this.txtFatherFirstName = new fecherFoundation.FCTextBox();
            this.txtFatherLastName = new fecherFoundation.FCTextBox();
            this.txtFatherMI = new fecherFoundation.FCTextBox();
            this.txtFathersBirthPlace = new fecherFoundation.FCTextBox();
            this.lblLabels_11 = new fecherFoundation.FCLabel();
            this.lblLabels_12 = new fecherFoundation.FCLabel();
            this.lblLabels_13 = new fecherFoundation.FCLabel();
            this.lblLabels_14 = new fecherFoundation.FCLabel();
            this.lblLabels_30 = new fecherFoundation.FCLabel();
            this.lblLabels_41 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtRegTown = new fecherFoundation.FCTextBox();
            this.mebRegistrarDateFiling = new fecherFoundation.FCTextBox();
            this.cmdRegistrar = new fecherFoundation.FCButton();
            this.txtRegistrarName = new fecherFoundation.FCTextBox();
            this.lbltown_42 = new fecherFoundation.FCLabel();
            this.lblLabels_48 = new fecherFoundation.FCLabel();
            this.lblLabels_47 = new fecherFoundation.FCLabel();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmdFindMunicipalClerk = new fecherFoundation.FCButton();
            this.txtCourt = new fecherFoundation.FCTextBox();
            this.txtAmended = new fecherFoundation.FCTextBox();
            this.txtStateRegistrar = new fecherFoundation.FCTextBox();
            this.txtTownOf = new fecherFoundation.FCTextBox();
            this.txtAttestDate = new Global.T2KDateBox();
            this.mebAmendedDate = new Global.T2KDateBox();
            this.lblLabels_33 = new fecherFoundation.FCLabel();
            this.lblLabels_32 = new fecherFoundation.FCLabel();
            this.lblLabels_31 = new fecherFoundation.FCLabel();
            this.lblLabels_27 = new fecherFoundation.FCLabel();
            this.lblLabels_26 = new fecherFoundation.FCLabel();
            this.lblLabels_5 = new fecherFoundation.FCLabel();
            this.Frame9 = new fecherFoundation.FCFrame();
            this.txtClerkCityTown = new fecherFoundation.FCTextBox();
            this.txtClerkFirstName = new fecherFoundation.FCTextBox();
            this.txtClerkLastName = new fecherFoundation.FCTextBox();
            this.txtClerkMI = new fecherFoundation.FCTextBox();
            this.cmdFind = new fecherFoundation.FCButton();
            this.txtClerkDateOfFiling = new Global.T2KDateBox();
            this.lblLabels_7 = new fecherFoundation.FCLabel();
            this.lblLabels_21 = new fecherFoundation.FCLabel();
            this.lblLabels_22 = new fecherFoundation.FCLabel();
            this.lblLabels_23 = new fecherFoundation.FCLabel();
            this.lblLabels_9 = new fecherFoundation.FCLabel();
            this.chkSoleParent = new fecherFoundation.FCCheckBox();
            this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
            this.VSFlexGrid1 = new fecherFoundation.FCGrid();
            this.chkRequired = new fecherFoundation.FCCheckBox();
            this.txtFileNumber = new fecherFoundation.FCTextBox();
            this.chkIllegitBirth = new fecherFoundation.FCCheckBox();
            this.cdlColor = new fecherFoundation.FCCommonDialog();
            this.txtBirthCertNum = new fecherFoundation.FCTextBox();
            this.mebDateDeceased = new Global.T2KDateBox();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.imgDocuments = new fecherFoundation.FCPictureBox();
            this.ImgComment = new fecherFoundation.FCPictureBox();
            this.lblBKey = new fecherFoundation.FCLabel();
            this.lblBirthID = new fecherFoundation.FCLabel();
            this.lblRecord = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuSetDefaultTowns = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRegistrarNames = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDefaultCounties = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDefaultStates = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewDocuments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExitNoValidation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessAddNewRecord = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessDeleteRecord = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
            this.ProcessSeparator1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.lblDeceased = new fecherFoundation.FCLabel();
            this.lblBirthCertNum_28 = new fecherFoundation.FCLabel();
            this.lblFile = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdComment = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCourtDetermination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAOP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFindClerk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFather)).BeginInit();
            this.fraFather.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRegistrar)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFindMunicipalClerk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttestDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAmendedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
            this.Frame9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClerkDateOfFiling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSoleParent)).BeginInit();
            this.SSTab1_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VSFlexGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIllegitBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDateDeceased)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkCourtDetermination);
            this.ClientArea.Controls.Add(this.chkAOP);
            this.ClientArea.Controls.Add(this.chkMarriageOnFile);
            this.ClientArea.Controls.Add(this.chkDeceased);
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.chkRequired);
            this.ClientArea.Controls.Add(this.txtFileNumber);
            this.ClientArea.Controls.Add(this.chkIllegitBirth);
            this.ClientArea.Controls.Add(this.txtBirthCertNum);
            this.ClientArea.Controls.Add(this.mebDateDeceased);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.imgDocuments);
            this.ClientArea.Controls.Add(this.ImgComment);
            this.ClientArea.Controls.Add(this.lblBKey);
            this.ClientArea.Controls.Add(this.lblBirthID);
            this.ClientArea.Controls.Add(this.lblRecord);
            this.ClientArea.Controls.Add(this.lblDeceased);
            this.ClientArea.Controls.Add(this.lblBirthCertNum_28);
            this.ClientArea.Controls.Add(this.lblFile);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdComment);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(127, 30);
            this.HeaderText.Text = "Live Births";
            // 
            // chkCourtDetermination
            // 
            this.chkCourtDetermination.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.chkCourtDetermination.Location = new System.Drawing.Point(330, 84);
            this.chkCourtDetermination.Name = "chkCourtDetermination";
            this.chkCourtDetermination.Size = new System.Drawing.Size(233, 24);
            this.chkCourtDetermination.TabIndex = 3;
            this.chkCourtDetermination.Text = "Court Determination of Paternity";
            // 
            // chkAOP
            // 
            this.chkAOP.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.chkAOP.Location = new System.Drawing.Point(330, 57);
            this.chkAOP.Name = "chkAOP";
            this.chkAOP.Size = new System.Drawing.Size(257, 24);
            this.chkAOP.TabIndex = 2;
            this.chkAOP.Text = "Have Acknowledgement of Paternity";
            // 
            // chkMarriageOnFile
            // 
            this.chkMarriageOnFile.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.chkMarriageOnFile.Location = new System.Drawing.Point(330, 165);
            this.chkMarriageOnFile.Name = "chkMarriageOnFile";
            this.chkMarriageOnFile.Size = new System.Drawing.Size(170, 24);
            this.chkMarriageOnFile.TabIndex = 7;
            this.chkMarriageOnFile.Text = "Marriage record on file";
            // 
            // chkDeceased
            // 
            this.chkDeceased.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.chkDeceased.Location = new System.Drawing.Point(330, 138);
            this.chkDeceased.Name = "chkDeceased";
            this.chkDeceased.Size = new System.Drawing.Size(237, 24);
            this.chkDeceased.TabIndex = 5;
            this.chkDeceased.Text = "Death record is filed in this office";
            this.chkDeceased.CheckedChanged += new System.EventHandler(this.chkDeceased_CheckedChanged);
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Controls.Add(this.SSTab1_Page4);
            this.SSTab1.Location = new System.Drawing.Point(30, 210);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
            this.SSTab1.Size = new System.Drawing.Size(1008, 664);
            this.SSTab1.TabIndex = 69;
            this.SSTab1.TabStop = false;
            this.SSTab1.Text = "Child / Certifier Info";
            this.SSTab1.SelectedIndexChanged += new System.EventHandler(this.SSTab1_SelectedIndexChanged);
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.Frame1);
            this.SSTab1_Page1.Controls.Add(this.Frame6);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Size = new System.Drawing.Size(1006, 628);
            this.SSTab1_Page1.Text = "Child / Certifier Info";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtCounty);
            this.Frame1.Controls.Add(this.txtTown);
            this.Frame1.Controls.Add(this.txtChildDOB);
            this.Frame1.Controls.Add(this.txtChildBirthPlace);
            this.Frame1.Controls.Add(this.txtChildDesignation);
            this.Frame1.Controls.Add(this.txtChildFirstName);
            this.Frame1.Controls.Add(this.txtChildLastName);
            this.Frame1.Controls.Add(this.txtChildMI);
            this.Frame1.Controls.Add(this.txtChildSex);
            this.Frame1.Controls.Add(this.txtTime);
            this.Frame1.Controls.Add(this.txtFacilityName);
            this.Frame1.Controls.Add(this.cmdFindClerk);
            this.Frame1.Controls.Add(this.txtActualDate);
            this.Frame1.Controls.Add(this.lblLabels_42);
            this.Frame1.Controls.Add(this.lblLabels_10);
            this.Frame1.Controls.Add(this.lblLabels_15);
            this.Frame1.Controls.Add(this.lblLabels_16);
            this.Frame1.Controls.Add(this.lblLabels_17);
            this.Frame1.Controls.Add(this.lblLabels_25);
            this.Frame1.Controls.Add(this.lblLabels_6);
            this.Frame1.Controls.Add(this.lblLabels_8);
            this.Frame1.Controls.Add(this.lblLabels_34);
            this.Frame1.Controls.Add(this.lblLabels_35);
            this.Frame1.Controls.Add(this.lblLabels_36);
            this.Frame1.Controls.Add(this.lblLabels_37);
            this.Frame1.Location = new System.Drawing.Point(20, 20);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(966, 320);
            this.Frame1.TabIndex = 70;
            this.Frame1.Text = "Child Information";
            // 
            // txtCounty
            // 
            this.txtCounty.BackColor = System.Drawing.SystemColors.Window;
            this.txtCounty.Location = new System.Drawing.Point(380, 120);
            this.txtCounty.MaxLength = 20;
            this.txtCounty.Name = "txtCounty";
            this.txtCounty.Size = new System.Drawing.Size(240, 40);
            this.txtCounty.TabIndex = 15;
            this.txtCounty.Text = " ";
            this.txtCounty.DoubleClick += new System.EventHandler(this.txtCounty_DoubleClick);
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(640, 120);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(306, 40);
            this.txtTown.TabIndex = 16;
            this.txtTown.Tag = "Required";
            this.txtTown.DoubleClick += new System.EventHandler(this.txtTown_DoubleClick);
            this.txtTown.Validating += new System.ComponentModel.CancelEventHandler(this.txtTown_Validating);
            // 
            // txtChildDOB
            // 
            this.txtChildDOB.Location = new System.Drawing.Point(20, 120);
            this.txtChildDOB.Name = "txtChildDOB";
            this.txtChildDOB.Size = new System.Drawing.Size(160, 40);
            this.txtChildDOB.TabIndex = 13;
            this.txtChildDOB.Tag = "Required";
            this.txtChildDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtChildDOB_Validating);
            // 
            // txtChildBirthPlace
            // 
            this.txtChildBirthPlace.Location = new System.Drawing.Point(20, 260);
            this.txtChildBirthPlace.MaxLength = 255;
            this.txtChildBirthPlace.Name = "txtChildBirthPlace";
            this.txtChildBirthPlace.Size = new System.Drawing.Size(453, 40);
            this.txtChildBirthPlace.TabIndex = 18;
            this.txtChildBirthPlace.Tag = "Required";
            this.txtChildBirthPlace.Text = " ";
            this.txtChildBirthPlace.DoubleClick += new System.EventHandler(this.txtChildBirthPlace_DoubleClick);
            // 
            // txtChildDesignation
            // 
            this.txtChildDesignation.BackColor = System.Drawing.SystemColors.Window;
            this.txtChildDesignation.Location = new System.Drawing.Point(706, 50);
            this.txtChildDesignation.Name = "txtChildDesignation";
            this.txtChildDesignation.Size = new System.Drawing.Size(140, 40);
            this.txtChildDesignation.TabIndex = 11;
            // 
            // txtChildFirstName
            // 
            this.txtChildFirstName.Location = new System.Drawing.Point(20, 50);
            this.txtChildFirstName.Name = "txtChildFirstName";
            this.txtChildFirstName.Size = new System.Drawing.Size(243, 40);
            this.txtChildFirstName.TabIndex = 8;
            this.txtChildFirstName.Tag = "Required";
            // 
            // txtChildLastName
            // 
            this.txtChildLastName.Location = new System.Drawing.Point(443, 50);
            this.txtChildLastName.Name = "txtChildLastName";
            this.txtChildLastName.Size = new System.Drawing.Size(243, 40);
            this.txtChildLastName.TabIndex = 10;
            this.txtChildLastName.Tag = "Required";
            // 
            // txtChildMI
            // 
            this.txtChildMI.BackColor = System.Drawing.SystemColors.Window;
            this.txtChildMI.Location = new System.Drawing.Point(283, 50);
            this.txtChildMI.MaxLength = 50;
            this.txtChildMI.Name = "txtChildMI";
            this.txtChildMI.Size = new System.Drawing.Size(140, 40);
            this.txtChildMI.TabIndex = 9;
            this.txtChildMI.Text = " ";
            // 
            // txtChildSex
            // 
            this.txtChildSex.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtChildSex.Location = new System.Drawing.Point(866, 50);
            this.txtChildSex.MaxLength = 1;
            this.txtChildSex.Name = "txtChildSex";
            this.txtChildSex.Size = new System.Drawing.Size(80, 40);
            this.txtChildSex.TabIndex = 12;
            this.txtChildSex.Tag = "Required";
            this.txtChildSex.Validating += new System.ComponentModel.CancelEventHandler(this.txtChildSex_Validating);
            // 
            // txtTime
            // 
            this.txtTime.BackColor = System.Drawing.SystemColors.Window;
            this.txtTime.Location = new System.Drawing.Point(200, 120);
            this.txtTime.MaxLength = 20;
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(160, 40);
            this.txtTime.TabIndex = 14;
            this.txtTime.Text = " ";
            // 
            // txtFacilityName
            // 
            this.txtFacilityName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFacilityName.Location = new System.Drawing.Point(493, 260);
            this.txtFacilityName.Name = "txtFacilityName";
            this.txtFacilityName.Size = new System.Drawing.Size(453, 40);
            this.txtFacilityName.TabIndex = 20;
            this.txtFacilityName.Text = " ";
            // 
            // cmdFindClerk
            // 
            this.cmdFindClerk.AppearanceKey = "actionButton";
            this.cmdFindClerk.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindClerk.Image")));
            this.cmdFindClerk.Location = new System.Drawing.Point(491, 260);
            this.cmdFindClerk.Name = "cmdFindClerk";
            this.cmdFindClerk.Size = new System.Drawing.Size(40, 40);
            this.cmdFindClerk.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.cmdFindClerk, "Find Facililty Name");
            this.cmdFindClerk.Visible = false;
            this.cmdFindClerk.Click += new System.EventHandler(this.cmdFindClerk_Click);
            // 
            // txtActualDate
            // 
            this.txtActualDate.Location = new System.Drawing.Point(20, 190);
            this.txtActualDate.MaxLength = 10;
            this.txtActualDate.Name = "txtActualDate";
            this.txtActualDate.Size = new System.Drawing.Size(115, 22);
            this.txtActualDate.TabIndex = 17;
            this.txtActualDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtActualDate_Validate);
            // 
            // lblLabels_42
            // 
            this.lblLabels_42.AutoSize = true;
            this.lblLabels_42.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_42.Location = new System.Drawing.Point(20, 170);
            this.lblLabels_42.Name = "lblLabels_42";
            this.lblLabels_42.Size = new System.Drawing.Size(96, 17);
            this.lblLabels_42.TabIndex = 127;
            this.lblLabels_42.Text = "ACTUAL DOB";
            // 
            // lblLabels_10
            // 
            this.lblLabels_10.AutoSize = true;
            this.lblLabels_10.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_10.Location = new System.Drawing.Point(706, 30);
            this.lblLabels_10.Name = "lblLabels_10";
            this.lblLabels_10.Size = new System.Drawing.Size(100, 17);
            this.lblLabels_10.TabIndex = 81;
            this.lblLabels_10.Text = "JR. / SR. / ETC";
            // 
            // lblLabels_15
            // 
            this.lblLabels_15.AutoSize = true;
            this.lblLabels_15.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_15.Location = new System.Drawing.Point(20, 30);
            this.lblLabels_15.Name = "lblLabels_15";
            this.lblLabels_15.Size = new System.Drawing.Size(90, 17);
            this.lblLabels_15.TabIndex = 80;
            this.lblLabels_15.Text = "FIRST NAME";
            // 
            // lblLabels_16
            // 
            this.lblLabels_16.AutoSize = true;
            this.lblLabels_16.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_16.Location = new System.Drawing.Point(443, 30);
            this.lblLabels_16.Name = "lblLabels_16";
            this.lblLabels_16.Size = new System.Drawing.Size(86, 17);
            this.lblLabels_16.TabIndex = 79;
            this.lblLabels_16.Text = "LAST NAME";
            // 
            // lblLabels_17
            // 
            this.lblLabels_17.AutoSize = true;
            this.lblLabels_17.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_17.Location = new System.Drawing.Point(283, 30);
            this.lblLabels_17.Name = "lblLabels_17";
            this.lblLabels_17.Size = new System.Drawing.Size(102, 17);
            this.lblLabels_17.TabIndex = 78;
            this.lblLabels_17.Text = "MIDDLE NAME";
            // 
            // lblLabels_25
            // 
            this.lblLabels_25.AutoSize = true;
            this.lblLabels_25.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_25.Location = new System.Drawing.Point(866, 30);
            this.lblLabels_25.Name = "lblLabels_25";
            this.lblLabels_25.Size = new System.Drawing.Size(35, 17);
            this.lblLabels_25.TabIndex = 77;
            this.lblLabels_25.Text = "SEX";
            // 
            // lblLabels_6
            // 
            this.lblLabels_6.AutoSize = true;
            this.lblLabels_6.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_6.Location = new System.Drawing.Point(20, 240);
            this.lblLabels_6.Name = "lblLabels_6";
            this.lblLabels_6.Size = new System.Drawing.Size(94, 17);
            this.lblLabels_6.TabIndex = 76;
            this.lblLabels_6.Text = "BIRTHPLACE";
            // 
            // lblLabels_8
            // 
            this.lblLabels_8.AutoSize = true;
            this.lblLabels_8.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_8.Location = new System.Drawing.Point(20, 100);
            this.lblLabels_8.Name = "lblLabels_8";
            this.lblLabels_8.Size = new System.Drawing.Size(130, 17);
            this.lblLabels_8.TabIndex = 75;
            this.lblLabels_8.Text = "CERTIFICATE DOB";
            // 
            // lblLabels_34
            // 
            this.lblLabels_34.AutoSize = true;
            this.lblLabels_34.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_34.Location = new System.Drawing.Point(200, 100);
            this.lblLabels_34.Name = "lblLabels_34";
            this.lblLabels_34.Size = new System.Drawing.Size(107, 17);
            this.lblLabels_34.TabIndex = 74;
            this.lblLabels_34.Text = "TIME OF BIRTH";
            // 
            // lblLabels_35
            // 
            this.lblLabels_35.AutoSize = true;
            this.lblLabels_35.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_35.Location = new System.Drawing.Point(380, 100);
            this.lblLabels_35.Name = "lblLabels_35";
            this.lblLabels_35.Size = new System.Drawing.Size(131, 17);
            this.lblLabels_35.TabIndex = 73;
            this.lblLabels_35.Text = "COUNTY OF BIRTH";
            // 
            // lblLabels_36
            // 
            this.lblLabels_36.AutoSize = true;
            this.lblLabels_36.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_36.Location = new System.Drawing.Point(640, 100);
            this.lblLabels_36.Name = "lblLabels_36";
            this.lblLabels_36.Size = new System.Drawing.Size(174, 17);
            this.lblLabels_36.TabIndex = 72;
            this.lblLabels_36.Text = "CITY OR TOWN OF BIRTH";
            // 
            // lblLabels_37
            // 
            this.lblLabels_37.AutoSize = true;
            this.lblLabels_37.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_37.Location = new System.Drawing.Point(493, 240);
            this.lblLabels_37.Name = "lblLabels_37";
            this.lblLabels_37.Size = new System.Drawing.Size(111, 17);
            this.lblLabels_37.TabIndex = 71;
            this.lblLabels_37.Text = "FACILITY NAME";
            // 
            // Frame6
            // 
            this.Frame6.Controls.Add(this.txtAttendantMI);
            this.Frame6.Controls.Add(this.txtAttendantLastName);
            this.Frame6.Controls.Add(this.txtAttendantFirstName);
            this.Frame6.Controls.Add(this.txtAttendantTitle);
            this.Frame6.Controls.Add(this.txtAttendantAddress);
            this.Frame6.Controls.Add(this.lblLabels_4);
            this.Frame6.Controls.Add(this.lblLabels_3);
            this.Frame6.Controls.Add(this.lblLabels_2);
            this.Frame6.Controls.Add(this.lblLabels_1);
            this.Frame6.Controls.Add(this.lblLabels_0);
            this.Frame6.Location = new System.Drawing.Point(20, 360);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(966, 180);
            this.Frame6.TabIndex = 82;
            this.Frame6.Text = "Certifier / Attendant";
            // 
            // txtAttendantMI
            // 
            this.txtAttendantMI.BackColor = System.Drawing.SystemColors.Window;
            this.txtAttendantMI.Location = new System.Drawing.Point(260, 50);
            this.txtAttendantMI.MaxLength = 40;
            this.txtAttendantMI.Name = "txtAttendantMI";
            this.txtAttendantMI.Size = new System.Drawing.Size(120, 40);
            this.txtAttendantMI.TabIndex = 22;
            // 
            // txtAttendantLastName
            // 
            this.txtAttendantLastName.Location = new System.Drawing.Point(400, 50);
            this.txtAttendantLastName.Name = "txtAttendantLastName";
            this.txtAttendantLastName.Size = new System.Drawing.Size(220, 40);
            this.txtAttendantLastName.TabIndex = 23;
            this.txtAttendantLastName.Tag = "Required";
            // 
            // txtAttendantFirstName
            // 
            this.txtAttendantFirstName.Location = new System.Drawing.Point(20, 50);
            this.txtAttendantFirstName.Name = "txtAttendantFirstName";
            this.txtAttendantFirstName.Size = new System.Drawing.Size(220, 40);
            this.txtAttendantFirstName.TabIndex = 21;
            this.txtAttendantFirstName.Tag = "Required";
            this.txtAttendantFirstName.Leave += new System.EventHandler(this.txtAttendantFirstName_Leave);
            // 
            // txtAttendantTitle
            // 
            this.txtAttendantTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtAttendantTitle.Location = new System.Drawing.Point(20, 120);
            this.txtAttendantTitle.Name = "txtAttendantTitle";
            this.txtAttendantTitle.Size = new System.Drawing.Size(600, 40);
            this.txtAttendantTitle.TabIndex = 24;
            // 
            // txtAttendantAddress
            // 
            this.txtAttendantAddress.AcceptsReturn = true;
            this.txtAttendantAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAttendantAddress.Location = new System.Drawing.Point(640, 50);
            this.txtAttendantAddress.Multiline = true;
            this.txtAttendantAddress.Name = "txtAttendantAddress";
            this.txtAttendantAddress.Size = new System.Drawing.Size(306, 110);
            this.txtAttendantAddress.TabIndex = 25;
            this.txtAttendantAddress.Leave += new System.EventHandler(this.txtAttendantAddress_Leave);
            // 
            // lblLabels_4
            // 
            this.lblLabels_4.AutoSize = true;
            this.lblLabels_4.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_4.Location = new System.Drawing.Point(260, 30);
            this.lblLabels_4.Name = "lblLabels_4";
            this.lblLabels_4.Size = new System.Drawing.Size(59, 17);
            this.lblLabels_4.TabIndex = 87;
            this.lblLabels_4.Text = "MIDDLE";
            // 
            // lblLabels_3
            // 
            this.lblLabels_3.AutoSize = true;
            this.lblLabels_3.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_3.Location = new System.Drawing.Point(400, 30);
            this.lblLabels_3.Name = "lblLabels_3";
            this.lblLabels_3.Size = new System.Drawing.Size(86, 17);
            this.lblLabels_3.TabIndex = 86;
            this.lblLabels_3.Text = "LAST NAME";
            // 
            // lblLabels_2
            // 
            this.lblLabels_2.AutoSize = true;
            this.lblLabels_2.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_2.Location = new System.Drawing.Point(20, 30);
            this.lblLabels_2.Name = "lblLabels_2";
            this.lblLabels_2.Size = new System.Drawing.Size(90, 17);
            this.lblLabels_2.TabIndex = 85;
            this.lblLabels_2.Text = "FIRST NAME";
            // 
            // lblLabels_1
            // 
            this.lblLabels_1.AutoSize = true;
            this.lblLabels_1.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_1.Location = new System.Drawing.Point(20, 100);
            this.lblLabels_1.Name = "lblLabels_1";
            this.lblLabels_1.Size = new System.Drawing.Size(45, 17);
            this.lblLabels_1.TabIndex = 84;
            this.lblLabels_1.Text = "TITLE";
            // 
            // lblLabels_0
            // 
            this.lblLabels_0.AutoSize = true;
            this.lblLabels_0.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_0.Location = new System.Drawing.Point(640, 30);
            this.lblLabels_0.Name = "lblLabels_0";
            this.lblLabels_0.Size = new System.Drawing.Size(171, 17);
            this.lblLabels_0.TabIndex = 83;
            this.lblLabels_0.Text = "ATTENDANT\'S ADDRESS";
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.Frame7);
            this.SSTab1_Page2.Controls.Add(this.fraFather);
            this.SSTab1_Page2.Controls.Add(this.Frame3);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Size = new System.Drawing.Size(1006, 628);
            this.SSTab1_Page2.Text = "Parent / Registrar   Info";
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.txtMotherDOB);
            this.Frame7.Controls.Add(this.txtMothersTown);
            this.Frame7.Controls.Add(this.txtMotherCounty);
            this.Frame7.Controls.Add(this.txtMotherState);
            this.Frame7.Controls.Add(this.txtYears);
            this.Frame7.Controls.Add(this.txtMothersAddress);
            this.Frame7.Controls.Add(this.txtMothersZipCode);
            this.Frame7.Controls.Add(this.txtMotherFirstName);
            this.Frame7.Controls.Add(this.txtMotherLastName);
            this.Frame7.Controls.Add(this.txtMotherMI);
            this.Frame7.Controls.Add(this.txtMotherMaidenName);
            this.Frame7.Controls.Add(this.txtMothersBirthPlace);
            this.Frame7.Controls.Add(this.lblLabels_45);
            this.Frame7.Controls.Add(this.lblLabels_44);
            this.Frame7.Controls.Add(this.lblLabels_43);
            this.Frame7.Controls.Add(this.lblLabels_18);
            this.Frame7.Controls.Add(this.lblLabels_19);
            this.Frame7.Controls.Add(this.lblLabels_20);
            this.Frame7.Controls.Add(this.lblLabels_28);
            this.Frame7.Controls.Add(this.lblLabels_29);
            this.Frame7.Controls.Add(this.lblLabels_24);
            this.Frame7.Controls.Add(this.lblLabels_38);
            this.Frame7.Controls.Add(this.lblLabels_39);
            this.Frame7.Controls.Add(this.lblLabels_40);
            this.Frame7.Location = new System.Drawing.Point(20, 160);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(966, 235);
            this.Frame7.TabIndex = 1;
            this.Frame7.Text = "Mother\'s Information";
            // 
            // txtMotherDOB
            // 
            this.txtMotherDOB.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtMotherDOB.Location = new System.Drawing.Point(20, 120);
            this.txtMotherDOB.Name = "txtMotherDOB";
            this.txtMotherDOB.Size = new System.Drawing.Size(160, 40);
            this.txtMotherDOB.TabIndex = 34;
            this.txtMotherDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtMotherDOB_Validating);
            // 
            // txtMothersTown
            // 
            this.txtMothersTown.Location = new System.Drawing.Point(763, 120);
            this.txtMothersTown.Name = "txtMothersTown";
            this.txtMothersTown.Size = new System.Drawing.Size(183, 40);
            this.txtMothersTown.TabIndex = 38;
            this.txtMothersTown.Tag = "Required";
            this.txtMothersTown.DoubleClick += new System.EventHandler(this.txtMothersTown_DoubleClick);
            // 
            // txtMotherCounty
            // 
            this.txtMotherCounty.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtMotherCounty.Location = new System.Drawing.Point(583, 120);
            this.txtMotherCounty.Name = "txtMotherCounty";
            this.txtMotherCounty.Size = new System.Drawing.Size(160, 40);
            this.txtMotherCounty.TabIndex = 37;
            this.txtMotherCounty.DoubleClick += new System.EventHandler(this.txtMotherCounty_DoubleClick);
            // 
            // txtMotherState
            // 
            this.txtMotherState.Location = new System.Drawing.Point(403, 120);
            this.txtMotherState.Name = "txtMotherState";
            this.txtMotherState.Size = new System.Drawing.Size(160, 40);
            this.txtMotherState.TabIndex = 36;
            this.txtMotherState.Tag = "Required";
            this.txtMotherState.DoubleClick += new System.EventHandler(this.txtMotherState_DoubleClick);
            // 
            // txtYears
            // 
            this.txtYears.BackColor = System.Drawing.SystemColors.Window;
            this.txtYears.Location = new System.Drawing.Point(846, 175);
            this.txtYears.Name = "txtYears";
            this.txtYears.Size = new System.Drawing.Size(100, 40);
            this.txtYears.TabIndex = 41;
            // 
            // txtMothersAddress
            // 
            this.txtMothersAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtMothersAddress.Location = new System.Drawing.Point(158, 175);
            this.txtMothersAddress.MaxLength = 255;
            this.txtMothersAddress.Name = "txtMothersAddress";
            this.txtMothersAddress.Size = new System.Drawing.Size(295, 40);
            this.txtMothersAddress.TabIndex = 39;
            // 
            // txtMothersZipCode
            // 
            this.txtMothersZipCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtMothersZipCode.Location = new System.Drawing.Point(559, 175);
            this.txtMothersZipCode.Name = "txtMothersZipCode";
            this.txtMothersZipCode.Size = new System.Drawing.Size(140, 40);
            this.txtMothersZipCode.TabIndex = 40;
            // 
            // txtMotherFirstName
            // 
            this.txtMotherFirstName.Location = new System.Drawing.Point(20, 50);
            this.txtMotherFirstName.Name = "txtMotherFirstName";
            this.txtMotherFirstName.Size = new System.Drawing.Size(233, 40);
            this.txtMotherFirstName.TabIndex = 30;
            this.txtMotherFirstName.Tag = "Required";
            // 
            // txtMotherLastName
            // 
            this.txtMotherLastName.Location = new System.Drawing.Point(460, 50);
            this.txtMotherLastName.Name = "txtMotherLastName";
            this.txtMotherLastName.Size = new System.Drawing.Size(233, 40);
            this.txtMotherLastName.TabIndex = 32;
            this.txtMotherLastName.Tag = "Required";
            // 
            // txtMotherMI
            // 
            this.txtMotherMI.BackColor = System.Drawing.SystemColors.Window;
            this.txtMotherMI.Location = new System.Drawing.Point(273, 50);
            this.txtMotherMI.MaxLength = 40;
            this.txtMotherMI.Name = "txtMotherMI";
            this.txtMotherMI.Size = new System.Drawing.Size(167, 40);
            this.txtMotherMI.TabIndex = 31;
            // 
            // txtMotherMaidenName
            // 
            this.txtMotherMaidenName.Location = new System.Drawing.Point(713, 50);
            this.txtMotherMaidenName.Name = "txtMotherMaidenName";
            this.txtMotherMaidenName.Size = new System.Drawing.Size(233, 40);
            this.txtMotherMaidenName.TabIndex = 33;
            this.txtMotherMaidenName.Tag = "Required";
            // 
            // txtMothersBirthPlace
            // 
            this.txtMothersBirthPlace.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtMothersBirthPlace.Location = new System.Drawing.Point(200, 120);
            this.txtMothersBirthPlace.Name = "txtMothersBirthPlace";
            this.txtMothersBirthPlace.Size = new System.Drawing.Size(183, 40);
            this.txtMothersBirthPlace.TabIndex = 35;
            this.txtMothersBirthPlace.DoubleClick += new System.EventHandler(this.txtMothersBirthPlace_DoubleClick);
            // 
            // lblLabels_45
            // 
            this.lblLabels_45.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_45.Location = new System.Drawing.Point(719, 183);
            this.lblLabels_45.Name = "lblLabels_45";
            this.lblLabels_45.Size = new System.Drawing.Size(132, 30);
            this.lblLabels_45.TabIndex = 121;
            this.lblLabels_45.Text = "YEARS LIVING IN PRESENT TOWN";
            // 
            // lblLabels_44
            // 
            this.lblLabels_44.AutoSize = true;
            this.lblLabels_44.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_44.Location = new System.Drawing.Point(20, 189);
            this.lblLabels_44.Name = "lblLabels_44";
            this.lblLabels_44.Size = new System.Drawing.Size(134, 17);
            this.lblLabels_44.TabIndex = 120;
            this.lblLabels_44.Text = "MAILING ADDRESS";
            // 
            // lblLabels_43
            // 
            this.lblLabels_43.AutoSize = true;
            this.lblLabels_43.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_43.Location = new System.Drawing.Point(473, 189);
            this.lblLabels_43.Name = "lblLabels_43";
            this.lblLabels_43.Size = new System.Drawing.Size(71, 17);
            this.lblLabels_43.TabIndex = 119;
            this.lblLabels_43.Text = "ZIP CODE";
            // 
            // lblLabels_18
            // 
            this.lblLabels_18.AutoSize = true;
            this.lblLabels_18.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_18.Location = new System.Drawing.Point(20, 30);
            this.lblLabels_18.Name = "lblLabels_18";
            this.lblLabels_18.Size = new System.Drawing.Size(90, 17);
            this.lblLabels_18.TabIndex = 97;
            this.lblLabels_18.Text = "FIRST NAME";
            // 
            // lblLabels_19
            // 
            this.lblLabels_19.AutoSize = true;
            this.lblLabels_19.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_19.Location = new System.Drawing.Point(460, 30);
            this.lblLabels_19.Name = "lblLabels_19";
            this.lblLabels_19.Size = new System.Drawing.Size(86, 17);
            this.lblLabels_19.TabIndex = 96;
            this.lblLabels_19.Text = "LAST NAME";
            // 
            // lblLabels_20
            // 
            this.lblLabels_20.AutoSize = true;
            this.lblLabels_20.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_20.Location = new System.Drawing.Point(273, 30);
            this.lblLabels_20.Name = "lblLabels_20";
            this.lblLabels_20.Size = new System.Drawing.Size(59, 17);
            this.lblLabels_20.TabIndex = 95;
            this.lblLabels_20.Text = "MIDDLE";
            // 
            // lblLabels_28
            // 
            this.lblLabels_28.AutoSize = true;
            this.lblLabels_28.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_28.Location = new System.Drawing.Point(713, 30);
            this.lblLabels_28.Name = "lblLabels_28";
            this.lblLabels_28.Size = new System.Drawing.Size(104, 17);
            this.lblLabels_28.TabIndex = 94;
            this.lblLabels_28.Text = "MAIDEN NAME";
            // 
            // lblLabels_29
            // 
            this.lblLabels_29.AutoSize = true;
            this.lblLabels_29.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_29.Location = new System.Drawing.Point(200, 100);
            this.lblLabels_29.Name = "lblLabels_29";
            this.lblLabels_29.Size = new System.Drawing.Size(150, 17);
            this.lblLabels_29.TabIndex = 93;
            this.lblLabels_29.Text = "BIRTHPLACE - STATE";
            // 
            // lblLabels_24
            // 
            this.lblLabels_24.AutoSize = true;
            this.lblLabels_24.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_24.Location = new System.Drawing.Point(763, 100);
            this.lblLabels_24.Name = "lblLabels_24";
            this.lblLabels_24.Size = new System.Drawing.Size(107, 17);
            this.lblLabels_24.TabIndex = 92;
            this.lblLabels_24.Text = "CITY OR TOWN";
            // 
            // lblLabels_38
            // 
            this.lblLabels_38.AutoSize = true;
            this.lblLabels_38.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_38.Location = new System.Drawing.Point(20, 100);
            this.lblLabels_38.Name = "lblLabels_38";
            this.lblLabels_38.Size = new System.Drawing.Size(111, 17);
            this.lblLabels_38.TabIndex = 91;
            this.lblLabels_38.Text = "DATE OF BIRTH";
            // 
            // lblLabels_39
            // 
            this.lblLabels_39.AutoSize = true;
            this.lblLabels_39.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_39.Location = new System.Drawing.Point(403, 100);
            this.lblLabels_39.Name = "lblLabels_39";
            this.lblLabels_39.Size = new System.Drawing.Size(95, 17);
            this.lblLabels_39.TabIndex = 90;
            this.lblLabels_39.Text = "RES. - STATE";
            // 
            // lblLabels_40
            // 
            this.lblLabels_40.AutoSize = true;
            this.lblLabels_40.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_40.Location = new System.Drawing.Point(583, 100);
            this.lblLabels_40.Name = "lblLabels_40";
            this.lblLabels_40.Size = new System.Drawing.Size(64, 17);
            this.lblLabels_40.TabIndex = 89;
            this.lblLabels_40.Text = "COUNTY";
            // 
            // fraFather
            // 
            this.fraFather.Controls.Add(this.txtFathersDOB);
            this.fraFather.Controls.Add(this.txtFatherDesignation);
            this.fraFather.Controls.Add(this.txtFatherFirstName);
            this.fraFather.Controls.Add(this.txtFatherLastName);
            this.fraFather.Controls.Add(this.txtFatherMI);
            this.fraFather.Controls.Add(this.txtFathersBirthPlace);
            this.fraFather.Controls.Add(this.lblLabels_11);
            this.fraFather.Controls.Add(this.lblLabels_12);
            this.fraFather.Controls.Add(this.lblLabels_13);
            this.fraFather.Controls.Add(this.lblLabels_14);
            this.fraFather.Controls.Add(this.lblLabels_30);
            this.fraFather.Controls.Add(this.lblLabels_41);
            this.fraFather.Location = new System.Drawing.Point(20, 415);
            this.fraFather.Name = "fraFather";
            this.fraFather.Size = new System.Drawing.Size(966, 180);
            this.fraFather.TabIndex = 2;
            this.fraFather.Text = "Father\'s Information";
            // 
            // txtFathersDOB
            // 
            this.txtFathersDOB.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtFathersDOB.Location = new System.Drawing.Point(20, 120);
            this.txtFathersDOB.Name = "txtFathersDOB";
            this.txtFathersDOB.Size = new System.Drawing.Size(160, 40);
            this.txtFathersDOB.TabIndex = 46;
            this.txtFathersDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtFathersDOB_Validating);
            // 
            // txtFatherDesignation
            // 
            this.txtFatherDesignation.BackColor = System.Drawing.SystemColors.Window;
            this.txtFatherDesignation.Location = new System.Drawing.Point(826, 50);
            this.txtFatherDesignation.Name = "txtFatherDesignation";
            this.txtFatherDesignation.Size = new System.Drawing.Size(120, 40);
            this.txtFatherDesignation.TabIndex = 45;
            // 
            // txtFatherFirstName
            // 
            this.txtFatherFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFatherFirstName.Location = new System.Drawing.Point(20, 50);
            this.txtFatherFirstName.Name = "txtFatherFirstName";
            this.txtFatherFirstName.Size = new System.Drawing.Size(283, 40);
            this.txtFatherFirstName.TabIndex = 42;
            // 
            // txtFatherLastName
            // 
            this.txtFatherLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFatherLastName.Location = new System.Drawing.Point(523, 50);
            this.txtFatherLastName.Name = "txtFatherLastName";
            this.txtFatherLastName.Size = new System.Drawing.Size(283, 40);
            this.txtFatherLastName.TabIndex = 44;
            // 
            // txtFatherMI
            // 
            this.txtFatherMI.BackColor = System.Drawing.SystemColors.Window;
            this.txtFatherMI.Location = new System.Drawing.Point(323, 50);
            this.txtFatherMI.MaxLength = 40;
            this.txtFatherMI.Name = "txtFatherMI";
            this.txtFatherMI.Size = new System.Drawing.Size(180, 40);
            this.txtFatherMI.TabIndex = 43;
            // 
            // txtFathersBirthPlace
            // 
            this.txtFathersBirthPlace.BackColor = System.Drawing.SystemColors.Window;
            this.txtFathersBirthPlace.Location = new System.Drawing.Point(200, 120);
            this.txtFathersBirthPlace.Name = "txtFathersBirthPlace";
            this.txtFathersBirthPlace.Size = new System.Drawing.Size(606, 40);
            this.txtFathersBirthPlace.TabIndex = 47;
            this.txtFathersBirthPlace.DoubleClick += new System.EventHandler(this.txtFathersBirthPlace_DoubleClick);
            // 
            // lblLabels_11
            // 
            this.lblLabels_11.AutoSize = true;
            this.lblLabels_11.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_11.Location = new System.Drawing.Point(826, 30);
            this.lblLabels_11.Name = "lblLabels_11";
            this.lblLabels_11.Size = new System.Drawing.Size(58, 17);
            this.lblLabels_11.TabIndex = 104;
            this.lblLabels_11.Text = "JR. / SR";
            // 
            // lblLabels_12
            // 
            this.lblLabels_12.AutoSize = true;
            this.lblLabels_12.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_12.Location = new System.Drawing.Point(20, 30);
            this.lblLabels_12.Name = "lblLabels_12";
            this.lblLabels_12.Size = new System.Drawing.Size(90, 17);
            this.lblLabels_12.TabIndex = 103;
            this.lblLabels_12.Text = "FIRST NAME";
            // 
            // lblLabels_13
            // 
            this.lblLabels_13.AutoSize = true;
            this.lblLabels_13.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_13.Location = new System.Drawing.Point(523, 30);
            this.lblLabels_13.Name = "lblLabels_13";
            this.lblLabels_13.Size = new System.Drawing.Size(86, 17);
            this.lblLabels_13.TabIndex = 102;
            this.lblLabels_13.Text = "LAST NAME";
            // 
            // lblLabels_14
            // 
            this.lblLabels_14.AutoSize = true;
            this.lblLabels_14.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_14.Location = new System.Drawing.Point(323, 30);
            this.lblLabels_14.Name = "lblLabels_14";
            this.lblLabels_14.Size = new System.Drawing.Size(59, 17);
            this.lblLabels_14.TabIndex = 101;
            this.lblLabels_14.Text = "MIDDLE";
            // 
            // lblLabels_30
            // 
            this.lblLabels_30.AutoSize = true;
            this.lblLabels_30.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_30.Location = new System.Drawing.Point(200, 100);
            this.lblLabels_30.Name = "lblLabels_30";
            this.lblLabels_30.Size = new System.Drawing.Size(150, 17);
            this.lblLabels_30.TabIndex = 100;
            this.lblLabels_30.Text = "BIRTHPLACE - STATE";
            // 
            // lblLabels_41
            // 
            this.lblLabels_41.AutoSize = true;
            this.lblLabels_41.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_41.Location = new System.Drawing.Point(20, 100);
            this.lblLabels_41.Name = "lblLabels_41";
            this.lblLabels_41.Size = new System.Drawing.Size(111, 17);
            this.lblLabels_41.TabIndex = 99;
            this.lblLabels_41.Text = "DATE OF BIRTH";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtRegTown);
            this.Frame3.Controls.Add(this.mebRegistrarDateFiling);
            this.Frame3.Controls.Add(this.cmdRegistrar);
            this.Frame3.Controls.Add(this.txtRegistrarName);
            this.Frame3.Controls.Add(this.lbltown_42);
            this.Frame3.Controls.Add(this.lblLabels_48);
            this.Frame3.Controls.Add(this.lblLabels_47);
            this.Frame3.Location = new System.Drawing.Point(20, 30);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(966, 110);
            this.Frame3.TabIndex = 3;
            this.Frame3.Text = "Registrar\'s Information";
            // 
            // txtRegTown
            // 
            this.txtRegTown.BackColor = System.Drawing.SystemColors.Window;
            this.txtRegTown.Location = new System.Drawing.Point(526, 50);
            this.txtRegTown.Name = "txtRegTown";
            this.txtRegTown.Size = new System.Drawing.Size(200, 40);
            this.txtRegTown.TabIndex = 28;
            this.txtRegTown.DoubleClick += new System.EventHandler(this.txtRegTown_DoubleClick);
            this.txtRegTown.Validating += new System.ComponentModel.CancelEventHandler(this.txtRegTown_Validating);
            // 
            // mebRegistrarDateFiling
            // 
            this.mebRegistrarDateFiling.Location = new System.Drawing.Point(746, 50);
            this.mebRegistrarDateFiling.Name = "mebRegistrarDateFiling";
            this.mebRegistrarDateFiling.Size = new System.Drawing.Size(200, 40);
            this.mebRegistrarDateFiling.TabIndex = 29;
            this.mebRegistrarDateFiling.Tag = "Required";
            this.mebRegistrarDateFiling.Validating += new System.ComponentModel.CancelEventHandler(this.mebRegistrarDateFiling_Validating);
            // 
            // cmdRegistrar
            // 
            this.cmdRegistrar.AppearanceKey = "actionButton";
            this.cmdRegistrar.Image = ((System.Drawing.Image)(resources.GetObject("cmdRegistrar.Image")));
            this.cmdRegistrar.Location = new System.Drawing.Point(20, 50);
            this.cmdRegistrar.Name = "cmdRegistrar";
            this.cmdRegistrar.Size = new System.Drawing.Size(40, 40);
            this.cmdRegistrar.TabIndex = 26;
            this.ToolTip1.SetToolTip(this.cmdRegistrar, "Find Recording Clerk\'s Name");
            this.cmdRegistrar.Enter += new System.EventHandler(this.cmdRegistrar_Enter);
            this.cmdRegistrar.Click += new System.EventHandler(this.cmdRegistrar_Click);
            // 
            // txtRegistrarName
            // 
            this.txtRegistrarName.Location = new System.Drawing.Point(80, 50);
            this.txtRegistrarName.Name = "txtRegistrarName";
            this.txtRegistrarName.Size = new System.Drawing.Size(426, 40);
            this.txtRegistrarName.TabIndex = 27;
            this.txtRegistrarName.Tag = "Required";
            // 
            // lbltown_42
            // 
            this.lbltown_42.AutoSize = true;
            this.lbltown_42.BackColor = System.Drawing.Color.Transparent;
            this.lbltown_42.Location = new System.Drawing.Point(526, 30);
            this.lbltown_42.Name = "lbltown_42";
            this.lbltown_42.Size = new System.Drawing.Size(49, 17);
            this.lbltown_42.TabIndex = 126;
            this.lbltown_42.Text = "TOWN";
            // 
            // lblLabels_48
            // 
            this.lblLabels_48.AutoSize = true;
            this.lblLabels_48.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_48.Location = new System.Drawing.Point(80, 30);
            this.lblLabels_48.Name = "lblLabels_48";
            this.lblLabels_48.Size = new System.Drawing.Size(47, 17);
            this.lblLabels_48.TabIndex = 124;
            this.lblLabels_48.Text = "NAME";
            // 
            // lblLabels_47
            // 
            this.lblLabels_47.AutoSize = true;
            this.lblLabels_47.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_47.Location = new System.Drawing.Point(746, 30);
            this.lblLabels_47.Name = "lblLabels_47";
            this.lblLabels_47.Size = new System.Drawing.Size(87, 17);
            this.lblLabels_47.TabIndex = 123;
            this.lblLabels_47.Text = "DATE FILED";
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.Frame2);
            this.SSTab1_Page3.Controls.Add(this.Frame9);
            this.SSTab1_Page3.Controls.Add(this.chkSoleParent);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Size = new System.Drawing.Size(1006, 628);
            this.SSTab1_Page3.Text = "Informant / Record Info";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cmdFindMunicipalClerk);
            this.Frame2.Controls.Add(this.txtCourt);
            this.Frame2.Controls.Add(this.txtAmended);
            this.Frame2.Controls.Add(this.txtStateRegistrar);
            this.Frame2.Controls.Add(this.txtTownOf);
            this.Frame2.Controls.Add(this.txtAttestDate);
            this.Frame2.Controls.Add(this.mebAmendedDate);
            this.Frame2.Controls.Add(this.lblLabels_33);
            this.Frame2.Controls.Add(this.lblLabels_32);
            this.Frame2.Controls.Add(this.lblLabels_31);
            this.Frame2.Controls.Add(this.lblLabels_27);
            this.Frame2.Controls.Add(this.lblLabels_26);
            this.Frame2.Controls.Add(this.lblLabels_5);
            this.Frame2.Location = new System.Drawing.Point(20, 187);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(966, 190);
            this.Frame2.TabIndex = 2;
            this.Frame2.Text = "Record Information";
            // 
            // cmdFindMunicipalClerk
            // 
            this.cmdFindMunicipalClerk.AppearanceKey = "actionButton";
            this.cmdFindMunicipalClerk.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindMunicipalClerk.Image")));
            this.cmdFindMunicipalClerk.Location = new System.Drawing.Point(906, 80);
            this.cmdFindMunicipalClerk.Name = "cmdFindMunicipalClerk";
            this.cmdFindMunicipalClerk.Size = new System.Drawing.Size(40, 40);
            this.cmdFindMunicipalClerk.TabIndex = 59;
            this.ToolTip1.SetToolTip(this.cmdFindMunicipalClerk, "Find Recording Clerk\'s Name");
            this.cmdFindMunicipalClerk.Click += new System.EventHandler(this.cmdFindMunicipalClerk_Click);
            // 
            // txtCourt
            // 
            this.txtCourt.BackColor = System.Drawing.SystemColors.Window;
            this.txtCourt.Location = new System.Drawing.Point(143, 130);
            this.txtCourt.Name = "txtCourt";
            this.txtCourt.Size = new System.Drawing.Size(316, 40);
            this.txtCourt.TabIndex = 56;
            // 
            // txtAmended
            // 
            this.txtAmended.BackColor = System.Drawing.SystemColors.Window;
            this.txtAmended.Location = new System.Drawing.Point(143, 80);
            this.txtAmended.Name = "txtAmended";
            this.txtAmended.Size = new System.Drawing.Size(316, 40);
            this.txtAmended.TabIndex = 55;
            // 
            // txtStateRegistrar
            // 
            this.txtStateRegistrar.Location = new System.Drawing.Point(625, 80);
            this.txtStateRegistrar.Name = "txtStateRegistrar";
            this.txtStateRegistrar.Size = new System.Drawing.Size(261, 40);
            this.txtStateRegistrar.TabIndex = 58;
            this.txtStateRegistrar.Tag = "Required";
            // 
            // txtTownOf
            // 
            this.txtTownOf.Location = new System.Drawing.Point(625, 130);
            this.txtTownOf.Name = "txtTownOf";
            this.txtTownOf.Size = new System.Drawing.Size(321, 40);
            this.txtTownOf.TabIndex = 60;
            this.txtTownOf.Tag = "Required";
            this.txtTownOf.Enter += new System.EventHandler(this.txtTownOf_Enter);
            this.txtTownOf.DoubleClick += new System.EventHandler(this.txtTownOf_DoubleClick);
            this.txtTownOf.Validating += new System.ComponentModel.CancelEventHandler(this.txtTownOf_Validating);
            // 
            // txtAttestDate
            // 
            this.txtAttestDate.Location = new System.Drawing.Point(625, 30);
            this.txtAttestDate.Name = "txtAttestDate";
            this.txtAttestDate.Size = new System.Drawing.Size(115, 22);
            this.txtAttestDate.TabIndex = 57;
            this.txtAttestDate.Tag = "Required";
            // 
            // mebAmendedDate
            // 
            this.mebAmendedDate.Location = new System.Drawing.Point(143, 30);
            this.mebAmendedDate.Name = "mebAmendedDate";
            this.mebAmendedDate.Size = new System.Drawing.Size(115, 22);
            this.mebAmendedDate.TabIndex = 54;
            // 
            // lblLabels_33
            // 
            this.lblLabels_33.AutoSize = true;
            this.lblLabels_33.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_33.Location = new System.Drawing.Point(20, 44);
            this.lblLabels_33.Name = "lblLabels_33";
            this.lblLabels_33.Size = new System.Drawing.Size(116, 17);
            this.lblLabels_33.TabIndex = 111;
            this.lblLabels_33.Text = "AMENDED DATE";
            // 
            // lblLabels_32
            // 
            this.lblLabels_32.AutoSize = true;
            this.lblLabels_32.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_32.Location = new System.Drawing.Point(20, 94);
            this.lblLabels_32.Name = "lblLabels_32";
            this.lblLabels_32.Size = new System.Drawing.Size(76, 17);
            this.lblLabels_32.TabIndex = 110;
            this.lblLabels_32.Text = "AMENDED ";
            // 
            // lblLabels_31
            // 
            this.lblLabels_31.AutoSize = true;
            this.lblLabels_31.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_31.Location = new System.Drawing.Point(20, 144);
            this.lblLabels_31.Name = "lblLabels_31";
            this.lblLabels_31.Size = new System.Drawing.Size(55, 17);
            this.lblLabels_31.TabIndex = 109;
            this.lblLabels_31.Text = "COURT";
            // 
            // lblLabels_27
            // 
            this.lblLabels_27.AutoSize = true;
            this.lblLabels_27.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_27.Location = new System.Drawing.Point(489, 144);
            this.lblLabels_27.Name = "lblLabels_27";
            this.lblLabels_27.Size = new System.Drawing.Size(71, 17);
            this.lblLabels_27.TabIndex = 108;
            this.lblLabels_27.Text = "TOWN OF";
            // 
            // lblLabels_26
            // 
            this.lblLabels_26.AutoSize = true;
            this.lblLabels_26.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_26.Location = new System.Drawing.Point(489, 94);
            this.lblLabels_26.Name = "lblLabels_26";
            this.lblLabels_26.Size = new System.Drawing.Size(132, 17);
            this.lblLabels_26.TabIndex = 107;
            this.lblLabels_26.Text = "MUNICIPAL CLERK";
            // 
            // lblLabels_5
            // 
            this.lblLabels_5.AutoSize = true;
            this.lblLabels_5.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_5.Location = new System.Drawing.Point(489, 44);
            this.lblLabels_5.Name = "lblLabels_5";
            this.lblLabels_5.Size = new System.Drawing.Size(100, 17);
            this.lblLabels_5.TabIndex = 106;
            this.lblLabels_5.Text = "ATTEST DATE";
            // 
            // Frame9
            // 
            this.Frame9.Controls.Add(this.txtClerkCityTown);
            this.Frame9.Controls.Add(this.txtClerkFirstName);
            this.Frame9.Controls.Add(this.txtClerkLastName);
            this.Frame9.Controls.Add(this.txtClerkMI);
            this.Frame9.Controls.Add(this.cmdFind);
            this.Frame9.Controls.Add(this.txtClerkDateOfFiling);
            this.Frame9.Controls.Add(this.lblLabels_7);
            this.Frame9.Controls.Add(this.lblLabels_21);
            this.Frame9.Controls.Add(this.lblLabels_22);
            this.Frame9.Controls.Add(this.lblLabels_23);
            this.Frame9.Controls.Add(this.lblLabels_9);
            this.Frame9.Location = new System.Drawing.Point(20, 30);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(966, 110);
            this.Frame9.TabIndex = 3;
            this.Frame9.Text = "Informant Information";
            // 
            // txtClerkCityTown
            // 
            this.txtClerkCityTown.BackColor = System.Drawing.SystemColors.Window;
            this.txtClerkCityTown.Location = new System.Drawing.Point(721, 50);
            this.txtClerkCityTown.Name = "txtClerkCityTown";
            this.txtClerkCityTown.Size = new System.Drawing.Size(225, 40);
            this.txtClerkCityTown.TabIndex = 52;
            this.txtClerkCityTown.DoubleClick += new System.EventHandler(this.txtClerkCityTown_DoubleClick);
            this.txtClerkCityTown.TextChanged += new System.EventHandler(this.txtClerkCityTown_TextChanged);
            // 
            // txtClerkFirstName
            // 
            this.txtClerkFirstName.Location = new System.Drawing.Point(80, 50);
            this.txtClerkFirstName.Name = "txtClerkFirstName";
            this.txtClerkFirstName.Size = new System.Drawing.Size(225, 40);
            this.txtClerkFirstName.TabIndex = 49;
            this.txtClerkFirstName.Tag = "Required";
            this.txtClerkFirstName.Enter += new System.EventHandler(this.txtClerkFirstName_Enter);
            // 
            // txtClerkLastName
            // 
            this.txtClerkLastName.Location = new System.Drawing.Point(476, 50);
            this.txtClerkLastName.Name = "txtClerkLastName";
            this.txtClerkLastName.Size = new System.Drawing.Size(225, 40);
            this.txtClerkLastName.TabIndex = 51;
            this.txtClerkLastName.Tag = "Required";
            // 
            // txtClerkMI
            // 
            this.txtClerkMI.BackColor = System.Drawing.SystemColors.Window;
            this.txtClerkMI.Location = new System.Drawing.Point(325, 50);
            this.txtClerkMI.MaxLength = 40;
            this.txtClerkMI.Name = "txtClerkMI";
            this.txtClerkMI.Size = new System.Drawing.Size(131, 40);
            this.txtClerkMI.TabIndex = 50;
            // 
            // cmdFind
            // 
            this.cmdFind.AppearanceKey = "actionButton";
            this.cmdFind.Image = ((System.Drawing.Image)(resources.GetObject("cmdFind.Image")));
            this.cmdFind.Location = new System.Drawing.Point(20, 50);
            this.cmdFind.Name = "cmdFind";
            this.cmdFind.Size = new System.Drawing.Size(40, 40);
            this.cmdFind.TabIndex = 48;
            this.ToolTip1.SetToolTip(this.cmdFind, "Find Recording Clerk\'s Name");
            this.cmdFind.Visible = false;
            this.cmdFind.Enter += new System.EventHandler(this.cmdFind_Enter);
            this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
            // 
            // txtClerkDateOfFiling
            // 
            this.txtClerkDateOfFiling.Location = new System.Drawing.Point(20, 51);
            this.txtClerkDateOfFiling.Name = "txtClerkDateOfFiling";
            this.txtClerkDateOfFiling.Size = new System.Drawing.Size(115, 22);
            this.txtClerkDateOfFiling.TabIndex = 62;
            this.txtClerkDateOfFiling.Visible = false;
            // 
            // lblLabels_7
            // 
            this.lblLabels_7.AutoSize = true;
            this.lblLabels_7.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_7.Location = new System.Drawing.Point(721, 30);
            this.lblLabels_7.Name = "lblLabels_7";
            this.lblLabels_7.Size = new System.Drawing.Size(107, 17);
            this.lblLabels_7.TabIndex = 117;
            this.lblLabels_7.Text = "CITY OR TOWN";
            // 
            // lblLabels_21
            // 
            this.lblLabels_21.AutoSize = true;
            this.lblLabels_21.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_21.Location = new System.Drawing.Point(80, 30);
            this.lblLabels_21.Name = "lblLabels_21";
            this.lblLabels_21.Size = new System.Drawing.Size(90, 17);
            this.lblLabels_21.TabIndex = 115;
            this.lblLabels_21.Text = "FIRST NAME";
            // 
            // lblLabels_22
            // 
            this.lblLabels_22.AutoSize = true;
            this.lblLabels_22.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_22.Location = new System.Drawing.Point(476, 30);
            this.lblLabels_22.Name = "lblLabels_22";
            this.lblLabels_22.Size = new System.Drawing.Size(86, 17);
            this.lblLabels_22.TabIndex = 114;
            this.lblLabels_22.Text = "LAST NAME";
            // 
            // lblLabels_23
            // 
            this.lblLabels_23.AutoSize = true;
            this.lblLabels_23.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_23.Location = new System.Drawing.Point(325, 30);
            this.lblLabels_23.Name = "lblLabels_23";
            this.lblLabels_23.Size = new System.Drawing.Size(59, 17);
            this.lblLabels_23.TabIndex = 113;
            this.lblLabels_23.Text = "MIDDLE";
            // 
            // lblLabels_9
            // 
            this.lblLabels_9.AutoSize = true;
            this.lblLabels_9.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_9.Location = new System.Drawing.Point(20, 29);
            this.lblLabels_9.Name = "lblLabels_9";
            this.lblLabels_9.Size = new System.Drawing.Size(114, 17);
            this.lblLabels_9.TabIndex = 116;
            this.lblLabels_9.Text = "DATE OF FILING";
            this.lblLabels_9.Visible = false;
            // 
            // chkSoleParent
            // 
            this.chkSoleParent.Location = new System.Drawing.Point(20, 150);
            this.chkSoleParent.Name = "chkSoleParent";
            this.chkSoleParent.Size = new System.Drawing.Size(215, 24);
            this.chkSoleParent.TabIndex = 1;
            this.chkSoleParent.Text = "\'Sole Legal Legitimate Parent\'";
            // 
            // SSTab1_Page4
            // 
            this.SSTab1_Page4.Controls.Add(this.VSFlexGrid1);
            this.SSTab1_Page4.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page4.Name = "SSTab1_Page4";
            this.SSTab1_Page4.Size = new System.Drawing.Size(1006, 628);
            this.SSTab1_Page4.Text = "Amended Info";
            // 
            // VSFlexGrid1
            // 
            this.VSFlexGrid1.Cols = 7;
            this.VSFlexGrid1.ExtendLastCol = true;
            this.VSFlexGrid1.FixedCols = 0;
            this.VSFlexGrid1.Location = new System.Drawing.Point(20, 20);
            this.VSFlexGrid1.Name = "VSFlexGrid1";
            this.VSFlexGrid1.RowHeadersVisible = false;
            this.VSFlexGrid1.Rows = 17;
            this.VSFlexGrid1.Size = new System.Drawing.Size(966, 576);
            this.VSFlexGrid1.StandardTab = false;
            this.VSFlexGrid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.VSFlexGrid1.TabIndex = 61;
            this.VSFlexGrid1.ComboCloseUp += new System.EventHandler(this.VSFlexGrid1_ComboCloseUp);
            this.VSFlexGrid1.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.VSFlexGrid1_MouseMoveEvent);
            this.VSFlexGrid1.Enter += new System.EventHandler(this.VSFlexGrid1_Enter);
            this.VSFlexGrid1.KeyDown += new Wisej.Web.KeyEventHandler(this.VSFlexGrid1_KeyDownEvent);
            // 
            // chkRequired
            // 
            this.chkRequired.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.chkRequired.Location = new System.Drawing.Point(330, 111);
            this.chkRequired.Name = "chkRequired";
            this.chkRequired.Size = new System.Drawing.Size(387, 24);
            this.chkRequired.TabIndex = 4;
            this.chkRequired.Text = "Record contains older data. Not all fields will be required";
            // 
            // txtFileNumber
            // 
            this.txtFileNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileNumber.Location = new System.Drawing.Point(100, 30);
            this.txtFileNumber.Name = "txtFileNumber";
            this.txtFileNumber.Size = new System.Drawing.Size(200, 40);
            this.txtFileNumber.TabIndex = 70;
            // 
            // chkIllegitBirth
            // 
            this.chkIllegitBirth.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.chkIllegitBirth.Location = new System.Drawing.Point(330, 30);
            this.chkIllegitBirth.Name = "chkIllegitBirth";
            this.chkIllegitBirth.Size = new System.Drawing.Size(259, 24);
            this.chkIllegitBirth.TabIndex = 1;
            this.chkIllegitBirth.Text = "Check this box if this birth was BOW";
            this.chkIllegitBirth.CheckedChanged += new System.EventHandler(this.chkIllegitBirth_CheckedChanged);
            // 
            // cdlColor
            // 
            this.cdlColor.Name = "cdlColor";
            this.cdlColor.Size = new System.Drawing.Size(0, 0);
            // 
            // txtBirthCertNum
            // 
            this.txtBirthCertNum.Appearance = 0;
            this.txtBirthCertNum.BackColor = System.Drawing.SystemColors.Info;
            this.txtBirthCertNum.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtBirthCertNum.Location = new System.Drawing.Point(30, 225);
            this.txtBirthCertNum.Name = "txtBirthCertNum";
            this.txtBirthCertNum.Size = new System.Drawing.Size(113, 40);
            this.txtBirthCertNum.TabIndex = 67;
            this.txtBirthCertNum.Visible = false;
            // 
            // mebDateDeceased
            // 
            this.mebDateDeceased.Location = new System.Drawing.Point(682, 145);
            this.mebDateDeceased.Name = "mebDateDeceased";
            this.mebDateDeceased.Size = new System.Drawing.Size(115, 22);
            this.mebDateDeceased.TabIndex = 6;
            this.mebDateDeceased.Visible = false;
            // 
            // gridTownCode
            // 
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.Location = new System.Drawing.Point(30, 130);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.ReadOnly = false;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.Size = new System.Drawing.Size(177, 41);
            this.gridTownCode.TabIndex = 125;
            this.gridTownCode.Tag = "land";
            // 
            // imgDocuments
            // 
            this.imgDocuments.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDocuments.Image = ((System.Drawing.Image)(resources.GetObject("imgDocuments.Image")));
            this.imgDocuments.Location = new System.Drawing.Point(70, 30);
            this.imgDocuments.Name = "imgDocuments";
            this.imgDocuments.Size = new System.Drawing.Size(29, 30);
            this.imgDocuments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgDocuments, "One or more documents are attached to this record");
            this.imgDocuments.Click += new System.EventHandler(this.imgDocuments_Click);
            // 
            // ImgComment
            // 
            this.ImgComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.ImgComment.Image = ((System.Drawing.Image)(resources.GetObject("ImgComment.Image")));
            this.ImgComment.Location = new System.Drawing.Point(30, 10);
            this.ImgComment.Name = "ImgComment";
            this.ImgComment.Size = new System.Drawing.Size(17, 18);
            this.ImgComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // lblBKey
            // 
            this.lblBKey.Location = new System.Drawing.Point(93, 473);
            this.lblBKey.Name = "lblBKey";
            this.lblBKey.Size = new System.Drawing.Size(112, 17);
            this.lblBKey.TabIndex = 66;
            this.lblBKey.Visible = false;
            // 
            // lblBirthID
            // 
            this.lblBirthID.Location = new System.Drawing.Point(30, 473);
            this.lblBirthID.Name = "lblBirthID";
            this.lblBirthID.Size = new System.Drawing.Size(55, 17);
            this.lblBirthID.TabIndex = 65;
            this.lblBirthID.Text = "BIRTH ID";
            this.lblBirthID.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBirthID.Visible = false;
            // 
            // lblRecord
            // 
            this.lblRecord.Location = new System.Drawing.Point(493, 222);
            this.lblRecord.Name = "lblRecord";
            this.lblRecord.Size = new System.Drawing.Size(79, 13);
            this.lblRecord.TabIndex = 64;
            this.lblRecord.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRecord.Visible = false;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSetDefaultTowns,
            this.mnuRegistrarNames,
            this.mnuDefaultCounties,
            this.mnuDefaultStates,
            this.mnuViewDocuments});
            this.MainMenu1.Name = null;
            // 
            // mnuSetDefaultTowns
            // 
            this.mnuSetDefaultTowns.Index = 0;
            this.mnuSetDefaultTowns.Name = "mnuSetDefaultTowns";
            this.mnuSetDefaultTowns.Text = "Edit Town Names";
            this.mnuSetDefaultTowns.Click += new System.EventHandler(this.mnuSetDefaultTowns_Click);
            // 
            // mnuRegistrarNames
            // 
            this.mnuRegistrarNames.Index = 1;
            this.mnuRegistrarNames.Name = "mnuRegistrarNames";
            this.mnuRegistrarNames.Text = "Edit Clerk Names";
            this.mnuRegistrarNames.Click += new System.EventHandler(this.mnuRegistrarNames_Click);
            // 
            // mnuDefaultCounties
            // 
            this.mnuDefaultCounties.Index = 2;
            this.mnuDefaultCounties.Name = "mnuDefaultCounties";
            this.mnuDefaultCounties.Text = "Edit County Names";
            this.mnuDefaultCounties.Visible = false;
            // 
            // mnuDefaultStates
            // 
            this.mnuDefaultStates.Index = 3;
            this.mnuDefaultStates.Name = "mnuDefaultStates";
            this.mnuDefaultStates.Text = "Edit State Names";
            this.mnuDefaultStates.Visible = false;
            // 
            // mnuViewDocuments
            // 
            this.mnuViewDocuments.Index = 4;
            this.mnuViewDocuments.Name = "mnuViewDocuments";
            this.mnuViewDocuments.Text = "View Attached Documents";
            this.mnuViewDocuments.Click += new System.EventHandler(this.mnuViewDocuments_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "";
            // 
            // mnuSaveExitNoValidation
            // 
            this.mnuSaveExitNoValidation.Index = -1;
            this.mnuSaveExitNoValidation.Name = "mnuSaveExitNoValidation";
            this.mnuSaveExitNoValidation.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuSaveExitNoValidation.Text = "Save & Exit (Without Validation)";
            this.mnuSaveExitNoValidation.Click += new System.EventHandler(this.mnuSaveExitNoValidation_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuProcessAddNewRecord
            // 
            this.mnuProcessAddNewRecord.Index = -1;
            this.mnuProcessAddNewRecord.Name = "mnuProcessAddNewRecord";
            this.mnuProcessAddNewRecord.Text = "New";
            this.mnuProcessAddNewRecord.Click += new System.EventHandler(this.mnuProcessAddNewRecord_Click);
            // 
            // mnuProcessDeleteRecord
            // 
            this.mnuProcessDeleteRecord.Index = -1;
            this.mnuProcessDeleteRecord.Name = "mnuProcessDeleteRecord";
            this.mnuProcessDeleteRecord.Text = "Delete";
            this.mnuProcessDeleteRecord.Click += new System.EventHandler(this.mnuProcessDeleteRecord_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuComment
            // 
            this.mnuComment.Index = -1;
            this.mnuComment.Name = "mnuComment";
            this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComment.Text = "Comments";
            this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // ProcessSeparator1
            // 
            this.ProcessSeparator1.Index = -1;
            this.ProcessSeparator1.Name = "ProcessSeparator1";
            this.ProcessSeparator1.Text = "-";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = -1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSP21
            // 
            this.mnuSP21.Index = -1;
            this.mnuSP21.Name = "mnuSP21";
            this.mnuSP21.Text = "-";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = -1;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuProcessSave.Text = "Save                              ";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = -1;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // lblDeceased
            // 
            this.lblDeceased.AutoSize = true;
            this.lblDeceased.BackColor = System.Drawing.Color.Transparent;
            this.lblDeceased.Location = new System.Drawing.Point(619, 159);
            this.lblDeceased.Name = "lblDeceased";
            this.lblDeceased.Size = new System.Drawing.Size(44, 17);
            this.lblDeceased.TabIndex = 118;
            this.lblDeceased.Text = "DATE ";
            this.lblDeceased.Visible = false;
            // 
            // lblBirthCertNum_28
            // 
            this.lblBirthCertNum_28.AutoSize = true;
            this.lblBirthCertNum_28.BackColor = System.Drawing.Color.Transparent;
            this.lblBirthCertNum_28.Location = new System.Drawing.Point(30, 193);
            this.lblBirthCertNum_28.Name = "lblBirthCertNum_28";
            this.lblBirthCertNum_28.Size = new System.Drawing.Size(151, 17);
            this.lblBirthCertNum_28.TabIndex = 68;
            this.lblBirthCertNum_28.Text = "BIRTH CERT NUMBER";
            this.lblBirthCertNum_28.Visible = false;
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.BackColor = System.Drawing.Color.Transparent;
            this.lblFile.Location = new System.Drawing.Point(30, 44);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(48, 17);
            this.lblFile.TabIndex = 63;
            this.lblFile.Text = "FILE #";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(468, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(78, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // cmdComment
            // 
            this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComment.Location = new System.Drawing.Point(1026, 29);
            this.cmdComment.Name = "cmdComment";
            this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComment.Size = new System.Drawing.Size(85, 24);
            this.cmdComment.TabIndex = 1;
            this.cmdComment.Text = "Comment";
            this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(927, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(93, 24);
            this.cmdPrint.TabIndex = 2;
            this.cmdPrint.Text = "Print Preview";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(858, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(63, 24);
            this.cmdDelete.TabIndex = 3;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuProcessDeleteRecord_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(805, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(47, 24);
            this.cmdNew.TabIndex = 4;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuProcessAddNewRecord_Click);
            // 
            // frmBirths
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmBirths";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Live Births";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmBirths_Load);
            this.Activated += new System.EventHandler(this.frmBirths_Activated);
            this.Resize += new System.EventHandler(this.frmBirths_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBirths_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBirths_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCourtDetermination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAOP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFindClerk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            this.Frame6.PerformLayout();
            this.SSTab1_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            this.Frame7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFather)).EndInit();
            this.fraFather.ResumeLayout(false);
            this.fraFather.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRegistrar)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            this.SSTab1_Page3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFindMunicipalClerk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttestDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebAmendedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
            this.Frame9.ResumeLayout(false);
            this.Frame9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClerkDateOfFiling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSoleParent)).EndInit();
            this.SSTab1_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VSFlexGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIllegitBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDateDeceased)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdPrint;
		private FCButton cmdComment;
		private FCButton cmdDelete;
		private FCButton cmdNew;
	}
}
