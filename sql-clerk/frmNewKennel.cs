//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using SharedApplication;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
    public partial class frmNewKennel : BaseForm
    {
        public frmNewKennel()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            lblLabels = new System.Collections.Generic.List<FCLabel>();
            lblLabels.AddControlArrayElement(lblLabels_1, 0);
        }
        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmNewKennel InstancePtr
        {
            get
            {
                return (frmNewKennel)Sys.GetInstance(typeof(frmNewKennel));
            }
        }

        protected frmNewKennel _InstancePtr = null;
        //=========================================================
        int lngYearForDogs;
        int lngOwnerNum;
        int lngLicenseID;
        bool boolMadeTrans;
        bool boolSaved;
        bool boolChangedData;
        bool boolOKToValidate;
        modGNBas.typDogTransactionRecord CurKennelTrans = new modGNBas.typDogTransactionRecord();
        modGNBas.typTransactionRecord curTrans = new modGNBas.typTransactionRecord();
        bool boolNewLicense;
        double dblTranAmount1;
        double dblTranAmount2;
        double dblTranAmount3;
        double dblTranAmount4;
        double dblTranAmount5;
        double dblTranAmount6;
        bool boolReRegistration;
        bool boolReadOnly;
        const int COLDOGNUM = 0;
        const int COLDOGNAME = 1;
        const int COLDOGBREED = 2;
        const int COLDOGSTICKER = 4;
        const int COLRABIES = 5;
        const int COLDOGSEX = 3;
        const int COLDOGUPDATED = 6;
        const int COLADDDOG = 0;
        const int COLADDDOGNUM = 2;
        const int COLADDDOGNAME = 1;
        const int COLADDDOGBREED = 3;
        const int COLADDDOGSTICKER = 4;
        const int COLADDDOGSEX = 5;
        const int COLADDDOGRABIES = 6;

        private void imgDocuments_Click(object sender, System.EventArgs e)
        {
            if (lngLicenseID <= 0)
                return;
            if (imgDocuments.Visible == true)
            {
                ViewDocs();
            }
        }

        private void ViewDocs()
        {
            if (lngLicenseID <= 0) return;

            //frmViewDocuments.InstancePtr.Unload();
            //frmViewDocuments.InstancePtr.Init(0, "Kennels", modGNBas.KENNELTRANSACTION.ToString(), lngLicenseID, "", "twck0000.vb1", FCForm.FormShowEnum.Modal, boolReadOnly);
            //imgDocuments.Visible = modGlobalRoutines.DocListHasItems();

            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer("Kennels", "Clerk", "Kennel",
                lngLicenseID, "", true, boolReadOnly));
            var documentHeaders = StaticSettings.GlobalCommandDispatcher.Send(new GetHeadersByReferenceCommand()).Result;
            imgDocuments.Visible = documentHeaders.Count > 0;
        }
        public bool Init(ref int lngOwnerNumber, int lngID, bool boolReReg = false, int lngYearToUse = 0)
        {
            bool Init = false;
            try
            {
                boolReRegistration = boolReReg;
                boolOKToValidate = false;
                boolSaved = false;
                boolChangedData = false;
                boolMadeTrans = false;
                boolSaved = false;
                lngOwnerNum = lngOwnerNumber;
                lngLicenseID = lngID;
                lngYearForDogs = lngYearToUse;
                Init = false;
                CurKennelTrans.TransactionDate = modGNBas.Statics.typTransaction.TransactionDate;
                curTrans.TransactionDate = modGNBas.Statics.typTransaction.TransactionDate;
                if (!IsLoaded)
                {
                    //FC:FINAL:SBE - #4398 - force load, but don't triggere the event, because the Modal flag will not be correctly only during Show() method
                    frmNewKennel_Load(this, EventArgs.Empty);
                }
                cmbLicenseNumber.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                t2kIssueDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                SetupGridDogs();
                FillLicenseCombo();
                LoadInfo();
                CheckPermissions();
                if (boolReRegistration)
                {
                    chkFees.CheckState = Wisej.Web.CheckState.Checked;
                    boolChangedData = true;
                    chkFees.Enabled = false;
                    chkLicense.CheckState = Wisej.Web.CheckState.Checked;
                    chkLicense.Enabled = false;
                    t2kIssueDate.Text = Strings.Format(modGNBas.Statics.typTransaction.TransactionDate, "MM/dd/yyyy");
                    t2kIssueDate.Enabled = false;
                }
                else if (lngID == 0)
                {
                    boolNewLicense = true;
                    chkFees.CheckState = Wisej.Web.CheckState.Checked;
                    chkLicense.CheckState = Wisej.Web.CheckState.Checked;
                    boolChangedData = true;
                    t2kIssueDate.Text = Strings.Format(modGNBas.Statics.typTransaction.TransactionDate, "MM/dd/yyyy");
                    t2kIssueDate.Enabled = false;
                }
                boolOKToValidate = true;
                Show(FCForm.FormShowEnum.Modal);
                Init = boolMadeTrans && boolSaved;
                return Init;
            }
            catch (Exception ex)
            {
            ErrorHandler:
                ;
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return Init;
            }
        }

        private void chkFees_CheckedChanged(object sender, System.EventArgs e)
        {
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
            if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
            {
                framFees.Visible = true;
            }
            else
            {
                framFees.Visible = false;
                RecalcAmount();
            }
        }

        private void chkLateFee_CheckedChanged(object sender, System.EventArgs e)
        {
            RecalcAmount();
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
        }

        private void chkLicense_CheckedChanged(object sender, System.EventArgs e)
        {
            RecalcAmount();
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
        }

        private void chkWarrantFee_CheckedChanged(object sender, System.EventArgs e)
        {
            RecalcAmount();
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
        }

        private void cmbLicenseNumber_TextChanged(object sender, System.EventArgs e)
        {
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
        }

        private void cmdOKAddDogs_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
            int x;
            framAddDogs.Visible = false;
            for (x = 1; x <= (GridAddDogs.Rows - 1); x++)
            {
                if (FCConvert.CBool(GridAddDogs.TextMatrix(x, COLADDDOG)))
                {
                    GridDogs.Rows += 1;
                    GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGNAME, GridAddDogs.TextMatrix(x, COLADDDOGNAME));
                    GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGBREED, GridAddDogs.TextMatrix(x, COLADDDOGBREED));
                    GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGNUM, GridAddDogs.TextMatrix(x, COLADDDOGNUM));
                    GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGSEX, GridAddDogs.TextMatrix(x, COLADDDOGSEX));
                    GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGSTICKER, GridAddDogs.TextMatrix(x, COLADDDOGSTICKER));
                    GridDogs.TextMatrix(GridDogs.Rows - 1, COLRABIES, GridAddDogs.TextMatrix(x, COLADDDOGRABIES));
                    GridDogs.TextMatrix(GridDogs.Rows - 1, COLDOGUPDATED, FCConvert.ToString(false));
                }
            }
            // x
        }

        private void frmNewKennel_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
                case Keys.Return:
                    {
                        KeyCode = (Keys)0;
                        Support.SendKeys("{TAB}", false);
                        //App.DoEvents();
                        break;
                    }
            }
            //end switch
        }

        private void frmNewKennel_Load(object sender, System.EventArgs e)
        {

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            if (lngLicenseID > 0)
            {
                mnuViewDocs.Enabled = true;
                imgDocuments.Visible = CheckForDocs(lngLicenseID, "Kennel");
            }
            else
            {
                mnuViewDocs.Enabled = false;
                imgDocuments.Visible = false;
            }
        }
        // vbPorter upgrade warning: lngLicense As int	OnWriteFCConvert.ToDouble(
        private void FillLicenseCombo(int lngLicense = 0)
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            int x;
            cmbLicenseNumber.Clear();
            if (lngLicense > 0)
            {
                clsLoad.OpenRecordset("select * from doginventory where ((status <> 'Used') or ID = " + FCConvert.ToString(lngLicense) + ") and Type = 'K' order by stickernumber", "twck0000.vb1");
            }
            else
            {
                clsLoad.OpenRecordset("select * from doginventory where status <> 'Used' and Type = 'K' order by stickernumber", "twck0000.vb1");
            }
            while (!clsLoad.EndOfFile())
            {
                cmbLicenseNumber.AddItem(FCConvert.ToString(clsLoad.Get_Fields_Int32("stickernumber")));
                cmbLicenseNumber.ItemData(cmbLicenseNumber.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID")));
                clsLoad.MoveNext();
            }
            if (lngLicense > 0 && cmbLicenseNumber.Items.Count > 0)
            {
                for (x = 0; x <= cmbLicenseNumber.Items.Count - 1; x++)
                {
                    if (cmbLicenseNumber.ItemData(x) == lngLicense)
                    {
                        cmbLicenseNumber.SelectedIndex = x;
                        break;
                    }
                }
                // x
            }
        }

        private void ClearControls()
        {
            FillLicenseCombo();
            txtAmount.Text = "";
            chkFees.CheckState = Wisej.Web.CheckState.Unchecked;
            chkLateFee.CheckState = Wisej.Web.CheckState.Unchecked;
            chkLicense.CheckState = Wisej.Web.CheckState.Unchecked;
            chkWarrantFee.CheckState = Wisej.Web.CheckState.Unchecked;
            t2kInspectionDate.Text = "";
            t2kIssueDate.Text = "";
            GridDogs.Rows = 1;
        }

        private void LoadInfo()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            // vbPorter upgrade warning: dtIssueDate As DateTime	OnWrite
            DateTime dtIssueDate;
            // vbPorter upgrade warning: dtReIssueDate As DateTime	OnWrite
            DateTime dtReIssueDate;
            string[] strAry = null;
            int x;
            clsLoad.OpenRecordset("select firstname,lastname,designation from dogowner inner join " + clsLoad.CurrentPrefix + "CentralParties.dbo.Parties AS p ON dogowner.PartyID = p.ID where dogowner.ID = " + FCConvert.ToString(lngOwnerNum), "twck0000.vb1");
            lblOwnerName.Text = fecherFoundation.Strings.Trim(clsLoad.Get_Fields_String("firstname") + " " + clsLoad.Get_Fields_String("Lastname") + " " + clsLoad.Get_Fields_String("designation"));
            if (lngLicenseID == 0)
            {
                ClearControls();
                txtYear.Text = FCConvert.ToString(lngYearForDogs);
            }
            else
            {
                clsLoad.OpenRecordset("select * from kennellicense where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    txtYear.Text = FCConvert.ToString(lngYearForDogs);
                    if (Information.IsDate(clsLoad.Get_Fields("reissuedate")))
                    {
                        CurKennelTrans.OldIssueDate = (DateTime)clsLoad.Get_Fields_DateTime("ReIssueDate");
                    }
                    CurKennelTrans.OldYear = FCConvert.ToInt32(clsLoad.Get_Fields("year"));
                    FillLicenseCombo(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("Licenseid"))));
                    if (!boolReRegistration)
                    {
                        if (Information.IsDate(clsLoad.Get_Fields("originalissuedate")))
                        {
                            dtIssueDate = (DateTime)clsLoad.Get_Fields_DateTime("originalissuedate");
                        }
                        else
                        {
                            dtIssueDate = DateTime.FromOADate(0);
                        }
                        if (Information.IsDate(clsLoad.Get_Fields("reissuedate")))
                        {
                            dtReIssueDate = (DateTime)clsLoad.Get_Fields_DateTime("Reissuedate");
                        }
                        else
                        {
                            dtReIssueDate = DateTime.FromOADate(0);
                        }
                        if (dtReIssueDate.ToOADate() != 0)
                        {
                            t2kIssueDate.Text = Strings.Format(dtReIssueDate, "MM/dd/yyyy");
                        }
                        else
                        {
                            t2kIssueDate.Text = Strings.Format(dtIssueDate, "MM/dd/yyyy");
                        }
                    }
                    else
                    {
                        t2kIssueDate.Text = Strings.Format(curTrans.TransactionDate, "MM/dd/yyyy");
                        t2kIssueDate.Enabled = false;
                    }
                    if (Information.IsDate(clsLoad.Get_Fields("inspectiondate")))
                    {
                        dtIssueDate = (DateTime)clsLoad.Get_Fields_DateTime("inspectiondate");
                    }
                    else
                    {
                        dtIssueDate = DateTime.FromOADate(0);
                    }
                    if (dtIssueDate.ToOADate() != 0)
                    {
                        t2kInspectionDate.Text = Strings.Format(dtIssueDate, "MM/dd/yyyy");
                    }
                    else
                    {
                        t2kInspectionDate.Text = "";
                    }
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("dognumbers"))) != string.Empty)
                    {
                        FillGridDogs(clsLoad.Get_Fields_String("dognumbers"));
                    }
                    else
                    {
                        GridDogs.Rows = 1;
                    }
                }
                else
                {
                    ClearControls();
                }
            }
            if (boolReRegistration)
            {
                chkFees.CheckState = Wisej.Web.CheckState.Checked;
                chkFees.Enabled = false;
                chkLicense.CheckState = Wisej.Web.CheckState.Checked;
                chkLicense.Enabled = false;
            }
        }

        private void FillGridDogs(string strDogNumbers)
        {
            string[] strAry = null;
            // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
            int x;
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strBreed = "";
            string strRabies = "";
            string strSex = "";
            strAry = Strings.Split(strDogNumbers, ",", -1, CompareConstants.vbTextCompare);
            for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
            {
                clsLoad.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[x])), "twck0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    strBreed = FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed"));
                    strSex = fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("dogsex")));
                    if (strSex != "M" && strSex != "F")
                    {
                        strSex = "";
                    }
                    if (Information.IsDate(clsLoad.Get_Fields("rabiesexpdate")))
                    {
                        if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("rabiesexpdate")).ToOADate() != 0)
                        {
                            strRabies = Strings.Format(clsLoad.Get_Fields_DateTime("rabiesexpdate"), "MM/dd/yyyy");
                        }
                        else
                        {
                            strRabies = "";
                        }
                    }
                    else
                    {
                        strRabies = "";
                    }
                    GridDogs.AddItem(clsLoad.Get_Fields_Int32("ID") + "\t" + clsLoad.Get_Fields_String("dogname") + "\t" + strBreed + "\t" + strSex + "\t" + clsLoad.Get_Fields_String("stickerlicnum") + "\t" + strRabies + "\t" + false);
                }
            }
            // x
        }

        private void SetupGridDogs()
        {
            GridDogs.Cols = 7;
            GridDogs.ColHidden(COLDOGNUM, true);
            GridDogs.ColHidden(COLDOGUPDATED, true);
            GridDogs.ExtendLastCol = true;
            GridDogs.TextMatrix(0, COLDOGNAME, "Name");
            GridDogs.TextMatrix(0, COLDOGBREED, "Breed");
            GridDogs.TextMatrix(0, COLDOGSTICKER, "Sticker Number");
            GridDogs.TextMatrix(0, COLRABIES, "Rabies Expiration");
            GridDogs.TextMatrix(0, COLDOGSEX, "Sex");
        }

        private void ResizeGridDogs()
        {
            int GridWidth = 0;
            GridWidth = GridDogs.WidthOriginal;
            GridDogs.ColWidth(COLDOGNAME, FCConvert.ToInt32(0.24 * GridWidth));
            GridDogs.ColWidth(COLDOGBREED, FCConvert.ToInt32(0.3 * GridWidth));
            GridDogs.ColWidth(COLDOGSTICKER, FCConvert.ToInt32(0.17 * GridWidth));
            GridDogs.ColWidth(COLDOGSEX, FCConvert.ToInt32(0.06 * GridWidth));
        }
        // vbPorter upgrade warning: Cancel As int	OnWrite(bool)
        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            if (boolChangedData)
            {
                if (MessageBox.Show("Data has been changed but not saved, do you wish to save now?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (!SaveData())
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                if (boolReRegistration && boolSaved)
                {
                    int x;
                    for (x = 1; x <= GridDogs.Rows - 1; x++)
                    {
                        if (!FCConvert.CBool(GridDogs.TextMatrix(x, COLDOGUPDATED)))
                        {
                            if (MessageBox.Show("You have re-registered but not updated all dogs. Do you still wish to exit?", "Dogs Not Updated", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                return;
                            }
                            else
                            {
                                e.Cancel = true;
                                return;
                            }
                        }
                    }
                    // x
                }
            }
        }

        private void frmNewKennel_Resize(object sender, System.EventArgs e)
        {
            ResizeGridDogs();
            ResizeGridAddDogs();
        }

        private void GridAddDogs_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (GridAddDogs.Col != COLADDDOG)
            {
                e.Cancel = true;
            }
        }

        private void GridDogs_DblClick(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
            DateTime dtDate;
            int lngRow;
            int lngDog = 0;
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strSex = "";
            string strRabies = "";
            if (GridDogs.MouseRow < 1)
                return;
            lngRow = GridDogs.MouseRow;
            if (lngLicenseID == 0)
            {
                if (!Information.IsDate(t2kIssueDate.Text))
                {
                    MessageBox.Show("You must enter a valid transaction date before continuing", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            if (boolReRegistration || lngLicenseID == 0)
            {
                dtDate = FCConvert.ToDateTime(Strings.Format(t2kIssueDate.Text, "MM/dd/yyyy"));
                lngDog = frmNewDog.InstancePtr.Init(FCConvert.ToInt32(GridDogs.TextMatrix(lngRow, COLDOGNUM)), lngOwnerNum, boolReRegistration, true, lngYearForDogs).dogId;

            }
            else
            {
                lngDog = frmNewDog.InstancePtr.Init(FCConvert.ToInt32(GridDogs.TextMatrix(lngRow, COLDOGNUM)), lngOwnerNum, boolReRegistration, true, lngYearForDogs).dogId;
            }
            if (!boolReadOnly)
            {
                GridDogs.TextMatrix(lngRow, COLDOGUPDATED, FCConvert.ToString(true));
                clsLoad.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDog), "twck0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    GridDogs.TextMatrix(lngRow, COLDOGBREED, FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed")));
                    strSex = fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("dogsex")));
                    if (strSex != "M" && strSex != "F")
                    {
                        strSex = "";
                    }
                    GridDogs.TextMatrix(lngRow, COLDOGSEX, strSex);
                    if (Information.IsDate(clsLoad.Get_Fields("rabiesexpdate")))
                    {
                        if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("rabiesexpdate")).ToOADate() != 0)
                        {
                            strRabies = Strings.Format(clsLoad.Get_Fields_DateTime("rabiesexpdate"), "MM/dd/yyyy");
                        }
                        else
                        {
                            strRabies = "";
                        }
                    }
                    else
                    {
                        strRabies = "";
                    }
                    GridDogs.TextMatrix(lngRow, COLRABIES, strRabies);
                    GridDogs.TextMatrix(lngRow, COLDOGNAME, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("dogname"))));
                    GridDogs.TextMatrix(lngRow, COLDOGSTICKER, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("stickerlicnum"))));
                }
            }
        }

        private void mnuAddExistingDog_Click(object sender, System.EventArgs e)
        {
            SetupGridAddDogs();
            FillGridAddDogs();
            framAddDogs.Visible = true;
            framAddDogs.BringToFront();
            framAddDogs.CenterToContainer(ClientArea);
        }

        private void FillGridAddDogs()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strBreed = "";
            string strSex = "";
            string strRabies = "";
            // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
            int x;
            bool boolOkToAdd = false;
            GridAddDogs.Rows = 1;
            clsLoad.OpenRecordset("select * from doginfo where ownernum = " + FCConvert.ToString(lngOwnerNum) + " and kenneldog <> 1 and dogdeceased <> 1 order by dogname", "twck0000.vb1");
            while (!clsLoad.EndOfFile())
            {
                boolOkToAdd = true;
                for (x = 1; x <= (GridDogs.Rows - 1); x++)
                {
                    if (Conversion.Val(GridDogs.TextMatrix(x, COLDOGNUM)) == Conversion.Val(clsLoad.Get_Fields_Int32("ID")))
                    {
                        boolOkToAdd = false;
                        break;
                    }
                }
                // x
                if (boolOkToAdd)
                {
                    strBreed = FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed"));
                    strSex = fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("dogsex")));
                    if (strSex != "M" && strSex != "F")
                    {
                        strSex = "";
                    }
                    if (Information.IsDate(clsLoad.Get_Fields("rabiesexpdate")))
                    {
                        if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("rabiesexpdate")).ToOADate() != 0)
                        {
                            strRabies = Strings.Format(clsLoad.Get_Fields_DateTime("rabiesexpdate"), "MM/dd/yyyy");
                        }
                        else
                        {
                            strRabies = "";
                        }
                    }
                    else
                    {
                        strRabies = "";
                    }
                    GridAddDogs.AddItem(false + "\t" + clsLoad.Get_Fields_String("dogname") + "\t" + clsLoad.Get_Fields_Int32("ID") + "\t" + strBreed + "\t" + clsLoad.Get_Fields_String("stickerlicnum") + "\t" + strSex + "\t" + strRabies);
                }
                clsLoad.MoveNext();
            }
        }

        private void SetupGridAddDogs()
        {
            GridAddDogs.Cols = 7;
            GridAddDogs.TextMatrix(0, COLADDDOG, "");
            GridAddDogs.TextMatrix(0, COLADDDOGNAME, "Name");
            GridAddDogs.ColHidden(COLADDDOGNUM, true);
            GridAddDogs.ColHidden(COLADDDOGBREED, true);
            GridAddDogs.ColHidden(COLADDDOGRABIES, true);
            GridAddDogs.ColHidden(COLADDDOGSEX, true);
            GridAddDogs.ColHidden(COLADDDOGSTICKER, true);
            GridAddDogs.ColDataType(COLADDDOG, FCGrid.DataTypeSettings.flexDTBoolean);
        }

        private void ResizeGridAddDogs()
        {
            int GridWidth = 0;
            GridWidth = GridAddDogs.WidthOriginal;
            GridAddDogs.ColWidth(COLADDDOG, FCConvert.ToInt32(0.1 * GridWidth));
        }

        private void mnuAddNewDog_Click(object sender, System.EventArgs e)
        {
            DateTime dtDate;
            int lngDNum;
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strBreed = "";
            string strSex = "";
            string strRabies = "";
            if (!Information.IsDate(t2kIssueDate.Text))
            {
                MessageBox.Show("You must enter a valid transaction date before continuing", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            lngDNum = frmNewDog.InstancePtr.Init(0, lngOwnerNum, false, true, lngYearForDogs).dogId;
            clsLoad.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDNum), "twck0000.vb1");
            if (!clsLoad.EndOfFile())
            {
                strBreed = FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed"));
                strSex = fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("dogsex")));
                if (strSex != "M" && strSex != "F")
                {
                    strSex = "";
                }
                if (Information.IsDate(clsLoad.Get_Fields("rabiesexpdate")))
                {
                    if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("rabiesexpdate")).ToOADate() != 0)
                    {
                        strRabies = Strings.Format(clsLoad.Get_Fields_DateTime("rabiesexpdate"), "MM/dd/yyyy");
                    }
                    else
                    {
                        strRabies = "";
                    }
                }
                else
                {
                    strRabies = "";
                }
                GridDogs.AddItem(clsLoad.Get_Fields_Int32("ID") + "\t" + clsLoad.Get_Fields_String("dogname") + "\t" + strBreed + "\t" + strSex + "\t" + clsLoad.Get_Fields_String("stickerlicnum") + "\t" + strRabies + "\t" + true);
            }
        }

        private void mnuDeleteKennel_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper clsSave = new clsDRWrapper();
            // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
            int x;
            int lngLicNum;
            if (MessageBox.Show("This will delete this license and return dogs from kennel.  Continue?", "Delete License?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            for (x = 1; x <= (GridDogs.Rows - 1); x++)
            {
                clsSave.Execute("update doginfo set kenneldog = 0,dogtokenneldate = '12:00:00 AM' where ID = " + GridDogs.TextMatrix(x, COLDOGNUM), "twck0000.vb1");
            }
            // x
            GridDogs.Rows = 1;
            lngLicNum = 0;
            clsSave.OpenRecordset("select * from kennellicense where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
            if (!clsSave.EndOfFile())
            {
                lngLicNum = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields_Int32("licenseid"))));
            }
            if (lngLicNum > 0 || cmbLicenseNumber.SelectedIndex >= 0)
            {
                if (MessageBox.Show("Return license number to inventory?", "Return license?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    // just in case the license is saved with one number but it is changed on the screen, must do both
                    if (lngLicNum > 0)
                    {
                        clsSave.Execute("update doginventory set status = 'Active' where ID = " + FCConvert.ToString(lngLicNum) + " and type = 'K'", "twck0000.vb1");
                    }
                    lngLicNum = cmbLicenseNumber.ItemData(cmbLicenseNumber.SelectedIndex);
                    clsSave.Execute("update doginventory set status = 'Active' where ID = " + FCConvert.ToString(lngLicNum) + " and type = 'K'", "twck0000.vb1");
                }
            }
            if (lngLicenseID > 0)
            {
                clsSave.Execute("delete from kennellicense where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
            }
            modGlobalFunctions.AddCYAEntry("CK", "Deleted Kennel License " + FCConvert.ToString(lngLicenseID), "Owner " + FCConvert.ToString(lngOwnerNum), "", "", "");
            boolChangedData = false;
            boolMadeTrans = false;
            mnuExit_Click();
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void mnuPrint_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsLicense = new clsDRWrapper();
            if (boolChangedData)
            {
                if (!SaveData())
                {
                    MessageBox.Show("Save Failed. Cannot print license.", "Print Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            rsLicense.OpenRecordset("Select * from PrinterSettings", "twck0000.vb1");
            if (!rsLicense.EndOfFile())
            {
                if (FCConvert.ToInt32(rsLicense.Get_Fields_Int16("KennelLicense")) == 1)
                {
                    rptNewKennelLicenseLaser.InstancePtr.Init(lngLicenseID, lngOwnerNum);
                }
                else
                {
                    rptKennelLicense.InstancePtr.Init(lngLicenseID, lngOwnerNum);
                }
            }
            else
            {
                rptKennelLicense.InstancePtr.Init(lngLicenseID, lngOwnerNum);
            }
        }

        private void mnuPrintBack_Click(object sender, System.EventArgs e)
        {
            if (boolChangedData)
            {
                if (!SaveData())
                {
                    MessageBox.Show("Save failed.  Cannot print license.", "Print Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            rptBackKennelLicense.InstancePtr.Init(lngLicenseID, (chkLateFee.CheckState == Wisej.Web.CheckState.Checked || chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked) && chkFees.CheckState == Wisej.Web.CheckState.Checked);
        }

        private void mnuRemoveDog_Click(object sender, System.EventArgs e)
        {
            int lngDNum;
            clsDRWrapper clsSave = new clsDRWrapper();
            // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
            int x;
            string strDogList = "";
            string[] aryList = null;
            if (GridDogs.Row < 1)
                return;
            lngDNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDogs.TextMatrix(GridDogs.Row, COLDOGNUM))));
            boolChangedData = true;
            clsSave.Execute("update doginfo set kenneldog = 0,dogtokenneldate = '12:00:00 AM' where ID = " + FCConvert.ToString(lngDNum), "twck0000.vb1");
            GridDogs.RemoveItem(GridDogs.Row);
            if (lngLicenseID > 0)
            {
                strDogList = "";
                clsSave.OpenRecordset("Select * from kennellicense where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
                strDogList = FCConvert.ToString(clsSave.Get_Fields_String("DogNumbers"));
                aryList = Strings.Split(strDogList, ",", -1, CompareConstants.vbTextCompare);
                strDogList = "";
                for (x = 0; x <= (Information.UBound(aryList, 1)); x++)
                {
                    if (Conversion.Val(aryList[x]) > 0)
                    {
                        if (Conversion.Val(aryList[x]) != lngDNum)
                        {
                            strDogList += FCConvert.ToString(Conversion.Val(aryList[x])) + ",";
                        }
                    }
                }
                // x
                if (strDogList != string.Empty)
                {
                    // get rid of the extra comma at the end
                    strDogList = Strings.Mid(strDogList, 1, strDogList.Length - 1);
                }
                clsSave.Execute("update kennellicense set dognumbers = '" + strDogList + "' where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
            }
        }

        private void mnuSave_Click(object sender, System.EventArgs e)
        {
            SaveData();
        }

        private void mnuSaveExit_Click(object sender, System.EventArgs e)
        {
            if (SaveData())
            {
                mnuExit_Click();
            }
        }

        private void mnuViewDocs_Click(object sender, System.EventArgs e)
        {
            ViewDocs();
        }

        private void t2kInspectionDate_Change(object sender, System.EventArgs e)
        {
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
        }

        private void t2kIssueDate_Change(object sender, System.EventArgs e)
        {
            if (boolOKToValidate)
            {
                boolChangedData = true;
            }
        }

        private bool SaveData()
        {
            bool SaveData = false;
            clsDRWrapper clsSave = new clsDRWrapper();
            // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
            int x;
            string strDogList;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                SaveData = false;
                if (cmbLicenseNumber.Items.Count < 1)
                {
                    MessageBox.Show("You must assign a valid license number before you can save", "No License Chosen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return SaveData;
                }
                if (cmbLicenseNumber.SelectedIndex < 0)
                {
                    MessageBox.Show("You must assign a valid license number before you can save", "No License Chosen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return SaveData;
                }
                strDogList = "";
                for (x = 0; x <= (GridDogs.Rows - 1); x++)
                {
                    if (Conversion.Val(GridDogs.TextMatrix(x, COLDOGNUM)) > 0)
                    {
                        clsSave.Execute("update doginfo set kenneldog = 1 where ID = " + FCConvert.ToString(Conversion.Val(GridDogs.TextMatrix(x, COLDOGNUM))), "twck0000.vb1");
                        clsSave.Execute("update doginfo set dogtokenneldate = '" + Strings.Format(curTrans.TransactionDate, "MM/dd/yyyy") + "' where ((isdate(dogtokenneldate)=0) or dogtokenneldate = '12:00 AM'  ) and ID = " + FCConvert.ToString(Conversion.Val(GridDogs.TextMatrix(x, COLDOGNUM))), "twck0000.vb1");
                        clsSave.Execute("update doginfo set [year] = " + FCConvert.ToString(lngYearForDogs) + " where ID = " + FCConvert.ToString(Conversion.Val(GridDogs.TextMatrix(x, COLDOGNUM))) + " and [year] < " + FCConvert.ToString(lngYearForDogs), "twck0000.vb1");
                        strDogList += FCConvert.ToString(Conversion.Val(GridDogs.TextMatrix(x, COLDOGNUM))) + ",";
                    }
                }
                // x
                if (strDogList != string.Empty)
                {
                    // get rid of the extra comma at the end
                    strDogList = Strings.Mid(strDogList, 1, strDogList.Length - 1);
                }
                clsSave.OpenRecordset("Select * from kennellicense where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
                if (lngLicenseID == 0 || clsSave.EndOfFile())
                {
                    clsSave.AddNew();
                    // lngLicenseID = clsSave.Fields("ID")
                    if (Information.IsDate(t2kIssueDate.Text))
                    {
                        clsSave.Set_Fields("originalissuedate", Strings.Format(t2kIssueDate.Text, "MM/dd/yyyy"));
                    }
                    else
                    {
                        clsSave.Set_Fields("originalissuedate", 0);
                    }
                }
                else
                {
                    clsSave.Edit();
                }
                clsSave.Set_Fields("Year", lngYearForDogs);
                // CurKennelTrans.KennelLicenseID = lngLicenseID
                clsSave.Set_Fields("DogNumbers", strDogList);
                CurKennelTrans.KennelDogs = strDogList;
                CurKennelTrans.OwnerNum = lngOwnerNum;
                if (Information.IsDate(t2kInspectionDate.Text))
                {
                    clsSave.Set_Fields("InspectionDate", Strings.Format(t2kInspectionDate.Text, "MM/dd/yyyy"));
                }
                else
                {
                    clsSave.Set_Fields("InspectionDate", 0);
                }
                if (Information.IsDate(t2kIssueDate.Text))
                {
                    clsSave.Set_Fields("reissuedate", Strings.Format(t2kIssueDate.Text, "MM/dd/yyyy"));
                }
                else
                {
                    clsSave.Set_Fields("reissuedate", 0);
                }
                clsSave.Set_Fields("OwnerNum", lngOwnerNum);
                if (cmbLicenseNumber.Items.Count > 0 && cmbLicenseNumber.SelectedIndex >= 0)
                {
                    clsSave.Set_Fields("licenseid", cmbLicenseNumber.ItemData(cmbLicenseNumber.SelectedIndex));
                }
                else
                {
                    clsSave.Set_Fields("licenseid", 0);
                }
                CurKennelTrans.KennelLic1Num = FCConvert.ToInt32(clsSave.Get_Fields_Int32("licenseid"));
                clsSave.Update();
                lngLicenseID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
                CurKennelTrans.KennelLicenseID = lngLicenseID;
                if (cmbLicenseNumber.Items.Count > 0 && cmbLicenseNumber.SelectedIndex >= 0)
                {
                    clsSave.Execute("update doginventory set status = 'Used' where ID = " + FCConvert.ToString(cmbLicenseNumber.ItemData(cmbLicenseNumber.SelectedIndex)) + " and type = 'K'", "twck0000.vb1");
                }
                boolChangedData = false;
                boolSaved = true;
                SaveData = true;
                mnuViewDocs.Enabled = true;
                // boolNewLicense = False
                if (boolMadeTrans)
                {
                    CopyToGlobalTrans();
                }
                MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return SaveData;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return SaveData;
        }

        private void RecalcAmount()
        {
            double dblAmount;
            double dblKennelFee = 0;
            double dblLateFee;
            double dblKennelLateFee = 0;
            double dblKennelWarrantFee = 0;
            clsDRWrapper clsKennel = new clsDRWrapper();
            double dblStateFee;
            double dblClerkFee;
            double dblTownFee;
            double dblAmount1;
            double dblAmount2;
            double dblamount3;
            double dblamount4;
            double dblamount5;
            double dblamount6;
            dblAmount1 = 0;
            dblAmount2 = 0;
            dblAmount2 = 0;
            dblamount3 = 0;
            dblamount4 = 0;
            dblamount5 = 0;
            dblamount6 = 0;
            dblLateFee = 0;
            dblAmount = 0;
            dblStateFee = 0;
            dblTownFee = 0;
            dblClerkFee = 0;
            modDogs.GetDogFees();
            if (chkFees.CheckState == Wisej.Web.CheckState.Checked)
            {
                dblKennelFee = modDogs.Statics.DogFee.KennelClerk + modDogs.Statics.DogFee.KennelState + modDogs.Statics.DogFee.KennelTown;
                dblKennelWarrantFee = modDogs.Statics.DogFee.KennelWarrantFee;
                dblKennelLateFee = modDogs.Statics.DogFee.KennelLateFee;
                if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
                {
                    dblClerkFee = modDogs.Statics.DogFee.KennelClerk;
                    dblStateFee = modDogs.Statics.DogFee.KennelState;
                    dblTownFee = modDogs.Statics.DogFee.KennelTown;
                }
                if (chkLateFee.CheckState == Wisej.Web.CheckState.Checked)
                {
                    dblLateFee += dblKennelLateFee;
                    CurKennelTrans.KennelLateFee = dblKennelLateFee;
                }
                if (chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked)
                {
                    dblLateFee += dblKennelWarrantFee;
                    CurKennelTrans.KennelWarrantFee = dblKennelWarrantFee;
                }
                dblAmount2 = dblTownFee;
                dblamount3 = dblStateFee;
                dblamount4 = dblClerkFee;
                dblamount5 = dblLateFee;
                dblAmount = dblAmount1 + dblAmount2 + dblamount3 + dblamount4 + dblamount5 + dblamount6;
                curTrans.Amount1 = dblAmount1;
                curTrans.Amount2 = dblAmount2;
                curTrans.Amount3 = dblamount3;
                curTrans.Amount4 = dblamount4;
                curTrans.Amount5 = dblamount5;
                curTrans.Amount6 = dblamount6;
                curTrans.TotalAmount = dblAmount;
                CurKennelTrans.Amount = dblAmount;
                CurKennelTrans.ClerkFee = dblamount4;
                CurKennelTrans.StateFee = dblamount3;
                CurKennelTrans.TownFee = dblAmount2;
                CurKennelTrans.LateFee = dblamount5;
                CurKennelTrans.boolKennel = true;
                CurKennelTrans.boolLicense = false;
                CurKennelTrans.Amount = dblAmount;
                CurKennelTrans.KennelLic1Fee = 0;
                if (cmbLicenseNumber.Items.Count > 0 && cmbLicenseNumber.SelectedIndex >= 0)
                {
                    CurKennelTrans.KennelLic1Num = cmbLicenseNumber.ItemData(cmbLicenseNumber.SelectedIndex);
                }
                else
                {
                    CurKennelTrans.KennelLic1Num = 0;
                }
                CurKennelTrans.KennelLicenseID = lngLicenseID;
                if (chkLicense.CheckState == Wisej.Web.CheckState.Checked)
                {
                    CurKennelTrans.boolLicense = true;
                    CurKennelTrans.Description = "Kennel License";
                    CurKennelTrans.KennelLic1Fee = dblKennelFee;
                }
                else if (chkLateFee.CheckState == Wisej.Web.CheckState.Checked)
                {
                    CurKennelTrans.Description = "Kennel Late Fee";
                }
                else if (chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked)
                {
                    CurKennelTrans.Description = "Kennel Warrant Fee";
                }
                curTrans.Description = CurKennelTrans.Description;
                CurKennelTrans.TransactionDate = curTrans.TransactionDate;
                curTrans.TransactionType = modGNBas.DOGTRANSACTION;
                txtAmount.Text = Strings.Format(dblAmount, "0.00");
                if (chkLicense.CheckState == Wisej.Web.CheckState.Checked || chkLateFee.CheckState == Wisej.Web.CheckState.Checked || chkWarrantFee.CheckState == Wisej.Web.CheckState.Checked)
                {
                    boolMadeTrans = true;
                }
                else
                {
                    boolMadeTrans = false;
                }
            }
            else
            {
                boolMadeTrans = false;
            }
        }

        private bool ValidateData()
        {
            bool ValidateData = false;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                ValidateData = false;
                ValidateData = true;
                return ValidateData;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ValidateData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return ValidateData;
        }

        private void CopyToGlobalTrans()
        {
            modGNBas.Statics.typTransaction.Amount1 = curTrans.Amount1;
            modGNBas.Statics.typTransaction.Amount2 = curTrans.Amount2;
            modGNBas.Statics.typTransaction.Amount3 = curTrans.Amount3;
            modGNBas.Statics.typTransaction.Amount4 = curTrans.Amount4;
            modGNBas.Statics.typTransaction.Amount5 = curTrans.Amount5;
            modGNBas.Statics.typTransaction.Amount6 = curTrans.Amount6;
            modGNBas.Statics.typTransaction.Description = curTrans.Description;
            modGNBas.Statics.typTransaction.TotalAmount = curTrans.TotalAmount;
            modGNBas.Statics.typTransaction.TransactionDate = curTrans.TransactionDate;
            modGNBas.Statics.typTransaction.TransactionType = curTrans.TransactionType;
            modGNBas.Statics.typTransaction.TownCode = curTrans.TownCode;
            modGNBas.Statics.typDogTransaction.Amount = CurKennelTrans.Amount;
            modGNBas.Statics.typDogTransaction.boolAdjustmentVoid = CurKennelTrans.boolAdjustmentVoid;
            modGNBas.Statics.typDogTransaction.boolHearingGuid = CurKennelTrans.boolHearingGuid;
            modGNBas.Statics.typDogTransaction.boolKennel = CurKennelTrans.boolKennel;
            modGNBas.Statics.typDogTransaction.boolLicense = CurKennelTrans.boolLicense;
            modGNBas.Statics.typDogTransaction.boolReplacementLicense = CurKennelTrans.boolReplacementLicense;
            modGNBas.Statics.typDogTransaction.boolReplacementSticker = CurKennelTrans.boolReplacementSticker;
            modGNBas.Statics.typDogTransaction.boolReplacementTag = CurKennelTrans.boolReplacementTag;
            modGNBas.Statics.typDogTransaction.boolSearchRescue = CurKennelTrans.boolSearchRescue;
            modGNBas.Statics.typDogTransaction.boolSpayNeuter = CurKennelTrans.boolSpayNeuter;
            modGNBas.Statics.typDogTransaction.boolTempLicense = CurKennelTrans.boolTempLicense;
            modGNBas.Statics.typDogTransaction.boolTransfer = CurKennelTrans.boolTransfer;
            modGNBas.Statics.typDogTransaction.ClerkFee = CurKennelTrans.ClerkFee;
            modGNBas.Statics.typDogTransaction.Description = CurKennelTrans.Description;
            modGNBas.Statics.typDogTransaction.DogNumber = CurKennelTrans.DogNumber;
            modGNBas.Statics.typDogTransaction.DogYear = CurKennelTrans.DogYear;
            modGNBas.Statics.typDogTransaction.KennelDogs = CurKennelTrans.KennelDogs;
            modGNBas.Statics.typDogTransaction.KennelLateFee = CurKennelTrans.KennelLateFee;
            modGNBas.Statics.typDogTransaction.KennelLic1Fee = CurKennelTrans.KennelLic1Fee;
            modGNBas.Statics.typDogTransaction.KennelLic1Num = CurKennelTrans.KennelLic1Num;
            modGNBas.Statics.typDogTransaction.KennelLic2Fee = CurKennelTrans.KennelLic2Fee;
            modGNBas.Statics.typDogTransaction.KennelLic2Num = CurKennelTrans.KennelLic2Num;
            modGNBas.Statics.typDogTransaction.KennelLicenseID = CurKennelTrans.KennelLicenseID;
            modGNBas.Statics.typDogTransaction.KennelWarrantFee = CurKennelTrans.KennelWarrantFee;
            modGNBas.Statics.typDogTransaction.LateFee = CurKennelTrans.LateFee;
            modGNBas.Statics.typDogTransaction.OwnerNum = CurKennelTrans.OwnerNum;
            modGNBas.Statics.typDogTransaction.RabiesTagNumber = CurKennelTrans.RabiesTagNumber;
            modGNBas.Statics.typDogTransaction.ReplacementLicenseFee = CurKennelTrans.ReplacementLicenseFee;
            modGNBas.Statics.typDogTransaction.ReplacementStickerFee = CurKennelTrans.ReplacementStickerFee;
            modGNBas.Statics.typDogTransaction.ReplacementTagFee = CurKennelTrans.ReplacementTagFee;
            modGNBas.Statics.typDogTransaction.SpayNonSpayAmount = CurKennelTrans.SpayNonSpayAmount;
            modGNBas.Statics.typDogTransaction.StateFee = CurKennelTrans.StateFee;
            modGNBas.Statics.typDogTransaction.StickerNumber = CurKennelTrans.StickerNumber;
            modGNBas.Statics.typDogTransaction.TAGNUMBER = CurKennelTrans.TAGNUMBER;
            modGNBas.Statics.typDogTransaction.TempLicenseFee = CurKennelTrans.TempLicenseFee;
            modGNBas.Statics.typDogTransaction.TownFee = CurKennelTrans.TownFee;
            modGNBas.Statics.typDogTransaction.TransactionDate = CurKennelTrans.TransactionDate;
            modGNBas.Statics.typDogTransaction.TransactionNumber = CurKennelTrans.TransactionNumber;
            modGNBas.Statics.typDogTransaction.TransferLicenseFee = CurKennelTrans.TransferLicenseFee;
            modGNBas.Statics.typDogTransaction.OldIssueDate = CurKennelTrans.OldIssueDate;
            modGNBas.Statics.typDogTransaction.OldYear = CurKennelTrans.OldYear;
            modGNBas.Statics.typDogTransaction.TownCode = CurKennelTrans.TownCode;
        }

        private void CheckPermissions()
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTEDITDOGSKENNELS)) != "F")
                {
                    boolReadOnly = true;
                    cmdSave.Enabled = false;
                    mnuSaveExit.Enabled = false;
                    cmdAddExistingDog.Enabled = false;
                    cmdAddNewDog.Enabled = false;
                    cmdDeleteKennelLicense.Enabled = false;
                    cmdRemoveDogFromKennel.Enabled = false;
                    txtYear.Enabled = false;
                    t2kInspectionDate.Enabled = false;
                    t2kIssueDate.Enabled = false;
                    txtAmount.Enabled = false;
                    chkFees.Visible = false;
                    cmbLicenseNumber.Enabled = false;
                }
                else
                {
                    boolReadOnly = false;
                    cmdSave.Enabled = true;
                    mnuSaveExit.Enabled = true;
                    cmdAddExistingDog.Enabled = true;
                    cmdAddNewDog.Enabled = true;
                    cmdDeleteKennelLicense.Enabled = true;
                    cmdRemoveDogFromKennel.Enabled = true;
                    txtYear.Enabled = true;
                    t2kInspectionDate.Enabled = true;
                    t2kIssueDate.Enabled = true;
                    txtAmount.Enabled = true;
                    chkFees.Visible = true;
                    cmbLicenseNumber.Enabled = true;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckPermissions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public bool CheckForDocs(int referenceId, string referenceType)
        {
            var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
            var docList = new Collection<CentralDocumentHeader>();

            docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand { DataGroup = "Clerk", ReferenceId = referenceId, ReferenceType = referenceType });

            return docList.Count > 0;
        }
    }
}
