﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmFacilityNames.
	/// </summary>
	partial class frmFacilityNames
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCTextBox txtFacilityNumber;
		public fecherFoundation.FCTextBox txtFacility;
		public fecherFoundation.FCComboBox cboState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCComboBox cboFacility;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtFacilityNumber = new fecherFoundation.FCTextBox();
            this.txtFacility = new fecherFoundation.FCTextBox();
            this.cboState = new fecherFoundation.FCComboBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.cboFacility = new fecherFoundation.FCComboBox();
            this.txtFirstName = new fecherFoundation.FCTextBox();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(564, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtFacilityNumber);
            this.ClientArea.Controls.Add(this.txtFacility);
            this.ClientArea.Controls.Add(this.txtAddress1);
            this.ClientArea.Controls.Add(this.Label1_4);
            this.ClientArea.Controls.Add(this.txtAddress2);
            this.ClientArea.Controls.Add(this.cboState);
            this.ClientArea.Controls.Add(this.Label1_2);
            this.ClientArea.Controls.Add(this.txtLastName);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.cboFacility);
            this.ClientArea.Controls.Add(this.Label1_5);
            this.ClientArea.Controls.Add(this.txtFirstName);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.Label1_8);
            this.ClientArea.Controls.Add(this.Label1_6);
            this.ClientArea.Controls.Add(this.Label1_7);
            this.ClientArea.Controls.Add(this.Label1_3);
            this.ClientArea.Controls.Add(this.Label1_1);
            this.ClientArea.Size = new System.Drawing.Size(564, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Size = new System.Drawing.Size(564, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(388, 30);
            this.HeaderText.Text = "Funeral Establishment Information";
            // 
            // txtFacilityNumber
            // 
            this.txtFacilityNumber.AutoSize = false;
            this.txtFacilityNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtFacilityNumber.LinkItem = null;
            this.txtFacilityNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFacilityNumber.LinkTopic = null;
            this.txtFacilityNumber.Location = new System.Drawing.Point(203, 90);
            this.txtFacilityNumber.Name = "txtFacilityNumber";
            this.txtFacilityNumber.Size = new System.Drawing.Size(325, 40);
            this.txtFacilityNumber.TabIndex = 1;
            // 
            // txtFacility
            // 
            this.txtFacility.AutoSize = false;
            this.txtFacility.BackColor = System.Drawing.SystemColors.Window;
            this.txtFacility.LinkItem = null;
            this.txtFacility.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFacility.LinkTopic = null;
            this.txtFacility.Location = new System.Drawing.Point(203, 510);
            this.txtFacility.Name = "txtFacility";
            this.txtFacility.Size = new System.Drawing.Size(325, 40);
            this.txtFacility.TabIndex = 8;
            // 
            // cboState
            // 
            this.cboState.AutoSize = false;
            this.cboState.BackColor = System.Drawing.SystemColors.Window;
            this.cboState.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboState.FormattingEnabled = true;
            this.cboState.Location = new System.Drawing.Point(203, 390);
            this.cboState.Name = "cboState";
            this.cboState.Size = new System.Drawing.Size(325, 40);
            this.cboState.Sorted = true;
            this.cboState.TabIndex = 6;
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(203, 330);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(325, 40);
            this.txtCity.TabIndex = 5;
            // 
            // txtZip
            // 
            this.txtZip.AutoSize = false;
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.LinkItem = null;
            this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip.LinkTopic = null;
            this.txtZip.Location = new System.Drawing.Point(203, 450);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(325, 40);
            this.txtZip.TabIndex = 7;
            // 
            // txtAddress1
            // 
            this.txtAddress1.AutoSize = false;
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.LinkItem = null;
            this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress1.LinkTopic = null;
            this.txtAddress1.Location = new System.Drawing.Point(203, 210);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(325, 40);
            this.txtAddress1.TabIndex = 3;
            // 
            // txtAddress2
            // 
            this.txtAddress2.AutoSize = false;
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.LinkItem = null;
            this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress2.LinkTopic = null;
            this.txtAddress2.Location = new System.Drawing.Point(203, 270);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(325, 40);
            this.txtAddress2.TabIndex = 4;
            // 
            // txtLastName
            // 
            this.txtLastName.AutoSize = false;
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.LinkItem = null;
            this.txtLastName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLastName.LinkTopic = null;
            this.txtLastName.Location = new System.Drawing.Point(203, 150);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(325, 40);
            this.txtLastName.TabIndex = 2;
            // 
            // cboFacility
            // 
            this.cboFacility.AutoSize = false;
            this.cboFacility.BackColor = System.Drawing.SystemColors.Window;
            this.cboFacility.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboFacility.FormattingEnabled = true;
            this.cboFacility.Location = new System.Drawing.Point(30, 30);
            this.cboFacility.Name = "cboFacility";
            this.cboFacility.Size = new System.Drawing.Size(498, 40);
            this.cboFacility.Sorted = true;
            this.cboFacility.TabIndex = 0;
            this.cboFacility.SelectedIndexChanged += new System.EventHandler(this.cboFacility_SelectedIndexChanged);
            // 
            // txtFirstName
            // 
            this.txtFirstName.AutoSize = false;
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.LinkItem = null;
            this.txtFirstName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFirstName.LinkTopic = null;
            this.txtFirstName.Location = new System.Drawing.Point(230, 90);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(179, 40);
            this.txtFirstName.TabIndex = 9;
            this.txtFirstName.Visible = false;
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.AppearanceKey = "toolbarButton";
            this.cmdNew.Location = new System.Drawing.Point(425, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(47, 24);
            this.cmdNew.TabIndex = 10;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(478, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(58, 24);
            this.cmdDelete.TabIndex = 12;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(241, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(79, 48);
            this.cmdSave.TabIndex = 13;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(30, 104);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(130, 18);
            this.Label1_8.TabIndex = 21;
            this.Label1_8.Text = "ESTABLISHMENT #";
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(30, 164);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(112, 18);
            this.Label1_7.TabIndex = 20;
            this.Label1_7.Text = "FACILITY NAME";
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(30, 344);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(60, 18);
            this.Label1_6.TabIndex = 19;
            this.Label1_6.Text = "CITY";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(30, 464);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(60, 18);
            this.Label1_5.TabIndex = 18;
            this.Label1_5.Text = "ZIP";
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(30, 404);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(65, 18);
            this.Label1_4.TabIndex = 17;
            this.Label1_4.Text = "STATE";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(30, 224);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(65, 18);
            this.Label1_3.TabIndex = 16;
            this.Label1_3.Text = "ADDRESS 1";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(30, 284);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(72, 18);
            this.Label1_1.TabIndex = 15;
            this.Label1_1.Text = "ADDRESS 2";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(30, 524);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(60, 18);
            this.Label1_2.TabIndex = 14;
            this.Label1_2.Text = "LICENSEE";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.mnuNew.Text = "New";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 2;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                   ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit             ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 5;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmFacilityNames
            // 
            this.AcceptButton = this.cmdSave;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(564, 688);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFacilityNames";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Funeral Establishment Information";
            this.Load += new System.EventHandler(this.frmFacilityNames_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmFacilityNames_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFacilityNames_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
