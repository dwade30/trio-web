//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogLicenseBackup.
	/// </summary>
	public partial class rptDogLicenseBackup : BaseSectionReport
	{
		public rptDogLicenseBackup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Dog License Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDogLicenseBackup InstancePtr
		{
			get
			{
				return (rptDogLicenseBackup)Sys.GetInstance(typeof(rptDogLicenseBackup));
			}
		}

		protected rptDogLicenseBackup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
				clsDog.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDogLicenseBackup	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		int intLine;
		clsDRWrapper clsDog = new clsDRWrapper();

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "DogLicenseListing");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			/*? On Error Resume Next  */
			eArgs.EOF = rs.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: strTemp As object	OnWrite(string())
			object strTemp;
			string strSQL;
			txtTown.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "H:mm AM/PM");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtCaption.Text = "Town Clerk's Report";
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			strTemp = Strings.Split(modGNBas.Statics.gstrMonthYear, "/", -1, CompareConstants.vbBinaryCompare);
			// Call RS.OpenRecordset("SELECT DogOwner.K10Num, DogOwner.K20Num, DogOwner.FirstName, DogOwner.MI, DogOwner.LastName, DogInfo.* FROM (DogInfo INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID) where (DogInfo.RabStickerIssue >=#" & strTemp(0) & "/01/" & strTemp(1) & "# and DogInfo.RabStickerIssue <=#" & DateAdd("D", -1, DateAdd("M", 1, strTemp(0) & "/01/" & strTemp(1))) & "#) or (DogInfo.DogToKennelDate >=#" & strTemp(0) & "/01/" & strTemp(1) & "# and DogInfo.DogToKennelDate <=#" & DateAdd("D", -1, DateAdd("M", 1, strTemp(0) & "/01/" & strTemp(1))) & "#) Order by DogOwner.LastName,DogOwner.FirstName", DEFAULTDATABASENAME)
			strSQL = "select * from dogtransactions where transactiondate between '" + ((object[])strTemp)[0] + "/01/" + ((object[])strTemp)[1] + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + ((object[])strTemp)[1])))) + "' order by transactiondate,ownernum";
			rs.OpenRecordset(strSQL, "twck0000.vb1");
			intLine = 0;
			// Call SetPrintProperties(Me)
			txtCaption2.Text = "Dog License # Backup - " + modGNBas.Statics.gstrMonthYear;
		}
		// Public Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports2.DDTool)
		// Call VerifyPrintToFile(Me, Tool)
		// End Sub
		private void Detail_Format(object sender, EventArgs e)
		{
			int lngOwnerNum;
			string strTemp = "";
			string[] strAry = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			if (rs.EndOfFile())
				return;
			intLine += 1;
			txtLine.Text = intLine.ToString();
			txtDatePaid.Text = Strings.Format(rs.Get_Fields("transactiondate"), "MM/dd/yyyy");
			txtAmount.Text = Strings.Format(rs.Get_Fields("Amount"), "#,###,##0.00");
			txtDescription.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("description")));
			lngOwnerNum = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("ownernum"))));
			// kk04172015   Call clsDog.OpenRecordset("select * from dogowner where ID = " & lngOwnerNum, "twck0000.vb1")
			clsDog.OpenRecordset("select FirstName, MiddleName, LastName from dogowner LEFT JOIN " + clsDog.CurrentPrefix + "CentralParties.dbo.Parties AS p ON dogowner.PartyID = p.ID where dogOwner.ID = " + FCConvert.ToString(lngOwnerNum), "twck0000.vb1");
			if (!clsDog.EndOfFile())
			{
				txtOwnerName.Text = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(clsDog.Get_Fields_String("firstname") + " " + clsDog.Get_Fields_String("MiddleName")) + " " + clsDog.Get_Fields_String("LastName"));
				// kk04172015    clsDog.Fields("MI")
			}
			else
			{
				txtOwnerName.Text = "";
			}
			txtDogName.Text = "";
			txtSticker.Text = "";
			txtTagNumber.Text = "";
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("boolkennel")))
			{
				txtDogName.Text = "";
				if (Conversion.Val(rs.Get_Fields_Int32("kennellic1num")) > 0)
				{
					txtTagNumber.Text = "K# " + FCConvert.ToString(Conversion.Val(modGlobalRoutines.GetKennelLicenseNumber(FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int32("kennellic1num"))))));
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("kenneldogs"))) != string.Empty)
				{
					strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("KennelDogs")));
					strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
					strTemp = "";
					for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
					{
						clsDog.OpenRecordset("select dogname from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[x])), "twck0000.vb1");
						if (!clsDog.EndOfFile())
						{
							strTemp += clsDog.Get_Fields_String("dogname") + ", ";
						}
					}
					// x
					if (strTemp != string.Empty)
					{
						// cut off the last , and space
						strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2);
					}
					txtDogName.Text = strTemp;
				}
			}
			else
			{
				if (rs.Get_Fields_Int32("stickernumber") > 0)
				{
					txtSticker.Text = FCConvert.ToString(rs.Get_Fields_Int32("stickernumber"));
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields("tagnumber"))) != string.Empty)
				{
					txtTagNumber.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields("tagnumber")));
				}
				if (Conversion.Val(rs.Get_Fields_Int32("dognumber")) > 0)
				{
					clsDog.OpenRecordset("select dogname from doginfo where ID = " + rs.Get_Fields_Int32("dognumber"), "twck0000.vb1");
					if (!clsDog.EndOfFile())
					{
						txtDogName.Text = clsDog.Get_Fields_String("dogname");
					}
				}
			}
			// 
			// If RS.Fields("KennelDog") Then
			// txtAmount = "K " & Format(RS.Fields("AmountC"), "Currency")
			// If Val(RS.Fields("K20Num")) = 0 Then
			// txtTagNumber = "K# " & GetKennelLicenseNumber(RS.Fields("K10Num"))
			// Else
			// txtTagNumber = "K# " & GetKennelLicenseNumber(RS.Fields("K10Num")) & "/" & GetKennelLicenseNumber(RS.Fields("K20Num"))
			// End If
			// Else
			// txtAmount = Format(RS.Fields("AmountC"), "Currency")
			// txtTagNumber = RS.Fields("TagLicNum")
			// End If
			rs.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
