//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptKennelLicense.
	/// </summary>
	public partial class rptKennelLicense : BaseSectionReport
	{
		public rptKennelLicense()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptKennelLicense InstancePtr
		{
			get
			{
				return (rptKennelLicense)Sys.GetInstance(typeof(rptKennelLicense));
			}
		}

		protected rptKennelLicense _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptKennelLicense	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		int lngLicense;
		// vbPorter upgrade warning: dtIssueDate As DateTime	OnWrite(string)
		DateTime dtIssueDate;
		// Public Sub Init(lngLicenseNumber As Long, dtLicDate As Date)
		public void Init(int lngLicenseID, int lngOwnerNum)
		{
			// lngLicense = lngLicenseNumber
			// dtIssueDate = dtLicDate
			rs.OpenRecordset("select * from kennellicense where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
			if (!rs.EndOfFile())
			{
				lngLicense = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("licenseid"))));
				if (Information.IsDate(rs.Get_Fields("reissuedate")))
				{
					dtIssueDate = (DateTime)rs.Get_Fields_DateTime("reissuedate");
					if (dtIssueDate.ToOADate() == 0)
					{
						dtIssueDate = FCConvert.ToDateTime(Strings.Format(rs.Get_Fields_DateTime("originalissuedate"), "MM/dd/yyyy"));
					}
					else
					{
						dtIssueDate = FCConvert.ToDateTime(Strings.Format(rs.Get_Fields_DateTime("reissuedate"), "MM/dd/yyyy"));
					}
				}
				rs.OpenRecordset("select * from doginventory where ID = " + FCConvert.ToString(lngLicense), "twck0000.vb1");
				if (!rs.EndOfFile())
				{
					lngLicense = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("stickernumber"))));
				}
				else
				{
					MessageBox.Show("Kennel license not found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("Kennel license not found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
				return;
			}
			// kk04172015  Call rs.OpenRecordset("select * from dogowner where ID = " & lngOwnerNum, "twck0000.vb1")
			rs.OpenRecordset("select * from dogowner inner join " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView as p on dogowner.partyid = p.Id where dogOwner.ID = " + FCConvert.ToString(lngOwnerNum), "twck0000.vb1");
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			/*? On Error Resume Next  */
			if (rs.EndOfFile())
				return;
			txtOwnerName.Text = rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("MiddleName") + " " + rs.Get_Fields_String("LastName");
			// kk04172015  rs.Fields("MI")
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("PhoneNumber"))).Length == 7)
			{
				// rs.Fields("Phone")
				txtPhone.Text = "(207) " + Strings.Left(FCConvert.ToString(rs.Get_Fields_String("PhoneNumber")), 3) + "-" + Strings.Right(FCConvert.ToString(rs.Get_Fields_String("PhoneNumber")), 4);
			}
			else
			{
				txtPhone.Text = Strings.Format(rs.Get_Fields_String("PhoneNumber"), "(###) ###-####");
			}
			// txtAddress = RS.Fields("Address") & "  " & RS.Fields("City") & ", " & GetDefaultStateName(Val(RS.Fields("State"))) & " " & IIf(Len(RS.Fields("Zip")) > 5, Left(RS.Fields("Zip"), 5) & "-" & Mid(RS.Fields("Zip"), 6, Len(RS.Fields("Zip")) - 5), RS.Fields("Zip"))
			txtAddress.Text = rs.Get_Fields_String("Address1") + "  " + rs.Get_Fields_String("City") + ", " + rs.Get_Fields("State") + " " + (FCConvert.ToString(rs.Get_Fields_String("Zip")).Length > 5 ? Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Zip")), 5) + "-" + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("Zip")), 6, FCConvert.ToString(rs.Get_Fields_String("Zip")).Length - 5) : rs.Get_Fields_String("Zip"));
			// txtLicenseNumber = GetKennelLicenseNumber(RS.Fields("K10Num"))
			txtLicenseNumber.Text = FCConvert.ToString(lngLicense);
			txtLocation.Text = rs.Get_Fields_String("LocationNum") + " " + rs.Get_Fields_String("LocationSTR");
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) == 0)
			{
				txtMuni.Text = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				txtMuni.Text = modRegionalTown.GetTownKeyName_2(rs.Get_Fields_Int32("towncode"));
			}
			// txtLicenseDate = Format(Date, "MM/dd/yyyy")
			txtLicenseDate.Text = Strings.Format(dtIssueDate, "MM/dd/yyyy");
			txtYear.Text = FCConvert.ToString(dtIssueDate.Year);
			txtMuni2.Text = modGlobalConstants.Statics.MuniName;
			txtCity.Text = modGNBas.Statics.ClerkDefaults.City;
			txtZip.Text = modGNBas.Statics.ClerkDefaults.Zip;
			txtStreet.Text = modGNBas.Statics.ClerkDefaults.Address1;
			if (!rs.EndOfFile())
				rs.MoveNext();
			eArgs.EOF = rs.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
		}
		
	}
}
