//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDogWarrant.
	/// </summary>
	partial class frmDogWarrant
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtCourtTown;
		public fecherFoundation.FCTextBox txtCourtCounty;
		public fecherFoundation.FCTextBox txtControlOfficer;
		public fecherFoundation.FCTextBox txtTownName;
		public fecherFoundation.FCTextBox txtTownCounty;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCButton cmdPrint;
		//public fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtCourtTown = new fecherFoundation.FCTextBox();
            this.txtCourtCounty = new fecherFoundation.FCTextBox();
            this.txtControlOfficer = new fecherFoundation.FCTextBox();
            this.txtTownName = new fecherFoundation.FCTextBox();
            this.txtTownCounty = new fecherFoundation.FCTextBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdExit = new fecherFoundation.FCButton();
            //this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(446, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Size = new System.Drawing.Size(446, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrintPreview);
            this.TopPanel.Size = new System.Drawing.Size(446, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(248, 30);
            this.HeaderText.Text = "Edit Dog Warrant Info";
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.txtCourtTown);
            this.Frame1.Controls.Add(this.txtCourtCounty);
            this.Frame1.Controls.Add(this.txtControlOfficer);
            this.Frame1.Controls.Add(this.txtTownName);
            this.Frame1.Controls.Add(this.txtTownCounty);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(318, 455);
            this.Frame1.TabIndex = 5;
            this.Frame1.Text = "Warrant Info";
            // 
            // txtCourtTown
            // 
            this.txtCourtTown.AutoSize = false;
            this.txtCourtTown.BackColor = System.Drawing.SystemColors.Window;
            this.txtCourtTown.LinkItem = null;
            this.txtCourtTown.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCourtTown.LinkTopic = null;
            this.txtCourtTown.Location = new System.Drawing.Point(20, 395);
            this.txtCourtTown.Name = "txtCourtTown";
            this.txtCourtTown.Size = new System.Drawing.Size(278, 40);
            this.txtCourtTown.TabIndex = 4;
            // 
            // txtCourtCounty
            // 
            this.txtCourtCounty.AutoSize = false;
            this.txtCourtCounty.BackColor = System.Drawing.SystemColors.Window;
            this.txtCourtCounty.LinkItem = null;
            this.txtCourtCounty.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCourtCounty.LinkTopic = null;
            this.txtCourtCounty.Location = new System.Drawing.Point(20, 310);
            this.txtCourtCounty.Name = "txtCourtCounty";
            this.txtCourtCounty.Size = new System.Drawing.Size(278, 40);
            this.txtCourtCounty.TabIndex = 3;
            // 
            // txtControlOfficer
            // 
            this.txtControlOfficer.AutoSize = false;
            this.txtControlOfficer.BackColor = System.Drawing.SystemColors.Window;
            this.txtControlOfficer.LinkItem = null;
            this.txtControlOfficer.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtControlOfficer.LinkTopic = null;
            this.txtControlOfficer.Location = new System.Drawing.Point(20, 225);
            this.txtControlOfficer.Name = "txtControlOfficer";
            this.txtControlOfficer.Size = new System.Drawing.Size(278, 40);
            this.txtControlOfficer.TabIndex = 2;
            // 
            // txtTownName
            // 
            this.txtTownName.AutoSize = false;
            this.txtTownName.BackColor = System.Drawing.SystemColors.Window;
            this.txtTownName.LinkItem = null;
            this.txtTownName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTownName.LinkTopic = null;
            this.txtTownName.Location = new System.Drawing.Point(20, 140);
            this.txtTownName.Name = "txtTownName";
            this.txtTownName.Size = new System.Drawing.Size(278, 40);
            this.txtTownName.TabIndex = 1;
            // 
            // txtTownCounty
            // 
            this.txtTownCounty.AutoSize = false;
            this.txtTownCounty.BackColor = System.Drawing.SystemColors.Window;
            this.txtTownCounty.LinkItem = null;
            this.txtTownCounty.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTownCounty.LinkTopic = null;
            this.txtTownCounty.Location = new System.Drawing.Point(20, 50);
            this.txtTownCounty.Name = "txtTownCounty";
            this.txtTownCounty.Size = new System.Drawing.Size(278, 40);
            this.txtTownCounty.TabIndex = 0;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 370);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(175, 15);
            this.Label5.TabIndex = 10;
            this.Label5.Text = "TOWN OF DISTRICT COURT";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 285);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(175, 15);
            this.Label4.TabIndex = 9;
            this.Label4.Text = "COUNTY OF DISTRICT COURT";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 200);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(175, 15);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "CONTROL OFFICER";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 115);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(175, 15);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "NAME OF TOWN";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(175, 15);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "TOWN\'S COUNTY";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(189, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 11;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdExit
            // 
            this.cmdExit.AppearanceKey = "actionButton";
            this.cmdExit.Location = new System.Drawing.Point(202, 30);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(80, 40);
            this.cmdExit.TabIndex = 12;
            this.cmdExit.Text = "Cancel";
            this.cmdExit.Visible = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // MainMenu1
            // 
            //this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            //this.mnuClear});
            //this.MainMenu1.Name = null;
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Fields";
            this.mnuClear.Visible = false;
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "actionButton";
            this.cmdPrint.Location = new System.Drawing.Point(30, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(150, 40);
            this.cmdPrint.TabIndex = 13;
            this.cmdPrint.Text = "Print / PreView";
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            this.mnuSP2.Visible = false;
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = -1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print/Preview";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                        ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit              ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.AppearanceKey = "toolbarButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(311, 29);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Size = new System.Drawing.Size(100, 24);
            this.cmdPrintPreview.TabIndex = 1;
            this.cmdPrintPreview.Text = "Print/Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmDogWarrant
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(446, 666);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            //this.Menu = this.MainMenu1;
            this.MinimizeBox = false;
            this.Name = "frmDogWarrant";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Edit Dog Warrant Info";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDogWarrant_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDogWarrant_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDogWarrant_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPrintPreview;
	}
}