//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmNewDog.
	/// </summary>
	partial class frmNewDog
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLabels;
		public fecherFoundation.FCFrame framStickerInventory;
		public fecherFoundation.FCButton cmdCancelSticker;
		public FCGrid GridStickerInventory;
		public fecherFoundation.FCCheckBox chkFees;
		public fecherFoundation.FCCheckBox chkNotRequired;
		public fecherFoundation.FCFrame framTransaction;
		public fecherFoundation.FCCheckBox chkUserDefinedFee;
		public fecherFoundation.FCTextBox txtAmount;
		public fecherFoundation.FCCheckBox chkLicense;
		public fecherFoundation.FCCheckBox chkReplacementSticker;
		public fecherFoundation.FCCheckBox chkReplacementLicense;
		public fecherFoundation.FCCheckBox chkTransferLicense;
		public fecherFoundation.FCCheckBox chkWarrantFee;
		public fecherFoundation.FCCheckBox chkLateFee;
		public fecherFoundation.FCCheckBox chkReplacementTag;
		public fecherFoundation.FCCheckBox chkTempLicense;
		public fecherFoundation.FCLabel lblLabels_1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbVet;
		public fecherFoundation.FCCheckBox chkExcludeWarrant;
		public fecherFoundation.FCCheckBox chkOutsideSource;
		public fecherFoundation.FCTextBox txtDogName;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCTextBox txtSpayNeuterNumber;
		public fecherFoundation.FCTextBox txtSex;
		public fecherFoundation.FCComboBox cmbColor;
		public fecherFoundation.FCComboBox cmbBreed;
		public fecherFoundation.FCTextBox txtMarkings;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCCheckBox chkRabiesExempt;
		public fecherFoundation.FCTextBox txtRabiesTag;
		public fecherFoundation.FCTextBox txtCertificateNumber;
		public Global.T2KDateBox T2KDateVaccinated;
		public Global.T2KDateBox t2kExpirationDate;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtPrevTag;
		public fecherFoundation.FCTextBox txtTag;
		public Global.T2KDateBox T2KIssueDate;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCCheckBox chkDeceased;
		public fecherFoundation.FCCheckBox chkNeuterSpay;
		public fecherFoundation.FCCheckBox chkWolfHybrid;
		public fecherFoundation.FCCheckBox chkHearingGuid;
		public fecherFoundation.FCCheckBox chkSearchRescue;
		public Global.T2KDateBox T2KDOB;
		public fecherFoundation.FCPictureBox imgDocuments;
		public fecherFoundation.FCPictureBox ImgComment;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblOwnerName;
		public fecherFoundation.FCLabel Label17;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuBreeds;
		public fecherFoundation.FCToolStripMenuItem mnuColors;
		public fecherFoundation.FCToolStripMenuItem mnuVets;
		public fecherFoundation.FCToolStripMenuItem mnuFees;
		public fecherFoundation.FCToolStripMenuItem mnuInventory;
		public fecherFoundation.FCToolStripMenuItem mnuPrinterSettings;
		public fecherFoundation.FCToolStripMenuItem mnuComments;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocs;
		public fecherFoundation.FCToolStripMenuItem mnuDogHistory;
		public fecherFoundation.FCToolStripMenuItem mnuPrintFront;
		public fecherFoundation.FCToolStripMenuItem mnuPrintBack;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewDog));
            this.framStickerInventory = new fecherFoundation.FCFrame();
            this.cmdCancelSticker = new fecherFoundation.FCButton();
            this.GridStickerInventory = new fecherFoundation.FCGrid();
            this.chkFees = new fecherFoundation.FCCheckBox();
            this.chkNotRequired = new fecherFoundation.FCCheckBox();
            this.framTransaction = new fecherFoundation.FCFrame();
            this.chkUserDefinedFee = new fecherFoundation.FCCheckBox();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.chkLicense = new fecherFoundation.FCCheckBox();
            this.chkReplacementSticker = new fecherFoundation.FCCheckBox();
            this.chkReplacementLicense = new fecherFoundation.FCCheckBox();
            this.chkTransferLicense = new fecherFoundation.FCCheckBox();
            this.chkWarrantFee = new fecherFoundation.FCCheckBox();
            this.chkLateFee = new fecherFoundation.FCCheckBox();
            this.chkReplacementTag = new fecherFoundation.FCCheckBox();
            this.chkTempLicense = new fecherFoundation.FCCheckBox();
            this.lblLabels_1 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkNuisance = new fecherFoundation.FCCheckBox();
            this.chkDangerous = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtPrevTag = new fecherFoundation.FCTextBox();
            this.txtTag = new fecherFoundation.FCTextBox();
            this.T2KIssueDate = new Global.T2KDateBox();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.cmbVet = new fecherFoundation.FCComboBox();
            this.chkExcludeWarrant = new fecherFoundation.FCCheckBox();
            this.chkOutsideSource = new fecherFoundation.FCCheckBox();
            this.txtDogName = new fecherFoundation.FCTextBox();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.txtSpayNeuterNumber = new fecherFoundation.FCTextBox();
            this.txtSex = new fecherFoundation.FCTextBox();
            this.cmbColor = new fecherFoundation.FCComboBox();
            this.cmbBreed = new fecherFoundation.FCComboBox();
            this.txtMarkings = new fecherFoundation.FCTextBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.chkRabiesExempt = new fecherFoundation.FCCheckBox();
            this.txtRabiesTag = new fecherFoundation.FCTextBox();
            this.txtCertificateNumber = new fecherFoundation.FCTextBox();
            this.T2KDateVaccinated = new Global.T2KDateBox();
            this.t2kExpirationDate = new Global.T2KDateBox();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.chkDeceased = new fecherFoundation.FCCheckBox();
            this.chkNeuterSpay = new fecherFoundation.FCCheckBox();
            this.chkWolfHybrid = new fecherFoundation.FCCheckBox();
            this.chkHearingGuid = new fecherFoundation.FCCheckBox();
            this.chkSearchRescue = new fecherFoundation.FCCheckBox();
            this.T2KDOB = new Global.T2KDateBox();
            this.imgDocuments = new fecherFoundation.FCPictureBox();
            this.ImgComment = new fecherFoundation.FCPictureBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.lblOwnerName = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuBreeds = new fecherFoundation.FCToolStripMenuItem();
            this.mnuColors = new fecherFoundation.FCToolStripMenuItem();
            this.mnuVets = new fecherFoundation.FCToolStripMenuItem();
            this.mnuInventory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrinterSettings = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewDocs = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDogHistory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintFront = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintBack = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFees = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdDeleteDog = new fecherFoundation.FCButton();
            this.cmdComments = new fecherFoundation.FCButton();
            this.cmdFees = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framStickerInventory)).BeginInit();
            this.framStickerInventory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStickerInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNotRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framTransaction)).BeginInit();
            this.framTransaction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserDefinedFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTransferLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWarrantFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLateFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTempLicense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNuisance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDangerous)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludeWarrant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOutsideSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRabiesExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateVaccinated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kExpirationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNeuterSpay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWolfHybrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHearingGuid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSearchRescue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(795, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkFees);
            this.ClientArea.Controls.Add(this.chkNotRequired);
            this.ClientArea.Controls.Add(this.framTransaction);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.lblOwnerName);
            this.ClientArea.Controls.Add(this.Label17);
            this.ClientArea.Size = new System.Drawing.Size(795, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFees);
            this.TopPanel.Controls.Add(this.cmdComments);
            this.TopPanel.Controls.Add(this.cmdDeleteDog);
            this.TopPanel.Size = new System.Drawing.Size(795, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteDog, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComments, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFees, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(105, 30);
            this.HeaderText.Text = "Dog Info";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // framStickerInventory
            // 
            this.framStickerInventory.BackColor = System.Drawing.Color.White;
            this.framStickerInventory.Controls.Add(this.cmdCancelSticker);
            this.framStickerInventory.Controls.Add(this.GridStickerInventory);
            this.framStickerInventory.Location = new System.Drawing.Point(350, 200);
            this.framStickerInventory.Name = "framStickerInventory";
            this.framStickerInventory.Size = new System.Drawing.Size(260, 354);
            this.framStickerInventory.TabIndex = 58;
            this.framStickerInventory.Text = "Sticker Inventory";
            this.ToolTip1.SetToolTip(this.framStickerInventory, null);
            this.framStickerInventory.Visible = false;
            // 
            // cmdCancelSticker
            // 
            this.cmdCancelSticker.AppearanceKey = "actionButton";
            this.cmdCancelSticker.Location = new System.Drawing.Point(20, 294);
            this.cmdCancelSticker.Name = "cmdCancelSticker";
            this.cmdCancelSticker.Size = new System.Drawing.Size(96, 40);
            this.cmdCancelSticker.TabIndex = 59;
            this.cmdCancelSticker.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdCancelSticker, null);
            this.cmdCancelSticker.Click += new System.EventHandler(this.cmdCancelSticker_Click);
            // 
            // GridStickerInventory
            // 
            this.GridStickerInventory.Cols = 1;
            this.GridStickerInventory.ColumnHeadersVisible = false;
            this.GridStickerInventory.ExtendLastCol = true;
            this.GridStickerInventory.FixedCols = 0;
            this.GridStickerInventory.FixedRows = 0;
            this.GridStickerInventory.Location = new System.Drawing.Point(20, 30);
            this.GridStickerInventory.Name = "GridStickerInventory";
            this.GridStickerInventory.RowHeadersVisible = false;
            this.GridStickerInventory.Rows = 0;
            this.GridStickerInventory.Size = new System.Drawing.Size(220, 254);
            this.GridStickerInventory.TabIndex = 60;
            this.GridStickerInventory.Tag = "Sticker";
            this.ToolTip1.SetToolTip(this.GridStickerInventory, null);
            this.GridStickerInventory.DoubleClick += new System.EventHandler(this.GridStickerInventory_DblClick);
            // 
            // chkFees
            // 
            this.chkFees.Location = new System.Drawing.Point(30, 761);
            this.chkFees.Name = "chkFees";
            this.chkFees.Size = new System.Drawing.Size(120, 27);
            this.chkFees.TabIndex = 25;
            this.chkFees.Text = "Apply Fee(s)";
            this.ToolTip1.SetToolTip(this.chkFees, null);
            this.chkFees.CheckedChanged += new System.EventHandler(this.chkFees_CheckedChanged);
            // 
            // chkNotRequired
            // 
            this.chkNotRequired.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.chkNotRequired.Location = new System.Drawing.Point(325, 30);
            this.chkNotRequired.Name = "chkNotRequired";
            this.chkNotRequired.Size = new System.Drawing.Size(442, 27);
            this.chkNotRequired.TabIndex = 26;
            this.chkNotRequired.Text = "Record contains older data.  Not all fields will be required";
            this.ToolTip1.SetToolTip(this.chkNotRequired, null);
            this.chkNotRequired.CheckedChanged += new System.EventHandler(this.chkNotRequired_CheckedChanged);
            // 
            // framTransaction
            // 
            this.framTransaction.Controls.Add(this.chkUserDefinedFee);
            this.framTransaction.Controls.Add(this.txtAmount);
            this.framTransaction.Controls.Add(this.chkLicense);
            this.framTransaction.Controls.Add(this.chkReplacementSticker);
            this.framTransaction.Controls.Add(this.chkReplacementLicense);
            this.framTransaction.Controls.Add(this.chkTransferLicense);
            this.framTransaction.Controls.Add(this.chkWarrantFee);
            this.framTransaction.Controls.Add(this.chkLateFee);
            this.framTransaction.Controls.Add(this.chkReplacementTag);
            this.framTransaction.Controls.Add(this.chkTempLicense);
            this.framTransaction.Controls.Add(this.lblLabels_1);
            this.framTransaction.Location = new System.Drawing.Point(30, 798);
            this.framTransaction.Name = "framTransaction";
            this.framTransaction.Size = new System.Drawing.Size(494, 238);
            this.framTransaction.TabIndex = 53;
            this.framTransaction.Text = "Transaction Information";
            this.ToolTip1.SetToolTip(this.framTransaction, null);
            this.framTransaction.Visible = false;
            // 
            // chkUserDefinedFee
            // 
            this.chkUserDefinedFee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkUserDefinedFee.Location = new System.Drawing.Point(20, 178);
            this.chkUserDefinedFee.Name = "chkUserDefinedFee";
            this.chkUserDefinedFee.Size = new System.Drawing.Size(156, 27);
            this.chkUserDefinedFee.TabIndex = 61;
            this.chkUserDefinedFee.Text = "User Defined Fee";
            this.ToolTip1.SetToolTip(this.chkUserDefinedFee, null);
            this.chkUserDefinedFee.Visible = false;
            this.chkUserDefinedFee.CheckedChanged += new System.EventHandler(this.chkUserDefinedFee_CheckedChanged);
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAmount.Location = new System.Drawing.Point(376, 178);
            this.txtAmount.LockedOriginal = true;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Size = new System.Drawing.Size(98, 40);
            this.txtAmount.TabIndex = 54;
            this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtAmount, null);
            // 
            // chkLicense
            // 
            this.chkLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkLicense.Location = new System.Drawing.Point(20, 30);
            this.chkLicense.Name = "chkLicense";
            this.chkLicense.Size = new System.Drawing.Size(84, 27);
            this.chkLicense.TabIndex = 26;
            this.chkLicense.Text = "License";
            this.ToolTip1.SetToolTip(this.chkLicense, null);
            this.chkLicense.CheckedChanged += new System.EventHandler(this.chkLicense_CheckedChanged);
            // 
            // chkReplacementSticker
            // 
            this.chkReplacementSticker.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkReplacementSticker.Location = new System.Drawing.Point(231, 30);
            this.chkReplacementSticker.Name = "chkReplacementSticker";
            this.chkReplacementSticker.Size = new System.Drawing.Size(179, 27);
            this.chkReplacementSticker.TabIndex = 30;
            this.chkReplacementSticker.Text = "Replacement Sticker";
            this.ToolTip1.SetToolTip(this.chkReplacementSticker, null);
            this.chkReplacementSticker.CheckedChanged += new System.EventHandler(this.chkReplacementSticker_CheckedChanged);
            // 
            // chkReplacementLicense
            // 
            this.chkReplacementLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkReplacementLicense.Location = new System.Drawing.Point(231, 67);
            this.chkReplacementLicense.Name = "chkReplacementLicense";
            this.chkReplacementLicense.Size = new System.Drawing.Size(185, 27);
            this.chkReplacementLicense.TabIndex = 31;
            this.chkReplacementLicense.Text = "Replacement License";
            this.ToolTip1.SetToolTip(this.chkReplacementLicense, null);
            this.chkReplacementLicense.CheckedChanged += new System.EventHandler(this.chkReplacementLicense_CheckedChanged);
            // 
            // chkTransferLicense
            // 
            this.chkTransferLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkTransferLicense.Location = new System.Drawing.Point(231, 105);
            this.chkTransferLicense.Name = "chkTransferLicense";
            this.chkTransferLicense.Size = new System.Drawing.Size(149, 27);
            this.chkTransferLicense.TabIndex = 32;
            this.chkTransferLicense.Text = "Transfer License";
            this.ToolTip1.SetToolTip(this.chkTransferLicense, null);
            this.chkTransferLicense.CheckedChanged += new System.EventHandler(this.chkTransferLicense_CheckedChanged);
            // 
            // chkWarrantFee
            // 
            this.chkWarrantFee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkWarrantFee.Location = new System.Drawing.Point(20, 104);
            this.chkWarrantFee.Name = "chkWarrantFee";
            this.chkWarrantFee.Size = new System.Drawing.Size(118, 27);
            this.chkWarrantFee.TabIndex = 28;
            this.chkWarrantFee.Text = "Warrant Fee";
            this.ToolTip1.SetToolTip(this.chkWarrantFee, null);
            this.chkWarrantFee.CheckedChanged += new System.EventHandler(this.chkWarrantFee_CheckedChanged);
            // 
            // chkLateFee
            // 
            this.chkLateFee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkLateFee.Location = new System.Drawing.Point(20, 67);
            this.chkLateFee.Name = "chkLateFee";
            this.chkLateFee.Size = new System.Drawing.Size(92, 27);
            this.chkLateFee.TabIndex = 27;
            this.chkLateFee.Text = "Late Fee";
            this.ToolTip1.SetToolTip(this.chkLateFee, null);
            this.chkLateFee.CheckedChanged += new System.EventHandler(this.chkLateFee_CheckedChanged);
            // 
            // chkReplacementTag
            // 
            this.chkReplacementTag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkReplacementTag.Location = new System.Drawing.Point(20, 141);
            this.chkReplacementTag.Name = "chkReplacementTag";
            this.chkReplacementTag.Size = new System.Drawing.Size(190, 27);
            this.chkReplacementTag.TabIndex = 29;
            this.chkReplacementTag.Text = "Replacement Tag Fee";
            this.ToolTip1.SetToolTip(this.chkReplacementTag, null);
            this.chkReplacementTag.CheckedChanged += new System.EventHandler(this.chkReplacementTag_CheckedChanged);
            // 
            // chkTempLicense
            // 
            this.chkTempLicense.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkTempLicense.Location = new System.Drawing.Point(231, 138);
            this.chkTempLicense.Name = "chkTempLicense";
            this.chkTempLicense.Size = new System.Drawing.Size(243, 27);
            this.chkTempLicense.TabIndex = 33;
            this.chkTempLicense.Text = "Temporary License (10 Days)";
            this.ToolTip1.SetToolTip(this.chkTempLicense, null);
            this.chkTempLicense.CheckedChanged += new System.EventHandler(this.chkTempLicense_CheckedChanged);
            // 
            // lblLabels_1
            // 
            this.lblLabels_1.Location = new System.Drawing.Point(231, 192);
            this.lblLabels_1.Name = "lblLabels_1";
            this.lblLabels_1.Size = new System.Drawing.Size(136, 17);
            this.lblLabels_1.TabIndex = 55;
            this.lblLabels_1.Text = "AMOUNT CHARGED";
            this.ToolTip1.SetToolTip(this.lblLabels_1, null);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxLeftBorder";
            this.Frame1.Controls.Add(this.chkNuisance);
            this.Frame1.Controls.Add(this.chkDangerous);
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.framStickerInventory);
            this.Frame1.Controls.Add(this.cmbVet);
            this.Frame1.Controls.Add(this.chkExcludeWarrant);
            this.Frame1.Controls.Add(this.chkOutsideSource);
            this.Frame1.Controls.Add(this.txtDogName);
            this.Frame1.Controls.Add(this.txtYear);
            this.Frame1.Controls.Add(this.txtSpayNeuterNumber);
            this.Frame1.Controls.Add(this.txtSex);
            this.Frame1.Controls.Add(this.cmbColor);
            this.Frame1.Controls.Add(this.cmbBreed);
            this.Frame1.Controls.Add(this.txtMarkings);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.Controls.Add(this.chkDeceased);
            this.Frame1.Controls.Add(this.chkNeuterSpay);
            this.Frame1.Controls.Add(this.chkWolfHybrid);
            this.Frame1.Controls.Add(this.chkHearingGuid);
            this.Frame1.Controls.Add(this.chkSearchRescue);
            this.Frame1.Controls.Add(this.T2KDOB);
            this.Frame1.Controls.Add(this.imgDocuments);
            this.Frame1.Controls.Add(this.ImgComment);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.Label9);
            this.Frame1.Location = new System.Drawing.Point(30, 67);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(658, 684);
            this.Frame1.TabIndex = 1;
            this.Frame1.Text = "Dog Information";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // chkNuisance
            // 
            this.chkNuisance.Location = new System.Drawing.Point(384, 356);
            this.chkNuisance.Name = "chkNuisance";
            this.chkNuisance.Size = new System.Drawing.Size(96, 27);
            this.chkNuisance.TabIndex = 16;
            this.chkNuisance.Text = "Nuisance";
            this.ToolTip1.SetToolTip(this.chkNuisance, null);
            this.chkNuisance.CheckedChanged += new System.EventHandler(this.chkNuisance_CheckedChanged);
            // 
            // chkDangerous
            // 
            this.chkDangerous.Location = new System.Drawing.Point(384, 319);
            this.chkDangerous.Name = "chkDangerous";
            this.chkDangerous.Size = new System.Drawing.Size(107, 27);
            this.chkDangerous.TabIndex = 15;
            this.chkDangerous.Text = "Dangerous";
            this.ToolTip1.SetToolTip(this.chkDangerous, null);
            this.chkDangerous.CheckedChanged += new System.EventHandler(this.chkDangerous_CheckedChanged);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtPrevTag);
            this.Frame2.Controls.Add(this.txtTag);
            this.Frame2.Controls.Add(this.T2KIssueDate);
            this.Frame2.Controls.Add(this.Label15);
            this.Frame2.Controls.Add(this.Label14);
            this.Frame2.Controls.Add(this.Label16);
            this.Frame2.Location = new System.Drawing.Point(345, 474);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(275, 190);
            this.Frame2.TabIndex = 35;
            this.Frame2.Text = "Municipal Tag Number";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // txtPrevTag
            // 
            this.txtPrevTag.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrevTag.Location = new System.Drawing.Point(142, 130);
            this.txtPrevTag.Name = "txtPrevTag";
            this.txtPrevTag.Size = new System.Drawing.Size(113, 40);
            this.txtPrevTag.TabIndex = 24;
            this.ToolTip1.SetToolTip(this.txtPrevTag, "Press F2 for available stickers");
            this.txtPrevTag.TextChanged += new System.EventHandler(this.txtPrevTag_TextChanged);
            // 
            // txtTag
            // 
            this.txtTag.BackColor = System.Drawing.SystemColors.Info;
            this.txtTag.Location = new System.Drawing.Point(142, 30);
            this.txtTag.Name = "txtTag";
            this.txtTag.Size = new System.Drawing.Size(113, 40);
            this.txtTag.TabIndex = 22;
            this.ToolTip1.SetToolTip(this.txtTag, "Press F2 for available tags");
            this.txtTag.TextChanged += new System.EventHandler(this.txtTag_TextChanged);
            this.txtTag.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTag_KeyDown);
            // 
            // T2KIssueDate
            // 
            this.T2KIssueDate.Location = new System.Drawing.Point(142, 80);
            this.T2KIssueDate.MaxLength = 10;
            this.T2KIssueDate.Name = "T2KIssueDate";
            this.T2KIssueDate.Size = new System.Drawing.Size(115, 40);
            this.T2KIssueDate.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.T2KIssueDate, null);
            this.T2KIssueDate.TextChanged += new System.EventHandler(this.T2KIssueDate_Change);
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(20, 94);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(90, 15);
            this.Label15.TabIndex = 38;
            this.Label15.Text = "ISSUE DATE";
            this.ToolTip1.SetToolTip(this.Label15, null);
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 44);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(90, 15);
            this.Label14.TabIndex = 37;
            this.Label14.Text = "TAG";
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(20, 144);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(90, 15);
            this.Label16.TabIndex = 36;
            this.Label16.Text = "PREVIOUS TAG";
            this.ToolTip1.SetToolTip(this.Label16, null);
            // 
            // cmbVet
            // 
            this.cmbVet.BackColor = System.Drawing.SystemColors.Info;
            this.cmbVet.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
            this.cmbVet.Location = new System.Drawing.Point(191, 80);
            this.cmbVet.Name = "cmbVet";
            this.cmbVet.Size = new System.Drawing.Size(175, 40);
            this.cmbVet.Sorted = true;
            this.cmbVet.TabIndex = 2;
            this.cmbVet.Tag = "Required";
            this.ToolTip1.SetToolTip(this.cmbVet, null);
            this.cmbVet.SelectedIndexChanged += new System.EventHandler(this.cmbVet_SelectedIndexChanged);
            this.cmbVet.TextChanged += new System.EventHandler(this.cmbVet_TextChanged);
            this.cmbVet.Validating += new System.ComponentModel.CancelEventHandler(this.cmbVet_Validating);
            // 
            // chkExcludeWarrant
            // 
            this.chkExcludeWarrant.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.chkExcludeWarrant.Location = new System.Drawing.Point(384, 389);
            this.chkExcludeWarrant.Name = "chkExcludeWarrant";
            this.chkExcludeWarrant.Size = new System.Drawing.Size(182, 27);
            this.chkExcludeWarrant.TabIndex = 17;
            this.chkExcludeWarrant.Text = "Exclude from warrant";
            this.ToolTip1.SetToolTip(this.chkExcludeWarrant, "Exclude this dog for reasons such as seasonal occupancy");
            this.chkExcludeWarrant.CheckedChanged += new System.EventHandler(this.chkExcludeWarrant_CheckedChanged);
            // 
            // chkOutsideSource
            // 
            this.chkOutsideSource.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.chkOutsideSource.Location = new System.Drawing.Point(384, 426);
            this.chkOutsideSource.Name = "chkOutsideSource";
            this.chkOutsideSource.Size = new System.Drawing.Size(264, 27);
            this.chkOutsideSource.TabIndex = 18;
            this.chkOutsideSource.Text = "Registration from outside source";
            this.ToolTip1.SetToolTip(this.chkOutsideSource, "Licenses not registered at the town may be missing  some rabies data and sticker " +
        "may not be in inventory");
            this.chkOutsideSource.CheckedChanged += new System.EventHandler(this.chkOutsideSource_CheckedChanged);
            // 
            // txtDogName
            // 
            this.txtDogName.BackColor = System.Drawing.SystemColors.Info;
            this.txtDogName.Location = new System.Drawing.Point(191, 30);
            this.txtDogName.Name = "txtDogName";
            this.txtDogName.Size = new System.Drawing.Size(175, 40);
            this.txtDogName.TabIndex = 1;
            this.txtDogName.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtDogName, null);
            this.txtDogName.TextChanged += new System.EventHandler(this.txtDogName_TextChanged);
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Info;
            this.txtYear.Location = new System.Drawing.Point(508, 30);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(105, 40);
            this.txtYear.TabIndex = 8;
            this.txtYear.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtYear, null);
            this.txtYear.TextChanged += new System.EventHandler(this.txtYear_TextChanged);
            // 
            // txtSpayNeuterNumber
            // 
            this.txtSpayNeuterNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtSpayNeuterNumber.Location = new System.Drawing.Point(191, 130);
            this.txtSpayNeuterNumber.Name = "txtSpayNeuterNumber";
            this.txtSpayNeuterNumber.Size = new System.Drawing.Size(175, 40);
            this.txtSpayNeuterNumber.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtSpayNeuterNumber, null);
            this.txtSpayNeuterNumber.TextChanged += new System.EventHandler(this.txtSpayNeuterNumber_TextChanged);
            // 
            // txtSex
            // 
            this.txtSex.BackColor = System.Drawing.SystemColors.Info;
            this.txtSex.Location = new System.Drawing.Point(508, 80);
            this.txtSex.MaxLength = 1;
            this.txtSex.Name = "txtSex";
            this.txtSex.Size = new System.Drawing.Size(104, 40);
            this.txtSex.TabIndex = 9;
            this.txtSex.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtSex, null);
            this.txtSex.TextChanged += new System.EventHandler(this.txtSex_TextChanged);
            // 
            // cmbColor
            // 
            this.cmbColor.BackColor = System.Drawing.SystemColors.Info;
            this.cmbColor.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
            this.cmbColor.Location = new System.Drawing.Point(191, 180);
            this.cmbColor.Name = "cmbColor";
            this.cmbColor.Size = new System.Drawing.Size(175, 40);
            this.cmbColor.Sorted = true;
            this.cmbColor.TabIndex = 4;
            this.cmbColor.Tag = "Required";
            this.cmbColor.Text = "cmbColor";
            this.ToolTip1.SetToolTip(this.cmbColor, null);
            this.cmbColor.TextChanged += new System.EventHandler(this.cmbColor_TextChanged);
            // 
            // cmbBreed
            // 
            this.cmbBreed.BackColor = System.Drawing.SystemColors.Window;
            this.cmbBreed.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
            this.cmbBreed.Location = new System.Drawing.Point(191, 230);
            this.cmbBreed.Name = "cmbBreed";
            this.cmbBreed.Size = new System.Drawing.Size(175, 40);
            this.cmbBreed.Sorted = true;
            this.cmbBreed.TabIndex = 5;
            this.cmbBreed.Text = "cmbBreed";
            this.ToolTip1.SetToolTip(this.cmbBreed, null);
            this.cmbBreed.SelectedIndexChanged += new System.EventHandler(this.cmbBreed_SelectedIndexChanged);
            this.cmbBreed.TextChanged += new System.EventHandler(this.cmbBreed_TextChanged);
            this.cmbBreed.Validating += new System.ComponentModel.CancelEventHandler(this.cmbBreed_Validating);
            // 
            // txtMarkings
            // 
            this.txtMarkings.BackColor = System.Drawing.SystemColors.Window;
            this.txtMarkings.Location = new System.Drawing.Point(191, 280);
            this.txtMarkings.Name = "txtMarkings";
            this.txtMarkings.Size = new System.Drawing.Size(175, 40);
            this.txtMarkings.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtMarkings, null);
            this.txtMarkings.TextChanged += new System.EventHandler(this.txtMarkings_TextChanged);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.chkRabiesExempt);
            this.Frame3.Controls.Add(this.txtRabiesTag);
            this.Frame3.Controls.Add(this.txtCertificateNumber);
            this.Frame3.Controls.Add(this.T2KDateVaccinated);
            this.Frame3.Controls.Add(this.t2kExpirationDate);
            this.Frame3.Controls.Add(this.Label18);
            this.Frame3.Controls.Add(this.Label10);
            this.Frame3.Controls.Add(this.Label11);
            this.Frame3.Controls.Add(this.Label12);
            this.Frame3.Controls.Add(this.Label13);
            this.Frame3.Location = new System.Drawing.Point(20, 389);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(311, 275);
            this.Frame3.TabIndex = 39;
            this.Frame3.Text = "Rabies Information";
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // chkRabiesExempt
            // 
            this.chkRabiesExempt.Location = new System.Drawing.Point(179, 236);
            this.chkRabiesExempt.Name = "chkRabiesExempt";
            this.chkRabiesExempt.Size = new System.Drawing.Size(22, 26);
            this.chkRabiesExempt.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.chkRabiesExempt, null);
            // 
            // txtRabiesTag
            // 
            this.txtRabiesTag.BackColor = System.Drawing.SystemColors.Info;
            this.txtRabiesTag.Location = new System.Drawing.Point(179, 30);
            this.txtRabiesTag.Name = "txtRabiesTag";
            this.txtRabiesTag.Size = new System.Drawing.Size(113, 40);
            this.txtRabiesTag.TabIndex = 15;
            this.txtRabiesTag.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtRabiesTag, null);
            this.txtRabiesTag.TextChanged += new System.EventHandler(this.txtRabiesTag_TextChanged);
            // 
            // txtCertificateNumber
            // 
            this.txtCertificateNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtCertificateNumber.Location = new System.Drawing.Point(179, 180);
            this.txtCertificateNumber.Name = "txtCertificateNumber";
            this.txtCertificateNumber.Size = new System.Drawing.Size(113, 40);
            this.txtCertificateNumber.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.txtCertificateNumber, null);
            this.txtCertificateNumber.TextChanged += new System.EventHandler(this.txtCertificateNumber_TextChanged);
            // 
            // T2KDateVaccinated
            // 
            this.T2KDateVaccinated.Location = new System.Drawing.Point(179, 80);
            this.T2KDateVaccinated.Name = "T2KDateVaccinated";
            this.T2KDateVaccinated.Size = new System.Drawing.Size(115, 40);
            this.T2KDateVaccinated.TabIndex = 16;
            this.T2KDateVaccinated.Tag = "Required";
            this.ToolTip1.SetToolTip(this.T2KDateVaccinated, null);
            this.T2KDateVaccinated.TextChanged += new System.EventHandler(this.T2KDateVaccinated_Change);
            // 
            // t2kExpirationDate
            // 
            this.t2kExpirationDate.Location = new System.Drawing.Point(179, 130);
            this.t2kExpirationDate.Name = "t2kExpirationDate";
            this.t2kExpirationDate.Size = new System.Drawing.Size(115, 40);
            this.t2kExpirationDate.TabIndex = 17;
            this.t2kExpirationDate.Tag = "Required";
            this.ToolTip1.SetToolTip(this.t2kExpirationDate, null);
            this.t2kExpirationDate.TextChanged += new System.EventHandler(this.t2kExpirationDate_Change);
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(20, 238);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(122, 18);
            this.Label18.TabIndex = 62;
            this.Label18.Text = "EXEMPT";
            this.ToolTip1.SetToolTip(this.Label18, null);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 44);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(90, 18);
            this.Label10.TabIndex = 43;
            this.Label10.Text = "TAG NUMBER";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 94);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(122, 18);
            this.Label11.TabIndex = 42;
            this.Label11.Text = "DATE VACCINATED";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 144);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(122, 18);
            this.Label12.TabIndex = 41;
            this.Label12.Text = "EXPIRATION DATE";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 194);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(130, 18);
            this.Label13.TabIndex = 40;
            this.Label13.Text = "CERTIFICATE NUMBER";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // chkDeceased
            // 
            this.chkDeceased.Location = new System.Drawing.Point(384, 278);
            this.chkDeceased.Name = "chkDeceased";
            this.chkDeceased.Size = new System.Drawing.Size(211, 27);
            this.chkDeceased.TabIndex = 14;
            this.chkDeceased.Text = "Current Dog is Deceased";
            this.ToolTip1.SetToolTip(this.chkDeceased, null);
            this.chkDeceased.CheckedChanged += new System.EventHandler(this.chkDeceased_CheckedChanged);
            // 
            // chkNeuterSpay
            // 
            this.chkNeuterSpay.Location = new System.Drawing.Point(384, 130);
            this.chkNeuterSpay.Name = "chkNeuterSpay";
            this.chkNeuterSpay.Size = new System.Drawing.Size(146, 27);
            this.chkNeuterSpay.TabIndex = 10;
            this.chkNeuterSpay.Text = "Neuter / Spayed";
            this.ToolTip1.SetToolTip(this.chkNeuterSpay, null);
            this.chkNeuterSpay.CheckedChanged += new System.EventHandler(this.chkNeuterSpay_CheckedChanged);
            // 
            // chkWolfHybrid
            // 
            this.chkWolfHybrid.Location = new System.Drawing.Point(384, 241);
            this.chkWolfHybrid.Name = "chkWolfHybrid";
            this.chkWolfHybrid.Size = new System.Drawing.Size(121, 27);
            this.chkWolfHybrid.TabIndex = 13;
            this.chkWolfHybrid.Text = "Wolf / Hybrid";
            this.ToolTip1.SetToolTip(this.chkWolfHybrid, null);
            this.chkWolfHybrid.CheckedChanged += new System.EventHandler(this.chkWolfHybrid_CheckedChanged);
            // 
            // chkHearingGuid
            // 
            this.chkHearingGuid.Location = new System.Drawing.Point(384, 204);
            this.chkHearingGuid.Name = "chkHearingGuid";
            this.chkHearingGuid.Size = new System.Drawing.Size(142, 27);
            this.chkHearingGuid.TabIndex = 12;
            this.chkHearingGuid.Text = "Hearing / Guide";
            this.ToolTip1.SetToolTip(this.chkHearingGuid, null);
            this.chkHearingGuid.CheckedChanged += new System.EventHandler(this.chkHearingGuid_CheckedChanged);
            // 
            // chkSearchRescue
            // 
            this.chkSearchRescue.Location = new System.Drawing.Point(384, 167);
            this.chkSearchRescue.Name = "chkSearchRescue";
            this.chkSearchRescue.Size = new System.Drawing.Size(245, 27);
            this.chkSearchRescue.TabIndex = 11;
            this.chkSearchRescue.Text = "Service - Search and  Rescue";
            this.ToolTip1.SetToolTip(this.chkSearchRescue, null);
            this.chkSearchRescue.CheckedChanged += new System.EventHandler(this.chkSearchRescue_CheckedChanged);
            // 
            // T2KDOB
            // 
            this.T2KDOB.Location = new System.Drawing.Point(191, 330);
            this.T2KDOB.MaxLength = 10;
            this.T2KDOB.Name = "T2KDOB";
            this.T2KDOB.Size = new System.Drawing.Size(115, 40);
            this.T2KDOB.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.T2KDOB, null);
            this.T2KDOB.TextChanged += new System.EventHandler(this.T2KDOB_Change);
            // 
            // imgDocuments
            // 
            this.imgDocuments.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDocuments.Image = ((System.Drawing.Image)(resources.GetObject("imgDocuments.Image")));
            this.imgDocuments.Location = new System.Drawing.Point(384, 80);
            this.imgDocuments.Name = "imgDocuments";
            this.imgDocuments.Size = new System.Drawing.Size(40, 40);
            this.imgDocuments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgDocuments, "One or more documents are attached to this record");
            this.imgDocuments.Click += new System.EventHandler(this.imgDocuments_Click);
            // 
            // ImgComment
            // 
            this.ImgComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.ImgComment.Image = ((System.Drawing.Image)(resources.GetObject("ImgComment.Image")));
            this.ImgComment.Location = new System.Drawing.Point(384, 30);
            this.ImgComment.Name = "ImgComment";
            this.ImgComment.Size = new System.Drawing.Size(40, 40);
            this.ImgComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.ImgComment, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(66, 18);
            this.Label1.TabIndex = 52;
            this.Label1.Text = "DOG NAME";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(443, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(41, 18);
            this.Label2.TabIndex = 51;
            this.Label2.Text = "YEAR";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 94);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(135, 16);
            this.Label3.TabIndex = 50;
            this.Label3.Text = "VETERINARIAN";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 144);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(164, 18);
            this.Label4.TabIndex = 49;
            this.Label4.Text = "SPAY / NEUTER NUMBER";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(443, 94);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(41, 18);
            this.Label5.TabIndex = 48;
            this.Label5.Text = "SEX";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 194);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(66, 18);
            this.Label6.TabIndex = 47;
            this.Label6.Text = "COLOR";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 244);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(66, 18);
            this.Label7.TabIndex = 46;
            this.Label7.Text = "BREED";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 294);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(103, 18);
            this.Label8.TabIndex = 45;
            this.Label8.Text = "MARKINGS";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 344);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(135, 18);
            this.Label9.TabIndex = 44;
            this.Label9.Text = "DATE OF BIRTH";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // lblOwnerName
            // 
            this.lblOwnerName.Location = new System.Drawing.Point(108, 30);
            this.lblOwnerName.Name = "lblOwnerName";
            this.lblOwnerName.Size = new System.Drawing.Size(187, 15);
            this.lblOwnerName.TabIndex = 57;
            this.ToolTip1.SetToolTip(this.lblOwnerName, null);
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(30, 30);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(50, 15);
            this.Label17.TabIndex = 56;
            this.Label17.Text = "OWNER";
            this.ToolTip1.SetToolTip(this.Label17, null);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBreeds,
            this.mnuColors,
            this.mnuVets,
            this.mnuInventory,
            this.mnuPrinterSettings,
            this.mnuViewDocs,
            this.mnuDogHistory,
            this.mnuPrintFront,
            this.mnuPrintBack});
            this.MainMenu1.Name = null;
            // 
            // mnuBreeds
            // 
            this.mnuBreeds.Index = 0;
            this.mnuBreeds.Name = "mnuBreeds";
            this.mnuBreeds.Text = "Breeds";
            this.mnuBreeds.Visible = false;
            this.mnuBreeds.Click += new System.EventHandler(this.mnuBreeds_Click);
            // 
            // mnuColors
            // 
            this.mnuColors.Index = 1;
            this.mnuColors.Name = "mnuColors";
            this.mnuColors.Text = "Colors";
            this.mnuColors.Visible = false;
            this.mnuColors.Click += new System.EventHandler(this.mnuColors_Click);
            // 
            // mnuVets
            // 
            this.mnuVets.Index = 2;
            this.mnuVets.Name = "mnuVets";
            this.mnuVets.Text = "Veterinarians";
            this.mnuVets.Visible = false;
            this.mnuVets.Click += new System.EventHandler(this.mnuVets_Click);
            // 
            // mnuInventory
            // 
            this.mnuInventory.Index = 3;
            this.mnuInventory.Name = "mnuInventory";
            this.mnuInventory.Text = "Tag / Sticker Inventory";
            this.mnuInventory.Click += new System.EventHandler(this.mnuInventory_Click);
            // 
            // mnuPrinterSettings
            // 
            this.mnuPrinterSettings.Index = 4;
            this.mnuPrinterSettings.Name = "mnuPrinterSettings";
            this.mnuPrinterSettings.Text = "Printer Settings";
            this.mnuPrinterSettings.Click += new System.EventHandler(this.mnuPrinterSettings_Click);
            // 
            // mnuViewDocs
            // 
            this.mnuViewDocs.Index = 5;
            this.mnuViewDocs.Name = "mnuViewDocs";
            this.mnuViewDocs.Text = "View Attached Documents";
            this.mnuViewDocs.Click += new System.EventHandler(this.mnuViewDocs_Click);
            // 
            // mnuDogHistory
            // 
            this.mnuDogHistory.Index = 6;
            this.mnuDogHistory.Name = "mnuDogHistory";
            this.mnuDogHistory.Text = "History Report";
            this.mnuDogHistory.Click += new System.EventHandler(this.mnuDogHistory_Click);
            // 
            // mnuPrintFront
            // 
            this.mnuPrintFront.Index = 7;
            this.mnuPrintFront.Name = "mnuPrintFront";
            this.mnuPrintFront.Text = "Print Preview Front of Dog License";
            this.mnuPrintFront.Click += new System.EventHandler(this.mnuPrintFront_Click);
            // 
            // mnuPrintBack
            // 
            this.mnuPrintBack.Index = 8;
            this.mnuPrintBack.Name = "mnuPrintBack";
            this.mnuPrintBack.Text = "Print Preview Back of Dog License";
            this.mnuPrintBack.Click += new System.EventHandler(this.mnuPrintBack_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = -1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Dog";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = -1;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuFees
            // 
            this.mnuFees.Index = -1;
            this.mnuFees.Name = "mnuFees";
            this.mnuFees.Text = "Fees";
            this.mnuFees.Click += new System.EventHandler(this.mnuFees_Click);
            // 
            // mnuComments
            // 
            this.mnuComments.Index = -1;
            this.mnuComments.Name = "mnuComments";
            this.mnuComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComments.Text = "Comments";
            this.mnuComments.Click += new System.EventHandler(this.mnuComments_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = -1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdDeleteDog
            // 
            this.cmdDeleteDog.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteDog.Location = new System.Drawing.Point(584, 29);
            this.cmdDeleteDog.Name = "cmdDeleteDog";
            this.cmdDeleteDog.Size = new System.Drawing.Size(85, 24);
            this.cmdDeleteDog.TabIndex = 1;
            this.cmdDeleteDog.Text = "Delete Dog";
            this.cmdDeleteDog.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdComments
            // 
            this.cmdComments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComments.Location = new System.Drawing.Point(675, 29);
            this.cmdComments.Name = "cmdComments";
            this.cmdComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComments.Size = new System.Drawing.Size(80, 24);
            this.cmdComments.TabIndex = 2;
            this.cmdComments.Text = "Comments";
            this.cmdComments.Click += new System.EventHandler(this.mnuComments_Click);
            // 
            // cmdFees
            // 
            this.cmdFees.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFees.Location = new System.Drawing.Point(533, 29);
            this.cmdFees.Name = "cmdFees";
            this.cmdFees.Size = new System.Drawing.Size(45, 24);
            this.cmdFees.TabIndex = 3;
            this.cmdFees.Text = "Fees";
            this.cmdFees.Click += new System.EventHandler(this.mnuFees_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(330, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmNewDog
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(795, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmNewDog";
            this.Text = "Dog Info";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmNewDog_Load);
            this.Activated += new System.EventHandler(this.frmNewDog_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewDog_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framStickerInventory)).EndInit();
            this.framStickerInventory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStickerInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNotRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framTransaction)).EndInit();
            this.framTransaction.ResumeLayout(false);
            this.framTransaction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserDefinedFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTransferLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWarrantFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLateFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReplacementTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTempLicense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNuisance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDangerous)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludeWarrant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOutsideSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRabiesExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateVaccinated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kExpirationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNeuterSpay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWolfHybrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHearingGuid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSearchRescue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdDeleteDog;
		private FCButton cmdComments;
		private FCButton cmdFees;
		private FCButton cmdSave;
        public FCCheckBox chkNuisance;
        public FCCheckBox chkDangerous;
    }
}
