//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptRegistrationListing.
	/// </summary>
	public partial class rptRegistrationListing : BaseSectionReport
	{
		public rptRegistrationListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Registration Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptRegistrationListing InstancePtr
		{
			get
			{
				return (rptRegistrationListing)Sys.GetInstance(typeof(rptRegistrationListing));
			}
		}

		protected rptRegistrationListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
				clsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRegistrationListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strPhone;
		int intLine;
		clsDRWrapper rs = new clsDRWrapper();
		int intPageNumber;
		// vbPorter upgrade warning: boolPrintDate As bool	OnWrite(CheckState)
		bool boolPrintDate;
		clsDRWrapper clsTemp = new clsDRWrapper();
		string strYear1;
		string strYear2;

		public void Init(string strStart, string strEnd, bool modalDialog)
		{
			strYear1 = fecherFoundation.Strings.Trim(strStart);
			strYear2 = fecherFoundation.Strings.Trim(strStart);
			frmReportViewer.InstancePtr.Init(this, intModal: 1, boolAllowEmail: true, strAttachmentName: "RegistrationListing", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			/*? On Error Resume Next  */
			string[] strAry = null;
			string strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strLocation;
			//clsDRWrapper rsPhone = new clsDRWrapper();
			newdog:
			;
			if (rs.EndOfFile())
				return;
			intLine += 1;
			if (Information.IsDate(rs.Get_Fields("rabstickerissue")))
			{
				if (!(rs.Get_Fields_DateTime("rabstickerissue").ToOADate() == 0))
				{
					txtDatePaid.Text = FCConvert.ToString(rs.Get_Fields("RabStickerIssue"));
				}
				else
				{
					txtDatePaid.Text = "";
				}
			}
			else
			{
				txtDatePaid.Text = "";
			}
			txtDogName.Text = rs.Get_Fields_String("DogName");
			if (FCConvert.ToInt32(rs.Get_Fields("year")) != 0)
			{
				txtRegYear.Text = FCConvert.ToString(rs.Get_Fields("year"));
			}
			else
			{
				txtRegYear.Text = "";
			}
			txtAddress.Text = rs.Get_Fields_String("Address1") + " " + rs.Get_Fields_String("City") + ", " + rs.Get_Fields("State") + " " + rs.Get_Fields_String("Zip");
			txtColorBreed.Text = rs.Get_Fields_String("dogColor") + "\r" + rs.Get_Fields_String("dogBreed");
			txtRabiesDate.Text = Strings.Format(rs.Get_Fields("RabiesExpDate"), "MM/dd/yyyy");
			strLocation = fecherFoundation.Strings.Trim(rs.Get_Fields_String("LocationNum") + " " + rs.Get_Fields_String("LocationSTR"));
			txtOwnerName.Text = rs.Get_Fields_String("FullName") + "\r\n" + strLocation;
			strPhone = "";
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("phonenumber"))).Length == 7)
			{
				strPhone = "(207) " + Strings.Left(FCConvert.ToString(rs.Get_Fields_String("phonenumber")), 3) + "-" + Strings.Right(FCConvert.ToString(rs.Get_Fields_String("phonenumber")), 4);
			}
			else if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("phonenumber"))).Length == 10 && Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("phonenumber"))), 3) == "000")
			{
				strPhone = "(207) " + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("phonenumber")), 4, 3) + "-" + Strings.Right(FCConvert.ToString(rs.Get_Fields_String("phonenumber")), 4);
			}
			else
			{
				strPhone = Strings.Format(rs.Get_Fields_String("phonenumber"), "(###) ###-####");
			}
			if (fecherFoundation.Strings.Trim(strPhone) != "() -")
			{
				txtAddress.Text = txtAddress.Text + "\r" + strPhone;
			}
			txtTagNumber.Text = "";
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("KennelDog")))
			{
				clsTemp.OpenRecordset("select * from kennellicense where ownernum = " + rs.Get_Fields_Int32("ownernum"), "twck0000.vb1");
				while (!clsTemp.EndOfFile())
				{
					if (Conversion.Val(strYear1) > 0 && Conversion.Val(strYear2) > 0)
					{
						if (!(Conversion.Val(clsTemp.Get_Fields("year")) >= Conversion.Val(strYear1) && Conversion.Val(clsTemp.Get_Fields("year")) <= Conversion.Val(strYear2)))
						{
							rs.MoveNext();
							goto newdog;
						}
					}
					strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("dognumbers"));
					if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
					{
						strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
						{
							if (Conversion.Val(strAry[x]) == Conversion.Val(rs.Get_Fields_Int32("dognumber")))
							{
								txtTagNumber.Text = "K# " + FCConvert.ToString(modGlobalRoutines.GetKennelLicenseNumber(clsTemp.Get_Fields_Int32("licenseid")));
							}
						}
						// x
					}
					clsTemp.MoveNext();
				}
			}
			else
			{
				txtTagNumber.Text = rs.Get_Fields_String("TagLicNum");
			}
			if (!rs.EndOfFile())
				rs.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtTown.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "H:mm AM/PM");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtCaption.Text = "Town Clerk's Report";
			txtCaption2.Text = "Registration Listing";
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			intPageNumber = 1;
			string strSQL;
			strSQL = "select " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView.*,DogOwner.LocationNum, DogOwner.PartyID, DogOwner.LocationSTR, DogInfo.* from doginfo inner join dogowner on (doginfo.ownernum = dogowner.id) inner join " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView on (dogowner.partyid = " + rs.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView.PartyID) ";
			if (modGNBas.Statics.gstrSortField == "LastName")
				modGNBas.Statics.gstrSortField = "LastName,FirstName";
			boolPrintDate = 0 != frmReportListing.InstancePtr.chkPrintDate.CheckState;
			// Call rs.OpenRecordset("SELECT p.*, DogOwner.LocationNum, DogOwner.PartyID, DogOwner.LocationSTR, DogInfo.* "
			// & "FROM DogInfo INNER JOIN DogOwner ON (DogInfo.OwnerNum = dogowner.ID) CROSS APPLY " & rs.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddressAndPhone(PartyID, NULL, 'CK', NULL) as p "
			// & gstrWhereClause & " Order by " & gstrSortField, DEFAULTDATABASENAME)
			strSQL += modGNBas.Statics.gstrWhereClause + "Order by " + modGNBas.Statics.gstrSortField;
			rs.OpenRecordset(strSQL, "Clerk");
			intLine = 0;
		}

		
	}
}
