//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWCK0000
{
	public partial class frmBirthForeign : BaseForm
	{
		public frmBirthForeign()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			lblLabels = new System.Collections.Generic.List<FCLabel>();
			lblLabels.AddControlArrayElement(lblLabels_0, 0);
			lblLabels.AddControlArrayElement(lblLabels_1, 1);
			lblLabels.AddControlArrayElement(lblLabels_2, 2);
			lblLabels.AddControlArrayElement(lblLabels_3, 3);
			lblLabels.AddControlArrayElement(lblLabels_4, 4);
			lblLabels.AddControlArrayElement(lblLabels_5, 5);
			lblLabels.AddControlArrayElement(lblLabels_6, 6);
			lblLabels.AddControlArrayElement(lblLabels_7, 7);
			lblLabels.AddControlArrayElement(lblLabels_8, 8);
			lblLabels.AddControlArrayElement(lblLabels_9, 9);
			lblLabels.AddControlArrayElement(lblLabels_11, 10);
			lblLabels.AddControlArrayElement(lblLabels_12, 11);
			lblLabels.AddControlArrayElement(lblLabels_13, 12);
			lblLabels.AddControlArrayElement(lblLabels_14, 13);
			lblLabels.AddControlArrayElement(lblLabels_15, 14);
			lblLabels.AddControlArrayElement(lblLabels_16, 15);
			lblLabels.AddControlArrayElement(lblLabels_17, 16);
			lblLabels.AddControlArrayElement(lblLabels_18, 17);
			lblLabels.AddControlArrayElement(lblLabels_19, 18);
			lblLabels.AddControlArrayElement(lblLabels_20, 19);
			lblLabels.AddControlArrayElement(lblLabels_21, 20);
			lblLabels.AddControlArrayElement(lblLabels_25, 21);
			lblLabels.AddControlArrayElement(lblLabels_28, 22);
			lblLabels.AddControlArrayElement(lblLabels_29, 23);
			lblLabels.AddControlArrayElement(lblLabels_30, 24);
			lblLabels.AddControlArrayElement(lblLabels_35, 25);
			lblLabels.AddControlArrayElement(lblLabels_36, 26);
			lblLabels.AddControlArrayElement(lblLabels_39, 27);
			lblLabels.AddControlArrayElement(lblLabels_42, 28);
			lblBirthCertNum = new System.Collections.Generic.List<FCLabel>();
			lblBirthCertNum.AddControlArrayElement(lblBirthCertNum_28, 0);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmBirthForeign InstancePtr
		{
			get
			{
				return (frmBirthForeign)Sys.GetInstance(typeof(frmBirthForeign));
			}
		}

		protected frmBirthForeign _InstancePtr = null;
		//=========================================================
		// **************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY: MATTHEW S. LARRABEE
		// DATE:       MAY 16,2002
		//
		// MODIFIED BY:
		//
		// NOTES: THIS MODULE WAS CREATED BY REQUEST FROM STATE AND SOME
		// OTHER TOWNS. THIS DATA IS JUST STORED AND CANNOT BE PRINTED.
		// **************************************************************
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rs = new clsDRWrapper();
			public clsDRWrapper rs_AutoInitialized = null;
			public clsDRWrapper rs
			{
				get
				{
					if ( rs_AutoInitialized == null)
					{
						 rs_AutoInitialized = new clsDRWrapper();
					}
					return rs_AutoInitialized;
				}
				set
				{
					 rs_AutoInitialized = value;
				}
			}
		public string strSQL = "";
		public bool Popped;
		public bool Adding;
		public bool FormActivated;
		public bool GoodValidation;
		public bool boolSaveSuccessfully;
		public object BirthCertNum;
		private int intDataChanged;
		private int intFrameValue;
		private int intRecordID;
		// vbPorter upgrade warning: boolExit As int	OnWrite(bool)
		private int boolExit;

		public void Init(int lngID)
		{
			intRecordID = lngID;
			this.Show(App.MainForm);
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGNBas.Statics.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblregionS where townnumber > 0 order by townnumber", "CentralData");
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("townnumber") + ";" + clsLoad.Get_Fields_String("townname") + "\t" + clsLoad.Get_Fields("townnumber") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				strTemp = "0";
				gridTownCode.Visible = false;
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}

		public void SetCurrentRecordID(int intID)
		{
			intRecordID = intID;
		}

		private void Comment_Click()
		{
		}

		private void frmBirthForeign_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTownCode();
		}

		private void mnuComment_Click(object sender, System.EventArgs e)
		{
			if (intRecordID == 0)
			{
				MessageBox.Show("This record doesn't exist yet.  You must save before this record can be tied to a comment.", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (frmCommentOld.InstancePtr.Init("CK", "BirthsForeign", "Comments", "ID", intRecordID, boolModal: true))
			{
				// Set ImgComment.Picture = Nothing
			}
			else
			{
				// Set ImgComment.Picture = MDIParent.ImageList2.ListImages(1).Picture
			}
		}

		private void txtAttestDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtAttestDate.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtAttestDate.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtAttestDate.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtAttestDate.Text = strD;
					}
				}
			}
		}

		private void txtChildState_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtChildState.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtChildTown_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtChildTown.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtChildTown_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 0;
		}

		private void txtClerkCityTown_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtClerkCityTown_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtClerkCityTown.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtFatherDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtFatherDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtFatherDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtFatherDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtFatherDOB.Text = strD;
					}
				}
			}
		}

		private void txtFathersState_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtFathersState.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtFathersState_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 1;
		}

		private void chkDeceased_CheckedChanged(object sender, System.EventArgs e)
		{
			lblDeceased.Visible = 0 != (chkDeceased.CheckState);
			mebDateDeceased.Visible = 0 != chkDeceased.CheckState;
		}

		private void cmdFind_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			modGNBas.Statics.boolGetClerk = true;
			frmListRegistrar.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrRegistrar) == string.Empty)
				return;
			rs.OpenRecordset("Select * from DefaultRegistrarNames where ID = " + modGNBas.Statics.gstrRegistrar, modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				if (FCConvert.ToString(rs.Get_Fields_String("MI")) != "")
				{
					txtClerkName.Text = rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("MI") + " " + rs.Get_Fields_String("LastName");
				}
				else
				{
					txtClerkName.Text = rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("LastName");
				}
				txtClerkCityTown.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ClerkTown")));
			}
			txtClerkName.Focus();
			modGNBas.Statics.boolGetClerk = false;
		}

		public void LoadData()
		{
			if (modGNBas.Statics.boolSetDefaultTowns)
			{
				modGNBas.Statics.boolSetDefaultTowns = false;
				return;
			}
			else if (modGNBas.Statics.boolGetClerk)
			{
				modGNBas.Statics.boolGetClerk = false;
				return;
			}
			if (FormActivated == false)
			{
				// lblWMuniName.Text = "- " & MuniName & " -"
				// lblScreenTitle.Text = "Birth Certificate"
				// lblDate.Text = Format(Date, "MM/dd/yyyy")
				strSQL = "SELECT * FROM BirthsForeign WHERE ID = " + FCConvert.ToString(intRecordID) + " order by ChildLastName,ChildFirstName ";
				rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
				FormActivated = true;
			}

			if (intRecordID == 0)
			{
				modClerkGeneral.Statics.AddingBirthCertificate = false;
				Adding = true;
				ClearBoxes();
			}
			else
			{
				FillBoxes();
			}
			MenuCheck();
			modDirtyForm.ClearDirtyControls(this);
			intDataChanged = 0;
		}

		private void cmdFind_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 2;
		}

		public void frmBirthForeign_Activated(object sender, System.EventArgs e)
		{
			//App.DoEvents();
		}

		private void ClearBoxes()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				/* object obj; */
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				foreach (Control obj in FCUtils.GetAllControls(this))
				{
					if (obj is FCTextBox)
						obj.Text = "";
				}
				chkIllegitBirth.CheckState = Wisej.Web.CheckState.Unchecked;
				chkDeceased.CheckState = Wisej.Web.CheckState.Unchecked;
				chkMarriageOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
				chkRequired.CheckState = Wisej.Web.CheckState.Unchecked;
				// txtAttestDate.Text = Format(Date, "MM/dd/yyyy")
				mebDateDeceased.Text = string.Empty;
				txtChildMI.Text = "";
				txtChildDOB.Text = "";
				txtActualDate.Text = "";
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillBoxes()
		{
			if (rs.EndOfFile())
				return;
			txtMotherDOB.Text = FCConvert.ToString(rs.Get_Fields_String("Motherdob"));
			txtFatherDOB.Text = FCConvert.ToString(rs.Get_Fields_String("Fatherdob"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FileNumber")))
				txtFileNumber.Text = FCConvert.ToString(rs.Get_Fields_String("FileNumber"));
			mebDateDeceased.Text = Strings.Format(rs.Get_Fields("DeceasedDate"), "MM/dd/yyyy");
			if (rs.Get_Fields_Boolean("Legitimate") == true)
			{
				chkIllegitBirth.CheckState = Wisej.Web.CheckState.Unchecked;
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersLastName")))
					txtFatherLastName.Text = FCConvert.ToString(rs.Get_Fields_String("FathersLastName"));
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersFirstName")))
					txtFatherFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FathersFirstName"));
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersMiddleName")))
					txtFatherMI.Text = FCConvert.ToString(rs.Get_Fields_String("FathersMiddleName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersDesignation")) == false)
					txtFatherDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("FathersDesignation"));
			}
			else
			{
				chkIllegitBirth.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("AmendedInfo")))
				txtAmendedInfo.Text = FCConvert.ToString(rs.Get_Fields_String("AmendedInfo"));
			chkRequired.CheckState = (CheckState)((rs.Get_Fields_Boolean("NoRequiredFields")) ? 1 : 0);
			chkDeceased.CheckState = (CheckState)((rs.Get_Fields_Boolean("DeathRecordInOffice")) ? 1 : 0);
			chkMarriageOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("MarriageOnFile")) ? 1 : 0);
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ChildLastName")))
				txtChildLastName.Text = FCConvert.ToString(rs.Get_Fields_String("ChildLastName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ChildFirstName")))
				txtChildFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("ChildFirstName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ChildMiddleName")))
				txtChildMI.Text = FCConvert.ToString(rs.Get_Fields_String("ChildMiddleName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ChildSex")))
				txtChildSex.Text = FCConvert.ToString(rs.Get_Fields_String("ChildSex"));
			if (Information.IsDate(rs.Get_Fields_String("ChildDOB")))
			{
				txtChildDOB.Text = Strings.Format(rs.Get_Fields_String("ChildDOB"), "MM/dd/yyyy");
			}
			else
			{
				txtChildDOB.Text = FCConvert.ToString(rs.Get_Fields_String("ChildDOBDescription"));
			}
			if (Information.IsDate(rs.Get_Fields("ActualDate")) && !rs.Get_Fields_DateTime("ActualDate").IsEmptyDate())
			{
				txtActualDate.Text = Strings.Format(rs.Get_Fields_DateTime("ActualDate"), "MM/dd/yyyy");
			}
			else
			{
				txtActualDate.Text = "";
			}
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ChildCountry")))
				txtChildCountry.Text = FCConvert.ToString(rs.Get_Fields_String("ChildCountry"));
			txtChildState.Text = FCConvert.ToString(rs.Get_Fields_String("ChildState"));
			txtChildTown.Text = FCConvert.ToString(rs.Get_Fields_String("ChildTown"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersLastName")))
				txtMotherLastName.Text = FCConvert.ToString(rs.Get_Fields_String("MothersLastName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersFirstName")))
				txtMotherFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("MothersFirstName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersMiddleName")))
				txtMotherMI.Text = rs.Get_Fields_String("MothersMiddleName") + "";
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersMaidenName")))
				txtMotherMaidenName.Text = rs.Get_Fields_String("MothersMaidenName") + "";
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersAge")))
				txtMothersAge.Text = rs.Get_Fields_String("MothersAge") + "";
			txtMothersStateOfBirth.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MothersStateOfBirth")));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MothersStreet")))
				txtMothersStreet.Text = rs.Get_Fields_String("MothersStreet") + "";
			txtMothersTown.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MothersTown")));
			txtMothersState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MothersState")));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersLastName")))
				txtFatherLastName.Text = FCConvert.ToString(rs.Get_Fields_String("FathersLastName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersFirstName")))
				txtFatherFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FathersFirstName"));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersMiddleName")))
				txtFatherMI.Text = rs.Get_Fields_String("FathersMiddleName") + "";
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersDesignation")))
				txtFatherDesignation.Text = rs.Get_Fields_String("FathersDesignation") + "";
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FathersAge")))
				txtFathersAge.Text = rs.Get_Fields_String("FathersAge") + "";
			txtFathersState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("FathersStateOfBirth")));
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ClerkName")))
				txtClerkName.Text = FCConvert.ToString(rs.Get_Fields_String("ClerkName"));
			txtClerkCityTown.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ClerkTown")));
			txtAttestDate.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ClerkAttestDate")));
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) > 0)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("towncode"))));
			}
		}

		private void frmBirthForeign_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			boolExit = FCConvert.ToInt16(false);
			if (KeyCode == Keys.Escape)
				mnuProcessQuit_Click();
		}

		private void frmBirthForeign_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FormActivated = false;
			//modGNBas.Statics.boolBirthOpen = false;
			intDataChanged = FCConvert.ToInt16(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "mnuProcessSave_Click");
			Adding = false;
			//MDIParent.InstancePtr.Show();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuGotoFirstRecord_Click()
		{
			rs.MoveFirst();
			FillBoxes();
		}

		private void mnuGotoLastRecord_Click()
		{
			rs.MoveLast();
			FillBoxes();
		}

		private void mnuGotoNextRecord_Click()
		{
			rs.MoveNext();
			if (rs.EndOfFile() == true)
				rs.MoveFirst();
			ClearBoxes();
			FillBoxes();
		}

		private void mnuGotoPreviousRecord_Click()
		{
			rs.MovePrevious();
			if (rs.BeginningOfFile() == true)
				rs.MoveLast();
			ClearBoxes();
			FillBoxes();
		}

		private void mnuGotoSpecificRecord_Click()
		{
			string hold;
			redotag:
			;
			hold = "";
			hold = Interaction.InputBox("Enter Child's Last Name.", "Update Record", null);
			if (hold == "")
				return;
			rs.MoveLast();
			rs.MoveFirst();
			rs.FindFirstRecord("LastName", "'" + hold + "'");
			if (rs.NoMatch)
			{
				MessageBox.Show("There is no match for the Name entered.");
				goto redotag;
			}
			FillBoxes();
		}

		private void mnuProcessAddNewRecord_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				Adding = true;
				intRecordID = 0;
				ClearBoxes();
				MenuCheck();
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " has been encountered." + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				return;
			}
		}

		private void mnuProcessDeleteRecord_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
			DialogResult answer;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				answer = MessageBox.Show("Are you sure you want to permanently delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					if (intRecordID > 0)
					{
						rs.Execute("Delete from BirthsForeign where ID = " + FCConvert.ToString(intRecordID), modGNBas.DEFAULTCLERKDATABASE);
						MessageBox.Show("Record deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						Close();
					}
					else
					{
						MessageBox.Show("There is no active record or record has not been saved. Delete cannot be completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " has been encountered." + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				return;
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		public void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			object Ans;
			SaveData();
		}

		private void chkIllegitBirth_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkIllegitBirth.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkIllegitBirth.ForeColor = Color.Red;
				modGNBas.Statics.boolIllegitbirth = true;
			}
			else
			{
				chkIllegitBirth.ForeColor = Color.Black;
				modGNBas.Statics.boolIllegitbirth = false;
			}
			intDataChanged += 1;
		}

		private bool SaveData()
		{
			bool SaveData = false;
			clsDRWrapper rsClerkInfo = new clsDRWrapper();
			bool blnUpdateFile = false;
			GoodValidation = false;
			SaveData = false;
			ValidateEntry();
			if (GoodValidation == false)
			{
				// MsgBox "Unable to Update.", vbOKOnly, "Error in Save"
				return SaveData;
			}
			boolSaveSuccessfully = false;
			// If comment has   ***  in front of the field name then that
			// field is required and the validation routine will not allow the
			// user to get this far if that field does not have data in it.
			rs.OpenRecordset("Select * from BirthsForeign where ID = " + FCConvert.ToString(intRecordID), modGNBas.DEFAULTCLERKDATABASE);
			if (rs.EndOfFile())
			{
				rs.AddNew();
				blnUpdateFile = true;
			}
			else
			{
				rs.Edit();
				blnUpdateFile = false;
			}
			rs.Set_Fields("MotherDOB", fecherFoundation.Strings.Trim(txtMotherDOB.Text));
			rs.Set_Fields("FatherDOB", fecherFoundation.Strings.Trim(txtFatherDOB.Text));
			if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != string.Empty && fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("filenumber"))) == string.Empty)
			{
				blnUpdateFile = true;
			}
			rs.Set_Fields("FileNumber", txtFileNumber.Text);
			if (fecherFoundation.FCUtils.IsNull(mebDateDeceased.Text) == true || mebDateDeceased.IsEmpty)
			{
				rs.Set_Fields("DeceasedDate", null);
			}
			else
			{
				rs.Set_Fields("DeceasedDate", mebDateDeceased.Text);
			}
			rs.Set_Fields("AmendedInfo", txtAmendedInfo.Text);
			rs.Set_Fields("Legitimate", chkIllegitBirth.CheckState == Wisej.Web.CheckState.Unchecked);
			rs.Set_Fields("NoRequiredFields", chkRequired.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("DeathRecordInOffice", chkDeceased.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("MarriageOnFile", chkMarriageOnFile.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("ChildFirstName", txtChildFirstName.Text);
			rs.Set_Fields("ChildMiddleName", txtChildMI.Text);
			rs.Set_Fields("ChildLastName", txtChildLastName.Text);

			if (Conversion.Val(txtChildDOB.Text) != 0 && Information.IsDate(txtChildDOB.Text))
			{
				rs.Set_Fields("ChildDOB", Strings.Format(txtChildDOB.Text, "MM/dd/yyyy"));
				rs.Set_Fields("ChildDOBDescription", "");
			}
			else
			{
				rs.Set_Fields("ChildDOB", null);
				rs.Set_Fields("ChildDOBDescription", txtChildDOB.Text);
			}
			if (Information.IsDate(txtActualDate.Text))
			{
				rs.Set_Fields("ActualDate", fecherFoundation.DateAndTime.DateValue(txtActualDate.Text));
			}
			else
			{
				rs.Set_Fields("ActualDate", null);
			}
			rs.Set_Fields("ChildCountry", txtChildCountry.Text);
			rs.Set_Fields("ChildState", fecherFoundation.Strings.Trim(txtChildState.Text));
			rs.Set_Fields("ChildTown", fecherFoundation.Strings.Trim(txtChildTown.Text));
			rs.Set_Fields("ChildSex", txtChildSex.Text);
			rs.Set_Fields("MothersFirstName", txtMotherFirstName.Text);
			rs.Set_Fields("MothersMiddleName", txtMotherMI.Text);
			rs.Set_Fields("MothersLastName", txtMotherLastName.Text);
			rs.Set_Fields("MothersMaidenName", txtMotherMaidenName.Text);
			rs.Set_Fields("MothersAge", txtMothersAge.Text);
			rs.Set_Fields("MothersStreet", txtMothersStreet.Text);
			rs.Set_Fields("MothersStateOfBirth", fecherFoundation.Strings.Trim(txtMothersStateOfBirth.Text));
			rs.Set_Fields("MothersState", fecherFoundation.Strings.Trim(txtMothersState.Text));
			rs.Set_Fields("MothersTown", fecherFoundation.Strings.Trim(txtMothersTown.Text));
			rs.Set_Fields("FathersLastName", txtFatherLastName.Text);
			rs.Set_Fields("FathersFirstName", txtFatherFirstName.Text);
			rs.Set_Fields("FathersMiddleName", txtFatherMI.Text);
			rs.Set_Fields("FathersDesignation", txtFatherDesignation.Text);
			rs.Set_Fields("FathersAge", txtFathersAge.Text);
			rs.Set_Fields("FathersStateOfBirth", fecherFoundation.Strings.Trim(txtFathersState.Text));
			rs.Set_Fields("ClerkName", txtClerkName.Text);
			rs.Set_Fields("ClerkTown", fecherFoundation.Strings.Trim(txtClerkCityTown.Text));
			rs.Set_Fields("ClerkAttestDate", txtAttestDate.Text);
			rs.Set_Fields("towncode", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			rs.Update();
			intRecordID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != "" && blnUpdateFile)
			{
				rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
				if (rsClerkInfo.BeginningOfFile() != true && rsClerkInfo.EndOfFile() != true)
				{
					rsClerkInfo.Edit();
				}
				else
				{
					rsClerkInfo.AddNew();
				}
				rsClerkInfo.Set_Fields("LastBirthFileNumber", fecherFoundation.Strings.Trim(txtFileNumber.Text));
				rsClerkInfo.Update();
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + fecherFoundation.Strings.Trim(txtFileNumber.Text));
			}
			SaveData = true;
			intDataChanged = 0;
			modDirtyForm.ClearDirtyControls(this);
			boolSaveSuccessfully = true;
			MessageBox.Show("Save Completed Successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			return SaveData;
		}

		private void ValidateEntry()
		{
			
			GoodValidation = true;
			// End If
		}

		private void mnuRegistrarNames_Click(object sender, System.EventArgs e)
		{
			frmRegistrarNames.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
				mnuProcessQuit_Click();
		}

		private void txtChildDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{

			string strD = "";
			if (fecherFoundation.Strings.Trim(txtChildDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtChildDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtChildDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtChildDOB.Text = strD;
					}
				}
			}
			if (Information.IsDate(txtChildDOB.Text))
			{
				txtActualDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateValue(txtChildDOB.Text), "MM/dd/yyyy");
			}
		}

		private void MenuCheck()
		{
			if (Adding == true)
			{
				cmdNew.Enabled = false;
				cmdDelete.Enabled = false;
			}
			else
			{
				cmdNew.Enabled = true;
				cmdDelete.Enabled = true;
			}
		}

		private void frmBirthForeign_Load(object sender, System.EventArgs e)
		{

			clsDRWrapper rsClerkInfo = new clsDRWrapper();
			//modGNBas.Statics.boolBirthOpen = true;
			SetupGridTownCode();
			LoadData();
			rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
			if (rsClerkInfo.EndOfFile() != true && rsClerkInfo.BeginningOfFile() != true)
			{
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + rsClerkInfo.Get_Fields_String("LastBirthFileNumber"));
			}
			else
			{
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: UNKNOWN");
			}
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
		}


        private void txtChildSex_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtChildSex.Modified)
            {
                string text = txtChildSex.Text;
                if (text != "M" && text != "F")
                {
                    MessageBox.Show("Must enter 'M' or 'F'.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
        }

        private void txtClerkName_Enter(object sender, System.EventArgs e)
		{
			txtClerkName.SelectionStart = 0;
			txtClerkName.SelectionLength = 50;
		}

		private void txtMotherDOB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtMotherDOB.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtMotherDOB.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtMotherDOB.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtMotherDOB.Text = strD;
					}
				}
			}
		}

		private void txtMotherFirstName_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 1;
		}

		private void txtMothersState_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtMothersState.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtMothersStateOfBirth_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtMothersStateOfBirth.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtMothersTown_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtMothersTown.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtMotherDOB_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtMothersStateOfBirth_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtMotherFirstName_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtMotherLastName_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtMotherMI_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtMotherMaidenName_TextChanged(object sender, EventArgs e)
		{

		}

		private void txtMothersAge_TextChanged(object sender, EventArgs e)
		{

		}

		private void lblLabels_5_Click(object sender, EventArgs e)
		{

		}

		private void lblLabels_39_Click(object sender, EventArgs e)
		{

		}

		private void lblLabels_19_Click(object sender, EventArgs e)
		{

		}

		private void lblLabels_20_Click(object sender, EventArgs e)
		{

		}

		private void lblLabels_28_Click(object sender, EventArgs e)
		{

		}

		private void lblLabels_29_Click(object sender, EventArgs e)
		{

		}
	}
}
