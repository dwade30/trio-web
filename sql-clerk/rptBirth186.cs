//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBirth186.
	/// </summary>
	public partial class rptBirth186 : BaseSectionReport
	{
		public rptBirth186()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Birth Certificate ";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBirth186 InstancePtr
		{
			get
			{
				return (rptBirth186)Sys.GetInstance(typeof(rptBirth186));
			}
		}

		protected rptBirth186 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData2.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBirth186	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private clsDRWrapper rsData2 = new clsDRWrapper();
		private bool boolPrintTest;
		private double dblLineAdjust;

		public void Init(bool boolTestPrint, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			if (boolPrintTest)
			{
				dblLineAdjust = dblAdjust;
			}
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted = this.Document.Printer.DeviceCopies;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblLaserLineAdjustment1;
			// Dim rsData As New clsDRWrapper
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			//Application.DoEvents();
			// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
			// If rsData.EndOfFile Then
			// dblLaserLineAdjustment1 = 0
			// Else
			// dblLaserLineAdjustment1 = CDbl(Val(rsData.Fields("BirthAdjustment")))
			// End If
			dblLaserLineAdjustment1 = Conversion.Val(modRegistry.GetRegistryKey("BirthAdjustment", "CK"));
			if (boolPrintTest)
			{
				dblLaserLineAdjustment1 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment1) / 1440F;
			}
			if (!boolPrintTest)
			{
				rsData2.OpenRecordset("Select * from Amendments where Type = 'BIRTHS' and AmendmentID = '" + frmBirths.InstancePtr.lblBKey.Text + "'", modGNBas.DEFAULTCLERKDATABASE);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// If gboolFromDosTrio = True Or bolFromWindowsCR Then
			if (boolPrinted)
			{
				// With typeCRData
				// .Type = "BIR"
				// .Name = Left(frmBirths.txtChildLastName.Text & " " & frmBirths.txtChildFirstName.Text, 30)
				// .Reference = frmBirths.txtFileNumber.Text
				// .Control1 = frmBirths.txtChildDOB.Text
				// .Control2 = frmBirths.txtChildBirthPlace
				// 
				// .Amount1 = Format((typClerkFees.NewBirth + ((intPrinted - 1) * typClerkFees.ReplacementBirth)), "000000.00")
				// .Amount2 = Format(0, "000000.00")
				// .Amount3 = Format(0, "000000.00")
				// .Amount4 = Format(0, "000000.00")
				// .Amount5 = Format(0, "000000.00")
				// .Amount6 = Format(0, "000000.00")
				// .ProcessReceipt = "Y"
				// End With
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
				// Call WriteCashReceiptingData(typeCRData)
				// End
			}
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */
			frmPrintVitalRec.InstancePtr.Hide();
			if (!boolPrintTest)
			{
				Field1.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FirstName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MiddleName"))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("LastName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("Designation")));
				// If IsDate(rsBirthCertificate.Fields("DateOfBirth")) Then
				// Field2 = Format(rsBirthCertificate.Fields("DateOfBirth"), "MM/dd/yyyy")
				// Else
				// Field2.Text = rsBirthCertificate.Fields("DateOfBirthDescription")
				// End If
				Field2.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields("dateofbirth");
				Field3.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("Sex");
				Field4.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("ChildTown");
				FileNumber.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FileNumber");
				// Field5 = rsBirthCertificate.Fields("AttendantsFirstName")
				// & " " & CheckForNA(rsBirthCertificate.Fields("AttendantsMiddleInitial"))
				// & " " & rsBirthCertificate.Fields("AttendantsLastName") & " " & Trim(CheckForNAReturnComma(rsBirthCertificate.Fields("AttendantTitle") & "")) & CheckForNA(rsBirthCertificate.Fields("AttendantTitle"))
				Field5.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsFirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsMiddleInitial")))) + (fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsMiddleInitial")))) == string.Empty ? "" : " ") + fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsLastName"))) + (FCConvert.ToString(modGlobalRoutines.CheckForNAReturnComma(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantTitle") + ""))) + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantTitle")));
				// THIS FIELD IS FOR THE ATTENDENT TITLE WHICH IS SHOWN IN FIELD 5
				Field6.Text = "";
				Field7.Text = FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantAddress")));
				Field8.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersFirstName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersMiddleName"))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersMaidenName");
				if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherState") + "")) == 0)
				{
					Field9.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherTown") + "");
				}
				else
				{
					Field9.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherTown") + "") + ", " + fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherState") + "");
				}
				if (modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("Legitimate") || modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("aop") || modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("CourtDeterminedPaternity"))
				{
					Field10.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersFirstName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersMiddleInitial"))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersLastName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersDesignation")));
				}
				else
				{
					Field10.Text = string.Empty;
				}
				if (FCConvert.ToString(modGlobalRoutines.CheckForNA(Field10.Text)) == string.Empty)
					Field10.Text = "- - -";
				Field11.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("RegistrarName");
				Field12.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("RegTown");
				if (Information.IsDate(modGNBas.Statics.rsBirthCertificate.Get_Fields("registrardatefiled")) && Convert.ToDateTime(modGNBas.Statics.rsBirthCertificate.Get_Fields_DateTime("registrardatefiled")).ToOADate() != 0)
				{
					Field13.Text = Strings.Format(modGNBas.Statics.rsBirthCertificate.Get_Fields_DateTime("RegistrarDateFiled"), "MM/dd/yyyy");
				}
				else
				{
					Field13.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("registrardatefileddescription");
				}
				if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
				{
					Field14.Text = "";
				}
				else
				{
					Field14.Text = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				}
				Field15.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				Field16.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("TownOf") + " ");
				if (FCConvert.ToBoolean(modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("DeathRecordInOffice")))
				{
					txtDeceased.Text = txtDeceased.Tag + "  " + modGNBas.Statics.rsBirthCertificate.Get_Fields("DeceasedDate");
					txtDeceased.Visible = true;
				}
				else
				{
					txtDeceased.Visible = false;
				}
				if (FCConvert.ToBoolean(modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("SoleParent")))
				{
					txtSoleParent.Text = "Sole Legal Legitimate Parent";
				}
				else
				{
					txtSoleParent.Text = string.Empty;
				}
				txtAmended.Text = modAmendments.GetAmendments(rsData2);
			}
			else
			{
				Field2.Text = "Date of Birth";
				Field4.Text = "Town";
				Field12.Text = "Town Name";
				txtSoleParent.Text = "Sole Legal Legitimate Parent";
			}
		}

		
	}
}
