﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptCustomLabels.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Custom Labels";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND frmCustomLabels.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As int	OnWrite(double, int)
		float intLabelWidth;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			int intControl;
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			// 
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
				return;
			}
			for (intRow = 1; intRow <= (frmCustomLabels.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomLabels.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
					{
						if (Detail.Controls[intControl].Name == "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol))
						{
							break;
						}
					}
					if (frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol) >= 0)
					{
						if (intCol > 0)
						{
							if (!FCUtils.IsEmpty(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))
							{
								if (FCUtils.IsEmpty(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))
								{
									// ADD THE DATA TO THE CORRECT CELL IN THE GRID
									if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))), "MM/dd/yyyy");
									}
									else
									{
										if (FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
										{
											(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = string.Empty;
										}
										else
										{
											(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = rsData.Get_Fields_String(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])));
										}
									}
								}
								else
								{
									if (frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1) >= 0)
									{
										// this will check to see if to merge the columns
										// not sure about this change but we ran into the case where
										// the data in the first two fields were the same but the fields
										// were different so we did NOT want to merge the two fields.
										// If rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))) = rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))))) Then
										if (fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]) == fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))]))
										{
											if ((intControl + 2) > Detail.Controls.Count)
											{
												Detail.Controls[intControl].Width = Detail.Controls[intControl].Width;
											}
											else
											{
												Detail.Controls[intControl].Width += Detail.Controls[intControl + 1].Width;
											}
											Detail.Controls[intControl - 1].Tag = Detail.Controls[intControl - 1].Width + Detail.Controls[intControl].Width;
										}
										else
										{
											// ADD THE DATA TO THE CORRECT CELL IN THE GRID
											if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
											{
												(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))), "MM/dd/yyyy");
											}
											else
											{
												if (FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
												{
													(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = string.Empty;
												}
												else
												{
													(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])));
												}
											}
										}
									}
									else
									{
										// ADD THE DATA TO THE CORRECT CELL IN THE GRID
										if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
										{
											(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))), "MM/dd/yyyy");
										}
										else
										{
											if (FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
											{
												(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = string.Empty;
											}
											else
											{
												(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = rsData.Get_Fields_String(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])));
											}
										}
									}
								}
							}
						}
						else
						{
							// ADD THE DATA TO THE CORRECT CELL IN THE GRID
							Detail.Controls[intControl].Tag = Detail.Controls[intControl].Width;
							if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
							{
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))), "MM/dd/yyyy");
							}
							else
							{
								if (FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = string.Empty;
								}
								else
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = rsData.Get_Fields_String(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])));
								}
							}
						}
					}
					// If frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol) >= 0 Then
					// If intCol > 0 Then
					// If frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1) < 0 Then
					// ADD THE DATA TO THE CORRECT CELL IN THE GRID
					// If strWhereType(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) = GRIDDATE Then
					// Detail.Controls[intControl].Text = Format(rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))), "MM/dd/yyyy")
					// Else
					// If frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol) = vbNullString Then
					// Detail.Controls[intControl].Text = vbNullString
					// Else
					// Detail.Controls[intControl].Text = rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))))))
					// End If
					// End If
					// Else
					// this will check to see if to merge the columns
					// If rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))) = rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))))) Then 'And (intCol <> frmCustomLabels.vsLayout.Cols - 1) Then
					// Detail.Controls[intControl - 1].Tag = Detail.Controls[intControl - 1].Width + Detail.Controls[intControl].Width
					// Else
					// ADD THE DATA TO THE CORRECT CELL IN THE GRID
					// If strWhereType(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) = GRIDDATE Then
					// Detail.Controls[intControl].Text = Format(rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))), "MM/dd/yyyy")
					// Else
					// If frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol) = vbNullString Then
					// Detail.Controls[intControl].Text = vbNullString
					// Else
					// Detail.Controls[intControl].Text = rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))))))
					// End If
					// End If
					// End If
					// End If
					// Else
					// ADD THE DATA TO THE CORRECT CELL IN THE GRID
					// Detail.Controls[intControl].Tag = Detail.Controls[intControl].Width
					// If strWhereType(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) = GRIDDATE Then
					// Detail.Controls[intControl].Text = Format(rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))), "MM/dd/yyyy")
					// Else
					// If frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol) = vbNullString Then
					// Detail.Controls[intControl].Text = vbNullString
					// Else
					// Detail.Controls[intControl].Text = rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomLabels.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))))))
					// End If
					// End If
					// End If
					// End If
				}
			}
			rsData.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, MDIParent.InstancePtr.Grid);
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGNBas.DEFAULTCLERKDATABASE);
			bool boolIsLaser = false;
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			// If frmCustomLabels.optType(0) Then
			if (frmCustomLabels.InstancePtr.cmbLabelType.SelectedIndex <= 0)
			{
				// Avery 5160 (3 X 10)  Height = 1" Width = 2.63"
				this.PageSettings.Margins.Top = 0.5F;
				this.PageSettings.Margins.Left = 0.35F;
				this.PageSettings.Margins.Right = 0.25F;
				modGlobalConstants.Statics.PrintWidth = 8.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
				intLabelWidth = 2.63F;
				Detail.Height = 1F;
				Detail.ColumnCount = 3;
				Detail.ColumnSpacing = 0.25F;
				boolIsLaser = true;
				// ElseIf frmCustomLabels.optType(1) Then
			}
			else if (frmCustomLabels.InstancePtr.cmbLabelType.SelectedIndex == 1)
			{
				// Avery 5161 (2 X 10)  Height = 1" Width = 4"
				this.PageSettings.Margins.Top = 0.5F;
				this.PageSettings.Margins.Left = 0.25F;
				this.PageSettings.Margins.Right = 0.25F;
				modGlobalConstants.Statics.PrintWidth = 8.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
				intLabelWidth = 4F;
				Detail.Height = 1F;
				Detail.ColumnCount = 2;
				Detail.ColumnSpacing = 0.3F;
				boolIsLaser = true;
				// ElseIf frmCustomLabels.optType(2) Then
			}
			else if (frmCustomLabels.InstancePtr.cmbLabelType.SelectedIndex == 2)
			{
				// Avery 5163 (2 X 5)  Height = 2" Width = 4"
				this.PageSettings.Margins.Top = 0.5F;
				this.PageSettings.Margins.Left = 0.25F;
				this.PageSettings.Margins.Right = 0.25F;
				modGlobalConstants.Statics.PrintWidth = 8.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
				intLabelWidth = 4F;
				Detail.Height = 2F;
				Detail.ColumnCount = 2;
				Detail.ColumnSpacing = 0.3F;
				boolIsLaser = true;
				// ElseIf frmCustomLabels.optType(3) Then
			}
			else if (frmCustomLabels.InstancePtr.cmbLabelType.SelectedIndex == 3)
			{
				// Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
				this.PageSettings.Margins.Top = 0.5F;
				this.PageSettings.Margins.Left = 0.25F;
				this.PageSettings.Margins.Right = 0.25F;
				modGlobalConstants.Statics.PrintWidth = 8.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
				intLabelWidth = 4F;
				boolIsLaser = true;
				Detail.Height = 1.33F;
				Detail.ColumnCount = 2;
				Detail.ColumnSpacing = 0.28F;
			}
			if (boolIsLaser)
			{
				double dblAdjust = 0;
				// vbPorter upgrade warning: dblMargin As double	OnWrite(float, int)
				double dblMargin = 0;
				// vbPorter upgrade warning: dblHMargin As double	OnWrite(float, int)
				double dblHMargin = 0;
				double dblHAdj = 0;
				dblAdjust = Conversion.Val(modRegistry.GetRegistryKey("CKLabelLaserAdjustment", "CK"));
				dblHAdj = Conversion.Val(modRegistry.GetRegistryKey("CKLabelLaserHorizontalAdjustment", "CK"));
				if (dblAdjust != 0)
				{
					if (dblAdjust > 3)
						dblAdjust = 3;
					if (dblAdjust < -3)
						dblAdjust = -3;
					dblMargin = this.PageSettings.Margins.Top + dblAdjust * 240 / 1440F;
					if (dblMargin < 0)
						dblMargin = 0;
					this.PageSettings.Margins.Top = FCConvert.ToSingle(dblMargin);
				}
				if (dblHAdj != 0)
				{
					if (dblHAdj > 3)
						dblHAdj = 3;
					if (dblHAdj < -3)
						dblHAdj = -3;
					dblHMargin = this.PageSettings.Margins.Left + dblHAdj * 120 / 1440F;
					if (dblHMargin < 0)
						dblHMargin = 0;
					this.PageSettings.Margins.Left = FCConvert.ToSingle(dblHMargin);
					if (dblHAdj > 0)
					{
						if (8.5F - modGlobalConstants.Statics.PrintWidth - this.PageSettings.Margins.Left > 0)
						{
							this.PageSettings.Margins.Right = 8.5F - modGlobalConstants.Statics.PrintWidth - this.PageSettings.Margins.Left;
						}
						else
						{
							this.PageSettings.Margins.Right = 0;
						}
					}
				}
			}
			CreateDataFields();
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			// Call SetPrintProperties(Me)
		}

		private void CreateHeaderFields()
		{
			int intControlNumber = 0;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 0; intRow <= (frmCustomLabels.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomLabels.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					intControlNumber += 1;
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.Name = "txtHeader" + FCConvert.ToString(intControlNumber);
					NewField.Top = ((intRow * frmCustomLabels.InstancePtr.vsLayout.RowHeight(0)) + 1000) / 1440F;
					NewField.Left = (frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol) + 50) / 1440F;
					if (intCol > 0)
					{
						if (frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) == frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol - 1))
						{
							NewField.Text = string.Empty;
						}
						else
						{
							NewField.Text = frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
						}
					}
					else
					{
						NewField.Text = frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					NewField.Width = frmCustomLabels.InstancePtr.vsLayout.ColWidth(intCol) / 1440F;
					NewField.WordWrap = false;
					PageHeader.Controls.Add(NewField);
				}
			}
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
            // vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
            double availableWidth = (modGlobalConstants.Statics.PrintWidth - (this.Detail.ColumnSpacing * (this.Detail.ColumnCount - 1))) / this.Detail.ColumnCount;
            double dblSizeRatio = (frmCustomLabels.InstancePtr.vsLayout.WidthOriginal / 1440F) / availableWidth;
			//dblSizeRatio = (frmCustomLabels.InstancePtr.Line2.X2 - frmCustomLabels.InstancePtr.Line2.X1) / 1440F;
			// line2 is invisible and is 1440 wide before resizing
			// this lets us know how much we have been resized
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 1; intRow <= (frmCustomLabels.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomLabels.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.Name = "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol);
					// NewField.Top = (intRow - 1) * frmCustomLabels.vsLayout.RowHeight(0) + 50
					NewField.Top = ((intRow - 1) * 240 / 1440F);
					NewField.Left = FCConvert.ToSingle(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol) / dblSizeRatio) / 1440F;
					NewField.Width = FCConvert.ToSingle(frmCustomLabels.InstancePtr.vsLayout.ColWidth(intCol) / dblSizeRatio) / 1440F;
					// NewField.Height = frmCustomLabels.vsLayout.RowHeight(1)
					NewField.Height = 240 / 1440F;
					NewField.Text = string.Empty;
					NewField.WordWrap = true;
					NewField.CanGrow = false;
					NewField.CanShrink = false;
					NewField.Alignment = 0;
					Detail.Controls.Add(NewField);
				}
			}
		}
		// Public Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports.DDTool)
		// Call VerifyPrintToFile(Me, Tool)
		// End Sub
		private void Detail_Format(object sender, EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= Detail.Controls.Count - 1; intCounter++)
			{
				if (FCConvert.ToString(Detail.Controls[intCounter].Tag) != string.Empty)
				{
					if (Strings.Left(Detail.Controls[intCounter].Name, 7) == "txtData")
					{
						if (Conversion.Val(Detail.Controls[intCounter].Tag) <= intLabelWidth)
						{
							Detail.Controls[intCounter].Width = FCConvert.ToSingle(Conversion.Val(Detail.Controls[intCounter].Tag));
						}
						else
						{
							Detail.Controls[intCounter].Width = intLabelWidth;
						}
					}
				}
			}
		}

		
	}
}
