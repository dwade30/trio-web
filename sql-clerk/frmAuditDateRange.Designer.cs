//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmAuditDateRange.
	/// </summary>
	partial class frmAuditDateRange
	{
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCCheckBox chkIncludeOnline;
		public Global.T2KDateBox t2kFrom;
		public Global.T2KDateBox t2kTo;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbOrder = new fecherFoundation.FCComboBox();
            this.lblOrder = new fecherFoundation.FCLabel();
            this.chkIncludeOnline = new fecherFoundation.FCCheckBox();
            this.t2kFrom = new Global.T2KDateBox();
            this.t2kTo = new Global.T2KDateBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeOnline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 276);
            this.BottomPanel.Size = new System.Drawing.Size(384, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkIncludeOnline);
            this.ClientArea.Controls.Add(this.cmbOrder);
            this.ClientArea.Controls.Add(this.lblOrder);
            this.ClientArea.Controls.Add(this.t2kFrom);
            this.ClientArea.Controls.Add(this.t2kTo);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(384, 216);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(384, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            // 
            // cmbOrder
            // 
            this.cmbOrder.AutoSize = false;
            this.cmbOrder.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbOrder.FormattingEnabled = true;
            this.cmbOrder.Items.AddRange(new object[] {
            "Transaction Date",
            "Owner",
            "Tag"});
            this.cmbOrder.Location = new System.Drawing.Point(156, 162);
            this.cmbOrder.Name = "cmbOrder";
            this.cmbOrder.Size = new System.Drawing.Size(200, 40);
            this.cmbOrder.TabIndex = 6;
            this.cmbOrder.Text = "Transaction Date";
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(30, 177);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(72, 15);
            this.lblOrder.TabIndex = 7;
            this.lblOrder.Text = "ORDER BY";
            // 
            // chkIncludeOnline
            // 
            this.chkIncludeOnline.Location = new System.Drawing.Point(30, 115);
            this.chkIncludeOnline.Name = "chkIncludeOnline";
            this.chkIncludeOnline.Size = new System.Drawing.Size(231, 27);
            this.chkIncludeOnline.TabIndex = 5;
            this.chkIncludeOnline.Text = "Include Online Transactions";
            // 
            // t2kFrom
            // 
            this.t2kFrom.Location = new System.Drawing.Point(30, 55);
            this.t2kFrom.Mask = "##/##/####";
            this.t2kFrom.MaxLength = 10;
            this.t2kFrom.Name = "t2kFrom";
            this.t2kFrom.Size = new System.Drawing.Size(115, 40);
            this.t2kFrom.TabIndex = 0;
            this.t2kFrom.Text = "  /  /";
            // 
            // t2kTo
            // 
            this.t2kTo.Location = new System.Drawing.Point(169, 55);
            this.t2kTo.Mask = "##/##/####";
            this.t2kTo.MaxLength = 10;
            this.t2kTo.Name = "t2kTo";
            this.t2kTo.Size = new System.Drawing.Size(115, 40);
            this.t2kTo.TabIndex = 1;
            this.t2kTo.Text = "  /  /";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(169, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(40, 15);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "TO";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(40, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "FROM";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(108, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(174, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmAuditDateRange
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(384, 384);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmAuditDateRange";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.Load += new System.EventHandler(this.frmAuditDateRange_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAuditDateRange_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeOnline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}