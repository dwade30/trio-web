//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMCnoL.
	/// </summary>
	public partial class rptMCnoL : BaseSectionReport
	{
		public rptMCnoL()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Marriage Certificate ";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMCnoL InstancePtr
		{
			get
			{
				return (rptMCnoL)Sys.GetInstance(typeof(rptMCnoL));
			}
		}

		protected rptMCnoL _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMCnoL	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private clsDRWrapper rsData = new clsDRWrapper();
		private bool boolPrintTest;
		private double dblLineAdjust;

		public void Init(bool boolTestPrint, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			if (boolPrintTest)
			{
				dblLineAdjust = dblAdjust;
			}
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_Error(int Number, String Description, int Scode, string Source, string HelpFile, int HelpContext)
		{
			MessageBox.Show("Error Number " + FCConvert.ToString(Number) + " " + Description + "\r\n" + "Source: " + FCConvert.ToString(Scode) + " " + Source, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolPrinted = true;
				// intPrinted = Me.Printer.DeviceCopies
				//intPrinted = this.Document.Printer.Copies;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In PrintProgress", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblLaserLineAdjustment5 = 0;
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			//Application.DoEvents();
			if (!boolPrintTest)
			{
				// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
				// If rsData.EndOfFile Then
				// dblLaserLineAdjustment5 = 0
				// Else
				// dblLaserLineAdjustment5 = CDbl(Val(rsData.Fields("MarriageAdjustment")))
				// End If
				dblLaserLineAdjustment5 = Conversion.Val(modRegistry.GetRegistryKey("MarriageAdjustment", "CK"));
				rsData.OpenRecordset("Select * from Amendments where Type = 'MARRIAGES' and AmendmentID = '" + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum) + "'", modGNBas.DEFAULTCLERKDATABASE);
			}
			else
			{
				dblLaserLineAdjustment5 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment5) / 1440F;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// If gboolFromDosTrio = True Or bolFromWindowsCR Then
			if (boolPrinted)
			{
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
				// With typeCRData
				// .Type = "MAR"
				// .Name = Left(frmMarriages.txtBridesCurrentLastName.Text & ", " & frmMarriages.txtBridesFirstName.Text, 30)
				// .Reference = Trim(frmMarriages.txtFileNumber.Text) & " "
				// .Control1 = Format(frmMarriages.mebDateIntentionsFiled.Text, "##/##/####")
				// .Control2 = Format(frmMarriages.mebDateLicenseIssued.Text, "##/##/####")
				// 
				// .Amount1 = Format((typClerkFees.NewMarriageCert + ((intPrinted - 1) * typClerkFees.ReplacementMarriageLicense)), "000000.00")
				// .Amount2 = Format(0, "000000.00")
				// .Amount3 = Format(0, "000000.00")
				// .Amount4 = Format(0, "000000.00")
				// .Amount5 = Format(0, "000000.00")
				// .Amount6 = Format(0, "000000.00")
				// .ProcessReceipt = "Y"
				// End With
				// 
				// Call WriteCashReceiptingData(typeCRData)
				// End
			}
			// End If
			if (!boolPrintTest)
			{
				if (modGNBas.Statics.typMarriageConsent.NeedConsent)
				{
					frmPrintMarriageConsent.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */
			if (!boolPrintTest)
			{
				string strMuni = "";
				txtF1.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFirstName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMiddleName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsLastName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsDesignation");
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMaidenSurname"))) != fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCurrentLastName"))))
				{
					txtF2.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFirstName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMiddleName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMaidenSurname") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCurrentLastName"));
				}
				else
				{
					txtF2.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFirstName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMiddleName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCurrentLastName"));
				}
				// txtF2 = Trim(rsMarriageCertificate.Fields("BridesFirstName")
				// & " " & rsMarriageCertificate.Fields("BridesMiddleName")
				// & " " & rsMarriageCertificate.Fields("BridesCurrentLastName"))
				// changed on 8/13 per request from Deb2 on call id 32713
				// & " " & rsMarriageCertificate.Fields("BridesMaidenSurname")
				txtF3.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsCity") + ", " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsState");
				txtF4.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCity") + ", " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesState");
				txtF5.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int16("GroomsAge"));
				txtF6.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsBirthplace");
				txtF7.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int16("BridesAge"));
				txtF8.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesBirthplace");
				txtF9.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("DateIntentionsFiled");
				txtF10.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("CeremonyDate");
				txtF11.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("PersonPerformingCeremony");
				txtF12.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("TitleofPersonPerforming");
				txtF13.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("CityMarried");
				txtF14.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("NameOfClerk");
				FileNumber.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("FileNumber");
				txtF15.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("CityOfIssue");
				txtF16.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("DateClerkFiled");
				txtF17.Text = Strings.Format(DateTime.Now, "MMMM dd, yyyy");
				if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
				{
					txtF18.Text = "";
				}
				else
				{
					txtF18.Text = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				}
				// txtF19 = StrConv(MuniName, vbProperCase)
				strMuni = fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase);
				if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("TownCode")) == 0)
				{
					// txtF19 = StrConv(MuniName, vbProperCase)
					strMuni = fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase);
				}
				else
				{
					// txtF19.Text = StrConv(GetTownKeyName(Val(rsMarriageCertificate.Fields("towncode"))), vbProperCase)
					strMuni = fecherFoundation.Strings.StrConv(modRegionalTown.GetTownKeyName_2(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("towncode")), VbStrConv.ProperCase);
				}
				txtF19.Text = strMuni;
				txtAmended.Text = modAmendments.GetAmendments(rsData);
			}
			else
			{
			}
		}

		private void rptMCnoL_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMCnoL properties;
			//rptMCnoL.Caption	= "Marriage Certificate ";
			//rptMCnoL.Icon	= "rptMCnoL.dsx":0000";
			//rptMCnoL.Left	= 0;
			//rptMCnoL.Top	= 0;
			//rptMCnoL.Width	= 19080;
			//rptMCnoL.Height	= 12990;
			//rptMCnoL.StartUpPosition	= 3;
			//rptMCnoL.SectionData	= "rptMCnoL.dsx":058A;
			//End Unmaped Properties
		}
	}
}
