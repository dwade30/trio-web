﻿using fecherFoundation;
using System.IO;
using System;
using TWSharedLibrary.Data;

namespace Global
{/// <summary>
/// This class is deliberately a duplicate.  It uses the datareader from twsharedlibrary but is still able to be referenced by the global classes that were designed to use a linked copy of this class
/// </summary>
	public class clsDRWrapper : IDisposable
 	{
		//=========================================================
		//
		public enum dbConnectionTypes
		{
			SQLServer = 0,
			ODBC = 1,
			OLEDB = 2,
		}

		public enum DRDataTypes
		{
			dtEmpty = 0,
			dtObject = 1,
			dtDBNull = 2,
			dtBoolean = 3,
			dtChar = 4,
			dtSByte = 5,
			dtByte = 6,
			dtInt16 = 7,
			dtUInt16 = 8,
			dtInt32 = 9,
			dtUInt32 = 10,
			dtInt64 = 11,
			dtUInt64 = 12,
			dtSingle = 13,
			dtDouble = 14,
			dtDecimal = 15,
			dtDateTime = 16,
			dtString = 18,
		}

		public enum dbTypes
		{
			dbBigInt = 16,
			dbBinary = 9,
			dbBoolean = 1,
			dbByte = 2,
			dbChar = 18,
			dbCurrency = 5,
			dbDate = 8,
			dbDecimal = 20,
			dbDouble = 7,
			dbFloat = 21,
			dbGUID = 15,
			dbInteger = 3,
			dbLong = 4,
			dbLongBinary = 11,
			dbMemo = 12,
			dbNumeric = 19,
			dbSingle = 6,
			dbText = 10,
			dbTime = 22,
			dbTimeStamp = 23,
			dbVarBinary = 17,
		}
		// Private strDefaultDBAlias As String
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private DataReader theReader = new DataReader();
        private DataReader theReader_AutoInitialized = null;

		private DataReader theReader
		{
			get
			{
				if (theReader_AutoInitialized == null)
				{
					theReader_AutoInitialized = new DataReader();
				}
				return theReader_AutoInitialized;
			}
			set
			{
				theReader_AutoInitialized = value;
			}
		}

		private bool boolRunSilent;

		public bool RunSilent
		{
			get
			{
				bool RunSilent = false;
				RunSilent = boolRunSilent;
				return RunSilent;
			}
			set
			{
				boolRunSilent = value;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short ConvertTypeToDBType(int lngDataType)
		{
			short ConvertTypeToDBType = 0;
			// VBto upgrade warning: intReturn As short --> As int	OnWrite(dbTypes)
			int intReturn = 0;
			if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtEmpty))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbText);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtObject))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbText);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtDBNull))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbText);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtBoolean))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbBoolean);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtChar))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbChar);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtSByte))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbByte);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtByte))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbByte);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtInt16))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbInteger);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtUInt16))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbInteger);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtInt32))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbLong);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtUInt32))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbLong);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtInt64))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbBigInt);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtUInt64))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbBigInt);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtSingle))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbSingle);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtDouble))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbDouble);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtDecimal))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbDecimal);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtDateTime))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbDate);
			}
			else if (lngDataType == FCConvert.ToInt32(DRDataTypes.dtString))
			{
				intReturn = FCConvert.ToInt32(dbTypes.dbText);
			}
			ConvertTypeToDBType = FCConvert.ToInt16(intReturn);
			return ConvertTypeToDBType;
		}

		public void DisposeOf()
		{
            if (theReader_AutoInitialized != null)
            {
                theReader_AutoInitialized.Dispose();
            }
        }

		public string CurrentPrefix
		{
			get
			{
				string CurrentPrefix = "";
				string strTemp;
				strTemp = "TRIO_" + MegaGroup + "_" + GroupName + "_";
				CurrentPrefix = strTemp;
				return CurrentPrefix;
			}
		}

		public string Get_GetFullDBName(string strDB)
		{
			string GetFullDBName = "";
			GetFullDBName = theReader.GetFullDBName(strDB);
			return GetFullDBName;
		}

		public string Get_ConnectionInformation(string strDB)
		{
			string ConnectionInformation = "";
			ConnectionInformation = theReader.ConnectionInformation(strDB);
			return ConnectionInformation;
		}

		public string MegaGroup
		{
			get
			{
				string MegaGroup = "";
				MegaGroup = theReader.MegaGroup;
				return MegaGroup;
			}
			set
			{
				theReader.MegaGroup = value;
			}
		}

		public string GroupName
		{
			get
			{
				string GroupName = "";
				GroupName = theReader.GroupName;
				return GroupName;
			}
			set
			{
				theReader.GroupName = value;
			}
		}

		public string DefaultMegaGroup
		{
			get
			{
				string DefaultMegaGroup = "";
				DefaultMegaGroup = theReader.DefaultMegaGroup;
				return DefaultMegaGroup;
			}
			set
			{
				theReader.DefaultMegaGroup = value;
			}
		}

		public string DefaultGroup
		{
			get
			{
				string DefaultGroup = "";
				DefaultGroup = theReader.DefaultGroup;
				return DefaultGroup;
			}
			set
			{
				theReader.DefaultGroup = value;
			}
		}

		public bool IsntAnything()
		{
			bool IsntAnything = false;
			IsntAnything = theReader.IsntAnything();
			return IsntAnything;
		}

		public void CreateStoredProcedure(string strProcName, string strSQL, string strDatabase = "")
		{
			theReader.CreateStoredProcedure(strProcName, strSQL, strDatabase);
		}

		public bool AddTableField(string strFieldName, int enumFieldType, int intSize = 0)
		{
			bool AddTableField = false;
			AddTableField = theReader.AddTableField(strFieldName, enumFieldType, intSize);
			return AddTableField;
		}

		public bool DoesFieldExist(string strFieldName, string strTableName, string strDatabaseName = "")
		{
			bool DoesFieldExist = false;
			DoesFieldExist = theReader.DoesFieldExist(strFieldName, strTableName, strDatabaseName);
			return DoesFieldExist;
		}

		public void Delete()
		{
			theReader.Delete();
		}

		public object Get_ParameterValue(string strParameterName)
		{
			object ParameterValue = null;
			ParameterValue = theReader.ParameterValue(strParameterName);
			return ParameterValue;
		}

		public void Set_ParameterValue(string strParameterName, object Value)
		{
			theReader.SetParameterValue(strParameterName, Value);
		}

		public bool SetStoredProcedure(string strProcedureName, string strConnectionName)
		{
			bool SetStoredProcedure = false;
			SetStoredProcedure = theReader.SetStoredProcedure(strProcedureName, strConnectionName);
			return SetStoredProcedure;
		}

		public string GetStoredProcedureParameters()
		{
			string GetStoredProcedureParameters = "";
			GetStoredProcedureParameters = theReader.GetStoredProcedureParameters();
			return GetStoredProcedureParameters;
		}

		public int ExecuteStoredProcedure(bool boolReturnTable, bool boolAddMissingSchema = false)
		{
			int ExecuteStoredProcedure = 0;
			ExecuteStoredProcedure = theReader.ExecuteStoredProcedure(boolReturnTable, boolAddMissingSchema);
			return ExecuteStoredProcedure;
		}

		public dynamic Get_Fields(string FieldName)
		{
			object Fields = null;
			// this is the getdata
			Fields = theReader.Fields(FieldName);
			return Fields;
		}

		public bool IsFieldNull(string FieldName)
		{
			return FCConvert.ToString(this.Get_Fields(FieldName)) == "";
		}
		//FC:FINAL:DSE Get_Fields separately for each type
		public String Get_Fields_String(string FieldName)
		{
			return FCConvert.ToString(Get_Fields(FieldName));
		}

		public byte[] Get_Fields_Binary(string FieldName)
		{
			dynamic value = Get_Fields(FieldName);
			if (value != null && !value.Equals(String.Empty))
			{
				return value as byte[];
			}
			else
			{
				return new byte[] {

				};
			}
		}

		public Int16 Get_Fields_Int16(string FieldName)
		{
			string value = FCConvert.ToString(Get_Fields(FieldName));
			short res;
			//FC:FINAL:MSH - i.issue #1543: replace with TryParse to avoid the conversion exceptions if input value isn't equal to an empty string(for example " ")
			if (Int16.TryParse(value, out res))
			{
				return res;
			}
			else
			{
				return 0;
			}
			//if (value != null && !value.Equals(String.Empty))
			//{
			//    return FCConvert.ToInt16(value);
			//}
			//else
			//{
			//    return 0;
			//}
		}

		public Int32 Get_Fields_Int32(string FieldName)
		{
			string value = FCConvert.ToString(Get_Fields(FieldName));
			int res;
			//FC:FINAL:MSH - i.issue #1543: replace with TryParse to avoid the conversion exceptions if input value isn't equal to an empty string(for example " ")
			if (Int32.TryParse(value, out res))
			{
				return res;
			}
			else
			{
				return 0;
			}
			//if (value != null && !value.Equals(String.Empty))
			//{
			//    return FCConvert.ToInt32(value);
			//}
			//else
			//{
			//    return 0;
			//}
		}

		public Double Get_Fields_Double(string FieldName)
		{
			string value = FCConvert.ToString(Get_Fields(FieldName));
			double res;
			//FC:FINAL:MSH - i.issue #1543: replace with TryParse to avoid the conversion exceptions if input value isn't equal to an empty string(for example " ")
			if (Double.TryParse(value, out res))
			{
				return res;
			}
			else
			{
				return 0;
			}
			//if (value != null && !value.Equals(String.Empty))
			//{
			//    return FCConvert.ToDouble(value);
			//}
			//else
			//{
			//    return 0;
			//}
		}

		public Guid Get_Fields_Guid(string FieldName)
		{
			object value = Get_Fields(FieldName);
			if (value != null && !value.Equals(String.Empty))
			{
				return (Guid)value;
			}
			else
			{
				return Guid.Empty;
			}
		}

		public DateTime Get_Fields_DateTime(string FieldName)
		{
            //FC:FINAL:MSH - incorrect string format for .ToDateTime
            //return Convert.ToDateTime(Get_Fields(FieldName));
            return FCConvert.ToDateTime(Get_Fields(FieldName));
        }

		public Decimal Get_Fields_Decimal(string FieldName)
		{
			string value = FCConvert.ToString(Get_Fields(FieldName));
			Decimal res;
			//FC:FINAL:MSH - i.issue #1543: replace with TryParse to avoid the conversion exceptions if input value isn't equal to an empty string(for example " ")
			if (Decimal.TryParse(value, out res))
			{
				return res;
			}
			else
			{
				return 0;
			}
			//if (value != null && !value.Equals(String.Empty))
			//{
			//    return FCConvert.ToDecimal(value);
			//}
			//else
			//{
			//    return 0;
			//}
		}

		public Boolean Get_Fields_Boolean(string FieldName)
		{
			object value = Get_Fields(FieldName);
			if (value != null && !value.Equals(String.Empty))
			{
				return Convert.ToBoolean(value);
			}
			else
			{
				return false;
			}
		}

		public void Set_Fields(string FieldName, object Value)
		{
			// this is the setdata
			// SetData FieldName, Value
			theReader.SetField(FieldName, Value, boolRunSilent);
		}

		public int FieldsCount
		{
			get
			{
				int FieldsCount = 0;
				FieldsCount = theReader.FieldsCount;
				return FieldsCount;
			}
		}

		public string Get_FieldsIndexName(int intIndex)
		{
			string FieldsIndexName = "";
			FieldsIndexName = theReader.FieldsIndexName(intIndex);
			return FieldsIndexName;
		}

		public object Get_FieldsIndexValue(int intIndex)
		{
			object FieldsIndexValue = null;
			FieldsIndexValue = theReader.FieldsIndexValue(intIndex);
			return FieldsIndexValue;
		}

		public void Set_FieldsIndexValue(int intIndex, object tValue)
		{
			theReader.SetFieldsIndexValue(intIndex, tValue);
		}

		public int Get_FieldsIndexDataType(int lngIndex)
		{
			int FieldsIndexDataType = 0;
			FieldsIndexDataType = theReader.FieldsIndexDataType(lngIndex);
			return FieldsIndexDataType;
		}

		public int Get_FieldsDataType(string strFieldName)
		{
			int FieldsDataType = 0;
			FieldsDataType = theReader.FieldsDataType(strFieldName);
			return FieldsDataType;
		}

		public string DefaultDB
		{
			get
			{
				string DefaultDB = "";
				DefaultDB = theReader.DefaultDB;
				return DefaultDB;
			}
			set
			{
				theReader.DefaultDB = value;
			}
		}

		public string SharedDefaultDB
		{
			get
			{
				string SharedDefaultDB = "";
				SharedDefaultDB = theReader.SharedDefaultDB;
				return SharedDefaultDB;
			}
			set
			{
				theReader.SharedDefaultDB = value;
			}
		}

		public bool NoMatch
		{
			get
			{
				bool NoMatch = false;
				NoMatch = theReader.NoMatch;
				return NoMatch;
			}
		}

		public int ErrorNumber
		{
			get
			{
				int ErrorNumber = 0;
				ErrorNumber = theReader.ErrorNumber;
				return ErrorNumber;
			}
			set
			{
				theReader.ErrorNumber = value;
			}
		}

		public string ErrorDescription
		{
			get
			{
				string ErrorDescription = "";
				ErrorDescription = theReader.ErrorDescription;
				return ErrorDescription;
			}
			set
			{
				theReader.ErrorDescription = value;
			}
		}

		public void ClearErrors()
		{
			theReader.ClearErrors();
		}

		public void CancelUpdate()
		{
			theReader.CancelUpdate();
		}

		public int RecordCount()
		{
			int RecordCount = 0;
			RecordCount = theReader.RecordCount();
			return RecordCount;
		}

		public int AbsolutePosition()
		{
			int AbsolutePosition = 0;
			AbsolutePosition = theReader.AbsolutePosition();
			return AbsolutePosition;
		}

		public bool AddConnection(string strConnectionString, string strName/*= ""*/, int Connectiontype/*= dbConnectionTypes.SQLServer*/)
		{
			bool AddConnection = false;
			AddConnection = theReader.AddConnection(strConnectionString, strName, Connectiontype);
			return AddConnection;
		}
		// VBto upgrade warning: boolReadOnly As bool	OnWriteFCConvert.ToInt16
		public bool OpenRecordset(string strSelect, string strDBName = "", int intType = 0, bool boolReadOnly = false, int LockEdit = 0, bool boolOption = false, string strPWD = "", bool boolShowError = true, string ConnectionOverride = "", bool boolAutoCreateCommands = true)
		{
			bool OpenRecordset = false;
			try
			{
				OpenRecordset = theReader.OpenRecordset(strSelect, strDBName, intType, boolReadOnly, LockEdit, boolOption, strPWD, boolShowError, ConnectionOverride, boolAutoCreateCommands);
			}
			catch (Exception ex)
			{
				if (boolShowError)
				{
					FCMessageBox.Show(ex.Message, MsgBoxStyle.Critical);
				}
			}
			return OpenRecordset;
		}

		public bool Execute(string strStatement, string strConnectionName, bool boolShowError = true)
		{
			bool Execute = false;
			Execute = theReader.Execute(strStatement, strConnectionName, boolShowError);
			return Execute;
		}

		public bool UpdateDatabaseTable(string strTableName, string strDatabaseName, string strDatabasePassword = "")
		{
			bool UpdateDatabaseTable = false;
			UpdateDatabaseTable = theReader.UpdateDatabaseTable(strTableName, strDatabaseName, strDatabasePassword);
			return UpdateDatabaseTable;
		}

		public void MoveFirst()
		{
			theReader.MoveFirst();
		}

		public void MoveLast()
		{
			theReader.MoveLast();
		}

		public bool MoveNext()
		{
			bool MoveNext = false;
			theReader.MoveNext();
			return MoveNext;
		}

		public bool FindLastRecord(string strFieldName, object obFindValue)
		{
			bool FindLastRecord = false;
			FindLastRecord = theReader.FindLastRecord(strFieldName, obFindValue);
			return FindLastRecord;
		}

		public bool FindPreviousRecord(string strFieldName, object obFindValue)
		{
			bool FindPreviousRecord = false;
			FindPreviousRecord = theReader.FindPreviousRecord(strFieldName, obFindValue);
			return FindPreviousRecord;
		}

		public bool FindPreviousRecord2(ref Array arFieldNames, ref Array arFindValues)
		{
			bool FindPreviousRecord2 = false;
			FindPreviousRecord2 = theReader.FindPreviousRecord(ref arFieldNames, ref arFindValues);
			return FindPreviousRecord2;
		}

		public bool FindFirst(string strSQL)
		{
			bool FindFirst = false;
			FindFirst = theReader.FindFirst(strSQL);
			return FindFirst;
		}
		// VBto upgrade warning: obFindValue As object	OnWrite(string, int, object, DateTime, double)
		public bool FindFirstRecord(string strFieldName, object obFindValue, string strOperator = "=")
		{
			bool FindFirstRecord = false;
			FindFirstRecord = theReader.FindFirstRecord(strFieldName, obFindValue, strOperator);
			return FindFirstRecord;
		}
		// VBto upgrade warning: obFindValue As object	OnWrite(object, int)
		public bool FindNextRecord(string strFieldName, object obFindValue)
		{
			bool FindNextRecord = false;
			FindNextRecord = theReader.FindNextRecord(strFieldName, obFindValue);
			return FindNextRecord;
		}

		public bool FindFirstRecord2(string strFieldNames, string strFindValues, string strDelimiter, string strOperator = "=")
		{
			bool FindFirstRecord2 = false;
			FindFirstRecord2 = theReader.FindFirstRecord(strFieldNames, strFindValues, strDelimiter, strOperator);
			return FindFirstRecord2;
		}

		public bool FindNextRecord2(Array arFieldNames, Array arFindValues)
		{
			bool FindNextRecord2 = false;
			FindNextRecord2 = theReader.FindNextRecord(arFieldNames, arFindValues);
			return FindNextRecord2;
		}

		public bool FindNextRecord3(ref string strFieldNames, ref string strFindValues, ref string strDelimiter, string strOperator = "=")
		{
			bool FindNextRecord3 = false;
			FindNextRecord3 = theReader.FindNextRecord(strFieldNames, strFindValues, strDelimiter, strOperator);
			return FindNextRecord3;
		}

		public bool FindNextRecord3_1(string strFieldNames, string strFindValues, string strDelimiter, string strOperator = "=")
		{
			return FindNextRecord3(ref strFieldNames, ref strFindValues, ref strDelimiter, strOperator);
		}

		public bool MovePrevious()
		{
			bool MovePrevious = false;
			MovePrevious = theReader.MovePrevious();
			return MovePrevious;
		}

		public bool EndOfFile()
		{
			bool EndOfFile = false;
			EndOfFile = theReader.EndOfFile();
			return EndOfFile;
		}

		public bool IsLastRecord()
		{
			bool IsLastRecord = false;
			IsLastRecord = theReader.IsLastRecord();
			return IsLastRecord;
		}

		public bool isFirstRecord()
		{
			bool isFirstRecord = false;
			isFirstRecord = theReader.IsFirstRecord();
			return isFirstRecord;
		}

		public bool BeginningOfFile()
		{
			bool BeginningOfFile = false;
			BeginningOfFile = theReader.BeginningOfFile();
			return BeginningOfFile;
		}

		public bool AddNew()
		{
			bool AddNew = false;
			AddNew = theReader.AddNew();
			return AddNew;
		}

		public bool Edit()
		{
			bool Edit = false;
			Edit = theReader.Edit();
			return Edit;
		}

		public bool Update(bool boolUpdateAllRows = false, string strUpdateCommandText = "", string strInsertCommandText = "", string strDeleteCommandText = "")
		{
			bool Update = false;
			Update = theReader.Update(boolUpdateAllRows, strUpdateCommandText, strInsertCommandText, strDeleteCommandText);
			return Update;
		}

		public bool OpenDataTable(string strSelect, string strConnectionName, string strTableName = "", bool boolAutoCreateCommands = true)
		{
			bool OpenDataTable = false;
			OpenDataTable = theReader.OpenDataTable(strSelect, strConnectionName, strTableName, boolAutoCreateCommands);
			return OpenDataTable;
		}
		// Public Function GetParameterSymbol(ByVal tType As dbConnectionTypes) As String
		//
		// End Function
		public string GetParameterSymbol(int tType)
		{
			string GetParameterSymbol = "";
			GetParameterSymbol = theReader.GetParameterSymbol(tType);
			return GetParameterSymbol;
		}

		public object GetData(string strFieldName)
		{
			object GetData = null;
			GetData = theReader.GetData(strFieldName);
			return GetData;
		}
		// VBto upgrade warning: obValue As object	OnWrite(int, string)
		public void SetData(string strFieldName, object obValue)
		{
			theReader.SetData(strFieldName, obValue);
		}

		public object GetFirstRecord()
		{
			object GetFirstRecord = null;
			GetFirstRecord = theReader.GetFirstRecord();
			return GetFirstRecord;
		}

		public string Name()
		{
			string Name = "";
			Name = theReader.Name();
			return Name;
		}

		public void Reset()
		{
			theReader.Reset();
		}

		public int BookMark()
		{
			int BookMark = 0;
			BookMark = theReader.BookMark();
			return BookMark;
		}

		public void SetRow(int intRow)
		{
			theReader.SetRow(intRow);
		}

		public bool BeginTrans()
		{
			bool BeginTrans = false;
			BeginTrans = theReader.BeginTrans();
			return BeginTrans;
		}

		public void EndTrans()
		{
			theReader.EndTrans();
		}

		public void RollBack()
		{
			theReader.Rollback();
		}

		public TWSharedLibrary.Data.ConnectionMegaGroup AvailableMegaGroups(int intIndex)
		{
			TWSharedLibrary.Data.ConnectionMegaGroup AvailableMegaGroups;
			if (theReader.MegaGroupsCount() > intIndex)
			{
				AvailableMegaGroups = theReader.AvailableMegaGroups(intIndex);
			}
			else
			{
				AvailableMegaGroups = null;
			}
			return AvailableMegaGroups;
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MegaGroupsCount()
		{
			short MegaGroupsCount = 0;
			MegaGroupsCount = FCConvert.ToInt16(theReader.MegaGroupsCount());
			return MegaGroupsCount;
		}

		public string MakeODBCConnectionStringForAccess(string strFilePath, bool boolUsePassword = false)
		{
			string MakeODBCConnectionStringForAccess = "";
			string strReturn = "";
			if (Strings.Trim(strFilePath) != "")
			{
				if (!boolUsePassword)
				{
                    //FC:FINAL:SBE - #i2498 - change connection string. Microsoft Access Driver name for x64 is different
                    //strReturn = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" + strFilePath + ";";
                    strReturn = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=" + strFilePath + ";";
                }
				else
				{
                    //FC:FINAL:SBE - #i2498 - change connection string. Microsoft Access Driver name for x64 is different
                    //strReturn = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" + strFilePath + ";Uid=admin;Pwd=TRIO2001;";
                    strReturn = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=" + strFilePath + ";Uid=admin;Pwd=TRIO2001;";
                }
			}
			MakeODBCConnectionStringForAccess = strReturn;
			return MakeODBCConnectionStringForAccess;
		}

		public string MakeOleDBConnectionStringForAccess(string strFilePath, bool boolUsePassword = false)
		{
			string MakeOleDBConnectionStringForAccess = "";
			string strReturn = "";
			if (Strings.Trim(strFilePath) != "")
			{
				if (!boolUsePassword)
				{
					strReturn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFilePath + ";";
				}
				else
				{
					strReturn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFilePath + ";Uid=admin;Pwd=TRIO2001;";
				}
			}
			MakeOleDBConnectionStringForAccess = strReturn;
			return MakeOleDBConnectionStringForAccess;
		}

		public string MakeODBCConnectionStringForDBase(string strFileDir)
		{
			string MakeODBCConnectionStringForDBase = "";
			string strReturn = "";
			if (Strings.Trim(strFileDir) != "")
			{
				strReturn = "Driver={Microsoft dBASE Driver (*.dbf)};DriverId=277;Dbq=" + strFileDir + ";";
			}
			MakeODBCConnectionStringForDBase = strReturn;
			return MakeODBCConnectionStringForDBase;
		}

		public string MakeODBCConnectionStringForExcel(string strFilePath)
		{
			string MakeODBCConnectionStringForExcel = "";
			string strReturn = "";
			if (Strings.Trim(strFilePath) != "")
			{
				string strPath = "";
				strPath = Directory.GetParent(strFilePath).FullName;
				strReturn = "Driver={Microsoft Excel Driver (*.xls)};DriverId=790;Dbq=" + strFilePath + ";";
				// DefaultDir=" & strPath
			}
			MakeODBCConnectionStringForExcel = strReturn;
			return MakeODBCConnectionStringForExcel;
		}

		public bool TableExists(string strTableName, string strConnectionName)
		{
			bool TableExists = false;
			TableExists = false;
			if (!(Strings.Trim(strTableName) == "") && !(Strings.Trim(strConnectionName) == ""))
			{
				DataReader tRead = new DataReader();
				TableExists = tRead.TableExists(strTableName, strConnectionName);
			}
			return TableExists;
		}

		public string TableToJSON()
		{
			string TableToJSON = "";
			TableToJSON = theReader.TableToJSON();
			return TableToJSON;
		}

		public TWSharedLibrary.Data.TableItem TableToTableItem()
		{
			TWSharedLibrary.Data.TableItem TableToTableItem;
			// TableToTableItem = theReader.TableToTableItem
			TableToTableItem = theReader.TableToTableItem();
			return TableToTableItem;
		}

		public string CurrentRecordToJSON()
		{
			string CurrentRecordToJSON = "";
			CurrentRecordToJSON = theReader.CurrentRecordToJSON();
			return CurrentRecordToJSON;
		}

		public bool TableItemToTable(bool boolAsNew, ref TWSharedLibrary.Data.TableItem tTableItem, string strIDField)
		{
			bool TableItemToTable = false;
			TableItemToTable = theReader.TableItemToTable(boolAsNew, tTableItem, strIDField);
			return TableItemToTable;
		}

		public bool JSONCollectionToTable(bool boolAsNew, string strJSON, string strIDField)
		{
			bool JSONCollectionToTable = false;
			JSONCollectionToTable = theReader.JSONCollectionToTable(boolAsNew, strJSON, strIDField);
			return JSONCollectionToTable;
		}

		public bool JSONToTable(bool boolAsNew, string strJSON, string strIDField)
		{
			bool JSONToTable = false;
			JSONToTable = theReader.JSONToTable(boolAsNew, strJSON, strIDField);
			return JSONToTable;
		}

		public bool RecordItemToRow(bool boolAsNew, ref TWSharedLibrary.Data.RecordItem TRec, string strIDField)
		{
			bool RecordItemToRow = false;
			RecordItemToRow = theReader.RecordItemToRow(boolAsNew, ref  TRec, strIDField);
			return RecordItemToRow;
		}

		public object CreateQueryDef(string str1, string str2)
		{
			object CreateQueryDef = null;
			CreateQueryDef = null;
			return CreateQueryDef;
		}

		public string GUIDToString(object tGUID)
		{
			string GUIDToString = "";
			GUIDToString = theReader.GUIDToString(ref tGUID);
			return GUIDToString;
		}

		public object StringToGUID(string strGuid)
		{
			object StringToGUID = null;
			StringToGUID = theReader.StringToGUID(strGuid);
			return StringToGUID;
		}
		// VBto upgrade warning: strOrderBy As Variant --> As string
		public void ReOrder(string strOrderBy)
		{
			theReader.ReOrder(strOrderBy);
		}

		public void ReOrderAndFilter(string strFilter, string strOrderBy)
		{
			theReader.ReOrderAndFilter(strFilter, strOrderBy);
		}

		public void Filter(string strFilter)
		{
			theReader.Filter(strFilter);
		}

		public void InsertAddress(string strIDFields, string strPrefixes, string strProgModule = "", bool boolUseIDAsModAccountID = false, string strReOrderBy = "")
		{
			theReader.InsertAddress(strIDFields, strPrefixes, strProgModule, boolUseIDAsModAccountID, strReOrderBy);
		}

		public void InsertName(string strIDFields, string strPrefixes, bool boolIncludeEmail = false, bool boolNameAsFull = false, bool boolIncludeAddress = false, string strProgModule = "", bool boolUseIDAsModAccountID = false, string strReOrderBy = "", bool boolLastFirst = false, string strFilter = "", bool boolFullNameBoth = false)
		{
			theReader.InsertName(strIDFields, strPrefixes, boolIncludeEmail, boolNameAsFull, boolIncludeAddress, strProgModule, boolUseIDAsModAccountID, strReOrderBy, boolLastFirst, strFilter, boolFullNameBoth);
		}

		public bool DBExists(string strShortName)
		{
			bool DBExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				DBExists = false;
				// Dim tCon As New ConnectionPool
				// Dim tMega As ConnectionMegaGroup
				// Dim tVar As Variant
				// Set tMega = tCon.GetMegaGroup(MegaGroup)
				// If Not tMega Is Nothing Then
				// tVar = tMega.FindExistingDatabasesByShortName(GroupName)
				// 
				// Dim x As Integer
				// For x = 0 To UBound(tVar)
				// If LCase(tVar(x)) = LCase(strShortName) Then
				// DBExists = True
				// Exit Function
				// End If
				// Next x
				// End If
				string strDB;
				strDB = Get_GetFullDBName(strShortName);
				string strSQL;
				clsDRWrapper rs = new clsDRWrapper();
				strSQL = "select * from sys.databases where name = '" + strDB + "'";
				rs.OpenRecordset(strSQL, "SystemSettings");
				if (!rs.EndOfFile())
				{
					DBExists = true;
				}
				return DBExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return DBExists;
		}

		public string[] AvailableDBs()
		{
			string[] AvailableDBs = null;
			ConnectionPool tCon = new ConnectionPool();
			ConnectionMegaGroup tMega = new ConnectionMegaGroup();
			string[] tVar = null;
			tMega = tCon.GetMegaGroup(MegaGroup);
			if (!(tMega == null))
			{
				tVar = tMega.FindExistingDatabasesByShortName(GroupName);
				AvailableDBs = tVar;
			}
			return AvailableDBs;
		}

		public string GetDBDescription(string strDBName)
		{
			string GetDBDescription = "";
			string strReturn = "";
			if (Strings.LCase(strDBName) == "accountsreceivable")
			{
				strReturn = "Accounts Receivable";
			}
			else if (Strings.LCase(strDBName) == "cashreceipts")
			{
				strReturn = "Cash Receipts";
			}
			else if (Strings.LCase(strDBName) == "centraldata")
			{
				strReturn = "Central Data";
			}
			else if (Strings.LCase(strDBName) == "centralparties")
			{
				strReturn = "Central Parties";
			}
			else if (Strings.LCase(strDBName) == "codeenforcement")
			{
				strReturn = "Code Enforcement";
			}
			else if (Strings.LCase(strDBName) == "recommit")
			{
				strReturn = "Real Estate Commitment";
			}
			else if (Strings.LCase(strDBName) == "fixedassets")
			{
				strReturn = "Fixed Assets";
			}
			else if (Strings.LCase(strDBName) == "motorvehicle")
			{
				strReturn = "Motor Vehicle";
			}
			else if (Strings.LCase(strDBName) == "personalproperty")
			{
				strReturn = "Personal Property";
			}
			else if (Strings.LCase(strDBName) == "realestate")
			{
				strReturn = "Real Estate";
			}
			else if (Strings.LCase(strDBName) == "realestatetransfer")
			{
				strReturn = "Real Estate Transfer";
			}
			else if (Strings.LCase(strDBName) == "bluebook")
			{
				strReturn = "Blue Book";
			}
			else if (Strings.LCase(strDBName) == "systemsettings")
			{
				strReturn = "System Settings";
			}
			else if (Strings.LCase(strDBName) == "taxbilling")
			{
				strReturn = "Tax Billing";
			}
			else if (Strings.LCase(strDBName) == "utilitybilling")
			{
				strReturn = "Utility Billing";
			}
			else
			{
				strReturn = strDBName;
			}
			GetDBDescription = strReturn;
			return GetDBDescription;
		}

		~clsDRWrapper()
		{
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			if (!(theReader_AutoInitialized == null))
			{
				theReader_AutoInitialized.Dispose();
				theReader_AutoInitialized = null;
			}
		}

        public void Dispose()
        {
            if (theReader_AutoInitialized != null)
            {
				theReader_AutoInitialized.Dispose();
                theReader_AutoInitialized = null;
            }
        }

        public bool OmitNullsOnInsert
		{
			get
			{
				bool OmitNullsOnInsert = false;
				OmitNullsOnInsert = theReader.OmitNullsOnInsert;
				return OmitNullsOnInsert;
			}
			set
			{
				theReader.OmitNullsOnInsert = value;
			}
		}

		public void Set_DefaultValue(string FieldName, object Value)
		{
			// this is the setdata
			// SetData FieldName, Value
			theReader.SetDefaultValue(FieldName, Value);
		}

		public object Get_DefaultValue(string FieldName)
		{
			object DefaultValue = null;
			DefaultValue = theReader.GetDefaultValue(FieldName);
			return DefaultValue;
		}

		public void BulkCopy(string strTableName, string strConnectionName, bool boolAutoMapColumns)
		{
			theReader.BulkCopy(strTableName, strConnectionName, boolAutoMapColumns);
		}

		public void BulkCopyWithID(string strTableName, string strConnectionName, bool boolAutoMapColumns)
		{
			theReader.BulkCopyWithID(strTableName, strConnectionName, boolAutoMapColumns);
		}
	}
}
