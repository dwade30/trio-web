//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmStateTownInfo : BaseForm
	{
		public frmStateTownInfo()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmStateTownInfo InstancePtr
		{
			get
			{
				return (frmStateTownInfo)Sys.GetInstance(typeof(frmStateTownInfo));
			}
		}

		protected frmStateTownInfo _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsState = new clsDRWrapper();
		const int NEWRECORD = 1;
		const int MODIFYRECORD = 2;
		const int STATESTATUSCOL = 3;
		const int TOWNSTATUSCOL = 3;
		const int COUNTYSTATUSCOL = 2;
		const int COUNTYID = 4;

		private void CountyGrid_RowColChange(object sender, System.EventArgs e)
		{
			// If CountyGrid.Row > 1 Then
			// CountyGrid.Editable = flexEDKbdMouse
			// Else
			// CountyGrid.Editable = flexEDNone
			// End If
		}

		private void frmStateTownInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmStateTownInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// Select Case SSTab1.Tab
				// Case 0
				// If StateGrid.Col <> 2 Then KeyAscii = KeyAscii - 32
				// Case 1
				// If TownGrid.Col <> 2 Then KeyAscii = KeyAscii - 32
				// Case 2
				// If CountyGrid.Col <> 2 Then KeyAscii = KeyAscii - 32
				// End Select
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmStateTownInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmStateTownInfo properties;
			//frmStateTownInfo.ScaleWidth	= 9075;
			//frmStateTownInfo.ScaleHeight	= 7335;
			//frmStateTownInfo.LinkTopic	= "Form1";
			//frmStateTownInfo.LockControls	= -1  'True;
			//End Unmaped Properties
			LoadStates();
			LoadTowns();
			// only want this run once
			TownGrid.ColComboList(2, FCConvert.ToString(LoadGridCellAsCombo("Select * from DefaultCounties", "Name", "ID")));
			LoadCounties();
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						DeleteState();
						break;
					}
				case 1:
					{
						DeleteTown();
						break;
					}
				case 2:
					{
						DeleteCounty();
						break;
					}
			}
			//end switch
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						NewState();
						break;
					}
				case 1:
					{
						NewTown();
						break;
					}
				case 2:
					{
						NewCounty();
						break;
					}
			}
			//end switch
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						SaveState();
						break;
					}
				case 1:
					{
						SaveTown();
						break;
					}
				case 2:
					{
						SaveCounties();
						break;
					}
			}
			//end switch
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						if (SaveState() == false)
							return;
						break;
					}
				case 1:
					{
						if (SaveTown() == false)
							return;
						break;
					}
				case 2:
					{
						if (SaveCounties() == false)
							return;
						break;
					}
			}
			//end switch
			mnuExit_Click();
		}

		private void SSTab1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			switch (SSTab1.SelectedIndex)
			{
				case 0:
					{
						StateGrid.Focus();
						break;
					}
				case 1:
					{
						TownGrid.Focus();
						break;
					}
				case 2:
					{
						CountyGrid.Focus();
						break;
					}
			}
			//end switch
		}

		private void StateGrid_KeyPressEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (FCConvert.ToDouble(StateGrid.TextMatrix(StateGrid.Row, 3)) != 1)
				StateGrid.TextMatrix(StateGrid.Row, 3, FCConvert.ToString(2));
		}

		private void NewState()
		{
			StateGrid.AddItem("", StateGrid.Rows);
			StateGrid.TextMatrix(StateGrid.Rows - 1, 3, FCConvert.ToString(1));
			StateGrid.TopRow = StateGrid.Rows - 1;
			StateGrid.Row = StateGrid.Rows - 1;
			StateGrid.Col = 1;
			StateGrid.Select(-1, 2);
		}

		private void LoadStates()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from States ORDER BY Description", modGNBas.DEFAULTCLERKDATABASE);
			StateGrid.Rows = rsData.RecordCount() + 1;
			StateGrid.Cols = 4;
			StateGrid.TextMatrix(0, 0, "ID");
			StateGrid.TextMatrix(0, 1, "State");
			StateGrid.TextMatrix(0, 2, "Description");
			StateGrid.ColHidden(0, true);
			StateGrid.ColHidden(1, true);
			StateGrid.ColHidden(3, true);
			//StateGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			StateGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, StateGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			StateGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2, true);
			StateGrid.ColWidth(2, 6550);
			StateGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				StateGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				StateGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields("State")));
				StateGrid.TextMatrix(intCounter, 2, FCConvert.ToString(rsData.Get_Fields_String("Description")));
				StateGrid.TextMatrix(intCounter, 3, FCConvert.ToString(0));
				rsData.MoveNext();
			}
			StateGrid.TopRow = 1;
			StateGrid.Row = 1;
			StateGrid.Col = 1;
			StateGrid.Select(0, 0);
		}

		private void DeleteState()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (StateGrid.Row > 1)
				{
					if (MessageBox.Show("Are you sure you wish to delete this State?   " + StateGrid.TextMatrix(StateGrid.Row, 1) + " - " + StateGrid.TextMatrix(StateGrid.Row, 2), "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (FCConvert.ToDouble(StateGrid.TextMatrix(StateGrid.Row, 3)) == 1)
						{
							StateGrid.RemoveItem(StateGrid.Row);
						}
						else
						{
							rsState.Execute("Delete from States where ID = " + StateGrid.TextMatrix(StateGrid.Row, 0), modGNBas.DEFAULTDATABASE);
							StateGrid.RemoveItem(StateGrid.Row);
						}
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveState()
		{
			bool SaveState = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				StateGrid.Row = 0;
				for (intCounter = 1; intCounter <= (StateGrid.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 1)).Length != 2)
					{
						// MsgBox "States must have a two digit identifier.", vbInformation + vbOKOnly, "TRIO Software"
						// StateGrid.Row = intCounter
						// StateGrid.TopRow = intCounter
						// StateGrid.Col = 1
						// Screen.MousePointer = vbDefault
						// Exit Function
					}
					else if (fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 2)).Length <= 0)
					{
						MessageBox.Show("States must have a description.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						StateGrid.Row = intCounter;
						StateGrid.TopRow = intCounter;
						StateGrid.Col = 2;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveState;
					}
					rsData.OpenRecordset("Select * from States where State = '" + FCConvert.ToString(modCustomReport.FixQuotes(fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 1)))) + "' AND Description = '" + FCConvert.ToString(modCustomReport.FixQuotes(fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 2)))) + "'", modGNBas.DEFAULTDATABASE);
					if (rsData.EndOfFile())
					{
					}
					else
					{
						while (!rsData.EndOfFile())
						{
							if (FCConvert.ToString(rsData.Get_Fields_Int32("ID")) == StateGrid.TextMatrix(intCounter, 0))
							{
							}
							else
							{
								if (MessageBox.Show("The state " + StateGrid.TextMatrix(intCounter, 2) + " already exists. Continue to save?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									goto ContinueWithSave;
								}
								else
								{
									goto NextRecord;
								}
							}
							rsData.MoveNext();
						}
					}
					ContinueWithSave:
					;
					if (FCConvert.ToDouble(StateGrid.TextMatrix(intCounter, STATESTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into States (State,Description) VALUES ('" + FCConvert.ToString(modCustomReport.FixQuotes(StateGrid.TextMatrix(intCounter, 1))) + " ','" + FCConvert.ToString(modCustomReport.FixQuotes(StateGrid.TextMatrix(intCounter, 2))) + "')", modGNBas.DEFAULTDATABASE);
						StateGrid.TextMatrix(intCounter, 3, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(StateGrid.TextMatrix(intCounter, STATESTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update States Set State = '" + FCConvert.ToString(modCustomReport.FixQuotes(fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 1)))) + "', Description = '" + FCConvert.ToString(modCustomReport.FixQuotes(fecherFoundation.Strings.Trim(StateGrid.TextMatrix(intCounter, 2)))) + "' where ID = " + StateGrid.TextMatrix(intCounter, 0), modGNBas.DEFAULTDATABASE);
						StateGrid.TextMatrix(intCounter, 3, FCConvert.ToString(0));
					}
					NextRecord:
					;
				}
				LoadStates();
				SaveState = true;
				MessageBox.Show("Save of state information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveState;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveState;
		}

		private void StateGrid_RowColChange(object sender, System.EventArgs e)
		{
			// If StateGrid.Row > 1 Then
			// StateGrid.Editable = flexEDKbdMouse
			// Else
			// StateGrid.Editable = flexEDNone
			// End If
		}

		private void TownGrid_ComboCloseUp(object sender, System.EventArgs e)
		{
			// test
			TownGrid.TextMatrix(TownGrid.Row, 4, TownGrid.ComboData(TownGrid.ComboIndex));
		}
		// *********************************************
		private void TownGrid_ComboDropDown(object sender, System.EventArgs e)
		{
			if (FCConvert.ToDouble(TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL)) != 1)
				TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL, FCConvert.ToString(2));
		}

		private void TownGrid_KeyPressEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (FCConvert.ToDouble(TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL)) != 1)
				TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL, FCConvert.ToString(2));
		}

		private void NewTown()
		{
			TownGrid.AddItem("", TownGrid.Rows);
			TownGrid.TextMatrix(TownGrid.Rows - 1, TOWNSTATUSCOL, FCConvert.ToString(1));
			TownGrid.TopRow = TownGrid.Rows - 1;
			TownGrid.Row = TownGrid.Rows - 1;
			TownGrid.Col = 1;
		}

		private void LoadTowns()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsCounty = new clsDRWrapper();
			rsData.OpenRecordset("Select * from DefaultTowns ORDER BY Name", modGNBas.DEFAULTCLERKDATABASE);
			rsCounty.OpenRecordset("Select * from DefaultCounties ORDER BY Name", modGNBas.DEFAULTCLERKDATABASE);
			TownGrid.Rows = rsData.RecordCount() + 1;
			TownGrid.Cols = 5;
			TownGrid.TextMatrix(0, 0, "ID");
			TownGrid.TextMatrix(0, 1, "Town");
			TownGrid.TextMatrix(0, 2, "County");
			TownGrid.ColHidden(COUNTYID, true);
			TownGrid.ColHidden(0, true);
			TownGrid.ColHidden(TOWNSTATUSCOL, true);
			//TownGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			TownGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, TownGrid.Rows - 1, 2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			TownGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2, true);
			TownGrid.ColWidth(1, 3850);
			TownGrid.ColWidth(2, 3900);
			TownGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				TownGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				TownGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Name")));
				rsCounty.FindFirstRecord("ID", Conversion.Val(rsData.Get_Fields("County")));
				if (rsCounty.NoMatch)
				{
					TownGrid.TextMatrix(intCounter, 2, string.Empty);
					TownGrid.TextMatrix(intCounter, COUNTYID, FCConvert.ToString(0));
				}
				else
				{
					TownGrid.TextMatrix(intCounter, 2, FCConvert.ToString(rsCounty.Get_Fields_String("Name")));
					TownGrid.TextMatrix(intCounter, COUNTYID, FCConvert.ToString(rsCounty.Get_Fields_Int32("ID")));
				}
				TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL, FCConvert.ToString(0));
				rsData.MoveNext();
			}
			TownGrid.TopRow = 1;
			TownGrid.Row = 1;
			TownGrid.Col = 1;
			TownGrid.Select(0, 0);
		}

		private void DeleteTown()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (TownGrid.Row > 1)
				{
					if (MessageBox.Show("Are you sure you wish to delete this town?   " + TownGrid.TextMatrix(TownGrid.Row, 1) + " - " + TownGrid.TextMatrix(TownGrid.Row, 2), "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (FCConvert.ToDouble(TownGrid.TextMatrix(TownGrid.Row, TOWNSTATUSCOL)) == 1)
						{
							TownGrid.RemoveItem(TownGrid.Row);
						}
						else
						{
							rsState.Execute("Delete from DefaultTowns where ID = " + TownGrid.TextMatrix(TownGrid.Row, 0), modGNBas.DEFAULTCLERKDATABASE);
							TownGrid.RemoveItem(TownGrid.Row);
						}
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveTown()
		{
			bool SaveTown = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				TownGrid.Row = 0;
				for (intCounter = 1; intCounter <= (TownGrid.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(TownGrid.TextMatrix(intCounter, 1)).Length <= 0)
					{
						MessageBox.Show("Town name must be entered.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						TownGrid.Row = intCounter;
						TownGrid.TopRow = intCounter;
						TownGrid.Col = 1;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveTown;
					}
					rsData.OpenRecordset("Select * from DefaultTowns where Name = '" + modGlobalFunctions.EscapeQuotes(fecherFoundation.Strings.Trim(TownGrid.TextMatrix(intCounter, 1))) + "' AND County = " + FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(TownGrid.TextMatrix(intCounter, 4)))), modGNBas.DEFAULTDATABASE);
					if (rsData.EndOfFile())
					{
					}
					else
					{
						while (!rsData.EndOfFile())
						{
							if (FCConvert.ToString(rsData.Get_Fields_Int32("ID")) == TownGrid.TextMatrix(intCounter, 0))
							{
							}
							else
							{
								if (MessageBox.Show("This Town/County already exists. Continue to save?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									goto ContinueWithSave;
								}
								else
								{
									goto NextRecord;
								}
							}
							rsData.MoveNext();
						}
					}
					ContinueWithSave:
					;
					if (FCConvert.ToDouble(TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into DefaultTowns (Name,County) VALUES ('" + modGlobalFunctions.EscapeQuotes(TownGrid.TextMatrix(intCounter, 1)) + "'," + FCConvert.ToString(Conversion.Val(TownGrid.TextMatrix(intCounter, 2))) + ")", modGNBas.DEFAULTDATABASE);
						TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update DefaultTowns Set Name = '" + modGlobalFunctions.EscapeQuotes(fecherFoundation.Strings.Trim(TownGrid.TextMatrix(intCounter, 1))) + "', County = " + FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(TownGrid.TextMatrix(intCounter, 4)))) + " where ID = " + TownGrid.TextMatrix(intCounter, 0), modGNBas.DEFAULTDATABASE);
						TownGrid.TextMatrix(intCounter, TOWNSTATUSCOL, FCConvert.ToString(0));
					}
					NextRecord:
					;
				}
				LoadTowns();
				SaveTown = true;
				MessageBox.Show("Save of town information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveTown;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveTown;
		}
		// *********************************************
		private void CountyGrid_KeyPressEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (FCConvert.ToDouble(CountyGrid.TextMatrix(CountyGrid.Row, COUNTYSTATUSCOL)) != 1)
				CountyGrid.TextMatrix(CountyGrid.Row, COUNTYSTATUSCOL, FCConvert.ToString(2));
		}

		private void NewCounty()
		{
			CountyGrid.AddItem("", CountyGrid.Rows);
			CountyGrid.TextMatrix(CountyGrid.Rows - 1, COUNTYSTATUSCOL, FCConvert.ToString(1));
			CountyGrid.TopRow = CountyGrid.Rows - 1;
			CountyGrid.Row = CountyGrid.Rows - 1;
			CountyGrid.Col = 1;
		}

		private void LoadCounties()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from DefaultCounties ORDER BY Name", modGNBas.DEFAULTCLERKDATABASE);
			CountyGrid.Rows = rsData.RecordCount() + 1;
			CountyGrid.Cols = 3;
			CountyGrid.TextMatrix(0, 0, "ID");
			CountyGrid.TextMatrix(0, 1, "County");
			CountyGrid.ColHidden(0, true);
			CountyGrid.ColHidden(COUNTYSTATUSCOL, true);
			//CountyGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			if (CountyGrid.Rows > 1)
			{
				CountyGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, CountyGrid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			CountyGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1, true);
			CountyGrid.ColWidth(1, 7760);
			CountyGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				CountyGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				CountyGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Name")));
				CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL, FCConvert.ToString(0));
				rsData.MoveNext();
			}
			CountyGrid.TopRow = 1;
			if (CountyGrid.Rows > 1)
				CountyGrid.Row = 1;
			CountyGrid.Col = 1;
			CountyGrid.Select(0, 0);
		}

		private void DeleteCounty()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (CountyGrid.Row > 1)
				{
					if (MessageBox.Show("Are you sure you wish to delete this County?   " + CountyGrid.TextMatrix(CountyGrid.Row, 1), "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (FCConvert.ToDouble(CountyGrid.TextMatrix(CountyGrid.Row, COUNTYSTATUSCOL)) == 1)
						{
							CountyGrid.RemoveItem(CountyGrid.Row);
						}
						else
						{
							rsState.Execute("Delete from DefaultCounties where ID = " + CountyGrid.TextMatrix(CountyGrid.Row, 0), modGNBas.DEFAULTCLERKDATABASE);
							CountyGrid.RemoveItem(CountyGrid.Row);
						}
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveCounties()
		{
			bool SaveCounties = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				CountyGrid.Row = 0;
				for (intCounter = 1; intCounter <= (CountyGrid.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(CountyGrid.TextMatrix(intCounter, 1)).Length <= 0)
					{
						MessageBox.Show("County name must be entered.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						CountyGrid.Row = intCounter;
						CountyGrid.TopRow = intCounter;
						CountyGrid.Col = 1;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveCounties;
					}
					rsData.OpenRecordset("Select * from DefaultCounties where Name = '" + FCConvert.ToString(modCustomReport.FixQuotes(fecherFoundation.Strings.Trim(CountyGrid.TextMatrix(intCounter, 1)))) + "'", modGNBas.DEFAULTDATABASE);
					if (rsData.EndOfFile())
					{
					}
					else
					{
						while (!rsData.EndOfFile())
						{
							if (FCConvert.ToString(rsData.Get_Fields_Int32("ID")) == CountyGrid.TextMatrix(intCounter, 0))
							{
							}
							else
							{
								if (MessageBox.Show("This County already exists. Continue to save?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									goto ContinueWithSave;
								}
								else
								{
									goto NextRecord;
								}
							}
							rsData.MoveNext();
						}
					}
					ContinueWithSave:
					;
					if (FCConvert.ToDouble(CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into DefaultCounties (Name) VALUES ('" + FCConvert.ToString(modCustomReport.FixQuotes(CountyGrid.TextMatrix(intCounter, 1))) + "')", modGNBas.DEFAULTCLERKDATABASE);
						CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update DefaultCounties Set Name = '" + FCConvert.ToString(modCustomReport.FixQuotes(fecherFoundation.Strings.Trim(CountyGrid.TextMatrix(intCounter, 1)))) + "' where ID = " + CountyGrid.TextMatrix(intCounter, 0), modGNBas.DEFAULTCLERKDATABASE);
						CountyGrid.TextMatrix(intCounter, COUNTYSTATUSCOL, FCConvert.ToString(0));
					}
					NextRecord:
					;
				}
				LoadCounties();
				TownGrid.ColComboList(2, FCConvert.ToString(LoadGridCellAsCombo("Select * from DefaultCounties", "Name", "ID")));
				SaveCounties = true;
				MessageBox.Show("Save of County information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveCounties;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveCounties;
		}
		// *************************************************
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object LoadGridCellAsCombo(string strSQL, string FieldName = "", string IDField = "")
		{
			object LoadGridCellAsCombo = null;
			int intCounter;
			clsDRWrapper rsCombo = new clsDRWrapper();
			rsCombo.OpenRecordset(strSQL, modGNBas.DEFAULTCLERKDATABASE);
			while (!rsCombo.EndOfFile())
			{
				LoadGridCellAsCombo = LoadGridCellAsCombo + "|";
				LoadGridCellAsCombo = LoadGridCellAsCombo + "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
				rsCombo.MoveNext();
			}
			return LoadGridCellAsCombo;
		}

		private void TownGrid_RowColChange(object sender, System.EventArgs e)
		{
			if (TownGrid.Row > 0)
			{
				TownGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				TownGrid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}
	}
}
