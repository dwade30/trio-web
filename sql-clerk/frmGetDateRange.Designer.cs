//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmGetDateRange.
	/// </summary>
	partial class frmGetDateRange
	{
		public Global.T2KDateBox t2kDate1;
		public Global.T2KDateBox T2KDate2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.t2kDate1 = new Global.T2KDateBox();
            this.T2KDate2 = new Global.T2KDateBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 231);
            this.BottomPanel.Size = new System.Drawing.Size(395, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.t2kDate1);
            this.ClientArea.Controls.Add(this.T2KDate2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(395, 171);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(395, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(143, 30);
            this.HeaderText.Text = "Date Range";
            // 
            // t2kDate1
            // 
            this.t2kDate1.Location = new System.Drawing.Point(30, 30);
            this.t2kDate1.Mask = "00/00/0000";
            this.t2kDate1.Name = "t2kDate1";
            this.t2kDate1.Size = new System.Drawing.Size(115, 40);
            this.t2kDate1.TabIndex = 0;
            this.t2kDate1.Text = "  /  /";
            // 
            // T2KDate2
            // 
            this.T2KDate2.Location = new System.Drawing.Point(30, 121);
            this.T2KDate2.Mask = "00/00/0000";
            this.T2KDate2.Name = "T2KDate2";
            this.T2KDate2.Size = new System.Drawing.Size(115, 40);
            this.T2KDate2.TabIndex = 1;
            this.T2KDate2.Text = "  /  /";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(35, 91);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(34, 21);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "TO";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(107, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(176, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmGetDateRange
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(395, 339);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmGetDateRange";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Date Range";
            this.Load += new System.EventHandler(this.frmGetDateRange_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetDateRange_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSaveContinue;
	}
}