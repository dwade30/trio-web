﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogWarrant.
	/// </summary>
	public partial class rptDogWarrant : BaseSectionReport
	{
		public rptDogWarrant()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Dog Warrant Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDogWarrant InstancePtr
		{
			get
			{
				return (rptDogWarrant)Sys.GetInstance(typeof(rptDogWarrant));
			}
		}

		protected rptDogWarrant _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDogWarrant	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool boolTest;
		double dblLineAdjust;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			rs.OpenRecordset("SELECT * FROM WarrantParameters", modGNBas.DEFAULTDATABASE);
			// Call SetPrintProperties(Me)
			double dblLaserLineAdjustment9;
			// Dim rsData As New clsDRWrapper
			// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
			// If rsData.EndOfFile Then
			// dblLaserLineAdjustment9 = 0
			// Else
			// dblLaserLineAdjustment9 = CDbl(Val(rsData.Fields("DogAdjustment")))
			// End If
			dblLaserLineAdjustment9 = Conversion.Val(modRegistry.GetRegistryKey("DogAdjustment", "CK"));
			if (boolTest)
			{
				dblLaserLineAdjustment9 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.PageHeader.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment9) / 1440F;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
            //FC:FINAL:AM:#4038 - load form if it's not already loaded
            if (!frmDogWarrant.InstancePtr.IsLoaded)
            {
                frmDogWarrant.InstancePtr.LoadForm();
            }
            txtTownCounty1.Text = frmDogWarrant.InstancePtr.txtTownCounty.Text;
			txtTownCounty2.Text = frmDogWarrant.InstancePtr.txtTownCounty.Text;
			txtTownName1.Text = frmDogWarrant.InstancePtr.txtTownName.Text;
			txtTownName2.Text = frmDogWarrant.InstancePtr.txtTownName.Text;
			txtTownName3.Text = frmDogWarrant.InstancePtr.txtTownName.Text;
			txtTownName4.Text = frmDogWarrant.InstancePtr.txtTownName.Text;
			txtControlOfficer.Text = frmDogWarrant.InstancePtr.txtControlOfficer.Text;
		}

		public void Init(bool boolPreview, bool modalDialog, double dblAdjust = 0)
		{
			boolTest = boolPreview;
			if (boolTest)
			{
				dblLineAdjust = dblAdjust;
			}
			// Me.Show 
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: false, showModal: modalDialog);
		}

		
	}
}
