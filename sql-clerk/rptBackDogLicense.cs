//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBackDogLicense.
	/// </summary>
	public partial class rptBackDogLicense : BaseSectionReport
	{
		public rptBackDogLicense()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Back Dog License";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBackDogLicense InstancePtr
		{
			get
			{
				return (rptBackDogLicense)Sys.GetInstance(typeof(rptBackDogLicense));
			}
		}

		protected rptBackDogLicense _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBackDogLicense	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool boolPrintReRegOnFront;
		bool boolPrintExpirationDate;
		int intLineToPrint;
		bool boolLookUpLast;

		public void Init(int lngdognum, int intLinePrint, bool boolPrintOnFront = false, bool boolPrintExpiration = false, bool boolLaser = false, bool boolLookUpAmount = false)
		{
			boolLookUpLast = boolLookUpAmount;
			intLineToPrint = intLinePrint;
			boolPrintExpirationDate = boolPrintExpiration;
			boolPrintReRegOnFront = boolPrintOnFront;
			string strSQL;
			strSQL = "Select FullName,Address1,address2,address3,city,state,zip, DogOwner.LocationSTR,DOGowner.towncode, DogInfo.* ,doginfo.id as dognumber from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID) inner join " + rs.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView on (dogowner.partyid = " + rs.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView.Id)  where DogInfo.ID = " + FCConvert.ToString(lngdognum);
			// Call rs.OpenRecordset("SELECT DogOwner.Address, DogOwner.Phone,DogOwner.FirstName, DogOwner.MI, DogOwner.LastName, DogInfo.* FROM DogInfo INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID where DogInfo.ID = " & lngdognum, "twck0000.vb1")
			rs.OpenRecordset(strSQL, "Clerk");
			lblExpirationDate.Visible = false;
			txtExpirationDate.Visible = false;
			if (boolPrintExpirationDate)
			{
				txtExpirationDate.Visible = true;
				if (intLineToPrint == 1)
				{
					lblExpirationDate.Visible = true;
				}
			}
			if (boolPrintReRegOnFront)
			{
				PageHeader.Visible = false;
				GroupHeader1.Visible = true;
				lnSignature.Visible = true;
				if (intLineToPrint == 1)
				{
					Label1.Visible = true;
					Label2.Visible = true;
					Label3.Visible = true;
					Label4.Visible = true;
					Label5.Visible = true;
					Label6.Visible = true;
					Label7.Visible = true;
					Label8.Visible = true;
					Label9.Visible = true;
					txtOwnerKeeper.Visible = true;
					txtVetOrNeuterSpay.Visible = true;
					lblSignature.Visible = true;
				}
				else
				{
					Label1.Visible = false;
					Label2.Visible = false;
					Label3.Visible = false;
					Label4.Visible = false;
					Label5.Visible = false;
					Label6.Visible = false;
					Label7.Visible = false;
					Label8.Visible = false;
					Label9.Visible = false;
					lblSignature.Visible = false;
					txtOwnerKeeper.Visible = false;
					txtVetOrNeuterSpay.Visible = false;
				}
				this.PageSettings.Margins.Top = 6F + ((intLineToPrint - 1) * 435) / 1440F;
			}
			else
			{
				GroupHeader1.Visible = false;
				if (boolLaser)
				{
					lnSignature.Visible = true;
				}
			}
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			double dblTemp;
			clsDRWrapper clsTemp = new clsDRWrapper();
			/*? On Error Resume Next  */
			if (rs.EndOfFile())
				return;
			txtIssueDate.Text = Strings.Format(rs.Get_Fields("RabStickerIssue"), "MM/DD/YY");
			txtCertNumber.Text = rs.Get_Fields_String("RabiesCertNum");
			txtYear.Text = FCConvert.ToString(rs.Get_Fields("Year"));
			// txtSticker = rs.Fields("StickerLicNum")
			txtSticker.Text = rs.Get_Fields_String("TAGLICNUM");
			txtExpires.Text = Strings.Format(rs.Get_Fields("RabiesExpDate"), "m/dd/yy");
			// txtAmountCharged = Format(RS.Fields("AmountC"), "Currency")
			dblTemp = Conversion.Val(modGNBas.Statics.typDogTransaction.SpayNonSpayAmount);
			txtAmountCharged.Text = Strings.Format(dblTemp, "0.00");
			if (Conversion.Val(modGNBas.Statics.typDogTransaction.LateFee) > 0)
			{
				dblTemp = Conversion.Val(modGNBas.Statics.typDogTransaction.LateFee);
				dblTemp -= Conversion.Val(modGNBas.Statics.typDogTransaction.WarrantFee);
				if (dblTemp > 0)
				{
					txtLateFee.Text = Strings.Format(dblTemp, "0.00");
				}
			}
			if (boolLookUpLast && modGNBas.Statics.typDogTransaction.SpayNonSpayAmount <= 0)
			{
				clsTemp.OpenRecordset("select * from dogtransactions where boollicense = 1 and dognumber = " + rs.Get_Fields_Int32("ID") + " order by transactiondate desc", "twck0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					dblTemp = Conversion.Val(clsTemp.Get_Fields_Double("spaynonspayamount"));
					txtAmountCharged.Text = Strings.Format(dblTemp, "0.00");
					dblTemp = Conversion.Val(clsTemp.Get_Fields("latefee"));
					dblTemp -= Conversion.Val(clsTemp.Get_Fields("warrantfee"));
					if (dblTemp > 0)
					{
						txtLateFee.Text = Strings.Format(dblTemp, "0.00");
					}
					else
					{
						txtLateFee.Text = "";
					}
				}
			}
			txtExpirationDate.Text = "12/31/" + FCConvert.ToString(rs.Get_Fields("Year"));
			clsTemp.Dispose();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
		}
		//
		// Public Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports2.DDTool)
		// Call VerifyPrintToFile(Me, Tool)
		// End Sub
		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (rs.EndOfFile())
				return;
			txtOwnerKeeper.Text = rs.Get_Fields_String("lastname") + ", " + rs.Get_Fields_String("firstname") + " " + rs.Get_Fields_String("MiddleName");
			// xxkk  "MI"
			txtVetOrNeuterSpay.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("dogvet")));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			if (rs.EndOfFile())
				return;
			txtOwnerName.Visible = modGNBas.Statics.gintLineToPrint == 1;
			txtVet.Visible = modGNBas.Statics.gintLineToPrint == 1;
			if (intLineToPrint == 1)
			{
				txtOwnerName.Text = rs.Get_Fields_String("LastName") + ", " + rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("MiddleName");
				// xxkk  "MI"
				txtVet.Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("DogVet") + " ");
			}
			else
			{
				PageHeader.Height += ((intLineToPrint - 1) * 350) / 1440F;
			}
		}

		
	}
}
