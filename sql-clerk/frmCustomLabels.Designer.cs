//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	partial class frmCustomLabels
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCTextBox txtHorizontalAdjust;
		public fecherFoundation.FCTextBox txtAlignment;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraReports;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCGrid vsLayout;
        public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCFrame fraType;
		public fecherFoundation.FCComboBox cmbLabelType;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblLaserAdjustment;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
        public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCCheckBox mnuEliminateDuplicates;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
			
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomLabels));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.txtHorizontalAdjust = new fecherFoundation.FCTextBox();
            this.txtAlignment = new fecherFoundation.FCTextBox();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.cmdClear = new fecherFoundation.FCButton();
            this.fraReports = new fecherFoundation.FCFrame();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.cmdExit = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.fraType = new fecherFoundation.FCFrame();
            this.cmbLabelType = new fecherFoundation.FCComboBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.mnuEliminateDuplicates = new fecherFoundation.FCCheckBox();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.Line1 = new fecherFoundation.FCLine();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblLaserAdjustment = new fecherFoundation.FCLabel();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.cmdAddColumn = new fecherFoundation.FCButton();
            this.cmdDeleteColumn = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReports)).BeginInit();
            this.fraReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
            this.fraType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mnuEliminateDuplicates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraReports);
            this.ClientArea.Controls.Add(this.txtHorizontalAdjust);
            this.ClientArea.Controls.Add(this.txtAlignment);
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblLaserAdjustment);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Controls.Add(this.cmdAddColumn);
            this.TopPanel.Controls.Add(this.cmdDeleteColumn);
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(176, 30);
            this.HeaderText.Text = "Custom Labels";
			 this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbReport
            // 
			 this.cmbReport.AutoSize = false;
            this.cmbReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbReport.FormattingEnabled = true;
            this.cmbReport.Items.AddRange(new object[] {
            "Create New Label",
            "Show Saved Label",
            "Delete Saved Label"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(301, 40);
            this.cmbReport.TabIndex = 11;
            this.cmbReport.Text = "Create New Label";
			 this.ToolTip1.SetToolTip(this.cmbReport, null);
            this.cmbReport.ToolTipText = null;
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // txtHorizontalAdjust
            // 
			 this.txtHorizontalAdjust.AutoSize = false;
            this.txtHorizontalAdjust.BackColor = System.Drawing.SystemColors.Window;
			this.txtHorizontalAdjust.LinkItem = null;
            this.txtHorizontalAdjust.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtHorizontalAdjust.LinkTopic = null;
            this.txtHorizontalAdjust.Location = new System.Drawing.Point(893, 646);
            this.txtHorizontalAdjust.Name = "txtHorizontalAdjust";
            this.txtHorizontalAdjust.Size = new System.Drawing.Size(79, 40);
            this.txtHorizontalAdjust.TabIndex = 24;
            this.txtHorizontalAdjust.Text = "0";
            this.ToolTip1.SetToolTip(this.txtHorizontalAdjust, "Horizontal adjustment is in terms of characters or 1/12 of an inch");
			this.txtHorizontalAdjust.ToolTipText = null;
            // 
            // txtAlignment
            // 
			 this.txtAlignment.AutoSize = false;
            this.txtAlignment.BackColor = System.Drawing.SystemColors.Window;
			this.txtAlignment.LinkItem = null;
            this.txtAlignment.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAlignment.LinkTopic = null;
            this.txtAlignment.Location = new System.Drawing.Point(893, 561);
            this.txtAlignment.Name = "txtAlignment";
            this.txtAlignment.Size = new System.Drawing.Size(79, 40);
            this.txtAlignment.TabIndex = 22;
            this.txtAlignment.Text = "0";
            this.ToolTip1.SetToolTip(this.txtAlignment, "Laser line adjustment expressed in lines");
			this.txtAlignment.ToolTipText = null;
            // 
            // fraWhere
            // 
            this.fraWhere.AppearanceKey = "groupBoxNoBorders";
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 507);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(843, 192);
            this.fraWhere.TabIndex = 11;
            this.fraWhere.Text = "Select Search Criteria";
			this.ToolTip1.SetToolTip(this.fraWhere, null);
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
			 this.vsWhere.AllowSelection = false;
            this.vsWhere.AllowUserToResizeColumns = false;
            this.vsWhere.AllowUserToResizeRows = false;
            this.vsWhere.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsWhere.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsWhere.BackColorBkg = System.Drawing.Color.Empty;
            this.vsWhere.BackColorFixed = System.Drawing.Color.Empty;
            this.vsWhere.BackColorSel = System.Drawing.Color.Empty;
            this.vsWhere.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsWhere.Cols = 10;
			 dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsWhere.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsWhere.ColumnHeadersHeight = 30;
            this.vsWhere.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
		
            this.vsWhere.ColumnHeadersVisible = false;
			 dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsWhere.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsWhere.DragIcon = null;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			 this.vsWhere.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsWhere.ExtendLastCol = true;
            this.vsWhere.FixedRows = 0;
			 this.vsWhere.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsWhere.FrozenCols = 0;
            this.vsWhere.GridColor = System.Drawing.Color.Empty;
            this.vsWhere.GridColorFixed = System.Drawing.Color.Empty;
            this.vsWhere.Location = new System.Drawing.Point(0, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.OutlineCol = 0;
			 this.vsWhere.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsWhere.RowHeightMin = 0;
            this.vsWhere.Rows = 0;
			;
            this.vsWhere.ScrollTipText = null;
            this.vsWhere.ShowColumnVisibilityMenu = false;
            this.vsWhere.Size = new System.Drawing.Size(843, 162);
            this.vsWhere.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.vsWhere, null);
            this.vsWhere.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEdit);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(698, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(144, 24);
            this.cmdClear.TabIndex = 13;
            this.cmdClear.Text = "Clear Search Criteria";
			this.ToolTip1.SetToolTip(this.cmdClear, null);
            this.cmdClear.ToolTipText = null;
            this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // fraReports
            // 
            this.fraReports.Controls.Add(this.cmdAdd);
            this.fraReports.Controls.Add(this.cmbReport);
            this.fraReports.Controls.Add(this.cboSavedReport);
            this.fraReports.Location = new System.Drawing.Point(713, 275);
            this.fraReports.Name = "fraReports";
            this.fraReports.Size = new System.Drawing.Size(341, 212);
            this.fraReports.TabIndex = 5;
            this.fraReports.Text = "Report";
			this.ToolTip1.SetToolTip(this.fraReports, null);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(20, 150);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(301, 40);
            this.cmdAdd.TabIndex = 10;
            this.cmdAdd.Text = "Add Custom Label to Library";
			 this.ToolTip1.SetToolTip(this.cmdAdd, null);
            this.cmdAdd.ToolTipText = null;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cboSavedReport
            // 
			
            this.cboSavedReport.AutoSize = false;
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
			 this.cboSavedReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboSavedReport.FormattingEnabled = true;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 90);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(301, 40);
            this.cboSavedReport.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.cboSavedReport, null);
            this.cboSavedReport.ToolTipText = null;
            this.cboSavedReport.Visible = false;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Location = new System.Drawing.Point(355, 275);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(338, 212);
            this.fraSort.TabIndex = 3;
            this.fraSort.Text = "Fields To Sort By";
			this.ToolTip1.SetToolTip(this.fraSort, null);
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
			 this.lstSort.AllowDrag = true;
            this.lstSort.AllowDrop = true;
            this.lstSort.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lstSort.Appearance = 0;
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
			 this.lstSort.MultiSelect = 0;
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(298, 162);
			this.lstSort.Sorted = false;
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.lstSort, "Drag and drop items to arrange the order");
			this.lstSort.ToolTipText = null;
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Location = new System.Drawing.Point(30, 275);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(305, 212);
			 this.fraFields.TabIndex = 0;
            this.fraFields.Text = "Fields To Display On Report";
			this.ToolTip1.SetToolTip(this.fraFields, null);
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
			this.lstFields.AllowDrag = true;
            this.lstFields.AllowDrop = true;
            this.lstFields.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			 this.lstFields.Appearance = 0;
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
			this.lstFields.MultiSelect = 0;
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(265, 162);
			this.lstFields.Sorted = false;
            this.lstFields.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.lstFields, null);
            this.lstFields.ToolTipText = null;
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // cmdExit
            // 
			this.cmdExit.AppearanceKey = "toolbarButton";
            this.cmdExit.Location = new System.Drawing.Point(275, 612);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(293, 26);
            this.cmdExit.TabIndex = 14;
            this.cmdExit.Text = "Exit";
			this.ToolTip1.SetToolTip(this.cmdExit, null);
            this.cmdExit.ToolTipText = null;
            this.cmdExit.Visible = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.fraType);
            this.Frame1.Controls.Add(this.fraMessage);
            this.Frame1.Controls.Add(this.vsLayout);
            this.Frame1.Controls.Add(this.Line1);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(960, 225);
            this.Frame1.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // Line1
            // 
            this.Line1.LineColor = System.Drawing.Color.Red;
            this.Line1.LineSize = 2;
            this.Line1.Location = new System.Drawing.Point(712, 31);
            this.Line1.Name = "Line1";
            this.Line1.Orientation = Wisej.Web.Orientation.Vertical;
            this.Line1.Size = new System.Drawing.Size(2, 240);
            // 
            // 
            // fraType
            // 
            this.fraType.Controls.Add(this.cmbLabelType);
            this.fraType.Controls.Add(this.lblDescription);
            this.fraType.Controls.Add(this.mnuEliminateDuplicates);
            this.fraType.Location = new System.Drawing.Point(661, 0);
            this.fraType.Name = "fraType";
            this.fraType.Size = new System.Drawing.Size(256, 195);
            this.fraType.TabIndex = 18;
            this.fraType.Text = "Label Type";
            // 
            // cmbLabelType
            // 
			this.cmbLabelType.AutoSize = false;
            this.cmbLabelType.BackColor = System.Drawing.SystemColors.Window;
			 this.cmbLabelType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbLabelType.FormattingEnabled = true;

            this.cmbLabelType.Location = new System.Drawing.Point(20, 30);
            this.cmbLabelType.Name = "cmbLabelType";
            this.cmbLabelType.Size = new System.Drawing.Size(217, 40);
            this.cmbLabelType.TabIndex = 20;
			 this.ToolTip1.SetToolTip(this.cmbLabelType, null);
            this.cmbLabelType.ToolTipText = null;
            this.cmbLabelType.SelectedIndexChanged += new System.EventHandler(this.cmbLabelType_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(20, 88);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(217, 52);
            this.lblDescription.TabIndex = 21;
            this.lblDescription.Text = "LABEL1";
			this.ToolTip1.SetToolTip(this.lblDescription, null);
            this.lblDescription.ToolTipText = null;		
            // 
            
            // fraMessage
            // 
            this.fraMessage.AppearanceKey = "groupBoxNoBorders";
            this.fraMessage.Controls.Add(this.Label3);
			this.fraMessage.Location = new System.Drawing.Point(0, 0);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(637, 56);
            this.fraMessage.TabIndex = 16;
			this.ToolTip1.SetToolTip(this.fraMessage, null);
            // 
            // Label3
            // 
			this.Label3.Location = new System.Drawing.Point(0, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(559, 26);
            this.Label3.TabIndex = 17;
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			 this.ToolTip1.SetToolTip(this.Label3, null);
            this.Label3.ToolTipText = null;
            // 
            // vsLayout
            // 
			this.vsLayout.AllowSelection = false;
            this.vsLayout.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
            this.vsLayout.AllowUserToResizeRows = false;
            this.vsLayout.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLayout.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsLayout.BackColorBkg = System.Drawing.Color.Empty;
            this.vsLayout.BackColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.BackColorSel = System.Drawing.Color.Empty;
            this.vsLayout.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsLayout.Cols = 2;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsLayout.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsLayout.ColumnHeadersHeight = 30;
            this.vsLayout.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsLayout.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsLayout.DragIcon = null;
            this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsLayout.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
			this.vsLayout.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.FrozenCols = 0;
            this.vsLayout.GridColor = System.Drawing.Color.Empty;
            this.vsLayout.GridColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.Location = new System.Drawing.Point(0, 0);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.OutlineCol = 0;
            this.vsLayout.RowHeadersVisible = false;
			this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsLayout.RowHeightMin = 0;
			this.vsLayout.RowHeightMin = 0;
            this.vsLayout.Rows = 1;
            this.vsLayout.ScrollTipText = null;
            this.vsLayout.ShowColumnVisibilityMenu = false;
            this.vsLayout.Size = new System.Drawing.Size(641, 225);
            this.vsLayout.StandardTab = true;
            this.vsLayout.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsLayout.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;

            this.vsLayout.TabIndex = 19;
			this.ToolTip1.SetToolTip(this.vsLayout, null);
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(893, 621);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(158, 15);
            this.Label1.TabIndex = 25;
            this.Label1.Text = "HORIZONTAL ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.Label1, "Horizontal adjustment is in terms of characters or 1/12 of an inch");
			this.Label1.ToolTipText = null;
            // 
            // lblLaserAdjustment
            // 
            this.lblLaserAdjustment.Location = new System.Drawing.Point(893, 536);
            this.lblLaserAdjustment.Name = "lblLaserAdjustment";
            this.lblLaserAdjustment.Size = new System.Drawing.Size(148, 15);
            this.lblLaserAdjustment.TabIndex = 23;
            this.lblLaserAdjustment.Text = "LASER LINE ADJUSTMENT";
            this.lblLaserAdjustment.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			 this.ToolTip1.SetToolTip(this.lblLaserAdjustment, null);
            this.lblLaserAdjustment.ToolTipText = null;
            // 
			 // mnuEliminateDuplicates
            // 
            this.mnuEliminateDuplicates.Checked = true;
            this.mnuEliminateDuplicates.Location = new System.Drawing.Point(20, 130);
            this.mnuEliminateDuplicates.Size = new System.Drawing.Size(227, 43);
            this.mnuEliminateDuplicates.Name = "mnuEliminateDuplicates";
            this.mnuEliminateDuplicates.Text = "Eliminate Duplicate Labels";
            this.mnuEliminateDuplicates.TabIndex = 3;
            this.mnuEliminateDuplicates.CheckedChanged += new System.EventHandler(this.mnuEliminateDuplicates_Click);
			//
            // mnuClear
            // 
            this.mnuClear.Index = -1;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSP2,
            this.mnuAddColumn,
            this.mnuDeleteColumn,
            this.mnuSepar3,
            this.mnuSP21,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 0;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column                      ";
            this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 2;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column     ";
            this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 3;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuSP21
            // 
            this.mnuSP21.Index = 4;
            this.mnuSP21.Name = "mnuSP21";
            this.mnuSP21.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 5;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = 6;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrintPreview.Text = "Print/Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 7;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 8;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(440, 30);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(148, 48);
			 this.cmdPrintPreview.TabIndex = 0;
            this.cmdPrintPreview.Text = "Print Preview";
			this.ToolTip1.SetToolTip(this.cmdPrintPreview, null);
            this.cmdPrintPreview.ToolTipText = null;
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // cmdAddColumn
            // 
            this.cmdAddColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
           this.cmdAddColumn.AppearanceKey = "toolbarButton";
			this.cmdAddColumn.Location = new System.Drawing.Point(848, 29);
            this.cmdAddColumn.Name = "cmdAddColumn";
            this.cmdAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdAddColumn.Size = new System.Drawing.Size(96, 24);
            this.cmdAddColumn.TabIndex = 1;
            this.cmdAddColumn.Text = "Add Column";
			this.ToolTip1.SetToolTip(this.cmdAddColumn, null);
            this.cmdAddColumn.ToolTipText = null;
            this.cmdAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // cmdDeleteColumn
            // 
            this.cmdDeleteColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteColumn.AppearanceKey = "toolbarButton";
			this.cmdDeleteColumn.Location = new System.Drawing.Point(950, 29);
            this.cmdDeleteColumn.Name = "cmdDeleteColumn";
            this.cmdDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdDeleteColumn.Size = new System.Drawing.Size(110, 24);
            this.cmdDeleteColumn.TabIndex = 2;
            this.cmdDeleteColumn.Text = "Delete Column";
			 this.ToolTip1.SetToolTip(this.cmdDeleteColumn, null);
            this.cmdDeleteColumn.ToolTipText = null;
            this.cmdDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(1066, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdPrint.Size = new System.Drawing.Size(48, 24);
            this.cmdPrint.TabIndex = 3;
            this.cmdPrint.Text = "Print";
			this.ToolTip1.SetToolTip(this.cmdPrint, null);
            this.cmdPrint.ToolTipText = null;
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmCustomLabels
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomLabels";
            this.Text = "Custom Labels";
			this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCustomLabels_Load);
            this.Activated += new System.EventHandler(this.frmCustomLabels_Activated);
            this.Resize += new System.EventHandler(this.frmCustomLabels_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomLabels_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReports)).EndInit();
            this.fraReports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
            this.fraType.ResumeLayout(false);
            this.fraType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mnuEliminateDuplicates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrintPreview;
		private FCButton cmdDeleteColumn;
		private FCButton cmdAddColumn;
        private FCButton cmdPrint;
    }
}