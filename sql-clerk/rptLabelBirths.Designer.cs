﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptLabelBirths.
	/// </summary>
	partial class rptLabelBirths
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLabelBirths));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmended = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoleParent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDateIssued = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAttestedBy = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDeceased = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.L1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCityOrTown = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoleParent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAttestedBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeceased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCityOrTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field6,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Field5,
				this.txtAmended,
				this.txtSoleParent,
				this.lblDateIssued,
				this.lblAttestedBy,
				this.txtDeceased,
				this.L1,
				this.L2,
				this.lblCityOrTown
			});
			this.Detail.Height = 10.5F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Field6
			// 
			this.Field6.Height = 0.1666667F;
			this.Field6.Left = 4.25F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field6.Text = "Attendant\'s Title";
			this.Field6.Top = 4.125F;
			this.Field6.Width = 1F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1.8125F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field1.Text = "Name of Child";
			this.Field1.Top = 2.75F;
			this.Field1.Width = 3.4375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1666667F;
			this.Field2.Left = 5.375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field2.Text = "DOB";
			this.Field2.Top = 2.75F;
			this.Field2.Width = 2.875F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 1.8125F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field3.Text = "Sex";
			this.Field3.Top = 3.25F;
			this.Field3.Width = 0.9375F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1666667F;
			this.Field4.Left = 5.375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field4.Text = "Place of Birth";
			this.Field4.Top = 3.25F;
			this.Field4.Width = 2.875F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.5F;
			this.Field7.Left = 5.3125F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field7.Text = "Attendant\'s Address";
			this.Field7.Top = 4.125F;
			this.Field7.Width = 2.1875F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 1.8125F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field8.Text = "Mother\'s Maiden Name";
			this.Field8.Top = 5.125F;
			this.Field8.Width = 2.5625F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 5.375F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field9.Text = "Mother\'s residence";
			this.Field9.Top = 5.125F;
			this.Field9.Width = 2.5625F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 1.8125F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field10.Text = null;
			this.Field10.Top = 6.0625F;
			this.Field10.Width = 2.5625F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 1.6875F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field11.Text = "Clerk of Record";
			this.Field11.Top = 7F;
			this.Field11.Width = 2.5F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1666667F;
			this.Field12.Left = 4.666667F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field12.Text = "Muni";
			this.Field12.Top = 7F;
			this.Field12.Width = 1.333333F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1666667F;
			this.Field13.Left = 6.125F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field13.Text = "Date of Filing";
			this.Field13.Top = 7F;
			this.Field13.Width = 2.208333F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 2.25F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field14.Text = "Attest";
			this.Field14.Top = 10.01389F;
			this.Field14.Width = 2.4375F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.2083333F;
			this.Field15.Left = 2.25F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field15.Text = "Date Issued";
			this.Field15.Top = 9.625F;
			this.Field15.Width = 3.083333F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 5.1875F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field16.Text = "Muni";
			this.Field16.Top = 10.01389F;
			this.Field16.Width = 2.25F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.8125F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label3.Text = "FULL NAME OF CHILD";
			this.Label3.Top = 2.5F;
			this.Label3.Width = 2.125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 5.375F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label4.Text = "DATE OF BIRTH";
			this.Label4.Top = 2.5F;
			this.Label4.Width = 1.625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.9375F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label5.Text = "CHILD";
			this.Label5.Top = 2.8125F;
			this.Label5.Width = 0.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.8125F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label6.Text = "ATTENDANT";
			this.Label6.Top = 3.875F;
			this.Label6.Width = 0.875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.875F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label7.Text = "MOTHER";
			this.Label7.Top = 4.875F;
			this.Label7.Width = 0.75F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.875F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label8.Text = "FATHER";
			this.Label8.Top = 5.875F;
			this.Label8.Width = 0.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.8125F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label9.Text = "SEX";
			this.Label9.Top = 3.0625F;
			this.Label9.Width = 1.3125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.375F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label10.Text = "BIRTHPLACE";
			this.Label10.Top = 3.0625F;
			this.Label10.Width = 1.625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.8125F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label11.Text = "NAME AND TITLE OF ATTENDANT";
			this.Label11.Top = 3.875F;
			this.Label11.Width = 2.875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 5.375F;
			this.Label12.MultiLine = false;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label12.Text = "ADDRESS";
			this.Label12.Top = 3.875F;
			this.Label12.Width = 1.1875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.8125F;
			this.Label13.MultiLine = false;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label13.Text = "MAIDEN NAME OF MOTHER";
			this.Label13.Top = 4.875F;
			this.Label13.Width = 2.5F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.375F;
			this.Label14.MultiLine = false;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label14.Text = "RESIDENCE OF MOTHER";
			this.Label14.Top = 4.875F;
			this.Label14.Width = 1.75F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 1.8125F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label15.Text = "FATHER\'S NAME";
			this.Label15.Top = 5.875F;
			this.Label15.Width = 2.3125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.6875F;
			this.Label16.MultiLine = false;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label16.Text = "NAME OF REGISTRAR RECORDING THIS BIRTH";
			this.Label16.Top = 6.8125F;
			this.Label16.Width = 2.875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.71875F;
			this.Label17.MultiLine = false;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label17.Text = "CITY OR TOWN";
			this.Label17.Top = 6.8125F;
			this.Label17.Width = 1.3125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.8125F;
			this.Label18.MultiLine = false;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label18.Text = "DATE OF FILING";
			this.Label18.Top = 6.8125F;
			this.Label18.Width = 1.5625F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 1.8125F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field5.Text = "Attendant\'s Name";
			this.Field5.Top = 4.125F;
			this.Field5.Width = 2.375F;
			// 
			// txtAmended
			// 
			this.txtAmended.CanGrow = false;
			this.txtAmended.Height = 0.4375F;
			this.txtAmended.Left = 1.8125F;
			this.txtAmended.Name = "txtAmended";
			this.txtAmended.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAmended.Text = null;
			this.txtAmended.Top = 2F;
			this.txtAmended.Width = 5.3125F;
			// 
			// txtSoleParent
			// 
			this.txtSoleParent.Height = 0.1875F;
			this.txtSoleParent.Left = 1.6875F;
			this.txtSoleParent.Name = "txtSoleParent";
			this.txtSoleParent.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSoleParent.Text = null;
			this.txtSoleParent.Top = 7.8125F;
			this.txtSoleParent.Width = 2.5625F;
			// 
			// lblDateIssued
			// 
			this.lblDateIssued.Height = 0.2083333F;
			this.lblDateIssued.HyperLink = null;
			this.lblDateIssued.Left = 2.25F;
			this.lblDateIssued.MultiLine = false;
			this.lblDateIssued.Name = "lblDateIssued";
			this.lblDateIssued.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblDateIssued.Text = "DATE ISSUED";
			this.lblDateIssued.Top = 9.416667F;
			this.lblDateIssued.Width = 0.875F;
			// 
			// lblAttestedBy
			// 
			this.lblAttestedBy.Height = 0.2083333F;
			this.lblAttestedBy.HyperLink = null;
			this.lblAttestedBy.Left = 2.25F;
			this.lblAttestedBy.MultiLine = false;
			this.lblAttestedBy.Name = "lblAttestedBy";
			this.lblAttestedBy.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblAttestedBy.Text = "ATTESTED BY";
			this.lblAttestedBy.Top = 9.833333F;
			this.lblAttestedBy.Width = 0.96875F;
			// 
			// txtDeceased
			// 
			this.txtDeceased.Height = 0.25F;
			this.txtDeceased.Left = 3.25F;
			this.txtDeceased.Name = "txtDeceased";
			this.txtDeceased.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDeceased.Text = null;
			this.txtDeceased.Top = 1.666667F;
			this.txtDeceased.Width = 2.583333F;
			// 
			// L1
			// 
			this.L1.Height = 0.1666667F;
			this.L1.HyperLink = null;
			this.L1.Left = 0.08333334F;
			this.L1.MultiLine = false;
			this.L1.Name = "L1";
			this.L1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.L1.Text = "CERTIFIED ABSTRACT OF A CERTIFICATE OF LIVE BIRTH";
			this.L1.Top = 1.166667F;
			this.L1.Width = 8F;
			// 
			// L2
			// 
			this.L2.Height = 0.1666667F;
			this.L2.HyperLink = null;
			this.L2.Left = 0.08333334F;
			this.L2.MultiLine = false;
			this.L2.Name = "L2";
			this.L2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.L2.Text = "DEPARTMENT OF HEALTH AND HUMAN SERVICES";
			this.L2.Top = 1.416667F;
			this.L2.Width = 8F;
			// 
			// lblCityOrTown
			// 
			this.lblCityOrTown.Height = 0.2083333F;
			this.lblCityOrTown.HyperLink = null;
			this.lblCityOrTown.Left = 5.1875F;
			this.lblCityOrTown.MultiLine = false;
			this.lblCityOrTown.Name = "lblCityOrTown";
			this.lblCityOrTown.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.lblCityOrTown.Text = "CITY OR TOWN";
			this.lblCityOrTown.Top = 9.833333F;
			this.lblCityOrTown.Width = 1.3125F;
			// 
			// rptLabelBirths
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.395833F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoleParent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAttestedBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeceased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCityOrTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoleParent;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateIssued;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAttestedBy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeceased;
		private GrapeCity.ActiveReports.SectionReportModel.Label L1;
		private GrapeCity.ActiveReports.SectionReportModel.Label L2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCityOrTown;
	}
}
