//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDeathSummary.
	/// </summary>
	public partial class rptDeathSummary : BaseSectionReport
	{
		public rptDeathSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Death Vital Statistics";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDeathSummary InstancePtr
		{
			get
			{
				return (rptDeathSummary)Sys.GetInstance(typeof(rptDeathSummary));
			}
		}

		protected rptDeathSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDeathSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		int intPageNumber;
		string strStartDate;
		string strEndDate;

		public void Init(ref string strRange)
		{
			string[] strTemp = null;
			strTemp = Strings.Split(strRange, ";", -1, CompareConstants.vbTextCompare);
			strStartDate = strTemp[0];
			strEndDate = strTemp[1];
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "DeathSummary");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rs.EndOfFile())
				return;
			txtName.Text = rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("MiddleName") + " " + rs.Get_Fields_String("LastName");
			if (Convert.ToDateTime(rs.Get_Fields("dateofdeath")).ToOADate() != 0 && Information.IsDate(rs.Get_Fields("dateofdeath")))
			{
				txtBorn.Text = Strings.Format(rs.Get_Fields("DateofDeath"), "MMMM dd, yyyy");
			}
			else if (rs.Get_Fields_String("dateofdeathdescription") != string.Empty)
			{
				txtBorn.Text = rs.Get_Fields_String("dateofdeathdescription");
			}
			else
			{
				txtBorn.Text = "";
			}
			if (!rs.EndOfFile())
				rs.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtTown.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "H:mm AM/PM");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtCaption.Text = "Town Clerk's Report";
			txtCaption2.Text = "Deaths - " + strStartDate + " to " + strEndDate;
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			txtTown.Text = modStartup.Statics.CityTown + " of " + modGlobalConstants.Statics.MuniName;
			intPageNumber = 1;
			// Set db = OpenDatabase(ClerkDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
			// SET DB =
			// Call RS.OpenRecordset("Select * from Deaths where (DateofDeath between #01/01/" & gintReportYear & "# and  #12/31/" & gintReportYear & "#) or (DATEOFdeathdescription like '*" & gintReportYear & "*') Order by LastName", DEFAULTDATABASENAME)
			rs.OpenRecordset("Select * from Deaths where (DateofDeath between '" + strStartDate + "' and  '" + strEndDate + "') or (isdate(dateofdeathdescription) = 1 and convert(datetime, dateofdeathdescription) between '" + strStartDate + "' and '" + strEndDate + "') or (ActualDate between '" + strStartDate + "' and  '" + strEndDate + "') Order by LastName", modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				rs.MoveLast();
				rs.MoveFirst();
				if (rs.RecordCount() == 0)
				{
					txtTotal.Text = string.Empty;
				}
				else if (rs.RecordCount() == 1)
				{
					txtTotal.Text = "There was a total of " + FCConvert.ToString(rs.RecordCount()) + " Death";
				}
				else
				{
					txtTotal.Text = "There were a total of " + FCConvert.ToString(rs.RecordCount()) + " Deaths";
				}
			}
			// Call SetPrintProperties(Me)
		}
		
	}
}
