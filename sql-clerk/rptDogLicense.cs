//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogLicense.
	/// </summary>
	public partial class rptDogLicense : BaseSectionReport
	{
		public rptDogLicense()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Dog License";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDogLicense InstancePtr
		{
			get
			{
				return (rptDogLicense)Sys.GetInstance(typeof(rptDogLicense));
			}
		}

		protected rptDogLicense _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDogLicense	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		// Private boolPrinted As Boolean
		// Private intPrinted  As Integer
		bool boolLookUpLast;

		private void ActiveReport_Initialize()
		{
			// boolPrinted = False
			// intPrinted = 0
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			// boolPrinted = True
			// intPrinted = Me.Printer.DeviceCopies
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string[] strAry = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngTemp = 0;
			/*? On Error Resume Next  */
			if (rs.EndOfFile())
				return;
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) == 0)
			{
				txtMuni.Text = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				txtMuni.Text = modRegionalTown.GetTownKeyName_2(rs.Get_Fields_Int32("towncode"));
			}
			cParty owner;
			cPartyAddress owneraddress;
			cPartyPhoneNumber ownerphone;
			cPartyController pCont = new cPartyController();
			owner = pCont.GetParty(rs.Get_Fields_Int32("partyid"));
			owneraddress = owner.GetPrimaryAddress();
			if (owner.PhoneNumbers.Count > 0)
			{
				ownerphone = owner.PhoneNumbers[1];
			}
			else
			{
				ownerphone = null;
			}
			if (rs.Get_Fields_DateTime("dogdob").ToOADate() != 0)
			{
				txtDOB.Text = FCConvert.ToString(rs.Get_Fields("DogDOB"));
			}
			txtSex.Text = rs.Get_Fields_String("DogSex");
			txtDogName.Text = rs.Get_Fields_String("DogName");
			txtVeterinarian.Text = rs.Get_Fields_String("DogVet");
			txtColor.Text = rs.Get_Fields_String("DogColor");
			txtOwnerName.Text = owner.FullName;
			// txtOwnerName = rs.Fields("FirstName") & " " & rs.Fields("MI") & " " & rs.Fields("LastName") & " " & rs.Fields("Desig")
			// txtAddress = RS.Fields("Address") & "  " & RS.Fields("City") & ", " & GetDefaultStateName(Val(RS.Fields("State"))) & " " & IIf(Len(RS.Fields("Zip")) > 5, Left(RS.Fields("Zip"), 5) & "-" & Mid(RS.Fields("Zip"), 6, Len(RS.Fields("Zip")) - 5), RS.Fields("Zip"))
			if (!(owneraddress == null))
			{
				txtAddress.Text = owneraddress.Address1 + "  " + owneraddress.City + ", " + owneraddress.State + " " + owneraddress.Zip;
			}
			if (!(ownerphone == null))
			{
				txtPhone.Text = ownerphone.PhoneNumber;
			}
			else
			{
				txtPhone.Text = "";
			}
			txtLocation.Text = rs.Get_Fields_String("LocationNum") + " " + rs.Get_Fields_String("LocationSTR");
			txtSticker.Text = "Sticker #" + rs.Get_Fields_String("StickerLicNum");
			txtBreed.Text = rs.Get_Fields_String("DogBreed");
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("KennelDog")))
			{
				// txtTagNumber = "K " & RS.Fields("TagLicNum")
				txtTagNumber.Text = "";
				clsTemp.OpenRecordset("select * from kennellicense where ownernum = " + rs.Get_Fields_Int32("ownernum"), "twck0000.vb1");
				while (!clsTemp.EndOfFile())
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("Dognumbers"))) != string.Empty)
					{
						strAry = Strings.Split(FCConvert.ToString(clsTemp.Get_Fields_Int32("dognumber")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
						{
							if (Conversion.Val(strAry[x]) == rs.Get_Fields_Int32("dognumber"))
							{
								lngTemp = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("licenseid"));
								clsTemp.OpenRecordset("select * from doginventory where ID = " + FCConvert.ToString(lngTemp), "twck0000.vb1");
								if (!clsTemp.EndOfFile())
								{
									txtTagNumber.Text = "K " + clsTemp.Get_Fields_Int32("stickernumber");
								}
								break;
							}
						}
						// x
					}
					clsTemp.MoveNext();
				}
			}
			else
			{
				txtTagNumber.Text = rs.Get_Fields_String("TagLicNum");
			}
			if (Information.IsDate(rs.Get_Fields("rabiesexpdate")))
			{
				if (rs.Get_Fields_DateTime("rabiesexpdate").ToOADate() != 0)
				{
					txtRabiesExpiration.Text = FCConvert.ToString(rs.Get_Fields_DateTime("RabiesExpDate"));
				}
				else if (rs.Get_Fields_Boolean("RabiesExempt") == true)
				{
					txtRabiesExpiration.Text = "EXEMPT";
				}
				else
				{
					txtRabiesExpiration.Text = "";
				}
			}
			else if (rs.Get_Fields_Boolean("RabiesExempt") == true)
			{
				txtRabiesExpiration.Text = "EXEMPT";
			}
			else
			{
				txtRabiesExpiration.Text = "";
			}
			// txtYear = gintLaserReportYear
			// corey 10/24/03 get year from record
			txtYear.Text = FCConvert.ToString(rs.Get_Fields("year"));
			txtRabiesCertNum.Text = rs.Get_Fields_String("RabiesCertNum");
			txtMF.Visible = true;
			txtMF.Text = rs.Get_Fields_String("DogSex");
			txtNeuter.Visible = rs.Get_Fields_Boolean("DogNeuter");
			txtSSR.Visible = rs.Get_Fields_Boolean("SandR");
			txtHearing.Visible = rs.Get_Fields_Boolean("HearGuide");
			txtHybrid.Visible = rs.Get_Fields_Boolean("WolfHybrid");
			txtLicenseDate.Text = FCConvert.ToString(rs.Get_Fields("RabStickerIssue"));
			txtCertNumber.Text = rs.Get_Fields_String("SNCertNum");
			// COMMENTED THIS OUT PER MARY ANN IN KITTERY ON SETPEMBER 12, 2002
			// SHE ALWAYS WANTS TO SEE THE VALUE IN THIS FIELD TO BE THE AMOUNT
			// CHANRGED AND NOT THE NEUTER VALUE
			// If RS.Fields("DogNeuter") Then
			// txtNeuterValue = Format(DogFee.Fixed_Fee, "#,###.00")
			// Else
			// txtNeuterValue = Format(DogFee.UN_Fixed_FEE, "#,###.00")
			// End If
			// txtNeuterValue = Format(RS.Fields("AmountC"), "#,##0.00")
			// txtNeuterValue.Text = Format(Val(typDogTransaction.Amount), "#,##0.00")
			if (FCConvert.ToBoolean(Conversion.Val(modGNBas.Statics.typDogTransaction.Amount > 0)) | !boolLookUpLast)
			{
				txtNeuterValue.Text = Strings.Format(Conversion.Val(modGNBas.Statics.typDogTransaction.Amount), "#,##0.00");
			}
			else
			{
				clsTemp.OpenRecordset("select * from dogtransactions where boollicense = 1 and dognumber = " + rs.Get_Fields_Int32("id") + " order by transactiondate desc", "twck0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					txtNeuterValue.Text = Strings.Format(Conversion.Val(clsTemp.Get_Fields("amount")), "#,##0.00");
				}
				else
				{
					txtNeuterValue.Text = Strings.Format(Conversion.Val(modGNBas.Statics.typDogTransaction.Amount), "#,##0.00");
				}
			}
			if (!rs.EndOfFile())
				rs.MoveNext();
			eArgs.EOF = rs.EndOfFile();
			clsTemp.Dispose();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	// Call VerifyPrintToFile(Me, Tool)
		//}
		public void Init(ref int lngdognum, bool boolLookUpAmount = false)
		{
			double dblLaserLineAdjustment8;
			// Dim rsData As New clsDRWrapper
			boolLookUpLast = boolLookUpAmount;
			rs.OpenRecordset("SELECT DogOwner.partyid, DogOwner.LocationNum, DogOwner.LocationSTR, dogowner.towncode, DogInfo.* FROM DogInfo INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID where DogInfo.ID = " + FCConvert.ToString(lngdognum), "twck0000.vb1");
			//Application.DoEvents();
			// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
			// If rsData.EndOfFile Then
			// dblLaserLineAdjustment8 = 0
			// Else
			// dblLaserLineAdjustment8 = CDbl(Val(rsData.Fields("DogAdjustment")))
			// End If
			dblLaserLineAdjustment8 = Conversion.Val(modRegistry.GetRegistryKey("DogAdjustment"));
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.PageHeader.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment8) / 1440F;
			}
            //FC:FINAL:SBE - #i1996 - show report as modal dialog
            //frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: false);
            frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: false, showModal: true);
        }

		
	}
}
