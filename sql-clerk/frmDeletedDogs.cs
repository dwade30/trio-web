//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDeletedDogs : BaseForm
	{
		public frmDeletedDogs()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDeletedDogs InstancePtr
		{
			get
			{
				return (frmDeletedDogs)Sys.GetInstance(typeof(frmDeletedDogs));
			}
		}

		protected frmDeletedDogs _InstancePtr = null;
		//=========================================================
		bool boolEditing;
		// vbPorter upgrade warning: lngDogNumber As int	OnWrite(string)
		int lngDogNumber;
		int lngOwnerNumber;
		private bool boolReadOnly;
		const int CNSTGRIDCOLNAME = 0;
		const int CNSTGRIDCOLBREED = 1;
		const int CNSTGRIDCOLCOLOR = 2;
		const int CNSTGRIDCOLDECEASED = 3;
		const int CNSTGRIDCOLLICENSE = 4;
		const int CNSTGRIDCOLSTICKER = 5;
		const int CNSTGRIDCOLID = 6;

		private void frmDeletedDogs_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDeletedDogs_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDeletedDogs properties;
			//frmDeletedDogs.ScaleWidth	= 9225;
			//frmDeletedDogs.ScaleHeight	= 8175;
			//frmDeletedDogs.LinkTopic	= "Form1";
			//frmDeletedDogs.LockControls	= -1  'True;
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			// Dim clsBreeds As New clsDRWrapper
			// Dim clsColors As New clsDRWrapper
			string strColor = "";
			string strBreed = "";
			bool boolDeceased = false;
			UpdateBreeds();
			UpdateColors();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			Grid.Rows = 1;
			SetupGrid();
			CheckPermissions();
			boolEditing = false;
			clsLoad.OpenRecordset("select * from doginfo where ownernum = -1 order by dogname,ID", "TWCK0000.VB1");
			// Call clsBreeds.OpenRecordset("select * from defaultbreeds", "twck0000.vb1")
			// Call clsColors.OpenRecordset("select * from defaultcolors", "twck0000.vb1")
			while (!clsLoad.EndOfFile())
			{
				// If clsBreeds.FindFirstRecord("id", Val(.Fields("DOGbreed"))) Then
				strBreed = clsLoad.Get_Fields_String("dogbreed");
				// Else
				// strBreed = ""
				// End If
				// If clsColors.FindFirstRecord("id", Val(.Fields("dogcolor"))) Then
				strColor = clsLoad.Get_Fields_String("dogcolor");
				// Else
				// strColor = ""
				// End If
				if (clsLoad.Get_Fields_Boolean("DogDeceased"))
				{
					boolDeceased = true;
				}
				else
				{
					boolDeceased = false;
				}
				Grid.AddItem(clsLoad.Get_Fields_String("dogname") + "\t" + strBreed + "\t" + strColor + "\t" + FCConvert.ToString(boolDeceased) + "\t" + clsLoad.Get_Fields_String("taglicnum") + "\t" + clsLoad.Get_Fields_String("StickerLicNum") + "\t" + clsLoad.Get_Fields_Int32("ID"));
				clsLoad.MoveNext();
			}
		}

		private void SetupGrid()
		{
			Grid.Cols = 7;
			Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Name");
			Grid.TextMatrix(0, CNSTGRIDCOLBREED, "Breed");
			Grid.TextMatrix(0, CNSTGRIDCOLCOLOR, "Color");
			Grid.TextMatrix(0, CNSTGRIDCOLDECEASED, "Deceased");
			Grid.TextMatrix(0, CNSTGRIDCOLLICENSE, "License");
			Grid.TextMatrix(0, CNSTGRIDCOLSTICKER, "Sticker");
			Grid.ColDataType(CNSTGRIDCOLDECEASED, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.ColSort(CNSTGRIDCOLDECEASED, FCGrid.SortSettings.flexSortStringAscending);
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.ExtendLastCol = true;
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLNAME, FCConvert.ToInt32(0.22 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLBREED, FCConvert.ToInt32(0.23 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCOLOR, FCConvert.ToInt32(0.2 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDECEASED, FCConvert.ToInt32(0.11 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLLICENSE, FCConvert.ToInt32(0.11 * GridWidth));
		}

		private void frmDeletedDogs_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_BeforeSort(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (Grid.Col == CNSTGRIDCOLDECEASED)
			{
				if (Grid.SortOrder == SortOrder.Ascending)
				{
					Grid.ColSort(CNSTGRIDCOLDECEASED, FCGrid.SortSettings.flexSortStringAscending);
				}
				else
				{
					Grid.ColSort(CNSTGRIDCOLDECEASED, FCGrid.SortSettings.flexSortStringDescending);
				}
				Grid.Sort = FCGrid.SortSettings.flexSortCustom;
			}
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			mnuEditOrList_Click();
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						if (!boolEditing)
						{
							KeyCode = 0;
							mnuDeleteFromGrid_Click();
						}
						break;
					}
			}
			//end switch
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsDelete = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (MessageBox.Show("This will permanently delete the record." + "\r\n" + "Are you sure you want to continue?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					return;
				modGlobalFunctions.AddCYAEntry("CK", "Permanently Deleted Dog", "Number " + FCConvert.ToString(lngDogNumber), "Name " + txtDogName.Text, "", "");
				clsDelete.Execute("delete from doginfo where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
				Grid.RemoveItem(Grid.Row);
				mnuEditOrList_Click();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuDelete_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuDeleteFromGrid_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngStart;
				int lngEnd;
				int lngTemp = 0;
				clsDRWrapper clsDelete = new clsDRWrapper();
				if (boolReadOnly)
					return;
				if (Grid.Row < 1)
					return;
				if (MessageBox.Show("This will permanently delete the record(s)." + "\r\n" + "Are you sure you want to continue?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					return;
				lngStart = Grid.Row;
				lngEnd = Grid.RowSel;
				if (lngEnd < lngStart)
				{
					lngTemp = lngEnd;
					lngEnd = lngStart;
					lngStart = lngTemp;
				}
				for (lngTemp = lngEnd; lngTemp >= lngStart; lngTemp--)
				{
					modGlobalFunctions.AddCYAEntry("CK", "Permanently Deleted Dog", "Number " + Grid.TextMatrix(lngTemp, CNSTGRIDCOLID), "Name " + Grid.TextMatrix(lngTemp, CNSTGRIDCOLNAME), "", "");
					clsDelete.Execute("delete from doginfo where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTGRIDCOLID))), "twck0000.vb1");
					Grid.RemoveItem(lngTemp);
				}
				// lngTemp
				Grid.Row = 0;
				Grid.Refresh();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuDeleteFromGrid_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuDeleteFromGrid_Click()
		{
			mnuDeleteFromGrid_Click(mnuDeleteFromGrid, new System.EventArgs());
		}

		private void mnuEditOrList_Click(object sender, System.EventArgs e)
		{
			if (boolEditing)
			{
				boolEditing = false;
				cmdEditOrList.Text = "Edit Dog";
				//FC:FINAL:DDU:#i2002 - change width of the button based on it's text width
				cmdEditOrList.Width = 70;
				cmdSave.Enabled = false;
				cmdDelete.Enabled = false;
				cmdDelete.Visible = false;
				if (!boolReadOnly)
				{
					cmdDeleteFromGrid.Enabled = true;
					cmdDeleteFromGrid.Visible = true;
				}
				framList.Visible = true;
				framDogs.Visible = false;
				cmdUndelete.Visible = false;
				framList.BringToFront();
			}
			else
			{
				if (Grid.Row < 1)
				{
					MessageBox.Show("You must select a dog to edit", "No dog selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				lngDogNumber = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
				boolEditing = true;
				cmdEditOrList.Text = "Return To List";
				//FC:FINAL:DDU:#i2002 - change width of the button based on it's text width
				cmdEditOrList.Width = 100;
				if (!boolReadOnly)
				{
					cmdSave.Enabled = true;
					cmdDelete.Enabled = true;
					mnuSaveExit.Enabled = true;
				}
				cmdDelete.Visible = true;
				cmdDeleteFromGrid.Enabled = false;
				cmdDeleteFromGrid.Visible = false;
				framDogs.Visible = true;
				framList.Visible = false;
				framDogs.BringToFront();
				LoadInfo();
			}
		}

		public void mnuEditOrList_Click()
		{
			mnuEditOrList_Click(mnuEditOrList, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void UpdateBreeds()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbBreed.Clear();
			clsLoad.OpenRecordset("select * from DEFAULTbreeds order by name", "twck0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				cmbBreed.AddItem(clsLoad.Get_Fields_String("name"));
				cmbBreed.ItemData(cmbBreed.NewIndex, clsLoad.Get_Fields_Int32("id"));
				clsLoad.MoveNext();
			}
		}

		private void UpdateColors()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbColor.Clear();
			clsLoad.OpenRecordset("select * from defaultcolors order by name", "twck0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				cmbColor.AddItem(clsLoad.Get_Fields_String("name"));
				cmbColor.ItemData(cmbColor.NewIndex, clsLoad.Get_Fields_Int32("id"));
				clsLoad.MoveNext();
			}
		}

		private void AssignBreed(ref int lngBreed)
		{
			int x;
			for (x = 0; x <= cmbBreed.Items.Count - 1; x++)
			{
				if (lngBreed == cmbBreed.ItemData(x))
				{
					cmbBreed.SelectedIndex = x;
					return;
				}
			}
			// x
			cmbBreed.SelectedIndex = -1;
		}

		private void AssignColor(ref int lngColor)
		{
			int x;
			for (x = 0; x <= cmbColor.Items.Count - 1; x++)
			{
				if (lngColor == cmbColor.ItemData(x))
				{
					cmbColor.SelectedIndex = x;
					return;
				}
			}
			// x
			cmbColor.SelectedIndex = -1;
		}

		private void LoadInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				lngOwnerNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("previousowner"))));
				if (lngOwnerNumber > 0)
				{
					cmdUndelete.Visible = true;
				}
				else
				{
					cmdUndelete.Visible = false;
					lblOwnerName.Text = "";
				}
				txtDogName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("dogname")));
				txtSex.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("dogsex")));
				txtMarkings.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("dogmarkings")));
				txtVet.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("dogvet")));
				// Call AssignBreed(Val(clsLoad.Fields("dogbreed")))
				// Call AssignColor(Val(clsLoad.Fields("dogcolor")))
				cmbBreed.Text = FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed"));
				cmbColor.Text = FCConvert.ToString(clsLoad.Get_Fields_String("dogcolor"));
				txtYear.Text = FCConvert.ToString(clsLoad.Get_Fields("year"));
				txtSpayNeuterNumber.Text = FCConvert.ToString(clsLoad.Get_Fields_String("sncertnum"));
				txtCertificateNumber.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rabiescertnum"));
				txtRabiesTag.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rabiestagno"));
				txtSticker.Text = FCConvert.ToString(clsLoad.Get_Fields_String("stickerlicnum"));
				txtTag.Text = FCConvert.ToString(clsLoad.Get_Fields_String("taglicnum"));
				if (Information.IsDate(clsLoad.Get_Fields("dogdob")))
				{
					if (clsLoad.Get_Fields_DateTime("dogdob").ToOADate() != 0)
					{
						T2KDOB.Text = Strings.Format(clsLoad.Get_Fields_DateTime("dogdob"), "MM/dd/yyyy");
					}
					else
					{
						T2KDOB.Text = "";
					}
				}
				else
				{
					T2KDOB.Text = "";
				}
				if (Information.IsDate(clsLoad.Get_Fields("rabiesshotdate")))
				{
					if (clsLoad.Get_Fields_DateTime("rabiesshotdate").ToOADate() != 0)
					{
						T2KDateVaccinated.Text = Strings.Format(clsLoad.Get_Fields_DateTime("rabiesshotdate"), "MM/dd/yyyy");
					}
					else
					{
						T2KDateVaccinated.Text = "";
					}
				}
				else
				{
					T2KDateVaccinated.Text = "";
				}
				if (Information.IsDate(clsLoad.Get_Fields("rabstickerissue")))
				{
					if (clsLoad.Get_Fields_DateTime("rabstickerissue").ToOADate() != 0)
					{
						T2KIssueDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("rabstickerissue"), "MM/dd/yyyy");
					}
					else
					{
						T2KIssueDate.Text = "";
					}
				}
				else
				{
					T2KIssueDate.Text = "";
				}
				if (Information.IsDate(clsLoad.Get_Fields("rabiesexpdate")))
				{
					if (clsLoad.Get_Fields_DateTime("rabiesexpdate").ToOADate() != 0)
					{
						t2kExpirationDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("rabiesexpdate"), "MM/dd/yyyy");
					}
					else
					{
						t2kExpirationDate.Text = "";
					}
				}
				else
				{
					t2kExpirationDate.Text = "";
				}
				if (clsLoad.Get_Fields_Boolean("dogneuter"))
				{
					chkNeuterSpay.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkNeuterSpay.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (clsLoad.Get_Fields_Boolean("sandr"))
				{
					chkSearchRescue.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkSearchRescue.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (clsLoad.Get_Fields_Boolean("hearguide"))
				{
					chkHearingGuid.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkHearingGuid.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (clsLoad.Get_Fields_Boolean("wolfhybrid"))
				{
					chkWolfHybrid.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkWolfHybrid.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (clsLoad.Get_Fields_Boolean("DOGDECEASED"))
				{
					chkDeceased.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDeceased.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				rtbComments.Text = clsLoad.Get_Fields_String("comments");
				if (lngOwnerNumber > 0)
				{
					clsLoad.OpenRecordset("select c.*, p.* from dogowner as c CROSS APPLY " + clsLoad.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'CK', NULL) as p where c.ID = " + FCConvert.ToString(lngOwnerNumber), "twck0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						lblOwnerName.Text = clsLoad.Get_Fields_String("FullName");
					}
					else
					{
						lblOwnerName.Text = "";
						cmdUndelete.Visible = false;
					}
				}
			}
			else
			{
				rtbComments.Text = "";
				txtCertificateNumber.Text = "";
				txtDogName.Text = "";
				txtMarkings.Text = "";
				txtRabiesTag.Text = "";
				txtSex.Text = "";
				txtSpayNeuterNumber.Text = "";
				txtSticker.Text = "";
				txtVet.Text = "";
				txtYear.Text = "";
				chkDeceased.CheckState = Wisej.Web.CheckState.Unchecked;
				chkHearingGuid.CheckState = Wisej.Web.CheckState.Unchecked;
				chkNeuterSpay.CheckState = Wisej.Web.CheckState.Unchecked;
				chkSearchRescue.CheckState = Wisej.Web.CheckState.Unchecked;
				chkWolfHybrid.CheckState = Wisej.Web.CheckState.Unchecked;
				T2KDateVaccinated.Text = "";
				T2KDOB.Text = "";
				t2kExpirationDate.Text = "";
				T2KIssueDate.Text = "";
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				clsSave.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
					clsSave.Set_Fields("OwnerNum", -1);
				}
				clsSave.Set_Fields("dogname", fecherFoundation.Strings.Trim(txtDogName.Text));
				Grid.TextMatrix(Grid.Row, CNSTGRIDCOLNAME, fecherFoundation.Strings.Trim(txtDogName.Text));
				clsSave.Set_Fields("dogsex", fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(txtSex.Text)));
				clsSave.Set_Fields("dogmarkings", fecherFoundation.Strings.Trim(txtMarkings.Text));
				clsSave.Set_Fields("dogvet", fecherFoundation.Strings.Trim(txtVet.Text));
				// If cmbBreed.ListIndex > -1 Then
				// clsSave.Fields("dogbreed") = cmbBreed.ItemData(cmbBreed.ListIndex)
				clsSave.Set_Fields("dogbreed", cmbBreed.Text);
				Grid.TextMatrix(Grid.Row, CNSTGRIDCOLBREED, cmbBreed.Text);
				// Else
				// clsSave.Fields("dogbreed") = 0
				// Grid.TextMatrix(Grid.Row, CNSTGRIDCOLBREED) = ""
				// End If
				// If cmbColor.ListIndex > -1 Then
				// clsSave.Fields("dogcolor") = cmbColor.ItemData(cmbColor.ListIndex)
				Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCOLOR, cmbColor.Text);
				clsSave.Set_Fields("dogcolor", cmbColor.Text);
				// Else
				// clsSave.Fields("dogcolor") = 0
				// Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCOLOR) = ""
				// End If
				clsSave.Set_Fields("year", txtYear.Text);
				clsSave.Set_Fields("sncertnum", txtSpayNeuterNumber.Text);
				clsSave.Set_Fields("rabiescertnum", txtCertificateNumber.Text);
				clsSave.Set_Fields("rabiestagno", txtRabiesTag.Text);
				clsSave.Set_Fields("stickerlicnum", txtSticker.Text);
				Grid.TextMatrix(Grid.Row, CNSTGRIDCOLSTICKER, txtSticker.Text);
				clsSave.Set_Fields("taglicnum", txtTag.Text);
				Grid.TextMatrix(Grid.Row, CNSTGRIDCOLLICENSE, txtTag.Text);
				if (Information.IsDate(T2KDOB.Text))
				{
					clsSave.Set_Fields("dogdob", T2KDOB.Text);
				}
				else
				{
					clsSave.Set_Fields("dogdob", 0);
				}
				if (Information.IsDate(T2KDateVaccinated.Text))
				{
					clsSave.Set_Fields("rabiesshotdate", T2KDateVaccinated.Text);
				}
				else
				{
					clsSave.Set_Fields("rabiesshotdate", 0);
				}
				if (Information.IsDate(T2KIssueDate.Text))
				{
					clsSave.Set_Fields("rabstickerissue", T2KIssueDate.Text);
				}
				else
				{
					clsSave.Set_Fields("rabstickerissue", 0);
				}
				if (Information.IsDate(t2kExpirationDate.Text))
				{
					clsSave.Set_Fields("rabiesexpdate", t2kExpirationDate.Text);
				}
				else
				{
					clsSave.Set_Fields("rabiesexpdate", 0);
				}
				clsSave.Set_Fields("dogneuter", chkNeuterSpay.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("sandr", chkSearchRescue.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("hearguide", chkHearingGuid.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("wolfhybrid", chkWolfHybrid.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("DogDeceased", chkDeceased.CheckState == Wisej.Web.CheckState.Checked);
				clsSave.Set_Fields("comments", rtbComments.Text);
				clsSave.Update();
				SaveInfo = true;
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

		private void mnuUndelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp = "";
			int lngSNum;
			int lngTNum;
			int lngY;
			// check to see if it is safe to just undelete it or if we have to clear some fields
			clsSave.OpenRecordset("select [year],stickerlicnum,taglicnum from doginfo where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
			lngSNum = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields_String("stickerlicnum"))));
			lngTNum = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields_String("taglicnum"))));
			lngY = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields("year"))));
			if (lngY > 0)
			{
				clsSave.OpenRecordset("select * from doginfo where  [year] = " + FCConvert.ToString(lngY) + " and convert(int, isnull(stickerlicnum, 0)) = " + FCConvert.ToString(lngSNum) + " and (not ID = " + FCConvert.ToString(lngDogNumber) + ") and ownernum > 0", "twck0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Execute("update doginfo set stickerlicnum = '' where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
					lngSNum = 0;
					MessageBox.Show("The sticker number is in use by another dog, this will be cleared", "Sticker Used", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			clsSave.OpenRecordset("select * from doginfo where convert(int, isnull(taglicnum, 0)) = " + FCConvert.ToString(lngTNum) + " and (not ID = " + FCConvert.ToString(lngDogNumber) + ") and ownernum > 0", "twck0000.vb1");
			if (!clsSave.EndOfFile())
			{
				clsSave.Execute("update doginfo set taglicnum = '' where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
				lngTNum = 0;
				MessageBox.Show("The tag number is in use by another dog, this will be cleared", "Tag Used", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			if (lngTNum > 0)
			{
				clsSave.Execute("update doginventory set status = 'Used' where stickernumber = " + FCConvert.ToString(lngTNum) + " and type = 'T'", "twck0000.vb1");
			}
			if (lngY > 0 && lngSNum > 0)
			{
				clsSave.Execute("update doginventory set status = 'Used' where stickernumber = " + FCConvert.ToString(lngSNum) + " and type = 'S' and [YEAR] = " + FCConvert.ToString(lngY), "twck0000.vb1");
			}
			clsSave.Execute("update doginfo set ownernum = previousowner,previousowner = 0 where ID = " + FCConvert.ToString(lngDogNumber), "twck0000.vb1");
			MessageBox.Show("Dog Undeleted", "Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
			modGlobalFunctions.AddCYAEntry("CK", "Undeleted dog " + FCConvert.ToString(lngDogNumber), "Owner " + FCConvert.ToString(lngOwnerNumber), "", "", "");
			clsSave.OpenRecordset("select * from dogowner where ID = " + FCConvert.ToString(lngOwnerNumber), "twck0000.vb1");
			if (!clsSave.EndOfFile())
			{
				strTemp = fecherFoundation.Strings.Trim(clsSave.Get_Fields_String("dognumbers"));
				clsSave.Edit();
				if (strTemp != string.Empty)
				{
					clsSave.Set_Fields("dognumbers", strTemp + "," + FCConvert.ToString(lngDogNumber));
				}
				else
				{
					clsSave.Set_Fields("dognumbers", lngDogNumber);
				}
				clsSave.Update();
				if (FCConvert.ToBoolean(clsSave.Get_Fields_Boolean("deleted")))
				{
					clsSave.Execute("update dogowner set deleted = false where ownernum = " + FCConvert.ToString(lngOwnerNumber), "twck0000.vb1");
					modGlobalFunctions.AddCYAEntry("CK", "Undeleted owner " + FCConvert.ToString(lngOwnerNumber), "", "", "", "");
					MessageBox.Show("Owner has been undeleted as well", "Owner Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			Grid.RemoveItem(Grid.Row);
			mnuEditOrList_Click();
		}

		private void CheckPermissions()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTEDITDOGSKENNELS)) != "F")
				{
					boolReadOnly = true;
					cmdDelete.Enabled = false;
					cmdDeleteFromGrid.Enabled = false;
					cmdSave.Enabled = false;
					mnuSaveExit.Enabled = false;
					cmdUndelete.Enabled = false;
					framDogs.Enabled = false;
				}
				else
				{
					boolReadOnly = false;
					cmdDelete.Enabled = true;
					cmdDeleteFromGrid.Enabled = true;
					cmdSave.Enabled = true;
					mnuSaveExit.Enabled = true;
					cmdUndelete.Enabled = true;
					framDogs.Enabled = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckPermissions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
