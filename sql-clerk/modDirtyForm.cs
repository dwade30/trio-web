﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWCK0000
{
	public class modDirtyForm
	{
		//=========================================================
		// vbPorter upgrade warning: objName As Form	OnWrite(frmDeaths, frmMarriages, frmClerkDefaults, frmDogWarrant, frmFacilityNames, frmRegistrarNames, frmFees, frmPrinterSettings, frmBirthDelayed, frmBirthForeign, frmDogInfo)
		//FC:FINAL:DDU:#i2033 - call methods correct adding args params
		public static void SaveChanges(int intDataChanged, FCForm objName, string strSaveRoutine, params object[] args)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// have any rows been altered?
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Current changes have not been saved. Do so now?", "Clerk", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						// save all changes
						//FC:FINAL:DDU:#i2033 - call methods correct adding args params
						FCUtils.CallByName(objName, strSaveRoutine, CallType.Method, args);
					}
					else
					{
						modGNBas.Statics.boolNotSave = false;
					}
				}
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmDeaths, frmMarriages, frmClerkDefaults, frmDogWarrant, frmFacilityNames, frmRegistrarNames, frmPrinterSettings, frmBirthDelayed, frmBirths, frmBirthForeign)
		public static object ClearDirtyControls(FCForm FormName)
		{
			object ClearDirtyControls = null;
			/*? On Error Resume Next  *//* Control ControlName = new Control(); */
			foreach (Control ControlName in FCUtils.GetAllControls(FormName))
			{
				if (ControlName is FCTextBox)
				{
					(ControlName as FCTextBox).DataChanged = false;
				}
				else if (ControlName is FCComboBox)
				{
					(ControlName as FCComboBox).DataChanged = false;
				}
				else if (ControlName is Global.T2KDateBox)
				{
					(ControlName as T2KDateBox).DataChanged = false;
				}
				else if (ControlName is FCCheckBox)
				{
					(ControlName as FCCheckBox).DataChanged = false;
				}
				else if (ControlName is FCRadioButton)
				{
					(ControlName as FCRadioButton).DataChanged = false;
				}
			}
			return ClearDirtyControls;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmDeaths, frmMarriages, frmClerkDefaults, frmDogWarrant, frmFacilityNames, frmRegistrarNames, frmPrinterSettings, frmBirthDelayed, frmBirths, frmBirthForeign)
		// vbPorter upgrade warning: 'Return' As object	OnWrite
		public static int NumberDirtyControls(FCForm FormName)
		{
			int NumberDirtyControls = 0;
			/*? On Error Resume Next  *//* Control ControlName = new Control(); */
			foreach (Control ControlName in FCUtils.GetAllControls(FormName))
			{
				if (ControlName is FCTextBox)
				{
					if (!((ControlName as FCTextBox).DataChanged == true))
					{
					}
					else
					{
						NumberDirtyControls += 1;
					}
				}
				else if (ControlName is FCComboBox)
				{
					if (!((ControlName as FCComboBox).DataChanged == true))
					{
					}
					else
					{
						NumberDirtyControls += 1;
					}
				}
				else if (ControlName is Global.T2KDateBox)
				{
					if (!((ControlName as T2KDateBox).DataChanged == true))
					{
					}
					else
					{
						NumberDirtyControls += 1;
					}
				}
				else if (ControlName is FCCheckBox)
				{
					if (!((ControlName as FCCheckBox).DataChanged == true))
					{
					}
					else
					{
						NumberDirtyControls += 1;
					}
				}
				else if (ControlName is FCRadioButton)
				{
					if (!((ControlName as FCRadioButton).DataChanged == true))
					{
					}
					else
					{
						NumberDirtyControls += 1;
					}
				}
			}
			return NumberDirtyControls;
		}
	}
}
