//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmBirthForeign.
	/// </summary>
	partial class frmBirthForeign
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLabels;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblBirthCertNum;
		public fecherFoundation.FCCheckBox chkMarriageOnFile;
		public fecherFoundation.FCCheckBox chkDeceased;
		public fecherFoundation.FCTextBox txtAmendedInfo;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtChildState;
		public fecherFoundation.FCTextBox txtChildTown;
		public fecherFoundation.FCTextBox txtChildDOB;
		public fecherFoundation.FCTextBox txtChildFirstName;
		public fecherFoundation.FCTextBox txtChildLastName;
		public fecherFoundation.FCTextBox txtChildMI;
		public fecherFoundation.FCTextBox txtChildSex;
		public fecherFoundation.FCTextBox txtChildCountry;
		public Global.T2KDateBox txtActualDate;
		public fecherFoundation.FCLabel Label70;
		public fecherFoundation.FCLabel lblLabels_15;
		public fecherFoundation.FCLabel lblLabels_16;
		public fecherFoundation.FCLabel lblLabels_17;
		public fecherFoundation.FCLabel lblLabels_25;
		public fecherFoundation.FCLabel lblLabels_6;
		public fecherFoundation.FCLabel lblLabels_8;
		public fecherFoundation.FCLabel lblLabels_35;
		public fecherFoundation.FCLabel lblLabels_36;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtMotherDOB;
		public fecherFoundation.FCTextBox txtMothersStateOfBirth;
		public fecherFoundation.FCTextBox txtMotherFirstName;
		public fecherFoundation.FCTextBox txtMotherLastName;
		public fecherFoundation.FCTextBox txtMotherMI;
		public fecherFoundation.FCTextBox txtMotherMaidenName;
		public fecherFoundation.FCTextBox txtMothersAge;
		public fecherFoundation.FCLabel lblLabels_5;
		public fecherFoundation.FCLabel lblLabels_39;
		public fecherFoundation.FCLabel lblLabels_18;
		public fecherFoundation.FCLabel lblLabels_19;
		public fecherFoundation.FCLabel lblLabels_20;
		public fecherFoundation.FCLabel lblLabels_28;
		public fecherFoundation.FCLabel lblLabels_29;
        public fecherFoundation.FCFrame Frame2;
        public fecherFoundation.FCFrame Frame8;
		public fecherFoundation.FCTextBox txtFatherDOB;
		public fecherFoundation.FCTextBox txtFathersState;
		public fecherFoundation.FCTextBox txtFatherDesignation;
		public fecherFoundation.FCTextBox txtFatherFirstName;
		public fecherFoundation.FCTextBox txtFatherLastName;
		public fecherFoundation.FCTextBox txtFatherMI;
		public fecherFoundation.FCTextBox txtFathersAge;
		public fecherFoundation.FCLabel lblLabels_9;
		public fecherFoundation.FCLabel lblLabels_1;
		public fecherFoundation.FCLabel lblLabels_11;
		public fecherFoundation.FCLabel lblLabels_12;
		public fecherFoundation.FCLabel lblLabels_13;
		public fecherFoundation.FCLabel lblLabels_14;
		public fecherFoundation.FCLabel lblLabels_30;
		public fecherFoundation.FCTextBox txtMothersState;
		public fecherFoundation.FCTextBox txtMothersTown;
		public fecherFoundation.FCTextBox txtMothersStreet;
		public fecherFoundation.FCLabel lblLabels_2;
		public fecherFoundation.FCLabel lblLabels_3;
		public fecherFoundation.FCLabel lblLabels_4;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCTextBox txtAttestDate;
		public fecherFoundation.FCTextBox txtClerkCityTown;
		public fecherFoundation.FCTextBox txtClerkName;
		public fecherFoundation.FCButton cmdFind;
		public fecherFoundation.FCLabel lblLabels_0;
		public fecherFoundation.FCLabel lblLabels_7;
		public fecherFoundation.FCLabel lblLabels_21;
		public fecherFoundation.FCCheckBox chkRequired;
		public fecherFoundation.FCTextBox txtFileNumber;
		public fecherFoundation.FCCheckBox chkIllegitBirth;
		public FCCommonDialog cdlColor;
		public fecherFoundation.FCTextBox txtBirthCertNum;
		public Global.T2KDateBox mebDateDeceased;
		public FCGrid gridTownCode;
		public fecherFoundation.FCLabel lblBKey;
		public fecherFoundation.FCLabel lblBirthID;
		public fecherFoundation.FCLabel lblRecord;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessAddNewRecord;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDeleteRecord;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSetDefaultTowns;
		public fecherFoundation.FCToolStripMenuItem mnuRegistrarNames;
		public fecherFoundation.FCToolStripMenuItem mnuDefaultState;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		public fecherFoundation.FCLabel lblDeceased;
		public fecherFoundation.FCLabel lblLabels_42;
		public fecherFoundation.FCLabel lblBirthCertNum_28;
		public fecherFoundation.FCLabel lblFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBirthForeign));
			this.chkMarriageOnFile = new fecherFoundation.FCCheckBox();
			this.chkDeceased = new fecherFoundation.FCCheckBox();
			this.txtAmendedInfo = new fecherFoundation.FCTextBox();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtChildState = new fecherFoundation.FCTextBox();
			this.txtChildTown = new fecherFoundation.FCTextBox();
			this.txtChildDOB = new fecherFoundation.FCTextBox();
			this.txtChildFirstName = new fecherFoundation.FCTextBox();
			this.txtChildLastName = new fecherFoundation.FCTextBox();
			this.txtChildMI = new fecherFoundation.FCTextBox();
			this.txtChildSex = new fecherFoundation.FCTextBox();
			this.txtChildCountry = new fecherFoundation.FCTextBox();
			this.txtActualDate = new Global.T2KDateBox();
			this.Label70 = new fecherFoundation.FCLabel();
			this.lblLabels_15 = new fecherFoundation.FCLabel();
			this.lblLabels_16 = new fecherFoundation.FCLabel();
			this.lblLabels_17 = new fecherFoundation.FCLabel();
			this.lblLabels_25 = new fecherFoundation.FCLabel();
			this.lblLabels_6 = new fecherFoundation.FCLabel();
			this.lblLabels_8 = new fecherFoundation.FCLabel();
			this.lblLabels_35 = new fecherFoundation.FCLabel();
			this.lblLabels_36 = new fecherFoundation.FCLabel();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.Frame7 = new fecherFoundation.FCFrame();
			this.txtMotherDOB = new fecherFoundation.FCTextBox();
			this.txtMothersStateOfBirth = new fecherFoundation.FCTextBox();
			this.txtMotherFirstName = new fecherFoundation.FCTextBox();
			this.txtMotherLastName = new fecherFoundation.FCTextBox();
			this.txtMotherMI = new fecherFoundation.FCTextBox();
			this.txtMotherMaidenName = new fecherFoundation.FCTextBox();
			this.txtMothersAge = new fecherFoundation.FCTextBox();
			this.lblLabels_5 = new fecherFoundation.FCLabel();
			this.lblLabels_39 = new fecherFoundation.FCLabel();
			this.lblLabels_18 = new fecherFoundation.FCLabel();
			this.lblLabels_19 = new fecherFoundation.FCLabel();
			this.lblLabels_20 = new fecherFoundation.FCLabel();
			this.lblLabels_28 = new fecherFoundation.FCLabel();
			this.lblLabels_29 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.Frame8 = new fecherFoundation.FCFrame();
			this.txtFatherDOB = new fecherFoundation.FCTextBox();
			this.txtFathersState = new fecherFoundation.FCTextBox();
			this.txtFatherDesignation = new fecherFoundation.FCTextBox();
			this.txtFatherFirstName = new fecherFoundation.FCTextBox();
			this.txtFatherLastName = new fecherFoundation.FCTextBox();
			this.txtFatherMI = new fecherFoundation.FCTextBox();
			this.txtFathersAge = new fecherFoundation.FCTextBox();
			this.lblLabels_9 = new fecherFoundation.FCLabel();
			this.lblLabels_1 = new fecherFoundation.FCLabel();
			this.lblLabels_11 = new fecherFoundation.FCLabel();
			this.lblLabels_12 = new fecherFoundation.FCLabel();
			this.lblLabels_13 = new fecherFoundation.FCLabel();
			this.lblLabels_14 = new fecherFoundation.FCLabel();
			this.lblLabels_30 = new fecherFoundation.FCLabel();
			this.txtMothersState = new fecherFoundation.FCTextBox();
			this.txtMothersTown = new fecherFoundation.FCTextBox();
			this.txtMothersStreet = new fecherFoundation.FCTextBox();
			this.lblLabels_2 = new fecherFoundation.FCLabel();
			this.lblLabels_3 = new fecherFoundation.FCLabel();
			this.lblLabels_4 = new fecherFoundation.FCLabel();
			this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
			this.Frame9 = new fecherFoundation.FCFrame();
			this.txtAttestDate = new fecherFoundation.FCTextBox();
			this.txtClerkCityTown = new fecherFoundation.FCTextBox();
			this.txtClerkName = new fecherFoundation.FCTextBox();
			this.cmdFind = new fecherFoundation.FCButton();
			this.lblLabels_0 = new fecherFoundation.FCLabel();
			this.lblLabels_7 = new fecherFoundation.FCLabel();
			this.lblLabels_21 = new fecherFoundation.FCLabel();
			this.chkRequired = new fecherFoundation.FCCheckBox();
			this.txtFileNumber = new fecherFoundation.FCTextBox();
			this.chkIllegitBirth = new fecherFoundation.FCCheckBox();
			this.cdlColor = new fecherFoundation.FCCommonDialog();
			this.txtBirthCertNum = new fecherFoundation.FCTextBox();
			this.mebDateDeceased = new Global.T2KDateBox();
			this.gridTownCode = new fecherFoundation.FCGrid();
			this.lblBKey = new fecherFoundation.FCLabel();
			this.lblBirthID = new fecherFoundation.FCLabel();
			this.lblRecord = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessAddNewRecord = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessDeleteRecord = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSetDefaultTowns = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRegistrarNames = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDefaultState = new fecherFoundation.FCToolStripMenuItem();
			this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.lblDeceased = new fecherFoundation.FCLabel();
			this.lblLabels_42 = new fecherFoundation.FCLabel();
			this.lblBirthCertNum_28 = new fecherFoundation.FCLabel();
			this.lblFile = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdRegistrarNames = new fecherFoundation.FCButton();
			this.cmdComment = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).BeginInit();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).BeginInit();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
			this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).BeginInit();
			this.Frame8.SuspendLayout();
			this.SSTab1_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
			this.Frame9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFind)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRequired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIllegitBirth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mebDateDeceased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRegistrarNames)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 592);
			this.BottomPanel.Size = new System.Drawing.Size(1088, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkMarriageOnFile);
			this.ClientArea.Controls.Add(this.chkDeceased);
			this.ClientArea.Controls.Add(this.txtAmendedInfo);
			this.ClientArea.Controls.Add(this.SSTab1);
			this.ClientArea.Controls.Add(this.chkRequired);
			this.ClientArea.Controls.Add(this.txtFileNumber);
			this.ClientArea.Controls.Add(this.chkIllegitBirth);
			this.ClientArea.Controls.Add(this.txtBirthCertNum);
			this.ClientArea.Controls.Add(this.mebDateDeceased);
			this.ClientArea.Controls.Add(this.gridTownCode);
			this.ClientArea.Controls.Add(this.lblBKey);
			this.ClientArea.Controls.Add(this.lblBirthID);
			this.ClientArea.Controls.Add(this.lblRecord);
			this.ClientArea.Controls.Add(this.lblDeceased);
			this.ClientArea.Controls.Add(this.lblLabels_42);
			this.ClientArea.Controls.Add(this.lblBirthCertNum_28);
			this.ClientArea.Controls.Add(this.lblFile);
			this.ClientArea.Size = new System.Drawing.Size(1088, 532);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdComment);
			this.TopPanel.Controls.Add(this.cmdRegistrarNames);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Size = new System.Drawing.Size(1088, 60);
			this.TopPanel.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdRegistrarNames, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(165, 30);
			this.HeaderText.Text = "Foreign Births";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// chkMarriageOnFile
			// 
			this.chkMarriageOnFile.Location = new System.Drawing.Point(330, 143);
			this.chkMarriageOnFile.Name = "chkMarriageOnFile";
			this.chkMarriageOnFile.Size = new System.Drawing.Size(191, 27);
			this.chkMarriageOnFile.TabIndex = 5;
			this.chkMarriageOnFile.Text = "Marriage record on file";
			this.ToolTip1.SetToolTip(this.chkMarriageOnFile, null);
			// 
			// chkDeceased
			// 
			this.chkDeceased.Location = new System.Drawing.Point(330, 106);
			this.chkDeceased.Name = "chkDeceased";
			this.chkDeceased.Size = new System.Drawing.Size(264, 27);
			this.chkDeceased.TabIndex = 4;
			this.chkDeceased.Text = "Death record is filed in this office";
			this.ToolTip1.SetToolTip(this.chkDeceased, null);
			this.chkDeceased.CheckedChanged += new System.EventHandler(this.chkDeceased_CheckedChanged);
			// 
			// txtAmendedInfo
			// 
			this.txtAmendedInfo.BackColor = System.Drawing.SystemColors.Window;
			this.txtAmendedInfo.Location = new System.Drawing.Point(30, 211);
			this.txtAmendedInfo.Multiline = true;
			this.txtAmendedInfo.Name = "txtAmendedInfo";
			this.txtAmendedInfo.ScrollBars = Wisej.Web.ScrollBars.Vertical;
			this.txtAmendedInfo.Size = new System.Drawing.Size(1008, 54);
			this.txtAmendedInfo.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtAmendedInfo, null);
			// 
			// SSTab1
			// 
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Controls.Add(this.SSTab1_Page3);
			this.SSTab1.Location = new System.Drawing.Point(30, 283);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
			this.SSTab1.Size = new System.Drawing.Size(1008, 660);
			this.SSTab1.TabIndex = 100;
            this.SSTab1.TabStop = false;
            this.ToolTip1.SetToolTip(this.SSTab1, null);
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.Controls.Add(this.Frame1);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Text = "Child Information";
			this.ToolTip1.SetToolTip(this.SSTab1_Page1, null);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtChildState);
			this.Frame1.Controls.Add(this.txtChildTown);
			this.Frame1.Controls.Add(this.txtChildDOB);
			this.Frame1.Controls.Add(this.txtChildFirstName);
			this.Frame1.Controls.Add(this.txtChildLastName);
			this.Frame1.Controls.Add(this.txtChildMI);
			this.Frame1.Controls.Add(this.txtChildSex);
			this.Frame1.Controls.Add(this.txtChildCountry);
			this.Frame1.Controls.Add(this.txtActualDate);
			this.Frame1.Controls.Add(this.Label70);
			this.Frame1.Controls.Add(this.lblLabels_15);
			this.Frame1.Controls.Add(this.lblLabels_16);
			this.Frame1.Controls.Add(this.lblLabels_17);
			this.Frame1.Controls.Add(this.lblLabels_25);
			this.Frame1.Controls.Add(this.lblLabels_6);
			this.Frame1.Controls.Add(this.lblLabels_8);
			this.Frame1.Controls.Add(this.lblLabels_35);
			this.Frame1.Controls.Add(this.lblLabels_36);
			this.Frame1.Location = new System.Drawing.Point(20, 20);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(966, 208);
            this.Frame1.TabIndex = 7;
			this.Frame1.Text = "Child Information";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// txtChildState
			// 
			this.txtChildState.BackColor = System.Drawing.SystemColors.Window;
			this.txtChildState.Location = new System.Drawing.Point(220, 148);
			this.txtChildState.MaxLength = 20;
			this.txtChildState.Name = "txtChildState";
			this.txtChildState.Size = new System.Drawing.Size(350, 40);
			this.txtChildState.TabIndex = 15;
			this.txtChildState.Text = " ";
			this.ToolTip1.SetToolTip(this.txtChildState, null);
			this.txtChildState.DoubleClick += new System.EventHandler(this.txtChildState_DoubleClick);
			// 
			// txtChildTown
			// 
			this.txtChildTown.BackColor = System.Drawing.SystemColors.Window;
			this.txtChildTown.Location = new System.Drawing.Point(590, 148);
			this.txtChildTown.MaxLength = 20;
			this.txtChildTown.Name = "txtChildTown";
			this.txtChildTown.Size = new System.Drawing.Size(356, 40);
			this.txtChildTown.TabIndex = 17;
			this.txtChildTown.Text = " ";
			this.ToolTip1.SetToolTip(this.txtChildTown, null);
			this.txtChildTown.Enter += new System.EventHandler(this.txtChildTown_Enter);
			this.txtChildTown.DoubleClick += new System.EventHandler(this.txtChildTown_DoubleClick);
			// 
			// txtChildDOB
			// 
			this.txtChildDOB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtChildDOB.Location = new System.Drawing.Point(660, 60);
			this.txtChildDOB.Name = "txtChildDOB";
			this.txtChildDOB.Size = new System.Drawing.Size(153, 40);
			this.txtChildDOB.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtChildDOB, null);
			this.txtChildDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtChildDOB_Validating);
			// 
			// txtChildFirstName
			// 
			this.txtChildFirstName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtChildFirstName.Location = new System.Drawing.Point(20, 60);
			this.txtChildFirstName.Name = "txtChildFirstName";
			this.txtChildFirstName.Size = new System.Drawing.Size(180, 40);
			this.txtChildFirstName.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtChildFirstName, null);
			// 
			// txtChildLastName
			// 
			this.txtChildLastName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtChildLastName.Location = new System.Drawing.Point(390, 60);
			this.txtChildLastName.Name = "txtChildLastName";
			this.txtChildLastName.Size = new System.Drawing.Size(180, 40);
			this.txtChildLastName.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtChildLastName, null);
			// 
			// txtChildMI
			// 
			this.txtChildMI.BackColor = System.Drawing.SystemColors.Window;
			this.txtChildMI.Location = new System.Drawing.Point(220, 60);
			this.txtChildMI.MaxLength = 20;
			this.txtChildMI.Name = "txtChildMI";
			this.txtChildMI.Size = new System.Drawing.Size(150, 40);
			this.txtChildMI.TabIndex = 2;
			this.txtChildMI.Text = " ";
			this.ToolTip1.SetToolTip(this.txtChildMI, null);
			// 
			// txtChildSex
			// 
			this.txtChildSex.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtChildSex.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtChildSex.Location = new System.Drawing.Point(590, 60);
			this.txtChildSex.MaxLength = 1;
			this.txtChildSex.Name = "txtChildSex";
			this.txtChildSex.Size = new System.Drawing.Size(50, 40);
			this.txtChildSex.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtChildSex, null);
			this.txtChildSex.Validating += new System.ComponentModel.CancelEventHandler(this.txtChildSex_Validating);
			// 
			// txtChildCountry
			// 
			this.txtChildCountry.BackColor = System.Drawing.SystemColors.Window;
			this.txtChildCountry.Location = new System.Drawing.Point(20, 148);
			this.txtChildCountry.MaxLength = 20;
			this.txtChildCountry.Name = "txtChildCountry";
			this.txtChildCountry.Size = new System.Drawing.Size(180, 40);
			this.txtChildCountry.TabIndex = 13;
			this.txtChildCountry.Text = " ";
			this.ToolTip1.SetToolTip(this.txtChildCountry, null);
			// 
			// txtActualDate
			// 
			this.txtActualDate.Location = new System.Drawing.Point(833, 60);
			this.txtActualDate.Mask = "##/##/####";
			this.txtActualDate.MaxLength = 10;
			this.txtActualDate.Name = "txtActualDate";
			this.txtActualDate.Size = new System.Drawing.Size(115, 40);
			this.txtActualDate.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtActualDate, null);
			// 
			// Label70
			// 
			this.Label70.Location = new System.Drawing.Point(833, 30);
			this.Label70.Name = "Label70";
			this.Label70.Size = new System.Drawing.Size(90, 16);
			this.Label70.Text = "ACTUAL DOB";
			this.ToolTip1.SetToolTip(this.Label70, null);
			// 
			// lblLabels_15
			// 
			this.lblLabels_15.AutoSize = true;
			this.lblLabels_15.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_15.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_15.Name = "lblLabels_15";
			this.lblLabels_15.Size = new System.Drawing.Size(82, 15);
			this.lblLabels_15.Text = "FIRST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_15, null);
			// 
			// lblLabels_16
			// 
			this.lblLabels_16.AutoSize = true;
			this.lblLabels_16.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_16.Location = new System.Drawing.Point(390, 30);
			this.lblLabels_16.Name = "lblLabels_16";
			this.lblLabels_16.Size = new System.Drawing.Size(77, 15);
			this.lblLabels_16.Text = "LAST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_16, null);
			// 
			// lblLabels_17
			// 
			this.lblLabels_17.AutoSize = true;
			this.lblLabels_17.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_17.Location = new System.Drawing.Point(220, 30);
			this.lblLabels_17.Name = "lblLabels_17";
			this.lblLabels_17.Size = new System.Drawing.Size(93, 15);
			this.lblLabels_17.Text = "MIDDLE NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_17, null);
			// 
			// lblLabels_25
			// 
			this.lblLabels_25.AutoSize = true;
			this.lblLabels_25.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_25.Location = new System.Drawing.Point(590, 30);
			this.lblLabels_25.Name = "lblLabels_25";
			this.lblLabels_25.Size = new System.Drawing.Size(32, 15);
			this.lblLabels_25.Text = "SEX";
			this.ToolTip1.SetToolTip(this.lblLabels_25, null);
			// 
			// lblLabels_6
			// 
			this.lblLabels_6.AutoSize = true;
			this.lblLabels_6.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_6.Location = new System.Drawing.Point(591, 120);
			this.lblLabels_6.Name = "lblLabels_6";
			this.lblLabels_6.Size = new System.Drawing.Size(180, 15);
			this.lblLabels_6.TabIndex = 16;
			this.lblLabels_6.Text = "CITY / TOWN / VILLAGE / ETC";
			this.ToolTip1.SetToolTip(this.lblLabels_6, null);
			// 
			// lblLabels_8
			// 
			this.lblLabels_8.AutoSize = true;
			this.lblLabels_8.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_8.Location = new System.Drawing.Point(660, 30);
			this.lblLabels_8.Name = "lblLabels_8";
			this.lblLabels_8.Size = new System.Drawing.Size(140, 15);
			this.lblLabels_8.Text = "DOB ON CERTIFICATE";
			this.ToolTip1.SetToolTip(this.lblLabels_8, null);
			// 
			// lblLabels_35
			// 
			this.lblLabels_35.AutoSize = true;
			this.lblLabels_35.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_35.Location = new System.Drawing.Point(20, 120);
			this.lblLabels_35.Name = "lblLabels_35";
			this.lblLabels_35.Size = new System.Drawing.Size(129, 15);
			this.lblLabels_35.TabIndex = 12;
			this.lblLabels_35.Text = "COUNTRY OF BIRTH";
			this.ToolTip1.SetToolTip(this.lblLabels_35, null);
			// 
			// lblLabels_36
			// 
			this.lblLabels_36.AutoSize = true;
			this.lblLabels_36.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_36.Location = new System.Drawing.Point(221, 120);
			this.lblLabels_36.Name = "lblLabels_36";
			this.lblLabels_36.Size = new System.Drawing.Size(157, 15);
			this.lblLabels_36.Text = "STATE / PROVINCE / ETC";
			this.ToolTip1.SetToolTip(this.lblLabels_36, null);
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.Controls.Add(this.Frame7);
            this.SSTab1_Page2.Controls.Add(this.Frame2);
            this.SSTab1_Page2.Controls.Add(this.Frame8);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Text = "Parent Information";
			this.ToolTip1.SetToolTip(this.SSTab1_Page2, null);
			// 
			// Frame7
			// 
			this.Frame7.Controls.Add(this.txtMotherDOB);
			this.Frame7.Controls.Add(this.txtMothersStateOfBirth);
			this.Frame7.Controls.Add(this.txtMotherFirstName);
			this.Frame7.Controls.Add(this.txtMotherLastName);
			this.Frame7.Controls.Add(this.txtMotherMI);
			this.Frame7.Controls.Add(this.txtMotherMaidenName);
			this.Frame7.Controls.Add(this.txtMothersAge);
			this.Frame7.Controls.Add(this.lblLabels_5);
			this.Frame7.Controls.Add(this.lblLabels_39);
			this.Frame7.Controls.Add(this.lblLabels_18);
			this.Frame7.Controls.Add(this.lblLabels_19);
			this.Frame7.Controls.Add(this.lblLabels_20);
			this.Frame7.Controls.Add(this.lblLabels_28);
			this.Frame7.Controls.Add(this.lblLabels_29);
			this.Frame7.Location = new System.Drawing.Point(20, 20);
			this.Frame7.Name = "Frame7";
			this.Frame7.Size = new System.Drawing.Size(966, 205);
            this.Frame7.TabIndex = 0;
			this.Frame7.Text = "Mother\'s Information";
			this.ToolTip1.SetToolTip(this.Frame7, null);
			// 
			// txtMotherDOB
			// 
			this.txtMotherDOB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMotherDOB.Location = new System.Drawing.Point(20, 146);
			this.txtMotherDOB.Name = "txtMotherDOB";
			this.txtMotherDOB.Size = new System.Drawing.Size(180, 40);
			this.txtMotherDOB.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtMotherDOB, null);
			this.txtMotherDOB.TextChanged += new System.EventHandler(this.txtMotherDOB_TextChanged);
			this.txtMotherDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtMotherDOB_Validating);
			// 
			// txtMothersStateOfBirth
			// 
			this.txtMothersStateOfBirth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMothersStateOfBirth.Location = new System.Drawing.Point(770, 59);
			this.txtMothersStateOfBirth.Name = "txtMothersStateOfBirth";
			this.txtMothersStateOfBirth.Size = new System.Drawing.Size(176, 40);
			this.txtMothersStateOfBirth.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtMothersStateOfBirth, null);
			this.txtMothersStateOfBirth.DoubleClick += new System.EventHandler(this.txtMothersStateOfBirth_DoubleClick);
			this.txtMothersStateOfBirth.TextChanged += new System.EventHandler(this.txtMothersStateOfBirth_TextChanged);
			// 
			// txtMotherFirstName
			// 
			this.txtMotherFirstName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMotherFirstName.Location = new System.Drawing.Point(20, 59);
			this.txtMotherFirstName.Name = "txtMotherFirstName";
			this.txtMotherFirstName.Size = new System.Drawing.Size(180, 40);
			this.txtMotherFirstName.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtMotherFirstName, null);
			this.txtMotherFirstName.Enter += new System.EventHandler(this.txtMotherFirstName_Enter);
			this.txtMotherFirstName.TextChanged += new System.EventHandler(this.txtMotherFirstName_TextChanged);
			// 
			// txtMotherLastName
			// 
			this.txtMotherLastName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMotherLastName.Location = new System.Drawing.Point(340, 59);
			this.txtMotherLastName.Name = "txtMotherLastName";
			this.txtMotherLastName.Size = new System.Drawing.Size(180, 40);
			this.txtMotherLastName.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtMotherLastName, null);
			this.txtMotherLastName.TextChanged += new System.EventHandler(this.txtMotherLastName_TextChanged);
			// 
			// txtMotherMI
			// 
			this.txtMotherMI.BackColor = System.Drawing.SystemColors.Window;
			this.txtMotherMI.Location = new System.Drawing.Point(220, 59);
			this.txtMotherMI.MaxLength = 40;
			this.txtMotherMI.Name = "txtMotherMI";
			this.txtMotherMI.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtMotherMI, null);
			this.txtMotherMI.TextChanged += new System.EventHandler(this.txtMotherMI_TextChanged);
			// 
			// txtMotherMaidenName
			// 
			this.txtMotherMaidenName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMotherMaidenName.Location = new System.Drawing.Point(540, 59);
			this.txtMotherMaidenName.Name = "txtMotherMaidenName";
			this.txtMotherMaidenName.Size = new System.Drawing.Size(140, 40);
			this.txtMotherMaidenName.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtMotherMaidenName, null);
			this.txtMotherMaidenName.TextChanged += new System.EventHandler(this.txtMotherMaidenName_TextChanged);
			// 
			// txtMothersAge
			// 
			this.txtMothersAge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMothersAge.Location = new System.Drawing.Point(700, 59);
			this.txtMothersAge.Name = "txtMothersAge";
			this.txtMothersAge.Size = new System.Drawing.Size(50, 40);
			this.txtMothersAge.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtMothersAge, null);
			this.txtMothersAge.TextChanged += new System.EventHandler(this.txtMothersAge_TextChanged);
			// 
			// lblLabels_5
			// 
			this.lblLabels_5.AutoSize = true;
			this.lblLabels_5.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_5.Location = new System.Drawing.Point(20, 119);
			this.lblLabels_5.Name = "lblLabels_5";
			this.lblLabels_5.Size = new System.Drawing.Size(101, 15);
			this.lblLabels_5.TabIndex = 12;
			this.lblLabels_5.Text = "DATE OF BIRTH";
			this.ToolTip1.SetToolTip(this.lblLabels_5, null);
			this.lblLabels_5.Click += new System.EventHandler(this.lblLabels_5_Click);
			// 
			// lblLabels_39
			// 
			this.lblLabels_39.AutoSize = true;
			this.lblLabels_39.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_39.Location = new System.Drawing.Point(770, 30);
			this.lblLabels_39.Name = "lblLabels_39";
			this.lblLabels_39.Size = new System.Drawing.Size(157, 15);
			this.lblLabels_39.TabIndex = 10;
			this.lblLabels_39.Text = "STATE / PROVINCE / ETC";
			this.ToolTip1.SetToolTip(this.lblLabels_39, null);
			this.lblLabels_39.Click += new System.EventHandler(this.lblLabels_39_Click);
			// 
			// lblLabels_18
			// 
			this.lblLabels_18.AutoSize = true;
			this.lblLabels_18.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_18.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_18.Name = "lblLabels_18";
			this.lblLabels_18.Size = new System.Drawing.Size(82, 15);
			this.lblLabels_18.TabIndex = 14;
			this.lblLabels_18.Text = "FIRST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_18, null);
			// 
			// lblLabels_19
			// 
			this.lblLabels_19.AutoSize = true;
			this.lblLabels_19.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_19.Location = new System.Drawing.Point(340, 30);
			this.lblLabels_19.Name = "lblLabels_19";
			this.lblLabels_19.Size = new System.Drawing.Size(77, 15);
			this.lblLabels_19.TabIndex = 4;
			this.lblLabels_19.Text = "LAST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_19, null);
			this.lblLabels_19.Click += new System.EventHandler(this.lblLabels_19_Click);
			// 
			// lblLabels_20
			// 
			this.lblLabels_20.AutoSize = true;
			this.lblLabels_20.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_20.Location = new System.Drawing.Point(220, 30);
			this.lblLabels_20.Name = "lblLabels_20";
			this.lblLabels_20.Size = new System.Drawing.Size(54, 15);
			this.lblLabels_20.TabIndex = 2;
			this.lblLabels_20.Text = "MIDDLE";
			this.ToolTip1.SetToolTip(this.lblLabels_20, null);
			this.lblLabels_20.Click += new System.EventHandler(this.lblLabels_20_Click);
			// 
			// lblLabels_28
			// 
			this.lblLabels_28.AutoSize = true;
			this.lblLabels_28.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_28.Location = new System.Drawing.Point(540, 30);
			this.lblLabels_28.Name = "lblLabels_28";
			this.lblLabels_28.Size = new System.Drawing.Size(95, 15);
			this.lblLabels_28.TabIndex = 6;
			this.lblLabels_28.Text = "MAIDEN NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_28, null);
			this.lblLabels_28.Click += new System.EventHandler(this.lblLabels_28_Click);
			// 
			// lblLabels_29
			// 
			this.lblLabels_29.AutoSize = true;
			this.lblLabels_29.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_29.Location = new System.Drawing.Point(700, 30);
			this.lblLabels_29.Name = "lblLabels_29";
			this.lblLabels_29.Size = new System.Drawing.Size(34, 15);
			this.lblLabels_29.TabIndex = 8;
			this.lblLabels_29.Text = "AGE";
			this.ToolTip1.SetToolTip(this.lblLabels_29, null);
			this.lblLabels_29.Click += new System.EventHandler(this.lblLabels_29_Click);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtMothersState);
            this.Frame2.Controls.Add(this.txtMothersTown);
            this.Frame2.Controls.Add(this.txtMothersStreet);
            this.Frame2.Controls.Add(this.lblLabels_2);
            this.Frame2.Controls.Add(this.lblLabels_3);
            this.Frame2.Controls.Add(this.lblLabels_4);
            this.Frame2.Location = new System.Drawing.Point(20, 245);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(966, 120);
            this.Frame2.TabIndex = 1;
            this.Frame2.Text = "Usual Residence";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // Frame8
            // 
            this.Frame8.Controls.Add(this.txtFatherFirstName);
            this.Frame8.Controls.Add(this.txtFatherDOB);
			this.Frame8.Controls.Add(this.txtFathersState);
			this.Frame8.Controls.Add(this.txtFatherDesignation);
			this.Frame8.Controls.Add(this.txtFatherLastName);
			this.Frame8.Controls.Add(this.txtFatherMI);
			this.Frame8.Controls.Add(this.txtFathersAge);
			this.Frame8.Controls.Add(this.lblLabels_9);
			this.Frame8.Controls.Add(this.lblLabels_1);
			this.Frame8.Controls.Add(this.lblLabels_11);
			this.Frame8.Controls.Add(this.lblLabels_12);
			this.Frame8.Controls.Add(this.lblLabels_13);
			this.Frame8.Controls.Add(this.lblLabels_14);
			this.Frame8.Controls.Add(this.lblLabels_30);
			this.Frame8.Location = new System.Drawing.Point(20, 385);
			this.Frame8.Name = "Frame8";
			this.Frame8.Size = new System.Drawing.Size(966, 206);
			this.Frame8.TabIndex = 2;
			this.Frame8.Text = "Father\'s Information";
			this.ToolTip1.SetToolTip(this.Frame8, null);
			// 
			// txtFatherDOB
			// 
			this.txtFatherDOB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtFatherDOB.Location = new System.Drawing.Point(90, 146);
			this.txtFatherDOB.Name = "txtFatherDOB";
			this.txtFatherDOB.Size = new System.Drawing.Size(230, 40);
			this.txtFatherDOB.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtFatherDOB, null);
			this.txtFatherDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtFatherDOB_Validating);
			// 
			// txtFathersState
			// 
			this.txtFathersState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtFathersState.Location = new System.Drawing.Point(340, 146);
			this.txtFathersState.Name = "txtFathersState";
			this.txtFathersState.Size = new System.Drawing.Size(323, 40);
			this.txtFathersState.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtFathersState, null);
			this.txtFathersState.Enter += new System.EventHandler(this.txtFathersState_Enter);
			this.txtFathersState.DoubleClick += new System.EventHandler(this.txtFathersState_DoubleClick);
			// 
			// txtFatherDesignation
			// 
			this.txtFatherDesignation.BackColor = System.Drawing.SystemColors.Window;
			this.txtFatherDesignation.Location = new System.Drawing.Point(846, 60);
			this.txtFatherDesignation.Name = "txtFatherDesignation";
			this.txtFatherDesignation.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtFatherDesignation, null);
			// 
			// txtFatherFirstName
			// 
			this.txtFatherFirstName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFatherFirstName.Location = new System.Drawing.Point(20, 60);
			this.txtFatherFirstName.Name = "txtFatherFirstName";
			this.txtFatherFirstName.Size = new System.Drawing.Size(300, 40);
			this.txtFatherFirstName.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.txtFatherFirstName, null);
			// 
			// txtFatherLastName
			// 
			this.txtFatherLastName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFatherLastName.Location = new System.Drawing.Point(526, 60);
			this.txtFatherLastName.Name = "txtFatherLastName";
			this.txtFatherLastName.Size = new System.Drawing.Size(300, 40);
			this.txtFatherLastName.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtFatherLastName, null);
			// 
			// txtFatherMI
			// 
			this.txtFatherMI.BackColor = System.Drawing.SystemColors.Window;
			this.txtFatherMI.Location = new System.Drawing.Point(340, 60);
			this.txtFatherMI.MaxLength = 40;
			this.txtFatherMI.Name = "txtFatherMI";
			this.txtFatherMI.Size = new System.Drawing.Size(166, 40);
			this.txtFatherMI.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtFatherMI, null);
			// 
			// txtFathersAge
			// 
			this.txtFathersAge.BackColor = System.Drawing.SystemColors.Window;
			this.txtFathersAge.Location = new System.Drawing.Point(20, 146);
			this.txtFathersAge.Name = "txtFathersAge";
			this.txtFathersAge.Size = new System.Drawing.Size(50, 40);
			this.txtFathersAge.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtFathersAge, null);
			// 
			// lblLabels_9
			// 
			this.lblLabels_9.AutoSize = true;
			this.lblLabels_9.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_9.Location = new System.Drawing.Point(90, 118);
			this.lblLabels_9.Name = "lblLabels_9";
			this.lblLabels_9.Size = new System.Drawing.Size(101, 15);
			this.lblLabels_9.TabIndex = 10;
			this.lblLabels_9.Text = "DATE OF BIRTH";
			this.ToolTip1.SetToolTip(this.lblLabels_9, null);
			// 
			// lblLabels_1
			// 
			this.lblLabels_1.AutoSize = true;
			this.lblLabels_1.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_1.Location = new System.Drawing.Point(340, 118);
			this.lblLabels_1.Name = "lblLabels_1";
			this.lblLabels_1.Size = new System.Drawing.Size(157, 15);
			this.lblLabels_1.TabIndex = 12;
			this.lblLabels_1.Text = "STATE / PROVINCE / ETC";
			this.ToolTip1.SetToolTip(this.lblLabels_1, null);
			// 
			// lblLabels_11
			// 
			this.lblLabels_11.AutoSize = true;
			this.lblLabels_11.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_11.Location = new System.Drawing.Point(846, 30);
			this.lblLabels_11.Name = "lblLabels_11";
			this.lblLabels_11.Size = new System.Drawing.Size(50, 15);
			this.lblLabels_11.TabIndex = 6;
			this.lblLabels_11.Text = "JR / SR";
			this.ToolTip1.SetToolTip(this.lblLabels_11, null);
			// 
			// lblLabels_12
			// 
			this.lblLabels_12.AutoSize = true;
			this.lblLabels_12.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_12.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_12.Name = "lblLabels_12";
			this.lblLabels_12.Size = new System.Drawing.Size(82, 15);
			this.lblLabels_12.TabIndex = 14;
			this.lblLabels_12.Text = "FIRST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_12, null);
			// 
			// lblLabels_13
			// 
			this.lblLabels_13.AutoSize = true;
			this.lblLabels_13.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_13.Location = new System.Drawing.Point(526, 30);
			this.lblLabels_13.Name = "lblLabels_13";
			this.lblLabels_13.Size = new System.Drawing.Size(77, 15);
			this.lblLabels_13.TabIndex = 4;
			this.lblLabels_13.Text = "LAST NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_13, null);
			// 
			// lblLabels_14
			// 
			this.lblLabels_14.AutoSize = true;
			this.lblLabels_14.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_14.Location = new System.Drawing.Point(340, 30);
			this.lblLabels_14.Name = "lblLabels_14";
			this.lblLabels_14.Size = new System.Drawing.Size(54, 15);
			this.lblLabels_14.TabIndex = 2;
			this.lblLabels_14.Text = "MIDDLE";
			this.ToolTip1.SetToolTip(this.lblLabels_14, null);
			// 
			// lblLabels_30
			// 
			this.lblLabels_30.AutoSize = true;
			this.lblLabels_30.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_30.Location = new System.Drawing.Point(20, 118);
			this.lblLabels_30.Name = "lblLabels_30";
			this.lblLabels_30.Size = new System.Drawing.Size(34, 15);
			this.lblLabels_30.TabIndex = 8;
			this.lblLabels_30.Text = "AGE";
			this.ToolTip1.SetToolTip(this.lblLabels_30, null);
			
			// 
			// txtMothersState
			// 
			this.txtMothersState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMothersState.Location = new System.Drawing.Point(663, 60);
			this.txtMothersState.Name = "txtMothersState";
			this.txtMothersState.Size = new System.Drawing.Size(283, 40);
			this.txtMothersState.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtMothersState, null);
			this.txtMothersState.DoubleClick += new System.EventHandler(this.txtMothersState_DoubleClick);
			// 
			// txtMothersTown
			// 
			this.txtMothersTown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMothersTown.Location = new System.Drawing.Point(360, 60);
			this.txtMothersTown.Name = "txtMothersTown";
			this.txtMothersTown.Size = new System.Drawing.Size(283, 40);
			this.txtMothersTown.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtMothersTown, null);
			this.txtMothersTown.DoubleClick += new System.EventHandler(this.txtMothersTown_DoubleClick);
			// 
			// txtMothersStreet
			// 
			this.txtMothersStreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtMothersStreet.Location = new System.Drawing.Point(22, 60);
			this.txtMothersStreet.Name = "txtMothersStreet";
			this.txtMothersStreet.Size = new System.Drawing.Size(320, 40);
			this.txtMothersStreet.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtMothersStreet, null);
			// 
			// lblLabels_2
			// 
			this.lblLabels_2.AutoSize = true;
			this.lblLabels_2.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_2.Location = new System.Drawing.Point(20, 30);
			this.lblLabels_2.Name = "lblLabels_2";
			this.lblLabels_2.Size = new System.Drawing.Size(142, 15);
			this.lblLabels_2.TabIndex = 6;
			this.lblLabels_2.Text = "STREET AND NUMBER";
			this.ToolTip1.SetToolTip(this.lblLabels_2, null);
			// 
			// lblLabels_3
			// 
			this.lblLabels_3.AutoSize = true;
			this.lblLabels_3.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_3.Location = new System.Drawing.Point(360, 30);
			this.lblLabels_3.Name = "lblLabels_3";
			this.lblLabels_3.Size = new System.Drawing.Size(180, 15);
			this.lblLabels_3.TabIndex = 2;
			this.lblLabels_3.Text = "CITY / TOWN / VILLAGE / ETC";
			this.ToolTip1.SetToolTip(this.lblLabels_3, null);
			// 
			// lblLabels_4
			// 
			this.lblLabels_4.AutoSize = true;
			this.lblLabels_4.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_4.Location = new System.Drawing.Point(663, 30);
			this.lblLabels_4.Name = "lblLabels_4";
			this.lblLabels_4.Size = new System.Drawing.Size(157, 15);
			this.lblLabels_4.TabIndex = 4;
			this.lblLabels_4.Text = "STATE / PROVINCE / ETC";
			this.ToolTip1.SetToolTip(this.lblLabels_4, null);
			// 
			// SSTab1_Page3
			// 
			this.SSTab1_Page3.Controls.Add(this.Frame9);
			this.SSTab1_Page3.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page3.Name = "SSTab1_Page3";
			this.SSTab1_Page3.Text = "Attested By";
			this.ToolTip1.SetToolTip(this.SSTab1_Page3, null);
			// 
			// Frame9
			// 
			this.Frame9.Controls.Add(this.txtAttestDate);
			this.Frame9.Controls.Add(this.txtClerkCityTown);
			this.Frame9.Controls.Add(this.txtClerkName);
			this.Frame9.Controls.Add(this.cmdFind);
			this.Frame9.Controls.Add(this.lblLabels_0);
			this.Frame9.Controls.Add(this.lblLabels_7);
			this.Frame9.Controls.Add(this.lblLabels_21);
			this.Frame9.Location = new System.Drawing.Point(20, 20);
			this.Frame9.Name = "Frame9";
			this.Frame9.Size = new System.Drawing.Size(966, 119);
			this.Frame9.Text = "Attested By:";
			this.ToolTip1.SetToolTip(this.Frame9, null);
			// 
			// txtAttestDate
			// 
			this.txtAttestDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtAttestDate.Location = new System.Drawing.Point(720, 59);
			this.txtAttestDate.Name = "txtAttestDate";
			this.txtAttestDate.Size = new System.Drawing.Size(226, 40);
			this.txtAttestDate.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtAttestDate, null);
			this.txtAttestDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtAttestDate_Validating);
			// 
			// txtClerkCityTown
			// 
			this.txtClerkCityTown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtClerkCityTown.Location = new System.Drawing.Point(400, 59);
			this.txtClerkCityTown.Name = "txtClerkCityTown";
			this.txtClerkCityTown.Size = new System.Drawing.Size(300, 40);
			this.txtClerkCityTown.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtClerkCityTown, null);
			this.txtClerkCityTown.DoubleClick += new System.EventHandler(this.txtClerkCityTown_DoubleClick);
			this.txtClerkCityTown.TextChanged += new System.EventHandler(this.txtClerkCityTown_TextChanged);
			// 
			// txtClerkName
			// 
			this.txtClerkName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.txtClerkName.Location = new System.Drawing.Point(80, 59);
			this.txtClerkName.Name = "txtClerkName";
			this.txtClerkName.Size = new System.Drawing.Size(300, 40);
			this.txtClerkName.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.txtClerkName, null);
			this.txtClerkName.Enter += new System.EventHandler(this.txtClerkName_Enter);
			// 
			// cmdFind
			// 
			this.cmdFind.AppearanceKey = "actionButton";
			this.cmdFind.Image = ((System.Drawing.Image)(resources.GetObject("cmdFind.Image")));
			this.cmdFind.Location = new System.Drawing.Point(20, 59);
			this.cmdFind.Name = "cmdFind";
			this.cmdFind.Size = new System.Drawing.Size(40, 40);
			this.cmdFind.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.cmdFind, "Find Recording Clerk\'s Name");
			this.cmdFind.Enter += new System.EventHandler(this.cmdFind_Enter);
			this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
			// 
			// lblLabels_0
			// 
			this.lblLabels_0.AutoSize = true;
			this.lblLabels_0.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_0.Location = new System.Drawing.Point(720, 30);
			this.lblLabels_0.Name = "lblLabels_0";
			this.lblLabels_0.Size = new System.Drawing.Size(91, 15);
			this.lblLabels_0.TabIndex = 5;
			this.lblLabels_0.Text = "ATTEST DATE";
			this.ToolTip1.SetToolTip(this.lblLabels_0, null);
			// 
			// lblLabels_7
			// 
			this.lblLabels_7.AutoSize = true;
			this.lblLabels_7.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_7.Location = new System.Drawing.Point(400, 30);
			this.lblLabels_7.Name = "lblLabels_7";
			this.lblLabels_7.Size = new System.Drawing.Size(180, 15);
			this.lblLabels_7.TabIndex = 3;
			this.lblLabels_7.Text = "CITY / TOWN / VILLAGE / ETC";
			this.ToolTip1.SetToolTip(this.lblLabels_7, null);
			// 
			// lblLabels_21
			// 
			this.lblLabels_21.AutoSize = true;
			this.lblLabels_21.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_21.Location = new System.Drawing.Point(80, 30);
			this.lblLabels_21.Name = "lblLabels_21";
			this.lblLabels_21.Size = new System.Drawing.Size(43, 15);
			this.lblLabels_21.TabIndex = 1;
			this.lblLabels_21.Text = "NAME";
			this.ToolTip1.SetToolTip(this.lblLabels_21, null);
			// 
			// chkRequired
			// 
			this.chkRequired.Location = new System.Drawing.Point(330, 69);
			this.chkRequired.Name = "chkRequired";
			this.chkRequired.Size = new System.Drawing.Size(437, 27);
			this.chkRequired.TabIndex = 3;
			this.chkRequired.Text = "Record contains older data. Not all fields will be required";
			this.ToolTip1.SetToolTip(this.chkRequired, null);
			// 
			// txtFileNumber
			// 
			this.txtFileNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtFileNumber.Location = new System.Drawing.Point(100, 30);
			this.txtFileNumber.Name = "txtFileNumber";
			this.txtFileNumber.Size = new System.Drawing.Size(200, 40);
			this.txtFileNumber.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtFileNumber, null);
			// 
			// chkIllegitBirth
			// 
			this.chkIllegitBirth.Location = new System.Drawing.Point(330, 32);
			this.chkIllegitBirth.Name = "chkIllegitBirth";
			this.chkIllegitBirth.Size = new System.Drawing.Size(291, 27);
			this.chkIllegitBirth.TabIndex = 2;
			this.chkIllegitBirth.Text = "Check this box if this birth was BOW";
			this.ToolTip1.SetToolTip(this.chkIllegitBirth, null);
			this.chkIllegitBirth.CheckedChanged += new System.EventHandler(this.chkIllegitBirth_CheckedChanged);
			// 
			// cdlColor
			// 
			this.cdlColor.Name = "cdlColor";
			this.cdlColor.Size = new System.Drawing.Size(0, 0);
			this.ToolTip1.SetToolTip(this.cdlColor, null);
			// 
			// txtBirthCertNum
			// 
			this.txtBirthCertNum.Appearance = 0;
			this.txtBirthCertNum.BackColor = System.Drawing.SystemColors.Info;
			this.txtBirthCertNum.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtBirthCertNum.Location = new System.Drawing.Point(183, 283);
			this.txtBirthCertNum.Name = "txtBirthCertNum";
			this.txtBirthCertNum.Size = new System.Drawing.Size(113, 40);
			this.txtBirthCertNum.TabIndex = 41;
			this.ToolTip1.SetToolTip(this.txtBirthCertNum, null);
			this.txtBirthCertNum.Visible = false;
			// 
			// mebDateDeceased
			// 
			this.mebDateDeceased.Location = new System.Drawing.Point(681, 106);
			this.mebDateDeceased.Mask = "##/##/####";
			this.mebDateDeceased.Name = "mebDateDeceased";
			this.mebDateDeceased.Size = new System.Drawing.Size(115, 40);
			this.mebDateDeceased.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.mebDateDeceased, null);
			this.mebDateDeceased.Visible = false;
			// 
			// gridTownCode
			// 
			this.gridTownCode.ColumnHeadersVisible = false;
			this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTownCode.ExtendLastCol = true;
			this.gridTownCode.FixedCols = 0;
			this.gridTownCode.FixedRows = 0;
			this.gridTownCode.Location = new System.Drawing.Point(30, 90);
			this.gridTownCode.Name = "gridTownCode";
			this.gridTownCode.ReadOnly = false;
			this.gridTownCode.RowHeadersVisible = false;
			this.gridTownCode.Rows = 1;
			this.gridTownCode.Size = new System.Drawing.Size(266, 41);
			this.gridTownCode.TabIndex = 1;
			this.gridTownCode.Tag = "land";
			this.ToolTip1.SetToolTip(this.gridTownCode, null);
			// 
			// lblBKey
			// 
			this.lblBKey.Location = new System.Drawing.Point(115, 343);
			this.lblBKey.Name = "lblBKey";
			this.lblBKey.Size = new System.Drawing.Size(112, 17);
			this.lblBKey.TabIndex = 40;
			this.ToolTip1.SetToolTip(this.lblBKey, null);
			this.lblBKey.Visible = false;
			// 
			// lblBirthID
			// 
			this.lblBirthID.Location = new System.Drawing.Point(30, 343);
			this.lblBirthID.Name = "lblBirthID";
			this.lblBirthID.Size = new System.Drawing.Size(60, 17);
			this.lblBirthID.TabIndex = 39;
			this.lblBirthID.Text = "BIRTH ID";
			this.ToolTip1.SetToolTip(this.lblBirthID, null);
			this.lblBirthID.Visible = false;
			// 
			// lblRecord
			// 
			this.lblRecord.Location = new System.Drawing.Point(30, 297);
			this.lblRecord.Name = "lblRecord";
			this.lblRecord.Size = new System.Drawing.Size(79, 13);
			this.lblRecord.TabIndex = 38;
			this.lblRecord.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRecord, null);
			this.lblRecord.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessAddNewRecord,
            this.mnuProcessDeleteRecord,
            this.mnuSP2,
            this.mnuSetDefaultTowns,
            this.mnuRegistrarNames,
            this.mnuDefaultState,
            this.mnuComment,
            this.mnuSP21,
            this.mnuProcessSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessAddNewRecord
			// 
			this.mnuProcessAddNewRecord.Index = 0;
			this.mnuProcessAddNewRecord.Name = "mnuProcessAddNewRecord";
			this.mnuProcessAddNewRecord.Text = "New";
			this.mnuProcessAddNewRecord.Click += new System.EventHandler(this.mnuProcessAddNewRecord_Click);
			// 
			// mnuProcessDeleteRecord
			// 
			this.mnuProcessDeleteRecord.Index = 1;
			this.mnuProcessDeleteRecord.Name = "mnuProcessDeleteRecord";
			this.mnuProcessDeleteRecord.Text = "Delete ";
			this.mnuProcessDeleteRecord.Click += new System.EventHandler(this.mnuProcessDeleteRecord_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 2;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuSetDefaultTowns
			// 
			this.mnuSetDefaultTowns.Index = 3;
			this.mnuSetDefaultTowns.Name = "mnuSetDefaultTowns";
			this.mnuSetDefaultTowns.Text = "Set Default Towns";
			this.mnuSetDefaultTowns.Visible = false;
			// 
			// mnuRegistrarNames
			// 
			this.mnuRegistrarNames.Index = 4;
			this.mnuRegistrarNames.Name = "mnuRegistrarNames";
			this.mnuRegistrarNames.Text = "Add / Edit Clerk Names";
			this.mnuRegistrarNames.Click += new System.EventHandler(this.mnuRegistrarNames_Click);
			// 
			// mnuDefaultState
			// 
			this.mnuDefaultState.Index = 5;
			this.mnuDefaultState.Name = "mnuDefaultState";
			this.mnuDefaultState.Text = "Set Default State";
			this.mnuDefaultState.Visible = false;
			// 
			// mnuComment
			// 
			this.mnuComment.Index = 6;
			this.mnuComment.Name = "mnuComment";
			this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuComment.Text = "Comment";
			this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
			// 
			// mnuSP21
			// 
			this.mnuSP21.Index = 7;
			this.mnuSP21.Name = "mnuSP21";
			this.mnuSP21.Text = "-";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 8;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuProcessSave.Text = "Save                               ";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 9;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 10;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 11;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// lblDeceased
			// 
			this.lblDeceased.AutoSize = true;
			this.lblDeceased.BackColor = System.Drawing.Color.Transparent;
			this.lblDeceased.Location = new System.Drawing.Point(607, 120);
			this.lblDeceased.Name = "lblDeceased";
			this.lblDeceased.Size = new System.Drawing.Size(40, 15);
			this.lblDeceased.TabIndex = 5;
			this.lblDeceased.Text = "DATE ";
			this.ToolTip1.SetToolTip(this.lblDeceased, null);
			this.lblDeceased.Visible = false;
			// 
			// lblLabels_42
			// 
			this.lblLabels_42.AutoSize = true;
			this.lblLabels_42.BackColor = System.Drawing.Color.Transparent;
			this.lblLabels_42.Location = new System.Drawing.Point(30, 188);
			this.lblLabels_42.Name = "lblLabels_42";
			this.lblLabels_42.Size = new System.Drawing.Size(159, 15);
			this.lblLabels_42.TabIndex = 8;
			this.lblLabels_42.Text = "AMENDED INFORMATION";
			this.ToolTip1.SetToolTip(this.lblLabels_42, null);
			// 
			// lblBirthCertNum_28
			// 
			this.lblBirthCertNum_28.AutoSize = true;
			this.lblBirthCertNum_28.BackColor = System.Drawing.Color.Transparent;
			this.lblBirthCertNum_28.Location = new System.Drawing.Point(30, 297);
			this.lblBirthCertNum_28.Name = "lblBirthCertNum_28";
			this.lblBirthCertNum_28.Size = new System.Drawing.Size(138, 15);
			this.lblBirthCertNum_28.TabIndex = 42;
			this.lblBirthCertNum_28.Text = "BIRTH CERT NUMBER";
			this.ToolTip1.SetToolTip(this.lblBirthCertNum_28, null);
			this.lblBirthCertNum_28.Visible = false;
			// 
			// lblFile
			// 
			this.lblFile.AutoSize = true;
			this.lblFile.BackColor = System.Drawing.Color.Transparent;
			this.lblFile.Location = new System.Drawing.Point(30, 44);
			this.lblFile.Name = "lblFile";
			this.lblFile.Size = new System.Drawing.Size(44, 15);
			this.lblFile.TabIndex = 43;
			this.lblFile.Text = "FILE #";
			this.ToolTip1.SetToolTip(this.lblFile, null);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.Location = new System.Drawing.Point(707, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(45, 24);
			this.cmdNew.TabIndex = 1;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.mnuProcessAddNewRecord_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.Location = new System.Drawing.Point(758, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(55, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.mnuProcessDeleteRecord_Click);
			// 
			// cmdRegistrarNames
			// 
			this.cmdRegistrarNames.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdRegistrarNames.Location = new System.Drawing.Point(819, 29);
			this.cmdRegistrarNames.Name = "cmdRegistrarNames";
			this.cmdRegistrarNames.Size = new System.Drawing.Size(150, 24);
			this.cmdRegistrarNames.TabIndex = 3;
			this.cmdRegistrarNames.Text = "Add/Edit Clerk Names";
			this.cmdRegistrarNames.Click += new System.EventHandler(this.mnuRegistrarNames_Click);
			// 
			// cmdComment
			// 
			this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdComment.Location = new System.Drawing.Point(975, 29);
			this.cmdComment.Name = "cmdComment";
			this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdComment.Size = new System.Drawing.Size(75, 24);
			this.cmdComment.TabIndex = 4;
			this.cmdComment.Text = "Comment";
			this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(473, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmBirthForeign
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1088, 700);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmBirthForeign";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Foreign Births ";
			this.ToolTip1.SetToolTip(this, null);
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmBirthForeign_Load);
			this.Activated += new System.EventHandler(this.frmBirthForeign_Activated);
			this.Resize += new System.EventHandler(this.frmBirthForeign_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBirthForeign_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBirthForeign_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).EndInit();
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).EndInit();
			this.SSTab1_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
			this.Frame7.ResumeLayout(false);
			this.Frame7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).EndInit();
			this.Frame8.ResumeLayout(false);
			this.Frame8.PerformLayout();
			this.SSTab1_Page3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
			this.Frame9.ResumeLayout(false);
			this.Frame9.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFind)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRequired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIllegitBirth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mebDateDeceased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRegistrarNames)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdDelete;
		private FCButton cmdNew;
		private FCButton cmdRegistrarNames;
		private FCButton cmdComment;
		private FCButton cmdSave;
	}
}
