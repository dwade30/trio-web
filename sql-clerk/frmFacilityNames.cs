//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmFacilityNames : BaseForm
	{
		public frmFacilityNames()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			Label1 = new System.Collections.Generic.List<FCLabel>();
			Label1.AddControlArrayElement(Label1_1, 0);
			Label1.AddControlArrayElement(Label1_2, 1);
			Label1.AddControlArrayElement(Label1_3, 2);
			Label1.AddControlArrayElement(Label1_4, 3);
			Label1.AddControlArrayElement(Label1_5, 4);
			Label1.AddControlArrayElement(Label1_6, 5);
			Label1.AddControlArrayElement(Label1_7, 6);
			Label1.AddControlArrayElement(Label1_8, 7);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFacilityNames InstancePtr
		{
			get
			{
				return (frmFacilityNames)Sys.GetInstance(typeof(frmFacilityNames));
			}
		}

		protected frmFacilityNames _InstancePtr = null;
		//=========================================================
		// ***********************************************************************************************************************************************
		// SWAPPED THE NAME AND FACILITY FIELDS AS SOME TOWNS WERE INTERPERTING THE
		// NAME FIELD TO BE THE NAME OF THE PRACTITIONER AND NOT THE NAME OF THE
		// FACILITY. 02-06-2002 Matthew Larrabee
		// ***********************************************************************************************************************************************
		string strName;
		clsDRWrapper rsFacility = new clsDRWrapper();
		clsDRWrapper rsStates = new clsDRWrapper();
		private int intDataChanged;

		private void cboFacility_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intCounter;
			// corey 12/5/2005
			// strName = Split(cboFacility, ",")
			// cboFacility.DataChanged = False
			// intDataChanged = NumberDirtyControls(Me)
			// Call SaveChanges(intDataChanged, Me, "cmdSave_Click")
			strName = cboFacility.Text;
			if (cboFacility.SelectedIndex >= 0)
				txtLastName.Text = fecherFoundation.Strings.Trim(strName);
			// If cboFacility.ListIndex >= 0 Then txtFirstName = Trim(strName(1))
			if (cboFacility.SelectedIndex >= 0)
				cboFacility.Tag = cboFacility.ItemData(cboFacility.SelectedIndex);
			rsFacility.OpenRecordset("Select * from FacilityNames where ID = " + FCConvert.ToString(cboFacility.ItemData(cboFacility.SelectedIndex)), modGNBas.DEFAULTDATABASE);
			if (!rsFacility.EndOfFile())
			{
				txtAddress1.Text = fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address1") + " ");
				txtAddress2.Text = fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address2") + " ");
				txtCity.Text = fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("City") + " ");
				for (intCounter = 0; intCounter <= cboState.Items.Count - 1; intCounter++)
				{
					if (Strings.Left(cboState.Items[intCounter].ToString(), 2) == fecherFoundation.Strings.Trim(rsFacility.Get_Fields("State") + ""))
					{
						cboState.SelectedIndex = intCounter;
						break;
					}
				}
				txtZip.Text = fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Zip") + " ");
				txtFacility.Text = fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Facility") + " ");
				txtFacilityNumber.Text = fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Facilitynumber") + " ");
			}
			modDirtyForm.ClearDirtyControls(this);
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			if (FCConvert.ToInt32(cboFacility.Tag) > 0)
			{
				if (MessageBox.Show("Are you sure you wish to delete this Facility?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsFacility.Execute("Delete from FacilityNames where ID = " + cboFacility.Tag, modGNBas.DEFAULTDATABASE);
					modGlobalRoutines.ShowWaitMessage(this);
					modGlobalRoutines.GetAllFacility(ref cboFacility);
					if (cboFacility.Items.Count > 0)
						cboFacility.SelectedIndex = 0;
					cboFacility.Focus();
				}
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			txtFirstName.Text = string.Empty;
			txtLastName.Text = string.Empty;
			txtAddress1.Text = string.Empty;
			txtAddress2.Text = string.Empty;
			txtCity.Text = string.Empty;
			txtZip.Text = string.Empty;
			txtFacility.Text = string.Empty;
			txtFacilityNumber.Text = string.Empty;
			cboFacility.Tag = 0;
			txtFacilityNumber.Focus();
		}

		public void cmdNew_Click()
		{
			cmdNew_Click(cmdNew, new System.EventArgs());
		}

		private bool SaveData()
		{
			bool SaveData = false;
			string strState = "";
			SaveData = false;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (fecherFoundation.Strings.Trim(txtLastName.Text) == string.Empty)
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Invalid Facility Name.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				txtLastName.Focus();
				return SaveData;
			}
			if (cboState.SelectedIndex < 0)
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Invalid State", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				cboState.Focus();
				return SaveData;
			}
			if (cboState.SelectedIndex >= 0)
			{
				if (rsStates.FindFirstRecord("ID", cboState.ItemData(cboState.SelectedIndex)))
				{
					strState = FCConvert.ToString(rsStates.Get_Fields("state"));
				}
				else
				{
					strState = "";
				}
			}
			else
			{
				strState = "";
			}
			if (Conversion.Val(cboFacility.Tag) == 0)
			{
				rsFacility.OpenRecordset("SELECT * FROM FacilityNames", modGNBas.DEFAULTDATABASE);
				rsFacility.AddNew();
				rsFacility.Set_Fields("FirstName", modGlobalRoutines.FixQuote(txtFirstName.Text));
				rsFacility.Set_Fields("LastName", modGlobalRoutines.FixQuote(txtLastName.Text));
				rsFacility.Set_Fields("Address1", modGlobalRoutines.FixQuote(txtAddress1.Text));
				rsFacility.Set_Fields("Address2", modGlobalRoutines.FixQuote(txtAddress2.Text));
				rsFacility.Set_Fields("City", modGlobalRoutines.FixQuote(txtCity.Text));
				rsFacility.Set_Fields("State", strState);
				rsFacility.Set_Fields("Zip", modGlobalRoutines.FixQuote(txtZip.Text));
				rsFacility.Set_Fields("Facility", modGlobalRoutines.FixQuote(txtFacility.Text));
				rsFacility.Set_Fields("FacilityNumber", modGlobalRoutines.FixQuote(txtFacilityNumber.Text));
				rsFacility.Update();
				cboFacility.AddItem(rsFacility.Get_Fields_String("Lastname"));
				cboFacility.ItemData(cboFacility.NewIndex, FCConvert.ToInt32(rsFacility.Get_Fields_Int32("ID")));
				cboFacility.SelectedIndex = cboFacility.NewIndex;
				// Call rsFacility.Execute("Insert into FacilityNames (FirstName,LastName,Address1,Address2,City,State,Zip,Facility,FacilityNumber) VALUES ('"
				// & FixQuote(txtFirstName) & "','" & FixQuote(txtLastName)
				// & "','" & FixQuote(txtAddress1) & "','" & FixQuote(txtAddress2)
				// & "','" & FixQuote(txtCity) & "','" & strState
				// & "','" & FixQuote(txtZip) & "','" & FixQuote(txtFacility) & "','" & FixQuote(txtFacilityNumber) & "')", DEFAULTDATABASENAME)
			}
			else
			{
				rsFacility.OpenRecordset("SELECT * FROM FacilityNames WHERE ID = " + cboFacility.Tag, modGNBas.DEFAULTDATABASE);
				if (rsFacility.EndOfFile() != true && rsFacility.BeginningOfFile() != true)
				{
					rsFacility.Edit();
					rsFacility.Set_Fields("FirstName", modGlobalRoutines.FixQuote(txtFirstName.Text));
					rsFacility.Set_Fields("LastName", modGlobalRoutines.FixQuote(txtLastName.Text));
					rsFacility.Set_Fields("Address1", modGlobalRoutines.FixQuote(txtAddress1.Text));
					rsFacility.Set_Fields("Address2", modGlobalRoutines.FixQuote(txtAddress2.Text));
					rsFacility.Set_Fields("City", modGlobalRoutines.FixQuote(txtCity.Text));
					rsFacility.Set_Fields("State", strState);
					rsFacility.Set_Fields("Zip", modGlobalRoutines.FixQuote(txtZip.Text));
					rsFacility.Set_Fields("Facility", modGlobalRoutines.FixQuote(txtFacility.Text));
					rsFacility.Set_Fields("FacilityNumber", modGlobalRoutines.FixQuote(txtFacilityNumber.Text));
                    //FC:FINAL:AM:#3933 - update before changing cboFacility - this will trigger the SelectedIndexChanged event which will reset rsFacility
                    rsFacility.Update();
                    cboFacility.Items.RemoveAt(cboFacility.SelectedIndex);
					cboFacility.AddItem(rsFacility.Get_Fields_String("Lastname"));
					cboFacility.ItemData(cboFacility.NewIndex, FCConvert.ToInt32(rsFacility.Get_Fields_Int32("ID")));
					//rsFacility.Update();
					cboFacility.SelectedIndex = cboFacility.NewIndex;
				}
				// Call rsFacility.Execute("Update FacilityNames Set FirstName = '"
				// & FixQuote(txtFirstName) & "',LastName ='" & FixQuote(txtLastName) & "',"
				// & "Address1 ='" & FixQuote(txtAddress1) & "',"
				// & "Address2 ='" & FixQuote(txtAddress2) & "',"
				// & "city  ='" & FixQuote(txtCity) & "',"
				// & "state='" & strState & "',"
				// & "zip ='" & FixQuote(txtZip) & "',Facility='" & FixQuote(txtFacility) & "',FacilityNumber = '" & FixQuote(txtFacilityNumber) & "' where ID = " & cboFacility.Tag, DEFAULTDATABASENAME)
			}
			// Call cmdNew_Click
			SaveData = true;
			cboFacility.Focus();
			// Call ShowWaitMessage(Me)
			// Call GetAllFacility(cboFacility)
			// If cboFacility.ListCount > 0 Then cboFacility.ListIndex = 0
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			return SaveData;
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmFacilityNames_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmFacilityNames_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFacilityNames_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFacilityNames properties;
			//frmFacilityNames.ScaleWidth	= 5940;
			//frmFacilityNames.ScaleHeight	= 4515;
			//frmFacilityNames.LinkTopic	= "Form1";
			//frmFacilityNames.LockControls	= -1  'True;
			//End Unmaped Properties
			// used in frmBirths activate event
			//modGNBas.Statics.boolSetDefaultRegistrar = true;
			// corey 12/5/2005
			// Call LoadStates(cboState)
			rsStates.OpenRecordset("SELECT * FROM States order by state", "twck0000.vb1");
			cboState.Clear();
			while (!rsStates.EndOfFile())
			{
				cboState.AddItem(FCConvert.ToString(rsStates.Get_Fields("State")));
				cboState.ItemData(cboState.NewIndex, FCConvert.ToInt32(rsStates.Get_Fields_Int32("ID")));
				rsStates.MoveNext();
			}
			modGlobalRoutines.GetAllFacility(ref cboFacility);
			modDirtyForm.ClearDirtyControls(this);
			if (cboFacility.Items.Count > 0)
				cboFacility.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			intDataChanged = FCConvert.ToInt16(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "mnuSaveExit_Click");
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			cmdNew_Click();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				cmdExit_Click();
			}
		}
	}
}
