﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptOldVitalReport.
	/// </summary>
	partial class rptOldVitalReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptOldVitalReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNameOfClerk2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownOfClerk2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAttestBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAttestDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtToDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFromDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownOfClerk1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNameOfClerk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFileNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtFamilyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtChild1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateOfEvent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtParents1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtParents2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtParents3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtParents4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtChild2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtChild3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlaceOfEvent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlaceOfResidence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameOfClerk2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownOfClerk2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttestBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttestDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtToDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFromDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownOfClerk1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameOfClerk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFileNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFamilyName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChild1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfEvent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtParents1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtParents2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtParents3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtParents4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChild2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChild3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfEvent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfResidence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDate,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Field17,
				this.Field18,
				this.Field19,
				this.Field20,
				this.Field21,
				this.txtNameOfClerk2,
				this.txtTownOfClerk2,
				this.txtAttestBy,
				this.txtAttestDate,
				this.txtToDate,
				this.txtFromDate,
				this.txtTownOfClerk1,
				this.txtNameOfClerk,
				this.txtFileNumber,
				this.txtPageNumber,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4,
				this.Line6,
				this.Line7,
				this.Line8,
				this.Line9,
				this.Line10,
				this.Line11,
				this.txtFamilyName,
				this.txtChild1,
				this.txtDateOfEvent,
				this.txtParents1,
				this.txtParents2,
				this.txtParents3,
				this.txtParents4,
				this.txtChild2,
				this.txtChild3,
				this.txtPlaceOfEvent,
				this.txtPlaceOfResidence
			});
			this.PageHeader.Height = 8.875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 0.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Courier New\'; font-size: 12pt; text-align: center";
			this.txtDate.Text = null;
			this.txtDate.Top = 1.5625F;
			this.txtDate.Width = 7.25F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field1.Text = "The following information is found in ";
			this.Field1.Top = 2.125F;
			this.Field1.Width = 2.875F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 0.0625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field2.Text = "describe old book so you could find it again.) On page";
			this.Field2.Top = 2.375F;
			this.Field2.Width = 4.125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.4375F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field3.Text = "Family Name:";
			this.Field3.Top = 2.8125F;
			this.Field3.Width = 1.25F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 6.4375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field4.Text = "(adequately";
			this.Field4.Top = 2.125F;
			this.Field4.Width = 0.9375F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 4.625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field5.Text = "is found the following information:";
			this.Field5.Top = 2.375F;
			this.Field5.Width = 2.75F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 0.4375F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field6.Text = "Name of child:";
			this.Field6.Top = 3.1875F;
			this.Field6.Width = 1.25F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 0.4375F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field7.Text = "Date of birth, marriage, or death:";
			this.Field7.Top = 3.8125F;
			this.Field7.Width = 2.6875F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 0.4375F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field8.Text = "Name of parents:";
			this.Field8.Top = 4.125F;
			this.Field8.Width = 1.25F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 0.4375F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field9.Text = "Place of event:";
			this.Field9.Top = 5F;
			this.Field9.Width = 1.25F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 0.4375F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field10.Text = "Place of residence:";
			this.Field10.Top = 5.375F;
			this.Field10.Width = 1.5F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 0.0625F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field11.Text = "This record appears in sequence with other vital records and was recorded by the " + "town clerk whose";
			this.Field11.Top = 6.1875F;
			this.Field11.Width = 7.3125F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 0.0625F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field12.Text = "handwriting and signature appear in other town records. This person, ";
			this.Field12.Top = 6.4375F;
			this.Field12.Width = 5.1875F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 6.75F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field13.Text = "was the";
			this.Field13.Top = 6.4375F;
			this.Field13.Width = 0.625F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 0.0625F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field14.Text = "Town Clerk of ";
			this.Field14.Top = 6.6875F;
			this.Field14.Width = 1.125F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 3.8125F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field15.Text = "from";
			this.Field15.Top = 6.6875F;
			this.Field15.Width = 0.5F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 5.625F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field16.Text = "to";
			this.Field16.Top = 6.6875F;
			this.Field16.Width = 0.375F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.1875F;
			this.Field17.Left = 0.5625F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field17.Text = "ATTEST:";
			this.Field17.Top = 7.125F;
			this.Field17.Width = 0.625F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.1875F;
			this.Field18.Left = 4.5F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field18.Text = "Date:";
			this.Field18.Top = 7.125F;
			this.Field18.Width = 0.4375F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1875F;
			this.Field19.Left = 0.5625F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field19.Text = "Town Clerk of ";
			this.Field19.Top = 7.5F;
			this.Field19.Width = 1.125F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 3.5F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field20.Text = "Maine";
			this.Field20.Top = 7.5F;
			this.Field20.Width = 0.5625F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.1875F;
			this.Field21.Left = 4.0625F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Field21.Text = "Seal your signature onto the record";
			this.Field21.Top = 7.9375F;
			this.Field21.Width = 3.0625F;
			// 
			// txtNameOfClerk2
			// 
			this.txtNameOfClerk2.Height = 0.1875F;
			this.txtNameOfClerk2.Left = 4.0625F;
			this.txtNameOfClerk2.Name = "txtNameOfClerk2";
			this.txtNameOfClerk2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtNameOfClerk2.Text = null;
			this.txtNameOfClerk2.Top = 7.5F;
			this.txtNameOfClerk2.Width = 3.0625F;
			// 
			// txtTownOfClerk2
			// 
			this.txtTownOfClerk2.Height = 0.1875F;
			this.txtTownOfClerk2.Left = 1.6875F;
			this.txtTownOfClerk2.Name = "txtTownOfClerk2";
			this.txtTownOfClerk2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtTownOfClerk2.Text = null;
			this.txtTownOfClerk2.Top = 7.5F;
			this.txtTownOfClerk2.Width = 1.625F;
			// 
			// txtAttestBy
			// 
			this.txtAttestBy.Height = 0.1875F;
			this.txtAttestBy.Left = 1.1875F;
			this.txtAttestBy.Name = "txtAttestBy";
			this.txtAttestBy.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtAttestBy.Text = null;
			this.txtAttestBy.Top = 7.125F;
			this.txtAttestBy.Width = 3.25F;
			// 
			// txtAttestDate
			// 
			this.txtAttestDate.Height = 0.1875F;
			this.txtAttestDate.Left = 5.0625F;
			this.txtAttestDate.Name = "txtAttestDate";
			this.txtAttestDate.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtAttestDate.Text = null;
			this.txtAttestDate.Top = 7.125F;
			this.txtAttestDate.Width = 1.625F;
			// 
			// txtToDate
			// 
			this.txtToDate.Height = 0.1875F;
			this.txtToDate.Left = 5.9375F;
			this.txtToDate.Name = "txtToDate";
			this.txtToDate.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtToDate.Text = null;
			this.txtToDate.Top = 6.6875F;
			this.txtToDate.Width = 1.0625F;
			// 
			// txtFromDate
			// 
			this.txtFromDate.Height = 0.1875F;
			this.txtFromDate.Left = 4.3125F;
			this.txtFromDate.Name = "txtFromDate";
			this.txtFromDate.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtFromDate.Text = null;
			this.txtFromDate.Top = 6.6875F;
			this.txtFromDate.Width = 1.0625F;
			// 
			// txtTownOfClerk1
			// 
			this.txtTownOfClerk1.Height = 0.1875F;
			this.txtTownOfClerk1.Left = 1.25F;
			this.txtTownOfClerk1.Name = "txtTownOfClerk1";
			this.txtTownOfClerk1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtTownOfClerk1.Text = null;
			this.txtTownOfClerk1.Top = 6.6875F;
			this.txtTownOfClerk1.Width = 2.5F;
			// 
			// txtNameOfClerk
			// 
			this.txtNameOfClerk.Height = 0.1875F;
			this.txtNameOfClerk.Left = 5.25F;
			this.txtNameOfClerk.Name = "txtNameOfClerk";
			this.txtNameOfClerk.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtNameOfClerk.Text = null;
			this.txtNameOfClerk.Top = 6.4375F;
			this.txtNameOfClerk.Width = 1.4375F;
			// 
			// txtFileNumber
			// 
			this.txtFileNumber.Height = 0.1875F;
			this.txtFileNumber.Left = 2.9375F;
			this.txtFileNumber.Name = "txtFileNumber";
			this.txtFileNumber.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtFileNumber.Text = null;
			this.txtFileNumber.Top = 2.125F;
			this.txtFileNumber.Width = 3.5F;
			// 
			// txtPageNumber
			// 
			this.txtPageNumber.Height = 0.1875F;
			this.txtPageNumber.Left = 4.1875F;
			this.txtPageNumber.Name = "txtPageNumber";
			this.txtPageNumber.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtPageNumber.Text = null;
			this.txtPageNumber.Top = 2.375F;
			this.txtPageNumber.Width = 0.4375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.9375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 2.3125F;
			this.Line1.Width = 3.5F;
			this.Line1.X1 = 2.9375F;
			this.Line1.X2 = 6.4375F;
			this.Line1.Y1 = 2.3125F;
			this.Line1.Y2 = 2.3125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.1875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 2.5625F;
			this.Line2.Width = 0.4375F;
			this.Line2.X1 = 4.1875F;
			this.Line2.X2 = 4.625F;
			this.Line2.Y1 = 2.5625F;
			this.Line2.Y2 = 2.5625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 5.25F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 6.625F;
			this.Line3.Width = 1.4375F;
			this.Line3.X1 = 5.25F;
			this.Line3.X2 = 6.6875F;
			this.Line3.Y1 = 6.625F;
			this.Line3.Y2 = 6.625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 4.3125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 6.875F;
			this.Line4.Width = 1F;
			this.Line4.X1 = 4.3125F;
			this.Line4.X2 = 5.3125F;
			this.Line4.Y1 = 6.875F;
			this.Line4.Y2 = 6.875F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 5.9375F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 6.875F;
			this.Line6.Width = 1F;
			this.Line6.X1 = 5.9375F;
			this.Line6.X2 = 6.9375F;
			this.Line6.Y1 = 6.875F;
			this.Line6.Y2 = 6.875F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 5.0625F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 7.3125F;
			this.Line7.Width = 1.625F;
			this.Line7.X1 = 5.0625F;
			this.Line7.X2 = 6.6875F;
			this.Line7.Y1 = 7.3125F;
			this.Line7.Y2 = 7.3125F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 4.0625F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 7.6875F;
			this.Line8.Width = 3.0625F;
			this.Line8.X1 = 4.0625F;
			this.Line8.X2 = 7.125F;
			this.Line8.Y1 = 7.6875F;
			this.Line8.Y2 = 7.6875F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 1.25F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 6.875F;
			this.Line9.Width = 2.5F;
			this.Line9.X1 = 1.25F;
			this.Line9.X2 = 3.75F;
			this.Line9.Y1 = 6.875F;
			this.Line9.Y2 = 6.875F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 1.1875F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 7.3125F;
			this.Line10.Width = 3.25F;
			this.Line10.X1 = 1.1875F;
			this.Line10.X2 = 4.4375F;
			this.Line10.Y1 = 7.3125F;
			this.Line10.Y2 = 7.3125F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 1.6875F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 7.6875F;
			this.Line11.Width = 1.6875F;
			this.Line11.X1 = 1.6875F;
			this.Line11.X2 = 3.375F;
			this.Line11.Y1 = 7.6875F;
			this.Line11.Y2 = 7.6875F;
			// 
			// txtFamilyName
			// 
			this.txtFamilyName.Height = 0.1875F;
			this.txtFamilyName.Left = 2F;
			this.txtFamilyName.Name = "txtFamilyName";
			this.txtFamilyName.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtFamilyName.Text = null;
			this.txtFamilyName.Top = 2.8125F;
			this.txtFamilyName.Width = 3.8125F;
			// 
			// txtChild1
			// 
			this.txtChild1.Height = 0.1875F;
			this.txtChild1.Left = 2F;
			this.txtChild1.Name = "txtChild1";
			this.txtChild1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtChild1.Text = null;
			this.txtChild1.Top = 3.1875F;
			this.txtChild1.Width = 3.8125F;
			// 
			// txtDateOfEvent
			// 
			this.txtDateOfEvent.Height = 0.1875F;
			this.txtDateOfEvent.Left = 3.1875F;
			this.txtDateOfEvent.Name = "txtDateOfEvent";
			this.txtDateOfEvent.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtDateOfEvent.Text = null;
			this.txtDateOfEvent.Top = 3.8125F;
			this.txtDateOfEvent.Width = 2.125F;
			// 
			// txtParents1
			// 
			this.txtParents1.Height = 0.1875F;
			this.txtParents1.Left = 2F;
			this.txtParents1.Name = "txtParents1";
			this.txtParents1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtParents1.Text = null;
			this.txtParents1.Top = 4.125F;
			this.txtParents1.Width = 3.8125F;
			// 
			// txtParents2
			// 
			this.txtParents2.Height = 0.1875F;
			this.txtParents2.Left = 2F;
			this.txtParents2.Name = "txtParents2";
			this.txtParents2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtParents2.Text = null;
			this.txtParents2.Top = 4.3125F;
			this.txtParents2.Width = 3.8125F;
			// 
			// txtParents3
			// 
			this.txtParents3.Height = 0.1875F;
			this.txtParents3.Left = 2F;
			this.txtParents3.Name = "txtParents3";
			this.txtParents3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtParents3.Text = null;
			this.txtParents3.Top = 4.5F;
			this.txtParents3.Width = 3.8125F;
			// 
			// txtParents4
			// 
			this.txtParents4.Height = 0.1875F;
			this.txtParents4.Left = 2F;
			this.txtParents4.Name = "txtParents4";
			this.txtParents4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtParents4.Text = null;
			this.txtParents4.Top = 4.6875F;
			this.txtParents4.Width = 3.8125F;
			// 
			// txtChild2
			// 
			this.txtChild2.Height = 0.1875F;
			this.txtChild2.Left = 2F;
			this.txtChild2.Name = "txtChild2";
			this.txtChild2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtChild2.Text = null;
			this.txtChild2.Top = 3.375F;
			this.txtChild2.Width = 3.8125F;
			// 
			// txtChild3
			// 
			this.txtChild3.Height = 0.1875F;
			this.txtChild3.Left = 2F;
			this.txtChild3.Name = "txtChild3";
			this.txtChild3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtChild3.Text = null;
			this.txtChild3.Top = 3.5625F;
			this.txtChild3.Width = 3.8125F;
			// 
			// txtPlaceOfEvent
			// 
			this.txtPlaceOfEvent.Height = 0.1875F;
			this.txtPlaceOfEvent.Left = 2F;
			this.txtPlaceOfEvent.Name = "txtPlaceOfEvent";
			this.txtPlaceOfEvent.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtPlaceOfEvent.Text = null;
			this.txtPlaceOfEvent.Top = 5F;
			this.txtPlaceOfEvent.Width = 3.8125F;
			// 
			// txtPlaceOfResidence
			// 
			this.txtPlaceOfResidence.Height = 0.1875F;
			this.txtPlaceOfResidence.Left = 2F;
			this.txtPlaceOfResidence.Name = "txtPlaceOfResidence";
			this.txtPlaceOfResidence.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.txtPlaceOfResidence.Text = null;
			this.txtPlaceOfResidence.Top = 5.375F;
			this.txtPlaceOfResidence.Width = 3.8125F;
			// 
			// rptOldVitalReport
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameOfClerk2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownOfClerk2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttestBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttestDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtToDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFromDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownOfClerk1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNameOfClerk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFileNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFamilyName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChild1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfEvent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtParents1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtParents2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtParents3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtParents4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChild2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChild3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfEvent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfResidence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNameOfClerk2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownOfClerk2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttestBy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttestDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFromDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownOfClerk1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNameOfClerk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFileNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFamilyName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtChild1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateOfEvent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtParents1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtParents2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtParents3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtParents4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtChild2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtChild3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlaceOfEvent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlaceOfResidence;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
