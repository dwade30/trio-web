//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptLabelBirths.
	/// </summary>
	public partial class rptLabelBirths : BaseSectionReport
	{
		public rptLabelBirths()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Birth Certificate ";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLabelBirths InstancePtr
		{
			get
			{
				return (rptLabelBirths)Sys.GetInstance(typeof(rptLabelBirths));
			}
		}

		protected rptLabelBirths _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLabelBirths	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private double dblLineAdjust;
		private bool boolPrintTest;

		public void Init(bool boolTestPrint, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			if (boolTestPrint)
			{
				dblLineAdjust = dblAdjust;
			}
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted = this.Document.Printer.DeviceCopies;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// txtTown = MuniName
			// txtTime = Format(Time, "H:mm AM/PM")
			// txtDate = Format(Date, "MM/dd/yyyy")
			// txtPage = "Page " & Me.pageNumber
			// txtCaption = "CERTIFIED ABSTRACT OF A CERTIFICATE OF LIVE BIRTH"
			// txtCaption2 = "DEPARTMENT OF HUMAN SERVICES"
			// Dim rsData As New clsDRWrapper
			double dblLaserLineAdjustment3;
			int X;
			// Call SetFixedSizeReport(Me.VSFlexGrid1)
			// Call SetPrintProperties(Me)
			if (modClerkGeneral.Statics.utPrintInfo.DontShowBottomHeaders)
			{
				lblAttestedBy.Visible = false;
				lblCityOrTown.Visible = false;
				lblDateIssued.Visible = false;
			}
			// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
			// If rsData.EndOfFile Then
			// dblLaserLineAdjustment3 = 0
			// Else
			// dblLaserLineAdjustment3 = CDbl(Val(rsData.Fields("BirthAdjustment")))
			// End If
			dblLaserLineAdjustment3 = Conversion.Val(modRegistry.GetRegistryKey("BirthAdjustment", "CK"));
			if (boolPrintTest)
			{
				dblLaserLineAdjustment3 = dblLineAdjust;
			}
			for (X = 0; X <= this.Detail.Controls.Count - 1; X++)
			{
				this.Detail.Controls[X].Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment3) / 1440F;
				// ControlName.Top = ControlName.Top + (200 * dblLaserLineAdjustment3)
			}
			// X
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (boolPrinted)
			{
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
			}
			// frmPrintVitalRec.Show 
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */
			frmPrintVitalRec.InstancePtr.Hide();
			if (!boolPrintTest)
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MiddleName")))) == "")
				{
					Field1.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FirstName") + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("LastName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("Designation")));
				}
				else
				{
					Field1.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FirstName") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MiddleName")))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("LastName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("Designation")));
				}
				// If IsDate(rsBirthCertificate.Fields("DateOfBirth")) Then
				// Field2 = Format(rsBirthCertificate.Fields("DateOfBirth"), "MM/dd/yyyy")
				// Else
				// Field2.Text = rsBirthCertificate.Fields("DateOfBirthDescription")
				// End If
				Field2.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields("dateofbirth");
				Field3.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("Sex");
				// Field4 = GetDefaultTownName(rsBirthCertificate.Fields("BirthPlace"))
				Field4.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("ChildTown");
				Field5.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsFirstName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsMiddleInitial"))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantsLastName") + fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNAReturnComma(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantTitle") + ""))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantTitle") + "")));
				Field6.Text = "";
				// CheckForNA(rsBirthCertificate.Fields("AttendantTitle") & "")
				Field7.Text = FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AttendantAddress") + ""));
				Field8.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersFirstName") + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersMiddleName"))) + " " + modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MothersMaidenName");
				if (FCConvert.ToDouble(fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherState") + "")) == 0)
				{
					Field9.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherTown") + "");
				}
				else
				{
					Field9.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherTown") + "") + ", " + fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("MotherState") + "");
				}
				if (modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("legitimate") || modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("aop") || modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("CourtDeterminedPaternity"))
				{
					Field10.Text = FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersFirstName"))) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersMiddleInitial"))) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersLastName"))) + " " + FCConvert.ToString(modGlobalRoutines.CheckForNA(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("FathersDesignation")));
				}
				if (FCConvert.ToString(modGlobalRoutines.CheckForNA(Field10.Text)) == string.Empty)
					Field10.Text = "- - -";
				if (frmPrintVitalRec.InstancePtr.chkPrint.CheckState == Wisej.Web.CheckState.Unchecked)
					Field10.Text = "- - -";
				if (fecherFoundation.Strings.Trim(Field10.Text) == string.Empty)
					Field10.Text = "N/A";
				// Field11 = rsBirthCertificate.Fields("RecordingClerksFirstName")
				// & " " & CheckForNA(rsBirthCertificate.Fields("RecordingClerksMiddleInitial"))
				// & " " & rsBirthCertificate.Fields("RecordingClerksLastName")
				// 
				Field11.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("RegistrarName");
				Field12.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("ChildTown");
				if (Information.IsDate(modGNBas.Statics.rsBirthCertificate.Get_Fields("RegistrarDateFiled")) && Convert.ToDateTime(modGNBas.Statics.rsBirthCertificate.Get_Fields_DateTime("RegistrarDateFiled")).ToOADate() != 0)
				{
					Field13.Text = Strings.Format(modGNBas.Statics.rsBirthCertificate.Get_Fields_DateTime("RegistrarDateFiled"), "MM/dd/yyyy");
				}
				else
				{
					Field13.Text = modGNBas.Statics.rsBirthCertificate.Get_Fields_String("RegistrarDateFileddescription");
				}
				if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
				{
					Field14.Text = "";
				}
				else
				{
					Field14.Text = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				}
				Field15.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				Field16.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("TownOf") + " ");
				if (FCConvert.ToBoolean(modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("DeathRecordInOffice")))
				{
					txtDeceased.Text = txtDeceased.Tag + "  " + modGNBas.Statics.rsBirthCertificate.Get_Fields("DeceasedDate");
					txtDeceased.Visible = true;
				}
				else
				{
					txtDeceased.Visible = false;
				}
				txtAmended.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.rsBirthCertificate.Get_Fields_String("AmendedInfo") + "");
				if (FCConvert.ToBoolean(modGNBas.Statics.rsBirthCertificate.Get_Fields_Boolean("SoleParent")))
				{
					txtSoleParent.Text = "Sole Legal Legitimate Parent";
				}
				else
				{
					txtSoleParent.Text = string.Empty;
				}
			}
			else
			{
				Field10.Text = "First MI Last";
			}
		}

		
	}
}
