//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.ObjectModel;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.CentralDocuments;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Burials;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmBurialPermit : BaseForm, IView<IBurialViewModel>
    {
        private bool cancelling = true;
		public frmBurialPermit()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmBurialPermit(IBurialViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			chkPermitType = new System.Collections.Generic.List<FCCheckBox>();
			chkPermitType.AddControlArrayElement(chkPermitType_0, 0);
			chkPermitType.AddControlArrayElement(chkPermitType_1, 1);
			chkPermitType.AddControlArrayElement(chkPermitType_2, 2);
			chkPermitType.AddControlArrayElement(chkPermitType_3, 3);
			chkPermitType.AddControlArrayElement(chkPermitType_4, 4);
			chkPermitType.AddControlArrayElement(chkPermitType_5, 5);
			chkPermitType.AddControlArrayElement(chkPermitType_6, 6);
			chkDisposition = new System.Collections.Generic.List<FCCheckBox>();
			chkDisposition.AddControlArrayElement(chkDisposition_0, 0);
			chkDisposition.AddControlArrayElement(chkDisposition_1, 1);
			chkDisposition.AddControlArrayElement(chkDisposition_2, 2);
			chkDisposition.AddControlArrayElement(chkDisposition_3, 3);
			chkDisposition.AddControlArrayElement(chkDisposition_4, 4);
			chkDisposition.AddControlArrayElement(chkDisposition_5, 5);
			chkDisposition.AddControlArrayElement(chkDisposition_6, 6);
			Label1 = new System.Collections.Generic.List<FCLabel>();
			Label1.AddControlArrayElement(Label1_0, 0);
			Label1.AddControlArrayElement(Label1_1, 1);
			Label1.AddControlArrayElement(Label1_2, 2);
			Label1.AddControlArrayElement(Label1_3, 3);
			Label1.AddControlArrayElement(Label1_4, 4);
			Label1.AddControlArrayElement(Label1_5, 5);
			Label1.AddControlArrayElement(Label1_6, 6);
			Label1.AddControlArrayElement(Label1_7, 7);
			Label1.AddControlArrayElement(Label1_8, 8);
			Label1.AddControlArrayElement(Label1_9, 9);
			Label1.AddControlArrayElement(Label1_10, 10);
			Label1.AddControlArrayElement(Label1_11, 11);
			Label1.AddControlArrayElement(Label1_12, 12);
			Label1.AddControlArrayElement(Label1_13, 13);
			Label1.AddControlArrayElement(Label1_14, 14);
			Label1.AddControlArrayElement(Label1_15, 15);
			Label1.AddControlArrayElement(Label1_16, 16);
			Label1.AddControlArrayElement(Label1_17, 17);
			Label1.AddControlArrayElement(Label1_18, 18);
			Label1.AddControlArrayElement(Label1_19, 19);
			Label1.AddControlArrayElement(Label1_20, 20);
			Label1.AddControlArrayElement(Label1_21, 21);
            this.Closing += FrmBurialPermit_Closing;
            this.txtAge.AllowOnlyNumericInput();
            this.Shown += FrmBurialPermit_Shown;
		}

        private void FrmBurialPermit_Shown(object sender, EventArgs e)
        {
            InitializeValues();
            ShowData();
        }

        private void FrmBurialPermit_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {


            if (modGNBas.Statics.boolBurialSearch)
            {
                if (cancelling && ViewModel != null)
                {
                    ViewModel.Cancel();
                }
            }
            else
            {
                frmDeaths.InstancePtr.Show(App.MainForm);
            }
            modGNBas.Statics.boolBurialSearch = false;
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmBurialPermit InstancePtr
		{
			get
			{
				return (frmBurialPermit)Sys.GetInstance(typeof(frmBurialPermit));
			}
		}

		protected frmBurialPermit _InstancePtr = null;

		clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		bool Adding;
		int intCount;
		bool boolShowMsg;
		bool OKtoSave;
		int lngDeathNum;
		int lngBurialNum;

		private void imgDocuments_Click(object sender, System.EventArgs e)
		{
			if (lngBurialNum <= 0) return;

			if (imgDocuments.Visible == true)
			{
				ViewDocs();
			}
		}

		private void ViewDocs()
		{
			if (lngBurialNum <= 0)
				return;

			//frmViewDocuments.InstancePtr.Unload();
   //         frmViewDocuments.InstancePtr.Init(0,"Burials",modGNBas.CNSTTYPEBURIALS.ToString(), lngBurialNum,"", "twck0000.vb1", FCForm.FormShowEnum.Modal);
   //         imgDocuments.Visible = modGlobalRoutines.DocListHasItems();
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer("Burials", "Clerk", "Burial",
                lngBurialNum, "", true, false));
            var documentHeaders = StaticSettings.GlobalCommandDispatcher.Send(new GetHeadersByReferenceCommand()).Result;
            imgDocuments.Visible = documentHeaders.Count > 0;
        }

        private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGNBas.Statics.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblregionS where townnumber > 0 order by townnumber", "CentralData");
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("townnumber") + ";" + clsLoad.Get_Fields_String("townname") + "\t" + clsLoad.Get_Fields("townnumber") + "|";
					clsLoad.MoveNext();
				}
                clsLoad.DisposeOf();

				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				strTemp = "0";
				gridTownCode.Visible = false;
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}

		private void chkDisposition_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			
			int x;
			if (chkDisposition[Index].CheckState == Wisej.Web.CheckState.Checked)
			{
				for (x = 0; x <= 6; x++)
				{
					if (x != Index)
					{
						chkDisposition[x].CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				// x
			}
		}

		private void chkDisposition_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = chkDisposition.GetIndex((FCCheckBox)sender);
			chkDisposition_CheckedChanged(index, sender, e);
		}

		private void chkPermitType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (boolShowMsg == true)
				return;
			if (chkPermitType[2].CheckState == Wisej.Web.CheckState.Checked)
			{
				chkBurial.Visible = true;
				chkCremation.Visible = true;
			}
			else
			{
				chkBurial.Visible = false;
				chkCremation.Visible = false;
				chkBurial.CheckState = Wisej.Web.CheckState.Unchecked;
				chkCremation.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			
		}

		private void chkPermitType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = chkPermitType.GetIndex((FCCheckBox)sender);
			chkPermitType_CheckedChanged(index, sender, e);
		}

		private void cmdFind_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: strName As object	OnWrite(string())
			object strName;
			clsDRWrapper rsFacility = new clsDRWrapper();
			modGNBas.Statics.boolDeathClerkFind = true;
			frmListFacility.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrFacility) == string.Empty)
				return;
			strName = Strings.Split(modGNBas.Statics.gstrFacility, ",", -1, CompareConstants.vbBinaryCompare);
			txtNameOfFuneralEstablishment.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.gstrFacility);
			rsFacility.OpenRecordset("Select * from FacilityNames where ID = " + FCConvert.ToString(modGNBas.Statics.gintFacility), modGNBas.DEFAULTDATABASE);
			if (!rsFacility.EndOfFile())
			{
				if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address1") + " ") != string.Empty)
				{
					txtAddress1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFacility.Get_Fields_String("Address1")));
				}
				if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address2") + " ") != string.Empty)
				{
					txtAddress2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFacility.Get_Fields_String("Address2")));
				}
				if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("City") + " ") != string.Empty)
				{
					txtCity.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFacility.Get_Fields_String("City")));
				}
				if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields("State") + " ") != string.Empty)
				{
					txtState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFacility.Get_Fields("State")));
				}
				if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Zip") + " ") != string.Empty)
				{
					txtZip.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFacility.Get_Fields_String("Zip")));
				}
				txtLicenseNumber.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFacility.Get_Fields_String("FacilityNumber")));
			}
		}

		private void CremDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(CremDate.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(CremDate.Text)))
				{
					strD = fecherFoundation.Strings.Trim(CremDate.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						CremDate.Text = strD;
					}
				}
			}
		}

		private void DispositionDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(DispositionDate.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(DispositionDate.Text)))
				{
					strD = fecherFoundation.Strings.Trim(DispositionDate.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						DispositionDate.Text = strD;
					}
				}
			}
		}
		// vbPorter upgrade warning: lngBurialNumber As int	OnWriteFCConvert.ToDouble(
		public void Init(int lngBurialNumber = 0, int lngDeathNumber = 0)
		{
			if (lngBurialNumber > 0)
			{
				lngBurialNum = lngBurialNumber;
				lngDeathNum = 0;
				modGNBas.Statics.boolBurialSearch = true;
			}
			else
			{
				lngDeathNum = lngDeathNumber;
				lngBurialNum = 0;
				modGNBas.Statics.boolBurialSearch = false;
			}
			this.Show(App.MainForm);
		}

        private void ShowData()
		{
			clsDRWrapper rsFuneralInfo = new clsDRWrapper();
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp = 0;
			string strTemp = "";
			// If boolBurialSearch Then Unload frmBurialSearch
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;

			if (modGNBas.Statics.boolBurialSearch)
			{
				strSQL = "SELECT * FROM BurialPermitMaster where ID = " + FCConvert.ToString(lngBurialNum);
			}
			else
			{
				strSQL = "SELECT * FROM BurialPermitMaster where ID = " + FCConvert.ToString(lngDeathNum);
			}
			rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				FillBoxes();
			}
			else
			{
				Adding = true;
				ClearBoxes();
				txtPlaceOfDeathState.Text = "Maine";
				if (!modGNBas.Statics.boolBurialSearch)
				{
					if (MessageBox.Show("Use default information from death screen?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						strSQL = "SELECT * FROM Deaths where ID = " + FCConvert.ToString(lngDeathNum);
						rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
						txtFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FirstName"));
						txtMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("MiddleName"));
						txtLastName.Text = FCConvert.ToString(rs.Get_Fields_String("LastName"));
						txtDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("Designation"));
						if (Information.IsDate(rs.Get_Fields("dateofdeath")))
						{
							txtDateOfDeath.Text = FCConvert.ToString(rs.Get_Fields("DateOfDeath"));
						}
						else
						{
							txtDateOfDeath.Text = FCConvert.ToString(rs.Get_Fields_String("dateofdeathdescription"));
						}
						txtRace.Text = FCConvert.ToString(rs.Get_Fields_String("Race"));
						txtAge.Text = FCConvert.ToString(rs.Get_Fields("Age"));
						txtSex.Text = FCConvert.ToString(rs.Get_Fields_String("Sex"));
						txtPlaceOfDeathCity.Text = FCConvert.ToString(rs.Get_Fields_String("CityorTownofDeath"));
						this.txtLicenseNumber.Text = FCConvert.ToString(rs.Get_Fields_String("FuneralEstablishmentLicenseNumber"));
						if (FCConvert.ToString(rs.Get_Fields_String("nameandaddressoffacility")) != string.Empty)
						{
							// rsFuneralInfo.OpenRecordset "SELECT * FROM FacilityNames WHERE LastName = '" & Left(RS.Fields("NameandAddressofFacility"), InStr(1, RS.Fields("NameandAddressofFacility"), vbCrLf) - 1) & "'", "TWCK0000.vb1"
							intTemp = Strings.InStr(1, FCConvert.ToString(rs.Get_Fields_String("NameandAddressofFacility")), "\r\n", CompareConstants.vbBinaryCompare);
							if (intTemp > 1)
							{
								rsFuneralInfo.OpenRecordset("SELECT * FROM FacilityNames WHERE LastName = '" + Strings.Left(FCConvert.ToString(rs.Get_Fields_String("NameandAddressofFacility")), Strings.InStr(1, FCConvert.ToString(rs.Get_Fields_String("NameandAddressofFacility")), "\r\n", CompareConstants.vbBinaryCompare) - 1) + "'", "TWCK0000.vb1");
							}
							else
							{
								rsFuneralInfo.OpenRecordset("SELECT * FROM FacilityNames WHERE LastName = '" + rs.Get_Fields_String("NameandAddressofFacility") + "'", "TWCK0000.vb1");
							}
							if (rsFuneralInfo.EndOfFile() != true && rsFuneralInfo.BeginningOfFile() != true)
							{
								txtNameOfFuneralEstablishment.Text = FCConvert.ToString(rsFuneralInfo.Get_Fields_String("LastName"));
								txtAddress1.Text = FCConvert.ToString(rsFuneralInfo.Get_Fields_String("Address1"));
								txtAddress2.Text = FCConvert.ToString(rsFuneralInfo.Get_Fields_String("Address2"));
								txtCity.Text = FCConvert.ToString(rsFuneralInfo.Get_Fields_String("City"));
								txtState.Text = FCConvert.ToString(rsFuneralInfo.Get_Fields("State"));
								txtZip.Text = FCConvert.ToString(rsFuneralInfo.Get_Fields_String("Zip"));
							}
							else
							{
								if (intTemp > 1)
								{
									txtNameOfFuneralEstablishment.Text = Strings.Left(FCConvert.ToString(rs.Get_Fields_String("NameandAddressofFacility")), Strings.InStr(1, FCConvert.ToString(rs.Get_Fields_String("NameandAddressofFacility")), "\r\n", CompareConstants.vbBinaryCompare) - 1);
								}
								else
								{
									txtNameOfFuneralEstablishment.Text = FCConvert.ToString(rs.Get_Fields_String("NameandAddressofFacility"));
								}
							}
						}
						else
						{
							txtNameOfFuneralEstablishment.Text = "";
						}
						strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("disposition")));
						// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
						int x;
						for (x = 1; x <= (strTemp.Length); x++)
						{
							if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "T")
							{
								chkPermitType[1].CheckState = Wisej.Web.CheckState.Checked;
							}
							else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "B")
							{
								chkPermitType[0].CheckState = Wisej.Web.CheckState.Checked;
							}
							else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "C")
							{
								chkPermitType[3].CheckState = Wisej.Web.CheckState.Checked;
							}
							else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "R")
							{
								chkPermitType[6].CheckState = Wisej.Web.CheckState.Checked;
							}
							else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "U")
							{
								chkPermitType[5].CheckState = Wisej.Web.CheckState.Checked;
							}
							else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "O")
							{
							}
							else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "S")
							{
								chkPermitType[4].CheckState = Wisej.Web.CheckState.Checked;
							}
							else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "D")
							{
								chkPermitType[2].CheckState = Wisej.Web.CheckState.Checked;
							}
						}

						txtPlaceOfDisposition.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("PlaceofDisposition")));
						txtCityOrTown.Text = FCConvert.ToString(rs.Get_Fields_String("PlaceofDispositionCity"));
						txtDateOfDisposition.Text = FCConvert.ToString(rs.Get_Fields_String("DateofDisposition"));
						txtNameOfClerkOrSubregistrar.Text = FCConvert.ToString(rs.Get_Fields_String("ClerkofRecordName"));
						txtClerkTown.Text = FCConvert.ToString(rs.Get_Fields_String("clerkofrecordcity"));
						txtDateSigned.Text = FCConvert.ToString(rs.Get_Fields_String("DateOriginalFiling"));
						if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("usarmedSERVICES")))
						{
							chkArmedForces.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkArmedForces.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (FCConvert.ToString(rs.Get_Fields_String("Disposition")) == "T")
						{
							// Temporary Storage
						}
						else if (rs.Get_Fields_String("Disposition") == "B")
						{
							// Burial
							chkDisposition[2].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (rs.Get_Fields_String("Disposition") == "C")
						{
							// Cremation
							chkDisposition[3].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (rs.Get_Fields_String("Disposition") == "R")
						{
							// Removal from State
							chkDisposition[6].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (rs.Get_Fields_String("Disposition") == "U")
						{
							// Use by Medical Science
							chkDisposition[5].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (rs.Get_Fields_String("Disposition") == "O")
						{
							// Other
						}
						DispositionDate.Text = FCConvert.ToString(rs.Get_Fields_String("DateofDisposition"));
						txtNameOfCemetery.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("PlaceofDisposition")));
						txtLocation.Text = FCConvert.ToString(rs.Get_Fields_String("PlaceofDispositionCity"));
						txtField3.Text = FCConvert.ToString(rs.Get_Fields_String("PlaceofDispositionState"));
					}
				}
			}

            if (lngBurialNum > 0)
            {
                mnuViewDocs.Enabled = true;               
               imgDocuments.Visible = CheckForDocs(lngBurialNum, "Burial");
            }
            else
            {
                mnuViewDocs.Enabled = false;
                imgDocuments.Visible = false;
            }
            rsFuneralInfo.DisposeOf();
            
        }

        public bool CheckForDocs(int referenceId, string referenceType)
        {
            var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
            var docList = new Collection<CentralDocumentHeader>();

            docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand { DataGroup = "Clerk", ReferenceId = referenceId, ReferenceType = referenceType });

            return docList.Count > 0;
        }

        private void frmBurialPermit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				mnuExit_Click();

		}

		private void frmBurialPermit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			FormUtilities.KeyPressHandler(e, this);
		}

		private void frmBurialPermit_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTownCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//if (modGNBas.Statics.boolBurialSearch)
			//{
			//	// boolBurialSearch = False
			//	// Unload Me
			//}
			//else
			//{
			//	frmDeaths.InstancePtr.Show(App.MainForm);
			//}
			//modGNBas.Statics.boolBurialSearch = false;
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void SaveBoxes()
		{
			modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			rs.Set_Fields("FirstName", txtFirstName.Text);
			rs.Set_Fields("MiddleName", txtMiddleName.Text);
			rs.Set_Fields("LastName", txtLastName.Text);
			rs.Set_Fields("Designation", txtDesignation.Text);
			rs.Set_Fields("Sex", txtSex.Text);
			rs.Set_Fields("DateOfDeath", txtDateOfDeath.Text);
			rs.Set_Fields("Race", txtRace.Text);
			rs.Set_Fields("Age", txtAge.Text);
			rs.Set_Fields("PlaceOfDeathCity", txtPlaceOfDeathCity.Text);
			rs.Set_Fields("PlaceOfDeathState", txtPlaceOfDeathState.Text);
			rs.Set_Fields("LicenseNumber", txtLicenseNumber.Text);
			rs.Set_Fields("NameOfFuneralEstablishment", txtNameOfFuneralEstablishment.Text);
			rs.Set_Fields("BusinessAddress", txtAddress1.Text);
			rs.Set_Fields("BusinessAddress2", txtAddress2.Text);
			rs.Set_Fields("BusinessCity", txtCity.Text);
			rs.Set_Fields("BusinessState", txtState.Text);
			rs.Set_Fields("BusinessZip", txtZip.Text);
			rs.Set_Fields("TypeOfPermit", GetTypeOfPermit());
			// RS.Fields("AuthorizationForPermit") = GetAuthorizationForPermit
			if (chkAuthorizationReport.CheckState == Wisej.Web.CheckState.Checked)
			{
				rs.Set_Fields("AuthorizationReport", true);
			}
			else
			{
				rs.Set_Fields("AuthorizationReport", false);
			}
			if (chkAuthorizationApplication.CheckState == Wisej.Web.CheckState.Checked)
			{
				rs.Set_Fields("AuthorizationApplication", true);
			}
			else
			{
				rs.Set_Fields("Authorizationapplication", false);
			}
			if (chkAuthorizationCertificate.CheckState == Wisej.Web.CheckState.Checked)
			{
				rs.Set_Fields("AuthorizationCertificate", true);
			}
			else
			{
				rs.Set_Fields("AuthorizationCertificate", false);
			}
			if (chkAuthorizationMedicalExaminer.CheckState == Wisej.Web.CheckState.Checked)
			{
				rs.Set_Fields("AuthorizationExaminer", true);
			}
			else
			{
				rs.Set_Fields("AuthorizationExaminer", false);
			}
			rs.Set_Fields("PlaceOfDisposition", txtPlaceOfDisposition.Text);
			rs.Set_Fields("CityOrTown", txtCityOrTown.Text);
			rs.Set_Fields("NameOfClerkOrSubregistrar", txtNameOfClerkOrSubregistrar.Text);
			rs.Set_Fields("DateOfDisposition", txtDateOfDisposition.Text);
			rs.Set_Fields("DateSigned", txtDateSigned.Text);
			rs.Set_Fields("Disposition", GetDisposition());
			rs.Set_Fields("DispositionDate", DispositionDate.Text);
			rs.Set_Fields("Location", txtLocation.Text);
			rs.Set_Fields("NameOfCemetery", txtNameOfCemetery.Text);
			rs.Set_Fields("Signature", txtField5.Text);
			rs.Set_Fields("LocationState", txtField3.Text);
			rs.Set_Fields("DeathNumber", lngDeathNum);
			rs.Set_Fields("clerktown", txtClerkTown.Text);
			rs.Set_Fields("Burial", chkBurial.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("Cremation", chkCremation.CheckState == Wisej.Web.CheckState.Checked);
			modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			rs.Set_Fields("towncode", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			rs.Set_Fields("CremDate", CremDate.Text);
			rs.Set_Fields("CremLoc", CremLoc.Text);
			rs.Set_Fields("CremName", CremName.Text);
			rs.Set_Fields("tempdate", txtTemporaryDate.Text);
			rs.Set_Fields("templocation", T2KTemporaryLocation.Text);
			rs.Set_Fields("TempCemetery", T2KTemporaryCemetery.Text);
			rs.Set_Fields("tempsignature", T2KTemporarySignature.Text);
			if (chkTemporaryStorage.CheckState == Wisej.Web.CheckState.Checked)
			{
				rs.Set_Fields("intemporarystorage", true);
			}
			else
			{
				rs.Set_Fields("intemporarystorage", false);
			}
			rs.Set_Fields("CremBuried", cmbCremains.Text == "Buried");
			rs.Set_Fields("CremScattered", cmbCremains.Text == "Scattered");
			rs.Set_Fields("CremFam", cmbCremains.Text == "To Family");
			if (chkArmedForces.CheckState == Wisej.Web.CheckState.Checked)
			{
				rs.Set_Fields("inarmedforces", true);
			}
			else
			{
				rs.Set_Fields("inarmedforces", false);
			}
			rs.Update();
			lngBurialNum = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			mnuViewDocs.Enabled = true;
			if (ViewModel != null &&  ViewModel.ChargeFees)
			{
				modCashReceiptingData.Statics.typeCRData.Type = "BUR";
				modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(txtLastName.Text + ", " + txtFirstName.Text, 30);
				modCashReceiptingData.Statics.typeCRData.Reference = txtNameOfFuneralEstablishment.Text;
				modCashReceiptingData.Statics.typeCRData.Control1 = txtDateOfDeath.Text;
				modCashReceiptingData.Statics.typeCRData.Control2 = txtLicenseNumber.Text;
				modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(modDogs.Statics.typClerkFees.BurialPermit, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(modDogs.Statics.typClerkFees.BurialStateFee, "000000.00"));
				// .Amount2 = Format(0, "000000.00")
				modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
				modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
				modCashReceiptingData.Statics.typeCRData.PartyID = 0;
				modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
                var transaction = new VitalTransaction();
                transaction.TypeCode = "BUR";
                transaction.Control1 = txtDateOfDeath.Text;
                transaction.Control2 = txtLicenseNumber.Text;
                transaction.VitalAmount = modDogs.Statics.typClerkFees.BurialPermit;
                transaction.StateFee = modDogs.Statics.typClerkFees.BurialStateFee;
                transaction.Name = txtLastName.Text + ", " + txtFirstName.Text;
                transaction.Reference = txtNameOfFuneralEstablishment.Text;
                ViewModel.UpdateTransaction(transaction);
				//Application.Exit();
			}
		}

		private void SetPermitType(string strValue)
		{
			int intCounter;
			string[] strTemp = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strStr = "";
			if (strValue == string.Empty)
			{
				for (intCounter = 0; intCounter <= 6; intCounter++)
				{
					chkPermitType[intCounter].CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// intCounter
				return;
			}
			strTemp = Strings.Split(strValue, ";", -1, CompareConstants.vbTextCompare);
			for (intCounter = 0; intCounter <= 6; intCounter++)
			{
				for (x = 0; x <= (Information.UBound(strTemp, 1)); x++)
				{
					strStr = strTemp[x];
					if (fecherFoundation.Strings.UCase(chkPermitType[intCounter].Text) == fecherFoundation.Strings.UCase(strStr))
					{
						chkPermitType[intCounter].CheckState = Wisej.Web.CheckState.Checked;
					}
				}
				// x
			}
		}

		private void SetDisposition(string strValue)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= 6; intCounter++)
			{
				if (fecherFoundation.Strings.UCase(chkDisposition[intCounter].Text) == fecherFoundation.Strings.UCase(strValue))
				{
					chkDisposition[intCounter].CheckState = Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void FillBoxes()
		{
			boolShowMsg = true;
			txtSex.Text = FCConvert.ToString(rs.Get_Fields_String("Sex"));
			SetPermitType(rs.Get_Fields_String("TypeOfPermit"));
			// Call SetAuthorization(RS.Fields("AuthorizationForPermit"))
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AuthorizationReport")))
			{
				chkAuthorizationReport.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkAuthorizationReport.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AuthorizationApplication")))
			{
				chkAuthorizationApplication.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkAuthorizationApplication.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AuthorizationCertificate")))
			{
				chkAuthorizationCertificate.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkAuthorizationCertificate.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AuthorizationExaminer")))
			{
				chkAuthorizationMedicalExaminer.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkAuthorizationMedicalExaminer.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			SetDisposition(rs.Get_Fields_String("Disposition"));
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("intemporarystorage")))
			{
				chkTemporaryStorage.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkTemporaryStorage.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			txtTemporaryDate.Text = FCConvert.ToString(rs.Get_Fields_String("tempdate"));
			T2KTemporaryCemetery.Text = FCConvert.ToString(rs.Get_Fields_String("tempcemetery"));
			T2KTemporaryLocation.Text = FCConvert.ToString(rs.Get_Fields_String("templocation"));
			T2KTemporarySignature.Text = FCConvert.ToString(rs.Get_Fields_String("tempsignature"));
			txtFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FirstName"));
			txtMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("MiddleName"));
			txtLastName.Text = FCConvert.ToString(rs.Get_Fields_String("LastName"));
			txtDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("Designation"));
			txtDateOfDeath.Text = FCConvert.ToString(rs.Get_Fields("DateOfDeath"));
			txtRace.Text = FCConvert.ToString(rs.Get_Fields_String("Race"));
			txtAge.Text = FCConvert.ToString(rs.Get_Fields("Age"));
			txtPlaceOfDeathCity.Text = FCConvert.ToString(rs.Get_Fields_String("PlaceOfDeathCity"));
			txtPlaceOfDeathState.Text = FCConvert.ToString(rs.Get_Fields_String("PlaceOfDeathState"));
			txtLicenseNumber.Text = FCConvert.ToString(rs.Get_Fields_String("LicenseNumber"));
			txtNameOfFuneralEstablishment.Text = FCConvert.ToString(rs.Get_Fields_String("NameOfFuneralEstablishment"));
			txtAddress1.Text = FCConvert.ToString(rs.Get_Fields_String("BusinessAddress"));
			txtAddress2.Text = FCConvert.ToString(rs.Get_Fields_String("BusinessAddress2"));
			txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("BusinessCity"));
			txtState.Text = FCConvert.ToString(rs.Get_Fields_String("BusinessState"));
			txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("BusinessZip"));
			txtPlaceOfDisposition.Text = FCConvert.ToString(rs.Get_Fields_String("PlaceOfDisposition"));
			txtCityOrTown.Text = FCConvert.ToString(rs.Get_Fields_String("CityOrTown"));
			txtNameOfClerkOrSubregistrar.Text = FCConvert.ToString(rs.Get_Fields_String("NameOfClerkOrSubregistrar"));
			txtDateOfDisposition.Text = FCConvert.ToString(rs.Get_Fields_String("DateOfDisposition"));
			txtDateSigned.Text = FCConvert.ToString(rs.Get_Fields_String("DateSigned"));
			DispositionDate.Text = FCConvert.ToString(rs.Get_Fields_String("DispositionDate"));
			txtLocation.Text = FCConvert.ToString(rs.Get_Fields_String("Location"));
			txtNameOfCemetery.Text = FCConvert.ToString(rs.Get_Fields_String("NameOfCemetery"));
			txtField5.Text = FCConvert.ToString(rs.Get_Fields_String("Signature"));
			txtField3.Text = FCConvert.ToString(rs.Get_Fields_String("LocationState"));
			txtClerkTown.Text = FCConvert.ToString(rs.Get_Fields_String("clerktown"));
			CremDate.Text = FCConvert.ToString(rs.Get_Fields_String("CremDate"));
			CremLoc.Text = FCConvert.ToString(rs.Get_Fields_String("CremLoc"));
			CremName.Text = FCConvert.ToString(rs.Get_Fields_String("CremName"));
			lngDeathNum = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("ID"))));
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("cremfam")))
			{
				cmbCremains.Text = "To Family";
			}
			else if (rs.Get_Fields_Boolean("cremscattered"))
			{
				cmbCremains.Text = "Scattered";
			}
			else
			{
				cmbCremains.Text = "Buried";
			}
			lngBurialNum = FCConvert.ToInt32(rs.Get_Fields_Int32("id"));
			if (Conversion.Val(rs.Get_Fields_Int32("towncode")) > 0)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("towncode"))));
			}
			chkBurial.CheckState = (CheckState)((rs.Get_Fields_Boolean("Burial")) ? 1 : 0);
			chkCremation.CheckState = (CheckState)((rs.Get_Fields_Boolean("Cremation")) ? 1 : 0);
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("InArmedForces")))
			{
				chkArmedForces.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkArmedForces.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			boolShowMsg = false;
		}
		
		private object GetDisposition()
		{
			object GetDisposition = null;
			int intCounter;
			for (intCounter = 0; intCounter <= 6; intCounter++)
			{
				if (chkDisposition[intCounter].CheckState == Wisej.Web.CheckState.Checked)
				{
					GetDisposition = chkDisposition[intCounter].Text;
					return GetDisposition;
				}
			}
			return GetDisposition;
		}

		private object GetTypeOfPermit()
		{
			object GetTypeOfPermit = null;
			int intCounter;
			GetTypeOfPermit = "";
			for (intCounter = 0; intCounter <= 6; intCounter++)
			{
				if (chkPermitType[intCounter].CheckState == Wisej.Web.CheckState.Checked)
				{
					GetTypeOfPermit = GetTypeOfPermit + chkPermitType[intCounter].Text + ";";
					// Exit Function
				}
			}
			if (FCConvert.ToString(GetTypeOfPermit) != string.Empty)
			{
				GetTypeOfPermit = Strings.Mid(FCConvert.ToString(GetTypeOfPermit), 1, FCConvert.ToString(GetTypeOfPermit).Length - 1);
			}
			return GetTypeOfPermit;
		}

		private void ClearBoxes()
		{
			boolShowMsg = true;
			txtFirstName.Text = "";
			txtMiddleName.Text = "";
			txtLastName.Text = "";
			txtDesignation.Text = "";
			txtSex.Text = string.Empty;
			txtDateOfDeath.Text = "";
			txtRace.Text = "";
			txtAge.Text = "";
			txtPlaceOfDeathCity.Text = "";
			txtPlaceOfDeathState.Text = "";
			txtLicenseNumber.Text = "";
			txtNameOfFuneralEstablishment.Text = "";
			txtAddress1.Text = "";
			txtAddress2.Text = "";
			txtCity.Text = "";
			txtState.Text = "";
			txtZip.Text = "";
			// optTypeOfPermit(0) = True
			int x;
			for (x = 0; x <= 6; x++)
			{
				chkPermitType[x].CheckState = Wisej.Web.CheckState.Unchecked;
			}
			// x
			// chkPermitType(0).Value = vbChecked
			// optAuthorizationForPermit(0) = True
			chkAuthorizationApplication.CheckState = Wisej.Web.CheckState.Unchecked;
			chkAuthorizationCertificate.CheckState = Wisej.Web.CheckState.Unchecked;
			chkAuthorizationMedicalExaminer.CheckState = Wisej.Web.CheckState.Unchecked;
			chkAuthorizationReport.CheckState = Wisej.Web.CheckState.Unchecked;
			txtPlaceOfDisposition.Text = "";
			txtCityOrTown.Text = "";
			txtNameOfClerkOrSubregistrar.Text = "";
			txtDateOfDisposition.Text = "";
			txtDateSigned.Text = "";
			// optDisposition(0) = True
			for (x = 0; x <= 6; x++)
			{
				chkDisposition[x].CheckState = Wisej.Web.CheckState.Unchecked;
			}
			// x
			DispositionDate.Text = "";
			txtLocation.Text = "";
			txtNameOfCemetery.Text = "";
			txtField5.Text = "";
			chkBurial.CheckState = Wisej.Web.CheckState.Unchecked;
			chkCremation.CheckState = Wisej.Web.CheckState.Unchecked;
			boolShowMsg = false;
		}

		private void mnuAssociateDeathCertificate_Click(object sender, System.EventArgs e)
		{
			int lngReturn;
			lngReturn = frmChooseDeath.InstancePtr.Init();
			if (lngReturn >= 0)
			{
				lngDeathNum = lngReturn;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (lngBurialNum != 0)
				{
					if (MessageBox.Show("Are you sure you want to delete this burial record?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						rsData.Execute("Delete from BurialPermitMaster where ID = " + FCConvert.ToString(lngBurialNum), modGNBas.DEFAULTCLERKDATABASE);
						MessageBox.Show("Delete completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						Close();
						modGNBas.Statics.boolBurialSearch = false;
						
					}
				}
				else
				{
					MessageBox.Show("Burial record not selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			frmBurialPermit.InstancePtr.Close();
			modGNBas.Statics.boolBurialSearch = false;
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.boolBurialSearch = true;
			lngBurialNum = 0;
			lngDeathNum = 0;
			ClearBoxes();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// rptBurialPermit.PrintReport False
				rptBurialPermitWordDoc.InstancePtr.PrintReport(false);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				rptBurialPermitWordDoc.InstancePtr.Init(false, this.Modal, 0, lngBurialNum);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			int lngReocrd;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				OKtoSave = true;
				strSQL = "SELECT * FROM BurialPermitMaster where ID = " + FCConvert.ToString(lngBurialNum);
				rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
				if (rs.EndOfFile())
				{
					rs.AddNew();
				}
				else
				{
					rs.Edit();
				}
				SaveBoxes();
				// RS.Update
				MessageBox.Show("Update completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				OKtoSave = true;
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveQuit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			if (OKtoSave)
				mnuExit_Click();
		}

		private void optDisposition_Click(ref int Index)
		{
			switch (Index)
			{
				case 0:
					{
						lblField1.Text = "Name Of Cemetery Or Vault";
						lblField2.Text = "Location";
						lblField3.Visible = false;
						txtField3.Visible = false;
						lblField4.Text = "Signature Of Person In Charge Or Municipal Official";
						break;
					}
				case 1:
					{
						lblField1.Text = "Name Of Cemetery Or Vault";
						lblField2.Text = "Location (City)";
						lblField3.Visible = true;
						txtField3.Visible = true;
						lblField3.Text = "Location (State)";
						lblField4.Text = "Signature Of Person In Charge Or Municipal Official";
						break;
					}
				case 2:
				case 3:
					{
						lblField1.Text = "Name Of Cemetery Or Crematory";
						lblField2.Text = "Location (City)";
						lblField3.Visible = true;
						txtField3.Visible = true;
						lblField3.Text = "Location (State)";
						lblField4.Text = "Signature Of Person In Charge Or Municipal Official";
						break;
					}
				default:
					{
						lblField1.Text = "Name Of Medical School Or Other Destination";
						lblField2.Text = "Location";
						lblField3.Visible = false;
						txtField3.Visible = false;
						lblField4.Text = "Signature Of Funeral Director Or Authorized Person";
						break;
					}
			}
			//end switch
		}

		private void frmBurialPermit_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetupGridTownCode();
			if (lngBurialNum > 0)
            {
                mnuViewDocs.Enabled = true;
                imgDocuments.Visible = CheckForDocs(lngBurialNum, "Burial");
            }
			else
			{
				mnuViewDocs.Enabled = false;
				imgDocuments.Visible = false;
			}
		}

        private void InitializeValues()
        {
            if (ViewModel != null)
            {
                if (ViewModel.VitalId > 0)
                {
                    lngBurialNum = ViewModel.VitalId;
                    lngDeathNum = 0;
                    modGNBas.Statics.boolBurialSearch = true;
                }
                else
                {
                    lngDeathNum = ViewModel.DeathId;
                    lngBurialNum = 0;
                    modGNBas.Statics.boolBurialSearch = false;
                }
            }
        }

        private void optTypeOfPermit_Click(ref int Index)
		{
			if (boolShowMsg == true)
				return;
			chkBurial.Visible = Index == 2;
			chkCremation.Visible = Index == 2;
			if (Index == 2)
			{
			}
			else
			{
				chkBurial.CheckState = Wisej.Web.CheckState.Unchecked;
				chkCremation.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void mnuViewDocs_Click(object sender, System.EventArgs e)
		{
			ViewDocs();
		}

		//private void txtAge_KeyPressEvent(object sender, KeyPressEventArgs e)
		//{
		//	int KeyAscii = Strings.Asc(e.KeyChar);
		//	if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
		//		KeyAscii = 0;
		//	e.KeyChar = Strings.Chr(KeyAscii);
		//}

		private void txtCityOrTown_DblClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCityOrTown.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtDateOfDeath_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateOfDeath.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateOfDeath.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateOfDeath.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateOfDeath.Text = strD;
					}
				}
			}
		}

		private void txtDateOfDisposition_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateOfDisposition.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateOfDisposition.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateOfDisposition.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateOfDisposition.Text = strD;
					}
				}
			}
		}

		private void txtDateSigned_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateSigned.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateSigned.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateSigned.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateSigned.Text = strD;
					}
				}
			}
		}

		private void txtLocation_DblClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtLocation.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtPlaceOfDeathCity_DblClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtPlaceOfDeathCity.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtPlaceOfDeathState_DblClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtPlaceOfDeathState.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtSex_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			// if this is the sex field
			// only allow the M and F keys to register in the sex field
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii != 70 && KeyAscii != 77 && KeyAscii != 8)
			{
				KeyAscii = 0;
				MessageBox.Show("Must enter 'M' or 'F'.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtTemporaryDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtTemporaryDate.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtTemporaryDate.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtTemporaryDate.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtTemporaryDate.Text = strD;
					}
				}
			}
		}

        public IBurialViewModel ViewModel { get; set; }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

    }
}
