//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogReminders.
	/// </summary>
	public partial class rptDogReminders : BaseSectionReport
	{
		public rptDogReminders()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Unlicensed Dog Reminders";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDogReminders InstancePtr
		{
			get
			{
				return (rptDogReminders)Sys.GetInstance(typeof(rptDogReminders));
			}
		}

		protected rptDogReminders _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDogReminders	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private string strTitle = "";
		private string strSigniture = "";
		private clsDRWrapper rsData = new clsDRWrapper();
		private clsDRWrapper rsTemp = new clsDRWrapper();
		private int intPageNumber;
		private int intCounter;
		private string strDate = string.Empty;
		string strMessage = "";
		private bool boolPrintTest;
		private double dblLineAdjust;
		private bool boolWarrant;
		private bool boolSendEmail;
		private bool boolPrintReminders;
		private bool boolPrintAllReminders;
		private string strEmailSubject = "";
		private string strEmailBegin = "";
		private string strEmailEnd = "";
		private clsBulkEmail lstEmailList = new clsBulkEmail();

		public void Init(bool boolTestPrint, bool modalDialog, double dblAdjust = 0, bool boolDogReminder = true, bool boolEmail = false, bool boolPrint = true, bool boolPrintAll = true, string strReminderDate = "")
		{
			boolPrintTest = boolTestPrint;
			dblLineAdjust = dblAdjust;
			boolPrintAllReminders = boolPrintAll;
			boolPrintReminders = boolPrint;
			boolSendEmail = boolEmail;
			strDate = strReminderDate;
			lstEmailList.ClearList();
			if (boolDogReminder)
			{
				boolWarrant = false;
				strEmailSubject = "Unlicensed dog reminder";
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: true, strAttachmentName: "DogReminders", showModal: modalDialog);
			}
			else
			{
				boolWarrant = true;
				strEmailSubject = "Dog warrant reminder";
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: true, strAttachmentName: "WarrantReminders", showModal: modalDialog);
			}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// txtPage = "Page " & intPageNumber
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strTemp = "";
			string strAddress1 = "";
			string strAddress2 = "";
			string strAddress3 = "";
			NextRecord:
			;
			// txtMuni = strTitle
			txtDate.Text = strDate;
			txtSigniture.Text = strSigniture;
			if (!boolPrintTest)
			{
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				string strEmailBody = "";
				clsEmailObject tMail;
				if (FCConvert.ToString(rsData.Get_Fields_String("MiddleName")) != "N/A")
				{
					// xxkk  "MI"
					// txtAddress1.Text = rsData.Fields("FirstName") & " " & rsData.Fields("MI") & " " & rsData.Fields("LastName")
					strAddress1 = rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("MiddleName") + " " + rsData.Get_Fields_String("LastName");
					// xxkk "MI"
				}
				else
				{
					// txtAddress1.Text = rsData.Fields("FirstName") & " " & rsData.Fields("LastName")
					strAddress1 = rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName");
				}
				// txtAddress2 = rsData.Fields("Address")
				strAddress2 = FCConvert.ToString(rsData.Get_Fields_String("Address1"));
				// txtAddress3 = rsData.Fields("City") & ", " & rsData.Fields("state") & " " & rsData.Fields("Zip")
				strAddress3 = rsData.Get_Fields_String("City") + ", " + rsData.Get_Fields("PartyState") + " " + rsData.Get_Fields_String("Zip");
				intCounter = 0;
				string strSQL = "";
				strSQL = "Select DogDOB,dogdeceased from DogInfo where OwnerNum = " + rsData.Get_Fields_Int32("ID") + " and year < " + FCConvert.ToString(DateTime.Today.Year) + " AND dogdeceased <> 1";
				if (boolWarrant)
				{
					strSQL += " and ExcludeFromWarrant <> 1";
				}
				rsTemp.OpenRecordset(strSQL, modGNBas.DEFAULTCLERKDATABASE);
				while (!rsTemp.EndOfFile())
				{
					if (!FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("dogdeceased")))
					{
						if (Information.IsDate(rsTemp.Get_Fields("DogDOB")))
						{
							if (fecherFoundation.DateAndTime.DateAdd("M", 6, (DateTime)rsTemp.Get_Fields_DateTime("DogDOB")).ToOADate() <= DateTime.Today.ToOADate())
							{
								intCounter += 1;
							}
						}
						else
						{
							// no age so assume it is over 6 months old
							intCounter += 1;
						}
					}
					rsTemp.MoveNext();
				}
				if (intCounter == 0)
				{
					rsData.MoveNext();
					goto NextRecord;
				}
				else
				{
					// txtNumber = intCounter
					strTemp = "Our records indicate that you are the owner or keeper of " + FCConvert.ToString(intCounter) + " dog(s) over six months of age.  ";
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("email"))) != "")
					{
						if (boolSendEmail)
						{
							strEmailBody = strEmailBegin + "\r\n" + "To: " + strAddress1 + "\r\n" + "\r\n";
							strEmailBody += "Dear dog owner/keeper" + "\r\n";
							strEmailBody += strTemp + strMessage + "\r\n" + "\r\n";
							strEmailBody += strEmailEnd;
							tMail = new clsEmailObject();
							tMail.Body = strEmailBody;
							tMail.EmailAddress = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("email")));
							tMail.Subject = strEmailSubject;
							tMail.Tag = strAddress1;
							lstEmailList.AddItem(ref tMail);
							// Call frmEMail.Init(, rsData.Fields("email"), strEmailSubject, strEmailBody, True, , , True, True, False)
						}
						if (!boolPrintAllReminders || !boolPrintReminders)
						{
							rsData.MoveNext();
							goto NextRecord;
						}
					}
					else if (!boolPrintReminders)
					{
						rsData.MoveNext();
						goto NextRecord;
					}
					txtAddress1.Text = strAddress1;
					txtAddress2.Text = strAddress2;
					txtAddress3.Text = strAddress3;
					//FC:FINAL:DSE WordWrapping not working in RichTextBox
					//RichEdit1.Text = strTemp + strMessage;
					RichEdit1.SetHtmlText(strTemp + strMessage);
				}
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				txtAddress1.Text = "Name";
				txtAddress2.Text = "Address";
				txtAddress3.Text = "City State Zip";
				//FC:FINAL:DSE WordWrapping not working in RichTextBox
				//RichEdit1.Text = "Our records indicate that you are the owner or keeper of 100 dog(s) over six months of age.  " + strMessage;
				RichEdit1.SetHtmlText("Our records indicate that you are the owner or keeper of 100 dog(s) over six months of age.  " + strMessage);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Dim clsLoad As New clsDRWrapper
			// vbPorter upgrade warning: lngMove As int	OnWrite(int, double)
			float lngMove;
			int x;
			lngMove = 0;
			if (!boolPrintTest)
			{
				// Call clsLoad.OpenRecordset("select * from printersettings", "twck0000.vb1")
				// If Not clsLoad.EndOfFile Then
				// adjust by this many lines
				if (Conversion.Val(modRegistry.GetRegistryKey("DogRemindersAdjustment", "CK")) != 0)
				{
					// If Val(clsLoad.Fields("DogRemindersAdjustment")) <> 0 Then
					lngMove = FCConvert.ToSingle(modRegistry.GetRegistryKey("DogRemindersAdjustment", "CK")) * 240 / 1440F;
					if (Field1.Top + lngMove < 0)
					{
						lngMove = -Field1.Top;
						// can only move this much
					}
				}
				// End If
			}
			else
			{
				lngMove = FCConvert.ToSingle(dblLineAdjust * 240) / 1440F;
				if (Field1.Top + lngMove < 0)
				{
					lngMove = -Field1.Top;
				}
			}
			for (x = 0; x <= Detail.Controls.Count - 1; x++)
			{
				if (fecherFoundation.Strings.UCase(Strings.Mid(Detail.Controls[x].Name, 1, 4)) == "LINE")
				{
					(Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 += lngMove;
					(Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Line).Y2 += lngMove;
					if (Detail.Height <= (Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1)
					{
						Detail.Height = (Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 + 10 / 1440F;
					}
				}
				else
				{
					Detail.Controls[x].Top += lngMove;
				}
			}
			// x
			RichEdit1.Font = new Font(Field5.Font.Name, RichEdit1.Font.Size);
			RichEdit1.Font = new Font(RichEdit1.Font.Name, Field5.Font.Size);
			rsData.OpenRecordset("select * from dogdefaults", "twck0000.vb1");
			if (!rsData.EndOfFile())
			{
				if (!boolWarrant)
				{
					strMessage = FCConvert.ToString(rsData.Get_Fields_String("unlicensedreminder"));
					strSigniture = FCConvert.ToString(rsData.Get_Fields_String("unlicensedsignature"));
				}
				else
				{
					strMessage = FCConvert.ToString(rsData.Get_Fields_String("warrantreminder"));
					strSigniture = FCConvert.ToString(rsData.Get_Fields_String("unlicensedsignature"));
				}
			}
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			rsData.OpenRecordset("Select * from GlobalVariables", "SystemSettings");
			if (!rsData.EndOfFile())
			{
				strTitle = rsData.Get_Fields_String("CityTown") + " of " + rsData.Get_Fields_String("MuniName");
				// strSigniture = rsData.Fields("MuniName") & " Animal Control Officer or " & Chr(13) & "Town Clerk for the " & rsData.Fields("CityTown") & " of " & rsData.Fields("MuniName")
			}
			// Call rsData.OpenRecordset("Select * from ClerkDefaults", "TWCK0000.vb1")
			// If Not rsData.EndOfFile Then
			// strSigniture = strSigniture & Chr(13) & "Phone # " & rsData.Fields("Phone")
			// End If
			if (!boolPrintTest)
			{
				rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGNBas.DEFAULTCLERKDATABASE);
				// strDate = InputBox("Enter date to show on reminders.", "TRIO Software", Date)
				// If strDate = vbNullString Then
				// Me.Cancel
				// Unload Me
				// Unload frmReportViewer
				// Exit Sub
				// End If
				strEmailBegin = "Notice to unlicensed dog owner/keeper";
				strEmailBegin += "\r\n" + "Title 7, M. R. S. A. #3921, #3931, & #3943" + "\r\n" + "\r\n";
				strEmailBegin += "Date: " + strDate + "\r\n";
				strEmailEnd = strSigniture + "\r\n" + "\r\n";
				strEmailEnd += "Maine certification of rabies vaccination means a current up to date certificate";
				// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
				if (rsData.RecordCount() != 0)
					rsData.MoveFirst();
			}
			else
			{
				strDate = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			intPageNumber = 1;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (!(lstEmailList == null))
			{
				if (lstEmailList.ItemCount() > 0)
				{
					if (boolSendEmail)
					{
						if (MessageBox.Show("Do you want to send emails now?", "Send Now?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
						{
							lstEmailList.SendMail();
						}
					}
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (lstEmailList.ItemCount() > 0)
			{
				ReportFooter.Visible = true;
				srptBulkEmailList tRept = new srptBulkEmailList();
				tRept.EmailList = lstEmailList;
				SubReport1.Report = tRept;
			}
			else
			{
				ReportFooter.Visible = false;
			}
		}

		
	}
}
