//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Clerk.Models;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBirthReport.
	/// </summary>
	public partial class rptBirthReport : BaseSectionReport
	{
		public rptBirthReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Birth Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBirthReport InstancePtr
		{
			get
			{
				return (rptBirthReport)Sys.GetInstance(typeof(rptBirthReport));
			}
		}

		protected rptBirthReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBirthReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int listIndex = 0;
		private List<BirthReportInfo> reportData;
		public void Init(List<BirthReportInfo> reportData)
		{
			if (reportData.Count == 0)
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			this.reportData = reportData;

			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "BirthReport");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = listIndex >= reportData.Count;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp;
			if (reportData[listIndex].ConvertedRecordedDateOfBirth != null)
			{
				txtBirthDate.Text = ((DateTime)(reportData[listIndex].ConvertedRecordedDateOfBirth)).ToShortDateString();
			}
			else
			{
				txtBirthDate.Text = reportData[listIndex].RecordedDateOfBirth;
			}
			strTemp = reportData[listIndex].MiddleName.Trim();
			if (strTemp.Length > 0)
			{
				strTemp = Strings.Left(strTemp, 1);
			}
			txtName.Text = (reportData[listIndex].FirstName + " " + strTemp).Trim() + " " + reportData[listIndex].LastName.Trim();
			txtBirthplace.Text = reportData[listIndex].BirthPlace.Trim();
			strTemp = reportData[listIndex].MothersLastName.Trim();
			if (strTemp.Length > 0)
			{
				strTemp = Strings.Left(strTemp, 1);
			}
			txtMothersName.Text = (reportData[listIndex].MothersFirstName + " " + strTemp).Trim() + " " + reportData[listIndex].MothersLastName.Trim();
			strTemp = reportData[listIndex].Sex;
			strTemp = fecherFoundation.Strings.UCase(strTemp);
			if (strTemp.Length > 0)
			{
				strTemp = Strings.Left(strTemp, 1);
			}
			txtSex.Text = strTemp;
			listIndex++;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}
	}
}
