//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmMarriageReport.
	/// </summary>
	partial class frmMarriageReport
	{
		public fecherFoundation.FCComboBox cmbSort;
		public fecherFoundation.FCLabel lblSort;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCGrid Grid;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbSort = new fecherFoundation.FCComboBox();
            this.lblSort = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.Grid = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 398);
            this.BottomPanel.Size = new System.Drawing.Size(658, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbSort);
            this.ClientArea.Controls.Add(this.lblSort);
            this.ClientArea.Size = new System.Drawing.Size(658, 338);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(658, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(190, 30);
            this.HeaderText.Text = "Marriage Report";
            // 
            // cmbSort
            // 
            this.cmbSort.Items.AddRange(new object[] {
            "Groom",
            "Bride",
            "Date Married",
            "City"});
            this.cmbSort.Location = new System.Drawing.Point(150, 30);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(238, 40);
            this.cmbSort.TabIndex = 6;
            this.cmbSort.Text = "Groom";
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(30, 44);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(62, 15);
            this.lblSort.TabIndex = 7;
            this.lblSort.Text = "SORT BY";
            // 
            // Frame2
            // 
            this.Frame2.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.Grid);
            this.Frame2.Location = new System.Drawing.Point(30, 90);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(600, 331);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Range";
            // 
            // Grid
            // 
            this.Grid.Cols = 3;
            this.Grid.ColumnHeadersVisible = false;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedRows = 0;
            this.Grid.Location = new System.Drawing.Point(0, 30);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = false;
            this.Grid.Rows = 4;
            this.Grid.Size = new System.Drawing.Size(600, 163);
            this.Grid.TabIndex = 6;
            this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(226, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(176, 48);
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.cmdSaveContinue_Click);
            // 
            // frmMarriageReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(658, 506);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmMarriageReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Marriage Report";
            this.Load += new System.EventHandler(this.frmMarriageReport_Load);
            this.Resize += new System.EventHandler(this.frmMarriageReport_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMarriageReport_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSaveContinue;
	}
}