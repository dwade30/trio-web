//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmMonthlyDogReport.
	/// </summary>
	partial class frmMonthlyDogReport
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtYearToReport;
		public System.Collections.Generic.List<fecherFoundation.FCButton> txtPrint;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtYearToReport_1;
		public fecherFoundation.FCTextBox txtClerkName;
		public fecherFoundation.FCTextBox txtReason;
		public fecherFoundation.FCTextBox txtAdjustments;
		public fecherFoundation.FCTextBox txtYearToReport_0;
		public fecherFoundation.FCButton txtPrint_2;
		public fecherFoundation.FCTextBox txtMonth;
		public FCGrid gridTownCode;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtYearToReport_1 = new fecherFoundation.FCTextBox();
			this.txtClerkName = new fecherFoundation.FCTextBox();
			this.txtReason = new fecherFoundation.FCTextBox();
			this.txtAdjustments = new fecherFoundation.FCTextBox();
			this.txtYearToReport_0 = new fecherFoundation.FCTextBox();
			this.txtMonth = new fecherFoundation.FCTextBox();
			this.gridTownCode = new fecherFoundation.FCGrid();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.txtPrint_2 = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrint_2)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.txtPrint_2);
			this.BottomPanel.Location = new System.Drawing.Point(0, 473);
			this.BottomPanel.Size = new System.Drawing.Size(556, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(556, 413);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(556, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(323, 30);
			this.HeaderText.Text = "Monthly Dog License Report";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtYearToReport_1);
			this.Frame1.Controls.Add(this.txtClerkName);
			this.Frame1.Controls.Add(this.txtReason);
			this.Frame1.Controls.Add(this.txtAdjustments);
			this.Frame1.Controls.Add(this.txtYearToReport_0);
			this.Frame1.Controls.Add(this.txtMonth);
			this.Frame1.Controls.Add(this.gridTownCode);
			this.Frame1.Controls.Add(this.Label5);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(498, 355);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Report Parameters";
			// 
			// txtYearToReport_1
			// 
			this.txtYearToReport_1.MaxLength = 4;
			this.txtYearToReport_1.AutoSize = false;
			this.txtYearToReport_1.LinkItem = null;
			this.txtYearToReport_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYearToReport_1.LinkTopic = null;
			this.txtYearToReport_1.Location = new System.Drawing.Point(398, 75);
			this.txtYearToReport_1.Name = "txtYearToReport_1";
			this.txtYearToReport_1.Size = new System.Drawing.Size(80, 40);
			this.txtYearToReport_1.TabIndex = 5;
			this.txtYearToReport_1.Tag = "Required";
			this.txtYearToReport_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYearToReport_KeyPress);
			this.txtYearToReport_1.Enter += new System.EventHandler(this.txtYearToReport_Enter);
			// 
			// txtClerkName
			// 
			this.txtClerkName.AutoSize = false;
			this.txtClerkName.LinkItem = null;
			this.txtClerkName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtClerkName.LinkTopic = null;
			this.txtClerkName.Location = new System.Drawing.Point(204, 295);
			this.txtClerkName.Name = "txtClerkName";
			this.txtClerkName.Size = new System.Drawing.Size(274, 40);
			this.txtClerkName.TabIndex = 11;
			this.txtClerkName.Tag = "Required";
			// 
			// txtReason
			// 
			this.txtReason.AutoSize = false;
			this.txtReason.BackColor = System.Drawing.SystemColors.Window;
			this.txtReason.LinkItem = null;
			this.txtReason.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReason.LinkTopic = null;
			this.txtReason.Location = new System.Drawing.Point(204, 135);
			this.txtReason.Multiline = true;
			this.txtReason.Name = "txtReason";
			this.txtReason.Size = new System.Drawing.Size(274, 80);
			this.txtReason.TabIndex = 7;
			this.txtReason.Enter += new System.EventHandler(this.txtReason_Enter);
			// 
			// txtAdjustments
			// 
			this.txtAdjustments.AutoSize = false;
			this.txtAdjustments.BackColor = System.Drawing.SystemColors.Window;
			this.txtAdjustments.LinkItem = null;
			this.txtAdjustments.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAdjustments.LinkTopic = null;
			this.txtAdjustments.Location = new System.Drawing.Point(204, 235);
			this.txtAdjustments.Name = "txtAdjustments";
			this.txtAdjustments.Size = new System.Drawing.Size(274, 40);
			this.txtAdjustments.TabIndex = 9;
			this.txtAdjustments.Enter += new System.EventHandler(this.txtAdjustments_Enter);
			// 
			// txtYearToReport_0
			// 
			this.txtYearToReport_0.MaxLength = 4;
			this.txtYearToReport_0.AutoSize = false;
			this.txtYearToReport_0.LinkItem = null;
			this.txtYearToReport_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYearToReport_0.LinkTopic = null;
			this.txtYearToReport_0.Location = new System.Drawing.Point(294, 75);
			this.txtYearToReport_0.Name = "txtYearToReport_0";
			this.txtYearToReport_0.Size = new System.Drawing.Size(80, 40);
			this.txtYearToReport_0.TabIndex = 3;
			this.txtYearToReport_0.Tag = "Required";
			this.txtYearToReport_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYearToReport_KeyPress);
			this.txtYearToReport_0.Enter += new System.EventHandler(this.txtYearToReport_Enter);
			// 
			// txtMonth
			// 
			this.txtMonth.MaxLength = 2;
			this.txtMonth.AutoSize = false;
			this.txtMonth.LinkItem = null;
			this.txtMonth.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMonth.LinkTopic = null;
			this.txtMonth.Location = new System.Drawing.Point(204, 75);
			this.txtMonth.Name = "txtMonth";
			this.txtMonth.Size = new System.Drawing.Size(60, 40);
			this.txtMonth.TabIndex = 2;
			this.txtMonth.Tag = "Required";
			this.txtMonth.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMonth_KeyPress);
			this.txtMonth.Enter += new System.EventHandler(this.txtMonth_Enter);
			// 
			// gridTownCode
			// 
			this.gridTownCode.AllowSelection = false;
			this.gridTownCode.AllowUserToResizeColumns = false;
			this.gridTownCode.AllowUserToResizeRows = false;
			this.gridTownCode.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridTownCode.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorBkg = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorSel = System.Drawing.Color.Empty;
			this.gridTownCode.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.gridTownCode.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridTownCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.gridTownCode.ColumnHeadersHeight = 30;
			this.gridTownCode.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridTownCode.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridTownCode.DefaultCellStyle = dataGridViewCellStyle2;
			this.gridTownCode.DragIcon = null;
			this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTownCode.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridTownCode.ExtendLastCol = true;
			this.gridTownCode.FixedCols = 0;
			this.gridTownCode.FixedRows = 0;
			this.gridTownCode.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.FrozenCols = 0;
			this.gridTownCode.GridColor = System.Drawing.Color.Empty;
			this.gridTownCode.GridColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.Location = new System.Drawing.Point(20, 30);
			this.gridTownCode.Name = "gridTownCode";
			this.gridTownCode.OutlineCol = 0;
			this.gridTownCode.RowHeadersVisible = false;
			this.gridTownCode.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridTownCode.RowHeightMin = 0;
			this.gridTownCode.Rows = 1;
			this.gridTownCode.ScrollTipText = null;
			this.gridTownCode.ShowColumnVisibilityMenu = false;
			this.gridTownCode.Size = new System.Drawing.Size(458, 25);
			this.gridTownCode.StandardTab = true;
			this.gridTownCode.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridTownCode.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridTownCode.TabIndex = 0;
			this.gridTownCode.Tag = "land";
			this.gridTownCode.Visible = false;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 309);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(99, 15);
			this.Label5.TabIndex = 10;
			this.Label5.Text = "  CLERK NAME";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 150);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(65, 15);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "REASON";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 249);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(110, 15);
			this.Label4.TabIndex = 8;
			this.Label4.Text = "ADJUSTMENTS";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(384, 89);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(12, 15);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "/";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 89);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(141, 15);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "FOR THE MONTH OF";
			// 
			// txtPrint_2
			// 
			this.txtPrint_2.AppearanceKey = "acceptButton";
			this.txtPrint_2.Location = new System.Drawing.Point(200, 30);
			this.txtPrint_2.Name = "txtPrint_2";
			this.txtPrint_2.Shortcut = Wisej.Web.Shortcut.F12;
			this.txtPrint_2.Size = new System.Drawing.Size(156, 48);
			this.txtPrint_2.TabIndex = 0;
			this.txtPrint_2.Text = "Print / Preview";
			this.txtPrint_2.Click += new System.EventHandler(this.txtPrint_Click);
			// 
			// frmMonthlyDogReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(556, 581);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmMonthlyDogReport";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Monthly Dog License Report";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmMonthlyDogReport_Load);
			this.Activated += new System.EventHandler(this.frmMonthlyDogReport_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMonthlyDogReport_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMonthlyDogReport_KeyPress);
			this.Resize += new System.EventHandler(this.frmMonthlyDogReport_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrint_2)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion
    }
}
