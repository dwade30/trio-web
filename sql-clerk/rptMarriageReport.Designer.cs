﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMarriageReport.
	/// </summary>
	partial class rptMarriageReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMarriageReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMarriageDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBride = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMarriageDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBride)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMarriageDate,
				this.txtName,
				this.txtBride,
				this.txtCity
			});
			this.Detail.Height = 0.2708333F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDate,
				this.txtPage,
				this.txtMuni,
				this.txtTime,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field7,
				this.Field8
			});
			this.PageHeader.Height = 0.7395833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 6.25F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0.08333334F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 6.25F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field2";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.25F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1666667F;
			this.txtMuni.Left = 0.08333334F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Text = "Field1";
			this.txtMuni.Top = 0.08333334F;
			this.txtMuni.Width = 2.5F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0.08333334F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 2F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.25F;
			this.Field1.Left = 2.5F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Marriage Report";
			this.Field1.Top = 0.08333334F;
			this.Field1.Width = 2.5F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1979167F;
			this.Field2.Left = 0.01041667F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field2.Text = "Date";
			this.Field2.Top = 0.5416667F;
			this.Field2.Width = 1.041667F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1979167F;
			this.Field3.Left = 1.083333F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field3.Text = "Groom";
			this.Field3.Top = 0.5416667F;
			this.Field3.Width = 2.135417F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1979167F;
			this.Field7.Left = 3.520833F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field7.Text = "Bride";
			this.Field7.Top = 0.5416667F;
			this.Field7.Width = 2.135417F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1979167F;
			this.Field8.Left = 5.927083F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field8.Text = "City";
			this.Field8.Top = 0.5416667F;
			this.Field8.Width = 0.875F;
			// 
			// txtMarriageDate
			// 
			this.txtMarriageDate.Height = 0.1979167F;
			this.txtMarriageDate.Left = 0.01041667F;
			this.txtMarriageDate.Name = "txtMarriageDate";
			this.txtMarriageDate.Style = "font-family: \'Tahoma\'";
			this.txtMarriageDate.Text = null;
			this.txtMarriageDate.Top = 0.04166667F;
			this.txtMarriageDate.Width = 1.041667F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1979167F;
			this.txtName.Left = 1.083333F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = null;
			this.txtName.Top = 0.04166667F;
			this.txtName.Width = 2.385417F;
			// 
			// txtBride
			// 
			this.txtBride.Height = 0.1979167F;
			this.txtBride.Left = 3.520833F;
			this.txtBride.Name = "txtBride";
			this.txtBride.Style = "font-family: \'Tahoma\'";
			this.txtBride.Text = null;
			this.txtBride.Top = 0.04166667F;
			this.txtBride.Width = 2.385417F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.1979167F;
			this.txtCity.Left = 5.927083F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "font-family: \'Tahoma\'";
			this.txtCity.Text = null;
			this.txtCity.Top = 0.04166667F;
			this.txtCity.Width = 1.510417F;
			// 
			// rptMarriageReport
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMarriageDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBride)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarriageDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBride;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
