//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmClerkDefaults.
	/// </summary>
	partial class frmClerkDefaults
	{
		public fecherFoundation.FCComboBox cmbDontImportTags;
		public fecherFoundation.FCLabel lblDontImportTags;
		public fecherFoundation.FCCheckBox chkDefaultAttestedBy;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtMotherState;
		public fecherFoundation.FCTextBox txtMotherCity;
		public fecherFoundation.FCTextBox txtMotherCounty;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtBirthDefaultCity;
		public fecherFoundation.FCTextBox txtBirthDefaultCounty;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCCheckBox chkDontPrintAttested;
		public fecherFoundation.FCCheckBox chkFileNumbers;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCounty;
		public fecherFoundation.FCTextBox txtPhone;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtAgentNumber;
		public fecherFoundation.FCTextBox txtLegalResidence;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaves;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbDontImportTags = new fecherFoundation.FCComboBox();
            this.lblDontImportTags = new fecherFoundation.FCLabel();
            this.chkDefaultAttestedBy = new fecherFoundation.FCCheckBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtMotherState = new fecherFoundation.FCTextBox();
            this.txtMotherCity = new fecherFoundation.FCTextBox();
            this.txtMotherCounty = new fecherFoundation.FCTextBox();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtBirthDefaultCity = new fecherFoundation.FCTextBox();
            this.txtBirthDefaultCounty = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.chkDontPrintAttested = new fecherFoundation.FCCheckBox();
            this.chkFileNumbers = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCounty = new fecherFoundation.FCTextBox();
            this.txtPhone = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtAgentNumber = new fecherFoundation.FCTextBox();
            this.txtLegalResidence = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaves = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultAttestedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDontPrintAttested)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFileNumbers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(790, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbDontImportTags);
            this.ClientArea.Controls.Add(this.lblDontImportTags);
            this.ClientArea.Controls.Add(this.chkDefaultAttestedBy);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.chkDontPrintAttested);
            this.ClientArea.Controls.Add(this.chkFileNumbers);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(790, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(790, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Customize";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbDontImportTags
            // 
            this.cmbDontImportTags.AutoSize = false;
            this.cmbDontImportTags.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDontImportTags.FormattingEnabled = true;
            this.cmbDontImportTags.Items.AddRange(new object[] {
            "Do not import tags",
            "Import tags in inventory",
            "Add missing tags to inventory"});
            this.cmbDontImportTags.Location = new System.Drawing.Point(203, 641);
            this.cmbDontImportTags.Name = "cmbDontImportTags";
            this.cmbDontImportTags.Size = new System.Drawing.Size(291, 40);
            this.cmbDontImportTags.TabIndex = 0;
            this.cmbDontImportTags.Text = "Do not import tags";
            this.ToolTip1.SetToolTip(this.cmbDontImportTags, null);
            // 
            // lblDontImportTags
            // 
            this.lblDontImportTags.AutoSize = true;
            this.lblDontImportTags.Location = new System.Drawing.Point(33, 655);
            this.lblDontImportTags.Name = "lblDontImportTags";
            this.lblDontImportTags.Size = new System.Drawing.Size(158, 15);
            this.lblDontImportTags.TabIndex = 1;
            this.lblDontImportTags.Text = "ONLINE REGISTRATIONS";
            this.ToolTip1.SetToolTip(this.lblDontImportTags, null);
            // 
            // chkDefaultAttestedBy
            // 
            this.chkDefaultAttestedBy.Location = new System.Drawing.Point(30, 604);
            this.chkDefaultAttestedBy.Name = "chkDefaultAttestedBy";
            this.chkDefaultAttestedBy.Size = new System.Drawing.Size(288, 27);
            this.chkDefaultAttestedBy.TabIndex = 34;
            this.chkDefaultAttestedBy.Text = "Default to last saved for Attested By";
            this.ToolTip1.SetToolTip(this.chkDefaultAttestedBy, null);
            // 
            // Frame3
            // 
            this.Frame3.AppearanceKey = "groupBoxLeftBorder";
            this.Frame3.Controls.Add(this.Frame4);
            this.Frame3.Controls.Add(this.Frame2);
            this.Frame3.Location = new System.Drawing.Point(460, 30);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(303, 401);
            this.Frame3.TabIndex = 26;
            this.Frame3.Text = "Live Birth";
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtMotherState);
            this.Frame4.Controls.Add(this.txtMotherCity);
            this.Frame4.Controls.Add(this.txtMotherCounty);
            this.Frame4.Controls.Add(this.Label14);
            this.Frame4.Controls.Add(this.Label13);
            this.Frame4.Controls.Add(this.Label12);
            this.Frame4.Location = new System.Drawing.Point(20, 191);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(264, 190);
            this.Frame4.TabIndex = 30;
            this.Frame4.Text = "Mother";
            this.ToolTip1.SetToolTip(this.Frame4, null);
            // 
            // txtMotherState
            // 
            this.txtMotherState.AutoSize = false;
            this.txtMotherState.BackColor = System.Drawing.SystemColors.Window;
            this.txtMotherState.LinkItem = null;
            this.txtMotherState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMotherState.LinkTopic = null;
            this.txtMotherState.Location = new System.Drawing.Point(103, 130);
            this.txtMotherState.Name = "txtMotherState";
            this.txtMotherState.Size = new System.Drawing.Size(141, 40);
            this.txtMotherState.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtMotherState, null);
            // 
            // txtMotherCity
            // 
            this.txtMotherCity.AutoSize = false;
            this.txtMotherCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtMotherCity.LinkItem = null;
            this.txtMotherCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMotherCity.LinkTopic = null;
            this.txtMotherCity.Location = new System.Drawing.Point(103, 30);
            this.txtMotherCity.Name = "txtMotherCity";
            this.txtMotherCity.Size = new System.Drawing.Size(141, 40);
            this.txtMotherCity.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtMotherCity, null);
            // 
            // txtMotherCounty
            // 
            this.txtMotherCounty.AutoSize = false;
            this.txtMotherCounty.BackColor = System.Drawing.SystemColors.Window;
            this.txtMotherCounty.LinkItem = null;
            this.txtMotherCounty.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMotherCounty.LinkTopic = null;
            this.txtMotherCounty.Location = new System.Drawing.Point(103, 80);
            this.txtMotherCounty.Name = "txtMotherCounty";
            this.txtMotherCounty.Size = new System.Drawing.Size(141, 40);
            this.txtMotherCounty.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtMotherCounty, null);
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 144);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(60, 18);
            this.Label14.TabIndex = 33;
            this.Label14.Text = "STATE";
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 44);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(60, 20);
            this.Label13.TabIndex = 32;
            this.Label13.Text = "CITY";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 94);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 18);
            this.Label12.TabIndex = 31;
            this.Label12.Text = "COUNTY";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtBirthDefaultCity);
            this.Frame2.Controls.Add(this.txtBirthDefaultCounty);
            this.Frame2.Controls.Add(this.Label10);
            this.Frame2.Controls.Add(this.Label11);
            this.Frame2.Location = new System.Drawing.Point(20, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(264, 140);
            this.Frame2.TabIndex = 27;
            this.Frame2.Text = "Default Child Info";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // txtBirthDefaultCity
            // 
            this.txtBirthDefaultCity.AutoSize = false;
            this.txtBirthDefaultCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtBirthDefaultCity.LinkItem = null;
            this.txtBirthDefaultCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtBirthDefaultCity.LinkTopic = null;
            this.txtBirthDefaultCity.Location = new System.Drawing.Point(103, 30);
            this.txtBirthDefaultCity.Name = "txtBirthDefaultCity";
            this.txtBirthDefaultCity.Size = new System.Drawing.Size(141, 40);
            this.txtBirthDefaultCity.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtBirthDefaultCity, null);
            // 
            // txtBirthDefaultCounty
            // 
            this.txtBirthDefaultCounty.AutoSize = false;
            this.txtBirthDefaultCounty.BackColor = System.Drawing.SystemColors.Window;
            this.txtBirthDefaultCounty.LinkItem = null;
            this.txtBirthDefaultCounty.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtBirthDefaultCounty.LinkTopic = null;
            this.txtBirthDefaultCounty.Location = new System.Drawing.Point(103, 80);
            this.txtBirthDefaultCounty.Name = "txtBirthDefaultCounty";
            this.txtBirthDefaultCounty.Size = new System.Drawing.Size(141, 40);
            this.txtBirthDefaultCounty.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtBirthDefaultCounty, null);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 44);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(60, 20);
            this.Label10.TabIndex = 29;
            this.Label10.Text = "CITY";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 94);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(60, 18);
            this.Label11.TabIndex = 28;
            this.Label11.Text = "COUNTY";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // chkDontPrintAttested
            // 
            this.chkDontPrintAttested.Location = new System.Drawing.Point(30, 567);
            this.chkDontPrintAttested.Name = "chkDontPrintAttested";
            this.chkDontPrintAttested.Size = new System.Drawing.Size(286, 27);
            this.chkDontPrintAttested.TabIndex = 15;
            this.chkDontPrintAttested.Text = "Leave Attested blank on certificates";
            this.ToolTip1.SetToolTip(this.chkDontPrintAttested, null);
            // 
            // chkFileNumbers
            // 
            this.chkFileNumbers.Location = new System.Drawing.Point(30, 530);
            this.chkFileNumbers.Name = "chkFileNumbers";
            this.chkFileNumbers.Size = new System.Drawing.Size(317, 27);
            this.chkFileNumbers.TabIndex = 14;
            this.chkFileNumbers.Text = "Warn if record has duplicate file number";
            this.ToolTip1.SetToolTip(this.chkFileNumbers, "Warn if the file number on birth,death and marriage records is used by another re" +
        "cord");
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtCounty);
            this.Frame1.Controls.Add(this.txtPhone);
            this.Frame1.Controls.Add(this.txtAddress2);
            this.Frame1.Controls.Add(this.txtAddress1);
            this.Frame1.Controls.Add(this.txtAgentNumber);
            this.Frame1.Controls.Add(this.txtLegalResidence);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.Label9);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(410, 490);
            this.Frame1.TabIndex = 16;
            this.Frame1.Text = "Town Information";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // txtState
            // 
            this.txtState.AutoSize = false;
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.LinkItem = null;
            this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(156, 180);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(120, 40);
            this.txtState.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtState, null);
            // 
            // txtCounty
            // 
            this.txtCounty.AutoSize = false;
            this.txtCounty.BackColor = System.Drawing.SystemColors.Window;
            this.txtCounty.LinkItem = null;
            this.txtCounty.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCounty.LinkTopic = null;
            this.txtCounty.Location = new System.Drawing.Point(156, 280);
            this.txtCounty.Name = "txtCounty";
            this.txtCounty.Size = new System.Drawing.Size(120, 40);
            this.txtCounty.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtCounty, null);
            this.txtCounty.Enter += new System.EventHandler(this.txtCounty_Enter);
            // 
            // txtPhone
            // 
            this.txtPhone.AutoSize = false;
            this.txtPhone.BackColor = System.Drawing.SystemColors.Window;
            this.txtPhone.LinkItem = null;
            this.txtPhone.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPhone.LinkTopic = null;
            this.txtPhone.Location = new System.Drawing.Point(156, 330);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(120, 40);
            this.txtPhone.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtPhone, null);
            this.txtPhone.Enter += new System.EventHandler(this.txtPhone_Enter);
            // 
            // txtAddress2
            // 
            this.txtAddress2.AutoSize = false;
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.LinkItem = null;
            this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress2.LinkTopic = null;
            this.txtAddress2.Location = new System.Drawing.Point(156, 80);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(234, 40);
            this.txtAddress2.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtAddress2, null);
            this.txtAddress2.Enter += new System.EventHandler(this.txtAddress2_Enter);
            // 
            // txtAddress1
            // 
            this.txtAddress1.AutoSize = false;
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.LinkItem = null;
            this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress1.LinkTopic = null;
            this.txtAddress1.Location = new System.Drawing.Point(156, 30);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(234, 40);
            this.txtAddress1.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtAddress1, null);
            this.txtAddress1.Enter += new System.EventHandler(this.txtAddress1_Enter);
            // 
            // txtAgentNumber
            // 
            this.txtAgentNumber.AutoSize = false;
            this.txtAgentNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtAgentNumber.LinkItem = null;
            this.txtAgentNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAgentNumber.LinkTopic = null;
            this.txtAgentNumber.Location = new System.Drawing.Point(156, 430);
            this.txtAgentNumber.Name = "txtAgentNumber";
            this.txtAgentNumber.Size = new System.Drawing.Size(120, 40);
            this.txtAgentNumber.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtAgentNumber, null);
            this.txtAgentNumber.Visible = false;
            this.txtAgentNumber.Enter += new System.EventHandler(this.txtAgentNumber_Enter);
            // 
            // txtLegalResidence
            // 
            this.txtLegalResidence.AutoSize = false;
            this.txtLegalResidence.BackColor = System.Drawing.SystemColors.Window;
            this.txtLegalResidence.LinkItem = null;
            this.txtLegalResidence.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLegalResidence.LinkTopic = null;
            this.txtLegalResidence.Location = new System.Drawing.Point(156, 380);
            this.txtLegalResidence.Name = "txtLegalResidence";
            this.txtLegalResidence.Size = new System.Drawing.Size(120, 40);
            this.txtLegalResidence.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtLegalResidence, null);
            this.txtLegalResidence.Enter += new System.EventHandler(this.txtLegalResidence_Enter);
            // 
            // txtZip
            // 
            this.txtZip.AutoSize = false;
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.LinkItem = null;
            this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip.LinkTopic = null;
            this.txtZip.Location = new System.Drawing.Point(156, 230);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(120, 40);
            this.txtZip.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtZip, null);
            this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(156, 130);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(120, 40);
            this.txtCity.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtCity, null);
            this.txtCity.Enter += new System.EventHandler(this.txtCity_Enter);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 294);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(92, 16);
            this.Label9.TabIndex = 25;
            this.Label9.Text = "COUNTY";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 344);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(92, 16);
            this.Label8.TabIndex = 24;
            this.Label8.Text = "PHONE";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 94);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(92, 16);
            this.Label7.TabIndex = 23;
            this.Label7.Text = "ADDRESS 2";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(92, 16);
            this.Label6.TabIndex = 22;
            this.Label6.Text = "ADDRESS 1";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 444);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(110, 16);
            this.Label5.TabIndex = 21;
            this.Label5.Text = "AGENT NUMBER";
            this.ToolTip1.SetToolTip(this.Label5, null);
            this.Label5.Visible = false;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 394);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(115, 16);
            this.Label4.TabIndex = 20;
            this.Label4.Text = "LEGAL RESIDENCE";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 244);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(92, 16);
            this.Label3.TabIndex = 19;
            this.Label3.Text = "ZIP";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 194);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(92, 16);
            this.Label2.TabIndex = 18;
            this.Label2.Text = "STATE";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 144);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(92, 16);
            this.Label1.TabIndex = 17;
            this.Label1.Text = "CITY";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaves,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaves
            // 
            this.mnuSaves.Index = 0;
            this.mnuSaves.Name = "mnuSaves";
            this.mnuSaves.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSaves.Text = "Save                     ";
            this.mnuSaves.Click += new System.EventHandler(this.mnuSaves_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 2;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(345, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.mnuSaves_Click);
            // 
            // frmClerkDefaults
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(790, 666);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmClerkDefaults";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Customize";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmClerkDefaults_Load);
            this.Activated += new System.EventHandler(this.frmClerkDefaults_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmClerkDefaults_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmClerkDefaults_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultAttestedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkDontPrintAttested)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFileNumbers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}