//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMonthlyDogReport.
	/// </summary>
	public partial class rptMonthlyDogReport : BaseSectionReport
	{
		public rptMonthlyDogReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Monthly Dog Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMonthlyDogReport InstancePtr
		{
			get
			{
				return (rptMonthlyDogReport)Sys.GetInstance(typeof(rptMonthlyDogReport));
			}
		}

		protected rptMonthlyDogReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
				clsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		
		object strTemp;
		int intDog1;
		int intDog2;
		int intKennel1;
		int intKennel2;
		int intNeuter1;
		int intNeuter2;
		int intCounter;
		int intTotal;
		int intCase;
		clsDRWrapper rs = new clsDRWrapper();
		double dblKennelLic;
		// vbPorter upgrade warning: dblStateKennelLic As double	OnWrite(string)
		double dblStateKennelLic;
		// vbPorter upgrade warning: dblFixedLic As double	OnWriteFCConvert.ToDecimal(
		double dblFixedLic;
		// vbPorter upgrade warning: dblStateFixedLic As double	OnWrite(string)
		double dblStateFixedLic;
		// vbPorter upgrade warning: dblUnfixedLic As double	OnWriteFCConvert.ToDecimal(
		double dblUnfixedLic;
		// vbPorter upgrade warning: dblStateUnfixedLic As double	OnWrite(string)
		double dblStateUnfixedLic;
		int lngYearToUse;
		clsDRWrapper clsTemp = new clsDRWrapper();
		int lngTowncode;

		private void FillInData()
		{
			modDogs.GetDogFees();
			if (modGNBas.Statics.gstrMonthYear == string.Empty)
			{
				modGNBas.Statics.gstrMonthYear = FCConvert.ToString(DateTime.Today.Month) + "/" + FCConvert.ToString(DateTime.Today.Year);
			}
			if (modGNBas.Statics.boolRegionalTown)
			{
				lngTowncode = FCConvert.ToInt32(Math.Round(Conversion.Val(frmMonthlyDogReport.InstancePtr.gridTownCode.TextMatrix(0, 0))));
			}
			else
			{
				lngTowncode = 0;
			}
			dblFixedLic = FCConvert.ToDouble(modDogs.Statics.DogFee.Fixed_Fee);
			dblUnfixedLic = FCConvert.ToDouble(modDogs.Statics.DogFee.UN_Fixed_FEE);
			dblKennelLic = modDogs.Statics.DogFee.KennelClerk + modDogs.Statics.DogFee.KennelState + modDogs.Statics.DogFee.KennelTown;
			dblStateKennelLic = FCConvert.ToDouble(Strings.Format(modDogs.Statics.DogFee.KennelState, "0.00"));
			dblStateUnfixedLic = FCConvert.ToDouble(Strings.Format(modDogs.Statics.DogFee.DogState, "0.00"));
			dblStateFixedLic = FCConvert.ToDouble(Strings.Format(modDogs.Statics.DogFee.FixedState, "0.00"));
			strTemp = Strings.Split(modGNBas.Statics.gstrMonthYear, "/", -1, CompareConstants.vbBinaryCompare);

			if ((Conversion.Val(((object[])strTemp)[1]) + 1 > DateTime.Today.Year) || (Conversion.Val(((object[])strTemp)[0]) > DateTime.Today.Month))
			{
				lngYearToUse = FCConvert.ToInt32(((object[])strTemp)[1]);
			}
			else
			{
				lngYearToUse = FCConvert.ToInt32(((object[])strTemp)[1]) + 1;
			}
			rs.OpenRecordset("select count(Transactionnumber) as MaleFemale From dogtransactions where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1 and (boolkennel <> 1) and (boolSpayNeuter <> 1)  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
			txtMF1.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("MaleFemale")) != 0)
				{
					txtMF1.Text = rs.Get_Fields_String("MaleFemale");
				}
			}
			rs.OpenRecordset("select count(Transactionnumber) as MaleFemale From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1 and (boolkennel <> 1) and (boolSpayNeuter <> 1)  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
			txtMF2.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("MaleFemale")) != 0)
				{
					txtMF2.Text = FCConvert.ToString(rs.Get_Fields("MaleFemale"));
				}
			}
			rs.OpenRecordset("select count(Transactionnumber) as Neutered From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1 and (boolkennel <> 1) and  boolSpayNeuter = 1  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
			txtNeuter1.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("Neutered")) != 0)
				{
					txtNeuter1.Text = rs.Get_Fields_String("Neutered");
				}
			}
			rs.OpenRecordset("select count(Transactionnumber) as Neutered From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1 and (boolkennel <> 1) and  boolSpayNeuter = 1  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
			txtNeuter2.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("Neutered")) != 0)
				{
					txtNeuter2.Text = rs.Get_Fields_String("Neutered");
				}
			}
			intTotal = 0;
			rs.OpenRecordset("select count(ID) as Thecount from dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boolkennel = 1 and boollicense = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
			txtKennelTotal.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("thecount")) > 0)
				{
					intTotal = Math.Round(Conversion.Val(rs.Get_Fields("thecount")));
					txtKennelTotal.Text = intTotal.ToString();
				}
			}
			// now go through each license and see how many dogs etc.
			int lngKDogsYr1;
			int lngKDogsYr2;
			int lngKDogsAddedYr1;
			int lngKDogsAddedYr2;
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
			DateTime dtDate;
			DateTime dtIssueDate;
			// vbPorter upgrade warning: dtTemp1 As DateTime	OnWrite(string)
			DateTime dtTemp1;
			DateTime dtTemp2;
			string[] strAry = null;
			string strDogs = "";
			// vbPorter upgrade warning: intNumDogs As int	OnWriteFCConvert.ToInt32(
			int intNumDogs = 0;
			int x;
			bool boolAddedToKennel = false;
			lngKDogsYr1 = 0;
			lngKDogsYr2 = 0;
			lngKDogsAddedYr1 = 0;
			lngKDogsAddedYr2 = 0;
			txtKennel1.Text = "";
			txtKennel2.Text = "";
			txtDogKennel1.Text = "";
			txtDogKennel2.Text = "";
			rs.OpenRecordset("select reissuedate,kennellicense.dognumbers as dognumbers from kennellicense inner join dogowner on (kennellicense.ownernum = dogowner.ID) where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode), "twck0000.vb1");
			while (!rs.EndOfFile())
			{
				dtIssueDate = (DateTime)rs.Get_Fields_DateTime("reissuedate");
				strDogs = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("dognumbers")));
				if (strDogs != string.Empty)
				{
					strAry = Strings.Split(strDogs, ",", -1, CompareConstants.vbTextCompare);
					// now we have a list of the dogs in the kennel
					intNumDogs = (Information.UBound(strAry, 1) + 1);
					for (x = 0; x <= intNumDogs - 1; x++)
					{
						// for each dog check
						clsTemp.OpenRecordset("select dogtokenneldate,[year] from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[x])), "twck0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							boolAddedToKennel = false;
							if (Information.IsDate(clsTemp.Get_Fields("dogtokenneldate")))
							{
								if (Convert.ToDateTime(clsTemp.Get_Fields_DateTime("dogtokenneldate")).ToOADate() != 0)
								{
									dtDate = FCConvert.ToDateTime(Strings.Format(clsTemp.Get_Fields_DateTime("dogtokenneldate"), "MM/dd/yyyy"));
									if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, dtIssueDate) < 0)
									{
										boolAddedToKennel = true;
									}
								}
							}
							dtTemp1 = FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse));
							dtTemp2 = fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))));
							if (Conversion.Val(clsTemp.Get_Fields("year")) == FCConvert.ToInt16(((object[])strTemp)[1]))
							{
								if (boolAddedToKennel)
								{
									lngKDogsAddedYr1 += 1;
								}
								else
								{
									if ((fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtIssueDate) >= 0) && (fecherFoundation.DateAndTime.DateDiff("d", dtTemp2, dtIssueDate) <= 0))
									{
										lngKDogsYr1 += 1;
									}
								}
							}
							else if (Conversion.Val(clsTemp.Get_Fields("year")) == FCConvert.ToInt16(((object[])strTemp)[1]) + 1)
							{
								if (boolAddedToKennel)
								{
									lngKDogsAddedYr2 += 1;
								}
								else
								{
									if ((fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtIssueDate) >= 0) && (fecherFoundation.DateAndTime.DateDiff("d", dtTemp2, dtIssueDate) <= 0))
									{
										lngKDogsYr2 += 1;
									}
								}
							}
						}
					}
					// x
				}
				rs.MoveNext();
			}
			if (lngKDogsYr1 > 0)
			{
				txtKennel1.Text = FCConvert.ToString(lngKDogsYr1);
			}
			if (lngKDogsYr2 > 0)
			{
				txtKennel2.Text = FCConvert.ToString(lngKDogsYr2);
			}
			if (lngKDogsAddedYr1 > 0)
			{
				txtDogKennel1.Text = FCConvert.ToString(lngKDogsAddedYr1);
			}
			if (lngKDogsAddedYr2 > 0)
			{
				txtDogKennel2.Text = FCConvert.ToString(lngKDogsAddedYr2);
			}
			// replacement stickers
			rs.OpenRecordset("select count(Transactionnumber) as replacementtotal From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boollicense <> 1) and (boolkennel <> 1)   and boolreplacementsticker = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
			txtReplacement1.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("ReplacementTotal")) != 0)
				{
					txtReplacement1.Text = rs.Get_Fields_String("ReplacementTotal");
				}
			}
			rs.OpenRecordset("select count(Transactionnumber) as replacementtagtotal From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and boolreplacementtag = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
			txtReplacementTag.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("ReplacementTagTotal")) != 0)
				{
					txtReplacementTag.Text = rs.Get_Fields_String("ReplacementTagTotal");
				}
				else
				{
					txtReplacementTag.Text = "0";
				}
			}
			else
			{
				txtReplacementTag.Text = "0";
			}
			rs.OpenRecordset("select count(transactionnumber) as newtag from dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1) and oldtagnumber = 0 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
            rs.OpenRecordset(
                "select count(transactionnumber) as newtag from dogtransactions where online <> 1 and convert(int,isnull(towncode,0)) = " +
                lngTowncode + " and (boolKennel <> 1) and isnull(oldtagnumber,'') = '' and transactiondate between '" +
                ((object[]) strTemp)[0] + "/01/" + lngYearToUse + "' and '" + FCConvert.ToString(
                    fecherFoundation.DateAndTime.DateAdd("D", -1,
                        fecherFoundation.DateAndTime.DateAdd("M", 1,
                            FCConvert.ToDateTime(((object[]) strTemp)[0] + "/01/" +
                                                 lngYearToUse)))) + "'", "Clerk");

            txtNewTag.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("NewTag")) != 0)
				{
					txtNewTag.Text = rs.Get_Fields_String("NewTag");
				}
				else
				{
					txtNewTag.Text = "0";
				}
			}
			else
			{
				txtNewTag.Text = "0";
			}
			txtTagOther.Text = "0";
			txtTagTotal.Text = FCConvert.ToString(Conversion.Val(txtReplacementTag.Text) + Conversion.Val(txtNewTag.Text) + Conversion.Val(txtTagOther.Text));
			rs.OpenRecordset("select count(Transactionnumber) as replacementtotal From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boollicense <> 1) and (boolkennel <> 1)   and boolreplacementsticker = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
			txtReplacement2.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("ReplacementTotal")) != 0)
				{
					txtReplacement2.Text = rs.Get_Fields_String("ReplacementTotal");
				}
			}
			// search and rescue
			rs.OpenRecordset("SELECT count(doginfo.ID) as SandRTotal FROM DogInfo inner join dogowner on (dogowner.ID = doginfo.ownernum) where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and DogInfo.RabStickerIssue >='" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND SandR = 1 and Year = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), modGNBas.DEFAULTDATABASE);
			txtSandR1.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("SandRTotal")) != 0)
				{
					txtSandR1.Text = rs.Get_Fields_String("SandRTotal");
				}
			}
			rs.OpenRecordset("SELECT count(doginfo.ID) as SandRTotal FROM DogInfo  inner join dogowner on (dogowner.ID = doginfo.ownernum) where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and DogInfo.RabStickerIssue >='" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND SandR = 1 and Year = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), modGNBas.DEFAULTDATABASE);
			txtSandR2.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("SandRTotal")) != 0)
				{
					txtSandR2.Text = rs.Get_Fields_String("SandRTotal");
				}
			}
			// Hearing/Guide
			rs.OpenRecordset("SELECT count(doginfo.ID) as HearGuideTotal FROM DogInfo  inner join dogowner on (dogowner.ID = doginfo.ownernum) where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and DogInfo.RabStickerIssue >='" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND HearGuide = 1 and Year = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), modGNBas.DEFAULTDATABASE);
			txtHearingGuide1.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("HearGuideTotal")) != 0)
				{
					txtHearingGuide1.Text = rs.Get_Fields_String("HearGuideTotal");
				}
			}
			rs.OpenRecordset("SELECT count(doginfo.ID) as HearGuideTotal FROM DogInfo  inner join dogowner on (dogowner.ID = doginfo.ownernum) where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and DogInfo.RabStickerIssue >='" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND HearGuide = 1 and Year = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), modGNBas.DEFAULTDATABASE);
			txtHearingGuide2.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("HearGuideTotal")) != 0)
				{
					txtHearingGuide2.Text = rs.Get_Fields_String("HearGuideTotal");
				}
			}
			// Transfers
			rs.OpenRecordset("select count(Transactionnumber) as transferstotal From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and booltransfer = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
			txtTransfers1.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("TransfersTotal")) != 0)
				{
					txtTransfers1.Text = rs.Get_Fields_String("TransfersTotal");
				}
			}
			rs.OpenRecordset("select count(Transactionnumber) as transferstotal From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and booltransfer = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
			txtTransfers2.Text = string.Empty;
			if (!rs.EndOfFile())
			{
				if (Conversion.Val(rs.Get_Fields("TransfersTotal")) != 0)
				{
					txtTransfers2.Text = rs.Get_Fields_String("TransfersTotal");
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
				FillInData();
				// Call SetPrintProperties(Me)
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Public Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports2.DDTool)
		// Call VerifyPrintToFile(Me, Tool)
		// End Sub
		private void PageHeader_BeforePrint(object sender, EventArgs e)
		{
			string strState;
			//clsDRWrapper rsState = new clsDRWrapper();
			txtAttestedBy.Text = frmMonthlyDogReport.InstancePtr.txtClerkName;
			txtMonth.Text = Strings.Left(modGNBas.Statics.gstrMonthYear, 2);
			if (!modGNBas.Statics.boolRegionalTown)
			{
				txtMuniname.Text = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				lngTowncode = FCConvert.ToInt32(Math.Round(Conversion.Val(frmMonthlyDogReport.InstancePtr.gridTownCode.TextMatrix(0, 0))));
				txtMuniname.Text = frmMonthlyDogReport.InstancePtr.gridTownCode.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 0, 0);
			}
			txtAddress1.Text = modGNBas.Statics.ClerkDefaults.Address1;
			strState = modGNBas.Statics.ClerkDefaults.State;
			txtAddress2.Text = modGNBas.Statics.ClerkDefaults.City + ", " + strState + " " + modGNBas.Statics.ClerkDefaults.Zip;
			txtPhone.Text = modGNBas.Statics.ClerkDefaults.Phone;
			txtMFTotal.Text = FCConvert.ToString(Conversion.Val(txtMF1.Text) + Conversion.Val(txtMF2.Text));
			if (txtMFTotal.Text == string.Empty)
				txtMFTotal.Text = "0";
			txtMFSummary.Text = Strings.Format(FCConvert.ToDouble(txtMFTotal.Text) * dblStateUnfixedLic, "###,###,##0.00");
			if (txtMFSummary.Text == "0")
				txtMFSummary.Text = string.Empty;
			txtNeuterTotal.Text = FCConvert.ToString(Conversion.Val(txtNeuter1.Text) + Conversion.Val(txtNeuter2.Text));
			if (Conversion.Val(txtNeuterTotal.Text) == 0)
			{
				txtNeuterTotal.Text = string.Empty;
			}
			else
			{
				txtNeuterSummary.Text = Strings.Format(FCConvert.ToDouble(txtNeuterTotal.Text) * dblStateFixedLic, "###,###,##0.00");
			}
			txtKennelSummary.Text = Strings.Format(Conversion.Val(txtKennelTotal.Text) * dblStateKennelLic, "###,###,##0.00");
			if (txtKennelSummary.Text == "0")
				txtKennelSummary.Text = string.Empty;
			// txtCorrections1 = frmMonthlyDogReport.txtYear1
			// txtCorrections2 = frmMonthlyDogReport.txtYear2
			txtReason.Text = frmMonthlyDogReport.InstancePtr.txtReason.Text;
			txtAdjustments.Text = frmMonthlyDogReport.InstancePtr.txtAdjustments.Text;
			// lblStickerYear1 = Right(frmMonthlyDogReport.txtYearToReport, 2)
			// lblYear1 = frmMonthlyDogReport.txtYearToReport
			// lblStickerYear2 = Right(Val(frmMonthlyDogReport.txtYearToReport) + 1, 2)
			// lblYear2 = Val(frmMonthlyDogReport.txtYearToReport) + 1
			txtReportTotal.Text = Strings.Format(Conversion.Val(txtMFSummary.Text) + Conversion.Val(txtNeuterSummary.Text) + Conversion.Val(txtKennelSummary.Text) - Conversion.Val(txtAdjustments.Text), "###,###,##0.00");
			if (txtReportTotal.Text == "0")
				txtReportTotal.Text = string.Empty;
			txtDogTotal1.Text = Strings.Format(Conversion.Val(txtReplacement1.Text) + Conversion.Val(txtSandR1.Text) + Conversion.Val(txtHearingGuide1.Text) + Conversion.Val(txtTransfers1.Text) + Conversion.Val(txtDogKennel1.Text) + Conversion.Val(txtMF1.Text) + Conversion.Val(txtNeuter1.Text) + Conversion.Val(txtKennel1.Text) - Conversion.Val(txtCorrections1.Text), "###,###,##0");
			if (txtDogTotal1.Text == "0")
				txtDogTotal1.Text = string.Empty;
			txtDogTotal2.Text = Strings.Format(Conversion.Val(txtReplacement2.Text) + Conversion.Val(txtSandR2.Text) + Conversion.Val(txtHearingGuide2.Text) + Conversion.Val(txtTransfers2.Text) + Conversion.Val(txtDogKennel2.Text) + Conversion.Val(txtMF2.Text) + Conversion.Val(txtNeuter2.Text) + Conversion.Val(txtKennel2.Text) - Conversion.Val(txtCorrections2.Text), "###,###,##0");
			if (txtDogTotal2.Text == "0")
				txtDogTotal2.Text = string.Empty;
		}

		
	}
}
