//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmChooseDeath : BaseForm
	{
		public frmChooseDeath()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmChooseDeath InstancePtr
		{
			get
			{
				return (frmChooseDeath)Sys.GetInstance(typeof(frmChooseDeath));
			}
		}

		protected frmChooseDeath _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLID = 0;
		const int CNSTGRIDCOLFIRSTNAME = 1;
		const int CNSTGRIDCOLMIDDLENAME = 2;
		const int CNSTGRIDCOLLASTNAME = 3;
		const int CNSTGRIDCOLDESIGNATION = 4;
		private int lngReturn;

		public int Init()
		{
			int Init = 0;
			lngReturn = -1;
			Init = -1;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void frmChooseDeath_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						if (Grid.Row > 1)
						{
							if (SaveData())
							{
								Close();
							}
						}
						break;
					}
			}
			//end switch
		}

		private void frmChooseDeath_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseDeath properties;
			//frmChooseDeath.FillStyle	= 0;
			//frmChooseDeath.ScaleWidth	= 9300;
			//frmChooseDeath.ScaleHeight	= 7620;
			//frmChooseDeath.LinkTopic	= "Form2";
			//frmChooseDeath.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void frmChooseDeath_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				Close();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			SaveData = false;
			if (Grid.Row < 1)
				return SaveData;
			lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID))));
			SaveData = true;
			return SaveData;
		}

		private void SetupGrid()
		{
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLFIRSTNAME, "First");
			Grid.TextMatrix(0, CNSTGRIDCOLMIDDLENAME, "Middle");
			Grid.TextMatrix(0, CNSTGRIDCOLLASTNAME, "Last");
			Grid.TextMatrix(0, CNSTGRIDCOLDESIGNATION, "Des.");
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				int lngRow;
				Grid.Rows = 1;
				strSQL = "Select ID,lastname,firstname,middlename,designation from deaths where ID not in (select ID from burialpermitmaster) order by lastname,firstname";
				rsLoad.OpenRecordset(strSQL, "twck0000.vb1");
				Grid.Rows = 2;
				lngRow = 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLID, FCConvert.ToString(0));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLFIRSTNAME, "None");
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLID, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ID"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFIRSTNAME, FCConvert.ToString(rsLoad.Get_Fields_String("FirstName")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLMIDDLENAME, FCConvert.ToString(rsLoad.Get_Fields_String("MiddleName")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLASTNAME, FCConvert.ToString(rsLoad.Get_Fields_String("lastname")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDESIGNATION, FCConvert.ToString(rsLoad.Get_Fields_String("designation")));
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLFIRSTNAME, FCConvert.ToInt32(0.28 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLMIDDLENAME, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLLASTNAME, FCConvert.ToInt32(0.28 * GridWidth));
		}
	}
}
