﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;

namespace TWCK0000
{
	public class modGNBas
	{
		//=========================================================
		// These Declares are used to get the block cursor
		public const int Main = 1;
		public const int MARRIAGES = 2;
		public const int BIRTHS = 3;
		public const int DEATHS = 4;
		public const int DOGS = 5;
		public const int WILDLIFE = 6;
		public const int FILEMAINTENANCE = 7;
		public const int REPORTS = 8;
		public const int CNSTVOIDDOGTRANSACTIONS = 9;
		public const int CNSTVOIDOWNDOGTRANSACTIONS = 10;
		public const int CNSTEDITTAGSTICKERINVENTORY = 11;
		public const int CNSTEDITDOGSKENNELS = 12;
		public const int CNSTEDITOWNERSSCREEN = 13;
		public const int CNSTEDITANYTRANSACTIONS = 14;
		public const int CNSTEDITOWNTRANSACTIONS = 16;
		public const int CNSTONLINEDOGTRANSACTIONS = 15;
		public const int CNSTTYPEDOGS = 1;
		public const int CNSTTYPEKENNELS = 2;
		public const int CNSTTYPEBIRTHS = 3;
		public const int CNSTTYPEDEATHS = 4;
		public const int CNSTTYPEBURIALS = 5;
		public const int CNSTTYPEMARRIAGES = 6;
		public const int CNSTMARRIAGEPARTYTYPESPOUSE = 0;
		public const int CNSTMARRIAGEPARTYTYPEBRIDE = 1;
		public const int CNSTMARRIAGEPARTYTYPEGROOM = 2;
		public const int GRIDROWS = 19;
		public const string DEFAULTDATABASE = "Twck0000.vb1";
		// Public clsSecurityClass As clsTrioSecurity
		public const int DOGTRANSACTION = 1;
		public const int KENNELTRANSACTION = 2;
		public const int BIRTHTRANSACTION = 3;
		public const int DEATHTRANSACTION = 4;
		public const int BURIALTRANSACTION = 5;
		public const int MARRIAGETRANSACTION = 6;
		public const int CNSTMANNEROFDEATHBLANK = 0;
		public const int CNSTMANNEROFDEATHNATURAL = 1;
		public const int CNSTMANNEROFDEATHACCIDENT = 2;
		public const int CNSTMANNEROFDEATHSUICIDE = 3;
		public const int CNSTMANNEROFDEATHHOMICIDE = 4;
		public const int CNSTMANNEROFDEATHPENDINGINVESTIGATION = 5;
		public const int CNSTMANNEROFDEATHCOULDNOTBEDETERMINED = 6;

		[DllImport("user32")]
		public static extern int CreateCaret(int hwnd, int hBitmap, int nWidth, int nHeight);

		[DllImport("user32")]
		public static extern int ShowCaret(int hwnd);

		[DllImport("user32")]
		public static extern int GetFocus();

		[DllImport("user32")]
		public static extern object SetCursorPos(int x, int y);
		// screen setup items
		const string Logo = "TRIO Software - Clerk ";
		public const string SearchInputBoxTitle = "Real Estate Assessment Search";

		[DllImport("kernel32")]
		public static extern void Sleep(int dwMilliseconds);

		[DllImport("kernel32")]
		public static extern int WinExec(string lpCmdLine, int nCmdShow);

		[DllImport("kernel32")]
		public static extern int GlobalAlloc(int wFlags, int dwBytes);

		public struct typDogSummaryRecord
        {
            public int lngDangerous1;
            public int lngDangerous2;
			public int lngMF1;
			public int lngMF2;
			public int lngNeuter1;
			public int lngNeuter2;
            public int lngNuisance1;
            public int lngNuisance2;
			public int lngKennelSticker1;
			public int lngKennelSticker2;
			public int lngKennelLicenses;
			// vbPorter upgrade warning: lngReplacement1 As int	OnWrite(int, double)
			public int lngReplacement1;
			// vbPorter upgrade warning: lngReplacement2 As int	OnWrite(int, double)
			public int lngReplacement2;
			// vbPorter upgrade warning: lngSearchRescue1 As int	OnWrite(int, double)
			public int lngSearchRescue1;
			// vbPorter upgrade warning: lngSearchRescue2 As int	OnWrite(int, double)
			public int lngSearchRescue2;
			// vbPorter upgrade warning: lngHearingGuide1 As int	OnWrite(int, double)
			public int lngHearingGuide1;
			// vbPorter upgrade warning: lngHearingGuide2 As int	OnWrite(int, double)
			public int lngHearingGuide2;
			// vbPorter upgrade warning: lngTransfer1 As int	OnWrite(int, double)
			public int lngTransfer1;
			// vbPorter upgrade warning: lngTransfer2 As int	OnWrite(int, double)
			public int lngTransfer2;
			public int lngAddedToKennel1;
			public int lngAddedToKennel2;
			public int lngTotalSticker1;
			public int lngTotalSticker2;
			// vbPorter upgrade warning: lngNewTag As int	OnWrite(int, double)
			public int lngNewTag;
			// vbPorter upgrade warning: lngReplacementTag As int	OnWrite(int, double)
			public int lngReplacementTag;
			public int lngOtherTag;
			public int lngTotalTag;
		};

		public struct typTransactionRecord
		{
			public double TotalAmount;
			public double Amount1;
			public double Amount2;
			public double Amount3;
			public double Amount4;
			public double Amount5;
			public double Amount6;
			// vbPorter upgrade warning: TransactionDate As DateTime	OnWrite(string, DateTime)
			public DateTime TransactionDate;
			// vbPorter upgrade warning: TransactionNumber As int	OnRead(int, FixedString)
			public int TransactionNumber;
			public int TransactionType;
			public string Description;
			public int UserID;
			public string UserName;
			public string TransactionPerson;
			public int TownCode;
		};

		public struct typDogTransactionRecord
		{
			public int TransactionNumber;
			public int ID;
			public string Description;
			public DateTime TransactionDate;
			public bool boolAdjustmentVoid;
			public int OwnerNum;
			public int DogNumber;
			public string KennelDogs;
			public bool boolLicense;
			public bool boolKennel;
			public int KennelLicenseID;
			public double Amount;
			public double StateFee;
			public double TownFee;
			public double ClerkFee;
			public double LateFee;
			public int DogYear;
			public double KennelWarrantFee;
			public double KennelLateFee;
			public double KennelLic1Fee;
			public double KennelLic2Fee;
			public int KennelLic1Num;
			public int KennelLic2Num;
			public double ReplacementStickerFee;
			public bool boolReplacementSticker;
			public double ReplacementLicenseFee;
			public bool boolReplacementLicense;
			public double ReplacementTagFee;
			public bool boolReplacementTag;
			public bool boolSpayNeuter;
			public double SpayNonSpayAmount;
			public bool boolSearchRescue;
			public bool boolHearingGuid;
			public bool boolTransfer;
			public double TransferLicenseFee;
			public bool boolTempLicense;
			public double TempLicenseFee;
			public string TAGNUMBER;
			public int StickerNumber;
			public string RabiesTagNumber;
			public int OldStickerNumber;
			public string OldTagNumber;
			// vbPorter upgrade warning: OldIssueDate As DateTime	OnWrite(DateTime, int)
			public DateTime OldIssueDate;
			public int OldYear;
			public double AnimalControl;
			// vbPorter upgrade warning: WarrantFee As double	OnWrite(int, Decimal, double)
			public double WarrantFee;
			public int TownCode;
            public bool IsDangerous;
            public bool IsNuisance;
        };

		public struct typOldVitals
		{
			public string AttestBy;
			public string FileNumber;
			public string pageNumber;
			public string FamilyName;
			public string ChildName1;
			public string ChildName2;
			public string ChildName3;
			public string DateOfEvent;
			public string Parent1;
			public string Parent2;
			public string Parent3;
			public string Parent4;
			public string PlaceOfEvent;
			public string PlaceOfResidence;
			public string NameOfClerk1;
			public string NameOfClerk2;
			public string TownOfClerk1;
			public string TownOfClerk2;
			public string DateTo;
			public string DateFrom;
			public string Notes;
		};

		public struct typConsent
		{
			public bool IncarceratedGroom;
			public bool IncarceratedBride;
			public bool GroomHospitalized;
			public bool BrideHospitalized;
			public bool WaiverGroom;
			public bool WaiverBride;
			public string GroomName;
			public string BrideName;
			public string DateOfConsent;
			public string ClerkName;
			public string TownName;
			public string CountyOfCommission;
			public string DateCommissionExpires;
			public string Notes;
			public bool NeedConsent;
			public bool Groom;
			public bool Bride;
			public string MarriageDate;
		};

		public struct ControlProportions
		{
			public float WidthProportions;
			public float HeightProportions;
			public float TopProportions;
			public float LeftProportions;
		};
		// Public Type ClerkDefault
		// Address1        As String
		// Address2        As String
		// Phone           As String
		// AgentNumber     As String
		// City            As String
		// State           As String
		// Zip             As String
		// County          As String
		// LegalResidence  As String
		// End Type
		public const string DEFAULTCLERKDATABASE = "TWCK0000.vb1";

		public static void GetPaths()
		{
			Statics.PPDatabase = "P:\\";
			Statics.TrioEXE = "\\T2K\\";
			Statics.TrioDATA = "\\VBTRIO\\VBTEST\\";
			Statics.PictureFormat = ".BMP";
			Statics.SketchLocation = "\\VBTRIO\\VBTEST\\";
			Statics.Display = "TRIOTEMP.TXT";
		}

		public static void ConvertToString()
		{
			Statics.IntVarTemp = Conversion.Str(Statics.IntvarNumeric);
			Statics.INTVARSTRING = "";
			Statics.IntVarTemp = fecherFoundation.Strings.LTrim(Statics.IntVarTemp);
			Statics.INTVARSTRING = Strings.StrDup(Statics.intLen - Statics.IntVarTemp.Length, "0") + Statics.IntVarTemp;
		}

		public static void MakeCaption()
		{
			Statics.TitleCaption = "TRIO Software - Clerk ";
			Statics.TitleTime = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm");
			if (Strings.Mid(Statics.TitleTime, 1, 1) == "0")
				Strings.MidSet(ref Statics.TitleTime, 1, 1, " ");
		}
		// Public Sub ErrorRoutine()
		// Dim answer
		// Screen.MousePointer = 0
		// If Err = 24 Or Err = 25 Or Err = 27 Or Err = 71 Then
		// answer = MsgBox("Printer appears to not be available", vbOKCancel)
		// If Not vbCancel Then
		// MsgBox "Processing-------"
		// Else
		// Bbort$ = "Y"
		// End If
		// ElseIf gstrNetworkFlag <> "N" And Err > 69 And Err < 71 Then
		// GoSub Err_9999_NET
		// Else
		// MsgBox "  Error Number " & Err & "  " & Err.Description & vbNewLine & "  Has been encountered - Program Terminated"
		// Bbort$ = "Y"
		// End If
		// If EntryMenuFlag = False Then
		// Call WriteYY
		// Shell "TWGNENTY.EXE", vbNormalFocus
		// End
		// End If
		// End
		// Err_9999_NET:
		// answer = MsgBox("Record is locked - Do you want to retry", vbYesNo)
		// If answer = vbNo Then Bbort$ = "Y"
		// Return
		// End Sub
		public static void PrintFormat()
		{
			// vbPorter upgrade warning: fmtextraspc As int	OnWriteFCConvert.ToInt32(
			int fmtextraspc;
			// vbPorter upgrade warning: Formatted As object	OnWrite(string)
			object Formatted = null;
			if (Statics.fmttype == "Whole")
			{
				Formatted = Strings.Format(Statics.fmtval, "###0");
			}
			else if (Statics.fmttype == "WholeC")
			{
				Formatted = Strings.Format(Statics.fmtval, "#,##0");
			}
			else if (Statics.fmttype == "Dollar")
			{
				Formatted = Strings.Format(Statics.fmtval / 100.0, "0.00");
			}
			else if (Statics.fmttype == "DollarC")
			{
				Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0.00");
			}
			else if (Statics.fmttype == "WholeS")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "###0-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "###0+");
				}
			}
			else if (Statics.fmttype == "WholeSC")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "#,##0-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0+");
				}
			}
			else if (Statics.fmttype == "DollarS")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "0.00-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "0.00+");
				}
			}
			else if (Statics.fmttype == "DollarSC")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "#,##0.00-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0.00+");
				}
			}
			fmtextraspc = 0;
			if (FCConvert.ToString(Formatted).Length < Statics.fmtlgth)
				fmtextraspc = (Statics.fmtlgth - FCConvert.ToString(Formatted).Length);
			if (fmtextraspc != 0)
				FCFileSystem.Print(40, "");
			if (Statics.fmtspcbef != 0)
				FCFileSystem.Print(40, "");
			FCFileSystem.Print(40, Formatted.ToStringES());
			if (Statics.fmtspcaft != 0)
				FCFileSystem.Print(40, "");
			if (Statics.fmtcolon != "Y")
				FCFileSystem.PrintLine(40, " ");
		}

		public static void CheckNumeric()
		{
			for (Statics.x = 1; Statics.x <= (Statics.Resp1.Length); Statics.x++)
			{
				Statics.Resp2 = Convert.ToByte(Strings.Mid(Statics.Resp1, Statics.x, 1)[0]);
				if (Statics.Resp2 < 48 || Statics.Resp2 > 57)
				{
					Strings.MidSet(ref Statics.Resp1, Statics.x, Strings.Len(Statics.Resp1) - Statics.x, Strings.Mid(Statics.Resp1, Statics.x + 1, Statics.Resp1.Length - 1));
					Strings.MidSet(ref Statics.Resp1, Strings.Len(Statics.Resp1), 1, " ");
					break;
				}
			}
			// x
		}
		// vbPorter upgrade warning: intValue As object	OnWrite
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static string PadToString(int intValue, int intLength)
		{
			string PadToString = null;
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			return PadToString;
		}

		public static object SetInsertMode(int InsertMode)
		{
			object SetInsertMode = null;
			// 0 = Insert Mode        = SelLength of 0
			// 1 = OverType Mode      = SelLength of 1
			// The Variable "OverType" returns either 0 or 1 for SelLength
			// 
			if ((FCGlobal.Screen.ActiveControl as FCTextBox).SelectionLength == 1)
			{
				Statics.OverType = 0;
			}
			else
			{
				Statics.OverType = 1;
			}
			return SetInsertMode;
		}

		public static void GetValue()
		{
			// vbPorter upgrade warning: y As int	OnWriteFCConvert.ToInt32(
			int y;
			// vbPorter upgrade warning: yy As int	OnWriteFCConvert.ToInt32(
			int yy;
			Statics.NewData = fecherFoundation.Strings.Trim(Statics.NewData);
			for (y = 1; y <= (Statics.NewData.Length); y++)
			{
				if (Strings.Mid(Statics.NewData, y, 1) == "," || Strings.Mid(Statics.NewData, y, 1) == ".")
				{
					for (yy = y; yy <= (Statics.NewData.Length - 1); yy++)
					{
						Strings.MidSet(ref Statics.NewData, yy, 1, Strings.Mid(Statics.NewData, yy + 1, 1));
					}
					// yy
					Strings.MidSet(ref Statics.NewData, Strings.Len(Statics.NewData), 1, " ");
				}
				else if (fecherFoundation.Strings.UCase(Strings.Mid(Statics.NewData, y, 1)) == "C")
				{
					Statics.NewData = "0";
					break;
				}
			}
			// y
			Statics.NewValue = FCConvert.ToInt32(Math.Round(Conversion.Val(Statics.NewData)));
		}

		public static double Round(ref double dValue, ref int iDigits)
		{
			double Round = 0;
			Round = Conversion.Int(dValue * (Math.Pow(10, iDigits)) + 0.5) / (Math.Pow(10, iDigits));
			return Round;
		}

		public static string Quotes(ref string strValue)
		{
			string Quotes = "";
			Quotes = FCConvert.ToString(Convert.ToChar(34)) + strValue + FCConvert.ToString(Convert.ToChar(34));
			return Quotes;
		}

		public static void CloseChildForms()
		{
			//FC:FINAL:DDU:cannot close a form in a foreach enumeration
			//foreach (FCForm frm in FCGlobal.Statics.Forms)
			//{
			//	if (frm.Name != "MDIParent")
			//	{
			//		if (!Statics.boolLoadHide)
			//			frm.Close();
			//	}
			//}
			FCUtils.CloseFormsOnProject(Statics.boolLoadHide);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmBurialPermit, frmClerkDefaults, frmDogInventory, frmMarriageSearchA, frmBurialSearch, frmMonthlyDogReport, frmPrintMarriageConsent, frmPrintVitalRec, frmBirthSearchA, frmDeathSearchA, frmNewDog, frmPrintOldVitals, frmSelectTownStateCounty, frmVoidDog, frmGroupListing, frmScanDocument, frmViewDocuments, frmAddDocument, frmCentralPartySearch, frmEditCentralParties, frmComment, frmDogHistorySetup, frmCalender)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static bool FormExist(FCForm FormName)
		{
			bool FormExist = false;
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		public class StaticVariables
		{
			public string Application = "";
			public string ProgramName = "";
			public string ProgramShort = "";
			public string TitleCaption = string.Empty;
			public string TitleTime = string.Empty;
			public string Ltime = "";
			// used in format print statements
			public int fmtlgth;
			public int fmtval;
			public string fmttype = "";
			public FCFixedString fmtcolon = new FCFixedString(1);
			public int fmtspcbef;
			public int fmtspcaft;
			// used in GNMenu
			public string MenuTitle = "";
			public string MenuShort = "";
			public int MenuCount;
			public FCFixedString MenuOptions = new FCFixedString(19);
			public string[] MenuTitles = new string[19 + 1];
			public FCFixedString MenuSelected = new FCFixedString(1);
			// used in ConvertToString
			public double IntvarNumeric;
			public string INTVARSTRING = string.Empty;
			public string IntVarTemp = string.Empty;
			public int intLen;
			public string newfld = "";
			// used by search routines
			public string SEQ = "";
			public int House;
			public string Search = "";
			public bool LongScreenSearch;
			// used for insert/overtype mode in text boxes
			public int OverType;
			// used in getvalue
			public string NewData = "";
			public int NewValue;
			public FCFixedString EntryMenuResponse = new FCFixedString(2);
			public FCFixedString MainMenuResponse = new FCFixedString(1);
			public FCFixedString MenuResponse = new FCFixedString(1);
			public FCFixedString CurrentApplication = new FCFixedString(1);
			public string Escape = "";
			public string Display = string.Empty;
			public int CommentAccount;
			public string CommentName = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			public int x;
			public string ABORT = "";
			public string Bbort = "";
			public string SortFileSpec = "";
			public int NextNumber;
			public bool SketcherCalled;
			public int Bp;
			// Message Box Response Variables
			// These are used to hold a Message Box Response
			public object MessageResp1;
			public object MessageResp2;
			public object MessageResp3;
			// Prefixes and Variables used for Pictures and Sketches
			public FCFileSystem SketchFile = new FCFileSystem();
			public FCFileSystem PictureFile = new FCFileSystem();
			public string PictureFormat = string.Empty;
			public string PictureLocation = "";
			public string DocumentLocationName = "";
			public string SketchLocation = string.Empty;
			// record locking fields
			public int LockFileNumber;
			public int LowLockNumber;
			public int HighLockNumber;
			public int[,] LowLock = new int[50 + 1, 20 + 1];
			public int[,] HighLock = new int[50 + 1, 20 + 1];
			public int[] LocksHeld = new int[50 + 1];
			// Prefixes for open statements
			public string PPDatabase = string.Empty;
			public string TrioEXE = string.Empty;
			public string TrioDATA = string.Empty;
			public string TrioSDrive = "";
			// File Editting copying, moving, deleting
			public FCFileSystem CopyFile = new FCFileSystem();
			public FCFileSystem MoveFile = new FCFileSystem();
			public FCFileSystem DelFile = new FCFileSystem();
			// Public boolReReg                As Boolean
			// Public boolTempReReg            As Boolean
			public bool boolRegionalTown;
			public int glngKennelOwnerNumber;
			public string gboolSearchType = "";
			public bool BoolPrintFromSearch;
			public int gintMarriageConfirmation;
			public bool gboolSplitMarriageFee;
			public FCSectionReport gobjReport = new FCSectionReport();
			public bool gboolSearchPrint;
			public bool gboolConversion;
			public bool gboolPrintPreview;
			public bool gboolReRegisterDog;
			public string strPreSetReport = "";
			public string gstrSortField = "";
			public string gstrWhereClause = "";
			public string gstrWarrantSQL = "";
			// vbPorter upgrade warning: gintBurialID As int	OnWriteFCConvert.ToInt32(
			public int gintBurialID;
			public string gstrReportOption = "";
			public bool gboolMonthlyLicenseLaser;
			// vbPorter upgrade warning: gintLaserReportYear As string	OnWriteFCConvert.ToInt32(
			public string gintLaserReportYear = "";
			public string gstrDate = "";
			public int intRecordNumber;
			public string gstrMonthYear = "";
			public bool boolIllegitbirth;
			public bool boolMarriageClerkFind;
			public bool boolGetClerk;
			public bool boolDeathClerkFind;
			public int gintReportYear;
			public bool gboolTransfer;
			public int gintNewOwnerNumber;
			public int gintDogNumber;
			public bool boolWarrant;
			public string gstrRegistrar = "";
			public string gstrRegistrarTown = string.Empty;
			public string gstrFacility = "";
			// vbPorter upgrade warning: gintFacility As int	OnWriteFCConvert.ToInt32(
			public int gintFacility;
			public int gintLineToPrint;
			public string gstrBeginSticker = "";
			public string gstrEndSticker = "";
			// vbPorter upgrade warning: gintYear As int	OnWrite(int, string)
			public int gintYear;
			public bool gboolFullMarriageLicense;
			public string gstrActiveLicense = "";
			public int gintSafetyPaperType;
			public bool gboolStateIntentions;
			public bool gboolIssuanceIntentions;
			public bool gboolMarriageIntentions;
			public bool gboolNewMarriageForms;
			public int gintMarriageForm;
			public bool boolNotSave;
			//public bool boolSetDefaultRegistrar;
			public bool boolSetDefaultTowns;
			//public bool boolDogOpen;
			//public bool boolBirthOpen;
			//public bool boolDeathOpen;
			// bool boolBurialOpen;
			public bool boolBurialSearch;
			public bool boolMarriageOpen;
			public bool boolLoadHide;
			// vbPorter upgrade warning: gobjLabel As object	OnWrite(FCLabel)
			//public FCLabel gobjLabel;
			// vbPorter upgrade warning: gobjFormName As object	OnRead(Form)
			//public FCForm gobjFormName;
			//public bool gboolfees;
			//public int[] SpecLength = new int[10 + 1];
			//public string[] SpecCase = new string[10 + 1];
			//public string[] SpecKeyword = new string[10 + 1];
			//public string[] SpecM1 = new string[10 + 1];
			//public string[] SpecM2 = new string[10 + 1];
			//public string[] SpecM3 = new string[10 + 1];
			//public string[] SpecM4 = new string[10 + 1];
			//public string[] SpecM5 = new string[10 + 1];
			//public string[] SpecM6 = new string[10 + 1];
			//public string[] SpecM7 = new string[10 + 1];
			//public string[] SpecM8 = new string[10 + 1];
			//public string[] SpecM9 = new string[10 + 1];
			//public string[] SpecM10 = new string[10 + 1];
			//public string[] SpecType = new string[10 + 1];
			//public string[] SpecParam = new string[10 + 1];
			//public string[] SpecLow = new string[10 + 1];
			//public string[] SpecHigh = new string[10 + 1];
			//public int RespLength;
			//public string RespCase = "";
			//public string RespM1 = "";
			//public string RespM2 = "";
			//public string RespM3 = "";
			//public string RespM4 = "";
			//public string RespM5 = "";
			//public string RespM6 = "";
			//public string RespM7 = "";
			//public string RespM8 = "";
			//public string RespM9 = "";
			//public string RespM10 = "";
			//public string RespOK = "";
			//public string RespType = "";
			//public string RespParam = "";
			//public string RespLow = "";
			//public string RespHigh = "";
			// vbPorter upgrade warning: Response As object	OnWrite(int, object, string)	OnReadFCConvert.ToInt32(
			public object Response;
			public int gintDeathNumber;
			// vbPorter upgrade warning: gintMarriageNumber As int	OnWriteFCConvert.ToInt32(
			public int gintMarriageNumber;
			//public object RespKeyword;
			public string Resp1 = string.Empty;
			// vbPorter upgrade warning: Resp2 As object	OnWriteFCConvert.ToInt32(
			public int Resp2;
			//public object Resp3;
			//public object Resp4;
			//public object Resp5;
			//public object Resp6;
			//public object Resp7;
			//public object Resp8;
			//public object Resp9;
			//public object Resp10;
			//public bool SystemSetup;
			//public bool LinkPicture;
			public clsCustomize ClerkDefaults = new clsCustomize();
			//public bool EntryMenuFlag;
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsMarriageCertificate = new clsDRWrapper();
			public clsDRWrapper rsMarriageCertificate_AutoInitialized = null;
			public clsDRWrapper rsMarriageCertificate
			{
				get
				{
					if ( rsMarriageCertificate_AutoInitialized == null)
					{
						 rsMarriageCertificate_AutoInitialized = new clsDRWrapper();
					}
					return rsMarriageCertificate_AutoInitialized;
				}
				set
				{
					 rsMarriageCertificate_AutoInitialized = value;
				}
			}
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsBirthCertificate = new clsDRWrapper();
			public clsDRWrapper rsBirthCertificate_AutoInitialized = null;
			public clsDRWrapper rsBirthCertificate
			{
				get
				{
					if ( rsBirthCertificate_AutoInitialized == null)
					{
						 rsBirthCertificate_AutoInitialized = new clsDRWrapper();
					}
					return rsBirthCertificate_AutoInitialized;
				}
				set
				{
					 rsBirthCertificate_AutoInitialized = value;
				}
			}
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsDeathCertificate = new clsDRWrapper();
			public clsDRWrapper rsDeathCertificate_AutoInitialized = null;
			public clsDRWrapper rsDeathCertificate
			{
				get
				{
					if ( rsDeathCertificate_AutoInitialized == null)
					{
						 rsDeathCertificate_AutoInitialized = new clsDRWrapper();
					}
					return rsDeathCertificate_AutoInitialized;
				}
				set
				{
					 rsDeathCertificate_AutoInitialized = value;
				}
			}
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsBurialCertificate = new clsDRWrapper();
			//public clsDRWrapper rsBurialCertificate_AutoInitialized = null;
			//public clsDRWrapper rsBurialCertificate
			//{
			//	get
			//	{
			//		if ( rsBurialCertificate_AutoInitialized == null)
			//		{
			//			 rsBurialCertificate_AutoInitialized = new clsDRWrapper();
			//		}
			//		return rsBurialCertificate_AutoInitialized;
			//	}
			//	set
			//	{
			//		 rsBurialCertificate_AutoInitialized = value;
			//	}
			//}
			//public bool gboolEndApplication;
			public typConsent typMarriageConsent = new typConsent();
			public typOldVitals typOldVitalsReport = new typOldVitals();
			public typDogTransactionRecord typDogTransaction = new typDogTransactionRecord();
			public typTransactionRecord typTransaction = new typTransactionRecord();
			public typDogSummaryRecord typDogSummary = new typDogSummaryRecord();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
