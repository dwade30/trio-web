//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBackKennelLicense.
	/// </summary>
	public partial class rptBackKennelLicense : BaseSectionReport
	{
		public rptBackKennelLicense()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Back Kennel License";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBackKennelLicense InstancePtr
		{
			get
			{
				return (rptBackKennelLicense)Sys.GetInstance(typeof(rptBackKennelLicense));
			}
		}

		protected rptBackKennelLicense _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBackKennelLicense	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		int intLicenseToPrint;
		// vbPorter upgrade warning: intNumDogs As int	OnWriteFCConvert.ToInt32(
		int intNumDogs;
		int intCurrentDog;
		int lngOwnerNum;
		bool boolLate;
		bool boolPrintTest;
		double dblLineAdjust;
		// Public Sub Init(intWhichLicense As Integer)
		// 1 is first license, 2 is second license
		// intLicenseToPrint = intWhichLicense
		// Me.Show
		// End Sub
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			/*? On Error Resume Next  */
			if (!boolPrintTest)
			{
				if (rs.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				txtName.Text = rs.Get_Fields_String("DogName");
				txtBreed.Text = rs.Get_Fields_String("DogBreed");
				txtSex.Text = rs.Get_Fields_String("DogSex");
				txtSN.Text = (FCConvert.ToBoolean(rs.Get_Fields_Boolean("DogNeuter")) ? "Y" : "N");
				txtRabiesNum.Text = rs.Get_Fields_String("RabiesCertNum");
				txtDateGiven.Text = string.Empty;
				txtStickerNum.Text = rs.Get_Fields_String("StickerLicNum");
				// txtLateFee = IIf(RS.Fields("LateFee"), "Y", "N")
				if (boolLate)
				{
					txtLateFee.Text = "Y";
				}
				else
				{
					txtLateFee.Text = "N";
				}
				intCurrentDog += 1;
				if (!rs.EndOfFile())
					rs.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				txtName.Text = "Dog " + FCConvert.ToString(intCurrentDog);
				txtBreed.Text = "Breed";
				txtSex.Text = "M";
				txtSN.Text = "Y";
				txtRabiesNum.Text = "123";
				txtDateGiven.Text = "";
				txtStickerNum.Text = "11111";
				txtLateFee.Text = "N";
				intCurrentDog += 1;
				intNumDogs -= 1;
				eArgs.EOF = intNumDogs <= 0;
			}
		}

		public void Init(int lngLicenseID, bool boolLateFees, bool boolTestPrint = false, double dblAdjust = 0)
		{
			string strSQL = "";
			string strDogList = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolPrintTest = boolTestPrint;
				dblLineAdjust = dblAdjust;
				boolLate = boolLateFees;
				if (!boolPrintTest)
				{
					rs.OpenRecordset("select * from kennellicense where ID = " + FCConvert.ToString(lngLicenseID), "twck0000.vb1");
					strDogList = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("dognumbers")));
					lngOwnerNum = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("ownernum"))));
					if (fecherFoundation.Strings.Trim(strDogList) == string.Empty)
					{
						MessageBox.Show("No dogs to print", "No Dogs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						this.Close();
						return;
					}
					strSQL = "select * from  doginfo where ID in (" + strDogList + ") order by ID";
					rs.OpenRecordset(strSQL, "twck0000.vb1");
					if (rs.EndOfFile())
					{
						MessageBox.Show("No dogs to print", "No Dogs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						this.Close();
						return;
					}
					intNumDogs = rs.RecordCount();
				}
				else
				{
					intNumDogs = 5;
					intCurrentDog = 1;
				}
                //FC:FINAL:IPI - #i1705 - show the report viewer on web
                //this.Show(FCForm.FormShowEnum.Modal);
                frmReportViewer.InstancePtr.Init(this, showModal: true);
                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			object ControlName;
			double dblKennelLineAdjustment = 0;
			// Dim rsData As New clsDRWrapper
			int X;
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			//Application.DoEvents();
			if (!boolPrintTest)
			{
				// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
				// If rsData.EndOfFile Then
				// dblKennelLineAdjustment = 0
				// Else
				// dblKennelLineAdjustment = CDbl(Val(rsData.Fields("KennelAdjustment")))
				// End If
				dblKennelLineAdjustment = Conversion.Val(modRegistry.GetRegistryKey("KennelAdjustment"));
			}
			else
			{
				dblKennelLineAdjustment = dblLineAdjust;
			}
			this.PageHeader.Height += FCConvert.ToSingle(200 * dblKennelLineAdjustment) / 1440F;
			// Call RS.OpenRecordset("SELECT DogOwner.Address, DogOwner.Phone,DogOwner.FirstName, DogOwner.MI, DogOwner.LastName, DogInfo.* FROM DogInfo INNER JOIN DogOwner ON DogInfo.OwnerNum = dogowner.ID where DogInfo.KennelDog = TRUE AND dogowner.ID = " & dOwner.OwnerNum & " order by DOGNUMBER", DEFAULTDATABASENAME)
			// intNumDogs = 0
			// intCurrentDog = 1
			// If Not RS.EndOfFile Then
			// intNumDogs = RS.RecordCount
			// End If
			// If intLicenseToPrint = 2 Then
			// intCurrentDog = 11
			// 
			// If intNumDogs > 10 Then
			// For x = 1 To 10
			// RS.MoveNext
			// Next x
			// End If
			// End If
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			if (!boolPrintTest)
			{
				if (rs.EndOfFile())
					return;
			}
			if (modGNBas.Statics.gintLineToPrint == 1)
			{
			}
			else
			{
				PageHeader.Height += ((modGNBas.Statics.gintLineToPrint - 1) * 350) / 1440F;
			}
		}

		
	}
}
