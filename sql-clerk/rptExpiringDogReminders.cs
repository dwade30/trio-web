//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptExpiringDogReminders.
	/// </summary>
	public partial class rptExpiringDogReminders : BaseSectionReport
	{
		public rptExpiringDogReminders()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Expiring License Reminders";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptExpiringDogReminders InstancePtr
		{
			get
			{
				return (rptExpiringDogReminders)Sys.GetInstance(typeof(rptExpiringDogReminders));
			}
		}

		protected rptExpiringDogReminders _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptExpiringDogReminders	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private string strTitle = "";
		private string strSigniture = "";
		private clsDRWrapper rsData = new clsDRWrapper();
		private clsDRWrapper rsTemp = new clsDRWrapper();
		private int intPageNumber;
		private int intCounter;
		private string strDate = string.Empty;
		string strMessage = "";
		private bool boolPrintTest;
		private double dblLineAdjust;
		private bool boolSendEmail;
		private bool boolPrintReminders;
		private bool boolPrintAllReminders;
		private string strEmailSubject = string.Empty;
		private string strEmailBegin = "";
		private string strEmailEnd = "";
		private clsBulkEmail lstEmailList = new clsBulkEmail();

		public void Init(bool boolTestPrint, double dblAdjust = 0, string strCaption = "", bool boolEmail = false, bool boolPrint = true, bool boolPrintAll = true, string strReminderDate = "")
		{
			if (strCaption != string.Empty)
			{
				this.Name = strCaption;
			}
			lstEmailList.ClearList();
			strDate = strReminderDate;
			boolPrintTest = boolTestPrint;
			dblLineAdjust = dblAdjust;
			boolPrintAllReminders = boolPrintAll;
			boolPrintReminders = boolPrint;
			boolSendEmail = boolEmail;
			strEmailSubject = "Expiring license reminder";
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "DogReminders");
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// txtPage = "Page " & intPageNumber
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strTemp = "";
			NextRecord:
			;
			// txtMuni = strTitle
			txtDate.Text = strDate;
			txtSigniture.Text = strSigniture;
			if (!boolPrintTest)
			{
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				string strEmailBody = "";
				clsEmailObject tMail;
				// If rsData.Fields("MI") <> "N/A" Then
				txtAddress1.Text = rsData.Get_Fields_String("FullName");
				// Else
				// txtAddress1.Text = rsData.Fields("FirstName") & " " & rsData.Fields("LastName")
				// End If
				txtAddress2.Text = rsData.Get_Fields_String("Address1");
				txtAddress3.Text = rsData.Get_Fields_String("City") + ", " + rsData.Get_Fields("PartyState") + " " + rsData.Get_Fields_String("Zip");
				intCounter = 0;
				if (DateTime.Today.Month > 1)
				{
					rsTemp.OpenRecordset("Select DogDOB,dogdeceased from DogInfo where OwnerNum = " + rsData.Get_Fields_Int32("ID") + " and year <= " + FCConvert.ToString(DateTime.Today.Year) + " and dogdeceased <> 1", modGNBas.DEFAULTCLERKDATABASE);
				}
				else
				{
					rsTemp.OpenRecordset("Select DogDOB,dogdeceased from DogInfo where OwnerNum = " + rsData.Get_Fields_Int32("ID") + " and year < " + FCConvert.ToString(DateTime.Today.Year) + " and dogdeceased <> 1", modGNBas.DEFAULTCLERKDATABASE);
				}
				while (!rsTemp.EndOfFile())
				{
					if (!FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("dogdeceased")))
					{
						if (Information.IsDate(rsTemp.Get_Fields("DogDOB")))
						{
							if (fecherFoundation.DateAndTime.DateAdd("M", 6, (DateTime)rsTemp.Get_Fields_DateTime("DogDOB")).ToOADate() <= DateTime.Today.ToOADate())
							{
								intCounter += 1;
							}
						}
						else
						{
							intCounter += 1;
						}
					}
					rsTemp.MoveNext();
				}
				if (intCounter == 0)
				{
					rsData.MoveNext();
					goto NextRecord;
				}
				else
				{
					// txtNumber = intCounter
					strTemp = "Our records indicate that you are the owner or keeper of " + FCConvert.ToString(intCounter) + " dog(s) over six months of age.  ";
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("email"))) != "")
					{
						if (boolSendEmail)
						{
							strEmailBody = strEmailBegin + "\r\n" + "To: " + txtAddress1.Text + "\r\n" + "\r\n";
							strEmailBody += "Dear dog owner/keeper" + "\r\n";
							strEmailBody += strTemp + strMessage + "\r\n" + "\r\n";
							strEmailBody += strEmailEnd;
							tMail = new clsEmailObject();
							tMail.Body = strEmailBody;
							tMail.EmailAddress = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("email")));
							tMail.Subject = strEmailSubject;
							tMail.Tag = txtAddress1.Text;
							lstEmailList.AddItem(ref tMail);
							// Call frmEMail.Init(, rsData.Fields("email"), strEmailSubject, strEmailBody, True, , , True, True, False)
						}
						if (!boolPrintAllReminders || !boolPrintReminders)
						{
							rsData.MoveNext();
							goto NextRecord;
						}
					}
					else if (!boolPrintReminders)
					{
						rsData.MoveNext();
						goto NextRecord;
					}
					//FC:FINAL:DSE WordWrapping not working in RichTextBox
					//RichEdit1.Text = strTemp + strMessage;
					RichEdit1.SetHtmlText(strTemp + strMessage);
				}
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				txtAddress1.Text = "Name";
				txtAddress2.Text = "Address";
				txtAddress3.Text = "City State Zip";
				//FC:FINAL:DSE WordWrapping not working in RichTextBox
				//RichEdit1.Text = "Our records indicate that you are the owner or keeper of 100 dog(s) over six months of age.  " + strMessage;
				RichEdit1.SetHtmlText("Our records indicate that you are the owner or keeper of 100 dog(s) over six months of age.  " + strMessage);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Dim clsLoad As New clsDRWrapper
			int X;
			// vbPorter upgrade warning: lngMove As int	OnWrite(int, double)
			float lngMove;
			lngMove = 0;
			if (!boolPrintTest)
			{
				// Call clsLoad.OpenRecordset("select * from printersettings", "twck0000.vb1")
				// If Not clsLoad.EndOfFile Then
				// adjust by this many lines
				if (Conversion.Val(modRegistry.GetRegistryKey("DogRemindersAdjustment", "CK")) != 0)
				{
					// If Val(clsLoad.Fields("DogRemindersAdjustment")) <> 0 Then
					lngMove = FCConvert.ToSingle(modRegistry.GetRegistryKey("DogRemindersAdjustment", "CK")) * 240 / 1440F;
					if (Field3.Top + lngMove < 0)
					{
						lngMove = -Field3.Top;
						// can only move this much
					}
				}
				// End If
			}
			else
			{
				lngMove = FCConvert.ToSingle(dblLineAdjust * 240) / 1440F;
				if (Field3.Top + lngMove < 0)
				{
					lngMove = -Field3.Top;
				}
			}
			for (X = 0; X <= Detail.Controls.Count - 1; X++)
			{
				if (fecherFoundation.Strings.UCase(Strings.Mid(Detail.Controls[X].Name, 1, 4)) == "LINE")
				{
					(Detail.Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 += lngMove;
					(Detail.Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Line).Y2 += lngMove;
					if (Detail.Height <= (Detail.Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1)
					{
						Detail.Height = (Detail.Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 + 10 / 1440F;
					}
				}
				else
				{
					Detail.Controls[X].Top += lngMove;
				}
			}
			// X
			RichEdit1.Font = new Font(Field5.Font.Name, RichEdit1.Font.Size);
			RichEdit1.Font = new Font(RichEdit1.Font.Name, Field5.Font.Size);
			rsData.OpenRecordset("select * from dogdefaults", "twck0000.vb1");
			if (!rsData.EndOfFile())
			{
				strMessage = FCConvert.ToString(rsData.Get_Fields_String("Expiringreminder"));
				strSigniture = FCConvert.ToString(rsData.Get_Fields_String("unlicensedsignature"));
			}
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			rsData.OpenRecordset("Select * from GlobalVariables", "SystemSettings");
			if (!rsData.EndOfFile())
			{
				strTitle = rsData.Get_Fields_String("CityTown") + " of " + rsData.Get_Fields_String("MuniName");
				// strSigniture = rsData.Fields("MuniName") & " Animal Control Officer or " & Chr(13) & "Town Clerk for the " & rsData.Fields("CityTown") & " of " & rsData.Fields("MuniName")
			}
			// Call rsData.OpenRecordset("Select * from ClerkDefaults", "TWCK0000.vb1")
			// If Not rsData.EndOfFile Then
			// strSigniture = strSigniture & Chr(13) & "Phone # " & rsData.Fields("Phone")
			// End If
			if (!boolPrintTest)
			{
				rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGNBas.DEFAULTCLERKDATABASE);
				// strDate = InputBox("Enter date to show on reminders.", "TRIO Software", Date)
				// If strDate = vbNullString Then
				// Me.Cancel
				// Unload Me
				// Unload frmReportViewer
				// Exit Sub
				// End If
				strEmailBegin = "Date: " + strDate + "\r\n";
				strEmailEnd = strSigniture + "\r\n" + "\r\n";
				strEmailEnd += "Maine certification of rabies vaccination means a current up to date certificate";
				// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
				if (rsData.RecordCount() != 0)
					rsData.MoveFirst();
			}
			else
			{
				strDate = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			intPageNumber = 1;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (!(lstEmailList == null))
			{
				if (lstEmailList.ItemCount() > 0)
				{
					if (boolSendEmail)
					{
						if (MessageBox.Show("Do you want to send emails now?", "Send Now?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
						{
							lstEmailList.SendMail();
						}
					}
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (lstEmailList.ItemCount() > 0)
			{
				ReportFooter.Visible = true;
				srptBulkEmailList tRept = new srptBulkEmailList();
				tRept.EmailList = lstEmailList;
				SubReport1.Report = tRept;
			}
			else
			{
				ReportFooter.Visible = false;
			}
		}

		
	}
}
