//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmPrinterSettings.
	/// </summary>
	partial class frmPrinterSettings
	{
		public fecherFoundation.FCComboBox cmbDogLicense;
		public fecherFoundation.FCComboBox cmbMonthlyReport;
		public fecherFoundation.FCComboBox cmbKennelLicense;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtMarriageIntentionsAdjustment;
		public fecherFoundation.FCButton cmdPreview_10;
		public fecherFoundation.FCTextBox txtNCDeath;
		public fecherFoundation.FCButton cmdPreview_9;
		public fecherFoundation.FCTextBox txtMarriageHorizontal;
		public fecherFoundation.FCButton cmdPreview_8;
		public fecherFoundation.FCButton cmdPreview_7;
		public fecherFoundation.FCButton cmdPreview_6;
		public fecherFoundation.FCButton cmdPreview_5;
		public fecherFoundation.FCButton cmdPreview_4;
		public fecherFoundation.FCButton cmdPreview_3;
		public fecherFoundation.FCButton cmdPreview_2;
		public fecherFoundation.FCButton cmdPreview_1;
		public fecherFoundation.FCButton cmdPreview_0;
		public fecherFoundation.FCTextBox txtDogReminder;
		public fecherFoundation.FCTextBox txtKennelAdjustment;
		public fecherFoundation.FCTextBox txtMarriageLicenseAdjustment;
		public fecherFoundation.FCTextBox txtBurialAdjustment;
		public fecherFoundation.FCTextBox txtDogAdjustment;
		public fecherFoundation.FCTextBox txtMarriageAdjustment;
		public fecherFoundation.FCTextBox txtDeathAdjustment;
		public fecherFoundation.FCTextBox txtBirthAdjustment;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCFrame fraParameters;
		public fecherFoundation.FCTextBox txtYear;
		public Wisej.Web.MaskedTextBox txtDate;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkPrintOptionalSection;
		public fecherFoundation.FCCheckBox chkPrintExpiration;
		public fecherFoundation.FCCheckBox chkPrintOnFront;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbDogLicense = new fecherFoundation.FCComboBox();
			this.cmbMonthlyReport = new fecherFoundation.FCComboBox();
			this.cmbKennelLicense = new fecherFoundation.FCComboBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtMarriageIntentionsAdjustment = new fecherFoundation.FCTextBox();
			this.cmdPreview_10 = new fecherFoundation.FCButton();
			this.txtNCDeath = new fecherFoundation.FCTextBox();
			this.cmdPreview_9 = new fecherFoundation.FCButton();
			this.txtMarriageHorizontal = new fecherFoundation.FCTextBox();
			this.cmdPreview_8 = new fecherFoundation.FCButton();
			this.cmdPreview_7 = new fecherFoundation.FCButton();
			this.cmdPreview_6 = new fecherFoundation.FCButton();
			this.cmdPreview_5 = new fecherFoundation.FCButton();
			this.cmdPreview_4 = new fecherFoundation.FCButton();
			this.cmdPreview_3 = new fecherFoundation.FCButton();
			this.cmdPreview_2 = new fecherFoundation.FCButton();
			this.cmdPreview_1 = new fecherFoundation.FCButton();
			this.cmdPreview_0 = new fecherFoundation.FCButton();
			this.txtDogReminder = new fecherFoundation.FCTextBox();
			this.txtKennelAdjustment = new fecherFoundation.FCTextBox();
			this.txtMarriageLicenseAdjustment = new fecherFoundation.FCTextBox();
			this.txtBurialAdjustment = new fecherFoundation.FCTextBox();
			this.txtDogAdjustment = new fecherFoundation.FCTextBox();
			this.txtMarriageAdjustment = new fecherFoundation.FCTextBox();
			this.txtDeathAdjustment = new fecherFoundation.FCTextBox();
			this.txtBirthAdjustment = new fecherFoundation.FCTextBox();
			this.Label16 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.fraParameters = new fecherFoundation.FCFrame();
			this.txtYear = new fecherFoundation.FCTextBox();
			this.txtDate = new Wisej.Web.MaskedTextBox();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.lblKennelLicense = new fecherFoundation.FCLabel();
			this.lblMonthlyReport = new fecherFoundation.FCLabel();
			this.lblDogLicense = new fecherFoundation.FCLabel();
			this.chkPrintOptionalSection = new fecherFoundation.FCCheckBox();
			this.chkPrintExpiration = new fecherFoundation.FCCheckBox();
			this.chkPrintOnFront = new fecherFoundation.FCCheckBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraParameters)).BeginInit();
			this.fraParameters.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintOptionalSection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintExpiration)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintOnFront)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(968, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.fraParameters);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(968, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(968, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(265, 30);
			this.HeaderText.Text = "Default Printer Settings";
			// 
			// cmbDogLicense
			// 
			this.cmbDogLicense.Items.AddRange(new object[] {
            "Pre-Printed",
            "Blank Form"});
			this.cmbDogLicense.Location = new System.Drawing.Point(188, 30);
			this.cmbDogLicense.Name = "cmbDogLicense";
			this.cmbDogLicense.Size = new System.Drawing.Size(140, 40);
			this.cmbDogLicense.SelectedIndexChanged += new System.EventHandler(this.optDogLicense_CheckedChanged);
			// 
			// cmbMonthlyReport
			// 
			this.cmbMonthlyReport.Items.AddRange(new object[] {
            "Pre-Printed",
            "Blank Form"});
			this.cmbMonthlyReport.Location = new System.Drawing.Point(188, 80);
			this.cmbMonthlyReport.Name = "cmbMonthlyReport";
			this.cmbMonthlyReport.Size = new System.Drawing.Size(140, 40);
			this.cmbMonthlyReport.TabIndex = 5;
			this.cmbMonthlyReport.SelectedIndexChanged += new System.EventHandler(this.optMonthlyReport_CheckedChanged);
			// 
			// cmbKennelLicense
			// 
			this.cmbKennelLicense.Items.AddRange(new object[] {
            "Pre-Printed",
            "Blank Form"});
			this.cmbKennelLicense.Location = new System.Drawing.Point(188, 130);
			this.cmbKennelLicense.Name = "cmbKennelLicense";
			this.cmbKennelLicense.Size = new System.Drawing.Size(140, 40);
			this.cmbKennelLicense.TabIndex = 7;
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtMarriageIntentionsAdjustment);
			this.Frame3.Controls.Add(this.cmdPreview_10);
			this.Frame3.Controls.Add(this.txtNCDeath);
			this.Frame3.Controls.Add(this.cmdPreview_9);
			this.Frame3.Controls.Add(this.txtMarriageHorizontal);
			this.Frame3.Controls.Add(this.cmdPreview_8);
			this.Frame3.Controls.Add(this.cmdPreview_7);
			this.Frame3.Controls.Add(this.cmdPreview_6);
			this.Frame3.Controls.Add(this.cmdPreview_5);
			this.Frame3.Controls.Add(this.cmdPreview_4);
			this.Frame3.Controls.Add(this.cmdPreview_3);
			this.Frame3.Controls.Add(this.cmdPreview_2);
			this.Frame3.Controls.Add(this.cmdPreview_1);
			this.Frame3.Controls.Add(this.cmdPreview_0);
			this.Frame3.Controls.Add(this.txtDogReminder);
			this.Frame3.Controls.Add(this.txtKennelAdjustment);
			this.Frame3.Controls.Add(this.txtMarriageLicenseAdjustment);
			this.Frame3.Controls.Add(this.txtBurialAdjustment);
			this.Frame3.Controls.Add(this.txtDogAdjustment);
			this.Frame3.Controls.Add(this.txtMarriageAdjustment);
			this.Frame3.Controls.Add(this.txtDeathAdjustment);
			this.Frame3.Controls.Add(this.txtBirthAdjustment);
			this.Frame3.Controls.Add(this.Label16);
			this.Frame3.Controls.Add(this.Label15);
			this.Frame3.Controls.Add(this.Label14);
			this.Frame3.Controls.Add(this.Label13);
			this.Frame3.Controls.Add(this.Label12);
			this.Frame3.Controls.Add(this.Label11);
			this.Frame3.Controls.Add(this.Label10);
			this.Frame3.Controls.Add(this.Label9);
			this.Frame3.Controls.Add(this.Label8);
			this.Frame3.Controls.Add(this.Label7);
			this.Frame3.Controls.Add(this.Label6);
			this.Frame3.Controls.Add(this.Label5);
			this.Frame3.Location = new System.Drawing.Point(30, 304);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(793, 590);
			this.Frame3.TabIndex = 40;
			this.Frame3.Text = "Laser Printer Line Adjustment";
			// 
			// txtMarriageIntentionsAdjustment
			// 
			this.txtMarriageIntentionsAdjustment.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtMarriageIntentionsAdjustment.Location = new System.Drawing.Point(294, 130);
			this.txtMarriageIntentionsAdjustment.Name = "txtMarriageIntentionsAdjustment";
			this.txtMarriageIntentionsAdjustment.Size = new System.Drawing.Size(105, 40);
			this.txtMarriageIntentionsAdjustment.TabIndex = 13;
			// 
			// cmdPreview_10
			// 
			this.cmdPreview_10.AppearanceKey = "actionButton";
			this.cmdPreview_10.Location = new System.Drawing.Point(419, 130);
			this.cmdPreview_10.Name = "cmdPreview_10";
			this.cmdPreview_10.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_10.TabIndex = 14;
			this.cmdPreview_10.Text = "Preview";
			this.cmdPreview_10.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// txtNCDeath
			// 
			this.txtNCDeath.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtNCDeath.Location = new System.Drawing.Point(294, 330);
			this.txtNCDeath.Name = "txtNCDeath";
			this.txtNCDeath.Size = new System.Drawing.Size(105, 40);
			this.txtNCDeath.TabIndex = 21;
			// 
			// cmdPreview_9
			// 
			this.cmdPreview_9.AppearanceKey = "actionButton";
			this.cmdPreview_9.Location = new System.Drawing.Point(419, 330);
			this.cmdPreview_9.Name = "cmdPreview_9";
			this.cmdPreview_9.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_9.TabIndex = 22;
			this.cmdPreview_9.Text = "Preview";
			this.cmdPreview_9.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// txtMarriageHorizontal
			// 
			this.txtMarriageHorizontal.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtMarriageHorizontal.Location = new System.Drawing.Point(294, 230);
			this.txtMarriageHorizontal.Name = "txtMarriageHorizontal";
			this.txtMarriageHorizontal.Size = new System.Drawing.Size(105, 40);
			this.txtMarriageHorizontal.TabIndex = 17;
			// 
			// cmdPreview_8
			// 
			this.cmdPreview_8.AppearanceKey = "actionButton";
			this.cmdPreview_8.Location = new System.Drawing.Point(419, 230);
			this.cmdPreview_8.Name = "cmdPreview_8";
			this.cmdPreview_8.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_8.TabIndex = 18;
			this.cmdPreview_8.Text = "Preview";
			this.cmdPreview_8.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdPreview_7
			// 
			this.cmdPreview_7.AppearanceKey = "actionButton";
			this.cmdPreview_7.Location = new System.Drawing.Point(419, 530);
			this.cmdPreview_7.Name = "cmdPreview_7";
			this.cmdPreview_7.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_7.TabIndex = 30;
			this.cmdPreview_7.Text = "Preview";
			this.cmdPreview_7.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdPreview_6
			// 
			this.cmdPreview_6.AppearanceKey = "actionButton";
			this.cmdPreview_6.Location = new System.Drawing.Point(419, 480);
			this.cmdPreview_6.Name = "cmdPreview_6";
			this.cmdPreview_6.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_6.TabIndex = 28;
			this.cmdPreview_6.Text = "Preview";
			this.cmdPreview_6.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdPreview_5
			// 
			this.cmdPreview_5.AppearanceKey = "actionButton";
			this.cmdPreview_5.Location = new System.Drawing.Point(419, 430);
			this.cmdPreview_5.Name = "cmdPreview_5";
			this.cmdPreview_5.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_5.TabIndex = 26;
			this.cmdPreview_5.Text = "Preview";
			this.cmdPreview_5.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdPreview_4
			// 
			this.cmdPreview_4.AppearanceKey = "actionButton";
			this.cmdPreview_4.Location = new System.Drawing.Point(419, 380);
			this.cmdPreview_4.Name = "cmdPreview_4";
			this.cmdPreview_4.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_4.TabIndex = 24;
			this.cmdPreview_4.Text = "Preview";
			this.cmdPreview_4.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdPreview_3
			// 
			this.cmdPreview_3.AppearanceKey = "actionButton";
			this.cmdPreview_3.Location = new System.Drawing.Point(419, 279);
			this.cmdPreview_3.Name = "cmdPreview_3";
			this.cmdPreview_3.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_3.TabIndex = 20;
			this.cmdPreview_3.Text = "Preview";
			this.cmdPreview_3.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdPreview_2
			// 
			this.cmdPreview_2.AppearanceKey = "actionButton";
			this.cmdPreview_2.Location = new System.Drawing.Point(419, 180);
			this.cmdPreview_2.Name = "cmdPreview_2";
			this.cmdPreview_2.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_2.TabIndex = 16;
			this.cmdPreview_2.Text = "Preview";
			this.cmdPreview_2.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdPreview_1
			// 
			this.cmdPreview_1.AppearanceKey = "actionButton";
			this.cmdPreview_1.Location = new System.Drawing.Point(419, 80);
			this.cmdPreview_1.Name = "cmdPreview_1";
			this.cmdPreview_1.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_1.TabIndex = 12;
			this.cmdPreview_1.Text = "Preview";
			this.cmdPreview_1.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdPreview_0
			// 
			this.cmdPreview_0.AppearanceKey = "actionButton";
			this.cmdPreview_0.Location = new System.Drawing.Point(419, 30);
			this.cmdPreview_0.Name = "cmdPreview_0";
			this.cmdPreview_0.Size = new System.Drawing.Size(105, 40);
			this.cmdPreview_0.TabIndex = 10;
			this.cmdPreview_0.Text = "Preview";
			this.cmdPreview_0.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// txtDogReminder
			// 
			this.txtDogReminder.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtDogReminder.Location = new System.Drawing.Point(294, 530);
			this.txtDogReminder.Name = "txtDogReminder";
			this.txtDogReminder.Size = new System.Drawing.Size(105, 40);
			this.txtDogReminder.TabIndex = 29;
			// 
			// txtKennelAdjustment
			// 
			this.txtKennelAdjustment.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtKennelAdjustment.Location = new System.Drawing.Point(294, 480);
			this.txtKennelAdjustment.Name = "txtKennelAdjustment";
			this.txtKennelAdjustment.Size = new System.Drawing.Size(105, 40);
			this.txtKennelAdjustment.TabIndex = 27;
			// 
			// txtMarriageLicenseAdjustment
			// 
			this.txtMarriageLicenseAdjustment.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtMarriageLicenseAdjustment.Location = new System.Drawing.Point(294, 180);
			this.txtMarriageLicenseAdjustment.Name = "txtMarriageLicenseAdjustment";
			this.txtMarriageLicenseAdjustment.Size = new System.Drawing.Size(105, 40);
			this.txtMarriageLicenseAdjustment.TabIndex = 15;
			// 
			// txtBurialAdjustment
			// 
			this.txtBurialAdjustment.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtBurialAdjustment.Location = new System.Drawing.Point(294, 380);
			this.txtBurialAdjustment.Name = "txtBurialAdjustment";
			this.txtBurialAdjustment.Size = new System.Drawing.Size(105, 40);
			this.txtBurialAdjustment.TabIndex = 23;
			// 
			// txtDogAdjustment
			// 
			this.txtDogAdjustment.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtDogAdjustment.Location = new System.Drawing.Point(294, 430);
			this.txtDogAdjustment.Name = "txtDogAdjustment";
			this.txtDogAdjustment.Size = new System.Drawing.Size(105, 40);
			this.txtDogAdjustment.TabIndex = 25;
			// 
			// txtMarriageAdjustment
			// 
			this.txtMarriageAdjustment.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtMarriageAdjustment.Location = new System.Drawing.Point(294, 80);
			this.txtMarriageAdjustment.Name = "txtMarriageAdjustment";
			this.txtMarriageAdjustment.Size = new System.Drawing.Size(105, 40);
			this.txtMarriageAdjustment.TabIndex = 11;
			// 
			// txtDeathAdjustment
			// 
			this.txtDeathAdjustment.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtDeathAdjustment.Location = new System.Drawing.Point(294, 280);
			this.txtDeathAdjustment.Name = "txtDeathAdjustment";
			this.txtDeathAdjustment.Size = new System.Drawing.Size(105, 40);
			this.txtDeathAdjustment.TabIndex = 19;
			// 
			// txtBirthAdjustment
			// 
			this.txtBirthAdjustment.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtBirthAdjustment.Location = new System.Drawing.Point(294, 30);
			this.txtBirthAdjustment.Name = "txtBirthAdjustment";
			this.txtBirthAdjustment.Size = new System.Drawing.Size(105, 40);
			this.txtBirthAdjustment.TabIndex = 9;
			// 
			// Label16
			// 
			this.Label16.Location = new System.Drawing.Point(20, 144);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(250, 16);
			this.Label16.TabIndex = 52;
			this.Label16.Text = "MARRIAGE INTENTIONS LINE ADJUSTMENT";
			// 
			// Label15
			// 
			this.Label15.Location = new System.Drawing.Point(20, 344);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(250, 16);
			this.Label15.TabIndex = 51;
			this.Label15.Text = "VS-30A (DEATH) LINE ADJUSTMENT";
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(20, 244);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(250, 16);
			this.Label14.TabIndex = 50;
			this.Label14.Text = "MARRIAGE HORIZONTAL ADJUSTMENT";
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(20, 544);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(250, 16);
			this.Label13.TabIndex = 49;
			this.Label13.Text = "DOG REMINDERS ADJUSTMENT";
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(20, 494);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(250, 16);
			this.Label12.TabIndex = 48;
			this.Label12.Text = "BACK OF KENNEL ADJUSTMENT";
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(20, 194);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(250, 16);
			this.Label11.TabIndex = 47;
			this.Label11.Text = "MARRIAGE LICENSE LINE ADJUSTMENT";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(20, 394);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(250, 16);
			this.Label10.TabIndex = 46;
			this.Label10.Text = "BURIAL PERMIT LINE ADJUSTMENT";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 444);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(250, 16);
			this.Label9.TabIndex = 45;
			this.Label9.Text = "DOG LINE ADJUSTMENT";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(20, 94);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(250, 16);
			this.Label8.TabIndex = 44;
			this.Label8.Text = "MARRIAGE CERT LINE ADJUSTMENT";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(20, 294);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(250, 16);
			this.Label7.TabIndex = 43;
			this.Label7.Text = "DEATH LINE ADJUSTMENT";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(544, 30);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(229, 112);
			this.Label6.TabIndex = 42;
			this.Label6.Text = "ENTER THE NUMBER OF LINES THAT THE DATA ON YOUR REPORTS NEEDS TO BE MOVED. NEGATI" +
    "VE AND FRACTIONAL NUMBERS CAN BE ENTERED.    EXAMPLES: (1, 1.5, 2, -1)";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 44);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(250, 16);
			this.Label5.TabIndex = 41;
			this.Label5.Text = "BIRTH LINE ADJUSTMENT";
			// 
			// fraParameters
			// 
			this.fraParameters.Controls.Add(this.txtYear);
			this.fraParameters.Controls.Add(this.txtDate);
			this.fraParameters.Controls.Add(this.Label4);
			this.fraParameters.Controls.Add(this.Label3);
			this.fraParameters.Location = new System.Drawing.Point(598, 30);
			this.fraParameters.Name = "fraParameters";
			this.fraParameters.Size = new System.Drawing.Size(343, 140);
			this.fraParameters.TabIndex = 35;
			this.fraParameters.Text = "Parameters";
			// 
			// txtYear
			// 
			this.txtYear.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtYear.Location = new System.Drawing.Point(161, 30);
			this.txtYear.MaxLength = 4;
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(162, 40);
			this.txtYear.TabIndex = 31;
			this.txtYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYear_KeyPress);
			// 
			// txtDate
			// 
			this.txtDate.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.txtDate.Enabled = false;
			this.txtDate.Location = new System.Drawing.Point(161, 80);
			this.txtDate.MaxLength = 11;
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(162, 22);
			this.txtDate.TabIndex = 38;
			this.txtDate.TabStop = false;
			this.txtDate.Visible = false;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 44);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(50, 16);
			this.Label4.TabIndex = 37;
			this.Label4.Text = "YEAR";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 94);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(120, 16);
			this.Label3.TabIndex = 36;
			this.Label3.Text = "DATE VACCINATED";
			this.Label3.Visible = false;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.lblKennelLicense);
			this.Frame1.Controls.Add(this.lblMonthlyReport);
			this.Frame1.Controls.Add(this.lblDogLicense);
			this.Frame1.Controls.Add(this.cmbDogLicense);
			this.Frame1.Controls.Add(this.chkPrintOptionalSection);
			this.Frame1.Controls.Add(this.cmbMonthlyReport);
			this.Frame1.Controls.Add(this.cmbKennelLicense);
			this.Frame1.Controls.Add(this.chkPrintExpiration);
			this.Frame1.Controls.Add(this.chkPrintOnFront);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(548, 264);
			this.Frame1.TabIndex = 32;
			this.Frame1.Text = "Report";
			// 
			// lblKennelLicense
			// 
			this.lblKennelLicense.AutoSize = true;
			this.lblKennelLicense.Location = new System.Drawing.Point(20, 144);
			this.lblKennelLicense.Name = "lblKennelLicense";
			this.lblKennelLicense.Size = new System.Drawing.Size(122, 16);
			this.lblKennelLicense.TabIndex = 9;
			this.lblKennelLicense.Text = "KENNEL LICENSE";
			// 
			// lblMonthlyReport
			// 
			this.lblMonthlyReport.AutoSize = true;
			this.lblMonthlyReport.Location = new System.Drawing.Point(20, 94);
			this.lblMonthlyReport.Name = "lblMonthlyReport";
			this.lblMonthlyReport.Size = new System.Drawing.Size(167, 16);
			this.lblMonthlyReport.TabIndex = 9;
			this.lblMonthlyReport.Text = "MONTHLY DOG REPORT";
			// 
			// lblDogLicense
			// 
			this.lblDogLicense.AutoSize = true;
			this.lblDogLicense.Location = new System.Drawing.Point(20, 44);
			this.lblDogLicense.Name = "lblDogLicense";
			this.lblDogLicense.Size = new System.Drawing.Size(99, 16);
			this.lblDogLicense.TabIndex = 9;
			this.lblDogLicense.Text = "DOG LICENSE";
			// 
			// chkPrintOptionalSection
			// 
			this.chkPrintOptionalSection.Location = new System.Drawing.Point(348, 87);
			this.chkPrintOptionalSection.Name = "chkPrintOptionalSection";
			this.chkPrintOptionalSection.Size = new System.Drawing.Size(150, 23);
			this.chkPrintOptionalSection.TabIndex = 4;
			this.chkPrintOptionalSection.Text = "Print optional section";
			// 
			// chkPrintExpiration
			// 
			this.chkPrintExpiration.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkPrintExpiration.Location = new System.Drawing.Point(20, 217);
			this.chkPrintExpiration.Name = "chkPrintExpiration";
			this.chkPrintExpiration.Size = new System.Drawing.Size(198, 23);
			this.chkPrintExpiration.TabIndex = 8;
			this.chkPrintExpiration.Text = "Print Re-Reg Expiration Date";
			this.chkPrintExpiration.Visible = false;
			// 
			// chkPrintOnFront
			// 
			this.chkPrintOnFront.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkPrintOnFront.Location = new System.Drawing.Point(20, 180);
			this.chkPrintOnFront.Name = "chkPrintOnFront";
			this.chkPrintOnFront.Size = new System.Drawing.Size(185, 23);
			this.chkPrintOnFront.TabIndex = 7;
			this.chkPrintOnFront.Text = "Print Dog Re-Reg on Front";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 0;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                                       ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 1;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit         ";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 2;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 3;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(403, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(78, 48);
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmPrinterSettings
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(968, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPrinterSettings";
			this.Text = "Default Printer Settings";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmPrinterSettings_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrinterSettings_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrinterSettings_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraParameters)).EndInit();
			this.fraParameters.ResumeLayout(false);
			this.fraParameters.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintOptionalSection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintExpiration)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrintOnFront)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
        private FCLabel lblKennelLicense;
        private FCLabel lblMonthlyReport;
        private FCLabel lblDogLicense;
    }
}
