//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.Clerk;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmPrintVitalRec : BaseForm, IView<IPrintVitalsViewModel>
    {
        private bool cancelling = true;
		public frmPrintVitalRec()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmPrintVitalRec(IPrintVitalsViewModel viewModel): this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			txtPrint = new System.Collections.Generic.List<FCButton>();
			txtPrint.AddControlArrayElement(txtPrint_0, 0);
			txtPrint.AddControlArrayElement(txtPrint_2, 2);
            this.Closing += FrmPrintVitalRec_Closing;
		}

        private void FrmPrintVitalRec_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel?.Cancel();
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmPrintVitalRec InstancePtr
		{
			get
			{
				return (frmPrintVitalRec)Sys.GetInstance(typeof(frmPrintVitalRec));
			}
		}

		protected frmPrintVitalRec _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsVitalRecordCertNum = new clsDRWrapper();
		bool boolPrintTest;
		double dblLineAdjust;
		int lngDeathNumber;

		public void Init(bool boolTestPrint, double dblAdjust = 0)
		{
			if (boolTestPrint)
			{
				boolPrintTest = true;
				dblLineAdjust = dblAdjust;
			}
			else
			{
				boolPrintTest = false;
				dblLineAdjust = 0;
			}
			this.Show(FCForm.FormShowEnum.Modeless);
		}

		private void frmPrintVitalRec_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
		}

		private void frmPrintVitalRec_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						return;
					}
			}
			//end switch
		}

		private void frmPrintVitalRec_Load(object sender, System.EventArgs e)
		{

			string strTemp = "";
			modGlobalFunctions.SetTRIOColors(this);
			txtAttest.Text = modClerkGeneral.Statics.ClerkName;
			// ClerkDefaults.LoadCustomizeInfo
			if (modGNBas.Statics.ClerkDefaults.DefaultAttestedBy)
			{
				strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("LastAttestedBy", "CK"));
				txtAttest.Text = fecherFoundation.Strings.Trim(strTemp);
			}
			cmbSafetyPaper.Visible = false;
			strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("SafetyPaperVersion", "CK", FCConvert.ToString(196)));
			if (Conversion.Val(strTemp) == 606)
			{
				cmbSafetyPaper.Text = "R0606";
				modGNBas.Statics.gintSafetyPaperType = 606;
			}
			else if (Conversion.Val(strTemp) == 31)
			{
				cmbSafetyPaper.Text = "VS-31";
				modGNBas.Statics.gintSafetyPaperType = 31;
			}
			else
			{
				cmbSafetyPaper.Text = "R196";
				modGNBas.Statics.gintSafetyPaperType = 196;
			}
			string vbPorterVar = modClerkGeneral.Statics.utPrintInfo.TblName;
            const string blankAbstractR402 = "Blank Abstract R402";

            const string vs10_R196R403 = "VS-10 R196,R403";

            const string vs10WordDoc = "VS-10 Word Doc";

            const string vs10_R0706 = "VS-10 R0706";

            if (vbPorterVar == "Births")
			{
				cmbSafetyPaper.Visible = true;
				chkPrint.Visible = true;
				this.Text = "Print a Birth Certificate";
				lblEvent.Text = "Birth Certificate";
				// Option1(0).Text = "VS-10 R186"
				cmbtion1.Items.Clear();
                cmbtion1.Items.Add(vs10WordDoc);
                cmbtion1.Items.Add(vs10_R196R403);
                cmbtion1.Items.Add(vs10_R0706);
				cmbtion1.Text = vs10_R196R403;
				chkPrint.Text = "Print Father's Information:";
				chkPrint.Visible = false;
				cmbtion1.Visible = true;
				strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("BirthCertificateType", "CK"));
				if (fecherFoundation.Strings.UCase(strTemp) == "R186" || fecherFoundation.Strings.UCase(strTemp) == "WORD")
				{
					cmbtion1.Text = vs10WordDoc;
				}
				else if (fecherFoundation.Strings.UCase(strTemp) == "R403")
				{
					cmbtion1.Text = vs10_R196R403;
				}
				else
				{
					cmbtion1.Text = vs10_R0706;
				}
			}
			else
            {
                if ((vbPorterVar == "Deaths") || (vbPorterVar == "NCDeath"))
                {
                    chkPrint.Visible = true;
                    this.Text = "Print a Death Certificate";
                    lblEvent.Text = "Death Certificate";
                    cmbtion1.Items.Clear();
                    cmbtion1.Items.Add("VS-30 Word Doc");
                    cmbtion1.Items.Add("VS-30 R191, R11/2006");
                    //FC:FINAL:BSE #3932 option is visible in vb6 application 
                    cmbtion1.Items.Add("Non-Confidential R2/2007");
                    //cmbtion1.Items.Add("Blank Abstract R402");
                    chkPrint.Text = "Print Causes of Death:";
                    chkPrint.Visible = true;
                    if (!boolPrintTest)
                    {
                        lngDeathNumber = FCConvert.ToInt32(modGNBas.Statics.rsDeathCertificate.Get_Fields_Int32("ID"));
                    }
                    else
                    {
                        lngDeathNumber = 0;
                    }
                    cmbtion1.Text = "VS-30 R191, R11/2006";
                    strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("DeathCertificateType", "CK"));
                    if (fecherFoundation.Strings.UCase(strTemp) == "R186" || fecherFoundation.Strings.UCase(strTemp) == "WORD")
                    {
                        cmbtion1.Text = "VS-30 Word Doc";
                    }
                    else if (fecherFoundation.Strings.UCase(strTemp) == "R191")
                        {
                            cmbtion1.Text = "Non-Confidential R2/2007";
                        }
                        else
                        {
                            cmbtion1.Text = "VS-30 R191, R11/2006";
                        }
                    if (modClerkGeneral.Statics.utPrintInfo.TblName == "NCDeath")
                    {
                        cmbtion1.Text = blankAbstractR402;
                    }
                }
                else
                {
                    const string vsWordDocument = "VS-20 Word Document";
                    const string R0706 = "R0706";

                    if (vbPorterVar == "Marriages")
                    {
                        cmbSafetyPaper.Visible = true;
                        chkPrint.Visible = false;
                        chkPrint.Text = "Print on Pre-Printed Cert";
                        this.Text = "Print Marriage Certificate";
                        lblEvent.Text = "Marriage Certificate";
                        cmbtion1.Items.Clear();
                        cmbtion1.Items.Add(vsWordDocument);
                        cmbtion1.Items.Add(R0706);
                        cmbtion1.Items.Add(blankAbstractR402);
                        strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("MarriageCertificateType", "CK", "Word"));
                        if (fecherFoundation.Strings.UCase(strTemp) == R0706)
                        {
                            cmbtion1.Text = R0706;
                        }
                        else
                        {
                            cmbtion1.Text = vsWordDocument;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error in Case Select Print Vital Records", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }

            if (modClerkGeneral.Statics.utPrintInfo.PreView)
			{
				txtPrint[2].Text = "Print/Preview";
			}
			else
			{
				txtPrint[2].Text = "Print";
			}
			modGlobalFunctions.SetFixedSize(this, 1);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			txtVitalCertNum.Enabled = false;
			modClerkGeneral.Statics.utPrintInfo.PreView = 0 != VbTriState.vbTrue;
			modClerkGeneral.Statics.utPrintInfo.DontShowBottomHeaders = false;
			if (cmbtion1.Text == "VS-10 R0706" || cmbtion1.Text == "Blank Abstract R402")
			{
				if (chkDontShowBottomHeadings.CheckState == Wisej.Web.CheckState.Checked)
				{
					modClerkGeneral.Statics.utPrintInfo.DontShowBottomHeaders = true;
				}
			}
			AttestTitle();
			FormPrint();
			txtVitalCertNum.Enabled = true;
		}

		private void Option1_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			chkDontShowBottomHeadings.Visible = false;
			cmbSafetyPaper.Visible = false;
            //FC:FINAl:BSE #3877 label should be visible only in case 0
            lblSafetyPaper.Visible = false;
            //FC:FINAL:BSE #3932 selected index is different than options in vb6 application(one option is invisible) 
			switch (cmbtion1.Text)
			{
				case "VS-30 Word Doc":
                case "VS-10 Word Doc":
                case "VS-20 Word Document":
                    {
						modClerkGeneral.Statics.utPrintInfo.GeneralForm = false;
						cmbSafetyPaper.Visible = true;
                        //FC:FINAL:BSE #3877 label should be visible only in case 0
                        lblSafetyPaper.Visible = true;
						if (fecherFoundation.Strings.UCase(modClerkGeneral.Statics.utPrintInfo.TblName) == "DEATHS" || fecherFoundation.Strings.UCase(modClerkGeneral.Statics.utPrintInfo.TblName) == "NCDEATH")
						{
							chkPrint.Visible = true;
						}
						break;
					}
				case "VS-30 R191, R11/2006":
				case "VS-10 R191, R11/2006":
                case "R0706":
                    {
						modClerkGeneral.Statics.utPrintInfo.GeneralForm = true;
						if (fecherFoundation.Strings.UCase(modClerkGeneral.Statics.utPrintInfo.TblName) == "DEATHS" || fecherFoundation.Strings.UCase(modClerkGeneral.Statics.utPrintInfo.TblName) == "NCDEATH")
						{
							chkPrint.Visible = true;
						}
						break;
					}
				case "Blank Abstract R402":
				case "VS-10 R0706":
					{
						if (fecherFoundation.Strings.UCase(modClerkGeneral.Statics.utPrintInfo.TblName) == "DEATHS" || fecherFoundation.Strings.UCase(modClerkGeneral.Statics.utPrintInfo.TblName) == "NCDEATH")
						{
							// Or UCase(utPrintInfo.TblName) = "BIRTHS" Then
                            chkDontShowBottomHeadings.Visible = true;
							chkPrint.Visible = true;
						}
						break;
					}
				case "Non-Confidential R2/2007":
					{
						if (fecherFoundation.Strings.UCase(modClerkGeneral.Statics.utPrintInfo.TblName) == "DEATHS" || fecherFoundation.Strings.UCase(modClerkGeneral.Statics.utPrintInfo.TblName) == "NCDEATH")
						{
							chkPrint.Visible = false;
						}
						break;
					}
			}
			//end switch
		}

		private void Option1_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtion1.SelectedIndex;
			Option1_CheckedChanged(index, sender, e);
		}

		private void txtPrint_Click(int Index, object sender, System.EventArgs e)
		{
			/*? On Error Resume Next  */
			const int lPrint = 0;
			const int lPreview = 2;
			const int lCancel = 1;
			switch (Index)
			{
			// ****** Print
				case lPrint:
					{
						if (txtVitalCertNum.Text == "")
						{
							frmPrintVitalRec.InstancePtr.Hide();
							MessageBox.Show("The Vital Record Certificate Number is Required: Print Aborted", "Invalid Certificate Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
							frmPrintVitalRec.InstancePtr.Show(App.MainForm);
							txtVitalCertNum.Focus();
							return;
						}
						else
						{
							modClerkGeneral.Statics.utPrintInfo.CertNumber = txtVitalCertNum.Text;
						}
						// utPrintInfo.PreView = vbFalse
						modClerkGeneral.Statics.utPrintInfo.PrintDate = DateTime.Now;
						AttestTitle();
						// ****** Test Print?
						if (txtVitalCertNum.Text == "TEST")
						{
							// MsgBox "Test Print - No information committed to the data base.", vbOKOnly, "Test Print"
							FormPrint();
						}
						else
						{
							SavePrintInfo();
							FormPrint();
							if (MessageBox.Show("Printing completed. Do you wish to print another?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								frmPrintVitalRec.InstancePtr.txtVitalCertNum.Text = string.Empty;
								frmPrintVitalRec.InstancePtr.Show(App.MainForm);
								frmPrintVitalRec.InstancePtr.txtVitalCertNum.Focus();
							}
							else
							{
								frmPrintVitalRec.InstancePtr.Close();
							}
						}
						// ****** PreView
						break;
					}
				case lPreview:
					{
						txtVitalCertNum.Enabled = false;
						modClerkGeneral.Statics.utPrintInfo.PreView = 0 != VbTriState.vbTrue;
						AttestTitle();
						FormPrint();
						txtVitalCertNum.Enabled = true;
						break;
					}
				case lCancel:
					{
						Close();
						break;
					}
				default:
					{
						MessageBox.Show("Error in the Case Select for Print.", "Error In Print", MessageBoxButtons.OK, MessageBoxIcon.Information);
						break;
					}
			}
			//end switch
		}

		private void txtPrint_Click(object sender, System.EventArgs e)
		{
			int index = txtPrint.GetIndex((FCButton)sender);
			txtPrint_Click(index, sender, e);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			rsVitalRecordCertNum = null;
			if (!boolPrintTest)
			{
				string vbPorterVar = modClerkGeneral.Statics.utPrintInfo.TblName;
				if (vbPorterVar == "Births")
				{
					//if (modGNBas.Statics.gboolSearchPrint)
					//{
					//	frmBirthSearchA.InstancePtr.Show(App.MainForm);
					//}
					//else
					//{
					//	frmBirths.InstancePtr.Show(App.MainForm);
					//	modGNBas.Statics.rsBirthCertificate = null;
					//}
				}
				else if ((vbPorterVar == "Deaths") || (vbPorterVar == "NCDeath"))
				{
					//if (modGNBas.Statics.gboolSearchPrint)
					//{
					//	frmDeathSearchA.InstancePtr.Show(App.MainForm);
					//}
					//else
					//{
					//	modGNBas.Statics.Response = lngDeathNumber;
					//	frmDeaths.InstancePtr.Show(App.MainForm);
					//}
				}
				else if (vbPorterVar == "Marriages")
				{
                    //if (modGNBas.Statics.gboolSearchPrint)
                    //{
                    //	frmMarriageSearchA.InstancePtr.Show(App.MainForm);
                    //}
                    //else
                    //{
                    //	frmMarriages.InstancePtr.Show(App.MainForm);
                    modGNBas.Statics.rsMarriageCertificate = null;
                    //}
                }
				else
				{
					MessageBox.Show("Error in Case Select Unload for frmPrintVitalRec");
				}
				modGNBas.Statics.gboolPrintPreview = false;
				modGNBas.Statics.gboolSearchPrint = false;
				//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
			}
		}

		public void SavePrintInfo()
		{
			string strSQL;
			try
			{
				// On Error GoTo Handler
				fecherFoundation.Information.Err().Clear();
				strSQL = "SELECT * FROM VitalRecordCertNum";
				rsVitalRecordCertNum.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
				if (rsVitalRecordCertNum.EndOfFile())
				{
					rsVitalRecordCertNum.AddNew();
				}
				else
				{
					rsVitalRecordCertNum.Edit();
				}
				rsVitalRecordCertNum.Set_Fields("CertNumber", modClerkGeneral.Statics.utPrintInfo.CertNumber);
				rsVitalRecordCertNum.Set_Fields("PrintDate", modClerkGeneral.Statics.utPrintInfo.PrintDate);
				rsVitalRecordCertNum.Set_Fields("tblKeyNum", modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
				rsVitalRecordCertNum.Set_Fields("AttestedBy", modClerkGeneral.Statics.utPrintInfo.AttestedBy);
				rsVitalRecordCertNum.Set_Fields("tblName", modClerkGeneral.Statics.utPrintInfo.TblName);
				rsVitalRecordCertNum.Update();
				return;
			}
			catch (Exception ex)
			{
				// Handler:
				if (fecherFoundation.Information.Err(ex).Number == 3022)
				{
				}
				else
				{
					MessageBox.Show(fecherFoundation.Information.Err(ex).Description, "error" + " -" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + "Print Aborted", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		public void AttestTitle()
		{
			modRegistry.SaveRegistryKey("LastAttestedBy", txtAttest.Text, "CK");
			if (cmbtion2.SelectedIndex == 0)
			{
				modClerkGeneral.Statics.utPrintInfo.AttestedBy = txtAttest.Text + ", Municipal Clerk";
			}
			else if (cmbtion2.SelectedIndex == 1)
			{
				modClerkGeneral.Statics.utPrintInfo.AttestedBy = txtAttest.Text + ", State Registrar";
			}
			else if (cmbtion2.SelectedIndex == 2)
			{
				modClerkGeneral.Statics.utPrintInfo.AttestedBy = txtAttest.Text + ", Assistant Clerk";
			}
			else if (cmbtion2.SelectedIndex == 3)
			{
				modClerkGeneral.Statics.utPrintInfo.AttestedBy = txtAttest.Text + ", Deputy Clerk";
			}
			else
			{
				MessageBox.Show("Error in Case Select Attest Title:");
			}
			if (Strings.Left(fecherFoundation.Strings.Trim(modClerkGeneral.Statics.utPrintInfo.AttestedBy), 1) == ",")
			{
				modClerkGeneral.Statics.utPrintInfo.AttestedBy = fecherFoundation.Strings.Trim(modClerkGeneral.Statics.utPrintInfo.AttestedBy);
				modClerkGeneral.Statics.utPrintInfo.AttestedBy = fecherFoundation.Strings.Trim(Strings.Right(modClerkGeneral.Statics.utPrintInfo.AttestedBy, modClerkGeneral.Statics.utPrintInfo.AttestedBy.Length - 1));
			}
		}

		private void Check1_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check1.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtVitalCertNum.Enabled = false;
				txtVitalCertNum.Text = "TEST";
				txtAttest.Enabled = false;
				txtAttest.Text = "TEST";
			}
			else
			{
				txtVitalCertNum.Enabled = true;
				txtVitalCertNum.Text = "";
				txtAttest.Enabled = true;
				txtAttest.Text = "";
			}
		}

		private void txtVitalCertNum_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: Ans As object	OnWrite(DialogResult)
			DialogResult Ans = 0;
			string strSQL = "";
			try
			{
				// On Error GoTo Handler
				fecherFoundation.Information.Err().Clear();
				rsVitalRecordCertNum.OpenRecordset("VitalRecordCertNum", modGNBas.DEFAULTDATABASE);
				rsVitalRecordCertNum.FindFirstRecord("PrimaryKey", txtVitalCertNum.Text);
				if (rsVitalRecordCertNum.NoMatch)
				{
					// MsgBox "no match"
				}
				else
				{
					frmPrintVitalRec.InstancePtr.Hide();
					Ans = MessageBox.Show("The Certificate Number you have entered is already in the Data Base" + "\r\n" + " Do you wish to enter another Number or Cancel.", "Duplicate dBase Key", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
					if (Ans == DialogResult.OK)
					{
						txtVitalCertNum.SelectionStart = 0;
						txtVitalCertNum.SelectionLength = txtVitalCertNum.Text.Length;
						frmPrintVitalRec.InstancePtr.Show(App.MainForm);
					}
					else
					{
						txtVitalCertNum.Text = "";
						rsVitalRecordCertNum = null;
						frmBirths.InstancePtr.Show(App.MainForm);
					}
				}
				rsVitalRecordCertNum = null;
				return;
			}
			catch (Exception ex)
			{
				// Handler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " -Error Number  " + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Number and Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
				{
					/*? Resume Next; */}
			}
		}

		public void FormPrint()
		{
			modCashReceiptingData.Statics.typeCRData.NumFirsts = 0;
			modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
			modCashReceiptingData.Statics.typeCRData.NumSeconds = 0;
			// vbPorter upgrade warning: dblAmt1 As double	OnWrite(int, Decimal)
			decimal dblAmt1;
			// vbPorter upgrade warning: dblAmt2 As double	OnWrite(int, Decimal)
			decimal dblAmt2;
			dblAmt1 = 0;
			dblAmt2 = 0;
			if (cmbSafetyPaper.Text == "R0606")
			{
				modRegistry.SaveRegistryKey("SafetyPaperVersion", "606", "CK");
				modGNBas.Statics.gintSafetyPaperType = 606;
			}
			else if (cmbSafetyPaper.Text == "VS-31")
			{
				modRegistry.SaveRegistryKey("SafetyPaperVersion", "31", "CK");
				modGNBas.Statics.gintSafetyPaperType = 31;
			}
			else
			{
				modRegistry.SaveRegistryKey("SafetyPaperVersion", "196", "CK");
				modGNBas.Statics.gintSafetyPaperType = 196;
			}
			string vbPorterVar = modClerkGeneral.Statics.utPrintInfo.TblName;
			if (vbPorterVar == "Births")
			{
				if (cmbtion1.Text == "VS-10 R196,R403" || cmbtion1.Text == "VS-30 R191, R11/2006" || cmbtion1.Text == "R0706")
				{
					Birth_R196(403);
					// **** General form *****
					modRegistry.SaveRegistryKey("BirthCertificateType", "R403", "CK");
				}
				else if (cmbtion1.Text == "VS-10 R0706" || cmbtion1.Text == "Blank Abstract R402")
				{
					Birth_R196(706);
				}
				else
				{
					// Birth_R186      '**** Specific form *****
					Birth_WordDoc();
					modRegistry.SaveRegistryKey("BirthCertificateType", "Word", "CK");
				}
				if ( modCashReceiptingData.Statics.bolFromWindowsCR)
				{
					if (modCashReceiptingData.Statics.typeCRData.NumFirsts + modCashReceiptingData.Statics.typeCRData.NumSubsequents > 0)
					{
						modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(frmChargeCert.InstancePtr.Init(FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewBirth + modDogs.Statics.typClerkFees.BirthStateFee)), FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementBirth + modDogs.Statics.typClerkFees.BirthReplacementStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents), "000000.00"));
						dblAmt1 = Convert.ToDecimal(((modDogs.Statics.typClerkFees.NewBirth * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementBirth * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
						dblAmt2 = Convert.ToDecimal(((modDogs.Statics.typClerkFees.BirthStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.BirthReplacementStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
						//modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(dblAmt1, "000000.00"));
						//modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(dblAmt2, "000000.00"));
						//modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						//modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						//modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						//modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						//modCashReceiptingData.Statics.typeCRData.PartyID = 0;
						//modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
						//modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);

                        ViewModel.UpdateTransaction(dblAmt1,dblAmt2);
                        ViewModel.CompleteTransaction();
                        CloseWithoutCancel();
                    }
				}
			}
			else if ((vbPorterVar == "Deaths") || (vbPorterVar == "NCDeath"))
			{
				if (cmbtion1.Text == "VS-10 R0706" || cmbtion1.Text == "Blank Abstract R402")
				{
					modRegistry.SaveRegistryKey("DeathCertificateType", "R191", "CK");
					Death_R196();
					// **** General form *****
				}
				else if (cmbtion1.Text == "Non-Confidential R2/2007")
				{
					Death_NonConfidential();
				}
				else
				{
					if (cmbtion1.Text == "VS-10 R196,R403" || cmbtion1.Text == "VS-30 R191, R11/2006" || cmbtion1.Text == "R0706")
					{
						modRegistry.SaveRegistryKey("DeathCertificateType", "R11/2006", "CK");
					}
					else
					{
						// Call SaveRegistryKey("DeathCertificateType", "R186", "CK")
						modRegistry.SaveRegistryKey("DeathCertificateType", "WORD", "CK");
					}
					Death_R186();
					// **** Specific form *****
				}
				if ( modCashReceiptingData.Statics.bolFromWindowsCR)
				{
					//if (modCashReceiptingData.Statics.typeCRData.NumFirsts + modCashReceiptingData.Statics.typeCRData.NumSubsequents + modCashReceiptingData.Statics.typeCRData.NumSeconds > 0)
					//{
						modCashReceiptingData.Statics.typeCRData.Type = "DEA";
						if (modGNBas.Statics.rsDeathCertificate.Get_Fields_Boolean("USArmedServices") == true)
						{
							// If frmDeaths.optMilitarySrv(0) Then
							// veterans fee
							// .Amount1 = Format((intPrinted * typClerkFees.AdditionalFee5), "000000.00")
							// .Amount1 = Format(frmChargeCert.Init(CDbl(typClerkFees.AdditionalFee5), CDbl(typClerkFees.AdditionalFee5), .NumFirsts, .NumSubsequents), "000000.00")
							if (modCashReceiptingData.Statics.typeCRData.NumSubsequents > 0)
							{
								modCashReceiptingData.Statics.typeCRData.NumSeconds = 1;
								modCashReceiptingData.Statics.typeCRData.NumSubsequents -= 1;
							}
							modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(frmChargeCert.InstancePtr.Init(FCConvert.ToDouble((modDogs.Statics.typClerkFees.AdditionalFee5 + modDogs.Statics.typClerkFees.DeathVetStateFee)), FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementDeath + modDogs.Statics.typClerkFees.DeathReplacementStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents, "Subsequent Copies", "Second Copy", FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewDeath + modDogs.Statics.typClerkFees.DeathStateFee)), modCashReceiptingData.Statics.typeCRData.NumSeconds), "000000.00"));
							dblAmt1 = Convert.ToDecimal(((modDogs.Statics.typClerkFees.AdditionalFee5 * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementDeath * modCashReceiptingData.Statics.typeCRData.NumSubsequents) + (modDogs.Statics.typClerkFees.NewDeath * modCashReceiptingData.Statics.typeCRData.NumSeconds)));
							dblAmt2 = Convert.ToDecimal(((modDogs.Statics.typClerkFees.DeathVetStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.DeathReplacementStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents) + (modDogs.Statics.typClerkFees.DeathStateFee * modCashReceiptingData.Statics.typeCRData.NumSeconds)));
						}
						else
						{
							modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(frmChargeCert.InstancePtr.Init(FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewDeath + modDogs.Statics.typClerkFees.DeathStateFee)), FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementDeath + modDogs.Statics.typClerkFees.DeathReplacementStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents), "000000.00"));
							dblAmt1 = Convert.ToDecimal(((modDogs.Statics.typClerkFees.NewDeath * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementDeath * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
							dblAmt2 = Convert.ToDecimal(((modDogs.Statics.typClerkFees.DeathStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.DeathReplacementStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
						}
						modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(dblAmt1, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(dblAmt2, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
						modCashReceiptingData.Statics.typeCRData.PartyID = 0;
						modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
						//Application.Exit();
                        ViewModel.UpdateTransaction(dblAmt1, dblAmt2);
                        ViewModel.CompleteTransaction();
                        CloseWithoutCancel();
                   // }
				}
			}
			else if (vbPorterVar == "Marriages")
			{
				if (cmbtion1.Text == "VS-10 R196,R403" || cmbtion1.Text == "VS-30 R191, R11/2006" || cmbtion1.Text == "R0706")
				{
					modRegistry.SaveRegistryKey("MarriageCertificateType", "R0706", "CK");
					Marriage_R186();
					// If Option1(1).Value = True Then
					// Marriage_R196 (False)     '**** General form *****
				}
				else if (cmbtion1.Text == "VS-10 R0706" || cmbtion1.Text == "Blank Abstract R402")
				{
					Marriage_R196(true);
					// general without some captions
				}
				else
				{
					// Marriage_R186      '**** Specific form *****
					modRegistry.SaveRegistryKey("MarriageCertificateType", "Word", "CK");
					Marriage_WordDoc();
				}
				//App.DoEvents();
				if (modCashReceiptingData.Statics.gboolFromDosTrio == true || modCashReceiptingData.Statics.bolFromWindowsCR)
				{
					if (modCashReceiptingData.Statics.typeCRData.NumFirsts + modCashReceiptingData.Statics.typeCRData.NumSubsequents > 0)
					{
						modCashReceiptingData.Statics.typeCRData.Type = "MAR";
						modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(frmChargeCert.InstancePtr.Init(FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewMarriageCert)) + FCConvert.ToDouble((modDogs.Statics.typClerkFees.MarriageCertStateFee)), FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementMarriageCert)) + FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementMarriageCertStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents), "000000.00"));
						dblAmt1 = Convert.ToDecimal(((modDogs.Statics.typClerkFees.NewMarriageCert * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementMarriageCert * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
						dblAmt2 = Convert.ToDecimal(((modDogs.Statics.typClerkFees.MarriageCertStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementMarriageCertStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents)));
						modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(dblAmt1, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(dblAmt2, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
						modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
						modCashReceiptingData.Statics.typeCRData.PartyID = 0;
						modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
						Application.Exit();
					}
				}
			}
			else
			{
				MessageBox.Show("Error in Case Select FormPrint()");
			}
		}

		public void Birth_R186()
		{
			// *****  This form HAS labels
			if (!boolPrintTest)
			{
				switch (modClerkGeneral.Statics.utPrintInfo.PreView)
				{
					case true:
						{
							frmReportViewer.InstancePtr.Init(rptBirth186.InstancePtr, showModal: this.Modal);
							//rptBirth186.InstancePtr.Show(FCForm.FormShowEnum.Modal);
							break;
						}
					case false:
						{
							modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
							modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
							rptBirth186.InstancePtr.PrintReport(false);
							frmPrintVitalRec.InstancePtr.Show(App.MainForm);
							break;
						}
					default:
						{
							MessageBox.Show("Error in: Case Select Birth_R196 Preview", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
							break;
						}
				}
				//end switch
			
			}
			else
			{
				rptBirth186.InstancePtr.Init(boolPrintTest, dblLineAdjust);
			}
		}

		public void Birth_R196(int intType)
		{
			// *****  This form is WITHOUT labels
			if (!boolPrintTest)
			{
				//frmBirths.InstancePtr.Hide();
				switch (modClerkGeneral.Statics.utPrintInfo.PreView)
				{
					case true:
						{
							// rptBirth196.Show 1
							rptBirth196.InstancePtr.Init(false, 0, intType, true);
							// Call rptVS20WordDoc.Init(False, , intType, True)
							break;
						}
					case false:
						{
							modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
							modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
							rptBirth196.InstancePtr.PrintReport(false);
							rptBirth196.InstancePtr.Init(false, 0, intType, false);
							//frmPrintVitalRec.InstancePtr.Show(App.MainForm);
							break;
						}
					default:
						{
							MessageBox.Show("Error in: Case Select Birth_R186 Preview", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
				}
				//end switch
			}
			else
			{
				rptBirth196.InstancePtr.Init(boolPrintTest, dblLineAdjust);
			}
			// Call UpdatePrintInformation
		}

		public void Birth_WordDoc()
		{
			if (!boolPrintTest)
			{
				//frmBirths.InstancePtr.Hide();
				switch (modClerkGeneral.Statics.utPrintInfo.PreView)
				{
					case true:
						{
							// Call frmReportViewer.init(rptVS35WordDoc)
							rptVS20WordDoc.InstancePtr.Init(false, boolShow: true);
							break;
						}
					case false:
						{
							modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
							modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
							rptVS20WordDoc.InstancePtr.PrintReport(false);
							rptVS20WordDoc.InstancePtr.Init(false, 0, false);
							frmPrintVitalRec.InstancePtr.Show(App.MainForm);
							break;
						}
					default:
						{
							MessageBox.Show("Error in birth preview", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
				}
				//end switch
			}
			else
			{
				rptVS20WordDoc.InstancePtr.Init(boolPrintTest, dblLineAdjust);
			}
		}



		public void Marriage_WordDoc()
		{
			frmMarriages.InstancePtr.Hide();
			if (!boolPrintTest)
			{
				switch (modClerkGeneral.Statics.utPrintInfo.PreView)
				{
					case true:
						{
							//rptMarriageAbstractWordDoc.InstancePtr.ToolbarVisible = true;
							frmReportViewer.InstancePtr.Init(rptMarriageAbstractWordDoc.InstancePtr, showModal: this.Modal);
							//rptMarriageAbstractWordDoc.InstancePtr.Show(FCForm.FormShowEnum.Modal);
							// rptMCnoL.ToolbarVisible = True
							// rptMCnoL.Show 1
							break;
						}
					case false:
						{
							modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
							modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
							rptMarriageAbstractWordDoc.InstancePtr.PrintReport(false);
							// rptMCnoL.PrintReport False
							frmPrintVitalRec.InstancePtr.Show(App.MainForm);
							break;
						}
					default:
						{
							MessageBox.Show("Error in: Case Select Marriage_WordDoc Preview", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
				}
				//end switch
				// Call UpdatePrintInformation
			}
			else
			{
				// Call rptMCnoL.Init(boolPrintTest, dblLineAdjust)
				rptMarriageAbstractWordDoc.InstancePtr.Init(boolPrintTest, dblLineAdjust);
			}
		}

		public void Marriage_R186()
		{
			// *****  This is form is WITHOUT labels
			frmMarriages.InstancePtr.Hide();
			if (!boolPrintTest)
			{
				switch (modClerkGeneral.Statics.utPrintInfo.PreView)
				{
					case true:
						{
							// rptMarriageAbstractWordDoc.ToolbarVisible = True
							// rptMarriageAbstractWordDoc.Show 1
							//rptMCnoL.InstancePtr.ToolbarVisible = true;
							frmReportViewer.InstancePtr.Init(rptMCnoL.InstancePtr, showModal: this.Modal);
							//rptMCnoL.InstancePtr.Show(FCForm.FormShowEnum.Modal);
							break;
						}
					case false:
						{
							modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
							modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
							// rptMarriageAbstractWordDoc.PrintReport False
							rptMCnoL.InstancePtr.PrintReport(false);
							frmPrintVitalRec.InstancePtr.Show(App.MainForm);
							break;
						}
					default:
						{
							MessageBox.Show("Error in: Case Select Marriage_R186 Preview", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
				}
				//end switch
				// Call UpdatePrintInformation
			}
			else
			{
				rptMCnoL.InstancePtr.Init(boolPrintTest, dblLineAdjust);
				// Call rptMarriageAbstractWordDoc.Init(boolPrintTest, dblLineAdjust)
			}
		}

		public void Marriage_R196(bool boolR402 = false)
		{
			// *****  This is form HAS labels
			frmMarriages.InstancePtr.Hide();
			switch (modClerkGeneral.Statics.utPrintInfo.PreView)
			{
				case true:
					{
						rptLabelMarriages.InstancePtr.Init(boolR402, boolPrintTest, dblLineAdjust);
						break;
					}
				case false:
					{
						// rptMCwL.PrintReport False
						// frmPrintVitalRec.Show 
						break;
					}
				default:
					{
						MessageBox.Show("Error in: Case Select Abstract Preview", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
						break;
					}
			}
			//end switch
			// Call UpdatePrintInformation
		}

		public void Death_R196()
		{
			// *****  This is form is HAS labels
			if (!boolPrintTest)
			{
				switch (modClerkGeneral.Statics.utPrintInfo.PreView)
				{
					case true:
						{
							frmReportViewer.InstancePtr.Init(rptLabelDeaths.InstancePtr, showModal: this.Modal);
							//rptLabelDeaths.InstancePtr.Show(FCForm.FormShowEnum.Modal);
							break;
						}
					case false:
						{
							// rptDCwL.PrintReport False
							// frmPrintVitalRec.Show 
							break;
						}
					default:
						{
							MessageBox.Show("Error in: Case Select Deaths_R196 Preview", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
							break;
						}
				}
				//end switch
				// Call UpdatePrintInformation
			}
			else
			{
				rptLabelDeaths.InstancePtr.Init(true, dblLineAdjust);
			}
		}

		public void Death_R186()
		{
			// *****  This is form is WITHOUT labels
			if (!boolPrintTest)
			{

				switch (modClerkGeneral.Statics.utPrintInfo.PreView)
				{
					case true:
						{
							// rptDCnoL.Show 1
							if (cmbtion1.Text == "VS-10 Word Doc" || cmbtion1.Text == "VS-30 Word Doc" || cmbtion1.Text == "VS-20 Word Document")
							{
								rptDeathCertWordDoc.InstancePtr.Init(false, 0);
								// Call rptDCnoL.Init(False, 0, True)
							}
							else
							{
								rptDCnoL.InstancePtr.UserData = "";
								rptDCnoL.InstancePtr.Init(false, 0, false);
							}
							break;
						}
					case false:
						{
							rptDCnoL.InstancePtr.UserData = "";
							modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
							modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
							rptDCnoL.InstancePtr.PrintReport(false);
							frmPrintVitalRec.InstancePtr.Show(App.MainForm);
							break;
						}
					default:
						{
							MessageBox.Show("Error in: Case Select Deaths_R186 Preview", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
							break;
						}
				}
				//end switch
			
			}
			else
			{
				// Call rptDCnoL.Init(True, dblLineAdjust)
				rptDeathCertWordDoc.InstancePtr.Init(true, dblLineAdjust);
			}
		}

		public void Death_NonConfidential()
		{
			if (!boolPrintTest)
			{
				if (modClerkGeneral.Statics.utPrintInfo.PreView)
				{
					rptDeathNonConfidential.InstancePtr.Init(false, 0);
				}
				else
				{
					modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
					modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
					rptDeathNonConfidential.InstancePtr.Init(false, 0);
					frmPrintVitalRec.InstancePtr.Show(App.MainForm);
				}
			}
			else
			{
				rptDeathNonConfidential.InstancePtr.Init(true, dblLineAdjust);
			}
		}

        public IPrintVitalsViewModel ViewModel { get; set; }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }
    }
}
