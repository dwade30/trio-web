//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDispositionLoc : BaseForm
	{
		public frmDispositionLoc()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDispositionLoc InstancePtr
		{
			get
			{
				return (frmDispositionLoc)Sys.GetInstance(typeof(frmDispositionLoc));
			}
		}

		protected frmDispositionLoc _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsState = new clsDRWrapper();
		const int NEWRECORD = 1;
		const int MODIFYRECORD = 2;
		const int DISPOSITIONSTATUSCOL = 2;

		private void frmDispositionLoc_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDispositionLoc_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// 09/24/03 State said no more all caps
				// If LocationGrid.Col <> 2 Then KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDispositionLoc_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDispositionLoc properties;
			//frmDispositionLoc.ScaleWidth	= 5940;
			//frmDispositionLoc.ScaleHeight	= 4065;
			//frmDispositionLoc.LinkTopic	= "Form1";
			//frmDispositionLoc.LockControls	= -1  'True;
			//End Unmaped Properties
			LoadLocations();
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this);
			LocationGrid.ColWidth(1, 5300);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			DeleteLocation();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			NewLocation();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveLocations();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveLocations() == false)
				return;
			//mnuExit_Click();
		}

		private void SSTab1_Click(ref int PreviousTab)
		{
			LocationGrid.Focus();
		}
		// *********************************************
		private void LocationGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			if (FCConvert.ToDouble(LocationGrid.TextMatrix(LocationGrid.Row, DISPOSITIONSTATUSCOL)) != 1)
				LocationGrid.TextMatrix(LocationGrid.Row, DISPOSITIONSTATUSCOL, FCConvert.ToString(2));
		}

		private void NewLocation()
		{
			LocationGrid.AddItem("", LocationGrid.Rows);
			LocationGrid.TextMatrix(LocationGrid.Rows - 1, DISPOSITIONSTATUSCOL, FCConvert.ToString(1));
			LocationGrid.TopRow = LocationGrid.Rows - 1;
			LocationGrid.Row = LocationGrid.Rows - 1;
			LocationGrid.Col = 1;
		}

		private void LoadLocations()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from DispositionLocation ORDER BY Name", modGNBas.DEFAULTCLERKDATABASE);
			LocationGrid.Rows = rsData.RecordCount() + 1;
			LocationGrid.Cols = 3;
			LocationGrid.TextMatrix(0, 0, "ID");
			LocationGrid.TextMatrix(0, 1, "Location");
			LocationGrid.ColHidden(0, true);
			LocationGrid.ColHidden(DISPOSITIONSTATUSCOL, true);
			//LocationGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			if (LocationGrid.Rows > 1)
			{
				LocationGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, LocationGrid.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			LocationGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1, true);
			LocationGrid.ColWidth(1, 5400);
			LocationGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				LocationGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				LocationGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_String("Name")));
				LocationGrid.TextMatrix(intCounter, DISPOSITIONSTATUSCOL, FCConvert.ToString(0));
				rsData.MoveNext();
			}
			LocationGrid.TopRow = 1;
			if (LocationGrid.Rows > 1)
				LocationGrid.Row = 1;
			LocationGrid.Col = 1;
		}

		private void DeleteLocation()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (LocationGrid.Row != 0)
				{
					if (MessageBox.Show("Are you sure you wish to delete this Location?   " + LocationGrid.TextMatrix(LocationGrid.Row, 1), "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (FCConvert.ToDouble(LocationGrid.TextMatrix(LocationGrid.Row, DISPOSITIONSTATUSCOL)) == 1)
						{
							LocationGrid.RemoveItem(LocationGrid.Row);
						}
						else
						{
							rsState.Execute("Delete from DispositionLocation where ID = " + LocationGrid.TextMatrix(LocationGrid.Row, 0), modGNBas.DEFAULTCLERKDATABASE);
							LocationGrid.RemoveItem(LocationGrid.Row);
						}
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private bool SaveLocations()
		{
			bool SaveLocations = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
				int intCounter;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				LocationGrid.Row = 0;
				for (intCounter = 1; intCounter <= (LocationGrid.Rows - 1); intCounter++)
				{
					if (fecherFoundation.Strings.Trim(LocationGrid.TextMatrix(intCounter, 1)).Length <= 0)
					{
						MessageBox.Show("Location must be entered.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						LocationGrid.Row = intCounter;
						LocationGrid.TopRow = intCounter;
						LocationGrid.Col = 1;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveLocations;
					}
					if (FCConvert.ToDouble(LocationGrid.TextMatrix(intCounter, DISPOSITIONSTATUSCOL)) == NEWRECORD)
					{
						rsState.Execute("Insert into DispositionLocation (Name) VALUES ('" + LocationGrid.TextMatrix(intCounter, 1) + "')", modGNBas.DEFAULTCLERKDATABASE);
						LocationGrid.TextMatrix(intCounter, DISPOSITIONSTATUSCOL, FCConvert.ToString(0));
					}
					else if (FCConvert.ToDouble(LocationGrid.TextMatrix(intCounter, DISPOSITIONSTATUSCOL)) == MODIFYRECORD)
					{
						rsState.Execute("Update DispositionLocation Set Name = '" + fecherFoundation.Strings.Trim(LocationGrid.TextMatrix(intCounter, 1)) + "' where ID = " + LocationGrid.TextMatrix(intCounter, 0), modGNBas.DEFAULTCLERKDATABASE);
						LocationGrid.TextMatrix(intCounter, DISPOSITIONSTATUSCOL, FCConvert.ToString(0));
					}
				}
				LoadLocations();
				SaveLocations = true;
				MessageBox.Show("Save of Location information completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return SaveLocations;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			return SaveLocations;
		}
	}
}
