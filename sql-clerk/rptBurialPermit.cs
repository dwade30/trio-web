//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBurialPermit.
	/// </summary>
	public partial class rptBurialPermit : BaseSectionReport
	{
		public rptBurialPermit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Burial Permit";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBurialPermit InstancePtr
		{
			get
			{
				return (rptBurialPermit)Sys.GetInstance(typeof(rptBurialPermit));
			}
		}

		protected rptBurialPermit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBurialPermit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		private bool boolPrinted;
		private int intPrinted;
		//clsPrinterFunctions clsPrint = new clsPrinterFunctions();
		private bool boolPrintTest;
		private double dblLineAdjust;
		// vbPorter upgrade warning: lngBurialID As int	OnRead
		public void Init(bool boolTestPrint, double dblAdjust = 0, int lngBurialID = 0, bool boolJustPrint = false)
		{
			boolPrintTest = boolTestPrint;
			modGNBas.Statics.gintBurialID = lngBurialID;
			dblLineAdjust = dblAdjust;
			if (!boolJustPrint)
			{
                //FC:FINAL:IPI - #i1705 - show the report viewer on web
                //this.Show(FCForm.FormShowEnum.Modal);
                frmReportViewer.InstancePtr.Init(this, showModal: true);
            }
			else
			{
				this.PrintReport(true);
			}
		}

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// On Error Resume Next
			// Call clsPrint.PrinterForwardInInches(0.3)
			// Call clsPrint.CloseSession
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted = this.Document.Printer.DeviceCopies;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if ( modCashReceiptingData.Statics.bolFromWindowsCR)
			{
				if (boolPrinted)
				{
					modCashReceiptingData.Statics.typeCRData.Type = "BUR";
					modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(frmBurialPermit.InstancePtr.txtLastName.Text + ", " + frmBurialPermit.InstancePtr.txtFirstName.Text, 30);
					modCashReceiptingData.Statics.typeCRData.Reference = frmBurialPermit.InstancePtr.txtNameOfFuneralEstablishment.Text;
					modCashReceiptingData.Statics.typeCRData.Control1 = frmBurialPermit.InstancePtr.txtDateOfDeath.Text;
					modCashReceiptingData.Statics.typeCRData.Control2 = frmBurialPermit.InstancePtr.txtLicenseNumber.Text;
					modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(intPrinted * modDogs.Statics.typClerkFees.BurialPermit, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(intPrinted * modDogs.Statics.typClerkFees.BurialStateFee, "000000.00"));
					// .Amount2 = Format(0, "000000.00")
					modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
					modCashReceiptingData.Statics.typeCRData.PartyID = 0;
					modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
					Application.Exit();
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblLaserLineAdjustment6 = 0;
			// Dim rsData As New clsDRWrapper
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			if (!boolPrintTest)
			{
				//Application.DoEvents();
				// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
				// If rsData.EndOfFile Then
				// dblLaserLineAdjustment6 = 0
				// Else
				// dblLaserLineAdjustment6 = CDbl(Val(rsData.Fields("BurialAdjustment")))
				// End If
				dblLaserLineAdjustment6 = Conversion.Val(modRegistry.GetRegistryKey("BurialAdjustment"));
				rs.OpenRecordset("SELECT * FROM BurialPermitMaster where ID = " + FCConvert.ToString(modGNBas.Statics.gintBurialID), modGNBas.DEFAULTDATABASE);
			}
			else
			{
				dblLaserLineAdjustment6 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.PageHeader.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment6) / 1440F;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: myIndex As int	OnWriteFCConvert.ToInt32(
			int myIndex;
			string strTemp = "";
			string[] strAry = null;
			if (!boolPrintTest)
			{
				if (rs.EndOfFile())
				{
					MessageBox.Show("Burial Data has yet to be saved.", "TRIO Software Burial Permit", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
					return;
				}
				txtSex.Text = rs.Get_Fields_String("Sex");
				strTemp = FCConvert.ToString(rs.Get_Fields_String("typeofpermit"));
				txtBurial.Visible = false;
				txtTempStorage.Visible = false;
				txtBurialSea.Visible = false;
				txtDisinterment.Visible = false;
				txtCremation.Visible = false;
				txtMedicalScience.Visible = false;
				txtRemovalState.Visible = false;
				txtMausoleum.Visible = false;
				if (strTemp != string.Empty)
				{
					strAry = Strings.Split(strTemp, ";", -1, CompareConstants.vbTextCompare);
					for (myIndex = 0; myIndex <= (Information.UBound(strAry, 1)); myIndex++)
					{
						if (fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("typeofpermit"))) == "Burial")
						{
							txtBurial.Visible = true;
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("typeofpermit"))) == "Temporary Storage")
						{
							txtTempStorage.Visible = true;
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("typeofpermit"))) == "Disinterment")
						{
							txtDisinterment.Visible = true;
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("typeofpermit"))) == "Cremation")
						{
							txtCremation.Visible = true;
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("typeofpermit"))) == "Burial At Sea")
						{
							txtBurialSea.Visible = true;
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("typeofpermit"))) == "Medical Science")
						{
							txtMedicalScience.Visible = true;
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("typeofpermit"))) == "Removal From State")
						{
							txtRemovalState.Visible = true;
						}
						else if (fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("typeofpermit"))) == "Mausoleum")
						{
							txtMausoleum.Visible = true;
						}
					}
					// myIndex
				}
				else
				{
					txtBurial.Visible = false;
					txtTempStorage.Visible = false;
					txtBurialSea.Visible = false;
					txtDisinterment.Visible = false;
					txtCremation.Visible = false;
					txtMedicalScience.Visible = false;
					txtRemovalState.Visible = false;
					txtMausoleum.Visible = false;
				}
				// myIndex = 0
				// Select Case RS.Fields("AuthorizationForPermit")
				// Case "Completed Death Certificate"
				// myIndex = 0
				// 
				// Case "Report Of Death (Funeral Directors Only)"
				// myIndex = 1
				// 
				// Case "Medical Examiner 's Release For Cremation, Burial At Sea, Use By Medical Science, Removal From State"
				// myIndex = 2
				// 
				// Case "Application Or Court Order For Disinterment"
				// myIndex = 3
				// End Select
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AuthorizationReport")))
				{
					txtReportDeath.Visible = true;
				}
				else
				{
					txtReportDeath.Visible = false;
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AuthorizationApplication")))
				{
					txtCourtOrder.Visible = true;
				}
				else
				{
					txtCourtOrder.Visible = false;
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AuthorizationExaminer")))
				{
					txtReleaseCreamation.Visible = true;
				}
				else
				{
					txtReleaseCreamation.Visible = false;
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("AuthorizationCertificate")))
				{
					txtDeathCertificate.Visible = true;
				}
				else
				{
					txtDeathCertificate.Visible = false;
				}
				// txtDeathCertificate.Visible = myIndex = 0
				// txtReportDeath.Visible = myIndex = 1
				// txtReleaseCreamation.Visible = myIndex = 2
				// txtCourtOrder.Visible = myIndex = 3
				txtFullName.Text = rs.Get_Fields_String("FirstName") + " " + rs.Get_Fields_String("MiddleName") + " " + rs.Get_Fields_String("LastName");
				txtDateOfDeath.Text = rs.Get_Fields_String("DateOfDeath");
				txtAge.Text = rs.Get_Fields_String("Age");
				txtPlaceOfDeath.Text = rs.Get_Fields_String("PlaceOfDeathCity") + ", " + rs.Get_Fields_String("PlaceOfDeathState");
				txtLicenseNumber.Text = rs.Get_Fields_String("LicenseNumber");
				txtFacility.Text = rs.Get_Fields_String("NameOfFuneralEstablishment") + " " + fecherFoundation.Strings.Trim(rs.Get_Fields_String("BusinessAddress") + " " + rs.Get_Fields_String("BusinessAddress2") + " " + rs.Get_Fields_String("BusinessCity") + ", " + rs.Get_Fields_String("BusinessState") + " " + rs.Get_Fields_String("BusinessZip"));
				txtClerkTown.Text = rs.Get_Fields_String("ClerkTown");
				txtTX.Visible = frmBurialPermit.InstancePtr.chkTemporaryStorage.CheckState == Wisej.Web.CheckState.Checked;
				txtRX1.Visible = frmBurialPermit.InstancePtr.chkDisposition[2].CheckState == Wisej.Web.CheckState.Checked;
				txtRX2.Visible = frmBurialPermit.InstancePtr.chkDisposition[3].CheckState == Wisej.Web.CheckState.Checked;
				txtRX3.Visible = frmBurialPermit.InstancePtr.chkDisposition[6].CheckState == Wisej.Web.CheckState.Checked;
				txtRX4.Visible = frmBurialPermit.InstancePtr.chkDisposition[1].CheckState == Wisej.Web.CheckState.Checked;
				txtRX5.Visible = frmBurialPermit.InstancePtr.chkDisposition[4].CheckState == Wisej.Web.CheckState.Checked;
				txtRX6.Visible = frmBurialPermit.InstancePtr.chkDisposition[5].CheckState == Wisej.Web.CheckState.Checked;
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("inarmedforces")))
				{
					txtArmedForcesNo.Visible = false;
					txtArmedForcesYes.Visible = true;
				}
				else
				{
					txtArmedForcesNo.Visible = true;
					txtArmedForcesYes.Visible = false;
				}
				txtDateSigned.Text = rs.Get_Fields_String("Datesigned");
				if (frmBurialPermit.InstancePtr.chkTemporaryStorage.CheckState == Wisej.Web.CheckState.Checked)
				{
					txtTX.Text = "X";
					txtTDate.Text = rs.Get_Fields_String("tempDate");
					txtTLocation.Text = rs.Get_Fields_String("tempLOCATION");
					txtTNameVault.Text = rs.Get_Fields_String("tempCemetery");
				}
				// Else
				if (frmBurialPermit.InstancePtr.chkDisposition[2].CheckState == Wisej.Web.CheckState.Checked || frmBurialPermit.InstancePtr.chkDisposition[1].CheckState == Wisej.Web.CheckState.Checked || frmBurialPermit.InstancePtr.chkDisposition[3].CheckState == Wisej.Web.CheckState.Checked || frmBurialPermit.InstancePtr.chkDisposition[4].CheckState == Wisej.Web.CheckState.Checked || frmBurialPermit.InstancePtr.chkDisposition[5].CheckState == Wisej.Web.CheckState.Checked || frmBurialPermit.InstancePtr.chkDisposition[6].CheckState == Wisej.Web.CheckState.Checked)
				{
					txtRX1.Text = "X";
					txtRX2.Text = "X";
					txtRX3.Text = "X";
					txtRX4.Text = "X";
					txtRX5.Text = "X";
					txtRX6.Text = "X";
					txtRDate.Text = rs.Get_Fields_String("DispositionDate");
					txtRLocation.Text = rs.Get_Fields_String("LOCATION");
					txtRName.Text = rs.Get_Fields_String("NameOfCemetery");
				}
				// End If
				// txtCX1.Visible = False
				// txtCX2.Visible = False
				// txtCX3.Visible = False
				// If frmBurialPermit.optDisposition(0) Then
				// txtCX1.Text = "X"
				// txtCX2.Text = "X"
				// txtCX3.Text = "X"
				// txtCDate.Text = rs.Fields("DispositionDate")
				// txtCLocation.Text = rs.Fields("LOCATION")
				// txtCName.Text = rs.Fields("NameOfCemetery")
				// End If
				txtDX.Visible = frmBurialPermit.InstancePtr.chkDisposition[0].CheckState == Wisej.Web.CheckState.Checked;
				if (frmBurialPermit.InstancePtr.chkDisposition[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					txtDX.Text = "X";
					txtDDate.Text = rs.Get_Fields_String("DispositionDate");
					txtDLocation.Text = rs.Get_Fields_String("LOCATION");
					txtDName.Text = rs.Get_Fields_String("NameOfCemetery");
				}
				if (frmBurialPermit.InstancePtr.chkDisposition[3].CheckState == Wisej.Web.CheckState.Checked)
				{
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CremBuried")))
					{
						CremBuried.Text = "X";
					}
					else
					{
						CremBuried.Text = "";
					}
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CremScattered")))
					{
						CremScattered.Text = "X";
					}
					else
					{
						CremScattered.Text = "";
					}
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CremFam")))
					{
						CremFamily.Text = "X";
					}
					else
					{
						CremFamily.Text = "";
					}
					CremDate.Text = rs.Get_Fields_String("CremDate");
					CremLoc.Text = rs.Get_Fields_String("CremLoc");
					CremName.Text = rs.Get_Fields_String("CremName");
				}
				else
				{
					CremBuried.Text = "";
					CremScattered.Text = "";
					CremFamily.Text = "";
					CremDate.Text = "";
					CremLoc.Text = "";
					CremName.Text = "";
				}
			}
			else
			{
				txtFullName.Text = "Name";
				txtDateOfDeath.Text = "00/00/0000";
				txtAge.Text = "99";
				txtPlaceOfDeath.Text = "Place of Death";
				txtLicenseNumber.Text = "License Number";
				txtFacility.Text = "Facility";
				txtTLocation.Text = "Location";
				txtRLocation.Text = "Location";
				txtTNameVault.Text = "Cemetary";
				txtDLocation.Text = "Location";
				txtDateSigned.Text = "00/00/0000";
				txtClerkTown.Text = "Town";
			}
		}

		
	}
}
