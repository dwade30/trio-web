//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDCnoL.
	/// </summary>
	public partial class rptDCnoL : BaseSectionReport
	{
		public rptDCnoL()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Death Certificate ";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDCnoL InstancePtr
		{
			get
			{
				return (rptDCnoL)Sys.GetInstance(typeof(rptDCnoL));
			}
		}

		protected rptDCnoL _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData2.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDCnoL	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private clsDRWrapper rsData2 = new clsDRWrapper();
		private bool boolPrintTest;
		private double dblLineAdjust;
		private bool boolOlderStyle;
		private int lngWidth;
		private int lngHeight;

		public void Init(bool boolTestPrint, double dblAdjust = 0, bool boolOld = false)
		{
			boolPrintTest = boolTestPrint;
			dblLineAdjust = dblAdjust;
			boolOlderStyle = boolOld;
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted += this.Document.Printer.DeviceCopies;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblLaserLineAdjustment4 = 0;
			// Dim rsData                  As New clsDRWrapper
			int lngID;
			double dblRatio;
			int lngWidth;
			int lngHeight;
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			// If UCase(Me.Tag) = "OLD" Then
			if (boolOlderStyle)
			{
				txtDecedentStreet.Visible = false;
				txtDecedentCity.Visible = false;
			}
			else
			{
				txtDecedentStreet.Visible = true;
				txtDecedentCity.Visible = true;
			}
			//Application.DoEvents();
			if (!boolPrintTest)
			{
				// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
				// If rsData.EndOfFile Then
				// dblLaserLineAdjustment4 = 0
				// Else
				// dblLaserLineAdjustment4 = CDbl(Val(rsData.Fields("DeathAdjustment")))
				// End If
				dblLaserLineAdjustment4 = Conversion.Val(modRegistry.GetRegistryKey("DeathAdjustment", "CK"));
				rsData2.OpenRecordset("Select * from Amendments where Type = 'DEATHS' and AmendmentID = '" + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum) + "'", modGNBas.DEFAULTCLERKDATABASE);
				// If ClerkDefaults.boolUseSignatures Then
				// lngID = frmSignaturePassword.Init(CLERKSIG)
				// If lngID > 0 Then
				// Image1.Picture = LoadPicture(CurDir & "\" & GetSigFileFromID(lngID))
				// Image1.Visible = True
				// Image1.SizeMode = ddSMStretch
				// lngWidth = Image1.Picture.Width
				// lngHeight = Image1.Picture.Height
				// dblRatio = lngWidth / lngHeight
				// If dblRatio * Image1.Height > Image1.Width Then
				// keep width, change height
				// Image1.Height = Image1.Width / dblRatio
				// Else
				// keep height, change width
				// Image1.Width = Image1.Height * dblRatio
				// End If
				// Else
				// Set Image1.Picture = Nothing
				// Image1.Visible = False
				// End If
				// End If
			}
			else
			{
				dblLaserLineAdjustment4 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(200 * dblLaserLineAdjustment4) / 1440F;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			int lngNumCopies;
			double dblTemp;
			if (modCashReceiptingData.Statics.gboolFromDosTrio == true || modCashReceiptingData.Statics.bolFromWindowsCR)
			{
				if (boolPrinted)
				{
					// .Type = "DEA"
					// .Name = Left(frmDeaths.txtLastName.Text & ", " & frmDeaths.txtFirstName.Text, 30)
					// .Reference = frmDeaths.txtFileNumber.Text
					// If IsDate(frmDeaths.mebDateOfDeath.Text) Then
					// .Control1 = Format(frmDeaths.mebDateOfDeath.Text, "mm,dd,yyyy")
					// Else
					// .Control1 = frmDeaths.mebDateOfDeath.Text
					// End If
					// .Control2 = frmDeaths.txtLicenseeNumber.Text
					// Call frmInput.Init(lngNumCopies, "Copies", "Enter number of copies to charge for", 1440, , idtWholeNumber)
					// If frmDeaths.optMilitarySrv(0) Then
					// veterans fee
					// .Amount1 = Format((intPrinted * typClerkFees.AdditionalFee5), "000000.00")
					// Else
					// .Amount1 = Format((typClerkFees.NewDeath + ((intPrinted - 1) * typClerkFees.ReplacementDeath)), "000000.00")
					// End If
					// 
					// .Amount2 = Format(0, "000000.00")
					// .Amount3 = Format(0, "000000.00")
					// .Amount4 = Format(0, "000000.00")
					// .Amount5 = Format(0, "000000.00")
					// .Amount6 = Format(0, "000000.00")
					// .ProcessReceipt = "Y"
					modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
					modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
					// Call WriteCashReceiptingData(typeCRData)
					// End
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngID;
			double dblRatio;
			/*? On Error Resume Next  */
			//clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			frmPrintVitalRec.InstancePtr.Hide();
			if (!boolPrintTest)
			{
				frmDeaths.InstancePtr.Hide();
				Field1.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("LastName") + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("Designation");
				if (Information.IsDate(modGNBas.Statics.rsDeathCertificate.Get_Fields("DateOfDeath")))
				{
					Field2.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields("DateofDeath"), "MM/dd/yyyy");
				}
				else
				{
					Field2.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateOfDeathDescription");
				}
				Field3.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CityorTownofDeath");
				Field4.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields("DateofBirth");
				if (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("socialsecuritynumber")).Length <= 9)
				{
					Field5.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("SocialSecurityNumber"), "000-00-0000");
				}
				else
				{
					strTemp = FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("Socialsecuritynumber"));
					strTemp = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Mid(strTemp, 6);
					Field5.Text = strTemp;
				}
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("sex"))) == "M")
				{
					Field6.Text = "Male";
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("sex"))) == "F")
				{
					Field6.Text = "Female";
				}
				else
				{
					Field6.Text = string.Empty;
				}
				// Field6 = rsDeathCertificate.Fields("sex")
				Field7.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherFirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherMiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherMiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherLastName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherDesignation")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherDesignation"));
				Field8.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherFirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherMiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherMiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherLastName");
				Field9.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("InformantName");
				Field10.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("InformantMailingAddress");
				// state added this to form 1/29/2004
				txtDecedentStreet.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ResidenceStreet");
				txtDecedentCity.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("residencecityortown");
				// Call clsTemp.OpenRecordset("select * from defaulttowns where id = " & Val(rsDeathCertificate.Fields("residencecityortown")), "TWCK0000.vb1")
				// If Not clsTemp.EndOfFile Then
				// txtDecedentCity.Text = clsTemp.Fields("name")
				// Else
				// txtDecedentCity.Text = ""
				// End If
				if (frmPrintVitalRec.InstancePtr.chkPrint.CheckState == CheckState.Checked)
				{
					fCauseA.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseA");
					fCauseB.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseB");
					fCauseC.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseC");
					fCauseD.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseD");
				}
				else
				{
					fCauseA.Text = "XXXXXXXXXXXXXXXXXXXX";
					fCauseB.Text = "XXXXXXXXXXXXXXXXXXXX";
					fCauseC.Text = "XXXXXXXXXXXXXXXXXXXX";
					fCauseD.Text = "XXXXXXXXXXXXXXXXXXXX";
				}
				Field12.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("NameofPhysician");
				Field23.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("NameofAttendingPhysician");
				Field13.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateSigned"), "MM/dd/yyyy");
				Field14.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceofDisposition")));
				Field15.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceOfDispositionCity");
				Field16.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceOfDispositionState");
				Field17.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ClerkofRecordName");
				Field18.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ClerkofRecordCity");
				Field19.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateOriginalFiling");
				Field20.Text = Strings.Format(DateTime.Now, "MMMM dd, yyyy");
				if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
				{
					Field21.Text = "";
				}
				else
				{
					Field21.Text = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				}
				if (Conversion.Val(modGNBas.Statics.rsDeathCertificate.Get_Fields_Int32("towncode")) == 0)
				{
					Field22.Text = fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase);
				}
				else
				{
					Field22.Text = fecherFoundation.Strings.StrConv(modRegionalTown.GetTownKeyName_2(modGNBas.Statics.rsDeathCertificate.Get_Fields_Int32("towncode")), VbStrConv.ProperCase);
				}
				Field24.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FileNumber");
				txtAmended.Text = modAmendments.GetAmendments(rsData2);
			}
			else
			{
				Field1.Text = "Name";
				Field2.Text = "00/00/0000";
				Field3.Text = "city";
				Field4.Text = "00/00/0000";
				Field5.Text = "000-00-0000";
				Field6.Text = "Male";
				Field7.Text = "Father's Name";
				Field8.Text = "Mother's Name";
				Field9.Text = "Informant";
				Field10.Text = "Address";
				txtDecedentStreet.Text = "Street";
				txtDecedentCity.Text = "Residence";
				fCauseA.Text = "XXXXXXXXXXXXXXXXXXXX";
				fCauseB.Text = "XXXXXXXXXXXXXXXXXXXX";
				fCauseC.Text = "XXXXXXXXXXXXXXXXXXXX";
				fCauseD.Text = "XXXXXXXXXXXXXXXXXXXX";
				Field12.Text = "Physician";
				Field13.Text = "00/00/0000";
				Field14.Text = "Place";
				Field15.Text = "City";
				Field16.Text = "State";
				Field17.Text = "Clerk Name";
				Field18.Text = "City";
				Field19.Text = "00/00/0000";
				Field20.Text = Strings.Format(DateTime.Now, "MMMM dd,yyyy");
				Field21.Text = "Attested By";
				Field22.Text = modGlobalConstants.Statics.MuniName;
			}
		}

		
	}
}
