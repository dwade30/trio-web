﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmMarriageSearchA.
	/// </summary>
	partial class frmMarriageSearchA
	{
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdOwnerS;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtFName;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtDSearch;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdOwnerS_0;
		public fecherFoundation.FCButton cmdOwnerS_2;
		public fecherFoundation.FCButton cmdOwnerS_3;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtFName_0;
		public fecherFoundation.FCTextBox txtFName_1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkGroomBirthOnFile;
		public fecherFoundation.FCCheckBox chkGroomDeathOnFile;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkDeathOnFile;
		public fecherFoundation.FCCheckBox chkBirthOnFile;
		public fecherFoundation.FCTextBox txtDSearch_2;
		public fecherFoundation.FCTextBox txtDSearch_1;
		public fecherFoundation.FCTextBox txtDSearch_0;
		public FCGrid Grid;
		public fecherFoundation.FCLabel Label2_2;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label2_4;
		public fecherFoundation.FCLabel Label2_5;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdOwnerS_0 = new fecherFoundation.FCButton();
			this.cmdOwnerS_2 = new fecherFoundation.FCButton();
			this.cmdOwnerS_3 = new fecherFoundation.FCButton();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtFName_0 = new fecherFoundation.FCTextBox();
			this.txtFName_1 = new fecherFoundation.FCTextBox();
			this.Grid = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkGroomBirthOnFile = new fecherFoundation.FCCheckBox();
			this.chkGroomDeathOnFile = new fecherFoundation.FCCheckBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.chkDeathOnFile = new fecherFoundation.FCCheckBox();
			this.chkBirthOnFile = new fecherFoundation.FCCheckBox();
			this.txtDSearch_2 = new fecherFoundation.FCTextBox();
			this.txtDSearch_1 = new fecherFoundation.FCTextBox();
			this.txtDSearch_0 = new fecherFoundation.FCTextBox();
			this.Label2_2 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.Label2_4 = new fecherFoundation.FCLabel();
			this.Label2_5 = new fecherFoundation.FCLabel();
			this.cmdNew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwnerS_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwnerS_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwnerS_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkGroomBirthOnFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkGroomDeathOnFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDeathOnFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBirthOnFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOwnerS_2);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(959, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Size = new System.Drawing.Size(959, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Size = new System.Drawing.Size(959, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(293, 30);
			this.HeaderText.Text = "Search Marriage Records";
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "actionButton";
			this.cmdPrint.Enabled = false;
			this.cmdPrint.Location = new System.Drawing.Point(230, 517);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(77, 40);
			this.cmdPrint.TabIndex = 15;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdOwnerS_0
			// 
			this.cmdOwnerS_0.AppearanceKey = "actionButton";
			this.cmdOwnerS_0.DialogResult = Wisej.Web.DialogResult.OK;
			this.cmdOwnerS_0.Location = new System.Drawing.Point(124, 517);
			this.cmdOwnerS_0.Name = "cmdOwnerS_0";
			this.cmdOwnerS_0.Size = new System.Drawing.Size(96, 40);
			this.cmdOwnerS_0.TabIndex = 5;
			this.cmdOwnerS_0.Text = "Search";
			this.cmdOwnerS_0.Click += new System.EventHandler(this.cmdOwnerS_Click);
			// 
			// cmdOwnerS_2
			// 
			this.cmdOwnerS_2.AppearanceKey = "acceptButton";
			this.cmdOwnerS_2.Location = new System.Drawing.Point(406, 30);
			this.cmdOwnerS_2.Name = "cmdOwnerS_2";
			this.cmdOwnerS_2.Size = new System.Drawing.Size(146, 48);
			this.cmdOwnerS_2.TabIndex = 6;
			this.cmdOwnerS_2.Text = "Show Record";
			this.cmdOwnerS_2.Click += new System.EventHandler(this.cmdOwnerS_Click);
			// 
			// cmdOwnerS_3
			// 
			this.cmdOwnerS_3.AppearanceKey = "actionButton";
			this.cmdOwnerS_3.Location = new System.Drawing.Point(20, 517);
			this.cmdOwnerS_3.Name = "cmdOwnerS_3";
			this.cmdOwnerS_3.Size = new System.Drawing.Size(84, 40);
			this.cmdOwnerS_3.TabIndex = 8;
			this.cmdOwnerS_3.Text = "Clear";
			this.cmdOwnerS_3.Click += new System.EventHandler(this.cmdOwnerS_Click);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.cmdPrint);
			this.Frame3.Controls.Add(this.cmdOwnerS_0);
			this.Frame3.Controls.Add(this.txtFName_0);
			this.Frame3.Controls.Add(this.cmdOwnerS_3);
			this.Frame3.Controls.Add(this.txtFName_1);
			this.Frame3.Controls.Add(this.Grid);
			this.Frame3.Controls.Add(this.Frame1);
			this.Frame3.Controls.Add(this.Frame2);
			this.Frame3.Controls.Add(this.txtDSearch_2);
			this.Frame3.Controls.Add(this.txtDSearch_1);
			this.Frame3.Controls.Add(this.txtDSearch_0);
			this.Frame3.Controls.Add(this.Label2_2);
			this.Frame3.Controls.Add(this.Label2_1);
			this.Frame3.Controls.Add(this.Label2_0);
			this.Frame3.Controls.Add(this.Label2_4);
			this.Frame3.Controls.Add(this.Label2_5);
			this.Frame3.Location = new System.Drawing.Point(30, 30);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(880, 581);
			this.Frame3.TabIndex = 9;
			this.Frame3.Text = "Information";
			// 
			// txtFName_0
			// 
			this.txtFName_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtFName_0.Location = new System.Drawing.Point(510, 68);
			this.txtFName_0.Name = "txtFName_0";
			this.txtFName_0.Size = new System.Drawing.Size(192, 40);
			this.txtFName_0.TabIndex = 1;
			// 
			// txtFName_1
			// 
			this.txtFName_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtFName_1.Location = new System.Drawing.Point(510, 128);
			this.txtFName_1.Name = "txtFName_1";
			this.txtFName_1.Size = new System.Drawing.Size(192, 40);
			this.txtFName_1.TabIndex = 3;
			// 
			// Grid
			// 
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Grid.Cols = 5;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.Location = new System.Drawing.Point(20, 345);
			this.Grid.Name = "Grid";
			this.Grid.RowHeadersVisible = false;
			this.Grid.Rows = 1;
			this.Grid.Size = new System.Drawing.Size(840, 153);
			this.Grid.TabIndex = 22;
			this.Grid.Click += new System.EventHandler(this.Grid_ClickEvent);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkGroomBirthOnFile);
			this.Frame1.Controls.Add(this.chkGroomDeathOnFile);
			this.Frame1.Location = new System.Drawing.Point(455, 248);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(405, 77);
			this.Frame1.TabIndex = 19;
			this.Frame1.Text = "Party B";
			// 
			// chkGroomBirthOnFile
			// 
			this.chkGroomBirthOnFile.Location = new System.Drawing.Point(225, 30);
			this.chkGroomBirthOnFile.Name = "chkGroomBirthOnFile";
			this.chkGroomBirthOnFile.Size = new System.Drawing.Size(134, 23);
			this.chkGroomBirthOnFile.TabIndex = 21;
			this.chkGroomBirthOnFile.Text = "Birth record on file";
			this.chkGroomBirthOnFile.CheckedChanged += new System.EventHandler(this.chkGroomBirthOnFile_CheckedChanged);
			// 
			// chkGroomDeathOnFile
			// 
			this.chkGroomDeathOnFile.Location = new System.Drawing.Point(20, 30);
			this.chkGroomDeathOnFile.Name = "chkGroomDeathOnFile";
			this.chkGroomDeathOnFile.Size = new System.Drawing.Size(143, 23);
			this.chkGroomDeathOnFile.TabIndex = 20;
			this.chkGroomDeathOnFile.Text = "Death record on file";
			this.chkGroomDeathOnFile.CheckedChanged += new System.EventHandler(this.chkGroomDeathOnFile_CheckedChanged);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.chkDeathOnFile);
			this.Frame2.Controls.Add(this.chkBirthOnFile);
			this.Frame2.Location = new System.Drawing.Point(20, 248);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(405, 77);
			this.Frame2.TabIndex = 16;
			this.Frame2.Text = "Party A";
			// 
			// chkDeathOnFile
			// 
			this.chkDeathOnFile.Location = new System.Drawing.Point(20, 30);
			this.chkDeathOnFile.Name = "chkDeathOnFile";
			this.chkDeathOnFile.Size = new System.Drawing.Size(143, 23);
			this.chkDeathOnFile.TabIndex = 18;
			this.chkDeathOnFile.Text = "Death record on file";
			this.chkDeathOnFile.CheckedChanged += new System.EventHandler(this.chkDeathOnFile_CheckedChanged);
			// 
			// chkBirthOnFile
			// 
			this.chkBirthOnFile.Location = new System.Drawing.Point(225, 30);
			this.chkBirthOnFile.Name = "chkBirthOnFile";
			this.chkBirthOnFile.Size = new System.Drawing.Size(134, 23);
			this.chkBirthOnFile.TabIndex = 17;
			this.chkBirthOnFile.Text = "Birth record on file";
			this.chkBirthOnFile.CheckedChanged += new System.EventHandler(this.chkBirthOnFile_CheckedChanged);
			// 
			// txtDSearch_2
			// 
			this.txtDSearch_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtDSearch_2.Location = new System.Drawing.Point(288, 188);
			this.txtDSearch_2.Name = "txtDSearch_2";
			this.txtDSearch_2.Size = new System.Drawing.Size(192, 40);
			this.txtDSearch_2.TabIndex = 4;
			this.txtDSearch_2.Enter += new System.EventHandler(this.txtDSearch_Enter);
			// 
			// txtDSearch_1
			// 
			this.txtDSearch_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtDSearch_1.Location = new System.Drawing.Point(288, 128);
			this.txtDSearch_1.Name = "txtDSearch_1";
			this.txtDSearch_1.Size = new System.Drawing.Size(192, 40);
			this.txtDSearch_1.TabIndex = 2;
			this.txtDSearch_1.Enter += new System.EventHandler(this.txtDSearch_Enter);
			// 
			// txtDSearch_0
			// 
			this.txtDSearch_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtDSearch_0.Location = new System.Drawing.Point(288, 68);
			this.txtDSearch_0.Name = "txtDSearch_0";
			this.txtDSearch_0.Size = new System.Drawing.Size(192, 40);
			this.txtDSearch_0.TabIndex = 23;
			this.txtDSearch_0.Enter += new System.EventHandler(this.txtDSearch_Enter);
			// 
			// Label2_2
			// 
			this.Label2_2.Location = new System.Drawing.Point(510, 30);
			this.Label2_2.Name = "Label2_2";
			this.Label2_2.Size = new System.Drawing.Size(167, 15);
			this.Label2_2.TabIndex = 14;
			this.Label2_2.Text = "FIRST NAME (OPTIONAL)";
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(288, 30);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(84, 15);
			this.Label2_1.TabIndex = 13;
			this.Label2_1.Text = "LAST NAME";
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(20, 142);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(126, 15);
			this.Label2_0.TabIndex = 12;
			this.Label2_0.Text = "PARTY B\'S NAME";
			// 
			// Label2_4
			// 
			this.Label2_4.Location = new System.Drawing.Point(20, 82);
			this.Label2_4.Name = "Label2_4";
			this.Label2_4.Size = new System.Drawing.Size(126, 15);
			this.Label2_4.TabIndex = 11;
			this.Label2_4.Text = "PARTY A\'S NAME";
			// 
			// Label2_5
			// 
			this.Label2_5.Location = new System.Drawing.Point(20, 202);
			this.Label2_5.Name = "Label2_5";
			this.Label2_5.Size = new System.Drawing.Size(212, 15);
			this.Label2_5.TabIndex = 10;
			this.Label2_5.Text = "PLACE OF MARRIAGE (CITY / TOWN)";
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = Wisej.Web.AnchorStyles.Top;
			this.cmdNew.Location = new System.Drawing.Point(890, 31);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(47, 24);
			this.cmdNew.TabIndex = 6;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// frmMarriageSearchA
			// 
			this.AcceptButton = this.cmdOwnerS_0;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(959, 688);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmMarriageSearchA";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Search Marriage Records";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmMarriageSearchA_Load);
			this.Activated += new System.EventHandler(this.frmMarriageSearchA_Activated);
			this.Resize += new System.EventHandler(this.frmMarriageSearchA_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMarriageSearchA_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMarriageSearchA_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwnerS_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwnerS_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwnerS_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkGroomBirthOnFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkGroomDeathOnFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDeathOnFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBirthOnFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdNew;
	}
}
