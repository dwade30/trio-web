﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmGetDateRange : BaseForm
	{
		public frmGetDateRange()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmGetDateRange InstancePtr
		{
			get
			{
				return (frmGetDateRange)Sys.GetInstance(typeof(frmGetDateRange));
			}
		}

		protected frmGetDateRange _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private string strReturn = string.Empty;

		public string Init(string strRange = "")
		{
			string Init = "";
			string[] strTemp = null;
			if (strRange != string.Empty)
			{
				strTemp = Strings.Split(strRange, ";", -1, CompareConstants.vbTextCompare);
				t2kDate1.Text = strTemp[0];
				T2KDate2.Text = strTemp[1];
			}
			strReturn = "";
			Init = "";
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private void frmGetDateRange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetDateRange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetDateRange properties;
			//frmGetDateRange.FillStyle	= 0;
			//frmGetDateRange.ScaleWidth	= 3885;
			//frmGetDateRange.ScaleHeight	= 2175;
			//frmGetDateRange.LinkTopic	= "Form2";
			//frmGetDateRange.LockControls	= -1  'True;
			//frmGetDateRange.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (Information.IsDate(t2kDate1.Text) && Information.IsDate(T2KDate2.Text))
			{
				if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(t2kDate1.Text), FCConvert.ToDateTime(T2KDate2.Text)) < 0)
				{
					MessageBox.Show("You must specify a valid date range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				MessageBox.Show("You must specify a valid date range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strReturn = t2kDate1.Text + ";" + T2KDate2.Text;
			Close();
		}
	}
}
