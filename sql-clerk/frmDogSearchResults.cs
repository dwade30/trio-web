//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDogSearchResults : BaseForm
	{
		public frmDogSearchResults()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null && System.Windows.Forms.Application.OpenForms.Count == 0)
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDogSearchResults InstancePtr
		{
			get
			{
				return (frmDogSearchResults)Sys.GetInstance(typeof(frmDogSearchResults));
			}
		}

		protected frmDogSearchResults _InstancePtr = null;
		//=========================================================
		bool boolByOwner;
		// sorted by owner info not dog info.  Affects what shows in the results
		const int COLOWNERNUM = 0;
		const int COLLASTNAME = 1;
		const int COLFIRSTNAME = 2;
		const int COLLOCNUM = 3;
		const int COLLOCSTREET = 4;
		const int COLKENNELNUM = 5;
		const int COLDOGNAME = 6;
		const int COLDOGBREED = 7;
		const int COLDOGCOLOR = 8;
		const int COLOWNERDELETED = 9;
		int intSearchType;

		public void Init(ref int intTypeofRecords, ref bool boolDeletedDogs, ref int intSearchBy, ref int intContain, ref string strSearch)
		{
			// accepts input from the search screen and then performs the search
			string strSQL = "";
			string strWhere;
			string strOrderBy = "";
			string[] strArySearch = null;
			string strTemp = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strWhere = "";
				strArySearch = Strings.Split(strSearch, ",", -1, CompareConstants.vbTextCompare);
				intSearchType = intSearchBy;
				if (intSearchBy == 4)
				{
					// kennel num
					strSQL = "select * from dogowner inner join (kennellicense inner join doginventory on (doginventory.ID = kennellicense.licenseid)) on (dogowner.ID = kennellicense.ownernum) CROSS APPLY " + rsTemp.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p ";
					if (boolDeletedDogs)
					{
					}
					else
					{
						strWhere = " and  (isnull(dogowner.deleted,0) <> 1) ";
					}
					strWhere += " and doginventory.type = 'K' ";
					boolByOwner = true;
					strOrderBy = " order by FullName,dogowner.ID";
				}
				else if (intSearchBy == 10)
				{
					if (boolDeletedDogs)
					{
						// strSQL = "Select * from dogowner inner join (doginfo inner join defaultbreeds on (defaultbreeds.id = val(doginfo.dogbreed & ''))) on (doginfo.ownernum = dogowner.ID) or (doginfo.previousowner = dogowner.ID) "
						// strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner inner join doginfo  on (doginfo.ownernum = dogowner.ID) or (doginfo.previousowner = dogowner.ID) CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p "
						strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID) inner join " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView on (dogowner.partyid = " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView.Id)  ";
					}
					else
					{
						// strSQL = "Select * from dogowner inner join (doginfo inner join DEFAULTBREEDS on (defaultbreeds.id = val(doginfo.dogbreed & ''))) on (doginfo.ownernum = dogowner.ID) "
						// strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p "
						strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID) inner join " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView on (dogowner.partyid = " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView.Id)  ";
						strWhere = " and (isnull(dogowner.deleted,0) <> 1) ";
					}
					if (intTypeofRecords == 0)
					{
						strWhere += " and (isnull(kenneldog,0) <> 1) ";
					}
					else if (intTypeofRecords == 1)
					{
						strWhere += " and isnull(kenneldog,0) = 1 ";
					}
					boolByOwner = false;
				}
				else if (intSearchBy > 4)
				{
					if (boolDeletedDogs)
					{
						// strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner inner join doginfo  on (doginfo.ownernum = dogowner.ID) or (doginfo.previousowner = dogowner.ID) CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p "
						strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID or isnull(doginfo.previousowner,0) = dogowner.id) inner join " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView on (dogowner.partyid = " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView.Id) ";
					}
					else
					{
						// strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p "
						strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID) inner join " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView on (dogowner.partyid = " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView.Id) ";
						strWhere = " and (isnull(dogowner.deleted,0) <> 1) ";
					}
					if (intTypeofRecords == 0)
					{
						strWhere += " and (isnull(kenneldog,0) <> 1) ";
					}
					else if (intTypeofRecords == 1)
					{
						strWhere += " and isnull(kenneldog,0) = 1 ";
					}
					boolByOwner = false;
				}
				else
				{
					// strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner left join kennellicense on (kennellicense.ownernum = dogowner.ID) CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p "
					strSQL = "Select DogOwner.ID as DogOwnerID, * from dogowner left join kennellicense on (kennellicense.ownernum = dogowner.ID) inner join " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView on (dogowner.partyid = " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView.Id)  ";
					if (boolDeletedDogs)
					{
					}
					else
					{
						strWhere = " and (isnull(dogowner.deleted,0) <> 1) ";
					}
					if (intTypeofRecords == 0)
					{
						strWhere += " and convert(int, isnull(licenseid, 0)) = 0 ";
					}
					else if (intTypeofRecords == 1)
					{
						strWhere += " and convert(int, isnull(licenseid, 0)) > 0 ";
					}
					boolByOwner = true;
					strOrderBy = " order by FullNameLF,dogowner.ID ";
				}
				switch (intContain)
				{
					case 0:
						{
							// contains
							switch (intSearchBy)
							{
								case 0:
									{
										strTemp = "";
										if (fecherFoundation.Strings.Trim(strArySearch[0]) != string.Empty)
										{
											strTemp = "Where lastname like '%" + strArySearch[0] + "%' ";
										}
										if (fecherFoundation.Strings.Trim(strArySearch[1]) != string.Empty)
										{
											if (strTemp != string.Empty)
											{
												strTemp += " and ";
											}
											else
											{
												strTemp = "Where ";
											}
											strTemp += " firstname like '%" + strArySearch[1] + "%' ";
										}
										strWhere = strTemp + strWhere;
										break;
									}
								case 1:
									{
										strWhere = "where city like '%" + strArySearch[0] + "%'" + strWhere;
										break;
									}
								case 2:
									{
										strTemp = "";
										if (Conversion.Val(strArySearch[0]) > 0)
										{
											strTemp = "where convert(int, isnull(locationnum, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0]));
										}
										if (fecherFoundation.Strings.Trim(strArySearch[1]) != string.Empty)
										{
											if (strTemp != string.Empty)
											{
												strTemp += " and ";
											}
											else
											{
												strTemp = "Where ";
											}
											strTemp += " locationstr like '%" + strArySearch[1] + "%' ";
										}
										strWhere = strTemp + strWhere;
										break;
									}
								case 3:
									{
										strTemp = strArySearch[0];
										strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
										strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
										strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
										// should have no characters other than digits now
										if (strTemp.Length == 7)
										{
											strWhere = "where phone = '207" + strTemp + "' or phone = '" + Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4) + "'" + strWhere;
										}
										else
										{
											strWhere = "where phone = '" + strTemp + "'" + strWhere;
										}
										break;
									}
								case 4:
									{
										strWhere = "where convert(int, isnull(doginventory.stickernumber, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0])) + strWhere;
										break;
									}
								case 5:
									{
										strWhere = "where dogname like '%" + strArySearch[0] + "%'" + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 6:
									{
										strWhere = "where dogvet like '%" + strArySearch[0] + "%'" + strWhere;
										strOrderBy = " order by dogvet,dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 7:
									{
										strWhere = "where rabiestagno = '" + strArySearch[0] + "' " + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 8:
									{
										strWhere = "where isnull(taglicnum, '') = '" + strArySearch[0] + "' " +strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 9:
									{
										strWhere = "where convert(int, isnull(stickerlicnum, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0])) + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 10:
									{
										strWhere = " where dogbreed like '%" + strArySearch[0] + "%' " + strWhere;
										strOrderBy = " order by dogbreed,dogname,FullNameLF,dogowner.ID ";
										break;
									}
							}
							//end switch
							break;
						}
					case 1:
						{
							// starts
							switch (intSearchBy)
							{
								case 0:
									{
										strTemp = "";
										if (fecherFoundation.Strings.Trim(strArySearch[0]) != string.Empty)
										{
											strTemp = "Where lastname between '" + strArySearch[0] + "' and '" + strArySearch[0] + "zzz' ";
										}
										if (fecherFoundation.Strings.Trim(strArySearch[1]) != string.Empty)
										{
											if (strTemp != string.Empty)
											{
												strTemp += " and ";
											}
											else
											{
												strTemp = "Where ";
											}
											strTemp += " firstname between '" + strArySearch[1] + "' and '" + strArySearch[1] + "zzz' ";
										}
										strWhere = strTemp + strWhere;
										break;
									}
								case 1:
									{
										strWhere = "where city between '" + strArySearch[0] + "' and '" + strArySearch[0] + "zzz' " + strWhere;
										break;
									}
								case 2:
									{
										strTemp = "";
										if (Conversion.Val(strArySearch[0]) > 0)
										{
											strTemp = "where convert(int, isnull(locationnum, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0]));
										}
										if (fecherFoundation.Strings.Trim(strArySearch[1]) != string.Empty)
										{
											if (strTemp != string.Empty)
											{
												strTemp += " and ";
											}
											else
											{
												strTemp = "Where ";
											}
											strTemp += " locationstr between '" + strArySearch[1] + "' and '" + strArySearch[1] + "zzz' ";
										}
										if (strTemp == string.Empty)
										{
											strTemp = "where convert(int, isnull(locationnum, 0)) = 0 and locationstr = '' ";
										}
										strWhere = strTemp + strWhere;
										break;
									}
								case 3:
									{
										strTemp = strArySearch[0];
										strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
										strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
										strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
										// should have no characters other than digits now
										if (strTemp.Length == 7)
										{
											strWhere = "where phone = '207" + strTemp + "' or phone = '" + Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4) + "' " + strWhere;
										}
										else
										{
											strWhere = "where phone = '" + strTemp + "'" + strWhere;
										}
										break;
									}
								case 4:
									{
										strWhere = "where convert(int, isnull(doginventory.stickernumber, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0])) + strWhere;
										break;
									}
								case 5:
									{
										strWhere = "where dogname between '" + strArySearch[0] + "' and '" + strArySearch[0] + "zzz' " + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 6:
									{
										strWhere = "where dogvet between '" + strArySearch[0] + "' and '" + strArySearch[0] + "zzz' " + strWhere;
										strOrderBy = " order by dogvet,dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 7:
									{
										strWhere = "where rabiestagno = '" + strArySearch[0] + "'" + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 8:
									{
										strWhere = "where  isnull(taglicnum, '') = '" + strArySearch[0] + "' " + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 9:
									{
										strWhere = "where convert(int, isnull(stickerlicnum, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0])) + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 10:
									{
										strWhere = " where dogbreed between '" + strArySearch[0] + "' and '" + strArySearch[0] + "zzz' " + strWhere;
										strOrderBy = " order by dogbreed,dogname,FullNameLF,dogowner.ID ";
										break;
									}
							}
							//end switch
							break;
						}
					case 2:
						{
							// ends
							switch (intSearchBy)
							{
								case 0:
									{
										strTemp = "";
										if (fecherFoundation.Strings.Trim(strArySearch[0]) != string.Empty)
										{
											strTemp = "Where lastname like '%" + strArySearch[0] + "' ";
										}
										if (fecherFoundation.Strings.Trim(strArySearch[1]) != string.Empty)
										{
											if (strTemp != string.Empty)
											{
												strTemp += " and ";
											}
											else
											{
												strTemp = "Where ";
											}
											strTemp += " firstname like '%" + strArySearch[1] + "' ";
										}
										strWhere = strTemp + strWhere;
										break;
									}
								case 1:
									{
										strWhere = "where city like '%" + strArySearch[0] + "'" + strWhere;
										break;
									}
								case 2:
									{
										strTemp = "";
										if (Conversion.Val(strArySearch[0]) > 0)
										{
											strTemp = "where convert(int, isnull(locationnum, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0]));
										}
										if (fecherFoundation.Strings.Trim(strArySearch[1]) != string.Empty)
										{
											if (strTemp != string.Empty)
											{
												strTemp += " and ";
											}
											else
											{
												strTemp = "Where ";
											}
											strTemp += " locationstr like '%" + strArySearch[1] + "' ";
										}
										strWhere = strTemp + strWhere;
										break;
									}
								case 3:
									{
										strTemp = strArySearch[0];
										strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
										strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
										strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
										// should have no characters other than digits now
										if (strTemp.Length == 7)
										{
											strWhere = "where phone = '207" + strTemp + "' or phone = '" + Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4) + "' " + strWhere;
										}
										else
										{
											strWhere = "where phone = '" + strTemp + "'" + strWhere;
										}
										break;
									}
								case 4:
									{
										strWhere = "where convert(int, isnull(doginventory.stickernumber, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0])) + strWhere;
										break;
									}
								case 5:
									{
										strWhere = "where dogname like '%" + strArySearch[0] + "'" + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 6:
									{
										strWhere = "where dogvet like '%" + strArySearch[0] + "'" + strWhere;
										strOrderBy = " order by dogvet,dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 7:
									{
										strWhere = "where rabiestagno = '" + strArySearch[0] + "' " + strWhere;
										strOrderBy = " order by dogname,FullNameLFdogowner.ID ";
										break;
									}
								case 8:
									{
										strWhere = "where isnull(taglicnum, '') = '" + strArySearch[0] + "' " + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 9:
									{
										strWhere = "where convert(int, isnull(stickerlicnum, 0)) = " + FCConvert.ToString(Conversion.Val(strArySearch[0])) + strWhere;
										strOrderBy = " order by dogname,FullNameLF,dogowner.ID ";
										break;
									}
								case 10:
									{
										strWhere = " where dogbreed like '%" + strArySearch[0] + "' " + strWhere;
										strOrderBy = " order by dogbreed,dogname,FullNameLF,dogowner.ID ";
										break;
									}
							}
							//end switch
							break;
						}
				}
				//end switch
				strSQL += strWhere + strOrderBy;
				SetupGridSearch(ref intSearchBy);
				if (FillGridSearch(ref strSQL, ref intSearchBy))
				{
					ResizeGridSearch();
					this.Show(App.MainForm);
					//App.DoEvents();
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool FillGridSearch(ref string strSQL, ref int intType)
		{
			bool FillGridSearch = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow = 0;
			int lngONum;
			string strDogList = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FillGridSearch = false;
				GridSearch.Rows = 1;
				clsLoad.OpenRecordset(strSQL, "twck0000.vb1");
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					return FillGridSearch;
				}
				lngONum = 0;
				while (!clsLoad.EndOfFile())
				{
					// let's not show the owner more than once if we are showing by owner
					if ((lngONum != Conversion.Val(clsLoad.Get_Fields("DogOwnerID"))) || (intType >= 4))
					{
						lngONum = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("DogOwnerID"))));
						GridSearch.Rows += 1;
						lngRow = GridSearch.Rows - 1;
						GridSearch.TextMatrix(lngRow, COLOWNERNUM, FCConvert.ToString(clsLoad.Get_Fields("DogOwnerID")));
						GridSearch.TextMatrix(lngRow, COLLASTNAME, FCConvert.ToString(clsLoad.Get_Fields_String("lastname")));
						GridSearch.TextMatrix(lngRow, COLFIRSTNAME, FCConvert.ToString(clsLoad.Get_Fields_String("firstname")));
						if (Convert.ToBoolean(clsLoad.Get_Fields_Boolean("deleted")))
						{
							GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
							GridSearch.TextMatrix(lngRow, COLOWNERDELETED, FCConvert.ToString(true));
						}
						else
						{
							GridSearch.TextMatrix(lngRow, COLOWNERDELETED, FCConvert.ToString(false));
						}
						if (Conversion.Val(clsLoad.Get_Fields_String("locationnum")) > 0)
						{
							GridSearch.TextMatrix(lngRow, COLLOCNUM, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("locationnum"))));
						}
						else
						{
							GridSearch.TextMatrix(lngRow, COLLOCNUM, "");
						}
						GridSearch.TextMatrix(lngRow, COLLOCSTREET, FCConvert.ToString(clsLoad.Get_Fields_String("locationstr")));
						if (intType > 4)
						{
							GridSearch.TextMatrix(lngRow, COLDOGNAME, FCConvert.ToString(clsLoad.Get_Fields_String("dogname")));
							GridSearch.TextMatrix(lngRow, COLDOGBREED, FCConvert.ToString(clsLoad.Get_Fields_String("dogbreed")));
							GridSearch.TextMatrix(lngRow, COLDOGCOLOR, FCConvert.ToString(clsLoad.Get_Fields_String("dogcolor")));
							if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ownernum")) == -1)
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
							}
						}
						else if (intType == 4)
						{
							GridSearch.TextMatrix(lngRow, COLKENNELNUM, FCConvert.ToString(clsLoad.Get_Fields_Int32("stickernumber")));
						}
						if (intType <= 4 && fecherFoundation.Strings.Trim(FCConvert.ToString(GridSearch.RowData(lngRow))) == string.Empty)
						{
							clsTemp.OpenRecordset("select dogname from doginfo where ownernum = " + FCConvert.ToString(lngONum) + " order by dogname", "twck0000.vb1");
							GridSearch.RowData(lngRow, "Dog(s): ");
							while (!clsTemp.EndOfFile())
							{
								GridSearch.RowData(lngRow, GridSearch.RowData(lngRow) + clsTemp.Get_Fields_String("dogname") + ",");
								clsTemp.MoveNext();
							}
							if (FCConvert.ToString(GridSearch.RowData(lngRow)).Length > 0)
							{
								GridSearch.RowData(lngRow, Strings.Mid(FCConvert.ToString(GridSearch.RowData(lngRow)), 1, FCConvert.ToString(GridSearch.RowData(lngRow)).Length - 1));
							}
						}
					}
					clsLoad.MoveNext();
				}
				FillGridSearch = true;
				return FillGridSearch;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridSearch", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillGridSearch;
		}

		private void SetupGridSearch(ref int intType)
		{
			GridSearch.Cols = 10;
			GridSearch.TextMatrix(0, COLLASTNAME, "Last Name");
			GridSearch.TextMatrix(0, COLFIRSTNAME, "First Name");
			GridSearch.TextMatrix(0, COLLOCNUM, "ST #");
			GridSearch.TextMatrix(0, COLLOCSTREET, "Street Name");
			GridSearch.TextMatrix(0, COLKENNELNUM, "Kennel #");
			GridSearch.TextMatrix(0, COLDOGBREED, "Breed");
			GridSearch.TextMatrix(0, COLDOGCOLOR, "Color");
			GridSearch.TextMatrix(0, COLDOGNAME, "Dog Name");
			GridSearch.ColHidden(COLOWNERDELETED, true);
			if (intType < 5)
			{
				GridSearch.ColHidden(COLDOGBREED, true);
				GridSearch.ColHidden(COLDOGNAME, true);
				GridSearch.ColHidden(COLDOGCOLOR, true);
				GridSearch.ColHidden(COLOWNERNUM, true);
				GridSearch.ColHidden(COLLOCNUM, false);
				GridSearch.ColHidden(COLLOCSTREET, false);
			}
			else
			{
				GridSearch.ColHidden(COLLOCNUM, true);
				GridSearch.ColHidden(COLLOCSTREET, true);
				GridSearch.ColHidden(COLDOGBREED, false);
				GridSearch.ColHidden(COLDOGCOLOR, false);
				GridSearch.ColHidden(COLDOGNAME, false);
				GridSearch.ColHidden(COLOWNERNUM, true);
			}
			if (intType == 4)
			{
				GridSearch.ColHidden(COLKENNELNUM, false);
			}
			else
			{
				GridSearch.ColHidden(COLKENNELNUM, true);
			}
		}

		private void ResizeGridSearch()
		{
			// vbPorter upgrade warning: GridWidth As object	OnWriteFCConvert.ToInt32(
			int GridWidth = GridSearch.WidthOriginal;
			GridSearch.ColWidth(COLLASTNAME, FCConvert.ToInt32(0.2 * GridWidth));
			GridSearch.ColWidth(COLFIRSTNAME, FCConvert.ToInt32(0.2 * GridWidth));
			if (intSearchType < 4)
			{
				GridSearch.ColWidth(COLLOCNUM, FCConvert.ToInt32(0.1 * GridWidth));
				// .ColWidth(COLLOCSTREET) = 0.25 * GridWidth
				GridSearch.ColWidth(COLLASTNAME, FCConvert.ToInt32(0.3 * GridWidth));
				GridSearch.ColWidth(COLFIRSTNAME, FCConvert.ToInt32(0.24 * GridWidth));
			}
			else if (intSearchType == 4)
			{
				GridSearch.ColWidth(COLLOCNUM, FCConvert.ToInt32(0.1 * GridWidth));
				GridSearch.ColWidth(COLLOCSTREET, FCConvert.ToInt32(0.27 * GridWidth));
				GridSearch.ColWidth(COLLASTNAME, FCConvert.ToInt32(0.27 * GridWidth));
				GridSearch.ColWidth(COLFIRSTNAME, FCConvert.ToInt32(0.25 * GridWidth));
				// .ColWidth(COLKENNELNUM) = 0.2 * GridWidth
			}
			else
			{
				GridSearch.ColWidth(COLDOGNAME, FCConvert.ToInt32(0.16 * GridWidth));
				GridSearch.ColWidth(COLDOGBREED, FCConvert.ToInt32(0.24 * GridWidth));
			}
		}

		private void frmDogSearchResults_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDogSearchResults_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDogSearchResults properties;
			//frmDogSearchResults.ScaleWidth	= 9045;
			//frmDogSearchResults.ScaleHeight	= 8280;
			//frmDogSearchResults.LinkTopic	= "Form1";
			//frmDogSearchResults.LockControls	= -1  'True;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void frmDogSearchResults_Resize(object sender, System.EventArgs e)
		{
			ResizeGridSearch();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// If bolFromWindowsCR Then
			// MDIParent.SetFocus
			// End If
		}

		private void GridSearch_DblClick(object sender, System.EventArgs e)
		{
			if (GridSearch.MouseRow > 0)
			{
				mnuContinue_Click();
			}
		}

		private void GridSearch_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = 0;
						if (GridSearch.Row > 0)
						{
							mnuContinue_Click();
						}
						break;
					}
			}
			//end switch
		}

		private void GridSearch_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = GridSearch.MouseRow;
			lngMC = GridSearch.MouseCol;
			ToolTip1.SetToolTip(GridSearch, "");
			if (lngMR > 0)
			{
				ToolTip1.SetToolTip(GridSearch, FCConvert.ToString(GridSearch.RowData(lngMR)));
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			if (GridSearch.Rows < 2)
			{
				MessageBox.Show("There are no records to open", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (GridSearch.Row < 1)
			{
				MessageBox.Show("You must select a record first", "No Record Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Convert.ToBoolean(GridSearch.TextMatrix(GridSearch.Row, COLOWNERDELETED)) == true)
			{
				if (MessageBox.Show("Owner record is deleted. Do you want to undelete it?", "Undelete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsSave.Execute("update dogowner set deleted = 0 where id = " + GridSearch.TextMatrix(GridSearch.Row, COLOWNERNUM), "twck0000.vb1");
					GridSearch.TextMatrix(GridSearch.Row, COLOWNERDELETED, FCConvert.ToString(false));
					GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridSearch.Row, 0, GridSearch.Row, GridSearch.Cols - 1, Information.RGB(255, 255, 255));
					modGlobalFunctions.AddCYAEntry("CK", "Undeleted Dog owner " + GridSearch.TextMatrix(GridSearch.Row, COLOWNERNUM), "", "", "", "");
				}
				else
				{
					return;
				}
			}
			frmDogOwner.InstancePtr.Init(FCConvert.ToInt32(GridSearch.TextMatrix(GridSearch.Row, COLOWNERNUM)));
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(mnuContinue, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
