//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmListFacility : BaseForm
	{
		public frmListFacility()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmListFacility InstancePtr
		{
			get
			{
				return (frmListFacility)Sys.GetInstance(typeof(frmListFacility));
			}
		}

		protected frmListFacility _InstancePtr = null;
		//=========================================================
		private void frmListFacility_Activated(object sender, System.EventArgs e)
		{
			LoadData();
		}

		private void frmListFacility_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmListFacility_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				lstData_DblClick();
			}
			else if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmListFacility_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmListFacility properties;
			//frmListFacility.ScaleWidth	= 5910;
			//frmListFacility.ScaleHeight	= 3930;
			//frmListFacility.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void LoadData()
		{
			modGNBas.Statics.gstrFacility = string.Empty;
			clsDRWrapper rsFacility = new clsDRWrapper();
			rsFacility.OpenRecordset("SELECT * FROM FacilityNames order by lastname asc", modGNBas.DEFAULTDATABASE);
			lstData.Clear();
			while (!rsFacility.EndOfFile())
			{
				// If Len(rsFacility.Fields("Lastname")) > 20 Then
				// lstData.AddItem rsFacility.Fields("Lastname") & "," & Space(1) & rsFacility.Fields("Firstname")
				// Else
				// lstData.AddItem rsFacility.Fields("Lastname") & "," & Space(20 - Len(rsFacility.Fields("Lastname"))) & rsFacility.Fields("Firstname")
				// End If
				// corey 12/5/2005
				lstData.AddItem(rsFacility.Get_Fields_String("Lastname"));
				lstData.ItemData(lstData.NewIndex, FCConvert.ToInt32(rsFacility.Get_Fields_Int32("ID")));
				rsFacility.MoveNext();
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void lstData_DoubleClick(object sender, System.EventArgs e)
		{
			if (lstData.SelectedIndex < 0)
			{
				MessageBox.Show("A valid entry must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
            modGNBas.Statics.gstrFacility = lstData.Items[lstData.SelectedIndex].Text;
            modGNBas.Statics.gintFacility = lstData.ItemData(lstData.SelectedIndex);
			Close();
		}

		public void lstData_DblClick()
		{
			lstData_DoubleClick(lstData, new System.EventArgs());
		}

		private void mnuEdit_Click(object sender, System.EventArgs e)
		{
			frmFacilityNames.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
