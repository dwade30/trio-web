//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmBirthSearchA.
	/// </summary>
	partial class frmBirthSearchA
	{
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdSch;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> Text2;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> Check1;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCCheckBox Check2;
		public fecherFoundation.FCCheckBox Check4;
		public fecherFoundation.FCCheckBox Check3;
		public fecherFoundation.FCGrid SearchGrid;
		public fecherFoundation.FCButton cmdSch_1;
		public fecherFoundation.FCButton cmdSch_3;
		public fecherFoundation.FCButton cmdSch_4;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtMotherFirstName;
		public fecherFoundation.FCTextBox txtFatherFirstName;
		public fecherFoundation.FCTextBox txtChildFirstName;
		public fecherFoundation.FCTextBox txtMaidenName;
		public fecherFoundation.FCTextBox Text2_1;
		public fecherFoundation.FCTextBox Text2_0;
		public fecherFoundation.FCTextBox Text2_2;
		public fecherFoundation.FCTextBox Text2_3;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox Check1_1;
		public fecherFoundation.FCCheckBox Check1_0;
		public Wisej.Web.MaskedTextBox meBox1_0;
		public Wisej.Web.MaskedTextBox meBox1_1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblLabel5;
		public fecherFoundation.FCLabel lblLabel1;
		public fecherFoundation.FCLabel lblLabel2;
		public fecherFoundation.FCLabel lblLabel3;
		public fecherFoundation.FCLabel lblLabel4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Check2 = new fecherFoundation.FCCheckBox();
			this.Check4 = new fecherFoundation.FCCheckBox();
			this.Check3 = new fecherFoundation.FCCheckBox();
			this.SearchGrid = new fecherFoundation.FCGrid();
			this.cmdSch_1 = new fecherFoundation.FCButton();
			this.cmdSch_3 = new fecherFoundation.FCButton();
			this.cmdSch_4 = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtMotherFirstName = new fecherFoundation.FCTextBox();
			this.txtFatherFirstName = new fecherFoundation.FCTextBox();
			this.txtChildFirstName = new fecherFoundation.FCTextBox();
			this.txtMaidenName = new fecherFoundation.FCTextBox();
			this.Text2_1 = new fecherFoundation.FCTextBox();
			this.Text2_0 = new fecherFoundation.FCTextBox();
			this.Text2_2 = new fecherFoundation.FCTextBox();
			this.Text2_3 = new fecherFoundation.FCTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.Check1_1 = new fecherFoundation.FCCheckBox();
			this.Check1_0 = new fecherFoundation.FCCheckBox();
			this.meBox1_0 = new Wisej.Web.MaskedTextBox();
			this.meBox1_1 = new Wisej.Web.MaskedTextBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblLabel5 = new fecherFoundation.FCLabel();
			this.lblLabel1 = new fecherFoundation.FCLabel();
			this.lblLabel2 = new fecherFoundation.FCLabel();
			this.lblLabel3 = new fecherFoundation.FCLabel();
			this.lblLabel4 = new fecherFoundation.FCLabel();
			this.cmdNew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Check2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Check1_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSch_4);
			this.BottomPanel.Location = new System.Drawing.Point(0, 671);
			this.BottomPanel.Size = new System.Drawing.Size(944, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Check2);
			this.ClientArea.Controls.Add(this.Check4);
			this.ClientArea.Controls.Add(this.Check3);
			this.ClientArea.Controls.Add(this.SearchGrid);
			this.ClientArea.Controls.Add(this.cmdSch_1);
			this.ClientArea.Controls.Add(this.cmdSch_3);
			this.ClientArea.Controls.Add(this.cmdPrint);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(964, 628);
			this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdSch_3, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdSch_1, 0);
			this.ClientArea.Controls.SetChildIndex(this.SearchGrid, 0);
			this.ClientArea.Controls.SetChildIndex(this.Check3, 0);
			this.ClientArea.Controls.SetChildIndex(this.Check4, 0);
			this.ClientArea.Controls.SetChildIndex(this.Check2, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Size = new System.Drawing.Size(964, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(231, 28);
			this.HeaderText.Text = "Search Birth Records";
			// 
			// Check2
			// 
			this.Check2.Checked = true;
			this.Check2.CheckState = Wisej.Web.CheckState.Checked;
			this.Check2.Location = new System.Drawing.Point(30, 594);
			this.Check2.Name = "Check2";
			this.Check2.Size = new System.Drawing.Size(92, 22);
			this.Check2.TabIndex = 12;
			this.Check2.TabStop = false;
			this.Check2.Text = "Live Births";
			// 
			// Check4
			// 
			this.Check4.Checked = true;
			this.Check4.CheckState = Wisej.Web.CheckState.Checked;
			this.Check4.Location = new System.Drawing.Point(298, 594);
			this.Check4.Name = "Check4";
			this.Check4.Size = new System.Drawing.Size(112, 22);
			this.Check4.TabIndex = 14;
			this.Check4.TabStop = false;
			this.Check4.Text = "Foreign Births";
			// 
			// Check3
			// 
			this.Check3.Checked = true;
			this.Check3.CheckState = Wisej.Web.CheckState.Checked;
			this.Check3.Location = new System.Drawing.Point(149, 594);
			this.Check3.Name = "Check3";
			this.Check3.Size = new System.Drawing.Size(116, 22);
			this.Check3.TabIndex = 13;
			this.Check3.TabStop = false;
			this.Check3.Text = "Delayed Births";
			// 
			// SearchGrid
			// 
			this.SearchGrid.Cols = 10;
			this.SearchGrid.Location = new System.Drawing.Point(30, 338);
			this.SearchGrid.Name = "SearchGrid";
			this.SearchGrid.Rows = 50;
			this.SearchGrid.Size = new System.Drawing.Size(890, 246);
			this.SearchGrid.TabIndex = 20;
			this.SearchGrid.Click += new System.EventHandler(this.SearchGrid_ClickEvent);
			this.SearchGrid.DoubleClick += new System.EventHandler(this.SearchGrid_DblClick);
			// 
			// cmdSch_1
			// 
			this.cmdSch_1.AppearanceKey = "actionButton";
			this.cmdSch_1.DialogResult = Wisej.Web.DialogResult.OK;
			this.cmdSch_1.Location = new System.Drawing.Point(148, 631);
			this.cmdSch_1.Name = "cmdSch_1";
			this.cmdSch_1.Size = new System.Drawing.Size(100, 40);
			this.cmdSch_1.TabIndex = 18;
			this.cmdSch_1.Text = "Search ";
			this.cmdSch_1.Click += new System.EventHandler(this.cmdSch_Click);
			// 
			// cmdSch_3
			// 
			this.cmdSch_3.AppearanceKey = "actionButton";
			this.cmdSch_3.DialogResult = Wisej.Web.DialogResult.OK;
			this.cmdSch_3.Location = new System.Drawing.Point(30, 631);
			this.cmdSch_3.Name = "cmdSch_3";
			this.cmdSch_3.Size = new System.Drawing.Size(100, 40);
			this.cmdSch_3.TabIndex = 16;
			this.cmdSch_3.Text = "Clear";
			this.cmdSch_3.Click += new System.EventHandler(this.cmdSch_Click);
			// 
			// cmdSch_4
			// 
			this.cmdSch_4.AppearanceKey = "acceptButton";
			this.cmdSch_4.Location = new System.Drawing.Point(388, 30);
			this.cmdSch_4.Name = "cmdSch_4";
			this.cmdSch_4.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSch_4.Size = new System.Drawing.Size(150, 48);
			this.cmdSch_4.TabIndex = 19;
			this.cmdSch_4.Text = "Show Record";
			this.cmdSch_4.Click += new System.EventHandler(this.cmdSch_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "actionButton";
			this.cmdPrint.Enabled = false;
			this.cmdPrint.Location = new System.Drawing.Point(268, 631);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(100, 40);
			this.cmdPrint.TabIndex = 17;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxLeftBorder";
			this.Frame1.Controls.Add(this.txtMotherFirstName);
			this.Frame1.Controls.Add(this.txtFatherFirstName);
			this.Frame1.Controls.Add(this.txtChildFirstName);
			this.Frame1.Controls.Add(this.txtMaidenName);
			this.Frame1.Controls.Add(this.Text2_1);
			this.Frame1.Controls.Add(this.Text2_0);
			this.Frame1.Controls.Add(this.Text2_2);
			this.Frame1.Controls.Add(this.Text2_3);
			this.Frame1.Controls.Add(this.Frame2);
			this.Frame1.Controls.Add(this.Label5);
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Controls.Add(this.lblLabel5);
			this.Frame1.Controls.Add(this.lblLabel1);
			this.Frame1.Controls.Add(this.lblLabel2);
			this.Frame1.Controls.Add(this.lblLabel3);
			this.Frame1.Controls.Add(this.lblLabel4);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(890, 288);
			this.Frame1.TabIndex = 21;
			this.Frame1.Text = "Search Criteria";
			// 
			// txtMotherFirstName
			// 
			this.txtMotherFirstName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtMotherFirstName.Location = new System.Drawing.Point(372, 148);
			this.txtMotherFirstName.Name = "txtMotherFirstName";
			this.txtMotherFirstName.Size = new System.Drawing.Size(175, 40);
			this.txtMotherFirstName.TabIndex = 6;
			// 
			// txtFatherFirstName
			// 
			this.txtFatherFirstName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtFatherFirstName.Location = new System.Drawing.Point(372, 97);
			this.txtFatherFirstName.Name = "txtFatherFirstName";
			this.txtFatherFirstName.Size = new System.Drawing.Size(175, 40);
			this.txtFatherFirstName.TabIndex = 4;
			// 
			// txtChildFirstName
			// 
			this.txtChildFirstName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.txtChildFirstName.Location = new System.Drawing.Point(372, 46);
			this.txtChildFirstName.Name = "txtChildFirstName";
			this.txtChildFirstName.Size = new System.Drawing.Size(175, 40);
			this.txtChildFirstName.TabIndex = 1;
			// 
			// txtMaidenName
			// 
			this.txtMaidenName.BackColor = System.Drawing.SystemColors.Window;
			this.txtMaidenName.Location = new System.Drawing.Point(162, 198);
			this.txtMaidenName.Name = "txtMaidenName";
			this.txtMaidenName.Size = new System.Drawing.Size(190, 40);
			this.txtMaidenName.TabIndex = 7;
			// 
			// Text2_1
			// 
			this.Text2_1.BackColor = System.Drawing.SystemColors.Window;
			this.Text2_1.Location = new System.Drawing.Point(162, 97);
			this.Text2_1.Name = "Text2_1";
			this.Text2_1.Size = new System.Drawing.Size(190, 40);
			this.Text2_1.TabIndex = 3;
			this.Text2_1.Enter += new System.EventHandler(this.Text2_Enter);
			// 
			// Text2_0
			// 
			this.Text2_0.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
			this.Text2_0.Location = new System.Drawing.Point(162, 46);
			this.Text2_0.Name = "Text2_0";
			this.Text2_0.Size = new System.Drawing.Size(190, 40);
			this.Text2_0.TabIndex = 8;
			this.Text2_0.Enter += new System.EventHandler(this.Text2_Enter);
			// 
			// Text2_2
			// 
			this.Text2_2.BackColor = System.Drawing.SystemColors.Window;
			this.Text2_2.Location = new System.Drawing.Point(162, 148);
			this.Text2_2.Name = "Text2_2";
			this.Text2_2.Size = new System.Drawing.Size(190, 40);
			this.Text2_2.TabIndex = 5;
			this.Text2_2.Enter += new System.EventHandler(this.Text2_Enter);
			// 
			// Text2_3
			// 
			this.Text2_3.BackColor = System.Drawing.SystemColors.Window;
			this.Text2_3.Location = new System.Drawing.Point(162, 248);
			this.Text2_3.Name = "Text2_3";
			this.Text2_3.Size = new System.Drawing.Size(385, 40);
			this.Text2_3.TabIndex = 2;
			this.Text2_3.Enter += new System.EventHandler(this.Text2_Enter);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.Check1_1);
			this.Frame2.Controls.Add(this.Check1_0);
			this.Frame2.Controls.Add(this.meBox1_0);
			this.Frame2.Controls.Add(this.meBox1_1);
			this.Frame2.Controls.Add(this.Label3);
			this.Frame2.Controls.Add(this.Label1);
			this.Frame2.Location = new System.Drawing.Point(567, 30);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(323, 158);
			this.Frame2.TabIndex = 26;
			this.Frame2.Text = "Live Birth Date Search";
			// 
			// Check1_1
			// 
			this.Check1_1.Location = new System.Drawing.Point(172, 30);
			this.Check1_1.Name = "Check1_1";
			this.Check1_1.Size = new System.Drawing.Size(120, 22);
			this.Check1_1.TabIndex = 9;
			this.Check1_1.Text = "Between Dates";
			this.Check1_1.CheckedChanged += new System.EventHandler(this.Check1_CheckedChanged);
			// 
			// Check1_0
			// 
			this.Check1_0.Location = new System.Drawing.Point(20, 30);
			this.Check1_0.Name = "Check1_0";
			this.Check1_0.Size = new System.Drawing.Size(125, 22);
			this.Check1_0.TabIndex = 8;
			this.Check1_0.Text = "Exact Birth Date ";
			this.Check1_0.CheckedChanged += new System.EventHandler(this.Check1_CheckedChanged);
			// 
			// meBox1_0
			// 
			this.meBox1_0.Enabled = false;
			this.meBox1_0.LabelText = "";
			this.meBox1_0.Location = new System.Drawing.Point(20, 67);
			this.meBox1_0.MaxLength = 8;
			this.meBox1_0.Name = "meBox1_0";
			this.meBox1_0.Size = new System.Drawing.Size(115, 22);
			this.meBox1_0.TabIndex = 10;
			// 
			// meBox1_1
			// 
			this.meBox1_1.Enabled = false;
			this.meBox1_1.LabelText = "";
			this.meBox1_1.Location = new System.Drawing.Point(188, 67);
			this.meBox1_1.MaxLength = 8;
			this.meBox1_1.Name = "meBox1_1";
			this.meBox1_1.Size = new System.Drawing.Size(115, 22);
			this.meBox1_1.TabIndex = 11;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 118);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(234, 30);
			this.Label3.TabIndex = 30;
			this.Label3.Text = "DATE SEARCH CAN ONLY BE PERFORMED ON LIVE BIRTH RECORDS";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(145, 81);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(33, 17);
			this.Label1.TabIndex = 27;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(372, 30);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(33, 17);
			this.Label5.TabIndex = 32;
			this.Label5.Text = "FIRST";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(162, 30);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(33, 17);
			this.Label4.TabIndex = 31;
			this.Label4.Text = "LAST";
			// 
			// lblLabel5
			// 
			this.lblLabel5.Location = new System.Drawing.Point(20, 212);
			this.lblLabel5.Name = "lblLabel5";
			this.lblLabel5.Size = new System.Drawing.Size(124, 15);
			this.lblLabel5.TabIndex = 29;
			this.lblLabel5.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblLabel1
			// 
			this.lblLabel1.Location = new System.Drawing.Point(20, 60);
			this.lblLabel1.Name = "lblLabel1";
			this.lblLabel1.Size = new System.Drawing.Size(122, 18);
			this.lblLabel1.TabIndex = 25;
			this.lblLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblLabel2
			// 
			this.lblLabel2.Location = new System.Drawing.Point(20, 260);
			this.lblLabel2.Name = "lblLabel2";
			this.lblLabel2.Size = new System.Drawing.Size(122, 15);
			this.lblLabel2.TabIndex = 24;
			this.lblLabel2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblLabel3
			// 
			this.lblLabel3.Location = new System.Drawing.Point(20, 111);
			this.lblLabel3.Name = "lblLabel3";
			this.lblLabel3.Size = new System.Drawing.Size(122, 15);
			this.lblLabel3.TabIndex = 23;
			this.lblLabel3.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblLabel4
			// 
			this.lblLabel4.Location = new System.Drawing.Point(20, 162);
			this.lblLabel4.Name = "lblLabel4";
			this.lblLabel4.Size = new System.Drawing.Size(122, 15);
			this.lblLabel4.TabIndex = 22;
			this.lblLabel4.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.Location = new System.Drawing.Point(903, 31);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(47, 24);
			this.cmdNew.TabIndex = 5;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// frmBirthSearchA
			// 
			this.AcceptButton = this.cmdSch_1;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(964, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmBirthSearchA";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Search Birth Records";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmBirthSearchA_Load);
			this.Activated += new System.EventHandler(this.frmBirthSearchA_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBirthSearchA_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBirthSearchA_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Check2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSch_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Check1_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdNew;
	}
}
