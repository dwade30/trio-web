﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;

namespace TWCK0000
{
	public class clsBulkEmail
	{
		//=========================================================
		private FCCollection lstEmailList = new FCCollection();
		private int intCurrentIndex;

		public clsBulkEmail() : base()
		{
			intCurrentIndex = -1;
		}

		public void ClearList()
		{
			if (!(lstEmailList == null))
			{
				foreach (clsEmailObject tMail in lstEmailList)
				{
					lstEmailList.Remove(1);
				}
				// tMail
			}
		}

		public bool MoveFirst()
		{
			bool MoveFirst = false;
			if (!(lstEmailList == null))
			{
				if (!FCUtils.IsEmpty(lstEmailList))
				{
					if (lstEmailList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
			if (intCurrentIndex > 0)
			{
				MoveFirst = true;
			}
			else
			{
				MoveFirst = false;
			}
			return MoveFirst;
		}

		public void MoveLast()
		{
			if (!(lstEmailList == null))
			{
				if (!FCUtils.IsEmpty(lstEmailList))
				{
					if (lstEmailList.Count > 0)
					{
						intCurrentIndex = lstEmailList.Count;
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public void AddItem(ref clsEmailObject tItem)
		{
			if (!(tItem == null))
			{
				lstEmailList.Add(tItem);
			}
		}

		public int ItemCount()
		{
			int ItemCount = 0;
			if (!FCUtils.IsEmpty(lstEmailList))
			{
				ItemCount = lstEmailList.Count;
			}
			else
			{
				ItemCount = 0;
			}
			return ItemCount;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(lstEmailList))
			{
				if (intCurrentIndex > lstEmailList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= lstEmailList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > lstEmailList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (lstEmailList[intCurrentIndex] == null)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public clsEmailObject GetCurrent()
		{
			clsEmailObject GetCurrent = null;
			clsEmailObject tMail;
			tMail = null;
			if (!FCUtils.IsEmpty(lstEmailList))
			{
				if (intCurrentIndex > 0)
				{
					if (!(lstEmailList[intCurrentIndex] == null))
					{
						tMail = lstEmailList[intCurrentIndex];
					}
				}
			}
			GetCurrent = tMail;
			return GetCurrent;
		}

		public void SendMail()
		{
			if (!FCUtils.IsEmpty(lstEmailList))
			{
				foreach (clsEmailObject tMail in lstEmailList)
				{
					if (fecherFoundation.Strings.Trim(tMail.EmailAddress) != "")
					{
						if (fecherFoundation.Strings.Trim(tMail.Subject) != "" || fecherFoundation.Strings.Trim(tMail.Body) != "")
						{
							frmEMail.InstancePtr.Init("", tMail.EmailAddress, tMail.Subject, tMail.Body, true, false, false, true, true, false, showAsModalForm: true);
						}
					}
				}
				// tMail
			}
		}
	}
}
