//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMarriageSummary.
	/// </summary>
	public partial class rptMarriageSummary : BaseSectionReport
	{
		public rptMarriageSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Marriage Vital Statistics";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMarriageSummary InstancePtr
		{
			get
			{
				return (rptMarriageSummary)Sys.GetInstance(typeof(rptMarriageSummary));
			}
		}

		protected rptMarriageSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMarriageSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		int intPageNumber;
		string strStartDate;
		string strEndDate;

		public void Init(ref string strRange)
		{
			string[] strTemp = null;
			strTemp = Strings.Split(strRange, ";", -1, CompareConstants.vbTextCompare);
			strStartDate = strTemp[0];
			strEndDate = strTemp[1];
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "MarriageSummary");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rs.EndOfFile())
				return;
			txtName.Text = rs.Get_Fields_String("GroomsFirstName") + " " + rs.Get_Fields_String("GroomsMiddleName") + " " + rs.Get_Fields_String("GroomsLastName") + " & " + rs.Get_Fields_String("BridesFirstName") + " " + rs.Get_Fields_String("BridesMiddleName") + " " + rs.Get_Fields_String("BridesCurrentLastName");
			txtBorn.Text = Strings.Format(rs.Get_Fields_String("CeremonyDate"), "MMMM dd, yyyy");
			if (!rs.EndOfFile())
				rs.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtTown.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "H:mm AM/PM");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtCaption.Text = "Town Clerk's Report";
			txtCaption2.Text = "Marriages - " + strStartDate + " to " + strEndDate;
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// txtTown = "Annual Town Report" 'CityTown & " of " & MuniName
			txtDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			intPageNumber = 1;
			// Call RS.OpenRecordset("Select * from Marriages where val(ceremonydate & '') > 0 and cdate(CeremonyDate & '') BETWEEN #01/01/" & gintReportYear & "# and #12/31/" & gintReportYear & "#", DEFAULTDATABASENAME)
			// 08/12/2005
			rs.OpenRecordset("Select * from Marriages where (ISDATE(ceremonydate) = 1 and convert(datetime, CeremonyDate) BETWEEN '" + strStartDate + "' and '" + strEndDate + "') or (ActualDate BETWEEN '" + strStartDate + "' and '" + strEndDate + "') order by actualdate", modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				rs.MoveLast();
				rs.MoveFirst();
				if (rs.RecordCount() == 0)
				{
					txtTotal.Text = string.Empty;
				}
				else if (rs.RecordCount() == 1)
				{
					txtTotal.Text = "There was a total of " + FCConvert.ToString(rs.RecordCount()) + " Marriage";
				}
				else
				{
					txtTotal.Text = "There were a total of " + FCConvert.ToString(rs.RecordCount()) + " Marriages";
				}
			}
			// Call SetPrintProperties(Me)
		}
		// Public Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports2.DDTool)
		// Call VerifyPrintToFile(Me, Tool)
		// End Sub
		
	}
}
