//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDogSummary.
	/// </summary>
	partial class frmDogSummary
	{
		public fecherFoundation.FCComboBox cmbDetail;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbDetail = new fecherFoundation.FCComboBox();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 213);
            this.BottomPanel.Size = new System.Drawing.Size(412, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbYear);
            this.ClientArea.Controls.Add(this.cmbDetail);
            this.ClientArea.Size = new System.Drawing.Size(412, 153);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(412, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(261, 30);
            this.HeaderText.Text = "Dog License Summary";
            // 
            // cmbDetail
            // 
            this.cmbDetail.AutoSize = false;
            this.cmbDetail.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDetail.FormattingEnabled = true;
            this.cmbDetail.Items.AddRange(new object[] {
            "Summary",
            "Detail"});
            this.cmbDetail.Location = new System.Drawing.Point(30, 90);
            this.cmbDetail.Name = "cmbDetail";
            this.cmbDetail.Size = new System.Drawing.Size(203, 40);
            this.cmbDetail.TabIndex = 4;
            this.cmbDetail.Text = "Detail";
            // 
            // cmbYear
            // 
            this.cmbYear.AutoSize = false;
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.Location = new System.Drawing.Point(30, 30);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(203, 40);
            this.cmbYear.TabIndex = 3;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(119, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(170, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmDogSummary
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(412, 321);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmDogSummary";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Dog License Summary";
            this.Load += new System.EventHandler(this.frmDogSummary_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDogSummary_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}