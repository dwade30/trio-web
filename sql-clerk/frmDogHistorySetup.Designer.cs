//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDogHistorySetup.
	/// </summary>
	partial class frmDogHistorySetup
	{
		public fecherFoundation.FCComboBox cmbAllOwners;
		public fecherFoundation.FCComboBox cmbSpecificDog;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCButton cmdEndDate;
		public fecherFoundation.FCButton cmdStartDate;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCFrame fraOwners;
		public fecherFoundation.FCComboBox cboOwner;
		public fecherFoundation.FCFrame fraDogs;
		public fecherFoundation.FCComboBox cboDogs;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDogHistorySetup));
            this.cmbAllOwners = new fecherFoundation.FCComboBox();
            this.cmbSpecificDog = new fecherFoundation.FCComboBox();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.cmdEndDate = new fecherFoundation.FCButton();
            this.cmdStartDate = new fecherFoundation.FCButton();
            this.txtStartDate = new Global.T2KDateBox();
            this.txtEndDate = new Global.T2KDateBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.fraOwners = new fecherFoundation.FCFrame();
            this.cboOwner = new fecherFoundation.FCComboBox();
            this.fraDogs = new fecherFoundation.FCFrame();
            this.cboDogs = new fecherFoundation.FCComboBox();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOwners)).BeginInit();
            this.fraOwners.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDogs)).BeginInit();
            this.fraDogs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 428);
            this.BottomPanel.Size = new System.Drawing.Size(460, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbAllOwners);
            this.ClientArea.Controls.Add(this.fraDateRange);
            this.ClientArea.Controls.Add(this.fraOwners);
            this.ClientArea.Size = new System.Drawing.Size(460, 368);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(460, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(140, 30);
            this.HeaderText.Text = "Dog History";
            // 
            // cmbAllOwners
            // 
            this.cmbAllOwners.AutoSize = false;
            this.cmbAllOwners.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAllOwners.FormattingEnabled = true;
            this.cmbAllOwners.Items.AddRange(new object[] {
            "All Owners",
            "Specific Owner"});
            this.cmbAllOwners.Location = new System.Drawing.Point(30, 140);
            this.cmbAllOwners.Name = "cmbAllOwners";
            this.cmbAllOwners.Size = new System.Drawing.Size(254, 40);
            this.cmbAllOwners.TabIndex = 0;
            this.cmbAllOwners.Text = "All Owners";
            this.cmbAllOwners.SelectedIndexChanged += new System.EventHandler(this.cmbAllOwners_SelectedIndexChanged);
            // 
            // cmbSpecificDog
            // 
            this.cmbSpecificDog.AutoSize = false;
            this.cmbSpecificDog.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSpecificDog.FormattingEnabled = true;
            this.cmbSpecificDog.Items.AddRange(new object[] {
            "Specific Dog",
            "All Dogs"});
            this.cmbSpecificDog.Location = new System.Drawing.Point(20, 79);
            this.cmbSpecificDog.Name = "cmbSpecificDog";
            this.cmbSpecificDog.Size = new System.Drawing.Size(254, 40);
            this.cmbSpecificDog.TabIndex = 12;
            this.cmbSpecificDog.Text = "All Dogs";
            this.cmbSpecificDog.SelectedIndexChanged += new System.EventHandler(this.cmbSpecificDog_SelectedIndexChanged);
            // 
            // fraDateRange
            // 
            this.fraDateRange.Controls.Add(this.cmdEndDate);
            this.fraDateRange.Controls.Add(this.cmdStartDate);
            this.fraDateRange.Controls.Add(this.txtStartDate);
            this.fraDateRange.Controls.Add(this.txtEndDate);
            this.fraDateRange.Controls.Add(this.lblTo);
            this.fraDateRange.Location = new System.Drawing.Point(30, 30);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(402, 90);
            this.fraDateRange.TabIndex = 0;
            this.fraDateRange.Text = "Transaction Date Range";
            // 
            // cmdEndDate
            // 
            this.cmdEndDate.AppearanceKey = "imageButton";
            this.cmdEndDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdEndDate.Location = new System.Drawing.Point(344, 30);
            this.cmdEndDate.Name = "cmdEndDate";
            this.cmdEndDate.Size = new System.Drawing.Size(40, 40);
            this.cmdEndDate.TabIndex = 2;
            this.cmdEndDate.Click += new System.EventHandler(this.cmdEndDate_Click);
            // 
            // cmdStartDate
            // 
            this.cmdStartDate.AppearanceKey = "imageButton";
            this.cmdStartDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdStartDate.Location = new System.Drawing.Point(142, 30);
            this.cmdStartDate.Name = "cmdStartDate";
            this.cmdStartDate.Size = new System.Drawing.Size(40, 40);
            this.cmdStartDate.TabIndex = 1;
            this.cmdStartDate.Click += new System.EventHandler(this.cmdStartDate_Click);
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(20, 30);
            this.txtStartDate.Mask = "00/00/0000";
            this.txtStartDate.MaxLength = 10;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate.TabIndex = 3;
            this.txtStartDate.Text = "  /  /";
            // 
            // txtEndDate
            // 
            this.txtEndDate.Location = new System.Drawing.Point(222, 30);
            this.txtEndDate.Mask = "00/00/0000";
            this.txtEndDate.MaxLength = 10;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate.TabIndex = 4;
            this.txtEndDate.Text = "  /  /";
            // 
            // lblTo
            // 
            this.lblTo.Enabled = false;
            this.lblTo.Location = new System.Drawing.Point(192, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(21, 15);
            this.lblTo.TabIndex = 5;
            this.lblTo.Text = "TO";
            // 
            // fraOwners
            // 
            this.fraOwners.AppearanceKey = "groupBoxNoBorders";
            this.fraOwners.Controls.Add(this.cboOwner);
            this.fraOwners.Controls.Add(this.cmbSpecificDog);
            this.fraOwners.Controls.Add(this.fraDogs);
            this.fraOwners.Enabled = false;
            this.fraOwners.Location = new System.Drawing.Point(10, 181);
            this.fraOwners.Name = "fraOwners";
            this.fraOwners.Size = new System.Drawing.Size(297, 202);
            this.fraOwners.TabIndex = 8;
            // 
            // cboOwner
            // 
            this.cboOwner.AutoSize = false;
            this.cboOwner.BackColor = System.Drawing.SystemColors.Window;
            this.cboOwner.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboOwner.Enabled = false;
            this.cboOwner.FormattingEnabled = true;
            this.cboOwner.Location = new System.Drawing.Point(20, 19);
            this.cboOwner.Name = "cboOwner";
            this.cboOwner.Size = new System.Drawing.Size(254, 40);
            this.cboOwner.TabIndex = 11;
            this.cboOwner.SelectedIndexChanged += new System.EventHandler(this.cboOwner_SelectedIndexChanged);
            // 
            // fraDogs
            // 
            this.fraDogs.AppearanceKey = "groupBoxNoBorders";
            this.fraDogs.Controls.Add(this.cboDogs);
            this.fraDogs.Enabled = false;
            this.fraDogs.Location = new System.Drawing.Point(0, 120);
            this.fraDogs.Name = "fraDogs";
            this.fraDogs.Size = new System.Drawing.Size(291, 64);
            this.fraDogs.TabIndex = 12;
            // 
            // cboDogs
            // 
            this.cboDogs.AutoSize = false;
            this.cboDogs.BackColor = System.Drawing.SystemColors.Window;
            this.cboDogs.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboDogs.Enabled = false;
            this.cboDogs.FormattingEnabled = true;
            this.cboDogs.Location = new System.Drawing.Point(20, 19);
            this.cboDogs.Name = "cboDogs";
            this.cboDogs.Size = new System.Drawing.Size(254, 40);
            this.cboDogs.TabIndex = 13;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 0;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePreview.Text = "Print Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdPreview
            // 
            this.cmdPreview.AppearanceKey = "acceptButton";
            this.cmdPreview.Location = new System.Drawing.Point(177, 30);
            this.cmdPreview.Name = "cmdPreview";
            this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPreview.Size = new System.Drawing.Size(104, 48);
            this.cmdPreview.TabIndex = 0;
            this.cmdPreview.Text = "Preview";
            this.cmdPreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // frmDogHistorySetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(460, 536);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmDogHistorySetup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Dog History";
            this.Load += new System.EventHandler(this.frmDogHistorySetup_Load);
            this.Activated += new System.EventHandler(this.frmDogHistorySetup_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDogHistorySetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            this.fraDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOwners)).EndInit();
            this.fraOwners.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDogs)).EndInit();
            this.fraDogs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPreview;
	}
}