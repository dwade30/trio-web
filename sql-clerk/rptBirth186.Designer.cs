﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBirth186.
	/// </summary>
	partial class rptBirth186
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBirth186));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeceased = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmended = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoleParent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.FileNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeceased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoleParent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FileNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field6,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Field5,
				this.txtDeceased,
				this.txtAmended,
				this.txtSoleParent,
				this.FileNumber
			});
			this.Detail.Height = 10.30208F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Field6
			// 
			this.Field6.CanGrow = false;
			this.Field6.Height = 0.4166667F;
			this.Field6.Left = 4.5F;
			this.Field6.Name = "Field6";
			this.Field6.Text = "Attendant\'s Title";
			this.Field6.Top = 4.5F;
			this.Field6.Width = 0.875F;
			// 
			// Field1
			// 
			this.Field1.CanGrow = false;
			this.Field1.Height = 0.375F;
			this.Field1.Left = 1.9375F;
			this.Field1.Name = "Field1";
			this.Field1.Text = "Name of Child";
			this.Field1.Top = 3.125F;
			this.Field1.Width = 3.375F;
			// 
			// Field2
			// 
			this.Field2.CanGrow = false;
			this.Field2.Height = 0.375F;
			this.Field2.Left = 5.4375F;
			this.Field2.Name = "Field2";
			this.Field2.Text = "DOB";
			this.Field2.Top = 3.125F;
			this.Field2.Width = 2.895833F;
			// 
			// Field3
			// 
			this.Field3.CanGrow = false;
			this.Field3.Height = 0.4583333F;
			this.Field3.Left = 1.9375F;
			this.Field3.Name = "Field3";
			this.Field3.Text = "Sex";
			this.Field3.Top = 3.625F;
			this.Field3.Width = 1.03125F;
			// 
			// Field4
			// 
			this.Field4.CanGrow = false;
			this.Field4.Height = 0.375F;
			this.Field4.Left = 5.4375F;
			this.Field4.Name = "Field4";
			this.Field4.Text = "Place of Birth";
			this.Field4.Top = 3.625F;
			this.Field4.Width = 2.3125F;
			// 
			// Field7
			// 
			this.Field7.CanGrow = false;
			this.Field7.Height = 0.5F;
			this.Field7.Left = 5.4375F;
			this.Field7.Name = "Field7";
			this.Field7.Text = "Attendant\'s Address";
			this.Field7.Top = 4.5F;
			this.Field7.Width = 2.1875F;
			// 
			// Field8
			// 
			this.Field8.CanGrow = false;
			this.Field8.Height = 0.375F;
			this.Field8.Left = 2F;
			this.Field8.Name = "Field8";
			this.Field8.Text = "Mother\'s Maiden Name";
			this.Field8.Top = 5.333333F;
			this.Field8.Width = 2.5625F;
			// 
			// Field9
			// 
			this.Field9.CanGrow = false;
			this.Field9.Height = 0.3333333F;
			this.Field9.Left = 5.5F;
			this.Field9.Name = "Field9";
			this.Field9.Text = "Mother\'s residence";
			this.Field9.Top = 5.333333F;
			this.Field9.Width = 2.375F;
			// 
			// Field10
			// 
			this.Field10.CanGrow = false;
			this.Field10.Height = 0.5F;
			this.Field10.Left = 2F;
			this.Field10.Name = "Field10";
			this.Field10.Text = "Father\'s Name";
			this.Field10.Top = 6.25F;
			this.Field10.Width = 2.5625F;
			// 
			// Field11
			// 
			this.Field11.CanGrow = false;
			this.Field11.Height = 0.4583333F;
			this.Field11.Left = 2F;
			this.Field11.Name = "Field11";
			this.Field11.Text = "Clerk of Record";
			this.Field11.Top = 7.208333F;
			this.Field11.Width = 2.5F;
			// 
			// Field12
			// 
			this.Field12.CanGrow = false;
			this.Field12.Height = 0.5F;
			this.Field12.Left = 4.5F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "text-align: center";
			this.Field12.Text = null;
			this.Field12.Top = 7.208333F;
			this.Field12.Width = 1.375F;
			// 
			// Field13
			// 
			this.Field13.CanGrow = false;
			this.Field13.Height = 0.4583333F;
			this.Field13.Left = 5.875F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "text-align: center";
			this.Field13.Text = "Date of Filing";
			this.Field13.Top = 7.208333F;
			this.Field13.Width = 2.427083F;
			// 
			// Field14
			// 
			this.Field14.CanGrow = false;
			this.Field14.Height = 0.3333333F;
			this.Field14.Left = 2.6875F;
			this.Field14.Name = "Field14";
			this.Field14.Text = "Attest";
			this.Field14.Top = 9.583333F;
			this.Field14.Width = 2.5625F;
			// 
			// Field15
			// 
			this.Field15.CanGrow = false;
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 2.6875F;
			this.Field15.Name = "Field15";
			this.Field15.Text = "Date Issued";
			this.Field15.Top = 10.0625F;
			this.Field15.Width = 2.1875F;
			// 
			// Field16
			// 
			this.Field16.CanGrow = false;
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 5.75F;
			this.Field16.Name = "Field16";
			this.Field16.Text = "Muni";
			this.Field16.Top = 10.0625F;
			this.Field16.Width = 2.125F;
			// 
			// Field5
			// 
			this.Field5.CanGrow = false;
			this.Field5.Height = 0.4166667F;
			this.Field5.Left = 1.9375F;
			this.Field5.Name = "Field5";
			this.Field5.Text = "Attendant\'s Name";
			this.Field5.Top = 4.5F;
			this.Field5.Width = 2.5F;
			// 
			// txtDeceased
			// 
			this.txtDeceased.Height = 0.1875F;
			this.txtDeceased.Left = 3.3125F;
			this.txtDeceased.Name = "txtDeceased";
			this.txtDeceased.Style = "font-size: 12pt; font-weight: bold";
			this.txtDeceased.Tag = "DECEASED";
			this.txtDeceased.Text = "Deceased";
			this.txtDeceased.Top = 1.8125F;
			this.txtDeceased.Width = 2.5F;
			// 
			// txtAmended
			// 
			this.txtAmended.CanGrow = false;
			this.txtAmended.Height = 0.625F;
			this.txtAmended.Left = 1F;
			this.txtAmended.Name = "txtAmended";
			this.txtAmended.Style = "font-size: 8pt";
			this.txtAmended.Text = null;
			this.txtAmended.Top = 8F;
			this.txtAmended.Width = 6.8125F;
			// 
			// txtSoleParent
			// 
			this.txtSoleParent.CanGrow = false;
			this.txtSoleParent.Height = 0.1875F;
			this.txtSoleParent.Left = 2.6875F;
			this.txtSoleParent.Name = "txtSoleParent";
			this.txtSoleParent.Text = null;
			this.txtSoleParent.Top = 7.8125F;
			this.txtSoleParent.Width = 2.8125F;
			// 
			// FileNumber
			// 
			this.FileNumber.CanGrow = false;
			this.FileNumber.DataField = "FileNumber";
			this.FileNumber.Height = 0.1979167F;
			this.FileNumber.Left = 5.989583F;
			this.FileNumber.Name = "FileNumber";
			this.FileNumber.OutputFormat = resources.GetString("FileNumber.OutputFormat");
			this.FileNumber.Text = "FileNumber";
			this.FileNumber.Top = 0.8020833F;
			this.FileNumber.Width = 1.03125F;
			// 
			// rptBirth186
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeceased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmended)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoleParent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FileNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeceased;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmended;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoleParent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox FileNumber;
	}
}
