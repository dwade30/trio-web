//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogHistory.
	/// </summary>
	public partial class rptDogHistory : BaseSectionReport
	{
		public rptDogHistory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Dog History";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDogHistory InstancePtr
		{
			get
			{
				return (rptDogHistory)Sys.GetInstance(typeof(rptDogHistory));
			}
		}

		protected rptDogHistory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
				rsDog.Dispose();
				rsOwner.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDogHistory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();
		private int lngPage;
		string strCurrentGroup = "";
		private clsDRWrapper rsOwner = new clsDRWrapper();
		private clsDRWrapper rsDog = new clsDRWrapper();
		// vbPorter upgrade warning: datStart As DateTime	OnWrite(string)
		// vbPorter upgrade warning: datEnd As DateTime	OnWrite(string)
		public void Init(int intType, int lngOwner, int lngDog, bool boolOrderDesc, bool boolModal = false, DateTime? tempdatStart = null, DateTime? tempdatEnd = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdatStart == null)
			{
                tempdatStart = new DateTime(1900, 1, 1);
			}
			DateTime datStart = tempdatStart.Value;
			if (tempdatEnd == null)
			{
				tempdatEnd = new DateTime(1900, 1, 1);
            }
			DateTime datEnd = tempdatEnd.Value;
			// inttype 0 is all
			// inttype 1 is owner
			// inttype 2 is dog
			string strSQL = "";
			string strWhere;
			string strOrder = "";
			rsOwner.OpenRecordset("select * from dogowner order by ID", "twck0000.vb1");
			rsDog.OpenRecordset("Select * from doginfo order by ID", "Twck0000.vb1");
			strWhere = "";
			if (datStart.ToOADate() != FCConvert.ToDateTime("1/1/1900").ToOADate() && datEnd.ToOADate() != FCConvert.ToDateTime("1/1/1900").ToOADate())
			{
				strSQL = "select dogtransactions.*,lastname,firstname,middlename,designation,dogname from  dogowner inner join (dogtransactions inner join doginfo on (doginfo.ID = dogtransactions.dognumber)) on (dogtransactions.ownernum = dogowner.ID) CROSS APPLY " + rsOwner.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p where (transactiondate >= '" + FCConvert.ToString(datStart) + "' AND transactiondate <= '" + FCConvert.ToString(datEnd) + "') AND (boollicense = 1 or boolreplacementtag = 1) and boolkennel <> 1";
			}
			else
			{
				strSQL = "select dogtransactions.*,lastname,firstname,middlename,designation,dogname from  dogowner inner join (dogtransactions inner join doginfo on (doginfo.ID = dogtransactions.dognumber)) on (dogtransactions.ownernum = dogowner.ID) CROSS APPLY " + rsOwner.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p where (boollicense = 1 or boolreplacementtag = 1) and boolkennel <> 1";
			}
			if (boolOrderDesc)
			{
				strOrder = " order by lastname,firstname,middlename,designation,dogname,transactiondate desc";
			}
			else
			{
				strOrder = " order by lastname,firstname,middlename,designation,dogname,transactiondate";
			}
			switch (intType)
			{
				case 1:
					{
						strWhere = " and dogowner.ID = " + FCConvert.ToString(lngOwner);
						break;
					}
				case 2:
					{
						strWhere = " and doginfo.ID = " + FCConvert.ToString(lngDog);
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
			rsReport.OpenRecordset(strSQL + strWhere + strOrder, "twck0000.vb1");
			if (!rsReport.EndOfFile())
			{
				strCurrentGroup = rsReport.Get_Fields_Int32("ownernum") + "," + rsReport.Get_Fields_Int32("dognumber");
				//FC:FINAL:AM:#2365 - no need to set here the grpHeader
                //this.Fields["grpHeader"].Value = strCurrentGroup;
			}
			if (!boolModal)
			{
				frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "DogHistory");
			}
			else
			{
				//FC:FINAL:SBE - #i1994 - open report viewer as modal dialog (opened from another modal dialog)
                //frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: true, strAttachmentName: "DogHistory");
                frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: true, strAttachmentName: "DogHistory", showModal: true);
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			lngPage = 1;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
			if (!rsReport.EndOfFile())
			{
				strCurrentGroup = rsReport.Get_Fields_Int32("ownernum") + "," + rsReport.Get_Fields_Int32("dognumber");
				this.Fields["grpHeader"].Value = strCurrentGroup;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				txtTranDate.Text = Strings.Format(rsReport.Get_Fields("transactiondate"), "MM/dd/yyyy");
				txtAmount.Text = Strings.Format(rsReport.Get_Fields("amount"), "0.00");
				txtDogYear.Text = FCConvert.ToString(rsReport.Get_Fields("dogyear"));
				txtRabiesTag.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("rabiestagnumber"));
				txtTag.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsReport.Get_Fields("TagNumber")));
				txtUser.Text = Strings.Format(rsReport.Get_Fields_Double("animalcontrol"), "0.00");
				txtState.Text = Strings.Format(rsReport.Get_Fields_Double("statefee"), "0.00");
				txtTown.Text = Strings.Format(rsReport.Get_Fields_Double("townfee"), "0.00");
				txtClerk.Text = Strings.Format(rsReport.Get_Fields_Double("clerkfee"), "0.00");
				txtLate.Text = Strings.Format(rsReport.Get_Fields("latefee"), "0.00");
				if (FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("boolReplacementTag")))
				{
					txtDescription.Text = "Replacement Tag";
				}
				else
				{
					txtDescription.Text = "License";
				}
				rsReport.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(this.Fields["grpHeader"].Value)) != "")
			{
				string[] strAry = null;
				strAry = Strings.Split(FCConvert.ToString(this.Fields["grpHeader"].Value), ",", -1, CompareConstants.vbBinaryCompare);
				int lngOwn = 0;
				int lngDog = 0;
				lngOwn = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				lngDog = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
				if (rsOwner.FindFirstRecord("ID", lngOwn))
				{
					txtOwnerName.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("Firstname") + " " + rsReport.Get_Fields_String("lastName") + " " + rsReport.Get_Fields("Desigation"));
				}
				else
				{
					txtOwnerName.Text = "";
				}
				if (rsDog.FindFirstRecord("ID", lngDog))
				{
					txtBreed.Text = fecherFoundation.Strings.Trim(rsDog.Get_Fields_String("DogBreed"));
					txtColor.Text = fecherFoundation.Strings.Trim(rsDog.Get_Fields_String("Dogcolor"));
					if (Information.IsDate(rsDog.Get_Fields("DogDob")))
					{
						if (!(rsDog.Get_Fields_DateTime("DogDOB").ToOADate() == 0))
						{
							txtDOB.Text = Strings.Format(rsDog.Get_Fields_DateTime("DogDOB"), "MM/dd/yyyy");
						}
						else
						{
							txtDOB.Text = "";
						}
					}
					else
					{
						txtDOB.Text = "";
					}
					txtDogName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDog.Get_Fields_String("DogName")));
					if (rsDog.Get_Fields_Boolean("HearGuide"))
					{
						txtHybrid.Text = "Yes";
					}
					else
					{
						txtHybrid.Text = "No";
					}
					if (rsDog.Get_Fields_Boolean("WolfHybrid"))
					{
						txtNuisanceDangerous.Text = "Yes";
					}
					else
					{
						txtNuisanceDangerous.Text = "No";
					}
					if (rsDog.Get_Fields_Boolean("DogNeuter"))
					{
						txtSSR.Text = "Yes";
					}
					else
					{
						txtSSR.Text = "No";
					}
					if (rsDog.Get_Fields_Boolean("SandR"))
					{
						txtHearing.Text = "Yes";
					}
					else
					{
						txtHearing.Text = "No";
					}

                    if (rsDog.Get_Fields_Boolean("IsDangerousDog"))
                    {
                        txtNuisanceDangerous.Text = "Yes";
                        lblNuisanceDangerous.Text = "Dangerous";
                    }
                    else if (rsDog.Get_Fields_Boolean("IsNuisanceDog"))
                    {
                        txtNuisanceDangerous.Text = "Yes";
                        lblNuisanceDangerous.Text = "Nuisance";
                    }
                    else
                    {
                        txtNuisanceDangerous.Text = "";
                        lblNuisanceDangerous.Text = "";
                    }
					txtSex.Text = fecherFoundation.Strings.Trim(rsDog.Get_Fields_String("DogSex"));
					txtVeterinarian.Text = fecherFoundation.Strings.Trim(rsDog.Get_Fields_String("dogvet"));
				}
				else
				{
					txtBreed.Text = "";
					txtColor.Text = "";
					txtDOB.Text = "";
					txtDogName.Text = "";
					txtHybrid.Text = "No";
					txtNuisanceDangerous.Text = "No";
					txtSSR.Text = "No";
					txtSex.Text = "";
					txtHearing.Text = "No";
					txtVeterinarian.Text = "";
                    lblNuisanceDangerous.Text = "Nuisance";
                    txtNuisanceDangerous.Text = "";
                }
			}
			else
			{
				txtOwnerName.Text = "";
				txtBreed.Text = "";
				txtColor.Text = "";
				txtDOB.Text = "";
				txtDogName.Text = "";
				txtHybrid.Text = "No";
				txtNuisanceDangerous.Text = "No";
				txtSSR.Text = "No";
				txtSex.Text = "";
				txtHearing.Text = "No";
				txtVeterinarian.Text = "";
                lblNuisanceDangerous.Text = "Nuisance";
                txtNuisanceDangerous.Text = "";
            }
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
			lngPage += 1;
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
