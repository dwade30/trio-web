//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmDogHistorySetup : BaseForm
	{
		public frmDogHistorySetup()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDogHistorySetup InstancePtr
		{
			get
			{
				return (frmDogHistorySetup)Sys.GetInstance(typeof(frmDogHistorySetup));
			}
		}

		protected frmDogHistorySetup _InstancePtr = null;
		DateTime datReturnedDate;

		private void cmdEndDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEndDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdStartDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtStartDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cboOwner_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillDogCombo();
		}

		private void frmDogHistorySetup_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmDogHistorySetup_Load(object sender, System.EventArgs e)
		{
			txtStartDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			FillOwnersCombo();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void FillOwnersCombo()
		{
			clsDRWrapper rs = new clsDRWrapper();
			cboOwner.Clear();
			rs.OpenRecordset("SELECT * FROM DogOwner ORDER BY lastname, firstname", "TWCK0000.vb1");
            rs.OpenRecordset(
                "select Dogowner.ID as OwnerNum, FirstName,MiddleName, LastName from dogowner left join " +
                rs.CurrentPrefix +
                "CentralParties.dbo.Parties as p on dogowner.partyid = p.Id order by lastname,firstname", "Clerk");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					cboOwner.AddItem(rs.Get_Fields_String("lastname") + ", " + rs.Get_Fields_String("firstname"));
					cboOwner.ItemData(cboOwner.NewIndex, FCConvert.ToInt32(rs.Get_Fields_Int32("OwnerNum")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
				cboOwner.SelectedIndex = 0;
			}
		}

		private void FillDogCombo()
		{
			clsDRWrapper rs = new clsDRWrapper();
			cboDogs.Clear();
			rs.OpenRecordset("SELECT * FROM DogInfo WHERE OwnerNum = " + FCConvert.ToString(cboOwner.ItemData(cboOwner.SelectedIndex)) + " ORDER BY dogname", "TWCK0000.vb1");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					cboDogs.AddItem(rs.Get_Fields_String("dogname"));
					cboDogs.ItemData(cboDogs.NewIndex, FCConvert.ToInt32(rs.Get_Fields_Int32("ID")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
				cboDogs.SelectedIndex = 0;
			}
		}

		private void frmDogHistorySetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			int intType = 0;
			int lngOwner = 0;
			int lngDog = 0;
			if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbAllOwners.Text == "All Owners")
			{
				intType = 0;
				lngOwner = 0;
				lngDog = 0;
			}
			else if (cmbSpecificDog.Text == "All")
			{
				intType = 1;
				lngOwner = cboOwner.ItemData(cboOwner.SelectedIndex);
				lngDog = 0;
			}
			else
			{
				intType = 2;
				lngOwner = cboOwner.ItemData(cboOwner.SelectedIndex);
				lngDog = cboDogs.ItemData(cboDogs.SelectedIndex);
			}
			rptDogHistory.InstancePtr.Init(intType, lngOwner, lngDog, false, false, Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAllDogs_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDogs.Enabled = false;
			cboDogs.Enabled = false;
		}

		private void optAllOwners_CheckedChanged(object sender, System.EventArgs e)
		{
			fraOwners.Enabled = false;
			cboOwner.Enabled = false;
		}

		private void optSpecificDog_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDogs.Enabled = true;
			cboDogs.Enabled = true;
		}

		private void optSpecificOwner_CheckedChanged(object sender, System.EventArgs e)
		{
			fraOwners.Enabled = true;
			cboOwner.Enabled = true;
		}

		private void cmbSpecificDog_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbSpecificDog.Text == "All Dogs")
			{
				optAllDogs_CheckedChanged(sender, e);
			}
			else if (cmbSpecificDog.Text == "Specific Dog")
			{
				optSpecificDog_CheckedChanged(sender, e);
			}
		}

		private void cmbAllOwners_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAllOwners.Text == "All Owners")
			{
				optAllOwners_CheckedChanged(sender, e);
			}
			if (cmbAllOwners.Text == "Specific Owner")
			{
				optSpecificOwner_CheckedChanged(sender, e);
			}
		}
	}
}
