﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmPrintIntentions : BaseForm
	{
		public frmPrintIntentions()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintIntentions InstancePtr
		{
			get
			{
				return (frmPrintIntentions)Sys.GetInstance(typeof(frmPrintIntentions));
			}
		}

		protected frmPrintIntentions _InstancePtr = null;
		//=========================================================
		bool boolOk;
		const int CNSTPRINTINTENTIONS = 1;
		const int CNSTPRINTLICENSE = 0;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.gboolStateIntentions = false;
			modGNBas.Statics.gboolIssuanceIntentions = false;
			modGNBas.Statics.gboolMarriageIntentions = false;
			modGNBas.Statics.gboolNewMarriageForms = false;
			boolOk = false;
			Close();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			string strTemp = "";
			int x;
			for (x = 0; x <= 3; x++)
			{
				if (cmbNew.SelectedIndex == x)
				{
					strTemp = FCConvert.ToString(cmbNew.Tag);
					break;
				}
			}
			// x
			// If optNew(0).Value Then
			// strTemp = "2004"
			// ElseIf optNew(1).Value Then
			// strTemp = "2005"
			// ElseIf optNew(2).Value Then
			// strTemp = "2006"
			// End If
			modRegistry.SaveRegistryKey("MarriageLicenseType", strTemp, "CK");
			// Call SaveRegistryKey("MarriageLicenseHorizontalAdjustment", Val(txtHorizontal.Text), "CK")
			modGNBas.Statics.gboolStateIntentions = cmbeck1.Text == "State";
			modGNBas.Statics.gboolIssuanceIntentions = cmbeck1.Text == "Place of Issuance Copy";
			// gboolMarriageIntentions = Check1(2).Value = vbChecked
			if (cmbtion1.Text == "Certificate Information (Top of Marriage License form)")
			{
				modGNBas.Statics.gintMarriageConfirmation = 0;
			}
			else if (cmbtion1.Text == "Print Ceremony Section (Bottom of Marriage License form)")
			{
				modGNBas.Statics.gintMarriageConfirmation = 1;
			}
			else if (cmbtion1.Text == "Print Ceremony Section AND Certificate Information")
			{
				modGNBas.Statics.gintMarriageConfirmation = 2;
			}
			else if (cmbtion1.Text == "Print Intentions")
			{
				modGNBas.Statics.gintMarriageConfirmation = 3;
			}
			modGNBas.Statics.gboolNewMarriageForms = cmbNew.Text == "R 01/2005" || cmbNew.Text == "R 08/2006" || cmbNew.Text == "R 05/2010" || cmbNew.Text == "Word Doc";
			if (fecherFoundation.Strings.UCase(strTemp) == "2005")
			{
				modGNBas.Statics.gintMarriageForm = 1;
			}
			else if (fecherFoundation.Strings.UCase(strTemp) == "2006")
			{
				modGNBas.Statics.gintMarriageForm = 2;
			}
			else if (fecherFoundation.Strings.UCase(strTemp) == "2010")
			{
				modGNBas.Statics.gintMarriageForm = 3;
			}
			else if (fecherFoundation.Strings.UCase(strTemp) == "WORD")
			{
				modGNBas.Statics.gintMarriageForm = 4;
			}
			else
			{
				modGNBas.Statics.gintMarriageForm = 4;
			}
			// If optNew(1).Value Then
			// gintMarriageForm = 1
			// ElseIf optNew(2).Value Then
			// gintMarriageForm = 2
			// ElseIf optNew(3).Value Then
			// gintMarriageForm = 3
			// Else
			// gintMarriageForm = 0
			// End If
			if (chkSplitFee.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGNBas.Statics.gboolSplitMarriageFee = true;
			}
			else
			{
				modGNBas.Statics.gboolSplitMarriageFee = false;
			}
			modGNBas.Statics.typMarriageConsent.NeedConsent = (modGNBas.Statics.gintMarriageConfirmation == 0 || modGNBas.Statics.gintMarriageConfirmation == 2);
			modGNBas.Statics.gboolFullMarriageLicense = cmbtion1.Text == "Certificate Information (Top of Marriage License form)";
			if (cmbSafetyPaper.Text == "R0606")
			{
				modGNBas.Statics.gintSafetyPaperType = 606;
			}
			else
			{
				modGNBas.Statics.gintSafetyPaperType = 196;
			}
			boolOk = true;
			Close();
		}

		private void frmPrintIntentions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintIntentions properties;
			//frmPrintIntentions.ScaleWidth	= 5910;
			//frmPrintIntentions.ScaleHeight	= 4350;
			//frmPrintIntentions.LinkTopic	= "Form1";
			//frmPrintIntentions.LockControls	= -1  'True;
			//End Unmaped Properties
			string strTemp;
			strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("MarriageLicenseType", "CK"));
			int x;
			for (x = 0; x <= 3; x++)
			{
				if (fecherFoundation.Strings.UCase(FCConvert.ToString(cmbNew.Tag)) == fecherFoundation.Strings.UCase(strTemp))
				{
					cmbNew.SelectedIndex = x;
					break;
				}
			}
			// x
			// Select Case UCase(strTemp)
			// Case "2004"
			// optNew(0).Value = True
			// Case "2005"
			// optNew(1).Value = True
			// Case "2006"
			// Case "2010"
			// Case "WORD"
			// optNew(3).Value = True
			// Case Else
			// End Select
			// txtHorizontal.Text = Val(GetRegistryKey("MarriageLicenseHorizontalAdjustment", "CK"))
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this);
			if (modGNBas.Statics.gboolSplitMarriageFee)
			{
				chkSplitFee.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkSplitFee.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			// If bolFromWindowsCR Then
			// chkSplitFee.Visible = True
			// Else
			chkSplitFee.Visible = false;
			// End If
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				modGNBas.Statics.gboolStateIntentions = false;
				modGNBas.Statics.gboolIssuanceIntentions = false;
				modGNBas.Statics.gboolMarriageIntentions = false;
				modGNBas.Statics.gboolNewMarriageForms = false;
				boolOk = false;
			}
		}

		// vbPorter upgrade warning: lngPrintType As object	OnWriteFCConvert.ToInt32(
		public bool Init(int lngPrintType = 0)
		{
			bool Init = false;
			boolOk = true;
			if (FCConvert.ToInt32(lngPrintType) == CNSTPRINTINTENTIONS)
			{
				cmbtion1.Text = "Print Intentions";
			}
			this.Show(FCForm.FormShowEnum.Modal);
			Init = boolOk;
			return Init;
		}

		private void Option1_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// Dim x As Integer
			// For x = 0 To 3
			// If optNew(x).Tag = "2010" Then
			// If Index = 3 Then
			// optNew(x).Visible = True
			// Else
			// If optNew(x).Value = True Then
			// optNew(x - 1).Value = True
			// End If
			// optNew(x).Visible = False
			// End If
			// End If
			// Next x
		}

		private void Option1_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtion1.SelectedIndex;
			Option1_CheckedChanged(index, sender, e);
		}
	}
}
