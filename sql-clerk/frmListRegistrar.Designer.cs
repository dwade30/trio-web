//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmListRegistrar.
	/// </summary>
	partial class frmListRegistrar
	{
		public FCGrid vsClerks;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEdit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vsClerks = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEdit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdEditNames = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsClerks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditNames)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 576);
            this.BottomPanel.Size = new System.Drawing.Size(1008, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsClerks);
            this.ClientArea.Size = new System.Drawing.Size(1028, 630);
            this.ClientArea.Controls.SetChildIndex(this.vsClerks, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdEditNames);
            this.TopPanel.Size = new System.Drawing.Size(1028, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdEditNames, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(124, 30);
            this.HeaderText.Text = "Registrars";
            // 
            // vsClerks
            // 
            this.vsClerks.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsClerks.Cols = 3;
            this.vsClerks.ExtendLastCol = true;
            this.vsClerks.FixedCols = 0;
            this.vsClerks.Location = new System.Drawing.Point(30, 30);
            this.vsClerks.Name = "vsClerks";
            this.vsClerks.RowHeadersVisible = false;
            this.vsClerks.Rows = 1;
            this.vsClerks.Size = new System.Drawing.Size(987, 545);
            this.vsClerks.TabIndex = 1001;
            this.vsClerks.DoubleClick += new System.EventHandler(this.vsClerks_DblClick);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEdit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuEdit
            // 
            this.mnuEdit.Index = 0;
            this.mnuEdit.Name = "mnuEdit";
            this.mnuEdit.Text = "Edit Names";
            this.mnuEdit.Click += new System.EventHandler(this.mnuEdit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdEditNames
            // 
            this.cmdEditNames.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdEditNames.Location = new System.Drawing.Point(910, 29);
            this.cmdEditNames.Name = "cmdEditNames";
            this.cmdEditNames.Size = new System.Drawing.Size(90, 24);
            this.cmdEditNames.TabIndex = 1;
            this.cmdEditNames.Text = "Edit Names";
            this.cmdEditNames.Click += new System.EventHandler(this.mnuEdit_Click);
            // 
            // frmListRegistrar
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1028, 690);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmListRegistrar";
            this.Text = "Registrars";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmListRegistrar_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmListRegistrar_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmListRegistrar_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsClerks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditNames)).EndInit();
            this.ResumeLayout(false);

        }
		#endregion

		private FCButton cmdEditNames;
	}
}