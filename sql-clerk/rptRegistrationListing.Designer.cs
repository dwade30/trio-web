﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptRegistrationListing.
	/// </summary>
	partial class rptRegistrationListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRegistrationListing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DLG1 = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtColorBreed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTagNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDogName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRabiesDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRegYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtColorBreed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRabiesDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRegYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtColorBreed,
				this.txtOwnerName,
				this.txtTagNumber,
				this.txtDatePaid,
				this.txtAddress,
				this.txtDogName,
				this.txtRabiesDate,
				this.txtRegYear
			});
			this.Detail.Height = 0.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Field2,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field8,
				this.Field9,
				this.DLG1,
				this.txtCaption,
				this.txtCaption2,
				this.txtTown,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Field10
			});
			this.PageHeader.Height = 0.9166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field1.Text = "Reg Date";
			this.Field1.Top = 0.71875F;
			this.Field1.Width = 0.6666667F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 1.0625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field2.Text = "Name / Location";
			this.Field2.Top = 0.71875F;
			this.Field2.Width = 1.3125F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 2.4375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field4.Text = "Address / Phone";
			this.Field4.Top = 0.71875F;
			this.Field4.Width = 1.25F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 4.65625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field5.Text = "Color / Breed";
			this.Field5.Top = 0.71875F;
			this.Field5.Width = 0.875F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 6.625F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field6.Text = "Dog Name";
			this.Field6.Top = 0.71875F;
			this.Field6.Width = 0.8125F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 7.3125F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field8.Text = "Tag #";
			this.Field8.Top = 0.71875F;
			this.Field8.Width = 0.5F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.2708333F;
			this.Field9.Left = 5.614583F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field9.Text = "Rabies Expiration Date";
			this.Field9.Top = 0.5798611F;
			this.Field9.Width = 1F;
			// 
			// DLG1
			// 
			this.DLG1.Height = 0.3125F;
			this.DLG1.Left = 0F;
			this.DLG1.Name = "DLG1";
			this.DLG1.Top = 0F;
			this.DLG1.Visible = false;
			this.DLG1.Width = 0.875F;
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1666667F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = null;
			this.txtCaption.Top = 0.04166667F;
			this.txtCaption.Width = 7.71875F;
			// 
			// txtCaption2
			// 
			this.txtCaption2.Height = 0.1666667F;
			this.txtCaption2.Left = 0.09375F;
			this.txtCaption2.Name = "txtCaption2";
			this.txtCaption2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.txtCaption2.Text = null;
			this.txtCaption2.Top = 0.25F;
			this.txtCaption2.Width = 7.71875F;
			// 
			// txtTown
			// 
			this.txtTown.Height = 0.1666667F;
			this.txtTown.Left = 0.03125F;
			this.txtTown.Name = "txtTown";
			this.txtTown.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTown.Text = null;
			this.txtTown.Top = 0.04166667F;
			this.txtTown.Width = 1.375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0.03125F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 6.5F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 6.5F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.375F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.2708333F;
			this.Field10.Left = 0.625F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Field10.Text = "Reg Year";
			this.Field10.Top = 0.5833333F;
			this.Field10.Width = 0.40625F;
			// 
			// txtColorBreed
			// 
			this.txtColorBreed.Height = 0.2083333F;
			this.txtColorBreed.Left = 4.65625F;
			this.txtColorBreed.Name = "txtColorBreed";
			this.txtColorBreed.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtColorBreed.Text = null;
			this.txtColorBreed.Top = 0F;
			this.txtColorBreed.Width = 0.96875F;
			// 
			// txtOwnerName
			// 
			this.txtOwnerName.Height = 0.2083333F;
			this.txtOwnerName.Left = 1.0625F;
			this.txtOwnerName.Name = "txtOwnerName";
			this.txtOwnerName.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtOwnerName.Text = null;
			this.txtOwnerName.Top = 0F;
			this.txtOwnerName.Width = 1.34375F;
			// 
			// txtTagNumber
			// 
			this.txtTagNumber.Height = 0.2083333F;
			this.txtTagNumber.Left = 7.3125F;
			this.txtTagNumber.Name = "txtTagNumber";
			this.txtTagNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTagNumber.Text = null;
			this.txtTagNumber.Top = 0F;
			this.txtTagNumber.Width = 0.5625F;
			// 
			// txtDatePaid
			// 
			this.txtDatePaid.Height = 0.2083333F;
			this.txtDatePaid.Left = 0F;
			this.txtDatePaid.Name = "txtDatePaid";
			this.txtDatePaid.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtDatePaid.Text = null;
			this.txtDatePaid.Top = 0F;
			this.txtDatePaid.Width = 0.6666667F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.2083333F;
			this.txtAddress.Left = 2.4375F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 0F;
			this.txtAddress.Width = 2.229167F;
			// 
			// txtDogName
			// 
			this.txtDogName.Height = 0.2083333F;
			this.txtDogName.Left = 6.625F;
			this.txtDogName.Name = "txtDogName";
			this.txtDogName.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtDogName.Text = null;
			this.txtDogName.Top = 0F;
			this.txtDogName.Width = 0.65625F;
			// 
			// txtRabiesDate
			// 
			this.txtRabiesDate.Height = 0.2083333F;
			this.txtRabiesDate.Left = 5.65625F;
			this.txtRabiesDate.Name = "txtRabiesDate";
			this.txtRabiesDate.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtRabiesDate.Text = null;
			this.txtRabiesDate.Top = 0F;
			this.txtRabiesDate.Width = 0.875F;
			// 
			// txtRegYear
			// 
			this.txtRegYear.Height = 0.2083333F;
			this.txtRegYear.Left = 0.625F;
			this.txtRegYear.Name = "txtRegYear";
			this.txtRegYear.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center";
			this.txtRegYear.Text = null;
			this.txtRegYear.Top = 0F;
			this.txtRegYear.Width = 0.40625F;
			// 
			// rptRegistrationListing
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.2777778F;
			this.PageSettings.Margins.Right = 0.2777778F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.916667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtColorBreed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRabiesDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRegYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtColorBreed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePaid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRabiesDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRegYear;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl DLG1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
