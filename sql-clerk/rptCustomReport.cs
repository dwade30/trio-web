//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptCustomReport.
	/// </summary>
	public partial class rptCustomReport : BaseSectionReport
	{
		public rptCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Custom Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomReport InstancePtr
		{
			get
			{
				return (rptCustomReport)Sys.GetInstance(typeof(rptCustomReport));
			}
		}

		protected rptCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		int intPageNumber;
		FieldMessage[] strMessage = new FieldMessage[50 + 1];
		// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
		double dblSizeRatio;

		private struct FieldMessage
		{
			public string strMessage;
			public bool boolShown;
			public bool boolDontContinue;
		};

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			int intControl;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				// 
				for (intRow = 1; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
				{
					for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
					{
						for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
						{
							if (Detail.Controls[intControl].Name == "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol))
							{
								break;
							}
						}
						if (frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol) >= 0)
						{
							if (intCol > 0)
							{
								if (!FCUtils.IsEmpty(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))
								{
									if (FCUtils.IsEmpty(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))
									{
										// ADD THE DATA TO THE CORRECT CELL IN THE GRID
										if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
										{
											SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))), "MM/dd/yyyy"), intRow, intCol);
										}
										else
										{
											if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
											{
												SetCellWithData(intControl, string.Empty, intRow, intCol);
											}
											else
											{
												SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])))), intRow, intCol);
											}
										}
									}
									else
									{
										if (frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1) >= 0)
										{
											// this will check to see if to merge the columns
											// not sure about this change but we ran into the case where
											// the data in the first two fields were the same but the fields
											// were different so we did NOT want to merge the two fields.
											// If rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomReport.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))) = rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomReport.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))))) Then
											if (fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]) == fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))]))
											{
												if ((intControl + 2) > Detail.Controls.Count)
												{
													Detail.Controls[intControl].Width = Detail.Controls[intControl].Width;
												}
												else
												{
													Detail.Controls[intControl].Width += Detail.Controls[intControl + 1].Width;
												}
											}
											else
											{
												// ADD THE DATA TO THE CORRECT CELL IN THE GRID
												if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
												{
													// Call SetCellWithData(intControl, Format(rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomReport.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))), "MM/dd/yyyy"), intRow, intCol)
													SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])))), intRow, intCol);
												}
												else
												{
													if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
													{
														SetCellWithData(intControl, string.Empty, intRow, intCol);
													}
													else
													{
														SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])))), intRow, intCol);
													}
												}
											}
										}
										else
										{
											// ADD THE DATA TO THE CORRECT CELL IN THE GRID
											if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
											{
												SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))), "MM/dd/yyyy"), intRow, intCol);
											}
											else
											{
												if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
												{
													SetCellWithData(intControl, string.Empty, intRow, intCol);
												}
												else
												{
													SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])))), intRow, intCol);
												}
											}
										}
									}
								}
							}
							else
							{
								// ADD THE DATA TO THE CORRECT CELL IN THE GRID
								if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
								{
									SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))), "MM/dd/yyyy"), intRow, intCol);
								}
								else
								{
									if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
									{
										SetCellWithData(intControl, string.Empty, intRow, intCol);
									}
									else
									{
										SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])))), intRow, intCol);
									}
								}
							}
						}
						if (strMessage[intControl].boolDontContinue)
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
				rsData.MoveNext();
				eArgs.EOF = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// MsgBox "Unable to build custom report. Close custom report screen and start again.", vbInformation + vbOKOnly, "TRIO Software"
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In rptCustomReport_FetchData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				eArgs.EOF = true;
			}
		}

		private void SetCellWithData(int intCounter, string strData, int intRow, int intCol)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "LEGITIMATE")
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(!FCConvert.CBool(strData));
					// Detail.Controls[intCounter].Text = strData
					// If strMessage(intCounter).boolShown Then
					// Detail.Controls[intCounter].Text = Not CBool(strData)
					// Else
					// 
					// If (MsgBox("A BOW record is about to be shown. Continue with report?", vbQuestion + vbYesNo, "TRIO Software") = vbYes) Then
					// Detail.Controls[intCounter].Text = Not CBool(strData)
					// strMessage(intCounter).boolShown = True
					// strMessage(intCounter).boolDontContinue = False
					// Else
					// strMessage(intCounter).boolDontContinue = True
					// End If
					// End If
					// If UCase(strData) = "TRUE" Then
					// strData = "False"
					// Else
					// strData = "True"
					// End If
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "DATEOFDEATH")
				{
					if (Strings.Left(fecherFoundation.Strings.UCase(modGNBas.Statics.strPreSetReport) + "     ", 5) == "DEATH")
					{
						if (!Information.IsDate(rsData.Get_Fields("DATEOFDEATH")))
						{
							if (Information.IsDate(rsData.Get_Fields_String("DateOfDeathDescription")))
							{
								(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields_String("DateOfDeathDescription"), "MM/dd/yyyy");
							}
							else
							{
								(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							}
						}
						else
						{
							if (Convert.ToDateTime(rsData.Get_Fields("DATEOFDEATH")).ToOADate() != 0)
							{
								(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields("DateOfDeath"), "MM/dd/yyyy");
							}
							else
							{
								if (Information.IsDate(rsData.Get_Fields_String("DateOfDeathDescription")))
								{
									(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields_String("DateOfDeathDescription"), "MM/dd/yyyy");
								}
								else
								{
									(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
								}
							}
						}
					}
					else
					{
						(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strData;
					}
				}
				else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "TAGLICNUM")
				{
					string strTemp = "";
					if (!FCConvert.ToBoolean(rsData.Get_Fields_Boolean("kenneldog")))
					{
						(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strData;
					}
					else
					{
						strTemp = modGlobalRoutines.GetKennelLicenseByDog(rsData.Get_Fields_Int32("ID"));
						(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "K " + strTemp;
					}
				}
				else
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strData;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetCellWithData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			//dblSizeRatio = (frmCustomReport.InstancePtr.Line2.X2 - frmCustomReport.InstancePtr.Line2.X1) / 1440F;
            dblSizeRatio = 1;
            // it is most likely resized. See by how much
            intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			txtTitle.Text = modCustomReport.Statics.strCustomTitle;
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGNBas.DEFAULTCLERKDATABASE);
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			// Call SetPrintProperties(Me)
			if (frmCustomReport.InstancePtr.chkLandscape.CheckState == CheckState.Checked)
			{
				this.PrintWidth = 14200 / 1440F;
				this.Document.Printer.DefaultPageSettings.Landscape = true;
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				;
				// Me.Width = Me.PrintWidth
				txtDate.Left = this.PrintWidth - txtDate.Width;
				this.txtPage.Left = this.PrintWidth - txtPage.Width;
				this.txtTitle.Width = this.PrintWidth;
			}
			CreateHeaderFields();
			CreateDataFields();
		}

		private void CreateHeaderFields()
		{
			int intControlNumber = 0;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 0; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					intControlNumber += 1;
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.Name = "txtHeader" + FCConvert.ToString(intControlNumber);
					NewField.Top = ((intRow * frmCustomReport.InstancePtr.vsLayout.RowHeight(0)) + 1000) / 1440F;
					NewField.Left = FCConvert.ToSingle(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol) / dblSizeRatio) / 1440F;
					if (intCol > 0)
					{
						if (frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) == frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol - 1))
						{
							NewField.Text = string.Empty;
						}
						else
						{
							NewField.Text = frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
						}
					}
					else
					{
						NewField.Text = frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					NewField.Width = FCConvert.ToSingle(frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol) / dblSizeRatio) / 1440F;
					NewField.WordWrap = false;
					PageHeader.Controls.Add(NewField);
				}
			}
			// ADD A LINE SEPERATOR BETWEEN THE CAPTIONS AND THE DATA THAT
			// IS DISPLAYED
			Line1.X1 = 0;
			Line1.X2 = this.PrintWidth;
			Line1.Y1 = ((intRow * frmCustomReport.InstancePtr.vsLayout.RowHeight(0)) + 1000) / 1440F;
			Line1.Y2 = Line1.Y1;
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 1; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.Name = "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol);
					NewField.Top = ((intRow - 1) * frmCustomReport.InstancePtr.vsLayout.RowHeight(0) + 50) / 1440F;
					NewField.Left = (FCConvert.ToSingle(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol) / dblSizeRatio)) / 1440F;
					NewField.Width = FCConvert.ToSingle(frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol) / dblSizeRatio) / 1440F;
					NewField.Height = frmCustomReport.InstancePtr.vsLayout.RowHeight(1) / 1440F;
					NewField.Text = string.Empty;
					NewField.WordWrap = false;
					Detail.Controls.Add(NewField);
				}
			}
		}
		// Public Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports.DDTool)
		// Call VerifyPrintToFile(Me, Tool)
		// End Sub
		private void Detail_Format(object sender, EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= Detail.Controls.Count - 1; intCounter++)
			{
				if (FCConvert.ToString(Detail.Controls[intCounter].Tag) != string.Empty)
				{
					if (Strings.Left(Detail.Controls[intCounter].Name, 7) == "txtData")
					{
						if (Conversion.Val(Detail.Controls[intCounter].Tag) <= this.PrintWidth)
						{
							// MsgBox Val(Detail.Controls[intCounter].Tag)
							Detail.Controls[intCounter].Width = FCConvert.ToSingle(Detail.Controls[intCounter].Tag);
						}
						else
						{
							Detail.Controls[intCounter].Width = this.PrintWidth;
						}
					}
				}
			}
			Detail.Height = (frmCustomReport.InstancePtr.vsLayout.RowHeight(0) * (frmCustomReport.InstancePtr.vsLayout.Rows - 1)) / 1440F;
		}

		
	}
}
