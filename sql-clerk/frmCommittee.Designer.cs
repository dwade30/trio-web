//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmCommittee.
	/// </summary>
	partial class frmCommittee
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame fraExistingMembers;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public FCGrid vsMembers;
		public FCGrid gridDeletedMembers;
		public Global.T2KDateBox t2kFrom;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtUserDefined;
		public fecherFoundation.FCTextBox txtEMail;
		public Global.T2KPhoneNumberBox T2KPhone;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtTitle;
		public FCGrid GridGroup;
		public FCGrid GridGroupList;
		public Global.T2KDateBox t2kTo;
		public FCGrid GridDeletedGroups;
		public Global.T2KPhoneNumberBox t2kHomePhone;
		public Global.T2KPhoneNumberBox t2kCellPhone;
		public Global.T2KPhoneNumberBox t2kOtherPhone;
		public fecherFoundation.FCLabel Label1_15;
		public fecherFoundation.FCLabel Label1_14;
		public fecherFoundation.FCLabel Label1_13;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddGroup;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteGroup;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuAddMember;
		public fecherFoundation.FCToolStripMenuItem mnuAddExistingMember;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteMember;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuPrintRoster;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAll;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.fraExistingMembers = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOK = new fecherFoundation.FCButton();
            this.vsMembers = new fecherFoundation.FCGrid();
            this.gridDeletedMembers = new fecherFoundation.FCGrid();
            this.t2kFrom = new Global.T2KDateBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtUserDefined = new fecherFoundation.FCTextBox();
            this.txtEMail = new fecherFoundation.FCTextBox();
            this.T2KPhone = new Global.T2KPhoneNumberBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtName = new fecherFoundation.FCTextBox();
            this.txtTitle = new fecherFoundation.FCTextBox();
            this.GridGroup = new fecherFoundation.FCGrid();
            this.GridGroupList = new fecherFoundation.FCGrid();
            this.t2kTo = new Global.T2KDateBox();
            this.GridDeletedGroups = new fecherFoundation.FCGrid();
            this.t2kHomePhone = new Global.T2KPhoneNumberBox();
            this.t2kCellPhone = new Global.T2KPhoneNumberBox();
            this.t2kOtherPhone = new Global.T2KPhoneNumberBox();
            this.Label1_15 = new fecherFoundation.FCLabel();
            this.Label1_14 = new fecherFoundation.FCLabel();
            this.Label1_13 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuAddMember = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddExistingMember = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteMember = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintRoster = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintAll = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddGroup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteGroup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdDeleteGroup = new fecherFoundation.FCButton();
            this.cmdAddGroup = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraExistingMembers)).BeginInit();
            this.fraExistingMembers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMembers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDeletedMembers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroupList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeletedGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kHomePhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kCellPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kOtherPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(908, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.gridDeletedMembers);
            this.ClientArea.Controls.Add(this.t2kFrom);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.txtUserDefined);
            this.ClientArea.Controls.Add(this.txtEMail);
            this.ClientArea.Controls.Add(this.T2KPhone);
            this.ClientArea.Controls.Add(this.txtZip4);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.txtAddress2);
            this.ClientArea.Controls.Add(this.txtAddress1);
            this.ClientArea.Controls.Add(this.txtName);
            this.ClientArea.Controls.Add(this.txtTitle);
            this.ClientArea.Controls.Add(this.t2kTo);
            this.ClientArea.Controls.Add(this.GridDeletedGroups);
            this.ClientArea.Controls.Add(this.t2kHomePhone);
            this.ClientArea.Controls.Add(this.t2kCellPhone);
            this.ClientArea.Controls.Add(this.t2kOtherPhone);
            this.ClientArea.Controls.Add(this.Label1_15);
            this.ClientArea.Controls.Add(this.Label1_14);
            this.ClientArea.Controls.Add(this.Label1_13);
            this.ClientArea.Controls.Add(this.Label1_12);
            this.ClientArea.Controls.Add(this.Label1_11);
            this.ClientArea.Controls.Add(this.Label1_10);
            this.ClientArea.Controls.Add(this.Label1_9);
            this.ClientArea.Controls.Add(this.Label1_8);
            this.ClientArea.Controls.Add(this.Label1_7);
            this.ClientArea.Controls.Add(this.Label1_6);
            this.ClientArea.Controls.Add(this.Label1_5);
            this.ClientArea.Controls.Add(this.Label1_4);
            this.ClientArea.Controls.Add(this.Label1_3);
            this.ClientArea.Controls.Add(this.Label1_2);
            this.ClientArea.Controls.Add(this.Label1_1);
            this.ClientArea.Controls.Add(this.Label1_0);
            this.ClientArea.Controls.Add(this.GridGroupList);
            this.ClientArea.Controls.Add(this.GridGroup);
            this.ClientArea.Controls.Add(this.fraExistingMembers);
            this.ClientArea.Size = new System.Drawing.Size(908, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddGroup);
            this.TopPanel.Controls.Add(this.cmdDeleteGroup);
            this.TopPanel.Size = new System.Drawing.Size(908, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteGroup, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddGroup, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(245, 30);
            this.HeaderText.Text = "Groups / Committees";
            // 
            // fraExistingMembers
            // 
            this.fraExistingMembers.BackColor = System.Drawing.Color.White;
            this.fraExistingMembers.Controls.Add(this.cmdCancel);
            this.fraExistingMembers.Controls.Add(this.cmdOK);
            this.fraExistingMembers.Controls.Add(this.vsMembers);
            this.fraExistingMembers.Location = new System.Drawing.Point(30, 442);
            this.fraExistingMembers.Name = "fraExistingMembers";
            this.fraExistingMembers.Size = new System.Drawing.Size(850, 641);
            this.fraExistingMembers.TabIndex = 36;
            this.fraExistingMembers.Text = "Add Existing Member";
            this.fraExistingMembers.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(108, 581);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(95, 40);
            this.cmdCancel.TabIndex = 39;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.ToolTipText = null;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(20, 581);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(68, 40);
            this.cmdOK.TabIndex = 38;
            this.cmdOK.Text = "OK";
            this.cmdOK.ToolTipText = null;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // vsMembers
            // 
            this.vsMembers.AllowSelection = false;
            this.vsMembers.AllowUserToResizeColumns = false;
            this.vsMembers.AllowUserToResizeRows = false;
            this.vsMembers.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsMembers.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsMembers.BackColorBkg = System.Drawing.Color.Empty;
            this.vsMembers.BackColorFixed = System.Drawing.Color.Empty;
            this.vsMembers.BackColorSel = System.Drawing.Color.Empty;
            this.vsMembers.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsMembers.Cols = 6;
            dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsMembers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.vsMembers.ColumnHeadersHeight = 30;
            this.vsMembers.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsMembers.DefaultCellStyle = dataGridViewCellStyle10;
            this.vsMembers.DragIcon = null;
            this.vsMembers.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsMembers.FixedCols = 0;
            this.vsMembers.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsMembers.FrozenCols = 0;
            this.vsMembers.GridColor = System.Drawing.Color.Empty;
            this.vsMembers.GridColorFixed = System.Drawing.Color.Empty;
            this.vsMembers.Location = new System.Drawing.Point(20, 30);
            this.vsMembers.Name = "vsMembers";
            this.vsMembers.OutlineCol = 0;
            this.vsMembers.ReadOnly = true;
            this.vsMembers.RowHeadersVisible = false;
            this.vsMembers.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsMembers.RowHeightMin = 0;
            this.vsMembers.Rows = 1;
            this.vsMembers.ScrollTipText = null;
            this.vsMembers.ShowColumnVisibilityMenu = false;
            this.vsMembers.Size = new System.Drawing.Size(810, 531);
            this.vsMembers.StandardTab = true;
            this.vsMembers.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsMembers.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsMembers.TabIndex = 37;
            this.vsMembers.DoubleClick += new System.EventHandler(this.vsMembers_DblClick);
            // 
            // gridDeletedMembers
            // 
            this.gridDeletedMembers.AllowSelection = false;
            this.gridDeletedMembers.AllowUserToResizeColumns = false;
            this.gridDeletedMembers.AllowUserToResizeRows = false;
            this.gridDeletedMembers.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridDeletedMembers.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridDeletedMembers.BackColorBkg = System.Drawing.Color.Empty;
            this.gridDeletedMembers.BackColorFixed = System.Drawing.Color.Empty;
            this.gridDeletedMembers.BackColorSel = System.Drawing.Color.Empty;
            this.gridDeletedMembers.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridDeletedMembers.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridDeletedMembers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDeletedMembers.ColumnHeadersHeight = 30;
            this.gridDeletedMembers.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridDeletedMembers.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridDeletedMembers.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridDeletedMembers.DragIcon = null;
            this.gridDeletedMembers.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridDeletedMembers.FixedRows = 0;
            this.gridDeletedMembers.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridDeletedMembers.FrozenCols = 0;
            this.gridDeletedMembers.GridColor = System.Drawing.Color.Empty;
            this.gridDeletedMembers.GridColorFixed = System.Drawing.Color.Empty;
            this.gridDeletedMembers.Location = new System.Drawing.Point(587, 545);
            this.gridDeletedMembers.Name = "gridDeletedMembers";
            this.gridDeletedMembers.OutlineCol = 0;
            this.gridDeletedMembers.ReadOnly = true;
            this.gridDeletedMembers.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridDeletedMembers.RowHeightMin = 0;
            this.gridDeletedMembers.Rows = 0;
            this.gridDeletedMembers.ScrollTipText = null;
            this.gridDeletedMembers.ShowColumnVisibilityMenu = false;
            this.gridDeletedMembers.Size = new System.Drawing.Size(33, 26);
            this.gridDeletedMembers.StandardTab = true;
            this.gridDeletedMembers.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridDeletedMembers.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridDeletedMembers.TabIndex = 31;
            this.gridDeletedMembers.Visible = false;
            // 
            // t2kFrom
            // 
            this.t2kFrom.Location = new System.Drawing.Point(211, 992);
            this.t2kFrom.Mask = "00/00/0000";
            this.t2kFrom.Name = "t2kFrom";
            this.t2kFrom.SelLength = 0;
            this.t2kFrom.SelStart = 0;
            this.t2kFrom.Size = new System.Drawing.Size(115, 40);
            this.t2kFrom.TabIndex = 16;
            this.t2kFrom.Text = "  /  /";
            this.t2kFrom.ToolTipText = null;
            this.t2kFrom.TextChanged += new System.EventHandler(this.t2kFrom_Change);
            this.t2kFrom.Validating += new System.ComponentModel.CancelEventHandler(this.t2kFrom_Validate);
            // 
            // txtZip
            // 
            this.txtZip.AutoSize = false;
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.LinkItem = null;
            this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip.LinkTopic = null;
            this.txtZip.Location = new System.Drawing.Point(527, 642);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(100, 40);
            this.txtZip.TabIndex = 8;
            this.txtZip.ToolTipText = null;
            this.txtZip.Validating += new System.ComponentModel.CancelEventHandler(this.txtZip_Validating);
            // 
            // txtUserDefined
            // 
            this.txtUserDefined.AutoSize = false;
            this.txtUserDefined.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserDefined.LinkItem = null;
            this.txtUserDefined.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUserDefined.LinkTopic = null;
            this.txtUserDefined.Location = new System.Drawing.Point(211, 942);
            this.txtUserDefined.Name = "txtUserDefined";
            this.txtUserDefined.Size = new System.Drawing.Size(259, 40);
            this.txtUserDefined.TabIndex = 15;
            this.txtUserDefined.ToolTipText = null;
            this.txtUserDefined.Validating += new System.ComponentModel.CancelEventHandler(this.txtUserDefined_Validating);
            // 
            // txtEMail
            // 
            this.txtEMail.AutoSize = false;
            this.txtEMail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEMail.LinkItem = null;
            this.txtEMail.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEMail.LinkTopic = null;
            this.txtEMail.Location = new System.Drawing.Point(211, 892);
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.Size = new System.Drawing.Size(259, 40);
            this.txtEMail.TabIndex = 14;
            this.txtEMail.ToolTipText = null;
            this.txtEMail.Validating += new System.ComponentModel.CancelEventHandler(this.txtEMail_Validating);
            // 
            // T2KPhone
            // 
            this.T2KPhone.Location = new System.Drawing.Point(211, 692);
            this.T2KPhone.Mask = "(999)000-0000";
            this.T2KPhone.Name = "T2KPhone";
            this.T2KPhone.Size = new System.Drawing.Size(187, 40);
            this.T2KPhone.TabIndex = 10;
            this.T2KPhone.Text = "(   )   -";
            this.T2KPhone.KeyDown += new Wisej.Web.KeyEventHandler(this.T2KPhone_KeyDownEvent);
            this.T2KPhone.Validating += new System.ComponentModel.CancelEventHandler(this.T2KPhone_Validate);
            // 
            // txtZip4
            // 
            this.txtZip4.AutoSize = false;
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.LinkItem = null;
            this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip4.LinkTopic = null;
            this.txtZip4.Location = new System.Drawing.Point(638, 642);
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(80, 40);
            this.txtZip4.TabIndex = 9;
            this.txtZip4.ToolTipText = null;
            this.txtZip4.Validating += new System.ComponentModel.CancelEventHandler(this.txtZip4_Validating);
            // 
            // txtState
            // 
            this.txtState.AutoSize = false;
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.LinkItem = null;
            this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(400, 642);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(90, 40);
            this.txtState.TabIndex = 7;
            this.txtState.ToolTipText = null;
            this.txtState.Validating += new System.ComponentModel.CancelEventHandler(this.txtState_Validating);
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(211, 642);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(155, 40);
            this.txtCity.TabIndex = 6;
            this.txtCity.ToolTipText = null;
            this.txtCity.Validating += new System.ComponentModel.CancelEventHandler(this.txtCity_Validating);
            // 
            // txtAddress2
            // 
            this.txtAddress2.AutoSize = false;
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.LinkItem = null;
            this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress2.LinkTopic = null;
            this.txtAddress2.Location = new System.Drawing.Point(211, 592);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(259, 40);
            this.txtAddress2.TabIndex = 5;
            this.txtAddress2.ToolTipText = null;
            this.txtAddress2.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress2_Validating);
            // 
            // txtAddress1
            // 
            this.txtAddress1.AutoSize = false;
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.LinkItem = null;
            this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress1.LinkTopic = null;
            this.txtAddress1.Location = new System.Drawing.Point(211, 542);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(259, 40);
            this.txtAddress1.TabIndex = 4;
            this.txtAddress1.ToolTipText = null;
            this.txtAddress1.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress1_Validating);
            // 
            // txtName
            // 
            this.txtName.AutoSize = false;
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.LinkItem = null;
            this.txtName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtName.LinkTopic = null;
            this.txtName.Location = new System.Drawing.Point(211, 492);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(259, 40);
            this.txtName.TabIndex = 3;
            this.txtName.ToolTipText = null;
            this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txtName_Validating);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle.LinkItem = null;
            this.txtTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTitle.LinkTopic = null;
            this.txtTitle.Location = new System.Drawing.Point(211, 442);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(259, 40);
            this.txtTitle.TabIndex = 2;
            this.txtTitle.ToolTipText = null;
            this.txtTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // GridGroup
            // 
            this.GridGroup.AllowSelection = false;
            this.GridGroup.AllowUserToResizeColumns = false;
            this.GridGroup.AllowUserToResizeRows = false;
            this.GridGroup.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridGroup.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridGroup.BackColorBkg = System.Drawing.Color.Empty;
            this.GridGroup.BackColorFixed = System.Drawing.Color.Empty;
            this.GridGroup.BackColorSel = System.Drawing.Color.Empty;
            this.GridGroup.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridGroup.Cols = 10;
            dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridGroup.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.GridGroup.ColumnHeadersHeight = 30;
            this.GridGroup.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridGroup.DefaultCellStyle = dataGridViewCellStyle8;
            this.GridGroup.DragIcon = null;
            this.GridGroup.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridGroup.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridGroup.FrozenCols = 0;
            this.GridGroup.GridColor = System.Drawing.Color.Empty;
            this.GridGroup.GridColorFixed = System.Drawing.Color.Empty;
            this.GridGroup.Location = new System.Drawing.Point(30, 236);
            this.GridGroup.Name = "GridGroup";
            this.GridGroup.OutlineCol = 0;
            this.GridGroup.ReadOnly = true;
            this.GridGroup.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridGroup.RowHeightMin = 0;
            this.GridGroup.Rows = 50;
            this.GridGroup.ScrollTipText = null;
            this.GridGroup.ShowColumnVisibilityMenu = false;
            this.GridGroup.Size = new System.Drawing.Size(528, 196);
            this.GridGroup.StandardTab = true;
            this.GridGroup.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridGroup.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridGroup.TabIndex = 1;
            this.GridGroup.CurrentCellChanged += new System.EventHandler(this.GridGroup_RowColChange);
            // 
            // GridGroupList
            // 
            this.GridGroupList.AllowSelection = false;
            this.GridGroupList.AllowUserToResizeColumns = false;
            this.GridGroupList.AllowUserToResizeRows = false;
            this.GridGroupList.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridGroupList.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridGroupList.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridGroupList.BackColorBkg = System.Drawing.Color.Empty;
            this.GridGroupList.BackColorFixed = System.Drawing.Color.Empty;
            this.GridGroupList.BackColorSel = System.Drawing.Color.Empty;
            this.GridGroupList.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridGroupList.Cols = 10;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridGroupList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.GridGroupList.ColumnHeadersHeight = 30;
            this.GridGroupList.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridGroupList.DefaultCellStyle = dataGridViewCellStyle6;
            this.GridGroupList.DragIcon = null;
            this.GridGroupList.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridGroupList.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridGroupList.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridGroupList.FrozenCols = 0;
            this.GridGroupList.GridColor = System.Drawing.Color.Empty;
            this.GridGroupList.GridColorFixed = System.Drawing.Color.Empty;
            this.GridGroupList.Location = new System.Drawing.Point(30, 30);
            this.GridGroupList.Name = "GridGroupList";
            this.GridGroupList.OutlineCol = 0;
            this.GridGroupList.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridGroupList.RowHeightMin = 0;
            this.GridGroupList.Rows = 50;
            this.GridGroupList.ScrollTipText = null;
            this.GridGroupList.ShowColumnVisibilityMenu = false;
            this.GridGroupList.Size = new System.Drawing.Size(816, 196);
            this.GridGroupList.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridGroupList.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridGroupList.TabIndex = 0;
            this.GridGroupList.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridGroupList_ValidateEdit);
            this.GridGroupList.CurrentCellChanged += new System.EventHandler(this.GridGroupList_RowColChange);
            // 
            // t2kTo
            // 
            this.t2kTo.Location = new System.Drawing.Point(211, 1042);
            this.t2kTo.Mask = "00/00/0000";
            this.t2kTo.Name = "t2kTo";
            this.t2kTo.SelLength = 0;
            this.t2kTo.SelStart = 0;
            this.t2kTo.Size = new System.Drawing.Size(115, 40);
            this.t2kTo.TabIndex = 17;
            this.t2kTo.Text = "  /  /";
            this.t2kTo.ToolTipText = null;
            this.t2kTo.TextChanged += new System.EventHandler(this.t2kTo_Change);
            this.t2kTo.Validating += new System.ComponentModel.CancelEventHandler(this.t2kTo_Validate);
            // 
            // GridDeletedGroups
            // 
            this.GridDeletedGroups.AllowSelection = false;
            this.GridDeletedGroups.AllowUserToResizeColumns = false;
            this.GridDeletedGroups.AllowUserToResizeRows = false;
            this.GridDeletedGroups.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridDeletedGroups.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridDeletedGroups.BackColorBkg = System.Drawing.Color.Empty;
            this.GridDeletedGroups.BackColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedGroups.BackColorSel = System.Drawing.Color.Empty;
            this.GridDeletedGroups.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridDeletedGroups.Cols = 2;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridDeletedGroups.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridDeletedGroups.ColumnHeadersHeight = 30;
            this.GridDeletedGroups.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridDeletedGroups.ColumnHeadersVisible = false;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridDeletedGroups.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridDeletedGroups.DragIcon = null;
            this.GridDeletedGroups.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridDeletedGroups.FixedRows = 0;
            this.GridDeletedGroups.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedGroups.FrozenCols = 0;
            this.GridDeletedGroups.GridColor = System.Drawing.Color.Empty;
            this.GridDeletedGroups.GridColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedGroups.Location = new System.Drawing.Point(587, 537);
            this.GridDeletedGroups.Name = "GridDeletedGroups";
            this.GridDeletedGroups.OutlineCol = 0;
            this.GridDeletedGroups.ReadOnly = true;
            this.GridDeletedGroups.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridDeletedGroups.RowHeightMin = 0;
            this.GridDeletedGroups.Rows = 0;
            this.GridDeletedGroups.ScrollTipText = null;
            this.GridDeletedGroups.ShowColumnVisibilityMenu = false;
            this.GridDeletedGroups.Size = new System.Drawing.Size(33, 26);
            this.GridDeletedGroups.StandardTab = true;
            this.GridDeletedGroups.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridDeletedGroups.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridDeletedGroups.TabIndex = 32;
            this.GridDeletedGroups.Visible = false;
            // 
            // t2kHomePhone
            // 
            this.t2kHomePhone.Location = new System.Drawing.Point(211, 742);
            this.t2kHomePhone.Mask = "(999)000-0000";
            this.t2kHomePhone.Name = "t2kHomePhone";
            this.t2kHomePhone.Size = new System.Drawing.Size(187, 40);
            this.t2kHomePhone.TabIndex = 11;
            this.t2kHomePhone.Text = "(   )   -";
            this.t2kHomePhone.KeyDown += new Wisej.Web.KeyEventHandler(this.t2kHomePhone_KeyDownEvent);
            this.t2kHomePhone.Validating += new System.ComponentModel.CancelEventHandler(this.t2kHomePhone_Validate);
            // 
            // t2kCellPhone
            // 
            this.t2kCellPhone.Location = new System.Drawing.Point(211, 792);
            this.t2kCellPhone.Mask = "(999)000-0000";
            this.t2kCellPhone.Name = "t2kCellPhone";
            this.t2kCellPhone.Size = new System.Drawing.Size(187, 40);
            this.t2kCellPhone.TabIndex = 12;
            this.t2kCellPhone.Text = "(   )   -";
            this.t2kCellPhone.KeyDown += new Wisej.Web.KeyEventHandler(this.t2kCellPhone_KeyDownEvent);
            this.t2kCellPhone.Validating += new System.ComponentModel.CancelEventHandler(this.t2kCellPhone_Validate);
            // 
            // t2kOtherPhone
            // 
            this.t2kOtherPhone.Location = new System.Drawing.Point(211, 842);
            this.t2kOtherPhone.Mask = "(999)000-0000";
            this.t2kOtherPhone.Name = "t2kOtherPhone";
            this.t2kOtherPhone.Size = new System.Drawing.Size(187, 40);
            this.t2kOtherPhone.TabIndex = 13;
            this.t2kOtherPhone.Text = "(   )   -";
            this.t2kOtherPhone.KeyDown += new Wisej.Web.KeyEventHandler(this.t2kOtherPhone_KeyDownEvent);
            this.t2kOtherPhone.Validating += new System.ComponentModel.CancelEventHandler(this.t2kOtherPhone_Validate);
            // 
            // Label1_15
            // 
            this.Label1_15.Location = new System.Drawing.Point(30, 858);
            this.Label1_15.Name = "Label1_15";
            this.Label1_15.Size = new System.Drawing.Size(90, 18);
            this.Label1_15.TabIndex = 35;
            this.Label1_15.Text = "OTHER PHONE";
            this.Label1_15.ToolTipText = null;
            // 
            // Label1_14
            // 
            this.Label1_14.Location = new System.Drawing.Point(30, 806);
            this.Label1_14.Name = "Label1_14";
            this.Label1_14.Size = new System.Drawing.Size(74, 18);
            this.Label1_14.TabIndex = 34;
            this.Label1_14.Text = "CELL PHONE";
            this.Label1_14.ToolTipText = null;
            // 
            // Label1_13
            // 
            this.Label1_13.Location = new System.Drawing.Point(30, 756);
            this.Label1_13.Name = "Label1_13";
            this.Label1_13.Size = new System.Drawing.Size(85, 18);
            this.Label1_13.TabIndex = 33;
            this.Label1_13.Text = "HOME PHONE";
            this.Label1_13.ToolTipText = null;
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(145, 1056);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(41, 18);
            this.Label1_12.TabIndex = 30;
            this.Label1_12.Text = "TO";
            this.Label1_12.ToolTipText = null;
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(145, 1006);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(41, 18);
            this.Label1_11.TabIndex = 29;
            this.Label1_11.Text = "FROM";
            this.Label1_11.ToolTipText = null;
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(30, 1006);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(41, 18);
            this.Label1_10.TabIndex = 28;
            this.Label1_10.Text = "TERM";
            this.Label1_10.ToolTipText = null;
            // 
            // Label1_9
            // 
            this.Label1_9.Location = new System.Drawing.Point(500, 658);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(25, 18);
            this.Label1_9.TabIndex = 27;
            this.Label1_9.Text = "ZIP";
            this.Label1_9.ToolTipText = null;
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(377, 658);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(17, 18);
            this.Label1_8.TabIndex = 26;
            this.Label1_8.Text = "ST";
            this.Label1_8.ToolTipText = null;
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(30, 956);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(175, 18);
            this.Label1_7.TabIndex = 25;
            this.Label1_7.Text = "USER DEFINED";
            this.Label1_7.ToolTipText = null;
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(30, 906);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(74, 18);
            this.Label1_6.TabIndex = 24;
            this.Label1_6.Text = "E-MAIL";
            this.Label1_6.ToolTipText = null;
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(30, 706);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(85, 18);
            this.Label1_5.TabIndex = 23;
            this.Label1_5.Text = "WORK PHONE";
            this.Label1_5.ToolTipText = null;
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(30, 658);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(74, 18);
            this.Label1_4.TabIndex = 22;
            this.Label1_4.Text = "CITY";
            this.Label1_4.ToolTipText = null;
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(30, 606);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(74, 18);
            this.Label1_3.TabIndex = 21;
            this.Label1_3.Text = "ADDRESS 2";
            this.Label1_3.ToolTipText = null;
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(30, 556);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(74, 18);
            this.Label1_2.TabIndex = 20;
            this.Label1_2.Text = "ADDRESS 1";
            this.Label1_2.ToolTipText = null;
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(30, 506);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(74, 18);
            this.Label1_1.TabIndex = 19;
            this.Label1_1.Text = "NAME";
            this.Label1_1.ToolTipText = null;
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(30, 456);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(74, 18);
            this.Label1_0.TabIndex = 18;
            this.Label1_0.Text = "TITLE";
            this.Label1_0.ToolTipText = null;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddMember,
            this.mnuAddExistingMember,
            this.mnuDeleteMember,
            this.mnuPrintRoster,
            this.mnuPrintAll});
            this.MainMenu1.Name = null;
            // 
            // mnuAddMember
            // 
            this.mnuAddMember.Index = 0;
            this.mnuAddMember.Name = "mnuAddMember";
            this.mnuAddMember.Text = "Add New Member";
            this.mnuAddMember.Click += new System.EventHandler(this.mnuAddMember_Click);
            // 
            // mnuAddExistingMember
            // 
            this.mnuAddExistingMember.Index = 1;
            this.mnuAddExistingMember.Name = "mnuAddExistingMember";
            this.mnuAddExistingMember.Text = "Add Existing Member";
            this.mnuAddExistingMember.Click += new System.EventHandler(this.mnuAddExistingMember_Click);
            // 
            // mnuDeleteMember
            // 
            this.mnuDeleteMember.Index = 2;
            this.mnuDeleteMember.Name = "mnuDeleteMember";
            this.mnuDeleteMember.Text = "Delete Member";
            this.mnuDeleteMember.Click += new System.EventHandler(this.mnuDeleteMember_Click);
            // 
            // mnuPrintRoster
            // 
            this.mnuPrintRoster.Index = 3;
            this.mnuPrintRoster.Name = "mnuPrintRoster";
            this.mnuPrintRoster.Text = "Print Group Information";
            this.mnuPrintRoster.Click += new System.EventHandler(this.mnuPrintRoster_Click);
            // 
            // mnuPrintAll
            // 
            this.mnuPrintAll.Index = 4;
            this.mnuPrintAll.Name = "mnuPrintAll";
            this.mnuPrintAll.Text = "Print All Groups";
            this.mnuPrintAll.Click += new System.EventHandler(this.mnuPrintAll_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSepar3,
            this.mnuSepar4});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 0;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = 1;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuAddGroup
            // 
            this.mnuAddGroup.Index = -1;
            this.mnuAddGroup.Name = "mnuAddGroup";
            this.mnuAddGroup.Text = "Add Group/Committee";
            this.mnuAddGroup.Click += new System.EventHandler(this.mnuAddGroup_Click);
            // 
            // mnuDeleteGroup
            // 
            this.mnuDeleteGroup.Index = -1;
            this.mnuDeleteGroup.Name = "mnuDeleteGroup";
            this.mnuDeleteGroup.Text = "Delete Group/Committee";
            this.mnuDeleteGroup.Click += new System.EventHandler(this.mnuDeleteGroup_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(382, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.ToolTipText = null;
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdDeleteGroup
            // 
            this.cmdDeleteGroup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteGroup.AppearanceKey = "toolbarButton";
            this.cmdDeleteGroup.Location = new System.Drawing.Point(705, 29);
            this.cmdDeleteGroup.Name = "cmdDeleteGroup";
            this.cmdDeleteGroup.Size = new System.Drawing.Size(174, 24);
            this.cmdDeleteGroup.TabIndex = 1;
            this.cmdDeleteGroup.Text = "Delete Group/Committee";
            this.cmdDeleteGroup.ToolTipText = null;
            this.cmdDeleteGroup.Click += new System.EventHandler(this.mnuDeleteGroup_Click);
            // 
            // cmdAddGroup
            // 
            this.cmdAddGroup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddGroup.AppearanceKey = "toolbarButton";
            this.cmdAddGroup.Location = new System.Drawing.Point(541, 29);
            this.cmdAddGroup.Name = "cmdAddGroup";
            this.cmdAddGroup.Size = new System.Drawing.Size(158, 24);
            this.cmdAddGroup.TabIndex = 2;
            this.cmdAddGroup.Text = "Add Group/Committee";
            this.cmdAddGroup.ToolTipText = null;
            this.cmdAddGroup.Click += new System.EventHandler(this.mnuAddGroup_Click);
            // 
            // frmCommittee
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(908, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmCommittee";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Groups / Committees";
            this.Load += new System.EventHandler(this.frmCommittee_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCommittee_KeyDown);
            this.Resize += new System.EventHandler(this.frmCommittee_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraExistingMembers)).EndInit();
            this.fraExistingMembers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMembers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDeletedMembers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroupList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeletedGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kHomePhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kCellPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kOtherPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddGroup)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
        private FCButton cmdAddGroup;
        private FCButton cmdDeleteGroup;
    }
}