﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogAudit.
	/// </summary>
	partial class rptDogAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDogAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUserFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTransactionDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClerk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUserFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDogs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDetail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalClerk = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalLate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalUserFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUserFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalClerk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalUserFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAmount,
				this.txtOwnerName,
				this.txtTransactionDate,
				this.txtDescription,
				this.txtState,
				this.txtTown,
				this.txtClerk,
				this.txtLate,
				this.txtUserFee,
				this.Label10,
				this.txtDogs,
				this.Label11,
				this.txtDetail,
				this.Label12,
				this.txtUser,
				this.fldTag
			});
			this.Detail.Height = 0.46875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalAmount,
				this.txtTotalState,
				this.txtTotalTown,
				this.txtTotalClerk,
				this.txtTotalLate,
				this.txtTotalUserFee,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.lblUser,
				this.Line1
			});
			this.ReportFooter.Height = 0.625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDate,
				this.txtPage,
				this.txtMuni,
				this.txtTime,
				this.Field1,
				this.txtRange,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label9,
				this.lblUserFee,
				this.Label19
			});
			this.PageHeader.Height = 0.8645833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 6.25F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0.08333334F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 6.25F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field2";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.25F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1666667F;
			this.txtMuni.Left = 0.08333334F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Text = "Field1";
			this.txtMuni.Top = 0.08333334F;
			this.txtMuni.Width = 2.5F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0.08333334F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 2F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.25F;
			this.Field1.Left = 2.5F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Audit of Dog Transactions";
			this.Field1.Top = 0.08333334F;
			this.Field1.Width = 2.5F;
			// 
			// txtRange
			// 
			this.txtRange.Height = 0.1666667F;
			this.txtRange.Left = 2.083333F;
			this.txtRange.Name = "txtRange";
			this.txtRange.Style = "text-align: center";
			this.txtRange.Text = "Field2";
			this.txtRange.Top = 0.3333333F;
			this.txtRange.Width = 3.333333F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label1.Text = "Date";
			this.Label1.Top = 0.6666667F;
			this.Label1.Width = 0.75F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.78125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label2.Text = "Owner";
			this.Label2.Top = 0.65625F;
			this.Label2.Width = 1.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.15625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label3.Text = "Amount";
			this.Label3.Top = 0.65625F;
			this.Label3.Width = 0.59375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.90625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label4.Text = "State";
			this.Label4.Top = 0.65625F;
			this.Label4.Width = 0.40625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.40625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label5.Text = "Town";
			this.Label5.Top = 0.65625F;
			this.Label5.Width = 0.40625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label6.Text = "Clerk";
			this.Label6.Top = 0.65625F;
			this.Label6.Width = 0.40625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.375F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label7.Text = "Late";
			this.Label7.Top = 0.65625F;
			this.Label7.Width = 0.40625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.84375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label9.Text = "Description";
			this.Label9.Top = 0.65625F;
			this.Label9.Width = 1.03125F;
			// 
			// lblUserFee
			// 
			this.lblUserFee.CanShrink = true;
			this.lblUserFee.Height = 0.15625F;
			this.lblUserFee.Left = 6.875F;
			this.lblUserFee.Name = "lblUserFee";
			this.lblUserFee.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblUserFee.Text = "User Fee";
			this.lblUserFee.Top = 0.65625F;
			this.lblUserFee.Width = 0.625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2.59375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label19.Text = "Tag #";
			this.Label19.Top = 0.65625F;
			this.Label19.Width = 0.53125F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.15625F;
			this.txtAmount.Left = 3.15625F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0.09375F;
			this.txtAmount.Width = 0.59375F;
			// 
			// txtOwnerName
			// 
			this.txtOwnerName.Height = 0.15625F;
			this.txtOwnerName.Left = 0.78125F;
			this.txtOwnerName.Name = "txtOwnerName";
			this.txtOwnerName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtOwnerName.Text = null;
			this.txtOwnerName.Top = 0.09375F;
			this.txtOwnerName.Width = 1.75F;
			// 
			// txtTransactionDate
			// 
			this.txtTransactionDate.Height = 0.1666667F;
			this.txtTransactionDate.Left = 0F;
			this.txtTransactionDate.Name = "txtTransactionDate";
			this.txtTransactionDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTransactionDate.Text = null;
			this.txtTransactionDate.Top = 0.08333334F;
			this.txtTransactionDate.Width = 0.75F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.15625F;
			this.txtDescription.Left = 3.84375F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.09375F;
			this.txtDescription.Width = 1.03125F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.15625F;
			this.txtState.Left = 4.90625F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtState.Text = null;
			this.txtState.Top = 0.09375F;
			this.txtState.Width = 0.40625F;
			// 
			// txtTown
			// 
			this.txtTown.Height = 0.15625F;
			this.txtTown.Left = 5.375F;
			this.txtTown.Name = "txtTown";
			this.txtTown.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTown.Text = null;
			this.txtTown.Top = 0.09375F;
			this.txtTown.Width = 0.40625F;
			// 
			// txtClerk
			// 
			this.txtClerk.Height = 0.15625F;
			this.txtClerk.Left = 5.875F;
			this.txtClerk.Name = "txtClerk";
			this.txtClerk.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtClerk.Text = null;
			this.txtClerk.Top = 0.09375F;
			this.txtClerk.Width = 0.40625F;
			// 
			// txtLate
			// 
			this.txtLate.Height = 0.15625F;
			this.txtLate.Left = 6.375F;
			this.txtLate.Name = "txtLate";
			this.txtLate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLate.Text = null;
			this.txtLate.Top = 0.09375F;
			this.txtLate.Width = 0.40625F;
			// 
			// txtUserFee
			// 
			this.txtUserFee.Height = 0.15625F;
			this.txtUserFee.Left = 6.875F;
			this.txtUserFee.Name = "txtUserFee";
			this.txtUserFee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtUserFee.Text = null;
			this.txtUserFee.Top = 0.09375F;
			this.txtUserFee.Width = 0.625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.583333F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label10.Text = "Dog(s)";
			this.Label10.Top = 0.25F;
			this.Label10.Width = 0.5F;
			// 
			// txtDogs
			// 
			this.txtDogs.CanShrink = true;
			this.txtDogs.Height = 0.1666667F;
			this.txtDogs.Left = 2.166667F;
			this.txtDogs.Name = "txtDogs";
			this.txtDogs.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDogs.Text = null;
			this.txtDogs.Top = 0.25F;
			this.txtDogs.Width = 2.833333F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.083333F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label11.Text = "Detail";
			this.Label11.Top = 0.25F;
			this.Label11.Width = 0.4166667F;
			// 
			// txtDetail
			// 
			this.txtDetail.CanShrink = true;
			this.txtDetail.Height = 0.1666667F;
			this.txtDetail.Left = 5.5F;
			this.txtDetail.Name = "txtDetail";
			this.txtDetail.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDetail.Text = null;
			this.txtDetail.Top = 0.25F;
			this.txtDetail.Width = 2F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.1666667F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Label12.Text = "UserID";
			this.Label12.Top = 0.25F;
			this.Label12.Width = 0.5F;
			// 
			// txtUser
			// 
			this.txtUser.CanShrink = true;
			this.txtUser.Height = 0.1666667F;
			this.txtUser.Left = 0.6666667F;
			this.txtUser.Name = "txtUser";
			this.txtUser.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtUser.Text = null;
			this.txtUser.Top = 0.25F;
			this.txtUser.Width = 0.9166667F;
			// 
			// fldTag
			// 
			this.fldTag.Height = 0.15625F;
			this.fldTag.Left = 2.5625F;
			this.fldTag.Name = "fldTag";
			this.fldTag.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.fldTag.Text = null;
			this.fldTag.Top = 0.09375F;
			this.fldTag.Width = 0.53125F;
			// 
			// txtTotalAmount
			// 
			this.txtTotalAmount.Height = 0.1666667F;
			this.txtTotalAmount.Left = 2.25F;
			this.txtTotalAmount.Name = "txtTotalAmount";
			this.txtTotalAmount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalAmount.Text = null;
			this.txtTotalAmount.Top = 0.4166667F;
			this.txtTotalAmount.Width = 1F;
			// 
			// txtTotalState
			// 
			this.txtTotalState.Height = 0.1666667F;
			this.txtTotalState.Left = 3.5F;
			this.txtTotalState.Name = "txtTotalState";
			this.txtTotalState.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalState.Text = null;
			this.txtTotalState.Top = 0.4166667F;
			this.txtTotalState.Width = 0.75F;
			// 
			// txtTotalTown
			// 
			this.txtTotalTown.Height = 0.1666667F;
			this.txtTotalTown.Left = 4.333333F;
			this.txtTotalTown.Name = "txtTotalTown";
			this.txtTotalTown.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalTown.Text = null;
			this.txtTotalTown.Top = 0.4166667F;
			this.txtTotalTown.Width = 0.75F;
			// 
			// txtTotalClerk
			// 
			this.txtTotalClerk.Height = 0.1666667F;
			this.txtTotalClerk.Left = 5.166667F;
			this.txtTotalClerk.Name = "txtTotalClerk";
			this.txtTotalClerk.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalClerk.Text = null;
			this.txtTotalClerk.Top = 0.4166667F;
			this.txtTotalClerk.Width = 0.75F;
			// 
			// txtTotalLate
			// 
			this.txtTotalLate.Height = 0.1666667F;
			this.txtTotalLate.Left = 6F;
			this.txtTotalLate.Name = "txtTotalLate";
			this.txtTotalLate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalLate.Text = null;
			this.txtTotalLate.Top = 0.4166667F;
			this.txtTotalLate.Width = 0.6666667F;
			// 
			// txtTotalUserFee
			// 
			this.txtTotalUserFee.Height = 0.1666667F;
			this.txtTotalUserFee.Left = 6.75F;
			this.txtTotalUserFee.Name = "txtTotalUserFee";
			this.txtTotalUserFee.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtTotalUserFee.Text = null;
			this.txtTotalUserFee.Top = 0.4166667F;
			this.txtTotalUserFee.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.5F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label13.Text = "Total";
			this.Label13.Top = 0.4166667F;
			this.Label13.Width = 0.5F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.666667F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label14.Text = "Amount";
			this.Label14.Top = 0.25F;
			this.Label14.Width = 0.5833333F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1666667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.833333F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label15.Text = "State";
			this.Label15.Top = 0.25F;
			this.Label15.Width = 0.4166667F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1666667F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 4.666667F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label16.Text = "Town";
			this.Label16.Top = 0.25F;
			this.Label16.Width = 0.4166667F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1666667F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 5.5F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label17.Text = "Clerk";
			this.Label17.Top = 0.25F;
			this.Label17.Width = 0.4166667F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1666667F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 6.25F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label18.Text = "Late";
			this.Label18.Top = 0.25F;
			this.Label18.Width = 0.4166667F;
			// 
			// lblUser
			// 
			this.lblUser.CanShrink = true;
			this.lblUser.Height = 0.1666667F;
			this.lblUser.Left = 6.75F;
			this.lblUser.Name = "lblUser";
			this.lblUser.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblUser.Text = "User Fee";
			this.lblUser.Top = 0.25F;
			this.lblUser.Width = 0.75F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.25F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.08333334F;
			this.Line1.Width = 5.25F;
			this.Line1.X1 = 2.25F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.08333334F;
			this.Line1.Y2 = 0.08333334F;
			// 
			// rptDogAudit
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUserFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClerk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalClerk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalUserFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransactionDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClerk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUserFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogs;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDetail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUser;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTag;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalClerk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalUserFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblUser;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblUserFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
