//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmSelectTownStateCounty : BaseForm
	{
		public frmSelectTownStateCounty()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSelectTownStateCounty InstancePtr
		{
			get
			{
				return (frmSelectTownStateCounty)Sys.GetInstance(typeof(frmSelectTownStateCounty));
			}
		}

		protected frmSelectTownStateCounty _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public string strType = string.Empty;
		string strReturn;

		private void frmSelectTownStateCounty_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
			{
				return;
			}
			this.Refresh();
			cboChoices.SelectedIndex = 0;
		}

		private void frmSelectTownStateCounty_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectTownStateCounty properties;
			//frmSelectTownStateCounty.FillStyle	= 0;
			//frmSelectTownStateCounty.ScaleWidth	= 3885;
			//frmSelectTownStateCounty.ScaleHeight	= 1995;
			//frmSelectTownStateCounty.LinkTopic	= "Form2";
			//frmSelectTownStateCounty.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			if (strType == "T")
			{
				this.Text = "Select Town";
				LoadTowns();
			}
			else if (strType == "C")
			{
				this.Text = "Select County";
				LoadCounties();
			}
			else if (strType == "S")
			{
				this.Text = "Select State";
				LoadStates();
			}
		}

		private void frmSelectTownStateCounty_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				// strReturnString = ""
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public string Init(string strInfoType, string strInitial)
		{
			string Init = "";
			strReturn = strInitial;
			Init = strInitial;
			strType = strInfoType;
			modClerkGeneral.Statics.strReturnString = strInitial;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			// strReturnString = ""
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			modClerkGeneral.Statics.strReturnString = cboChoices.Text;
			strReturn = cboChoices.Text;
			Close();
		}

		private void LoadTowns()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM DefaultTowns ORDER BY Name", "TWCK0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				cboChoices.Clear();
				do
				{
					// cboChoices.AddItem StrConv(rsInfo.Fields("Name"), vbProperCase)
					cboChoices.AddItem(rsInfo.Get_Fields_String("Name"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadCounties()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM DefaultCounties ORDER BY Name", "TWCK0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				cboChoices.Clear();
				do
				{
					// cboChoices.AddItem StrConv(rsInfo.Fields("Name"), vbProperCase)
					cboChoices.AddItem(rsInfo.Get_Fields_String("Name"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadStates()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM States ORDER BY Description", "TWCK0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				cboChoices.Clear();
				do
				{
					// cboChoices.AddItem StrConv(rsInfo.Fields("Description"), vbProperCase)
					cboChoices.AddItem(rsInfo.Get_Fields_String("Description"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}
	}
}
