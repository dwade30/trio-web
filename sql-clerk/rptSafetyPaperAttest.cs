﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptSafetyPaperAttest.
	/// </summary>
	public partial class rptSafetyPaperAttest : BaseSectionReport
	{
		public rptSafetyPaperAttest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSafetyPaperAttest InstancePtr
		{
			get
			{
				return (rptSafetyPaperAttest)Sys.GetInstance(typeof(rptSafetyPaperAttest));
			}
		}

		protected rptSafetyPaperAttest _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSafetyPaperAttest	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			switch (modGNBas.Statics.gintSafetyPaperType)
			{
				case 606:
				case 31:
					{
						SubReport1.Report = new srptSafetyPaperBottomSectionR0606();
						break;
					}
				default:
					{
						SubReport1.Report = new srptSafetyPaperBottomSection();
						break;
					}
			}
			//end switch
			SubReport1.Report.UserData = this.UserData;
		}

		
	}
}
