﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWCK0000
{
	public class clsEmailObject
	{
		//=========================================================
		private string strBody = string.Empty;
		private string strEmailAddress = string.Empty;
		private string strSubject = string.Empty;
		private string strTag = string.Empty;
		private string strID = string.Empty;

		public string EmailAddress
		{
			set
			{
				strEmailAddress = value;
			}
			get
			{
				string EmailAddress = "";
				EmailAddress = strEmailAddress;
				return EmailAddress;
			}
		}

		public string Subject
		{
			set
			{
				strSubject = value;
			}
			get
			{
				string Subject = "";
				Subject = strSubject;
				return Subject;
			}
		}

		public string Tag
		{
			set
			{
				strTag = value;
			}
			get
			{
				string Tag = "";
				Tag = strTag;
				return Tag;
			}
		}

		public string Body
		{
			set
			{
				strBody = value;
			}
			get
			{
				string Body = "";
				Body = strBody;
				return Body;
			}
		}

		public string ID
		{
			set
			{
				strID = value;
			}
			get
			{
				string ID = "";
				ID = strID;
				return ID;
			}
		}
	}
}
