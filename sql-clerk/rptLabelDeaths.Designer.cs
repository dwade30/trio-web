﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptLabelDeaths.
	/// </summary>
	partial class rptLabelDeaths
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLabelDeaths));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.L1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.L30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fCauseA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fCauseB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fCauseC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fCauseD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCityOrTown = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAttestedBy = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateOfFiling = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDecedentStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDecedentCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.L30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCityOrTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAttestedBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateOfFiling)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedentStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedentCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Field17,
				this.Line1,
				this.L1,
				this.L2,
				this.L3,
				this.L4,
				this.L5,
				this.L7,
				this.L8,
				this.L15,
				this.L18,
				this.L19,
				this.L20,
				this.L21,
				this.L24,
				this.L10,
				this.L9,
				this.L11,
				this.L13,
				this.L16,
				this.L22,
				this.L25,
				this.L28,
				this.L12,
				this.L14,
				this.L17,
				this.L23,
				this.L26,
				this.L29,
				this.L27,
				this.L30,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Field18,
				this.Field19,
				this.Field20,
				this.Field21,
				this.Field22,
				this.fCauseA,
				this.fCauseB,
				this.fCauseC,
				this.fCauseD,
				this.Line2,
				this.Line3,
				this.Line4,
				this.Label2,
				this.Label3,
				this.Label4,
				this.lblCityOrTown,
				this.lblAttestedBy,
				this.lblDateOfFiling,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.txtDecedentStreet,
				this.txtDecedentCity
			});
			this.Detail.Height = 10.39583F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.875F;
			this.Label1.MultiLine = false;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.Label1.Text = "a.";
			this.Label1.Top = 5.875F;
			this.Label1.Width = 0.3125F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.1875F;
			this.Field17.Left = 1.875F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field17.Text = "Field17";
			this.Field17.Top = 8.625F;
			this.Field17.Width = 2.875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 6.0625F;
			this.Line1.Width = 5.5F;
			this.Line1.X1 = 2.125F;
			this.Line1.X2 = 7.625F;
			this.Line1.Y1 = 6.0625F;
			this.Line1.Y2 = 6.0625F;
			// 
			// L1
			// 
			this.L1.Height = 0.1875F;
			this.L1.HyperLink = null;
			this.L1.Left = 0F;
			this.L1.MultiLine = false;
			this.L1.Name = "L1";
			this.L1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.L1.Text = "CERTIFIED ABSTRACT OF A CERTIFICATE OF DEATH";
			this.L1.Top = 1.1875F;
			this.L1.Width = 8F;
			// 
			// L2
			// 
			this.L2.Height = 0.1875F;
			this.L2.HyperLink = null;
			this.L2.Left = 0F;
			this.L2.MultiLine = false;
			this.L2.Name = "L2";
			this.L2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.L2.Text = "DEPARTMENT OF HEALTH AND HUMAN SERVICES";
			this.L2.Top = 1.4375F;
			this.L2.Width = 8F;
			// 
			// L3
			// 
			this.L3.Height = 0.1875F;
			this.L3.HyperLink = null;
			this.L3.Left = 1.875F;
			this.L3.MultiLine = false;
			this.L3.Name = "L3";
			this.L3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L3.Text = "FULL NAME OF DECEASED";
			this.L3.Top = 2.625F;
			this.L3.Width = 2.125F;
			// 
			// L4
			// 
			this.L4.Height = 0.1875F;
			this.L4.HyperLink = null;
			this.L4.Left = 5.75F;
			this.L4.MultiLine = false;
			this.L4.Name = "L4";
			this.L4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L4.Text = "DATE OF DEATH";
			this.L4.Top = 2.625F;
			this.L4.Width = 1.625F;
			// 
			// L5
			// 
			this.L5.Height = 0.1666667F;
			this.L5.HyperLink = null;
			this.L5.Left = 0.75F;
			this.L5.MultiLine = false;
			this.L5.Name = "L5";
			this.L5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.L5.Text = "DECEDENT";
			this.L5.Top = 2.916667F;
			this.L5.Width = 0.875F;
			// 
			// L7
			// 
			this.L7.Height = 0.1875F;
			this.L7.HyperLink = null;
			this.L7.Left = 0.75F;
			this.L7.MultiLine = false;
			this.L7.Name = "L7";
			this.L7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.L7.Text = "PERSONAL";
			this.L7.Top = 3.0625F;
			this.L7.Width = 0.875F;
			// 
			// L8
			// 
			this.L8.Height = 0.1875F;
			this.L8.HyperLink = null;
			this.L8.Left = 0.75F;
			this.L8.MultiLine = false;
			this.L8.Name = "L8";
			this.L8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.L8.Text = "DATA";
			this.L8.Top = 3.1875F;
			this.L8.Width = 0.875F;
			// 
			// L15
			// 
			this.L15.Height = 0.1875F;
			this.L15.HyperLink = null;
			this.L15.Left = 0.875F;
			this.L15.MultiLine = false;
			this.L15.Name = "L15";
			this.L15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L15.Text = "INFORMANT";
			this.L15.Top = 4.8125F;
			this.L15.Width = 0.875F;
			// 
			// L18
			// 
			this.L18.Height = 0.1875F;
			this.L18.HyperLink = null;
			this.L18.Left = 0.75F;
			this.L18.MultiLine = false;
			this.L18.Name = "L18";
			this.L18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.L18.Text = "CAUSE(S)";
			this.L18.Top = 6.125F;
			this.L18.Width = 0.875F;
			// 
			// L19
			// 
			this.L19.Height = 0.1875F;
			this.L19.HyperLink = null;
			this.L19.Left = 0.75F;
			this.L19.MultiLine = false;
			this.L19.Name = "L19";
			this.L19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.L19.Text = "OF";
			this.L19.Top = 6.25F;
			this.L19.Width = 0.875F;
			// 
			// L20
			// 
			this.L20.Height = 0.1875F;
			this.L20.HyperLink = null;
			this.L20.Left = 0.75F;
			this.L20.MultiLine = false;
			this.L20.Name = "L20";
			this.L20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.L20.Text = "DEATH";
			this.L20.Top = 6.375F;
			this.L20.Width = 0.875F;
			// 
			// L21
			// 
			this.L21.Height = 0.1875F;
			this.L21.HyperLink = null;
			this.L21.Left = 0.625F;
			this.L21.MultiLine = false;
			this.L21.Name = "L21";
			this.L21.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L21.Text = "CERTIFICATION";
			this.L21.Top = 7.3125F;
			this.L21.Width = 1.125F;
			// 
			// L24
			// 
			this.L24.Height = 0.1875F;
			this.L24.HyperLink = null;
			this.L24.Left = 0.6875F;
			this.L24.MultiLine = false;
			this.L24.Name = "L24";
			this.L24.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L24.Text = "DISPOSITION";
			this.L24.Top = 7.9375F;
			this.L24.Width = 1F;
			// 
			// L10
			// 
			this.L10.Height = 0.1875F;
			this.L10.HyperLink = null;
			this.L10.Left = 5.75F;
			this.L10.MultiLine = false;
			this.L10.Name = "L10";
			this.L10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L10.Text = "DATE OF BIRTH";
			this.L10.Top = 3F;
			this.L10.Width = 1.625F;
			// 
			// L9
			// 
			this.L9.Height = 0.1875F;
			this.L9.HyperLink = null;
			this.L9.Left = 1.875F;
			this.L9.MultiLine = false;
			this.L9.Name = "L9";
			this.L9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L9.Text = "PLACE OF DEATH";
			this.L9.Top = 3F;
			this.L9.Width = 2.125F;
			// 
			// L11
			// 
			this.L11.Height = 0.1875F;
			this.L11.HyperLink = null;
			this.L11.Left = 1.875F;
			this.L11.MultiLine = false;
			this.L11.Name = "L11";
			this.L11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L11.Text = "SOCIAL SECURITY NUMBER";
			this.L11.Top = 3.5F;
			this.L11.Width = 2.125F;
			// 
			// L13
			// 
			this.L13.Height = 0.1875F;
			this.L13.HyperLink = null;
			this.L13.Left = 1.875F;
			this.L13.MultiLine = false;
			this.L13.Name = "L13";
			this.L13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L13.Text = "FATHER\'S NAME";
			this.L13.Top = 3.875F;
			this.L13.Width = 2.125F;
			// 
			// L16
			// 
			this.L16.Height = 0.1875F;
			this.L16.HyperLink = null;
			this.L16.Left = 1.875F;
			this.L16.MultiLine = false;
			this.L16.Name = "L16";
			this.L16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L16.Text = "NAME OF INFORMANT";
			this.L16.Top = 4.6875F;
			this.L16.Width = 2.125F;
			// 
			// L22
			// 
			this.L22.Height = 0.1875F;
			this.L22.HyperLink = null;
			this.L22.Left = 1.875F;
			this.L22.MultiLine = false;
			this.L22.Name = "L22";
			this.L22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L22.Text = "NAME OF PHYSICIAN OR MEDICAL EXAMINER CERTIFYING DEATH";
			this.L22.Top = 7.3125F;
			this.L22.Width = 4F;
			// 
			// L25
			// 
			this.L25.Height = 0.1875F;
			this.L25.HyperLink = null;
			this.L25.Left = 1.875F;
			this.L25.MultiLine = false;
			this.L25.Name = "L25";
			this.L25.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L25.Text = "NAME OF CEMETERY OR CREMATORY";
			this.L25.Top = 7.9375F;
			this.L25.Width = 2.75F;
			// 
			// L28
			// 
			this.L28.Height = 0.1875F;
			this.L28.HyperLink = null;
			this.L28.Left = 1.875F;
			this.L28.MultiLine = false;
			this.L28.Name = "L28";
			this.L28.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L28.Text = "NAME OF CLERK RECORDING THIS DEATH";
			this.L28.Top = 8.4375F;
			this.L28.Width = 2.875F;
			// 
			// L12
			// 
			this.L12.Height = 0.1875F;
			this.L12.HyperLink = null;
			this.L12.Left = 5.75F;
			this.L12.MultiLine = false;
			this.L12.Name = "L12";
			this.L12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L12.Text = "SEX";
			this.L12.Top = 3.5F;
			this.L12.Width = 1.625F;
			// 
			// L14
			// 
			this.L14.Height = 0.1875F;
			this.L14.HyperLink = null;
			this.L14.Left = 4.5625F;
			this.L14.MultiLine = false;
			this.L14.Name = "L14";
			this.L14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L14.Text = "MOTHER\'S MAIDEN NAME";
			this.L14.Top = 3.875F;
			this.L14.Width = 1.8125F;
			// 
			// L17
			// 
			this.L17.Height = 0.1875F;
			this.L17.HyperLink = null;
			this.L17.Left = 4.5625F;
			this.L17.MultiLine = false;
			this.L17.Name = "L17";
			this.L17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L17.Text = "ADDRESS";
			this.L17.Top = 4.6875F;
			this.L17.Width = 1.625F;
			// 
			// L23
			// 
			this.L23.Height = 0.1875F;
			this.L23.HyperLink = null;
			this.L23.Left = 6.25F;
			this.L23.MultiLine = false;
			this.L23.Name = "L23";
			this.L23.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L23.Text = "DATE SIGNED";
			this.L23.Top = 7.3125F;
			this.L23.Width = 1.625F;
			// 
			// L26
			// 
			this.L26.Height = 0.1875F;
			this.L26.HyperLink = null;
			this.L26.Left = 4.8125F;
			this.L26.MultiLine = false;
			this.L26.Name = "L26";
			this.L26.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L26.Text = "CITY OR TOWN";
			this.L26.Top = 7.9375F;
			this.L26.Width = 1.625F;
			// 
			// L29
			// 
			this.L29.Height = 0.1875F;
			this.L29.HyperLink = null;
			this.L29.Left = 4.8125F;
			this.L29.MultiLine = false;
			this.L29.Name = "L29";
			this.L29.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L29.Text = "CITY OR TOWN";
			this.L29.Top = 8.4375F;
			this.L29.Width = 1.625F;
			// 
			// L27
			// 
			this.L27.Height = 0.1875F;
			this.L27.HyperLink = null;
			this.L27.Left = 6.25F;
			this.L27.MultiLine = false;
			this.L27.Name = "L27";
			this.L27.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L27.Text = "STATE";
			this.L27.Top = 7.9375F;
			this.L27.Width = 1.625F;
			// 
			// L30
			// 
			this.L30.Height = 0.1875F;
			this.L30.HyperLink = null;
			this.L30.Left = 6.25F;
			this.L30.MultiLine = false;
			this.L30.Name = "L30";
			this.L30.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.L30.Text = "DATE OF FILING";
			this.L30.Top = 8.4375F;
			this.L30.Width = 1.625F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1.875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field1.Text = null;
			this.Field1.Top = 2.75F;
			this.Field1.Width = 3.25F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 5.75F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field2.Text = null;
			this.Field2.Top = 2.75F;
			this.Field2.Width = 2.125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 1.875F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field3.Text = "Field3";
			this.Field3.Top = 3.125F;
			this.Field3.Width = 3.25F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 5.75F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field4.Text = null;
			this.Field4.Top = 3.125F;
			this.Field4.Width = 2.125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 1.875F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field5.Text = null;
			this.Field5.Top = 3.625F;
			this.Field5.Width = 3.25F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 5.75F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field6.Text = null;
			this.Field6.Top = 3.625F;
			this.Field6.Width = 2F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 1.875F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field7.Text = "Field7";
			this.Field7.Top = 4.0625F;
			this.Field7.Width = 2.625F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 4.5625F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field8.Text = "Field8";
			this.Field8.Top = 4.0625F;
			this.Field8.Width = 3.1875F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 1.875F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field9.Text = "Field9";
			this.Field9.Top = 4.875F;
			this.Field9.Width = 2.125F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 4.5625F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field10.Text = "Field10";
			this.Field10.Top = 4.875F;
			this.Field10.Width = 3.4375F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 1.875F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field12.Text = "Field12";
			this.Field12.Top = 7.5F;
			this.Field12.Width = 3.75F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 6.25F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field13.Text = "Field13";
			this.Field13.Top = 7.5F;
			this.Field13.Width = 1.75F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 1.875F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field14.Text = "Field14";
			this.Field14.Top = 8.125F;
			this.Field14.Width = 2.875F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 4.8125F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field15.Text = "Field15";
			this.Field15.Top = 8.125F;
			this.Field15.Width = 1.3125F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 6.25F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field16.Text = "Field16";
			this.Field16.Top = 8.125F;
			this.Field16.Width = 1.75F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.1875F;
			this.Field18.Left = 4.8125F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field18.Text = "Field18";
			this.Field18.Top = 8.625F;
			this.Field18.Width = 1.4375F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1875F;
			this.Field19.Left = 6.25F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field19.Text = "Field19";
			this.Field19.Top = 8.625F;
			this.Field19.Width = 1.625F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 2F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field20.Text = "Field20";
			this.Field20.Top = 9.5625F;
			this.Field20.Width = 2.5625F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.1875F;
			this.Field21.Left = 2F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field21.Text = "Field21";
			this.Field21.Top = 10.125F;
			this.Field21.Width = 2.75F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.1875F;
			this.Field22.Left = 4.8125F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field22.Text = "Field22";
			this.Field22.Top = 10.125F;
			this.Field22.Width = 3.3125F;
			// 
			// fCauseA
			// 
			this.fCauseA.Height = 0.1875F;
			this.fCauseA.Left = 2.1875F;
			this.fCauseA.Name = "fCauseA";
			this.fCauseA.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fCauseA.Text = "a";
			this.fCauseA.Top = 5.875F;
			this.fCauseA.Width = 5.4375F;
			// 
			// fCauseB
			// 
			this.fCauseB.Height = 0.1875F;
			this.fCauseB.Left = 2.1875F;
			this.fCauseB.Name = "fCauseB";
			this.fCauseB.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fCauseB.Text = "b";
			this.fCauseB.Top = 6.1875F;
			this.fCauseB.Width = 5.4375F;
			// 
			// fCauseC
			// 
			this.fCauseC.Height = 0.1875F;
			this.fCauseC.Left = 2.1875F;
			this.fCauseC.Name = "fCauseC";
			this.fCauseC.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fCauseC.Text = "c";
			this.fCauseC.Top = 6.5F;
			this.fCauseC.Width = 5.4375F;
			// 
			// fCauseD
			// 
			this.fCauseD.Height = 0.1875F;
			this.fCauseD.Left = 2.1875F;
			this.fCauseD.Name = "fCauseD";
			this.fCauseD.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fCauseD.Text = "d";
			this.fCauseD.Top = 6.8125F;
			this.fCauseD.Width = 5.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 6.375F;
			this.Line2.Width = 5.5F;
			this.Line2.X1 = 2.125F;
			this.Line2.X2 = 7.625F;
			this.Line2.Y1 = 6.375F;
			this.Line2.Y2 = 6.375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 2.125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 6.6875F;
			this.Line3.Width = 5.5F;
			this.Line3.X1 = 2.125F;
			this.Line3.X2 = 7.625F;
			this.Line3.Y1 = 6.6875F;
			this.Line3.Y2 = 6.6875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 2.125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 7F;
			this.Line4.Width = 5.4375F;
			this.Line4.X1 = 2.125F;
			this.Line4.X2 = 7.5625F;
			this.Line4.Y1 = 7F;
			this.Line4.Y2 = 7F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.875F;
			this.Label2.MultiLine = false;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.Label2.Text = "b.";
			this.Label2.Top = 6.1875F;
			this.Label2.Width = 0.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.875F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.Label3.Text = "c.";
			this.Label3.Top = 6.5F;
			this.Label3.Width = 0.3125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.875F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.Label4.Text = "d.";
			this.Label4.Top = 6.8125F;
			this.Label4.Width = 0.3125F;
			// 
			// lblCityOrTown
			// 
			this.lblCityOrTown.Height = 0.2083333F;
			this.lblCityOrTown.HyperLink = null;
			this.lblCityOrTown.Left = 4.8125F;
			this.lblCityOrTown.MultiLine = false;
			this.lblCityOrTown.Name = "lblCityOrTown";
			this.lblCityOrTown.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblCityOrTown.Text = "CITY OR TOWN";
			this.lblCityOrTown.Top = 9.958333F;
			this.lblCityOrTown.Width = 1.625F;
			// 
			// lblAttestedBy
			// 
			this.lblAttestedBy.Height = 0.2083333F;
			this.lblAttestedBy.HyperLink = null;
			this.lblAttestedBy.Left = 2F;
			this.lblAttestedBy.MultiLine = false;
			this.lblAttestedBy.Name = "lblAttestedBy";
			this.lblAttestedBy.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblAttestedBy.Text = "ATTESTED BY";
			this.lblAttestedBy.Top = 9.958333F;
			this.lblAttestedBy.Width = 1.625F;
			// 
			// lblDateOfFiling
			// 
			this.lblDateOfFiling.Height = 0.2083333F;
			this.lblDateOfFiling.HyperLink = null;
			this.lblDateOfFiling.Left = 2F;
			this.lblDateOfFiling.MultiLine = false;
			this.lblDateOfFiling.Name = "lblDateOfFiling";
			this.lblDateOfFiling.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblDateOfFiling.Text = "DATE OF FILING";
			this.lblDateOfFiling.Top = 9.375F;
			this.lblDateOfFiling.Width = 1.625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.75F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label8.Text = "PLACE OF";
			this.Label8.Top = 5.21875F;
			this.Label8.Width = 0.875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.6875F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label9.Text = "RESIDENCE OF";
			this.Label9.Top = 5.375F;
			this.Label9.Width = 1F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.75F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label10.Text = "DECEDENT";
			this.Label10.Top = 5.5F;
			this.Label10.Width = 0.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.875F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label11.Text = "STREET AND NUMBER";
			this.Label11.Top = 5.15625F;
			this.Label11.Width = 2.125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.5625F;
			this.Label12.MultiLine = false;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label12.Text = "CITY/TOWN";
			this.Label12.Top = 5.15625F;
			this.Label12.Width = 1.625F;
			// 
			// txtDecedentStreet
			// 
			this.txtDecedentStreet.Height = 0.19F;
			this.txtDecedentStreet.Left = 1.875F;
			this.txtDecedentStreet.Name = "txtDecedentStreet";
			this.txtDecedentStreet.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDecedentStreet.Text = "Field23";
			this.txtDecedentStreet.Top = 5.375F;
			this.txtDecedentStreet.Width = 2.125F;
			// 
			// txtDecedentCity
			// 
			this.txtDecedentCity.Height = 0.19F;
			this.txtDecedentCity.Left = 4.5625F;
			this.txtDecedentCity.Name = "txtDecedentCity";
			this.txtDecedentCity.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDecedentCity.Text = "Field24";
			this.txtDecedentCity.Top = 5.375F;
			this.txtDecedentCity.Width = 2.125F;
			// 
			// rptLabelDeaths
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.L30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fCauseD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCityOrTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAttestedBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateOfFiling)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedentStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedentCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label L1;
		private GrapeCity.ActiveReports.SectionReportModel.Label L2;
		private GrapeCity.ActiveReports.SectionReportModel.Label L3;
		private GrapeCity.ActiveReports.SectionReportModel.Label L4;
		private GrapeCity.ActiveReports.SectionReportModel.Label L5;
		private GrapeCity.ActiveReports.SectionReportModel.Label L7;
		private GrapeCity.ActiveReports.SectionReportModel.Label L8;
		private GrapeCity.ActiveReports.SectionReportModel.Label L15;
		private GrapeCity.ActiveReports.SectionReportModel.Label L18;
		private GrapeCity.ActiveReports.SectionReportModel.Label L19;
		private GrapeCity.ActiveReports.SectionReportModel.Label L20;
		private GrapeCity.ActiveReports.SectionReportModel.Label L21;
		private GrapeCity.ActiveReports.SectionReportModel.Label L24;
		private GrapeCity.ActiveReports.SectionReportModel.Label L10;
		private GrapeCity.ActiveReports.SectionReportModel.Label L9;
		private GrapeCity.ActiveReports.SectionReportModel.Label L11;
		private GrapeCity.ActiveReports.SectionReportModel.Label L13;
		private GrapeCity.ActiveReports.SectionReportModel.Label L16;
		private GrapeCity.ActiveReports.SectionReportModel.Label L22;
		private GrapeCity.ActiveReports.SectionReportModel.Label L25;
		private GrapeCity.ActiveReports.SectionReportModel.Label L28;
		private GrapeCity.ActiveReports.SectionReportModel.Label L12;
		private GrapeCity.ActiveReports.SectionReportModel.Label L14;
		private GrapeCity.ActiveReports.SectionReportModel.Label L17;
		private GrapeCity.ActiveReports.SectionReportModel.Label L23;
		private GrapeCity.ActiveReports.SectionReportModel.Label L26;
		private GrapeCity.ActiveReports.SectionReportModel.Label L29;
		private GrapeCity.ActiveReports.SectionReportModel.Label L27;
		private GrapeCity.ActiveReports.SectionReportModel.Label L30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fCauseA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fCauseB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fCauseC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fCauseD;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCityOrTown;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAttestedBy;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateOfFiling;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDecedentStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDecedentCity;
	}
}
