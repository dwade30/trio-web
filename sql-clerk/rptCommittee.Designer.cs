﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptCommittee.
	/// </summary>
	partial class rptCommittee
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCommittee));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEMail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTermFrom = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTermTo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUserDefined = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTermFrom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTermTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUserDefined = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblHome = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHomePhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCell = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCell = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOther = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTermFrom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTermTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUserDefined)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTermFrom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTermTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserDefined)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHome)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomePhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCell)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCell)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.txtName,
				this.txtAddress1,
				this.txtAddress2,
				this.txtCityStateZip,
				this.txtPhone,
				this.txtEMail,
				this.Label5,
				this.lblTermFrom,
				this.lblTermTo,
				this.lblUserDefined,
				this.txtTitle,
				this.txtTermFrom,
				this.txtTermTo,
				this.txtUserDefined,
				this.Line1,
				this.lblHome,
				this.txtHomePhone,
				this.lblCell,
				this.txtCell,
				this.lblOther,
				this.txtOther
			});
			this.Detail.Height = 1.885417F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.txtGroupTitle,
				this.txtDescription
			});
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.65625F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Text = "Field1";
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2.5F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.1666667F;
			this.txtTime.Width = 1.416667F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 6F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.416667F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 6F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.1666667F;
			this.txtPage.Width = 1.416667F;
			// 
			// txtGroupTitle
			// 
			this.txtGroupTitle.Height = 0.4166667F;
			this.txtGroupTitle.Left = 2F;
			this.txtGroupTitle.Name = "txtGroupTitle";
			this.txtGroupTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.txtGroupTitle.Text = null;
			this.txtGroupTitle.Top = 0F;
			this.txtGroupTitle.Width = 3.5F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1666667F;
			this.txtDescription.Left = 0.5F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-size: 10pt; text-align: center";
			this.txtDescription.Text = "Field1";
			this.txtDescription.Top = 0.4166667F;
			this.txtDescription.Width = 6.5F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.08333334F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Name";
			this.Label1.Top = 0.1666667F;
			this.Label1.Width = 0.75F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.08333334F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Address";
			this.Label2.Top = 0.3333333F;
			this.Label2.Width = 0.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.08333334F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Work";
			this.Label3.Top = 1F;
			this.Label3.Width = 0.8333333F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.08333334F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "E-Mail";
			this.Label4.Top = 0.8333333F;
			this.Label4.Width = 0.75F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 0.9166667F;
			this.txtName.Name = "txtName";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0.1666667F;
			this.txtName.Width = 3.083333F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.1666667F;
			this.txtAddress1.Left = 0.9166667F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Text = "Field2";
			this.txtAddress1.Top = 0.3333333F;
			this.txtAddress1.Width = 3.083333F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.1666667F;
			this.txtAddress2.Left = 0.9166667F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Text = "Field3";
			this.txtAddress2.Top = 0.5F;
			this.txtAddress2.Width = 3.083333F;
			// 
			// txtCityStateZip
			// 
			this.txtCityStateZip.Height = 0.1666667F;
			this.txtCityStateZip.Left = 0.9166667F;
			this.txtCityStateZip.Name = "txtCityStateZip";
			this.txtCityStateZip.Text = "Field4";
			this.txtCityStateZip.Top = 0.6666667F;
			this.txtCityStateZip.Width = 3.083333F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1666667F;
			this.txtPhone.Left = 0.9166667F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Text = "Field5";
			this.txtPhone.Top = 1F;
			this.txtPhone.Width = 1.666667F;
			// 
			// txtEMail
			// 
			this.txtEMail.Height = 0.1666667F;
			this.txtEMail.Left = 0.9166667F;
			this.txtEMail.Name = "txtEMail";
			this.txtEMail.Text = "Field6";
			this.txtEMail.Top = 0.8333333F;
			this.txtEMail.Width = 3.083333F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.083333F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Title";
			this.Label5.Top = 0.1666667F;
			this.Label5.Width = 0.75F;
			// 
			// lblTermFrom
			// 
			this.lblTermFrom.Height = 0.1666667F;
			this.lblTermFrom.HyperLink = null;
			this.lblTermFrom.Left = 4F;
			this.lblTermFrom.Name = "lblTermFrom";
			this.lblTermFrom.Style = "font-weight: bold; text-align: right";
			this.lblTermFrom.Text = "Term From";
			this.lblTermFrom.Top = 0.5F;
			this.lblTermFrom.Width = 0.8333333F;
			// 
			// lblTermTo
			// 
			this.lblTermTo.Height = 0.1666667F;
			this.lblTermTo.HyperLink = null;
			this.lblTermTo.Left = 4.083333F;
			this.lblTermTo.Name = "lblTermTo";
			this.lblTermTo.Style = "font-weight: bold; text-align: right";
			this.lblTermTo.Text = "Term To";
			this.lblTermTo.Top = 0.6666667F;
			this.lblTermTo.Width = 0.75F;
			// 
			// lblUserDefined
			// 
			this.lblUserDefined.Height = 0.1666667F;
			this.lblUserDefined.HyperLink = null;
			this.lblUserDefined.Left = 3.25F;
			this.lblUserDefined.Name = "lblUserDefined";
			this.lblUserDefined.Style = "font-weight: bold; text-align: right";
			this.lblUserDefined.Text = "User Defined";
			this.lblUserDefined.Top = 1F;
			this.lblUserDefined.Width = 1.583333F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.1666667F;
			this.txtTitle.Left = 4.916667F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Text = "Field7";
			this.txtTitle.Top = 0.1666667F;
			this.txtTitle.Width = 2.5F;
			// 
			// txtTermFrom
			// 
			this.txtTermFrom.Height = 0.1666667F;
			this.txtTermFrom.Left = 4.916667F;
			this.txtTermFrom.Name = "txtTermFrom";
			this.txtTermFrom.Text = "Field8";
			this.txtTermFrom.Top = 0.5F;
			this.txtTermFrom.Width = 2.5F;
			// 
			// txtTermTo
			// 
			this.txtTermTo.Height = 0.1666667F;
			this.txtTermTo.Left = 4.916667F;
			this.txtTermTo.Name = "txtTermTo";
			this.txtTermTo.Text = "Field9";
			this.txtTermTo.Top = 0.6666667F;
			this.txtTermTo.Width = 2.5F;
			// 
			// txtUserDefined
			// 
			this.txtUserDefined.Height = 0.1666667F;
			this.txtUserDefined.Left = 4.916667F;
			this.txtUserDefined.Name = "txtUserDefined";
			this.txtUserDefined.Text = "Field10";
			this.txtUserDefined.Top = 1F;
			this.txtUserDefined.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.08333334F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.08333334F;
			this.Line1.Width = 7.333333F;
			this.Line1.X1 = 0.08333334F;
			this.Line1.X2 = 7.416667F;
			this.Line1.Y1 = 0.08333334F;
			this.Line1.Y2 = 0.08333334F;
			// 
			// lblHome
			// 
			this.lblHome.Height = 0.1875F;
			this.lblHome.HyperLink = null;
			this.lblHome.Left = 0.08333334F;
			this.lblHome.Name = "lblHome";
			this.lblHome.Style = "font-weight: bold";
			this.lblHome.Text = "Home";
			this.lblHome.Top = 1.166667F;
			this.lblHome.Visible = false;
			this.lblHome.Width = 0.875F;
			// 
			// txtHomePhone
			// 
			this.txtHomePhone.Height = 0.1875F;
			this.txtHomePhone.Left = 0.9166667F;
			this.txtHomePhone.Name = "txtHomePhone";
			this.txtHomePhone.Text = "Field5";
			this.txtHomePhone.Top = 1.166667F;
			this.txtHomePhone.Visible = false;
			this.txtHomePhone.Width = 1.666667F;
			// 
			// lblCell
			// 
			this.lblCell.Height = 0.1875F;
			this.lblCell.HyperLink = null;
			this.lblCell.Left = 0.08333334F;
			this.lblCell.Name = "lblCell";
			this.lblCell.Style = "font-weight: bold";
			this.lblCell.Text = "Cell";
			this.lblCell.Top = 1.354167F;
			this.lblCell.Visible = false;
			this.lblCell.Width = 0.875F;
			// 
			// txtCell
			// 
			this.txtCell.Height = 0.1875F;
			this.txtCell.Left = 0.9166667F;
			this.txtCell.Name = "txtCell";
			this.txtCell.Text = "Field5";
			this.txtCell.Top = 1.354167F;
			this.txtCell.Visible = false;
			this.txtCell.Width = 1.666667F;
			// 
			// lblOther
			// 
			this.lblOther.Height = 0.1875F;
			this.lblOther.HyperLink = null;
			this.lblOther.Left = 0.08333334F;
			this.lblOther.Name = "lblOther";
			this.lblOther.Style = "font-weight: bold";
			this.lblOther.Text = "Other";
			this.lblOther.Top = 1.541667F;
			this.lblOther.Visible = false;
			this.lblOther.Width = 0.875F;
			// 
			// txtOther
			// 
			this.txtOther.Height = 0.1875F;
			this.txtOther.Left = 0.9166667F;
			this.txtOther.Name = "txtOther";
			this.txtOther.Text = "Field5";
			this.txtOther.Top = 1.541667F;
			this.txtOther.Visible = false;
			this.txtOther.Width = 1.666667F;
			// 
			// rptCommittee
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTermFrom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTermTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUserDefined)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTermFrom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTermTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserDefined)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHome)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomePhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCell)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCell)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEMail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTermFrom;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTermTo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUserDefined;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTermFrom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTermTo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUserDefined;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHome;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHomePhone;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCell;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCell;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
