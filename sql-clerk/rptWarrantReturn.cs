﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptWarrantReturn.
	/// </summary>
	public partial class rptWarrantReturn : BaseSectionReport
	{
		public rptWarrantReturn()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Warrant Return";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptWarrantReturn InstancePtr
		{
			get
			{
				return (rptWarrantReturn)Sys.GetInstance(typeof(rptWarrantReturn));
			}
		}

		protected rptWarrantReturn _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWarrantReturn	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), boolAllowEmail: false, showModal: modalDialog);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.Grid);
			rs.OpenRecordset("SELECT * FROM WarrantParameters", modGNBas.DEFAULTDATABASE);
			// Call SetPrintProperties(Me)
			double dblLaserLineAdjustment9;
			// Dim rsData As New clsDRWrapper
			// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
			// If rsData.EndOfFile Then
			// dblLaserLineAdjustment9 = 0
			// Else
			// dblLaserLineAdjustment9 = CDbl(Val(rsData.Fields("DogAdjustment")))
			// End If
			dblLaserLineAdjustment9 = Conversion.Val(modRegistry.GetRegistryKey("DogAdjustment"));
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.PageHeader.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment9) / 1440F;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
            //FC:FINAL:AM:#4038 - load form if it's not already loaded
            if (!frmDogWarrant.InstancePtr.IsLoaded)
            {
                frmDogWarrant.InstancePtr.LoadForm();
            }
            txtTownCounty1.Text = frmDogWarrant.InstancePtr.txtTownCounty.Text;
			txtCourtCounty.Text = frmDogWarrant.InstancePtr.txtCourtCounty.Text;
			txtTownName1.Text = frmDogWarrant.InstancePtr.txtTownName.Text;
			txtTownName2.Text = frmDogWarrant.InstancePtr.txtTownName.Text;
			txtCourtTown.Text = frmDogWarrant.InstancePtr.txtCourtTown.Text;
			txtControlOfficer.Text = frmDogWarrant.InstancePtr.txtControlOfficer.Text;
		}

		
	}
}
