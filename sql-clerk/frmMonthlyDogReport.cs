//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	public partial class frmMonthlyDogReport : BaseForm
	{
		public frmMonthlyDogReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			txtYearToReport = new System.Collections.Generic.List<FCTextBox>();
			txtYearToReport.AddControlArrayElement(txtYearToReport_0, 0);
			txtYearToReport.AddControlArrayElement(txtYearToReport_1, 1);
			txtPrint = new System.Collections.Generic.List<FCButton>();
            //FC:FINAL: AKV cancel button
			//txtPrint.AddControlArrayElement(txtPrint_1, 0);
			txtPrint.AddControlArrayElement(txtPrint_2, 2);
            this.txtMonth.AllowOnlyNumericInput();
            this.txtYearToReport_0.AllowOnlyNumericInput();
            this.txtYearToReport_1.AllowOnlyNumericInput();
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMonthlyDogReport InstancePtr
		{
			get
			{
				return (frmMonthlyDogReport)Sys.GetInstance(typeof(frmMonthlyDogReport));
			}
		}

		protected frmMonthlyDogReport _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsVitalRecordCertNum = new clsDRWrapper();

		private void frmMonthlyDogReport_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
			//App.DoEvents();
			//Support.ZOrder(this, 0);
		}

		private void frmMonthlyDogReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmMonthlyDogReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmMonthlyDogReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMonthlyDogReport properties;
			//frmMonthlyDogReport.ScaleWidth	= 5940;
			//frmMonthlyDogReport.ScaleHeight	= 4215;
			//frmMonthlyDogReport.LinkTopic	= "Form1";
			//frmMonthlyDogReport.LockControls	= -1  'True;
			//End Unmaped Properties
			if (DateTime.Today.Month < 10)
			{
				txtYearToReport[0].Text = FCConvert.ToString(DateTime.Today.Year - 1);
				txtYearToReport[1].Text = FCConvert.ToString(DateTime.Today.Year);
			}
			else
			{
				txtYearToReport[0].Text = FCConvert.ToString(DateTime.Today.Year);
				txtYearToReport[1].Text = FCConvert.ToString(DateTime.Today.Year + 1);
			}
			txtMonth.Text = FCConvert.ToString(DateTime.Today.Month);
			// txtClerkName = ClerkName
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this, false);
			/* Control ctl = new Control(); */
			foreach (Control ctl in this.GetAllControls())
			{
				if (FCConvert.ToString(ctl.Tag) == "Required")
				{
					ctl.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
			}
			// ctl
			SetupGridTownCode();
			if (!modGNBas.Statics.boolRegionalTown)
			{
				gridTownCode.Visible = false;
			}
			else
			{
				gridTownCode.Visible = true;
			}
		}

		private void frmMonthlyDogReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTownCode();
		}

		private void txtAdjustments_Enter(object sender, System.EventArgs e)
		{
			//HighlightText();
		}

		private void txtMonth_Enter(object sender, System.EventArgs e)
		{
			//HighlightText();
		}

		private void txtMonth_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			KeyAscii = CheckNumeric(KeyAscii);
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPrint_Click(int Index, object sender, System.EventArgs e)
		{
			if (Index == 2)
			{
				if (Conversion.Val(txtMonth.Text) <= 0 || Conversion.Val(txtMonth.Text) > 12)
				{
					MessageBox.Show("Invalid Month to report on.", "Invalid Month", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtMonth.Text = string.Empty;
					txtMonth.Focus();
					return;
				}
				if (fecherFoundation.Strings.Trim(txtClerkName.Text) == "")
				{
					MessageBox.Show("You must enter the clerk name before you may continue.", "Invalid Clerk Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtClerkName.Focus();
					return;
				}
				if (Conversion.Val(txtYearToReport[0].Text) == 0)
				{
					MessageBox.Show("Invalid Year to report on.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtYearToReport[0].Text = string.Empty;
					txtYearToReport[0].Focus();
					return;
				}
				if (Conversion.Val(txtYearToReport[1].Text) == 0)
				{
					MessageBox.Show("Invalid Year to report on.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtYearToReport[1].Text = string.Empty;
					txtYearToReport[1].Focus();
					return;
				}
				modGNBas.Statics.gstrMonthYear = Strings.Format(txtMonth.Text, "00") + "/" + txtYearToReport[0].Text;
				if (modGNBas.Statics.gboolMonthlyLicenseLaser)
				{
					// MsgBox "Laser Monthly Dog License can no longer be printed. Reset Print settings in Default Printer Settings under File Maintenance screen.", vbInformation + vbOKOnly, "TRIO Software"
					// rptNewMonthlyDogReportLaser.Show
					// rptNewMonthlyDogReportLaser.Zoom = -1
					frmReportViewer.InstancePtr.Init(rptNewMonthlyDogReportLaser.InstancePtr, boolAllowEmail: true, strAttachmentName: "MonthlyDogReport");
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptMonthlyDogReport.InstancePtr, showModal: this.Modal);
					//rptMonthlyDogReport.InstancePtr.Show();
					//rptMonthlyDogReport.InstancePtr.Zoom = -1;
				}
			}
			else if (Index == 1)
			{
				Close();
			}
		}

		private void txtPrint_Click(object sender, System.EventArgs e)
		{
			int index = txtPrint.GetIndex((FCButton)sender);
			txtPrint_Click(index, sender, e);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			rsVitalRecordCertNum = null;
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void txtReason_Enter(object sender, System.EventArgs e)
		{
			//HighlightText();
		}
		//
		// Private Sub txtYearToReport_Change(Index As Integer)
		// If Len(txtYearToReport(Index)) = 4 And Index = 0 Then
		// lblCorrections = lblCorrections.Tag & "    " & txtYearToReport(Index) & "        " & CInt(txtYearToReport(Index)) + 1
		// End If
		// End Sub
		private void txtYearToReport_Enter(int Index, object sender, System.EventArgs e)
		{
			//HighlightText();
		}

		private void txtYearToReport_Enter(object sender, System.EventArgs e)
		{
			int index = txtYearToReport.GetIndex((FCTextBox)sender);
			txtYearToReport_Enter(index, sender, e);
		}
		// vbPorter upgrade warning: ControlName As object	OnWrite(Control)
		public void HighlightText(ref FCTextBox ControlName)
		{
			/*? On Error Resume Next  */
			if (Information.IsNothing(ControlName))
				ControlName = this.ActiveControl as FCTextBox;
			ControlName.SelectionStart = 0;
			ControlName.SelectionLength = fecherFoundation.Strings.Trim(FCConvert.ToString(ControlName.Text)).Length;
		}
		// vbPorter upgrade warning: KeyAscii As int	OnWrite(Keys)
		// vbPorter upgrade warning: 'Return' As object	OnWrite
		private Keys CheckNumeric(Keys KeyAscii)
		{
			Keys CheckNumeric = Keys.None;
			// let the backspace ID go through
			if (KeyAscii == (Keys)8)
			{
				CheckNumeric = KeyAscii;
				return CheckNumeric;
			}
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
			{
				CheckNumeric = 0;
			}
			else
			{
				CheckNumeric = KeyAscii;
			}
			return CheckNumeric;
		}

		private void txtYearToReport_KeyPress(int Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			KeyAscii = CheckNumeric(KeyAscii);
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtYearToReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int index = txtYearToReport.GetIndex((FCTextBox)sender);
			txtYearToReport_KeyPress(index, sender, e);
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGNBas.Statics.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblregionS where townnumber > 0 order by townnumber", "CentralData");
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("townnumber") + ";" + clsLoad.Get_Fields_String("townname") + "\t" + clsLoad.Get_Fields("townnumber") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				strTemp = "0";
				gridTownCode.Visible = false;
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}
	}
}
