//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmPrintOldVitals.
	/// </summary>
	partial class frmPrintOldVitals
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtAttestTown;
		public fecherFoundation.FCButton cmdFind;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCTextBox txtPageNum;
		public fecherFoundation.FCTextBox txtFileNum;
		public fecherFoundation.FCTextBox txtTown;
		public fecherFoundation.FCTextBox txtClerk;
		public fecherFoundation.FCTextBox txtAttest;
		public Global.T2KDateBox mebToDate;
		public Global.T2KDateBox mebFromDate;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintOldVitals));
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtAttestTown = new fecherFoundation.FCTextBox();
            this.cmdFind = new fecherFoundation.FCButton();
            this.txtPageNum = new fecherFoundation.FCTextBox();
            this.txtFileNum = new fecherFoundation.FCTextBox();
            this.txtTown = new fecherFoundation.FCTextBox();
            this.txtClerk = new fecherFoundation.FCTextBox();
            this.txtAttest = new fecherFoundation.FCTextBox();
            this.mebToDate = new Global.T2KDateBox();
            this.mebFromDate = new Global.T2KDateBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(388, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(388, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(388, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(204, 30);
            this.HeaderText.Text = "Old Vital Records";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtAttestTown);
            this.Frame1.Controls.Add(this.cmdFind);
            this.Frame1.Controls.Add(this.txtPageNum);
            this.Frame1.Controls.Add(this.txtFileNum);
            this.Frame1.Controls.Add(this.txtTown);
            this.Frame1.Controls.Add(this.txtClerk);
            this.Frame1.Controls.Add(this.txtAttest);
            this.Frame1.Controls.Add(this.mebToDate);
            this.Frame1.Controls.Add(this.mebFromDate);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(315, 558);
            this.Frame1.TabIndex = 8;
            this.Frame1.Text = "Print Parameters";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // txtAttestTown
            // 
            this.txtAttestTown.AutoSize = false;
            this.txtAttestTown.BackColor = System.Drawing.SystemColors.Window;
            this.txtAttestTown.LinkItem = null;
            this.txtAttestTown.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAttestTown.LinkTopic = null;
            this.txtAttestTown.Location = new System.Drawing.Point(20, 410);
            this.txtAttestTown.Name = "txtAttestTown";
            this.txtAttestTown.Size = new System.Drawing.Size(275, 40);
            this.txtAttestTown.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtAttestTown, null);
            // 
            // cmdFind
            // 
            this.cmdFind.AppearanceKey = "actionButton";
            this.cmdFind.Image = ((System.Drawing.Image)(resources.GetObject("cmdFind.Image")));
            this.cmdFind.Location = new System.Drawing.Point(255, 322);
            this.cmdFind.Name = "cmdFind";
            this.cmdFind.Size = new System.Drawing.Size(40, 40);
            this.cmdFind.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.cmdFind, "Find Recording Clerk\'s Name");
            this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
            // 
            // txtPageNum
            // 
            this.txtPageNum.AutoSize = false;
            this.txtPageNum.BackColor = System.Drawing.SystemColors.Window;
            this.txtPageNum.LinkItem = null;
            this.txtPageNum.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPageNum.LinkTopic = null;
            this.txtPageNum.Location = new System.Drawing.Point(168, 498);
            this.txtPageNum.Name = "txtPageNum";
            this.txtPageNum.Size = new System.Drawing.Size(127, 40);
            this.txtPageNum.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtPageNum, null);
            // 
            // txtFileNum
            // 
            this.txtFileNum.AutoSize = false;
            this.txtFileNum.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileNum.LinkItem = null;
            this.txtFileNum.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFileNum.LinkTopic = null;
            this.txtFileNum.Location = new System.Drawing.Point(20, 498);
            this.txtFileNum.Name = "txtFileNum";
            this.txtFileNum.Size = new System.Drawing.Size(128, 40);
            this.txtFileNum.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtFileNum, null);
            // 
            // txtTown
            // 
            this.txtTown.AutoSize = false;
            this.txtTown.BackColor = System.Drawing.SystemColors.Window;
            this.txtTown.LinkItem = null;
            this.txtTown.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTown.LinkTopic = null;
            this.txtTown.Location = new System.Drawing.Point(20, 146);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(275, 40);
            this.txtTown.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtTown, null);
            // 
            // txtClerk
            // 
            this.txtClerk.AutoSize = false;
            this.txtClerk.BackColor = System.Drawing.SystemColors.Window;
            this.txtClerk.LinkItem = null;
            this.txtClerk.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtClerk.LinkTopic = null;
            this.txtClerk.Location = new System.Drawing.Point(20, 58);
            this.txtClerk.Name = "txtClerk";
            this.txtClerk.Size = new System.Drawing.Size(275, 40);
            this.txtClerk.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtClerk, null);
            // 
            // txtAttest
            // 
            this.txtAttest.AutoSize = false;
            this.txtAttest.BackColor = System.Drawing.SystemColors.Window;
            this.txtAttest.LinkItem = null;
            this.txtAttest.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAttest.LinkTopic = null;
            this.txtAttest.Location = new System.Drawing.Point(20, 322);
            this.txtAttest.Name = "txtAttest";
            this.txtAttest.Size = new System.Drawing.Size(215, 40);
            this.txtAttest.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtAttest, null);
            // 
            // mebToDate
            // 
            this.mebToDate.Location = new System.Drawing.Point(153, 234);
            this.mebToDate.Mask = "00/00/0000";
            this.mebToDate.Name = "mebToDate";
            this.mebToDate.Size = new System.Drawing.Size(115, 40);
            this.mebToDate.TabIndex = 3;
            this.mebToDate.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.mebToDate, null);
            // 
            // mebFromDate
            // 
            this.mebFromDate.Location = new System.Drawing.Point(20, 234);
            this.mebFromDate.Mask = "00/00/0000";
            this.mebFromDate.Name = "mebFromDate";
            this.mebFromDate.Size = new System.Drawing.Size(115, 40);
            this.mebFromDate.TabIndex = 2;
            this.mebFromDate.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.mebFromDate, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 382);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(175, 15);
            this.Label8.TabIndex = 21;
            this.Label8.Text = "TOWN";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 294);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(175, 15);
            this.Label1.TabIndex = 17;
            this.Label1.Text = "ATTESTED BY";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(175, 15);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "NAME OF CLERK";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 118);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(175, 15);
            this.Label3.TabIndex = 15;
            this.Label3.Text = "TOWN OF CLERK";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 206);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(100, 15);
            this.Label4.TabIndex = 14;
            this.Label4.Text = "CLERK FROM";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(153, 206);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(72, 15);
            this.Label5.TabIndex = 13;
            this.Label5.Text = "CLERK TO";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 470);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(78, 15);
            this.Label6.TabIndex = 12;
            this.Label6.Text = "FOUND IN";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(168, 470);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(78, 15);
            this.Label7.TabIndex = 11;
            this.Label7.Text = "ON PAGE";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(110, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(158, 48);
            this.cmdPrint.TabIndex = 19;
            this.cmdPrint.Text = "Print / Preview";
            this.ToolTip1.SetToolTip(this.cmdPrint, null);
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // frmPrintOldVitals
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(388, 688);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPrintOldVitals";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Old Vital Records";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmPrintOldVitals_Load);
            this.Activated += new System.EventHandler(this.frmPrintOldVitals_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintOldVitals_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintOldVitals_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}