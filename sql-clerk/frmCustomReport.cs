//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Clerk.Commands;
using SharedApplication.Clerk.Enums;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmCustomReport : BaseForm
	{
		public frmCustomReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			//FC:FINAL:BB:#327 - show borders for columns
			this.vsLayout.CellBorderStyle = DataGridViewCellBorderStyle.Both;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomReport InstancePtr
		{
			get
			{
				return (frmCustomReport)Sys.GetInstance(typeof(frmCustomReport));
			}
		}

		protected frmCustomReport _InstancePtr = null;
		//=========================================================
		// ****************************************************************************
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomReport.rpt
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		// SetFormFieldCaptions IN modCustomReport.mod
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		//
		// A CALL TO THIS FORM WOULD BE:
		// frmCustomReport.Show
		// Call SetFormFieldCaptions(frmCustomReport, "Births")
		// ****************************************************************************
		int intCounter;
		// vbPorter upgrade warning: intStart As int	OnWriteFCConvert.ToInt32(
		int intStart;
		int intEnd;
		// vbPorter upgrade warning: intID As int	OnWriteFCConvert.ToInt32(
		int intID;
		string strReportName = "";
		ListViewItem strTemp;
		bool boolPrintPreview;
		bool boolSaveReport;
		bool boolDelete;
		double dblDataLength;
        // vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
        double dblSizeRatio;

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsLandscape = new clsDRWrapper();
			if (cmbReport.Text == "Delete Saved Report")
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.Execute("Delete from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGNBas.DEFAULTCLERKDATABASE);
					LoadCombo();
					MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					strReportName = string.Empty;
				}
			}
			else if (cmbReport.Text == "Show Saved Report")
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				vsLayout.Clear();
				vsLayout.Rows = 2;
				vsLayout.Cols = 1;
				rsLandscape.OpenRecordset("SELECT * FROM tblCustomReports WHERE ID = " + FCConvert.ToString(this.cboSavedReport.ItemData(this.cboSavedReport.SelectedIndex)), modGNBas.DEFAULTCLERKDATABASE);
				rs.OpenRecordset("Select * from tblReportLayout where ReportID = " + FCConvert.ToString(this.cboSavedReport.ItemData(this.cboSavedReport.SelectedIndex)) + " Order by RowID, ColumnID", modGNBas.DEFAULTCLERKDATABASE);
				strReportName = cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString();
				if (FCConvert.ToBoolean(rsLandscape.Get_Fields_Boolean("Landscape")))
				{
					chkLandscape.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkLandscape.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				while (!rs.EndOfFile())
				{
					if (FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")) == 1)
					{
						if (rs.Get_Fields_Int16("ColumnID") > 0)
						{
							vsLayout.Cols += 1;
						}
					}
					else
					{
						if (rs.Get_Fields_Int16("RowID") >= vsLayout.Rows)
						{
							vsLayout.Rows += 1;
                            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
                            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
						}
					}
					vsLayout.TextMatrix(rs.Get_Fields_Int16("RowID"), rs.Get_Fields_Int16("ColumnID"), FCConvert.ToString(rs.Get_Fields_String("DisplayText")));
					vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, rs.Get_Fields_Int16("RowID"), rs.Get_Fields_Int16("ColumnID"), rs.Get_Fields("FieldID"));
					vsLayout.ColWidth(rs.Get_Fields_Int16("ColumnID"), FCConvert.ToInt32(rs.Get_Fields("Width")));
					rs.MoveNext();
				}
                cmbReport.Text = "Create New Report";
			}
		}

		private void chkLandscape_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkLandscape.CheckState == Wisej.Web.CheckState.Checked)
			{
				dblDataLength = 10;
			}
			else
			{
				dblDataLength = 7.5;
			}
			//Line1.X1 = FCConvert.ToSingle(dblDataLength * 1440 * dblSizeRatio);
            Line1.Left = FCConvert.ToInt32(dblDataLength * 96);
			// If Image1.Left < 0 Then
			// Line1.X1 = (1440 * dblDataLength * dblSizeRatio) - 750
			// Line1.X2 = (1440 * dblDataLength * dblSizeRatio) - 750
			// Else
			// Line1.X1 = Image1.Left + (1440 * dblDataLength * dblSizeRatio) - 750
			// Line1.X2 = Image1.Left + (1440 * dblDataLength * dblSizeRatio) - 750
			// End If
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY*** IMPORTANT
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			string strReturn;
			clsDRWrapper RSLayout = new clsDRWrapper();
			clsDRWrapper rsSave = new clsDRWrapper();
			int intReportID = 0;
			bool blnLandscape = false;
			strReturn = Interaction.InputBox("Enter name for new report.", "New Custom Report", strReportName);
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
				// NOT SHOW IT
				boolSaveReport = true;
				cmdPrint_Click();
				boolSaveReport = false;
				// SAVE THE REPORT
				rsSave.OpenRecordset("Select * from tblCustomReports where ReportName = '" + strReturn + "' and type = '" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGNBas.DEFAULTCLERKDATABASE);
				if (!rsSave.EndOfFile())
				{
					if (MessageBox.Show("A report by that name already exists. Save new formatted report?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
						intReportID = FCConvert.ToInt16(rsSave.Get_Fields_Int32("ID"));
						rsSave.Execute("Delete from tblCustomReports where ID = " + FCConvert.ToString(intReportID), modGNBas.DEFAULTCLERKDATABASE);
						rsSave.Execute("Delete from tblReportLayout where ReportID = " + FCConvert.ToString(intReportID), modGNBas.DEFAULTCLERKDATABASE);
						blnLandscape = chkLandscape.CheckState == Wisej.Web.CheckState.Checked;
						rsSave.Execute("Insert into tblCustomReports (ReportName,SQL,Type,Landscape) VALUES ('" + strReturn + "','" + FCConvert.ToString(modCustomReport.FixQuotes(modCustomReport.Statics.strCustomSQL)) + "','" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'," + FCConvert.ToString((blnLandscape ? 1 : 0)) + ")", modGNBas.DEFAULTCLERKDATABASE);
						strReportName = strReturn;
						rsSave.OpenRecordset("Select max(ID) as ReportID from tblCustomReports", modGNBas.DEFAULTCLERKDATABASE);
						if (!rsSave.EndOfFile())
						{
							rsSave.Execute("Delete from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGNBas.DEFAULTCLERKDATABASE);
							for (intRow = 1; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
							{
								for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
								{
									RSLayout.OpenRecordset("Select * from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGNBas.DEFAULTCLERKDATABASE);
									RSLayout.AddNew();
									RSLayout.Set_Fields("ReportID", rsSave.Get_Fields("ReportID"));
									RSLayout.Set_Fields("RowID", intRow);
									RSLayout.Set_Fields("ColumnID", intCol);
									if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
									{
										RSLayout.Set_Fields("FieldID", -1);
									}
									else
									{
										RSLayout.Set_Fields("FieldID", FCConvert.ToString(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))));
									}
									RSLayout.Set_Fields("Width", frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol));
									RSLayout.Set_Fields("DisplayText", frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
									RSLayout.Update();
								}
							}
						}
						LoadCombo();
						MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("Report was not saved.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				else
				{
					NewReport:
					;
					blnLandscape = chkLandscape.CheckState == Wisej.Web.CheckState.Checked;
					rsSave.Execute("Insert into tblCustomReports (ReportName,SQL,Type,Landscape) VALUES ('" + strReturn + "','" + FCConvert.ToString(modCustomReport.FixQuotes(modCustomReport.Statics.strCustomSQL)) + "','" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'," + FCConvert.ToString((blnLandscape ? 1 : 0)) + ")", modGNBas.DEFAULTCLERKDATABASE);
					strReportName = strReturn;
					rsSave.OpenRecordset("Select max(ID) as ReportID from tblCustomReports", modGNBas.DEFAULTCLERKDATABASE);
					if (!rsSave.EndOfFile())
					{
						rsSave.Execute("Delete from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGNBas.DEFAULTCLERKDATABASE);
						for (intRow = 1; intRow <= (frmCustomReport.InstancePtr.vsLayout.Rows - 1); intRow++)
						{
							for (intCol = 0; intCol <= (frmCustomReport.InstancePtr.vsLayout.Cols - 1); intCol++)
							{
								RSLayout.OpenRecordset("Select * from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGNBas.DEFAULTCLERKDATABASE);
								RSLayout.AddNew();
								RSLayout.Set_Fields("ReportID", rsSave.Get_Fields("ReportID"));
								RSLayout.Set_Fields("RowID", intRow);
								RSLayout.Set_Fields("ColumnID", intCol);
								if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
								{
									RSLayout.Set_Fields("FieldID", -1);
								}
								else
								{
									RSLayout.Set_Fields("FieldID", FCConvert.ToString(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))));
								}
								RSLayout.Set_Fields("Width", frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol));
								RSLayout.Set_Fields("DisplayText", frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
								RSLayout.Update();
							}
						}
					}
					LoadCombo();
					MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			for (intCounter = 0; intCounter <= (vsWhere.Rows - 1); intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (modGNBas.Statics.strPreSetReport == "Expiring Reminders" || modGNBas.Statics.strPreSetReport == "Dog Reminders" || modGNBas.Statics.strPreSetReport == "Warrant Reminder")
			{
				if (chkPrint.CheckState != Wisej.Web.CheckState.Checked && chkSendEmail.CheckState != Wisej.Web.CheckState.Checked)
				{
					MessageBox.Show("You must choose at least one output format.", "Invalid Output Choice", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (!Information.IsDate(T2KReminderDate.Text))
				{
					MessageBox.Show("You must enter a valid reminder date.", "Invalid Reminder Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			clsDRWrapper rsSQL = new clsDRWrapper();
			// CLEAR THE FIELDS WIDTH ARRAY
			for (intCounter = 0; intCounter <= 49; intCounter++)
			{
				modCustomReport.Statics.strFieldWidth[intCounter] = 0;
			}
			// PREPARE THE SQL TO SHOW THE REPORT
			bool executeNoFields = false;
			if (cmbReport.Text == "Create New Report")
			{
				// BUILD THE SQL FOR THE NEW CUSTOM REPORT
				BuildSQL();
				// IF NO FIELDS WERE CHOSEN TO PRINT THEN DO NOT SHOW THE REPORT
				if (modCustomReport.Statics.intNumberOfSQLFields < 0)
				{
					executeNoFields = true;
					goto NoFields;
				}
			}
			else
			{
				// IF THE USER DOES NOT CHOSE A REPORT THEN DO NOT SHOW ONE
				if (cboSavedReport.SelectedIndex < 0)
					return;
				// GET THE SAVED SQL STATEMENT FOR THE CUSTOM REPORT
				rsSQL.OpenRecordset("Select SQL from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGNBas.DEFAULTCLERKDATABASE);
				if (!rsSQL.EndOfFile())
				{
					modCustomReport.Statics.strCustomSQL = FCConvert.ToString(rsSQL.Get_Fields_String("SQL"));
				}
			}
			// GET THE NUMBER OF FIELDS TO DISPLAY AND FILL THE FIELDS CAPTION
			// ARRAY WITH THE DATABASE FIELD NAMES FOR THOSE THAT WERE CHOSEN
			modCustomReport.GetNumberOfFields(modCustomReport.Statics.strCustomSQL);
			if (modGNBas.Statics.strPreSetReport == string.Empty)
			{
				if (modCustomReport.Statics.intNumberOfSQLFields < 0)
				{
					executeNoFields = true;
					goto NoFields;
				}
			}
			NoFields:
			;
			if (executeNoFields)
			{
				executeNoFields = false;
				MessageBox.Show("No fields were selected to display.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			// GET THE CAPTIONS TO DISPLAY ON THE REPORT. THE DEVELOPER MAY
			// WANT THE DISPLAYED 'HEADINGS' TO BE DIFFERENT THEN THE DATABASE
			// FIELD NAMES OR THE CUSTOM REPORT FORM'S DISPLAY NAME
			// Call SetColumnCaptions(Me)
			if (!boolSaveReport)
			{
				if (modGNBas.Statics.strPreSetReport == "Dog Reminders")
				{
                    // rptDogReminders.Show 
                    // Call frmReportViewer.Init(rptDogReminders)
                    rptDogReminders.InstancePtr.Init(false, this.Modal, 0, true, chkSendEmail.CheckState == Wisej.Web.CheckState.Checked, chkPrint.CheckState == Wisej.Web.CheckState.Checked, cmbPrint.Text == "All", T2KReminderDate.Text);
				}
				else if (modGNBas.Statics.strPreSetReport == "Warrant Reminder")
				{
					rptDogReminders.InstancePtr.Init(false, this.Modal, 0, false, chkSendEmail.CheckState == Wisej.Web.CheckState.Checked, chkPrint.CheckState == Wisej.Web.CheckState.Checked, cmbPrint.Text == "All", T2KReminderDate.Text);
				}
				else if (modGNBas.Statics.strPreSetReport == "Expiring Reminders")
				{
					// rptExpiringDogReminders.Show 
					// Call frmReportViewer.Init(rptExpiringDogReminders)
					rptExpiringDogReminders.InstancePtr.Init(false, 0, "", chkSendEmail.CheckState == Wisej.Web.CheckState.Checked, chkPrint.CheckState == Wisej.Web.CheckState.Checked, cmbPrint.Text == "All", T2KReminderDate.Text);
				}
				else
				{
					// SHOW THE REPORT
					if (boolPrintPreview)
					{
						rptCustomReport.InstancePtr.Name = this.Text;
						// rptCustomReport.Show 
						frmReportViewer.InstancePtr.Init(rptCustomReport.InstancePtr, boolAllowEmail: true, strAttachmentName: "CustomReport");
					}
					else
					{
						rptCustomReport.InstancePtr.PrintReport(false);
					}
				}
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		public void BuildSQL()
		{
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			int intCounter;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			string[] strSelectedFields = new string[500 + 1];
			clsDRWrapper rsTemp = new clsDRWrapper();
			vsWhere.Select(0, 0);
			// CLEAR OUT VARIABLES
			modCustomReport.Statics.intNumberOfSQLFields = 0;
			modCustomReport.Statics.strCustomSQL = string.Empty;
			// GET THE FIELD NAMES THAT THE USER HAS SELECTED
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				strSelectedFields[intCounter] = string.Empty;
			}
			for (intRow = 1; intRow <= (vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (vsLayout.Cols - 1); intCol++)
				{
					for (intCounter = 0; intCounter <= 499; intCounter++)
					{
						if (strSelectedFields[intCounter] == string.Empty)
						{
							modCustomReport.Statics.intNumberOfSQLFields += 1;
							strSelectedFields[intCounter] = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
							break;
						}
						else if (strSelectedFields[intCounter] == vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol))
						{
							goto NextCell;
						}
					}
					NextCell:
					;
				}
			}
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				if (strSelectedFields[intCounter] == string.Empty)
				{
					goto Continue;
				}
				else
				{
					for (intRow = 0; intRow <= lstFields.Items.Count - 1; intRow++)
					{
						if (lstFields.Items[intRow].Text == strSelectedFields[intCounter])
						{
							if (modCustomReport.Statics.strCustomSQL != string.Empty)
								modCustomReport.Statics.strCustomSQL += ", ";
							if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) == "DOGS" && fecherFoundation.Strings.UCase(modCustomReport.Statics.strFields[lstFields.ItemData(intRow)]) == "NOREQUIREDFIELDS")
							{
								modCustomReport.Statics.strCustomSQL += " doginfo.norequiredfields ";
							}
							else
							{
								modCustomReport.Statics.strCustomSQL += modCustomReport.Statics.strFields[lstFields.ItemData(intRow)];
								if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strFields[lstFields.ItemData(intRow)]) == "DATEOFDEATH" && Strings.Left(fecherFoundation.Strings.UCase(modGNBas.Statics.strPreSetReport) + "     ", 5) == "DEATH")
								{
									modCustomReport.Statics.strCustomSQL += ", DateOfDeathDescription";
								}
							}
							break;
						}
					}
				}
			}
			Continue:
			;
			// CHECK TO SEE IF ANY FIELDS WERE SELCTED. IF THEY WERE THEN THE
			// STRCUSTOMSQL WOULD HAVE SOME DATA IN IT
			if (modGNBas.Statics.strPreSetReport == string.Empty)
			{
				if (fecherFoundation.Strings.Trim(modCustomReport.Statics.strCustomSQL) == string.Empty)
				{
					modCustomReport.Statics.intNumberOfSQLFields = -1;
					return;
				}
				// End If
				BuildFraFieldsTag();
			}
			else
			{
				if ((fecherFoundation.Strings.UCase(modGNBas.Statics.strPreSetReport) == "EXPIRING REMINDERS") || (fecherFoundation.Strings.UCase(modGNBas.Statics.strPreSetReport) == "DOG REMINDERS") || (fecherFoundation.Strings.UCase(modGNBas.Statics.strPreSetReport) == "WARRANT REMINDER"))
				{
					fraFields.Tag = "";
					// kk04172015 trocks-15   strCustomSQL = " c.*, p.* from dogowner as c CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'CK', NULL) as p "
					modCustomReport.Statics.strCustomSQL = " c.*, p.*, p.State AS PartyState from dogowner as c LEFT JOIN " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID = p.PartyID ";
					// strCustomSQL = " * from dogowner inner join doginfo on (dogowner.ID = doginfo.ownernum) "
					// strCustomSQL = " *,STATES.STATE AS newstate "
					// Case "DOG REMINDERS"
					// strCustomSQL = " * from dogowner inner join doginfo on (dogowner.ID = doginfo.ownernum) "
					// strCustomSQL = " *,states.state as newstate "
				}
				else if (fecherFoundation.Strings.UCase(modGNBas.Statics.strPreSetReport) == "MARRIAGE")
				{
					modCustomReport.Statics.strCustomSQL = " * ";
				}
			}

			if (modCustomReport.Statics.strCustomSQL == "")
			{
				modCustomReport.Statics.strCustomSQL = " * ";
			}
			// BuildFraFieldsTag
			// CREATE THE SQL STATEMENT WITH THE SELECTED FIELDS
			// strCustomSQL = "SELECT " & strCustomSQL & " " & fraFields.Tag
			if (mnuEliminateDuplicates.Checked)
			{
				modCustomReport.Statics.strCustomSQL = "Select distinct " + modCustomReport.Statics.strCustomSQL + " " + fraFields.Tag;
			}
			else
			{
				modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + " " + fraFields.Tag;
			}
			// BUILD A WHERE CLAUSE TO APPEND TO THE SQL STATEMENT
			BuildWhereParameter();
			// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
			BuildSortParameter();
		}

		private void mnuEliminateDuplicates_Click(object sender, System.EventArgs e)
		{
			mnuEliminateDuplicates.Checked = !mnuEliminateDuplicates.Checked;
		}

		private void BuildFraFieldsTag()
		{
			// check what options have been selected and see what tables need to be included
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
			int intRow;
			string strSQL = "";
			bool boolColors;
			bool boolBreeds;
			bool boolDogowner;
			bool boolStates;
			bool boolDogTransactions;
			bool boolAny;
			// vbPorter upgrade warning: intType As int	OnWriteFCConvert.ToInt32(
			int intType;
			bool boolFirst = false;
			bool boolDeleted;
			clsDRWrapper rsTemp = new clsDRWrapper();
			boolAny = false;
			boolColors = false;
			boolBreeds = false;
			boolDogowner = false;
			boolStates = false;
			boolDogTransactions = false;
			boolDeleted = false;
			// check what we selected by
			for (x = 0; x <= (vsWhere.Rows - 1); x++)
			{
				if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, x, 1)) != string.Empty)
				{
					if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) == "DOGS" && x == 29)
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, x, 1))) == "T")
						{
							boolDeleted = true;
						}
					}
					if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[x]) == "DEFAULTCOLORS")
					{
						boolAny = true;
						boolColors = true;
					}
					else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[x]) == "DEFAULTBREEDS")
					{
						boolAny = true;
						boolBreeds = true;
					}
					else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[x]) == "STATES")
					{
						boolAny = true;
						boolStates = true;
						boolDogowner = true;
					}
					else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[x]) == "DOGOWNER")
					{
						boolAny = true;
						boolDogowner = true;
					}
					else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[x]) == "DOGTRANSACTIONS")
					{
						boolAny = true;
						boolDogTransactions = true;
					}
				}
			}
			// x
			// check what we sorted by
			for (x = 0; x <= lstSort.Items.Count - 1; x++)
			{
				if (lstSort.Selected(x))
				{
					intType = lstSort.ItemData(x);
					if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[intType]) == "DEFAULTCOLORS")
					{
						boolAny = true;
						boolColors = true;
					}
					else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[intType]) == "DEFAULTBREEDS")
					{
						boolAny = true;
						boolBreeds = true;
					}
					else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[intType]) == "STATES")
					{
						boolAny = true;
						boolStates = true;
						boolDogowner = true;
					}
					else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[intType]) == "DOGOWNER")
					{
						boolAny = true;
						boolDogowner = true;
					}
					else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[intType]) == "DOGTRANSACTIONS")
					{
						boolAny = true;
						boolDogTransactions = true;
					}
				}
			}
			// x
			// now check what info we are showing
			for (intRow = 1; intRow <= (vsLayout.Rows - 1); intRow++)
			{
				for (intCol = 0; intCol <= (vsLayout.Cols - 1); intCol++)
				{
					if (Conversion.Val(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) >= 0)
					{
						if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == "DEFAULTCOLORS")
						{
							boolAny = true;
							boolColors = true;
						}
						else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == "DEFAULTBREEDS")
						{
							boolAny = true;
							boolBreeds = true;
						}
						else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == "STATES")
						{
							boolAny = true;
							boolStates = true;
							boolDogowner = true;
						}
						else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == "DOGOWNER")
						{
							boolAny = true;
							boolDogowner = true;
						}
						else if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strTableNames[vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == "DOGTRANSACTIONS")
						{
							boolAny = true;
							boolDogTransactions = true;
						}
					}
				}
				// intCol
			}
			// intRow
			if (modGNBas.Statics.strPreSetReport == "Dog Reminders" || modGNBas.Statics.strPreSetReport == "Expiring Reminders" || modGNBas.Statics.strPreSetReport == "Warrant Reminder")
			{
				boolDogowner = true;
				boolStates = true;
				boolAny = true;
			}
			// now we know what tables were used.
			if (!boolAny)
			{
				strSQL = " ,kenneldog,ownernum,ID from doginfo ";
			}
			else
			{
				boolFirst = true;
				strSQL = "";
				if (boolColors)
				{
					strSQL = " Doginfo ";
					boolFirst = false;
				}
				if (boolBreeds)
				{
					if (boolFirst)
					{
						strSQL = " doginfo ";
					}
					else
					{
						// strSQL = "(" & strSQL & " inner join defaultbreeds on val(doginfo.dogbreed) = defaultbreeds.id)"
					}
					boolFirst = false;
				}
				if (boolDogowner)
				{
					if (!boolDeleted)
					{
						if (boolFirst)
						{
							// kk04172015 trocks-15  strSQL = "(doginfo inner join dogowner on doginfo.ownernum = dogowner.ID) CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddressAndPhone(PartyID, NULL, 'CK', NULL) as p"
							strSQL = "(doginfo inner join dogowner on doginfo.ownernum = dogowner.ID) LEFT JOIN " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView AS p ON dogowner.PartyID = p.PartyID ";
						}
						else
						{
							// kk04172105 trocks-15  strSQL = "(" & strSQL & " inner join dogowner on doginfo.ownernum = dogowner.ID) CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddressAndPhone(PartyID, NULL, 'CK', NULL) as p"
							strSQL = "(" + strSQL + " inner join dogowner on doginfo.ownernum = dogowner.ID) LEFT JOIN " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView AS p ON dogowner.PartyID = p.PartyID ";
						}
					}
					else
					{
						if (boolFirst)
						{
							// kk04172015 trocks-15  strSQL = "(doginfo inner join dogowner on doginfo.previousowner = dogowner.ID) CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddressAndPhone(PartyID, NULL, 'CK', NULL) as p"
							strSQL = "(doginfo inner join dogowner on doginfo.previousowner = dogowner.ID) LEFT JOIN " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhoneView AS as p ON dogowner.PartyID = p.PartyID ";
						}
						else
						{
							// kk04172015 trocks-15  strSQL = "(" & strSQL & " inner join dogowner on doginfo.PREVIOUSOWNER = dogowner.ID) CROSS APPLY " & rsTemp.CurrentPrefix & "CentralParties.dbo.GetCentralPartyNameAndAddressAndPhone(PartyID, NULL, 'CK', NULL) as p"
							strSQL = "(" + strSQL + " inner join dogowner on doginfo.PREVIOUSOWNER = dogowner.ID) LEFT JOIN " + rsTemp.CurrentPrefix + "CentralParties.dbo.PartyAddressAndPhone as p ON dogowner.PartyID = p.PartyID ";
						}
					}
					boolFirst = false;
				}

				if (boolDogTransactions)
				{
					if (boolFirst)
					{
						strSQL = "(doginfo left join dogtransactions on doginfo.ID = dogtransactions.dognumber)";
					}
					else
					{
						strSQL = "(" + strSQL + " left join dogtransactions on doginfo.ID = dogtransactions.dognumber)";
					}
					boolFirst = false;
				}
				if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) == "DOGS")
				{
					strSQL = ",kenneldog,doginfo.ID from " + strSQL;
				}
				else
				{
					strSQL = " from " + strSQL;
				}
			}
			fraFields.Tag = strSQL;
		}

		public void BuildSortParameter()
		{
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[intCounter]))
					{
						case modCustomReport.GRIDDATE:
							{
								if (strSort != " ")
									strSort += ", ";
								// strSort = strSort & "cdate(" & CheckForAS(strFields(lstSort.ItemData(intCounter)), 1) & " & '')"
								strSort += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)], 1)) + " ";
								break;
							}
						default:
							{
								if (strSort != " ")
									strSort += ", ";
								strSort += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)], 1));
								break;
							}
					}
					//end switch
				}
			}
			// IF THERE WERE SOME FIELDS TO SORT BY THEN APPEND THEM TO THE
			// PREVIOUSLY CREATED SQL STATEMENT
			if (fecherFoundation.Strings.Trim(strSort) != string.Empty)
			{
				modCustomReport.Statics.strCustomSQL += " Order by" + strSort;
			}
		}

		public void BuildWhereParameter()
		{
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			string strTemp = "";
			// CLEAR THE VARIABLES
			strWhere = " ";
			// GET THE FIELDS TO FILTER BY
			for (intCounter = 0; intCounter <= (vsWhere.Rows - 1); intCounter++)
			{
				// NEED TO KNOW WHAT TYPE OF WHERE CLAUSE TO APPEND BY
				switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[intCounter]))
				{
					case modCustomReport.GRIDTEXT:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDDATE:
						{
							// THIS IS A DATE FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS
							// THE POUND SYMBOLS WRAPPED AROUND IT
							// THE P
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								strTemp = FCConvert.ToString(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]), 1));
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									// THE USER HAS FILLED IN BOTH FIELDS FOR THIS DATE
									// FIELD SO WE NOW HAVE A DATE RANGE
									if (strWhere != " ")
										strWhere += " AND ";
									if (fecherFoundation.Strings.UCase(strTemp) != "DATEOFDEATH" || Strings.Left(fecherFoundation.Strings.UCase(modGNBas.Statics.strPreSetReport) + "     ", 5) != "DEATH")
									{
										if (fecherFoundation.Strings.UCase(strTemp) == "CEREMONYDATE")
										{
											strWhere += " ((isdate(" + strTemp + ") = 1 and ";
											strWhere += "convert(datetime," + strTemp + ") between '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "') OR (ActualDate between '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'))";
										}
										else if (fecherFoundation.Strings.UCase(strTemp) == "GROOMSDOB" || fecherFoundation.Strings.UCase(strTemp) == "BRIDESDOB" || fecherFoundation.Strings.UCase(strTemp) == "DATEOFBIRTH")
										{
											var result = StaticSettings.GlobalCommandDispatcher.Send(new GetBirthIdsBasedOnBirthDate
											{
												Type = fecherFoundation.Strings.UCase(strTemp) == "GROOMSDOB" ? BirthDateType.MarraigeGroom : fecherFoundation.Strings.UCase(strTemp) == "BRIDESDOB" ? BirthDateType.MarraigeBride : BirthDateType.Death,
												StartDate = Convert.ToDateTime(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)),
												EndDate = Convert.ToDateTime(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2))
											}).Result;

											string ids = "(";
											foreach (var birthMatch in result)
											{
												ids += (birthMatch + ", ");
											}
											if (ids == "(")
											{
												ids += "0)";
											}
											else
											{
												ids = ids.Left(ids.Length - 2) + ")";
											}
											strWhere += "Id IN " + ids;
										}
										else
										{
											strWhere += " isdate(" + strTemp + ") = 1 and ";
											strWhere += "convert(datetime," + strTemp + ") between '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'";
										}
									}
									else
									{
										strWhere += " (((isdate(" + strTemp + ") = 1 and ";
										strWhere += "convert(datetime," + strTemp + ") between '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "')";
										strWhere += " or (isdate(dateofdeathdescription) = 1 and ";
										strWhere += " (convert(datetime,dateofdeathdescription) between '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "') ) ";
										strWhere += ") OR (ActualDate between '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "')) ";
									}
								}
								else
								{
									// ONE ONE FIELD WAS FILLED IN FOR THIS FIELD
									// SO WE HAVE THIS BEING AN EQUAL TO CLAUSE
									if (strWhere != " ")
										strWhere += " AND ";

									if (fecherFoundation.Strings.UCase(strTemp) == "GROOMSDOB" || fecherFoundation.Strings.UCase(strTemp) == "BRIDESDOB" || fecherFoundation.Strings.UCase(strTemp) == "DATEOFBIRTH")
									{
										var result = StaticSettings.GlobalCommandDispatcher.Send(new GetBirthIdsBasedOnBirthDate
										{
											Type = fecherFoundation.Strings.UCase(strTemp) == "GROOMSDOB" ? BirthDateType.MarraigeGroom : fecherFoundation.Strings.UCase(strTemp) == "BRIDESDOB" ? BirthDateType.MarraigeBride : BirthDateType.Death,
											StartDate = Convert.ToDateTime(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)),
											EndDate = null
										}).Result;

										string ids = "(";
										foreach (var birthMatch in result)
										{
											ids += (birthMatch + ", ");
										}
										if (ids == "(")
										{
											ids += "0)";
										}
										else
										{
											ids = ids.Left(ids.Length - 2) + ")";
										}
										strWhere += "Id IN " + ids;
									}
									else
									{
										strWhere += strTemp + " = '" +
										            vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay,
											            intCounter, 1) + "'";
									}
								}
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDTEXT:
						{
							// THIS IS OF A TYPE COMBO SO THE DATA IN THE DATABASE
							// IS THE ***ID*** NUMBER FROM ANOTHER TABLE
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " LIKE '" + vsWhere.TextMatrix(intCounter, 3) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDNUM:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								// strWhere = strWhere & Trim(strComboList(intCounter, 1)) & " = " & vsWhere.TextMatrix(intCounter, 3)
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " = " + vsWhere.TextMatrix(intCounter, 3);
							}
							break;
						}
					case modCustomReport.GRIDCOMBOTEXT:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " LIKE '" + vsWhere.TextMatrix(intCounter, 1) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDNUMRANGE:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[intCounter], 1)) + " >= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + " AND " + FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[intCounter], 1)) + " <= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[intCounter], 1)) + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1);
								}
							}
							break;
						}
					case modCustomReport.GRIDTEXTRANGE:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								strTemp = FCConvert.ToString(modCustomReport.CheckForAS(fecherFoundation.Strings.Trim(modCustomReport.Statics.strFields[intCounter]), 1));
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									if ((fecherFoundation.Strings.UCase(strTemp) != "DATEOFBIRTH" && fecherFoundation.Strings.UCase(strTemp) != "CHILDDOB") || !Information.IsDate(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) || !Information.IsDate(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)))
									{
										strWhere += modCustomReport.Statics.strFields[intCounter] + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' AND " + modCustomReport.Statics.strFields[intCounter] + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "zzz'";
									}
									else
									{
										var result = StaticSettings.GlobalCommandDispatcher.Send(new GetBirthIdsBasedOnBirthDate
										{
											Type = fraFields.Tag.ToString().Trim() == "from Births" ? BirthDateType.LiveBirth :
												fraFields.Tag.ToString().Trim() == "from BirthsForeign" ? BirthDateType.ForeignBirth :
												BirthDateType.DelayedBirth,
											StartDate = Convert.ToDateTime(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)),
											EndDate = Convert.ToDateTime(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2))
										}).Result;

										string ids = "(";
										foreach (var birthMatch in result)
										{
											ids += (birthMatch + ", ");
										}
										if (ids == "(")
										{
											ids += "0)";
										}
										else
										{
											ids = ids.Left(ids.Length - 2) + ")";
										}
										strWhere += "Id IN " + ids;
									}
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									if ((fecherFoundation.Strings.UCase(strTemp) != "DATEOFBIRTH" && fecherFoundation.Strings.UCase(strTemp) != "CHILDDOB") || !Information.IsDate(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)))
									{
										strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "%'";
									}
									else
									{
										var result = StaticSettings.GlobalCommandDispatcher.Send(new GetBirthIdsBasedOnBirthDate
										{
											Type = fraFields.Tag.ToString().Trim() == "from Births" ? BirthDateType.LiveBirth :
												fraFields.Tag.ToString().Trim() == "from BirthsForeign" ? BirthDateType.ForeignBirth :
												BirthDateType.DelayedBirth,
											StartDate = Convert.ToDateTime(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)),
											EndDate = null
										}).Result;

										string ids = "(";
										foreach (var birthMatch in result)
										{
											ids += (birthMatch + ", ");
										}

										if (ids == "(")
										{
											ids += "0)";
										}
										else
										{
											ids = ids.Left(ids.Length - 2) + ")";
										}
										strWhere += "Id IN " + ids;

										//if (fecherFoundation.Strings.UCase(strTemp) == "DATEOFBIRTH")
										//{
										//	strWhere += " (( isdate(dateofbirth) = 1 and convert(datetime,dateofbirth) =  '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "') or (ActualDate =  '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "')) ";
										//}
										//else
										//{
										//	strWhere += " (( isdate(childdob) = 1 and convert(datetime,childdob) =  '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "') OR (ActualDate =  '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "')) ";
										//}
									}
								}
							}
							break;
						}
					case modCustomReport.GRIDBOOLEAN:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if ((fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) != "DOGS" && fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) != "DOGSLABELS") || (intCounter != 29 && intCounter != 30 && intCounter != 32))
								{
									if (modCustomReport.Statics.strFields[intCounter] == "NoRequiredFields")
									{
										modCustomReport.Statics.strFields[intCounter] = "doginfo.NoRequiredFields";
									}

									if (fecherFoundation.Strings.UCase(modCustomReport.Statics.strFields[intCounter]) != "LEGITIMATE")
									{
										if (strWhere != " ")
											strWhere += " AND ";
                                        strWhere += modCustomReport.Statics.strFields[intCounter] + (fecherFoundation.Strings.Trim(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))) == "T" ? " = 1 " : " = false");
                                    }
									else
									{
										if (strWhere != " ")
											strWhere += " and ";
										if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)))) == "T")
										{
											strWhere += " not " + modCustomReport.Statics.strFields[intCounter] + " ";
										}
										else
										{
											strWhere += modCustomReport.Statics.strFields[intCounter] + " ";
										}
									}
									// End If
									if (modCustomReport.Statics.strFields[intCounter] == "doginfo.NoRequiredFields")
									{
										modCustomReport.Statics.strFields[intCounter] = "NoRequiredFields";
									}
								}
								else if (intCounter == 29)
								{
									if (fecherFoundation.Strings.Trim(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))) == "T")
									{
										if (fecherFoundation.Strings.Trim(strWhere) != string.Empty)
											strWhere += " and ";
										strWhere += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[intCounter])) + " = -1 ";
									}
									else
									{
										if (fecherFoundation.Strings.Trim(strWhere) != string.Empty)
											strWhere += " and ";
										strWhere += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[intCounter])) + " <> -1 ";
									}
								}
								else if (intCounter == 30)
								{
									if (fecherFoundation.Strings.Trim(strWhere) != string.Empty)
										strWhere += " and ";
									if (fecherFoundation.Strings.Trim(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))) == "T")
									{
										strWhere += " [year] >= " + FCConvert.ToString(DateTime.Today.Year) + " ";
									}
									else
									{
										strWhere += " [year] < " + FCConvert.ToString(DateTime.Today.Year) + " ";
									}
								}
								else if (intCounter == 32)
								{
									if (fecherFoundation.Strings.Trim(strWhere) != string.Empty)
										strWhere += " and ";
									if (fecherFoundation.Strings.Trim(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))) == "T")
									{
										strWhere += " isnull(EMail,'')  <> '' ";
									}
									else
									{
										strWhere += " isnull(EMail,'') = '' ";
									}
								}
							}
							break;
						}
				}
				//end switch
			}
			// IF SOME WHERE PARAMETERS WHERE CHOSEN THEN APPEND THIS
			// TO THE SQL STATEMENT
			if (modGNBas.Statics.strPreSetReport == "DOGINVENTORY")
			{
				if (strWhere != " ")
				{
					modCustomReport.Statics.strCustomSQL += " WHERE Type = 'T' AND" + strWhere;
				}
				else
				{
					modCustomReport.Statics.strCustomSQL += " WHERE Type = 'T'";
				}
			}
			else
			{
				if (strWhere != " ")
				{
					modCustomReport.Statics.strCustomSQL += " WHERE" + strWhere;
				}
			}
		}

		private void frmCustomReport_Activated(object sender, System.EventArgs e)
		{
			ResizeVSWhere();
		}

		private void frmCustomReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F2:
					{
						lstFields.Focus();
						mnuAddRow_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F3:
					{
						lstFields.Focus();
						AddColumn();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F4:
					{
						lstFields.Focus();
						mnuDeleteRow_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F5:
					{
						lstFields.Focus();
						mnuDeleteColumn_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Escape:
					{
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmCustomReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomReport properties;
			//frmCustomReport.ScaleWidth	= 9045;
			//frmCustomReport.ScaleHeight	= 7380;
			//frmCustomReport.LinkTopic	= "Form1";
			//frmCustomReport.LockControls	= -1  'True;
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			this.Text = modCustomReport.Statics.strCustomTitle;
			// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
			// OF THE MDI PARENT FORM
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			// SET THE PROPERTIES FOR THE REPORT LAYOUT SECTION
			//Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			vsLayout.Rows = 2;
			vsLayout.Cols = 1;
			//Image1.WidthOriginal = 1440 * 14;
			//Image1.Image = ImageList1.Images[0];
			//HScroll1.Minimum = 0;
			//if (Image1.WidthOriginal > Frame1.WidthOriginal)
			//{
			//	HScroll1.Maximum = Image1.WidthOriginal - Frame1.WidthOriginal;
			//}
			//else
			//{
			//	HScroll1.Enabled = false;
			//}
			//vsLayout.WidthOriginal = Image1.WidthOriginal - 720;
			vsLayout.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
			//vsLayout.MergeRow(1, true);
			vsLayout.Left = Image1.Left;
			dblDataLength = 7.5F;
			//Line1.X1 = FCConvert.ToSingle(dblDataLength * 1440 * dblSizeRatio);
			//Line1.X2 = Line1.X1;
			//Line1.Y2 = vsLayout.TopOriginal + vsLayout.HeightOriginal;
			// fraLayout.Enabled = strPreSetReport = vbNullString
			// fraFields.Enabled = strPreSetReport = vbNullString
			framEmailReminders.Visible = false;
			if (modGNBas.Statics.strPreSetReport == string.Empty)
			{
				fraSort.Enabled = true;
				fraFields.Enabled = true;
				fraReports.Enabled = true;
				fraMessage.Visible = false;
				vsLayout.Visible = true;
				chkLandscape.Visible = true;
			}
			else if (modGNBas.Statics.strPreSetReport == "Expiring Reminders" || modGNBas.Statics.strPreSetReport == "Dog Reminders" || modGNBas.Statics.strPreSetReport == "Warrant Reminder")
			{
				fraSort.Enabled = true;
				fraFields.Enabled = false;
				fraReports.Visible = false;
				fraMessage.Visible = true;
				vsLayout.Visible = false;
				chkLandscape.Visible = false;
				btnPrint.Visible = false;
				btnAddRow.Visible = false;
				btnAddColumn.Visible = false;
				btnDeleteColumn.Visible = false;
				btnDeleteRow.Visible = false;
				framEmailReminders.Visible = true;
				T2KReminderDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			else if (modGNBas.Statics.strPreSetReport == "MARRIAGE" || modGNBas.Statics.strPreSetReport == "DEATH" || modGNBas.Statics.strPreSetReport == "GROUP" || modGNBas.Statics.strPreSetReport == "BIRTHS" || modGNBas.Statics.strPreSetReport == "DELAYEDBIRTH" || modGNBas.Statics.strPreSetReport == "FOREIGNBIRTH" || fecherFoundation.Strings.UCase(modGNBas.Statics.strPreSetReport) == "BURIAL")
			{
				fraSort.Enabled = true;
				fraFields.Enabled = true;
				fraReports.Enabled = true;
				fraMessage.Visible = false;
				vsLayout.Visible = true;
				chkLandscape.Visible = false;
			}
			else
			{
				fraSort.Enabled = false;
				fraFields.Enabled = false;
				fraReports.Visible = false;
				fraMessage.Visible = true;
				vsLayout.Visible = false;
				chkLandscape.Visible = false;
			}
			// 
			// fraNotes.Enabled = strPreSetReport = vbNullString
			// fraReports.Enabled = strPreSetReport = vbNullString
			// fraMessage.Visible = Not strPreSetReport = vbNullString
			strReportName = string.Empty;
			//FC:FINAL:DDU:#i2301 - fixed show of list index
			lstFields.SelectedIndex = -1;

            lstSort.Columns[0].Width = (int)(lstSort.Width * 0.93);
            lstSort.GridLineStyle = GridLineStyle.None;

            lstFields.Columns[0].Width = (int)(lstFields.Width * 0.93);
            lstFields.GridLineStyle = GridLineStyle.None;
        }

		private void frmCustomReport_Resize(object sender, System.EventArgs e)
		{
			dblSizeRatio = Line2.X2 / 1440;
			//Line1.X1 = FCConvert.ToSingle(dblDataLength * 1440 * dblSizeRatio);
			//Line1.X2 = Line1.X1;
			ResizeVSWhere();
		}

		private void ResizeVSWhere()
		{
			int GridWidth = 0;
			GridWidth = vsWhere.WidthOriginal;
			vsWhere.ColWidth(0, FCConvert.ToInt32(0.33 * GridWidth));
			vsWhere.ColWidth(1, FCConvert.ToInt32(0.33 * GridWidth));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGNBas.Statics.strPreSetReport = string.Empty;
			//MDIParent.InstancePtr.Focus();
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//Image1.LeftOriginal = -HScroll1.Value + 30;
			// vsLayout.Left = 720 + (-HScroll1.Value + 30)
			vsLayout.Left = Image1.Left;
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			//Image1.LeftOriginal = -HScroll1.Value + 30;
			// vsLayout.Left = 720 + (-HScroll1.Value + 30)
			vsLayout.Left = Image1.Left;
		}

		public void LoadCombo()
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			rsReports.OpenRecordset("Select * from tblCustomReports where Type = '" + fecherFoundation.Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGNBas.DEFAULTCLERKDATABASE);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				cboSavedReport.AddItem(rsReports.Get_Fields_String("ReportName"));
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields_Int32("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			// Dim intCounter As Integer
			// For intCounter = 0 To lstFields.ListCount - 1
			// lstFields.Selected(intCounter) = True
			// Next
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			// Dim intCounter As Integer
			// For intCounter = 0 To lstSort.ListCount - 1
			// lstSort.Selected(intCounter) = True
			// Next
			int intCounter;
			int intCount = 0;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					intCount += 1;
				}
				lstSort.SetSelected(intCounter, true);
			}
			if (intCount == lstSort.Items.Count)
			{
				for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
				{
					lstSort.SetSelected(intCounter, false);
				}
			}
			if (lstSort.Items.Count > 0)
				lstSort.SelectedIndex = 0;
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			if (vsLayout.Row < 1)
				vsLayout.Row = 1;
			vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col, lstFields.List(lstFields.ListIndex));
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, lstFields.ItemData(lstFields.SelectedIndex));
		}

		//private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// intStart = lstFields.ListIndex
		//}

		//private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
		//	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	// strtemp = lstFields.List(lstFields.ListIndex)
		//	// intID = lstFields.ItemData(lstFields.ListIndex)
		//	// 
		//	// CHANGE THE NEW ITEM
		//	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
		//	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
		//	// 
		//	// SAVE THE OLD ITEM
		//	// lstFields.List(intStart) = strtemp
		//	// lstFields.ItemData(intStart) = intID
		//	// 
		//	// SET BOTH ITEMS TO BE SELECTED
		//	// lstFields.Selected(lstFields.ListIndex) = True
		//	// lstFields.Selected(intStart) = True
		//	// End If
		//}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	bool blnTempSelected = false;
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	if (intStart != lstSort.SelectedIndex)
		//	{
		//		// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//		strTemp = lstSort.Items[lstSort.SelectedIndex];
		//		intID = lstSort.ItemData(lstSort.SelectedIndex);
		//		// CHANGE THE NEW ITEM
		//		lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//		lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//		// SAVE THE OLD ITEM
		//		lstSort.Items[intStart] = strTemp;
		//		lstSort.ItemData(intStart, intID);
		//		// SET BOTH ITEMS TO BE SELECTED
		//		blnTempSelected = lstSort.Selected(lstSort.SelectedIndex);
		//		lstSort.SetSelected(lstSort.ListIndex, lstSort.Selected(intStart));
		//		lstSort.SetSelected(intStart, blnTempSelected);
		//	}
		//}

		private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			AddColumn();
		}

		private void AddColumn()
		{
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
            vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		public void mnuAddRow_Click()
		{
			mnuAddRow_Click(mnuAddRow, new System.EventArgs());
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			vsWhere.Select(0, 0);
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.");
			}
		}

		public void mnuDeleteColumn_Click()
		{
			mnuDeleteColumn_Click(mnuDeleteColumn, new System.EventArgs());
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.");
			}
		}

		public void mnuDeleteRow_Click()
		{
			mnuDeleteRow_Click(mnuDeleteRow, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			boolPrintPreview = false;
			cmdPrint_Click();
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			boolPrintPreview = true;
			cmdPrint_Click();
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			//FC:FINAL:DDU:#2657 - always show delete prompt when selecting a report to delete
			if ((cmbReport.Text == "Show Saved Report") || (cmbReport.Text == "Delete Saved Report"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
            }
            else
            {
                //FC:FINAL:BSE #3970 dropdown should not be visible
                cboSavedReport.Visible = false;
            }

			//FC:FINAL:AM:#i2291 - reset the text in the combo to trigger the SelectedIndexChanged event
			cboSavedReport.Text = "";
			cmdAdd.Visible = Index == 0;
			fraSort.Enabled = Index == 0;
			fraFields.Enabled = Index == 0;
			fraWhere.Enabled = Index == 0;
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (fecherFoundation.Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (fecherFoundation.Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						vsWhere.EditMask = "##/##/####";
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
				case modCustomReport.GRIDCOMBOIDNUM:
				case modCustomReport.GRIDCOMBOTEXT:
				case modCustomReport.GRIDBOOLEAN:
					{
						if (boolDelete)
						{
							boolDelete = false;
						}
						else
						{
							vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						}
						break;
					}
			}
			//end switch
		}

		private void vsWhere_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, string.Empty);
			}
		}

		private void vsWhere_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, string.Empty);
			}
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (vsWhere.Row >= 0)
			{
				if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
				{
					vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
				}
				if (vsWhere.Col == 2)
				{
					//FC:FINAL:BB:#i2301 #349 - form BackColor was changed on web (redesign). Replaced ColorTranslator.ToOle(this.BackColor) with modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND (non editable cells)
					//if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == System.Drawing.ColorTranslator.ToOle(this.BackColor))
					if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsWhere.Col = 1;
					}
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (fecherFoundation.Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
                            vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, string.Empty);
							//vsWhere.Refresh();
							return;
						}
						if (fecherFoundation.Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (fecherFoundation.Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
						if (!FCConvert.ToBoolean(modCustomReport.IsValidDate(vsWhere.EditText)))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}

        private void VsLayout_ColumnStateChanged(object sender, DataGridViewColumnStateChangedEventArgs e)
        {
            if (e.StateChanged == DataGridViewElementStates.Visible)
            {
                ResizeGrid();
            }
        }

        private void VsLayout_ColumnRemoved(object sender, DataGridViewColumnEventArgs e)
        {
            ResizeGrid();
        }

        private void VsLayout_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            ResizeGrid();
        }

        private void ResizeGrid()
        {
            int gridWidth = 0;
            //add grid columns width
            foreach (DataGridViewColumn column in vsLayout.Columns)
            {
                if (column.Visible)
                {
                    gridWidth += column.Width;
                }
            }
            //add grid border
            gridWidth += 2;
            gridWidth = gridWidth < 984 ? 984 : gridWidth;
            vsLayout.Width = gridWidth;
        }
    }
}
