//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmRegistrarNames.
	/// </summary>
	partial class frmRegistrarNames
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCTextBox txtTown;
		public fecherFoundation.FCTextBox txtMiddleName;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCComboBox cboRegistrar;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCButton cmdNew;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtTown = new fecherFoundation.FCTextBox();
			this.txtMiddleName = new fecherFoundation.FCTextBox();
			this.txtLastName = new fecherFoundation.FCTextBox();
			this.cboRegistrar = new fecherFoundation.FCComboBox();
			this.txtFirstName = new fecherFoundation.FCTextBox();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.Label1_4 = new fecherFoundation.FCLabel();
			this.Label1_3 = new fecherFoundation.FCLabel();
			this.Label1_2 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 378);
			this.BottomPanel.Size = new System.Drawing.Size(377, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtTown);
			this.ClientArea.Controls.Add(this.txtMiddleName);
			this.ClientArea.Controls.Add(this.txtLastName);
			this.ClientArea.Controls.Add(this.cboRegistrar);
			this.ClientArea.Controls.Add(this.txtFirstName);
			this.ClientArea.Controls.Add(this.Label1_4);
			this.ClientArea.Controls.Add(this.Label1_3);
			this.ClientArea.Controls.Add(this.Label1_2);
			this.ClientArea.Controls.Add(this.Label1_1);
			this.ClientArea.Controls.Add(this.Label1_0);
			this.ClientArea.Size = new System.Drawing.Size(377, 318);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(377, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(154, 30);
			this.HeaderText.Text = "Clerk Names";
			// 
			// txtTown
			// 
			this.txtTown.AutoSize = false;
			this.txtTown.BackColor = System.Drawing.SystemColors.Window;
			this.txtTown.LinkItem = null;
			this.txtTown.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTown.LinkTopic = null;
			this.txtTown.Location = new System.Drawing.Point(143, 270);
			this.txtTown.Name = "txtTown";
			this.txtTown.Size = new System.Drawing.Size(206, 40);
			this.txtTown.TabIndex = 13;
			this.txtTown.Validating += new System.ComponentModel.CancelEventHandler(this.txtTown_Validating);
			// 
			// txtMiddleName
			// 
			this.txtMiddleName.AutoSize = false;
			this.txtMiddleName.BackColor = System.Drawing.SystemColors.Window;
			this.txtMiddleName.LinkItem = null;
			this.txtMiddleName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMiddleName.LinkTopic = null;
			this.txtMiddleName.Location = new System.Drawing.Point(143, 150);
			this.txtMiddleName.Name = "txtMiddleName";
			this.txtMiddleName.Size = new System.Drawing.Size(206, 40);
			this.txtMiddleName.TabIndex = 2;
			this.txtMiddleName.Validating += new System.ComponentModel.CancelEventHandler(this.txtMiddleName_Validating);
			// 
			// txtLastName
			// 
			this.txtLastName.AutoSize = false;
			this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
			this.txtLastName.LinkItem = null;
			this.txtLastName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLastName.LinkTopic = null;
			this.txtLastName.Location = new System.Drawing.Point(143, 210);
			this.txtLastName.Name = "txtLastName";
			this.txtLastName.Size = new System.Drawing.Size(206, 40);
			this.txtLastName.TabIndex = 3;
			this.txtLastName.Validating += new System.ComponentModel.CancelEventHandler(this.txtLastName_Validating);
			// 
			// cboRegistrar
			// 
			this.cboRegistrar.AutoSize = false;
			this.cboRegistrar.BackColor = System.Drawing.SystemColors.Window;
			this.cboRegistrar.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboRegistrar.FormattingEnabled = true;
			this.cboRegistrar.Location = new System.Drawing.Point(30, 30);
			this.cboRegistrar.Name = "cboRegistrar";
			this.cboRegistrar.Size = new System.Drawing.Size(319, 40);
			this.cboRegistrar.Sorted = true;
			this.cboRegistrar.TabIndex = 0;
			this.cboRegistrar.SelectedIndexChanged += new System.EventHandler(this.cboRegistrar_SelectedIndexChanged);
			// 
			// txtFirstName
			// 
			this.txtFirstName.AutoSize = false;
			this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFirstName.LinkItem = null;
			this.txtFirstName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFirstName.LinkTopic = null;
			this.txtFirstName.Location = new System.Drawing.Point(143, 90);
			this.txtFirstName.Name = "txtFirstName";
			this.txtFirstName.Size = new System.Drawing.Size(206, 40);
			this.txtFirstName.TabIndex = 1;
			this.txtFirstName.Validating += new System.ComponentModel.CancelEventHandler(this.txtFirstName_Validating);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(239, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(47, 24);
			this.cmdNew.TabIndex = 4;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(151, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 8;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// Label1_4
			// 
			this.Label1_4.Location = new System.Drawing.Point(30, 284);
			this.Label1_4.Name = "Label1_4";
			this.Label1_4.Size = new System.Drawing.Size(60, 18);
			this.Label1_4.TabIndex = 12;
			this.Label1_4.Text = "TOWN";
			// 
			// Label1_3
			// 
			this.Label1_3.Location = new System.Drawing.Point(30, 164);
			this.Label1_3.Name = "Label1_3";
			this.Label1_3.Size = new System.Drawing.Size(64, 15);
			this.Label1_3.TabIndex = 11;
			this.Label1_3.Text = "MIDDLE";
			// 
			// Label1_2
			// 
			this.Label1_2.Location = new System.Drawing.Point(30, 224);
			this.Label1_2.Name = "Label1_2";
			this.Label1_2.Size = new System.Drawing.Size(60, 18);
			this.Label1_2.TabIndex = 10;
			this.Label1_2.Text = "LAST";
			// 
			// Label1_1
			// 
			this.Label1_1.BorderStyle = 1;
			this.Label1_1.Location = new System.Drawing.Point(30, 254);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(319, 25);
			this.Label1_1.TabIndex = 9;
			this.Label1_1.Text = "A SPACE IN THE NAME WILL BE THE SEPERATOR. A FIRST NAME, AND LAST NAME MUST BE SP" +
    "ECIFIED. A MIDDLE NAME IS OPTIONAL";
			this.Label1_1.Visible = false;
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(30, 104);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(65, 15);
			this.Label1_0.TabIndex = 6;
			this.Label1_0.Text = "FIRST";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete,
            this.mnuSP2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 0;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 2;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 3;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                                ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 4;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit              ";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 5;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 6;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(292, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(57, 24);
			this.cmdDelete.TabIndex = 5;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // frmRegistrarNames
            // 
            this.AcceptButton = this.cmdSave;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(377, 486);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmRegistrarNames";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Clerk Names";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmRegistrarNames_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRegistrarNames_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRegistrarNames_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdDelete;
    }
}