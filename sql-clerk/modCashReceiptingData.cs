﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCK0000
{
	public class modCashReceiptingData
	{
		//=========================================================
		public struct CRData
		{
			public int ID;
			// vbPorter upgrade warning: Name As FixedString	OnWrite(string)
			public string Name;
			// vbPorter upgrade warning: Reference As FixedString	OnWrite(string)
			public string Reference;
			// vbPorter upgrade warning: Control1 As FixedString	OnWrite(string, int)
			public string Control1;
			// vbPorter upgrade warning: Control2 As FixedString	OnWrite(string)
			public string Control2;
			// vbPorter upgrade warning: Type As FixedString	OnWrite(string)
			public string Type;
			// vbPorter upgrade warning: Amount1 As double	OnWrite(string)
			public double Amount1;
			// vbPorter upgrade warning: Amount2 As double	OnWrite(string)
			public double Amount2;
			// vbPorter upgrade warning: Amount3 As double	OnWrite(string)
			public double Amount3;
			// vbPorter upgrade warning: Amount4 As double	OnWrite(string)
			public double Amount4;
			// vbPorter upgrade warning: Amount5 As double	OnWrite(string)
			public double Amount5;
			// vbPorter upgrade warning: Amount6 As double	OnWrite(string)
			public double Amount6;
			// vbPorter upgrade warning: ProcessReceipt As FixedString	OnWrite(string)
			public string ProcessReceipt;
			public int NumFirsts;
			// number copies
			public int NumSubsequents;
			// number of same day duplicates
			public int NumSeconds;
			// used only with vets
			public int TownCode;
			public int PartyID;
		};

		public static void WriteCashReceiptingData(ref CRData TypeArray)
		{
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "ID", TypeArray.ID.ToString());
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Type", TypeArray.Type);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Amount1", TypeArray.Amount1.ToString());
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Amount2", TypeArray.Amount2.ToString());
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Amount3", TypeArray.Amount3.ToString());
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Amount4", TypeArray.Amount4.ToString());
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Amount5", TypeArray.Amount5.ToString());
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Amount6", TypeArray.Amount6.ToString());
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Ref", fecherFoundation.Strings.Trim(TypeArray.Reference));
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Control1", fecherFoundation.Strings.Trim(TypeArray.Control1));
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Control2", fecherFoundation.Strings.Trim(TypeArray.Control2));
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "Name", fecherFoundation.Strings.Trim(TypeArray.Name));
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "PartyID", TypeArray.PartyID.ToString());
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "ProcessReceipt", fecherFoundation.Strings.Trim(TypeArray.ProcessReceipt));
			if (modGNBas.Statics.boolRegionalTown)
			{
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "ResCode", TypeArray.TownCode.ToString());
			}
			else
			{
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\CK\\", "ResCode", 0.ToString());
			}
		}

		public class StaticVariables
		{
			public CRData typeCRData = new CRData();
			public bool gboolFromDosTrio = false;
			public bool bolFromWindowsCR;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
