//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmReportListing.
	/// </summary>
	partial class frmReportListing
	{
		public fecherFoundation.FCComboBox cmbSort;
		public fecherFoundation.FCLabel lblSort;
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCCheckBox chkServeWarrant;
		public fecherFoundation.FCCheckBox chkPrintDate;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtYear2;
		public fecherFoundation.FCTextBox txtYear1;
		public T2KDateBox txtRabiesDate1;
		public T2KDateBox txtRabiesDate2;
		public fecherFoundation.FCTextBox txtTagNumber2;
		public fecherFoundation.FCTextBox txtBreed;
		public fecherFoundation.FCTextBox txtColor;
		public fecherFoundation.FCTextBox txtTagNumber1;
		public fecherFoundation.FCTextBox txtDogName;
		public fecherFoundation.FCTextBox txtLocation;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkUnregistered;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCLabel lblYear;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClearSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbSort = new fecherFoundation.FCComboBox();
            this.lblSort = new fecherFoundation.FCLabel();
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.chkServeWarrant = new fecherFoundation.FCCheckBox();
            this.chkPrintDate = new fecherFoundation.FCCheckBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtYear2 = new fecherFoundation.FCTextBox();
            this.txtYear1 = new fecherFoundation.FCTextBox();
            this.txtRabiesDate1 = new T2KDateBox();
            this.txtRabiesDate2 = new T2KDateBox();
            this.txtTagNumber2 = new fecherFoundation.FCTextBox();
            this.txtBreed = new fecherFoundation.FCTextBox();
            this.txtColor = new fecherFoundation.FCTextBox();
            this.txtTagNumber1 = new fecherFoundation.FCTextBox();
            this.txtDogName = new fecherFoundation.FCTextBox();
            this.txtLocation = new fecherFoundation.FCTextBox();
            this.txtAddress = new fecherFoundation.FCTextBox();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkUnregistered = new fecherFoundation.FCCheckBox();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.lblYear = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClearSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdClearSearch = new fecherFoundation.FCButton();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkServeWarrant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnregistered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(865, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkServeWarrant);
            this.ClientArea.Controls.Add(this.chkPrintDate);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.cmbSort);
            this.ClientArea.Controls.Add(this.lblSort);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(865, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClearSearch);
            this.TopPanel.Size = new System.Drawing.Size(865, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClearSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(317, 30);
            this.HeaderText.Text = "Registration/Warrant Listing";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbSort
            // 
            this.cmbSort.AutoSize = false;
            this.cmbSort.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSort.FormattingEnabled = true;
            this.cmbSort.Items.AddRange(new object[] {
            "Owners Name",
            "Address",
            "Location",
            "Dog Name",
            "Tag Number",
            "Color",
            "Breed",
            "Rabies Expiration Date",
            "Registration Year"});
            this.cmbSort.Location = new System.Drawing.Point(437, 140);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(255, 40);
            this.cmbSort.TabIndex = 42;
            this.cmbSort.Text = "Owners Name";
            this.ToolTip1.SetToolTip(this.cmbSort, null);
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(321, 154);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(62, 15);
            this.lblSort.TabIndex = 43;
            this.lblSort.Text = "SORT BY";
            this.ToolTip1.SetToolTip(this.lblSort, null);
            // 
            // cmbReport
            // 
            this.cmbReport.AutoSize = false;
            this.cmbReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbReport.FormattingEnabled = true;
            this.cmbReport.Items.AddRange(new object[] {
            "Registration Listing",
            "Warrant Listing"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(221, 40);
            this.cmbReport.TabIndex = 41;
            this.ToolTip1.SetToolTip(this.cmbReport, null);
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // chkServeWarrant
            // 
            this.chkServeWarrant.Location = new System.Drawing.Point(321, 247);
            this.chkServeWarrant.Name = "chkServeWarrant";
            this.chkServeWarrant.Size = new System.Drawing.Size(316, 27);
            this.chkServeWarrant.TabIndex = 41;
            this.chkServeWarrant.Text = "Mark records as being served a warrant";
            this.ToolTip1.SetToolTip(this.chkServeWarrant, "Warrant fee will be automatically selected when these dogs are re-registered");
            this.chkServeWarrant.Visible = false;
            // 
            // chkPrintDate
            // 
            this.chkPrintDate.Location = new System.Drawing.Point(321, 200);
            this.chkPrintDate.Name = "chkPrintDate";
            this.chkPrintDate.Size = new System.Drawing.Size(232, 27);
            this.chkPrintDate.TabIndex = 39;
            this.chkPrintDate.Text = "Print Issue Date as Date PD";
            this.ToolTip1.SetToolTip(this.chkPrintDate, null);
            this.chkPrintDate.Visible = false;
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtYear2);
            this.Frame3.Controls.Add(this.txtYear1);
            this.Frame3.Controls.Add(this.txtRabiesDate1);
            this.Frame3.Controls.Add(this.txtRabiesDate2);
            this.Frame3.Controls.Add(this.txtTagNumber2);
            this.Frame3.Controls.Add(this.txtBreed);
            this.Frame3.Controls.Add(this.txtColor);
            this.Frame3.Controls.Add(this.txtTagNumber1);
            this.Frame3.Controls.Add(this.txtDogName);
            this.Frame3.Controls.Add(this.txtLocation);
            this.Frame3.Controls.Add(this.txtAddress);
            this.Frame3.Controls.Add(this.txtLastName);
            this.Frame3.Controls.Add(this.Label13);
            this.Frame3.Controls.Add(this.Label12);
            this.Frame3.Controls.Add(this.Label11);
            this.Frame3.Controls.Add(this.Label10);
            this.Frame3.Controls.Add(this.Label9);
            this.Frame3.Controls.Add(this.Label8);
            this.Frame3.Controls.Add(this.Label7);
            this.Frame3.Controls.Add(this.Label6);
            this.Frame3.Controls.Add(this.Label5);
            this.Frame3.Controls.Add(this.Label4);
            this.Frame3.Controls.Add(this.Label3);
            this.Frame3.Controls.Add(this.Label2);
            this.Frame3.Location = new System.Drawing.Point(30, 140);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(271, 822);
            this.Frame3.TabIndex = 24;
            this.Frame3.Text = "Search Criteria";
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // txtYear2
            // 
            this.txtYear2.AutoSize = false;
            this.txtYear2.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear2.LinkItem = null;
            this.txtYear2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtYear2.LinkTopic = null;
            this.txtYear2.Location = new System.Drawing.Point(144, 762);
            this.txtYear2.Name = "txtYear2";
            this.txtYear2.Size = new System.Drawing.Size(97, 40);
            this.txtYear2.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtYear2, null);
            // 
            // txtYear1
            // 
            this.txtYear1.AutoSize = false;
            this.txtYear1.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear1.LinkItem = null;
            this.txtYear1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtYear1.LinkTopic = null;
            this.txtYear1.Location = new System.Drawing.Point(20, 762);
            this.txtYear1.Name = "txtYear1";
            this.txtYear1.Size = new System.Drawing.Size(97, 40);
            this.txtYear1.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtYear1, null);
            // 
            // txtRabiesDate1
            // 
            this.txtRabiesDate1.AutoSize = false;
            this.txtRabiesDate1.BackColor = System.Drawing.SystemColors.Window;
            this.txtRabiesDate1.Location = new System.Drawing.Point(20, 674);
            this.txtRabiesDate1.Name = "txtRabiesDate1";
            this.txtRabiesDate1.Size = new System.Drawing.Size(107, 40);
            this.txtRabiesDate1.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtRabiesDate1, null);
            //this.txtRabiesDate1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRabiesDate1_KeyPress);
            //this.txtRabiesDate1.Enter += new System.EventHandler(this.txtRabiesDate1_Enter);
            //this.txtRabiesDate1.TextChanged += new System.EventHandler(this.txtRabiesDate1_TextChanged);
            // 
            // txtRabiesDate2
            // 
            this.txtRabiesDate2.AutoSize = false;
            this.txtRabiesDate2.BackColor = System.Drawing.SystemColors.Window;
            this.txtRabiesDate2.Location = new System.Drawing.Point(141, 674);
            this.txtRabiesDate2.Name = "txtRabiesDate2";
            this.txtRabiesDate2.Size = new System.Drawing.Size(107, 40);
            this.txtRabiesDate2.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtRabiesDate2, null);
            //this.txtRabiesDate2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRabiesDate2_KeyPress);
            //this.txtRabiesDate2.Enter += new System.EventHandler(this.txtRabiesDate2_Enter);
            //this.txtRabiesDate2.TextChanged += new System.EventHandler(this.txtRabiesDate2_TextChanged);
            // 
            // txtTagNumber2
            // 
            this.txtTagNumber2.AutoSize = false;
            this.txtTagNumber2.BackColor = System.Drawing.SystemColors.Window;
            this.txtTagNumber2.LinkItem = null;
            this.txtTagNumber2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTagNumber2.LinkTopic = null;
            this.txtTagNumber2.Location = new System.Drawing.Point(144, 410);
            this.txtTagNumber2.Name = "txtTagNumber2";
            this.txtTagNumber2.Size = new System.Drawing.Size(97, 40);
            this.txtTagNumber2.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtTagNumber2, null);
            // 
            // txtBreed
            // 
            this.txtBreed.AutoSize = false;
            this.txtBreed.BackColor = System.Drawing.SystemColors.Window;
            this.txtBreed.LinkItem = null;
            this.txtBreed.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtBreed.LinkTopic = null;
            this.txtBreed.Location = new System.Drawing.Point(20, 586);
            this.txtBreed.Name = "txtBreed";
            this.txtBreed.Size = new System.Drawing.Size(221, 40);
            this.txtBreed.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtBreed, null);
            // 
            // txtColor
            // 
            this.txtColor.AutoSize = false;
            this.txtColor.BackColor = System.Drawing.SystemColors.Window;
            this.txtColor.LinkItem = null;
            this.txtColor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColor.LinkTopic = null;
            this.txtColor.Location = new System.Drawing.Point(20, 498);
            this.txtColor.Name = "txtColor";
            this.txtColor.Size = new System.Drawing.Size(221, 40);
            this.txtColor.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtColor, null);
            // 
            // txtTagNumber1
            // 
            this.txtTagNumber1.AutoSize = false;
            this.txtTagNumber1.BackColor = System.Drawing.SystemColors.Window;
            this.txtTagNumber1.LinkItem = null;
            this.txtTagNumber1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTagNumber1.LinkTopic = null;
            this.txtTagNumber1.Location = new System.Drawing.Point(20, 410);
            this.txtTagNumber1.Name = "txtTagNumber1";
            this.txtTagNumber1.Size = new System.Drawing.Size(97, 40);
            this.txtTagNumber1.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtTagNumber1, null);
            // 
            // txtDogName
            // 
            this.txtDogName.AutoSize = false;
            this.txtDogName.BackColor = System.Drawing.SystemColors.Window;
            this.txtDogName.LinkItem = null;
            this.txtDogName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDogName.LinkTopic = null;
            this.txtDogName.Location = new System.Drawing.Point(20, 322);
            this.txtDogName.Name = "txtDogName";
            this.txtDogName.Size = new System.Drawing.Size(221, 40);
            this.txtDogName.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtDogName, null);
            // 
            // txtLocation
            // 
            this.txtLocation.AutoSize = false;
            this.txtLocation.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation.LinkItem = null;
            this.txtLocation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLocation.LinkTopic = null;
            this.txtLocation.Location = new System.Drawing.Point(20, 234);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(221, 40);
            this.txtLocation.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtLocation, null);
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress.LinkItem = null;
            this.txtAddress.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress.LinkTopic = null;
            this.txtAddress.Location = new System.Drawing.Point(20, 146);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(221, 40);
            this.txtAddress.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtAddress, null);
            // 
            // txtLastName
            // 
            this.txtLastName.AutoSize = false;
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.LinkItem = null;
            this.txtLastName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLastName.LinkTopic = null;
            this.txtLastName.Location = new System.Drawing.Point(20, 58);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(221, 40);
            this.txtLastName.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtLastName, null);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 734);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(96, 15);
            this.Label13.TabIndex = 38;
            this.Label13.Text = "YEARS FROM";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(129, 776);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(12, 16);
            this.Label12.TabIndex = 37;
            this.Label12.Text = "-";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(129, 688);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(12, 16);
            this.Label11.TabIndex = 34;
            this.Label11.Text = "-";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(129, 424);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(10, 14);
            this.Label10.TabIndex = 33;
            this.Label10.Text = "-";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 646);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(180, 15);
            this.Label9.TabIndex = 32;
            this.Label9.Text = "RABIES EXPIRATION DATE";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 558);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(55, 15);
            this.Label8.TabIndex = 31;
            this.Label8.Text = "BREED";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 470);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(54, 15);
            this.Label7.TabIndex = 30;
            this.Label7.Text = "COLOR";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 382);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(98, 15);
            this.Label6.TabIndex = 29;
            this.Label6.Text = "TAG NUMBER";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 294);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(80, 15);
            this.Label5.TabIndex = 28;
            this.Label5.Text = "DOG NAME";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 206);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(75, 15);
            this.Label4.TabIndex = 27;
            this.Label4.Text = "LOCATION";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 118);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(74, 15);
            this.Label3.TabIndex = 26;
            this.Label3.Text = "ADDRESS";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(139, 15);
            this.Label2.TabIndex = 25;
            this.Label2.Text = "OWNER LAST NAME";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkUnregistered);
            this.Frame1.Controls.Add(this.cmbReport);
            this.Frame1.Controls.Add(this.txtYear);
            this.Frame1.Controls.Add(this.lblYear);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(799, 90);
            this.Frame1.TabIndex = 22;
            this.Frame1.Text = "Select Report To Print";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // chkUnregistered
            // 
            this.chkUnregistered.Location = new System.Drawing.Point(261, 30);
            this.chkUnregistered.Name = "chkUnregistered";
            this.chkUnregistered.Size = new System.Drawing.Size(231, 27);
            this.chkUnregistered.TabIndex = 40;
            this.chkUnregistered.Text = "Show as Unregistered Dogs";
            this.ToolTip1.SetToolTip(this.chkUnregistered, null);
            this.chkUnregistered.Visible = false;
            // 
            // txtYear
            // 
			this.txtYear.MaxLength = 4;
            this.txtYear.AutoSize = false;
            this.txtYear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtYear.LinkItem = null;
            this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtYear.LinkTopic = null;
            this.txtYear.Location = new System.Drawing.Point(699, 30);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(80, 40);
            this.txtYear.TabIndex = 35;
            this.txtYear.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtYear, null);
            this.txtYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYear_KeyPress);
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(512, 44);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(150, 15);
            this.lblYear.TabIndex = 36;
            this.lblYear.Text = "YEAR TO REPORT ON";
            this.ToolTip1.SetToolTip(this.lblYear, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClearSearch,
            this.mnuSepar2,
            this.mnuPrintPreview,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClearSearch
            // 
            this.mnuClearSearch.Index = 0;
            this.mnuClearSearch.Name = "mnuClearSearch";
            this.mnuClearSearch.Text = "Clear Search Options";
            this.mnuClearSearch.Click += new System.EventHandler(this.mnuClearSearch_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = 2;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrintPreview.Text = "Print Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 3;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdClearSearch
            // 
            this.cmdClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClearSearch.AppearanceKey = "toolbarButton";
            this.cmdClearSearch.Location = new System.Drawing.Point(691, 29);
            this.cmdClearSearch.Name = "cmdClearSearch";
            this.cmdClearSearch.Size = new System.Drawing.Size(144, 24);
            this.cmdClearSearch.TabIndex = 1;
            this.cmdClearSearch.Text = "Clear Search Options";
            this.ToolTip1.SetToolTip(this.cmdClearSearch, null);
            this.cmdClearSearch.Click += new System.EventHandler(this.mnuClearSearch_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(339, 30);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(150, 48);
            this.cmdPrintPreview.TabIndex = 0;
            this.cmdPrintPreview.Text = "Print Preview";
            this.ToolTip1.SetToolTip(this.cmdPrintPreview, null);
            this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
            // 
            // frmReportListing
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(865, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmReportListing";
            this.Text = "Registration/Warrant Listing";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmReportListing_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReportListing_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReportListing_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkServeWarrant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnregistered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdPrintPreview;
        private FCButton cmdClearSearch;
    }
}
