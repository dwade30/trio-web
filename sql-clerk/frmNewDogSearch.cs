﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmNewDogSearch : BaseForm
	{
		public frmNewDogSearch()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null && System.Windows.Forms.Application.OpenForms.Count == 0)
				_InstancePtr = this;

            this.cmdClearSearch.Click += cmdClearSearch_Click;
		}

 

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmNewDogSearch InstancePtr
		{
			get
			{
				return (frmNewDogSearch)Sys.GetInstance(typeof(frmNewDogSearch));
			}
		}

		protected frmNewDogSearch _InstancePtr = null;
		//=========================================================
		private void cmdClearSearch_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 0; x <= (GridCriteria.Cols - 1); x++)
			{
				GridCriteria.TextMatrix(1, x, "");
			}
			// x
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			mnuSearch_Click();
		}

		private void frmNewDogSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmNewDogSearch_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridCriteria("Owner Name");
		}

		private void frmNewDogSearch_Resize(object sender, System.EventArgs e)
		{
			ResizeGridCriteria();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{

		}

		private void GridCriteria_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (GridCriteria.Row > 0)
			{
				switch (KeyCode)
				{
					case Keys.Return:
						{
							KeyCode = 0;
							mnuSearch_Click();
							return;
						}
				}
				//end switch
			}
		}

		private void GridCriteria_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (GridCriteria.Row > 0)
			{
				switch (KeyCode)
				{
					case Keys.Return:
						{
							KeyCode = 0;
							mnuSearch_Click();
							return;
						}
				}
				//end switch
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNewOwner_Click(object sender, System.EventArgs e)
		{
			frmDogOwner.InstancePtr.Init(0);
		}

		private void mnuSearch_Click(object sender, System.EventArgs e)
		{
			int intTypeofRecords = 0;
			bool boolIncludeDeletedDogs = false;
			int intSearchBy = 0;
			int intDisplay = 0;
			string strSearch;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strTemp;
			GridCriteria.Row = 0;
			//App.DoEvents();
			if (cmbTypes.Text == "Dogs")
			{
				// regular dogs
				intTypeofRecords = 0;
			}
			else if (cmbTypes.Text == "Dogs in Kennels")
			{
				// kennel dogs
				intTypeofRecords = 1;
			}
			else if (cmbTypes.Text == "All")
			{
				// both
				intTypeofRecords = 2;
			}
			if (chkIncludeDeleted.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolIncludeDeletedDogs = true;
			}
			else
			{
				boolIncludeDeletedDogs = false;
			}
			if (cmbSearchBy.Text == "Owner Name")
			{
				// owner name
				intSearchBy = 0;
			}
			else if (cmbSearchBy.Text == "City or Town")
			{
				// city
				intSearchBy = 1;
			}
			else if (cmbSearchBy.Text == "Location")
			{
				// location
				intSearchBy = 2;
			}
			else if (cmbSearchBy.Text == "Kennel License Number *")
			{
				// kennel num
				intSearchBy = 4;
			}
			else if (cmbSearchBy.Text == "Dog Name")
			{
				// dogname
				intSearchBy = 5;
			}
			else if (cmbSearchBy.Text == "Veterinarian")
			{
				// vet
				intSearchBy = 6;
			}
			else if (cmbSearchBy.Text == "Rabies Tag Number *")
			{
				// rabies sticker
				intSearchBy = 7;
			}
			else if (cmbSearchBy.Text == "License Tag Number *")
			{
				// tag number
				intSearchBy = 8;
			}
			else if (cmbSearchBy.Text == "License Sticker Number *")
			{
				// sticker number
				intSearchBy = 9;
			}
			else if (cmbSearchBy.Text == "Breed")
			{
				// breed
				intSearchBy = 10;
			}
			if (cmbCriteria.Text == "Contain Search Criteria")
			{
				intDisplay = 0;
			}
			else if (cmbCriteria.Text == "Start With Search Criteria")
			{
				intDisplay = 1;
			}
			else if (cmbCriteria.Text == "End With Search Criteria")
			{
				intDisplay = 2;
			}
			strSearch = "";
			for (x = 0; x <= (GridCriteria.Cols - 1); x++)
			{
				if (x == 1)
				{
					strSearch += ",";
				}
				strSearch += GridCriteria.TextMatrix(1, x);
			}
			// x
			strSearch = Strings.Replace(strSearch, "'", "''", 1, -1, CompareConstants.vbTextCompare);
			strTemp = strSearch;
			strTemp = Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			if (strTemp == string.Empty)
			{
				MessageBox.Show("You must enter criteria to search by", "Criteria not Specified", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmDogSearchResults.InstancePtr.Init(ref intTypeofRecords, ref boolIncludeDeletedDogs, ref intSearchBy, ref intDisplay, ref strSearch);
		}

		public void mnuSearch_Click()
		{
			mnuSearch_Click(mnuSearch, new System.EventArgs());
		}

		private void optSearchBy_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			SetupGridCriteria(cmbSearchBy.Text);
		}

		private void optSearchBy_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSearchBy.SelectedIndex;
			optSearchBy_CheckedChanged(index, sender, e);
		}

		private void ResizeGridCriteria()
		{
			int x;
			int GridWidth = 0;
			for (x = 0; x <= 9; x++)
			{
				//FC:FINAL:DDU:#i1979 - index 3 was deleted in original, replaced with empty string
				if (cmbSearchBy.Text != "")
				{
					if (cmbSearchBy.SelectedIndex == x)
					{
						break;
					}
				}
			}
			// x
			GridWidth = GridCriteria.WidthOriginal;
			switch (x)
			{
				case 0:
					{
						GridCriteria.ColWidth(0, FCConvert.ToInt32(0.5 * GridWidth));
						break;
					}
				case 2:
					{
						GridCriteria.ColWidth(0, FCConvert.ToInt32(0.2 * GridWidth));
						break;
					}
				default:
					{
						// one column, no need to resize
						//FC:FINAL:DDU:#i1979 - set width to full grid width
						GridCriteria.ColWidth(0, FCConvert.ToInt32(0.99 * GridWidth));
						break;
					}
			}
			//end switch
		}

		private void SetupGridCriteria(string intIndex)
        {
            GridCriteria.Rows = 2;
			switch (intIndex)
			{
				case "Owner Name":
					{
						// ownername
						GridCriteria.Cols = 2;
						GridCriteria.TextMatrix(0, 0, "Last Name");
						GridCriteria.TextMatrix(0, 1, "First Name");
						break;
					}
				case "City or Town":
					{
						// city
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "City / Town");
						break;
					}
				case "Location":
					{
						// location
						GridCriteria.Cols = 2;
						GridCriteria.TextMatrix(0, 0, "Number");
						GridCriteria.TextMatrix(0, 1, "Street Name");
						break;
					}
				case "":
					{
						// Phone Number
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "Telephone");
						break;
					}
				case "Kennel License Number *":
					{
						// kennel license number
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "Kennel License Number");
						break;
					}
				case "Dog Name":
					{
						// dogname
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "Dog Name");
						break;
					}
				case "Veterinarian":
					{
						// vet
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "Veterinarian");
						break;
					}
				case "Rabies Tag Number *":
					{
						// Rabies tag number
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "Rabies Tag Number");
						break;
					}
				case "License Tag Number *":
					{
						// License Tag Number
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "License Tag Number");
						break;
					}
				case "License Sticker Number *":
					{
						// License Sticker Number
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "License Sticker Number");
						break;
					}
				case "Breed":
					{
						// breed
						GridCriteria.Cols = 1;
						GridCriteria.TextMatrix(0, 0, "Breed");
						break;
					}
			}
			//end switch
			ResizeGridCriteria();
		}
	}
}
