//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for srptDogSummary.
	/// </summary>
	public partial class srptDogSummary : FCSectionReport
	{
		public srptDogSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptDogSummary InstancePtr
		{
			get
			{
				return (srptDogSummary)Sys.GetInstance(typeof(srptDogSummary));
			}
		}

		protected srptDogSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptDogSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngYearToUse;
		int intMonth;
		bool boolSummary;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp;
			string[] strAry = null;
			int X;
			int lngTowncode = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngMF1;
			int lngMF2;
			int intTotal;
			clsDRWrapper clsTemp = new clsDRWrapper();
			strTemp = FCConvert.ToString(this.UserData);
			strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
			lblMonth.Text = Strings.Format(strAry[0] + "/1/" + strAry[1], "MMMM");
			lngYearToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
			intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
			boolSummary = FCConvert.CBool(strAry[2]);
			lblStickerYear1.Text = FCConvert.ToString(lngYearToUse);
			lblStickerYear2.Text = FCConvert.ToString(lngYearToUse + 1);
			if (modGNBas.Statics.boolRegionalTown)
			{
				lngTowncode = FCConvert.ToInt32(Math.Round(Conversion.Val(frmMonthlyDogReport.InstancePtr.gridTownCode.TextMatrix(0, 0))));
			}
			else
			{
				lngTowncode = 0;
			}
			rsLoad.OpenRecordset("select count(Transactionnumber) as MaleFemale From dogtransactions where DangerousDog <> 1 and NuisanceDog <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1 and (boolkennel <> 1) and (boolSpayNeuter <> 1)  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(lngYearToUse), "twck0000.vb1");
			txtMF1.Text = "0";
			lngMF1 = 0;
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("MaleFemale")) != 0)
				{
					txtMF1.Text = FCConvert.ToString(rsLoad.Get_Fields("MaleFemale"));
					lngMF1 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("malefemale"))));
				}
			}
			modGNBas.Statics.typDogSummary.lngMF1 += lngMF1;
			rsLoad.OpenRecordset("select count(Transactionnumber) as MaleFemale From dogtransactions where DangerousDog <> 1 and NuisanceDog <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1 and (boolkennel <> 1) and (boolSpayNeuter <> 1)  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(lngYearToUse + 1), "twck0000.vb1");
			txtMF2.Text = "0";
			lngMF2 = 0;
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("MaleFemale")) != 0)
				{
					txtMF2.Text = FCConvert.ToString(rsLoad.Get_Fields("MaleFemale"));
					lngMF2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("malefemale"))));
				}
			}
			modGNBas.Statics.typDogSummary.lngMF2 += lngMF2;
			// vbPorter upgrade warning: lngNeuter1 As object	OnWrite(int, double)
			int lngNeuter1;
			int lngNeuter2;
			rsLoad.OpenRecordset("select count(Transactionnumber) as Neutered From dogtransactions where DangerousDog <> 1 and NuisanceDog <> 1 and  convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1 and (boolkennel <> 1) and  boolSpayNeuter = 1  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(lngYearToUse), "twck0000.vb1");
			txtNeuter1.Text = "0";
			lngNeuter1 = 0;
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("Neutered")) != 0)
				{
					txtNeuter1.Text = FCConvert.ToString(rsLoad.Get_Fields("Neutered"));
					lngNeuter1 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("neutered"))));
				}
			}
			modGNBas.Statics.typDogSummary.lngNeuter1 += lngNeuter1;
			rsLoad.OpenRecordset("select count(Transactionnumber) as Neutered From dogtransactions where DangerousDog <> 1 and NuisanceDog <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1 and (boolkennel <> 1) and  boolSpayNeuter = 1  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(lngYearToUse + 1), "twck0000.vb1");
			txtNeuter2.Text = "0";
			lngNeuter2 = 0;
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("Neutered")) != 0)
				{
					txtNeuter2.Text = FCConvert.ToString(rsLoad.Get_Fields("Neutered"));
					lngNeuter2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("neutered"))));
				}
			}
			modGNBas.Statics.typDogSummary.lngNeuter2 += lngNeuter2;

            var dangerous1 = 0;
            var dangerous2 = 0;
            var nuisance1 = 0;
            var nuisance2 = 0;
            rsLoad.OpenRecordset(
                "select count (TransactionNumber) as DangerousCount From dogtransactions where DangerousDog = 1 and NuisanceDog <> 1 and  convert(int, isnull(towncode, 0)) = " +
                FCConvert.ToString(lngTowncode) +
                " and boollicense = 1 and (boolkennel <> 1) and  boolSpayNeuter = 1  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" +
                FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" +
                FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1,
                    fecherFoundation.DateAndTime.DateAdd("M", 1,
                        FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" +
                                             FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " +
                FCConvert.ToString(lngYearToUse), "Clerk");
            txtDangerous1.Text = "0";
            dangerous1 = 0;
            if (!rsLoad.EndOfFile())
            {
                if (rsLoad.Get_Fields_Int32("DangerousCount") != 0)
                {
                    dangerous1 = rsLoad.Get_Fields_Int32("DangerousCount");
                    txtDangerous1.Text = dangerous1.ToString();
                }
            }
            modGNBas.Statics.typDogSummary.lngDangerous1 += dangerous1;

            rsLoad.OpenRecordset(
                "select count (TransactionNumber) as NuisanceCount From dogtransactions where DangerousDog <> 1 and NuisanceDog = 1 and  convert(int, isnull(towncode, 0)) = " +
                FCConvert.ToString(lngTowncode) +
                " and boollicense = 1 and (boolkennel <> 1) and  boolSpayNeuter = 1  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" +
                FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" +
                FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1,
                    fecherFoundation.DateAndTime.DateAdd("M", 1,
                        FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" +
                                             FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " +
                FCConvert.ToString(lngYearToUse), "Clerk");
            txtNuisance1.Text = "0";
            nuisance1 = 0;
            if (!rsLoad.EndOfFile())
            {
                if (rsLoad.Get_Fields_Int32("NuisanceCount") != 0)
                {
                    nuisance1 = rsLoad.Get_Fields_Int32("NuisanceCount");
                    txtNuisance1.Text = nuisance1.ToString();
                }
            }
            modGNBas.Statics.typDogSummary.lngNuisance1 += nuisance1;

            rsLoad.OpenRecordset(
               "select count (TransactionNumber) as DangerousCount From dogtransactions where DangerousDog = 1 and NuisanceDog <> 1 and  convert(int, isnull(towncode, 0)) = " +
               FCConvert.ToString(lngTowncode) +
               " and boollicense = 1 and (boolkennel <> 1) and  boolSpayNeuter = 1  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" +
               FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" +
               FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1,
                   fecherFoundation.DateAndTime.DateAdd("M", 1,
                       FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" +
                                            FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " +
               FCConvert.ToString(lngYearToUse + 1), "Clerk");
            txtDangerous2.Text = "0";
            dangerous2 = 0;
            if (!rsLoad.EndOfFile())
            {
                if (rsLoad.Get_Fields_Int32("DangerousCount") != 0)
                {
                    dangerous2 = rsLoad.Get_Fields_Int32("DangerousCount");
                    txtDangerous2.Text = dangerous2.ToString();
                }
            }
            modGNBas.Statics.typDogSummary.lngDangerous2 += dangerous2;

            rsLoad.OpenRecordset(
                "select count (TransactionNumber) as NuisanceCount From dogtransactions where DangerousDog <> 1 and NuisanceDog = 1 and  convert(int, isnull(towncode, 0)) = " +
                FCConvert.ToString(lngTowncode) +
                " and boollicense = 1 and (boolkennel <> 1) and  boolSpayNeuter = 1  and (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" +
                FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" +
                FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1,
                    fecherFoundation.DateAndTime.DateAdd("M", 1,
                        FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" +
                                             FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " +
                FCConvert.ToString(lngYearToUse + 1), "Clerk");
            txtNuisance2.Text = "0";
            nuisance2 = 0;
            if (!rsLoad.EndOfFile())
            {
                if (rsLoad.Get_Fields_Int32("NuisanceCount") != 0)
                {
                    nuisance2 = rsLoad.Get_Fields_Int32("NuisanceCount");
                    txtNuisance2.Text = nuisance2.ToString();
                }
            }
            modGNBas.Statics.typDogSummary.lngNuisance2 += nuisance2;

            int lngKennelTotal;
			intTotal = 0;
			rsLoad.OpenRecordset("select count(ID) as Thecount from dogtransactions where isnull(DangerousDog,0) <> 1 and isnull(NuisanceDog,0) <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boolkennel = 1 and boollicense = 1 and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
			txtKennelTotal.Text = "0";
			lngKennelTotal = 0;
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("thecount")) > 0)
				{
					intTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("thecount"))));
					txtKennelTotal.Text = FCConvert.ToString(intTotal);
					lngKennelTotal = intTotal;
				}
			}
			modGNBas.Statics.typDogSummary.lngKennelLicenses += lngKennelTotal;
			// now go through each license and see how many dogs etc.
			int lngKDogsYr1;
			int lngKDogsYr2;
			int lngKDogsAddedYr1;
			int lngKDogsAddedYr2;
			DateTime dtDate;
			DateTime dtIssueDate;
			DateTime dtTemp1;
			DateTime dtTemp2;
			string strDogs = "";
			int intNumDogs = 0;
			// Dim x As Integer
			bool boolAddedToKennel = false;
			lngKDogsYr1 = 0;
			lngKDogsYr2 = 0;
			lngKDogsAddedYr1 = 0;
			lngKDogsAddedYr2 = 0;
			txtKennel1.Text = "0";
			txtKennel2.Text = "0";
			txtDogKennel1.Text = "0";
			txtDogKennel2.Text = "0";
			rsLoad.OpenRecordset("select reissuedate,kennellicense.dognumbers as dognumbers from kennellicense inner join dogowner on (dogowner.ID = kennellicense.ownernum) where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode), "twck0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				strDogs = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("dognumbers"));
				if (strDogs != string.Empty)
				{
					dtIssueDate = rsLoad.Get_Fields_DateTime("reissuedate");
					strAry = Strings.Split(strDogs, ",", -1, CompareConstants.vbTextCompare);
					// now we have a list of the dogs in the kennel
					intNumDogs = (Information.UBound(strAry, 1) + 1);
					for (X = 0; X <= intNumDogs - 1; X++)
					{
						// for each dog check
						clsTemp.OpenRecordset("select dogtokenneldate,[year] from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[X])), "twck0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							boolAddedToKennel = false;
							if (Information.IsDate(clsTemp.Get_Fields("dogtokenneldate")))
							{
								if (Convert.ToDateTime(clsTemp.Get_Fields_DateTime("dogtokenneldate")).ToOADate() != 0)
								{
									dtDate = FCConvert.ToDateTime(Strings.Format(clsTemp.Get_Fields_DateTime("dogtokenneldate"), "MM/dd/yyyy"));
									if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, dtIssueDate) < 0)
									{
										if (dtDate.Month == intMonth)
										{
											if (dtDate.Year >= lngYearToUse)
											{
												boolAddedToKennel = true;
											}
										}
									}
								}
							}
							dtTemp1 = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse));
							dtTemp2 = fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))));
							if (Conversion.Val(clsTemp.Get_Fields("year")) == lngYearToUse)
							{
								if (boolAddedToKennel)
								{
									lngKDogsAddedYr1 += 1;
								}
								else
								{
									if ((fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtIssueDate) >= 0) && (fecherFoundation.DateAndTime.DateDiff("d", dtTemp2, dtIssueDate) <= 0))
									{
										lngKDogsYr1 += 1;
									}
								}
							}
							else if (Conversion.Val(clsTemp.Get_Fields("year")) == lngYearToUse + 1)
							{
								if (boolAddedToKennel)
								{
									lngKDogsAddedYr2 += 1;
								}
								else
								{
									if ((fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtIssueDate) >= 0) && (fecherFoundation.DateAndTime.DateDiff("d", dtTemp2, dtIssueDate) <= 0))
									{
										lngKDogsYr2 += 1;
									}
								}
							}
						}
					}
					// X
				}
				rsLoad.MoveNext();
			}
			txtKennel1.Text = lngKDogsYr1.ToString();
			txtKennel2.Text = lngKDogsYr2.ToString();
			txtDogKennel1.Text = lngKDogsAddedYr1.ToString();
			txtDogKennel2.Text = lngKDogsAddedYr2.ToString();
			modGNBas.Statics.typDogSummary.lngKennelSticker1 += lngKDogsYr1;
			modGNBas.Statics.typDogSummary.lngKennelSticker2 += lngKDogsYr2;
			modGNBas.Statics.typDogSummary.lngAddedToKennel1 += lngKDogsAddedYr1;
			modGNBas.Statics.typDogSummary.lngAddedToKennel2 += lngKDogsAddedYr2;
			rsLoad.OpenRecordset("select count(Transactionnumber) as replacementtotal From dogtransactions where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boollicense <> 1) and (boolkennel <> 1)   and boolreplacementsticker = 1 and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(lngYearToUse), "twck0000.vb1");
			txtReplacement1.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("ReplacementTotal")) != 0)
				{
					txtReplacement1.Text = FCConvert.ToString(rsLoad.Get_Fields("ReplacementTotal"));
				}
			}
			modGNBas.Statics.typDogSummary.lngReplacement1 += FCConvert.ToInt32(txtReplacement1.Text);
			rsLoad.OpenRecordset("select count(Transactionnumber) as replacementtagtotal From dogtransactions where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and boolreplacementtag = 1 and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
			txtReplacementTag.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("ReplacementTagTotal")) != 0)
				{
					txtReplacementTag.Text = FCConvert.ToString(rsLoad.Get_Fields("ReplacementTagTotal"));
					modGNBas.Statics.typDogSummary.lngReplacementTag += Conversion.Val(rsLoad.Get_Fields("replacementtagtotal"));
				}
				else
				{
					txtReplacementTag.Text = "0";
				}
			}
			else
			{
				txtReplacementTag.Text = "0";
			}
			rsLoad.OpenRecordset("select count(transactionnumber) as newtag from dogtransactions where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1) and convert(int, isnull(oldtagnumber, 0)) = 0 and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
			txtNewTag.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("NewTag")) != 0)
				{
					txtNewTag.Text = FCConvert.ToString(rsLoad.Get_Fields("NewTag"));
					modGNBas.Statics.typDogSummary.lngNewTag += FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("newtag"))));
				}
				else
				{
					txtNewTag.Text = "0";
				}
			}
			else
			{
				txtNewTag.Text = "0";
			}
			txtTagOther.Text = "0";
			txtTagTotal.Text = FCConvert.ToString(Conversion.Val(txtReplacementTag.Text) + Conversion.Val(txtNewTag.Text) + Conversion.Val(txtTagOther.Text));
			rsLoad.OpenRecordset("select count(Transactionnumber) as replacementtotal From dogtransactions where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boollicense <> 1) and (boolkennel <> 1)   and boolreplacementsticker = 1 and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(lngYearToUse + 1), "twck0000.vb1");
			txtReplacement2.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("ReplacementTotal")) != 0)
				{
					txtReplacement2.Text = FCConvert.ToString(rsLoad.Get_Fields("ReplacementTotal"));
					modGNBas.Statics.typDogSummary.lngReplacement2 += FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("replacementtotal"))));
				}
			}
			// search and rescue
			rsLoad.OpenRecordset("SELECT count(DogInfo.ID) as SandRTotal FROM DogInfo inner join dogowner on (dogowner.ID = doginfo.ownernum) where isnuisancedog <> 1 and isdangerousdog <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and DogInfo.RabStickerIssue >='" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND SandR = 1 and Year = " + FCConvert.ToString(lngYearToUse), modGNBas.DEFAULTDATABASE);
			txtSandR1.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("SandRTotal")) != 0)
				{
					txtSandR1.Text = FCConvert.ToString(rsLoad.Get_Fields("SandRTotal"));
					modGNBas.Statics.typDogSummary.lngSearchRescue1 += FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("sandrtotal"))));
				}
			}
			rsLoad.OpenRecordset("SELECT count(DogInfo.ID) as SandRTotal FROM DogInfo inner join dogowner on (dogowner.ID = doginfo.ownernum) where isnuisancedog <> 1 and isdangerousdog <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and  DogInfo.RabStickerIssue >='" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND SandR = 1 and Year = " + FCConvert.ToString(lngYearToUse + 1), modGNBas.DEFAULTDATABASE);
			txtSandR2.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("SandRTotal")) != 0)
				{
					txtSandR2.Text = FCConvert.ToString(rsLoad.Get_Fields("SandRTotal"));
					modGNBas.Statics.typDogSummary.lngSearchRescue2 += FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("sandrtotal"))));
				}
			}
			// Hearing/Guide
			rsLoad.OpenRecordset("SELECT count(DogInfo.ID) as HearGuideTotal FROM DogInfo  inner join dogowner on (dogowner.ID = doginfo.ownernum) where isnuisancedog <> 1 and isdangerousdog <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and  DogInfo.RabStickerIssue >='" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND HearGuide = 1 and Year = " + FCConvert.ToString(lngYearToUse), modGNBas.DEFAULTDATABASE);
			txtHearingGuide1.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("HearGuideTotal")) != 0)
				{
					txtHearingGuide1.Text = FCConvert.ToString(rsLoad.Get_Fields("HearGuideTotal"));
					modGNBas.Statics.typDogSummary.lngHearingGuide1 += FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("hearguidetotal"))));
				}
			}
			rsLoad.OpenRecordset("SELECT count(DogInfo.ID) as HearGuideTotal FROM DogInfo  inner join dogowner on (dogowner.ID = doginfo.ownernum) where isnuisancedog <> 1 and isdangerousdog <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and  DogInfo.RabStickerIssue >='" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND HearGuide = 1 and Year = " + FCConvert.ToString(lngYearToUse + 1), modGNBas.DEFAULTDATABASE);
			txtHearingGuide2.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("HearGuideTotal")) != 0)
				{
					txtHearingGuide2.Text = FCConvert.ToString(rsLoad.Get_Fields("HearGuideTotal"));
					modGNBas.Statics.typDogSummary.lngHearingGuide2 += FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("hearguidetotal"))));
				}
			}
			// Transfers
			rsLoad.OpenRecordset("select count(Transactionnumber) as transferstotal From dogtransactions where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and booltransfer = 1 and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(lngYearToUse), "twck0000.vb1");
			txtTransfers1.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("TransfersTotal")) != 0)
				{
					txtTransfers1.Text = FCConvert.ToString(rsLoad.Get_Fields("TransfersTotal"));
					modGNBas.Statics.typDogSummary.lngTransfer1 += FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("transferstotal"))));
				}
			}
			rsLoad.OpenRecordset("select count(Transactionnumber) as transferstotal From dogtransactions where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and booltransfer = 1 and transactiondate between '" + FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(lngYearToUse + 1), "twck0000.vb1");
			txtTransfers2.Text = "0";
			if (!rsLoad.EndOfFile())
			{
				if (Conversion.Val(rsLoad.Get_Fields("TransfersTotal")) != 0)
				{
					txtTransfers2.Text = FCConvert.ToString(rsLoad.Get_Fields("TransfersTotal"));
					modGNBas.Statics.typDogSummary.lngTransfer2 += FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("transferstotal"))));
				}
			}
			txtDogTotal1.Text = (txtReplacement1.Text.ToIntegerValue() + txtSandR1.Text.ToIntegerValue() + txtHearingGuide1.Text.ToIntegerValue() + txtTransfers1.Text.ToIntegerValue() + txtDogKennel1.Text.ToIntegerValue() + lngMF1 + lngNeuter1 + txtKennel1.Text.ToIntegerValue() + dangerous1 + nuisance1).FormatAsNumber();
			txtDogTotal2.Text = (txtReplacement2.Text.ToIntegerValue() + txtSandR2.Text.ToIntegerValue() + txtHearingGuide2.Text.ToIntegerValue() + txtTransfers2.Text.ToIntegerValue() + txtDogKennel2.Text.ToIntegerValue() + lngMF2 + lngNeuter2 + txtKennel2.Text.ToIntegerValue() + dangerous2 + nuisance2).FormatAsNumber();
			if (boolSummary)
			{
				Detail.Visible = false;
			}
			else
			{
				Detail.Visible = true;
			}
			rsLoad.Dispose();
			clsTemp.Dispose();
		}

		
	}
}
