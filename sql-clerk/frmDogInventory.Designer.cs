﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDogInventory.
	/// </summary>
	partial class frmDogInventory
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCTextBox txtYear;
		public FCGrid vs1;
		public fecherFoundation.FCTextBox txtLow;
		public fecherFoundation.FCTextBox txtHigh;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuRemove;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuView;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbType = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.vs1 = new fecherFoundation.FCGrid();
            this.txtLow = new fecherFoundation.FCTextBox();
            this.txtHigh = new fecherFoundation.FCTextBox();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRemove = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuView = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdView = new fecherFoundation.FCButton();
            this.cmdRemove = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemove)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdAdd);
            this.BottomPanel.Location = new System.Drawing.Point(0, 528);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.txtYear);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.txtLow);
            this.ClientArea.Controls.Add(this.txtHigh);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Size = new System.Drawing.Size(1078, 468);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdRemove);
            this.TopPanel.Controls.Add(this.cmdView);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdView, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRemove, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(310, 30);
            this.HeaderText.Text = "Dog Tag / Sticker Inventory";
            // 
            // cmbType
            // 
            this.cmbType.Items.AddRange(new object[] {
            "Tag Number",
            "Kennel License Number",
            "Dangerous Dog"});
            this.cmbType.Location = new System.Drawing.Point(179, 30);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(240, 40);
            this.cmbType.TabIndex = 12;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.optType_CheckedChanged);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(30, 44);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(43, 17);
            this.lblType.TabIndex = 1;
            this.lblType.Text = "TYPE";
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.Location = new System.Drawing.Point(179, 256);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(240, 40);
            this.txtYear.TabIndex = 2;
            this.txtYear.Enter += new System.EventHandler(this.txtYear_Enter);
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 4;
            this.vs1.ExtendLastCol = true;
            this.vs1.Location = new System.Drawing.Point(449, 30);
            this.vs1.Name = "vs1";
            this.vs1.Rows = 50;
            this.vs1.Size = new System.Drawing.Size(601, 432);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 3;
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            // 
            // txtLow
            // 
            this.txtLow.BackColor = System.Drawing.SystemColors.Window;
            this.txtLow.Location = new System.Drawing.Point(179, 136);
            this.txtLow.Name = "txtLow";
            this.txtLow.Size = new System.Drawing.Size(240, 40);
            this.txtLow.TabIndex = 13;
            // 
            // txtHigh
            // 
            this.txtHigh.BackColor = System.Drawing.SystemColors.Window;
            this.txtHigh.Location = new System.Drawing.Point(179, 196);
            this.txtHigh.Name = "txtHigh";
            this.txtHigh.Size = new System.Drawing.Size(240, 40);
            this.txtHigh.TabIndex = 1;
            // 
            // cmdAdd
            // 
            this.cmdAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAdd.AppearanceKey = "acceptButton";
            this.cmdAdd.Location = new System.Drawing.Point(560, 30);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdAdd.Size = new System.Drawing.Size(80, 48);
            this.cmdAdd.TabIndex = 6;
            this.cmdAdd.Text = "Save";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 270);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(88, 16);
            this.Label4.TabIndex = 11;
            this.Label4.Text = "YEAR";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 150);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(88, 16);
            this.Label1.TabIndex = 10;
            this.Label1.Text = "LOW NUMBER";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 210);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(88, 16);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "HIGH NUMBER";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 90);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(310, 26);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "IN ORDER TO ADD ITEMS TO INVENTORY, BOTH A LOW NUMBER AND A HIGH NUMBER HAVE TO B" +
    "E ADDED";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRemove,
            this.mnuSP3,
            this.mnuView,
            this.mnuSP21,
            this.mnuAdd,
            this.mnuSaveExit,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuRemove
            // 
            this.mnuRemove.Index = 0;
            this.mnuRemove.Name = "mnuRemove";
            this.mnuRemove.Text = "Delete";
            this.mnuRemove.Click += new System.EventHandler(this.mnuRemove_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuView
            // 
            this.mnuView.Index = 2;
            this.mnuView.Name = "mnuView";
            this.mnuView.Text = "Refresh";
            this.mnuView.Click += new System.EventHandler(this.mnuView_Click);
            // 
            // mnuSP21
            // 
            this.mnuSP21.Index = 3;
            this.mnuSP21.Name = "mnuSP21";
            this.mnuSP21.Text = "-";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = 4;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuAdd.Text = "Save                                    ";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 5;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit               ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 6;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 7;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdView
            // 
            this.cmdView.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdView.Location = new System.Drawing.Point(1048, 29);
            this.cmdView.Name = "cmdView";
            this.cmdView.Size = new System.Drawing.Size(66, 24);
            this.cmdView.TabIndex = 1;
            this.cmdView.Text = "Refresh";
            this.cmdView.Click += new System.EventHandler(this.mnuView_Click);
            // 
            // cmdRemove
            // 
            this.cmdRemove.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRemove.Location = new System.Drawing.Point(982, 29);
            this.cmdRemove.Name = "cmdRemove";
            this.cmdRemove.Size = new System.Drawing.Size(60, 24);
            this.cmdRemove.TabIndex = 2;
            this.cmdRemove.Text = "Delete";
            this.cmdRemove.Click += new System.EventHandler(this.mnuRemove_Click);
            // 
            // frmDogInventory
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 636);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmDogInventory";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Dog Tag / Sticker Inventory";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDogInventory_Load);
            this.Activated += new System.EventHandler(this.frmDogInventory_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDogInventory_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDogInventory_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemove)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdRemove;
        private FCButton cmdView;
    }
}
