//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDogSearchResults.
	/// </summary>
	partial class frmDogSearchResults
	{
		public FCGrid GridSearch;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.GridSearch = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Shape1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdOpenRecord = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOpenRecord)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdOpenRecord);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(665, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridSearch);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Shape1);
            this.ClientArea.Size = new System.Drawing.Size(665, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(665, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(179, 30);
            this.HeaderText.Text = "Search Results";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // GridSearch
            // 
            this.GridSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridSearch.Cols = 10;
            this.GridSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridSearch.ExtendLastCol = true;
            this.GridSearch.FixedCols = 0;
            this.GridSearch.Location = new System.Drawing.Point(30, 65);
            this.GridSearch.Name = "GridSearch";
            this.GridSearch.RowHeadersVisible = false;
            this.GridSearch.Rows = 1;
            this.GridSearch.Size = new System.Drawing.Size(609, 440);
            this.ToolTip1.SetToolTip(this.GridSearch, null);
            this.GridSearch.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridSearch_MouseMoveEvent);
            this.GridSearch.DoubleClick += new System.EventHandler(this.GridSearch_DblClick);
            this.GridSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.GridSearch_KeyDownEvent);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(54, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(250, 15);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "DELETED DOG OR OWNER RECORD";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Shape1
            // 
            this.Shape1.BackColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.Shape1.Location = new System.Drawing.Point(30, 30);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(9, 10);
            this.Shape1.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.Shape1, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuContinue,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuContinue
            // 
            this.mnuContinue.Index = 0;
            this.mnuContinue.Name = "mnuContinue";
            this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuContinue.Text = "Open Record";
            this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdOpenRecord
            // 
            this.cmdOpenRecord.AppearanceKey = "acceptButton";
            this.cmdOpenRecord.Location = new System.Drawing.Point(244, 30);
            this.cmdOpenRecord.Name = "cmdOpenRecord";
            this.cmdOpenRecord.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdOpenRecord.Size = new System.Drawing.Size(148, 48);
            this.cmdOpenRecord.Text = "Open Record";
            this.cmdOpenRecord.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // frmDogSearchResults
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(665, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmDogSearchResults";
            this.Text = "Search Results";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDogSearchResults_Load);
            this.Resize += new System.EventHandler(this.frmDogSearchResults_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDogSearchResults_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOpenRecord)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdOpenRecord;
	}
}