//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmVoidDog : BaseForm
	{
		public frmVoidDog()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVoidDog InstancePtr
		{
			get
			{
				return (frmVoidDog)Sys.GetInstance(typeof(frmVoidDog));
			}
		}

		protected frmVoidDog _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int KeyCol;
		int NameCol;
		int DescriptionCol;
		int AmountCol;
		bool boolOnlyOwnTransactions;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (Information.IsDate(txtTransactionDate.Text))
			{
				rsInfo.OpenRecordset("SELECT * FROM TransactionTable WHERE TransactionDate = '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtTransactionDate.Text)) + "'", "TWCK0000.vb1");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					LoadGrid();
					fraTransactionDate.Visible = false;
					vsTransactions.Visible = true;
					chkRevertInfo.Visible = true;
					vsTransactions.Focus();
				}
				else
				{
					MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtTransactionDate.Focus();
				}
			}
		}

		public void cmdOk_Click()
		{
			cmdOk_Click(cmdProcessSave, new System.EventArgs());
		}

		private void frmVoidDog_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
			{
				return;
			}
			this.Refresh();
			if (fraTransactionDate.Visible == true)
			{
				txtTransactionDate.Focus();
			}
			//FC:FINAL:DDU:#i2306 - add expand button to all rows
			this.vsTransactions.AddExpandButton(1);
		}

		private void frmVoidDog_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVoidDog properties;
			//frmVoidDog.FillStyle	= 0;
			//frmVoidDog.ScaleWidth	= 9045;
			//frmVoidDog.ScaleHeight	= 7245;
			//frmVoidDog.LinkTopic	= "Form2";
			//frmVoidDog.LockControls	= -1  'True;
			//frmVoidDog.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			KeyCol = 1;
			NameCol = 2;
			DescriptionCol = 3;
			AmountCol = 4;
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTVOIDDOGTRANSACTIONS)) == "F")
			{
				boolOnlyOwnTransactions = false;
			}
			else
			{
				boolOnlyOwnTransactions = true;
			}
			vsTransactions.ColHidden(KeyCol, true);
			vsTransactions.TextMatrix(vsTransactions.Rows - 1, NameCol, "Name");
			vsTransactions.TextMatrix(vsTransactions.Rows - 1, DescriptionCol, "Description");
			vsTransactions.TextMatrix(vsTransactions.Rows - 1, AmountCol, "Amount");
			vsTransactions.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsTransactions.ColFormat(AmountCol, "#,##0.00");
			vsTransactions.ColWidth(NameCol, FCConvert.ToInt32(vsTransactions.WidthOriginal * 0.45));
			vsTransactions.ColWidth(DescriptionCol, FCConvert.ToInt32(vsTransactions.WidthOriginal * 0.32));
			fraTransactionDate.Visible = true;
			txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmVoidDog_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmVoidDog_Resize(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			bool blnCollapsed;
			int intRows;
			intRows = 1;
			blnCollapsed = false;
			for (counter = 1; counter <= (vsTransactions.Rows - 1); counter++)
			{
				if (vsTransactions.RowOutlineLevel(counter) == 0)
				{
					if (vsTransactions.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						intRows += 1;
						blnCollapsed = true;
					}
					else
					{
						intRows += 1;
						blnCollapsed = false;
					}
				}
				else
				{
					if (!blnCollapsed)
					{
						if (vsTransactions.IsCollapsed(counter) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							intRows += 1;
						}
					}
				}
			}
			//FC:FINAL:DDU:#i2206 - designer part manages this
			//if (intRows <= 22)
			//{
			//	vsTransactions.Height = (vsTransactions.RowHeight(0) * intRows) + 75;
			//}
			//else
			//{
			//	vsTransactions.Height = (vsTransactions.RowHeight(0) * 22) + 75;
			//}
			vsTransactions.ColWidth(NameCol, FCConvert.ToInt32(vsTransactions.WidthOriginal * 0.45));
			vsTransactions.ColWidth(DescriptionCol, FCConvert.ToInt32(vsTransactions.WidthOriginal * 0.32));
            //fraTransactionDate.CenterToContainer(this.ClientArea);
        }

		public void Form_Resize()
		{
			frmVoidDog_Resize(this, new System.EventArgs());
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void LoadGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsDogInfo = new clsDRWrapper();
			int counter;
			// vbPorter upgrade warning: counter2 As int	OnWriteFCConvert.ToInt32(
			int counter2;
			string strToolTip = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strComma = "";
			string[] strAry = null;
			vsTransactions.Rows = 1;
			if (!boolOnlyOwnTransactions)
			{
				rsInfo.OpenRecordset("SELECT * FROM TransactionTable WHERE TransactionDate = '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtTransactionDate.Text)) + "' ORDER BY TransactionPerson", "TWCK0000.vb1");
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM TransactionTable WHERE TransactionDate = '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtTransactionDate.Text)) + "' and USERID = " + modGlobalConstants.Statics.clsSecurityClass.Get_UserID() + " ORDER BY TransactionPerson", "TWCK0000.vb1");
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsTransactions.Rows += 1;
					vsTransactions.TextMatrix(vsTransactions.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					vsTransactions.TextMatrix(vsTransactions.Rows - 1, NameCol, FCConvert.ToString(rsInfo.Get_Fields_String("TransactionPerson")));
					vsTransactions.TextMatrix(vsTransactions.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields_String("Description")));
					vsTransactions.TextMatrix(vsTransactions.Rows - 1, AmountCol, FCConvert.ToString(rsInfo.Get_Fields("TotalAmount")));
					vsTransactions.RowOutlineLevel(vsTransactions.Rows - 1, 0);
					vsTransactions.IsSubtotal(vsTransactions.Rows - 1, true);
					//vsTransactions.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsTransactions.Rows - 1, KeyCol, vsTransactions.Rows - 1, vsTransactions.Cols - 1, 0x80000017);
					//vsTransactions.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsTransactions.Rows - 1, KeyCol, vsTransactions.Rows - 1, vsTransactions.Cols - 1, 0x80000018);
					rsDetailInfo.OpenRecordset("SELECT * FROM DogTransactions WHERE TransactionNumber = " + rsInfo.Get_Fields_Int32("ID"), "TWCK0000.vb1");
					if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
					{
						do
						{
							vsTransactions.Rows += 1;
							vsTransactions.TextMatrix(vsTransactions.Rows - 1, KeyCol, FCConvert.ToString(rsDetailInfo.Get_Fields_Int32("ID")));
							if (FCConvert.ToBoolean(rsDetailInfo.Get_Fields_Boolean("boolKennel")))
							{
								vsTransactions.TextMatrix(vsTransactions.Rows - 1, NameCol, "Kennel Transaction");
								strToolTip = "";
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDetailInfo.Get_Fields_String("kenneldogs"))) != string.Empty)
								{
									strAry = Strings.Split(FCConvert.ToString(rsDetailInfo.Get_Fields_String("kenneldogs")), ",", -1, CompareConstants.vbTextCompare);
									strComma = "";
									for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
									{
										rsDogInfo.OpenRecordset("select dogname from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[x])), "twck0000.vb1");
										if (!rsDogInfo.EndOfFile())
										{
											strToolTip += strComma + rsDogInfo.Get_Fields_String("dogname");
										}
										strComma = ",";
									}
									// x
								}
								vsTransactions.RowData(vsTransactions.Rows - 1, strToolTip);
							}
							else
							{
								rsDogInfo.OpenRecordset("SELECT * FROM DogInfo WHERE ID = " + rsDetailInfo.Get_Fields_Int32("DogNumber"), "TWCK0000.vb1");
								if (rsDogInfo.EndOfFile() != true && rsDogInfo.BeginningOfFile() != true)
								{
									vsTransactions.TextMatrix(vsTransactions.Rows - 1, NameCol, FCConvert.ToString(rsDogInfo.Get_Fields_String("DogName")));
								}
								else
								{
									vsTransactions.TextMatrix(vsTransactions.Rows - 1, NameCol, "UNKNOWN");
								}
							}
							vsTransactions.TextMatrix(vsTransactions.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
							vsTransactions.TextMatrix(vsTransactions.Rows - 1, AmountCol, FCConvert.ToString(rsDetailInfo.Get_Fields("Amount")));
							vsTransactions.RowOutlineLevel(vsTransactions.Rows - 1, 1);
							rsDetailInfo.MoveNext();
						}
						while (rsDetailInfo.EndOfFile() != true);
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			for (counter = 1; counter >= 0; counter--)
			{
				for (counter2 = 1; counter2 <= (vsTransactions.Rows - 1); counter2++)
				{
					if (vsTransactions.RowOutlineLevel(counter2) == counter)
					{
						vsTransactions.IsCollapsed(counter2, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					}
				}
			}
            modColorScheme.ColorGrid(vsTransactions);
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			DialogResult Ans = 0;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsInventoryInfo = new clsDRWrapper();
			clsDRWrapper rsKennelInfo = new clsDRWrapper();
			clsDRWrapper rsDogInfo = new clsDRWrapper();
			string[] strAry = null;
			int x;
			int lngTransactionNumber = 0;
			clsDRWrapper rsTransInfo = new clsDRWrapper();
			int TempRow = 0;
			bool boolReturnTag = false;
			clsDRWrapper rsDogOwner = new clsDRWrapper();
			if (fraTransactionDate.Visible == true)
			{
				cmdOk_Click();
			}
			else
			{
				TempRow = vsTransactions.Row;
				if (TempRow > 0)
				{
					if (vsTransactions.RowOutlineLevel(TempRow) == 0)
					{
						// do nothing
						MessageBox.Show("You must select a transaction to void", "No Transaction Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						Ans = MessageBox.Show("Once you have voided this transaction you will not be able to get this information back.  Are you sure you wish to void this transaction?", "Void Transaction?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (Ans == DialogResult.Yes)
						{
							rsInfo.OpenRecordset("SELECT * FROM DogTransactions WHERE ID = " + vsTransactions.TextMatrix(TempRow, KeyCol), "TWCK0000.vb1");
							if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
							{
								modGlobalFunctions.AddCYAEntry("CK", "Voided dog transaction ID " + vsTransactions.TextMatrix(TempRow, KeyCol), "Transaction Number " + rsInfo.Get_Fields_Int32("transactionnumber"), "Transaction Date " + rsInfo.Get_Fields_DateTime("transactiondate"), " Amount " + rsInfo.Get_Fields("Amount") + " Owner Num " + rsInfo.Get_Fields_Int32("ownernum"), "Description " + rsInfo.Get_Fields_String("description"));
								if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("boolKennel")))
								{
									if (rsInfo.Get_Fields_DateTime("OldIssueDate") == DateTime.FromOADate(0) || fecherFoundation.FCUtils.IsEmptyDateTime(rsInfo.Get_Fields_DateTime("OldIssueDate")))
									{
										Ans = MessageBox.Show("Do you wish to return the kennel license to inventory?", "Return Kennel License?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
										if (Ans == DialogResult.Yes)
										{
											rsInventoryInfo.OpenRecordset("SELECT * FROM DogInventory WHERE ID = " + rsInfo.Get_Fields_Int32("KennelLic1Num") + " AND Type = 'K'", "TWCK0000.vb1");
											if (rsInventoryInfo.EndOfFile() != true && rsInventoryInfo.BeginningOfFile() != true)
											{
												rsInventoryInfo.Edit();
												rsInventoryInfo.Set_Fields("Status", "Active");
												rsInventoryInfo.Update();
											}
											rsKennelInfo.OpenRecordset("SELECT * FROM KennelLicense WHERE LicenseID = " + rsInfo.Get_Fields_Int32("KennelLic1Num"), "TWCK0000.vb1");
											if (rsKennelInfo.EndOfFile() != true && rsKennelInfo.BeginningOfFile() != true)
											{
												strAry = Strings.Split(FCConvert.ToString(rsKennelInfo.Get_Fields_String("DogNumbers")), ",", -1, CompareConstants.vbTextCompare);
												for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
												{
													rsDogInfo.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[x])), "twck0000.vb1");
													if (!rsDogInfo.EndOfFile())
													{
														rsDogInfo.Edit();
														rsDogInfo.Set_Fields("KennelDog", false);
														rsDogInfo.Update();
													}
												}
												rsKennelInfo.Delete();
											}
										}
									}
									else
									{
										rsKennelInfo.OpenRecordset("SELECT * FROM KennelLicense WHERE LicenseID = " + rsInfo.Get_Fields_Int32("KennelLic1Num"), "TWCK0000.vb1");
										if (!rsKennelInfo.EndOfFile())
										{
											if (chkRevertInfo.CheckState == Wisej.Web.CheckState.Checked)
											{
												rsKennelInfo.Edit();
												rsKennelInfo.Set_Fields("ReIssueDate", rsInfo.Get_Fields_DateTime("OldIssueDate"));
												if (Conversion.Val(rsInfo.Get_Fields_Int32("oldyear")) > 0)
												{
													rsKennelInfo.Set_Fields("Year", FCConvert.ToString(Conversion.Val(rsInfo.Get_Fields_Int32("OldYear"))));
												}
												rsKennelInfo.Update();
											}
										}
									}
								}
								else
								{
									rsDogInfo.OpenRecordset("SELECT * FROM DogInfo WHERE OwnerNum = " + rsInfo.Get_Fields_Int32("OwnerNum") + " and ID = " + rsInfo.Get_Fields_Int32("DogNumber"), "TWCK0000.vb1");
									boolReturnTag = false;
									if (rsDogInfo.EndOfFile() != true)
									{
										if (FCConvert.ToString(rsDogInfo.Get_Fields_String("taglicnum")) != fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_Int32("OldTagNumber"))) && FCConvert.ToString(rsDogInfo.Get_Fields_String("taglicnum")) == fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("tagnumber"))))
										{
											Ans = MessageBox.Show("Do you wish to return the tags to inventory?", "Return Tags?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
											if (Ans == DialogResult.Yes)
											{
												boolReturnTag = true;
                                                if (!rsDogInfo.Get_Fields_Boolean("IsDangerousDog"))
                                                {
                                                    rsInventoryInfo.OpenRecordset("SELECT * FROM DogInventory WHERE StickerNumber = '" + rsDogInfo.Get_Fields_String("taglicnum") + "' AND Type = 'T' and [year] = " + rsDogInfo.Get_Fields("Year"), "TWCK0000.vb1");
                                                }
                                                else
                                                {
                                                    rsInventoryInfo.OpenRecordset("SELECT * FROM DogInventory WHERE StickerNumber = '" + rsDogInfo.Get_Fields_String("taglicnum") + "' AND Type = 'D' ", "TWCK0000.vb1");
                                                }
												
												if (rsInventoryInfo.EndOfFile() != true && rsInventoryInfo.BeginningOfFile() != true)
												{
													rsInventoryInfo.Edit();
													rsInventoryInfo.Set_Fields("Status", "Active");
													rsInventoryInfo.Update();
												}
											}
										}
										// If rsDogInfo.Fields("stickerlicnum") <> rsInfo.Fields("OldSticker") Then
										// ans = MsgBox("Do you wish to return the stickers to inventory?", vbQuestion + vbYesNo, "Return Stickers?")
										// If ans = vbYes Then
										// rsInventoryInfo.OpenRecordset "SELECT * FROM DogInventory WHERE StickerNumber = " & rsDogInfo.Fields("stickerlicnum") & " AND Type = 'S'", "TWCK0000.vb1"
										// If rsInventoryInfo.EndOfFile <> True And rsInventoryInfo.BeginningOfFile <> True Then
										// rsInventoryInfo.Edit
										// rsInventoryInfo.Fields("Status") = "Active"
										// rsInventoryInfo.Update
										// End If
										// End If
										// End If
										if (chkRevertInfo.CheckState == Wisej.Web.CheckState.Checked)
										{
											rsDogInfo.Edit();
											rsDogInfo.Set_Fields("stickerlicnum", rsInfo.Get_Fields_Int32("OldSticker"));
											rsDogInfo.Set_Fields("taglicnum", rsInfo.Get_Fields_Int32("OldTagNumber"));
											if (boolReturnTag && rsInfo.Get_Fields_Int32("oldtagnumber") == rsInfo.Get_Fields("tagnumber"))
												rsDogInfo.Set_Fields("taglicnum", 0);
											if (Information.IsDate(rsInfo.Get_Fields("oldissuedate")))
											{
												rsDogInfo.Set_Fields("rabstickerissue", rsInfo.Get_Fields_DateTime("oldissuedate"));
											}
											else
											{
												// If rsInfo.Fields("boollicense") Then
												rsDogInfo.Set_Fields("rabstickerissue", 0);
												// End If
											}
											// If rsInfo.Fields("boollicense") Then
											rsDogInfo.Set_Fields("Year", FCConvert.ToString(Conversion.Val(rsInfo.Get_Fields_Int32("OldYear"))));
											// End If
											rsDogInfo.Update();
										}
									}
								}
								lngTransactionNumber = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("TransactionNumber"));
								rsTransInfo.OpenRecordset("SELECT * FROM TransactionTable WHERE ID = " + FCConvert.ToString(lngTransactionNumber), "TWCK0000.vb1");
								if (rsTransInfo.EndOfFile() != true && rsTransInfo.BeginningOfFile() != true)
								{
									rsTransInfo.Edit();
									rsTransInfo.Set_Fields("TotalAmount", Conversion.Val(rsTransInfo.Get_Fields("TotalAmount")) - Conversion.Val(rsInfo.Get_Fields("Amount")));
									rsTransInfo.Set_Fields("Amount2", Conversion.Val(rsTransInfo.Get_Fields("Amount2")) - Conversion.Val(rsInfo.Get_Fields_Double("TownFee")));
									rsTransInfo.Set_Fields("Amount3", Conversion.Val(rsTransInfo.Get_Fields("Amount3")) - Conversion.Val(rsInfo.Get_Fields_Double("StateFee")));
									rsTransInfo.Set_Fields("Amount4", Conversion.Val(rsTransInfo.Get_Fields("Amount4")) - Conversion.Val(rsInfo.Get_Fields_Double("ClerkFee")));
									rsTransInfo.Set_Fields("Amount5", Conversion.Val(rsTransInfo.Get_Fields("Amount5")) - Conversion.Val(rsInfo.Get_Fields("LateFee")));
									rsTransInfo.Update();
								}
								if (modCashReceiptingData.Statics.bolFromWindowsCR)
								{
									modCashReceiptingData.Statics.typeCRData.ID = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("DogNumber"));
									modCashReceiptingData.Statics.typeCRData.Type = "DOG";
									modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(FCConvert.ToString(rsTransInfo.Get_Fields_String("TransactionPerson")), 30);
									rsDogOwner.OpenRecordset("SELECT * FROM DogOwner WHERE ID = " + rsInfo.Get_Fields_Int32("OwnerNum"), "TWCK0000.vb1");
									if (rsDogOwner.EndOfFile() != true && rsDogOwner.BeginningOfFile() != true)
									{
										modCashReceiptingData.Statics.typeCRData.PartyID = FCConvert.ToInt32(rsDogOwner.Get_Fields_Int32("PartyID"));
									}
									else
									{
										modCashReceiptingData.Statics.typeCRData.PartyID = 0;
									}
									modCashReceiptingData.Statics.typeCRData.Reference = Strings.Left(FCConvert.ToString(rsInfo.Get_Fields_String("Description")), 10);
									modCashReceiptingData.Statics.typeCRData.Control1 = FCConvert.ToString(rsInfo.Get_Fields_Int32("TransactionNumber"));
									modCashReceiptingData.Statics.typeCRData.Control2 = " ";
									modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format("0", "000000.00"));
									// vital
									modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(Conversion.Val(rsInfo.Get_Fields_Double("TownFee")) * -1, "000000.00"));
									// town
									modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(Conversion.Val(rsInfo.Get_Fields_Double("StateFee")) * -1, "000000.00"));
									// state
									modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(Conversion.Val(rsInfo.Get_Fields_Double("ClerkFee")) * -1, "000000.00"));
									// clerk
									modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(Conversion.Val(rsInfo.Get_Fields("LateFee")) * -1, "000000.00"));
									// late
									modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format("0", "000000.00"));
									// kennel
									modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rsInfo.Get_Fields_Int32("towncode"))));
									modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
								}
								rsInfo.Delete();
								rsInfo.Update();
								rsInfo.MoveNext();
								rsTransInfo.OpenRecordset("SELECT * FROM DogTransactions WHERE TransactionNumber = " + FCConvert.ToString(lngTransactionNumber), "TWCK0000.vb1");
								if (rsTransInfo.EndOfFile() != true && rsTransInfo.BeginningOfFile() != true)
								{
									// do nothing
								}
								else
								{
									rsTransInfo.OpenRecordset("SELECT * FROM TransactionTable WHERE ID = " + FCConvert.ToString(lngTransactionNumber), "TWCK0000.vb1");
									if (rsTransInfo.EndOfFile() != true && rsTransInfo.BeginningOfFile() != true)
									{
										rsTransInfo.Delete();
										rsTransInfo.Update();
										rsTransInfo.MoveNext();
									}
								}
								if (modCashReceiptingData.Statics.bolFromWindowsCR)
								{
									modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
									Application.Exit();
								}
								else
								{
									LoadGrid();
								}
							}
						}
					}
				}
				else
				{
					MessageBox.Show("You must select a transaction to void", "No Transaction Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private void vsTransactions_AfterCollapse(object sender, DataGridViewRowEventArgs e)
		{
			Form_Resize();
		}

		private void vsTransactions_DblClick(object sender, System.EventArgs e)
		{
			if (vsTransactions.MouseRow <= 0)
				return;
			vsTransactions.Row = vsTransactions.MouseRow;
			mnuProcessSave_Click();
		}

		private void vsTransactions_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsTransactions[e.ColumnIndex, e.RowIndex];
            int lngRow;
			string strToolTip = "";
			lngRow = vsTransactions.GetFlexRowIndex(e.RowIndex);
			if (lngRow > 0)
			{
				strToolTip = FCConvert.ToString(vsTransactions.RowData(lngRow));
				// Else
				// strToolTip = "Click on a column heading to sort by that column"
			}
			//ToolTip1.SetToolTip(vsTransactions, strToolTip);
			cell.ToolTipText = strToolTip;
		}

		private void vsTransactions_RowColChange(object sender, System.EventArgs e)
		{
			if (vsTransactions.RowOutlineLevel(vsTransactions.Row) == 0)
			{
				vsTransactions.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
			else
			{
				vsTransactions.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
			}
		}
	}
}
