﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogReminders.
	/// </summary>
	partial class rptDogReminders
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDogReminders));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSalutation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSigniture = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.RichEdit1 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalutation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSigniture)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Field2,
				this.Field3,
				this.txtDate,
				this.Field4,
				this.txtAddress1,
				this.txtAddress2,
				this.txtAddress3,
				this.Field5,
				this.txtSalutation,
				this.Field23,
				this.txtSigniture,
				this.Field25,
				this.Field26,
				this.Field27,
				this.Field28,
				this.Field29,
				this.Field30,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4,
				this.Line5,
				this.Line6,
				this.Line7,
				this.RichEdit1
			});
			this.Detail.Height = 4.854167F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.SubReport1
			});
			this.ReportFooter.Height = 0.4791667F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.1666667F;
			this.Field1.Left = 0.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Arial\'; font-weight: bold; text-align: center";
			this.Field1.Text = "NOTICE TO UNLICENSED DOG OWNER/KEEPER";
			this.Field1.Top = 0F;
			this.Field1.Width = 7.270833F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.2291667F;
			this.Field2.Left = 0.0625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Arial\'; font-weight: bold; text-align: center";
			this.Field2.Text = "Title 7, M. R. S. A. #3921, #3931, & #3943";
			this.Field2.Top = 0.1875F;
			this.Field2.Width = 7.270833F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.1875F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Courier New\'; font-weight: bold; ddo-char-set: 1";
			this.Field3.Text = "Date:";
			this.Field3.Top = 0.6875F;
			this.Field3.Width = 0.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 0.75F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.txtDate.Text = "Date:";
			this.txtDate.Top = 0.6875F;
			this.txtDate.Width = 1.375F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 0.1875F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Courier New\'; font-weight: bold; ddo-char-set: 1";
			this.Field4.Text = "To:";
			this.Field4.Top = 0.9375F;
			this.Field4.Width = 0.5625F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.1875F;
			this.txtAddress1.Left = 0.75F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.txtAddress1.Text = "Date:";
			this.txtAddress1.Top = 0.9375F;
			this.txtAddress1.Width = 3F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.1875F;
			this.txtAddress2.Left = 0.75F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.txtAddress2.Text = "Date:";
			this.txtAddress2.Top = 1.125F;
			this.txtAddress2.Width = 3F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.Height = 0.1875F;
			this.txtAddress3.Left = 0.75F;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.txtAddress3.Text = "Date:";
			this.txtAddress3.Top = 1.3125F;
			this.txtAddress3.Width = 3F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 0.1875F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Field5.Text = "Dear Dog Owner/Keeper:";
			this.Field5.Top = 1.6875F;
			this.Field5.Width = 3F;
			// 
			// txtSalutation
			// 
			this.txtSalutation.Height = 0.1875F;
			this.txtSalutation.Left = 4.125F;
			this.txtSalutation.Name = "txtSalutation";
			this.txtSalutation.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.txtSalutation.Text = null;
			this.txtSalutation.Top = 2.375F;
			this.txtSalutation.Width = 1.5F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.5F;
			this.Field23.Left = 0.75F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Field23.Text = "Maine certification of rabies vaccination means a current up to date certificate";
			this.Field23.Top = 2.5625F;
			this.Field23.Width = 2.75F;
			// 
			// txtSigniture
			// 
			this.txtSigniture.Height = 1.6875F;
			this.txtSigniture.Left = 4.125F;
			this.txtSigniture.Name = "txtSigniture";
			this.txtSigniture.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.txtSigniture.Text = "txtSigniture";
			this.txtSigniture.Top = 3.0625F;
			this.txtSigniture.Width = 3.125F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1875F;
			this.Field25.Left = 0.75F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Courier New\'; font-weight: bold; ddo-char-set: 1";
			this.Field25.Text = "FOR OFFICE USE ONLY";
			this.Field25.Top = 3.625F;
			this.Field25.Width = 1.75F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 0.75F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Field26.Text = "Submitted:";
			this.Field26.Top = 3.8125F;
			this.Field26.Width = 1.0625F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.1875F;
			this.Field27.Left = 0.75F;
			this.Field27.Name = "Field27";
			this.Field27.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Field27.Text = "License Fee:";
			this.Field27.Top = 4F;
			this.Field27.Width = 1.0625F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.1875F;
			this.Field28.Left = 0.75F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Field28.Text = "Late Fee:";
			this.Field28.Top = 4.1875F;
			this.Field28.Width = 1.0625F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 0.75F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Field29.Text = "TOTAL:";
			this.Field29.Top = 4.375F;
			this.Field29.Width = 1.0625F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.1875F;
			this.Field30.Left = 0.75F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Field30.Text = "Date Paid:";
			this.Field30.Top = 4.5625F;
			this.Field30.Width = 1.0625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 4.125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3F;
			this.Line1.Width = 3.125F;
			this.Line1.X1 = 4.125F;
			this.Line1.X2 = 7.25F;
			this.Line1.Y1 = 3F;
			this.Line1.Y2 = 3F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 4F;
			this.Line2.Width = 2F;
			this.Line2.X1 = 1.875F;
			this.Line2.X2 = 3.875F;
			this.Line2.Y1 = 4F;
			this.Line2.Y2 = 4F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 1.875F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 4.1875F;
			this.Line3.Width = 2F;
			this.Line3.X1 = 1.875F;
			this.Line3.X2 = 3.875F;
			this.Line3.Y1 = 4.1875F;
			this.Line3.Y2 = 4.1875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 1.875F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 4.375F;
			this.Line4.Width = 2F;
			this.Line4.X1 = 1.875F;
			this.Line4.X2 = 3.875F;
			this.Line4.Y1 = 4.375F;
			this.Line4.Y2 = 4.375F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 1.875F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 4.5625F;
			this.Line5.Width = 2F;
			this.Line5.X1 = 1.875F;
			this.Line5.X2 = 3.875F;
			this.Line5.Y1 = 4.5625F;
			this.Line5.Y2 = 4.5625F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 1.875F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 4.5625F;
			this.Line6.Width = 2F;
			this.Line6.X1 = 1.875F;
			this.Line6.X2 = 3.875F;
			this.Line6.Y1 = 4.5625F;
			this.Line6.Y2 = 4.5625F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 1.875F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 4.75F;
			this.Line7.Width = 2F;
			this.Line7.X1 = 1.875F;
			this.Line7.X2 = 3.875F;
			this.Line7.Y1 = 4.75F;
			this.Line7.Y2 = 4.75F;
			// 
			// RichEdit1
			// 
			//this.RichEdit1.Font = new System.Drawing.Font("Arial", 10F);
			this.RichEdit1.Height = 0.0625F;
			this.RichEdit1.Left = 0.5625F;
			this.RichEdit1.Name = "RichEdit1";
			this.RichEdit1.RTF = resources.GetString("RichEdit1.RTF");
			this.RichEdit1.Top = 2.0625F;
			this.RichEdit1.Width = 6.6875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label1.Text = "Emails to be sent to:";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 2F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0.1875F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.375F;
			this.SubReport1.Width = 7.0625F;
			// 
			// rptDogReminders
			//
			// 
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalutation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSigniture)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSalutation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSigniture;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
