//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmBirthReport.
	/// </summary>
	partial class frmBirthReport
	{
		public fecherFoundation.FCComboBox cmbSort;
		public fecherFoundation.FCLabel lblSort;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCGrid Grid;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbSort = new fecherFoundation.FCComboBox();
            this.lblSort = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.Grid = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 400);
            this.BottomPanel.Size = new System.Drawing.Size(658, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbSort);
            this.ClientArea.Controls.Add(this.lblSort);
            this.ClientArea.Size = new System.Drawing.Size(658, 340);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(658, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(143, 30);
            this.HeaderText.Text = "Birth Report";
            // 
            // cmbSort
            // 
            this.cmbSort.AutoSize = false;
            this.cmbSort.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSort.FormattingEnabled = true;
            this.cmbSort.Items.AddRange(new object[] {
            "Name",
            "Birthdate",
            "Mother\'s name",
            "Birthplace"});
            this.cmbSort.Location = new System.Drawing.Point(150, 30);
            this.cmbSort.Name = "cmbSort";
            this.cmbSort.Size = new System.Drawing.Size(238, 40);
            this.cmbSort.TabIndex = 6;
            this.cmbSort.Text = "Name";
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(30, 44);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(62, 15);
            this.lblSort.TabIndex = 7;
            this.lblSort.Text = "SORT BY";
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.Grid);
            this.Frame2.Location = new System.Drawing.Point(30, 90);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(600, 231);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Range";
            // 
            // Grid
            // 
            this.Grid.Anchor = AnchorStyles.None;
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 3;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.Grid.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedRows = 0;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(0, 30);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 4;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.Size = new System.Drawing.Size(600, 163);
            this.Grid.StandardTab = true;
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.Grid.TabIndex = 6;
            this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(235, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(175, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmBirthReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(658, 508);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBirthReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Birth Report";
            this.Load += new System.EventHandler(this.frmBirthReport_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBirthReport_KeyDown);
            this.Resize += new System.EventHandler(this.frmBirthReport_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}