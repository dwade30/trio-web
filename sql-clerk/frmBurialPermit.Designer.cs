//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmBurialPermit.
	/// </summary>
	partial class frmBurialPermit
	{
		public fecherFoundation.FCComboBox cmbCremains;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkPermitType;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkDisposition;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public Global.T2KOverTypeBox txtNameOfClerkOrSubregistrar;
		public fecherFoundation.FCButton cmdFind;
		public fecherFoundation.FCTextBox txtDateSigned;
		public fecherFoundation.FCTextBox txtDateOfDeath;
		public fecherFoundation.FCTextBox txtClerkTown;
		public fecherFoundation.FCCheckBox chkArmedForces;
		public fecherFoundation.FCTextBox txtDateOfDisposition;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkAuthorizationApplication;
		public fecherFoundation.FCCheckBox chkAuthorizationMedicalExaminer;
		public fecherFoundation.FCCheckBox chkAuthorizationReport;
		public fecherFoundation.FCCheckBox chkAuthorizationCertificate;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkPermitType_6;
		public fecherFoundation.FCCheckBox chkPermitType_5;
		public fecherFoundation.FCCheckBox chkPermitType_4;
		public fecherFoundation.FCCheckBox chkPermitType_3;
		public fecherFoundation.FCCheckBox chkPermitType_2;
		public fecherFoundation.FCCheckBox chkPermitType_1;
		public fecherFoundation.FCCheckBox chkPermitType_0;
		public fecherFoundation.FCCheckBox chkBurial;
		public fecherFoundation.FCCheckBox chkCremation;
		public Global.T2KOverTypeBox txtMiddleName;
		public Global.T2KOverTypeBox txtFirstName;
		public Global.T2KOverTypeBox txtLastName;
		public Global.T2KOverTypeBox txtAge;
		public Global.T2KOverTypeBox txtCityOrTown;
		public Global.T2KOverTypeBox txtPlaceOfDeathState;
		public Global.T2KOverTypeBox txtNameOfFuneralEstablishment;
		public Global.T2KOverTypeBox txtRace;
		public Global.T2KOverTypeBox txtPlaceOfDisposition;
		public Global.T2KOverTypeBox txtDesignation;
		public Global.T2KOverTypeBox txtPlaceOfDeathCity;
		public Global.T2KOverTypeBox txtLicenseNumber;
		public Global.T2KOverTypeBox txtAddress1;
		public Global.T2KOverTypeBox txtSex;
		public Global.T2KOverTypeBox txtAddress2;
		public Global.T2KOverTypeBox txtCity;
		public Global.T2KOverTypeBox txtState;
		public Global.T2KOverTypeBox txtZip;
		public FCGrid gridTownCode;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCCheckBox chkDisposition_6;
		public fecherFoundation.FCCheckBox chkDisposition_5;
		public fecherFoundation.FCCheckBox chkDisposition_4;
		public fecherFoundation.FCCheckBox chkDisposition_3;
		public fecherFoundation.FCCheckBox chkDisposition_2;
		public fecherFoundation.FCCheckBox chkDisposition_1;
		public fecherFoundation.FCCheckBox chkDisposition_0;
		public fecherFoundation.FCTextBox DispositionDate;
		public Global.T2KOverTypeBox txtNameOfCemetery;
		public Global.T2KOverTypeBox txtLocation;
		public Global.T2KOverTypeBox txtField3;
		public Global.T2KOverTypeBox txtField5;
		public fecherFoundation.FCLabel lblField4;
		public fecherFoundation.FCLabel lblField1;
		public fecherFoundation.FCLabel lblField3;
		public fecherFoundation.FCLabel lblfraDate;
		public fecherFoundation.FCLabel lblField2;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox CremDate;
		public Global.T2KOverTypeBox CremLoc;
		public Global.T2KOverTypeBox CremName;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public Global.T2KOverTypeBox T2KTemporarySignature;
		public Global.T2KOverTypeBox T2KTemporaryLocation;
		public Global.T2KOverTypeBox T2KTemporaryCemetery;
		public fecherFoundation.FCTextBox txtTemporaryDate;
		public fecherFoundation.FCCheckBox chkTemporaryStorage;
		public fecherFoundation.FCPictureBox imgDocuments;
		public fecherFoundation.FCLabel Label1_21;
		public fecherFoundation.FCLabel Label1_20;
		public fecherFoundation.FCLabel Label1_19;
		public fecherFoundation.FCLabel Label1_18;
		public fecherFoundation.FCLabel Label1_17;
		public fecherFoundation.FCLabel Label1_16;
		public fecherFoundation.FCLabel Label1_15;
		public fecherFoundation.FCLabel Label1_14;
		public fecherFoundation.FCLabel Label1_13;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAssociateDeathCertificate;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem Separator;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocs;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveQuit;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBurialPermit));
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbCremains = new fecherFoundation.FCComboBox();
            this.txtNameOfClerkOrSubregistrar = new Global.T2KOverTypeBox();
            this.cmdFind = new fecherFoundation.FCButton();
            this.txtDateSigned = new fecherFoundation.FCTextBox();
            this.txtDateOfDeath = new fecherFoundation.FCTextBox();
            this.txtClerkTown = new fecherFoundation.FCTextBox();
            this.chkArmedForces = new fecherFoundation.FCCheckBox();
            this.txtDateOfDisposition = new fecherFoundation.FCTextBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.chkAuthorizationApplication = new fecherFoundation.FCCheckBox();
            this.chkAuthorizationMedicalExaminer = new fecherFoundation.FCCheckBox();
            this.chkAuthorizationReport = new fecherFoundation.FCCheckBox();
            this.chkAuthorizationCertificate = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkPermitType_6 = new fecherFoundation.FCCheckBox();
            this.chkPermitType_5 = new fecherFoundation.FCCheckBox();
            this.chkPermitType_4 = new fecherFoundation.FCCheckBox();
            this.chkPermitType_3 = new fecherFoundation.FCCheckBox();
            this.chkPermitType_2 = new fecherFoundation.FCCheckBox();
            this.chkPermitType_1 = new fecherFoundation.FCCheckBox();
            this.chkPermitType_0 = new fecherFoundation.FCCheckBox();
            this.chkBurial = new fecherFoundation.FCCheckBox();
            this.chkCremation = new fecherFoundation.FCCheckBox();
            this.txtMiddleName = new Global.T2KOverTypeBox();
            this.txtFirstName = new Global.T2KOverTypeBox();
            this.txtLastName = new Global.T2KOverTypeBox();
            this.txtAge = new Global.T2KOverTypeBox();
            this.txtCityOrTown = new Global.T2KOverTypeBox();
            this.txtPlaceOfDeathState = new Global.T2KOverTypeBox();
            this.txtNameOfFuneralEstablishment = new Global.T2KOverTypeBox();
            this.txtRace = new Global.T2KOverTypeBox();
            this.txtPlaceOfDisposition = new Global.T2KOverTypeBox();
            this.txtDesignation = new Global.T2KOverTypeBox();
            this.txtPlaceOfDeathCity = new Global.T2KOverTypeBox();
            this.txtLicenseNumber = new Global.T2KOverTypeBox();
            this.txtAddress1 = new Global.T2KOverTypeBox();
            this.txtSex = new Global.T2KOverTypeBox();
            this.txtAddress2 = new Global.T2KOverTypeBox();
            this.txtCity = new Global.T2KOverTypeBox();
            this.txtState = new Global.T2KOverTypeBox();
            this.txtZip = new Global.T2KOverTypeBox();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.chkDisposition_6 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_5 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_4 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_3 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_2 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_1 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_0 = new fecherFoundation.FCCheckBox();
            this.DispositionDate = new fecherFoundation.FCTextBox();
            this.txtNameOfCemetery = new Global.T2KOverTypeBox();
            this.txtLocation = new Global.T2KOverTypeBox();
            this.txtField3 = new Global.T2KOverTypeBox();
            this.txtField5 = new Global.T2KOverTypeBox();
            this.lblField4 = new fecherFoundation.FCLabel();
            this.lblField1 = new fecherFoundation.FCLabel();
            this.lblField3 = new fecherFoundation.FCLabel();
            this.lblfraDate = new fecherFoundation.FCLabel();
            this.lblField2 = new fecherFoundation.FCLabel();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.CremDate = new fecherFoundation.FCTextBox();
            this.CremLoc = new Global.T2KOverTypeBox();
            this.CremName = new Global.T2KOverTypeBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.T2KTemporarySignature = new Global.T2KOverTypeBox();
            this.T2KTemporaryLocation = new Global.T2KOverTypeBox();
            this.T2KTemporaryCemetery = new Global.T2KOverTypeBox();
            this.txtTemporaryDate = new fecherFoundation.FCTextBox();
            this.chkTemporaryStorage = new fecherFoundation.FCCheckBox();
            this.imgDocuments = new fecherFoundation.FCPictureBox();
            this.Label1_21 = new fecherFoundation.FCLabel();
            this.Label1_20 = new fecherFoundation.FCLabel();
            this.Label1_19 = new fecherFoundation.FCLabel();
            this.Label1_18 = new fecherFoundation.FCLabel();
            this.Label1_17 = new fecherFoundation.FCLabel();
            this.Label1_16 = new fecherFoundation.FCLabel();
            this.Label1_15 = new fecherFoundation.FCLabel();
            this.Label1_14 = new fecherFoundation.FCLabel();
            this.Label1_13 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuAssociateDeathCertificate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewDocs = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.Separator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveQuit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameOfClerkOrSubregistrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkArmedForces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationApplication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationMedicalExaminer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationCertificate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBurial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCremation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityOrTown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDeathState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameOfFuneralEstablishment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDisposition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesignation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDeathCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenseNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameOfCemetery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtField3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtField5)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CremLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CremName)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KTemporarySignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KTemporaryLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KTemporaryCemetery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTemporaryStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(876, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtNameOfClerkOrSubregistrar);
            this.ClientArea.Controls.Add(this.cmdFind);
            this.ClientArea.Controls.Add(this.txtDateSigned);
            this.ClientArea.Controls.Add(this.txtDateOfDeath);
            this.ClientArea.Controls.Add(this.txtClerkTown);
            this.ClientArea.Controls.Add(this.chkArmedForces);
            this.ClientArea.Controls.Add(this.txtDateOfDisposition);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.txtMiddleName);
            this.ClientArea.Controls.Add(this.txtFirstName);
            this.ClientArea.Controls.Add(this.txtLastName);
            this.ClientArea.Controls.Add(this.txtAge);
            this.ClientArea.Controls.Add(this.txtCityOrTown);
            this.ClientArea.Controls.Add(this.txtPlaceOfDeathState);
            this.ClientArea.Controls.Add(this.txtNameOfFuneralEstablishment);
            this.ClientArea.Controls.Add(this.txtRace);
            this.ClientArea.Controls.Add(this.txtPlaceOfDisposition);
            this.ClientArea.Controls.Add(this.txtDesignation);
            this.ClientArea.Controls.Add(this.txtPlaceOfDeathCity);
            this.ClientArea.Controls.Add(this.txtLicenseNumber);
            this.ClientArea.Controls.Add(this.txtAddress1);
            this.ClientArea.Controls.Add(this.txtSex);
            this.ClientArea.Controls.Add(this.txtAddress2);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.imgDocuments);
            this.ClientArea.Controls.Add(this.Label1_21);
            this.ClientArea.Controls.Add(this.Label1_20);
            this.ClientArea.Controls.Add(this.Label1_19);
            this.ClientArea.Controls.Add(this.Label1_18);
            this.ClientArea.Controls.Add(this.Label1_17);
            this.ClientArea.Controls.Add(this.Label1_16);
            this.ClientArea.Controls.Add(this.Label1_15);
            this.ClientArea.Controls.Add(this.Label1_14);
            this.ClientArea.Controls.Add(this.Label1_13);
            this.ClientArea.Controls.Add(this.Label1_12);
            this.ClientArea.Controls.Add(this.Label1_11);
            this.ClientArea.Controls.Add(this.Label1_10);
            this.ClientArea.Controls.Add(this.Label1_9);
            this.ClientArea.Controls.Add(this.Label1_8);
            this.ClientArea.Controls.Add(this.Label1_7);
            this.ClientArea.Controls.Add(this.Label1_6);
            this.ClientArea.Controls.Add(this.Label1_5);
            this.ClientArea.Controls.Add(this.Label1_4);
            this.ClientArea.Controls.Add(this.Label1_3);
            this.ClientArea.Controls.Add(this.Label1_2);
            this.ClientArea.Controls.Add(this.Label1_1);
            this.ClientArea.Controls.Add(this.Label1_0);
            this.ClientArea.Size = new System.Drawing.Size(876, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdPrintPreview);
            this.TopPanel.Size = new System.Drawing.Size(876, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(153, 30);
            this.HeaderText.Text = "Burial Permit";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbCremains
            // 
            this.cmbCremains.AutoSize = false;
            this.cmbCremains.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbCremains.FormattingEnabled = true;
            this.cmbCremains.Items.AddRange(new object[] {
            "Buried",
            "Scattered",
            "To Family"});
            this.cmbCremains.Location = new System.Drawing.Point(20, 55);
            this.cmbCremains.Name = "cmbCremains";
            this.cmbCremains.Size = new System.Drawing.Size(170, 40);
            this.cmbCremains.TabIndex = 85;
            this.cmbCremains.Text = "Buried";
            this.ToolTip1.SetToolTip(this.cmbCremains, null);
            // 
            // txtNameOfClerkOrSubregistrar
            // 
            this.txtNameOfClerkOrSubregistrar.AutoSize = false;
            this.txtNameOfClerkOrSubregistrar.LinkItem = null;
            this.txtNameOfClerkOrSubregistrar.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtNameOfClerkOrSubregistrar.LinkTopic = null;
            this.txtNameOfClerkOrSubregistrar.Location = new System.Drawing.Point(101, 848);
            this.txtNameOfClerkOrSubregistrar.MaxLength = 34;
            this.txtNameOfClerkOrSubregistrar.Name = "txtNameOfClerkOrSubregistrar";
            this.txtNameOfClerkOrSubregistrar.Size = new System.Drawing.Size(196, 40);
            this.txtNameOfClerkOrSubregistrar.TabIndex = 34;
            this.ToolTip1.SetToolTip(this.txtNameOfClerkOrSubregistrar, null);
            // 
            // cmdFind
            // 
            this.cmdFind.AppearanceKey = "actionButton";
            this.cmdFind.Image = ((System.Drawing.Image)(resources.GetObject("cmdFind.Image")));
            this.cmdFind.Location = new System.Drawing.Point(514, 317);
            this.cmdFind.Name = "cmdFind";
            this.cmdFind.Size = new System.Drawing.Size(40, 40);
            this.cmdFind.TabIndex = 100;
            this.ToolTip1.SetToolTip(this.cmdFind, "Find Funeral Establishment Info.");
            this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
            // 
            // txtDateSigned
            // 
            this.txtDateSigned.AutoSize = false;
            this.txtDateSigned.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateSigned.LinkItem = null;
            this.txtDateSigned.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDateSigned.LinkTopic = null;
            this.txtDateSigned.Location = new System.Drawing.Point(708, 848);
            this.txtDateSigned.Name = "txtDateSigned";
            this.txtDateSigned.Size = new System.Drawing.Size(131, 40);
            this.txtDateSigned.TabIndex = 36;
            this.ToolTip1.SetToolTip(this.txtDateSigned, null);
            this.txtDateSigned.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateSigned_Validating);
            // 
            // txtDateOfDeath
            // 
            this.txtDateOfDeath.AutoSize = false;
            this.txtDateOfDeath.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateOfDeath.LinkItem = null;
            this.txtDateOfDeath.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDateOfDeath.LinkTopic = null;
            this.txtDateOfDeath.Location = new System.Drawing.Point(518, 80);
            this.txtDateOfDeath.Name = "txtDateOfDeath";
            this.txtDateOfDeath.Size = new System.Drawing.Size(143, 40);
            this.txtDateOfDeath.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtDateOfDeath, null);
            this.txtDateOfDeath.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateOfDeath_Validating);
            // 
            // txtClerkTown
            // 
            this.txtClerkTown.AutoSize = false;
            this.txtClerkTown.BackColor = System.Drawing.SystemColors.Window;
            this.txtClerkTown.LinkItem = null;
            this.txtClerkTown.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtClerkTown.LinkTopic = null;
            this.txtClerkTown.Location = new System.Drawing.Point(414, 848);
            this.txtClerkTown.Name = "txtClerkTown";
            this.txtClerkTown.Size = new System.Drawing.Size(165, 40);
            this.txtClerkTown.TabIndex = 35;
            this.ToolTip1.SetToolTip(this.txtClerkTown, null);
            // 
            // chkArmedForces
            // 
            this.chkArmedForces.Location = new System.Drawing.Point(179, 280);
            this.chkArmedForces.Name = "chkArmedForces";
            this.chkArmedForces.Size = new System.Drawing.Size(185, 27);
            this.chkArmedForces.TabIndex = 10;
            this.chkArmedForces.Text = "Was in Armed Forces";
            this.ToolTip1.SetToolTip(this.chkArmedForces, null);
            // 
            // txtDateOfDisposition
            // 
            this.txtDateOfDisposition.AutoSize = false;
            this.txtDateOfDisposition.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateOfDisposition.LinkItem = null;
            this.txtDateOfDisposition.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDateOfDisposition.LinkTopic = null;
            this.txtDateOfDisposition.Location = new System.Drawing.Point(708, 798);
            this.txtDateOfDisposition.Name = "txtDateOfDisposition";
            this.txtDateOfDisposition.Size = new System.Drawing.Size(131, 40);
            this.txtDateOfDisposition.TabIndex = 33;
            this.ToolTip1.SetToolTip(this.txtDateOfDisposition, null);
            this.txtDateOfDisposition.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateOfDisposition_Validating);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.chkAuthorizationApplication);
            this.Frame2.Controls.Add(this.chkAuthorizationMedicalExaminer);
            this.Frame2.Controls.Add(this.chkAuthorizationReport);
            this.Frame2.Controls.Add(this.chkAuthorizationCertificate);
            this.Frame2.Location = new System.Drawing.Point(30, 631);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(785, 132);
            this.Frame2.TabIndex = 50;
            this.Frame2.Text = "Authorization For Permit";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // chkAuthorizationApplication
            // 
            this.chkAuthorizationApplication.Location = new System.Drawing.Point(346, 85);
            this.chkAuthorizationApplication.Name = "chkAuthorizationApplication";
            this.chkAuthorizationApplication.Size = new System.Drawing.Size(348, 27);
            this.chkAuthorizationApplication.TabIndex = 30;
            this.chkAuthorizationApplication.Text = "Application Or Court Order For Disinterment";
            this.ToolTip1.SetToolTip(this.chkAuthorizationApplication, null);
            // 
            // chkAuthorizationMedicalExaminer
            // 
            this.chkAuthorizationMedicalExaminer.AutoSize = false;
            this.chkAuthorizationMedicalExaminer.Location = new System.Drawing.Point(347, 30);
            this.chkAuthorizationMedicalExaminer.Name = "chkAuthorizationMedicalExaminer";
            this.chkAuthorizationMedicalExaminer.Size = new System.Drawing.Size(403, 45);
            this.chkAuthorizationMedicalExaminer.TabIndex = 29;
            this.chkAuthorizationMedicalExaminer.Text = "Medical Examiner\'s Release For Cremation, Burial At Sea, Use By Medical Science, " +
    "Removal From State";
            this.chkAuthorizationMedicalExaminer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTip1.SetToolTip(this.chkAuthorizationMedicalExaminer, null);
            // 
            // chkAuthorizationReport
            // 
            this.chkAuthorizationReport.Location = new System.Drawing.Point(20, 85);
            this.chkAuthorizationReport.Name = "chkAuthorizationReport";
            this.chkAuthorizationReport.Size = new System.Drawing.Size(326, 27);
            this.chkAuthorizationReport.TabIndex = 28;
            this.chkAuthorizationReport.Text = "Report Of Death (Funeral Directors Only)";
            this.ToolTip1.SetToolTip(this.chkAuthorizationReport, null);
            // 
            // chkAuthorizationCertificate
            // 
            this.chkAuthorizationCertificate.Location = new System.Drawing.Point(20, 30);
            this.chkAuthorizationCertificate.Name = "chkAuthorizationCertificate";
            this.chkAuthorizationCertificate.Size = new System.Drawing.Size(234, 27);
            this.chkAuthorizationCertificate.TabIndex = 27;
            this.chkAuthorizationCertificate.Text = "Completed Death Certificate";
            this.ToolTip1.SetToolTip(this.chkAuthorizationCertificate, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkPermitType_6);
            this.Frame1.Controls.Add(this.chkPermitType_5);
            this.Frame1.Controls.Add(this.chkPermitType_4);
            this.Frame1.Controls.Add(this.chkPermitType_3);
            this.Frame1.Controls.Add(this.chkPermitType_2);
            this.Frame1.Controls.Add(this.chkPermitType_1);
            this.Frame1.Controls.Add(this.chkPermitType_0);
            this.Frame1.Controls.Add(this.chkBurial);
            this.Frame1.Controls.Add(this.chkCremation);
            this.Frame1.Location = new System.Drawing.Point(30, 517);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(785, 104);
            this.Frame1.TabIndex = 49;
            this.Frame1.Text = "Type Of Permit";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // chkPermitType_6
            // 
            this.chkPermitType_6.Location = new System.Drawing.Point(335, 67);
            this.chkPermitType_6.Name = "chkPermitType_6";
            this.chkPermitType_6.Size = new System.Drawing.Size(178, 27);
            this.chkPermitType_6.TabIndex = 26;
            this.chkPermitType_6.Text = "Removal From State";
            this.ToolTip1.SetToolTip(this.chkPermitType_6, null);
            this.chkPermitType_6.CheckedChanged += new System.EventHandler(this.chkPermitType_CheckedChanged);
            // 
            // chkPermitType_5
            // 
            this.chkPermitType_5.Location = new System.Drawing.Point(152, 68);
            this.chkPermitType_5.Name = "chkPermitType_5";
            this.chkPermitType_5.Size = new System.Drawing.Size(147, 27);
            this.chkPermitType_5.TabIndex = 25;
            this.chkPermitType_5.Text = "Medical Science";
            this.ToolTip1.SetToolTip(this.chkPermitType_5, null);
            this.chkPermitType_5.CheckedChanged += new System.EventHandler(this.chkPermitType_CheckedChanged);
            // 
            // chkPermitType_4
            // 
            this.chkPermitType_4.Location = new System.Drawing.Point(20, 68);
            this.chkPermitType_4.Name = "chkPermitType_4";
            this.chkPermitType_4.Size = new System.Drawing.Size(121, 27);
            this.chkPermitType_4.TabIndex = 24;
            this.chkPermitType_4.Text = "Burial at Sea";
            this.ToolTip1.SetToolTip(this.chkPermitType_4, null);
            this.chkPermitType_4.CheckedChanged += new System.EventHandler(this.chkPermitType_CheckedChanged);
            // 
            // chkPermitType_3
            // 
            this.chkPermitType_3.Location = new System.Drawing.Point(335, 30);
            this.chkPermitType_3.Name = "chkPermitType_3";
            this.chkPermitType_3.Size = new System.Drawing.Size(103, 27);
            this.chkPermitType_3.TabIndex = 20;
            this.chkPermitType_3.Text = "Cremation";
            this.ToolTip1.SetToolTip(this.chkPermitType_3, null);
            this.chkPermitType_3.CheckedChanged += new System.EventHandler(this.chkPermitType_CheckedChanged);
            // 
            // chkPermitType_2
            // 
            this.chkPermitType_2.Location = new System.Drawing.Point(482, 30);
            this.chkPermitType_2.Name = "chkPermitType_2";
            this.chkPermitType_2.Size = new System.Drawing.Size(300, 27);
            this.chkPermitType_2.TabIndex = 21;
            this.chkPermitType_2.Text = "Disinterment (Funeral Directors Only)";
            this.ToolTip1.SetToolTip(this.chkPermitType_2, null);
            this.chkPermitType_2.CheckedChanged += new System.EventHandler(this.chkPermitType_CheckedChanged);
            // 
            // chkPermitType_1
            // 
            this.chkPermitType_1.Location = new System.Drawing.Point(153, 30);
            this.chkPermitType_1.Name = "chkPermitType_1";
            this.chkPermitType_1.Size = new System.Drawing.Size(169, 27);
            this.chkPermitType_1.TabIndex = 19;
            this.chkPermitType_1.Text = "Temporary Storage";
            this.ToolTip1.SetToolTip(this.chkPermitType_1, null);
            this.chkPermitType_1.CheckedChanged += new System.EventHandler(this.chkPermitType_CheckedChanged);
            // 
            // chkPermitType_0
            // 
            this.chkPermitType_0.Location = new System.Drawing.Point(20, 30);
            this.chkPermitType_0.Name = "chkPermitType_0";
            this.chkPermitType_0.Size = new System.Drawing.Size(69, 27);
            this.chkPermitType_0.TabIndex = 18;
            this.chkPermitType_0.Text = "Burial";
            this.ToolTip1.SetToolTip(this.chkPermitType_0, null);
            this.chkPermitType_0.CheckedChanged += new System.EventHandler(this.chkPermitType_CheckedChanged);
            // 
            // chkBurial
            // 
            this.chkBurial.Location = new System.Drawing.Point(640, 67);
            this.chkBurial.Name = "chkBurial";
            this.chkBurial.Size = new System.Drawing.Size(69, 27);
            this.chkBurial.TabIndex = 23;
            this.chkBurial.Text = "Burial";
            this.ToolTip1.SetToolTip(this.chkBurial, null);
            this.chkBurial.Visible = false;
            // 
            // chkCremation
            // 
            this.chkCremation.Location = new System.Drawing.Point(522, 67);
            this.chkCremation.Name = "chkCremation";
            this.chkCremation.Size = new System.Drawing.Size(103, 27);
            this.chkCremation.TabIndex = 22;
            this.chkCremation.Text = "Cremation";
            this.ToolTip1.SetToolTip(this.chkCremation, null);
            this.chkCremation.Visible = false;
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.AutoSize = false;
            this.txtMiddleName.LinkItem = null;
            this.txtMiddleName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMiddleName.LinkTopic = null;
            this.txtMiddleName.Location = new System.Drawing.Point(179, 180);
            this.txtMiddleName.MaxLength = 25;
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(193, 40);
            this.txtMiddleName.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtMiddleName, null);
            // 
            // txtFirstName
            // 
            this.txtFirstName.AutoSize = false;
            this.txtFirstName.LinkItem = null;
            this.txtFirstName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFirstName.LinkTopic = null;
            this.txtFirstName.Location = new System.Drawing.Point(179, 80);
            this.txtFirstName.MaxLength = 25;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(193, 40);
            this.txtFirstName.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtFirstName, null);
            // 
            // txtLastName
            // 
            this.txtLastName.AutoSize = false;
            this.txtLastName.LinkItem = null;
            this.txtLastName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLastName.LinkTopic = null;
            this.txtLastName.Location = new System.Drawing.Point(179, 130);
            this.txtLastName.MaxLength = 25;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(193, 40);
            this.txtLastName.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtLastName, null);
            // 
            // txtAge
            // 
            this.txtAge.AutoSize = false;
            this.txtAge.LinkItem = null;
            this.txtAge.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAge.LinkTopic = null;
            this.txtAge.Location = new System.Drawing.Point(741, 80);
            this.txtAge.MaxLength = 3;
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(71, 40);
            this.txtAge.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtAge, null);
            //this.txtAge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAge_KeyPressEvent);
            // 
            // txtCityOrTown
            // 
            this.txtCityOrTown.AutoSize = false;
            this.txtCityOrTown.LinkItem = null;
            this.txtCityOrTown.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCityOrTown.LinkTopic = null;
            this.txtCityOrTown.Location = new System.Drawing.Point(369, 798);
            this.txtCityOrTown.MaxLength = 34;
            this.txtCityOrTown.Name = "txtCityOrTown";
            this.txtCityOrTown.Size = new System.Drawing.Size(319, 40);
            this.txtCityOrTown.TabIndex = 32;
            this.ToolTip1.SetToolTip(this.txtCityOrTown, null);
            this.txtCityOrTown.DoubleClick += new System.EventHandler(this.txtCityOrTown_DblClick);
            // 
            // txtPlaceOfDeathState
            // 
            this.txtPlaceOfDeathState.AutoSize = false;
            this.txtPlaceOfDeathState.LinkItem = null;
            this.txtPlaceOfDeathState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPlaceOfDeathState.LinkTopic = null;
            this.txtPlaceOfDeathState.Location = new System.Drawing.Point(570, 230);
            this.txtPlaceOfDeathState.MaxLength = 20;
            this.txtPlaceOfDeathState.Name = "txtPlaceOfDeathState";
            this.txtPlaceOfDeathState.Size = new System.Drawing.Size(242, 40);
            this.txtPlaceOfDeathState.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtPlaceOfDeathState, null);
            this.txtPlaceOfDeathState.DoubleClick += new System.EventHandler(this.txtPlaceOfDeathState_DblClick);
            // 
            // txtNameOfFuneralEstablishment
            // 
            this.txtNameOfFuneralEstablishment.AutoSize = false;
            this.txtNameOfFuneralEstablishment.LinkItem = null;
            this.txtNameOfFuneralEstablishment.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtNameOfFuneralEstablishment.LinkTopic = null;
            this.txtNameOfFuneralEstablishment.Location = new System.Drawing.Point(214, 317);
            this.txtNameOfFuneralEstablishment.Name = "txtNameOfFuneralEstablishment";
            this.txtNameOfFuneralEstablishment.Size = new System.Drawing.Size(290, 40);
            this.txtNameOfFuneralEstablishment.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtNameOfFuneralEstablishment, null);
            // 
            // txtRace
            // 
            this.txtRace.AutoSize = false;
            this.txtRace.LinkItem = null;
            this.txtRace.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtRace.LinkTopic = null;
            this.txtRace.Location = new System.Drawing.Point(518, 130);
            this.txtRace.MaxLength = 25;
            this.txtRace.Name = "txtRace";
            this.txtRace.Size = new System.Drawing.Size(143, 40);
            this.txtRace.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtRace, null);
            // 
            // txtPlaceOfDisposition
            // 
            this.txtPlaceOfDisposition.AutoSize = false;
            this.txtPlaceOfDisposition.LinkItem = null;
            this.txtPlaceOfDisposition.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPlaceOfDisposition.LinkTopic = null;
            this.txtPlaceOfDisposition.Location = new System.Drawing.Point(30, 798);
            this.txtPlaceOfDisposition.MaxLength = 34;
            this.txtPlaceOfDisposition.Name = "txtPlaceOfDisposition";
            this.txtPlaceOfDisposition.Size = new System.Drawing.Size(319, 40);
            this.txtPlaceOfDisposition.TabIndex = 31;
            this.ToolTip1.SetToolTip(this.txtPlaceOfDisposition, null);
            // 
            // txtDesignation
            // 
            this.txtDesignation.AutoSize = false;
            this.txtDesignation.LinkItem = null;
            this.txtDesignation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDesignation.LinkTopic = null;
            this.txtDesignation.Location = new System.Drawing.Point(179, 230);
            this.txtDesignation.MaxLength = 5;
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.Size = new System.Drawing.Size(70, 40);
            this.txtDesignation.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtDesignation, null);
            // 
            // txtPlaceOfDeathCity
            // 
            this.txtPlaceOfDeathCity.AutoSize = false;
            this.txtPlaceOfDeathCity.LinkItem = null;
            this.txtPlaceOfDeathCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPlaceOfDeathCity.LinkTopic = null;
            this.txtPlaceOfDeathCity.Location = new System.Drawing.Point(570, 180);
            this.txtPlaceOfDeathCity.MaxLength = 20;
            this.txtPlaceOfDeathCity.Name = "txtPlaceOfDeathCity";
            this.txtPlaceOfDeathCity.Size = new System.Drawing.Size(242, 40);
            this.txtPlaceOfDeathCity.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtPlaceOfDeathCity, null);
            this.txtPlaceOfDeathCity.DoubleClick += new System.EventHandler(this.txtPlaceOfDeathCity_DblClick);
            // 
            // txtLicenseNumber
            // 
            this.txtLicenseNumber.AutoSize = false;
            this.txtLicenseNumber.LinkItem = null;
            this.txtLicenseNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLicenseNumber.LinkTopic = null;
            this.txtLicenseNumber.Location = new System.Drawing.Point(605, 367);
            this.txtLicenseNumber.MaxLength = 20;
            this.txtLicenseNumber.Name = "txtLicenseNumber";
            this.txtLicenseNumber.Size = new System.Drawing.Size(210, 40);
            this.txtLicenseNumber.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.txtLicenseNumber, null);
            // 
            // txtAddress1
            // 
            this.txtAddress1.AutoSize = false;
            this.txtAddress1.LinkItem = null;
            this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress1.LinkTopic = null;
            this.txtAddress1.Location = new System.Drawing.Point(214, 367);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(290, 40);
            this.txtAddress1.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtAddress1, null);
            // 
            // txtSex
            // 
            this.txtSex.AutoSize = false;
            this.txtSex.LinkItem = null;
            this.txtSex.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSex.LinkTopic = null;
            this.txtSex.Location = new System.Drawing.Point(322, 230);
            this.txtSex.MaxLength = 1;
            this.txtSex.Name = "txtSex";
            this.txtSex.Size = new System.Drawing.Size(50, 40);
            this.txtSex.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtSex, null);
            this.txtSex.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSex_KeyPressEvent);
            // 
            // txtAddress2
            // 
            this.txtAddress2.AutoSize = false;
            this.txtAddress2.LinkItem = null;
            this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress2.LinkTopic = null;
            this.txtAddress2.Location = new System.Drawing.Point(214, 417);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(290, 40);
            this.txtAddress2.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtAddress2, null);
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(214, 467);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(229, 40);
            this.txtCity.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.txtCity, null);
            // 
            // txtState
            // 
            this.txtState.AutoSize = false;
            this.txtState.LinkItem = null;
            this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(453, 467);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(229, 40);
            this.txtState.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.txtState, null);
            // 
            // txtZip
            // 
            this.txtZip.AutoSize = false;
            this.txtZip.LinkItem = null;
            this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip.LinkTopic = null;
            this.txtZip.Location = new System.Drawing.Point(693, 467);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(122, 40);
            this.txtZip.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.txtZip, null);
            // 
            // gridTownCode
            // 
            this.gridTownCode.AllowSelection = false;
            this.gridTownCode.AllowUserToResizeColumns = false;
            this.gridTownCode.AllowUserToResizeRows = false;
            this.gridTownCode.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridTownCode.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridTownCode.BackColorBkg = System.Drawing.Color.Empty;
            this.gridTownCode.BackColorFixed = System.Drawing.Color.Empty;
            this.gridTownCode.BackColorSel = System.Drawing.Color.Empty;
            this.gridTownCode.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridTownCode.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridTownCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridTownCode.ColumnHeadersHeight = 30;
            this.gridTownCode.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridTownCode.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridTownCode.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridTownCode.DragIcon = null;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridTownCode.FrozenCols = 0;
            this.gridTownCode.GridColor = System.Drawing.Color.Empty;
            this.gridTownCode.GridColorFixed = System.Drawing.Color.Empty;
            this.gridTownCode.Location = new System.Drawing.Point(30, 30);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.OutlineCol = 0;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridTownCode.RowHeightMin = 0;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.ScrollTipText = null;
            this.gridTownCode.ShowColumnVisibilityMenu = false;
            this.gridTownCode.Size = new System.Drawing.Size(193, 41);
            this.gridTownCode.StandardTab = true;
            this.gridTownCode.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridTownCode.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridTownCode.TabIndex = 72;
            this.gridTownCode.Tag = "land";
            this.ToolTip1.SetToolTip(this.gridTownCode, null);
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Location = new System.Drawing.Point(30, 898);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.SSTab1.ShowFocusRect = false;
            this.SSTab1.Size = new System.Drawing.Size(809, 358);
            this.SSTab1.TabIndex = 74;
            this.SSTab1.TabsPerRow = 0;
            this.SSTab1.TabStop = false;
            this.SSTab1.Text = "Disposition Information";
            this.ToolTip1.SetToolTip(this.SSTab1, null);
            this.SSTab1.WordWrap = false;
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.Frame3);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Text = "Disposition Information";
            this.ToolTip1.SetToolTip(this.SSTab1_Page1, null);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.chkDisposition_6);
            this.Frame3.Controls.Add(this.chkDisposition_5);
            this.Frame3.Controls.Add(this.chkDisposition_4);
            this.Frame3.Controls.Add(this.chkDisposition_3);
            this.Frame3.Controls.Add(this.chkDisposition_2);
            this.Frame3.Controls.Add(this.chkDisposition_1);
            this.Frame3.Controls.Add(this.chkDisposition_0);
            this.Frame3.Controls.Add(this.DispositionDate);
            this.Frame3.Controls.Add(this.txtNameOfCemetery);
            this.Frame3.Controls.Add(this.txtLocation);
            this.Frame3.Controls.Add(this.txtField3);
            this.Frame3.Controls.Add(this.txtField5);
            this.Frame3.Controls.Add(this.lblField4);
            this.Frame3.Controls.Add(this.lblField1);
            this.Frame3.Controls.Add(this.lblField3);
            this.Frame3.Controls.Add(this.lblfraDate);
            this.Frame3.Controls.Add(this.lblField2);
            this.Frame3.Location = new System.Drawing.Point(20, 20);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(767, 275);
            this.Frame3.TabIndex = 79;
            this.Frame3.Text = "Disposition Information";
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // chkDisposition_6
            // 
            this.chkDisposition_6.Location = new System.Drawing.Point(20, 192);
            this.chkDisposition_6.Name = "chkDisposition_6";
            this.chkDisposition_6.Size = new System.Drawing.Size(169, 27);
            this.chkDisposition_6.TabIndex = 43;
            this.chkDisposition_6.Text = "Removal from state";
            this.ToolTip1.SetToolTip(this.chkDisposition_6, null);
            this.chkDisposition_6.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_5
            // 
            this.chkDisposition_5.Location = new System.Drawing.Point(20, 165);
            this.chkDisposition_5.Name = "chkDisposition_5";
            this.chkDisposition_5.Size = new System.Drawing.Size(312, 27);
            this.chkDisposition_5.TabIndex = 42;
            this.chkDisposition_5.Text = "Body was removed to a medical school";
            this.ToolTip1.SetToolTip(this.chkDisposition_5, null);
            this.chkDisposition_5.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_4
            // 
            this.chkDisposition_4.Location = new System.Drawing.Point(20, 138);
            this.chkDisposition_4.Name = "chkDisposition_4";
            this.chkDisposition_4.Size = new System.Drawing.Size(197, 27);
            this.chkDisposition_4.TabIndex = 41;
            this.chkDisposition_4.Text = "Body was buried at sea";
            this.ToolTip1.SetToolTip(this.chkDisposition_4, null);
            this.chkDisposition_4.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_3
            // 
            this.chkDisposition_3.Location = new System.Drawing.Point(20, 111);
            this.chkDisposition_3.Name = "chkDisposition_3";
            this.chkDisposition_3.Size = new System.Drawing.Size(171, 27);
            this.chkDisposition_3.TabIndex = 40;
            this.chkDisposition_3.Text = "Body was cremated";
            this.ToolTip1.SetToolTip(this.chkDisposition_3, null);
            this.chkDisposition_3.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_2
            // 
            this.chkDisposition_2.Location = new System.Drawing.Point(20, 87);
            this.chkDisposition_2.Name = "chkDisposition_2";
            this.chkDisposition_2.Size = new System.Drawing.Size(148, 27);
            this.chkDisposition_2.TabIndex = 39;
            this.chkDisposition_2.Text = "Body was buried";
            this.ToolTip1.SetToolTip(this.chkDisposition_2, null);
            this.chkDisposition_2.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_1
            // 
            this.chkDisposition_1.Location = new System.Drawing.Point(20, 57);
            this.chkDisposition_1.Name = "chkDisposition_1";
            this.chkDisposition_1.Size = new System.Drawing.Size(224, 27);
            this.chkDisposition_1.TabIndex = 38;
            this.chkDisposition_1.Text = "Body placed in mausoleum";
            this.ToolTip1.SetToolTip(this.chkDisposition_1, null);
            this.chkDisposition_1.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_0
            // 
            this.chkDisposition_0.Location = new System.Drawing.Point(20, 30);
            this.chkDisposition_0.Name = "chkDisposition_0";
            this.chkDisposition_0.Size = new System.Drawing.Size(180, 27);
            this.chkDisposition_0.TabIndex = 37;
            this.chkDisposition_0.Text = "Body was disinterred";
            this.ToolTip1.SetToolTip(this.chkDisposition_0, null);
            this.chkDisposition_0.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // DispositionDate
            // 
            this.DispositionDate.AutoSize = false;
            this.DispositionDate.BackColor = System.Drawing.SystemColors.Window;
            this.DispositionDate.LinkItem = null;
            this.DispositionDate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.DispositionDate.LinkTopic = null;
            this.DispositionDate.Location = new System.Drawing.Point(343, 65);
            this.DispositionDate.Name = "DispositionDate";
            this.DispositionDate.Size = new System.Drawing.Size(113, 40);
            this.DispositionDate.TabIndex = 44;
            this.ToolTip1.SetToolTip(this.DispositionDate, null);
            this.DispositionDate.Validating += new System.ComponentModel.CancelEventHandler(this.DispositionDate_Validating);
            // 
            // txtNameOfCemetery
            // 
            this.txtNameOfCemetery.AutoSize = false;
            this.txtNameOfCemetery.LinkItem = null;
            this.txtNameOfCemetery.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtNameOfCemetery.LinkTopic = null;
            this.txtNameOfCemetery.Location = new System.Drawing.Point(476, 65);
            this.txtNameOfCemetery.Name = "txtNameOfCemetery";
            this.txtNameOfCemetery.Size = new System.Drawing.Size(271, 40);
            this.txtNameOfCemetery.TabIndex = 45;
            this.ToolTip1.SetToolTip(this.txtNameOfCemetery, null);
            // 
            // txtLocation
            // 
            this.txtLocation.AutoSize = false;
            this.txtLocation.LinkItem = null;
            this.txtLocation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLocation.LinkTopic = null;
            this.txtLocation.Location = new System.Drawing.Point(343, 140);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(192, 40);
            this.txtLocation.TabIndex = 46;
            this.ToolTip1.SetToolTip(this.txtLocation, null);
            this.txtLocation.DoubleClick += new System.EventHandler(this.txtLocation_DblClick);
            // 
            // txtField3
            // 
            this.txtField3.AutoSize = false;
            this.txtField3.LinkItem = null;
            this.txtField3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtField3.LinkTopic = null;
            this.txtField3.Location = new System.Drawing.Point(555, 140);
            this.txtField3.Name = "txtField3";
            this.txtField3.Size = new System.Drawing.Size(192, 40);
            this.txtField3.TabIndex = 47;
            this.ToolTip1.SetToolTip(this.txtField3, null);
            // 
            // txtField5
            // 
            this.txtField5.AutoSize = false;
            this.txtField5.LinkItem = null;
            this.txtField5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtField5.LinkTopic = null;
            this.txtField5.Location = new System.Drawing.Point(343, 215);
            this.txtField5.Name = "txtField5";
            this.txtField5.Size = new System.Drawing.Size(405, 40);
            this.txtField5.TabIndex = 48;
            this.ToolTip1.SetToolTip(this.txtField5, null);
            // 
            // lblField4
            // 
            this.lblField4.Location = new System.Drawing.Point(343, 190);
            this.lblField4.Name = "lblField4";
            this.lblField4.Size = new System.Drawing.Size(405, 15);
            this.lblField4.TabIndex = 90;
            this.lblField4.Text = "SIGNATURE OF PERSON IN CHARGE, MUNICIPAL OFFICIAL OR OTHER";
            this.ToolTip1.SetToolTip(this.lblField4, null);
            // 
            // lblField1
            // 
            this.lblField1.Location = new System.Drawing.Point(477, 30);
            this.lblField1.Name = "lblField1";
            this.lblField1.Size = new System.Drawing.Size(271, 25);
            this.lblField1.TabIndex = 89;
            this.lblField1.Text = "NAME OF CEMETERY, RECIPIENT OR OTHER DESTINATION";
            this.ToolTip1.SetToolTip(this.lblField1, null);
            // 
            // lblField3
            // 
            this.lblField3.Location = new System.Drawing.Point(548, 115);
            this.lblField3.Name = "lblField3";
            this.lblField3.Size = new System.Drawing.Size(200, 15);
            this.lblField3.TabIndex = 88;
            this.lblField3.Text = "LOCATION (STATE)";
            this.ToolTip1.SetToolTip(this.lblField3, null);
            // 
            // lblfraDate
            // 
            this.lblfraDate.Location = new System.Drawing.Point(343, 30);
            this.lblfraDate.Name = "lblfraDate";
            this.lblfraDate.Size = new System.Drawing.Size(87, 15);
            this.lblfraDate.TabIndex = 87;
            this.lblfraDate.Text = "DATE";
            this.ToolTip1.SetToolTip(this.lblfraDate, null);
            // 
            // lblField2
            // 
            this.lblField2.Location = new System.Drawing.Point(343, 115);
            this.lblField2.Name = "lblField2";
            this.lblField2.Size = new System.Drawing.Size(181, 15);
            this.lblField2.TabIndex = 83;
            this.lblField2.Text = "LOCATION";
            this.ToolTip1.SetToolTip(this.lblField2, null);
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.Frame4);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Text = "Post Cremation Disposition";
            this.ToolTip1.SetToolTip(this.SSTab1_Page2, null);
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.CremDate);
            this.Frame4.Controls.Add(this.cmbCremains);
            this.Frame4.Controls.Add(this.CremLoc);
            this.Frame4.Controls.Add(this.CremName);
            this.Frame4.Controls.Add(this.Label2);
            this.Frame4.Controls.Add(this.Label3);
            this.Frame4.Controls.Add(this.Label4);
            this.Frame4.Location = new System.Drawing.Point(20, 20);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(662, 190);
            this.Frame4.TabIndex = 75;
            this.Frame4.Text = "Disposition ";
            this.ToolTip1.SetToolTip(this.Frame4, null);
            // 
            // CremDate
            // 
            this.CremDate.AutoSize = false;
            this.CremDate.BackColor = System.Drawing.SystemColors.Window;
            this.CremDate.LinkItem = null;
            this.CremDate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.CremDate.LinkTopic = null;
            this.CremDate.Location = new System.Drawing.Point(210, 55);
            this.CremDate.Name = "CremDate";
            this.CremDate.Size = new System.Drawing.Size(115, 40);
            this.CremDate.TabIndex = 84;
            this.ToolTip1.SetToolTip(this.CremDate, null);
            this.CremDate.Validating += new System.ComponentModel.CancelEventHandler(this.CremDate_Validating);
            // 
            // CremLoc
            // 
            this.CremLoc.AutoSize = false;
            this.CremLoc.LinkItem = null;
            this.CremLoc.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.CremLoc.LinkTopic = null;
            this.CremLoc.Location = new System.Drawing.Point(210, 130);
            this.CremLoc.Name = "CremLoc";
            this.CremLoc.Size = new System.Drawing.Size(432, 40);
            this.CremLoc.TabIndex = 86;
            this.ToolTip1.SetToolTip(this.CremLoc, null);
            // 
            // CremName
            // 
            this.CremName.AutoSize = false;
            this.CremName.LinkItem = null;
            this.CremName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.CremName.LinkTopic = null;
            this.CremName.Location = new System.Drawing.Point(343, 55);
            this.CremName.Name = "CremName";
            this.CremName.Size = new System.Drawing.Size(299, 40);
            this.CremName.TabIndex = 85;
            this.ToolTip1.SetToolTip(this.CremName, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(210, 105);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(68, 15);
            this.Label2.TabIndex = 78;
            this.Label2.Text = "LOCATION";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(343, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(299, 15);
            this.Label3.TabIndex = 77;
            this.Label3.Text = "NAME OF CEMETERY OR VAULT";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(210, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(87, 15);
            this.Label4.TabIndex = 76;
            this.Label4.Text = "DATE";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.Label5);
            this.SSTab1_Page3.Controls.Add(this.Label6);
            this.SSTab1_Page3.Controls.Add(this.Label8);
            this.SSTab1_Page3.Controls.Add(this.Label9);
            this.SSTab1_Page3.Controls.Add(this.T2KTemporarySignature);
            this.SSTab1_Page3.Controls.Add(this.T2KTemporaryLocation);
            this.SSTab1_Page3.Controls.Add(this.T2KTemporaryCemetery);
            this.SSTab1_Page3.Controls.Add(this.txtTemporaryDate);
            this.SSTab1_Page3.Controls.Add(this.chkTemporaryStorage);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Text = "Temporary Storage";
            this.ToolTip1.SetToolTip(this.SSTab1_Page3, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 208);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(362, 15);
            this.Label5.TabIndex = 96;
            this.Label5.Text = "SIGNATURE OF PERSON IN CHARGE OR MUNICIPAL OFFICIAL";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 57);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(314, 15);
            this.Label6.TabIndex = 97;
            this.Label6.Text = "NAME OF CEMETERY OR VAULT";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(402, 57);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(87, 16);
            this.Label8.TabIndex = 98;
            this.Label8.Text = "DATE";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 133);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(314, 15);
            this.Label9.TabIndex = 99;
            this.Label9.Text = "LOCATION";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // T2KTemporarySignature
            // 
            this.T2KTemporarySignature.AutoSize = false;
            this.T2KTemporarySignature.LinkItem = null;
            this.T2KTemporarySignature.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.T2KTemporarySignature.LinkTopic = null;
            this.T2KTemporarySignature.Location = new System.Drawing.Point(20, 233);
            this.T2KTemporarySignature.Name = "T2KTemporarySignature";
            this.T2KTemporarySignature.Size = new System.Drawing.Size(362, 40);
            this.T2KTemporarySignature.TabIndex = 95;
            this.ToolTip1.SetToolTip(this.T2KTemporarySignature, null);
            // 
            // T2KTemporaryLocation
            // 
            this.T2KTemporaryLocation.AutoSize = false;
            this.T2KTemporaryLocation.LinkItem = null;
            this.T2KTemporaryLocation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.T2KTemporaryLocation.LinkTopic = null;
            this.T2KTemporaryLocation.Location = new System.Drawing.Point(20, 158);
            this.T2KTemporaryLocation.Name = "T2KTemporaryLocation";
            this.T2KTemporaryLocation.Size = new System.Drawing.Size(362, 40);
            this.T2KTemporaryLocation.TabIndex = 94;
            this.ToolTip1.SetToolTip(this.T2KTemporaryLocation, null);
            // 
            // T2KTemporaryCemetery
            // 
            this.T2KTemporaryCemetery.AutoSize = false;
            this.T2KTemporaryCemetery.LinkItem = null;
            this.T2KTemporaryCemetery.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.T2KTemporaryCemetery.LinkTopic = null;
            this.T2KTemporaryCemetery.Location = new System.Drawing.Point(20, 83);
            this.T2KTemporaryCemetery.Name = "T2KTemporaryCemetery";
            this.T2KTemporaryCemetery.Size = new System.Drawing.Size(362, 40);
            this.T2KTemporaryCemetery.TabIndex = 92;
            this.ToolTip1.SetToolTip(this.T2KTemporaryCemetery, null);
            // 
            // txtTemporaryDate
            // 
            this.txtTemporaryDate.AutoSize = false;
            this.txtTemporaryDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtTemporaryDate.LinkItem = null;
            this.txtTemporaryDate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTemporaryDate.LinkTopic = null;
            this.txtTemporaryDate.Location = new System.Drawing.Point(402, 83);
            this.txtTemporaryDate.Name = "txtTemporaryDate";
            this.txtTemporaryDate.Size = new System.Drawing.Size(115, 40);
            this.txtTemporaryDate.TabIndex = 93;
            this.ToolTip1.SetToolTip(this.txtTemporaryDate, null);
            this.txtTemporaryDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtTemporaryDate_Validating);
            // 
            // chkTemporaryStorage
            // 
            this.chkTemporaryStorage.Location = new System.Drawing.Point(20, 20);
            this.chkTemporaryStorage.Name = "chkTemporaryStorage";
            this.chkTemporaryStorage.Size = new System.Drawing.Size(340, 27);
            this.chkTemporaryStorage.TabIndex = 91;
            this.chkTemporaryStorage.Text = "Remains were placed in temporary storage";
            this.ToolTip1.SetToolTip(this.chkTemporaryStorage, null);
            // 
            // imgDocuments
            // 
            this.imgDocuments.AllowDrop = true;
            this.imgDocuments.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDocuments.DrawStyle = ((short)(0));
            this.imgDocuments.DrawWidth = ((short)(1));
            this.imgDocuments.FillStyle = ((short)(1));
            this.imgDocuments.FontTransparent = true;
            this.imgDocuments.Image = ((System.Drawing.Image)(resources.GetObject("imgDocuments.Image")));
            this.imgDocuments.Location = new System.Drawing.Point(33, 80);
            this.imgDocuments.Name = "imgDocuments";
            this.imgDocuments.Picture = ((System.Drawing.Image)(resources.GetObject("imgDocuments.Picture")));
            this.imgDocuments.Size = new System.Drawing.Size(29, 29);
            this.imgDocuments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgDocuments.TabIndex = 101;
            this.ToolTip1.SetToolTip(this.imgDocuments, "One or more documents are attached to this record");
            this.imgDocuments.Click += new System.EventHandler(this.imgDocuments_Click);
            // 
            // Label1_21
            // 
            this.Label1_21.Location = new System.Drawing.Point(317, 864);
            this.Label1_21.Name = "Label1_21";
            this.Label1_21.Size = new System.Drawing.Size(75, 14);
            this.Label1_21.TabIndex = 73;
            this.Label1_21.Text = "CITY/TOWN";
            this.ToolTip1.SetToolTip(this.Label1_21, null);
            // 
            // Label1_20
            // 
            this.Label1_20.Location = new System.Drawing.Point(30, 481);
            this.Label1_20.Name = "Label1_20";
            this.Label1_20.Size = new System.Drawing.Size(138, 16);
            this.Label1_20.TabIndex = 71;
            this.Label1_20.Text = "CITY / STATE / ZIP";
            this.ToolTip1.SetToolTip(this.Label1_20, null);
            // 
            // Label1_19
            // 
            this.Label1_19.Location = new System.Drawing.Point(30, 431);
            this.Label1_19.Name = "Label1_19";
            this.Label1_19.Size = new System.Drawing.Size(138, 16);
            this.Label1_19.TabIndex = 70;
            this.Label1_19.Text = "ADDRESS 2";
            this.ToolTip1.SetToolTip(this.Label1_19, null);
            // 
            // Label1_18
            // 
            this.Label1_18.Location = new System.Drawing.Point(599, 862);
            this.Label1_18.Name = "Label1_18";
            this.Label1_18.Size = new System.Drawing.Size(78, 16);
            this.Label1_18.TabIndex = 69;
            this.Label1_18.Text = "DATE SIGNED";
            this.ToolTip1.SetToolTip(this.Label1_18, null);
            // 
            // Label1_17
            // 
            this.Label1_17.Location = new System.Drawing.Point(369, 773);
            this.Label1_17.Name = "Label1_17";
            this.Label1_17.Size = new System.Drawing.Size(100, 15);
            this.Label1_17.TabIndex = 68;
            this.Label1_17.Text = "CITY OR TOWN";
            this.ToolTip1.SetToolTip(this.Label1_17, null);
            // 
            // Label1_16
            // 
            this.Label1_16.Location = new System.Drawing.Point(30, 864);
            this.Label1_16.Name = "Label1_16";
            this.Label1_16.Size = new System.Drawing.Size(39, 16);
            this.Label1_16.TabIndex = 67;
            this.Label1_16.Text = "CLERK";
            this.ToolTip1.SetToolTip(this.Label1_16, null);
            // 
            // Label1_15
            // 
            this.Label1_15.Location = new System.Drawing.Point(30, 773);
            this.Label1_15.Name = "Label1_15";
            this.Label1_15.Size = new System.Drawing.Size(150, 15);
            this.Label1_15.TabIndex = 66;
            this.Label1_15.Text = "PLACE OF DISPOSITION";
            this.ToolTip1.SetToolTip(this.Label1_15, null);
            // 
            // Label1_14
            // 
            this.Label1_14.Location = new System.Drawing.Point(708, 773);
            this.Label1_14.Name = "Label1_14";
            this.Label1_14.Size = new System.Drawing.Size(131, 15);
            this.Label1_14.TabIndex = 65;
            this.Label1_14.Text = "DATE OF DISPOSITION";
            this.ToolTip1.SetToolTip(this.Label1_14, null);
            // 
            // Label1_13
            // 
            this.Label1_13.Location = new System.Drawing.Point(605, 341);
            this.Label1_13.Name = "Label1_13";
            this.Label1_13.Size = new System.Drawing.Size(160, 13);
            this.Label1_13.TabIndex = 64;
            this.Label1_13.Text = "(FUNERAL ESTABLISHMENT)";
            this.Label1_13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label1_13, null);
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(683, 94);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(32, 16);
            this.Label1_12.TabIndex = 63;
            this.Label1_12.Text = "AGE";
            this.ToolTip1.SetToolTip(this.Label1_12, null);
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(269, 244);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(38, 16);
            this.Label1_11.TabIndex = 62;
            this.Label1_11.Text = "SEX";
            this.ToolTip1.SetToolTip(this.Label1_11, null);
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(393, 94);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(92, 16);
            this.Label1_10.TabIndex = 61;
            this.Label1_10.Text = "DATE OF DEATH";
            this.ToolTip1.SetToolTip(this.Label1_10, null);
            // 
            // Label1_9
            // 
            this.Label1_9.Location = new System.Drawing.Point(30, 331);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(160, 16);
            this.Label1_9.TabIndex = 60;
            this.Label1_9.Text = "FUNERAL ESTABLISHMENT";
            this.ToolTip1.SetToolTip(this.Label1_9, null);
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(30, 381);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(138, 16);
            this.Label1_8.TabIndex = 59;
            this.Label1_8.Text = "ADDRESS 1";
            this.ToolTip1.SetToolTip(this.Label1_8, null);
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(605, 315);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(110, 16);
            this.Label1_7.TabIndex = 58;
            this.Label1_7.Text = "LICENSE NUMBER";
            this.ToolTip1.SetToolTip(this.Label1_7, null);
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(393, 244);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(147, 16);
            this.Label1_6.TabIndex = 57;
            this.Label1_6.Text = "PLACE OF DEATH (STATE)";
            this.ToolTip1.SetToolTip(this.Label1_6, null);
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(393, 194);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(147, 16);
            this.Label1_5.TabIndex = 56;
            this.Label1_5.Text = "PLACE OF DEATH (CITY)";
            this.ToolTip1.SetToolTip(this.Label1_5, null);
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(393, 144);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(92, 16);
            this.Label1_4.TabIndex = 55;
            this.Label1_4.Text = "RACE";
            this.ToolTip1.SetToolTip(this.Label1_4, null);
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(77, 244);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(84, 16);
            this.Label1_3.TabIndex = 54;
            this.Label1_3.Text = "JR. / SR. / ETC.";
            this.ToolTip1.SetToolTip(this.Label1_3, null);
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(77, 194);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(80, 16);
            this.Label1_2.TabIndex = 53;
            this.Label1_2.Text = "MIDDLE NAME";
            this.ToolTip1.SetToolTip(this.Label1_2, null);
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(77, 94);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(70, 16);
            this.Label1_1.TabIndex = 52;
            this.Label1_1.Text = "FIRST NAME";
            this.ToolTip1.SetToolTip(this.Label1_1, null);
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(77, 144);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(70, 16);
            this.Label1_0.TabIndex = 51;
            this.Label1_0.Text = "LAST NAME";
            this.ToolTip1.SetToolTip(this.Label1_0, null);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAssociateDeathCertificate,
            this.mnuPrint,
            this.mnuViewDocs});
            this.MainMenu1.Name = null;
            // 
            // mnuAssociateDeathCertificate
            // 
            this.mnuAssociateDeathCertificate.Index = 0;
            this.mnuAssociateDeathCertificate.Name = "mnuAssociateDeathCertificate";
            this.mnuAssociateDeathCertificate.Text = "Associate Death Certificate";
            this.mnuAssociateDeathCertificate.Click += new System.EventHandler(this.mnuAssociateDeathCertificate_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuViewDocs
            // 
            this.mnuViewDocs.Index = 2;
            this.mnuViewDocs.Name = "mnuViewDocs";
            this.mnuViewDocs.Text = "View Attached Documents";
            this.mnuViewDocs.Click += new System.EventHandler(this.mnuViewDocs_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = -1;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = -1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // Separator
            // 
            this.Separator.Index = -1;
            this.Separator.Name = "Separator";
            this.Separator.Text = "-";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = -1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print / Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                  ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveQuit
            // 
            this.mnuSaveQuit.Index = -1;
            this.mnuSaveQuit.Name = "mnuSaveQuit";
            this.mnuSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveQuit.Text = "Save & Exit";
            this.mnuSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(390, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.AppearanceKey = "toolbarButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(745, 29);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Size = new System.Drawing.Size(100, 24);
            this.cmdPrintPreview.TabIndex = 1;
            this.cmdPrintPreview.Text = "Print / Preview";
            this.ToolTip1.SetToolTip(this.cmdPrintPreview, null);
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(686, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(53, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.ToolTip1.SetToolTip(this.cmdDelete, null);
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.AppearanceKey = "toolbarButton";
            this.cmdNew.Location = new System.Drawing.Point(635, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(45, 24);
            this.cmdNew.TabIndex = 3;
            this.cmdNew.Text = "New";
            this.ToolTip1.SetToolTip(this.cmdNew, null);
            this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // frmBurialPermit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(876, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmBurialPermit";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Burial Permit";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmBurialPermit_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBurialPermit_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBurialPermit_KeyPress);
            this.Resize += new System.EventHandler(this.frmBurialPermit_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameOfClerkOrSubregistrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkArmedForces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationApplication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationMedicalExaminer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuthorizationCertificate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermitType_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBurial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCremation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCityOrTown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDeathState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameOfFuneralEstablishment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDisposition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesignation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDeathCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenseNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameOfCemetery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtField3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtField5)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CremLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CremName)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            this.SSTab1_Page3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KTemporarySignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KTemporaryLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KTemporaryCemetery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTemporaryStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private System.ComponentModel.IContainer components;
		private FCButton cmdPrintPreview;
		private FCButton cmdDelete;
		private FCButton cmdNew;
	}
}