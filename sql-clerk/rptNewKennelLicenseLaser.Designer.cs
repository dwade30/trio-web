﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptNewKennelLicenseLaser.
	/// </summary>
	partial class rptNewKennelLicenseLaser
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewKennelLicenseLaser));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLicenseNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLicenseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExpYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtIssuedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssuedBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtOwnerName,
				this.txtPhone,
				this.txtAddress,
				this.txtLicenseNumber,
				this.txtLocation,
				this.txtLicenseDate,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Shape1,
				this.Field13,
				this.Field14,
				this.Line1,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label12,
				this.Field18,
				this.Field19,
				this.Field20,
				this.Field21,
				this.Label13,
				this.txtExpYear,
				this.Label14,
				this.Label15,
				this.Label16,
				this.txtIssuedBy,
				this.txtMuni2,
				this.txtTownAddress,
				this.Field23,
				this.Line3,
				this.Field24,
				this.Line4,
				this.Field25,
				this.Line5,
				this.Label17
			});
			this.PageHeader.Height = 5.052083F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.21875F;
			this.txtMuni.Left = 2.625F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.625F;
			this.txtMuni.Width = 1.5F;
			// 
			// txtOwnerName
			// 
			this.txtOwnerName.Height = 0.1875F;
			this.txtOwnerName.Left = 1.25F;
			this.txtOwnerName.Name = "txtOwnerName";
			this.txtOwnerName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtOwnerName.Text = null;
			this.txtOwnerName.Top = 1.447917F;
			this.txtOwnerName.Width = 3.125F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1875F;
			this.txtPhone.Left = 1.25F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtPhone.Text = null;
			this.txtPhone.Top = 2.104167F;
			this.txtPhone.Width = 3.125F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1875F;
			this.txtAddress.Left = 1.25F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 1.666667F;
			this.txtAddress.Width = 3.125F;
			// 
			// txtLicenseNumber
			// 
			this.txtLicenseNumber.Height = 0.21875F;
			this.txtLicenseNumber.Left = 6.125F;
			this.txtLicenseNumber.Name = "txtLicenseNumber";
			this.txtLicenseNumber.Text = null;
			this.txtLicenseNumber.Top = 0.21875F;
			this.txtLicenseNumber.Width = 1.0625F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 1.25F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1.885417F;
			this.txtLocation.Width = 3.125F;
			// 
			// txtLicenseDate
			// 
			this.txtLicenseDate.Height = 0.1875F;
			this.txtLicenseDate.Left = 1.25F;
			this.txtLicenseDate.Name = "txtLicenseDate";
			this.txtLicenseDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtLicenseDate.Text = null;
			this.txtLicenseDate.Top = 2.322917F;
			this.txtLicenseDate.Width = 1.1875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.1875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label1.Text = "Owner/Keeper";
			this.Label1.Top = 1.447917F;
			this.Label1.Width = 1F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.1875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label2.Text = "Address";
			this.Label2.Top = 1.666667F;
			this.Label2.Width = 1F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label3.Text = "Location";
			this.Label3.Top = 1.885417F;
			this.Label3.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label4.Text = "Telephone";
			this.Label4.Top = 2.104167F;
			this.Label4.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.21875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.1875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label5.Text = ", Maine";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 1F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.1875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label6.Text = "Issue Date";
			this.Label6.Top = 2.322917F;
			this.Label6.Width = 0.875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Label7.Text = "License Number";
			this.Label7.Top = 0.0625F;
			this.Label7.Width = 1.125F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.53125F;
			this.Shape1.Left = 0.125F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 2.833333F;
			this.Shape1.Width = 7.125F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 2.1875F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field13.Text = "EXPIRES:  DECEMBER 31ST,";
			this.Field13.Top = 0.90625F;
			this.Field13.Width = 2.0625F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1666667F;
			this.Field14.Left = 0.1666667F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field14.Text = "I CERTIFY THAT THE DOGS IN THIS KENNEL ARE KEPT FOR";
			this.Field14.Top = 4.333333F;
			this.Field14.Width = 3.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.5F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 4.5F;
			this.Line1.Width = 3.833333F;
			this.Line1.X1 = 3.5F;
			this.Line1.X2 = 7.333333F;
			this.Line1.Y1 = 4.5F;
			this.Line1.Y2 = 4.5F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 5.9375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label8.Text = "( 10 Dogs Per Lic.)";
			this.Label8.Top = 0.4375F;
			this.Label8.Width = 1.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.21875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2.75F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label9.Text = "MUNICIPAL KENNEL LICENSE";
			this.Label9.Top = 0.03125F;
			this.Label9.Width = 2F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.21875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.9375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label10.Text = "Issued by Municipality of";
			this.Label10.Top = 0.40625F;
			this.Label10.Width = 1.6875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.1875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label12.Text = "REQUIREMENTS FOR A KENNEL LICENSE:";
			this.Label12.Top = 2.927083F;
			this.Label12.Width = 2.5F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.1666667F;
			this.Field18.Left = 4.25F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field18.Text = "Print Name";
			this.Field18.Top = 4.833333F;
			this.Field18.Width = 1.166667F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1666667F;
			this.Field19.Left = 0.1666667F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field19.Text = "Signature Owner/Keeper";
			this.Field19.Top = 4.833333F;
			this.Field19.Width = 2F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 2.6875F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.Field20.Text = "KENNEL LICENSEE IS FOR 5 OR MORE DOGS KEPT IN A SINGLE LOCATION";
			this.Field20.Top = 2.927083F;
			this.Field20.Width = 4.625F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.21875F;
			this.Field21.Left = 0.1875F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.Field21.Text = "UNDER ONE OWNERSHIP FOR BREEDING, HUNTING, SHOW, TRAINING, FIELD TRIALS OR EXHIBI" + "TION PURPOSES";
			this.Field21.Top = 3.114583F;
			this.Field21.Width = 7.0625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.9583333F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label13.Text = "KENNELS MUST BE INSPECTED ANNUALLY BY MUNICIPAL ACO PRIOR TO LICENSING";
			this.Label13.Top = 1.15625F;
			this.Label13.Width = 5.604167F;
			// 
			// txtExpYear
			// 
			this.txtExpYear.Height = 0.1875F;
			this.txtExpYear.Left = 4.25F;
			this.txtExpYear.Name = "txtExpYear";
			this.txtExpYear.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtExpYear.Text = null;
			this.txtExpYear.Top = 0.90625F;
			this.txtExpYear.Width = 1.166667F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.1666667F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label14.Text = "Issued By:";
			this.Label14.Top = 3.416667F;
			this.Label14.Width = 0.9166667F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1666667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.1666667F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label15.Text = "Address:";
			this.Label15.Top = 3.666667F;
			this.Label15.Width = 0.9166667F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1666667F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.770833F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label16.Text = "City clerk of:";
			this.Label16.Top = 3.416667F;
			this.Label16.Width = 0.9166667F;
			// 
			// txtIssuedBy
			// 
			this.txtIssuedBy.Height = 0.1875F;
			this.txtIssuedBy.Left = 1.104167F;
			this.txtIssuedBy.Name = "txtIssuedBy";
			this.txtIssuedBy.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtIssuedBy.Text = null;
			this.txtIssuedBy.Top = 3.416667F;
			this.txtIssuedBy.Width = 2.520833F;
			// 
			// txtMuni2
			// 
			this.txtMuni2.Height = 0.1875F;
			this.txtMuni2.Left = 4.6875F;
			this.txtMuni2.Name = "txtMuni2";
			this.txtMuni2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtMuni2.Text = null;
			this.txtMuni2.Top = 3.416667F;
			this.txtMuni2.Width = 2.6875F;
			// 
			// txtTownAddress
			// 
			this.txtTownAddress.Height = 0.1875F;
			this.txtTownAddress.Left = 1.104167F;
			this.txtTownAddress.Name = "txtTownAddress";
			this.txtTownAddress.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.txtTownAddress.Text = null;
			this.txtTownAddress.Top = 3.666667F;
			this.txtTownAddress.Width = 5.9375F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1666667F;
			this.Field23.Left = 0.1666667F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field23.Text = "Signature of Town Clerk";
			this.Field23.Top = 4.083333F;
			this.Field23.Width = 2F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.1666667F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 4.833333F;
			this.Line3.Width = 7.166667F;
			this.Line3.X1 = 0.1666667F;
			this.Line3.X2 = 7.333333F;
			this.Line3.Y1 = 4.833333F;
			this.Line3.Y2 = 4.833333F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1666667F;
			this.Field24.Left = 3.770833F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field24.Text = "Print Name";
			this.Field24.Top = 4.083333F;
			this.Field24.Width = 1.166667F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.1875F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 4.083333F;
			this.Line4.Width = 7.166667F;
			this.Line4.X1 = 0.1875F;
			this.Line4.X2 = 7.354167F;
			this.Line4.Y1 = 4.083333F;
			this.Line4.Y2 = 4.083333F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1666667F;
			this.Field25.Left = 4.46875F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field25.Text = "MUNICIPAL ACO SIGNATURE";
			this.Field25.Top = 2.541667F;
			this.Field25.Width = 2F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 3.666667F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.520833F;
			this.Line5.Width = 3.666667F;
			this.Line5.X1 = 3.666667F;
			this.Line5.X2 = 7.333333F;
			this.Line5.Y1 = 2.520833F;
			this.Line5.Y2 = 2.520833F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.833333F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label17.Text = "Approved By";
			this.Label17.Top = 2.322917F;
			this.Label17.Width = 0.875F;
			// 
			// rptNewKennelLicenseLaser
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.FetchData += new FetchEventHandler(this.ActiveReport_FetchData);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLicenseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssuedBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLicenseDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExpYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssuedBy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
