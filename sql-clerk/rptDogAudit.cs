//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
//using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDogAudit.
	/// </summary>
	public partial class rptDogAudit : BaseSectionReport
	{
		public rptDogAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Transaction Audit";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDogAudit InstancePtr
		{
			get
			{
				return (rptDogAudit)Sys.GetInstance(typeof(rptDogAudit));
			}
		}

		protected rptDogAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDogAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// Date           :               05/16/2005              *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :                                       *
		// ********************************************************
		string strDateList;
		clsDRWrapper clsReport = new clsDRWrapper();
		bool boolOrderBackwards;
		// vbPorter upgrade warning: lngTotalAmount As int	OnWrite(int, double)
		int lngTotalAmount;
		// vbPorter upgrade warning: lngTotalState As int	OnWrite(int, double)
		int lngTotalState;
		// vbPorter upgrade warning: lngTotalTown As int	OnWrite(int, double)
		int lngTotalTown;
		// vbPorter upgrade warning: lngTotalClerk As int	OnWrite(int, double)
		int lngTotalClerk;
		// vbPorter upgrade warning: lngTotalLate As int	OnWrite(int, double)
		int lngTotalLate;
		// vbPorter upgrade warning: lngTotalUser As int	OnWrite(int, double)
		int lngTotalUser;
		string strStartDate;
		string strEndDate;
		bool boolIncludeOnline;
		int intOrderBy;

		public void Init(string strList, bool boolOrderDesc = false, int intOrder = 0, string strFromDate = "", string strToDate = "", bool boolShowOnline = false)
		{
			// accepts a comma delimited list
			// first number is the year which is separated by |'s from the months
			// so 2005|3|5,2004|6|7 would be month 3 and 5 for 2005 and months 6 and 7 for 2004
			intOrderBy = intOrder;
			strDateList = strList;
			boolIncludeOnline = boolShowOnline;
			boolOrderBackwards = boolOrderDesc;
			strStartDate = strFromDate;
			strEndDate = strToDate;
			if (strEndDate == string.Empty)
			{
				strEndDate = strStartDate;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "DogAudit");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			string strWhere;
			string[] strAry = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: y As int	OnWriteFCConvert.ToInt32(
			int y;
			string[] strMonthArray = null;
			string strYear = "";
			string strRange;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lngTotalAmount = 0;
				lngTotalState = 0;
				lngTotalTown = 0;
				lngTotalClerk = 0;
				lngTotalLate = 0;
				lngTotalUser = 0;
				txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
				txtMuni.Text = modGlobalConstants.Statics.MuniName;
				strWhere = "";
				strRange = "";
				if (strStartDate == string.Empty)
				{
					strAry = Strings.Split(strDateList, ",", -1, CompareConstants.vbTextCompare);
					// each year is separated, will still have to separate months
					for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
					{
						// for each year
						strWhere += " (";
						strMonthArray = Strings.Split(strAry[x], "|", -1, CompareConstants.vbTextCompare);
						strYear = strMonthArray[0];
						strWhere += " year(dogtransactions.TRANSACTIONDATE) = " + strYear + " and ";
						for (y = 1; y <= (Information.UBound(strMonthArray, 1)); y++)
						{
							// for each month
							strWhere += "(";
							strWhere += "month(dogtransactions.transactiondate) = " + FCConvert.ToString(Conversion.Val(strMonthArray[y]));
							strWhere += ") or ";
							strRange += ", " + FCConvert.ToString(Conversion.Val(strMonthArray[y])) + "/" + strYear;
						}
						// y
						strWhere = Strings.Mid(strWhere, 1, strWhere.Length - 4);
						strWhere += ") or ";
					}
					// x
					strWhere = Strings.Mid(strWhere, 1, strWhere.Length - 4);
					strRange = Strings.Mid(strRange, 3);
				}
				else
				{
					strWhere = " dogtransactions.transactiondate between '" + strStartDate + "' and '" + strEndDate + "' ";
					strRange = strStartDate + " to " + strEndDate;
				}
				if (!boolIncludeOnline)
				{
					if (fecherFoundation.Strings.Trim(strWhere) != "")
					{
						strWhere += " and online <> 1 ";
					}
					else
					{
						strWhere = " where online <> 1 ";
					}
				}
				txtRange.Text = strRange;
				strSQL = "select * from dogtransactions inner join transactiontable on (transactiontable.ID = dogtransactions.transactionnumber) where " + strWhere;
				switch (intOrderBy)
				{
					case 0:
						{
							// transaction date
							strSQL += " order by dogtransactions.transactiondate ";
							break;
						}
					case 1:
						{
							// owner
							strSQL = "select dogtransactions.*,transactiontable.* from dogowner right join (dogtransactions inner join transactiontable on (transactiontable.ID = dogtransactions.transactionnumber)) on (dogowner.ID = dogtransactions.ownernum) where " + strWhere;
							// strSQL = strSQL & " order by transactionperson,dognumber,dogtransactions.transactiondate "
							strSQL += " order by dogowner.lastname,dogowner.firstname,transactionperson,dognumber,dogtransactions.transactiondate ";
							break;
						}
					case 2:
						{
							// tag number
							strSQL += " order by tagnumber, dogyear , dogtransactions.transactiondate ";
							break;
						}
				}
				//end switch
				if (boolOrderBackwards)
				{
					strSQL += "desc";
				}
				clsReport.OpenRecordset(strSQL, "twck0000.vb1");
				if (clsReport.EndOfFile())
				{
					MessageBox.Show("No transactions in this range", "No Transactions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					return;
				}
				modDogs.GetDogFees();
				if (fecherFoundation.Strings.Trim(modDogs.Statics.DogFee.UserDefinedDescription1) != string.Empty)
				{
					lblUserFee.Text = fecherFoundation.Strings.Trim(modDogs.Statics.DogFee.UserDefinedDescription1);
					lblUser.Text = lblUserFee.Text;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strComma = "";
			string[] strAry = null;
			int x;
			string strTemp = "";
			int lngTemp = 0;
			if (!clsReport.EndOfFile())
			{
				txtTransactionDate.Text = Strings.Format(clsReport.Get_Fields("Transactiondate"), "MM/dd/yyyy");
				txtDescription.Text = clsReport.Get_Fields_String("description");
				if (!String.IsNullOrWhiteSpace(clsReport.Get_Fields_String("tagnumber")))
				{
					fldTag.Text = clsReport.Get_Fields_String("tagnumber");
				}
				else
				{
					fldTag.Text = "";
				}
				txtAmount.Text = FCConvert.ToString(clsReport.Get_Fields("amount"));
				lngTotalAmount += Conversion.Val(clsReport.Get_Fields("amount"));
				txtState.Text = FCConvert.ToString(clsReport.Get_Fields_Double("StateFee"));
				lngTotalState += FCConvert.ToInt32(clsReport.Get_Fields_Double("statefee"));
				txtTown.Text = FCConvert.ToString(clsReport.Get_Fields_Double("townfee"));
				lngTotalTown += FCConvert.ToInt32(clsReport.Get_Fields_Double("townfee"));
				txtClerk.Text = FCConvert.ToString(clsReport.Get_Fields_Double("clerkfee"));
				lngTotalClerk += FCConvert.ToInt32(clsReport.Get_Fields_Double("clerkfee"));
				txtLate.Text = FCConvert.ToString(clsReport.Get_Fields("latefee"));
				lngTotalLate += Conversion.Val(clsReport.Get_Fields("latefee"));
				txtUserFee.Text = FCConvert.ToString(clsReport.Get_Fields_Double("animalcontrol"));
				lngTotalUser += FCConvert.ToInt32(clsReport.Get_Fields_Double("animalcontrol"));
				txtUser.Text = clsReport.Get_Fields_String("username");
				txtOwnerName.Text = clsReport.Get_Fields_String("transactionperson");
				if (FCConvert.ToBoolean(clsReport.Get_Fields_Boolean("boolkennel")))
				{
					strComma = "";
					strTemp = "";
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("kenneldogs"))) != string.Empty)
					{
						strAry = Strings.Split(FCConvert.ToString(clsReport.Get_Fields_String("kenneldogs")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
						{
							clsTemp.OpenRecordset("select dogname from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[x])), "twck0000.vb1");
							if (!clsTemp.EndOfFile())
							{
								strTemp += strComma + clsTemp.Get_Fields_String("dogname");
							}
							strComma = ",";
						}
						// x
						txtDogs.Text = strTemp;
					}
					else
					{
						txtDogs.Text = "unknown";
					}
					strTemp = "";
					strComma = "";
					if (FCConvert.ToBoolean(clsReport.Get_Fields_Boolean("boolLicense")))
					{
						strTemp += strComma + "Kennel License " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("kennellic1fee")));
						strComma = ", ";
					}
					if (Conversion.Val(clsReport.Get_Fields_Double("kennelwarrantfee")) > 0)
					{
						strTemp += strComma + "Warrant Fee " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("kennelwarrantfee")));
						strComma = ", ";
					}
					if (Conversion.Val(clsReport.Get_Fields_Double("kennellatefee")) > 0)
					{
						strTemp += strComma + "Late Fee " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("kennellatefee")));
						strComma = ", ";
					}
					txtDetail.Text = strTemp;
				}
				else
				{
					clsTemp.OpenRecordset("select * from doginfo where ID = " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Int32("dognumber"))), "twck0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						txtDogs.Text = clsTemp.Get_Fields_String("dogname");
					}
					else
					{
						txtDogs.Text = "unknown";
					}
					strTemp = "";
					if (FCConvert.ToBoolean(clsReport.Get_Fields_Boolean("boollicense")))
					{
						strTemp += strComma + "License " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("spaynonspayamount")));
						strComma = ", ";
					}
					if (Conversion.Val(clsReport.Get_Fields("Warrantfee")) > 0)
					{
						strTemp += strComma + "Warrant Fee " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields("warrantfee")));
						strComma = ", ";
					}
					if (Conversion.Val(clsReport.Get_Fields("latefee")) > 0)
					{
						lngTemp = FCConvert.ToInt32(Conversion.Val(clsReport.Get_Fields("latefee")) - Conversion.Val(clsReport.Get_Fields("warrantfee")));
						if (lngTemp > 0)
						{
							strTemp += strComma + "Late Fee " + FCConvert.ToString(lngTemp);
							strComma = ", ";
						}
					}
					if (FCConvert.ToBoolean(clsReport.Get_Fields_Boolean("boolReplacementSTicker")))
					{
						strTemp += strComma + "Repl. Sticker " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields("replacementstickerfee")));
						strComma = ", ";
					}
					if (FCConvert.ToBoolean(clsReport.Get_Fields_Boolean("boolReplacementLicense")))
					{
						strTemp += strComma + "Repl. License " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("replacementlicensefee")));
						strComma = ", ";
					}
					if (FCConvert.ToBoolean(clsReport.Get_Fields_Boolean("boolReplacementTag")))
					{
						strTemp += strComma + "Repl. Tag " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields("replacementtagfee")));
						strComma = ", ";
					}
					if (FCConvert.ToBoolean(clsReport.Get_Fields_Boolean("boolTransfer")))
					{
						strTemp += strComma + "Transfer " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("transferlicensefee")));
						strComma = ", ";
					}
					if (FCConvert.ToBoolean(clsReport.Get_Fields_Boolean("boolTempLicense")))
					{
						strTemp += strComma + "Temp License " + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("templicensefee")));
						strComma = ", ";
					}
					txtDetail.Text = strTemp;
				}
				clsReport.MoveNext();
			}
			clsTemp.Dispose();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalAmount.Text = Strings.Format(lngTotalAmount, "#,###,###,##0");
			txtTotalState.Text = Strings.Format(lngTotalState, "#,###,###,##0");
			txtTotalTown.Text = Strings.Format(lngTotalTown, "#,###,###,##0");
			txtTotalClerk.Text = Strings.Format(lngTotalClerk, "#,###,###,##0");
			txtTotalLate.Text = Strings.Format(lngTotalLate, "#,###,###,##0");
			txtTotalUserFee.Text = Strings.Format(lngTotalUser, "#,###,###,##0");
		}

		
	}
}
