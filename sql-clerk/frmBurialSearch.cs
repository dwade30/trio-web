//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.Clerk.Burials;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmBurialSearch : BaseForm, IView<IBurialSearchViewModel>
    {
        private bool cancelling = true;
		public frmBurialSearch()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmBurialSearch(IBurialSearchViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			Text2 = new System.Collections.Generic.List<FCTextBox>();
			Text2.AddControlArrayElement(Text2_0, 0);
			Text2.AddControlArrayElement(Text2_1, 1);
			//Text2.AddControlArrayElement(Text2_4, 2);
			cmdSch = new System.Collections.Generic.List<FCButton>();
			cmdSch.AddControlArrayElement(cmdSch_0, 0);
			cmdSch.AddControlArrayElement(cmdSch_1, 1);
			cmdSch.AddControlArrayElement(cmdSch_2, 2);
			cmdSch.AddControlArrayElement(cmdSch_3, 3);
			cmdSch.AddControlArrayElement(cmdSch_4, 4);
			Label2 = new System.Collections.Generic.List<FCLabel>();
			Label2.AddControlArrayElement(Label2_0, 0);
			Label2.AddControlArrayElement(Label2_1, 1);
			Label2.AddControlArrayElement(Label2_4, 3);
			Label2.AddControlArrayElement(Label2_5, 4);
            this.Closing += FrmBurialSearch_Closing;
		}

        private void FrmBurialSearch_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmBurialSearch InstancePtr
		{
			get
			{
				return (frmBurialSearch)Sys.GetInstance(typeof(frmBurialSearch));
			}
		}

		protected frmBurialSearch _InstancePtr = null;
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		object temp;
		const int CNSTGRIDCOLID = 0;
		const int CNSTGRIDCOLLAST = 1;
		const int CNSTGRIDCOLFIRST = 2;
		const int CNSTGRIDCOLDATE = 3;

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			if (Grid.Row < 1)
				return;
			modGNBas.Statics.gboolSearchPrint = true;
			modClerkGeneral.Statics.utPrintInfo.tblKeyNum = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
			modClerkGeneral.Statics.utPrintInfo.TblName = "BurialPermitMaster";
			strSQL = "Select * from BurialPermitMaster where ID = " + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
			//modGNBas.Statics.rsBurialCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			// Call rptBurialPermit.Init(False, 0, utPrintInfo.tblKeyNum, True)
			rptBurialPermitWordDoc.InstancePtr.Init(false, this.Modal, 0, modClerkGeneral.Statics.utPrintInfo.tblKeyNum, true);
			// rptBurialPermit.PrintReport False
		}

		private void cmdSch_Click(int Index, object sender, System.EventArgs e)
		{
			object temp;
			switch (Index)
			{
				case 0:
					{
						rs = null;
						Close();
						break;
					}
				case 1:
					{
						SearchBth();
						break;
					}
				case 2:
					{
						break;
					}
				case 3:
					{
						clrForm();
						break;
					}
				case 4:
					{
						if (Grid.Row > 0)
						{
							modGNBas.Statics.boolBurialSearch = true;
							//frmBurialPermit.InstancePtr.Unload();
							//frmBurialPermit.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID))));
                            ViewModel.Select(Grid.TextMatrix(Grid.Row,CNSTGRIDCOLID).ToString().ToIntegerValue());
                            CloseWithoutCancel();
						}
						else
						{
							MessageBox.Show("Burial record must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}

						break;
					}
			}
			//end switch
		}

		private void cmdSch_Click(object sender, System.EventArgs e)
		{
			int index = cmdSch.GetIndex((FCButton)sender);
			cmdSch_Click(index, sender, e);
		}

		private void frmBurialSearch_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
		}

		private void frmBurialSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmBurialSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// KeyAscii = KeyAscii - 32
			}
			if (KeyAscii == Keys.Return)
				Support.SendKeys("{TAB}", false);
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmBurialSearch_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this, false);
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCMaskedTextBox)
				{
					(obj as FCMaskedTextBox).Mask = "##/##/####";
					(obj as FCMaskedTextBox).SelectionStart = 0;
				}
			}
			SetupGrid();
		}

		private void frmBurialSearch_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//if (this.WindowState != FormWindowState.Minimized)
			//{
			//	modWindowSize.SaveWindowSize(this);
			//}
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void Grid_ClickEvent(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			rs.FindFirstRecord("ID", Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
			modGNBas.Statics.Response = rs.Get_Fields_Int32("ID");
			modGNBas.Statics.gintBurialID = FCConvert.ToInt16(rs.Get_Fields_Int32("ID"));
			clrForm(false);
			if (FCConvert.ToString(rs.Get_Fields_String("LastName")) != "")
				Text2[0].Text = Text2[0].Text + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("LastName")));
			if (FCConvert.ToString(rs.Get_Fields_String("FirstName")) != "")
				Text2[0].Text = Text2[0].Text + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("FirstName")));
			if (FCConvert.ToString(rs.Get_Fields_String("MiddleName")) != "")
				Text2[0].Text = Text2[0].Text + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MiddleName")));
			Text2[0].Text = fecherFoundation.Strings.Trim(Text2[0].Text);
			Text2[1].Text = fecherFoundation.Strings.Trim(rs.Get_Fields_String("PlaceOfDisposition") + " ");
			//meBox1_0.Mask = "";
			meBox1_0.Text = Strings.Format(rs.Get_Fields("DateofDeath"), "MM/dd/yyyy");
			//meBox1_0.Mask = "##/##/####";
		}

		private void optDateofDeath_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				meBox1_0.Enabled = false;
				meBox1_1.Enabled = false;
				cmbDateofDeath.Text = "Disable";
				cmdSch[1].Focus();
			}
			else
			{
				if (cmbDateofDeath.Text == "Exact Date of Death or")
				{
					meBox1_0.Enabled = true;
					meBox1_1.Enabled = false;
				}
				else if (cmbDateofDeath.Text == "Between Dates")
				{
					meBox1_0.Enabled = true;
					meBox1_1.Enabled = true;
				}
				meBox1_0.Focus();
			}
		}

		private void optDateofDeath_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbDateofDeath.SelectedIndex;
			optDateofDeath_CheckedChanged(index, sender, e);
		}

		private void Text2_Enter(int Index, object sender, System.EventArgs e)
		{
			Text2[Index].SelectionStart = 0;
			Text2[Index].SelectionLength = Text2[Index].Text.Length;
		}

		private void Text2_Enter(object sender, System.EventArgs e)
		{
			int index = Text2.GetIndex((FCTextBox)sender);
			Text2_Enter(index, sender, e);
		}

		public void SearchBth()
		{
			string strSQL = "";
			int I;
			// vbPorter upgrade warning: tmpString As object	OnWrite(string)
			object[] tmpString = new object[2 + 1];
			for (I = 0; I <= 1; I++)
			{
				if (Text2[I].Text != "")
					tmpString[I] = modClerkGeneral.SortString2(Text2[I].Text);
			}
			// I
			if (FCConvert.ToString(tmpString[0]) != "")
				strSQL = DeceasedsName(Strings.Split(FCConvert.ToString(tmpString[0]), " ", -1, CompareConstants.vbBinaryCompare)) + " AND ";
			if (FCConvert.ToString(tmpString[1]) != "")
				strSQL += "Placeofdisposition LIKE '" + tmpString[1] + "' AND ";
			if (cmbDateofDeath.Text == "Exact Date of Death or")
			{
				strSQL += "DateofDeath = '" + meBox1_0.Text + "' AND ";
			}
			else if (cmbDateofDeath.Text == "Between Dates")
			{
				strSQL += "DateofDeath BETWEEN '" + meBox1_0.Text + "' AND '" + meBox1_1.Text + "' AND ";
			}
			if (strSQL == "")
			{
				strSQL = "SELECT top 25 * FROM BurialPermitMaster ORDER BY lastname";
			}
			else
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 5) + ";";
				strSQL = "SELECT top 25 * FROM BurialPermitMaster WHERE " + strSQL;
			}
			rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			int lngRow;
			Grid.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLID, FCConvert.ToString(rs.Get_Fields_Int32("id")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLAST, FCConvert.ToString(rs.Get_Fields_String("lastname")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFIRST, rs.Get_Fields_String("Firstname") + " " + rs.Get_Fields_String("middlename"));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDATE, FCConvert.ToString(rs.Get_Fields("dateofdeath")));
					rs.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("Nothing Found", "Nothing Found:", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		// vbPorter upgrade warning: CN As object	OnWrite(string())
		public string DeceasedsName(string[] CN)
		{
			string DeceasedsName = "";
			switch (Information.UBound(CN, 1) + 1)
			{
				case 1:
					{
						DeceasedsName = "LastName LIKE '" + ((object[])CN)[0] + "%'";
						break;
					}
				case 2:
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])CN)[1])) != string.Empty)
						{
							DeceasedsName = "LastName LIKE '" + ((object[])CN)[0] + "%*' AND FirstName LIKE '" + ((object[])CN)[1] + "%'";
						}
						else
						{
							DeceasedsName = "lastname like '" + ((object[])CN)[0] + "%'";
						}
						break;
					}
				case 3:
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(((object[])CN)[2])) != string.Empty)
						{
							DeceasedsName = "LastName LIKE '" + ((object[])CN)[0] + "%' AND FirstName LIKE '" + ((object[])CN)[1] + "%' AND MiddleName LIKE '" + ((object[])CN)[2] + "%'";
						}
						else
						{
							DeceasedsName = "LastName LIKE '" + ((object[])CN)[0] + "%' AND FirstName LIKE '" + ((object[])CN)[1] + "%'";
						}
						break;
					}
				default:
					{
						MessageBox.Show("Deceased's Name Error");
						break;
					}
			}
			//end switch
			return DeceasedsName;
		}

		public void clrForm(bool boolClear = true)
		{
			object[] tmpString = null;
			// - "AutoDim"
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";

                if (obj is T2KDateBox)
                {
                    obj.Text = "";
                }
			}
			// obj
			if (boolClear)
				Grid.Rows = 1;
			cmbDateofDeath.Text = "Disable";
			tmpString = new object[2 + 1];
		}

		public void FillBoxes()
		{
			Text2[0].Text = rs.Get_Fields_String("FirstName") + "  " + rs.Get_Fields_String("MiddleName") + " " + rs.Get_Fields_String("LastName");
		}

		private void SetupGrid()
		{
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLLAST, "Last");
			Grid.TextMatrix(0, CNSTGRIDCOLFIRST, "First Middle");
			Grid.TextMatrix(0, CNSTGRIDCOLDATE, "Date of Death");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLLAST, FCConvert.ToInt32(0.33 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLFIRST, FCConvert.ToInt32(0.4 * GridWidth));
		}

        public IBurialSearchViewModel ViewModel { get; set; }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

    }
}
