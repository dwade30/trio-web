//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication;
using SharedApplication.CentralDocuments;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Deaths;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCK0000
{
	public partial class frmDeaths : BaseForm, IView<IDeathViewModel>
    {
        private bool cancelling = true;
		public frmDeaths()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmDeaths(IDeathViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
            if (_InstancePtr == null)
            {
                _InstancePtr = this;
                Sys.SaveInstance(this);
            }
				
			Label1 = new System.Collections.Generic.List<FCLabel>();
			Label1.AddControlArrayElement(Label1_0, 0);
			Label1.AddControlArrayElement(Label1_1, 1);
			Label1.AddControlArrayElement(Label1_2, 2);
			Label1.AddControlArrayElement(Label1_3, 3);
			Label1.AddControlArrayElement(Label1_4, 4);
			Label1.AddControlArrayElement(Label1_5, 5);
			Label1.AddControlArrayElement(Label1_6, 6);
			Label1.AddControlArrayElement(Label1_7, 7);
			Label1.AddControlArrayElement(Label1_8, 8);
			Label1.AddControlArrayElement(Label1_9, 9);
			Label1.AddControlArrayElement(Label1_10, 10);
			Label1.AddControlArrayElement(Label1_11, 11);
			Label1.AddControlArrayElement(Label1_12, 12);
			Label1.AddControlArrayElement(Label1_13, 13);
			Label1.AddControlArrayElement(Label1_14, 14);
			Label1.AddControlArrayElement(Label1_16, 15);
			Label1.AddControlArrayElement(Label1_17, 16);
			Label1.AddControlArrayElement(Label1_18, 17);
			Label1.AddControlArrayElement(Label1_19, 18);
			Label1.AddControlArrayElement(Label1_20, 19);
			Label1.AddControlArrayElement(Label1_21, 20);
			Label1.AddControlArrayElement(Label1_22, 21);
			Label1.AddControlArrayElement(Label1_23, 22);
			Label1.AddControlArrayElement(Label1_25, 23);
			Label1.AddControlArrayElement(Label1_27, 24);
			Label1.AddControlArrayElement(Label1_28, 25);
			Label1.AddControlArrayElement(Label1_29, 26);
			Label1.AddControlArrayElement(Label1_30, 27);
			Label1.AddControlArrayElement(Label1_31, 28);
			Label1.AddControlArrayElement(Label1_32, 29);
			Label1.AddControlArrayElement(Label1_33, 30);
			Label1.AddControlArrayElement(Label1_34, 31);
			Label1.AddControlArrayElement(Label1_35, 32);
			Label1.AddControlArrayElement(Label1_36, 33);
			Label1.AddControlArrayElement(Label1_37, 34);
			Label1.AddControlArrayElement(Label1_38, 35);
			Label1.AddControlArrayElement(Label1_39, 36);
			Label1.AddControlArrayElement(Label1_40, 37);
			Label1.AddControlArrayElement(Label1_41, 38);
			Label1.AddControlArrayElement(Label1_42, 39);
			Label1.AddControlArrayElement(Label1_43, 40);
			Label1.AddControlArrayElement(Label1_44, 41);
			Label1.AddControlArrayElement(Label1_45, 42);
			Label1.AddControlArrayElement(Label1_46, 43);
			Label1.AddControlArrayElement(Label1_47, 44);
			Label1.AddControlArrayElement(Label1_48, 45);
			Label1.AddControlArrayElement(Label1_49, 46);
			Label1.AddControlArrayElement(Label1_50, 47);
			Label1.AddControlArrayElement(Label1_51, 48);
			Label1.AddControlArrayElement(Label1_52, 49);
			Label1.AddControlArrayElement(Label1_54, 50);
			Label1.AddControlArrayElement(Label1_55, 51);
			Label1.AddControlArrayElement(Label1_56, 52);
			Label1.AddControlArrayElement(Label1_57, 53);
			Label1.AddControlArrayElement(Label1_58, 54);
			Label1.AddControlArrayElement(Label1_59, 55);
			Label1.AddControlArrayElement(Label1_60, 56);
			Label1.AddControlArrayElement(Label1_61, 57);
			Label1.AddControlArrayElement(Label1_62, 58);
			Label1.AddControlArrayElement(Label1_63, 59);
			Label1.AddControlArrayElement(Label1_67, 60);
			lblLabels = new System.Collections.Generic.List<FCLabel>();
			lblLabels.AddControlArrayElement(lblLabels_42, 0);
			chkDisposition = new System.Collections.Generic.List<FCCheckBox>();
			chkDisposition.AddControlArrayElement(chkDisposition_0, 0);
			chkDisposition.AddControlArrayElement(chkDisposition_1, 1);
			chkDisposition.AddControlArrayElement(chkDisposition_2, 2);
			chkDisposition.AddControlArrayElement(chkDisposition_3, 3);
			chkDisposition.AddControlArrayElement(chkDisposition_4, 4);
			chkDisposition.AddControlArrayElement(chkDisposition_5, 5);
			//FC:FINAL:DDU:#2405 - set fields to input only numerics
			this.txtAge.AllowOnlyNumericInput();
			this.txtAgeMonths.AllowOnlyNumericInput();
			this.txtAgeDays.AllowOnlyNumericInput();
			this.txtAgeHours.AllowOnlyNumericInput();
			this.txtAgeMinutes.AllowOnlyNumericInput();
            this.Closing += FrmDeaths_Closing;
		}

        private void FrmDeaths_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmDeaths InstancePtr
		{
			get
			{
				return (frmDeaths)Sys.GetInstance(typeof(frmDeaths));
			}
		}

		protected frmDeaths _InstancePtr = null;
		//=========================================================
		private clsDRWrapper rs = new clsDRWrapper();
		private string strSQL = string.Empty;
		private bool Adding;
		private bool DontUnload;
		private bool GoodValidation;
		private bool boolFormLoading;
		private int intDataChanged;
		private bool boolDataChanged;
		int lngDeathNumber;

		private void ResizeGridTownCode()
		{
			//gridTownCode.HeightOriginal = gridTownCode.RowHeight(0) + 60;
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGNBas.Statics.boolRegionalTown)
			{
				clsLoad.OpenRecordset("select * from tblregionS where townnumber > 0 order by townnumber", "CentralData");
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("townnumber") + ";" + clsLoad.Get_Fields_String("townname") + "\t" + clsLoad.Get_Fields("townnumber") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			else
			{
				strTemp = "0";
				gridTownCode.Visible = false;
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}

		private void SetupcmbMannerofDeath()
		{
			cmbMannerOfDeath.Clear();
			cmbMannerOfDeath.AddItem(" ");
			cmbMannerOfDeath.AddItem("Natural");
			cmbMannerOfDeath.AddItem("Accident");
			cmbMannerOfDeath.AddItem("Suicide");
			cmbMannerOfDeath.AddItem("Homicide");
			cmbMannerOfDeath.AddItem("Pending Investigation");
			cmbMannerOfDeath.AddItem("Could Not Be Determined");
		}

		private void chkAutopsyAvailablePrior_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkAutopsyPerformed_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkDisposition_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void chkDisposition_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = chkDisposition.GetIndex((FCCheckBox)sender);
			chkDisposition_CheckedChanged(index, sender, e);
		}

		private void chkSignature_CheckedChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cmbMannerOfDeath_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void cmdFind_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: strName As object	OnWrite(string())
			object strName;
			clsDRWrapper rsFacility = new clsDRWrapper();
			modGNBas.Statics.boolDeathClerkFind = true;
			frmListFacility.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrFacility) == string.Empty)
				return;
			strName = Strings.Split(modGNBas.Statics.gstrFacility, ",", -1, CompareConstants.vbBinaryCompare);
			txtNameandAddressofFacility.Text = fecherFoundation.Strings.Trim(modGNBas.Statics.gstrFacility) + "\r\n";
			rsFacility.OpenRecordset("Select * from FacilityNames where ID = " + FCConvert.ToString(modGNBas.Statics.gintFacility), modGNBas.DEFAULTDATABASE);
			if (!rsFacility.EndOfFile())
			{
				if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address1") + " ") != string.Empty)
				{
					txtNameandAddressofFacility.Text = txtNameandAddressofFacility.Text + fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address1") + " ") + "\r\n";
				}
				if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address2") + " ") != string.Empty)
				{
					txtNameandAddressofFacility.Text = txtNameandAddressofFacility.Text + fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address2") + " ") + "\r\n";
				}
				if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("City") + " ") != string.Empty)
				{
					txtNameandAddressofFacility.Text = txtNameandAddressofFacility.Text + fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("City") + " ") + ", " + fecherFoundation.Strings.Trim(rsFacility.Get_Fields("State") + " ") + " " + fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Zip") + " ");
				}
				this.txtFuneralEstablishmentLicenseNumber.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFacility.Get_Fields_String("FacilityNumber")));
			}
		}

		private void cmdFindClerk_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			modGNBas.Statics.boolDeathClerkFind = true;
			frmListRegistrar.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (fecherFoundation.Strings.Trim(modGNBas.Statics.gstrRegistrar) == string.Empty)
				return;
			rs.OpenRecordset("Select * from DefaultRegistrarNames where ID = " + modGNBas.Statics.gstrRegistrar, modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				txtNameofClerk.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MI"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("LastName")));
				this.txtCityofClerk.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ClerkTown")));
			}
		}

		public void frmDeaths_Activated(object sender, System.EventArgs e)
		{
		}

		private void frmDeaths_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDeaths_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuQuit_Click();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != txtNameandAddressofCertifier.Name && this.ActiveControl.GetName() != txtNameandAddressofFacility.Name && this.ActiveControl.GetName() != txtInformantMailingAddress.Name && this.ActiveControl.GetName() != txtCausesOther.Name)
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDeaths_Resize(object sender, System.EventArgs e)
		{
			ResizeVsFlexGrid1();
			ResizeGridTownCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			intDataChanged += FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "mnuSave_Click");
			if (DontUnload == true)
			{
				// dont unload
				e.Cancel = true;
				DontUnload = false;
				return;
			}
			else
			{
				//modGNBas.Statics.boolDeathOpen = false;
			}
			//if (modCashReceiptingData.Statics.bolFromWindowsCR)
			//{
			//	modGNBas.Statics.gboolEndApplication = true;
			//}
			//MDIParent.InstancePtr.Show();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void gridTownCode_ChangeEdit(object sender, System.EventArgs e)
		{
			intDataChanged = 1;
		}

		private void mnuEditTowns_Click(object sender, System.EventArgs e)
		{
			frmStateTownInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuViewDocuments_Click(object sender, System.EventArgs e)
		{
			ViewDocs();
		}

		private void ViewDocs()
		{
			if (lngDeathNumber <= 0) return;

			//frmViewDocuments.InstancePtr.Unload();
			//frmViewDocuments.InstancePtr.Init(0, "Deaths", modGNBas.CNSTTYPEDEATHS.ToString(), lngDeathNumber, "", "twck0000.vb1", FCForm.FormShowEnum.Modal);
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer("Deaths", "Clerk", "Death",
                lngDeathNumber, "", true, false));
            var documentHeaders = StaticSettings.GlobalCommandDispatcher.Send(new GetHeadersByReferenceCommand()).Result;
        }

		private void txtActualDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsDate(txtActualDate.Text))
			{
				MessageBox.Show("This date of death is in the future and should be changed", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void txtAgeDays_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && (KeyAscii != Keys.Back && KeyAscii != Keys.Escape && KeyAscii != Keys.F16))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAgeHours_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && (KeyAscii != Keys.Back && KeyAscii != Keys.Escape && KeyAscii != Keys.F16))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAgeMinutes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && (KeyAscii != Keys.Back && KeyAscii != Keys.Escape && KeyAscii != Keys.F16))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAgeMonths_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && (KeyAscii != Keys.Back && KeyAscii != Keys.Escape && KeyAscii != Keys.F16))
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtBirthplace_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtBirthplace.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtCityofClerk_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCityofClerk.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtCityOrTownOfDeath_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCityOrTownOfDeath.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtCountyOfDeath_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "C";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtCountyOfDeath.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtDateFiled_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void txtDateFiled_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateFiled.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateFiled.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateFiled.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateFiled.Text = strD;
					}
				}
			}
		}

		private void txtDateofBirth_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void mebDateOfDeath_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void mebDateOfDeath_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(mebDateOfDeath.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(mebDateOfDeath.Text)))
				{
					strD = fecherFoundation.Strings.Trim(mebDateOfDeath.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						mebDateOfDeath.Text = strD;
					}
				}
			}
			if (Information.IsDate(fecherFoundation.Strings.Trim(mebDateOfDeath.Text)))
			{
				txtActualDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateValue(fecherFoundation.Strings.Trim(mebDateOfDeath.Text)), "MM/dd/yyyy");
			}
		}

		private void mebDateOfDisposition_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void mebDateOfDisposition_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(mebDateOfDisposition.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(mebDateOfDisposition.Text)))
				{
					strD = fecherFoundation.Strings.Trim(mebDateOfDisposition.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						mebDateOfDisposition.Text = strD;
					}
				}
			}
		}

		private void txtDateofBirth_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//string strD = "";
			//if (fecherFoundation.Strings.Trim(txtDateofBirth.Text).Length == 8)
			//{
			//	if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateofBirth.Text)))
			//	{
			//		strD = fecherFoundation.Strings.Trim(txtDateofBirth.Text);
			//		strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
			//		if (Information.IsDate(strD))
			//		{
			//			txtDateofBirth.Text = strD;
			//		}
			//	}
			//}
			if (Information.IsDate(txtDateofBirth.Text))
			{
				if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Now, FCConvert.ToDateTime(txtDateofBirth.Text)) > 0)
				{
					MessageBox.Show("This birth date is in the future and should be changed", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void txtDateSigned_TextChanged(object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void mnuAddNew_Click(object sender, System.EventArgs e)
		{
			lngDeathNumber = 0;
			ClearBoxes();
			Adding = true;
		}

		private void mnuBurialPermit_Click(object sender, System.EventArgs e)
		{
			if (lngDeathNumber != 0)
			{
				if (intDataChanged > 0)
				{
					if (MessageBox.Show("Save current death information and proceed to Burial Permit?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (ValidateData())
						{
							SaveData(false);
							// frmBurialPermit.Show 
							frmBurialPermit.InstancePtr.Init(0, lngDeathNumber);
							Hide();
						}
						else
						{
						}
					}
				}
				else
				{
					frmBurialPermit.InstancePtr.Init(0, lngDeathNumber);
					// frmBurialPermit.Show 
					frmDeaths.InstancePtr.Hide();
				}
			}
			else
			{
				MessageBox.Show("You must enter information and save the death certificate before you can process a burial permit.", "Invalid Option", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuComments_Click(object sender, System.EventArgs e)
		{
			if (lngDeathNumber == 0)
			{
				MessageBox.Show("This record does not exist yet.  You must save before this record can be tied to a comment.", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (frmCommentOld.InstancePtr.Init("CK", "Deaths", "Comments", "ID", lngDeathNumber, boolModal: true))
			{
				ImgComment.Image = null;
			}
			else
			{
				ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
			DialogResult answer;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				answer = MessageBox.Show("Are you sure you want to permanently delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Delete();
					if (rs.EndOfFile())
					{
						rs.MoveFirst();
					}
					else
					{
						rs.MoveNext();
					}
				}
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " has been encountered." + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				return;
			}
		}

		private void mnuFacilityNames_Click(object sender, System.EventArgs e)
		{
			// will not allow the form_activate event to fire
			modGNBas.Statics.boolDeathClerkFind = true;
			frmFacilityNames.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		//private void mnuNext_Click()
		//{
		//	if (!rs.EndOfFile())
		//	{
		//		rs.MoveNext();
		//		if (rs.EndOfFile() == true)
		//			rs.MoveFirst();
		//		ClearBoxes();
		//		FillBoxes();
		//		Adding = false;
		//	}
		//}

		//private void mnuPrevious_Click()
		//{
		//	if (!rs.EndOfFile())
		//	{
		//		rs.MovePrevious();
		//		if (rs.BeginningOfFile() == true)
		//			rs.MoveLast();
		//		ClearBoxes();
		//		FillBoxes();
		//		Adding = false;
		//	}
		//}

		//private void mnuPrint_Click()
		//{
		//	int lngNumCopies;
		//	double dblTemp;
		//	modCashReceiptingData.Statics.typeCRData.Type = "DEA";
		//	modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(txtLastName.Text + ", " + txtFirstName.Text, 30);
		//	modCashReceiptingData.Statics.typeCRData.Reference = txtFileNumber.Text;
		//	if (Conversion.Val(mebDateOfDeath.Text) != 0 && Information.IsDate(mebDateOfDeath.Text))
		//	{
		//		modCashReceiptingData.Statics.typeCRData.Control1 = Strings.Format(mebDateOfDeath.Text, "mm,dd,yyyy");
		//	}
		//	else
		//	{
		//		modCashReceiptingData.Statics.typeCRData.Control1 = mebDateOfDeath.Text;
		//	}
		//	modCashReceiptingData.Statics.typeCRData.Control2 = txtLicenseeNumber.Text;
		//	intDataChanged += FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this));
		//	if (boolDataChanged)
		//		intDataChanged += 1;
		//	modDirtyForm.SaveChanges(intDataChanged, this, "mnuSave_Click");
		//	boolDataChanged = false;
		//	if (DontUnload == true)
		//	{
		//		// dont unload
		//	}
		//	else
		//	{
		//		modClerkGeneral.Statics.utPrintInfo.PreView = false;
		//		ShowPrint();
		//	}
		//	// If gboolFromDosTrio = True Or bolFromWindowsCR Then
		//	// With typeCRData
		//	// .Type = "DEA"
		//	// .Name = Left(frmDeaths.txtLastName.Text & ", " & frmDeaths.txtFirstName.Text, 30)
		//	// .Reference = frmDeaths.txtFileNumber.Text
		//	// If Val(mebDateOfDeath.Text) <> 0 And IsDate(mebDateOfDeath.Text) Then
		//	// .Control1 = Format(frmDeaths.mebDateOfDeath.Text, "mm,dd,yyyy")
		//	// Else
		//	// .Control1 = mebDateOfDeath.Text
		//	// End If
		//	// .Control2 = frmDeaths.txtLicenseeNumber.Text
		//	// Call frmInput.Init(lngNumCopies, "Copies", "Enter number of copies to charge for", 1440, , idtWholeNumber)
		//	// If frmDeaths.optMilitarySrv(0) Then
		//	// veterans fee
		//	// dbltemp = typClerkFees.AdditionalFee5 * lngNumCopies
		//	// .Amount1 = Format((1 * typClerkFees.AdditionalFee5), "000000.00")
		//	// .Amount1 = Format((dbltemp), "0.00")
		//	// Else
		//	// dbltemp = typClerkFees.NewDeath
		//	// dbltemp = dbltemp + (typClerkFees.ReplacementDeath * (lngNumCopies - 1))
		//	// .Amount1 = Format(typClerkFees.NewDeath, "000000.00")
		//	// .Amount1 = Format(dbltemp, "0.00")
		//	// End If
		//	// 
		//	// .Amount2 = Format(0, "000000.00")
		//	// .Amount3 = Format(0, "000000.00")
		//	// .Amount4 = Format(0, "000000.00")
		//	// .Amount5 = Format(0, "000000.00")
		//	// .Amount6 = Format(0, "000000.00")
		//	// .ProcessReceipt = "Y"
		//	// End With
		//	// 
		//	// Call WriteCashReceiptingData(typeCRData)
		//	// End
		//	// End If
		//}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			if (chkSupplemental.CheckState == Wisej.Web.CheckState.Checked)
			{
				// MsgBox "This certificate has a supplemental" & vbNewLine & "Please remember to attach it" & vbNewLine & "Also note that the original must be photo copied", vbInformation, "Supplemental"
				MessageBox.Show("Remember to attach the supplemental to the certificate and that the original certificate must be photocopied", "Supplemental", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			intDataChanged += FCConvert.ToInt32(modDirtyForm.NumberDirtyControls(this));
			if (boolDataChanged)
				intDataChanged += 1;
			modDirtyForm.SaveChanges(intDataChanged, this, "mnuSave_Click");
			if (modGNBas.Statics.boolNotSave)
			{
				// dont unload
			}
			else
			{
				modClerkGeneral.Statics.utPrintInfo.PreView = true;
				ShowPrint();
			}
		}

		private void ShowPrint()
		{
			string strSQL = "";
			bool boolOld;
			// vbPorter upgrade warning: dblAmt1 As double	OnWriteFCConvert.ToDecimal(
			decimal dblAmt1 = 0;
			// vbPorter upgrade warning: dblAmt2 As double	OnWrite(bool, Decimal)
			decimal dblAmt2 = 0;
			modDogs.GetClerkFees();
			// If RS.EndOfFile Then Exit Sub
			if (cmbCertifiedType.Text == "Medical Examiner")
			{
				MessageBox.Show("This record cannot be printed as a Medical Examiner has been chosen. MUST COPY ORIGINAL DOCUMENT", "Cannot Print", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			boolOld = false;
			if (Information.IsDate(mebDateOfDeath.Text))
			{
				// If mebDateOfDeath.Text <> 0 Then
				if (FCConvert.ToDateTime(mebDateOfDeath.Text).Year < 1892)
				{
					boolOld = true;
				}
				// End If
			}
			modCashReceiptingData.Statics.typeCRData.Type = "DEA";
			modCashReceiptingData.Statics.typeCRData.Name = Strings.Left(txtLastName.Text + ", " + txtFirstName.Text, 30);
			modCashReceiptingData.Statics.typeCRData.Reference = txtFileNumber.Text;
			if (Information.IsDate(mebDateOfDeath.Text))
			{
				modCashReceiptingData.Statics.typeCRData.Control1 = Strings.Format(mebDateOfDeath.Text, "mm,dd,yyyy");
			}
			else
			{
				modCashReceiptingData.Statics.typeCRData.Control1 = mebDateOfDeath.Text;
			}
			modCashReceiptingData.Statics.typeCRData.Control2 = txtLicenseeNumber.Text;
			if (boolOld)
			{
				// If CompareDates(StripDateSlashes(mebDateOfDeath.Text), "01011892", "<") Then
				modGNBas.Statics.typOldVitalsReport.AttestBy = modClerkGeneral.Statics.ClerkName;
				modGNBas.Statics.typOldVitalsReport.FileNumber = txtFileNumber.Text;
				modGNBas.Statics.typOldVitalsReport.pageNumber = string.Empty;
				modGNBas.Statics.typOldVitalsReport.FamilyName = string.Empty;
				modGNBas.Statics.typOldVitalsReport.ChildName1 = txtFirstName.Text + " " + txtLastName.Text;
				modGNBas.Statics.typOldVitalsReport.ChildName2 = string.Empty;
				modGNBas.Statics.typOldVitalsReport.ChildName3 = string.Empty;
				modGNBas.Statics.typOldVitalsReport.DateOfEvent = txtDateSigned.Text;
				modGNBas.Statics.typOldVitalsReport.Parent1 = txtFatherFirstName.Text + " " + txtFatherLastName.Text;
				modGNBas.Statics.typOldVitalsReport.Parent2 = txtMotherFirstName.Text + " " + txtMotherLastName.Text;
				modGNBas.Statics.typOldVitalsReport.Parent3 = string.Empty;
				modGNBas.Statics.typOldVitalsReport.Parent4 = string.Empty;
				if (cmbPlaceofDeath.Text == "DOA")
				{
					modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = "DOA";
				}
				else if (cmbPlaceofDeath.Text == "Inpatient")
				{
					modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = "Inpatient";
				}
				else if (cmbPlaceofDeath.Text == "ER/Outpatient")
				{
					modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = "ER/Outpatient";
				}
				else if (cmbPlaceofDeath.Text == "Nursing Home")
				{
					modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = "Nursing Home";
				}
				else if (cmbPlaceofDeath.Text == "Residence")
				{
					modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = "Residence";
				}
				else if (cmbPlaceofDeath.Text == "Other")
				{
					modGNBas.Statics.typOldVitalsReport.PlaceOfEvent = "Other";
				}
				modGNBas.Statics.typOldVitalsReport.PlaceOfResidence = string.Empty;
				modGNBas.Statics.typOldVitalsReport.NameOfClerk1 = txtNameofClerk.Text;
				modGNBas.Statics.typOldVitalsReport.NameOfClerk2 = string.Empty;
				// .TownOfClerk1 = MuniName
				modGNBas.Statics.typOldVitalsReport.TownOfClerk1 = txtCityofClerk.Text;
				modGNBas.Statics.typOldVitalsReport.TownOfClerk2 = modGlobalConstants.Statics.MuniName;
				modGNBas.Statics.typOldVitalsReport.DateTo = FCConvert.ToString(modDateRoutines.LongDate(DateTime.Today));
				modGNBas.Statics.typOldVitalsReport.DateFrom = string.Empty;
				modGNBas.Statics.typOldVitalsReport.Notes = "Record is dated prior to January 1, 1892. This record must be printed on Town letterhead. Please load letterhead into printer now.";
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 0;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = 0;
				modCashReceiptingData.Statics.typeCRData.NumSeconds = 0;

				frmPrintOldVitals.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if ( modCashReceiptingData.Statics.bolFromWindowsCR)
				{
					if (cmbMilitarySrv.Text == "Yes")
					{
						// veterans fee
						// .Amount1 = Format((intPrinted * typClerkFees.AdditionalFee5), "000000.00")
						// .Amount1 = Format(frmChargeCert.Init(CDbl(typClerkFees.AdditionalFee5), CDbl(typClerkFees.AdditionalFee5), .NumFirsts, .NumSubsequents), "000000.00")
						if (modCashReceiptingData.Statics.typeCRData.NumSubsequents > 0)
						{
							modCashReceiptingData.Statics.typeCRData.NumSeconds = 1;
							modCashReceiptingData.Statics.typeCRData.NumSubsequents -= 1;
						}
						modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(frmChargeCert.InstancePtr.Init(FCConvert.ToDouble((modDogs.Statics.typClerkFees.AdditionalFee5 + modDogs.Statics.typClerkFees.DeathVetStateFee)), FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementDeath + modDogs.Statics.typClerkFees.DeathVetStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents, "Subsequent Copies", "Second Copy", FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewDeath + modDogs.Statics.typClerkFees.DeathStateFee)), modCashReceiptingData.Statics.typeCRData.NumSeconds), "000000.00"));
						dblAmt1 = (modDogs.Statics.typClerkFees.AdditionalFee5 * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementDeath * modCashReceiptingData.Statics.typeCRData.NumSubsequents) + (modDogs.Statics.typClerkFees.NewDeath * modCashReceiptingData.Statics.typeCRData.NumSeconds);
						dblAmt2 = dblAmt1 = (modDogs.Statics.typClerkFees.DeathVetStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.DeathReplacementStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents) + (modDogs.Statics.typClerkFees.DeathStateFee * modCashReceiptingData.Statics.typeCRData.NumSeconds);
					}
					else
					{
						modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(frmChargeCert.InstancePtr.Init(FCConvert.ToDouble((modDogs.Statics.typClerkFees.NewDeath + modDogs.Statics.typClerkFees.DeathStateFee)), FCConvert.ToDouble((modDogs.Statics.typClerkFees.ReplacementDeath + modDogs.Statics.typClerkFees.DeathReplacementStateFee)), modCashReceiptingData.Statics.typeCRData.NumFirsts, modCashReceiptingData.Statics.typeCRData.NumSubsequents), "000000.00"));
						dblAmt1 = ((modDogs.Statics.typClerkFees.NewDeath * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.ReplacementDeath * modCashReceiptingData.Statics.typeCRData.NumSubsequents));
						dblAmt2 = ((modDogs.Statics.typClerkFees.DeathStateFee * modCashReceiptingData.Statics.typeCRData.NumFirsts) + (modDogs.Statics.typClerkFees.DeathReplacementStateFee * modCashReceiptingData.Statics.typeCRData.NumSubsequents));
					}
					modCashReceiptingData.Statics.typeCRData.Amount1 = FCConvert.ToDouble(Strings.Format(dblAmt1, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(dblAmt2, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount2 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount3 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount4 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount5 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.Amount6 = FCConvert.ToDouble(Strings.Format(0, "000000.00"));
					modCashReceiptingData.Statics.typeCRData.ProcessReceipt = "Y";
					modCashReceiptingData.Statics.typeCRData.PartyID = 0;
					modCashReceiptingData.WriteCashReceiptingData(ref modCashReceiptingData.Statics.typeCRData);
                    UpdateTransaction();
                    //Application.Exit();
				}
			}
			else
			{
				// With RS
				modClerkGeneral.Statics.utPrintInfo.tblKeyNum = lngDeathNumber;
				modClerkGeneral.Statics.utPrintInfo.TblName = "Deaths";
				// End With
				strSQL = "Select * from Deaths where ID = " + FCConvert.ToString(modClerkGeneral.Statics.utPrintInfo.tblKeyNum);
				modGNBas.Statics.rsDeathCertificate.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
				//frmDeaths.InstancePtr.Hide();
				//// frmPrintVitalRec.Show 
				//frmPrintVitalRec.InstancePtr.Init(false);
                UpdateTransaction();
                ViewModel.PrintVital();
                CloseWithoutCancel();
			}
		}

        private void UpdateTransaction()
        {
            cancelling = false;
            var transaction = new VitalTransaction();
            transaction.TypeCode = "DEA";
            transaction.TownCode = gridTownCode.TextMatrix(0, 0).ToString().ToIntegerValue();
            transaction.VitalAmount = Convert.ToDecimal(modCashReceiptingData.Statics.typeCRData.Amount1);
            transaction.StateFee = Convert.ToDecimal(modCashReceiptingData.Statics.typeCRData.Amount2);
            transaction.Name = txtLastName.Text + ", " + txtFirstName.Text;
            transaction.Reference = txtFileNumber.Text;
            if (Information.IsDate(mebDateOfDeath.Text))
            {
                transaction.Control1 = Strings.Format(mebDateOfDeath.Text, "mm,dd,yyyy");
            }
            else
            {
                transaction.Control1 = mebDateOfDeath.Text;
            }
            transaction.Control2 = txtLicenseeNumber.Text;

            ViewModel.UpdateTransaction(transaction);
        }

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			frmDeaths.InstancePtr.Close();
		}

		public void mnuQuit_Click()
		{
			mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void FillBoxes()
		{
			strSQL = "SELECT * FROM Deaths Where(ID =" + FCConvert.ToString(lngDeathNumber) + " )";
			rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
			if (!rs.EndOfFile())
			{
				// Response = Val(RS.Fields("ID"))
				lngDeathNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("ID"))));
				modGNBas.Statics.gintBurialID = lngDeathNumber;
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FileNumber")))
					txtFileNumber.Text = FCConvert.ToString(rs.Get_Fields_String("FileNumber"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("LastName")) == false)
					txtLastName.Text = FCConvert.ToString(rs.Get_Fields_String("LastName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FirstName")) == false)
					txtFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FirstName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MiddleName")) == false)
					txtMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("MiddleName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Designation")) == false)
					txtDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("Designation"));
				// If IsNull(.Fields("DateofDeath")) = False And .Fields("DateofDeath") <> vbNullString Then mebDateOfDeath.Text = Format(.Fields("DateofDeath"), "MM/dd/yyyy")
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("dateofdeath")))
				{
					if (Information.IsDate(rs.Get_Fields("dateofdeath")) && Convert.ToDateTime(rs.Get_Fields("dateofdeath")).ToOADate() != 0)
					{
						mebDateOfDeath.Text = Strings.Format(rs.Get_Fields("DateOfDeath"), "MM/dd/yyyy");
						// mebDateOfDeath.Text = .Fields("dateofdeath")
					}
					else
					{
						mebDateOfDeath.Text = rs.Get_Fields_String("DateOfDeathDescription") + "";
					}
				}
				else
				{
					mebDateOfDeath.Text = rs.Get_Fields_String("DateOfDeathDescription") + "";
				}
				if (Information.IsDate(rs.Get_Fields("ActualDate")) && Convert.ToDateTime(rs.Get_Fields_DateTime("ActualDate")).ToOADate() != 0)
				{
					txtActualDate.Text = Strings.Format(rs.Get_Fields_DateTime("ActualDate"), "MM/dd/yyyy");
				}
				else
				{
					txtActualDate.Text = "";
				}
				// If IsNull(.Fields("DateofDeath")) = False And .Fields("DateofDeath") <> vbNullString Then mebDateOfDeath.Text = .Fields("DateofDeath")
				txtDateofBirth.Text = FCConvert.ToString(rs.Get_Fields("DateOfBirth"));
				txtCausesOther.Text = FCConvert.ToString(rs.Get_Fields_String("CausesOther"));
				// If IsNull(.Fields("SocialSecurityNumber")) = False Then
				// If Not IsNumeric(Right(.Fields("SocialSecurityNumber"), 1)) And Len(.Fields("socialsecuritynumber")) > 0 Then
				// txtSocialSecurityNumber.Text = Format(Val(.Fields("SocialSecurityNumber")), "000-00-0000") & Right(.Fields("SocialSecurityNumber"), 1)
				// Else
				// txtSocialSecurityNumber.Text = Format(Val(.Fields("SocialSecurityNumber")), "000-00-00000")
				// End If
				// End If
				txtTimeOfDeath.Text = FCConvert.ToString(rs.Get_Fields_String("timeofdeath"));
				txtSocialSecurityNumber.Text = FCConvert.ToString(rs.Get_Fields_String("socialsecuritynumber"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Sex")) == false)
					txtSex.Text = FCConvert.ToString(rs.Get_Fields_String("Sex"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FatherLastName")) == false)
					txtFatherLastName.Text = FCConvert.ToString(rs.Get_Fields_String("FatherLastName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FatherFirstName")) == false)
					txtFatherFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("FatherFirstName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FatherMiddleName")) == false)
					txtFatherMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("FatherMiddleName"));
				if (FCConvert.ToString(rs.Get_Fields_String("FatherMiddlename")) != "N/A")
				{
					txtFatherMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("fathermiddlename"));
				}
				else
				{
					txtFatherMiddleName.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FatherDesignation")) == false)
					txtFatherDesignation.Text = FCConvert.ToString(rs.Get_Fields_String("FatherDesignation"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MotherLastName")) == false)
					txtMotherLastName.Text = FCConvert.ToString(rs.Get_Fields_String("MotherLastName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MotherFirstName")) == false)
					txtMotherFirstName.Text = FCConvert.ToString(rs.Get_Fields_String("MotherFirstName"));
				// If IsNull(.Fields("MotherMiddleName")) = False Then txtMotherMiddleName.Text = .Fields("MotherMiddleName")
				if (FCConvert.ToString(rs.Get_Fields_String("MotherMiddleName")) != "N/A")
				{
					txtMotherMiddleName.Text = FCConvert.ToString(rs.Get_Fields_String("mothermiddlename"));
				}
				else
				{
					txtMotherMiddleName.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("InformantName")) == false)
					txtInformantName.Text = FCConvert.ToString(rs.Get_Fields_String("InformantName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("InformantMailingAddress")) == false)
					txtInformantMailingAddress.Text = FCConvert.ToString(rs.Get_Fields_String("InformantMailingAddress"));
				if (FCConvert.ToInt32(rs.Get_Fields_String("Signature")) == 1)
					chkSignature.CheckState = Wisej.Web.CheckState.Checked;
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CauseA")) == false)
					txtCauseA.Text = FCConvert.ToString(rs.Get_Fields_String("CauseA"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CauseB")) == false)
				{
					txtCauseB.Text = FCConvert.ToString(rs.Get_Fields_String("CauseB"));
				}
				else
				{
					txtCauseB.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CauseC")) == false)
				{
					txtCauseC.Text = FCConvert.ToString(rs.Get_Fields_String("CauseC"));
				}
				else
				{
					txtCauseC.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CauseD")) == false)
				{
					txtCauseD.Text = FCConvert.ToString(rs.Get_Fields_String("CauseD"));
				}
				else
				{
					txtCauseD.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("OnsetA")) == false)
				{
					txtOnsetA.Text = FCConvert.ToString(rs.Get_Fields_String("OnsetA"));
				}
				else
				{
					txtCauseA.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("OnsetB")) == false)
				{
					txtOnsetB.Text = FCConvert.ToString(rs.Get_Fields_String("OnsetB"));
				}
				else
				{
					txtCauseB.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("OnsetC")) == false)
				{
					txtOnsetC.Text = FCConvert.ToString(rs.Get_Fields_String("OnsetC"));
				}
				else
				{
					txtOnsetC.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("OnsetD")) == false)
				{
					txtOnsetD.Text = FCConvert.ToString(rs.Get_Fields_String("OnsetD"));
				}
				else
				{
					txtOnsetD.Text = "";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("DispositionOther")) == false)
					txtDispositionOther.Text = FCConvert.ToString(rs.Get_Fields_String("DispositionOther"));
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				string strTemp = "";
				strTemp = FCConvert.ToString(rs.Get_Fields_String("disposition"));
				for (x = 0; x <= 5; x++)
				{
					chkDisposition[x].CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// x
				if (strTemp != string.Empty)
				{
					for (x = 1; x <= (strTemp.Length); x++)
					{
						if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "T")
						{
							chkDisposition[0].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "B")
						{
							chkDisposition[1].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "C")
						{
							chkDisposition[2].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "R")
						{
							chkDisposition[3].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "U")
						{
							chkDisposition[4].CheckState = Wisej.Web.CheckState.Checked;
						}
						else if (fecherFoundation.Strings.UCase(Strings.Mid(strTemp, x, 1)) == "O")
						{
							chkDisposition[5].CheckState = Wisej.Web.CheckState.Checked;
						}
					}
					// x
				}
				// If .Fields("Disposition") = "T" Then
				// optDisposition(0).Value = True
				// ElseIf .Fields("Disposition") = "B" Then
				// optDisposition(1).Value = True
				// ElseIf .Fields("Disposition") = "C" Then
				// optDisposition(2).Value = True
				// ElseIf .Fields("Disposition") = "R" Then
				// optDisposition(3).Value = True
				// ElseIf .Fields("Disposition") = "U" Then
				// optDisposition(4).Value = True
				// ElseIf .Fields("Disposition") = "O" Then
				// optDisposition(5).Value = True
				// Else
				// Do Nothing
				// End If
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Boolean("Embalmed")) == false)
				{
					// If .Fields("Embalmed") = True Then
					// optEmbalmed(0).Value = vbChecked
					// Else
					// optEmbalmed(1).Value = vbChecked
					// End If
					if (rs.Get_Fields_Boolean("embalmed") == true)
					{
						chkBodyEmbalmed.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkBodyEmbalmed.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					chkBodyEmbalmed.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// If IsNull(.Fields("PlaceOfDispositionCity")) = False Then txtDispositionCity.Text = .Fields("PlaceOfDispositionCity")
				// If IsNull(.Fields("PlaceOfDispositionState")) = False Then txtDispositionState.Text = .Fields("PlaceOfDispositionState")
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FuneralEstablishmentLicenseNumber")) == false)
					txtFuneralEstablishmentLicenseNumber.Text = FCConvert.ToString(rs.Get_Fields_String("FuneralEstablishmentLicenseNumber"));
				txtPlaceofDisposition.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("PlaceofDisposition")));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("LicenseeNumber")) == false)
					txtLicenseeNumber.Text = FCConvert.ToString(rs.Get_Fields_String("LicenseeNumber"));
				if (FCConvert.ToString(rs.Get_Fields_String("DateofDisposition")) != "N/A")
				{
					// If IsNull(.Fields("DateofDisposition")) = False Then mebDateOfDisposition.Text = Format(.Fields("DateofDisposition"), "MM/dd/yyyy")
					if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("DateofDisposition")) == false)
						mebDateOfDisposition.Text = FCConvert.ToString(rs.Get_Fields_String("DateofDisposition"));
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameandAddressofFacility")) == false)
					txtNameandAddressofFacility.Text = FCConvert.ToString(rs.Get_Fields_String("NameandAddressofFacility"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameofCertifier")) == false)
					txtNameandAddressofCertifier.Text = FCConvert.ToString(rs.Get_Fields_String("NameofCertifier"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameofAttendingPhysician")) == false)
					txtNameofAttendingPhysician.Text = FCConvert.ToString(rs.Get_Fields_String("NameofAttendingPhysician"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Boolean("ViewedBody")) == false)
				{
					if (rs.Get_Fields_Int32("viewedbody") == -1)
					{
						cmbViewedBody.Text = 0 != (Wisej.Web.CheckState.Checked) ? "Yes" : "";
					}
					if (rs.Get_Fields_Int32("viewedbody") == 0)
					{
						cmbViewedBody.Text = 0 != (Wisej.Web.CheckState.Checked) ? "No" : "";
					}
					else
					{
						cmbViewedBody.Text = 0 != (Wisej.Web.CheckState.Checked) ? "Unknown" : "";
					}
					// If .Fields("ViewedBody") = -1 Then
					// optViewedBody(0).Value = vbChecked
					// Else
					// optViewedBody(1).Value = vbChecked
					// End If
				}
				else
				{
					cmbViewedBody.Text = 0 != (Wisej.Web.CheckState.Checked) ? "Unknown" : "";
				}
				txtDateSigned.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("DateSigned")));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameofAttendingPhysician")) == false)
					txtNameofAttendingPhysician.Text = FCConvert.ToString(rs.Get_Fields_String("NameofAttendingPhysician"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("PlaceOfDeathOther")) == false)
					txtPlaceofDeathOther.Text = FCConvert.ToString(rs.Get_Fields_String("PlaceOfDeathOther"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("EducationElementary")) == false)
					txtEducationElementary.Text = FCConvert.ToString(rs.Get_Fields_String("EducationElementary"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("EducationCollege")) == false)
					txtEducationCollege.Text = FCConvert.ToString(rs.Get_Fields_String("EducationCollege"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("MostRecentSpouseName")) == false)
					txtMostRecentSpouseName.Text = FCConvert.ToString(rs.Get_Fields_String("MostRecentSpouseName"));
				if (FCConvert.ToString(rs.Get_Fields_String("MaritalStatus")) == "M")
				{
					cmbMaritalStatus.Text = "Married";
				}
				else if (rs.Get_Fields_String("MaritalStatus") == "N")
				{
					cmbMaritalStatus.Text = "Never Married";
					Frame8.Enabled = false;
					cmbMostRecentSpouse.Text = "";
				}
				else if (rs.Get_Fields_String("MaritalStatus") == "W")
				{
					cmbMaritalStatus.Text = "Widowed";
				}
				else if (rs.Get_Fields_String("MaritalStatus") == "D")
				{
					cmbMaritalStatus.Text = "Divorced";
				}
				else if (rs.Get_Fields_String("MaritalStatus") == "O")
				{
					cmbMaritalStatus.Text = "Other";
				}
				else if (rs.Get_Fields_String("Maritalstatus") == "P")
				{
					cmbMaritalStatus.Text = "Domestic Partner";
				}
				if (FCConvert.ToString(rs.Get_Fields_String("MostRecentSpouse")) == "L")
				{
					cmbMostRecentSpouse.Text = "Living";
				}
				else if (rs.Get_Fields_String("MostRecentSpouse") == "D")
				{
					cmbMostRecentSpouse.Text = "Deceased";
				}
				else
				{
					cmbMostRecentSpouse.Text = "N/A";
				}
				if (rs.Get_Fields_Boolean("USArmedServices") == false)
				{
					cmbMilitarySrv.Text = 0 != Wisej.Web.CheckState.Checked ? "No" : "Yes";
				}
				else if (rs.Get_Fields_Boolean("USArmedServices") == true)
				{
					cmbMilitarySrv.Text = 0 != Wisej.Web.CheckState.Checked ? "Yes" : "No";
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ResidenceStreet")) == false)
					txtResidenceStreet.Text = FCConvert.ToString(rs.Get_Fields_String("ResidenceStreet"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Race")) == false)
					txtRace.Text = FCConvert.ToString(rs.Get_Fields_String("Race"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Ancestry")) == false)
					txtAncestry.Text = FCConvert.ToString(rs.Get_Fields_String("Ancestry"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("KindofBusiness")) == false)
					txtKindofBusiness.Text = FCConvert.ToString(rs.Get_Fields_String("KindofBusiness"));
				if (FCConvert.ToString(rs.Get_Fields_String("CityorTownofDeath")) != "N/A")
				{
					txtCityOrTownOfDeath.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CityorTownofDeath")));
				}
				if (FCConvert.ToString(rs.Get_Fields_String("ResidenceCityorTown")) != "N/A")
				{
					txtResidenceCity.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ResidenceCityorTown")));
				}
				if (FCConvert.ToString(rs.Get_Fields_String("PlaceOfDispositionCity")) != "N/A")
				{
					txtDispositionCity.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("PlaceOfDispositionCity")));
				}
				if (FCConvert.ToString(rs.Get_Fields_String("CountyofDeath")) != "N/A")
				{
					txtCountyOfDeath.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CountyofDeath")));
				}
				if (FCConvert.ToString(rs.Get_Fields_String("ResidenceCounty")) != "N/A")
				{
					txtResidenceCounty.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ResidenceCounty")));
				}
				if (FCConvert.ToString(rs.Get_Fields_String("ResidenceState")) != "N/A")
				{
					txtResidenceState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("ResidenceState")));
				}
				if (FCConvert.ToString(rs.Get_Fields_String("PlaceOfDispositionState")) != "N/A")
				{
					txtDispositionState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("PlaceOfDispositionState")));
				}
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Occupation")) == false)
					txtOccupation.Text = FCConvert.ToString(rs.Get_Fields_String("Occupation"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("BirthPlace")) == false)
					txtBirthplace.Text = FCConvert.ToString(rs.Get_Fields_String("BirthPlace"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("AgeDays")) == false)
					txtAgeDays.Text = FCConvert.ToString(rs.Get_Fields_Int32("AgeDays"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("AgeMonths")) == false)
					txtAgeMonths.Text = FCConvert.ToString(rs.Get_Fields_Int32("AgeMonths"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("AgeMinutes")) == false)
					txtAgeMinutes.Text = FCConvert.ToString(rs.Get_Fields_Int32("AgeMinutes"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("AgeHours")) == false)
					txtAgeHours.Text = FCConvert.ToString(rs.Get_Fields_Int32("AgeHours"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields("Age")) == false)
					txtAge.Text = FCConvert.ToString(rs.Get_Fields("Age"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("FacilityName")) == false)
					txtFacilityName.Text = FCConvert.ToString(rs.Get_Fields_String("FacilityName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ClerkofRecordName")) == false)
					txtNameofClerk.Text = FCConvert.ToString(rs.Get_Fields_String("ClerkofRecordName"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ClerkofRecordCity")) == false)
					txtCityofClerk.Text = FCConvert.ToString(rs.Get_Fields_String("ClerkofRecordCity"));
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("DateOriginalFiling")) == false)
					txtDateFiled.Text = FCConvert.ToString(rs.Get_Fields_String("DateOriginalFiling"));
				if (rs.Get_Fields_Int32("placeofdeath") == 0)
				{
					cmbPlaceofDeath.Text = "DOA";
				}
				else if (rs.Get_Fields_Int32("placeofdeath") == 1)
				{
					cmbPlaceofDeath.Text = "Inpatient";
				}
				else if (rs.Get_Fields_Int32("placeofdeath") == 2)
				{
					cmbPlaceofDeath.Text = "ER/Outpatient";
				}
				else if (rs.Get_Fields_Int32("placeofdeath") == 3)
				{
					cmbPlaceofDeath.Text = "Nursing Home";
				}
				else if (rs.Get_Fields_Int32("placeofdeath") == 4)
				{
					cmbPlaceofDeath.Text = "Residence";
				}
				else if (rs.Get_Fields_Int32("placeofdeath") == 5)
				{
					cmbPlaceofDeath.Text = "Other";
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("PlaceOfDeathOther")))
					{
						txtPlaceofDeathOther.Text = FCConvert.ToString(rs.Get_Fields_String("PlaceOfDeathOther"));
					}
				}
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameandAddressofCertifier")))
				{
					txtNameandAddressofCertifier.Text = FCConvert.ToString(rs.Get_Fields_String("NameandAddressofCertifier"));
				}
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("NameofPhysician")))
				{
					txtNameofCertifyingPhysician.Text = FCConvert.ToString(rs.Get_Fields_String("NameofPhysician"));
				}
				cmbCertifiedType.SelectedIndex = FCConvert.ToInt32(rs.Get_Fields_Int16("CertifiedType"));
				chkRequired.CheckState = (CheckState)((rs.Get_Fields_Boolean("NoRequiredFields")) ? 1 : 0);
				chkAmendment.CheckState = (CheckState)((rs.Get_Fields("Amendment")) ? 1 : 0);
				chkCorrection.CheckState = (CheckState)((rs.Get_Fields_Boolean("Correction")) ? 1 : 0);
				chkSupplemental.CheckState = (CheckState)((rs.Get_Fields_Boolean("Supplemental")) ? 1 : 0);
				chkFetalDeath.CheckState = (CheckState)((rs.Get_Fields_Boolean("FetalDeath")) ? 1 : 0);
				chkMarriageOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("MarriageOnFile")) ? 1 : 0);
				chkBirthOnFile.CheckState = (CheckState)((rs.Get_Fields_Boolean("BirthOnFile")) ? 1 : 0);
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("comments"))) != string.Empty)
				{
					ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
				}
				else
				{
					ImgComment.Image = null;
				}
				if (Conversion.Val(rs.Get_Fields_Int32("towncode")) > 0)
				{
					gridTownCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("towncode"))));
				}
				modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("autopsyperformed")))
				{
					chkAutopsyPerformed.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkAutopsyPerformed.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("autopsyavailableprior")))
				{
					chkAutopsyAvailablePrior.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkAutopsyAvailablePrior.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				cmbMannerOfDeath.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int32("mannerofdeath")));
			}
			intDataChanged = 0;
			modDirtyForm.ClearDirtyControls(this);
		}

		private void ClearBoxes()
		{
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is FCCheckBox)
					(obj as FCCheckBox).Value = 0;
				if (obj is FCMaskedTextBox)
				{
					(obj as FCMaskedTextBox).Mask = "";
					(obj as FCMaskedTextBox).Text = "";
					(obj as FCMaskedTextBox).Mask = "##/##/####";
				}
			}
			// lngDeathNumber = 0
			// txtSocialSecurityNumber.Mask = "###-##-####&"
			if (txtCauseB.Text == "")
				txtCauseB.Text = "";
			if (txtCauseC.Text == "")
				txtCauseC.Text = "";
			if (txtCauseD.Text == "")
				txtCauseD.Text = "";
			if (txtOnsetB.Text == "")
				txtOnsetB.Text = "";
			if (txtOnsetC.Text == "")
				txtOnsetC.Text = "";
			if (txtOnsetD.Text == "")
				txtOnsetD.Text = "";
			chkRequired.CheckState = Wisej.Web.CheckState.Unchecked;
			chkCorrection.CheckState = Wisej.Web.CheckState.Unchecked;
			chkSupplemental.CheckState = Wisej.Web.CheckState.Unchecked;
			chkFetalDeath.CheckState = Wisej.Web.CheckState.Unchecked;
			chkMarriageOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkBirthOnFile.CheckState = Wisej.Web.CheckState.Unchecked;
			chkAutopsyAvailablePrior.CheckState = Wisej.Web.CheckState.Unchecked;
			chkAutopsyPerformed.CheckState = Wisej.Web.CheckState.Unchecked;
			cmbMannerOfDeath.SelectedIndex = 0;
			cmbCertifiedType.Text = "Unknown";
			intDataChanged = 0;
			modDirtyForm.ClearDirtyControls(this);
			VSFlexGrid1.Clear();
		}

		private void SaveData(bool boolShow = true, bool boolUseValidation = true)
		{
			try
			{
				// On Error GoTo Handler
				fecherFoundation.Information.Err().Clear();
				strSQL = "SELECT * FROM Deaths Where(ID =" + FCConvert.ToString(lngDeathNumber) + " )";
				rs.OpenRecordset(strSQL, modGNBas.DEFAULTDATABASE);
				if (boolUseValidation)
				{
					if (ValidateData() == false)
					{
						DontUnload = true;
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return;
					}
				}
				if (gridTownCode.Visible == true)
				{
					gridTownCode.Row = -1;
				}
				clsDRWrapper rsClerkInfo = new clsDRWrapper();
				bool blnUpdateFile = false;
				if (modGNBas.Statics.ClerkDefaults.boolFileDuplicateWarning && fecherFoundation.Strings.Trim(txtFileNumber.Text) != string.Empty)
				{
					clsDRWrapper clsLoad = new clsDRWrapper();
					clsLoad.OpenRecordset("select ID from deaths  where filenumber = '" + txtFileNumber.Text + "' and ID <> " + FCConvert.ToString(lngDeathNumber), "twck0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						if (MessageBox.Show("There is already another death record with this file number" + "\r\n" + "Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
						{
							DontUnload = true;
							return;
						}
					}
				}
				if (lngDeathNumber < 1)
				{
					rs.AddNew();
					blnUpdateFile = true;
				}
				else
				{
					rs.Edit();
					blnUpdateFile = false;
				}
				// ***File Number
				if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != string.Empty && fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("filenumber"))) == string.Empty)
				{
					blnUpdateFile = true;
				}
				rs.Set_Fields("FileNumber", txtFileNumber.Text);
				rs.Set_Fields("LastName", txtLastName.Text);
				rs.Set_Fields("FirstName", txtFirstName.Text);
				rs.Set_Fields("MiddleName", txtMiddleName.Text);
				rs.Set_Fields("Designation", txtDesignation.Text);
				// If mebDateOfDeath.ClipText = vbNullString Then
				// .Fields("DateofDeath") = Null
				// Else
				// .Fields("DateofDeath") = Format(mebDateOfDeath.Text, "mm,dd,yyyy")
				// End If
				if (fecherFoundation.Strings.Trim(mebDateOfDeath.Text) == string.Empty)
				{
					rs.Set_Fields("DateOfDeath", null);
					rs.Set_Fields("DateOfDeathDescription", "");
				}
				else
				{
					if (Conversion.Val(mebDateOfDeath.Text) != 0 && Information.IsDate(mebDateOfDeath.Text) && mebDateOfDeath.Text.Length == 10)
					{
						rs.Set_Fields("DateOfDeathDescription", "");
						// .Fields("DateOfDeath") = Format(mebDateOfDeath.Text, "MM/dd/yyyy")
						rs.Set_Fields("dateofdeath", mebDateOfDeath.Text);
					}
					else
					{
						rs.Set_Fields("DateOfDeath", null);
						rs.Set_Fields("DateOfDeathDescription", fecherFoundation.Strings.Trim(mebDateOfDeath.Text));
					}
				}
				if (Information.IsDate(txtActualDate.Text))
				{
					rs.Set_Fields("ActualDate", fecherFoundation.DateAndTime.DateValue(txtActualDate.Text));
				}
				else
				{
					rs.Set_Fields("ActualDate", null);
				}
				rs.Set_Fields("timeofdeath", txtTimeOfDeath.Text);
				rs.Set_Fields("DateOfBirth", fecherFoundation.Strings.Trim(txtDateofBirth.Text));
				rs.Set_Fields("Signature", chkSignature.CheckState);
				if (cmbCertifiedType.Text == "Medical Examiner")
				{
					rs.Set_Fields("CertifiedType", 0);
				}
				else if (cmbCertifiedType.Text == "Certifying Physician")
				{
					rs.Set_Fields("CertifiedType", 1);
				}
				else if (cmbCertifiedType.Text == "Certified Nurse Practitioner")
				{
					rs.Set_Fields("CertifiedType", 3);
				}
				else if (cmbCertifiedType.Text == "Unknown")
				{
					rs.Set_Fields("certifiedtype", 4);
				}
				else
				{
					rs.Set_Fields("CertifiedType", 2);
				}
				// txtSocialSecurityNumber.Mask = "###-##-####&"
				// If txtSocialSecurityNumber.ClipText = "" Then
				// .Fields("SocialSecurityNumber") = ""
				// Else
				// If Len(txtSocialSecurityNumber.ClipText) > 9 Then
				// .Fields("SocialSecurityNumber") = txtSocialSecurityNumber.ClipText
				// Else
				// .Fields("SocialSecurityNumber") = txtSocialSecurityNumber.ClipText & " "
				// End If
				// End If
				rs.Set_Fields("socialsecuritynumber", txtSocialSecurityNumber.Text.Replace("-", ""));
				rs.Set_Fields("Sex", txtSex.Text);
				rs.Set_Fields("FatherLastName", txtFatherLastName.Text);
				rs.Set_Fields("FatherFirstName", txtFatherFirstName.Text);
				rs.Set_Fields("FatherMiddleName", txtFatherMiddleName.Text);
				rs.Set_Fields("FatherDesignation", txtFatherDesignation.Text);
				rs.Set_Fields("MotherLastName", txtMotherLastName.Text);
				rs.Set_Fields("MotherFirstName", txtMotherFirstName.Text);
				rs.Set_Fields("MotherMiddleName", txtMotherMiddleName.Text);
				rs.Set_Fields("InformantName", txtInformantName.Text);
				rs.Set_Fields("InformantMailingAddress", txtInformantMailingAddress.Text);
				rs.Set_Fields("CauseA", txtCauseA.Text);
				rs.Set_Fields("CauseB", txtCauseB.Text);
				rs.Set_Fields("CauseC", txtCauseC.Text);
				rs.Set_Fields("CauseD", txtCauseD.Text);
				rs.Set_Fields("OnsetA", txtOnsetA.Text);
				rs.Set_Fields("OnsetB", txtOnsetB.Text);
				rs.Set_Fields("OnsetC", txtOnsetC.Text);
				rs.Set_Fields("OnsetD", txtOnsetD.Text);
				rs.Set_Fields("DispositionOther", txtDispositionOther.Text);
				string strTemp = "";
				strTemp = "";
				if (chkDisposition[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					strTemp += "T";
					// .Fields("Disposition") = "T"
				}
				if (chkDisposition[1].CheckState == Wisej.Web.CheckState.Checked)
				{
					// .Fields("Disposition") = "B"
					strTemp += "B";
				}
				if (chkDisposition[2].CheckState == Wisej.Web.CheckState.Checked)
				{
					strTemp += "C";
					// .Fields("Disposition") = "C"
				}
				if (chkDisposition[3].CheckState == Wisej.Web.CheckState.Checked)
				{
					strTemp += "R";
					// .Fields("Disposition") = "R"
				}
				if (chkDisposition[4].CheckState == Wisej.Web.CheckState.Checked)
				{
					strTemp += "U";
					// .Fields("Disposition") = "U"
				}
				if (chkDisposition[5].CheckState == Wisej.Web.CheckState.Checked)
				{
					strTemp += "O";
					// .Fields("Disposition") = "O"
				}
				rs.Set_Fields("disposition", strTemp);
				if (cmbMaritalStatus.Text == "Married")
				{
					rs.Set_Fields("MaritalStatus", "M");
				}
				else if (cmbMaritalStatus.Text == "Never Married")
				{
					rs.Set_Fields("MaritalStatus", "N");
				}
				else if (cmbMaritalStatus.Text == "Widowed")
				{
					rs.Set_Fields("MaritalStatus", "W");
				}
				else if (cmbMaritalStatus.Text == "Divorced")
				{
					rs.Set_Fields("MaritalStatus", "D");
				}
				else if (cmbMaritalStatus.Text == "Other")
				{
					rs.Set_Fields("MaritalStatus", "O");
				}
				else if (cmbMaritalStatus.Text == "Domestic Partner")
				{
					rs.Set_Fields("maritalstatus", "P");
				}
				// If optEmbalmed(0).Value Then
				// .Fields("Embalmed") = True
				// Else
				// .Fields("Embalmed") = False
				// End If
				if (chkBodyEmbalmed.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs.Set_Fields("embalmed", true);
				}
				else
				{
					rs.Set_Fields("embalmed", false);
				}
				rs.Set_Fields("FuneralEstablishmentLicenseNumber", txtFuneralEstablishmentLicenseNumber.Text);
				rs.Set_Fields("PlaceofDisposition", fecherFoundation.Strings.Trim(txtPlaceofDisposition.Text));
				rs.Set_Fields("LicenseeNumber", txtLicenseeNumber.Text);
				rs.Set_Fields("DateofDisposition", mebDateOfDisposition.Text);
				rs.Set_Fields("NameandAddressofFacility", txtNameandAddressofFacility.Text);
				rs.Set_Fields("NameandAddressofCertifier", txtNameandAddressofCertifier.Text);
				rs.Set_Fields("NameofAttendingPhysician", txtNameofAttendingPhysician.Text);
				rs.Set_Fields("NameofPhysician", txtNameofCertifyingPhysician.Text);
				if (cmbViewedBody.Text == "Yes")
				{
					rs.Set_Fields("ViewedBody", -1);
				}
				else if (cmbViewedBody.Text == "No")
				{
					rs.Set_Fields("ViewedBody", 0);
				}
				else
				{
					rs.Set_Fields("ViewedBody", 1);
				}
				rs.Set_Fields("DateSigned", fecherFoundation.Strings.Trim(txtDateSigned.Text));
				rs.Set_Fields("PlaceOfDeathOther", txtPlaceofDeathOther.Text);
				if (cmbPlaceofDeath.Text == "DOA")
					rs.Set_Fields("placeofdeath", "0");
				if (cmbPlaceofDeath.Text == "Inpatient")
					rs.Set_Fields("placeofdeath", "1");
				if (cmbPlaceofDeath.Text == "ER/Outpatient")
					rs.Set_Fields("placeofdeath", "2");
				if (cmbPlaceofDeath.Text == "Nursing Home")
					rs.Set_Fields("placeofdeath", "3");
				if (cmbPlaceofDeath.Text == "Residence")
					rs.Set_Fields("placeofdeath", "4");
				if (cmbPlaceofDeath.Text == "Other")
					rs.Set_Fields("placeofdeath", "5");
				rs.Set_Fields("EducationElementary", txtEducationElementary.Text);
				rs.Set_Fields("EducationCollege", txtEducationCollege.Text);
				if (cmbMostRecentSpouse.Text == "Living")
				{
					rs.Set_Fields("MostRecentSpouse", "L");
				}
				else if (cmbMostRecentSpouse.Text == "Deceased")
				{
					rs.Set_Fields("MostRecentSpouse", "D");
				}
				else
				{
					rs.Set_Fields("MostRecentSpouse", "N");
				}
				if (txtMostRecentSpouseName.Text != "")
					rs.Set_Fields("MostRecentSpouseName", txtMostRecentSpouseName.Text);
				if (cmbMilitarySrv.Text == "Yes")
				{
					rs.Set_Fields("USArmedServices", true);
				}
				else
				{
					rs.Set_Fields("USArmedServices", false);
				}
				rs.Set_Fields("ResidenceStreet", txtResidenceStreet.Text);
				rs.Set_Fields("Race", txtRace.Text);
				rs.Set_Fields("Ancestry", txtAncestry.Text);
				rs.Set_Fields("KindofBusiness", txtKindofBusiness.Text);
				rs.Set_Fields("CityorTownofDeath", fecherFoundation.Strings.Trim(txtCityOrTownOfDeath.Text));
				rs.Set_Fields("CountyofDeath", fecherFoundation.Strings.Trim(txtCountyOfDeath.Text));
				rs.Set_Fields("ResidenceCounty", fecherFoundation.Strings.Trim(txtResidenceCounty.Text));
				rs.Set_Fields("ResidenceState", fecherFoundation.Strings.Trim(txtResidenceState.Text));
				rs.Set_Fields("PlaceOfDispositionState", fecherFoundation.Strings.Trim(txtDispositionState.Text));
				rs.Set_Fields("ResidenceCityorTown", fecherFoundation.Strings.Trim(txtResidenceCity.Text));
				rs.Set_Fields("PlaceOfDispositionCity", fecherFoundation.Strings.Trim(txtDispositionCity.Text));
				rs.Set_Fields("CausesOther", fecherFoundation.Strings.Trim(txtCausesOther.Text));
				rs.Set_Fields("Occupation", txtOccupation.Text);
				rs.Set_Fields("BirthPlace", txtBirthplace.Text);
				if (!(txtAgeDays.Text == ""))
					rs.Set_Fields("AgeDays", txtAgeDays.Text);
				if (!(txtAgeMonths.Text == ""))
					rs.Set_Fields("AgeMonths", txtAgeMonths.Text);
				if (!(txtAgeMinutes.Text == ""))
					rs.Set_Fields("AgeMinutes", txtAgeMinutes.Text);
				if (!(txtAgeHours.Text == ""))
					rs.Set_Fields("AgeHours", txtAgeHours.Text);
				if (!(txtAge.Text == ""))
					rs.Set_Fields("Age", txtAge.Text);
				rs.Set_Fields("FacilityName", txtFacilityName.Text);
				rs.Set_Fields("ClerkofRecordName", txtNameofClerk.Text + "");
				rs.Set_Fields("ClerkofRecordCity", txtCityofClerk.Text + "");
				rs.Set_Fields("DateOriginalFiling", txtDateFiled.Text + "");
				rs.Set_Fields("NoRequiredFields", chkRequired.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("Amendment", chkAmendment.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("Correction", chkCorrection.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("Supplemental", chkSupplemental.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("FetalDeath", chkFetalDeath.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("MarriageOnFile", chkMarriageOnFile.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("BirthOnFile", chkBirthOnFile.CheckState == Wisej.Web.CheckState.Checked);
				rs.Set_Fields("towncode", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				modCashReceiptingData.Statics.typeCRData.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				rs.Set_Fields("mannerofdeath", cmbMannerOfDeath.SelectedIndex);
				if (chkAutopsyAvailablePrior.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs.Set_Fields("autopsyavailableprior", true);
				}
				else
				{
					rs.Set_Fields("autopsyavailableprior", false);
				}
				if (chkAutopsyPerformed.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs.Set_Fields("autopsyperformed", true);
				}
				else
				{
					rs.Set_Fields("autopsyperformed", false);
				}
				rs.Update();
				lngDeathNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				if (fecherFoundation.Strings.Trim(txtFileNumber.Text) != "" && blnUpdateFile)
				{
					rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
					if (rsClerkInfo.BeginningOfFile() != true && rsClerkInfo.EndOfFile() != true)
					{
						rsClerkInfo.Edit();
					}
					else
					{
						rsClerkInfo.AddNew();
					}
					rsClerkInfo.Set_Fields("LastDeathFileNumber", fecherFoundation.Strings.Trim(txtFileNumber.Text));
					rsClerkInfo.Update();
					ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + fecherFoundation.Strings.Trim(txtFileNumber.Text));
					ToolTip1.SetToolTip(lblFile, ToolTip1.GetToolTip(txtFileNumber));
				}
				modAmendments.SaveAmendments(ref VSFlexGrid1, modGNBas.Statics.gintDeathNumber, "DEATHS");
				DontUnload = false;
				intDataChanged = 0;
				modDirtyForm.ClearDirtyControls(this);
				mnuViewDocuments.Enabled = true;
				if (boolShow)
					MessageBox.Show("Update Completed Successfully.", "Update Complete:", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// Handler:
				MessageBox.Show("Unable to Complete the Record Update:  Error Number - " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				return;
				{
					/*? Resume Next; */}
			}
		}

		private bool ValidateData()
		{
			bool ValidateData = false;
			ValidateData = true;
			GoodValidation = false;
			// FirstName
			if (txtFirstName.Text == "")
			{
				ValidateData = false;
				MessageBox.Show("Please enter a valid First Name", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtFirstName.Focus();
				return ValidateData;
			}
			// LastName
			if (txtLastName.Text == "")
			{
				ValidateData = false;
				MessageBox.Show("Please enter a valid Last Name", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtLastName.Focus();
				ss.SelectedIndex = 0;
				return ValidateData;
			}
			if (chkRequired.CheckState == CheckState.Unchecked)
			{
				// MiddleName
				if (txtMiddleName.Text == "")
				{
					txtMiddleName.Text = "";
				}
				// Designation
				// If txtDesignation.Text = "" Then ValidateData = False
				// DateofBirth
				if (fecherFoundation.Strings.Trim(txtDateofBirth.Text) == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Date of Birth", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtDateofBirth.Focus();
					ss.SelectedIndex = 0;
					return ValidateData;
				}
				// DateofDeath
				// If mebDateOfDeath.ClipText = "" Or Not IsDate(mebDateOfDeath) Then
				if (fecherFoundation.Strings.Trim(mebDateOfDeath.Text) == string.Empty)
				{
					ValidateData = false;
					MessageBox.Show("Please enter a Date of Death", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					mebDateOfDeath.Focus();
					ss.SelectedIndex = 0;
					return ValidateData;
				}
				// If CompareDates(StripDateSlashes(txtDateofBirth), StripDateSlashes(mebDateOfDeath), ">") Then
				// ValidateData = False
				// MsgBox "Date of Birth must be before Date of Death.", vbInformation + vbOKOnly, "Data Error"
				// txtDateofBirth.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// Sex
				if (txtSex.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Decedant's Sex", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtSex.Focus();
					ss.SelectedIndex = 0;
					return ValidateData;
				}
				// SocialSecurityNumber
				// If txtSocialSecurityNumber.ClipText = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Social Security Number", vbOKOnly, "Data Error"
				// txtSocialSecurityNumber.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// Birthplace
				if (txtBirthplace.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Birth Place", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtBirthplace.Focus();
					ss.SelectedIndex = 0;
					return ValidateData;
				}
				// AgeDays
				// If txtAgeDays.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Age in Days", vbOKOnly, "Data Error"
				// txtAgeDays.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// AgeMonths
				// If txtAgeMonths.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Age in Months", vbOKOnly, "Data Error"
				// txtAgeDays.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// AgeMinutes
				// If txtAgeMinutes.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Age in Minutes", vbOKOnly, "Data Error"
				// txtAgeMinutes.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// AgeHours
				// If txtAgeHours.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Age in Hours", vbOKOnly, "Data Error"
				// txtAgeHours.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// 
				// Age
				// If txtAge.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Age", vbOKOnly, "Data Error"
				// txtAge.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// PlaceofDeathOther
				if (cmbPlaceofDeath.Text == "Other")
				{
					if (txtPlaceofDeathOther.Text == "")
					{
						ValidateData = false;
						MessageBox.Show("Please enter a valid Place of Death Other", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPlaceofDeathOther.Focus();
						ss.SelectedIndex = 0;
						return ValidateData;
					}
				}
				// FacilityName
				// If txtFacilityName.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Facility Name", vbOKOnly, "Data Error"
				// txtFacilityName.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// CountyofDeath
				// If txtCountyofDeath.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid County of Death", vbOKOnly, "Data Error"
				// txtCountyofDeath.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// Marital Status
				// If optMaritalStatus(0).Value = True Then
				// .Fields("MaritalStatus = "M"
				// ElseIf optMaritalStatus(1).Value = True Then
				// .Fields("MaritalStatus = "N"
				// ElseIf optMaritalStatus(2).Value = True Then
				// .Fields("MaritalStatus = "W"
				// ElseIf optMaritalStatus(3).Value = True Then
				// .Fields("MaritalStatus = "D"
				// Else
				// ValidateData = False
				// MsgBox "Nothing Checked for Marital Status:", vbOKOnly, "Marital Status:"
				// optMaritalStatus(0).SetFocus
				// ss.Tab = 0
				// End If
				// EducationElementary
				// If txtEducationElementary.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Education Elementary", vbOKOnly, "Data Error"
				// txtEducationElementary.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// EducationCollege
				// If txtEducationCollege.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Education College", vbOKOnly, "Data Error"
				// txtEducationCollege.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// MostRecentSpouseName
				// If optMaritalStatus(1).Value <> True Then
				// If txtMostRecentSpouseName.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Most Recent Spouse Name", vbOKOnly, "Data Error"
				// txtMostRecentSpouseName.SetFocus
				// ss.Tab = 0
				// Exit Function
				// End If
				// End If
				// ResidenceSate
				if (txtResidenceState.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid State of Residence", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtResidenceState.Focus();
					ss.SelectedIndex = 0;
					return ValidateData;
				}
				// ResidenceCounty
				if (txtResidenceCounty.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid County of Residence", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtResidenceCounty.Focus();
					ss.SelectedIndex = 0;
					return ValidateData;
				}
				// ResidenceCityorTown
				if (txtResidenceCity.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid City or Town of Residence", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtResidenceCity.Focus();
					ss.SelectedIndex = 0;
					return ValidateData;
				}
				// ResidenceStreet&Number
				if (txtResidenceStreet.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Number and Street of Residence", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtResidenceStreet.Focus();
					ss.SelectedIndex = 0;
					return ValidateData;
				}
				// ********************************************************************
				// ********************************************************************
				// **** Tab 2 / Parents / Informant / Disposition / Certifier  ********
				// ********************************************************************
				// ********************************************************************
				// FatherFirstName
				if (txtFatherFirstName.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Father's First Name", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ss.SelectedIndex = 1;
					txtFatherFirstName.Focus();
					return ValidateData;
				}
				// FatherLastName
				if (txtFatherLastName.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Father's Last Name", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtFatherLastName.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// FatherMiddleName
				// If txtFatherMiddleName.Text = "" Then
				// txtFatherMiddleName = "N/A"
				// End If
				// If txtFatherMiddleName.Text = "" Then ValidateData = False
				// FatherDesignation
				// If txtFatherDesignation.Text = "" Then ValidateData = False
				// MotherFirstName
				if (txtMotherFirstName.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Mother's First Name", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtMotherFirstName.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// MotherLastName
				if (txtMotherLastName.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Mother's Last Name", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtMotherLastName.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// MotherMiddleName
				// If txtMotherMiddleName.Text = "" Then
				// txtMotherMiddleName = "N/A"
				// End If
				// If txtMotherMiddleName.Text = "" Then ValidateData = False
				// InformantName
				if (txtInformantName.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Informant's Name", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtInformantName.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// InformantMailingAddress
				if (txtInformantMailingAddress.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Informant's Mailing Address", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtInformantMailingAddress.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// DispositinOther
				if (chkDisposition[5].CheckState == Wisej.Web.CheckState.Checked && txtDispositionOther.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Disposition Other:", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtDispositionOther.Focus();
					return ValidateData;
				}
				// PlaceofDisposition
				if (fecherFoundation.Strings.Trim(txtPlaceofDisposition.Text) == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a Cemetery or Crematory Name", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ss.SelectedIndex = 1;
					txtPlaceofDisposition.Focus();
					return ValidateData;
				}
				// CityofDisposition
				if (txtDispositionCity.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid City of Disposition", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ss.SelectedIndex = 1;
					txtDispositionCity.Focus();
					return ValidateData;
				}
				// StateofDisposition
				if (txtDispositionState.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid State of Disposition", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ss.SelectedIndex = 1;
					txtDispositionState.Focus();
					return ValidateData;
				}
				// DateofDisposition
				// If mebDateOfDisposition.ClipText = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Date of Disposition", vbOKOnly, "Data Error"
				// mebDateOfDisposition.SetFocus
				// ss.Tab = 1
				// Exit Function
				// End If
				// If Trim(mebDateOfDisposition.Text) = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Date of Disposition", vbOKOnly, "Data Error"
				// mebDateOfDisposition.SetFocus
				// ss.Tab = 1
				// Exit Function
				// End If
				// LicenseeNumber
				if (txtLicenseeNumber.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Licensee Number", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLicenseeNumber.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// FuneralEstablishmentLicenseNumber
				if (txtFuneralEstablishmentLicenseNumber.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Funeral Establishment License Number", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtFuneralEstablishmentLicenseNumber.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// NameandAddressofFacility
				if (txtNameandAddressofFacility.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Address of Facility", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtNameandAddressofFacility.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// PHYSICIAN
				// If optCertifiedType(0) Then
				// ONLY CHECK IF THE OPTION FOR MEDICAL PHYSICIAN IS SELECTED
				// IF not THEN THIS IS NOT A REQUIRED FIELD SO THERE IS
				// NO REASON FOR US TO VALIDATE THIS FIELD
				// If txtNameofAttendingPhysician = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Physician's Name", vbOKOnly, "Data Error"
				// txtNameofPhysician.SetFocus
				// ss.Tab = 1
				// Exit Function
				// End If
				// End If
				// DateSigned
				if (fecherFoundation.Strings.Trim(txtDateSigned.Text) == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Date Signed", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ss.SelectedIndex = 1;
					txtDateSigned.Focus();
					txtDateSigned.SelectionStart = 0;
					txtDateSigned.SelectionLength = txtDateSigned.Text.Length;
					return ValidateData;
				}
				// NameandAddressofCertifier
				if (txtNameandAddressofCertifier.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Name and Address", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtNameandAddressofCertifier.Focus();
					ss.SelectedIndex = 1;
					return ValidateData;
				}
				// NameofPhysician
				// If txtNameofPhysician.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Name of Physician", vbOKOnly, "Data Error"
				// txtNameofPhysician.SetFocus
				// ss.Tab = 1
				// Exit Function
				// End If
				// MaritalStatus
				// USArmedServices
				// ResidenceStreet
				// If txtResidenceStreet.Text = "" Then ValidateData = False
				// ResidenceCityorTown
				// If txtResidenceCityorTown.Text = "" Then ValidateData = False
				// ResidenceState
				// If txtResidenceState.Text = "" Then ValidateData = False
				// Race
				// If txtRace.Text = "" Then ValidateData = False
				// Ancestry
				// If txtAncestry.Text = "" Then ValidateData = False
				// KindofBusiness
				// If txtKindofBusiness.Text = "" Then ValidateData = False
				// ********************************************************************
				// ********************************************************************
				// ************* Tab 3 / Clerk / Confidential Information *************
				// ********************************************************************
				// ********************************************************************
				// CauseA
				if (txtCauseA.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Cause of Death", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtCauseA.Focus();
					ss.SelectedIndex = 2;
					return ValidateData;
				}
				// OnsetA
				// If txtOnsetA.Text = "" Then
				// ValidateData = False
				// MsgBox "Please enter a valid Cause of Death", vbOKOnly, "Data Error"
				// txtOnsetA.SetFocus
				// ss.Tab = 2
				// Exit Function
				// End If
				// CauseB
				// If txtCauseB.Text = "" Then txtCauseB.Text = "N/A"
				// CauseC
				// If txtCauseC.Text = "" Then txtCauseC.Text = "N/A"
				// CauseD
				// If txtCauseD.Text = "" Then txtCauseD.Text = "N/A"
				// OnsetA
				// If txtOnsetA.Text = "" Then txtCauseA.Text = "N/A"
				// OnsetB
				// If txtOnsetB.Text = "" Then txtCauseB.Text = "N/A"
				// OnsetC
				// If txtOnsetC.Text = "" Then txtCauseC.Text = "N/A"
				// OnsetD
				// If txtOnsetD.Text = "" Then txtCauseD.Text = "N/A"
				if (txtNameofClerk.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Clerk Recording Death", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ss.SelectedIndex = 2;
					txtNameofClerk.Focus();
					return ValidateData;
				}
				if (txtCityofClerk.Text == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid City for the Clerk Recording Death", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ss.SelectedIndex = 2;
					txtCityofClerk.Focus();
					return ValidateData;
				}
				if (fecherFoundation.Strings.Trim(txtDateFiled.Text) == "")
				{
					ValidateData = false;
					MessageBox.Show("Please enter a valid Date for the Original Filing", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ss.SelectedIndex = 2;
					txtDateFiled.Focus();
					return ValidateData;
				}
				GoodValidation = true;
			}
			else
			{
				ValidateData = true;
				GoodValidation = true;
			}
			return ValidateData;
		}

		private void mnuRegistrarNames_Click(object sender, System.EventArgs e)
		{
			frmRegistrarNames.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		public void mnuSave_Click(object sender, System.EventArgs e)
		{
			object Ans;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			string varSavedAlready = "";
			SaveData();
			if (GoodValidation == false)
				return;
			boolDataChanged = false;
			// If Not DontUnload Then
			// If Adding = True Then
			// Ans = MsgBox("Would you like to add another Death Certificate?", vbYesNo)
			// If Ans = vbYes Then
			// Call ClearBoxes
			// Adding = True
			// txtFirstName.SetFocus
			// Else
			// Adding = False
			// Unload Me
			// End If
			// End If
			// End If
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			if (!DontUnload)
			{
				mnuQuit_Click();
			}
		}

		private void mnuSaveExitNoValidation_Click(object sender, System.EventArgs e)
		{
			SaveData(true, false);
			if (!DontUnload)
			{
				mnuQuit_Click();
			}
		}

		private void optCertifiedType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL BE EITHER A CERTIFIED PHYSICIAN OR A MEDICAL EXAMINER
			if (Index == 0)
			{
				MessageBox.Show("This record cannot be printed because it was Certified by a Medical Examiner. MUST COPY ORIGINAL DOCUMENT", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void optCertifiedType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbCertifiedType.SelectedIndex;
			optCertifiedType_CheckedChanged(index, sender, e);
		}

		private void optDisposition_Click(ref int Index)
		{
			intDataChanged += 1;
			txtDispositionOther.TabStop = Index == 5;
		}

		private void optEmbalmed_Click(ref int Index)
		{
			intDataChanged += 1;
		}

		private void optMaritalStatus_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (cmbMaritalStatus.Text == "Never Married" || cmbMaritalStatus.Text == "Other")
			{
				Frame8.Enabled = false;
				this.txtMostRecentSpouseName.Text = "N/A";
				cmbMostRecentSpouse.Text = "";
			}
			else if (cmbMaritalStatus.Text == "Widowed")
			{
				Frame8.Enabled = true;
				cmbMostRecentSpouse.Text = "Deceased";
			}
			else
			{
				Frame8.Enabled = true;
				cmbMostRecentSpouse.Text = "Living";
			}
			intDataChanged += 1;
		}

		private void optMaritalStatus_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbMaritalStatus.SelectedIndex;
			optMaritalStatus_CheckedChanged(index, sender, e);
		}

		private void optMilitarySrv_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void optMilitarySrv_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbMilitarySrv.SelectedIndex;
			optMilitarySrv_CheckedChanged(index, sender, e);
		}

		private void optMostRecentSpouse_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void optMostRecentSpouse_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbMostRecentSpouse.SelectedIndex;
			optMostRecentSpouse_CheckedChanged(index, sender, e);
		}

		private void optPlaceofDeath_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void optPlaceofDeath_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbPlaceofDeath.SelectedIndex;
			optPlaceofDeath_CheckedChanged(index, sender, e);
		}

		private void optViewedBody_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			intDataChanged += 1;
		}

		private void optViewedBody_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbViewedBody.SelectedIndex;
			optViewedBody_CheckedChanged(index, sender, e);
		}

		private void ss_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (chkRequired.CheckState == CheckState.Unchecked)
			{
				if (ss.PreviousTab == 0)
				{
					// DateofDeath
					if (mebDateOfDeath.Text == "")
					{
						MessageBox.Show("Please enter a Date of Death", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						mebDateOfDeath.Focus();
						ss.SelectedIndex = 0;
						return;
					}
					// Sex
					if (txtSex.Text == "")
					{
						MessageBox.Show("Please enter a valid Decedant's Sex", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtSex.Focus();
						ss.SelectedIndex = 0;
						return;
					}
				}
			}
		}

		private void txtAge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
				KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCauseA_Enter(object sender, System.EventArgs e)
		{
			txtCauseA.SelectionStart = 0;
			txtCauseA.SelectionLength = txtCauseA.Text.Length;
		}

		private void txtCauseB_Enter(object sender, System.EventArgs e)
		{
			txtCauseB.SelectionStart = 0;
			txtCauseB.SelectionLength = txtCauseB.Text.Length;
		}

		private void txtCauseC_Enter(object sender, System.EventArgs e)
		{
			txtCauseC.SelectionStart = 0;
			txtCauseC.SelectionLength = txtCauseC.Text.Length;
		}

		private void txtCauseD_Enter(object sender, System.EventArgs e)
		{
			txtCauseD.SelectionStart = 0;
			txtCauseD.SelectionLength = txtCauseD.Text.Length;
		}

		private void txtDateSigned_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strD = "";
			if (fecherFoundation.Strings.Trim(txtDateSigned.Text).Length == 8)
			{
				if (Information.IsNumeric(fecherFoundation.Strings.Trim(txtDateSigned.Text)))
				{
					strD = fecherFoundation.Strings.Trim(txtDateSigned.Text);
					strD = Strings.Mid(strD, 1, 2) + "/" + Strings.Mid(strD, 3, 2) + "/" + Strings.Mid(strD, 5);
					if (Information.IsDate(strD))
					{
						txtDateSigned.Text = strD;
					}
				}
			}
		}

		private void txtDispositionCity_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtDispositionCity.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtDispositionState_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtDispositionState.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtFuneralEstablishmentLicenseNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtFuneralEstablishmentLicenseNumber.Text) != string.Empty)
			{
				if (MessageBox.Show("Use default information for Funeral Establishment #" + fecherFoundation.Strings.Trim(txtFuneralEstablishmentLicenseNumber.Text) + "?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsFacility = new clsDRWrapper();
					rsFacility.OpenRecordset("Select * from FacilityNames where FacilityNumber = '" + fecherFoundation.Strings.Trim(txtFuneralEstablishmentLicenseNumber.Text) + "'", modGNBas.DEFAULTDATABASE);
					if (!rsFacility.EndOfFile())
					{
						txtNameandAddressofFacility.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFacility.Get_Fields_String("LastName"))) + "\r\n";
						if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address1") + " ") != string.Empty)
						{
							txtNameandAddressofFacility.Text = txtNameandAddressofFacility.Text + fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address1") + " ") + "\r\n";
						}
						if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address2") + " ") != string.Empty)
						{
							txtNameandAddressofFacility.Text = txtNameandAddressofFacility.Text + fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Address2") + " ") + "\r\n";
						}
						if (fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("City") + " ") != string.Empty)
						{
							txtNameandAddressofFacility.Text = txtNameandAddressofFacility.Text + fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("City") + " ") + ", " + fecherFoundation.Strings.Trim(rsFacility.Get_Fields("State") + " ") + " " + fecherFoundation.Strings.Trim(rsFacility.Get_Fields_String("Zip") + " ");
						}
					}
				}
			}
		}

		private void txtNameandAddressofCertifier_Leave(object sender, System.EventArgs e)
		{
			ss.SelectedIndex = 2;
		}

		private void txtOnsetA_Enter(object sender, System.EventArgs e)
		{
			txtOnsetA.SelectionStart = 0;
			txtOnsetA.SelectionLength = txtOnsetA.Text.Length;
		}

		private void txtOnsetB_Enter(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtCauseB.Text) == "N/A" || fecherFoundation.Strings.Trim(txtCauseB.Text) == "")
			{
				txtCausesOther.Focus();
			}
			txtOnsetB.SelectionStart = 0;
			txtOnsetB.SelectionLength = txtOnsetB.Text.Length;
		}

		private void txtOnsetC_Enter(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtCauseC.Text) == "N/A" || fecherFoundation.Strings.Trim(txtCauseC.Text) == "")
			{
				txtCausesOther.Focus();
			}
			txtOnsetC.SelectionStart = 0;
			txtOnsetC.SelectionLength = txtOnsetC.Text.Length;
		}

		private void txtOnsetD_Enter(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtCauseD.Text) == "N/A" || fecherFoundation.Strings.Trim(txtCauseD.Text) == "")
			{
				txtCausesOther.Focus();
			}
			txtOnsetD.SelectionStart = 0;
			txtOnsetD.SelectionLength = txtOnsetD.Text.Length;
		}

		private void txtPlaceofDeathOther_Enter(object sender, System.EventArgs e)
		{
			if (cmbPlaceofDeath.Text != "Other")
			{
                //FC:FINAL:AM:#2408 - focus directly the next control
                //Support.SendKeys("{TAB}", false);
                this.txtFacilityName.Focus();
			}
		}

		private void txtResidenceCity_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "T";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtResidenceCity.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtResidenceCounty_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "C";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtResidenceCounty.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtResidenceState_DoubleClick(object sender, System.EventArgs e)
		{
			frmSelectTownStateCounty.InstancePtr.strType = "S";
			frmSelectTownStateCounty.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			txtResidenceState.Text = modClerkGeneral.Statics.strReturnString;
		}

		private void txtResidenceStreet_Leave(object sender, System.EventArgs e)
		{
			ss.SelectedIndex = 1;
		}
		// vbPorter upgrade warning: lngDNumber As int	OnWrite(int, double)
		public void Init(int lngDNumber)
		{
			lngDeathNumber = lngDNumber;
			this.Show(App.MainForm);
		}

		private void frmDeaths_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDeaths properties;
			//frmDeaths.ScaleWidth	= 9045;
			//frmDeaths.ScaleHeight	= 7740;
			//frmDeaths.LinkTopic	= "Form1";
			//frmDeaths.LockControls	= -1  'True;
			//End Unmaped Properties
            lngDeathNumber = ViewModel.VitalId;
			clsDRWrapper rsClerkInfo = new clsDRWrapper();
			/* Control ctl = new Control(); */
			foreach (Control ctl in this.GetAllControls())
			{
				if (FCConvert.ToString(ctl.Tag) == "Required")
				{
					ctl.BackColor = ColorTranslator.FromOle(StaticSettings.TrioColorSettings.TRIOCOLORHIGHLIGHT);
				}
			}
			// ctl
			boolFormLoading = true;
			// lngDeathNumber = 0
			// lngDeathNumber = Val(Response)
			//modGNBas.Statics.boolDeathOpen = true;
			SetupGridTownCode();
			SetupcmbMannerofDeath();

			ss.SelectedIndex = 0;
			if (modClerkGeneral.Statics.AddingDeathCertificate == true)
			{
				ClearBoxes();
				modClerkGeneral.Statics.AddingDeathCertificate = false;
				Adding = true;
				modAmendments.LoadAmendments(ref VSFlexGrid1, -1, "DEATHS");
			}
			else
			{
				Adding = false;
				ClearBoxes();
				FillBoxes();
				modAmendments.LoadAmendments(ref VSFlexGrid1, lngDeathNumber, "DEATHS");
				// End If
			}
			rsClerkInfo.OpenRecordset("SELECT * FROM ClerkDefaults", "TWCK0000.vb1");
			if (rsClerkInfo.EndOfFile() != true && rsClerkInfo.BeginningOfFile() != true)
			{
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: " + rsClerkInfo.Get_Fields_String("LastDeathFileNumber"));
			}
			else
			{
				ToolTip1.SetToolTip(txtFileNumber, "Last number issued is: UNKNOWN");
			}
			ToolTip1.SetToolTip(lblFile, ToolTip1.GetToolTip(txtFileNumber));
			if (lngDeathNumber > 0)
			{
				mnuViewDocuments.Enabled = true;
			}
			else
			{
				mnuViewDocuments.Enabled = false;
			}
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this, false);
			boolFormLoading = false;
		}



        private void txtSex_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtSex.Modified)
            {
                string text = txtSex.Text;
                if (text != "M" && text != "F")
                {
                    MessageBox.Show("Must enter 'M' or 'F'.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
        }

        private void txtSocialSecurityNumber_TextChanged(object sender, System.EventArgs e)
		{
			string strTemp;
			string strSSN;
			intDataChanged += 1;
			strTemp = txtSocialSecurityNumber.Text;
			strSSN = "";
			strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
			if (strTemp.Length > 3)
			{
				strSSN = Strings.Mid(strTemp, 1, 3) + "-";
				if (strTemp.Length > 5)
				{
					strSSN += Strings.Mid(strTemp, 4, 2) + "-";
					strSSN += Strings.Mid(strTemp, 6);
				}
				else
				{
					strSSN += Strings.Mid(strTemp, 4);
				}
			}
			else
			{
				strSSN = strTemp;
			}
			txtSocialSecurityNumber.Text = strSSN;
			txtSocialSecurityNumber.SelectionStart = strSSN.Length + 1;
			txtSocialSecurityNumber.SelectionLength = 0;
		}

		private void txtSocialSecurityNumber_Enter(object sender, System.EventArgs e)
		{
			txtSocialSecurityNumber.SelectionStart = 0;
			txtSocialSecurityNumber.SelectionLength = txtSocialSecurityNumber.Text.Length;
		}

		private void VSFlexGrid1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void VSFlexGrid1_Enter(object sender, System.EventArgs e)
		{
			ss.SelectedIndex = 3;
		}

		private void VSFlexGrid1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == (Keys)46)
			{
				if (MessageBox.Show("Delete current record", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					VSFlexGrid1.RemoveItem(VSFlexGrid1.Row);
					VSFlexGrid1.AddItem(string.Empty);
				}
			}
		}

		private void VSFlexGrid1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = VSFlexGrid1[e.ColumnIndex, e.RowIndex];
            int lngRow;
			// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
			int intCol;
			lngRow = VSFlexGrid1.GetFlexRowIndex(e.RowIndex);
			intCol = VSFlexGrid1.GetFlexColIndex(e.ColumnIndex);
			if (intCol == 2 || lngRow < 1)
			{
                //ToolTip1.SetToolTip(VSFlexGrid1, "");
                cell.ToolTipText = "";
			}
			else
			{
				//ToolTip1.SetToolTip(VSFlexGrid1, FCConvert.ToString(VSFlexGrid1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, intCol)));
				cell.ToolTipText = FCConvert.ToString(VSFlexGrid1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, intCol));
			}
		}

		private void ResizeVsFlexGrid1()
		{
			int GridWidth = 0;
			GridWidth = VSFlexGrid1.WidthOriginal;
			VSFlexGrid1.ColWidth(1, FCConvert.ToInt32(0.26 * GridWidth));
			VSFlexGrid1.ColWidth(2, FCConvert.ToInt32(0.08 * GridWidth));
			VSFlexGrid1.ColWidth(3, FCConvert.ToInt32(0.21 * GridWidth));
			VSFlexGrid1.ColWidth(4, FCConvert.ToInt32(0.15 * GridWidth));
			VSFlexGrid1.ColWidth(5, FCConvert.ToInt32(0.15 * GridWidth));
		}

        public IDeathViewModel ViewModel { get; set; }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }
    }
}
