//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptNewMonthlyDogReportLaser.
	/// </summary>
	public partial class rptNewMonthlyDogReportLaser : BaseSectionReport
	{
		public rptNewMonthlyDogReportLaser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Monthly Dog Report Laser";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNewMonthlyDogReportLaser InstancePtr
		{
			get
			{
				return (rptNewMonthlyDogReportLaser)Sys.GetInstance(typeof(rptNewMonthlyDogReportLaser));
			}
		}

		protected rptNewMonthlyDogReportLaser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
				clsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
	
		object strTemp;
		int intDog1;
		int intDog2;
		int intKennel1;
		int intKennel2;
		int intNeuter1;
		int intNeuter2;
		int intCounter;
		int intTotal;
		int intCase;
		clsDRWrapper rs = new clsDRWrapper();
		double dblKennelLic;
		double dblStateKennelLic;
		// vbPorter upgrade warning: dblFixedLic As double	OnWriteFCConvert.ToDecimal(
		double dblFixedLic;
		double dblStateFixedLic;
		// vbPorter upgrade warning: dblUnfixedLic As double	OnWriteFCConvert.ToDecimal(
		double dblUnfixedLic;
		double dblStateUnfixedLic;
		int lngYearToUse;
		clsDRWrapper clsTemp = new clsDRWrapper();
		int lngTowncode;
		int lngEndingBalance;
		int lngLastEndingBalance;
		int lngAddedSinceLastTime;
		int lngTotalSticksIssued;
		bool boolShowRange;
		int[,] lngAryIssued = new int[100 + 1, 2 + 1];
		bool boolShowOptional;
		// vbPorter upgrade warning: lngMF1 As object	OnWrite(int, double)
		int lngMF1;
		int lngMF2;
        private double stateDangerousFee = 0;
        private double stateNuisanceFee = 0;
        private int dangerous1 = 0;
        private int dangerous2 = 0;
        private int nuisance1 = 0;
        private int nuisance2 = 0;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolShowOptional = false;
				rs.OpenRecordset("select * from printersettings", "twck0000.vb1");
				boolShowOptional = FCConvert.ToBoolean(rs.Get_Fields_Boolean("PrintMonthlyDogOptionalSection"));
				//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
				int x;
				int intCurRange;
				string strSQL = "";
				for (x = 0; x <= 2; x++)
				{
					lngAryIssued[x, 0] = 0;
					lngAryIssued[x, 1] = 0;
					lngAryIssued[x, 2] = 0;
				}
				// x
				intCurRange = 0;
				lngEndingBalance = 0;
				lngLastEndingBalance = 0;
				lngTotalSticksIssued = 0;
				lngAddedSinceLastTime = 0;
				boolShowRange = false;
				if (modGNBas.Statics.boolRegionalTown)
				{
					lngTowncode = FCConvert.ToInt32(Math.Round(Conversion.Val(frmMonthlyDogReport.InstancePtr.gridTownCode.TextMatrix(0, 0))));
				}
				else
				{
					lngTowncode = 0;
				}
				if (modGNBas.Statics.gstrMonthYear == string.Empty)
				{
					modGNBas.Statics.gstrMonthYear = FCConvert.ToString(DateTime.Today.Month) + "/" + FCConvert.ToString(DateTime.Today.Year);
				}
				modDogs.GetDogFees();
				// Call RS.OpenRecordset("select * from settingsfees", "twck0000.vb1")
				dblFixedLic = FCConvert.ToDouble(modDogs.Statics.DogFee.Fixed_Fee);
				dblUnfixedLic = FCConvert.ToDouble(modDogs.Statics.DogFee.UN_Fixed_FEE);
				dblKennelLic = modDogs.Statics.DogFee.KennelClerk + modDogs.Statics.DogFee.KennelState + modDogs.Statics.DogFee.KennelTown;
				dblStateKennelLic = modDogs.Statics.DogFee.KennelState;
				dblStateUnfixedLic = modDogs.Statics.DogFee.DogState;
				dblStateFixedLic = modDogs.Statics.DogFee.FixedState;
                stateNuisanceFee = modDogs.Statics.DogFee.NuisanceStateFee;
                stateDangerousFee = modDogs.Statics.DogFee.DangerousStateFee;

				Label26.Text = "   X    $   " + Strings.Format(dblStateUnfixedLic, "0.00") + "    =    $";
				Label27.Text = "   X    $   " + Strings.Format(dblStateFixedLic, "0.00") + "    =    $";
				Label28.Text =         "   X    $   " + Strings.Format(dblStateKennelLic, "0.00") + "    =    $";
                lblDangerousFee.Text = "   X    $   " + stateDangerousFee.FormatAsMoney() + "    =    $";
                lblNuisanceFee.Text = "   X     $  " + stateNuisanceFee.FormatAsMoney() + "    =    $";
                strTemp = Strings.Split(modGNBas.Statics.gstrMonthYear, "/", -1, CompareConstants.vbBinaryCompare);

				if ((Conversion.Val(((object[])strTemp)[1]) + 1 > DateTime.Today.Year) || (Conversion.Val(((object[])strTemp)[0]) > DateTime.Today.Month))
				{
					lngYearToUse = FCConvert.ToInt32(((object[])strTemp)[1]);
				}
				else
				{
					lngYearToUse = FCConvert.ToInt32(((object[])strTemp)[1]) + 1;
				}

                txtReportDate.Text = ((object[])strTemp)[0] + ", " + lngYearToUse;
				rs.OpenRecordset("select count(Transactionnumber) as MaleFemale From dogtransactions where isnull(DangerousDog,0) <> 1 and isnull(NuisanceDog,0) <> 1 and isnull(online,0) <> 1 and  isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and isnull(boollicense,0) = 1 and (isnull(boolkennel,0) <> 1) and (isnull(boolSpayNeuter,0) <> 1)  and (isnull(boolHearingGuid,0) <> 1) and (isnull(boolSearchRescue,0) <> 1) and (isnull(boolTransfer,0) <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and isnull(dogyear,0) = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
				txtMF1.Text = string.Empty;
				lngMF1 = 0;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("MaleFemale")) != 0)
					{
						txtMF1.Text = FCConvert.ToString(rs.Get_Fields("MaleFemale"));
						lngMF1 = rs.Get_Fields_Int32("malefemale");
					}
				}
				
				rs.OpenRecordset("select count(Transactionnumber) as MaleFemale From dogtransactions where isnull(DangerousDog,0) <> 1 and isnull(NuisanceDog,0) <> 1 and isnull(online,0) <> 1 and isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and isnull(boollicense,0) = 1 and (isnull(boolkennel,0) <> 1) and (isnull(boolSpayNeuter,0) <> 1)  and (isnull(boolHearingGuid,0) <> 1) and (isnull(boolSearchRescue,0) <> 1) and (isnull(boolTransfer,0) <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
				txtMF2.Text = string.Empty;
				lngMF2 = 0;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("MaleFemale")) != 0)
					{
						txtMF2.Text = FCConvert.ToString(rs.Get_Fields("MaleFemale"));
						lngMF2 = rs.Get_Fields_Int32("malefemale");
					}
				}

				int lngNeuter1;
				int lngNeuter2;
				
				rs.OpenRecordset("select count(Transactionnumber) as Neutered From dogtransactions where isnull(DangerousDog,0) <> 1 and isnull(NuisanceDog,0) <> 1 and isnull(online,0) <> 1 and  isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and isnull(boollicense,0) = 1 and (isnull(boolkennel,0) <> 1) and  isnull(boolSpayNeuter,0) = 1  and (isnull(boolHearingGuid,0) <> 1) and (isnull(boolSearchRescue,0) <> 1) and (isnull(boolTransfer,0) <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
				txtNeuter1.Text = string.Empty;
				lngNeuter1 = 0;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("Neutered")) != 0)
					{
						txtNeuter1.Text = FCConvert.ToString(rs.Get_Fields("Neutered"));
						lngNeuter1 = rs.Get_Fields_Int32("neutered");
					}
				}

				rs.OpenRecordset("select count(Transactionnumber) as Neutered From dogtransactions where isnull(DangerousDog,0) <> 1 and isnull(NuisanceDog,0) <> 1 and isnull(online,0) <> 1 and isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and isnull(boollicense,0) = 1 and (isnull(boolkennel,0) <> 1) and  isnull(boolSpayNeuter,0) = 1  and (isnull(boolHearingGuid,0) <> 1) and (isnull(boolSearchRescue,0) <> 1) and (isnull(boolTransfer,0) <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
				txtNeuter2.Text = string.Empty;
				lngNeuter2 = 0;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("Neutered")) != 0)
					{
						txtNeuter2.Text = FCConvert.ToString(rs.Get_Fields("Neutered"));
						lngNeuter2 = rs.Get_Fields_Int32("neutered");
					}
				}

				double lngOnline1;
				double lngOnline2;
				rs.OpenRecordset("select count(Transactionnumber) as Total From dogtransactions where isnull(DangerousDog,0) <> 1 and isnull(NuisanceDog,0) <> 1 and isnull(online,0) = 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1  and  (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
				txtOnline1.Text = string.Empty;
				lngOnline1 = 0;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields_Decimal("total")) != 0)
					{
						txtOnline1.Text = FCConvert.ToString(rs.Get_Fields_Int32("total"));
						lngOnline1 = rs.Get_Fields_Int32("total");
					}
				}
				rs.OpenRecordset("select count(Transactionnumber) as Total From dogtransactions where isnull(DangerousDog,0) <> 1 and isnull(NuisanceDog,0) <> 1 and isnull(online,0) = 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boollicense = 1  and  (boolHearingGuid <> 1) and (boolSearchRescue <> 1) and (boolTransfer <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
				txtOnline2.Text = string.Empty;
				lngOnline2 = 0;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields_Decimal("total")) != 0)
					{
						txtOnline2.Text = FCConvert.ToString(rs.Get_Fields_Int32("total"));
						lngOnline2 = rs.Get_Fields_Int32("total");
					}
				}
				
                rs.OpenRecordset("select count (TransactionNumber) as DangerousCount from dogtransactions where isnull(DangerousDog,0) = 1 and isnull(NuisanceDog,0) <> 1 and isnull(online,0) = 0 and convert(int,isnull(towncode,0)) = " + lngTowncode + " and isnull(boollicense,0) = 1 and isnull(boolkennel,0)  <> 1 and (isnull(boolTransfer,0) <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + lngYearToUse + "' and '" + FCConvert
                        .ToString(fecherFoundation.DateAndTime.DateAdd("D", -1,
                            fecherFoundation.DateAndTime.DateAdd("M", 1,
                                FCConvert.ToDateTime(((object[]) strTemp)[0] + "/01/" +
                                                     lngYearToUse)))) + "' and dogyear = " +
                   (FCConvert.ToInt16(((object[]) strTemp)[1]) ),"Clerk");
                txtDangerous1.Text = "";
                dangerous1 = 0;
                if (!rs.EndOfFile())
                {
                    if (rs.Get_Fields_Int32("DangerousCount") != 0)
                    {
                        dangerous1 = rs.Get_Fields_Int32("DangerousCount");
                        txtDangerous1.Text = dangerous1.ToString();
                    }
                }
                rs.OpenRecordset("select count (TransactionNumber) as NuisanceCount from dogtransactions where isnull(DangerousDog,0) = 0 and isnull(NuisanceDog,0) = 1 and isnull(online,0) = 0 and convert(int,isnull(towncode,0)) = " + lngTowncode + " and isnull(boollicense,0) = 1 and isnull(boolkennel,0) <> 1 and (isnull(boolTransfer,0) <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + lngYearToUse + "' and '" + FCConvert
                                     .ToString(fecherFoundation.DateAndTime.DateAdd("D", -1,
                                         fecherFoundation.DateAndTime.DateAdd("M", 1,
                                             FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" +
                                                                  lngYearToUse)))) + "' and dogyear = " +
                                 (FCConvert.ToInt16(((object[])strTemp)[1]) ), "Clerk");
                txtNuisance1.Text = "";
                nuisance1 = 0;
                if (!rs.EndOfFile())
                {
                    if (rs.Get_Fields_Int32("NuisanceCount") != 0)
                    {
                        nuisance1 = rs.Get_Fields_Int32("NuisanceCount");
                        txtNuisance1.Text = nuisance1.ToString();
                    }
                }

                rs.OpenRecordset("select count (TransactionNumber) as DangerousCount from dogtransactions where isnull(DangerousDog,0) = 1 and isnull(NuisanceDog,0) <> 1 and isnull(online,0) = 0 and convert(int,isnull(towncode,0)) = " + lngTowncode + " and isnull(boollicense,0) = 1 and isnull(boolkennel,0)  <> 1 and (isnull(boolTransfer,0) <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + lngYearToUse + "' and '" + FCConvert
                        .ToString(fecherFoundation.DateAndTime.DateAdd("D", -1,
                            fecherFoundation.DateAndTime.DateAdd("M", 1,
                                FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" +
                                                     lngYearToUse)))) + "' and dogyear = " +
                   (FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "Clerk");
                txtDangerous2.Text = "";
                dangerous2 = 0;
                if (!rs.EndOfFile())
                {
                    if (rs.Get_Fields_Int32("DangerousCount") != 0)
                    {
                        dangerous2 = rs.Get_Fields_Int32("DangerousCount");
                        txtDangerous2.Text = dangerous2.ToString();
                    }
                }
                rs.OpenRecordset("select count (TransactionNumber) as NuisanceCount from dogtransactions where isnull(DangerousDog,0) = 0 and isnull(NuisanceDog,0) = 1 and isnull(online,0) = 0 and convert(int,isnull(towncode,0)) = " + lngTowncode + " and isnull(boollicense,0) = 1 and isnull(boolkennel,0)  <> 1 and (isnull(boolTransfer,0) <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + lngYearToUse + "' and '" + FCConvert
                                     .ToString(fecherFoundation.DateAndTime.DateAdd("D", -1,
                                         fecherFoundation.DateAndTime.DateAdd("M", 1,
                                             FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" +
                                                                  lngYearToUse)))) + "' and dogyear = " +
                                 (FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "Clerk");
                txtNuisance2.Text = "";
                nuisance2 = 0;
                if (!rs.EndOfFile())
                {
                    if (rs.Get_Fields_Int32("NuisanceCount") != 0)
                    {
                        nuisance2 = rs.Get_Fields_Int32("NuisanceCount");
                        txtNuisance2.Text = nuisance2.ToString();
                    }
                }

                int lngKennelTotal;
				intTotal = 0;
				
				rs.OpenRecordset("select count(ID) as Thecount from dogtransactions where isnull(online,0) <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and boolkennel = 1 and boollicense = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
				txtKennelTotal.Text = string.Empty;
				lngKennelTotal = 0;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("thecount")) > 0)
					{
						intTotal = rs.Get_Fields_Int32("thecount");
						txtKennelTotal.Text = FCConvert.ToString(intTotal);
						lngKennelTotal = intTotal;
					}
				}
				// now go through each license and see how many dogs etc.
				int lngKDogsYr1;
				int lngKDogsYr2;
				int lngKDogsAddedYr1;
				int lngKDogsAddedYr2;
				// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
				DateTime dtDate;
				// vbPorter upgrade warning: dtIssueDate As DateTime	OnWrite
				DateTime dtIssueDate;
				// vbPorter upgrade warning: dtTemp1 As DateTime	OnWrite(string)
				DateTime dtTemp1;
				DateTime dtTemp2;
				string[] strAry = null;
				string strDogs = "";
				// vbPorter upgrade warning: intNumDogs As int	OnWriteFCConvert.ToInt32(
				int intNumDogs = 0;
				// Dim x As Integer
				bool boolAddedToKennel = false;
				lngKDogsYr1 = 0;
				lngKDogsYr2 = 0;
				lngKDogsAddedYr1 = 0;
				lngKDogsAddedYr2 = 0;
				txtKennel1.Text = "";
				txtKennel2.Text = "";
				txtDogKennel1.Text = "";
				txtDogKennel2.Text = "";
				rs.OpenRecordset("select reissuedate,kennellicense.dognumbers as dognumbers from kennellicense inner join dogowner on (dogowner.ID = kennellicense.ownernum) where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode), "twck0000.vb1");
				while (!rs.EndOfFile())
				{
					if (Information.IsDate(rs.Get_Fields("reissuedate")))
					{
						dtIssueDate = rs.Get_Fields_DateTime("reissuedate");
					}
					else
					{
						dtIssueDate = DateTime.FromOADate(0);
					}
					strDogs = fecherFoundation.Strings.Trim(rs.Get_Fields_String("dognumbers"));
					if (strDogs != string.Empty)
					{
						strAry = Strings.Split(strDogs, ",", -1, CompareConstants.vbTextCompare);
						// now we have a list of the dogs in the kennel
						intNumDogs = (Information.UBound(strAry, 1) + 1);
						for (x = 0; x <= intNumDogs - 1; x++)
						{
							// for each dog check
							clsTemp.OpenRecordset("select dogtokenneldate,[year] from doginfo where ID = " + FCConvert.ToString(Conversion.Val(strAry[x])), "twck0000.vb1");
							if (!clsTemp.EndOfFile())
							{
								boolAddedToKennel = false;
								if (Information.IsDate(clsTemp.Get_Fields("dogtokenneldate")))
								{
									if (clsTemp.Get_Fields_DateTime("dogtokenneldate").ToOADate() != 0)
									{
										dtDate = FCConvert.ToDateTime(Strings.Format(clsTemp.Get_Fields_DateTime("dogtokenneldate"), "MM/dd/yyyy"));
										if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, dtIssueDate) < 0)
										{
											if (dtDate.Month == Conversion.Val(((object[])strTemp)[0]))
											{
												if (dtDate.Year >= lngYearToUse)
												{
													boolAddedToKennel = true;
												}
											}
										}
									}
								}
								dtTemp1 = FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse));
								dtTemp2 = fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))));
								if (Conversion.Val(clsTemp.Get_Fields("year")) == FCConvert.ToInt16(((object[])strTemp)[1]))
								{
									if (boolAddedToKennel)
									{
										lngKDogsAddedYr1 += 1;
									}
									else
									{
										if ((fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtIssueDate) >= 0) && (fecherFoundation.DateAndTime.DateDiff("d", dtTemp2, dtIssueDate) <= 0))
										{
											lngKDogsYr1 += 1;
										}
									}
								}
								else if (Conversion.Val(clsTemp.Get_Fields("year")) == FCConvert.ToInt16(((object[])strTemp)[1]) + 1)
								{
									if (boolAddedToKennel)
									{
										lngKDogsAddedYr2 += 1;
									}
									else
									{
										if ((fecherFoundation.DateAndTime.DateDiff("d", dtTemp1, dtIssueDate) >= 0) && (fecherFoundation.DateAndTime.DateDiff("d", dtTemp2, dtIssueDate) <= 0))
										{
											lngKDogsYr2 += 1;
										}
									}
								}
							}
						}
						// x
					}
					rs.MoveNext();
				}
				if (lngKDogsYr1 > 0)
				{
					txtKennel1.Text = FCConvert.ToString(lngKDogsYr1);
				}
				if (lngKDogsYr2 > 0)
				{
					txtKennel2.Text = FCConvert.ToString(lngKDogsYr2);
				}
				if (lngKDogsAddedYr1 > 0)
				{
					txtDogKennel1.Text = FCConvert.ToString(lngKDogsAddedYr1);
				}
				if (lngKDogsAddedYr2 > 0)
				{
					txtDogKennel2.Text = FCConvert.ToString(lngKDogsAddedYr2);
				}
				
				rs.OpenRecordset("select count(Transactionnumber) as replacementtotal From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boollicense <> 1) and (boolkennel <> 1)   and boolreplacementsticker = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
				txtReplacement1.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("ReplacementTotal")) != 0)
					{
						txtReplacement1.Text = FCConvert.ToString(rs.Get_Fields("ReplacementTotal"));
					}
				}

				rs.OpenRecordset("select count(Transactionnumber) as replacementtagtotal From dogtransactions where  isnull(online,0) <> 1 and  isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and boolreplacementtag = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
				txtReplacementTag.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("ReplacementTagTotal")) != 0)
					{
						txtReplacementTag.Text = FCConvert.ToString(rs.Get_Fields("ReplacementTagTotal"));
					}
					else
					{
						txtReplacementTag.Text = "0";
					}
				}
				else
				{
					txtReplacementTag.Text = "0";
				}

				rs.OpenRecordset("select count(transactionnumber) as newtag from dogtransactions where isnull(online,0) <> 1 and isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1) and convert(int, isnull(oldtagnumber, 0)) = 0 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
				txtNewTag.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("NewTag")) != 0)
					{
						txtNewTag.Text = FCConvert.ToString(rs.Get_Fields("NewTag"));
					}
					else
					{
						txtNewTag.Text = "0";
					}
				}
				else
				{
					txtNewTag.Text = "0";
				}
				rs.OpenRecordset("select count(transactionnumber) as total from dogtransactions where isnull(online,0) = 1 and isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "'", "twck0000.vb1");
				txtTagOnline.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields_Decimal("total")) != 0)
					{
						txtTagOnline.Text = FCConvert.ToString(rs.Get_Fields_Decimal("total"));
					}
					else
					{
						txtTagOnline.Text = "0";
					}
				}
				else
				{
					txtTagOnline.Text = "0";
				}
				
				// txtTagOther.Text = "0"
				int lngTagTotal;
				txtTagTotal.Text = FCConvert.ToString(Conversion.Val(txtReplacementTag.Text) + Conversion.Val(txtNewTag.Text) + Conversion.Val(txtTagOnline.Text));
				
				rs.OpenRecordset("select count(Transactionnumber) as replacementtotal From dogtransactions where isnull(DangerousDog,0) = 0 and isnull(NuisanceDog,0) = 0 and isnull(online,0) <> 1 and  isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and (boollicense <> 1) and (boolkennel <> 1)   and boolreplacementsticker = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
				txtReplacement2.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("ReplacementTotal")) != 0)
					{
						txtReplacement2.Text = FCConvert.ToString(rs.Get_Fields("ReplacementTotal"));
					}
				}
				// search and rescue
				rs.OpenRecordset("SELECT count(DogInfo.ID) as SandRTotal FROM DogInfo inner join dogowner on (dogowner.ID = doginfo.ownernum) where  isnull(IsDangerousDog,0) = 0 and isnull(IsNuisanceDog,0) = 0 and isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and DogInfo.RabStickerIssue >='" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND SandR = 1 and Year = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), modGNBas.DEFAULTDATABASE);
				txtSandR1.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("SandRTotal")) != 0)
					{
						txtSandR1.Text = FCConvert.ToString(rs.Get_Fields("SandRTotal"));
					}
				}
				rs.OpenRecordset("SELECT count(DogInfo.ID) as SandRTotal FROM DogInfo inner join dogowner on (dogowner.ID = doginfo.ownernum) where isnull(IsDangerousDog,0) = 0 and isnull(IsNuisanceDog,0) = 0 and isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and  DogInfo.RabStickerIssue >='" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND SandR = 1 and Year = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), modGNBas.DEFAULTDATABASE);
				txtSandR2.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("SandRTotal")) != 0)
					{
						txtSandR2.Text = FCConvert.ToString(rs.Get_Fields("SandRTotal"));
					}
				}
				// Hearing/Guide
				rs.OpenRecordset("SELECT count(DogInfo.ID) as HearGuideTotal FROM DogInfo  inner join dogowner on (dogowner.ID = doginfo.ownernum) where isnull(IsDangerousDog,0) = 0 and isnull(IsNuisanceDog,0) = 0 and  isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and  DogInfo.RabStickerIssue >='" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND HearGuide = 1 and Year = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), modGNBas.DEFAULTDATABASE);
				txtHearingGuide1.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("HearGuideTotal")) != 0)
					{
						txtHearingGuide1.Text = FCConvert.ToString(rs.Get_Fields("HearGuideTotal"));
					}
				}
				rs.OpenRecordset("SELECT count(DogInfo.ID) as HearGuideTotal FROM DogInfo  inner join dogowner on (dogowner.ID = doginfo.ownernum) where  isnull(towncode, 0) = " + FCConvert.ToString(lngTowncode) + " and  DogInfo.RabStickerIssue >='" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and DogInfo.RabStickerIssue <='" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' AND HearGuide = 1 and Year = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), modGNBas.DEFAULTDATABASE);
				txtHearingGuide2.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (Conversion.Val(rs.Get_Fields("HearGuideTotal")) != 0)
					{
						txtHearingGuide2.Text = rs.Get_Fields_Int32("HearGuideTotal").ToString();
					}
				}
				// Transfers
				rs.OpenRecordset("select count(Transactionnumber) as transferstotal From dogtransactions where online <> 1 and  convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and booltransfer = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])), "twck0000.vb1");
				txtTransfers1.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (rs.Get_Fields_Int32("TransfersTotal") != 0)
					{
						txtTransfers1.Text = rs.Get_Fields_Int32("TransfersTotal").ToString();
					}
				}

				rs.OpenRecordset("select count(Transactionnumber) as transferstotal From dogtransactions where online <> 1 and convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boolkennel <> 1)   and booltransfer = 1 and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1), "twck0000.vb1");
				txtTransfers2.Text = string.Empty;
				if (!rs.EndOfFile())
				{
					if (rs.Get_Fields_Int32("TransfersTotal") != 0)
					{
						txtTransfers2.Text = rs.Get_Fields_Int32("TransfersTotal").ToString();
					}
				}
				if (boolShowOptional)
				{
					rs.OpenRecordset("select count(stickernumber) as thecount from doginventory where type = 'T' and status = 'Active' and ([year] = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])) + " or [year] = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1) + ")", "twck0000.vb1");
					if (!rs.EndOfFile())
					{
						lngEndingBalance = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("thecount"))));
					}
					strSQL = "select * From dogtransactions where  convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boollicense = 1 or boolreplacementsticker = 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1])) + " and TAGnumber  <> '' order by TAGnumber ";
					boolShowRange = true;
					rs.OpenRecordset(strSQL, "twck0000.vb1");
					if (!rs.EndOfFile())
					{
						lngAryIssued[intCurRange, 0] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
						lngAryIssued[intCurRange, 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
						rs.MoveNext();
						while (!rs.EndOfFile())
						{
							// And intCurRange < 2
							if (Conversion.Val(rs.Get_Fields("TAGnumber")) > (lngAryIssued[intCurRange, 1] + 1))
							{
								intCurRange += 1;
								if (intCurRange < 2)
								{
									lngAryIssued[intCurRange, 0] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
									lngAryIssued[intCurRange, 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
								}
								else
								{
									lngAryIssued[intCurRange, 0] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
									lngAryIssued[intCurRange, 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
									boolShowRange = false;
								}
							}
							else
							{
								lngAryIssued[intCurRange, 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
							}
							rs.MoveNext();
						}
					}
					// If boolShowRange Then
					strSQL = "select * From dogtransactions where convert(int, isnull(towncode, 0)) = " + FCConvert.ToString(lngTowncode) + " and (boollicense = 1 or boolreplacementsticker = 1) and transactiondate between '" + ((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse) + "' and '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("D", -1, fecherFoundation.DateAndTime.DateAdd("M", 1, FCConvert.ToDateTime(((object[])strTemp)[0] + "/01/" + FCConvert.ToString(lngYearToUse))))) + "' and dogyear = " + FCConvert.ToString(FCConvert.ToInt16(((object[])strTemp)[1]) + 1) + " and TAGnumber  <> '' order by TAGnumber ";
					rs.OpenRecordset(strSQL, "twck0000.vb1");
					if (!rs.EndOfFile())
					{
						if (intCurRange > 0)
						{
							boolShowRange = false;
						}
						else
						{
							lngAryIssued[intCurRange, 0] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
							lngAryIssued[intCurRange, 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
							rs.MoveNext();
							while (!rs.EndOfFile())
							{
								// And intCurRange < 2
								if (Conversion.Val(rs.Get_Fields("TAGnumber")) > (lngAryIssued[intCurRange, 1] + 1))
								{
									intCurRange += 1;
									if (intCurRange < 2)
									{
										lngAryIssued[intCurRange, 0] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
										lngAryIssued[intCurRange, 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
									}
									else
									{
										lngAryIssued[intCurRange, 0] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
										lngAryIssued[intCurRange, 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
										boolShowRange = false;
									}
								}
								else
								{
									lngAryIssued[intCurRange, 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TAGnumber"))));
								}
								rs.MoveNext();
							}
						}
					}
					// End If
				}

				string strState;
				// Dim rsState     As New clsDRWrapper
				clsDRWrapper clsLoad = new clsDRWrapper();
				txtAttestedBy.Text = frmMonthlyDogReport.InstancePtr.txtClerkName;
				txtMonth.Text = Strings.Left(modGNBas.Statics.gstrMonthYear, 2);
				if (!modGNBas.Statics.boolRegionalTown)
				{
					txtMuniName.Text = modGlobalConstants.Statics.MuniName;
				}
				else
				{
					lngTowncode = FCConvert.ToInt32(Math.Round(Conversion.Val(frmMonthlyDogReport.InstancePtr.gridTownCode.TextMatrix(0, 0))));
					txtMuniName.Text = frmMonthlyDogReport.InstancePtr.gridTownCode.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 0, 0);
				}
				txtAddress1.Text = modGNBas.Statics.ClerkDefaults.Address1;
				strState = modGNBas.Statics.ClerkDefaults.State;
				txtAddress2.Text = modGNBas.Statics.ClerkDefaults.City + ", " + strState + " " + modGNBas.Statics.ClerkDefaults.Zip;
				txtPhone.Text = modGNBas.Statics.ClerkDefaults.Phone.Replace("(207)","");
                if ((txtPhone.Text + "    ").Left(4) == "207-" && txtPhone.Text.Length > 4)
                {
                    txtPhone.Text = txtPhone.Text.Substring(4);
                }
				// txtMFTotal = Val(txtMF1) + Val(txtMF2)
				txtMFTotal.Text = FCConvert.ToString(lngMF1 + lngMF2);
				if (txtMFTotal.Text == "0")
					txtMFTotal.Text = string.Empty;
				double dblMFSummary;
				// txtMFSummary = Format(Val(txtMFTotal) * dblStateUnfixedLic, "###,###,##0.00")
				txtMFSummary.Text = Strings.Format(dblStateUnfixedLic * (lngMF1 + lngMF2), "#,###,###,##0.00");
				dblMFSummary = dblStateUnfixedLic * (lngMF1 + lngMF2);
				if (txtMFSummary.Text == "0")
					txtMFSummary.Text = string.Empty;
				double lngNeuterTotal;
				double lngOnlineTotal;
				txtNeuterTotal.Text = FCConvert.ToString(lngNeuter1 + lngNeuter2);
				lngNeuterTotal = lngNeuter1 + lngNeuter2;
				if (txtNeuterTotal.Text == "0")
					txtNeuterTotal.Text = string.Empty;
				lngOnlineTotal = lngOnline1 + lngOnline2;
				txtOnlineTotal.Text = FCConvert.ToString(lngOnlineTotal);
				if (lngOnlineTotal == 0)
					txtOnlineTotal.Text = "";
				// vbPorter upgrade warning: dblNeuterSummary As double	OnWrite(string)
				double dblNeuterSummary;
				txtNeuterSummary.Text = Strings.Format(lngNeuterTotal * dblStateFixedLic, "###,###,##0.00");
				dblNeuterSummary = FCConvert.ToDouble(Strings.Format(lngNeuterTotal * dblStateFixedLic, "###,###,##0.00"));
				if (txtNeuterSummary.Text == "0")
					txtNeuterSummary.Text = string.Empty;
				// vbPorter upgrade warning: dblKennelSummary As double	OnWrite(string)
				double dblKennelSummary;
				txtKennelSummary.Text = Strings.Format(lngKennelTotal * dblStateKennelLic, "###,###,##0.00");
				dblKennelSummary = FCConvert.ToDouble(Strings.Format(lngKennelTotal * dblStateKennelLic, "###,###,##0.00"));
				if (txtKennelSummary.Text == "0")
					txtKennelSummary.Text = string.Empty;

                double dangerousSummary = 0;
                double nuisanceSummary = 0;
                int dangerousTotal = 0;
                int nuisanceTotal = 0;
                dangerousTotal = dangerous1 + dangerous2;
                nuisanceTotal = nuisance1 + nuisance2;
                if (dangerousTotal > 0)
                {
                    txtDangerousTotal.Text = dangerousTotal.ToString();
                }

                if (nuisanceTotal > 0)
                {
                    txtNuisanceTotal.Text = nuisanceTotal.ToString();
                }

                dangerousSummary = (dangerousTotal * stateDangerousFee).RoundToMoney();
                nuisanceSummary = (nuisanceTotal * stateNuisanceFee).RoundToMoney();
                if (dangerousSummary != 0)
                {
                    txtDangerousSummary.Text = dangerousSummary.FormatAsCurrencyNoSymbol();
                }
                else
                {
                    txtDangerousSummary.Text = "";
                }

                if (nuisanceSummary != 0)
                {
                    txtNuisanceSummary.Text = nuisanceSummary.FormatAsCurrencyNoSymbol();
                }
                else
                {
                    txtNuisanceSummary.Text = "";
                }
				txtReason.Text = frmMonthlyDogReport.InstancePtr.txtReason.Text;
				txtAdjustments.Text = frmMonthlyDogReport.InstancePtr.txtAdjustments.Text;
				double dblAdjustments = 0;
				if (Conversion.Val(frmMonthlyDogReport.InstancePtr.txtAdjustments.Text) == 0)
				{
					dblAdjustments = 0;
				}
				else
				{
					dblAdjustments = FCConvert.ToDouble(frmMonthlyDogReport.InstancePtr.txtAdjustments.Text);
				}
				lblStickerYear1.Text = ((object[])strTemp)[1].ToString();

				lblYear1.Text = ((object[])strTemp)[1].ToString();

				lblStickerYear2.Text = (Convert.ToInt32(((object[])strTemp)[1]) + 1).ToString();
				lblYear2.Text = (Convert.ToInt32(((object[])strTemp)[1]) + 1).ToString();
				txtReportTotal.Text = Strings.Format(dblMFSummary + dblNeuterSummary + dblKennelSummary + dblAdjustments, "###,###,##0.00");
				if (txtReportTotal.Text == "0")
					txtReportTotal.Text = string.Empty;
                lngTotalSticksIssued = txtReplacement1.Text.ToIntegerValue() + txtSandR1.Text.ToIntegerValue() + txtHearingGuide1.Text.ToIntegerValue() + txtTransfers1.Text.ToIntegerValue() + txtDogKennel1.Text.ToIntegerValue() + lngMF1 + lngNeuter1 + txtKennel1.Text.ToIntegerValue() + dangerous1 + nuisance1;
                lngTotalSticksIssued += txtReplacement2.Text.ToIntegerValue() + txtSandR2.Text.ToIntegerValue() + txtHearingGuide2.Text.ToIntegerValue() + txtTransfers2.Text.ToIntegerValue() + txtDogKennel2.Text.ToIntegerValue() + lngMF2 + lngNeuter2 + txtKennel2.Text.ToIntegerValue() + dangerous2 + nuisance2;

                if (boolShowOptional)
				{
					lngLastEndingBalance = lngEndingBalance + lngTotalSticksIssued;
					txtLastEndingBalance.Text = FCConvert.ToString(lngLastEndingBalance);
					txtEndingBalance.Text = FCConvert.ToString(lngEndingBalance);
					txtStickersIssued.Text = FCConvert.ToString(lngTotalSticksIssued);
					if (boolShowRange)
					{
						if (lngAryIssued[0, 0] > 0)
						{
							txtStickerRangeStart1.Text = FCConvert.ToString(lngAryIssued[0, 0]);
							txtStickerRangeEnd1.Text = FCConvert.ToString(lngAryIssued[0, 1]);
						}
						if (lngAryIssued[1, 0] > 0)
						{
							txtStickerRangeStart2.Text = FCConvert.ToString(lngAryIssued[1, 0]);
							txtStickerRangeEnd2.Text = FCConvert.ToString(lngAryIssued[1, 1]);
						}
					}
					else
					{
						if (lngAryIssued[0, 0] > 0)
						{
							ReportFooter.Visible = true;
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		
		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (ReportFooter.Visible)
			{
				int x;
				lblThru.Text = "";
				txtStickerStart.Text = "";
				txtStickerEnd.Text = "";
				for (x = 0; x <= 100; x++)
				{
					if (lngAryIssued[x, 0] > 0)
					{
						txtStickerStart.Text = txtStickerStart.Text + FCConvert.ToString(lngAryIssued[x, 0]) + "\r\n";
						txtStickerEnd.Text = txtStickerEnd.Text + FCConvert.ToString(lngAryIssued[x, 1]) + "\r\n";
						lblThru.Text = lblThru.Text + "through" + "\r\n";
					}
				}
				// x
			}
		}

	
	}
}
