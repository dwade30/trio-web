﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for srptSafetyPaperBottomSectionR196.
	/// </summary>
	public partial class srptSafetyPaperBottomSection : FCSectionReport
	{
		public srptSafetyPaperBottomSection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptSafetyPaperBottomSection InstancePtr
		{
			get
			{
				return (srptSafetyPaperBottomSection)Sys.GetInstance(typeof(srptSafetyPaperBottomSection));
			}
		}

		protected srptSafetyPaperBottomSection _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptSafetyPaperBottomSectionR196	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string[] strAry = null;
			string strTemp;
			strTemp = FCConvert.ToString(this.UserData);
			txtAttest.Text = "";
			txtDateIssued.Text = "";
			txtMuni.Text = "";
			if (strTemp != "")
			{
				strAry = Strings.Split(strTemp, "|", -1, CompareConstants.vbBinaryCompare);
				txtMuni.Text = strAry[0];
				if (Information.UBound(strAry, 1) > 0)
				{
					txtAttest.Text = strAry[1];
					if (Information.UBound(strAry, 1) > 1)
					{
						txtDateIssued.Text = strAry[2];
					}
				}
			}
		}

		
	}
}
