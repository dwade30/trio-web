//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmImportDogs : BaseForm
	{
		public frmImportDogs()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmImportDogs InstancePtr
		{
			get
			{
				return (frmImportDogs)Sys.GetInstance(typeof(frmImportDogs));
			}
		}

		protected frmImportDogs _InstancePtr = null;
		//=========================================================
		private void frmImportDogs_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportDogs properties;
			//frmImportDogs.ScaleWidth	= 5880;
			//frmImportDogs.ScaleHeight	= 4140;
			//frmImportDogs.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// set the size of the form
				modGlobalFunctions.SetFixedSize(this, 1);
				modGlobalFunctions.SetTRIOColors(this);
				LoadGrid();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void frmImportDogs_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// make the enter key work like the tab
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void frmImportDogs_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadGrid()
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			vsData.Rows = 1;
			vsData.Cols = 7;
			vsData.FixedCols = 0;
			vsData.ColHidden(0, true);
			vsData.ColHidden(1, true);
			vsData.TextMatrix(0, 1, "");
			vsData.TextMatrix(0, 2, "");
			vsData.TextMatrix(0, 3, "Dog Name");
			vsData.TextMatrix(0, 4, "Owner Name");
			vsData.TextMatrix(0, 5, "Sticker #");
			vsData.TextMatrix(0, 6, "Tag #");
			vsData.ColDataType(2, FCGrid.DataTypeSettings.flexDTBoolean);
			//vsData.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsData.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsData.EditCell();
			// FILL THE GRID WITH DATA FROM THE DEPT DIV TABLE
			rsData.OpenRecordset("SELECT DogInfo.*, dogowner.ID as DogOwnerID, DogOwner.FirstName, DogOwner.LastName FROM DogOwner RIGHT JOIN DogInfo ON dogowner.ID = DogInfo.OwnerNum Where KennelDog = 0 Order by DogInfo.DogName", "TWCK0000.vb1");
			for (intCounter = 1; intCounter <= (rsData.RecordCount()); intCounter++)
			{
				vsData.Rows += 1;
				vsData.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				vsData.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields("dogownerID")));
				vsData.TextMatrix(intCounter, 3, FCConvert.ToString(rsData.Get_Fields_String("DogName")));
				vsData.TextMatrix(intCounter, 4, rsData.Get_Fields_String("FirstName") + " " + rsData.Get_Fields_String("LastName"));
				vsData.TextMatrix(intCounter, 5, FCConvert.ToString(rsData.Get_Fields_String("StickerLicNum")));
				vsData.TextMatrix(intCounter, 6, FCConvert.ToString(rsData.Get_Fields_String("RabiesTagNo")));
				rsData.MoveNext();
			}
		}

		private void frmImportDogs_Resize(object sender, System.EventArgs e)
		{
			vsData.ColWidth(2, FCConvert.ToInt32(vsData.WidthOriginal * 0.08));
			vsData.ColWidth(3, FCConvert.ToInt32(vsData.WidthOriginal * 0.2));
			vsData.ColWidth(4, FCConvert.ToInt32(vsData.WidthOriginal * 0.4));
			vsData.ColWidth(5, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
			vsData.ColWidth(6, FCConvert.ToInt32(vsData.WidthOriginal * 0.15));
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			for (intCounter = 1; intCounter <= (vsData.Rows - 1); intCounter++)
			{
				if (FCConvert.CBool(Conversion.Val(vsData.TextMatrix(intCounter, 2) + "")) == true)
				{
					rsData.Execute("Update DogInfo Set KennelDog = 1, DogToKennelDate = '" + DateTime.Today.ToShortDateString() + "' where ID = " + vsData.TextMatrix(intCounter, 0), "TWCK0000.vb1");
					rsData.OpenRecordset("Select * from dogowner where ID = " + FCConvert.ToString(modGNBas.Statics.glngKennelOwnerNumber), "TWCK0000.vb1");
					if (rsData.EndOfFile())
					{
						MessageBox.Show("Owner Number: " + FCConvert.ToString(modGNBas.Statics.glngKennelOwnerNumber) + " cannot be found.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else
					{
						rsData.Edit();
						if (FCConvert.ToString(rsData.Get_Fields_String("DogNumbers")) == string.Empty)
						{
							rsData.Set_Fields("DogNumbers", vsData.TextMatrix(intCounter, 0));
						}
						else
						{
							rsData.Set_Fields("DogNumbers", rsData.Get_Fields_String("DogNumbers") + "," + vsData.TextMatrix(intCounter, 0));
						}
						rsData.Update();
					}
				}
			}
			Close();
		}
	}
}
