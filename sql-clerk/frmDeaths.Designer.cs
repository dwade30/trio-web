//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDeaths.
	/// </summary>
	partial class frmDeaths
	{
		public fecherFoundation.FCComboBox cmbMilitarySrv;
		public fecherFoundation.FCComboBox cmbPlaceofDeath;
		public fecherFoundation.FCComboBox cmbMostRecentSpouse;
		public fecherFoundation.FCComboBox cmbMaritalStatus;
		public fecherFoundation.FCLabel lblMaritalStatus;
		public fecherFoundation.FCComboBox cmbViewedBody;
		public fecherFoundation.FCComboBox cmbCertifiedType;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLabels;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkDisposition;
		public fecherFoundation.FCCheckBox chkMarriageOnFile;
		public fecherFoundation.FCCheckBox chkBirthOnFile;
		public fecherFoundation.FCCheckBox chkFetalDeath;
		public fecherFoundation.FCCheckBox chkSupplemental;
		public fecherFoundation.FCCheckBox chkCorrection;
		public fecherFoundation.FCCheckBox chkRequired;
		public fecherFoundation.FCCheckBox chkAmendment;
		public fecherFoundation.FCTextBox txtFileNumber;
		public fecherFoundation.FCTabControl ss;
		public fecherFoundation.FCTabPage ss_Page1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtSocialSecurityNumber;
		public Global.T2KDateBox txtDateofBirth;
		public fecherFoundation.FCTextBox txtResidenceCity;
		public fecherFoundation.FCTextBox txtResidenceCounty;
		public fecherFoundation.FCTextBox txtResidenceState;
		public fecherFoundation.FCTextBox txtCityOrTownOfDeath;
		public fecherFoundation.FCTextBox txtCountyOfDeath;
		public fecherFoundation.FCTextBox mebDateOfDeath;
		public fecherFoundation.FCTextBox txtSex;
		public fecherFoundation.FCTextBox txtAge;
		public fecherFoundation.FCTextBox txtFacilityName;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCTextBox txtPlaceofDeathOther;
		public fecherFoundation.FCLabel Label1_49;
		public fecherFoundation.FCTextBox txtEducationCollege;
		public fecherFoundation.FCTextBox txtEducationElementary;
		public fecherFoundation.FCFrame Frame8;
		public fecherFoundation.FCTextBox txtMostRecentSpouseName;
		public fecherFoundation.FCLabel Label1_44;
		public fecherFoundation.FCTextBox txtResidenceStreet;
		public fecherFoundation.FCTextBox txtRace;
		public fecherFoundation.FCTextBox txtAncestry;
		public fecherFoundation.FCTextBox txtKindofBusiness;
		public fecherFoundation.FCTextBox txtOccupation;
		public fecherFoundation.FCTextBox txtBirthplace;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtAgeDays;
		public fecherFoundation.FCTextBox txtAgeMonths;
		public fecherFoundation.FCLabel Label1_32;
		public fecherFoundation.FCLabel Label1_33;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtAgeMinutes;
		public fecherFoundation.FCTextBox txtAgeHours;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCTextBox txtMiddleName;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCTextBox txtDesignation;
		public Global.T2KDateBox txtActualDate;
		public fecherFoundation.FCLabel Label1_1;
        public fecherFoundation.FCLabel Label1_48;
        public fecherFoundation.FCLabel Label1_47;
		public fecherFoundation.FCLabel Label1_46;
		public fecherFoundation.FCLabel Label1_45;
		public fecherFoundation.FCLabel Label1_43;
		public fecherFoundation.FCLabel Label1_42;
		public fecherFoundation.FCLabel Label1_41;
		public fecherFoundation.FCLabel Label1_40;
		public fecherFoundation.FCLabel Label1_39;
		public fecherFoundation.FCLabel Label1_38;
		public fecherFoundation.FCLabel Label1_37;
		public fecherFoundation.FCLabel Label1_36;
		public fecherFoundation.FCLabel Label1_35;
		public fecherFoundation.FCLabel Label1_29;
		public fecherFoundation.FCLabel Label1_28;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_20;
		public fecherFoundation.FCLabel Label1_19;
		public fecherFoundation.FCLabel Label1_18;
		public fecherFoundation.FCLabel Label1_17;
		public fecherFoundation.FCLabel Label1_13;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label1_31;
		public fecherFoundation.FCLabel lblLabels_42;
		public fecherFoundation.FCTabPage ss_Page2;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtInformantMailingAddress;
		public fecherFoundation.FCTextBox txtInformantName;
		public fecherFoundation.FCLabel Label1_27;
		public fecherFoundation.FCLabel Label1_14;
		public fecherFoundation.FCFrame Frame10;
		public fecherFoundation.FCTextBox txtMotherMiddleName;
		public fecherFoundation.FCTextBox txtMotherFirstName;
		public fecherFoundation.FCTextBox txtMotherLastName;
		public fecherFoundation.FCTextBox txtFatherMiddleName;
		public fecherFoundation.FCTextBox txtFatherFirstName;
		public fecherFoundation.FCTextBox txtFatherLastName;
		public fecherFoundation.FCTextBox txtFatherDesignation;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label1_60;
		public fecherFoundation.FCLabel Label1_59;
		public fecherFoundation.FCLabel Label1_58;
		public fecherFoundation.FCLabel Label1_57;
		public fecherFoundation.FCLabel Label1_56;
		public fecherFoundation.FCLabel Label1_55;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCCheckBox chkBodyEmbalmed;
		public fecherFoundation.FCCheckBox chkDisposition_5;
		public fecherFoundation.FCCheckBox chkDisposition_4;
		public fecherFoundation.FCCheckBox chkDisposition_3;
		public fecherFoundation.FCCheckBox chkDisposition_2;
		public fecherFoundation.FCCheckBox chkDisposition_1;
		public fecherFoundation.FCCheckBox chkDisposition_0;
		public fecherFoundation.FCTextBox txtPlaceofDisposition;
		public fecherFoundation.FCTextBox txtDispositionState;
		public fecherFoundation.FCTextBox txtDispositionCity;
		public fecherFoundation.FCTextBox mebDateOfDisposition;
		public fecherFoundation.FCButton cmdFind;
		public fecherFoundation.FCPanel Frame13;
		public fecherFoundation.FCTextBox txtDispositionOther;
		public fecherFoundation.FCCheckBox chkSignature;
		public fecherFoundation.FCTextBox txtFuneralEstablishmentLicenseNumber;
		public fecherFoundation.FCTextBox txtLicenseeNumber;
		public fecherFoundation.FCTextBox txtNameandAddressofFacility;
		public fecherFoundation.FCLabel Label1_62;
		public fecherFoundation.FCLabel Label1_52;
		public fecherFoundation.FCLabel Label1_51;
		public fecherFoundation.FCLabel Label1_50;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtNameofCertifyingPhysician;
		public fecherFoundation.FCTextBox txtTimeOfDeath;
		public fecherFoundation.FCTextBox txtDateSigned;
		public fecherFoundation.FCTextBox txtNameandAddressofCertifier;
		public fecherFoundation.FCTextBox txtNameofAttendingPhysician;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1_34;
		public fecherFoundation.FCLabel lblMedical;
		public fecherFoundation.FCLabel Label1_54;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_16;
		public fecherFoundation.FCTabPage ss_Page3;
		public fecherFoundation.FCFrame Frame15;
		public fecherFoundation.FCCheckBox chkAutopsyAvailablePrior;
		public fecherFoundation.FCCheckBox chkAutopsyPerformed;
		public fecherFoundation.FCComboBox cmbMannerOfDeath;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCFrame Frame12;
		public fecherFoundation.FCTextBox txtDateFiled;
		public fecherFoundation.FCButton cmdFindClerk;
		public fecherFoundation.FCTextBox txtCityofClerk;
		public fecherFoundation.FCTextBox txtNameofClerk;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame11;
		public fecherFoundation.FCTextBox txtCausesOther;
		public fecherFoundation.FCTextBox txtOnsetD;
		public fecherFoundation.FCTextBox txtOnsetC;
		public fecherFoundation.FCTextBox txtOnsetB;
		public fecherFoundation.FCTextBox txtOnsetA;
		public fecherFoundation.FCTextBox txtCauseD;
		public fecherFoundation.FCTextBox txtCauseC;
		public fecherFoundation.FCTextBox txtCauseB;
		public fecherFoundation.FCTextBox txtCauseA;
		public fecherFoundation.FCLabel Label1_63;
		public fecherFoundation.FCLabel Label1_67;
		public fecherFoundation.FCLabel Label1_61;
		public fecherFoundation.FCLabel Label1_30;
		public fecherFoundation.FCLabel Label1_25;
		public fecherFoundation.FCLabel Label1_22;
		public fecherFoundation.FCLabel Label1_23;
		public fecherFoundation.FCLabel Label1_21;
		public fecherFoundation.FCTabPage ss_Page4;
		public fecherFoundation.FCGrid VSFlexGrid1;
		public FCGrid gridTownCode;
		public fecherFoundation.FCPictureBox ImgComment;
		public fecherFoundation.FCLabel lblRecord;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem Separator1;
		public fecherFoundation.FCToolStripMenuItem mnuBurialPermit;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuRegistrarNames;
		public fecherFoundation.FCToolStripMenuItem mnuFacilityNames;
		public fecherFoundation.FCToolStripMenuItem mnuEditTowns;
		public fecherFoundation.FCToolStripMenuItem mnuComments;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocuments;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExitNoValidation;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Separator2;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		public fecherFoundation.FCLabel lblFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDeaths));
            this.cmbMilitarySrv = new fecherFoundation.FCComboBox();
            this.cmbPlaceofDeath = new fecherFoundation.FCComboBox();
            this.cmbMostRecentSpouse = new fecherFoundation.FCComboBox();
            this.cmbMaritalStatus = new fecherFoundation.FCComboBox();
            this.lblMaritalStatus = new fecherFoundation.FCLabel();
            this.cmbViewedBody = new fecherFoundation.FCComboBox();
            this.cmbCertifiedType = new fecherFoundation.FCComboBox();
            this.chkMarriageOnFile = new fecherFoundation.FCCheckBox();
            this.chkBirthOnFile = new fecherFoundation.FCCheckBox();
            this.chkFetalDeath = new fecherFoundation.FCCheckBox();
            this.chkSupplemental = new fecherFoundation.FCCheckBox();
            this.chkCorrection = new fecherFoundation.FCCheckBox();
            this.chkRequired = new fecherFoundation.FCCheckBox();
            this.chkAmendment = new fecherFoundation.FCCheckBox();
            this.txtFileNumber = new fecherFoundation.FCTextBox();
            this.ss = new fecherFoundation.FCTabControl();
            this.ss_Page1 = new fecherFoundation.FCTabPage();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtSocialSecurityNumber = new fecherFoundation.FCTextBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtAgeDays = new fecherFoundation.FCTextBox();
            this.txtAgeMonths = new fecherFoundation.FCTextBox();
            this.Label1_32 = new fecherFoundation.FCLabel();
            this.Label1_33 = new fecherFoundation.FCLabel();
            this.txtDateofBirth = new Global.T2KDateBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtAgeMinutes = new fecherFoundation.FCTextBox();
            this.txtAgeHours = new fecherFoundation.FCTextBox();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.txtResidenceCity = new fecherFoundation.FCTextBox();
            this.txtResidenceCounty = new fecherFoundation.FCTextBox();
            this.txtResidenceState = new fecherFoundation.FCTextBox();
            this.txtCityOrTownOfDeath = new fecherFoundation.FCTextBox();
            this.txtCountyOfDeath = new fecherFoundation.FCTextBox();
            this.mebDateOfDeath = new fecherFoundation.FCTextBox();
            this.txtSex = new fecherFoundation.FCTextBox();
            this.txtAge = new fecherFoundation.FCTextBox();
            this.txtFacilityName = new fecherFoundation.FCTextBox();
            this.Frame9 = new fecherFoundation.FCFrame();
            this.txtPlaceofDeathOther = new fecherFoundation.FCTextBox();
            this.Label1_49 = new fecherFoundation.FCLabel();
            this.txtEducationCollege = new fecherFoundation.FCTextBox();
            this.txtEducationElementary = new fecherFoundation.FCTextBox();
            this.Frame8 = new fecherFoundation.FCFrame();
            this.txtMostRecentSpouseName = new fecherFoundation.FCTextBox();
            this.Label1_44 = new fecherFoundation.FCLabel();
            this.txtResidenceStreet = new fecherFoundation.FCTextBox();
            this.txtRace = new fecherFoundation.FCTextBox();
            this.txtAncestry = new fecherFoundation.FCTextBox();
            this.txtKindofBusiness = new fecherFoundation.FCTextBox();
            this.txtOccupation = new fecherFoundation.FCTextBox();
            this.txtBirthplace = new fecherFoundation.FCTextBox();
            this.txtMiddleName = new fecherFoundation.FCTextBox();
            this.txtFirstName = new fecherFoundation.FCTextBox();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.txtDesignation = new fecherFoundation.FCTextBox();
            this.txtActualDate = new Global.T2KDateBox();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_47 = new fecherFoundation.FCLabel();
            this.Label1_46 = new fecherFoundation.FCLabel();
            this.Label1_45 = new fecherFoundation.FCLabel();
            this.Label1_43 = new fecherFoundation.FCLabel();
            this.Label1_42 = new fecherFoundation.FCLabel();
            this.Label1_41 = new fecherFoundation.FCLabel();
            this.Label1_40 = new fecherFoundation.FCLabel();
            this.Label1_39 = new fecherFoundation.FCLabel();
            this.Label1_38 = new fecherFoundation.FCLabel();
            this.Label1_37 = new fecherFoundation.FCLabel();
            this.Label1_36 = new fecherFoundation.FCLabel();
            this.Label1_35 = new fecherFoundation.FCLabel();
            this.Label1_29 = new fecherFoundation.FCLabel();
            this.Label1_28 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_20 = new fecherFoundation.FCLabel();
            this.Label1_19 = new fecherFoundation.FCLabel();
            this.Label1_18 = new fecherFoundation.FCLabel();
            this.Label1_17 = new fecherFoundation.FCLabel();
            this.Label1_13 = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.Label1_31 = new fecherFoundation.FCLabel();
            this.lblLabels_42 = new fecherFoundation.FCLabel();
            this.ss_Page2 = new fecherFoundation.FCTabPage();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtInformantMailingAddress = new fecherFoundation.FCTextBox();
            this.txtInformantName = new fecherFoundation.FCTextBox();
            this.Label1_27 = new fecherFoundation.FCLabel();
            this.Label1_14 = new fecherFoundation.FCLabel();
            this.Frame10 = new fecherFoundation.FCFrame();
            this.txtMotherMiddleName = new fecherFoundation.FCTextBox();
            this.txtMotherFirstName = new fecherFoundation.FCTextBox();
            this.txtMotherLastName = new fecherFoundation.FCTextBox();
            this.txtFatherMiddleName = new fecherFoundation.FCTextBox();
            this.txtFatherFirstName = new fecherFoundation.FCTextBox();
            this.txtFatherLastName = new fecherFoundation.FCTextBox();
            this.txtFatherDesignation = new fecherFoundation.FCTextBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label1_60 = new fecherFoundation.FCLabel();
            this.Label1_59 = new fecherFoundation.FCLabel();
            this.Label1_58 = new fecherFoundation.FCLabel();
            this.Label1_57 = new fecherFoundation.FCLabel();
            this.Label1_56 = new fecherFoundation.FCLabel();
            this.Label1_55 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Frame6 = new fecherFoundation.FCFrame();
            this.chkBodyEmbalmed = new fecherFoundation.FCCheckBox();
            this.chkDisposition_5 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_4 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_3 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_2 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_1 = new fecherFoundation.FCCheckBox();
            this.chkDisposition_0 = new fecherFoundation.FCCheckBox();
            this.txtPlaceofDisposition = new fecherFoundation.FCTextBox();
            this.txtDispositionState = new fecherFoundation.FCTextBox();
            this.txtDispositionCity = new fecherFoundation.FCTextBox();
            this.mebDateOfDisposition = new fecherFoundation.FCTextBox();
            this.cmdFind = new fecherFoundation.FCButton();
            this.Frame13 = new fecherFoundation.FCPanel();
            this.txtDispositionOther = new fecherFoundation.FCTextBox();
            this.chkSignature = new fecherFoundation.FCCheckBox();
            this.txtFuneralEstablishmentLicenseNumber = new fecherFoundation.FCTextBox();
            this.txtLicenseeNumber = new fecherFoundation.FCTextBox();
            this.txtNameandAddressofFacility = new fecherFoundation.FCTextBox();
            this.Label1_62 = new fecherFoundation.FCLabel();
            this.Label1_52 = new fecherFoundation.FCLabel();
            this.Label1_51 = new fecherFoundation.FCLabel();
            this.Label1_50 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtNameofCertifyingPhysician = new fecherFoundation.FCTextBox();
            this.txtTimeOfDeath = new fecherFoundation.FCTextBox();
            this.txtDateSigned = new fecherFoundation.FCTextBox();
            this.txtNameandAddressofCertifier = new fecherFoundation.FCTextBox();
            this.txtNameofAttendingPhysician = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1_34 = new fecherFoundation.FCLabel();
            this.lblMedical = new fecherFoundation.FCLabel();
            this.Label1_54 = new fecherFoundation.FCLabel();
            this.Label1_7 = new fecherFoundation.FCLabel();
            this.Label1_16 = new fecherFoundation.FCLabel();
            this.ss_Page3 = new fecherFoundation.FCTabPage();
            this.Frame15 = new fecherFoundation.FCFrame();
            this.chkAutopsyAvailablePrior = new fecherFoundation.FCCheckBox();
            this.chkAutopsyPerformed = new fecherFoundation.FCCheckBox();
            this.cmbMannerOfDeath = new fecherFoundation.FCComboBox();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Frame12 = new fecherFoundation.FCFrame();
            this.txtDateFiled = new fecherFoundation.FCTextBox();
            this.cmdFindClerk = new fecherFoundation.FCButton();
            this.txtCityofClerk = new fecherFoundation.FCTextBox();
            this.txtNameofClerk = new fecherFoundation.FCTextBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame11 = new fecherFoundation.FCFrame();
            this.txtCausesOther = new fecherFoundation.FCTextBox();
            this.txtOnsetD = new fecherFoundation.FCTextBox();
            this.txtOnsetC = new fecherFoundation.FCTextBox();
            this.txtOnsetB = new fecherFoundation.FCTextBox();
            this.txtOnsetA = new fecherFoundation.FCTextBox();
            this.txtCauseD = new fecherFoundation.FCTextBox();
            this.txtCauseC = new fecherFoundation.FCTextBox();
            this.txtCauseB = new fecherFoundation.FCTextBox();
            this.txtCauseA = new fecherFoundation.FCTextBox();
            this.Label1_63 = new fecherFoundation.FCLabel();
            this.Label1_67 = new fecherFoundation.FCLabel();
            this.Label1_61 = new fecherFoundation.FCLabel();
            this.Label1_30 = new fecherFoundation.FCLabel();
            this.Label1_25 = new fecherFoundation.FCLabel();
            this.Label1_22 = new fecherFoundation.FCLabel();
            this.Label1_23 = new fecherFoundation.FCLabel();
            this.Label1_21 = new fecherFoundation.FCLabel();
            this.ss_Page4 = new fecherFoundation.FCTabPage();
            this.VSFlexGrid1 = new fecherFoundation.FCGrid();
            this.Label1_48 = new fecherFoundation.FCLabel();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.ImgComment = new fecherFoundation.FCPictureBox();
            this.lblRecord = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuBurialPermit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRegistrarNames = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFacilityNames = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditTowns = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewDocuments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.Separator1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.Separator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExitNoValidation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.lblFile = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdComments = new fecherFoundation.FCButton();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.cmdSaveExitNoValidation = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBirthOnFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFetalDeath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSupplemental)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAmendment)).BeginInit();
            this.ss.SuspendLayout();
            this.ss_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateofBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
            this.Frame9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).BeginInit();
            this.Frame8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).BeginInit();
            this.ss_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame10)).BeginInit();
            this.Frame10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBodyEmbalmed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            this.ss_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame15)).BeginInit();
            this.Frame15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutopsyAvailablePrior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutopsyPerformed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).BeginInit();
            this.Frame12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFindClerk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame11)).BeginInit();
            this.Frame11.SuspendLayout();
            this.ss_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VSFlexGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExitNoValidation)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 592);
            this.BottomPanel.Size = new System.Drawing.Size(1088, 108);
            this.BottomPanel.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkMarriageOnFile);
            this.ClientArea.Controls.Add(this.chkBirthOnFile);
            this.ClientArea.Controls.Add(this.chkFetalDeath);
            this.ClientArea.Controls.Add(this.chkSupplemental);
            this.ClientArea.Controls.Add(this.chkCorrection);
            this.ClientArea.Controls.Add(this.chkRequired);
            this.ClientArea.Controls.Add(this.chkAmendment);
            this.ClientArea.Controls.Add(this.txtFileNumber);
            this.ClientArea.Controls.Add(this.ss);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.ImgComment);
            this.ClientArea.Controls.Add(this.lblRecord);
            this.ClientArea.Controls.Add(this.lblFile);
            this.ClientArea.Size = new System.Drawing.Size(1088, 532);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdComments);
            this.TopPanel.Controls.Add(this.cmdPrintPreview);
            this.TopPanel.Controls.Add(this.cmdSaveExitNoValidation);
            this.TopPanel.Size = new System.Drawing.Size(1088, 60);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSaveExitNoValidation, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComments, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(208, 30);
            this.HeaderText.Text = "Death Certificates";
            // 
            // cmbMilitarySrv
            // 
            this.cmbMilitarySrv.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbMilitarySrv.Location = new System.Drawing.Point(309, 320);
            this.cmbMilitarySrv.Name = "cmbMilitarySrv";
            this.cmbMilitarySrv.Size = new System.Drawing.Size(79, 40);
            this.cmbMilitarySrv.TabIndex = 26;
            this.cmbMilitarySrv.Text = "No";
            this.cmbMilitarySrv.SelectedIndexChanged += new System.EventHandler(this.optMilitarySrv_CheckedChanged);
            // 
            // cmbPlaceofDeath
            // 
            this.cmbPlaceofDeath.Items.AddRange(new object[] {
            "DOA",
            "Inpatient",
            "ER/Outpatient",
            "Nursing Home",
            "Residence",
            "Other"});
            this.cmbPlaceofDeath.Location = new System.Drawing.Point(20, 50);
            this.cmbPlaceofDeath.Name = "cmbPlaceofDeath";
            this.cmbPlaceofDeath.Size = new System.Drawing.Size(197, 40);
            this.cmbPlaceofDeath.TabIndex = 3;
            this.cmbPlaceofDeath.Text = "DOA";
            this.cmbPlaceofDeath.SelectedIndexChanged += new System.EventHandler(this.optPlaceofDeath_CheckedChanged);
            // 
            // cmbMostRecentSpouse
            // 
            this.cmbMostRecentSpouse.Items.AddRange(new object[] {
            "Living",
            "Deceased",
            "N/A"});
            this.cmbMostRecentSpouse.Location = new System.Drawing.Point(20, 30);
            this.cmbMostRecentSpouse.Name = "cmbMostRecentSpouse";
            this.cmbMostRecentSpouse.Size = new System.Drawing.Size(220, 40);
            this.cmbMostRecentSpouse.TabIndex = 3;
            this.cmbMostRecentSpouse.SelectedIndexChanged += new System.EventHandler(this.optMostRecentSpouse_CheckedChanged);
            // 
            // cmbMaritalStatus
            // 
            this.cmbMaritalStatus.Items.AddRange(new object[] {
            "Married",
            "Never Married",
            "Widowed",
            "Divorced",
            "Domestic Partner",
            "Other"});
            this.cmbMaritalStatus.Location = new System.Drawing.Point(20, 480);
            this.cmbMaritalStatus.Name = "cmbMaritalStatus";
            this.cmbMaritalStatus.Size = new System.Drawing.Size(217, 40);
            this.cmbMaritalStatus.TabIndex = 34;
            this.cmbMaritalStatus.Text = "Married";
            this.cmbMaritalStatus.SelectedIndexChanged += new System.EventHandler(this.optMaritalStatus_CheckedChanged);
            // 
            // lblMaritalStatus
            // 
            this.lblMaritalStatus.AutoSize = true;
            this.lblMaritalStatus.Location = new System.Drawing.Point(20, 460);
            this.lblMaritalStatus.Name = "lblMaritalStatus";
            this.lblMaritalStatus.Size = new System.Drawing.Size(113, 15);
            this.lblMaritalStatus.TabIndex = 33;
            this.lblMaritalStatus.Text = "MARITAL STATUS";
            // 
            // cmbViewedBody
            // 
            this.cmbViewedBody.Items.AddRange(new object[] {
            "Yes",
            "No",
            "Unknown"});
            this.cmbViewedBody.Location = new System.Drawing.Point(567, 30);
            this.cmbViewedBody.Name = "cmbViewedBody";
            this.cmbViewedBody.Size = new System.Drawing.Size(153, 40);
            this.cmbViewedBody.TabIndex = 2;
            this.cmbViewedBody.Text = "Unknown";
            this.cmbViewedBody.SelectedIndexChanged += new System.EventHandler(this.optViewedBody_CheckedChanged);
            // 
            // cmbCertifiedType
            // 
            this.cmbCertifiedType.Items.AddRange(new object[] {
            "Unknown",
            "Medical Examiner",
            "Certifying Physician",
            "Certified Nurse Practitioner",
            "Attending Physician"});
            this.cmbCertifiedType.Location = new System.Drawing.Point(20, 30);
            this.cmbCertifiedType.Name = "cmbCertifiedType";
            this.cmbCertifiedType.Size = new System.Drawing.Size(297, 40);
            this.cmbCertifiedType.Text = "Unknown";
            this.cmbCertifiedType.SelectedIndexChanged += new System.EventHandler(this.optCertifiedType_CheckedChanged);
            // 
            // chkMarriageOnFile
            // 
            this.chkMarriageOnFile.Location = new System.Drawing.Point(287, 104);
            this.chkMarriageOnFile.Name = "chkMarriageOnFile";
            this.chkMarriageOnFile.Size = new System.Drawing.Size(160, 26);
            this.chkMarriageOnFile.TabIndex = 4;
            this.chkMarriageOnFile.Text = "Marriage record on file";
            // 
            // chkBirthOnFile
            // 
            this.chkBirthOnFile.Location = new System.Drawing.Point(498, 104);
            this.chkBirthOnFile.Name = "chkBirthOnFile";
            this.chkBirthOnFile.Size = new System.Drawing.Size(134, 26);
            this.chkBirthOnFile.TabIndex = 5;
            this.chkBirthOnFile.Text = "Birth record on file";
            // 
            // chkFetalDeath
            // 
            this.chkFetalDeath.Location = new System.Drawing.Point(744, 141);
            this.chkFetalDeath.Name = "chkFetalDeath";
            this.chkFetalDeath.Size = new System.Drawing.Size(95, 26);
            this.chkFetalDeath.TabIndex = 8;
            this.chkFetalDeath.Text = "Fetal Death";
            // 
            // chkSupplemental
            // 
            this.chkSupplemental.Location = new System.Drawing.Point(744, 104);
            this.chkSupplemental.Name = "chkSupplemental";
            this.chkSupplemental.Size = new System.Drawing.Size(108, 26);
            this.chkSupplemental.TabIndex = 7;
            this.chkSupplemental.Text = "Supplemental";
            // 
            // chkCorrection
            // 
            this.chkCorrection.Location = new System.Drawing.Point(744, 67);
            this.chkCorrection.Name = "chkCorrection";
            this.chkCorrection.Size = new System.Drawing.Size(88, 26);
            this.chkCorrection.TabIndex = 6;
            this.chkCorrection.Text = "Correction";
            // 
            // chkRequired
            // 
            this.chkRequired.Location = new System.Drawing.Point(287, 67);
            this.chkRequired.Name = "chkRequired";
            this.chkRequired.Size = new System.Drawing.Size(360, 26);
            this.chkRequired.TabIndex = 3;
            this.chkRequired.Text = "Record contains older data. Not all fields will be required";
            // 
            // chkAmendment
            // 
            this.chkAmendment.Location = new System.Drawing.Point(68, 30);
            this.chkAmendment.Name = "chkAmendment";
            this.chkAmendment.Size = new System.Drawing.Size(266, 26);
            this.chkAmendment.TabIndex = 2;
            this.chkAmendment.Text = "An amendment was added to this record";
            this.chkAmendment.Visible = false;
            // 
            // txtFileNumber
            // 
            this.txtFileNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileNumber.Location = new System.Drawing.Point(97, 67);
            this.txtFileNumber.Name = "txtFileNumber";
            this.txtFileNumber.Size = new System.Drawing.Size(170, 40);
            this.txtFileNumber.TabIndex = 1;
            // 
            // ss
            // 
            this.ss.Controls.Add(this.ss_Page1);
            this.ss.Controls.Add(this.ss_Page2);
            this.ss.Controls.Add(this.ss_Page3);
            this.ss.Controls.Add(this.ss_Page4);
            this.ss.Location = new System.Drawing.Point(30, 188);
            this.ss.Name = "ss";
            this.ss.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.ss.Size = new System.Drawing.Size(1020, 1015);
            this.ss.TabIndex = 10;
            this.ss.TabStop = false;
            this.ss.Text = "Decedent";
            this.ss.SelectedIndexChanged += new System.EventHandler(this.ss_SelectedIndexChanged);
            // 
            // ss_Page1
            // 
            this.ss_Page1.Controls.Add(this.Frame1);
            this.ss_Page1.Location = new System.Drawing.Point(1, 47);
            this.ss_Page1.Name = "ss_Page1";
            this.ss_Page1.Size = new System.Drawing.Size(1018, 967);
            this.ss_Page1.Text = "Decedent";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtSocialSecurityNumber);
            this.Frame1.Controls.Add(this.cmbMilitarySrv);
            this.Frame1.Controls.Add(this.cmbMaritalStatus);
            this.Frame1.Controls.Add(this.lblMaritalStatus);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.Controls.Add(this.txtDateofBirth);
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.txtResidenceCity);
            this.Frame1.Controls.Add(this.txtResidenceCounty);
            this.Frame1.Controls.Add(this.txtResidenceState);
            this.Frame1.Controls.Add(this.txtCityOrTownOfDeath);
            this.Frame1.Controls.Add(this.txtCountyOfDeath);
            this.Frame1.Controls.Add(this.mebDateOfDeath);
            this.Frame1.Controls.Add(this.txtSex);
            this.Frame1.Controls.Add(this.txtAge);
            this.Frame1.Controls.Add(this.txtFacilityName);
            this.Frame1.Controls.Add(this.Frame9);
            this.Frame1.Controls.Add(this.txtEducationCollege);
            this.Frame1.Controls.Add(this.txtEducationElementary);
            this.Frame1.Controls.Add(this.Frame8);
            this.Frame1.Controls.Add(this.txtResidenceStreet);
            this.Frame1.Controls.Add(this.txtRace);
            this.Frame1.Controls.Add(this.txtAncestry);
            this.Frame1.Controls.Add(this.txtKindofBusiness);
            this.Frame1.Controls.Add(this.txtOccupation);
            this.Frame1.Controls.Add(this.txtBirthplace);
            this.Frame1.Controls.Add(this.txtMiddleName);
            this.Frame1.Controls.Add(this.txtFirstName);
            this.Frame1.Controls.Add(this.txtLastName);
            this.Frame1.Controls.Add(this.txtDesignation);
            this.Frame1.Controls.Add(this.txtActualDate);
            this.Frame1.Controls.Add(this.Label1_1);
            this.Frame1.Controls.Add(this.Label1_47);
            this.Frame1.Controls.Add(this.Label1_46);
            this.Frame1.Controls.Add(this.Label1_45);
            this.Frame1.Controls.Add(this.Label1_43);
            this.Frame1.Controls.Add(this.Label1_42);
            this.Frame1.Controls.Add(this.Label1_41);
            this.Frame1.Controls.Add(this.Label1_40);
            this.Frame1.Controls.Add(this.Label1_39);
            this.Frame1.Controls.Add(this.Label1_38);
            this.Frame1.Controls.Add(this.Label1_37);
            this.Frame1.Controls.Add(this.Label1_36);
            this.Frame1.Controls.Add(this.Label1_35);
            this.Frame1.Controls.Add(this.Label1_29);
            this.Frame1.Controls.Add(this.Label1_28);
            this.Frame1.Controls.Add(this.Label1_3);
            this.Frame1.Controls.Add(this.Label1_20);
            this.Frame1.Controls.Add(this.Label1_19);
            this.Frame1.Controls.Add(this.Label1_18);
            this.Frame1.Controls.Add(this.Label1_17);
            this.Frame1.Controls.Add(this.Label1_13);
            this.Frame1.Controls.Add(this.Label1_11);
            this.Frame1.Controls.Add(this.Label1_2);
            this.Frame1.Controls.Add(this.Label1_0);
            this.Frame1.Controls.Add(this.Label1_31);
            this.Frame1.Controls.Add(this.lblLabels_42);
            this.Frame1.Location = new System.Drawing.Point(20, 20);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(978, 810);
            this.Frame1.Text = "Decedent\'s Personal Data";
            // 
            // txtSocialSecurityNumber
            // 
            this.txtSocialSecurityNumber.Location = new System.Drawing.Point(299, 120);
            this.txtSocialSecurityNumber.Name = "txtSocialSecurityNumber";
            this.txtSocialSecurityNumber.Size = new System.Drawing.Size(179, 40);
            this.txtSocialSecurityNumber.TabIndex = 13;
            this.txtSocialSecurityNumber.Tag = "Required";
            this.txtSocialSecurityNumber.Enter += new System.EventHandler(this.txtSocialSecurityNumber_Enter);
            this.txtSocialSecurityNumber.TextChanged += new System.EventHandler(this.txtSocialSecurityNumber_TextChanged);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtAgeDays);
            this.Frame3.Controls.Add(this.txtAgeMonths);
            this.Frame3.Controls.Add(this.Label1_32);
            this.Frame3.Controls.Add(this.Label1_33);
            this.Frame3.Location = new System.Drawing.Point(578, 100);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(180, 110);
            this.Frame3.TabIndex = 16;
            this.Frame3.Text = "Under 1 Year";
            // 
            // txtAgeDays
            // 
            this.txtAgeDays.BackColor = System.Drawing.SystemColors.Window;
            this.txtAgeDays.Location = new System.Drawing.Point(100, 50);
            this.txtAgeDays.Name = "txtAgeDays";
            this.txtAgeDays.Size = new System.Drawing.Size(60, 40);
            this.txtAgeDays.TabIndex = 3;
            this.txtAgeDays.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAgeDays_KeyPress);
            // 
            // txtAgeMonths
            // 
            this.txtAgeMonths.BackColor = System.Drawing.SystemColors.Window;
            this.txtAgeMonths.Location = new System.Drawing.Point(20, 50);
            this.txtAgeMonths.Name = "txtAgeMonths";
            this.txtAgeMonths.Size = new System.Drawing.Size(60, 40);
            this.txtAgeMonths.TabIndex = 1;
            this.txtAgeMonths.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAgeMonths_KeyPress);
            // 
            // Label1_32
            // 
            this.Label1_32.Location = new System.Drawing.Point(100, 30);
            this.Label1_32.Name = "Label1_32";
            this.Label1_32.Size = new System.Drawing.Size(38, 15);
            this.Label1_32.TabIndex = 2;
            this.Label1_32.Text = "DAYS";
            // 
            // Label1_33
            // 
            this.Label1_33.Location = new System.Drawing.Point(20, 30);
            this.Label1_33.Name = "Label1_33";
            this.Label1_33.Size = new System.Drawing.Size(60, 13);
            this.Label1_33.TabIndex = 4;
            this.Label1_33.Text = "MONTHS";
            // 
            // txtDateofBirth
            // 
            this.txtDateofBirth.Location = new System.Drawing.Point(20, 260);
            this.txtDateofBirth.Name = "txtDateofBirth";
            this.txtDateofBirth.Size = new System.Drawing.Size(222, 40);
            this.txtDateofBirth.TabIndex = 21;
            this.txtDateofBirth.Tag = "Required";
            this.txtDateofBirth.TextChanged += new System.EventHandler(this.txtDateofBirth_TextChanged);
            this.txtDateofBirth.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateofBirth_Validating);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtAgeMinutes);
            this.Frame2.Controls.Add(this.txtAgeHours);
            this.Frame2.Controls.Add(this.Label1_6);
            this.Frame2.Controls.Add(this.Label1_5);
            this.Frame2.Location = new System.Drawing.Point(778, 100);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(180, 110);
            this.Frame2.TabIndex = 17;
            this.Frame2.Text = "Under 1 Day";
            // 
            // txtAgeMinutes
            // 
            this.txtAgeMinutes.BackColor = System.Drawing.SystemColors.Window;
            this.txtAgeMinutes.Location = new System.Drawing.Point(100, 50);
            this.txtAgeMinutes.Name = "txtAgeMinutes";
            this.txtAgeMinutes.Size = new System.Drawing.Size(60, 40);
            this.txtAgeMinutes.TabIndex = 3;
            this.txtAgeMinutes.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAgeMinutes_KeyPress);
            // 
            // txtAgeHours
            // 
            this.txtAgeHours.BackColor = System.Drawing.SystemColors.Window;
            this.txtAgeHours.Location = new System.Drawing.Point(20, 50);
            this.txtAgeHours.Name = "txtAgeHours";
            this.txtAgeHours.Size = new System.Drawing.Size(60, 40);
            this.txtAgeHours.TabIndex = 1;
            this.txtAgeHours.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAgeHours_KeyPress);
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(100, 30);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(62, 15);
            this.Label1_6.TabIndex = 2;
            this.Label1_6.Text = "MINUTES";
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 30);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(51, 13);
            this.Label1_5.TabIndex = 4;
            this.Label1_5.Text = "HOURS";
            // 
            // txtResidenceCity
            // 
            this.txtResidenceCity.Location = new System.Drawing.Point(498, 750);
            this.txtResidenceCity.Name = "txtResidenceCity";
            this.txtResidenceCity.Size = new System.Drawing.Size(220, 40);
            this.txtResidenceCity.TabIndex = 54;
            this.txtResidenceCity.Tag = "Required";
            this.txtResidenceCity.DoubleClick += new System.EventHandler(this.txtResidenceCity_DoubleClick);
            // 
            // txtResidenceCounty
            // 
            this.txtResidenceCounty.Location = new System.Drawing.Point(259, 750);
            this.txtResidenceCounty.Name = "txtResidenceCounty";
            this.txtResidenceCounty.Size = new System.Drawing.Size(219, 40);
            this.txtResidenceCounty.TabIndex = 52;
            this.txtResidenceCounty.Tag = "Required";
            this.txtResidenceCounty.DoubleClick += new System.EventHandler(this.txtResidenceCounty_DoubleClick);
            // 
            // txtResidenceState
            // 
            this.txtResidenceState.Location = new System.Drawing.Point(20, 750);
            this.txtResidenceState.Name = "txtResidenceState";
            this.txtResidenceState.Size = new System.Drawing.Size(219, 40);
            this.txtResidenceState.TabIndex = 50;
            this.txtResidenceState.Tag = "Required";
            this.txtResidenceState.DoubleClick += new System.EventHandler(this.txtResidenceState_DoubleClick);
            // 
            // txtCityOrTownOfDeath
            // 
            this.txtCityOrTownOfDeath.BackColor = System.Drawing.SystemColors.Window;
            this.txtCityOrTownOfDeath.Location = new System.Drawing.Point(658, 410);
            this.txtCityOrTownOfDeath.Name = "txtCityOrTownOfDeath";
            this.txtCityOrTownOfDeath.Size = new System.Drawing.Size(300, 40);
            this.txtCityOrTownOfDeath.TabIndex = 32;
            this.txtCityOrTownOfDeath.DoubleClick += new System.EventHandler(this.txtCityOrTownOfDeath_DoubleClick);
            // 
            // txtCountyOfDeath
            // 
            this.txtCountyOfDeath.BackColor = System.Drawing.SystemColors.Window;
            this.txtCountyOfDeath.Location = new System.Drawing.Point(339, 410);
            this.txtCountyOfDeath.Name = "txtCountyOfDeath";
            this.txtCountyOfDeath.Size = new System.Drawing.Size(299, 40);
            this.txtCountyOfDeath.TabIndex = 30;
            this.txtCountyOfDeath.DoubleClick += new System.EventHandler(this.txtCountyOfDeath_DoubleClick);
            // 
            // mebDateOfDeath
            // 
            this.mebDateOfDeath.Location = new System.Drawing.Point(20, 120);
            this.mebDateOfDeath.Name = "mebDateOfDeath";
            this.mebDateOfDeath.Size = new System.Drawing.Size(179, 40);
            this.mebDateOfDeath.TabIndex = 9;
            this.mebDateOfDeath.Tag = "Required";
            this.mebDateOfDeath.TextChanged += new System.EventHandler(this.mebDateOfDeath_TextChanged);
            this.mebDateOfDeath.Validating += new System.ComponentModel.CancelEventHandler(this.mebDateOfDeath_Validating);
            // 
            // txtSex
            // 
            this.txtSex.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtSex.Location = new System.Drawing.Point(219, 120);
            this.txtSex.MaxLength = 1;
            this.txtSex.Name = "txtSex";
            this.txtSex.Size = new System.Drawing.Size(60, 40);
            this.txtSex.TabIndex = 11;
            this.txtSex.Tag = "Required";
            this.txtSex.Validating += new System.ComponentModel.CancelEventHandler(this.txtSex_Validating);
            // 
            // txtAge
            // 
            this.txtAge.BackColor = System.Drawing.SystemColors.Window;
            this.txtAge.Location = new System.Drawing.Point(498, 120);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(60, 40);
            this.txtAge.TabIndex = 15;
            this.txtAge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAge_KeyPress);
            // 
            // txtFacilityName
            // 
            this.txtFacilityName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFacilityName.Location = new System.Drawing.Point(20, 410);
            this.txtFacilityName.Name = "txtFacilityName";
            this.txtFacilityName.Size = new System.Drawing.Size(299, 40);
            this.txtFacilityName.TabIndex = 28;
            // 
            // Frame9
            // 
            this.Frame9.Controls.Add(this.txtPlaceofDeathOther);
            this.Frame9.Controls.Add(this.cmbPlaceofDeath);
            this.Frame9.Controls.Add(this.Label1_49);
            this.Frame9.Location = new System.Drawing.Point(504, 220);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(454, 110);
            this.Frame9.TabIndex = 24;
            this.Frame9.Tag = "Required";
            this.Frame9.Text = "Place Of Death";
            // 
            // txtPlaceofDeathOther
            // 
            this.txtPlaceofDeathOther.BackColor = System.Drawing.SystemColors.Window;
            this.txtPlaceofDeathOther.Location = new System.Drawing.Point(237, 50);
            this.txtPlaceofDeathOther.Name = "txtPlaceofDeathOther";
            this.txtPlaceofDeathOther.Size = new System.Drawing.Size(197, 40);
            this.txtPlaceofDeathOther.TabIndex = 2;
            this.txtPlaceofDeathOther.Enter += new System.EventHandler(this.txtPlaceofDeathOther_Enter);
            // 
            // Label1_49
            // 
            this.Label1_49.Location = new System.Drawing.Point(237, 30);
            this.Label1_49.Name = "Label1_49";
            this.Label1_49.Size = new System.Drawing.Size(49, 13);
            this.Label1_49.TabIndex = 1;
            this.Label1_49.Text = "OTHER";
            // 
            // txtEducationCollege
            // 
            this.txtEducationCollege.BackColor = System.Drawing.SystemColors.Window;
            this.txtEducationCollege.Location = new System.Drawing.Point(128, 680);
            this.txtEducationCollege.Name = "txtEducationCollege";
            this.txtEducationCollege.Size = new System.Drawing.Size(60, 40);
            this.txtEducationCollege.TabIndex = 48;
            this.txtEducationCollege.Text = "0";
            this.txtEducationCollege.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtEducationElementary
            // 
            this.txtEducationElementary.BackColor = System.Drawing.SystemColors.Window;
            this.txtEducationElementary.Location = new System.Drawing.Point(128, 630);
            this.txtEducationElementary.Name = "txtEducationElementary";
            this.txtEducationElementary.Size = new System.Drawing.Size(60, 40);
            this.txtEducationElementary.TabIndex = 42;
            this.txtEducationElementary.Text = "0";
            this.txtEducationElementary.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Frame8
            // 
            this.Frame8.Controls.Add(this.txtMostRecentSpouseName);
            this.Frame8.Controls.Add(this.cmbMostRecentSpouse);
            this.Frame8.Controls.Add(this.Label1_44);
            this.Frame8.Location = new System.Drawing.Point(257, 460);
            this.Frame8.Name = "Frame8";
            this.Frame8.Size = new System.Drawing.Size(260, 140);
            this.Frame8.TabIndex = 35;
            this.Frame8.Text = "Most Recent Spouse";
            // 
            // txtMostRecentSpouseName
            // 
            this.txtMostRecentSpouseName.BackColor = System.Drawing.SystemColors.Window;
            this.txtMostRecentSpouseName.Location = new System.Drawing.Point(86, 80);
            this.txtMostRecentSpouseName.Name = "txtMostRecentSpouseName";
            this.txtMostRecentSpouseName.Size = new System.Drawing.Size(154, 40);
            this.txtMostRecentSpouseName.TabIndex = 2;
            // 
            // Label1_44
            // 
            this.Label1_44.Location = new System.Drawing.Point(20, 94);
            this.Label1_44.Name = "Label1_44";
            this.Label1_44.Size = new System.Drawing.Size(42, 14);
            this.Label1_44.TabIndex = 1;
            this.Label1_44.Text = "NAME";
            // 
            // txtResidenceStreet
            // 
            this.txtResidenceStreet.Location = new System.Drawing.Point(738, 750);
            this.txtResidenceStreet.Name = "txtResidenceStreet";
            this.txtResidenceStreet.Size = new System.Drawing.Size(220, 40);
            this.txtResidenceStreet.TabIndex = 56;
            this.txtResidenceStreet.Tag = "Required";
            this.txtResidenceStreet.Leave += new System.EventHandler(this.txtResidenceStreet_Leave);
            // 
            // txtRace
            // 
            this.txtRace.BackColor = System.Drawing.SystemColors.Window;
            this.txtRace.Location = new System.Drawing.Point(618, 630);
            this.txtRace.Name = "txtRace";
            this.txtRace.Size = new System.Drawing.Size(340, 40);
            this.txtRace.TabIndex = 46;
            // 
            // txtAncestry
            // 
            this.txtAncestry.BackColor = System.Drawing.SystemColors.Window;
            this.txtAncestry.Location = new System.Drawing.Point(257, 630);
            this.txtAncestry.Name = "txtAncestry";
            this.txtAncestry.Size = new System.Drawing.Size(341, 40);
            this.txtAncestry.TabIndex = 44;
            // 
            // txtKindofBusiness
            // 
            this.txtKindofBusiness.BackColor = System.Drawing.SystemColors.Window;
            this.txtKindofBusiness.Location = new System.Drawing.Point(757, 480);
            this.txtKindofBusiness.Name = "txtKindofBusiness";
            this.txtKindofBusiness.Size = new System.Drawing.Size(201, 40);
            this.txtKindofBusiness.TabIndex = 39;
            // 
            // txtOccupation
            // 
            this.txtOccupation.BackColor = System.Drawing.SystemColors.Window;
            this.txtOccupation.Location = new System.Drawing.Point(537, 480);
            this.txtOccupation.Name = "txtOccupation";
            this.txtOccupation.Size = new System.Drawing.Size(200, 40);
            this.txtOccupation.TabIndex = 37;
            // 
            // txtBirthplace
            // 
            this.txtBirthplace.Location = new System.Drawing.Point(262, 260);
            this.txtBirthplace.Name = "txtBirthplace";
            this.txtBirthplace.Size = new System.Drawing.Size(222, 40);
            this.txtBirthplace.TabIndex = 23;
            this.txtBirthplace.Tag = "Required";
            this.txtBirthplace.DoubleClick += new System.EventHandler(this.txtBirthplace_DoubleClick);
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.BackColor = System.Drawing.SystemColors.Window;
            this.txtMiddleName.Location = new System.Drawing.Point(283, 50);
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(243, 40);
            this.txtMiddleName.TabIndex = 3;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(20, 50);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(243, 40);
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.Tag = "Required";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(546, 50);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(243, 40);
            this.txtLastName.TabIndex = 5;
            this.txtLastName.Tag = "Required";
            // 
            // txtDesignation
            // 
            this.txtDesignation.BackColor = System.Drawing.SystemColors.Window;
            this.txtDesignation.Location = new System.Drawing.Point(809, 50);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.Size = new System.Drawing.Size(149, 40);
            this.txtDesignation.TabIndex = 7;
            // 
            // txtActualDate
            // 
            this.txtActualDate.Location = new System.Drawing.Point(20, 190);
            this.txtActualDate.MaxLength = 10;
            this.txtActualDate.Name = "txtActualDate";
            this.txtActualDate.Size = new System.Drawing.Size(125, 40);
            this.txtActualDate.TabIndex = 19;
            this.txtActualDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtActualDate_Validate);
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(20, 390);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(94, 14);
            this.Label1_1.TabIndex = 27;
            this.Label1_1.Text = "FACILITY NAME";
            // 
            // Label1_47
            // 
            this.Label1_47.Location = new System.Drawing.Point(20, 694);
            this.Label1_47.Name = "Label1_47";
            this.Label1_47.Size = new System.Drawing.Size(63, 18);
            this.Label1_47.TabIndex = 47;
            this.Label1_47.Text = "COLLEGE";
            // 
            // Label1_46
            // 
            this.Label1_46.Location = new System.Drawing.Point(20, 644);
            this.Label1_46.Name = "Label1_46";
            this.Label1_46.Size = new System.Drawing.Size(88, 18);
            this.Label1_46.TabIndex = 41;
            this.Label1_46.Text = "ELEMENTARY";
            // 
            // Label1_45
            // 
            this.Label1_45.Location = new System.Drawing.Point(20, 610);
            this.Label1_45.Name = "Label1_45";
            this.Label1_45.Size = new System.Drawing.Size(156, 14);
            this.Label1_45.TabIndex = 40;
            this.Label1_45.Text = "DECEDENT\'S EDUCATION";
            // 
            // Label1_43
            // 
            this.Label1_43.AutoSize = true;
            this.Label1_43.Location = new System.Drawing.Point(20, 334);
            this.Label1_43.Name = "Label1_43";
            this.Label1_43.Size = new System.Drawing.Size(290, 15);
            this.Label1_43.TabIndex = 25;
            this.Label1_43.Text = "WAS DECEDENT EVER IN U.S. ARMED FORCES";
            // 
            // Label1_42
            // 
            this.Label1_42.Location = new System.Drawing.Point(738, 730);
            this.Label1_42.Name = "Label1_42";
            this.Label1_42.Size = new System.Drawing.Size(187, 14);
            this.Label1_42.TabIndex = 55;
            this.Label1_42.Text = "RESIDENCE STREET & NUMBER";
            // 
            // Label1_41
            // 
            this.Label1_41.Location = new System.Drawing.Point(498, 730);
            this.Label1_41.Name = "Label1_41";
            this.Label1_41.Size = new System.Drawing.Size(168, 14);
            this.Label1_41.TabIndex = 53;
            this.Label1_41.Text = "RESIDENCE CITY OR TOWN ";
            // 
            // Label1_40
            // 
            this.Label1_40.Location = new System.Drawing.Point(20, 730);
            this.Label1_40.Name = "Label1_40";
            this.Label1_40.Size = new System.Drawing.Size(120, 14);
            this.Label1_40.TabIndex = 49;
            this.Label1_40.Text = "RESIDENCE STATE ";
            // 
            // Label1_39
            // 
            this.Label1_39.Location = new System.Drawing.Point(618, 610);
            this.Label1_39.Name = "Label1_39";
            this.Label1_39.Size = new System.Drawing.Size(42, 14);
            this.Label1_39.TabIndex = 45;
            this.Label1_39.Text = "RACE ";
            // 
            // Label1_38
            // 
            this.Label1_38.Location = new System.Drawing.Point(257, 610);
            this.Label1_38.Name = "Label1_38";
            this.Label1_38.Size = new System.Drawing.Size(70, 14);
            this.Label1_38.TabIndex = 43;
            this.Label1_38.Text = "ANCESTRY";
            // 
            // Label1_37
            // 
            this.Label1_37.Location = new System.Drawing.Point(757, 460);
            this.Label1_37.Name = "Label1_37";
            this.Label1_37.Size = new System.Drawing.Size(185, 15);
            this.Label1_37.TabIndex = 38;
            this.Label1_37.Text = "KIND OF BUSINESS / INDUSTRY";
            // 
            // Label1_36
            // 
            this.Label1_36.Location = new System.Drawing.Point(658, 390);
            this.Label1_36.Name = "Label1_36";
            this.Label1_36.Size = new System.Drawing.Size(154, 16);
            this.Label1_36.TabIndex = 31;
            this.Label1_36.Text = "CITY OR TOWN OF DEATH";
            // 
            // Label1_35
            // 
            this.Label1_35.Location = new System.Drawing.Point(537, 460);
            this.Label1_35.Name = "Label1_35";
            this.Label1_35.Size = new System.Drawing.Size(200, 15);
            this.Label1_35.TabIndex = 36;
            this.Label1_35.Text = "DECEDENT\'S USUAL OCCUPATION";
            // 
            // Label1_29
            // 
            this.Label1_29.Location = new System.Drawing.Point(259, 730);
            this.Label1_29.Name = "Label1_29";
            this.Label1_29.Size = new System.Drawing.Size(132, 14);
            this.Label1_29.TabIndex = 51;
            this.Label1_29.Text = "RESIDENCE COUNTY ";
            // 
            // Label1_28
            // 
            this.Label1_28.Location = new System.Drawing.Point(262, 240);
            this.Label1_28.Name = "Label1_28";
            this.Label1_28.Size = new System.Drawing.Size(81, 15);
            this.Label1_28.TabIndex = 22;
            this.Label1_28.Text = "BIRTHPLACE";
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(498, 100);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(31, 15);
            this.Label1_3.TabIndex = 14;
            this.Label1_3.Text = "AGE";
            // 
            // Label1_20
            // 
            this.Label1_20.Location = new System.Drawing.Point(20, 30);
            this.Label1_20.Name = "Label1_20";
            this.Label1_20.Size = new System.Drawing.Size(79, 15);
            this.Label1_20.TabIndex = 57;
            this.Label1_20.Text = "FIRST NAME";
            // 
            // Label1_19
            // 
            this.Label1_19.Location = new System.Drawing.Point(283, 30);
            this.Label1_19.Name = "Label1_19";
            this.Label1_19.Size = new System.Drawing.Size(92, 15);
            this.Label1_19.TabIndex = 2;
            this.Label1_19.Text = "MIDDLE NAME";
            // 
            // Label1_18
            // 
            this.Label1_18.Location = new System.Drawing.Point(20, 240);
            this.Label1_18.Name = "Label1_18";
            this.Label1_18.Size = new System.Drawing.Size(96, 15);
            this.Label1_18.TabIndex = 20;
            this.Label1_18.Text = "DATE OF BIRTH";
            // 
            // Label1_17
            // 
            this.Label1_17.Location = new System.Drawing.Point(339, 390);
            this.Label1_17.Name = "Label1_17";
            this.Label1_17.Size = new System.Drawing.Size(118, 14);
            this.Label1_17.TabIndex = 29;
            this.Label1_17.Text = "COUNTY OF DEATH";
            // 
            // Label1_13
            // 
            this.Label1_13.Location = new System.Drawing.Point(20, 100);
            this.Label1_13.Name = "Label1_13";
            this.Label1_13.Size = new System.Drawing.Size(112, 15);
            this.Label1_13.TabIndex = 8;
            this.Label1_13.Text = "CERTIFICATE DOD";
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(299, 100);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(121, 15);
            this.Label1_11.TabIndex = 12;
            this.Label1_11.Text = "SOCIAL SECURITY #";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(219, 100);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(31, 15);
            this.Label1_2.TabIndex = 10;
            this.Label1_2.Text = "SEX";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(546, 30);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(74, 15);
            this.Label1_0.TabIndex = 4;
            this.Label1_0.Text = "LAST NAME";
            // 
            // Label1_31
            // 
            this.Label1_31.Location = new System.Drawing.Point(809, 30);
            this.Label1_31.Name = "Label1_31";
            this.Label1_31.Size = new System.Drawing.Size(89, 15);
            this.Label1_31.TabIndex = 6;
            this.Label1_31.Text = "JR. / SR. / ETC";
            // 
            // lblLabels_42
            // 
            this.lblLabels_42.AutoSize = true;
            this.lblLabels_42.BackColor = System.Drawing.Color.Transparent;
            this.lblLabels_42.Location = new System.Drawing.Point(20, 170);
            this.lblLabels_42.Name = "lblLabels_42";
            this.lblLabels_42.Size = new System.Drawing.Size(87, 15);
            this.lblLabels_42.TabIndex = 18;
            this.lblLabels_42.Text = "ACTUAL DOD";
            // 
            // ss_Page2
            // 
            this.ss_Page2.Controls.Add(this.Frame4);
            this.ss_Page2.Controls.Add(this.Frame10);
            this.ss_Page2.Controls.Add(this.Frame6);
            this.ss_Page2.Controls.Add(this.Frame5);
            this.ss_Page2.Location = new System.Drawing.Point(1, 47);
            this.ss_Page2.Name = "ss_Page2";
            this.ss_Page2.Size = new System.Drawing.Size(1018, 967);
            this.ss_Page2.Text = "Parents / Disposition";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtInformantMailingAddress);
            this.Frame4.Controls.Add(this.txtInformantName);
            this.Frame4.Controls.Add(this.Label1_27);
            this.Frame4.Controls.Add(this.Label1_14);
            this.Frame4.Location = new System.Drawing.Point(20, 220);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(978, 90);
            this.Frame4.TabIndex = 1;
            this.Frame4.Text = "Informant";
            // 
            // txtInformantMailingAddress
            // 
            this.txtInformantMailingAddress.AcceptsReturn = true;
            this.txtInformantMailingAddress.Location = new System.Drawing.Point(663, 30);
            this.txtInformantMailingAddress.Multiline = true;
            this.txtInformantMailingAddress.Name = "txtInformantMailingAddress";
            this.txtInformantMailingAddress.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtInformantMailingAddress.Size = new System.Drawing.Size(295, 40);
            this.txtInformantMailingAddress.TabIndex = 3;
            this.txtInformantMailingAddress.Tag = "Required";
            // 
            // txtInformantName
            // 
            this.txtInformantName.Location = new System.Drawing.Point(209, 30);
            this.txtInformantName.Name = "txtInformantName";
            this.txtInformantName.Size = new System.Drawing.Size(295, 40);
            this.txtInformantName.TabIndex = 1;
            this.txtInformantName.Tag = "Required";
            // 
            // Label1_27
            // 
            this.Label1_27.Location = new System.Drawing.Point(524, 44);
            this.Label1_27.Name = "Label1_27";
            this.Label1_27.Size = new System.Drawing.Size(113, 17);
            this.Label1_27.TabIndex = 2;
            this.Label1_27.Text = "MAILING ADDRESS";
            // 
            // Label1_14
            // 
            this.Label1_14.Location = new System.Drawing.Point(20, 44);
            this.Label1_14.Name = "Label1_14";
            this.Label1_14.Size = new System.Drawing.Size(168, 15);
            this.Label1_14.TabIndex = 4;
            this.Label1_14.Text = "FULL NAME OF INFORMANT";
            // 
            // Frame10
            // 
            this.Frame10.Controls.Add(this.txtMotherMiddleName);
            this.Frame10.Controls.Add(this.txtMotherFirstName);
            this.Frame10.Controls.Add(this.txtMotherLastName);
            this.Frame10.Controls.Add(this.txtFatherMiddleName);
            this.Frame10.Controls.Add(this.txtFatherFirstName);
            this.Frame10.Controls.Add(this.txtFatherLastName);
            this.Frame10.Controls.Add(this.txtFatherDesignation);
            this.Frame10.Controls.Add(this.Label5);
            this.Frame10.Controls.Add(this.Label4);
            this.Frame10.Controls.Add(this.Label1_60);
            this.Frame10.Controls.Add(this.Label1_59);
            this.Frame10.Controls.Add(this.Label1_58);
            this.Frame10.Controls.Add(this.Label1_57);
            this.Frame10.Controls.Add(this.Label1_56);
            this.Frame10.Controls.Add(this.Label1_55);
            this.Frame10.Controls.Add(this.Label1_4);
            this.Frame10.Location = new System.Drawing.Point(20, 30);
            this.Frame10.Name = "Frame10";
            this.Frame10.Size = new System.Drawing.Size(978, 180);
            this.Frame10.TabIndex = 2;
            this.Frame10.Text = "Parents";
            // 
            // txtMotherMiddleName
            // 
            this.txtMotherMiddleName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtMotherMiddleName.Location = new System.Drawing.Point(365, 120);
            this.txtMotherMiddleName.Name = "txtMotherMiddleName";
            this.txtMotherMiddleName.Size = new System.Drawing.Size(235, 40);
            this.txtMotherMiddleName.TabIndex = 13;
            // 
            // txtMotherFirstName
            // 
            this.txtMotherFirstName.Location = new System.Drawing.Point(110, 120);
            this.txtMotherFirstName.Name = "txtMotherFirstName";
            this.txtMotherFirstName.Size = new System.Drawing.Size(235, 40);
            this.txtMotherFirstName.TabIndex = 11;
            this.txtMotherFirstName.Tag = "Required";
            // 
            // txtMotherLastName
            // 
            this.txtMotherLastName.Location = new System.Drawing.Point(620, 120);
            this.txtMotherLastName.Name = "txtMotherLastName";
            this.txtMotherLastName.Size = new System.Drawing.Size(238, 40);
            this.txtMotherLastName.TabIndex = 15;
            this.txtMotherLastName.Tag = "Required";
            // 
            // txtFatherMiddleName
            // 
            this.txtFatherMiddleName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtFatherMiddleName.Location = new System.Drawing.Point(365, 50);
            this.txtFatherMiddleName.Name = "txtFatherMiddleName";
            this.txtFatherMiddleName.Size = new System.Drawing.Size(235, 40);
            this.txtFatherMiddleName.TabIndex = 4;
            // 
            // txtFatherFirstName
            // 
            this.txtFatherFirstName.Location = new System.Drawing.Point(110, 50);
            this.txtFatherFirstName.Name = "txtFatherFirstName";
            this.txtFatherFirstName.Size = new System.Drawing.Size(235, 40);
            this.txtFatherFirstName.TabIndex = 2;
            this.txtFatherFirstName.Tag = "Required";
            // 
            // txtFatherLastName
            // 
            this.txtFatherLastName.Location = new System.Drawing.Point(620, 50);
            this.txtFatherLastName.Name = "txtFatherLastName";
            this.txtFatherLastName.Size = new System.Drawing.Size(238, 40);
            this.txtFatherLastName.TabIndex = 6;
            this.txtFatherLastName.Tag = "Required";
            // 
            // txtFatherDesignation
            // 
            this.txtFatherDesignation.BackColor = System.Drawing.SystemColors.Window;
            this.txtFatherDesignation.Location = new System.Drawing.Point(878, 50);
            this.txtFatherDesignation.Name = "txtFatherDesignation";
            this.txtFatherDesignation.Size = new System.Drawing.Size(80, 40);
            this.txtFatherDesignation.TabIndex = 8;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 134);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(65, 17);
            this.Label5.TabIndex = 9;
            this.Label5.Text = "MOTHER\'S";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 64);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(65, 17);
            this.Label4.TabIndex = 16;
            this.Label4.Text = "FATHER\'S";
            // 
            // Label1_60
            // 
            this.Label1_60.Location = new System.Drawing.Point(110, 100);
            this.Label1_60.Name = "Label1_60";
            this.Label1_60.Size = new System.Drawing.Size(79, 15);
            this.Label1_60.TabIndex = 10;
            this.Label1_60.Text = "FIRST NAME";
            // 
            // Label1_59
            // 
            this.Label1_59.Location = new System.Drawing.Point(365, 100);
            this.Label1_59.Name = "Label1_59";
            this.Label1_59.Size = new System.Drawing.Size(92, 15);
            this.Label1_59.TabIndex = 12;
            this.Label1_59.Text = "MIDDLE NAME";
            // 
            // Label1_58
            // 
            this.Label1_58.AutoSize = true;
            this.Label1_58.Location = new System.Drawing.Point(620, 100);
            this.Label1_58.Name = "Label1_58";
            this.Label1_58.Size = new System.Drawing.Size(121, 15);
            this.Label1_58.TabIndex = 14;
            this.Label1_58.Text = "MAIDEN SURNAME";
            // 
            // Label1_57
            // 
            this.Label1_57.Location = new System.Drawing.Point(110, 30);
            this.Label1_57.Name = "Label1_57";
            this.Label1_57.Size = new System.Drawing.Size(79, 15);
            this.Label1_57.TabIndex = 1;
            this.Label1_57.Text = "FIRST NAME";
            // 
            // Label1_56
            // 
            this.Label1_56.Location = new System.Drawing.Point(365, 30);
            this.Label1_56.Name = "Label1_56";
            this.Label1_56.Size = new System.Drawing.Size(92, 14);
            this.Label1_56.TabIndex = 3;
            this.Label1_56.Text = "MIDDLE NAME";
            // 
            // Label1_55
            // 
            this.Label1_55.Location = new System.Drawing.Point(620, 30);
            this.Label1_55.Name = "Label1_55";
            this.Label1_55.Size = new System.Drawing.Size(74, 15);
            this.Label1_55.TabIndex = 5;
            this.Label1_55.Text = "LAST NAME";
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(878, 30);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(86, 17);
            this.Label1_4.TabIndex = 7;
            this.Label1_4.Text = "JR /  SR /  ETC";
            // 
            // Frame6
            // 
            this.Frame6.Controls.Add(this.chkBodyEmbalmed);
            this.Frame6.Controls.Add(this.chkDisposition_5);
            this.Frame6.Controls.Add(this.chkDisposition_4);
            this.Frame6.Controls.Add(this.chkDisposition_3);
            this.Frame6.Controls.Add(this.chkDisposition_2);
            this.Frame6.Controls.Add(this.chkDisposition_1);
            this.Frame6.Controls.Add(this.chkDisposition_0);
            this.Frame6.Controls.Add(this.txtPlaceofDisposition);
            this.Frame6.Controls.Add(this.txtDispositionState);
            this.Frame6.Controls.Add(this.txtDispositionCity);
            this.Frame6.Controls.Add(this.mebDateOfDisposition);
            this.Frame6.Controls.Add(this.cmdFind);
            this.Frame6.Controls.Add(this.Frame13);
            this.Frame6.Controls.Add(this.txtDispositionOther);
            this.Frame6.Controls.Add(this.chkSignature);
            this.Frame6.Controls.Add(this.txtFuneralEstablishmentLicenseNumber);
            this.Frame6.Controls.Add(this.txtLicenseeNumber);
            this.Frame6.Controls.Add(this.txtNameandAddressofFacility);
            this.Frame6.Controls.Add(this.Label1_62);
            this.Frame6.Controls.Add(this.Label1_52);
            this.Frame6.Controls.Add(this.Label1_51);
            this.Frame6.Controls.Add(this.Label1_50);
            this.Frame6.Controls.Add(this.Label1_12);
            this.Frame6.Controls.Add(this.Label1_10);
            this.Frame6.Controls.Add(this.Label1_9);
            this.Frame6.Controls.Add(this.Label1_8);
            this.Frame6.Location = new System.Drawing.Point(20, 320);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(978, 329);
            this.Frame6.TabIndex = 2;
            this.Frame6.Text = "Disposition";
            // 
            // chkBodyEmbalmed
            // 
            this.chkBodyEmbalmed.Location = new System.Drawing.Point(603, 50);
            this.chkBodyEmbalmed.Name = "chkBodyEmbalmed";
            this.chkBodyEmbalmed.Size = new System.Drawing.Size(148, 27);
            this.chkBodyEmbalmed.TabIndex = 5;
            this.chkBodyEmbalmed.Text = "Body Embalmed";
            // 
            // chkDisposition_5
            // 
            this.chkDisposition_5.Location = new System.Drawing.Point(242, 87);
            this.chkDisposition_5.Name = "chkDisposition_5";
            this.chkDisposition_5.Size = new System.Drawing.Size(138, 27);
            this.chkDisposition_5.TabIndex = 8;
            this.chkDisposition_5.Text = "Other (Specify)";
            this.chkDisposition_5.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_4
            // 
            this.chkDisposition_4.Location = new System.Drawing.Point(20, 87);
            this.chkDisposition_4.Name = "chkDisposition_4";
            this.chkDisposition_4.Size = new System.Drawing.Size(202, 27);
            this.chkDisposition_4.TabIndex = 7;
            this.chkDisposition_4.Text = "Use by Medical Science";
            this.chkDisposition_4.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_3
            // 
            this.chkDisposition_3.Location = new System.Drawing.Point(411, 50);
            this.chkDisposition_3.Name = "chkDisposition_3";
            this.chkDisposition_3.Size = new System.Drawing.Size(172, 27);
            this.chkDisposition_3.TabIndex = 4;
            this.chkDisposition_3.Text = "Removal from State";
            this.chkDisposition_3.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_2
            // 
            this.chkDisposition_2.Location = new System.Drawing.Point(288, 50);
            this.chkDisposition_2.Name = "chkDisposition_2";
            this.chkDisposition_2.Size = new System.Drawing.Size(103, 27);
            this.chkDisposition_2.TabIndex = 3;
            this.chkDisposition_2.Text = "Cremation";
            this.chkDisposition_2.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_1
            // 
            this.chkDisposition_1.Location = new System.Drawing.Point(199, 50);
            this.chkDisposition_1.Name = "chkDisposition_1";
            this.chkDisposition_1.Size = new System.Drawing.Size(69, 27);
            this.chkDisposition_1.TabIndex = 2;
            this.chkDisposition_1.Text = "Burial";
            this.chkDisposition_1.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // chkDisposition_0
            // 
            this.chkDisposition_0.Location = new System.Drawing.Point(20, 50);
            this.chkDisposition_0.Name = "chkDisposition_0";
            this.chkDisposition_0.Size = new System.Drawing.Size(169, 27);
            this.chkDisposition_0.TabIndex = 1;
            this.chkDisposition_0.Text = "Temporary Storage";
            this.chkDisposition_0.CheckedChanged += new System.EventHandler(this.chkDisposition_CheckedChanged);
            // 
            // txtPlaceofDisposition
            // 
            this.txtPlaceofDisposition.Location = new System.Drawing.Point(20, 162);
            this.txtPlaceofDisposition.Name = "txtPlaceofDisposition";
            this.txtPlaceofDisposition.Size = new System.Drawing.Size(235, 40);
            this.txtPlaceofDisposition.TabIndex = 11;
            this.txtPlaceofDisposition.Tag = "Required";
            // 
            // txtDispositionState
            // 
            this.txtDispositionState.Location = new System.Drawing.Point(529, 162);
            this.txtDispositionState.Name = "txtDispositionState";
            this.txtDispositionState.Size = new System.Drawing.Size(234, 40);
            this.txtDispositionState.TabIndex = 15;
            this.txtDispositionState.Tag = "Required";
            this.txtDispositionState.DoubleClick += new System.EventHandler(this.txtDispositionState_DoubleClick);
            // 
            // txtDispositionCity
            // 
            this.txtDispositionCity.Location = new System.Drawing.Point(275, 162);
            this.txtDispositionCity.Name = "txtDispositionCity";
            this.txtDispositionCity.Size = new System.Drawing.Size(234, 40);
            this.txtDispositionCity.TabIndex = 13;
            this.txtDispositionCity.Tag = "Required";
            this.txtDispositionCity.DoubleClick += new System.EventHandler(this.txtDispositionCity_DoubleClick);
            // 
            // mebDateOfDisposition
            // 
            this.mebDateOfDisposition.BackColor = System.Drawing.SystemColors.Window;
            this.mebDateOfDisposition.Location = new System.Drawing.Point(783, 162);
            this.mebDateOfDisposition.Name = "mebDateOfDisposition";
            this.mebDateOfDisposition.Size = new System.Drawing.Size(175, 40);
            this.mebDateOfDisposition.TabIndex = 17;
            this.mebDateOfDisposition.TextChanged += new System.EventHandler(this.mebDateOfDisposition_TextChanged);
            this.mebDateOfDisposition.Validating += new System.ComponentModel.CancelEventHandler(this.mebDateOfDisposition_Validating);
            // 
            // cmdFind
            // 
            this.cmdFind.AppearanceKey = "actionButton";
            this.cmdFind.Image = ((System.Drawing.Image)(resources.GetObject("cmdFind.Image")));
            this.cmdFind.Location = new System.Drawing.Point(242, 269);
            this.cmdFind.Name = "cmdFind";
            this.cmdFind.Size = new System.Drawing.Size(40, 40);
            this.cmdFind.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.cmdFind, "Find Funeral Establishment Info.");
            this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
            // 
            // Frame13
            // 
            this.Frame13.Location = new System.Drawing.Point(878, 50);
            this.Frame13.Name = "Frame13";
            this.Frame13.Size = new System.Drawing.Size(20, 20);
            this.Frame13.TabIndex = 6;
            this.Frame13.Text = "Frame13";
            // 
            // txtDispositionOther
            // 
            this.txtDispositionOther.BackColor = System.Drawing.SystemColors.Window;
            this.txtDispositionOther.Location = new System.Drawing.Point(400, 87);
            this.txtDispositionOther.Name = "txtDispositionOther";
            this.txtDispositionOther.Size = new System.Drawing.Size(190, 40);
            this.txtDispositionOther.TabIndex = 9;
            // 
            // chkSignature
            // 
            this.chkSignature.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSignature.Location = new System.Drawing.Point(20, 212);
            this.chkSignature.Name = "chkSignature";
            this.chkSignature.Size = new System.Drawing.Size(423, 27);
            this.chkSignature.TabIndex = 18;
            this.chkSignature.Text = "Signature of Funeral Practitioner or Authorized Person";
            this.chkSignature.CheckedChanged += new System.EventHandler(this.chkSignature_CheckedChanged);
            // 
            // txtFuneralEstablishmentLicenseNumber
            // 
            this.txtFuneralEstablishmentLicenseNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtFuneralEstablishmentLicenseNumber.Location = new System.Drawing.Point(640, 269);
            this.txtFuneralEstablishmentLicenseNumber.Name = "txtFuneralEstablishmentLicenseNumber";
            this.txtFuneralEstablishmentLicenseNumber.Size = new System.Drawing.Size(318, 40);
            this.txtFuneralEstablishmentLicenseNumber.TabIndex = 25;
            this.txtFuneralEstablishmentLicenseNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuneralEstablishmentLicenseNumber_Validating);
            // 
            // txtLicenseeNumber
            // 
            this.txtLicenseeNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtLicenseeNumber.Location = new System.Drawing.Point(20, 269);
            this.txtLicenseeNumber.Name = "txtLicenseeNumber";
            this.txtLicenseeNumber.Size = new System.Drawing.Size(202, 40);
            this.txtLicenseeNumber.TabIndex = 20;
            // 
            // txtNameandAddressofFacility
            // 
            this.txtNameandAddressofFacility.AcceptsReturn = true;
            this.txtNameandAddressofFacility.Location = new System.Drawing.Point(302, 269);
            this.txtNameandAddressofFacility.MaxLength = 254;
            this.txtNameandAddressofFacility.Multiline = true;
            this.txtNameandAddressofFacility.Name = "txtNameandAddressofFacility";
            this.txtNameandAddressofFacility.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtNameandAddressofFacility.Size = new System.Drawing.Size(318, 40);
            this.txtNameandAddressofFacility.TabIndex = 23;
            this.txtNameandAddressofFacility.Tag = "Required";
            // 
            // Label1_62
            // 
            this.Label1_62.Location = new System.Drawing.Point(529, 137);
            this.Label1_62.Name = "Label1_62";
            this.Label1_62.Size = new System.Drawing.Size(113, 15);
            this.Label1_62.TabIndex = 14;
            this.Label1_62.Text = "LOCATION - STATE";
            // 
            // Label1_52
            // 
            this.Label1_52.AutoSize = true;
            this.Label1_52.Location = new System.Drawing.Point(20, 249);
            this.Label1_52.Name = "Label1_52";
            this.Label1_52.Size = new System.Drawing.Size(125, 15);
            this.Label1_52.TabIndex = 19;
            this.Label1_52.Text = "LICENSEE NUMBER ";
            // 
            // Label1_51
            // 
            this.Label1_51.Location = new System.Drawing.Point(302, 249);
            this.Label1_51.Name = "Label1_51";
            this.Label1_51.Size = new System.Drawing.Size(323, 15);
            this.Label1_51.TabIndex = 22;
            this.Label1_51.Text = "NAME / ADDRESS OF FACILITY OR AUTHORIZED PERSON";
            // 
            // Label1_50
            // 
            this.Label1_50.AutoSize = true;
            this.Label1_50.Location = new System.Drawing.Point(20, 30);
            this.Label1_50.Name = "Label1_50";
            this.Label1_50.Size = new System.Drawing.Size(164, 15);
            this.Label1_50.TabIndex = 26;
            this.Label1_50.Text = "METHOD OF DISPOSITION";
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(783, 137);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(136, 15);
            this.Label1_12.TabIndex = 16;
            this.Label1_12.Text = "DATE OF DISPOSITION";
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(275, 137);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(101, 15);
            this.Label1_10.TabIndex = 12;
            this.Label1_10.Text = "LOCATION - CITY";
            // 
            // Label1_9
            // 
            this.Label1_9.Location = new System.Drawing.Point(640, 249);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(266, 15);
            this.Label1_9.TabIndex = 24;
            this.Label1_9.Text = "FUNERAL ESTABLISHMENT LICENSE NUMBER";
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(20, 137);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(143, 15);
            this.Label1_8.TabIndex = 10;
            this.Label1_8.Text = "PLACE OF DISPOSITION";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.cmbCertifiedType);
            this.Frame5.Controls.Add(this.cmbViewedBody);
            this.Frame5.Controls.Add(this.txtNameofCertifyingPhysician);
            this.Frame5.Controls.Add(this.txtTimeOfDeath);
            this.Frame5.Controls.Add(this.txtDateSigned);
            this.Frame5.Controls.Add(this.txtNameandAddressofCertifier);
            this.Frame5.Controls.Add(this.txtNameofAttendingPhysician);
            this.Frame5.Controls.Add(this.Label3);
            this.Frame5.Controls.Add(this.Label1_34);
            this.Frame5.Controls.Add(this.lblMedical);
            this.Frame5.Controls.Add(this.Label1_54);
            this.Frame5.Controls.Add(this.Label1_7);
            this.Frame5.Controls.Add(this.Label1_16);
            this.Frame5.Location = new System.Drawing.Point(20, 659);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(978, 290);
            this.Frame5.TabIndex = 3;
            this.Frame5.Text = "Certified";
            // 
            // txtNameofCertifyingPhysician
            // 
            this.txtNameofCertifyingPhysician.BackColor = System.Drawing.SystemColors.Window;
            this.txtNameofCertifyingPhysician.Location = new System.Drawing.Point(156, 80);
            this.txtNameofCertifyingPhysician.Name = "txtNameofCertifyingPhysician";
            this.txtNameofCertifyingPhysician.Size = new System.Drawing.Size(313, 40);
            this.txtNameofCertifyingPhysician.TabIndex = 4;
            // 
            // txtTimeOfDeath
            // 
            this.txtTimeOfDeath.BackColor = System.Drawing.SystemColors.Window;
            this.txtTimeOfDeath.Location = new System.Drawing.Point(156, 180);
            this.txtTimeOfDeath.Name = "txtTimeOfDeath";
            this.txtTimeOfDeath.Size = new System.Drawing.Size(313, 40);
            this.txtTimeOfDeath.TabIndex = 8;
            // 
            // txtDateSigned
            // 
            this.txtDateSigned.Location = new System.Drawing.Point(156, 230);
            this.txtDateSigned.Name = "txtDateSigned";
            this.txtDateSigned.Size = new System.Drawing.Size(313, 40);
            this.txtDateSigned.TabIndex = 10;
            this.txtDateSigned.Tag = "Required";
            this.txtDateSigned.TextChanged += new System.EventHandler(this.txtDateSigned_TextChanged);
            this.txtDateSigned.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateSigned_Validating);
            // 
            // txtNameandAddressofCertifier
            // 
            this.txtNameandAddressofCertifier.AcceptsReturn = true;
            this.txtNameandAddressofCertifier.Location = new System.Drawing.Point(576, 230);
            this.txtNameandAddressofCertifier.MaxLength = 254;
            this.txtNameandAddressofCertifier.Multiline = true;
            this.txtNameandAddressofCertifier.Name = "txtNameandAddressofCertifier";
            this.txtNameandAddressofCertifier.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtNameandAddressofCertifier.Size = new System.Drawing.Size(382, 40);
            this.txtNameandAddressofCertifier.TabIndex = 12;
            this.txtNameandAddressofCertifier.Tag = "Required";
            this.txtNameandAddressofCertifier.Leave += new System.EventHandler(this.txtNameandAddressofCertifier_Leave);
            // 
            // txtNameofAttendingPhysician
            // 
            this.txtNameofAttendingPhysician.BackColor = System.Drawing.SystemColors.Window;
            this.txtNameofAttendingPhysician.Location = new System.Drawing.Point(156, 130);
            this.txtNameofAttendingPhysician.Name = "txtNameofAttendingPhysician";
            this.txtNameofAttendingPhysician.Size = new System.Drawing.Size(313, 40);
            this.txtNameofAttendingPhysician.TabIndex = 6;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 94);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(107, 15);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "CERTIFIED NAME";
            // 
            // Label1_34
            // 
            this.Label1_34.Location = new System.Drawing.Point(20, 194);
            this.Label1_34.Name = "Label1_34";
            this.Label1_34.Size = new System.Drawing.Size(98, 15);
            this.Label1_34.TabIndex = 7;
            this.Label1_34.Text = "TIME OF DEATH";
            // 
            // lblMedical
            // 
            this.lblMedical.Location = new System.Drawing.Point(20, 144);
            this.lblMedical.Name = "lblMedical";
            this.lblMedical.Size = new System.Drawing.Size(113, 15);
            this.lblMedical.TabIndex = 5;
            this.lblMedical.Text = "ATTENDING NAME";
            // 
            // Label1_54
            // 
            this.Label1_54.Location = new System.Drawing.Point(489, 244);
            this.Label1_54.Name = "Label1_54";
            this.Label1_54.Size = new System.Drawing.Size(68, 15);
            this.Label1_54.TabIndex = 11;
            this.Label1_54.Text = "ADDRESS ";
            // 
            // Label1_7
            // 
            this.Label1_7.Location = new System.Drawing.Point(337, 44);
            this.Label1_7.Name = "Label1_7";
            this.Label1_7.Size = new System.Drawing.Size(209, 15);
            this.Label1_7.TabIndex = 1;
            this.Label1_7.Text = "WAS BODY VIEWED AFTER DEATH?";
            // 
            // Label1_16
            // 
            this.Label1_16.Location = new System.Drawing.Point(20, 244);
            this.Label1_16.Name = "Label1_16";
            this.Label1_16.Size = new System.Drawing.Size(87, 15);
            this.Label1_16.TabIndex = 9;
            this.Label1_16.Text = "DATE SIGNED";
            // 
            // ss_Page3
            // 
            this.ss_Page3.Controls.Add(this.Frame15);
            this.ss_Page3.Controls.Add(this.Frame12);
            this.ss_Page3.Controls.Add(this.Frame11);
            this.ss_Page3.Location = new System.Drawing.Point(1, 47);
            this.ss_Page3.Name = "ss_Page3";
            this.ss_Page3.Size = new System.Drawing.Size(1018, 967);
            this.ss_Page3.Text = "Clerk / Confidential";
            // 
            // Frame15
            // 
            this.Frame15.AppearanceKey = "groupBoxNoBorders";
            this.Frame15.Controls.Add(this.chkAutopsyAvailablePrior);
            this.Frame15.Controls.Add(this.chkAutopsyPerformed);
            this.Frame15.Controls.Add(this.cmbMannerOfDeath);
            this.Frame15.Controls.Add(this.Label6);
            this.Frame15.Location = new System.Drawing.Point(20, 150);
            this.Frame15.Name = "Frame15";
            this.Frame15.Size = new System.Drawing.Size(978, 77);
            this.Frame15.TabIndex = 196;
            // 
            // chkAutopsyAvailablePrior
            // 
            this.chkAutopsyAvailablePrior.Location = new System.Drawing.Point(186, 0);
            this.chkAutopsyAvailablePrior.Name = "chkAutopsyAvailablePrior";
            this.chkAutopsyAvailablePrior.Size = new System.Drawing.Size(401, 23);
            this.chkAutopsyAvailablePrior.TabIndex = 1;
            this.chkAutopsyAvailablePrior.Text = "Autopsy findings available prior to completion of cause of death";
            this.chkAutopsyAvailablePrior.CheckedChanged += new System.EventHandler(this.chkAutopsyAvailablePrior_CheckedChanged);
            // 
            // chkAutopsyPerformed
            // 
            this.chkAutopsyPerformed.Name = "chkAutopsyPerformed";
            this.chkAutopsyPerformed.Size = new System.Drawing.Size(139, 23);
            this.chkAutopsyPerformed.TabIndex = 2;
            this.chkAutopsyPerformed.Text = "Autopsy performed";
            this.chkAutopsyPerformed.CheckedChanged += new System.EventHandler(this.chkAutopsyPerformed_CheckedChanged);
            // 
            // cmbMannerOfDeath
            // 
            this.cmbMannerOfDeath.BackColor = System.Drawing.SystemColors.Window;
            this.cmbMannerOfDeath.Location = new System.Drawing.Point(145, 37);
            this.cmbMannerOfDeath.Name = "cmbMannerOfDeath";
            this.cmbMannerOfDeath.Size = new System.Drawing.Size(262, 40);
            this.cmbMannerOfDeath.TabIndex = 3;
            this.cmbMannerOfDeath.TextChanged += new System.EventHandler(this.cmbMannerOfDeath_TextChanged);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(0, 51);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(122, 15);
            this.Label6.TabIndex = 2;
            this.Label6.Text = "MANNER OF DEATH";
            // 
            // Frame12
            // 
            this.Frame12.Controls.Add(this.txtDateFiled);
            this.Frame12.Controls.Add(this.cmdFindClerk);
            this.Frame12.Controls.Add(this.txtCityofClerk);
            this.Frame12.Controls.Add(this.txtNameofClerk);
            this.Frame12.Controls.Add(this.Label8);
            this.Frame12.Controls.Add(this.Label7);
            this.Frame12.Controls.Add(this.Label2);
            this.Frame12.Location = new System.Drawing.Point(20, 30);
            this.Frame12.Name = "Frame12";
            this.Frame12.Size = new System.Drawing.Size(978, 110);
            this.Frame12.TabIndex = 197;
            this.Frame12.Text = "Registrar";
            // 
            // txtDateFiled
            // 
            this.txtDateFiled.Location = new System.Drawing.Point(773, 50);
            this.txtDateFiled.Name = "txtDateFiled";
            this.txtDateFiled.Size = new System.Drawing.Size(185, 40);
            this.txtDateFiled.TabIndex = 6;
            this.txtDateFiled.Tag = "Required";
            this.txtDateFiled.TextChanged += new System.EventHandler(this.txtDateFiled_TextChanged);
            this.txtDateFiled.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateFiled_Validating);
            // 
            // cmdFindClerk
            // 
            this.cmdFindClerk.AppearanceKey = "actionButton";
            this.cmdFindClerk.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindClerk.Image")));
            this.cmdFindClerk.Location = new System.Drawing.Point(20, 50);
            this.cmdFindClerk.Name = "cmdFindClerk";
            this.cmdFindClerk.Size = new System.Drawing.Size(40, 40);
            this.cmdFindClerk.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.cmdFindClerk, "Find Recording Clerk\'s Name");
            this.cmdFindClerk.Click += new System.EventHandler(this.cmdFindClerk_Click);
            // 
            // txtCityofClerk
            // 
            this.txtCityofClerk.Location = new System.Drawing.Point(427, 50);
            this.txtCityofClerk.Name = "txtCityofClerk";
            this.txtCityofClerk.Size = new System.Drawing.Size(326, 40);
            this.txtCityofClerk.TabIndex = 4;
            this.txtCityofClerk.Tag = "Required";
            this.txtCityofClerk.DoubleClick += new System.EventHandler(this.txtCityofClerk_DoubleClick);
            // 
            // txtNameofClerk
            // 
            this.txtNameofClerk.Location = new System.Drawing.Point(80, 50);
            this.txtNameofClerk.Name = "txtNameofClerk";
            this.txtNameofClerk.Size = new System.Drawing.Size(327, 40);
            this.txtNameofClerk.TabIndex = 2;
            this.txtNameofClerk.Tag = "Required";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(773, 30);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(79, 15);
            this.Label8.TabIndex = 5;
            this.Label8.Text = "FILING DATE";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(427, 30);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(43, 15);
            this.Label7.TabIndex = 3;
            this.Label7.Text = "TOWN";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(80, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(119, 15);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "(FIRST    MI    LAST) ";
            // 
            // Frame11
            // 
            this.Frame11.Controls.Add(this.txtCausesOther);
            this.Frame11.Controls.Add(this.txtOnsetD);
            this.Frame11.Controls.Add(this.txtOnsetC);
            this.Frame11.Controls.Add(this.txtOnsetB);
            this.Frame11.Controls.Add(this.txtOnsetA);
            this.Frame11.Controls.Add(this.txtCauseD);
            this.Frame11.Controls.Add(this.txtCauseC);
            this.Frame11.Controls.Add(this.txtCauseB);
            this.Frame11.Controls.Add(this.txtCauseA);
            this.Frame11.Controls.Add(this.Label1_63);
            this.Frame11.Controls.Add(this.Label1_67);
            this.Frame11.Controls.Add(this.Label1_61);
            this.Frame11.Controls.Add(this.Label1_30);
            this.Frame11.Controls.Add(this.Label1_25);
            this.Frame11.Controls.Add(this.Label1_22);
            this.Frame11.Controls.Add(this.Label1_23);
            this.Frame11.Controls.Add(this.Label1_21);
            this.Frame11.Location = new System.Drawing.Point(20, 237);
            this.Frame11.Name = "Frame11";
            this.Frame11.Size = new System.Drawing.Size(978, 335);
            this.Frame11.TabIndex = 1;
            this.Frame11.Text = "Confidential";
            // 
            // txtCausesOther
            // 
            this.txtCausesOther.AcceptsReturn = true;
            this.txtCausesOther.BackColor = System.Drawing.SystemColors.Window;
            this.txtCausesOther.Location = new System.Drawing.Point(283, 250);
            this.txtCausesOther.Multiline = true;
            this.txtCausesOther.Name = "txtCausesOther";
            this.txtCausesOther.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtCausesOther.Size = new System.Drawing.Size(328, 65);
            this.txtCausesOther.TabIndex = 16;
            // 
            // txtOnsetD
            // 
            this.txtOnsetD.BackColor = System.Drawing.SystemColors.Window;
            this.txtOnsetD.Location = new System.Drawing.Point(769, 200);
            this.txtOnsetD.Name = "txtOnsetD";
            this.txtOnsetD.Size = new System.Drawing.Size(60, 40);
            this.txtOnsetD.TabIndex = 14;
            this.txtOnsetD.Enter += new System.EventHandler(this.txtOnsetD_Enter);
            // 
            // txtOnsetC
            // 
            this.txtOnsetC.BackColor = System.Drawing.SystemColors.Window;
            this.txtOnsetC.Location = new System.Drawing.Point(769, 150);
            this.txtOnsetC.Name = "txtOnsetC";
            this.txtOnsetC.Size = new System.Drawing.Size(60, 40);
            this.txtOnsetC.TabIndex = 11;
            this.txtOnsetC.Enter += new System.EventHandler(this.txtOnsetC_Enter);
            // 
            // txtOnsetB
            // 
            this.txtOnsetB.BackColor = System.Drawing.SystemColors.Window;
            this.txtOnsetB.Location = new System.Drawing.Point(769, 100);
            this.txtOnsetB.Name = "txtOnsetB";
            this.txtOnsetB.Size = new System.Drawing.Size(60, 40);
            this.txtOnsetB.TabIndex = 8;
            this.txtOnsetB.Enter += new System.EventHandler(this.txtOnsetB_Enter);
            // 
            // txtOnsetA
            // 
            this.txtOnsetA.BackColor = System.Drawing.SystemColors.Window;
            this.txtOnsetA.Location = new System.Drawing.Point(769, 50);
            this.txtOnsetA.Name = "txtOnsetA";
            this.txtOnsetA.Size = new System.Drawing.Size(60, 40);
            this.txtOnsetA.TabIndex = 5;
            this.txtOnsetA.Enter += new System.EventHandler(this.txtOnsetA_Enter);
            // 
            // txtCauseD
            // 
            this.txtCauseD.BackColor = System.Drawing.SystemColors.Window;
            this.txtCauseD.Location = new System.Drawing.Point(283, 200);
            this.txtCauseD.MaxLength = 255;
            this.txtCauseD.Name = "txtCauseD";
            this.txtCauseD.Size = new System.Drawing.Size(328, 40);
            this.txtCauseD.TabIndex = 13;
            this.txtCauseD.Enter += new System.EventHandler(this.txtCauseD_Enter);
            // 
            // txtCauseC
            // 
            this.txtCauseC.BackColor = System.Drawing.SystemColors.Window;
            this.txtCauseC.Location = new System.Drawing.Point(283, 150);
            this.txtCauseC.MaxLength = 255;
            this.txtCauseC.Name = "txtCauseC";
            this.txtCauseC.Size = new System.Drawing.Size(328, 40);
            this.txtCauseC.TabIndex = 10;
            this.txtCauseC.Enter += new System.EventHandler(this.txtCauseC_Enter);
            // 
            // txtCauseB
            // 
            this.txtCauseB.BackColor = System.Drawing.SystemColors.Window;
            this.txtCauseB.Location = new System.Drawing.Point(283, 100);
            this.txtCauseB.MaxLength = 255;
            this.txtCauseB.Name = "txtCauseB";
            this.txtCauseB.Size = new System.Drawing.Size(328, 40);
            this.txtCauseB.TabIndex = 7;
            this.txtCauseB.Enter += new System.EventHandler(this.txtCauseB_Enter);
            // 
            // txtCauseA
            // 
            this.txtCauseA.Location = new System.Drawing.Point(283, 50);
            this.txtCauseA.MaxLength = 255;
            this.txtCauseA.Name = "txtCauseA";
            this.txtCauseA.Size = new System.Drawing.Size(328, 40);
            this.txtCauseA.TabIndex = 3;
            this.txtCauseA.Tag = "Required";
            this.txtCauseA.Enter += new System.EventHandler(this.txtCauseA_Enter);
            // 
            // Label1_63
            // 
            this.Label1_63.AutoSize = true;
            this.Label1_63.Location = new System.Drawing.Point(20, 253);
            this.Label1_63.Name = "Label1_63";
            this.Label1_63.Size = new System.Drawing.Size(186, 15);
            this.Label1_63.TabIndex = 15;
            this.Label1_63.Text = "OTHER SIGNIFICANT CAUSES";
            // 
            // Label1_67
            // 
            this.Label1_67.Location = new System.Drawing.Point(631, 30);
            this.Label1_67.Name = "Label1_67";
            this.Label1_67.Size = new System.Drawing.Size(323, 15);
            this.Label1_67.TabIndex = 4;
            this.Label1_67.Text = "APPROXIMATE INTERVAL BETWEEN ONSET AND DEATH";
            // 
            // Label1_61
            // 
            this.Label1_61.AutoSize = true;
            this.Label1_61.Location = new System.Drawing.Point(283, 30);
            this.Label1_61.Name = "Label1_61";
            this.Label1_61.Size = new System.Drawing.Size(236, 15);
            this.Label1_61.TabIndex = 1;
            this.Label1_61.Text = "DUE TO (OR AS A CONSEQUENCE OF)";
            // 
            // Label1_30
            // 
            this.Label1_30.AutoSize = true;
            this.Label1_30.Location = new System.Drawing.Point(242, 214);
            this.Label1_30.Name = "Label1_30";
            this.Label1_30.Size = new System.Drawing.Size(16, 15);
            this.Label1_30.TabIndex = 12;
            this.Label1_30.Text = "D";
            // 
            // Label1_25
            // 
            this.Label1_25.AutoSize = true;
            this.Label1_25.Location = new System.Drawing.Point(242, 164);
            this.Label1_25.Name = "Label1_25";
            this.Label1_25.Size = new System.Drawing.Size(16, 15);
            this.Label1_25.TabIndex = 9;
            this.Label1_25.Text = "C";
            // 
            // Label1_22
            // 
            this.Label1_22.AutoSize = true;
            this.Label1_22.Location = new System.Drawing.Point(242, 114);
            this.Label1_22.Name = "Label1_22";
            this.Label1_22.Size = new System.Drawing.Size(16, 15);
            this.Label1_22.TabIndex = 6;
            this.Label1_22.Text = "B";
            // 
            // Label1_23
            // 
            this.Label1_23.AutoSize = true;
            this.Label1_23.Location = new System.Drawing.Point(242, 64);
            this.Label1_23.Name = "Label1_23";
            this.Label1_23.Size = new System.Drawing.Size(16, 15);
            this.Label1_23.TabIndex = 2;
            this.Label1_23.Text = "A";
            // 
            // Label1_21
            // 
            this.Label1_21.AutoSize = true;
            this.Label1_21.Location = new System.Drawing.Point(20, 64);
            this.Label1_21.Name = "Label1_21";
            this.Label1_21.Size = new System.Drawing.Size(122, 15);
            this.Label1_21.TabIndex = 17;
            this.Label1_21.Text = "IMMEDIATE CAUSE";
            // 
            // ss_Page4
            // 
            this.ss_Page4.Controls.Add(this.VSFlexGrid1);
            this.ss_Page4.Location = new System.Drawing.Point(1, 47);
            this.ss_Page4.Name = "ss_Page4";
            this.ss_Page4.Size = new System.Drawing.Size(1018, 967);
            this.ss_Page4.Text = "Amendments";
            // 
            // VSFlexGrid1
            // 
            this.VSFlexGrid1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.VSFlexGrid1.Cols = 6;
            this.VSFlexGrid1.ExtendLastCol = true;
            this.VSFlexGrid1.FixedCols = 0;
            this.VSFlexGrid1.Location = new System.Drawing.Point(20, 20);
            this.VSFlexGrid1.Name = "VSFlexGrid1";
            this.VSFlexGrid1.RowHeadersVisible = false;
            this.VSFlexGrid1.Rows = 17;
            this.VSFlexGrid1.Size = new System.Drawing.Size(979, 923);
            this.VSFlexGrid1.StandardTab = false;
            this.VSFlexGrid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.VSFlexGrid1.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.VSFlexGrid1_MouseMoveEvent);
            this.VSFlexGrid1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.VSFlexGrid1_BeforeEdit);
            this.VSFlexGrid1.Enter += new System.EventHandler(this.VSFlexGrid1_Enter);
            this.VSFlexGrid1.KeyDown += new Wisej.Web.KeyEventHandler(this.VSFlexGrid1_KeyDownEvent);
            // 
            // Label1_48
            // 
            this.Label1_48.Name = "Label1_48";
            // 
            // gridTownCode
            // 
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.Location = new System.Drawing.Point(30, 137);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.ReadOnly = false;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.Size = new System.Drawing.Size(237, 22);
            this.gridTownCode.TabIndex = 9;
            this.gridTownCode.Tag = "land";
            this.gridTownCode.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.gridTownCode_ChangeEdit);
            // 
            // ImgComment
            // 
            this.ImgComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.ImgComment.Image = ((System.Drawing.Image)(resources.GetObject("ImgComment.Image")));
            this.ImgComment.Location = new System.Drawing.Point(30, 30);
            this.ImgComment.Name = "ImgComment";
            this.ImgComment.Size = new System.Drawing.Size(18, 18);
            this.ImgComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // lblRecord
            // 
            this.lblRecord.Location = new System.Drawing.Point(93, 73);
            this.lblRecord.Name = "lblRecord";
            this.lblRecord.Size = new System.Drawing.Size(73, 13);
            this.lblRecord.TabIndex = 2;
            this.lblRecord.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBurialPermit,
            this.mnuRegistrarNames,
            this.mnuFacilityNames,
            this.mnuEditTowns,
            this.mnuViewDocuments});
            this.MainMenu1.Name = null;
            // 
            // mnuBurialPermit
            // 
            this.mnuBurialPermit.Index = 0;
            this.mnuBurialPermit.Name = "mnuBurialPermit";
            this.mnuBurialPermit.Text = "Process Burial Permit";
            this.mnuBurialPermit.Click += new System.EventHandler(this.mnuBurialPermit_Click);
            // 
            // mnuRegistrarNames
            // 
            this.mnuRegistrarNames.Index = 1;
            this.mnuRegistrarNames.Name = "mnuRegistrarNames";
            this.mnuRegistrarNames.Text = "Add / Edit Clerk Names";
            this.mnuRegistrarNames.Click += new System.EventHandler(this.mnuRegistrarNames_Click);
            // 
            // mnuFacilityNames
            // 
            this.mnuFacilityNames.Index = 2;
            this.mnuFacilityNames.Name = "mnuFacilityNames";
            this.mnuFacilityNames.Text = "Add / Edit Facility Names";
            this.mnuFacilityNames.Click += new System.EventHandler(this.mnuFacilityNames_Click);
            // 
            // mnuEditTowns
            // 
            this.mnuEditTowns.Index = 3;
            this.mnuEditTowns.Name = "mnuEditTowns";
            this.mnuEditTowns.Text = "Add / Edit Town Names";
            this.mnuEditTowns.Click += new System.EventHandler(this.mnuEditTowns_Click);
            // 
            // mnuViewDocuments
            // 
            this.mnuViewDocuments.Index = 4;
            this.mnuViewDocuments.Name = "mnuViewDocuments";
            this.mnuViewDocuments.Text = "View Attached Documents";
            this.mnuViewDocuments.Click += new System.EventHandler(this.mnuViewDocuments_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.Separator1,
            this.mnuSP21,
            this.mnuSP3,
            this.mnuSP2,
            this.Separator2,
            this.mnuQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // Separator1
            // 
            this.Separator1.Index = 0;
            this.Separator1.Name = "Separator1";
            this.Separator1.Text = "-";
            // 
            // mnuSP21
            // 
            this.mnuSP21.Index = 1;
            this.mnuSP21.Name = "mnuSP21";
            this.mnuSP21.Text = "-";
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = 2;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 3;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // Separator2
            // 
            this.Separator2.Index = 4;
            this.Separator2.Name = "Separator2";
            this.Separator2.Text = "-";
            // 
            // mnuQuit
            // 
            this.mnuQuit.Index = 5;
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Text = "Exit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // mnuAddNew
            // 
            this.mnuAddNew.Index = -1;
            this.mnuAddNew.Name = "mnuAddNew";
            this.mnuAddNew.Text = "New";
            this.mnuAddNew.Click += new System.EventHandler(this.mnuAddNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = -1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuComments
            // 
            this.mnuComments.Index = -1;
            this.mnuComments.Name = "mnuComments";
            this.mnuComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComments.Text = "Comments";
            this.mnuComments.Click += new System.EventHandler(this.mnuComments_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = -1;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Text = "Print Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                                            ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExitNoValidation
            // 
            this.mnuSaveExitNoValidation.Index = -1;
            this.mnuSaveExitNoValidation.Name = "mnuSaveExitNoValidation";
            this.mnuSaveExitNoValidation.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuSaveExitNoValidation.Text = "Save & Exit (Without Validation)";
            this.mnuSaveExitNoValidation.Click += new System.EventHandler(this.mnuSaveExitNoValidation_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                          ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.BackColor = System.Drawing.Color.Transparent;
            this.lblFile.Location = new System.Drawing.Point(30, 81);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(44, 15);
            this.lblFile.TabIndex = 195;
            this.lblFile.Text = "FILE #";
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(510, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(50, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuAddNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(566, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(60, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(481, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdComments
            // 
            this.cmdComments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComments.Location = new System.Drawing.Point(632, 29);
            this.cmdComments.Name = "cmdComments";
            this.cmdComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComments.Size = new System.Drawing.Size(88, 24);
            this.cmdComments.TabIndex = 3;
            this.cmdComments.Text = "Comments";
            this.cmdComments.Click += new System.EventHandler(this.mnuComments_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.Location = new System.Drawing.Point(726, 29);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Size = new System.Drawing.Size(100, 24);
            this.cmdPrintPreview.TabIndex = 4;
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // cmdSaveExitNoValidation
            // 
            this.cmdSaveExitNoValidation.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSaveExitNoValidation.Location = new System.Drawing.Point(832, 29);
            this.cmdSaveExitNoValidation.Name = "cmdSaveExitNoValidation";
            this.cmdSaveExitNoValidation.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdSaveExitNoValidation.Size = new System.Drawing.Size(212, 24);
            this.cmdSaveExitNoValidation.TabIndex = 5;
            this.cmdSaveExitNoValidation.Text = "Save & Exit (Without Validation)";
            this.cmdSaveExitNoValidation.Click += new System.EventHandler(this.mnuSaveExitNoValidation_Click);
            // 
            // frmDeaths
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1088, 700);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmDeaths";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Death Certificates";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmDeaths_Load);
            this.Activated += new System.EventHandler(this.frmDeaths_Activated);
            this.Resize += new System.EventHandler(this.frmDeaths_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDeaths_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDeaths_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMarriageOnFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBirthOnFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFetalDeath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSupplemental)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAmendment)).EndInit();
            this.ss.ResumeLayout(false);
            this.ss_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDateofBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
            this.Frame9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).EndInit();
            this.Frame8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).EndInit();
            this.ss_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame10)).EndInit();
            this.Frame10.ResumeLayout(false);
            this.Frame10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            this.Frame6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBodyEmbalmed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisposition_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.ss_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame15)).EndInit();
            this.Frame15.ResumeLayout(false);
            this.Frame15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutopsyAvailablePrior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutopsyPerformed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).EndInit();
            this.Frame12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdFindClerk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame11)).EndInit();
            this.Frame11.ResumeLayout(false);
            this.Frame11.PerformLayout();
            this.ss_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VSFlexGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExitNoValidation)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdNew;
		private FCButton cmdDelete;
		private FCButton cmdComments;
		private FCButton cmdPrintPreview;
        private FCButton cmdSaveExitNoValidation;
    }
}
