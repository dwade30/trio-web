//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptNewMarriageIntentionsRev082006.
	/// </summary>
	public partial class rptNewIntentionsRev082006 : BaseSectionReport
	{
		public rptNewIntentionsRev082006()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.Name = "Marriage License";
        }

		public static rptNewIntentionsRev082006 InstancePtr
		{
			get
			{
				return (rptNewIntentionsRev082006)Sys.GetInstance(typeof(rptNewIntentionsRev082006));
			}
		}

		protected rptNewIntentionsRev082006 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewMarriageIntentionsRev082006	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private bool boolPrintTest;
		private double dblLineAdjust;

		public void Init(bool boolTestPrint, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			dblLineAdjust = dblAdjust;
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_Initialize(object sender, EventArgs e)
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(object sender, EventArgs e)
		{
			boolPrinted = true;
			//intPrinted = this.Document.Printer.DeviceCopies;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// If gboolFromDosTrio = True Or bolFromWindowsCR Then
			if (boolPrinted)
			{
				// With typeCRData
				// .Type = "MAR"
				// .Name = Left(frmMarriages.txtBridesCurrentLastName.Text & ", " & frmMarriages.txtBridesFirstName.Text, 30)
				// .Reference = frmMarriages.txtFileNumber.Text
				// .Control1 = Format(frmMarriages.mebDateIntentionsFiled.Text, "##/##/####")
				// .Control2 = Format(frmMarriages.mebDateLicenseIssued.Text, "##/##/####")
				// 
				// 
				// .Amount1 = Format((typClerkFees.NewMarriageLicense + ((intPrinted - 1) * typClerkFees.ReplacementMarriageLicense)), "000000.00")
				// .Amount2 = Format(0, "000000.00")
				// .Amount3 = Format(0, "000000.00")
				// .Amount4 = Format(0, "000000.00")
				// .Amount5 = Format(0, "000000.00")
				// .Amount6 = Format(0, "000000.00")
				// .ProcessReceipt = "Y"
				// End With
				// 
				// Call WriteCashReceiptingData(typeCRData)
				// End
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
			}
			// End If
			if (!boolPrintTest)
			{
				if (modGNBas.Statics.typMarriageConsent.NeedConsent)
				{
					if (fecherFoundation.DateAndTime.DateValue(modGNBas.Statics.typMarriageConsent.MarriageDate).ToOADate() >= DateTime.Today.ToOADate())
					{
						frmPrintMarriageConsent.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			modGNBas.Statics.gboolFullMarriageLicense = false;
			// Call SetPrintProperties(Me)
			double dblLaserLineAdjustment2 = 0;
			// Dim rsData As New clsDRWrapper
			double dblHAdjust;
			// vbPorter upgrade warning: lngAdjust As int	OnWriteFCConvert.ToDouble(
			float lngAdjust;
			dblHAdjust = Conversion.Val(modRegistry.GetRegistryKey("MarriageLicenseHorizontalAdjustment", "CK"));
			lngAdjust = FCConvert.ToSingle(dblHAdjust * 144) / 1440F;
			if (lngAdjust < 0)
			{
				if (this.PageSettings.Margins.Left - lngAdjust < 0)
				{
					this.PageSettings.Margins.Left = 0;
				}
				else
				{
					this.PageSettings.Margins.Left += lngAdjust;
				}
			}
			else
			{
				if (lngAdjust > this.PageSettings.Margins.Right)
					lngAdjust = FCConvert.ToInt32(this.PageSettings.Margins.Right);
				this.PageSettings.Margins.Right -= lngAdjust;
				this.PageSettings.Margins.Left += lngAdjust;
			}
			//Application.DoEvents();
			if (!boolPrintTest)
			{
				// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
				// If rsData.EndOfFile Then
				// dblLaserLineAdjustment2 = 0
				// Else
				// dblLaserLineAdjustment2 = CDbl(Val(rsData.Fields("MarriageLicenseAdjustment")))
				// End If
				// dblLaserLineAdjustment2 = Val(GetRegistryKey("MarriageLicenseAdjustment", "CK"))
				dblLaserLineAdjustment2 = Conversion.Val(modRegistry.GetRegistryKey("MarriageIntentionsAdjustment", "CK"));
			}
			else
			{
				dblLaserLineAdjustment2 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment2) / 1440F;
			}
		}
		// Public Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports2.DDTool)
		// Call VerifyPrintToFile(Me, Tool)
		// End Sub
		private void Detail_Format(object sender, EventArgs e)
		{
			// If gintMarriageConfirmation = 0 Then Print Top of Form
			// If gintMarriageConfirmation = 1 Then Print bottom of Form
			// If gintMarriageConfirmation = 2 Then Print both top and bottom of Form
			if (!boolPrintTest)
			{
				// txtState.Visible = True
				// txtState.Visible = gstrActiveLicense = "STATE"
				// If txtState.Visible Then txtState.Visible = True
				// 
				// txtIssuance.Visible = True
				// txtIssuance.Visible = gstrActiveLicense = "ISSUANCE"
				// If txtIssuance.Visible Then txtIssuance.Visible = True
				// 
				// txtMarriage.Visible = True
				// txtMarriage.Visible = gstrActiveLicense = "MARRIAGE"
				// If txtMarriage.Visible Then txtMarriage.Visible = True
				txtGroomFirst.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFirstName");
				txtGroomFirst.Visible = true;
				txtGroomMiddle.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMiddleName");
				txtGroomMiddle.Visible = true;
				txtGroomLast.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsLastName");
				txtGroomLast.Visible = true;
				txtGroomAge.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int16("GroomsAge"));
				txtGroomAge.Visible = true;
				txtGroomStreet.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsStreetAddress") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsStreetName");
				txtGroomStreet.Visible = true;
				txtGroomBirthplace.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsBirthplace");
				txtGroomBirthplace.Visible = true;
				txtGroomsDesig.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsDesignation");
				txtGroomsDesig.Visible = true;
				txtGroomsState.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsState");
				txtGroomsState.Visible = true;
				txtGroomsCounty.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsCounty");
				txtGroomsCounty.Visible = true;
				txtGroomsCity.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsCity");
				txtGroomsCity.Visible = true;
				// txtGroomsDOB = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("GroomsDOB"))))
				txtGroomsDOB.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GROOMSDOB");
				txtGroomsDOB.Visible = true;
				txtGroomsFathersName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFathersName");
				txtGroomsFathersName.Visible = true;
				txtGroomsFathersBirthplace.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFathersBirthplace");
				txtGroomsFathersBirthplace.Visible = true;
				txtGroomsMothersName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMothersName");
				txtGroomsMothersName.Visible = true;
				txtGroomsMothersBirthplace.Text = fecherFoundation.Strings.Trim(Strings.Left(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMothersBirthplace") + Strings.StrDup(20, " "), 20));
				txtGroomsMothersBirthplace.Visible = true;
				txtBridesFirst.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFirstName");
				txtBridesFirst.Visible = true;
				txtBridesMiddle.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMiddleName");
				txtBridesMiddle.Visible = true;
				txtBridesSurName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMaidenSurName");
				txtBridesSurName.Visible = true;
				txtBridesLast.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCurrentLastName");
				txtBridesLast.Visible = true;
				txtBridesAge.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int16("BridesAge"));
				txtBridesAge.Visible = true;
				txtBridesStreet.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesStreetNumber") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesStreetName");
				txtBridesStreet.Visible = true;
				txtBridesBirthplace.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesBirthplace");
				txtBridesBirthplace.Visible = true;
				txtBridesState.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesState");
				txtBridesState.Visible = true;
				txtBridesCounty.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCounty");
				txtBridesCounty.Visible = true;
				txtBridesCity.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCity");
				txtBridesCity.Visible = true;
				// txtBridesDOB = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("BridesDOB"))))
				txtBridesDOB.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BRIDESDOB");
				txtBridesDOB.Visible = true;
				txtBridesFathersName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFathersName");
				txtBridesFathersName.Visible = true;
				txtBridesFathersBirthplace.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFathersBirthplace");
				txtBridesFathersBirthplace.Visible = true;
				txtBridesMothersName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMothersName");
				txtBridesMothersName.Visible = true;
				txtBridesMothersBirthplace.Text = fecherFoundation.Strings.Trim(Strings.Left(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMothersBirthplace") + Strings.StrDup(20, " "), 20));
				txtBridesMothersBirthplace.Visible = true;
				txtGroomsMarriageNumber.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMarriageNumber");
				txtGroomsMarriageNumber.Visible = true;
				txtGroomEndedAnnulment.Visible = false;
				txtGroomEndedDeath.Visible = false;
				txtGroomEndedDivorce.Visible = false;
				// This places the xx's over the
				if (FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsPrevMarriageEndDate")) != string.Empty)
				{
					if (Information.IsDate(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("groomsprevmarriageenddate")))
					{
						// txtGroomsMarriageEnded = ConvertDateToHaveSlashes(rsMarriageCertificate.Fields("GroomsPrevMarriageEndDate"))
						txtGroomsMarriageEnded.Text = FCConvert.ToString(Convert.ToDateTime(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GROOMSPREVMARRIAGEENDdate")).Month);
						txtGroomsMarriageEndedDay.Text = FCConvert.ToString(Convert.ToDateTime(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("groomsprevmarriageenddate")).Day);
						txtGroomsMarriageEndedYear.Text = FCConvert.ToString(Convert.ToDateTime(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("groomsprevmarriageenddate")).Year);
						txtGroomsMarriageEnded.Visible = true;
						txtGroomsMarriageEndedDay.Visible = true;
						txtGroomsMarriageEndedYear.Visible = true;
					}
					if (FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsPreviousMarriageEnded")) == "Death")
					{
						txtGroomEndedDeath.Visible = true;
					}
					else if (modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsPreviousMarriageEnded") == "Divorce")
					{
						txtGroomEndedDivorce.Visible = true;
					}
					else if (modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsPreviousMarriageEnded") == "Annulment")
					{
						txtGroomEndedAnnulment.Visible = true;
					}
				}
				fldGroomFormerSpouse.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFormerSpouseName");
				fldGroomCourtName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsCourtName");
				fldBrideFormerSpouse.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFormerSpouseName");
				fldBrideCourtName.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCourtName");
				txtBridesMarriageNumber.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMarriageNumber");
				txtBridesMarriageNumber.Visible = true;
				txtBrideEndedAnnulment.Visible = false;
				txtBrideEndedDeath.Visible = false;
				txtBrideEndedDivorce.Visible = false;
				// If StripDateSlashes(LongDate(rsMarriageCertificate.Fields("BridesPrevMarriageEndDate"))) <> vbNullString Then
				if (FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BRIDESPREVMARRIAGEENDDATE")) != string.Empty)
				{
					// txtBridesMarriageEnded = ConvertDateToHaveSlashes(StripDateSlashes(LongDate(rsMarriageCertificate.Fields("BridesPrevMarriageEndDate"))))
					if (Information.IsDate(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("bridesprevmarriageenddate")))
					{
						txtBridesMarriageEnded.Text = FCConvert.ToString(Convert.ToDateTime(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("bridesprevmarriageenddate")).Month);
						txtBridesMarriageEnded.Visible = true;
						txtBridesMarriageEndedDay.Visible = true;
						txtBridesMarriageEndedYear.Visible = true;
						txtBridesMarriageEndedDay.Text = FCConvert.ToString(Convert.ToDateTime(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("bridesprevmarriageenddate")).Day);
						txtBridesMarriageEndedYear.Text = FCConvert.ToString(Convert.ToDateTime(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("bridesprevmarriageenddate")).Year);
					}
					if (FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesPreviousMarriageEnded")) == "Death")
					{
						txtBrideEndedDeath.Visible = true;
					}
					else if (modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesPreviousMarriageEnded") == "Divorce")
					{
						txtBrideEndedDivorce.Visible = true;
					}
					else if (modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesPreviousMarriageEnded") == "Annulment")
					{
						txtBrideEndedAnnulment.Visible = true;
					}
				}
				if (FCConvert.ToBoolean(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Boolean("GroomDomesticPartner")))
				{
					fldGroomDomesticPartnersNo.Text = "";
					fldGroomDomesticPartnerYes.Text = "X";
					if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("GroomDomesticPartnerYearRegistered")) != 0)
					{
						fldGroomDomesticPartnersYearRegistered.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("GroomDomesticPartnerYearRegistered"));
					}
					else
					{
						fldGroomDomesticPartnersYearRegistered.Text = "";
					}
				}
				else
				{
					fldGroomDomesticPartnersNo.Text = "X";
					fldGroomDomesticPartnerYes.Text = "";
					fldGroomDomesticPartnersYearRegistered.Text = "";
				}
				if (FCConvert.ToBoolean(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Boolean("BrideDomesticPartner")))
				{
					fldBrideDomesticPartnerNo.Text = "";
					fldBrideDomesticPartnerYes.Text = "X";
					if (Conversion.Val(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("BrideDomesticPartnerYearRegistered")) != 0)
					{
						fldBrideDomesticPartnersYearRegistered.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int32("BrideDomesticPartnerYearRegistered"));
					}
					else
					{
						fldBrideDomesticPartnersYearRegistered.Text = "";
					}
				}
				else
				{
					fldBrideDomesticPartnerNo.Text = "X";
					fldBrideDomesticPartnerYes.Text = "";
					fldBrideDomesticPartnersYearRegistered.Text = "";
				}
			}
			else
			{
				txtGroomFirst.Text = "First";
				txtGroomMiddle.Text = "M";
				txtGroomLast.Text = "Last";
				txtGroomAge.Text = "99";
				txtGroomStreet.Text = "0 Street";
				txtGroomBirthplace.Text = "Birth Place";
				txtGroomsDesig.Text = "Sr";
				txtGroomsState.Text = "ME";
				txtGroomsCounty.Text = "County";
				txtGroomsCity.Text = "City";
				txtGroomsDOB.Text = "00/00/00";
				txtGroomsFathersName.Text = "Father's Name";
				txtGroomsFathersBirthplace.Text = "Birth Place";
				txtGroomsMothersName.Text = "Mother's Name";
				txtGroomsMothersBirthplace.Text = "Birth Place";
				txtBridesFirst.Text = "First";
				txtBridesMiddle.Text = "M";
				txtBridesSurName.Text = "";
				txtBridesLast.Text = "Last";
				txtBridesAge.Text = "99";
				txtBridesStreet.Text = "0 Street";
				txtBridesBirthplace.Text = "Place of Birth";
				txtBridesState.Text = "ME";
				txtBridesCounty.Text = "County";
				txtBridesCity.Text = "City";
				txtBridesDOB.Text = "00/00/00";
				txtBridesFathersName.Text = "Father's Name";
				txtBridesFathersBirthplace.Text = "Place of Birth";
				txtBridesMothersName.Text = "Mother's Name";
				txtBridesMothersBirthplace.Text = "Place of Birth";
				txtGroomsMarriageNumber.Text = "First";
				txtBridesMarriageNumber.Text = "First";
				txtBrideEndedAnnulment.Visible = true;
				txtBrideEndedDeath.Visible = true;
				txtBrideEndedDivorce.Visible = true;
				txtGroomEndedAnnulment.Visible = true;
				txtGroomEndedDeath.Visible = true;
				txtGroomEndedDivorce.Visible = true;
				txtGroomsMarriageEnded.Text = "00";
				txtGroomsMarriageEndedDay.Text = "00";
				txtGroomsMarriageEndedYear.Text = "00";
				txtGroomsMarriageEnded.Visible = true;
				txtGroomsMarriageEndedDay.Visible = true;
				txtGroomsMarriageEndedYear.Visible = true;
				txtBridesMarriageEnded.Visible = true;
				txtBridesMarriageEndedDay.Visible = true;
				txtBridesMarriageEndedYear.Visible = true;
				txtBridesMarriageEnded.Text = "00";
				txtBridesMarriageEndedDay.Text = "00";
				txtBridesMarriageEndedYear.Text = "00";
			}
		}

		
	}
}
