﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMonthlyDogReport.
	/// </summary>
	partial class rptMonthlyDogReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMonthlyDogReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAttestedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMF1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMF2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNeuter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNeuter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMFTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNeuterTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKennel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKennel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKennelTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKennelSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNeuterSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMFSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReportTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAdjustments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCorrections1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCorrections2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReason = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReplacement1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReplacement2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSandR1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSandR2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHearingGuide1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHearingGuide2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTransfers1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTransfers2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDogKennel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDogKennel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDogTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDogTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReplacementTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTagOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTagTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttestedBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMF1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMF2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuter1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuter2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMFTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuterTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKennel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKennel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKennelTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKennelSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuterSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMFSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReportTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCorrections1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCorrections2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacement1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacement2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSandR1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSandR2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransfers1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransfers2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogKennel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogKennel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacementTag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewTag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Height = 0F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.BeforePrint += new System.EventHandler(this.PageHeader_BeforePrint);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMonth,
				this.txtAttestedBy,
				this.txtMuniname,
				this.txtMF1,
				this.txtMF2,
				this.txtNeuter1,
				this.txtNeuter2,
				this.txtMFTotal,
				this.txtNeuterTotal,
				this.txtKennel1,
				this.txtKennel2,
				this.txtKennelTotal,
				this.txtKennelSummary,
				this.txtNeuterSummary,
				this.txtMFSummary,
				this.txtAddress1,
				this.txtPhone,
				this.txtAddress2,
				this.txtReportTotal,
				this.txtAdjustments,
				this.txtCorrections1,
				this.txtCorrections2,
				this.txtReason,
				this.txtReplacement1,
				this.txtReplacement2,
				this.txtSandR1,
				this.txtSandR2,
				this.txtHearingGuide1,
				this.txtHearingGuide2,
				this.txtTransfers1,
				this.txtTransfers2,
				this.txtDogKennel1,
				this.txtDogKennel2,
				this.txtDogTotal1,
				this.txtDogTotal2,
				this.txtReplacementTag,
				this.txtNewTag,
				this.txtTagOther,
				this.txtTagTotal
			});
			this.PageHeader.Height = 6.53125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMonth
			// 
			this.txtMonth.Height = 0.1875F;
			this.txtMonth.Left = 3.6875F;
			this.txtMonth.Name = "txtMonth";
			this.txtMonth.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMonth.Text = "Month";
			this.txtMonth.Top = 0F;
			this.txtMonth.Width = 0.4375F;
			// 
			// txtAttestedBy
			// 
			this.txtAttestedBy.Height = 0.1875F;
			this.txtAttestedBy.Left = 4F;
			this.txtAttestedBy.Name = "txtAttestedBy";
			this.txtAttestedBy.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAttestedBy.Text = "Attest";
			this.txtAttestedBy.Top = 0.4375F;
			this.txtAttestedBy.Width = 2.25F;
			// 
			// txtMuniName
			// 
			this.txtMuniname.Height = 0.1875F;
			this.txtMuniname.Left = 4F;
			this.txtMuniname.Name = "txtMuniName";
			this.txtMuniname.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMuniname.Text = "Muni";
			this.txtMuniname.Top = 0.25F;
			this.txtMuniname.Width = 2.25F;
			// 
			// txtMF1
			// 
			this.txtMF1.Height = 0.1875F;
			this.txtMF1.Left = 1.8125F;
			this.txtMF1.Name = "txtMF1";
			this.txtMF1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMF1.Text = null;
			this.txtMF1.Top = 1.9375F;
			this.txtMF1.Width = 0.4375F;
			// 
			// txtMF2
			// 
			this.txtMF2.Height = 0.1875F;
			this.txtMF2.Left = 2.3125F;
			this.txtMF2.Name = "txtMF2";
			this.txtMF2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMF2.Text = null;
			this.txtMF2.Top = 1.9375F;
			this.txtMF2.Width = 0.4375F;
			// 
			// txtNeuter1
			// 
			this.txtNeuter1.Height = 0.1875F;
			this.txtNeuter1.Left = 1.8125F;
			this.txtNeuter1.Name = "txtNeuter1";
			this.txtNeuter1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtNeuter1.Text = null;
			this.txtNeuter1.Top = 2.125F;
			this.txtNeuter1.Width = 0.4375F;
			// 
			// txtNeuter2
			// 
			this.txtNeuter2.Height = 0.1875F;
			this.txtNeuter2.Left = 2.3125F;
			this.txtNeuter2.Name = "txtNeuter2";
			this.txtNeuter2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtNeuter2.Text = null;
			this.txtNeuter2.Top = 2.125F;
			this.txtNeuter2.Width = 0.4375F;
			// 
			// txtMFTotal
			// 
			this.txtMFTotal.Height = 0.1875F;
			this.txtMFTotal.Left = 3.25F;
			this.txtMFTotal.Name = "txtMFTotal";
			this.txtMFTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMFTotal.Text = null;
			this.txtMFTotal.Top = 1.9375F;
			this.txtMFTotal.Width = 0.75F;
			// 
			// txtNeuterTotal
			// 
			this.txtNeuterTotal.Height = 0.1875F;
			this.txtNeuterTotal.Left = 3.25F;
			this.txtNeuterTotal.Name = "txtNeuterTotal";
			this.txtNeuterTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtNeuterTotal.Text = null;
			this.txtNeuterTotal.Top = 2.125F;
			this.txtNeuterTotal.Width = 0.75F;
			// 
			// txtKennel1
			// 
			this.txtKennel1.Height = 0.1875F;
			this.txtKennel1.Left = 1.8125F;
			this.txtKennel1.Name = "txtKennel1";
			this.txtKennel1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtKennel1.Text = null;
			this.txtKennel1.Top = 2.5F;
			this.txtKennel1.Width = 0.4375F;
			// 
			// txtKennel2
			// 
			this.txtKennel2.Height = 0.1875F;
			this.txtKennel2.Left = 2.3125F;
			this.txtKennel2.Name = "txtKennel2";
			this.txtKennel2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtKennel2.Text = null;
			this.txtKennel2.Top = 2.5F;
			this.txtKennel2.Width = 0.4375F;
			// 
			// txtKennelTotal
			// 
			this.txtKennelTotal.Height = 0.1875F;
			this.txtKennelTotal.Left = 3.625F;
			this.txtKennelTotal.Name = "txtKennelTotal";
			this.txtKennelTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtKennelTotal.Text = null;
			this.txtKennelTotal.Top = 2.5F;
			this.txtKennelTotal.Width = 0.75F;
			// 
			// txtKennelSummary
			// 
			this.txtKennelSummary.Height = 0.1875F;
			this.txtKennelSummary.Left = 5.8125F;
			this.txtKennelSummary.Name = "txtKennelSummary";
			this.txtKennelSummary.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtKennelSummary.Text = null;
			this.txtKennelSummary.Top = 2.5F;
			this.txtKennelSummary.Width = 0.75F;
			// 
			// txtNeuterSummary
			// 
			this.txtNeuterSummary.Height = 0.1875F;
			this.txtNeuterSummary.Left = 5.8125F;
			this.txtNeuterSummary.Name = "txtNeuterSummary";
			this.txtNeuterSummary.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtNeuterSummary.Text = null;
			this.txtNeuterSummary.Top = 2.125F;
			this.txtNeuterSummary.Width = 0.75F;
			// 
			// txtMFSummary
			// 
			this.txtMFSummary.Height = 0.1875F;
			this.txtMFSummary.Left = 5.8125F;
			this.txtMFSummary.Name = "txtMFSummary";
			this.txtMFSummary.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMFSummary.Text = null;
			this.txtMFSummary.Top = 1.9375F;
			this.txtMFSummary.Width = 0.75F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.1875F;
			this.txtAddress1.Left = 4F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAddress1.Text = "Attest";
			this.txtAddress1.Top = 0.625F;
			this.txtAddress1.Width = 2.25F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1875F;
			this.txtPhone.Left = 4.5625F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtPhone.Text = "Attest";
			this.txtPhone.Top = 1F;
			this.txtPhone.Width = 1.6875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.1875F;
			this.txtAddress2.Left = 4F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAddress2.Text = "Attest";
			this.txtAddress2.Top = 0.8125F;
			this.txtAddress2.Width = 2.25F;
			// 
			// txtReportTotal
			// 
			this.txtReportTotal.Height = 0.1875F;
			this.txtReportTotal.Left = 5.8125F;
			this.txtReportTotal.Name = "txtReportTotal";
			this.txtReportTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtReportTotal.Text = null;
			this.txtReportTotal.Top = 3.5F;
			this.txtReportTotal.Width = 0.75F;
			// 
			// txtAdjustments
			// 
			this.txtAdjustments.Height = 0.1875F;
			this.txtAdjustments.Left = 5.8125F;
			this.txtAdjustments.Name = "txtAdjustments";
			this.txtAdjustments.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAdjustments.Text = null;
			this.txtAdjustments.Top = 2.9375F;
			this.txtAdjustments.Width = 0.75F;
			// 
			// txtCorrections1
			// 
			this.txtCorrections1.Height = 0.1875F;
			this.txtCorrections1.Left = 1.8125F;
			this.txtCorrections1.Name = "txtCorrections1";
			this.txtCorrections1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtCorrections1.Text = null;
			this.txtCorrections1.Top = 2.9375F;
			this.txtCorrections1.Width = 0.4375F;
			// 
			// txtCorrections2
			// 
			this.txtCorrections2.Height = 0.1875F;
			this.txtCorrections2.Left = 2.3125F;
			this.txtCorrections2.Name = "txtCorrections2";
			this.txtCorrections2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtCorrections2.Text = null;
			this.txtCorrections2.Top = 2.9375F;
			this.txtCorrections2.Width = 0.4375F;
			// 
			// txtReason
			// 
			this.txtReason.Height = 0.1875F;
			this.txtReason.Left = 1.625F;
			this.txtReason.Name = "txtReason";
			this.txtReason.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtReason.Text = null;
			this.txtReason.Top = 3.1875F;
			this.txtReason.Width = 4.1875F;
			// 
			// txtReplacement1
			// 
			this.txtReplacement1.Height = 0.1875F;
			this.txtReplacement1.Left = 1.8125F;
			this.txtReplacement1.Name = "txtReplacement1";
			this.txtReplacement1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtReplacement1.Text = null;
			this.txtReplacement1.Top = 3.6875F;
			this.txtReplacement1.Width = 0.4375F;
			// 
			// txtReplacement2
			// 
			this.txtReplacement2.Height = 0.1875F;
			this.txtReplacement2.Left = 2.3125F;
			this.txtReplacement2.Name = "txtReplacement2";
			this.txtReplacement2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtReplacement2.Text = null;
			this.txtReplacement2.Top = 3.6875F;
			this.txtReplacement2.Width = 0.4375F;
			// 
			// txtSandR1
			// 
			this.txtSandR1.Height = 0.1875F;
			this.txtSandR1.Left = 1.8125F;
			this.txtSandR1.Name = "txtSandR1";
			this.txtSandR1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSandR1.Text = null;
			this.txtSandR1.Top = 3.875F;
			this.txtSandR1.Width = 0.4375F;
			// 
			// txtSandR2
			// 
			this.txtSandR2.Height = 0.1875F;
			this.txtSandR2.Left = 2.3125F;
			this.txtSandR2.Name = "txtSandR2";
			this.txtSandR2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSandR2.Text = null;
			this.txtSandR2.Top = 3.875F;
			this.txtSandR2.Width = 0.4375F;
			// 
			// txtHearingGuide1
			// 
			this.txtHearingGuide1.Height = 0.1875F;
			this.txtHearingGuide1.Left = 1.8125F;
			this.txtHearingGuide1.Name = "txtHearingGuide1";
			this.txtHearingGuide1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtHearingGuide1.Text = null;
			this.txtHearingGuide1.Top = 4.0625F;
			this.txtHearingGuide1.Width = 0.4375F;
			// 
			// txtHearingGuide2
			// 
			this.txtHearingGuide2.Height = 0.1875F;
			this.txtHearingGuide2.Left = 2.3125F;
			this.txtHearingGuide2.Name = "txtHearingGuide2";
			this.txtHearingGuide2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtHearingGuide2.Text = null;
			this.txtHearingGuide2.Top = 4.0625F;
			this.txtHearingGuide2.Width = 0.4375F;
			// 
			// txtTransfers1
			// 
			this.txtTransfers1.Height = 0.1875F;
			this.txtTransfers1.Left = 1.8125F;
			this.txtTransfers1.Name = "txtTransfers1";
			this.txtTransfers1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTransfers1.Text = null;
			this.txtTransfers1.Top = 4.25F;
			this.txtTransfers1.Width = 0.4375F;
			// 
			// txtTransfers2
			// 
			this.txtTransfers2.Height = 0.1875F;
			this.txtTransfers2.Left = 2.3125F;
			this.txtTransfers2.Name = "txtTransfers2";
			this.txtTransfers2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTransfers2.Text = null;
			this.txtTransfers2.Top = 4.25F;
			this.txtTransfers2.Width = 0.4375F;
			// 
			// txtDogKennel1
			// 
			this.txtDogKennel1.Height = 0.1875F;
			this.txtDogKennel1.Left = 1.8125F;
			this.txtDogKennel1.Name = "txtDogKennel1";
			this.txtDogKennel1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDogKennel1.Text = null;
			this.txtDogKennel1.Top = 4.75F;
			this.txtDogKennel1.Width = 0.4375F;
			// 
			// txtDogKennel2
			// 
			this.txtDogKennel2.Height = 0.1875F;
			this.txtDogKennel2.Left = 2.3125F;
			this.txtDogKennel2.Name = "txtDogKennel2";
			this.txtDogKennel2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDogKennel2.Text = null;
			this.txtDogKennel2.Top = 4.75F;
			this.txtDogKennel2.Width = 0.4375F;
			// 
			// txtDogTotal1
			// 
			this.txtDogTotal1.Height = 0.1875F;
			this.txtDogTotal1.Left = 1.875F;
			this.txtDogTotal1.Name = "txtDogTotal1";
			this.txtDogTotal1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDogTotal1.Text = null;
			this.txtDogTotal1.Top = 5.4375F;
			this.txtDogTotal1.Width = 0.4375F;
			// 
			// txtDogTotal2
			// 
			this.txtDogTotal2.Height = 0.1875F;
			this.txtDogTotal2.Left = 2.375F;
			this.txtDogTotal2.Name = "txtDogTotal2";
			this.txtDogTotal2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDogTotal2.Text = null;
			this.txtDogTotal2.Top = 5.4375F;
			this.txtDogTotal2.Width = 0.4375F;
			// 
			// txtReplacementTag
			// 
			this.txtReplacementTag.Height = 0.2083333F;
			this.txtReplacementTag.Left = 0.5625F;
			this.txtReplacementTag.Name = "txtReplacementTag";
			this.txtReplacementTag.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtReplacementTag.Text = null;
			this.txtReplacementTag.Top = 2.083333F;
			this.txtReplacementTag.Width = 0.4375F;
			// 
			// txtNewTag
			// 
			this.txtNewTag.Height = 0.2083333F;
			this.txtNewTag.Left = 0.5625F;
			this.txtNewTag.Name = "txtNewTag";
			this.txtNewTag.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtNewTag.Text = null;
			this.txtNewTag.Top = 1.833333F;
			this.txtNewTag.Width = 0.4375F;
			// 
			// txtTagOther
			// 
			this.txtTagOther.Height = 0.2083333F;
			this.txtTagOther.Left = 0.5625F;
			this.txtTagOther.Name = "txtTagOther";
			this.txtTagOther.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTagOther.Text = null;
			this.txtTagOther.Top = 2.291667F;
			this.txtTagOther.Width = 0.4375F;
			// 
			// txtTagTotal
			// 
			this.txtTagTotal.Height = 0.2083333F;
			this.txtTagTotal.Left = 0.5625F;
			this.txtTagTotal.Name = "txtTagTotal";
			this.txtTagTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTagTotal.Text = null;
			this.txtTagTotal.Top = 2.541667F;
			this.txtTagTotal.Width = 0.4375F;
			// 
			// rptMonthlyDogReport
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.625F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttestedBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMF1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMF2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuter1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuter2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMFTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuterTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKennel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKennel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKennelTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKennelSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeuterSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMFSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReportTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCorrections1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCorrections2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacement1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacement2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSandR1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSandR2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHearingGuide2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransfers1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransfers2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogKennel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogKennel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDogTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReplacementTag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewTag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTagTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttestedBy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMF1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMF2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMFTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennel1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennel2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennelTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKennelSummary;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeuterSummary;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMFSummary;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReportTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAdjustments;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCorrections1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCorrections2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReason;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacement1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacement2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSandR1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSandR2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearingGuide1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHearingGuide2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransfers1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTransfers2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogKennel1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogKennel2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDogTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReplacementTag;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewTag;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTagTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
