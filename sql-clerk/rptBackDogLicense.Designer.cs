﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBackDogLicense.
	/// </summary>
	partial class rptBackDogLicense
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBackDogLicense));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtVet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVetOrNeuterSpay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwnerKeeper = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExpirationDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSignature = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAmountCharged = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIssueDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCertNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLateFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExpirationDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnSignature = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtVet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVetOrNeuterSpay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerKeeper)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpirationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountCharged)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssueDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSticker)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLateFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpirationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAmountCharged,
				this.txtIssueDate,
				this.txtYear,
				this.txtSticker,
				this.txtCertNumber,
				this.txtExpires,
				this.txtLateFee,
				this.txtExpirationDate,
				this.lnSignature
			});
			this.Detail.Height = 0.3020833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtVet,
				this.txtOwnerName
			});
			this.PageHeader.Height = 0.4895833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.05208333F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.CanGrow = false;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtVetOrNeuterSpay,
				this.txtOwnerKeeper,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.lblExpirationDate,
				this.lblSignature
			});
			this.GroupHeader1.Height = 0.6104167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Visible = false;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtVet
			// 
			this.txtVet.Height = 0.1875F;
			this.txtVet.Left = 1.5625F;
			this.txtVet.Name = "txtVet";
			this.txtVet.Text = null;
			this.txtVet.Top = 0.1875F;
			this.txtVet.Width = 3.1875F;
			// 
			// txtOwnerName
			// 
			this.txtOwnerName.Height = 0.1875F;
			this.txtOwnerName.Left = 4.9375F;
			this.txtOwnerName.Name = "txtOwnerName";
			this.txtOwnerName.Text = null;
			this.txtOwnerName.Top = 0.1875F;
			this.txtOwnerName.Width = 2.625F;
			// 
			// txtVetOrNeuterSpay
			// 
			this.txtVetOrNeuterSpay.Height = 0.1875F;
			this.txtVetOrNeuterSpay.Left = 1.5625F;
			this.txtVetOrNeuterSpay.Name = "txtVetOrNeuterSpay";
			this.txtVetOrNeuterSpay.Text = null;
			this.txtVetOrNeuterSpay.Top = 0F;
			this.txtVetOrNeuterSpay.Width = 1.8125F;
			// 
			// txtOwnerKeeper
			// 
			this.txtOwnerKeeper.Height = 0.1875F;
			this.txtOwnerKeeper.Left = 4.9375F;
			this.txtOwnerKeeper.Name = "txtOwnerKeeper";
			this.txtOwnerKeeper.Text = null;
			this.txtOwnerKeeper.Top = 0F;
			this.txtOwnerKeeper.Width = 2.625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 8.5pt";
			this.Label1.Text = "Owner/Keeper Name";
			this.Label1.Top = 0F;
			this.Label1.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 8.5pt";
			this.Label2.Text = "Neuter/Spay Cert. # or Vet";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.28125F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; text-align: center";
			this.Label3.Text = "Issue Date";
			this.Label3.Top = 0.1875F;
			this.Label3.Width = 0.4375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8.5pt";
			this.Label4.Text = "Sticker #";
			this.Label4.Top = 0.21875F;
			this.Label4.Width = 0.8125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.4375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 8.5pt";
			this.Label5.Text = "Rabies Cert. #";
			this.Label5.Top = 0.21875F;
			this.Label5.Width = 0.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.28125F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 3.375F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 8.5pt; text-align: center";
			this.Label6.Text = "Rabies Expires";
			this.Label6.Top = 0.1875F;
			this.Label6.Width = 0.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.28125F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 8.5pt; text-align: center";
			this.Label7.Text = "License Fee";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 0.5625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.28125F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.6875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 8.5pt; text-align: center";
			this.Label8.Text = "Late Fee";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 0.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.28125F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.5625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 8.5pt; text-align: center";
			this.Label9.Text = "Licensing Year";
			this.Label9.Top = 0.1875F;
			this.Label9.Width = 0.5625F;
			// 
			// lblExpirationDate
			// 
			this.lblExpirationDate.Height = 0.281F;
			this.lblExpirationDate.HyperLink = null;
			this.lblExpirationDate.Left = 5.166667F;
			this.lblExpirationDate.Name = "lblExpirationDate";
			this.lblExpirationDate.Style = "font-size: 8.5pt; text-align: center";
			this.lblExpirationDate.Text = "Expiration Date";
			this.lblExpirationDate.Top = 0.1875F;
			this.lblExpirationDate.Visible = false;
			this.lblExpirationDate.Width = 0.5833333F;
			// 
			// lblSignature
			// 
			this.lblSignature.Height = 0.1666667F;
			this.lblSignature.HyperLink = null;
			this.lblSignature.Left = 5.916667F;
			this.lblSignature.Name = "lblSignature";
			this.lblSignature.Style = "font-size: 8.5pt; text-align: center";
			this.lblSignature.Text = "Owner/Keeper Signature";
			this.lblSignature.Top = 0.21875F;
			this.lblSignature.Visible = false;
			this.lblSignature.Width = 1.666667F;
			// 
			// txtAmountCharged
			// 
			this.txtAmountCharged.Height = 0.1875F;
			this.txtAmountCharged.Left = 4.125F;
			this.txtAmountCharged.Name = "txtAmountCharged";
			this.txtAmountCharged.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtAmountCharged.Text = null;
			this.txtAmountCharged.Top = 0.0625F;
			this.txtAmountCharged.Width = 0.5902778F;
			// 
			// txtIssueDate
			// 
			this.txtIssueDate.Height = 0.1875F;
			this.txtIssueDate.Left = 0.0625F;
			this.txtIssueDate.Name = "txtIssueDate";
			this.txtIssueDate.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtIssueDate.Text = null;
			this.txtIssueDate.Top = 0.0625F;
			this.txtIssueDate.Width = 1F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1875F;
			this.txtYear.Left = 0.625F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtYear.Text = null;
			this.txtYear.Top = 0.0625F;
			this.txtYear.Width = 0.4375F;
			// 
			// txtSticker
			// 
			this.txtSticker.Height = 0.1875F;
			this.txtSticker.Left = 1.1875F;
			this.txtSticker.Name = "txtSticker";
			this.txtSticker.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtSticker.Text = null;
			this.txtSticker.Top = 0.0625F;
			this.txtSticker.Width = 1.1875F;
			// 
			// txtCertNumber
			// 
			this.txtCertNumber.Height = 0.1875F;
			this.txtCertNumber.Left = 2.4375F;
			this.txtCertNumber.Name = "txtCertNumber";
			this.txtCertNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtCertNumber.Text = null;
			this.txtCertNumber.Top = 0.0625F;
			this.txtCertNumber.Width = 0.875F;
			// 
			// txtExpires
			// 
			this.txtExpires.Height = 0.1875F;
			this.txtExpires.Left = 3.375F;
			this.txtExpires.Name = "txtExpires";
			this.txtExpires.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtExpires.Text = null;
			this.txtExpires.Top = 0.0625F;
			this.txtExpires.Width = 0.6875F;
			// 
			// txtLateFee
			// 
			this.txtLateFee.Height = 0.1875F;
			this.txtLateFee.Left = 4.6875F;
			this.txtLateFee.Name = "txtLateFee";
			this.txtLateFee.Style = "font-size: 9pt";
			this.txtLateFee.Text = null;
			this.txtLateFee.Top = 0.0625F;
			this.txtLateFee.Width = 0.4375F;
			// 
			// txtExpirationDate
			// 
			this.txtExpirationDate.Height = 0.1666667F;
			this.txtExpirationDate.Left = 5.166667F;
			this.txtExpirationDate.Name = "txtExpirationDate";
			this.txtExpirationDate.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtExpirationDate.Text = null;
			this.txtExpirationDate.Top = 0.08333334F;
			this.txtExpirationDate.Visible = false;
			this.txtExpirationDate.Width = 1F;
			// 
			// lnSignature
			// 
			this.lnSignature.Height = 0F;
			this.lnSignature.Left = 5.916667F;
			this.lnSignature.LineWeight = 1F;
			this.lnSignature.Name = "lnSignature";
			this.lnSignature.Top = 0.25F;
			this.lnSignature.Visible = false;
			this.lnSignature.Width = 1.666667F;
			this.lnSignature.X1 = 5.916667F;
			this.lnSignature.X2 = 7.583333F;
			this.lnSignature.Y1 = 0.25F;
			this.lnSignature.Y2 = 0.25F;
			// 
			// rptBackDogLicense
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.3472222F;
			this.PageSettings.Margins.Right = 0.3472222F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.666667F;
			this.Script = "\r\nSub ActiveReport_ReportStart\r\n\r\nEnd Sub\r\n";
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtVet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVetOrNeuterSpay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwnerKeeper)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpirationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountCharged)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssueDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSticker)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLateFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpirationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountCharged;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssueDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCertNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExpires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLateFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExpirationDate;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSignature;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVetOrNeuterSpay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwnerKeeper;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExpirationDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSignature;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
