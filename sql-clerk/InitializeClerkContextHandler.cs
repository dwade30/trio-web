﻿using Global;
using SharedApplication.Clerk.Commands;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace TWCK0000
{
    public class InitializeClerkContextHandler : CommandHandler<InitializeClerkContext>
    {
        protected override void Handle(InitializeClerkContext command)
        {
            modStartup.InitializeClerkStatics();
        }
    }
}