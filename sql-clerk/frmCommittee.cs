//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmCommittee : BaseForm
	{
		public frmCommittee()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			Label1 = new System.Collections.Generic.List<FCLabel>();
			Label1.AddControlArrayElement(Label1_0, 0);
			Label1.AddControlArrayElement(Label1_1, 1);
			Label1.AddControlArrayElement(Label1_2, 2);
			Label1.AddControlArrayElement(Label1_3, 3);
			Label1.AddControlArrayElement(Label1_4, 4);
			Label1.AddControlArrayElement(Label1_5, 5);
			Label1.AddControlArrayElement(Label1_6, 6);
			Label1.AddControlArrayElement(Label1_7, 7);
			Label1.AddControlArrayElement(Label1_8, 8);
			Label1.AddControlArrayElement(Label1_9, 9);
			Label1.AddControlArrayElement(Label1_10, 10);
			Label1.AddControlArrayElement(Label1_11, 11);
			Label1.AddControlArrayElement(Label1_12, 12);
			Label1.AddControlArrayElement(Label1_13, 13);
			Label1.AddControlArrayElement(Label1_14, 14);
			Label1.AddControlArrayElement(Label1_15, 15);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCommittee InstancePtr
		{
			get
			{
				return (frmCommittee)Sys.GetInstance(typeof(frmCommittee));
			}
		}

		protected frmCommittee _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDGROUPLISTCOLID = 0;
		const int CNSTGRIDGROUPLISTCOLGROUPNAME = 1;
		const int CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION = 2;
		const int CNSTGRIDGROUPLISTCOLGROUPDESCRIPTION = 3;
		const int CNSTGRIDGROUPCOLID = 0;
		const int CNSTGRIDGROUPCOLNAME = 1;
		const int CNSTGRIDGROUPCOLADDRESS1 = 2;
		const int CNSTGRIDGROUPCOLADDRESS2 = 3;
		const int CNSTGRIDGROUPCOLCITY = 4;
		const int CNSTGRIDGROUPCOLSTATE = 5;
		const int CNSTGRIDGROUPCOLZIP = 6;
		const int CNSTGRIDGROUPCOLZIP4 = 7;
		const int CNSTGRIDGROUPCOLEMAIL = 8;
		const int CNSTGRIDGROUPCOLPHONE = 9;
		const int CNSTGRIDGROUPCOLTITLE = 10;
		const int CNSTGRIDGROUPCOLFROM = 11;
		const int CNSTGRIDGROUPCOLTO = 12;
		const int CNSTGRIDGROUPCOLUSERDEFINED = 13;
		const int CNSTGRIDGROUPCOLGROUPID = 14;
		const int CNSTGRIDGROUPCOLHOMEPHONE = 15;
		const int CNSTGRIDGROUPCOLCELLPHONE = 16;
		const int CNSTGRIDGROUPCOLOTHERPHONE = 17;
		int lngTempGroupID;
		int lngCurrentGroup;
		int lngCurrentMemberRow;
		bool blnNotSaved;
		int NameCol;
		int Address1Col;
		int Address2Col;
		int CityCol;
		int StateCol;
		int ZipCol;
		bool boolChangesDontCount;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraExistingMembers.Visible = false;
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (vsMembers.Row > 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM GroupMembers WHERE rtrim(isnull(Name, '')) = '" + fecherFoundation.Strings.Trim(vsMembers.TextMatrix(vsMembers.Row, NameCol)) + "' AND rtrim(isnull(Address1, '')) = '" + fecherFoundation.Strings.Trim(vsMembers.TextMatrix(vsMembers.Row, Address1Col)) + "' AND rtrim(isnull(Address2, '')) = '" + fecherFoundation.Strings.Trim(vsMembers.TextMatrix(vsMembers.Row, Address2Col)) + "' AND rtrim(isnull(City, '')) = '" + fecherFoundation.Strings.Trim(vsMembers.TextMatrix(vsMembers.Row, CityCol)) + "' AND rtrim(isnull(State, '')) = '" + fecherFoundation.Strings.Trim(vsMembers.TextMatrix(vsMembers.Row, StateCol)) + "'", "TWCK0000.vb1");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					mnuAddMember_Click();
					txtTitle.Text = FCConvert.ToString(rsInfo.Get_Fields("Title"));
					txtAddress1.Text = FCConvert.ToString(rsInfo.Get_Fields_String("Address1"));
					txtAddress2.Text = FCConvert.ToString(rsInfo.Get_Fields_String("Address2"));
					txtCity.Text = FCConvert.ToString(rsInfo.Get_Fields_String("City"));
					txtEMail.Text = FCConvert.ToString(rsInfo.Get_Fields_String("EMail"));
					txtName.Text = FCConvert.ToString(rsInfo.Get_Fields_String("Name"));
					txtState.Text = FCConvert.ToString(rsInfo.Get_Fields("State"));
					txtUserDefined.Text = FCConvert.ToString(rsInfo.Get_Fields_String("UserDefined"));
					txtZip.Text = FCConvert.ToString(rsInfo.Get_Fields_String("Zip"));
					txtZip4.Text = FCConvert.ToString(rsInfo.Get_Fields_String("Zip4"));
					T2KPhone.Text = FCConvert.ToString(rsInfo.Get_Fields_String("Telephone"));
					t2kCellPhone.Text = FCConvert.ToString(rsInfo.Get_Fields_String("CellNumber"));
					t2kHomePhone.Text = FCConvert.ToString(rsInfo.Get_Fields_String("HomeNumber"));
					t2kOtherPhone.Text = FCConvert.ToString(rsInfo.Get_Fields_String("OtherNumber"));
				}
			}
			fraExistingMembers.Visible = false;
			t2kFrom.Focus();
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdOK, new System.EventArgs());
		}

		private void frmCommittee_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCommittee_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCommittee properties;
			//frmCommittee.FillStyle	= 0;
			//frmCommittee.ScaleWidth	= 9300;
			//frmCommittee.ScaleHeight	= 7860;
			//frmCommittee.LinkTopic	= "Form2";
			//frmCommittee.LockControls	= -1  'True;
			//frmCommittee.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridGroupList();
			setupvsmembers();
			SetupGridGroup();
			lngTempGroupID = 0;
			lngCurrentGroup = 0;
			lngCurrentMemberRow = -1;
			LockUnlockBoxes(false);
			FillGridGroupList();
			if (GridGroupList.Rows > 1)
			{
				GridGroupList.Row = 1;
			}
			// vsMembers.ColWidth(NameCol) = vsMembers.Width * 0.2
			// vsMembers.ColWidth(Address1Col) = vsMembers.Width * 0.22
			// vsMembers.ColWidth(Address2Col) = vsMembers.Width * 0.22
			// vsMembers.ColWidth(CityCol) = vsMembers.Width * 0.15
			// vsMembers.ColWidth(StateCol) = vsMembers.Width * 0.1
			// vsMembers.ColWidth(ZipCol) = vsMembers.Width * 0.05
		}

		private void setupvsmembers()
		{
			NameCol = 0;
			Address1Col = 1;
			Address2Col = 2;
			CityCol = 3;
			StateCol = 4;
			ZipCol = 5;
			vsMembers.TextMatrix(0, NameCol, "Name");
			vsMembers.TextMatrix(0, Address1Col, "Address 1");
			vsMembers.TextMatrix(0, Address2Col, "Address 2");
			vsMembers.TextMatrix(0, CityCol, "City");
			vsMembers.TextMatrix(0, StateCol, "State");
			vsMembers.TextMatrix(0, ZipCol, "Zip");
		}

		private void frmCommittee_Resize(object sender, System.EventArgs e)
		{
			ResizeGridGroupList();
			ResizeGridGroup();
			vsMembers.ColWidth(NameCol, FCConvert.ToInt32(vsMembers.WidthOriginal * 0.27));
			vsMembers.ColWidth(Address1Col, FCConvert.ToInt32(vsMembers.WidthOriginal * 0.27));
			vsMembers.ColWidth(Address2Col, FCConvert.ToInt32(vsMembers.WidthOriginal * 0.27));
			vsMembers.ColWidth(CityCol, FCConvert.ToInt32(vsMembers.WidthOriginal * 0.18));
			vsMembers.ColWidth(StateCol, FCConvert.ToInt32(vsMembers.WidthOriginal * 0.07));
			vsMembers.ColWidth(ZipCol, FCConvert.ToInt32(vsMembers.WidthOriginal * 0.13));

            fraExistingMembers.CenterToContainer(fraExistingMembers.Parent);
		}

		private void ClearBoxes()
		{
			LockUnlockBoxes(false);
			t2kFrom.Text = "";
			t2kTo.Text = "";
			T2KPhone.Text = "";
			t2kHomePhone.Text = "";
			t2kCellPhone.Text = "";
			t2kOtherPhone.Text = "";
			txtAddress1.Text = "";
			txtAddress2.Text = "";
			txtCity.Text = "";
			txtEMail.Text = "";
			txtName.Text = "";
			txtState.Text = "";
			txtTitle.Text = "";
			txtUserDefined.Text = "";
			txtZip.Text = "";
			txtZip4.Text = "";
			lngCurrentMemberRow = -1;
		}

		private void GridGroup_RowColChange(object sender, EventArgs e)
		{
			if (GridGroup.Row < 0)
			{
				//FC:FINAL:DDU:#i2349 - fixed saving new member
				ClearBoxes();
				return;
			}
			if (GridGroup.Row != lngCurrentMemberRow)
			{
				UpdateMember(ref lngCurrentMemberRow);
				ClearBoxes();
				lngCurrentMemberRow = GridGroup.Row;
				FillMemberInfo(lngCurrentMemberRow);
			}
		}

		private void FillMemberInfo(int lngRow)
		{
			if (lngRow > 0)
			{
				LockUnlockBoxes(true);
			}
			else
			{
				LockUnlockBoxes(false);
			}
			boolChangesDontCount = true;
			txtTitle.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTITLE);
			txtAddress1.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLADDRESS1);
			txtAddress2.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLADDRESS2);
			txtCity.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLCITY);
			txtEMail.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLEMAIL);
			txtName.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLNAME);
			txtState.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLSTATE);
			txtUserDefined.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLUSERDEFINED);
			txtZip.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLZIP);
			txtZip4.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLZIP4);
			T2KPhone.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLPHONE);
			t2kCellPhone.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLCELLPHONE);
			t2kHomePhone.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLHOMEPHONE);
			t2kOtherPhone.Text = GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLOTHERPHONE);
			if (Information.IsDate(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLFROM)))
			{
				t2kFrom.Text = Strings.Format(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLFROM), "MM/dd/yyyy");
			}
			if (Information.IsDate(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTO)))
			{
				t2kTo.Text = Strings.Format(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTO), "MM/dd/yyyy");
			}
			boolChangesDontCount = false;
		}

		private void UpdateMember(ref int lngRow)
		{
			string strTemp = "";
			if (lngRow < 1)
				return;
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLADDRESS1, fecherFoundation.Strings.Trim(txtAddress1.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLADDRESS2, fecherFoundation.Strings.Trim(txtAddress2.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLCITY, fecherFoundation.Strings.Trim(txtCity.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLEMAIL, fecherFoundation.Strings.Trim(txtEMail.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLNAME, fecherFoundation.Strings.Trim(txtName.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLSTATE, fecherFoundation.Strings.Trim(txtState.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTITLE, fecherFoundation.Strings.Trim(txtTitle.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLUSERDEFINED, fecherFoundation.Strings.Trim(txtUserDefined.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLZIP, fecherFoundation.Strings.Trim(txtZip.Text));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLZIP4, fecherFoundation.Strings.Trim(txtZip4.Text));
			if (Information.IsDate(t2kFrom.Text))
			{
				GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLFROM, Strings.Format(t2kFrom.Text, "MM/dd/yyyy"));
			}
			else
			{
				GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLFROM, "");
			}
			if (Information.IsDate(t2kTo.Text))
			{
				GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTO, Strings.Format(t2kTo.Text, "MM/dd/yyyy"));
			}
			else
			{
				GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTO, "");
			}
			strTemp = T2KPhone.Text;
			strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, "_", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.StrDup(10, "0") + strTemp;
			strTemp = Strings.Right(strTemp, 10);
			strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLPHONE, strTemp);
			strTemp = t2kHomePhone.Text;
			strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, "_", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.StrDup(10, "0") + strTemp;
			strTemp = Strings.Right(strTemp, 10);
			strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLHOMEPHONE, strTemp);
			strTemp = t2kCellPhone.Text;
			strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, "_", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.StrDup(10, "0") + strTemp;
			strTemp = Strings.Right(strTemp, 10);
			strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLCELLPHONE, strTemp);
			strTemp = t2kOtherPhone.Text;
			strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = Strings.Replace(strTemp, "_", "", 1, -1, CompareConstants.vbTextCompare);
			strTemp = fecherFoundation.Strings.Trim(strTemp);
			strTemp = Strings.StrDup(10, "0") + strTemp;
			strTemp = Strings.Right(strTemp, 10);
			strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLOTHERPHONE, strTemp);
		}

		private void GridGroupList_RowColChange(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			int x;
			lngRow = -1;
			if (Conversion.Val(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLID)) != lngCurrentGroup)
			{
				UpdateMember(ref lngCurrentMemberRow);
				lngCurrentMemberRow = -1;
				ClearBoxes();
				if (fecherFoundation.Strings.Trim(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION)) != string.Empty)
				{
					Label1[7].Text = GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION);
				}
				else
				{
					Label1[7].Text = "User Defined";
				}
				GridGroup.RowHidden(-1, true);
				//FC:FINAL:DDU:#i2307 - show Column headers
				//GridGroup.RowHidden(0, false);
				for (x = 1; x <= GridGroup.Rows - 1; x++)
				{
					if (Conversion.Val(GridGroup.TextMatrix(x, CNSTGRIDGROUPCOLGROUPID)) == Conversion.Val(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLID)))
					{
						GridGroup.RowHidden(x, false);
						if (lngRow < 0)
							lngRow = x;
					}
				}
				// x
				GridGroup.Row = -1;
				if (lngRow > 0)
				{
					GridGroup.Row = lngRow;
					// lngCurrentMemberRow = lngRow
				}
				lngCurrentGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLID))));
			}
		}

		private void GridGroupList_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GridGroupList.Row > 0)
			{
				GridGroupList.RowData(GridGroupList.Row, true);
			}
		}

		private void mnuAddExistingMember_Click(object sender, System.EventArgs e)
		{
			LoadExistingMembersGrid();
			fraExistingMembers.Visible = true;
			vsMembers.Focus();
		}

		private void mnuAddGroup_Click(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			lngTempGroupID -= 1;
			GridGroupList.Rows += 1;
			lngRow = GridGroupList.Rows - 1;
			GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLID, FCConvert.ToString(lngTempGroupID));
			GridGroupList.TopRow = lngRow;
			GridGroupList.RowData(lngRow, true);
			// edited
			blnNotSaved = true;
		}

		private void mnuAddMember_Click(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			if (GridGroupList.Row < 1)
				return;
			GridGroup.Rows += 1;
			lngRow = GridGroup.Rows - 1;
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLID, FCConvert.ToString(0));
			GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLGROUPID, FCConvert.ToString(lngCurrentGroup));
			GridGroup.TopRow = lngRow;
			GridGroup.RowData(lngRow, true);
			// edited
			GridGroup.Row = GridGroup.Rows - 1;
			blnNotSaved = true;
		}

		public void mnuAddMember_Click()
		{
			mnuAddMember_Click(mnuAddMember, new System.EventArgs());
		}

		private void mnuDeleteGroup_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWrite(DialogResult, int)
			DialogResult ans;
			int lngRow = 0;
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (GridGroupList.Row < 1)
			{
				return;
			}
			ans = MessageBox.Show("You will not be able to retrieve this information once it is deleted.  Do you wish to continue?", "Delete Group?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.No)
			{
				return;
			}
			rsDelete.Execute("DELETE FROM GroupMembers WHERE GroupID = " + FCConvert.ToString(Conversion.Val(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLID))), modGNBas.DEFAULTCLERKDATABASE);
			rsDelete.Execute("DELETE FROM GroupList WHERE ID = " + FCConvert.ToString(Conversion.Val(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLID))), modGNBas.DEFAULTCLERKDATABASE);
			// GridDeletedGroups.AddItem Val(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLID))
			int x = 0;
			for (x = GridGroup.Rows - 1; x >= 1; x--)
			{
				if (Conversion.Val(GridGroup.TextMatrix(x, CNSTGRIDGROUPCOLGROUPID)) == lngCurrentGroup)
				{
					GridGroup.RemoveItem(x);
				}
			}
			// x
			lngCurrentGroup = -1;
			lngCurrentMemberRow = -1;
			ClearBoxes();
			x = GridGroupList.Row;
			GridGroupList.RemoveItem(x);
			if (GridGroupList.Rows > 1)
			{
				if (x == 1)
				{
					GridGroupList.Row = 1;
				}
				else
				{
					GridGroupList.Row = x - 1;
				}
				if (fecherFoundation.Strings.Trim(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION)) != string.Empty)
				{
					Label1[7].Text = GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION);
				}
				else
				{
					Label1[7].Text = "User Defined";
				}
				GridGroup.RowHidden(-1, true);
				//FC:FINAL:DDU:#i2307 - show Column headers
				//GridGroup.RowHidden(0, false);
				lngRow = -1;
				for (x = 1; x <= (GridGroup.Rows - 1); x++)
				{
					if (Conversion.Val(GridGroup.TextMatrix(x, CNSTGRIDGROUPCOLGROUPID)) == Conversion.Val(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLID)))
					{
						GridGroup.RowHidden(x, false);
						if (lngRow < 0)
							lngRow = x;
					}
				}
				// x
				GridGroup.Row = -1;
				if (lngRow > 0)
				{
					GridGroup.Row = lngRow;
					// lngCurrentMemberRow = lngRow
				}
				lngCurrentGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGroupList.TextMatrix(GridGroupList.Row, CNSTGRIDGROUPLISTCOLID))));
			}
			blnNotSaved = true;
		}

		private void mnuDeleteMember_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			if (GridGroup.Row < 1)
			{
				return;
			}
			gridDeletedMembers.AddItem(FCConvert.ToString(Conversion.Val(GridGroup.TextMatrix(GridGroup.Row, CNSTGRIDGROUPCOLID))));
			GridGroup.RemoveItem(GridGroup.Row);
			for (x = 1; x <= (GridGroup.Rows - 1); x++)
			{
				if (!GridGroup.RowHidden(x))
				{
					lngCurrentMemberRow = -1;
					GridGroup.Row = x;
					ClearBoxes();
					lngCurrentMemberRow = x;
					FillMemberInfo(lngCurrentMemberRow);
					return;
				}
			}
			// x
			blnNotSaved = true;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			if (blnNotSaved == true)
			{
				if (MessageBox.Show("Current changes have not been saved. Do so now?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					GridGroup.Row = 0;
					if (GridGroupList.Col == CNSTGRIDGROUPLISTCOLGROUPNAME)
					{
						GridGroupList.Col = CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION;
					}
					else
					{
						GridGroupList.Col = CNSTGRIDGROUPLISTCOLGROUPNAME;
					}
					//App.DoEvents();
					if (SaveInfo())
					{
						Close();
					}
				}
				else
				{
					Close();
				}
			}
			else
			{
				Close();
			}
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGridGroupList()
		{
			GridGroupList.Cols = 4;
			GridGroupList.Rows = 1;
			GridGroupList.FixedRows = 1;
			GridGroupList.FixedCols = 0;
			GridGroupList.ExtendLastCol = true;
			GridGroupList.ColHidden(CNSTGRIDGROUPLISTCOLID, true);
			GridGroupList.TextMatrix(0, CNSTGRIDGROUPLISTCOLGROUPNAME, "Group Name");
			GridGroupList.TextMatrix(0, CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION, "User Defined Name");
			GridGroupList.TextMatrix(0, CNSTGRIDGROUPLISTCOLGROUPDESCRIPTION, "Group Description");
		}

		private void FillGridGroupList()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			clsLoad.OpenRecordset("select * from grouplist order by groupname", "twck0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				GridGroupList.Rows += 1;
				lngRow = GridGroupList.Rows - 1;
				GridGroupList.RowData(lngRow, false);
				GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLGROUPNAME, FCConvert.ToString(clsLoad.Get_Fields_String("GroupName")));
				GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLGROUPDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("GroupDescription")));
				GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("UserDefinedDescription")));
				FillGridGroup(clsLoad.Get_Fields_Int32("ID"));
				GridGroup.RowHidden(-1, true);
				//FC:FINAL:DDU:#i2307 - show Column headers
				//GridGroup.RowHidden(0, false);
				clsLoad.MoveNext();
			}
		}

		private void FillGridGroup(int lngGroup)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			string strTemp = "";
			if (lngGroup > 0)
			{
				clsLoad.OpenRecordset("select * from groupmembers where groupid = " + FCConvert.ToString(lngGroup) + " order by name ", "twck0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					GridGroup.Rows += 1;
					lngRow = GridGroup.Rows - 1;
					GridGroup.RowData(lngRow, false);
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLADDRESS1, FCConvert.ToString(clsLoad.Get_Fields_String("address1")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLADDRESS2, FCConvert.ToString(clsLoad.Get_Fields_String("address2")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLCITY, FCConvert.ToString(clsLoad.Get_Fields_String("city")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLEMAIL, FCConvert.ToString(clsLoad.Get_Fields_String("email")));
					if (clsLoad.Get_Fields_DateTime("termfrom").ToOADate() != 0)
					{
						GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLFROM, FCConvert.ToString(clsLoad.Get_Fields_DateTime("termfrom")));
					}
					else
					{
						GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLFROM, "");
					}
					if (clsLoad.Get_Fields_DateTime("termto").ToOADate() != 0)
					{
						GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTO, FCConvert.ToString(clsLoad.Get_Fields_DateTime("termto")));
					}
					else
					{
						GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTO, "");
					}
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLGROUPID, FCConvert.ToString(clsLoad.Get_Fields("groupid")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLNAME, FCConvert.ToString(clsLoad.Get_Fields_String("name")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLSTATE, FCConvert.ToString(clsLoad.Get_Fields("state")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTITLE, FCConvert.ToString(clsLoad.Get_Fields("title")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLUSERDEFINED, FCConvert.ToString(clsLoad.Get_Fields_String("userdefined")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLZIP, FCConvert.ToString(clsLoad.Get_Fields_String("zip")));
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLZIP4, FCConvert.ToString(clsLoad.Get_Fields_String("zip4")));
					strTemp = Strings.StrDup(10, "0") + clsLoad.Get_Fields_String("telephone");
					strTemp = Strings.Right(strTemp, 10);
					strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLPHONE, strTemp);
					strTemp = Strings.StrDup(10, "0") + clsLoad.Get_Fields_String("HomeNumber");
					strTemp = Strings.Right(strTemp, 10);
					strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLHOMEPHONE, strTemp);
					strTemp = Strings.StrDup(10, "0") + clsLoad.Get_Fields_String("CellNumber");
					strTemp = Strings.Right(strTemp, 10);
					strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLCELLPHONE, strTemp);
					strTemp = Strings.StrDup(10, "0") + clsLoad.Get_Fields_String("otherNumber");
					strTemp = Strings.Right(strTemp, 10);
					strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
					GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLOTHERPHONE, strTemp);
					clsLoad.MoveNext();
				}
			}
		}

		private void ResizeGridGroupList()
		{
			int GridWidth = 0;
			GridWidth = GridGroupList.WidthOriginal;
			GridGroupList.ColWidth(CNSTGRIDGROUPLISTCOLGROUPNAME, FCConvert.ToInt32(0.25 * GridWidth));
			GridGroupList.ColWidth(CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void ResizeGridGroup()
		{
			int GridWidth = 0;
			GridWidth = GridGroup.WidthOriginal;
			GridGroup.ColWidth(CNSTGRIDGROUPCOLNAME, FCConvert.ToInt32(0.5 * GridWidth));
		}

		private void SetupGridGroup()
		{
			GridGroup.Cols = 18;
			GridGroup.Rows = 1;
			GridGroup.FixedRows = 1;
			GridGroup.FixedCols = 0;
			GridGroup.ExtendLastCol = true;
			GridGroup.ColHidden(CNSTGRIDGROUPCOLADDRESS1, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLADDRESS2, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLID, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLCITY, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLEMAIL, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLFROM, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLGROUPID, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLPHONE, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLSTATE, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLTO, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLUSERDEFINED, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLZIP, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLZIP4, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLCELLPHONE, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLHOMEPHONE, true);
			GridGroup.ColHidden(CNSTGRIDGROUPCOLOTHERPHONE, true);
			GridGroup.TextMatrix(0, CNSTGRIDGROUPCOLNAME, "Name");
			GridGroup.TextMatrix(0, CNSTGRIDGROUPCOLTITLE, "Title");
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngRow = 0;
			int lngID = 0;
			int lngOID = 0;
			int lngGroupID = 0;
			int lngMemID = 0;
			string strTemp = "";
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				for (x = 1; x <= (GridGroup.Rows - 1); x++)
				{
					if (fecherFoundation.Strings.Trim(GridGroup.TextMatrix(x, CNSTGRIDGROUPCOLNAME)) == "")
					{
						MessageBox.Show("You must enter a name for each committee member before you may save", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return SaveInfo;
					}
				}
				for (x = 0; x <= (gridDeletedMembers.Rows - 1); x++)
				{
					clsSave.Execute("delete from groupmembers where ID = " + FCConvert.ToString(Conversion.Val(gridDeletedMembers.TextMatrix(x, 0))), "twck0000.vb1");
				}
				// x
				gridDeletedMembers.Rows = 0;
				for (x = 0; x <= (GridDeletedGroups.Rows - 1); x++)
				{
					clsSave.Execute("delete from groupLIST where ID = " + FCConvert.ToString(Conversion.Val(GridDeletedGroups.TextMatrix(x, 0))), "twck0000.vb1");
					clsSave.Execute("delete from groupmembers where groupid = " + FCConvert.ToString(Conversion.Val(GridDeletedGroups.TextMatrix(x, 0))), "twck0000.vb1");
				}
				// x
				GridDeletedGroups.Rows = 0;
				if (GridGroupList.Rows > 1)
				{
					lngRow = GridGroupList.FindRow(true, 1);
					while (lngRow > 0)
					{
						lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLID))));
						clsSave.OpenRecordset("select * from grouplist where ID = " + FCConvert.ToString(lngID), "twck0000.vb1");
						if (!clsSave.EndOfFile())
						{
							lngGroupID = lngID;
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
							lngGroupID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
							GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLID, FCConvert.ToString(lngGroupID));
							if (lngRow == GridGroupList.Row)
							{
								lngCurrentGroup = lngGroupID;
							}
						}
						clsSave.Set_Fields("GroupName", fecherFoundation.Strings.Trim(GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLGROUPNAME)));
						clsSave.Set_Fields("GroupDescription", fecherFoundation.Strings.Trim(GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLGROUPDESCRIPTION)));
						clsSave.Set_Fields("UserDefinedDescription", fecherFoundation.Strings.Trim(GridGroupList.TextMatrix(lngRow, CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION)));
						clsSave.Update();
						for (x = 1; x <= (GridGroup.Rows - 1); x++)
						{
							if (Conversion.Val(GridGroup.TextMatrix(x, CNSTGRIDGROUPCOLGROUPID)) == lngID)
							{
								GridGroup.TextMatrix(x, CNSTGRIDGROUPCOLGROUPID, FCConvert.ToString(lngGroupID));
							}
						}
						// x
						GridGroupList.RowData(lngRow, false);
						if (lngRow < GridGroupList.Rows - 1)
						{
							lngRow = GridGroupList.FindRow(true, lngRow + 1);
						}
						else
						{
							lngRow = -1;
						}
					}
				}
				if (GridGroup.Rows > 1)
				{
					lngRow = GridGroup.FindRow(true, 1);
					while (lngRow > 0)
					{
						lngOID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLID))));
						clsSave.OpenRecordset("select * from groupmembers where ID = " + FCConvert.ToString(lngOID), "twck0000.vb1");
						if (!clsSave.EndOfFile())
						{
							lngMemID = lngOID;
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
							lngMemID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
							GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLID, FCConvert.ToString(lngMemID));
						}
						clsSave.Set_Fields("GroupID", FCConvert.ToString(Conversion.Val(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLGROUPID))));
						clsSave.Set_Fields("Name", fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLNAME)));
						clsSave.Set_Fields("Address1", fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLADDRESS1)));
						clsSave.Set_Fields("address2", fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLADDRESS2)));
						clsSave.Set_Fields("City", fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLCITY)));
						clsSave.Set_Fields("State", fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLSTATE)));
						clsSave.Set_Fields("Zip", GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLZIP));
						clsSave.Set_Fields("zip4", GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLZIP4));
						clsSave.Set_Fields("email", fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLEMAIL)));
						clsSave.Set_Fields("userdefined", fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLUSERDEFINED)));
						clsSave.Set_Fields("Title", fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTITLE)));
						strTemp = fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLPHONE));
						strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.StrDup(10, "0") + strTemp;
						strTemp = Strings.Right(strTemp, 10);
						clsSave.Set_Fields("TELEPHONE", strTemp);
						strTemp = fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLHOMEPHONE));
						strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.StrDup(10, "0") + strTemp;
						strTemp = Strings.Right(strTemp, 10);
						clsSave.Set_Fields("HomeNumber", strTemp);
						strTemp = fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLCELLPHONE));
						strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.StrDup(10, "0") + strTemp;
						strTemp = Strings.Right(strTemp, 10);
						clsSave.Set_Fields("CellNumber", strTemp);
						strTemp = fecherFoundation.Strings.Trim(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLOTHERPHONE));
						strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.StrDup(10, "0") + strTemp;
						strTemp = Strings.Right(strTemp, 10);
						clsSave.Set_Fields("OtherNumber", strTemp);
						if (Information.IsDate(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLFROM)))
						{
							clsSave.Set_Fields("termfrom", Strings.Format(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLFROM), "MM/dd/yyyy"));
						}
						else
						{
							clsSave.Set_Fields("termfrom", 0);
						}
						if (Information.IsDate(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTO)))
						{
							clsSave.Set_Fields("termto", Strings.Format(GridGroup.TextMatrix(lngRow, CNSTGRIDGROUPCOLTO), "MM/dd/yyyy"));
						}
						else
						{
							clsSave.Set_Fields("termto", 0);
						}
						clsSave.Update();
						GridGroup.RowData(lngRow, false);
						if (lngRow < GridGroup.Rows - 1)
						{
							lngRow = GridGroup.FindRow(true, lngRow + 1);
						}
						else
						{
							lngRow = -1;
						}
					}
				}
				MessageBox.Show("Saved Successfully", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveInfo = true;
				blnNotSaved = false;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuPrintAll_Click(object sender, System.EventArgs e)
		{
			rptCommittee.InstancePtr.Init();
		}

		private void mnuPrintRoster_Click(object sender, System.EventArgs e)
		{
			if (GridGroupList.Row < 1)
			{
				MessageBox.Show("You must choose a group first", "No Group Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rptCommittee.InstancePtr.Init(lngCurrentGroup);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			GridGroup.Row = 0;
			if (GridGroupList.Col == CNSTGRIDGROUPLISTCOLGROUPNAME)
			{
				GridGroupList.Col = CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION;
			}
			else
			{
				GridGroupList.Col = CNSTGRIDGROUPLISTCOLGROUPNAME;
			}
			//App.DoEvents();
			SaveInfo();
			//App.DoEvents();
			SelectFirstMember();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			GridGroup.Row = 0;
			if (GridGroupList.Col == CNSTGRIDGROUPLISTCOLGROUPNAME)
			{
				GridGroupList.Col = CNSTGRIDGROUPLISTCOLUSERDEFINEDDESCRIPTION;
			}
			else
			{
				GridGroupList.Col = CNSTGRIDGROUPLISTCOLGROUPNAME;
			}
			//App.DoEvents();
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}

		private void SelectFirstMember()
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 1; x <= (GridGroup.Rows - 1); x++)
			{
				if (!GridGroup.RowHidden(x))
				{
					GridGroup.Row = x;
					return;
				}
			}
			// x
		}

		private void t2kCellPhone_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode >= Keys.D1 && e.KeyCode <= Keys.D9)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode == Keys.D0)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode == Keys.NumPad0)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode >= Keys.NumPad1 && e.KeyCode <= Keys.NumPad9)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
		}

		private void t2kCellPhone_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void t2kFrom_Change(object sender, System.EventArgs e)
		{
			if (!boolChangesDontCount)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
		}

		private void t2kFrom_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void t2kHomePhone_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode >= Keys.D1 && e.KeyCode <= Keys.D9)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode == Keys.D0)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode == Keys.NumPad0)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode >= Keys.NumPad1 && e.KeyCode <= Keys.NumPad9)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
		}

		private void t2kHomePhone_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void t2kOtherPhone_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode >= Keys.D1 && e.KeyCode <= Keys.D9)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode == Keys.D0)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode == Keys.NumPad0)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode >= Keys.NumPad1 && e.KeyCode <= Keys.NumPad9)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
		}

		private void t2kOtherPhone_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void T2KPhone_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode >= Keys.D1 && e.KeyCode <= Keys.D9)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode == Keys.D0)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode == Keys.NumPad0)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
			else if (e.KeyCode >= Keys.NumPad1 && e.KeyCode <= Keys.NumPad9)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
		}

		private void T2KPhone_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void t2kTo_Change(object sender, System.EventArgs e)
		{
			if (!boolChangesDontCount)
			{
				if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
				{
					GridGroup.RowData(lngCurrentMemberRow, true);
				}
				blnNotSaved = true;
			}
		}

		private void t2kTo_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtAddress1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtAddress2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtCity_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtEMail_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtState_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtTitle_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtUserDefined_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtZip_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void txtZip4_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (lngCurrentMemberRow > 0 && lngCurrentMemberRow < GridGroup.Rows)
			{
				GridGroup.RowData(lngCurrentMemberRow, true);
			}
			blnNotSaved = true;
		}

		private void LockUnlockBoxes(bool boolEditable)
		{
			txtName.Enabled = boolEditable;
			txtAddress1.Enabled = boolEditable;
			txtAddress2.Enabled = boolEditable;
			txtCity.Enabled = boolEditable;
			txtState.Enabled = boolEditable;
			txtZip.Enabled = boolEditable;
			txtZip4.Enabled = boolEditable;
			txtTitle.Enabled = boolEditable;
			txtUserDefined.Enabled = boolEditable;
			txtEMail.Enabled = boolEditable;
			T2KPhone.Enabled = boolEditable;
			t2kTo.Enabled = boolEditable;
			t2kFrom.Enabled = boolEditable;
		}

		private void LoadExistingMembersGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			vsMembers.Rows = 1;
			rsInfo.OpenRecordset("SELECT DISTINCT Name, Address1, Address2, City, State, Zip, Zip4 FROM GroupMembers ORDER BY Name, Address1, Address2, City, State, Zip, Zip4", "TWCK0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsMembers.Rows += 1;
					vsMembers.TextMatrix(vsMembers.Rows - 1, NameCol, FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
					vsMembers.TextMatrix(vsMembers.Rows - 1, Address1Col, FCConvert.ToString(rsInfo.Get_Fields_String("Address1")));
					vsMembers.TextMatrix(vsMembers.Rows - 1, Address2Col, FCConvert.ToString(rsInfo.Get_Fields_String("Address2")));
					vsMembers.TextMatrix(vsMembers.Rows - 1, CityCol, FCConvert.ToString(rsInfo.Get_Fields_String("City")));
					vsMembers.TextMatrix(vsMembers.Rows - 1, StateCol, FCConvert.ToString(rsInfo.Get_Fields("State")));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip4"))) == "")
					{
						vsMembers.TextMatrix(vsMembers.Rows - 1, ZipCol, FCConvert.ToString(rsInfo.Get_Fields_String("Zip")));
					}
					else
					{
						vsMembers.TextMatrix(vsMembers.Rows - 1, ZipCol, rsInfo.Get_Fields_String("Zip") + "-" + rsInfo.Get_Fields_String("Zip4"));
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void vsMembers_DblClick(object sender, System.EventArgs e)
		{
			cmdOK_Click();
		}
	}
}
