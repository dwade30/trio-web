﻿using SharedApplication;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Messaging;

namespace TWCK0000.Dogs
{
    public class GetDogAuditOptionsHandler : CommandHandler<GetDogAuditOptions,DogAuditOptions>
    {
        private IModalView<IDogAuditOptionsViewModel> view;
        public GetDogAuditOptionsHandler(IModalView<IDogAuditOptionsViewModel> view)
        {
            this.view = view;
        }
        protected override DogAuditOptions Handle(GetDogAuditOptions command)
        {
            view.ViewModel.InitializeOptions(command.InlcudeOnlineTransactions,command.StartDate,command.EndDate);
            view.ShowModal();
            return new DogAuditOptions(view.ViewModel.IncludeOnlineTransactions,view.ViewModel.StartDate,view.ViewModel.EndDate,view.ViewModel.Cancelled);
        }
    }
}