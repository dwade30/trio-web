﻿using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Dogs;
using System;
using Wisej.Web;

namespace TWCK0000.Dogs
{
    public partial class frmSearchResultsDog : BaseForm, IView<ISearchResultsDogViewModel>
    {
        private ISearchResultsDogViewModel viewModel;
        private bool cancelling = true;
        public frmSearchResultsDog()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            GridSearch.DoubleClick += GridSearch_DoubleClick;
            GridSearch.KeyDown += GridSearch_KeyDown;
            GridSearch.MouseMove += GridSearch_MouseMove;
            Closing += FrmSearchResultsDog_Closing;
            Load += FrmSearchResultsDog_Load;
        }

        private void FrmSearchResultsDog_Load(object sender, EventArgs e)
        {
            SetupGrid();
            FillGrid();
            ResizeGridSearch();
        }

        private void FrmSearchResultsDog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                viewModel.Cancel();
            }
        }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

        private void GridSearch_MouseMove(object sender, MouseEventArgs e)
        {
	        if (!viewModel.SearchCriteria.ByDog())
	        {
		        var currentRow = GridSearch.MouseRow;
		        toolTip1.SetToolTip(GridSearch, "");

		        if (currentRow <= 0) return;

		        var dogNames = DogNames(currentRow) ?? "No dogs";
		        toolTip1.SetToolTip(GridSearch, dogNames);
			}
        }

        private void GridSearch_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Return:
                    {
                        if (GridSearch.Row > 0)
                        {
                            SelectRow(GridSearch.Row);
                        }

                        break;
                    }
            }
        }

        private void GridSearch_DoubleClick(object sender, EventArgs e)
        {
            if (GridSearch.MouseRow > 0)
            {
                SelectRow(GridSearch.MouseRow);
            }
        }


        public frmSearchResultsDog(ISearchResultsDogViewModel viewModel) : this()
        {
            this.viewModel = viewModel;
            if (viewModel != null)
            {
                viewModel.OwnerSelected += ViewModel_OwnerSelected;
            }

        }

        private void ViewModel_OwnerSelected(object sender, int e)
        {
            CloseWithoutCancel();
        }

        public ISearchResultsDogViewModel ViewModel
        {
            get { return viewModel; }
            set
            {
                viewModel = value;
                viewModel.OwnerSelected += ViewModel_OwnerSelected;
            }
        }

        private void SetupGrid()
        {
            GridSearch.Cols = 10;
            GridSearch.TextMatrix(0, (int)SearchColumn.LastName, "Last Name");
            GridSearch.TextMatrix(0, (int)SearchColumn.FirstName, "First Name");
            GridSearch.TextMatrix(0, (int)SearchColumn.LocationNumber, "ST #");
            GridSearch.TextMatrix(0, (int)SearchColumn.LocationStreet, "Street Name");
            GridSearch.TextMatrix(0, (int)SearchColumn.KennelNumber, "Kennel #");
            GridSearch.TextMatrix(0, (int)SearchColumn.Breed, "Breed");
            GridSearch.TextMatrix(0, (int)SearchColumn.DogColor, "Color");
            GridSearch.TextMatrix(0, (int)SearchColumn.DogName, "Dog Name");
            GridSearch.ColHidden((int)SearchColumn.OwnerDeleted, true);
            switch (viewModel.SearchCriteria.TypeToSearchBy)
            {
                case DogSearchBy.KennelLicense:
                case DogSearchBy.City:
                case DogSearchBy.Location:
                case DogSearchBy.OwnerName:
                    GridSearch.ColHidden((int)SearchColumn.Breed, true);
                    GridSearch.ColHidden((int)SearchColumn.DogName, true);
                    GridSearch.ColHidden((int)SearchColumn.DogColor, true);
                    GridSearch.ColHidden((int)SearchColumn.OwnerNum, true);
                    GridSearch.ColHidden((int)SearchColumn.LocationStreet, false);
                    GridSearch.ColHidden((int)SearchColumn.LocationNumber, false);
                    if (viewModel.SearchCriteria.TypeToSearchBy == DogSearchBy.KennelLicense)
                    {
                        GridSearch.ColHidden((int)SearchColumn.KennelNumber, false);
                    }
                    else
                    {
                        GridSearch.ColHidden((int)SearchColumn.KennelNumber, true);
                    }
                    break;
                default:
                    GridSearch.ColHidden((int)SearchColumn.Breed, false);
                    GridSearch.ColHidden((int)SearchColumn.DogName, false);
                    GridSearch.ColHidden((int)SearchColumn.DogColor, false);
                    GridSearch.ColHidden((int)SearchColumn.OwnerNum, true);
                    GridSearch.ColHidden((int)SearchColumn.LocationStreet, true);
                    GridSearch.ColHidden((int)SearchColumn.LocationNumber, true);
                    GridSearch.ColHidden((int)SearchColumn.KennelNumber, true);
                    break;
            }

        }
        private void FillGrid()
        {
            int currentOwner = 0;
            int currentRow = 0;
            GridSearch.Rows = 1;
            foreach (var searchResult in viewModel.SearchResults)
            {
                if (viewModel.SearchCriteria.ByDog() ||
                    viewModel.SearchCriteria.TypeToSearchBy == DogSearchBy.KennelLicense ||
                    currentOwner != searchResult.OwnerId)
                {
                    currentOwner = searchResult.OwnerId;
                    GridSearch.Rows += 1;
                    currentRow = GridSearch.Rows - 1;
                    GridSearch.TextMatrix(currentRow, (int)SearchColumn.OwnerNum, currentOwner);
                    GridSearch.TextMatrix(currentRow, (int)SearchColumn.LastName, searchResult.OwnerLast);
                    GridSearch.TextMatrix(currentRow, (int)SearchColumn.FirstName, searchResult.OwnerFirst);
                    if (searchResult.OwnerDeleted)
                    {
                        GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, currentRow, 0, currentRow,
                            GridSearch.Cols - 1, Shape1.BackColor);
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.OwnerDeleted, true.ToString());
                    }
                    else
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.OwnerDeleted, false.ToString());
                    }

                    if (searchResult.StreetNumber > 0)
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.LocationNumber,
                            searchResult.StreetNumber.ToString());
                    }
                    else
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.LocationNumber, "");
                    }

                    GridSearch.TextMatrix(currentRow, (int)SearchColumn.LocationStreet, searchResult.StreetName);
                    if (viewModel.SearchCriteria.ByDog())
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.DogName, searchResult.DogName);
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.Breed, searchResult.Breed);
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.DogColor, searchResult.Color);
                    }
                    else if (viewModel.SearchCriteria.TypeToSearchBy == DogSearchBy.KennelLicense)
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.KennelNumber, searchResult.KennelLicense);
                    }

                    if (!viewModel.SearchCriteria.ByDog())
                    {
                        var dogNames = "Dog(s): ";
                        var separator = "";
                        foreach (var dogName in searchResult.DogNames)
                        {
                            dogNames += separator + dogName;
                            separator = ",";
                        }

                        GridSearch.RowData(currentRow, dogNames);
                    }

                }
            }
        }

        private void ResizeGridSearch()
        {
            int GridWidth = GridSearch.WidthOriginal;
            GridSearch.ColWidth((int)SearchColumn.LastName, Convert.ToInt32(0.2 * GridWidth));
            GridSearch.ColWidth((int)SearchColumn.FirstName, Convert.ToInt32(0.2 * GridWidth));
            switch (viewModel.SearchCriteria.TypeToSearchBy)
            {
                case DogSearchBy.City:
                case DogSearchBy.Location:
                case DogSearchBy.OwnerName:
                    GridSearch.ColWidth((int)SearchColumn.LocationNumber, Convert.ToInt32(.1 * GridWidth));
                    GridSearch.ColWidth((int)SearchColumn.LastName, Convert.ToInt32(.3 * GridWidth));
                    GridSearch.ColWidth((int)SearchColumn.FirstName, Convert.ToInt32(.24 * GridWidth));
                    break;
                case DogSearchBy.KennelLicense:
                    GridSearch.ColWidth((int)SearchColumn.LocationNumber, Convert.ToInt32(.1 * GridWidth));
                    GridSearch.ColWidth((int)SearchColumn.LocationStreet, Convert.ToInt32(.27 * GridWidth));
                    GridSearch.ColWidth((int)SearchColumn.LastName, Convert.ToInt32(.27 * GridWidth));
                    GridSearch.ColWidth((int)SearchColumn.FirstName, Convert.ToInt32(.25 * GridWidth));
                    break;
                default:
                    GridSearch.ColWidth((int)SearchColumn.LastName, Convert.ToInt32(.2 * GridWidth));
                    GridSearch.ColWidth((int)SearchColumn.FirstName, Convert.ToInt32(.2 * GridWidth));
                    GridSearch.ColWidth((int)SearchColumn.DogName, Convert.ToInt32(.16 * GridWidth));
                    GridSearch.ColWidth((int)SearchColumn.Breed, Convert.ToInt32(.24 * GridWidth));
                    break;
            }
        }


        private void SelectRow(int selectedRow)
        {
            if (GridSearch.Rows < 2)
            {
                MessageBox.Show("There are no records to open", "No Records", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            if (selectedRow < 1)
            {
                MessageBox.Show("You must select a record first", "No Record Selected", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            if (OwnerDeleted(selectedRow))
            {
                if (MessageBox.Show("Owner record is deleted. Do you want to undelete it?", "Undelete?",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    viewModel.UndeleteOwner(OwnerId(selectedRow));
                    GridSearch.TextMatrix(selectedRow, (int)SearchColumn.OwnerDeleted, false.ToString());

                    GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, selectedRow, 0, selectedRow,
	                    GridSearch.Cols - 1, GridSearch.BackColor);
				}
				else
                {
                    return;
                }
            }

            viewModel.SelectOwner(OwnerId(selectedRow));
            // CloseWithoutCancel();
        }


        private int OwnerId(int row)
        {
            return Convert.ToInt32(GridSearch.TextMatrix(row, (int)SearchColumn.OwnerNum));
        }

        private bool OwnerDeleted(int row)
        {
            return Convert.ToBoolean(GridSearch.TextMatrix(row, (int)SearchColumn.OwnerDeleted));
        }
        private string DogNames(int row)
        {
            return GridSearch.RowData(row) != null ? GridSearch.RowData(row).ToString() : "";
        }
        private enum SearchColumn
        {
            OwnerNum = 0,
            LastName = 1,
            FirstName = 2,
            LocationNumber = 3,
            LocationStreet = 4,
            KennelNumber = 5,
            DogName = 6,
            Breed = 7,
            DogColor = 8,
            OwnerDeleted = 9
        }
    }
}
