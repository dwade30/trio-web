﻿namespace TWCK0000.Dogs
{
    partial class frmSearchDog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.cmdNewOwner = new fecherFoundation.FCButton();
			this.cmbCriteria = new Wisej.Web.ComboBox();
			this.cmbSearchBy = new Wisej.Web.ComboBox();
			this.lblSearchBy = new fecherFoundation.FCLabel();
			this.lblCriteria1 = new fecherFoundation.FCLabel();
			this.chkIncludeDeleted = new fecherFoundation.FCCheckBox();
			this.lblTypes = new fecherFoundation.FCLabel();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.cmbTypes = new Wisej.Web.ComboBox();
			this.lblCriteria2 = new fecherFoundation.FCLabel();
			this.txtCriteria1 = new fecherFoundation.FCTextBox();
			this.txtCriteria2 = new fecherFoundation.FCTextBox();
			this.GridSearch = new fecherFoundation.FCGrid();
			this.toolTip1 = new Wisej.Web.ToolTip(this.components);
			this.toolTip2 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcessClearSearch = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 402);
			this.BottomPanel.Size = new System.Drawing.Size(1089, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridSearch);
			this.ClientArea.Controls.Add(this.txtCriteria2);
			this.ClientArea.Controls.Add(this.txtCriteria1);
			this.ClientArea.Controls.Add(this.lblCriteria2);
			this.ClientArea.Controls.Add(this.cmbCriteria);
			this.ClientArea.Controls.Add(this.lblCriteria1);
			this.ClientArea.Controls.Add(this.chkIncludeDeleted);
			this.ClientArea.Controls.Add(this.cmbSearchBy);
			this.ClientArea.Controls.Add(this.cmbTypes);
			this.ClientArea.Controls.Add(this.lblSearchBy);
			this.ClientArea.Controls.Add(this.lblTypes);
			this.ClientArea.Size = new System.Drawing.Size(1109, 518);
			this.ClientArea.Controls.SetChildIndex(this.lblTypes, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblSearchBy, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbTypes, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbSearchBy, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkIncludeDeleted, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCriteria1, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbCriteria, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCriteria2, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtCriteria1, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtCriteria2, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridSearch, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdProcessClearSearch);
			this.TopPanel.Controls.Add(this.cmdSearch);
			this.TopPanel.Controls.Add(this.cmdNewOwner);
			this.TopPanel.Size = new System.Drawing.Size(1109, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNewOwner, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessClearSearch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(143, 28);
			this.HeaderText.Text = "Dog Records";
			// 
			// cmdNewOwner
			// 
			this.cmdNewOwner.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
			this.cmdNewOwner.Location = new System.Drawing.Point(962, 26);
			this.cmdNewOwner.Name = "cmdNewOwner";
			this.cmdNewOwner.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.cmdNewOwner.Size = new System.Drawing.Size(90, 24);
			this.cmdNewOwner.TabIndex = 2;
			this.cmdNewOwner.Text = "New Owner";
			// 
			// cmbCriteria
			// 
			this.cmbCriteria.AutoSize = false;
			this.cmbCriteria.DisplayMember = "Description";
			this.cmbCriteria.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCriteria.LabelText = "";
			this.cmbCriteria.Location = new System.Drawing.Point(318, 79);
			this.cmbCriteria.Name = "cmbCriteria";
			this.cmbCriteria.Size = new System.Drawing.Size(125, 40);
			this.cmbCriteria.TabIndex = 6;
			this.cmbCriteria.ValueMember = "ID";
			// 
			// cmbSearchBy
			// 
			this.cmbSearchBy.AutoSize = false;
			this.cmbSearchBy.DisplayMember = "Description";
			this.cmbSearchBy.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSearchBy.LabelText = "";
			this.cmbSearchBy.Location = new System.Drawing.Point(129, 79);
			this.cmbSearchBy.Name = "cmbSearchBy";
			this.cmbSearchBy.Size = new System.Drawing.Size(165, 40);
			this.cmbSearchBy.TabIndex = 5;
			this.cmbSearchBy.ValueMember = "ID";
			// 
			// lblSearchBy
			// 
			this.lblSearchBy.AutoSize = true;
			this.lblSearchBy.Location = new System.Drawing.Point(30, 93);
			this.lblSearchBy.Name = "lblSearchBy";
			this.lblSearchBy.Size = new System.Drawing.Size(79, 15);
			this.lblSearchBy.TabIndex = 30;
			this.lblSearchBy.Text = "SEARCH BY";
			// 
			// lblCriteria1
			// 
			this.lblCriteria1.AutoSize = true;
			this.lblCriteria1.Location = new System.Drawing.Point(498, 93);
			this.lblCriteria1.Name = "lblCriteria1";
			this.lblCriteria1.Size = new System.Drawing.Size(64, 15);
			this.lblCriteria1.TabIndex = 20;
			this.lblCriteria1.Text = "CRITERIA";
			// 
			// chkIncludeDeleted
			// 
			this.chkIncludeDeleted.Location = new System.Drawing.Point(315, 34);
			this.chkIncludeDeleted.Name = "chkIncludeDeleted";
			this.chkIncludeDeleted.Size = new System.Drawing.Size(210, 22);
			this.chkIncludeDeleted.TabIndex = 3;
			this.chkIncludeDeleted.Text = "Show Deleted Dogs or Owners";
			// 
			// lblTypes
			// 
			this.lblTypes.AutoSize = true;
			this.lblTypes.Location = new System.Drawing.Point(30, 41);
			this.lblTypes.Name = "lblTypes";
			this.lblTypes.Size = new System.Drawing.Size(46, 15);
			this.lblTypes.TabIndex = 28;
			this.lblTypes.Text = "TYPES";
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(292, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(96, 48);
			this.cmdProcess.TabIndex = 1;
			this.cmdProcess.Text = "Process";
			// 
			// cmbTypes
			// 
			this.cmbTypes.AutoSize = false;
			this.cmbTypes.DisplayMember = "Description";
			this.cmbTypes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTypes.LabelText = "";
			this.cmbTypes.Location = new System.Drawing.Point(129, 27);
			this.cmbTypes.Name = "cmbTypes";
			this.cmbTypes.Size = new System.Drawing.Size(165, 40);
			this.cmbTypes.TabIndex = 1;
			this.cmbTypes.ValueMember = "ID";
			// 
			// lblCriteria2
			// 
			this.lblCriteria2.AutoSize = true;
			this.lblCriteria2.Location = new System.Drawing.Point(798, 93);
			this.lblCriteria2.Name = "lblCriteria2";
			this.lblCriteria2.Size = new System.Drawing.Size(64, 15);
			this.lblCriteria2.TabIndex = 1002;
			this.lblCriteria2.Text = "CRITERIA";
			// 
			// txtCriteria1
			// 
			this.txtCriteria1.Location = new System.Drawing.Point(606, 80);
			this.txtCriteria1.Name = "txtCriteria1";
			this.txtCriteria1.Size = new System.Drawing.Size(132, 40);
			this.txtCriteria1.TabIndex = 1004;
			this.txtCriteria1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCriteria1_KeyDown);
			// 
			// txtCriteria2
			// 
			this.txtCriteria2.Location = new System.Drawing.Point(890, 79);
			this.txtCriteria2.Name = "txtCriteria2";
			this.txtCriteria2.Size = new System.Drawing.Size(132, 40);
			this.txtCriteria2.TabIndex = 1005;
			this.txtCriteria2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCriteria2_KeyDown);
			// 
			// GridSearch
			// 
			this.GridSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridSearch.Cols = 10;
			this.GridSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.GridSearch.ExtendLastCol = true;
			this.GridSearch.FixedCols = 0;
			this.GridSearch.Location = new System.Drawing.Point(30, 158);
			this.GridSearch.Name = "GridSearch";
			this.GridSearch.RowHeadersVisible = false;
			this.GridSearch.Rows = 1;
			this.GridSearch.Size = new System.Drawing.Size(997, 244);
			this.GridSearch.TabIndex = 5;
			// 
			// cmdProcessClearSearch
			// 
			this.cmdProcessClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessClearSearch.Location = new System.Drawing.Point(774, 26);
			this.cmdProcessClearSearch.Name = "cmdProcessClearSearch";
			this.cmdProcessClearSearch.Size = new System.Drawing.Size(95, 24);
			this.cmdProcessClearSearch.TabIndex = 56;
			this.cmdProcessClearSearch.Text = "Clear Search";
			this.cmdProcessClearSearch.Click += new System.EventHandler(this.cmdProcessClearSearch_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSearch.ImageSource = "button-search";
			this.cmdSearch.Location = new System.Drawing.Point(875, 26);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdSearch.TabIndex = 55;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// frmSearchDog
			// 
			this.ClientSize = new System.Drawing.Size(1109, 578);
			this.Name = "frmSearchDog";
			this.Text = "Dog Records";
			this.Resize += new System.EventHandler(this.frmSearchDog_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCButton cmdNewOwner;
        public fecherFoundation.FCLabel lblSearchBy;
        public fecherFoundation.FCLabel lblCriteria1;
        public fecherFoundation.FCCheckBox chkIncludeDeleted;
        public fecherFoundation.FCLabel lblTypes;
        private fecherFoundation.FCButton cmdProcess;
        private Wisej.Web.ComboBox cmbTypes;
        private Wisej.Web.ComboBox cmbSearchBy;
        private Wisej.Web.ComboBox cmbCriteria;
		private fecherFoundation.FCTextBox txtCriteria2;
		private fecherFoundation.FCTextBox txtCriteria1;
		public fecherFoundation.FCLabel lblCriteria2;
		public fecherFoundation.FCGrid GridSearch;
		private Wisej.Web.ToolTip toolTip1;
		private Wisej.Web.ToolTip toolTip2;
		public fecherFoundation.FCButton cmdProcessClearSearch;
		public fecherFoundation.FCButton cmdSearch;
	}
}