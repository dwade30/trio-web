﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using Wisej.Web;

namespace TWCK0000.Dogs
{
    public partial class frmSearchDog : BaseForm, IView<IDogOwnerSearchViewModel>
    {
        private IDogOwnerSearchViewModel viewModel;
        private bool cancelling = true;

        public frmSearchDog()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            cmdProcess.Click += CmdProcess_Click;
            cmbSearchBy.SelectedValueChanged += CmbSearchBy_SelectedValueChanged;
            GridSearch.DoubleClick += GridSearch_DoubleClick;
            GridSearch.KeyDown += GridSearch_KeyDown;
            GridSearch.MouseMove += GridSearch_MouseMove;
            this.Closing += FrmSearchDog_Closing;
            cmdNewOwner.Click += CmdNewOwner_Click;
        }

        private void ViewModel_OwnerSelected(object sender, int e)
        {
	        CloseWithoutCancel();
        }

        private void CmdNewOwner_Click(object sender, EventArgs e)
        {
            AddNewOwner();
        }

        private void AddNewOwner()
        {
            ViewModel.AddNewOwner();
        }

        private void GridSearch_MouseMove(object sender, MouseEventArgs e)
        {
	        if (!viewModel.SearchCriteria.ByDog())
	        {
		        var currentRow = GridSearch.MouseRow;
		        toolTip1.SetToolTip(GridSearch, "");

		        if (currentRow <= 0) return;

		        var dogNames = DogNames(currentRow) ?? "No dogs";
		        toolTip1.SetToolTip(GridSearch, dogNames);
	        }
        }

        private void GridSearch_KeyDown(object sender, KeyEventArgs e)
        {
	        switch (e.KeyCode)
	        {
		        case Keys.Return:
		        {
			        if (GridSearch.Row > 0)
			        {
				        SelectRow(GridSearch.Row);
			        }

			        break;
		        }
	        }
        }

        private void GridSearch_DoubleClick(object sender, EventArgs e)
        {
	        if (GridSearch.MouseRow > 0)
	        {
		        SelectRow(GridSearch.MouseRow);
	        }
        }

        private void SelectRow(int selectedRow)
        {
	        if (GridSearch.Rows < 2)
	        {
		        MessageBox.Show("There are no records to open", "No Records", MessageBoxButtons.OK,
			        MessageBoxIcon.Warning);
		        return;
	        }

	        if (selectedRow < 1)
	        {
		        MessageBox.Show("You must select a record first", "No Record Selected", MessageBoxButtons.OK,
			        MessageBoxIcon.Warning);
		        return;
	        }

	        if (OwnerDeleted(selectedRow))
	        {
		        if (MessageBox.Show("Owner record is deleted. Do you want to undelete it?", "Undelete?",
			        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
		        {
			        viewModel.UndeleteOwner(OwnerId(selectedRow));
			        GridSearch.TextMatrix(selectedRow, (int)SearchColumn.OwnerDeleted, false.ToString());

			        GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, selectedRow, 0, selectedRow,
				        GridSearch.Cols - 1, GridSearch.BackColor);
		        }
		        else
		        {
			        return;
		        }
	        }

	        viewModel.SelectOwner(OwnerId(selectedRow));
        }

        private void FrmSearchDog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                viewModel.Cancel();
            }
        }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

        private void CmbSearchBy_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateViewModel();
            UpdateView();
        }

        private void ClearSearchCriteria()
        {
	        txtCriteria1.Text = "";
	        txtCriteria2.Text = "";
        }

        private void CmdProcess_Click(object sender, EventArgs e)
        {
	        if (GridSearch.Row > 0)
	        {
		        SelectRow(GridSearch.Row);
	        }
        }

        private void Search()
        {
            UpdateViewModel();
            viewModel.Search();
            if (viewModel.SearchResults().Any())
            {
	            SetupGrid();
	            ResizeGridSearch();
                ShowResults();
            }
            else
            {
	            MessageBox.Show("No records found.", "No Matches");
            }
        }

        private void ShowResults()
        {
            int currentOwner = 0;
            int currentRow = 0;
            GridSearch.Rows = 1;
            foreach (var searchResult in ViewModel.SearchResults())
            {
                if (viewModel.SearchCriteria.ByDog() ||
                    viewModel.SearchCriteria.TypeToSearchBy == DogSearchBy.KennelLicense ||
                    currentOwner != searchResult.OwnerId)
                {
                    currentOwner = searchResult.OwnerId;
                    GridSearch.Rows += 1;
                    currentRow = GridSearch.Rows - 1;
                    GridSearch.TextMatrix(currentRow, (int)SearchColumn.OwnerNum, currentOwner);
                    GridSearch.TextMatrix(currentRow, (int)SearchColumn.LastName, searchResult.OwnerLast);
                    GridSearch.TextMatrix(currentRow, (int)SearchColumn.FirstName, searchResult.OwnerFirst);
                    if (searchResult.OwnerDeleted)
                    {
                        GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, currentRow, 0, currentRow,
                            GridSearch.Cols - 1, Color.Red);
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.OwnerDeleted, true.ToString());
                    }
                    else
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.OwnerDeleted, false.ToString());
                    }

                    if (searchResult.StreetNumber > 0)
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.LocationNumber,
                            searchResult.StreetNumber.ToString());
                    }
                    else
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.LocationNumber, "");
                    }

                    GridSearch.TextMatrix(currentRow, (int)SearchColumn.LocationStreet, searchResult.StreetName);
                    if (viewModel.SearchCriteria.ByDog())
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.DogName, searchResult.DogName);
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.Breed, searchResult.Breed);
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.DogColor, searchResult.Color);
                    }
                    else if (viewModel.SearchCriteria.TypeToSearchBy == DogSearchBy.KennelLicense)
                    {
                        GridSearch.TextMatrix(currentRow, (int)SearchColumn.KennelNumber, searchResult.KennelLicense);
                    }

                    if (!viewModel.SearchCriteria.ByDog())
                    {
                        var dogNames = "Dog(s): ";
                        var separator = "";
                        foreach (var dogName in searchResult.DogNames)
                        {
                            dogNames += separator + dogName;
                            separator = ",";
                        }

                        GridSearch.RowData(currentRow, dogNames);
                    }

                }
            }
        }

        public IDogOwnerSearchViewModel ViewModel
        {
            get { return viewModel;}
            set
            {
                viewModel = value;
                if (viewModel != null)
                {
	                viewModel.OwnerSelected += ViewModel_OwnerSelected;
                }
                SetUpView();
                UpdateView();
            }
        }

        private void SetUpView()
        {
            SetCmbTypes();
            SetSearchByTypes();
            SetCriteriaChoices();
            SetupGrid();
            ResizeGridSearch();

            var criteriaFields = GetCriteriaFields(viewModel.SearchCriteria);
            txtCriteria1.Text = criteriaFields.ElementAt(0);
            txtCriteria2.Text = criteriaFields.ElementAt(1);
        }

        private void ResizeGridSearch()
        {
	        int GridWidth = GridSearch.WidthOriginal;
	        GridSearch.ColWidth((int)SearchColumn.LastName, Convert.ToInt32(0.2 * GridWidth));
	        GridSearch.ColWidth((int)SearchColumn.FirstName, Convert.ToInt32(0.2 * GridWidth));
	        switch (viewModel.SearchCriteria.TypeToSearchBy)
	        {
		        case DogSearchBy.City:
		        case DogSearchBy.Location:
		        case DogSearchBy.OwnerName:
			        GridSearch.ColWidth((int)SearchColumn.LocationNumber, Convert.ToInt32(.1 * GridWidth));
			        GridSearch.ColWidth((int)SearchColumn.LastName, Convert.ToInt32(.3 * GridWidth));
			        GridSearch.ColWidth((int)SearchColumn.FirstName, Convert.ToInt32(.24 * GridWidth));
			        break;
		        case DogSearchBy.KennelLicense:
			        GridSearch.ColWidth((int)SearchColumn.LocationNumber, Convert.ToInt32(.1 * GridWidth));
			        GridSearch.ColWidth((int)SearchColumn.LocationStreet, Convert.ToInt32(.27 * GridWidth));
			        GridSearch.ColWidth((int)SearchColumn.LastName, Convert.ToInt32(.27 * GridWidth));
			        GridSearch.ColWidth((int)SearchColumn.FirstName, Convert.ToInt32(.25 * GridWidth));
			        break;
		        default:
			        GridSearch.ColWidth((int)SearchColumn.LastName, Convert.ToInt32(.2 * GridWidth));
			        GridSearch.ColWidth((int)SearchColumn.FirstName, Convert.ToInt32(.2 * GridWidth));
			        GridSearch.ColWidth((int)SearchColumn.DogName, Convert.ToInt32(.16 * GridWidth));
			        GridSearch.ColWidth((int)SearchColumn.Breed, Convert.ToInt32(.24 * GridWidth));
			        break;
	        }
        }

        private void SetupGrid()
        {
            GridSearch.Cols = 10;
            GridSearch.TextMatrix(0, (int)SearchColumn.LastName, "Last Name");
            GridSearch.TextMatrix(0, (int)SearchColumn.FirstName, "First Name");
            GridSearch.TextMatrix(0, (int)SearchColumn.LocationNumber, "ST #");
            GridSearch.TextMatrix(0, (int)SearchColumn.LocationStreet, "Street Name");
            GridSearch.TextMatrix(0, (int)SearchColumn.KennelNumber, "Kennel #");
            GridSearch.TextMatrix(0, (int)SearchColumn.Breed, "Breed");
            GridSearch.TextMatrix(0, (int)SearchColumn.DogColor, "Color");
            GridSearch.TextMatrix(0, (int)SearchColumn.DogName, "Dog Name");
            GridSearch.ColHidden((int)SearchColumn.OwnerDeleted, true);
            switch (viewModel.SearchCriteria.TypeToSearchBy)
            {
                case DogSearchBy.KennelLicense:
                case DogSearchBy.City:
                case DogSearchBy.Location:
                case DogSearchBy.OwnerName:
                    GridSearch.ColHidden((int)SearchColumn.Breed, true);
                    GridSearch.ColHidden((int)SearchColumn.DogName, true);
                    GridSearch.ColHidden((int)SearchColumn.DogColor, true);
                    GridSearch.ColHidden((int)SearchColumn.OwnerNum, true);
                    GridSearch.ColHidden((int)SearchColumn.LocationStreet, false);
                    GridSearch.ColHidden((int)SearchColumn.LocationNumber, false);
                    if (viewModel.SearchCriteria.TypeToSearchBy == DogSearchBy.KennelLicense)
                    {
                        GridSearch.ColHidden((int)SearchColumn.KennelNumber, false);
                    }
                    else
                    {
                        GridSearch.ColHidden((int)SearchColumn.KennelNumber, true);
                    }
                    break;
                default:
                    GridSearch.ColHidden((int)SearchColumn.Breed, false);
                    GridSearch.ColHidden((int)SearchColumn.DogName, false);
                    GridSearch.ColHidden((int)SearchColumn.DogColor, false);
                    GridSearch.ColHidden((int)SearchColumn.OwnerNum, true);
                    GridSearch.ColHidden((int)SearchColumn.LocationStreet, true);
                    GridSearch.ColHidden((int)SearchColumn.LocationNumber, true);
                    GridSearch.ColHidden((int)SearchColumn.KennelNumber, true);
                    break;
            }

        }

        private void SetCmbTypes()
        {
            cmbTypes.Items.Clear();
            foreach (var recordType in viewModel.DogSearchRecordOptions)
            {
                cmbTypes.Items.Add(recordType);
            }
        }

        private void SetSearchByTypes()
        {
            cmbSearchBy.Items.Clear();
            foreach (var searchByOption in viewModel.SearchByOptions)
            {
                cmbSearchBy.Items.Add(searchByOption);
            }
        }

        private void SetCriteriaChoices()
        {
            cmbCriteria.Items.Clear();
            foreach (var matchOption in viewModel.CriteriaMatchOptions)
            {
                cmbCriteria.Items.Add(matchOption);
            }
        }

        private void UpdateView()
        {
            if (ViewModel.SearchCriteria.InlcudeDeletedDogsOrOwners)
            {
                chkIncludeDeleted.Checked = true;
            }
            else
            {
                chkIncludeDeleted.Checked = false;
            }

            SetTypeOfRecords(ViewModel.SearchCriteria.TypesOfRecords);
            SetSearchByOption(viewModel.SearchCriteria.TypeToSearchBy);
            SetSearchCriteria(viewModel.SearchCriteria);
            SetCriteriaMatchOption(viewModel.SearchCriteria.MatchCriteriaType);
        }

        private void SetTypeOfRecords(DogSearchRecordType typesOfRecords)
        {
            foreach (GenericDescriptionPair<DogSearchRecordType> item in cmbTypes.Items)
            {
                if (item.ID == typesOfRecords)
                {
                    cmbTypes.SelectedItem = item;
                    return;
                }
            }
        }

        private void SetSearchByOption(DogSearchBy searchBy)
        {
            foreach (GenericDescriptionPair<DogSearchBy> item in cmbSearchBy.Items)
            {
                if (item.ID == searchBy)
                {
                    cmbSearchBy.SelectedItem = item;
                    return;                    
                }
            }
        }

        private void SetCriteriaMatchOption(SearchCriteriaMatchType matchType)
        {
            foreach (GenericDescriptionPair<SearchCriteriaMatchType> item in cmbCriteria.Items)
            {
                if (item.ID == matchType)
                {
                    cmbCriteria.SelectedItem = item;
                    return;
                }
            }
        }

        private IEnumerable<string> GetCriteriaFields(DogSearchCriteria searchCriteria)
        {
            var criteriaFields = new List<string>();
            switch (searchCriteria.TypeToSearchBy)
            {
                case DogSearchBy.Breed:
                    criteriaFields.Add(searchCriteria.Breed);
                    break;
                case DogSearchBy.City:
                    criteriaFields.Add(searchCriteria.City);
                    break;
                case DogSearchBy.DogName:
                    criteriaFields.Add(searchCriteria.DogName);
                    break;
                case DogSearchBy.KennelLicense:
                    criteriaFields.Add(searchCriteria.KennelLicense.ToString());
                    break;
                case DogSearchBy.LicenseTagNumber:
                    criteriaFields.Add(searchCriteria.LicenseTag);
                    break;
                case DogSearchBy.Location:
                    criteriaFields.Add(searchCriteria.LocationNumber.ToString());
                    criteriaFields.Add(searchCriteria.LocationStreet);
                    break;
                case DogSearchBy.OwnerName:
                    criteriaFields.Add(searchCriteria.OwnerLastName);
                    criteriaFields.Add(searchCriteria.OwnerFirstName);
                    break;
                case DogSearchBy.RabiesTagNumber:
                    criteriaFields.Add(searchCriteria.RabiesTag);
                    break;
                case DogSearchBy.Veterinarian:
                    criteriaFields.Add(searchCriteria.Veterinarian);
                    break;
            }

            if (criteriaFields.Count() < 2)
            {
                criteriaFields.Add("");
            }
            return criteriaFields;
        }

        private void SetSearchCriteria(DogSearchCriteria searchCriteria)
        {
            List<string> headerList = new List<string>();
            List<string> criteriaList = new List<string>(GetCriteriaChoices());
            int numberOfColumns = 1;
            switch (searchCriteria.TypeToSearchBy)
            {
                case DogSearchBy.Breed:
                    headerList.Add("CRITERIA");
                    break;
                case DogSearchBy.City:
                    headerList.Add("CRITERIA");
                    break;
                case DogSearchBy.DogName:
                    headerList.Add("CRITERIA");
                    break;
                case DogSearchBy.KennelLicense:
                    headerList.Add("CRITERIA");
                    break;
                case DogSearchBy.LicenseTagNumber:
                    headerList.Add("CRITERIA");
                    break;
                case DogSearchBy.RabiesTagNumber:
                    headerList.Add("CRITERIA");
                    break;
                case DogSearchBy.Location:
                    numberOfColumns = 2;
                    headerList.Add("NUMBER");
                    headerList.Add("STREET NAME");
                    break;
                case DogSearchBy.OwnerName:
                    numberOfColumns = 2;
                    headerList.Add("LAST NAME");
                    headerList.Add("FIRST NAME");
                    break;
                case DogSearchBy.Veterinarian:
                    headerList.Add("CRITERIA");
                    break;
            }
            SetCriteriaFields(numberOfColumns,headerList,criteriaList);
        }

        private void SetCriteriaFields(int visibleColumns, IEnumerable<string> headers, IEnumerable<string> values)
        {
	        lblCriteria1.Text = headers.ElementAt(0);
	        txtCriteria1.Text = values.ElementAt(0);
	        txtCriteria1.Left = lblCriteria1.Left + lblCriteria1.Width + 24;

	        if (visibleColumns > 1)
	        {
		        if (headers.ElementAt(0) == "NUMBER")
		        {
			        txtCriteria1.Width = 70;
			        txtCriteria2.Width = 220;
                }
		        else
		        {
			        txtCriteria1.Width = 145;
			        txtCriteria2.Width = 145;
                }

		        lblCriteria2.Left = txtCriteria1.Left + txtCriteria1.Width + 48;
		        lblCriteria2.Visible = true;
		        txtCriteria2.Visible = true;
                lblCriteria2.Text = headers.ElementAt(1);
		        txtCriteria2.Text = values.ElementAt(1);
		        txtCriteria2.Left = lblCriteria2.Left + lblCriteria2.Width + 24;
            }
	        else
	        {
		        txtCriteria1.Width = 385;
		        lblCriteria2.Visible = false;
		        txtCriteria2.Visible = false;
	        }
        }
        
        private void UpdateViewModel()
        {
            viewModel.SearchCriteria.TypesOfRecords = GetTypeOfRecords();
            viewModel.SearchCriteria.InlcudeDeletedDogsOrOwners = chkIncludeDeleted.Checked;
            viewModel.SearchCriteria.MatchCriteriaType = GetMatchCriteriaType();
            viewModel.SearchCriteria.TypeToSearchBy = GetSearchBy();
            SetSearchByFields(viewModel.SearchCriteria);
        }

        private void SetSearchByFields(DogSearchCriteria searchCriteria)
        {
            var criteria = GetCriteriaChoices();
            switch (searchCriteria.TypeToSearchBy)
            {
                case DogSearchBy.OwnerName:
                    searchCriteria.OwnerLastName = criteria.ElementAt(0);
                    searchCriteria.OwnerFirstName = criteria.ElementAt(1);
                    break;
                case DogSearchBy.Breed:
                    searchCriteria.Breed = criteria.ElementAt(0);
                    break;
                case DogSearchBy.City:
                    searchCriteria.City = criteria.ElementAt(0) ;
                    break;
                case DogSearchBy.DogName:
                    searchCriteria.DogName = criteria.ElementAt(0);
                    break;
                case DogSearchBy.KennelLicense:
                    searchCriteria.KennelLicense = criteria.ElementAt(0).ToIntegerValue();
                    break;
                case DogSearchBy.LicenseTagNumber:
                    searchCriteria.LicenseTag = criteria.ElementAt(0);
                    break;
                case DogSearchBy.Location:
                    searchCriteria.LocationNumber = criteria.ElementAt(0).ToIntegerValue();
                    searchCriteria.LocationStreet = criteria.ElementAt(1);
                    break;
                case DogSearchBy.RabiesTagNumber:
                    searchCriteria.RabiesTag = criteria.ElementAt(0);
                    break;
                case DogSearchBy.Veterinarian:
                    searchCriteria.Veterinarian = criteria.ElementAt(0);
                    break;
            }
        }

        private DogSearchRecordType GetTypeOfRecords()
        {
            GenericDescriptionPair<DogSearchRecordType> choice = (GenericDescriptionPair<DogSearchRecordType> )cmbTypes.SelectedItem;
            if (choice != null)
            {
                return choice.ID;
            }

            return DogSearchRecordType.All;
        }

        private SearchCriteriaMatchType GetMatchCriteriaType()
        {
            GenericDescriptionPair<SearchCriteriaMatchType> choice =
                (GenericDescriptionPair<SearchCriteriaMatchType>) cmbCriteria.SelectedItem;
            if (choice != null)
            {
                return choice.ID;
            }

            return SearchCriteriaMatchType.StartsWith;
        }

        private DogSearchBy GetSearchBy()
        {
            GenericDescriptionPair<DogSearchBy> choice = (GenericDescriptionPair<DogSearchBy>)cmbSearchBy.SelectedItem;
            if (choice != null)
            {
                return choice.ID;
            }

            return DogSearchBy.OwnerName;
        }

        private IEnumerable<string> GetCriteriaChoices()
        {
            var choices = new List<string>();
            choices.Add(txtCriteria1.Text);
            if (txtCriteria2.Visible)
            {
                choices.Add(txtCriteria2.Text);
            }
            else
            {
                choices.Add("");
            }

            return choices;
        }

        private void txtCriteria1_KeyDown(object sender, KeyEventArgs e)
        {
	        if (e.KeyCode == Keys.Return)
	        {
		        e.Handled = true;
		        cmdProcess.Focus();
		        Search();
	        }
        }

        private void txtCriteria2_KeyDown(object sender, KeyEventArgs e)
        {
	        if (e.KeyCode == Keys.Return)
	        {
		        e.Handled = true;
		        cmdProcess.Focus();
		        Search();
	        }
        }

        private int OwnerId(int row)
        {
	        return Convert.ToInt32(GridSearch.TextMatrix(row, (int)SearchColumn.OwnerNum));
        }

        private bool OwnerDeleted(int row)
        {
	        return Convert.ToBoolean(GridSearch.TextMatrix(row, (int)SearchColumn.OwnerDeleted));
        }

        private string DogNames(int row)
        {
	        return GridSearch.RowData(row) != null ? GridSearch.RowData(row).ToString() : "";
        }

        private enum SearchColumn
        {
	        OwnerNum = 0,
	        LastName = 1,
	        FirstName = 2,
	        LocationNumber = 3,
	        LocationStreet = 4,
	        KennelNumber = 5,
	        DogName = 6,
	        Breed = 7,
	        DogColor = 8,
	        OwnerDeleted = 9
        }

        private void frmSearchDog_Resize(object sender, EventArgs e)
        {
            ResizeGridSearch();
        }

        private void cmdProcessClearSearch_Click(object sender, EventArgs e)
        {
	        ClearSearchCriteria();
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
	        Search();
        }
    }
}
