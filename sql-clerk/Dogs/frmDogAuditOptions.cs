﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Extensions;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDateChange.
	/// </summary>
	public partial class frmDogAuditOptions : fecherFoundation.FCForm, IModalView<IDogAuditOptionsViewModel>
    {
        private bool cancelling = true;
		public frmDogAuditOptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmDogAuditOptions(IDogAuditOptionsViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

		private void InitializeComponentEx()
		{
            this.Closing += FrmDogAuditOptions_Closing;
            this.Load += FrmDogAuditOptions_Load;
            this.cmdFromDate.Click += CmdFromDate_Click;
            this.cmdToDate.Click += CmdToDate_Click;
		}

        private void CmdToDate_Click(object sender, EventArgs e)
        {
            DateTime datSelectedDate = DateTime.FromOADate(0);
            if (dtpEndRange.NullableValue.HasValue)
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, dtpEndRange.Value);
            }
            else
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.Now);
            }

            if (datSelectedDate.ToOADate() != 0)
            {
                dtpEndRange.Value = datSelectedDate;
            }
        }

        private void CmdFromDate_Click(object sender, EventArgs e)
        {
            DateTime datSelectedDate = DateTime.FromOADate(0);
            if (dtpStartRange.NullableValue.HasValue)
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, dtpStartRange.Value);
            }
            else
            {
                frmCalender.InstancePtr.Init(ref datSelectedDate, DateTime.Now);
            }

            if (datSelectedDate.ToOADate() != 0)
            {
                dtpStartRange.Value = datSelectedDate;
            }
        }

        private void FrmDogAuditOptions_Load(object sender, EventArgs e)
        {
            FillForm();
        }

        private void FillForm()
        {
            chkIncludeOnline.Checked = ViewModel.IncludeOnlineTransactions;
            if (!ViewModel.StartDate.Equals(DateTime.MinValue))
            {
                dtpStartRange.Value = ViewModel.StartDate;
            }

            if (!ViewModel.EndDate.Equals(DateTime.MinValue) && !ViewModel.EndDate.Equals(DateTime.MaxValue))
            {
                dtpEndRange.Value = ViewModel.EndDate;
            }
        }

        private void FrmDogAuditOptions_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
			if (cancelling)
            {
                ViewModel.Cancelled = true;
            }
            else
            {
                ViewModel.Cancelled = false;
            }
		}

       

		private void cmdOk_Click(object sender, System.EventArgs e)
        {

            var startDate = DateTime.MinValue;
            var endDate = DateTime.MaxValue;
            if (dtpStartRange.NullableValue.HasValue)
            {
                startDate = dtpStartRange.NullableValue.Value;
            }

            if (dtpEndRange.NullableValue.HasValue)
            {
                endDate = dtpEndRange.NullableValue.Value;
            }
            
            ViewModel.SetOptions(chkIncludeOnline.Checked, startDate, endDate);
            CloseWithoutCancelling();
        }


        private void CloseWithoutCancelling()
        {
            cancelling = false;
            Close();
        }
		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

        private void frmDogAuditOptions_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
				Close();
			e.KeyChar = Strings.Chr(KeyAscii);
		}

        public IDogAuditOptionsViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private void frmDogAuditOptions_Load_1(object sender, EventArgs e)
        {

        }

        private void cmdToDate_Click_1(object sender, EventArgs e)
        {

        }
    }
}
