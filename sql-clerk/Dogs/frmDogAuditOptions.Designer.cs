﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDateChange.
	/// </summary>
	partial class frmDogAuditOptions : fecherFoundation.FCForm
	{
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdOk;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			//if (_InstancePtr == this)
			//{
			//	_InstancePtr = null;
			//	Sys.ClearInstance(this);
			//}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDogAuditOptions));
            this.cmdQuit = new fecherFoundation.FCButton();
            this.cmdOk = new fecherFoundation.FCButton();
            this.chkIncludeOnline = new fecherFoundation.FCCheckBox();
            this.dtpStartRange = new fecherFoundation.FCDateTimePicker();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.dtpEndRange = new fecherFoundation.FCDateTimePicker();
            this.cmdFromDate = new fecherFoundation.FCButton();
            this.cmdToDate = new fecherFoundation.FCButton();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeOnline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToDate)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdQuit
            // 
            this.cmdQuit.AppearanceKey = "actionButton";
            this.cmdQuit.Location = new System.Drawing.Point(187, 224);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(87, 40);
            this.cmdQuit.TabIndex = 7;
            this.cmdQuit.Text = "Cancel";
            this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "actionButton";
            this.cmdOk.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdOk.Location = new System.Drawing.Point(80, 224);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(87, 40);
            this.cmdOk.TabIndex = 6;
            this.cmdOk.Text = "Process";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // chkIncludeOnline
            // 
            this.chkIncludeOnline.Location = new System.Drawing.Point(80, 16);
            this.chkIncludeOnline.Name = "chkIncludeOnline";
            this.chkIncludeOnline.Size = new System.Drawing.Size(193, 22);
            this.chkIncludeOnline.TabIndex = 1;
            this.chkIncludeOnline.Text = "Include Online Transactions";
            // 
            // dtpStartRange
            // 
            this.dtpStartRange.AutoSize = false;
            this.dtpStartRange.CustomFormat = "MM/dd/yyyy";
            this.dtpStartRange.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpStartRange.Location = new System.Drawing.Point(70, 81);
            this.dtpStartRange.Mask = "00/00/0000";
            this.dtpStartRange.Name = "dtpStartRange";
            this.dtpStartRange.ShowCalendar = false;
            this.dtpStartRange.Size = new System.Drawing.Size(142, 36);
            this.dtpStartRange.TabIndex = 2;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(70, 60);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(42, 15);
            this.fcLabel1.TabIndex = 9;
            this.fcLabel1.Text = "FROM";
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(70, 134);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(22, 15);
            this.fcLabel2.TabIndex = 11;
            this.fcLabel2.Text = "TO";
            // 
            // dtpEndRange
            // 
            this.dtpEndRange.AutoSize = false;
            this.dtpEndRange.CustomFormat = "MM/dd/yyyy";
            this.dtpEndRange.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpEndRange.Location = new System.Drawing.Point(70, 155);
            this.dtpEndRange.Mask = "00/00/0000";
            this.dtpEndRange.Name = "dtpEndRange";
            this.dtpEndRange.ShowCalendar = false;
            this.dtpEndRange.Size = new System.Drawing.Size(142, 36);
            this.dtpEndRange.TabIndex = 4;
            // 
            // cmdFromDate
            // 
            this.cmdFromDate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.cmdFromDate.AppearanceKey = "imageButton";
            this.cmdFromDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdFromDate.Location = new System.Drawing.Point(229, 81);
            this.cmdFromDate.Name = "cmdFromDate";
            this.cmdFromDate.Size = new System.Drawing.Size(44, 36);
            this.cmdFromDate.TabIndex = 3;
            // 
            // cmdToDate
            // 
            this.cmdToDate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.cmdToDate.AppearanceKey = "imageButton";
            this.cmdToDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdToDate.Location = new System.Drawing.Point(229, 155);
            this.cmdToDate.Name = "cmdToDate";
            this.cmdToDate.Size = new System.Drawing.Size(44, 36);
            this.cmdToDate.TabIndex = 5;
            this.cmdToDate.Click += new System.EventHandler(this.cmdToDate_Click_1);
            // 
            // frmDogAuditOptions
            // 
            this.AcceptButton = this.cmdOk;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(356, 277);
            this.Controls.Add(this.cmdToDate);
            this.Controls.Add(this.cmdFromDate);
            this.Controls.Add(this.fcLabel2);
            this.Controls.Add(this.dtpEndRange);
            this.Controls.Add(this.fcLabel1);
            this.Controls.Add(this.dtpStartRange);
            this.Controls.Add(this.chkIncludeOnline);
            this.Controls.Add(this.cmdQuit);
            this.Controls.Add(this.cmdOk);
            this.FillColor = 16777215;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDogAuditOptions";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Transaction Audit";
            this.Load += new System.EventHandler(this.frmDogAuditOptions_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeOnline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

        private FCCheckBox chkIncludeOnline;
        private FCDateTimePicker dtpStartRange;
        private FCLabel fcLabel1;
        private FCLabel fcLabel2;
        private FCDateTimePicker dtpEndRange;
        public FCButton cmdFromDate;
        public FCButton cmdToDate;
    }
}
