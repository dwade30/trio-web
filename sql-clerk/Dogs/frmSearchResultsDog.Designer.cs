﻿namespace TWCK0000.Dogs
{
    partial class frmSearchResultsDog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GridSearch = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Shape1 = new fecherFoundation.FCLabel();
            this.btnOpenRecord = new fecherFoundation.FCButton();
            this.toolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenRecord)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnOpenRecord);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Shape1);
            this.ClientArea.Controls.Add(this.GridSearch);
            // 
            // TopPanel
            // 
            this.toolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(179, 30);
            this.HeaderText.Text = "Search Results";
            // 
            // GridSearch
            // 
            this.GridSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridSearch.Cols = 10;
            this.GridSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridSearch.ExtendLastCol = true;
            this.GridSearch.FixedCols = 0;
            this.GridSearch.Location = new System.Drawing.Point(44, 74);
            this.GridSearch.Name = "GridSearch";
            this.GridSearch.RowHeadersVisible = false;
            this.GridSearch.Rows = 1;
            this.GridSearch.Size = new System.Drawing.Size(609, 394);
            this.GridSearch.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(68, 31);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(250, 15);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "DELETED DOG OR OWNER RECORD";
            // 
            // Shape1
            // 
            this.Shape1.BackColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.Shape1.Location = new System.Drawing.Point(44, 31);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(9, 10);
            this.Shape1.TabIndex = 4;
            // 
            // btnOpenRecord
            // 
            this.btnOpenRecord.AppearanceKey = "acceptButton";
            this.btnOpenRecord.Location = new System.Drawing.Point(405, 38);
            this.btnOpenRecord.Name = "btnOpenRecord";
            this.btnOpenRecord.Size = new System.Drawing.Size(160, 39);
            this.btnOpenRecord.Text = "Open Record";
            // 
            // frmSearchResultsDog
            // 
            this.Name = "frmSearchResultsDog";
            this.Text = "Search Results";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenRecord)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCGrid GridSearch;
        public fecherFoundation.FCLabel Label1;
        public fecherFoundation.FCLabel Shape1;
        private fecherFoundation.FCButton btnOpenRecord;
        private Wisej.Web.ToolTip toolTip1;
    }
}