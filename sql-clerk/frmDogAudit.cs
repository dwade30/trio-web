//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using System.Linq;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
    public partial class frmDogAudit : BaseForm
    {
        public frmDogAudit()
        {
            //
            // required for windows form designer support
            //
            StickPanelToBottom = true;
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }

     

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmDogAudit InstancePtr
        {
            get
            {
                return (frmDogAudit)Sys.GetInstance(typeof(frmDogAudit));
            }
        }

        protected frmDogAudit _InstancePtr = null;
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By
        // Date
        // ********************************************************
        const int CNSTGRIDCOLYEAR = 0;
        const int CNSTGRIDCOLMONTH = 1;
        const int CNSTGRIDCOLTRANSACTIONDATE = 2;
        const int CNSTGRIDCOLUSER = 3;
        const int CNSTGRIDCOLAMOUNT = 4;
        const int CNSTGRIDCOLDESCRIPTION = 5;
        const int CNSTGRIDCOLOWNER = 6;
        const int CNSTGRIDCOLID = 7;
        const int CNSTGRIDCOLSTATE = 8;
        const int CNSTGRIDCOLTOWN = 9;
        const int CNSTGRIDCOLCLERK = 10;
        const int CNSTGRIDCOLLATE = 11;
        const int CNSTGRIDCOLANIMALCONTROL = 12;
        const int CNSTGRIDCOLLICENSEFEE = 13;
        const int CNSTGRIDCOLWARRANTFEE = 14;
        const int CNSTGRIDCOLLATEFEE = 15;
        const int CNSTGRIDCOLREPLACEMENTSTICKERFEE = 16;
        const int CNSTGRIDCOLREPLACEMENTLICENSEFEE = 17;
        const int CNSTGRIDCOLREPLACEMENTTAGFEE = 18;
        const int CNSTGRIDCOLTRANSFERLICENSEFEE = 19;
        const int CNSTGRIDCOLTEMPLICENSEFEE = 20;
        const int CNSTGRIDCOLUSERID = 21;
        
        private bool boolEditAny;
        private bool boolEditOwn;
        private DogAuditOptions auditOptions;
        private void frmDogAudit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            }
            //end switch
        }

        public void Init(DogAuditOptions options)
        {
            auditOptions = options;
            if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTEDITANYTRANSACTIONS)) != "F")
            {
                boolEditAny = false;
                boolEditOwn = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTEDITOWNTRANSACTIONS)) == "F";
            }
            else
            {
                boolEditAny = true;
                boolEditOwn = true;
            }
            Show(App.MainForm);
        }

        private void frmDogAudit_Load(object sender, System.EventArgs e)
        {
            FCUtils.StartTask(this, () =>
            {
                try
                {
                    modDogs.GetDogFees();
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                    modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
                    modGlobalFunctions.SetTRIOColors(this);
                    SetupGrid();
                    try
                    {
                        Grid.Redraw = false;
                        ShowLoader = true;
                        LoadGrid();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        ShowLoader = false;
                    }
                    modColorScheme.ColorGrid(Grid);
                    Grid.Redraw = true;

                    //FC:FINAl:SBE - #i2338 - because of Redraw false/true the Row index is not updated on client side
                    Grid.Row = 0;
                    Grid.AddExpandButton(0);
                }
                catch (Exception ex)
                {
                    Telemetry.TrackException(ex);

                    throw;
                }
                finally
                {
                    //App.MainForm.ShowLoader = false;
                    EndWait();
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                }
            });

        }

        private void SetupGrid()
        {
            string strTemp;
            strTemp = modDogs.Statics.DogFee.UserDefinedDescription1;
            if (strTemp == string.Empty)
                strTemp = "User Fee";
            Grid.FixedCols = 0;
            Grid.Rows = 1;
            Grid.Cols = 22;
            Grid.ColHidden(CNSTGRIDCOLID, true);
            Grid.ColHidden(CNSTGRIDCOLUSERID, true);
            Grid.ColDataType(CNSTGRIDCOLTRANSACTIONDATE, FCGrid.DataTypeSettings.flexDTDate);
            Grid.ColDataType(CNSTGRIDCOLMONTH, FCGrid.DataTypeSettings.flexDTLong);
            Grid.ColDataType(CNSTGRIDCOLYEAR, FCGrid.DataTypeSettings.flexDTLong);
            Grid.ColDataType(CNSTGRIDCOLAMOUNT, FCGrid.DataTypeSettings.flexDTDouble);
            Grid.TextMatrix(0, CNSTGRIDCOLYEAR, "Year");
            Grid.TextMatrix(0, CNSTGRIDCOLMONTH, "Month");
            Grid.TextMatrix(0, CNSTGRIDCOLTRANSACTIONDATE, "Date");
            Grid.TextMatrix(0, CNSTGRIDCOLAMOUNT, "Amount");
            Grid.TextMatrix(0, CNSTGRIDCOLOWNER, "Owner");
            Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
            Grid.TextMatrix(0, CNSTGRIDCOLUSER, "User");
            Grid.TextMatrix(0, CNSTGRIDCOLSTATE, "State");
            Grid.TextMatrix(0, CNSTGRIDCOLTOWN, "Town");
            Grid.TextMatrix(0, CNSTGRIDCOLCLERK, "Clerk");
            Grid.TextMatrix(0, CNSTGRIDCOLLATE, "Late");
            Grid.TextMatrix(0, CNSTGRIDCOLANIMALCONTROL, strTemp);
            Grid.TextMatrix(0, CNSTGRIDCOLLICENSEFEE, "License");
            Grid.TextMatrix(0, CNSTGRIDCOLWARRANTFEE, "Warrant");
            Grid.TextMatrix(0, CNSTGRIDCOLLATEFEE, "Late Fee");
            Grid.TextMatrix(0, CNSTGRIDCOLREPLACEMENTSTICKERFEE, "Repl. Stick");
            Grid.TextMatrix(0, CNSTGRIDCOLREPLACEMENTLICENSEFEE, "Repl. Lic.");
            Grid.TextMatrix(0, CNSTGRIDCOLREPLACEMENTTAGFEE, "Repl. Tag");
            Grid.TextMatrix(0, CNSTGRIDCOLTRANSFERLICENSEFEE, "Tran. Lic.");
            Grid.TextMatrix(0, CNSTGRIDCOLTEMPLICENSEFEE, "Temp Lic.");
        }

        private void ResizeGrid()
        {
            int GridWidth = 0;
            GridWidth = Grid.WidthOriginal;
            Grid.ColWidth(CNSTGRIDCOLYEAR, FCConvert.ToInt32(0.07 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLMONTH, FCConvert.ToInt32(0.07 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLTRANSACTIONDATE, FCConvert.ToInt32(0.11 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLUSER, FCConvert.ToInt32(0.2 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLAMOUNT, FCConvert.ToInt32(0.08 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLDESCRIPTION, FCConvert.ToInt32(0.2 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLOWNER, FCConvert.ToInt32(0.20 * GridWidth));
            //Grid.AutoSize(CNSTGRIDCOLANIMALCONTROL);
            //FC:FINAL:DDU:#2491 - align to right month column
            Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDCOLMONTH, Grid.Rows - 1, CNSTGRIDCOLMONTH, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

        private void LoadGrid()
        {
            var dogAuditItems = StaticSettings.GlobalCommandDispatcher.Send(new GetDogAuditItems(auditOptions.StartDate.StartOfDay(), auditOptions.EndDate.EndOfDay(),auditOptions.InlcudeOnlineTransactions)).Result;
           string strSQL = "";
            int lngYear = 0;
            int lngMonth = 0;
            string strToolTip = "";
            string strComma = "";
            clsDRWrapper clsTemp = new clsDRWrapper();
            string[] strAry = null;
            int x;
            int lngTemp = 0;
            decimal dblTemp = 0;
            int lngCurYearRow;
            int lngCurMonthRow;
            decimal dblYearTot;
            decimal dblMonthTot;
            decimal dblYearState;
            decimal dblYearTown;
            decimal dblYearClerk;
            decimal dblYearLate;
            decimal dblYearUser;
            decimal dblYearLicense;
            decimal dblYearWarrant;
            decimal dblYearReplLic;
            decimal dblYearReplTag;
            decimal dblYearTranLic;
            decimal dblYearTempLic;
            decimal dblMonthState;
            decimal dblMonthTown;
            decimal dblMonthClerk;
            decimal dblMonthLate;
            decimal dblMonthUser;
            decimal dblMonthLicense;
            decimal dblMonthWarrant;
            decimal dblMonthReplLic;
            decimal dblMonthReplTag;
            decimal dblMonthTranLic;
            decimal dblMonthTempLic;
            dblYearTot = 0;
            dblMonthTot = 0;
            dblYearState = 0;
            dblYearTown = 0;
            dblYearClerk = 0;
            dblYearLate = 0;
            dblYearUser = 0;
            dblYearLicense = 0;
            dblYearWarrant = 0;
            dblYearReplLic = 0;
            dblYearReplTag = 0;
            dblYearTranLic = 0;
            dblYearTempLic = 0;
            dblMonthState = 0;
            dblMonthTown = 0;
            dblMonthClerk = 0;
            dblMonthLate = 0;
            dblMonthUser = 0;
            dblMonthLicense = 0;
            dblMonthWarrant = 0;
            dblMonthReplLic = 0;
            dblMonthReplTag = 0;
            dblMonthTranLic = 0;
            dblMonthTempLic = 0;
            lngCurYearRow = 0;
            lngCurMonthRow = 0;

            Grid.OutlineBar = FCGrid.OutlineBarSettings.flexOutlineBarSimple;

            const string numericFormat = "#,###,###,##0";
            int rowCount = 0;
            int recordCount = dogAuditItems.Count();
            int itemNumber = 0;
            var partyCont = new cPartyController();
            foreach (var dogAuditItem in dogAuditItems)
            {
                //itemNumber++;
                //rowCount++;
                //if (rowCount == 100)
                //{
                //    rowCount = 0;
                //    UpdateWait($"Loading data ... Record {itemNumber} of {recordCount}");
                //}
                strToolTip = "";

                var transactionDate = dogAuditItem.TransactionDate;

                if (transactionDate.Year != lngYear)
                {
                    if (lngCurYearRow > 0)
                    {
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLAMOUNT, dblYearTot.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLCLERK, dblYearClerk.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLSTATE, dblYearState.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLTOWN, dblYearTown.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLLATE, dblYearLate.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLANIMALCONTROL, dblYearUser.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLLATEFEE, (dblYearLate - dblYearWarrant).FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLLICENSEFEE, dblYearLicense.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLWARRANTFEE, dblYearWarrant.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLREPLACEMENTLICENSEFEE, dblYearReplLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLREPLACEMENTTAGFEE, dblYearReplTag.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLTRANSFERLICENSEFEE, dblYearTranLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLTEMPLICENSEFEE, dblYearTempLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                    }
                    if (lngCurMonthRow > 0)
                    {
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLAMOUNT, dblMonthTot.FormatAsCurrencyOptionalDecimalNoSymbol());
                    }
                    lngMonth = 0;
                    lngYear = transactionDate.Year;
                    Grid.Rows += 1;
                    Grid.RowOutlineLevel(Grid.Rows - 1, 0);
                    Grid.IsSubtotal(Grid.Rows - 1, true);
                    Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLYEAR, lngYear.ToString());
                    lngCurYearRow = Grid.Rows - 1;
                    dblYearTot = 0;
                    dblYearState = 0;
                    dblYearTown = 0;
                    dblYearClerk = 0;
                    dblYearLate = 0;
                    dblYearUser = 0;
                    dblYearLicense = 0;
                    dblYearWarrant = 0;
                    dblYearReplLic = 0;
                    dblYearReplTag = 0;
                    dblYearTranLic = 0;
                    dblYearTempLic = 0;
                }
                if (transactionDate.Month != lngMonth)
                {
                    if (lngCurMonthRow > 0)
                    {
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLAMOUNT, dblMonthTot.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLSTATE, dblMonthState.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLCLERK, dblMonthClerk.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLTOWN, dblMonthTown.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLLATE, dblMonthLate.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLANIMALCONTROL, dblMonthUser.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLLATEFEE, (dblMonthLate - dblMonthWarrant).FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLLICENSEFEE, dblMonthLicense.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLWARRANTFEE, dblMonthWarrant.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLREPLACEMENTLICENSEFEE, dblMonthReplLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLREPLACEMENTTAGFEE, dblMonthReplTag.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLTRANSFERLICENSEFEE, dblMonthTranLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                        Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLTEMPLICENSEFEE, dblMonthTempLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                    }
                    dblMonthTot = 0;
                    dblMonthTown = 0;
                    dblMonthState = 0;
                    dblMonthClerk = 0;
                    dblMonthLate = 0;
                    dblMonthUser = 0;
                    dblMonthLicense = 0;
                    dblMonthWarrant = 0;
                    dblMonthReplLic = 0;
                    dblMonthReplTag = 0;
                    dblMonthTranLic = 0;
                    dblMonthTempLic = 0;
                    lngMonth = transactionDate.Month;
                    Grid.Rows += 1;
                    Grid.RowOutlineLevel(Grid.Rows - 1, 1);
                    Grid.IsSubtotal(Grid.Rows - 1, true);
                    Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLMONTH, lngMonth.ToString());
                    Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Rows - 1, 0, Grid.Rows - 1, Grid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                    lngCurMonthRow = Grid.Rows - 1;
                }
                Grid.Rows += 1;
                //FC:FINAL:AM:#i2029 - have to set the level for the data rows too
                Grid.RowOutlineLevel(Grid.Rows - 1, 2);
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLTRANSACTIONDATE, transactionDate.FormatAndPadShortDate());
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLAMOUNT,  dogAuditItem.Amount);
                var amount = dogAuditItem.Amount;
                dblMonthTot += amount;
                dblYearTot += amount;
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLUSER, dogAuditItem.UserName);
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLUSERID, dogAuditItem.UserID);
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLDESCRIPTION, dogAuditItem.Description);
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLOWNER, dogAuditItem.TransactionPerson);
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLOWNER, dogAuditItem.OwnerName);
                var stateFee = dogAuditItem.StateFee;
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLSTATE, stateFee.ToString());
                dblYearState += stateFee;
                dblMonthState += stateFee;
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLTOWN, dogAuditItem.TownFee.ToString());
                dblYearTown += dogAuditItem.TownFee;
                dblMonthTown += dogAuditItem.TownFee;
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLCLERK, dogAuditItem.ClerkFee.ToString());
                dblYearClerk += dogAuditItem.ClerkFee;
                dblMonthClerk += dogAuditItem.ClerkFee;
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLLATE, dogAuditItem.LateFee.ToString());
                dblYearLate += dogAuditItem.LateFee;
                dblMonthLate += dogAuditItem.LateFee;
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLANIMALCONTROL, dogAuditItem.AnimalControl.ToString());
                dblYearUser += dogAuditItem.AnimalControl;
                dblMonthUser += dogAuditItem.AnimalControl;
                Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLID, dogAuditItem.DogTransactionID);
                //Grid.Row = Grid.Rows - 1;
                //Grid.Col = CNSTGRIDCOLSTATE;
                //Grid.CellBorder(Color.Black, 3, -1, -1, -1, -1, -1);
                //Grid.Col = CNSTGRIDCOLANIMALCONTROL;
                //Grid.CellBorder(Color.Black, -1, -1, 3, -1, -1, -1);
                if (dogAuditItem.IsKennel)
                {
                    // kennel
                    strComma = "";
                    strToolTip = "Dog Name(s): ";
                    if (!dogAuditItem.KennelDogs.IsNullOrWhiteSpace())
                    {
                        strAry = Strings.Split(dogAuditItem.KennelDogs, ",", -1, CompareConstants.vbTextCompare);
                        for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
                        {
                            clsTemp.OpenRecordset("select dogname from doginfo where ID = " + strAry[x], "Clerk");
                            if (!clsTemp.EndOfFile())
                            {
                                strToolTip += strComma + clsTemp.Get_Fields_String("dogname");
                            }
                            strComma = ",";
                        }
                        // x
                    }
                    if (dogAuditItem.IsLicense)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLLICENSEFEE,dogAuditItem.KennelLic1Fee.ToString());
                        dblYearLicense += dogAuditItem.KennelLic1Fee;
                        dblMonthLicense += dogAuditItem.KennelLic1Fee;
                    }
                    if (dogAuditItem.KennelWarrantFee > 0)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLWARRANTFEE, dogAuditItem.KennelWarrantFee.ToString());
                        dblYearWarrant += dogAuditItem.KennelWarrantFee;
                        dblMonthWarrant += dogAuditItem.KennelWarrantFee;
                    }
                    if (dogAuditItem.KennelLateFee > 0)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLLATEFEE, dogAuditItem.KennelLateFee.ToString());
                    }
                }
                else
                {
                    // reg dog
                    //clsTemp.OpenRecordset("select dogname,id from doginfo where ID = " + dogAuditItem.DogNumber, "Clerk");
                    //if (!clsTemp.EndOfFile())
                    //{
                    //    strToolTip = "Dog Name: " + clsTemp.Get_Fields_String("dogname");
                    //}
                    strToolTip = "Dog Name: " + dogAuditItem.DogName;
                    if (dogAuditItem.WarrantFee > 0)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLWARRANTFEE, dogAuditItem.WarrantFee);
                        dblYearWarrant += dogAuditItem.WarrantFee;
                        dblMonthWarrant += dogAuditItem.WarrantFee;
                    }
                    if (dogAuditItem.LateFee > 0)
                    {
                        dblTemp = dogAuditItem.LateFee - dogAuditItem.WarrantFee;
                        if (dblTemp > 0)
                        {
                            Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLLATEFEE, dblTemp);
                        }
                    }
                    if (dogAuditItem.IsLicense)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLLICENSEFEE, dogAuditItem.SpayNonSpayAmount);
                        dblYearLicense += dogAuditItem.SpayNonSpayAmount;
                        dblMonthLicense += dogAuditItem.SpayNonSpayAmount;
                    }
                    if (dogAuditItem.IsReplacementSticker)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLREPLACEMENTSTICKERFEE, dogAuditItem.ReplacementStickerFee);
                    }
                    if ( dogAuditItem.IsReplacementLicense)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLREPLACEMENTLICENSEFEE, dogAuditItem.ReplacementLicenseFee);
                        dblYearReplLic += dogAuditItem.ReplacementLicenseFee;
                        dblMonthReplLic += dogAuditItem.ReplacementLicenseFee;
                    }
                    if (dogAuditItem.IsReplacementTag)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLREPLACEMENTTAGFEE,dogAuditItem.ReplacementTagFee);
                        dblYearReplTag += dogAuditItem.ReplacementTagFee;
                        dblMonthReplTag += dogAuditItem.ReplacementTagFee;
                    }
                    if (dogAuditItem.IsTransfer)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLTRANSFERLICENSEFEE, dogAuditItem.TransferLicenseFee);
                        dblYearTranLic += dogAuditItem.TransferLicenseFee;
                        dblMonthTranLic += dogAuditItem.TransferLicenseFee;
                    }
                    if (dogAuditItem.IsTempLicense)
                    {
                        Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLTEMPLICENSEFEE,dogAuditItem.TempLicenseFee);
                        dblYearTempLic += dogAuditItem.TempLicenseFee;
                        dblMonthTempLic += dogAuditItem.TempLicenseFee;
                    }
                }
                Grid.RowData(Grid.Rows - 1, strToolTip);
            }
            if (lngCurYearRow > 0)
            {
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLAMOUNT, dblYearTot.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLCLERK, dblYearClerk.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLSTATE, dblYearState.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLTOWN, dblYearTown.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLLATE, dblYearLate.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLANIMALCONTROL, dblYearUser.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLLATEFEE, (dblYearLate - dblYearWarrant).FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLLICENSEFEE, dblYearLicense.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLWARRANTFEE, dblYearWarrant.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLREPLACEMENTLICENSEFEE, dblYearReplLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLREPLACEMENTTAGFEE, dblYearReplTag.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLTRANSFERLICENSEFEE, dblYearTranLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurYearRow, CNSTGRIDCOLTEMPLICENSEFEE, dblYearTempLic.FormatAsCurrencyOptionalDecimalNoSymbol());
            }
            if (lngCurMonthRow > 0)
            {
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLAMOUNT, dblMonthTot.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLSTATE, dblMonthState.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLCLERK, dblMonthClerk.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLTOWN, dblMonthTown.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLLATE, dblMonthLate.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLANIMALCONTROL, dblMonthUser.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLLATEFEE, (dblMonthLate - dblMonthWarrant).FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLLICENSEFEE, dblMonthLicense.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLWARRANTFEE, dblMonthWarrant.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLREPLACEMENTLICENSEFEE, dblMonthReplLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLREPLACEMENTTAGFEE, dblMonthReplTag.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLTRANSFERLICENSEFEE,dblMonthTranLic.FormatAsCurrencyOptionalDecimalNoSymbol());
                Grid.TextMatrix(lngCurMonthRow, CNSTGRIDCOLTEMPLICENSEFEE, dblMonthTempLic.FormatAsCurrencyOptionalDecimalNoSymbol());
            }
            Grid.Row = 0;
            Grid.Outline(1);
            //Grid.Redraw = true;
            //Grid.Visible = true;
        }

        private void frmDogAudit_Resize(object sender, System.EventArgs e)
        {
            ResizeGrid();
        }

        private void Grid_DblClick(object sender, System.EventArgs e)
        {
            int lngID;
            clsDRWrapper clsLoad = new clsDRWrapper();
            int lngRow;
            // vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
            int lngTemp = 0;
            int lngUID;
            if (Grid.MouseRow < 1)
                return;
            lngRow = Grid.MouseRow;
            if (Grid.IsSubtotal(lngRow))
            {
                return;
            }
            if (!boolEditAny && !boolEditOwn)
            {
                return;
            }
            if (modGlobalConstants.Statics.clsSecurityClass.Get_UserID() != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERID)))
            {
                if (!boolEditAny)
                    return;
            }
            lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLID))));
            frmEditTransaction.InstancePtr.Init(ref lngID);
            clsLoad.OpenRecordset("select * from dogtransactions where ID = " + FCConvert.ToString(lngID), "twck0000.vb1");
            if (!clsLoad.EndOfFile())
            {
                Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE, FCConvert.ToString(clsLoad.Get_Fields_DateTime("transactiondate")));
                Grid.TextMatrix(lngRow, CNSTGRIDCOLAMOUNT, FCConvert.ToString(clsLoad.Get_Fields("amount")));
                Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE, FCConvert.ToString(clsLoad.Get_Fields_Double("statefee")));
                Grid.TextMatrix(lngRow, CNSTGRIDCOLTOWN, FCConvert.ToString(clsLoad.Get_Fields_Double("townfee")));
                Grid.TextMatrix(lngRow, CNSTGRIDCOLCLERK, FCConvert.ToString(clsLoad.Get_Fields_Double("clerkfee")));
                Grid.TextMatrix(lngRow, CNSTGRIDCOLLATE, FCConvert.ToString(clsLoad.Get_Fields("latefee")));
                Grid.TextMatrix(lngRow, CNSTGRIDCOLANIMALCONTROL, FCConvert.ToString(clsLoad.Get_Fields_Double("animalcontrol")));
                if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("boolkennel")))
                {
                    // kennel
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("boolLicense")))
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLLICENSEFEE, FCConvert.ToString(clsLoad.Get_Fields_Double("kennellic1fee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLLICENSEFEE, "");
                    }
                    if (clsLoad.Get_Fields_Double("kennelwarrantfee") > 0)
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLWARRANTFEE, FCConvert.ToString(clsLoad.Get_Fields_Double("kennelwarrantfee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLWARRANTFEE, "");
                    }
                    if (clsLoad.Get_Fields_Double("kennellatefee") > 0)
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLLATEFEE, FCConvert.ToString(clsLoad.Get_Fields_Double("kennellatefee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLLATEFEE, "");
                    }
                }
                else
                {
                    // reg dog
                    if (Conversion.Val(clsLoad.Get_Fields("Warrantfee")) > 0)
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLWARRANTFEE, FCConvert.ToString(clsLoad.Get_Fields("warrantfee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLWARRANTFEE, "");
                    }
                    if (Conversion.Val(clsLoad.Get_Fields("latefee")) > 0)
                    {
                        lngTemp = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("latefee")) - Conversion.Val(clsLoad.Get_Fields("warrantfee")));
                        if (lngTemp > 0)
                        {
                            Grid.TextMatrix(lngRow, CNSTGRIDCOLLATEFEE, FCConvert.ToString(lngTemp));
                        }
                    }
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("boollicense")))
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLLICENSEFEE, FCConvert.ToString(clsLoad.Get_Fields_Double("spaynonspayamount")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLLICENSEFEE, "");
                    }
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("boolReplacementSTicker")))
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLREPLACEMENTSTICKERFEE, FCConvert.ToString(clsLoad.Get_Fields("replacementstickerfee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLREPLACEMENTSTICKERFEE, "");
                    }
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("boolReplacementLicense")))
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLREPLACEMENTLICENSEFEE, FCConvert.ToString(clsLoad.Get_Fields_Double("replacementlicensefee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLREPLACEMENTLICENSEFEE, "");
                    }
                    if (clsLoad.Get_Fields_Boolean("boolReplacementTag"))
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLREPLACEMENTTAGFEE, FCConvert.ToString(clsLoad.Get_Fields("replacementtagfee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLREPLACEMENTTAGFEE, "");
                    }
                    if (clsLoad.Get_Fields_Boolean("boolTransfer"))
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSFERLICENSEFEE, FCConvert.ToString(clsLoad.Get_Fields_Double("transferlicensefee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLTRANSFERLICENSEFEE, "");
                    }
                    if (clsLoad.Get_Fields_Boolean("boolTempLicense"))
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLTEMPLICENSEFEE, FCConvert.ToString(clsLoad.Get_Fields_Double("templicensefee")));
                    }
                    else
                    {
                        Grid.TextMatrix(lngRow, CNSTGRIDCOLTEMPLICENSEFEE, "");
                    }
                }
            }
        }

        private void Grid_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = Grid[e.ColumnIndex, e.RowIndex];
            int lngRow;
            string strToolTip = "";
            lngRow = Grid.GetFlexRowIndex(e.RowIndex);
            if (lngRow > 0)
            {
                strToolTip = FCConvert.ToString(Grid.RowData(lngRow));
            }
            else
            {
                strToolTip = "Click on a column heading to sort by that column";
            }
            //ToolTip1.SetToolTip(Grid, strToolTip);
            cell.ToolTipText = strToolTip;
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            //Close();
        }

        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void mnuPrintPreview_Click(object sender, System.EventArgs e)
        {
            // only send a list of visible months
            string strList;
            int x;
            string strMonth = "";
            string strYear = "";
            string strComma;
            string strYearList = "";
            strList = "";
            strComma = "";
            for (x = 1; x <= Grid.Rows - 1; x++)
            {
                if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLYEAR)) > 0)
                {
                    strYear = strComma + Grid.TextMatrix(x, CNSTGRIDCOLYEAR);
                    strYearList = "";
                }
                else if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLMONTH)) > 0)
                {
                    if (!Grid.RowHidden(x + 1))
                    {
                        strComma = ",";
                        strMonth = FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLMONTH)));
                        if (strYearList == string.Empty)
                        {
                            strYearList = strYear;
                            strList += strYearList;
                        }
                        strYearList += "|" + strMonth;
                        strList += "|" + strMonth;
                    }
                }
            }
            // x
            if (strList != string.Empty)
            {
                rptDogAudit.InstancePtr.Init(strList, true, 0, "", "", auditOptions.InlcudeOnlineTransactions);
            }
            else
            {
                MessageBox.Show("The report will only print months that are visible" + "\r\n" + "Please show at least one month", "No Month Showing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Grid_Click(object sender, EventArgs e)
        {

        }

        private void ClientArea_PanelCollapsed(object sender, EventArgs e)
        {

        }
    }
}
