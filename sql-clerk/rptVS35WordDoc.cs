﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptVS35WordDoc.
	/// </summary>
	public partial class rptVS35WordDoc : BaseSectionReport
	{
		public rptVS35WordDoc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptVS35WordDoc InstancePtr
		{
			get
			{
				return (rptVS35WordDoc)Sys.GetInstance(typeof(rptVS35WordDoc));
			}
		}

		protected rptVS35WordDoc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVS35WordDoc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		
	}
}
