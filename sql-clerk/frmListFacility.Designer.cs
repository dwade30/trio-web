//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmListFacility.
	/// </summary>
	partial class frmListFacility
	{
		public fecherFoundation.FCListBox lstData;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEdit;
		public fecherFoundation.FCToolStripMenuItem mnuSp1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lstData = new fecherFoundation.FCListBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEdit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSp1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdEditNames = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditNames)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdEditNames);
            this.BottomPanel.Location = new System.Drawing.Point(0, 335);
            this.BottomPanel.Size = new System.Drawing.Size(575, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lstData);
            this.ClientArea.Size = new System.Drawing.Size(575, 275);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(575, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(198, 30);
            this.HeaderText.Text = "Facility Members";
            // 
            // lstData
            // 
            this.lstData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lstData.Appearance = 0;
            this.lstData.BackColor = System.Drawing.SystemColors.Window;
            this.lstData.Location = new System.Drawing.Point(30, 30);
            this.lstData.MultiSelect = 0;
            this.lstData.Name = "lstData";
            this.lstData.Size = new System.Drawing.Size(513, 228);
            this.lstData.Sorted = false;
            this.lstData.TabIndex = 0;
            this.lstData.DoubleClick += new System.EventHandler(this.lstData_DoubleClick);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEdit,
            this.mnuSp1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuEdit
            // 
            this.mnuEdit.Index = 0;
            this.mnuEdit.Name = "mnuEdit";
            this.mnuEdit.Text = "Edit Names";
            this.mnuEdit.Click += new System.EventHandler(this.mnuEdit_Click);
            // 
            // mnuSp1
            // 
            this.mnuSp1.Index = 1;
            this.mnuSp1.Name = "mnuSp1";
            this.mnuSp1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdEditNames
            // 
            this.cmdEditNames.AppearanceKey = "acceptButton";
            this.cmdEditNames.Location = new System.Drawing.Point(215, 30);
            this.cmdEditNames.Name = "cmdEditNames";
            this.cmdEditNames.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdEditNames.Size = new System.Drawing.Size(129, 48);
            this.cmdEditNames.TabIndex = 0;
            this.cmdEditNames.Text = "Edit Names";
            this.cmdEditNames.Click += new System.EventHandler(this.mnuEdit_Click);
            // 
            // frmListFacility
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(575, 443);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmListFacility";
            this.Text = "Facility Members";
            this.Load += new System.EventHandler(this.frmListFacility_Load);
            this.Activated += new System.EventHandler(this.frmListFacility_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmListFacility_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmListFacility_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditNames)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdEditNames;
	}
}