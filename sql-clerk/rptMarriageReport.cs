//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptMarriageReport.
	/// </summary>
	public partial class rptMarriageReport : BaseSectionReport
	{
		public rptMarriageReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMarriageReport InstancePtr
		{
			get
			{
				return (rptMarriageReport)Sys.GetInstance(typeof(rptMarriageReport));
			}
		}

		protected rptMarriageReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMarriageReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();

		public void Init(ref string strSQL)
		{
			rsReport.OpenRecordset(strSQL, "twck0000.vb1");
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "MarriageReport");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Information.IsDate(rsReport.Get_Fields_String("ceremonydate")))
			{
				if (FCConvert.ToDateTime(rsReport.Get_Fields_String("ceremonydate")).ToOADate() != 0)
				{
					txtMarriageDate.Text = Strings.Format(rsReport.Get_Fields_String("ceremonydate"), "MM/dd/yyyy");
				}
				else
				{
					txtMarriageDate.Text = "";
				}
			}
			else
			{
				txtMarriageDate.Text = rsReport.Get_Fields_String("ceremonydate");
			}
			string strTemp;
			strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("groomsmiddlename")));
			if (strTemp != string.Empty)
				strTemp = Strings.Left(strTemp, 1);
			txtName.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("groomsfirstname") + " " + strTemp) + " " + rsReport.Get_Fields_String("groomslastname");
			txtCity.Text = rsReport.Get_Fields_String("citymarried");
			strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("bridesmiddlename")));
			if (strTemp != string.Empty)
				strTemp = Strings.Left(strTemp, 1);
			txtBride.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("bridesfirstname") + " " + strTemp) + " " + rsReport.Get_Fields_String("bridescurrentlastname");
			rsReport.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
