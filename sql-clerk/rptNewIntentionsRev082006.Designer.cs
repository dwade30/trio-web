﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptNewMarriageIntentionsRev082006.
	/// </summary>
	partial class rptNewIntentionsRev082006
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewIntentionsRev082006));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtGroomFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsDesig = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsFathersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMothersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMothersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFirst = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMiddle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesSurName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFathersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesFathersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMothersName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesDOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMothersBirthplace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesLast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMarriageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMarriageEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMarriageNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMarriageEnded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomFormerSpouse = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomCourtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideFormerSpouse = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideCourtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomDomesticPartnerYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomDomesticPartnersNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideDomesticPartnerYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideDomesticPartnerNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtstate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIssuance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMarriage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldGroomDomesticPartnersYearRegistered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBrideDomesticPartnersYearRegistered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMarriageEndedDay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomsMarriageEndedYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMarriageEndedDay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBridesMarriageEndedYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomEndedDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomEndedDivorce = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroomEndedAnnulment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBrideEndedDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBrideEndedDivorce = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBrideEndedAnnulment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDesig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesSurName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersBirthplace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEnded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEnded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomFormerSpouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomCourtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideFormerSpouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideCourtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnerYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssuance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarriage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersYearRegistered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnersYearRegistered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEndedDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEndedYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEndedDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEndedYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedDeath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedDivorce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedAnnulment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedDeath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedDivorce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedAnnulment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroomFirst,
            this.txtGroomStreet,
            this.txtGroomAge,
            this.txtGroomBirthplace,
            this.txtGroomMiddle,
            this.txtGroomLast,
            this.txtGroomsDesig,
            this.txtGroomsState,
            this.txtGroomsCounty,
            this.txtGroomsCity,
            this.txtGroomsDOB,
            this.txtGroomsFathersName,
            this.txtGroomsFathersBirthplace,
            this.txtGroomsMothersName,
            this.txtGroomsMothersBirthplace,
            this.txtBridesFirst,
            this.txtBridesStreet,
            this.txtBridesAge,
            this.txtBridesBirthplace,
            this.txtBridesMiddle,
            this.txtBridesSurName,
            this.txtBridesState,
            this.txtBridesCounty,
            this.txtBridesCity,
            this.txtBridesFathersName,
            this.txtBridesFathersBirthplace,
            this.txtBridesMothersName,
            this.txtBridesDOB,
            this.txtBridesMothersBirthplace,
            this.txtBridesLast,
            this.txtGroomsMarriageNumber,
            this.txtGroomsMarriageEnded,
            this.txtBridesMarriageNumber,
            this.txtBridesMarriageEnded,
            this.fldGroomFormerSpouse,
            this.fldGroomCourtName,
            this.fldBrideFormerSpouse,
            this.fldBrideCourtName,
            this.fldGroomDomesticPartnerYes,
            this.fldGroomDomesticPartnersNo,
            this.fldBrideDomesticPartnerYes,
            this.fldBrideDomesticPartnerNo,
            this.txtstate,
            this.txtIssuance,
            this.txtMarriage,
            this.fldGroomDomesticPartnersYearRegistered,
            this.fldBrideDomesticPartnersYearRegistered,
            this.txtGroomsMarriageEndedDay,
            this.txtGroomsMarriageEndedYear,
            this.txtBridesMarriageEndedDay,
            this.txtBridesMarriageEndedYear,
            this.txtGroomEndedDeath,
            this.txtGroomEndedDivorce,
            this.txtGroomEndedAnnulment,
            this.txtBrideEndedDeath,
            this.txtBrideEndedDivorce,
            this.txtBrideEndedAnnulment});
            this.Detail.Height = 9.666667F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtGroomFirst
            // 
            this.txtGroomFirst.CanGrow = false;
            this.txtGroomFirst.Height = 0.1875F;
            this.txtGroomFirst.Left = 0.0625F;
            this.txtGroomFirst.Name = "txtGroomFirst";
            this.txtGroomFirst.Text = null;
            this.txtGroomFirst.Top = 0.7951389F;
            this.txtGroomFirst.Width = 2.1875F;
            // 
            // txtGroomStreet
            // 
            this.txtGroomStreet.CanGrow = false;
            this.txtGroomStreet.Height = 0.19F;
            this.txtGroomStreet.Left = 0.0625F;
            this.txtGroomStreet.Name = "txtGroomStreet";
            this.txtGroomStreet.Text = null;
            this.txtGroomStreet.Top = 1.475694F;
            this.txtGroomStreet.Width = 3.6875F;
            // 
            // txtGroomAge
            // 
            this.txtGroomAge.CanGrow = false;
            this.txtGroomAge.Height = 0.1875F;
            this.txtGroomAge.Left = 0.0625F;
            this.txtGroomAge.Name = "txtGroomAge";
            this.txtGroomAge.Text = null;
            this.txtGroomAge.Top = 1.135417F;
            this.txtGroomAge.Width = 0.8125F;
            // 
            // txtGroomBirthplace
            // 
            this.txtGroomBirthplace.CanGrow = false;
            this.txtGroomBirthplace.Height = 0.19F;
            this.txtGroomBirthplace.Left = 3.9375F;
            this.txtGroomBirthplace.Name = "txtGroomBirthplace";
            this.txtGroomBirthplace.Text = null;
            this.txtGroomBirthplace.Top = 1.479167F;
            this.txtGroomBirthplace.Width = 2.125F;
            // 
            // txtGroomMiddle
            // 
            this.txtGroomMiddle.CanGrow = false;
            this.txtGroomMiddle.Height = 0.1875F;
            this.txtGroomMiddle.Left = 2.3125F;
            this.txtGroomMiddle.Name = "txtGroomMiddle";
            this.txtGroomMiddle.Text = null;
            this.txtGroomMiddle.Top = 0.7916667F;
            this.txtGroomMiddle.Width = 1.5625F;
            // 
            // txtGroomLast
            // 
            this.txtGroomLast.CanGrow = false;
            this.txtGroomLast.Height = 0.1875F;
            this.txtGroomLast.Left = 4F;
            this.txtGroomLast.Name = "txtGroomLast";
            this.txtGroomLast.Text = null;
            this.txtGroomLast.Top = 0.7916667F;
            this.txtGroomLast.Width = 2.5625F;
            // 
            // txtGroomsDesig
            // 
            this.txtGroomsDesig.CanGrow = false;
            this.txtGroomsDesig.Height = 0.1875F;
            this.txtGroomsDesig.Left = 6.625F;
            this.txtGroomsDesig.Name = "txtGroomsDesig";
            this.txtGroomsDesig.Text = null;
            this.txtGroomsDesig.Top = 0.7916667F;
            this.txtGroomsDesig.Width = 0.4375F;
            // 
            // txtGroomsState
            // 
            this.txtGroomsState.CanGrow = false;
            this.txtGroomsState.Height = 0.1875F;
            this.txtGroomsState.Left = 1F;
            this.txtGroomsState.Name = "txtGroomsState";
            this.txtGroomsState.Text = null;
            this.txtGroomsState.Top = 1.135417F;
            this.txtGroomsState.Width = 0.8125F;
            // 
            // txtGroomsCounty
            // 
            this.txtGroomsCounty.CanGrow = false;
            this.txtGroomsCounty.Height = 0.1875F;
            this.txtGroomsCounty.Left = 1.875F;
            this.txtGroomsCounty.Name = "txtGroomsCounty";
            this.txtGroomsCounty.Text = null;
            this.txtGroomsCounty.Top = 1.135417F;
            this.txtGroomsCounty.Width = 1.375F;
            // 
            // txtGroomsCity
            // 
            this.txtGroomsCity.CanGrow = false;
            this.txtGroomsCity.Height = 0.1875F;
            this.txtGroomsCity.Left = 3.9375F;
            this.txtGroomsCity.Name = "txtGroomsCity";
            this.txtGroomsCity.Text = null;
            this.txtGroomsCity.Top = 1.135417F;
            this.txtGroomsCity.Width = 3.125F;
            // 
            // txtGroomsDOB
            // 
            this.txtGroomsDOB.CanGrow = false;
            this.txtGroomsDOB.Height = 0.19F;
            this.txtGroomsDOB.Left = 6.125F;
            this.txtGroomsDOB.Name = "txtGroomsDOB";
            this.txtGroomsDOB.Text = null;
            this.txtGroomsDOB.Top = 1.479167F;
            this.txtGroomsDOB.Width = 1.333333F;
            // 
            // txtGroomsFathersName
            // 
            this.txtGroomsFathersName.CanGrow = false;
            this.txtGroomsFathersName.Height = 0.1875F;
            this.txtGroomsFathersName.Left = 0.0625F;
            this.txtGroomsFathersName.Name = "txtGroomsFathersName";
            this.txtGroomsFathersName.Text = null;
            this.txtGroomsFathersName.Top = 1.857639F;
            this.txtGroomsFathersName.Width = 2.125F;
            // 
            // txtGroomsFathersBirthplace
            // 
            this.txtGroomsFathersBirthplace.CanGrow = false;
            this.txtGroomsFathersBirthplace.Height = 0.1875F;
            this.txtGroomsFathersBirthplace.Left = 2.625F;
            this.txtGroomsFathersBirthplace.Name = "txtGroomsFathersBirthplace";
            this.txtGroomsFathersBirthplace.Text = null;
            this.txtGroomsFathersBirthplace.Top = 1.854167F;
            this.txtGroomsFathersBirthplace.Width = 1.052083F;
            // 
            // txtGroomsMothersName
            // 
            this.txtGroomsMothersName.CanGrow = false;
            this.txtGroomsMothersName.Height = 0.1875F;
            this.txtGroomsMothersName.Left = 3.9375F;
            this.txtGroomsMothersName.Name = "txtGroomsMothersName";
            this.txtGroomsMothersName.Text = null;
            this.txtGroomsMothersName.Top = 1.854167F;
            this.txtGroomsMothersName.Width = 2.03125F;
            // 
            // txtGroomsMothersBirthplace
            // 
            this.txtGroomsMothersBirthplace.CanGrow = false;
            this.txtGroomsMothersBirthplace.Height = 0.1875F;
            this.txtGroomsMothersBirthplace.Left = 6.53125F;
            this.txtGroomsMothersBirthplace.Name = "txtGroomsMothersBirthplace";
            this.txtGroomsMothersBirthplace.Text = null;
            this.txtGroomsMothersBirthplace.Top = 1.854167F;
            this.txtGroomsMothersBirthplace.Width = 0.9375F;
            // 
            // txtBridesFirst
            // 
            this.txtBridesFirst.CanGrow = false;
            this.txtBridesFirst.Height = 0.19F;
            this.txtBridesFirst.Left = 0.0625F;
            this.txtBridesFirst.Name = "txtBridesFirst";
            this.txtBridesFirst.Text = null;
            this.txtBridesFirst.Top = 2.451389F;
            this.txtBridesFirst.Width = 2.125F;
            // 
            // txtBridesStreet
            // 
            this.txtBridesStreet.CanGrow = false;
            this.txtBridesStreet.Height = 0.1875F;
            this.txtBridesStreet.Left = 0.0625F;
            this.txtBridesStreet.Name = "txtBridesStreet";
            this.txtBridesStreet.Text = null;
            this.txtBridesStreet.Top = 3.170139F;
            this.txtBridesStreet.Width = 3.125F;
            // 
            // txtBridesAge
            // 
            this.txtBridesAge.CanGrow = false;
            this.txtBridesAge.Height = 0.19F;
            this.txtBridesAge.Left = 0.0625F;
            this.txtBridesAge.Name = "txtBridesAge";
            this.txtBridesAge.Text = null;
            this.txtBridesAge.Top = 2.826389F;
            this.txtBridesAge.Width = 0.625F;
            // 
            // txtBridesBirthplace
            // 
            this.txtBridesBirthplace.CanGrow = false;
            this.txtBridesBirthplace.Height = 0.1875F;
            this.txtBridesBirthplace.Left = 3.9375F;
            this.txtBridesBirthplace.Name = "txtBridesBirthplace";
            this.txtBridesBirthplace.Text = null;
            this.txtBridesBirthplace.Top = 3.166667F;
            this.txtBridesBirthplace.Width = 2.0625F;
            // 
            // txtBridesMiddle
            // 
            this.txtBridesMiddle.CanGrow = false;
            this.txtBridesMiddle.Height = 0.19F;
            this.txtBridesMiddle.Left = 2.3125F;
            this.txtBridesMiddle.Name = "txtBridesMiddle";
            this.txtBridesMiddle.Text = null;
            this.txtBridesMiddle.Top = 2.447917F;
            this.txtBridesMiddle.Width = 1.5625F;
            // 
            // txtBridesSurName
            // 
            this.txtBridesSurName.CanGrow = false;
            this.txtBridesSurName.Height = 0.19F;
            this.txtBridesSurName.Left = 4F;
            this.txtBridesSurName.Name = "txtBridesSurName";
            this.txtBridesSurName.Text = null;
            this.txtBridesSurName.Top = 2.447917F;
            this.txtBridesSurName.Width = 1.75F;
            // 
            // txtBridesState
            // 
            this.txtBridesState.CanGrow = false;
            this.txtBridesState.Height = 0.19F;
            this.txtBridesState.Left = 1F;
            this.txtBridesState.Name = "txtBridesState";
            this.txtBridesState.Text = null;
            this.txtBridesState.Top = 2.822917F;
            this.txtBridesState.Width = 0.8125F;
            // 
            // txtBridesCounty
            // 
            this.txtBridesCounty.CanGrow = false;
            this.txtBridesCounty.Height = 0.19F;
            this.txtBridesCounty.Left = 1.875F;
            this.txtBridesCounty.Name = "txtBridesCounty";
            this.txtBridesCounty.Text = null;
            this.txtBridesCounty.Top = 2.822917F;
            this.txtBridesCounty.Width = 1.9375F;
            // 
            // txtBridesCity
            // 
            this.txtBridesCity.CanGrow = false;
            this.txtBridesCity.Height = 0.19F;
            this.txtBridesCity.Left = 3.9375F;
            this.txtBridesCity.Name = "txtBridesCity";
            this.txtBridesCity.Text = null;
            this.txtBridesCity.Top = 2.822917F;
            this.txtBridesCity.Width = 3.125F;
            // 
            // txtBridesFathersName
            // 
            this.txtBridesFathersName.CanGrow = false;
            this.txtBridesFathersName.Height = 0.1875F;
            this.txtBridesFathersName.Left = 0.0625F;
            this.txtBridesFathersName.Name = "txtBridesFathersName";
            this.txtBridesFathersName.Text = null;
            this.txtBridesFathersName.Top = 3.545139F;
            this.txtBridesFathersName.Width = 2.125F;
            // 
            // txtBridesFathersBirthplace
            // 
            this.txtBridesFathersBirthplace.CanGrow = false;
            this.txtBridesFathersBirthplace.Height = 0.1875F;
            this.txtBridesFathersBirthplace.Left = 2.625F;
            this.txtBridesFathersBirthplace.Name = "txtBridesFathersBirthplace";
            this.txtBridesFathersBirthplace.Text = null;
            this.txtBridesFathersBirthplace.Top = 3.541667F;
            this.txtBridesFathersBirthplace.Width = 1.052083F;
            // 
            // txtBridesMothersName
            // 
            this.txtBridesMothersName.CanGrow = false;
            this.txtBridesMothersName.Height = 0.1875F;
            this.txtBridesMothersName.Left = 3.9375F;
            this.txtBridesMothersName.Name = "txtBridesMothersName";
            this.txtBridesMothersName.Text = null;
            this.txtBridesMothersName.Top = 3.541667F;
            this.txtBridesMothersName.Width = 2.0625F;
            // 
            // txtBridesDOB
            // 
            this.txtBridesDOB.CanGrow = false;
            this.txtBridesDOB.Height = 0.1875F;
            this.txtBridesDOB.Left = 6.125F;
            this.txtBridesDOB.Name = "txtBridesDOB";
            this.txtBridesDOB.Text = null;
            this.txtBridesDOB.Top = 3.166667F;
            this.txtBridesDOB.Width = 1.333333F;
            // 
            // txtBridesMothersBirthplace
            // 
            this.txtBridesMothersBirthplace.CanGrow = false;
            this.txtBridesMothersBirthplace.Height = 0.1875F;
            this.txtBridesMothersBirthplace.Left = 6.53125F;
            this.txtBridesMothersBirthplace.Name = "txtBridesMothersBirthplace";
            this.txtBridesMothersBirthplace.Text = null;
            this.txtBridesMothersBirthplace.Top = 3.541667F;
            this.txtBridesMothersBirthplace.Width = 0.9375F;
            // 
            // txtBridesLast
            // 
            this.txtBridesLast.CanGrow = false;
            this.txtBridesLast.Height = 0.19F;
            this.txtBridesLast.Left = 5.8125F;
            this.txtBridesLast.Name = "txtBridesLast";
            this.txtBridesLast.Text = null;
            this.txtBridesLast.Top = 2.447917F;
            this.txtBridesLast.Width = 1.25F;
            // 
            // txtGroomsMarriageNumber
            // 
            this.txtGroomsMarriageNumber.CanGrow = false;
            this.txtGroomsMarriageNumber.Height = 0.19F;
            this.txtGroomsMarriageNumber.Left = 0.0625F;
            this.txtGroomsMarriageNumber.Name = "txtGroomsMarriageNumber";
            this.txtGroomsMarriageNumber.Text = null;
            this.txtGroomsMarriageNumber.Top = 4.666667F;
            this.txtGroomsMarriageNumber.Width = 0.625F;
            // 
            // txtGroomsMarriageEnded
            // 
            this.txtGroomsMarriageEnded.CanGrow = false;
            this.txtGroomsMarriageEnded.Height = 0.19F;
            this.txtGroomsMarriageEnded.Left = 1.90625F;
            this.txtGroomsMarriageEnded.Name = "txtGroomsMarriageEnded";
            this.txtGroomsMarriageEnded.Text = null;
            this.txtGroomsMarriageEnded.Top = 4.604167F;
            this.txtGroomsMarriageEnded.Width = 0.3958333F;
            // 
            // txtBridesMarriageNumber
            // 
            this.txtBridesMarriageNumber.CanGrow = false;
            this.txtBridesMarriageNumber.Height = 0.19F;
            this.txtBridesMarriageNumber.Left = 3.75F;
            this.txtBridesMarriageNumber.Name = "txtBridesMarriageNumber";
            this.txtBridesMarriageNumber.Text = null;
            this.txtBridesMarriageNumber.Top = 4.666667F;
            this.txtBridesMarriageNumber.Width = 0.625F;
            // 
            // txtBridesMarriageEnded
            // 
            this.txtBridesMarriageEnded.CanGrow = false;
            this.txtBridesMarriageEnded.Height = 0.19F;
            this.txtBridesMarriageEnded.Left = 5.8125F;
            this.txtBridesMarriageEnded.Name = "txtBridesMarriageEnded";
            this.txtBridesMarriageEnded.Text = null;
            this.txtBridesMarriageEnded.Top = 4.604167F;
            this.txtBridesMarriageEnded.Width = 0.3541667F;
            // 
            // fldGroomFormerSpouse
            // 
            this.fldGroomFormerSpouse.CanGrow = false;
            this.fldGroomFormerSpouse.Height = 0.1875F;
            this.fldGroomFormerSpouse.Left = 1.75F;
            this.fldGroomFormerSpouse.Name = "fldGroomFormerSpouse";
            this.fldGroomFormerSpouse.Text = null;
            this.fldGroomFormerSpouse.Top = 4.822917F;
            this.fldGroomFormerSpouse.Width = 1.875F;
            // 
            // fldGroomCourtName
            // 
            this.fldGroomCourtName.CanGrow = false;
            this.fldGroomCourtName.Height = 0.1875F;
            this.fldGroomCourtName.Left = 0F;
            this.fldGroomCourtName.Name = "fldGroomCourtName";
            this.fldGroomCourtName.Text = null;
            this.fldGroomCourtName.Top = 5.416667F;
            this.fldGroomCourtName.Width = 2.65625F;
            // 
            // fldBrideFormerSpouse
            // 
            this.fldBrideFormerSpouse.CanGrow = false;
            this.fldBrideFormerSpouse.Height = 0.1875F;
            this.fldBrideFormerSpouse.Left = 5.59375F;
            this.fldBrideFormerSpouse.Name = "fldBrideFormerSpouse";
            this.fldBrideFormerSpouse.Text = null;
            this.fldBrideFormerSpouse.Top = 4.8125F;
            this.fldBrideFormerSpouse.Width = 1.875F;
            // 
            // fldBrideCourtName
            // 
            this.fldBrideCourtName.CanGrow = false;
            this.fldBrideCourtName.Height = 0.1875F;
            this.fldBrideCourtName.Left = 3.75F;
            this.fldBrideCourtName.Name = "fldBrideCourtName";
            this.fldBrideCourtName.Text = null;
            this.fldBrideCourtName.Top = 5.416667F;
            this.fldBrideCourtName.Width = 2.65625F;
            // 
            // fldGroomDomesticPartnerYes
            // 
            this.fldGroomDomesticPartnerYes.Height = 0.21875F;
            this.fldGroomDomesticPartnerYes.Left = 0.625F;
            this.fldGroomDomesticPartnerYes.Name = "fldGroomDomesticPartnerYes";
            this.fldGroomDomesticPartnerYes.Style = "text-align: center";
            this.fldGroomDomesticPartnerYes.Text = "X";
            this.fldGroomDomesticPartnerYes.Top = 5.100695F;
            this.fldGroomDomesticPartnerYes.Width = 0.1875F;
            // 
            // fldGroomDomesticPartnersNo
            // 
            this.fldGroomDomesticPartnersNo.Height = 0.21875F;
            this.fldGroomDomesticPartnersNo.Left = 1.125F;
            this.fldGroomDomesticPartnersNo.Name = "fldGroomDomesticPartnersNo";
            this.fldGroomDomesticPartnersNo.Style = "text-align: right";
            this.fldGroomDomesticPartnersNo.Text = "X";
            this.fldGroomDomesticPartnersNo.Top = 5.104167F;
            this.fldGroomDomesticPartnersNo.Width = 0.1875F;
            // 
            // fldBrideDomesticPartnerYes
            // 
            this.fldBrideDomesticPartnerYes.Height = 0.21875F;
            this.fldBrideDomesticPartnerYes.Left = 4.65625F;
            this.fldBrideDomesticPartnerYes.Name = "fldBrideDomesticPartnerYes";
            this.fldBrideDomesticPartnerYes.Style = "text-align: right";
            this.fldBrideDomesticPartnerYes.Text = "X";
            this.fldBrideDomesticPartnerYes.Top = 5.104167F;
            this.fldBrideDomesticPartnerYes.Width = 0.1875F;
            // 
            // fldBrideDomesticPartnerNo
            // 
            this.fldBrideDomesticPartnerNo.Height = 0.21875F;
            this.fldBrideDomesticPartnerNo.Left = 5.166667F;
            this.fldBrideDomesticPartnerNo.Name = "fldBrideDomesticPartnerNo";
            this.fldBrideDomesticPartnerNo.Style = "text-align: center";
            this.fldBrideDomesticPartnerNo.Text = "X";
            this.fldBrideDomesticPartnerNo.Top = 5.104167F;
            this.fldBrideDomesticPartnerNo.Width = 0.1875F;
            // 
            // txtstate
            // 
            this.txtstate.Height = 0.2083333F;
            this.txtstate.Left = 0.25F;
            this.txtstate.Name = "txtstate";
            this.txtstate.Text = "X";
            this.txtstate.Top = 0.2916667F;
            this.txtstate.Visible = false;
            this.txtstate.Width = 0.1875F;
            // 
            // txtIssuance
            // 
            this.txtIssuance.Height = 0.2083333F;
            this.txtIssuance.Left = 0.9375F;
            this.txtIssuance.Name = "txtIssuance";
            this.txtIssuance.Text = "X";
            this.txtIssuance.Top = 0.4166667F;
            this.txtIssuance.Visible = false;
            this.txtIssuance.Width = 0.1875F;
            // 
            // txtMarriage
            // 
            this.txtMarriage.Height = 0.2083333F;
            this.txtMarriage.Left = 1.0625F;
            this.txtMarriage.Name = "txtMarriage";
            this.txtMarriage.Text = "X";
            this.txtMarriage.Top = 0.5416667F;
            this.txtMarriage.Visible = false;
            this.txtMarriage.Width = 0.1875F;
            // 
            // fldGroomDomesticPartnersYearRegistered
            // 
            this.fldGroomDomesticPartnersYearRegistered.CanGrow = false;
            this.fldGroomDomesticPartnersYearRegistered.Height = 0.19F;
            this.fldGroomDomesticPartnersYearRegistered.Left = 2.90625F;
            this.fldGroomDomesticPartnersYearRegistered.Name = "fldGroomDomesticPartnersYearRegistered";
            this.fldGroomDomesticPartnersYearRegistered.Text = null;
            this.fldGroomDomesticPartnersYearRegistered.Top = 5.135417F;
            this.fldGroomDomesticPartnersYearRegistered.Width = 0.59375F;
            // 
            // fldBrideDomesticPartnersYearRegistered
            // 
            this.fldBrideDomesticPartnersYearRegistered.CanGrow = false;
            this.fldBrideDomesticPartnersYearRegistered.Height = 0.19F;
            this.fldBrideDomesticPartnersYearRegistered.Left = 6.90625F;
            this.fldBrideDomesticPartnersYearRegistered.Name = "fldBrideDomesticPartnersYearRegistered";
            this.fldBrideDomesticPartnersYearRegistered.Text = null;
            this.fldBrideDomesticPartnersYearRegistered.Top = 5.135417F;
            this.fldBrideDomesticPartnersYearRegistered.Width = 0.53125F;
            // 
            // txtGroomsMarriageEndedDay
            // 
            this.txtGroomsMarriageEndedDay.CanGrow = false;
            this.txtGroomsMarriageEndedDay.Height = 0.19F;
            this.txtGroomsMarriageEndedDay.Left = 2.375F;
            this.txtGroomsMarriageEndedDay.Name = "txtGroomsMarriageEndedDay";
            this.txtGroomsMarriageEndedDay.Text = null;
            this.txtGroomsMarriageEndedDay.Top = 4.604167F;
            this.txtGroomsMarriageEndedDay.Width = 0.3958333F;
            // 
            // txtGroomsMarriageEndedYear
            // 
            this.txtGroomsMarriageEndedYear.CanGrow = false;
            this.txtGroomsMarriageEndedYear.Height = 0.19F;
            this.txtGroomsMarriageEndedYear.Left = 2.8125F;
            this.txtGroomsMarriageEndedYear.Name = "txtGroomsMarriageEndedYear";
            this.txtGroomsMarriageEndedYear.Text = null;
            this.txtGroomsMarriageEndedYear.Top = 4.604167F;
            this.txtGroomsMarriageEndedYear.Width = 0.3958333F;
            // 
            // txtBridesMarriageEndedDay
            // 
            this.txtBridesMarriageEndedDay.CanGrow = false;
            this.txtBridesMarriageEndedDay.Height = 0.19F;
            this.txtBridesMarriageEndedDay.Left = 6.25F;
            this.txtBridesMarriageEndedDay.Name = "txtBridesMarriageEndedDay";
            this.txtBridesMarriageEndedDay.Text = null;
            this.txtBridesMarriageEndedDay.Top = 4.604167F;
            this.txtBridesMarriageEndedDay.Width = 0.3958333F;
            // 
            // txtBridesMarriageEndedYear
            // 
            this.txtBridesMarriageEndedYear.CanGrow = false;
            this.txtBridesMarriageEndedYear.Height = 0.19F;
            this.txtBridesMarriageEndedYear.Left = 6.6875F;
            this.txtBridesMarriageEndedYear.Name = "txtBridesMarriageEndedYear";
            this.txtBridesMarriageEndedYear.Text = null;
            this.txtBridesMarriageEndedYear.Top = 4.600695F;
            this.txtBridesMarriageEndedYear.Width = 0.3958333F;
            // 
            // txtGroomEndedDeath
            // 
            this.txtGroomEndedDeath.Height = 0.21875F;
            this.txtGroomEndedDeath.Left = 0.875F;
            this.txtGroomEndedDeath.Name = "txtGroomEndedDeath";
            this.txtGroomEndedDeath.Text = "X";
            this.txtGroomEndedDeath.Top = 4.385417F;
            this.txtGroomEndedDeath.Visible = false;
            this.txtGroomEndedDeath.Width = 0.1875F;
            // 
            // txtGroomEndedDivorce
            // 
            this.txtGroomEndedDivorce.Height = 0.21875F;
            this.txtGroomEndedDivorce.Left = 1.75F;
            this.txtGroomEndedDivorce.Name = "txtGroomEndedDivorce";
            this.txtGroomEndedDivorce.Style = "text-align: center";
            this.txtGroomEndedDivorce.Text = "X";
            this.txtGroomEndedDivorce.Top = 4.385417F;
            this.txtGroomEndedDivorce.Visible = false;
            this.txtGroomEndedDivorce.Width = 0.1875F;
            // 
            // txtGroomEndedAnnulment
            // 
            this.txtGroomEndedAnnulment.Height = 0.21875F;
            this.txtGroomEndedAnnulment.Left = 2.625F;
            this.txtGroomEndedAnnulment.Name = "txtGroomEndedAnnulment";
            this.txtGroomEndedAnnulment.Style = "text-align: right";
            this.txtGroomEndedAnnulment.Text = "X";
            this.txtGroomEndedAnnulment.Top = 4.385417F;
            this.txtGroomEndedAnnulment.Visible = false;
            this.txtGroomEndedAnnulment.Width = 0.1875F;
            // 
            // txtBrideEndedDeath
            // 
            this.txtBrideEndedDeath.Height = 0.21875F;
            this.txtBrideEndedDeath.Left = 4.6875F;
            this.txtBrideEndedDeath.Name = "txtBrideEndedDeath";
            this.txtBrideEndedDeath.Text = "X";
            this.txtBrideEndedDeath.Top = 4.385417F;
            this.txtBrideEndedDeath.Visible = false;
            this.txtBrideEndedDeath.Width = 0.1875F;
            // 
            // txtBrideEndedDivorce
            // 
            this.txtBrideEndedDivorce.Height = 0.21875F;
            this.txtBrideEndedDivorce.Left = 5.5625F;
            this.txtBrideEndedDivorce.Name = "txtBrideEndedDivorce";
            this.txtBrideEndedDivorce.Style = "text-align: center";
            this.txtBrideEndedDivorce.Text = "X";
            this.txtBrideEndedDivorce.Top = 4.385417F;
            this.txtBrideEndedDivorce.Visible = false;
            this.txtBrideEndedDivorce.Width = 0.1875F;
            // 
            // txtBrideEndedAnnulment
            // 
            this.txtBrideEndedAnnulment.Height = 0.21875F;
            this.txtBrideEndedAnnulment.Left = 6.4375F;
            this.txtBrideEndedAnnulment.Name = "txtBrideEndedAnnulment";
            this.txtBrideEndedAnnulment.Style = "text-align: center";
            this.txtBrideEndedAnnulment.Text = "X";
            this.txtBrideEndedAnnulment.Top = 4.385417F;
            this.txtBrideEndedAnnulment.Visible = false;
            this.txtBrideEndedAnnulment.Width = 0.1875F;
            // 
            // rptNewIntentionsRev082006
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.3472222F;
            this.PageSettings.Margins.Left = 0.625F;
            this.PageSettings.Margins.Right = 0.375F;
            this.PageSettings.Margins.Top = 0.4166667F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.499306F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_Terminate);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_Initialize);
            this.PrintProgress += new System.EventHandler(this.ActiveReport_PrintProgress);
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDesig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsFathersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMothersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesSurName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesFathersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMothersBirthplace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEnded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEnded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomFormerSpouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomCourtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideFormerSpouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideCourtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnerYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnerNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssuance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarriage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldGroomDomesticPartnersYearRegistered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBrideDomesticPartnersYearRegistered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEndedDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomsMarriageEndedYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEndedDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBridesMarriageEndedYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedDeath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedDivorce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroomEndedAnnulment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedDeath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedDivorce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBrideEndedAnnulment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsDesig;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsFathersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMothersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMothersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFirst;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMiddle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesSurName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFathersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesFathersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMothersName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesDOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMothersBirthplace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesLast;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageEnded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomFormerSpouse;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomCourtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideFormerSpouse;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideCourtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomDomesticPartnerYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomDomesticPartnersNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideDomesticPartnerYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideDomesticPartnerNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtstate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssuance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarriage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroomDomesticPartnersYearRegistered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBrideDomesticPartnersYearRegistered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageEndedDay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomsMarriageEndedYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageEndedDay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBridesMarriageEndedYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomEndedDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomEndedDivorce;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroomEndedAnnulment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideEndedDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideEndedDivorce;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBrideEndedAnnulment;
	}
}
