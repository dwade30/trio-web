﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptSafetyPaper.
	/// </summary>
	public partial class rptSafetyPaper : BaseSectionReport
	{
		public rptSafetyPaper()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Safety Paper";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSafetyPaper InstancePtr
		{
			get
			{
				return (rptSafetyPaper)Sys.GetInstance(typeof(rptSafetyPaper));
			}
		}

		protected rptSafetyPaper _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSafetyPaper	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public void SetPic(ref Image thepic)
		{
			Image1.Image = thepic;
		}

		public void init(ref Image thepic, bool modalDialog)
		{
			Image1.Image = thepic;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		
	}
}
