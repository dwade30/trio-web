//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptLabelDeaths.
	/// </summary>
	public partial class rptLabelDeaths : BaseSectionReport
	{
		public rptLabelDeaths()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Death Certificate ";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLabelDeaths InstancePtr
		{
			get
			{
				return (rptLabelDeaths)Sys.GetInstance(typeof(rptLabelDeaths));
			}
		}

		protected rptLabelDeaths _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLabelDeaths	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private bool boolPrintTest;
		private double dblLineAdjust;

		public void Init(bool boolTestPrint, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			dblLineAdjust = dblAdjust;
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted = this.Printer.DeviceCopies;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			if (modClerkGeneral.Statics.utPrintInfo.DontShowBottomHeaders)
			{
				lblAttestedBy.Visible = false;
				lblCityOrTown.Visible = false;
				lblDateOfFiling.Visible = false;
			}
			// Dim rsData As New clsDRWrapper
			double dblLaserLineAdjustment3 = 0;
			int X;
			if (!boolPrintTest)
			{
				// Call rsData.OpenRecordset("Select * from PrinterSettings", DEFAULTCLERKDATABASE)
				// If rsData.EndOfFile Then
				// dblLaserLineAdjustment3 = 0
				// Else
				// dblLaserLineAdjustment3 = CDbl(Val(rsData.Fields("DeathAdjustment")))
				// End If
				dblLaserLineAdjustment3 = Conversion.Val(modRegistry.GetRegistryKey("DeathAdjustment", "CK"));
			}
			else
			{
				dblLaserLineAdjustment3 = dblLineAdjust;
			}
			for (X = 0; X <= this.Detail.Controls.Count - 1; X++)
			{
				if (Strings.Left(fecherFoundation.Strings.UCase(this.Detail.Controls[X].Name), 4) != "LINE")
				{
					this.Detail.Controls[X].Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment3) / 1440F;
				}
				else
				{
					(this.Detail.Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 += FCConvert.ToSingle(240 * dblLaserLineAdjustment3) / 1440F;
					(this.Detail.Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Line).Y2 += FCConvert.ToSingle(240 * dblLaserLineAdjustment3) / 1440F;
				}
				// ControlName.Top = ControlName.Top + (200 * dblLaserLineAdjustment3)
			}
			// X
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			int lngNumCopies;
			double dblTemp;
			// If gboolFromDosTrio = True Or bolFromWindowsCR Then
			if (boolPrinted)
			{
				// .Type = "DEA"
				// .Name = Left(frmDeaths.txtLastName.Text & ", " & frmDeaths.txtFirstName.Text, 30)
				// .Reference = frmDeaths.txtFileNumber.Text
				// If IsDate(frmDeaths.mebDateOfDeath.Text) Then
				// .Control1 = Format(frmDeaths.mebDateOfDeath.Text, "mm,dd,yyyy")
				// Else
				// .Control1 = frmDeaths.mebDateOfDeath.Text
				// End If
				// .Control2 = frmDeaths.txtLicenseeNumber.Text
				// Call frmInput.Init(lngNumCopies, "Copies", "Enter number of copies to charge for", 1440, , idtWholeNumber)
				// If frmDeaths.optMilitarySrv(0) Then
				// veterans fee
				// .Amount1 = Format((intPrinted * typClerkFees.AdditionalFee5), "000000.00")
				// Else
				// .Amount1 = Format((typClerkFees.NewDeath + ((intPrinted - 1) * typClerkFees.ReplacementDeath)), "000000.00")
				// End If
				// 
				// .Amount2 = Format(0, "000000.00")
				// .Amount3 = Format(0, "000000.00")
				// .Amount4 = Format(0, "000000.00")
				// .Amount5 = Format(0, "000000.00")
				// .Amount6 = Format(0, "000000.00")
				// .ProcessReceipt = "Y"
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);
			}
			// End If
			// frmPrintVitalRec.Show 
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */
			//clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			frmPrintVitalRec.InstancePtr.Hide();
			if (!boolPrintTest)
			{
				frmDeaths.InstancePtr.Hide();
				Field1.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("LastName") + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("Designation");
				if (Information.IsDate(modGNBas.Statics.rsDeathCertificate.Get_Fields("DateOfDeath")))
				{
					Field2.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_DateTime("DateofDeath").FormatAndPadShortDate();
				}
				else
				{
					Field2.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateOfDeathDescription");
				}
				Field3.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CityorTownofDeath");
				if (Information.IsDate(modGNBas.Statics.rsDeathCertificate.Get_Fields("dateofbirth")))
				{
					Field4.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields("DateofBirth"), "MM/dd/yyyy");
				}
				else
				{
					Field4.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields("dateofbirth") + "";
				}
				if (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("socialsecuritynumber")).Length <= 9)
				{
					Field5.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("SocialSecurityNumber"), "000-00-0000");
				}
				else
				{
					strTemp = FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("Socialsecuritynumber"));
					strTemp = Strings.Mid(strTemp, 1, 3) + "-" + Strings.Mid(strTemp, 4, 2) + "-" + Strings.Mid(strTemp, 6);
					Field5.Text = strTemp;
				}
				Field6.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("sex");
				Field7.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherFirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherMiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherMiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("FatherLastName");
				Field8.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherFirstName") + " " + (FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherMiddleName")) == "N/A" ? "" : modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherMiddleName")) + " " + modGNBas.Statics.rsDeathCertificate.Get_Fields_String("MotherLastName");
				Field9.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("InformantName");
				Field10.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("InformantMailingAddress");
				// state added this to form 1/29/2004
				// Call clsTemp.OpenRecordset("select * from defaulttowns where id = " & Val(rsDeathCertificate.Fields("residencecityortown")), "TWCK0000.vb1")
				// If Not clsTemp.EndOfFile Then
				// txtDecedentCity.Text = clsTemp.Fields("name")
				// Else
				// txtDecedentCity.Text = ""
				// End If
				txtDecedentCity.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("residencecityortown");
				txtDecedentStreet.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ResidenceStreet");
				if (frmPrintVitalRec.InstancePtr.chkPrint.CheckState == CheckState.Checked)
				{
					fCauseA.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseA");
					fCauseB.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseB");
					fCauseC.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseC");
					fCauseD.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("CauseD");
				}
				else
				{
					fCauseA.Text = "XXXXXXXXXXXXXXXXXXXX";
					fCauseB.Text = "XXXXXXXXXXXXXXXXXXXX";
					fCauseC.Text = "XXXXXXXXXXXXXXXXXXXX";
					fCauseD.Text = "XXXXXXXXXXXXXXXXXXXX";
				}
				Field12.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("NameofPhysician");
				Field13.Text = Strings.Format(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateSigned"), "MM/dd/yyyy");
				Field14.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceofDisposition")));
				Field15.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceOfDispositionCity");
				Field16.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("PlaceOfDispositionState");
				Field17.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ClerkofRecordName");
				Field18.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("ClerkofRecordCity");
				Field19.Text = modGNBas.Statics.rsDeathCertificate.Get_Fields_String("DateOriginalFiling");
				Field20.Text = Strings.Format(DateTime.Now, "MMMM dd, yyyy");
				if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
				{
					Field21.Text = "";
				}
				else
				{
					Field21.Text = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				}
				Field22.Text = fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase);
				// changed per request of china Matthew 6/19/2001
				// Field22 = rsDeathCertificate.Fields("CityorTownofDeath")
			}
			else
			{
				Field4.Text = "00/00/0000";
				Field2.Text = "00/00/0000";
				Field6.Text = "Male";
				Field8.Text = "Maiden Name";
				Field1.Text = "Name";
				Field5.Text = "000-00-0000";
			}
		}

		private void rptLabelDeaths_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLabelDeaths properties;
			//rptLabelDeaths.Caption	= "Death Certificate ";
			//rptLabelDeaths.Icon	= "rptLabelDeaths.dsx":0000";
			//rptLabelDeaths.Left	= 0;
			//rptLabelDeaths.Top	= 0;
			//rptLabelDeaths.Width	= 11880;
			//rptLabelDeaths.Height	= 8595;
			//rptLabelDeaths.StartUpPosition	= 3;
			//rptLabelDeaths.SectionData	= "rptLabelDeaths.dsx":058A;
			//End Unmaped Properties
		}
	}
}
