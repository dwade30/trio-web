//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmBurialSearch.
	/// </summary>
	partial class frmBurialSearch
	{
		public fecherFoundation.FCComboBox cmbDateofDeath;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> Text2;
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdSch;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCTextBox Text2_1;
		public fecherFoundation.FCTextBox Text2_0;
		public fecherFoundation.FCButton cmdSch_1;
		public fecherFoundation.FCButton cmdSch_3;
		public fecherFoundation.FCButton cmdSch_0;
		public fecherFoundation.FCButton cmdSch_4;
		public Global.T2KDateBox meBox1_0;
		public Global.T2KDateBox meBox1_1;
		public fecherFoundation.FCButton cmdSch_2;
		public FCGrid Grid;
		public fecherFoundation.FCLabel Label2_4;
		public fecherFoundation.FCLabel Label2_5;
		//public fecherFoundation.FCLine Line2;
		//public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbDateofDeath = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.Text2_1 = new fecherFoundation.FCTextBox();
            this.Text2_0 = new fecherFoundation.FCTextBox();
            this.cmdSch_1 = new fecherFoundation.FCButton();
            this.cmdSch_3 = new fecherFoundation.FCButton();
            this.cmdSch_0 = new fecherFoundation.FCButton();
            this.meBox1_0 = new Global.T2KDateBox();
            this.meBox1_1 = new Global.T2KDateBox();
            this.cmdSch_2 = new fecherFoundation.FCButton();
            this.Grid = new fecherFoundation.FCGrid();
            this.Label2_4 = new fecherFoundation.FCLabel();
            this.Label2_5 = new fecherFoundation.FCLabel();
            this.Label2_1 = new fecherFoundation.FCLabel();
            this.Label2_0 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSch_4 = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meBox1_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meBox1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_4)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSch_4);
            this.BottomPanel.Location = new System.Drawing.Point(0, 643);
            this.BottomPanel.Size = new System.Drawing.Size(708, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(708, 583);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(708, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(238, 30);
            this.HeaderText.Text = "Search Burial Permit";
            // 
            // cmbDateofDeath
            // 
            this.cmbDateofDeath.Items.AddRange(new object[] {
            "Disable",
            "Exact Date of Death or",
            "Between Dates"});
            this.cmbDateofDeath.Location = new System.Drawing.Point(20, 178);
            this.cmbDateofDeath.Name = "cmbDateofDeath";
            this.cmbDateofDeath.Size = new System.Drawing.Size(247, 40);
            this.cmbDateofDeath.TabIndex = 20;
            this.cmbDateofDeath.Text = "Disable";
            this.cmbDateofDeath.SelectedIndexChanged += new System.EventHandler(this.optDateofDeath_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cmdPrint);
            this.Frame1.Controls.Add(this.cmbDateofDeath);
            this.Frame1.Controls.Add(this.Text2_1);
            this.Frame1.Controls.Add(this.Text2_0);
            this.Frame1.Controls.Add(this.cmdSch_1);
            this.Frame1.Controls.Add(this.cmdSch_3);
            this.Frame1.Controls.Add(this.cmdSch_0);
            this.Frame1.Controls.Add(this.meBox1_0);
            this.Frame1.Controls.Add(this.meBox1_1);
            this.Frame1.Controls.Add(this.cmdSch_2);
            this.Frame1.Controls.Add(this.Grid);
            this.Frame1.Controls.Add(this.Label2_4);
            this.Frame1.Controls.Add(this.Label2_5);
            this.Frame1.Controls.Add(this.Label2_1);
            this.Frame1.Controls.Add(this.Label2_0);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(650, 521);
            this.Frame1.TabIndex = 12;
            this.Frame1.Text = "Burial Record Search";
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "actionButton";
            this.cmdPrint.Enabled = false;
            this.cmdPrint.Location = new System.Drawing.Point(457, 450);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(80, 40);
            this.cmdPrint.TabIndex = 19;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // Text2_1
            // 
            this.Text2_1.BackColor = System.Drawing.SystemColors.Window;
            this.Text2_1.Location = new System.Drawing.Point(186, 118);
            this.Text2_1.Name = "Text2_1";
            this.Text2_1.Size = new System.Drawing.Size(444, 40);
            this.Text2_1.TabIndex = 1;
            this.Text2_1.Enter += new System.EventHandler(this.Text2_Enter);
            // 
            // Text2_0
            // 
            this.Text2_0.BackColor = System.Drawing.SystemColors.Window;
            this.Text2_0.Location = new System.Drawing.Point(186, 58);
            this.Text2_0.Name = "Text2_0";
            this.Text2_0.Size = new System.Drawing.Size(444, 40);
            this.Text2_0.TabIndex = 21;
            this.Text2_0.Enter += new System.EventHandler(this.Text2_Enter);
            // 
            // cmdSch_1
            // 
            this.cmdSch_1.AppearanceKey = "actionButton";
            this.cmdSch_1.Location = new System.Drawing.Point(115, 450);
            this.cmdSch_1.Name = "cmdSch_1";
            this.cmdSch_1.Size = new System.Drawing.Size(96, 40);
            this.cmdSch_1.TabIndex = 7;
            this.cmdSch_1.Text = "Search ";
            this.cmdSch_1.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // cmdSch_3
            // 
            this.cmdSch_3.AppearanceKey = "actionButton";
            this.cmdSch_3.Location = new System.Drawing.Point(21, 450);
            this.cmdSch_3.Name = "cmdSch_3";
            this.cmdSch_3.Size = new System.Drawing.Size(84, 40);
            this.cmdSch_3.TabIndex = 10;
            this.cmdSch_3.Text = "Clear";
            this.cmdSch_3.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // cmdSch_0
            // 
            this.cmdSch_0.AppearanceKey = "actionButton";
            this.cmdSch_0.Location = new System.Drawing.Point(371, 450);
            this.cmdSch_0.Name = "cmdSch_0";
            this.cmdSch_0.Size = new System.Drawing.Size(70, 40);
            this.cmdSch_0.TabIndex = 11;
            this.cmdSch_0.Text = "Exit";
            this.cmdSch_0.Visible = false;
            this.cmdSch_0.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // meBox1_0
            // 
            this.meBox1_0.Enabled = false;
            this.meBox1_0.Location = new System.Drawing.Point(286, 178);
            this.meBox1_0.MaxLength = 10;
            this.meBox1_0.Name = "meBox1_0";
            this.meBox1_0.Size = new System.Drawing.Size(144, 22);
            this.meBox1_0.TabIndex = 5;
            // 
            // meBox1_1
            // 
            this.meBox1_1.Enabled = false;
            this.meBox1_1.Location = new System.Drawing.Point(486, 178);
            this.meBox1_1.MaxLength = 10;
            this.meBox1_1.Name = "meBox1_1";
            this.meBox1_1.Size = new System.Drawing.Size(144, 22);
            this.meBox1_1.TabIndex = 6;
            // 
            // cmdSch_2
            // 
            this.cmdSch_2.AppearanceKey = "actionButton";
            this.cmdSch_2.Location = new System.Drawing.Point(221, 450);
            this.cmdSch_2.Name = "cmdSch_2";
            this.cmdSch_2.Size = new System.Drawing.Size(140, 40);
            this.cmdSch_2.TabIndex = 9;
            this.cmdSch_2.Text = "Long Search";
            this.cmdSch_2.Visible = false;
            this.cmdSch_2.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // Grid
            // 
            this.Grid.Cols = 4;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(20, 238);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.Size = new System.Drawing.Size(610, 190);
            this.Grid.TabIndex = 20;
            this.Grid.Click += new System.EventHandler(this.Grid_ClickEvent);
            // 
            // Label2_4
            // 
            this.Label2_4.Location = new System.Drawing.Point(281, 30);
            this.Label2_4.Name = "Label2_4";
            this.Label2_4.Size = new System.Drawing.Size(150, 18);
            this.Label2_4.TabIndex = 18;
            this.Label2_4.Text = "FIRST NAME (OPTIONAL)";
            // 
            // Label2_5
            // 
            this.Label2_5.Location = new System.Drawing.Point(186, 30);
            this.Label2_5.Name = "Label2_5";
            this.Label2_5.Size = new System.Drawing.Size(72, 18);
            this.Label2_5.TabIndex = 17;
            this.Label2_5.Text = "LAST NAME";
            // 
            // Label2_1
            // 
            this.Label2_1.Location = new System.Drawing.Point(20, 132);
            this.Label2_1.Name = "Label2_1";
            this.Label2_1.Size = new System.Drawing.Size(140, 16);
            this.Label2_1.TabIndex = 16;
            this.Label2_1.Text = "PLACE OF DISPOSITION";
            // 
            // Label2_0
            // 
            this.Label2_0.Location = new System.Drawing.Point(20, 72);
            this.Label2_0.Name = "Label2_0";
            this.Label2_0.Size = new System.Drawing.Size(120, 17);
            this.Label2_0.TabIndex = 15;
            this.Label2_0.Text = "DECEASED\'S NAME";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(450, 192);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(29, 16);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "TO";
            // 
            // cmdSch_4
            // 
            this.cmdSch_4.AppearanceKey = "acceptButton";
            this.cmdSch_4.Location = new System.Drawing.Point(273, 30);
            this.cmdSch_4.Name = "cmdSch_4";
            this.cmdSch_4.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSch_4.Size = new System.Drawing.Size(146, 48);
            this.cmdSch_4.TabIndex = 8;
            this.cmdSch_4.Text = "Show Record";
            this.cmdSch_4.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // frmBurialSearch
            // 
            this.AcceptButton = this.cmdSch_4;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(708, 751);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmBurialSearch";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Search Burial Permit";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmBurialSearch_Load);
            this.Activated += new System.EventHandler(this.frmBurialSearch_Activated);
            this.Resize += new System.EventHandler(this.frmBurialSearch_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBurialSearch_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBurialSearch_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meBox1_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meBox1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_4)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
