//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmTagInventory.
	/// </summary>
	partial class frmTagInventory
	{
		public fecherFoundation.FCComboBox cmbToTag;
		public fecherFoundation.FCComboBox cmbFromTag;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbToTag = new fecherFoundation.FCComboBox();
            this.cmbFromTag = new fecherFoundation.FCComboBox();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 214);
            this.BottomPanel.Size = new System.Drawing.Size(338, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbToTag);
            this.ClientArea.Controls.Add(this.cmbFromTag);
            this.ClientArea.Controls.Add(this.cmbYear);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(338, 154);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(338, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(113, 30);
            this.HeaderText.Text = "Inventory";
            // 
            // cmbToTag
            // 
            this.cmbToTag.AutoSize = false;
            this.cmbToTag.BackColor = System.Drawing.SystemColors.Window;
            this.cmbToTag.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbToTag.FormattingEnabled = true;
            this.cmbToTag.Location = new System.Drawing.Point(197, 90);
            this.cmbToTag.Name = "cmbToTag";
            this.cmbToTag.Size = new System.Drawing.Size(111, 40);
            this.cmbToTag.TabIndex = 2;
            // 
            // cmbFromTag
            // 
            this.cmbFromTag.AutoSize = false;
            this.cmbFromTag.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFromTag.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFromTag.FormattingEnabled = true;
            this.cmbFromTag.Location = new System.Drawing.Point(30, 90);
            this.cmbFromTag.Name = "cmbFromTag";
            this.cmbFromTag.Size = new System.Drawing.Size(113, 40);
            this.cmbFromTag.TabIndex = 1;
            // 
            // cmbYear
            // 
            this.cmbYear.AutoSize = false;
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.Location = new System.Drawing.Point(130, 30);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(127, 40);
            this.cmbYear.TabIndex = 0;
            this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(163, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(20, 16);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "TO";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(46, 16);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "YEAR";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(80, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(174, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Save & Continue";
            this.cmdProcess.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmTagInventory
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(338, 322);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmTagInventory";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Inventory";
            this.Load += new System.EventHandler(this.frmTagInventory_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTagInventory_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}