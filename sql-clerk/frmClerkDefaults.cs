﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmClerkDefaults : BaseForm
	{
		public frmClerkDefaults()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmClerkDefaults InstancePtr
		{
			get
			{
				return (frmClerkDefaults)Sys.GetInstance(typeof(frmClerkDefaults));
			}
		}

		protected frmClerkDefaults _InstancePtr = null;
		//=========================================================
		private int intDataChanged;

		private void cmdExit_Click()
		{
			modClerkGeneral.Statics.bolEditDefaults = false;
			frmClerkDefaults.InstancePtr.Close();
		}

		public void cmdSave_Click()
		{
			SaveInfo();
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsD = new clsDRWrapper();
				SaveInfo = false;
				modClerkGeneral.Statics.bolEditDefaults = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				modGNBas.Statics.ClerkDefaults.Address1 = txtAddress1.Text;
				modGNBas.Statics.ClerkDefaults.Address2 = txtAddress2.Text;
				modGNBas.Statics.ClerkDefaults.Phone = txtPhone.Text;
				modGNBas.Statics.ClerkDefaults.AgentNumber = txtAgentNumber.Text;
				modGNBas.Statics.ClerkDefaults.City = txtCity.Text;
				modGNBas.Statics.ClerkDefaults.State = fecherFoundation.Strings.Trim(txtState.Text);
				modGNBas.Statics.ClerkDefaults.Zip = txtZip.Text;
				modGNBas.Statics.ClerkDefaults.County = txtCounty.Text;
				modGNBas.Statics.ClerkDefaults.LegalResidence = txtLegalResidence.Text;
				modGNBas.Statics.ClerkDefaults.BirthDefaultCounty = txtBirthDefaultCounty.Text;
				modGNBas.Statics.ClerkDefaults.BirthDefaultTown = txtBirthDefaultCity.Text;
				modGNBas.Statics.ClerkDefaults.MotherDefaultCity = txtMotherCity.Text;
				modGNBas.Statics.ClerkDefaults.MotherDefaultCounty = txtMotherCounty.Text;
				modGNBas.Statics.ClerkDefaults.MotherDefaultState = txtMotherState.Text;
				if (chkFileNumbers.CheckState == Wisej.Web.CheckState.Checked)
				{
					modGNBas.Statics.ClerkDefaults.boolFileDuplicateWarning = true;
				}
				else
				{
					modGNBas.Statics.ClerkDefaults.boolFileDuplicateWarning = false;
				}
				if (chkDontPrintAttested.CheckState == Wisej.Web.CheckState.Checked)
				{
					modGNBas.Statics.ClerkDefaults.boolDontPrintAttested = true;
				}
				else
				{
					modGNBas.Statics.ClerkDefaults.boolDontPrintAttested = false;
				}
				if (chkDefaultAttestedBy.CheckState == Wisej.Web.CheckState.Checked)
				{
					modGNBas.Statics.ClerkDefaults.DefaultAttestedBy = true;
				}
				else
				{
					modGNBas.Statics.ClerkDefaults.DefaultAttestedBy = false;
				}
				if (cmbDontImportTags.Text == "Import tags in inventory")
				{
					modGNBas.Statics.ClerkDefaults.OnlineDogImportOption = FCConvert.ToInt32(clsCustomize.OnlineDogImportOptionType.OnlineDogImportOptionExisting);
				}
				else if (cmbDontImportTags.Text == "Add missing tags to inventory")
				{
					modGNBas.Statics.ClerkDefaults.OnlineDogImportOption = FCConvert.ToInt32(clsCustomize.OnlineDogImportOptionType.OnlineDogImportOptionAddMissing);
				}
				else
				{
					modGNBas.Statics.ClerkDefaults.OnlineDogImportOption = FCConvert.ToInt32(clsCustomize.OnlineDogImportOptionType.OnlineDogImportOptionNoTags);
				}
				if (modGNBas.Statics.ClerkDefaults.SaveCustomizeInfo())
				{
					MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					modDirtyForm.ClearDirtyControls(this);
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				SaveInfo = true;
                //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                App.MainForm.ReloadNavigationMenu();
                return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void frmClerkDefaults_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
		}

		private void frmClerkDefaults_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				cmdExit_Click();
			// If KeyCode = vbKeyF10 Then
			// Call cmdSave_Click
			// Call cmdExit_Click
			// bolEditDefaults = True
			// End If
			// 
			// If KeyCode = vbKeyF9 Then Call cmdSave_Click
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
		}

		private void frmClerkDefaults_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmClerkDefaults.InstancePtr.Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				// KeyAscii = KeyAscii - 32
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmClerkDefaults_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmClerkDefaults properties;
			//frmClerkDefaults.ScaleWidth	= 9075;
			//frmClerkDefaults.ScaleHeight	= 7365;
			//frmClerkDefaults.LinkTopic	= "Form1";
			//frmClerkDefaults.LockControls	= -1  'True;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			ShowData();
		}

		private void ShowData()
		{
			txtAddress1.Text = modGNBas.Statics.ClerkDefaults.Address1;
			txtAddress2.Text = modGNBas.Statics.ClerkDefaults.Address2;
			txtPhone.Text = modGNBas.Statics.ClerkDefaults.Phone;
			txtAgentNumber.Text = modGNBas.Statics.ClerkDefaults.AgentNumber;
			txtCity.Text = modGNBas.Statics.ClerkDefaults.City;
			txtState.Text = modGNBas.Statics.ClerkDefaults.State;
			txtZip.Text = modGNBas.Statics.ClerkDefaults.Zip;
			txtCounty.Text = modGNBas.Statics.ClerkDefaults.County;
			txtLegalResidence.Text = modGNBas.Statics.ClerkDefaults.LegalResidence;
			txtBirthDefaultCounty.Text = modGNBas.Statics.ClerkDefaults.BirthDefaultCounty;
			txtBirthDefaultCity.Text = modGNBas.Statics.ClerkDefaults.BirthDefaultTown;
			txtMotherCity.Text = modGNBas.Statics.ClerkDefaults.MotherDefaultCity;
			txtMotherCounty.Text = modGNBas.Statics.ClerkDefaults.MotherDefaultCounty;
			txtMotherState.Text = modGNBas.Statics.ClerkDefaults.MotherDefaultState;
			if (modGNBas.Statics.ClerkDefaults.boolFileDuplicateWarning)
			{
				chkFileNumbers.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkFileNumbers.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
			{
				chkDontPrintAttested.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkDontPrintAttested.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGNBas.Statics.ClerkDefaults.DefaultAttestedBy)
			{
				chkDefaultAttestedBy.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkDefaultAttestedBy.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGNBas.Statics.ClerkDefaults.OnlineDogImportOption == FCConvert.ToInt32(clsCustomize.OnlineDogImportOptionType.OnlineDogImportOptionExisting))
			{
				cmbDontImportTags.Text = "Import tags in inventory";
			}
			else if (modGNBas.Statics.ClerkDefaults.OnlineDogImportOption == FCConvert.ToInt32(clsCustomize.OnlineDogImportOptionType.OnlineDogImportOptionAddMissing))
			{
				cmbDontImportTags.Text = "Add missing tags to inventory";
			}
			else
			{
				cmbDontImportTags.Text = "Do not import tags";
			}
			modDirtyForm.ClearDirtyControls(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			intDataChanged = FCConvert.ToInt16(modDirtyForm.NumberDirtyControls(this));
			modDirtyForm.SaveChanges(intDataChanged, this, "cmdSave_Click");
			modGNBas.Statics.ClerkDefaults.LoadCustomizeInfo();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuSave_Click()
		{
			cmdSave_Click();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				cmdExit_Click();
			}
		}

		private void mnuSaves_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void txtAddress1_Enter(object sender, System.EventArgs e)
		{
			txtAddress1.SelectionStart = 0;
			txtAddress1.SelectionLength = 50;
		}

		private void txtAddress2_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 50;
		}

		private void txtAgentNumber_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 50;
		}

		private void txtCity_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 50;
		}

		private void txtCounty_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 50;
		}

		private void txtLegalResidence_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 50;
		}

		private void txtPhone_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 50;
		}

		private void txtZip_Enter(object sender, System.EventArgs e)
		{
			(this.ActiveControl as FCTextBox).SelectionStart = 0;
			(this.ActiveControl as FCTextBox).SelectionLength = 50;
		}
	}
}
