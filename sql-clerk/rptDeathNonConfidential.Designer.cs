﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptDeathNonConfidential.
	/// </summary>
	partial class rptDeathNonConfidential
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDeathNonConfidential));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAttest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownOf = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.txtDecedent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateOfDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocationOfDeath = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtAttest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownOf)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocationOfDeath)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAttest,
				this.txtTownOf,
				this.txtDateIssued,
				this.Image1,
				this.txtDecedent,
				this.txtDateOfDeath,
				this.txtAge,
				this.txtLocationOfDeath
			});
			this.Detail.Height = 6.572917F;
			this.Detail.Name = "Detail";
			// 
			// txtAttest
			// 
			this.txtAttest.CanGrow = false;
			this.txtAttest.Height = 0.4166667F;
			this.txtAttest.Left = 2F;
			this.txtAttest.Name = "txtAttest";
			this.txtAttest.Text = "Attested by";
			this.txtAttest.Top = 5.666667F;
			this.txtAttest.Width = 1.802083F;
			// 
			// txtTownOf
			// 
			this.txtTownOf.CanGrow = false;
			this.txtTownOf.Height = 0.1666667F;
			this.txtTownOf.Left = 2.1F;
			this.txtTownOf.Name = "txtTownOf";
			this.txtTownOf.Text = "Town Of";
			this.txtTownOf.Top = 5.354167F;
			this.txtTownOf.Width = 1.7375F;
			// 
			// txtDateIssued
			// 
			this.txtDateIssued.CanGrow = false;
			this.txtDateIssued.Height = 0.1666667F;
			this.txtDateIssued.Left = 4.8F;
			this.txtDateIssued.Name = "txtDateIssued";
			this.txtDateIssued.Text = "Date Issued";
			this.txtDateIssued.Top = 5.25F;
			this.txtDateIssued.Width = 2.28125F;
			// 
			// Image1
			// 
			this.Image1.Height = 0.4166667F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = null;
			this.Image1.Left = 3.9F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.Top = 5.416667F;
			this.Image1.Width = 3.1875F;
			// 
			// txtDecedent
			// 
			this.txtDecedent.Height = 0.1666667F;
			this.txtDecedent.Left = 1.4F;
			this.txtDecedent.Name = "txtDecedent";
			this.txtDecedent.Text = "Decedent";
			this.txtDecedent.Top = 3F;
			this.txtDecedent.Width = 2.104167F;
			// 
			// txtDateOfDeath
			// 
			this.txtDateOfDeath.Height = 0.1666667F;
			this.txtDateOfDeath.Left = 1.4F;
			this.txtDateOfDeath.Name = "txtDateOfDeath";
			this.txtDateOfDeath.Text = "Date of Death";
			this.txtDateOfDeath.Top = 3.666667F;
			this.txtDateOfDeath.Width = 2.104167F;
			// 
			// txtAge
			// 
			this.txtAge.Height = 0.1666667F;
			this.txtAge.Left = 4.9F;
			this.txtAge.Name = "txtAge";
			this.txtAge.Text = "Decedent Age";
			this.txtAge.Top = 3F;
			this.txtAge.Width = 2.104167F;
			// 
			// txtLocationOfDeath
			// 
			this.txtLocationOfDeath.Height = 0.1666667F;
			this.txtLocationOfDeath.Left = 4.9F;
			this.txtLocationOfDeath.Name = "txtLocationOfDeath";
			this.txtLocationOfDeath.Text = "Location of Death";
			this.txtLocationOfDeath.Top = 3.666667F;
			this.txtLocationOfDeath.Width = 2.104167F;
			// 
			// rptDeathNonConfidential
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtAttest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownOf)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDecedent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOfDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocationOfDeath)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownOf;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateIssued;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDecedent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateOfDeath;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocationOfDeath;
	}
}
