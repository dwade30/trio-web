//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmMarriages.
	/// </summary>
	partial class frmMarriages
	{
		public fecherFoundation.FCCheckBox chkVerificationOfForeignGroom;
		public fecherFoundation.FCCheckBox chkVerificationOfForeignBride;
		public fecherFoundation.FCCheckBox chkDeathOnFile;
		public fecherFoundation.FCCheckBox chkBirthOnFile;
		public fecherFoundation.FCCheckBox chkGroomBirthOnFile;
		public fecherFoundation.FCCheckBox chkGroomDeathOnFile;
		public fecherFoundation.FCCheckBox chkSignitureOnFile;
		public fecherFoundation.FCCheckBox chkVoided;
		public fecherFoundation.FCCheckBox chkConsent;
		public fecherFoundation.FCCheckBox chkRequired;
		public fecherFoundation.FCTextBox txtFileNumber;
		public fecherFoundation.FCTabControl tabMarriages;
		public fecherFoundation.FCTabPage tabMarriages_Page1;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtPartyBDesig;
		public fecherFoundation.FCComboBox cmbPartyBType;
		public fecherFoundation.FCComboBox cmbPartyBGender;
		public fecherFoundation.FCTextBox txtBrideSSN;
		public fecherFoundation.FCTextBox txtBridesDOB;
		public fecherFoundation.FCTextBox txtBridesMothersBirthplace;
		public fecherFoundation.FCTextBox txtBridesFathersBirthplace;
		public fecherFoundation.FCTextBox txtState2;
		public fecherFoundation.FCTextBox txtBrideZip;
		public fecherFoundation.FCTextBox txtBridesMothersName;
		public fecherFoundation.FCTextBox txtBridesFathersName;
		public fecherFoundation.FCTextBox txtBridesAge;
		public fecherFoundation.FCTextBox txtBridesCounty;
		public fecherFoundation.FCTextBox txtBridesState;
		public fecherFoundation.FCTextBox txtBridesCity;
		public fecherFoundation.FCTextBox txtBridesStreetNumber;
		public fecherFoundation.FCTextBox txtBridesStreetAddress;
		public fecherFoundation.FCTextBox txtBridesMaidenSurname;
		public fecherFoundation.FCTextBox txtBridesMiddleName;
		public fecherFoundation.FCTextBox txtBridesFirstName;
		public fecherFoundation.FCTextBox txtBridesCurrentLastName;
		public fecherFoundation.FCData datMarriage;
		public fecherFoundation.FCLabel Label72;
		public fecherFoundation.FCLabel Label74;
		public fecherFoundation.FCLabel lblCopyGroomsInfo;
		public fecherFoundation.FCLabel Label69;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label32;
		public fecherFoundation.FCLabel Label33;
		public fecherFoundation.FCLabel Label34;
		public fecherFoundation.FCLabel Label31;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbPartyAType;
		public fecherFoundation.FCComboBox cmbPartyAGender;
		public fecherFoundation.FCTextBox txtPartyABirthName;
		public fecherFoundation.FCTextBox txtGroomSSN;
		public fecherFoundation.FCTextBox txtGroomsDOB;
		public fecherFoundation.FCTextBox txtGroomsMothersBirthplace;
		public fecherFoundation.FCTextBox txtGroomsFathersBirthplace;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtGroomZip;
		public fecherFoundation.FCTextBox txtGroomsFirstName;
		public fecherFoundation.FCTextBox txtGroomsMiddleName;
		public fecherFoundation.FCTextBox txtGroomsLastName;
		public fecherFoundation.FCTextBox txtGroomsDesignation;
		public fecherFoundation.FCTextBox txtGroomsStreetName;
		public fecherFoundation.FCTextBox txtGroomsStreetNumber;
		public fecherFoundation.FCTextBox txtGroomsCity;
		public fecherFoundation.FCTextBox txtGroomsState;
		public fecherFoundation.FCTextBox txtGroomsCounty;
		public fecherFoundation.FCTextBox txtGroomsAge;
		public fecherFoundation.FCTextBox txtGroomsFathersName;
		public fecherFoundation.FCTextBox txtGroomsMothersName;
		public fecherFoundation.FCLabel Label73;
		public fecherFoundation.FCLabel Label71;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label68;
		public fecherFoundation.FCLabel Label61;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCTabPage tabMarriages_Page2;
		public fecherFoundation.FCTextBox txtDateClerkFiled;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCTextBox txtBrideDomesticPartnerYearRegistered;
		public fecherFoundation.FCTextBox txtGroomDomesticPartnerYearRegistered;
		public fecherFoundation.FCCheckBox chkBrideDomesticPartner;
		public fecherFoundation.FCCheckBox chkGroomDomesticPartner;
		public fecherFoundation.FCTextBox txtBridesMarriageEndedWhen;
		public fecherFoundation.FCTextBox txtGroomsMarriageEndedWhen;
		public fecherFoundation.FCTextBox txtBrideCourtName;
		public fecherFoundation.FCTextBox txtBrideFormerSpouseName;
		public fecherFoundation.FCTextBox txtGroomCourtName;
		public fecherFoundation.FCTextBox txtGroomFormerSpouseName;
		public fecherFoundation.FCComboBox cboGroomMarriageNumber;
		public fecherFoundation.FCComboBox cboBridesMarriageNumber;
		public fecherFoundation.FCComboBox lstBridesMarriageEndedHow;
		public fecherFoundation.FCComboBox lstGroomsMarriageEndedHow;
		public fecherFoundation.FCLabel lblBrideDomesticPartnerYearRegistered;
		public fecherFoundation.FCLabel lblGroomDomesticPartnerYearRegistered;
		public fecherFoundation.FCLabel Label67;
		public fecherFoundation.FCLabel Label66;
		public fecherFoundation.FCLabel Label65;
		public fecherFoundation.FCLabel Label64;
		public fecherFoundation.FCLabel Label58;
		public fecherFoundation.FCLabel Label57;
		public fecherFoundation.FCLabel Label56;
		public fecherFoundation.FCLabel Label55;
		public fecherFoundation.FCLabel Label54;
		public fecherFoundation.FCLabel Label53;
		public fecherFoundation.FCLabel Label52;
		public fecherFoundation.FCLabel Label51;
		public fecherFoundation.FCLabel Label50;
		public fecherFoundation.FCLabel Label49;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtDateLicenseValid;
		public fecherFoundation.FCTextBox txtDateLicenseIssued;
		public fecherFoundation.FCTextBox txtDateIntentionsFiled;
		public fecherFoundation.FCTextBox txtCityofIssue;
		public fecherFoundation.FCLabel Label48;
		public fecherFoundation.FCLabel Label47;
		public fecherFoundation.FCLabel Label46;
		public fecherFoundation.FCLabel Label45;
		public fecherFoundation.FCFrame Frame3;
		public Global.T2KDateBox txtActualDate;
		public fecherFoundation.FCComboBox txtTitleOfPersonPerforming;
		public fecherFoundation.FCTextBox txtDateOfCommision;
		public fecherFoundation.FCTextBox txtCeremonyDate;
		public fecherFoundation.FCTextBox txtCityMarried;
		public fecherFoundation.FCTextBox txtCountyMarried;
		public fecherFoundation.FCTextBox txtResidenceofPersonPerforming;
		public fecherFoundation.FCTextBox txtMailingAddressofPersonPerforming;
		public fecherFoundation.FCTextBox txtPersonPerformingCeremony;
		public fecherFoundation.FCLabel Label70;
		public fecherFoundation.FCLabel Label44;
		public fecherFoundation.FCLabel Label43;
		public fecherFoundation.FCLabel Label42;
		public fecherFoundation.FCLabel Label41;
		public fecherFoundation.FCLabel Label40;
		public fecherFoundation.FCLabel Label39;
		public fecherFoundation.FCLabel Label38;
		public fecherFoundation.FCLabel Label37;
		public fecherFoundation.FCLabel Label36;
		public fecherFoundation.FCLabel Label35;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtNameofWitnessOne;
		public fecherFoundation.FCTextBox txtNameofWitnessTwo;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCButton cmdFind;
		public fecherFoundation.FCTextBox txtNameOfClerk;
		public fecherFoundation.FCLabel Label60;
		public fecherFoundation.FCLabel Label59;
		public fecherFoundation.FCTabPage tabMarriages_Page3;
		public fecherFoundation.FCGrid VSFlexGrid1;
		public Global.T2KDateBox mebVoided;
		public FCGrid gridTownCode;
		public fecherFoundation.FCPictureBox imgDocuments;
		public fecherFoundation.FCPictureBox ImgComment;
		public fecherFoundation.FCLabel Label63;
		public fecherFoundation.FCLabel Label62;
		public fecherFoundation.FCLabel Label30;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuStates;
		public fecherFoundation.FCToolStripMenuItem mnuRegistrarNames;
		public fecherFoundation.FCToolStripMenuItem mnuEditTown;
		public fecherFoundation.FCToolStripMenuItem mnuEditTitles;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocs;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreviewCertificate;
		public fecherFoundation.FCToolStripMenuItem mnuPrintIntentions;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreviewLicense;
		public fecherFoundation.FCToolStripMenuItem Separator1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExitNoValidation;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Separator2;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		public fecherFoundation.FCLabel lblFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMarriages));
            this.chkVerificationOfForeignGroom = new fecherFoundation.FCCheckBox();
            this.chkVerificationOfForeignBride = new fecherFoundation.FCCheckBox();
            this.chkDeathOnFile = new fecherFoundation.FCCheckBox();
            this.chkBirthOnFile = new fecherFoundation.FCCheckBox();
            this.chkGroomBirthOnFile = new fecherFoundation.FCCheckBox();
            this.chkGroomDeathOnFile = new fecherFoundation.FCCheckBox();
            this.chkSignitureOnFile = new fecherFoundation.FCCheckBox();
            this.chkVoided = new fecherFoundation.FCCheckBox();
            this.chkConsent = new fecherFoundation.FCCheckBox();
            this.chkRequired = new fecherFoundation.FCCheckBox();
            this.txtFileNumber = new fecherFoundation.FCTextBox();
            this.tabMarriages = new fecherFoundation.FCTabControl();
            this.tabMarriages_Page1 = new fecherFoundation.FCTabPage();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtPartyBDesig = new fecherFoundation.FCTextBox();
            this.cmbPartyBType = new fecherFoundation.FCComboBox();
            this.cmbPartyBGender = new fecherFoundation.FCComboBox();
            this.txtBrideSSN = new fecherFoundation.FCTextBox();
            this.txtBridesDOB = new fecherFoundation.FCTextBox();
            this.txtBridesMothersBirthplace = new fecherFoundation.FCTextBox();
            this.txtBridesFathersBirthplace = new fecherFoundation.FCTextBox();
            this.txtState2 = new fecherFoundation.FCTextBox();
            this.txtBrideZip = new fecherFoundation.FCTextBox();
            this.txtBridesMothersName = new fecherFoundation.FCTextBox();
            this.txtBridesFathersName = new fecherFoundation.FCTextBox();
            this.txtBridesAge = new fecherFoundation.FCTextBox();
            this.txtBridesCounty = new fecherFoundation.FCTextBox();
            this.txtBridesState = new fecherFoundation.FCTextBox();
            this.txtBridesCity = new fecherFoundation.FCTextBox();
            this.txtBridesStreetNumber = new fecherFoundation.FCTextBox();
            this.txtBridesStreetAddress = new fecherFoundation.FCTextBox();
            this.txtBridesMaidenSurname = new fecherFoundation.FCTextBox();
            this.txtBridesMiddleName = new fecherFoundation.FCTextBox();
            this.txtBridesFirstName = new fecherFoundation.FCTextBox();
            this.txtBridesCurrentLastName = new fecherFoundation.FCTextBox();
            this.datMarriage = new fecherFoundation.FCData();
            this.Label72 = new fecherFoundation.FCLabel();
            this.Label74 = new fecherFoundation.FCLabel();
            this.lblCopyGroomsInfo = new fecherFoundation.FCLabel();
            this.Label69 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label24 = new fecherFoundation.FCLabel();
            this.Label25 = new fecherFoundation.FCLabel();
            this.Label26 = new fecherFoundation.FCLabel();
            this.Label28 = new fecherFoundation.FCLabel();
            this.Label29 = new fecherFoundation.FCLabel();
            this.Label32 = new fecherFoundation.FCLabel();
            this.Label33 = new fecherFoundation.FCLabel();
            this.Label34 = new fecherFoundation.FCLabel();
            this.Label31 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmbPartyAType = new fecherFoundation.FCComboBox();
            this.cmbPartyAGender = new fecherFoundation.FCComboBox();
            this.txtPartyABirthName = new fecherFoundation.FCTextBox();
            this.txtGroomSSN = new fecherFoundation.FCTextBox();
            this.txtGroomsDOB = new fecherFoundation.FCTextBox();
            this.txtGroomsMothersBirthplace = new fecherFoundation.FCTextBox();
            this.txtGroomsFathersBirthplace = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtGroomZip = new fecherFoundation.FCTextBox();
            this.txtGroomsFirstName = new fecherFoundation.FCTextBox();
            this.txtGroomsMiddleName = new fecherFoundation.FCTextBox();
            this.txtGroomsLastName = new fecherFoundation.FCTextBox();
            this.txtGroomsDesignation = new fecherFoundation.FCTextBox();
            this.txtGroomsStreetName = new fecherFoundation.FCTextBox();
            this.txtGroomsStreetNumber = new fecherFoundation.FCTextBox();
            this.txtGroomsCity = new fecherFoundation.FCTextBox();
            this.txtGroomsState = new fecherFoundation.FCTextBox();
            this.txtGroomsCounty = new fecherFoundation.FCTextBox();
            this.txtGroomsAge = new fecherFoundation.FCTextBox();
            this.txtGroomsFathersName = new fecherFoundation.FCTextBox();
            this.txtGroomsMothersName = new fecherFoundation.FCTextBox();
            this.Label73 = new fecherFoundation.FCLabel();
            this.Label71 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label68 = new fecherFoundation.FCLabel();
            this.Label61 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.tabMarriages_Page2 = new fecherFoundation.FCTabPage();
            this.txtDateClerkFiled = new fecherFoundation.FCTextBox();
            this.Frame6 = new fecherFoundation.FCFrame();
            this.txtBrideDomesticPartnerYearRegistered = new fecherFoundation.FCTextBox();
            this.txtGroomDomesticPartnerYearRegistered = new fecherFoundation.FCTextBox();
            this.chkBrideDomesticPartner = new fecherFoundation.FCCheckBox();
            this.chkGroomDomesticPartner = new fecherFoundation.FCCheckBox();
            this.txtBridesMarriageEndedWhen = new fecherFoundation.FCTextBox();
            this.txtGroomsMarriageEndedWhen = new fecherFoundation.FCTextBox();
            this.txtBrideCourtName = new fecherFoundation.FCTextBox();
            this.txtBrideFormerSpouseName = new fecherFoundation.FCTextBox();
            this.txtGroomCourtName = new fecherFoundation.FCTextBox();
            this.txtGroomFormerSpouseName = new fecherFoundation.FCTextBox();
            this.cboGroomMarriageNumber = new fecherFoundation.FCComboBox();
            this.cboBridesMarriageNumber = new fecherFoundation.FCComboBox();
            this.lstBridesMarriageEndedHow = new fecherFoundation.FCComboBox();
            this.lstGroomsMarriageEndedHow = new fecherFoundation.FCComboBox();
            this.lblBrideDomesticPartnerYearRegistered = new fecherFoundation.FCLabel();
            this.lblGroomDomesticPartnerYearRegistered = new fecherFoundation.FCLabel();
            this.Label67 = new fecherFoundation.FCLabel();
            this.Label66 = new fecherFoundation.FCLabel();
            this.Label65 = new fecherFoundation.FCLabel();
            this.Label64 = new fecherFoundation.FCLabel();
            this.Label58 = new fecherFoundation.FCLabel();
            this.Label57 = new fecherFoundation.FCLabel();
            this.Label56 = new fecherFoundation.FCLabel();
            this.Label55 = new fecherFoundation.FCLabel();
            this.Label54 = new fecherFoundation.FCLabel();
            this.Label53 = new fecherFoundation.FCLabel();
            this.Label52 = new fecherFoundation.FCLabel();
            this.Label51 = new fecherFoundation.FCLabel();
            this.Label50 = new fecherFoundation.FCLabel();
            this.Label49 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtDateLicenseValid = new fecherFoundation.FCTextBox();
            this.txtDateLicenseIssued = new fecherFoundation.FCTextBox();
            this.txtDateIntentionsFiled = new fecherFoundation.FCTextBox();
            this.txtCityofIssue = new fecherFoundation.FCTextBox();
            this.Label48 = new fecherFoundation.FCLabel();
            this.Label47 = new fecherFoundation.FCLabel();
            this.Label46 = new fecherFoundation.FCLabel();
            this.Label45 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtActualDate = new Global.T2KDateBox();
            this.txtTitleOfPersonPerforming = new fecherFoundation.FCComboBox();
            this.txtDateOfCommision = new fecherFoundation.FCTextBox();
            this.txtCeremonyDate = new fecherFoundation.FCTextBox();
            this.txtCityMarried = new fecherFoundation.FCTextBox();
            this.txtCountyMarried = new fecherFoundation.FCTextBox();
            this.txtResidenceofPersonPerforming = new fecherFoundation.FCTextBox();
            this.txtMailingAddressofPersonPerforming = new fecherFoundation.FCTextBox();
            this.txtPersonPerformingCeremony = new fecherFoundation.FCTextBox();
            this.Label70 = new fecherFoundation.FCLabel();
            this.Label44 = new fecherFoundation.FCLabel();
            this.Label43 = new fecherFoundation.FCLabel();
            this.Label42 = new fecherFoundation.FCLabel();
            this.Label41 = new fecherFoundation.FCLabel();
            this.Label40 = new fecherFoundation.FCLabel();
            this.Label39 = new fecherFoundation.FCLabel();
            this.Label38 = new fecherFoundation.FCLabel();
            this.Label37 = new fecherFoundation.FCLabel();
            this.Label36 = new fecherFoundation.FCLabel();
            this.Label35 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtNameofWitnessOne = new fecherFoundation.FCTextBox();
            this.txtNameofWitnessTwo = new fecherFoundation.FCTextBox();
            this.Label27 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.cmdFind = new fecherFoundation.FCButton();
            this.txtNameOfClerk = new fecherFoundation.FCTextBox();
            this.Label60 = new fecherFoundation.FCLabel();
            this.Label59 = new fecherFoundation.FCLabel();
            this.tabMarriages_Page3 = new fecherFoundation.FCTabPage();
            this.VSFlexGrid1 = new fecherFoundation.FCGrid();
            this.mebVoided = new Global.T2KDateBox();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.imgDocuments = new fecherFoundation.FCPictureBox();
            this.ImgComment = new fecherFoundation.FCPictureBox();
            this.Label63 = new fecherFoundation.FCLabel();
            this.Label62 = new fecherFoundation.FCLabel();
            this.Label30 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuStates = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRegistrarNames = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditTown = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditTitles = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewDocs = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreviewCertificate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintIntentions = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreviewLicense = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
            this.Separator1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExitNoValidation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Separator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
            this.lblFile = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdComments = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkVerificationOfForeignGroom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVerificationOfForeignBride)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeathOnFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBirthOnFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroomBirthOnFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroomDeathOnFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSignitureOnFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoided)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequired)).BeginInit();
            this.tabMarriages.SuspendLayout();
            this.tabMarriages_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            this.tabMarriages_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBrideDomesticPartner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroomDomesticPartner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).BeginInit();
            this.tabMarriages_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VSFlexGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebVoided)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkVerificationOfForeignGroom);
            this.ClientArea.Controls.Add(this.chkVerificationOfForeignBride);
            this.ClientArea.Controls.Add(this.chkDeathOnFile);
            this.ClientArea.Controls.Add(this.chkBirthOnFile);
            this.ClientArea.Controls.Add(this.chkGroomBirthOnFile);
            this.ClientArea.Controls.Add(this.chkGroomDeathOnFile);
            this.ClientArea.Controls.Add(this.chkSignitureOnFile);
            this.ClientArea.Controls.Add(this.chkVoided);
            this.ClientArea.Controls.Add(this.chkConsent);
            this.ClientArea.Controls.Add(this.chkRequired);
            this.ClientArea.Controls.Add(this.txtFileNumber);
            this.ClientArea.Controls.Add(this.tabMarriages);
            this.ClientArea.Controls.Add(this.mebVoided);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.imgDocuments);
            this.ClientArea.Controls.Add(this.ImgComment);
            this.ClientArea.Controls.Add(this.Label63);
            this.ClientArea.Controls.Add(this.Label62);
            this.ClientArea.Controls.Add(this.Label30);
            this.ClientArea.Controls.Add(this.lblFile);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAdd);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdComments);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComments, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAdd, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(122, 30);
            this.HeaderText.Text = "Marriages";
            // 
            // chkVerificationOfForeignGroom
            // 
            this.chkVerificationOfForeignGroom.Location = new System.Drawing.Point(489, 104);
            this.chkVerificationOfForeignGroom.Name = "chkVerificationOfForeignGroom";
            this.chkVerificationOfForeignGroom.Size = new System.Drawing.Size(73, 24);
            this.chkVerificationOfForeignGroom.TabIndex = 11;
            this.chkVerificationOfForeignGroom.Text = "Party A";
            // 
            // chkVerificationOfForeignBride
            // 
            this.chkVerificationOfForeignBride.Location = new System.Drawing.Point(586, 104);
            this.chkVerificationOfForeignBride.Name = "chkVerificationOfForeignBride";
            this.chkVerificationOfForeignBride.Size = new System.Drawing.Size(73, 24);
            this.chkVerificationOfForeignBride.TabIndex = 12;
            this.chkVerificationOfForeignBride.Text = "Party B";
            // 
            // chkDeathOnFile
            // 
            this.chkDeathOnFile.Location = new System.Drawing.Point(586, 30);
            this.chkDeathOnFile.Name = "chkDeathOnFile";
            this.chkDeathOnFile.Size = new System.Drawing.Size(73, 24);
            this.chkDeathOnFile.TabIndex = 4;
            this.chkDeathOnFile.Text = "Party B";
            // 
            // chkBirthOnFile
            // 
            this.chkBirthOnFile.Location = new System.Drawing.Point(586, 67);
            this.chkBirthOnFile.Name = "chkBirthOnFile";
            this.chkBirthOnFile.Size = new System.Drawing.Size(73, 24);
            this.chkBirthOnFile.TabIndex = 8;
            this.chkBirthOnFile.Text = "Party B";
            // 
            // chkGroomBirthOnFile
            // 
            this.chkGroomBirthOnFile.Location = new System.Drawing.Point(489, 67);
            this.chkGroomBirthOnFile.Name = "chkGroomBirthOnFile";
            this.chkGroomBirthOnFile.Size = new System.Drawing.Size(73, 24);
            this.chkGroomBirthOnFile.TabIndex = 7;
            this.chkGroomBirthOnFile.Text = "Party A";
            // 
            // chkGroomDeathOnFile
            // 
            this.chkGroomDeathOnFile.Location = new System.Drawing.Point(489, 30);
            this.chkGroomDeathOnFile.Name = "chkGroomDeathOnFile";
            this.chkGroomDeathOnFile.Size = new System.Drawing.Size(73, 24);
            this.chkGroomDeathOnFile.TabIndex = 3;
            this.chkGroomDeathOnFile.Text = "Party A";
            // 
            // chkSignitureOnFile
            // 
            this.chkSignitureOnFile.Location = new System.Drawing.Point(682, 30);
            this.chkSignitureOnFile.Name = "chkSignitureOnFile";
            this.chkSignitureOnFile.Size = new System.Drawing.Size(142, 24);
            this.chkSignitureOnFile.TabIndex = 5;
            this.chkSignitureOnFile.Text = "Signatures on File";
            // 
            // chkVoided
            // 
            this.chkVoided.Location = new System.Drawing.Point(682, 67);
            this.chkVoided.Name = "chkVoided";
            this.chkVoided.Size = new System.Drawing.Size(70, 24);
            this.chkVoided.TabIndex = 9;
            this.chkVoided.Text = "Voided";
            this.chkVoided.CheckedChanged += new System.EventHandler(this.chkVoided_CheckedChanged);
            // 
            // chkConsent
            // 
            this.chkConsent.Location = new System.Drawing.Point(30, 178);
            this.chkConsent.Name = "chkConsent";
            this.chkConsent.Size = new System.Drawing.Size(365, 24);
            this.chkConsent.TabIndex = 16;
            this.chkConsent.Text = "Consent information has been received and recorded";
            // 
            // chkRequired
            // 
            this.chkRequired.Location = new System.Drawing.Point(30, 143);
            this.chkRequired.Name = "chkRequired";
            this.chkRequired.Size = new System.Drawing.Size(387, 24);
            this.chkRequired.TabIndex = 15;
            this.chkRequired.Text = "Record contains older data. Not all fields will be required";
            // 
            // txtFileNumber
            // 
            this.txtFileNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileNumber.Location = new System.Drawing.Point(209, 30);
            this.txtFileNumber.Name = "txtFileNumber";
            this.txtFileNumber.Size = new System.Drawing.Size(86, 40);
            this.txtFileNumber.TabIndex = 1;
            // 
            // tabMarriages
            // 
            this.tabMarriages.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.tabMarriages.Controls.Add(this.tabMarriages_Page1);
            this.tabMarriages.Controls.Add(this.tabMarriages_Page2);
            this.tabMarriages.Controls.Add(this.tabMarriages_Page3);
            this.tabMarriages.Location = new System.Drawing.Point(30, 222);
            this.tabMarriages.Name = "tabMarriages";
            this.tabMarriages.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
            this.tabMarriages.Size = new System.Drawing.Size(916, 1060);
            this.tabMarriages.TabIndex = 17;
            this.tabMarriages.TabStop = false;
            this.tabMarriages.Text = "Parties\' Information";
            // 
            // tabMarriages_Page1
            // 
            this.tabMarriages_Page1.Controls.Add(this.Frame2);
            this.tabMarriages_Page1.Controls.Add(this.Frame1);
            this.tabMarriages_Page1.Location = new System.Drawing.Point(1, 35);
            this.tabMarriages_Page1.Name = "tabMarriages_Page1";
            this.tabMarriages_Page1.Size = new System.Drawing.Size(914, 1024);
            this.tabMarriages_Page1.Text = "Parties\' Information";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtPartyBDesig);
            this.Frame2.Controls.Add(this.cmbPartyBType);
            this.Frame2.Controls.Add(this.cmbPartyBGender);
            this.Frame2.Controls.Add(this.txtBrideSSN);
            this.Frame2.Controls.Add(this.txtBridesDOB);
            this.Frame2.Controls.Add(this.txtBridesMothersBirthplace);
            this.Frame2.Controls.Add(this.txtBridesFathersBirthplace);
            this.Frame2.Controls.Add(this.txtState2);
            this.Frame2.Controls.Add(this.txtBrideZip);
            this.Frame2.Controls.Add(this.txtBridesMothersName);
            this.Frame2.Controls.Add(this.txtBridesFathersName);
            this.Frame2.Controls.Add(this.txtBridesAge);
            this.Frame2.Controls.Add(this.txtBridesCounty);
            this.Frame2.Controls.Add(this.txtBridesState);
            this.Frame2.Controls.Add(this.txtBridesCity);
            this.Frame2.Controls.Add(this.txtBridesStreetNumber);
            this.Frame2.Controls.Add(this.txtBridesStreetAddress);
            this.Frame2.Controls.Add(this.txtBridesMaidenSurname);
            this.Frame2.Controls.Add(this.txtBridesMiddleName);
            this.Frame2.Controls.Add(this.txtBridesFirstName);
            this.Frame2.Controls.Add(this.txtBridesCurrentLastName);
            this.Frame2.Controls.Add(this.datMarriage);
            this.Frame2.Controls.Add(this.Label72);
            this.Frame2.Controls.Add(this.Label74);
            this.Frame2.Controls.Add(this.lblCopyGroomsInfo);
            this.Frame2.Controls.Add(this.Label69);
            this.Frame2.Controls.Add(this.Label5);
            this.Frame2.Controls.Add(this.Label18);
            this.Frame2.Controls.Add(this.Label19);
            this.Frame2.Controls.Add(this.Label20);
            this.Frame2.Controls.Add(this.Label21);
            this.Frame2.Controls.Add(this.Label22);
            this.Frame2.Controls.Add(this.Label23);
            this.Frame2.Controls.Add(this.Label24);
            this.Frame2.Controls.Add(this.Label25);
            this.Frame2.Controls.Add(this.Label26);
            this.Frame2.Controls.Add(this.Label28);
            this.Frame2.Controls.Add(this.Label29);
            this.Frame2.Controls.Add(this.Label32);
            this.Frame2.Controls.Add(this.Label33);
            this.Frame2.Controls.Add(this.Label34);
            this.Frame2.Controls.Add(this.Label31);
            this.Frame2.Location = new System.Drawing.Point(20, 467);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(969, 427);
            this.Frame2.TabIndex = 2;
            this.Frame2.Text = "Party B";
            // 
            // txtPartyBDesig
            // 
            this.txtPartyBDesig.BackColor = System.Drawing.SystemColors.Window;
            this.txtPartyBDesig.Location = new System.Drawing.Point(722, 49);
            this.txtPartyBDesig.Name = "txtPartyBDesig";
            this.txtPartyBDesig.TabIndex = 9;
            // 
            // cmbPartyBType
            // 
            this.cmbPartyBType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPartyBType.Location = new System.Drawing.Point(522, 117);
            this.cmbPartyBType.Name = "cmbPartyBType";
            this.cmbPartyBType.Size = new System.Drawing.Size(230, 40);
            this.cmbPartyBType.TabIndex = 18;
            // 
            // cmbPartyBGender
            // 
            this.cmbPartyBGender.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPartyBGender.Location = new System.Drawing.Point(828, 117);
            this.cmbPartyBGender.Name = "cmbPartyBGender";
            this.cmbPartyBGender.Size = new System.Drawing.Size(121, 40);
            this.cmbPartyBGender.TabIndex = 20;
            // 
            // txtBrideSSN
            // 
            this.txtBrideSSN.BackColor = System.Drawing.SystemColors.Window;
            this.txtBrideSSN.Location = new System.Drawing.Point(176, 367);
            this.txtBrideSSN.Name = "txtBrideSSN";
            this.txtBrideSSN.Size = new System.Drawing.Size(246, 40);
            this.txtBrideSSN.TabIndex = 31;
            // 
            // txtBridesDOB
            // 
            this.txtBridesDOB.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesDOB.Location = new System.Drawing.Point(176, 317);
            this.txtBridesDOB.Name = "txtBridesDOB";
            this.txtBridesDOB.Size = new System.Drawing.Size(246, 40);
            this.txtBridesDOB.TabIndex = 29;
            this.txtBridesDOB.Tag = "Required";
            this.txtBridesDOB.Enter += new System.EventHandler(this.txtBridesDOB_Enter);
            this.txtBridesDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtBridesDOB_Validating);
            // 
            // txtBridesMothersBirthplace
            // 
            this.txtBridesMothersBirthplace.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtBridesMothersBirthplace.Location = new System.Drawing.Point(719, 367);
            this.txtBridesMothersBirthplace.Name = "txtBridesMothersBirthplace";
            this.txtBridesMothersBirthplace.Size = new System.Drawing.Size(230, 40);
            this.txtBridesMothersBirthplace.TabIndex = 40;
            this.txtBridesMothersBirthplace.Leave += new System.EventHandler(this.txtBridesMothersBirthplace_Leave);
            this.txtBridesMothersBirthplace.DoubleClick += new System.EventHandler(this.txtBridesMothersBirthplace_DoubleClick);
            this.txtBridesMothersBirthplace.TextChanged += new System.EventHandler(this.txtBridesMothersBirthplace_TextChanged);
            // 
            // txtBridesFathersBirthplace
            // 
            this.txtBridesFathersBirthplace.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtBridesFathersBirthplace.Location = new System.Drawing.Point(719, 267);
            this.txtBridesFathersBirthplace.Name = "txtBridesFathersBirthplace";
            this.txtBridesFathersBirthplace.Size = new System.Drawing.Size(230, 40);
            this.txtBridesFathersBirthplace.TabIndex = 36;
            this.txtBridesFathersBirthplace.DoubleClick += new System.EventHandler(this.txtBridesFathersBirthplace_DoubleClick);
            this.txtBridesFathersBirthplace.TextChanged += new System.EventHandler(this.txtBridesFathersBirthplace_TextChanged);
            // 
            // txtState2
            // 
            this.txtState2.BackColor = System.Drawing.SystemColors.Info;
            this.txtState2.Location = new System.Drawing.Point(176, 267);
            this.txtState2.Name = "txtState2";
            this.txtState2.Size = new System.Drawing.Size(246, 40);
            this.txtState2.TabIndex = 27;
            this.txtState2.Tag = "Required";
            this.txtState2.DoubleClick += new System.EventHandler(this.txtState2_DoubleClick);
            this.txtState2.TextChanged += new System.EventHandler(this.txtState2_TextChanged);
            // 
            // txtBrideZip
            // 
            this.txtBrideZip.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtBrideZip.Location = new System.Drawing.Point(432, 117);
            this.txtBrideZip.Name = "txtBrideZip";
            this.txtBrideZip.Size = new System.Drawing.Size(75, 40);
            this.txtBrideZip.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.txtBrideZip, "For international addresses only state and city are required (Use state field to " +
        "record the country)");
            this.txtBrideZip.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBrideZip_KeyDown);
            // 
            // txtBridesMothersName
            // 
            this.txtBridesMothersName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtBridesMothersName.Location = new System.Drawing.Point(719, 317);
            this.txtBridesMothersName.Name = "txtBridesMothersName";
            this.txtBridesMothersName.Size = new System.Drawing.Size(230, 40);
            this.txtBridesMothersName.TabIndex = 38;
            // 
            // txtBridesFathersName
            // 
            this.txtBridesFathersName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtBridesFathersName.Location = new System.Drawing.Point(719, 217);
            this.txtBridesFathersName.Name = "txtBridesFathersName";
            this.txtBridesFathersName.Size = new System.Drawing.Size(230, 40);
            this.txtBridesFathersName.TabIndex = 34;
            // 
            // txtBridesAge
            // 
            this.txtBridesAge.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesAge.Location = new System.Drawing.Point(832, 49);
            this.txtBridesAge.Name = "txtBridesAge";
            this.txtBridesAge.Size = new System.Drawing.Size(70, 40);
            this.txtBridesAge.TabIndex = 11;
            this.txtBridesAge.Tag = "Required";
            this.txtBridesAge.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtBridesAge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBridesAge_KeyPress);
            // 
            // txtBridesCounty
            // 
            this.txtBridesCounty.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesCounty.Location = new System.Drawing.Point(266, 117);
            this.txtBridesCounty.Name = "txtBridesCounty";
            this.txtBridesCounty.Size = new System.Drawing.Size(156, 40);
            this.txtBridesCounty.TabIndex = 16;
            this.txtBridesCounty.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtBridesCounty, "For international addresses only state and city are required (Use state field to " +
        "record the country)");
            this.txtBridesCounty.DoubleClick += new System.EventHandler(this.txtBridesCounty_DoubleClick);
            this.txtBridesCounty.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBridesCounty_KeyDown);
            // 
            // txtBridesState
            // 
            this.txtBridesState.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesState.Location = new System.Drawing.Point(176, 117);
            this.txtBridesState.Name = "txtBridesState";
            this.txtBridesState.Size = new System.Drawing.Size(80, 40);
            this.txtBridesState.TabIndex = 15;
            this.txtBridesState.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtBridesState, "For international addresses only state and city are required (Use state field to " +
        "record the country)");
            this.txtBridesState.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBridesState_KeyDown);
            // 
            // txtBridesCity
            // 
            this.txtBridesCity.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesCity.Location = new System.Drawing.Point(176, 167);
            this.txtBridesCity.Name = "txtBridesCity";
            this.txtBridesCity.Size = new System.Drawing.Size(246, 40);
            this.txtBridesCity.TabIndex = 22;
            this.txtBridesCity.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtBridesCity, "For international addresses only state and city are required (Use state field to " +
        "record the country)");
            this.txtBridesCity.DoubleClick += new System.EventHandler(this.txtBridesCity_DoubleClick);
            this.txtBridesCity.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBridesCity_KeyDown);
            // 
            // txtBridesStreetNumber
            // 
            this.txtBridesStreetNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesStreetNumber.Location = new System.Drawing.Point(176, 217);
            this.txtBridesStreetNumber.Name = "txtBridesStreetNumber";
            this.txtBridesStreetNumber.Size = new System.Drawing.Size(80, 40);
            this.txtBridesStreetNumber.TabIndex = 24;
            this.txtBridesStreetNumber.Tag = "Required";
            // 
            // txtBridesStreetAddress
            // 
            this.txtBridesStreetAddress.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesStreetAddress.Location = new System.Drawing.Point(266, 217);
            this.txtBridesStreetAddress.Name = "txtBridesStreetAddress";
            this.txtBridesStreetAddress.Size = new System.Drawing.Size(156, 40);
            this.txtBridesStreetAddress.TabIndex = 25;
            this.txtBridesStreetAddress.Tag = "Required";
            // 
            // txtBridesMaidenSurname
            // 
            this.txtBridesMaidenSurname.BackColor = System.Drawing.SystemColors.Window;
            this.txtBridesMaidenSurname.Location = new System.Drawing.Point(371, 49);
            this.txtBridesMaidenSurname.Name = "txtBridesMaidenSurname";
            this.txtBridesMaidenSurname.Size = new System.Drawing.Size(165, 40);
            this.txtBridesMaidenSurname.TabIndex = 5;
            // 
            // txtBridesMiddleName
            // 
            this.txtBridesMiddleName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBridesMiddleName.Location = new System.Drawing.Point(196, 49);
            this.txtBridesMiddleName.Name = "txtBridesMiddleName";
            this.txtBridesMiddleName.Size = new System.Drawing.Size(165, 40);
            this.txtBridesMiddleName.TabIndex = 3;
            // 
            // txtBridesFirstName
            // 
            this.txtBridesFirstName.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesFirstName.Location = new System.Drawing.Point(20, 49);
            this.txtBridesFirstName.Name = "txtBridesFirstName";
            this.txtBridesFirstName.Size = new System.Drawing.Size(166, 40);
            this.txtBridesFirstName.TabIndex = 1;
            this.txtBridesFirstName.Tag = "Required";
            // 
            // txtBridesCurrentLastName
            // 
            this.txtBridesCurrentLastName.BackColor = System.Drawing.SystemColors.Info;
            this.txtBridesCurrentLastName.Location = new System.Drawing.Point(546, 49);
            this.txtBridesCurrentLastName.Name = "txtBridesCurrentLastName";
            this.txtBridesCurrentLastName.Size = new System.Drawing.Size(166, 40);
            this.txtBridesCurrentLastName.TabIndex = 7;
            this.txtBridesCurrentLastName.Tag = "Required";
            // 
            // datMarriage
            // 
            this.datMarriage.BackColor = System.Drawing.Color.FromName("@white");
            this.datMarriage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("datMarriage.BackgroundImage")));
            this.datMarriage.ConnectAsString = "Access";
            this.datMarriage.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.datMarriage.Location = new System.Drawing.Point(500, -22);
            this.datMarriage.Name = "datMarriage";
            this.datMarriage.RecordSource = "";
            this.datMarriage.Size = new System.Drawing.Size(77, 24);
            this.datMarriage.TabIndex = 54;
            this.datMarriage.UserControlBackColor = System.Drawing.Color.FromName("@white");
            this.datMarriage.UserControlForeColor = System.Drawing.Color.FromName("@windowText");
            this.datMarriage.UserControlHeight = 360;
            this.datMarriage.UserControlWidth = 1155;
            this.datMarriage.Visible = false;
            // 
            // Label72
            // 
            this.Label72.Location = new System.Drawing.Point(722, 30);
            this.Label72.Name = "Label72";
            this.Label72.Size = new System.Drawing.Size(50, 16);
            this.Label72.TabIndex = 8;
            this.Label72.Text = "DESIG";
            // 
            // Label74
            // 
            this.Label74.Location = new System.Drawing.Point(772, 131);
            this.Label74.Name = "Label74";
            this.Label74.Size = new System.Drawing.Size(36, 13);
            this.Label74.TabIndex = 19;
            this.Label74.Text = "SEX";
            // 
            // lblCopyGroomsInfo
            // 
            this.lblCopyGroomsInfo.Location = new System.Drawing.Point(196, 99);
            this.lblCopyGroomsInfo.Name = "lblCopyGroomsInfo";
            this.lblCopyGroomsInfo.Size = new System.Drawing.Size(190, 16);
            this.lblCopyGroomsInfo.TabIndex = 14;
            this.lblCopyGroomsInfo.Text = "COPY PARTY A\'S INFORMATION";
            this.ToolTip1.SetToolTip(this.lblCopyGroomsInfo, "Click here to copy groom\'s residence information");
            this.lblCopyGroomsInfo.Click += new System.EventHandler(this.lblCopyGroomsInfo_Click);
            // 
            // Label69
            // 
            this.Label69.Location = new System.Drawing.Point(20, 381);
            this.Label69.Name = "Label69";
            this.Label69.Size = new System.Drawing.Size(130, 16);
            this.Label69.TabIndex = 30;
            this.Label69.Text = "SOCIAL SECURITY #";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 99);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(155, 16);
            this.Label5.TabIndex = 12;
            this.Label5.Text = "RESIDENCE INFORMATION";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(522, 381);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(170, 16);
            this.Label18.TabIndex = 39;
            this.Label18.Text = "MOTHER\'S BIRTHPLACE";
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(522, 331);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(170, 16);
            this.Label19.TabIndex = 37;
            this.Label19.Text = "MOTHER\'S FULL BIRTH NAME";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(522, 281);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(170, 16);
            this.Label20.TabIndex = 35;
            this.Label20.Text = "FATHER\'S BIRTHPLACE";
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(522, 231);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(170, 16);
            this.Label21.TabIndex = 33;
            this.Label21.Text = "FATHER\'S FULL NAME";
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(522, 191);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(250, 16);
            this.Label22.TabIndex = 32;
            this.Label22.Text = "INFORMATION OF PARTY B\'S PARENTS";
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(20, 281);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(130, 20);
            this.Label23.TabIndex = 26;
            this.Label23.Text = "BIRTHPLACE (STATE)";
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(832, 30);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(130, 16);
            this.Label24.TabIndex = 10;
            this.Label24.Text = "AGE LAST BIRTHDAY";
            // 
            // Label25
            // 
            this.Label25.Location = new System.Drawing.Point(20, 331);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(130, 16);
            this.Label25.TabIndex = 28;
            this.Label25.Text = "DATE OF BIRTH";
            // 
            // Label26
            // 
            this.Label26.Location = new System.Drawing.Point(20, 131);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(128, 14);
            this.Label26.TabIndex = 13;
            this.Label26.Text = "STATE / COUNTY / ZIP";
            // 
            // Label28
            // 
            this.Label28.Location = new System.Drawing.Point(20, 181);
            this.Label28.Name = "Label28";
            this.Label28.TabIndex = 21;
            this.Label28.Text = "CITY OR TOWN";
            // 
            // Label29
            // 
            this.Label29.Location = new System.Drawing.Point(20, 231);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(124, 14);
            this.Label29.TabIndex = 23;
            this.Label29.Text = "STREET # AND NAME";
            // 
            // Label32
            // 
            this.Label32.Location = new System.Drawing.Point(371, 30);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(102, 13);
            this.Label32.TabIndex = 4;
            this.Label32.Text = "BIRTH SURNAME";
            // 
            // Label33
            // 
            this.Label33.Location = new System.Drawing.Point(196, 30);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(84, 13);
            this.Label33.TabIndex = 2;
            this.Label33.Text = "MIDDLE NAME";
            // 
            // Label34
            // 
            this.Label34.Location = new System.Drawing.Point(20, 30);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(81, 13);
            this.Label34.TabIndex = 55;
            this.Label34.Text = "FIRST NAME";
            // 
            // Label31
            // 
            this.Label31.Location = new System.Drawing.Point(546, 30);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(128, 13);
            this.Label31.TabIndex = 6;
            this.Label31.Text = "CURRENT LAST NAME";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cmbPartyAType);
            this.Frame1.Controls.Add(this.cmbPartyAGender);
            this.Frame1.Controls.Add(this.txtPartyABirthName);
            this.Frame1.Controls.Add(this.txtGroomSSN);
            this.Frame1.Controls.Add(this.txtGroomsDOB);
            this.Frame1.Controls.Add(this.txtGroomsMothersBirthplace);
            this.Frame1.Controls.Add(this.txtGroomsFathersBirthplace);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtGroomZip);
            this.Frame1.Controls.Add(this.txtGroomsFirstName);
            this.Frame1.Controls.Add(this.txtGroomsMiddleName);
            this.Frame1.Controls.Add(this.txtGroomsLastName);
            this.Frame1.Controls.Add(this.txtGroomsDesignation);
            this.Frame1.Controls.Add(this.txtGroomsStreetName);
            this.Frame1.Controls.Add(this.txtGroomsStreetNumber);
            this.Frame1.Controls.Add(this.txtGroomsCity);
            this.Frame1.Controls.Add(this.txtGroomsState);
            this.Frame1.Controls.Add(this.txtGroomsCounty);
            this.Frame1.Controls.Add(this.txtGroomsAge);
            this.Frame1.Controls.Add(this.txtGroomsFathersName);
            this.Frame1.Controls.Add(this.txtGroomsMothersName);
            this.Frame1.Controls.Add(this.Label73);
            this.Frame1.Controls.Add(this.Label71);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label68);
            this.Frame1.Controls.Add(this.Label61);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label9);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Controls.Add(this.Label13);
            this.Frame1.Controls.Add(this.Label14);
            this.Frame1.Controls.Add(this.Label15);
            this.Frame1.Controls.Add(this.Label16);
            this.Frame1.Controls.Add(this.Label17);
            this.Frame1.Location = new System.Drawing.Point(20, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(969, 427);
            this.Frame1.TabIndex = 1;
            this.Frame1.Text = "Party A";
            // 
            // cmbPartyAType
            // 
            this.cmbPartyAType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPartyAType.Location = new System.Drawing.Point(522, 117);
            this.cmbPartyAType.Name = "cmbPartyAType";
            this.cmbPartyAType.Size = new System.Drawing.Size(230, 40);
            this.cmbPartyAType.TabIndex = 17;
            // 
            // cmbPartyAGender
            // 
            this.cmbPartyAGender.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPartyAGender.Location = new System.Drawing.Point(828, 117);
            this.cmbPartyAGender.Name = "cmbPartyAGender";
            this.cmbPartyAGender.Size = new System.Drawing.Size(121, 40);
            this.cmbPartyAGender.TabIndex = 19;
            // 
            // txtPartyABirthName
            // 
            this.txtPartyABirthName.BackColor = System.Drawing.SystemColors.Window;
            this.txtPartyABirthName.Location = new System.Drawing.Point(371, 49);
            this.txtPartyABirthName.Name = "txtPartyABirthName";
            this.txtPartyABirthName.Size = new System.Drawing.Size(165, 40);
            this.txtPartyABirthName.TabIndex = 5;
            // 
            // txtGroomSSN
            // 
            this.txtGroomSSN.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroomSSN.Location = new System.Drawing.Point(176, 367);
            this.txtGroomSSN.Name = "txtGroomSSN";
            this.txtGroomSSN.Size = new System.Drawing.Size(246, 40);
            this.txtGroomSSN.TabIndex = 30;
            // 
            // txtGroomsDOB
            // 
            this.txtGroomsDOB.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsDOB.Location = new System.Drawing.Point(176, 317);
            this.txtGroomsDOB.Name = "txtGroomsDOB";
            this.txtGroomsDOB.Size = new System.Drawing.Size(246, 40);
            this.txtGroomsDOB.TabIndex = 28;
            this.txtGroomsDOB.Tag = "Required";
            this.txtGroomsDOB.Enter += new System.EventHandler(this.txtGroomsDOB_Enter);
            this.txtGroomsDOB.Validating += new System.ComponentModel.CancelEventHandler(this.txtGroomsDOB_Validating);
            // 
            // txtGroomsMothersBirthplace
            // 
            this.txtGroomsMothersBirthplace.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtGroomsMothersBirthplace.Location = new System.Drawing.Point(719, 367);
            this.txtGroomsMothersBirthplace.Name = "txtGroomsMothersBirthplace";
            this.txtGroomsMothersBirthplace.Size = new System.Drawing.Size(230, 40);
            this.txtGroomsMothersBirthplace.TabIndex = 39;
            this.txtGroomsMothersBirthplace.DoubleClick += new System.EventHandler(this.txtGroomsMothersBirthplace_DoubleClick);
            this.txtGroomsMothersBirthplace.TextChanged += new System.EventHandler(this.txtGroomsMothersBirthplace_TextChanged);
            // 
            // txtGroomsFathersBirthplace
            // 
            this.txtGroomsFathersBirthplace.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtGroomsFathersBirthplace.Location = new System.Drawing.Point(719, 267);
            this.txtGroomsFathersBirthplace.Name = "txtGroomsFathersBirthplace";
            this.txtGroomsFathersBirthplace.Size = new System.Drawing.Size(230, 40);
            this.txtGroomsFathersBirthplace.TabIndex = 35;
            this.txtGroomsFathersBirthplace.DoubleClick += new System.EventHandler(this.txtGroomsFathersBirthplace_DoubleClick);
            this.txtGroomsFathersBirthplace.TextChanged += new System.EventHandler(this.txtGroomsFathersBirthplace_TextChanged);
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Info;
            this.txtState.Location = new System.Drawing.Point(176, 267);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(246, 40);
            this.txtState.TabIndex = 26;
            this.txtState.Tag = "Required";
            this.txtState.DoubleClick += new System.EventHandler(this.txtState_DoubleClick);
            this.txtState.TextChanged += new System.EventHandler(this.txtState_TextChanged);
            // 
            // txtGroomZip
            // 
            this.txtGroomZip.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtGroomZip.Location = new System.Drawing.Point(432, 117);
            this.txtGroomZip.Name = "txtGroomZip";
            this.txtGroomZip.Size = new System.Drawing.Size(75, 40);
            this.txtGroomZip.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.txtGroomZip, "For international addresses only state and city are required (Use state field to " +
        "record the country)");
            this.txtGroomZip.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGroomZip_KeyDown);
            // 
            // txtGroomsFirstName
            // 
            this.txtGroomsFirstName.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsFirstName.Location = new System.Drawing.Point(20, 49);
            this.txtGroomsFirstName.Name = "txtGroomsFirstName";
            this.txtGroomsFirstName.Size = new System.Drawing.Size(166, 40);
            this.txtGroomsFirstName.TabIndex = 1;
            this.txtGroomsFirstName.Tag = "Required";
            // 
            // txtGroomsMiddleName
            // 
            this.txtGroomsMiddleName.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroomsMiddleName.Location = new System.Drawing.Point(196, 49);
            this.txtGroomsMiddleName.Name = "txtGroomsMiddleName";
            this.txtGroomsMiddleName.Size = new System.Drawing.Size(165, 40);
            this.txtGroomsMiddleName.TabIndex = 3;
            // 
            // txtGroomsLastName
            // 
            this.txtGroomsLastName.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsLastName.Location = new System.Drawing.Point(546, 49);
            this.txtGroomsLastName.Name = "txtGroomsLastName";
            this.txtGroomsLastName.Size = new System.Drawing.Size(166, 40);
            this.txtGroomsLastName.TabIndex = 7;
            this.txtGroomsLastName.Tag = "Required";
            // 
            // txtGroomsDesignation
            // 
            this.txtGroomsDesignation.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroomsDesignation.Location = new System.Drawing.Point(722, 49);
            this.txtGroomsDesignation.Name = "txtGroomsDesignation";
            this.txtGroomsDesignation.TabIndex = 9;
            // 
            // txtGroomsStreetName
            // 
            this.txtGroomsStreetName.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsStreetName.Location = new System.Drawing.Point(266, 217);
            this.txtGroomsStreetName.Name = "txtGroomsStreetName";
            this.txtGroomsStreetName.Size = new System.Drawing.Size(156, 40);
            this.txtGroomsStreetName.TabIndex = 24;
            this.txtGroomsStreetName.Tag = "Required";
            // 
            // txtGroomsStreetNumber
            // 
            this.txtGroomsStreetNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsStreetNumber.Location = new System.Drawing.Point(176, 217);
            this.txtGroomsStreetNumber.Name = "txtGroomsStreetNumber";
            this.txtGroomsStreetNumber.Size = new System.Drawing.Size(80, 40);
            this.txtGroomsStreetNumber.TabIndex = 23;
            this.txtGroomsStreetNumber.Tag = "Required";
            // 
            // txtGroomsCity
            // 
            this.txtGroomsCity.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsCity.Location = new System.Drawing.Point(176, 167);
            this.txtGroomsCity.Name = "txtGroomsCity";
            this.txtGroomsCity.Size = new System.Drawing.Size(246, 40);
            this.txtGroomsCity.TabIndex = 21;
            this.txtGroomsCity.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtGroomsCity, "For international addresses only state and city are required (Use state field to " +
        "record the country)");
            this.txtGroomsCity.DoubleClick += new System.EventHandler(this.txtGroomsCity_DoubleClick);
            this.txtGroomsCity.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGroomsCity_KeyDown);
            // 
            // txtGroomsState
            // 
            this.txtGroomsState.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsState.Location = new System.Drawing.Point(176, 117);
            this.txtGroomsState.Name = "txtGroomsState";
            this.txtGroomsState.Size = new System.Drawing.Size(80, 40);
            this.txtGroomsState.TabIndex = 14;
            this.txtGroomsState.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtGroomsState, "For international addresses only state and city are required (Use state field to " +
        "record the country)");
            this.txtGroomsState.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGroomsState_KeyDown);
            // 
            // txtGroomsCounty
            // 
            this.txtGroomsCounty.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsCounty.Location = new System.Drawing.Point(266, 117);
            this.txtGroomsCounty.Name = "txtGroomsCounty";
            this.txtGroomsCounty.Size = new System.Drawing.Size(156, 40);
            this.txtGroomsCounty.TabIndex = 15;
            this.txtGroomsCounty.Tag = "Required";
            this.ToolTip1.SetToolTip(this.txtGroomsCounty, "For international addresses only state and city are required (Use state field to " +
        "record the country)");
            this.txtGroomsCounty.DoubleClick += new System.EventHandler(this.txtGroomsCounty_DoubleClick);
            this.txtGroomsCounty.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGroomsCounty_KeyDown);
            // 
            // txtGroomsAge
            // 
            this.txtGroomsAge.BackColor = System.Drawing.SystemColors.Info;
            this.txtGroomsAge.Location = new System.Drawing.Point(832, 49);
            this.txtGroomsAge.Name = "txtGroomsAge";
            this.txtGroomsAge.Size = new System.Drawing.Size(70, 40);
            this.txtGroomsAge.TabIndex = 11;
            this.txtGroomsAge.Tag = "Required";
            this.txtGroomsAge.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtGroomsAge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGroomsAge_KeyPress);
            // 
            // txtGroomsFathersName
            // 
            this.txtGroomsFathersName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtGroomsFathersName.Location = new System.Drawing.Point(719, 217);
            this.txtGroomsFathersName.Name = "txtGroomsFathersName";
            this.txtGroomsFathersName.Size = new System.Drawing.Size(230, 40);
            this.txtGroomsFathersName.TabIndex = 33;
            // 
            // txtGroomsMothersName
            // 
            this.txtGroomsMothersName.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.txtGroomsMothersName.Location = new System.Drawing.Point(719, 317);
            this.txtGroomsMothersName.Name = "txtGroomsMothersName";
            this.txtGroomsMothersName.Size = new System.Drawing.Size(230, 40);
            this.txtGroomsMothersName.TabIndex = 37;
            // 
            // Label73
            // 
            this.Label73.Location = new System.Drawing.Point(772, 131);
            this.Label73.Name = "Label73";
            this.Label73.Size = new System.Drawing.Size(36, 13);
            this.Label73.TabIndex = 18;
            this.Label73.Text = "SEX";
            // 
            // Label71
            // 
            this.Label71.Location = new System.Drawing.Point(371, 30);
            this.Label71.Name = "Label71";
            this.Label71.Size = new System.Drawing.Size(98, 13);
            this.Label71.TabIndex = 4;
            this.Label71.Text = "BIRTH SURNAME";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(832, 30);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(117, 16);
            this.Label11.TabIndex = 10;
            this.Label11.Text = "AGE LAST BIRTHDAY";
            // 
            // Label68
            // 
            this.Label68.Location = new System.Drawing.Point(20, 381);
            this.Label68.Name = "Label68";
            this.Label68.Size = new System.Drawing.Size(120, 14);
            this.Label68.TabIndex = 29;
            this.Label68.Text = "SOCIAL SECURITY #";
            // 
            // Label61
            // 
            this.Label61.Location = new System.Drawing.Point(20, 99);
            this.Label61.Name = "Label61";
            this.Label61.Size = new System.Drawing.Size(155, 16);
            this.Label61.TabIndex = 12;
            this.Label61.Text = "RESIDENCE INFORMATION";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 13);
            this.Label1.TabIndex = 40;
            this.Label1.Text = "FIRST NAME";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(196, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(88, 13);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "MIDDLE NAME";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(546, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(128, 13);
            this.Label3.TabIndex = 6;
            this.Label3.Text = "CURRENT LAST NAME";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(722, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(60, 14);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "DESIG";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 231);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(124, 14);
            this.Label6.TabIndex = 22;
            this.Label6.Text = "STREET # AND NAME";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 181);
            this.Label7.Name = "Label7";
            this.Label7.TabIndex = 20;
            this.Label7.Text = "CITY OR TOWN";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 131);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(125, 14);
            this.Label9.TabIndex = 13;
            this.Label9.Text = "STATE / COUNTY / ZIP";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 331);
            this.Label10.Name = "Label10";
            this.Label10.TabIndex = 27;
            this.Label10.Text = "DATE OF BIRTH";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 281);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(130, 14);
            this.Label12.TabIndex = 25;
            this.Label12.Text = "BIRTHPLACE (STATE)";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(522, 191);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(250, 15);
            this.Label13.TabIndex = 31;
            this.Label13.Text = "INFORMATION OF PARTY A\'S PARENTS";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(522, 231);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(170, 16);
            this.Label14.TabIndex = 32;
            this.Label14.Text = "FATHER\'S FULL NAME";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(522, 281);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(170, 16);
            this.Label15.TabIndex = 34;
            this.Label15.Text = "FATHER\'S BIRTHPLACE";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(522, 331);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(170, 16);
            this.Label16.TabIndex = 36;
            this.Label16.Text = "MOTHER\'S FULL BIRTH NAME";
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(522, 381);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(170, 16);
            this.Label17.TabIndex = 38;
            this.Label17.Text = "MOTHER\'S BIRTHPLACE";
            // 
            // tabMarriages_Page2
            // 
            this.tabMarriages_Page2.Controls.Add(this.txtDateClerkFiled);
            this.tabMarriages_Page2.Controls.Add(this.Frame6);
            this.tabMarriages_Page2.Controls.Add(this.Frame5);
            this.tabMarriages_Page2.Controls.Add(this.Frame3);
            this.tabMarriages_Page2.Controls.Add(this.Frame4);
            this.tabMarriages_Page2.Controls.Add(this.cmdFind);
            this.tabMarriages_Page2.Controls.Add(this.txtNameOfClerk);
            this.tabMarriages_Page2.Controls.Add(this.Label60);
            this.tabMarriages_Page2.Controls.Add(this.Label59);
            this.tabMarriages_Page2.Location = new System.Drawing.Point(1, 35);
            this.tabMarriages_Page2.Name = "tabMarriages_Page2";
            this.tabMarriages_Page2.Size = new System.Drawing.Size(914, 1024);
            this.tabMarriages_Page2.Text = "Status / Ceremony / Other";
            // 
            // txtDateClerkFiled
            // 
            this.txtDateClerkFiled.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateClerkFiled.Location = new System.Drawing.Point(118, 954);
            this.txtDateClerkFiled.Name = "txtDateClerkFiled";
            this.txtDateClerkFiled.Size = new System.Drawing.Size(162, 40);
            this.txtDateClerkFiled.TabIndex = 6;
            this.txtDateClerkFiled.Enter += new System.EventHandler(this.txtDateClerkFiled_Enter);
            this.txtDateClerkFiled.TextChanged += new System.EventHandler(this.txtDateClerkFiled_TextChanged);
            this.txtDateClerkFiled.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateClerkFiled_Validating);
            // 
            // Frame6
            // 
            this.Frame6.Controls.Add(this.txtBrideDomesticPartnerYearRegistered);
            this.Frame6.Controls.Add(this.txtGroomDomesticPartnerYearRegistered);
            this.Frame6.Controls.Add(this.chkBrideDomesticPartner);
            this.Frame6.Controls.Add(this.chkGroomDomesticPartner);
            this.Frame6.Controls.Add(this.txtBridesMarriageEndedWhen);
            this.Frame6.Controls.Add(this.txtGroomsMarriageEndedWhen);
            this.Frame6.Controls.Add(this.txtBrideCourtName);
            this.Frame6.Controls.Add(this.txtBrideFormerSpouseName);
            this.Frame6.Controls.Add(this.txtGroomCourtName);
            this.Frame6.Controls.Add(this.txtGroomFormerSpouseName);
            this.Frame6.Controls.Add(this.cboGroomMarriageNumber);
            this.Frame6.Controls.Add(this.cboBridesMarriageNumber);
            this.Frame6.Controls.Add(this.lstBridesMarriageEndedHow);
            this.Frame6.Controls.Add(this.lstGroomsMarriageEndedHow);
            this.Frame6.Controls.Add(this.lblBrideDomesticPartnerYearRegistered);
            this.Frame6.Controls.Add(this.lblGroomDomesticPartnerYearRegistered);
            this.Frame6.Controls.Add(this.Label67);
            this.Frame6.Controls.Add(this.Label66);
            this.Frame6.Controls.Add(this.Label65);
            this.Frame6.Controls.Add(this.Label64);
            this.Frame6.Controls.Add(this.Label58);
            this.Frame6.Controls.Add(this.Label57);
            this.Frame6.Controls.Add(this.Label56);
            this.Frame6.Controls.Add(this.Label55);
            this.Frame6.Controls.Add(this.Label54);
            this.Frame6.Controls.Add(this.Label53);
            this.Frame6.Controls.Add(this.Label52);
            this.Frame6.Controls.Add(this.Label51);
            this.Frame6.Controls.Add(this.Label50);
            this.Frame6.Controls.Add(this.Label49);
            this.Frame6.Location = new System.Drawing.Point(20, 30);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(969, 290);
            this.Frame6.TabIndex = 1;
            this.Frame6.Text = "Marital Status";
            // 
            // txtBrideDomesticPartnerYearRegistered
            // 
            this.txtBrideDomesticPartnerYearRegistered.BackColor = System.Drawing.SystemColors.Window;
            this.txtBrideDomesticPartnerYearRegistered.Enabled = false;
            this.txtBrideDomesticPartnerYearRegistered.Location = new System.Drawing.Point(886, 230);
            this.txtBrideDomesticPartnerYearRegistered.MaxLength = 4;
            this.txtBrideDomesticPartnerYearRegistered.Name = "txtBrideDomesticPartnerYearRegistered";
            this.txtBrideDomesticPartnerYearRegistered.Size = new System.Drawing.Size(64, 40);
            this.txtBrideDomesticPartnerYearRegistered.TabIndex = 29;
            // 
            // txtGroomDomesticPartnerYearRegistered
            // 
            this.txtGroomDomesticPartnerYearRegistered.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroomDomesticPartnerYearRegistered.Location = new System.Drawing.Point(411, 230);
            this.txtGroomDomesticPartnerYearRegistered.MaxLength = 4;
            this.txtGroomDomesticPartnerYearRegistered.Name = "txtGroomDomesticPartnerYearRegistered";
            this.txtGroomDomesticPartnerYearRegistered.Size = new System.Drawing.Size(64, 40);
            this.txtGroomDomesticPartnerYearRegistered.TabIndex = 22;
            // 
            // chkBrideDomesticPartner
            // 
            this.chkBrideDomesticPartner.Location = new System.Drawing.Point(495, 234);
            this.chkBrideDomesticPartner.Name = "chkBrideDomesticPartner";
            this.chkBrideDomesticPartner.Size = new System.Drawing.Size(210, 24);
            this.chkBrideDomesticPartner.TabIndex = 27;
            this.chkBrideDomesticPartner.Text = "Registered Domestic Partner";
            this.chkBrideDomesticPartner.CheckedChanged += new System.EventHandler(this.chkBrideDomesticPartner_CheckedChanged);
            // 
            // chkGroomDomesticPartner
            // 
            this.chkGroomDomesticPartner.Location = new System.Drawing.Point(20, 234);
            this.chkGroomDomesticPartner.Name = "chkGroomDomesticPartner";
            this.chkGroomDomesticPartner.Size = new System.Drawing.Size(210, 24);
            this.chkGroomDomesticPartner.TabIndex = 20;
            this.chkGroomDomesticPartner.Text = "Registered Domestic Partner";
            this.chkGroomDomesticPartner.CheckedChanged += new System.EventHandler(this.chkGroomDomesticPartner_CheckedChanged);
            // 
            // txtBridesMarriageEndedWhen
            // 
            this.txtBridesMarriageEndedWhen.BackColor = System.Drawing.SystemColors.Window;
            this.txtBridesMarriageEndedWhen.Location = new System.Drawing.Point(805, 80);
            this.txtBridesMarriageEndedWhen.Name = "txtBridesMarriageEndedWhen";
            this.txtBridesMarriageEndedWhen.Size = new System.Drawing.Size(145, 40);
            this.txtBridesMarriageEndedWhen.TabIndex = 15;
            this.txtBridesMarriageEndedWhen.Enter += new System.EventHandler(this.txtBridesMarriageEndedWhen_Enter);
            this.txtBridesMarriageEndedWhen.TextChanged += new System.EventHandler(this.txtBridesMarriageEndedWhen_TextChanged);
            this.txtBridesMarriageEndedWhen.Validating += new System.ComponentModel.CancelEventHandler(this.txtBridesMarriageEndedWhen_Validating);
            // 
            // txtGroomsMarriageEndedWhen
            // 
            this.txtGroomsMarriageEndedWhen.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroomsMarriageEndedWhen.Location = new System.Drawing.Point(330, 80);
            this.txtGroomsMarriageEndedWhen.Name = "txtGroomsMarriageEndedWhen";
            this.txtGroomsMarriageEndedWhen.Size = new System.Drawing.Size(145, 40);
            this.txtGroomsMarriageEndedWhen.TabIndex = 7;
            this.txtGroomsMarriageEndedWhen.Enter += new System.EventHandler(this.txtGroomsMarriageEndedWhen_Enter);
            this.txtGroomsMarriageEndedWhen.TextChanged += new System.EventHandler(this.txtGroomsMarriageEndedWhen_TextChanged);
            this.txtGroomsMarriageEndedWhen.Validating += new System.ComponentModel.CancelEventHandler(this.txtGroomsMarriageEndedWhen_Validating);
            // 
            // txtBrideCourtName
            // 
            this.txtBrideCourtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBrideCourtName.Location = new System.Drawing.Point(694, 180);
            this.txtBrideCourtName.Name = "txtBrideCourtName";
            this.txtBrideCourtName.Size = new System.Drawing.Size(256, 40);
            this.txtBrideCourtName.TabIndex = 26;
            this.txtBrideCourtName.TextChanged += new System.EventHandler(this.txtBrideCourtName_TextChanged);
            // 
            // txtBrideFormerSpouseName
            // 
            this.txtBrideFormerSpouseName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBrideFormerSpouseName.Location = new System.Drawing.Point(694, 130);
            this.txtBrideFormerSpouseName.Name = "txtBrideFormerSpouseName";
            this.txtBrideFormerSpouseName.Size = new System.Drawing.Size(256, 40);
            this.txtBrideFormerSpouseName.TabIndex = 24;
            this.txtBrideFormerSpouseName.TextChanged += new System.EventHandler(this.txtBrideFormerSpouseName_TextChanged);
            // 
            // txtGroomCourtName
            // 
            this.txtGroomCourtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroomCourtName.Location = new System.Drawing.Point(220, 180);
            this.txtGroomCourtName.Name = "txtGroomCourtName";
            this.txtGroomCourtName.Size = new System.Drawing.Size(255, 40);
            this.txtGroomCourtName.TabIndex = 19;
            this.txtGroomCourtName.TextChanged += new System.EventHandler(this.txtGroomCourtName_TextChanged);
            // 
            // txtGroomFormerSpouseName
            // 
            this.txtGroomFormerSpouseName.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroomFormerSpouseName.Location = new System.Drawing.Point(220, 130);
            this.txtGroomFormerSpouseName.Name = "txtGroomFormerSpouseName";
            this.txtGroomFormerSpouseName.Size = new System.Drawing.Size(255, 40);
            this.txtGroomFormerSpouseName.TabIndex = 17;
            this.txtGroomFormerSpouseName.TextChanged += new System.EventHandler(this.txtGroomFormerSpouseName_TextChanged);
            // 
            // cboGroomMarriageNumber
            // 
            this.cboGroomMarriageNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboGroomMarriageNumber.Items.AddRange(new object[] {
            "",
            "First",
            "Second",
            "Third",
            "Fourth",
            "Fifth",
            "Sixth",
            "Seventh",
            "Eighth"});
            this.cboGroomMarriageNumber.Location = new System.Drawing.Point(20, 80);
            this.cboGroomMarriageNumber.Name = "cboGroomMarriageNumber";
            this.cboGroomMarriageNumber.Size = new System.Drawing.Size(145, 40);
            this.cboGroomMarriageNumber.TabIndex = 1;
            this.cboGroomMarriageNumber.Tag = "Required";
            this.cboGroomMarriageNumber.SelectedIndexChanged += new System.EventHandler(this.cboGroomMarriageNumber_SelectedIndexChanged);
            this.cboGroomMarriageNumber.Enter += new System.EventHandler(this.cboGroomMarriageNumber_Enter);
            // 
            // cboBridesMarriageNumber
            // 
            this.cboBridesMarriageNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboBridesMarriageNumber.Items.AddRange(new object[] {
            "",
            "First",
            "Second",
            "Third",
            "Fourth",
            "Fifth",
            "Sixth",
            "Seventh",
            "Eighth"});
            this.cboBridesMarriageNumber.Location = new System.Drawing.Point(495, 80);
            this.cboBridesMarriageNumber.Name = "cboBridesMarriageNumber";
            this.cboBridesMarriageNumber.Size = new System.Drawing.Size(145, 40);
            this.cboBridesMarriageNumber.TabIndex = 9;
            this.cboBridesMarriageNumber.Tag = "Required";
            this.cboBridesMarriageNumber.SelectedIndexChanged += new System.EventHandler(this.cboBridesMarriageNumber_SelectedIndexChanged);
            // 
            // lstBridesMarriageEndedHow
            // 
            this.lstBridesMarriageEndedHow.BackColor = System.Drawing.SystemColors.Window;
            this.lstBridesMarriageEndedHow.Items.AddRange(new object[] {
            "",
            "Annulment",
            "Death",
            "Divorce"});
            this.lstBridesMarriageEndedHow.Location = new System.Drawing.Point(650, 80);
            this.lstBridesMarriageEndedHow.Name = "lstBridesMarriageEndedHow";
            this.lstBridesMarriageEndedHow.Size = new System.Drawing.Size(145, 40);
            this.lstBridesMarriageEndedHow.TabIndex = 12;
            this.lstBridesMarriageEndedHow.SelectedIndexChanged += new System.EventHandler(this.lstBridesMarriageEndedHow_SelectedIndexChanged);
            // 
            // lstGroomsMarriageEndedHow
            // 
            this.lstGroomsMarriageEndedHow.BackColor = System.Drawing.SystemColors.Window;
            this.lstGroomsMarriageEndedHow.Items.AddRange(new object[] {
            "",
            "Annulment",
            "Death",
            "Divorce"});
            this.lstGroomsMarriageEndedHow.Location = new System.Drawing.Point(175, 80);
            this.lstGroomsMarriageEndedHow.Name = "lstGroomsMarriageEndedHow";
            this.lstGroomsMarriageEndedHow.Size = new System.Drawing.Size(145, 40);
            this.lstGroomsMarriageEndedHow.TabIndex = 4;
            this.lstGroomsMarriageEndedHow.SelectedIndexChanged += new System.EventHandler(this.lstGroomsMarriageEndedHow_SelectedIndexChanged);
            // 
            // lblBrideDomesticPartnerYearRegistered
            // 
            this.lblBrideDomesticPartnerYearRegistered.Enabled = false;
            this.lblBrideDomesticPartnerYearRegistered.Location = new System.Drawing.Point(748, 244);
            this.lblBrideDomesticPartnerYearRegistered.Name = "lblBrideDomesticPartnerYearRegistered";
            this.lblBrideDomesticPartnerYearRegistered.Size = new System.Drawing.Size(120, 14);
            this.lblBrideDomesticPartnerYearRegistered.TabIndex = 28;
            this.lblBrideDomesticPartnerYearRegistered.Text = "YEAR REGISTERED";
            // 
            // lblGroomDomesticPartnerYearRegistered
            // 
            this.lblGroomDomesticPartnerYearRegistered.Enabled = false;
            this.lblGroomDomesticPartnerYearRegistered.Location = new System.Drawing.Point(273, 244);
            this.lblGroomDomesticPartnerYearRegistered.Name = "lblGroomDomesticPartnerYearRegistered";
            this.lblGroomDomesticPartnerYearRegistered.Size = new System.Drawing.Size(120, 14);
            this.lblGroomDomesticPartnerYearRegistered.TabIndex = 21;
            this.lblGroomDomesticPartnerYearRegistered.Text = "YEAR REGISTERED";
            // 
            // Label67
            // 
            this.Label67.Enabled = false;
            this.Label67.Location = new System.Drawing.Point(495, 194);
            this.Label67.Name = "Label67";
            this.Label67.Size = new System.Drawing.Size(170, 16);
            this.Label67.TabIndex = 25;
            this.Label67.Text = "LOCATION / NAME OF COURT";
            // 
            // Label66
            // 
            this.Label66.Enabled = false;
            this.Label66.Location = new System.Drawing.Point(495, 144);
            this.Label66.Name = "Label66";
            this.Label66.Size = new System.Drawing.Size(170, 16);
            this.Label66.TabIndex = 23;
            this.Label66.Text = "NAME OF FORMER SPOUSE";
            // 
            // Label65
            // 
            this.Label65.Enabled = false;
            this.Label65.Location = new System.Drawing.Point(20, 194);
            this.Label65.Name = "Label65";
            this.Label65.Size = new System.Drawing.Size(170, 16);
            this.Label65.TabIndex = 18;
            this.Label65.Text = "LOCATION / NAME OF COURT";
            // 
            // Label64
            // 
            this.Label64.Enabled = false;
            this.Label64.Location = new System.Drawing.Point(20, 144);
            this.Label64.Name = "Label64";
            this.Label64.Size = new System.Drawing.Size(170, 16);
            this.Label64.TabIndex = 16;
            this.Label64.Text = "NAME OF FORMER SPOUSE";
            // 
            // Label58
            // 
            this.Label58.Location = new System.Drawing.Point(175, 30);
            this.Label58.Name = "Label58";
            this.Label58.Size = new System.Drawing.Size(55, 18);
            this.Label58.TabIndex = 2;
            this.Label58.Text = "PARTY A";
            // 
            // Label57
            // 
            this.Label57.Location = new System.Drawing.Point(650, 30);
            this.Label57.Name = "Label57";
            this.Label57.Size = new System.Drawing.Size(55, 18);
            this.Label57.TabIndex = 10;
            this.Label57.Text = "PARTY B";
            // 
            // Label56
            // 
            this.Label56.Location = new System.Drawing.Point(20, 50);
            this.Label56.Name = "Label56";
            this.Label56.Size = new System.Drawing.Size(145, 30);
            this.Label56.TabIndex = 30;
            this.Label56.Text = "NUMBER OF THIS MARRIAGE";
            this.Label56.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label55
            // 
            this.Label55.Enabled = false;
            this.Label55.Location = new System.Drawing.Point(265, 30);
            this.Label55.Name = "Label55";
            this.Label55.Size = new System.Drawing.Size(150, 29);
            this.Label55.TabIndex = 5;
            this.Label55.Text = "IF PREVIOUSLY MARRIED, LAST MARRIAGE ENDED";
            // 
            // Label54
            // 
            this.Label54.Enabled = false;
            this.Label54.Location = new System.Drawing.Point(175, 64);
            this.Label54.Name = "Label54";
            this.Label54.Size = new System.Drawing.Size(145, 16);
            this.Label54.TabIndex = 3;
            this.Label54.Text = "HOW";
            this.Label54.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label53
            // 
            this.Label53.Enabled = false;
            this.Label53.Location = new System.Drawing.Point(330, 64);
            this.Label53.Name = "Label53";
            this.Label53.Size = new System.Drawing.Size(145, 16);
            this.Label53.TabIndex = 6;
            this.Label53.Text = "WHEN";
            this.Label53.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label52
            // 
            this.Label52.Location = new System.Drawing.Point(495, 50);
            this.Label52.Name = "Label52";
            this.Label52.Size = new System.Drawing.Size(145, 30);
            this.Label52.TabIndex = 8;
            this.Label52.Text = "NUMBER OF THIS MARRIAGE";
            this.Label52.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label51
            // 
            this.Label51.Enabled = false;
            this.Label51.Location = new System.Drawing.Point(735, 30);
            this.Label51.Name = "Label51";
            this.Label51.Size = new System.Drawing.Size(150, 29);
            this.Label51.TabIndex = 13;
            this.Label51.Text = "IF PREVIOUSLY MARRIED, LAST MARRIAGE ENDED";
            // 
            // Label50
            // 
            this.Label50.Enabled = false;
            this.Label50.Location = new System.Drawing.Point(650, 64);
            this.Label50.Name = "Label50";
            this.Label50.Size = new System.Drawing.Size(145, 16);
            this.Label50.TabIndex = 11;
            this.Label50.Text = "HOW";
            this.Label50.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label49
            // 
            this.Label49.Enabled = false;
            this.Label49.Location = new System.Drawing.Point(805, 64);
            this.Label49.Name = "Label49";
            this.Label49.Size = new System.Drawing.Size(145, 16);
            this.Label49.TabIndex = 14;
            this.Label49.Text = "WHEN";
            this.Label49.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtDateLicenseValid);
            this.Frame5.Controls.Add(this.txtDateLicenseIssued);
            this.Frame5.Controls.Add(this.txtDateIntentionsFiled);
            this.Frame5.Controls.Add(this.txtCityofIssue);
            this.Frame5.Controls.Add(this.Label48);
            this.Frame5.Controls.Add(this.Label47);
            this.Frame5.Controls.Add(this.Label46);
            this.Frame5.Controls.Add(this.Label45);
            this.Frame5.Location = new System.Drawing.Point(20, 330);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(969, 140);
            this.Frame5.TabIndex = 2;
            this.Frame5.Text = "License To Marry";
            // 
            // txtDateLicenseValid
            // 
            this.txtDateLicenseValid.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateLicenseValid.Location = new System.Drawing.Point(562, 30);
            this.txtDateLicenseValid.Name = "txtDateLicenseValid";
            this.txtDateLicenseValid.Size = new System.Drawing.Size(190, 40);
            this.txtDateLicenseValid.TabIndex = 6;
            this.txtDateLicenseValid.Enter += new System.EventHandler(this.txtDateLicenseValid_Enter);
            this.txtDateLicenseValid.TextChanged += new System.EventHandler(this.txtDateLicenseValid_TextChanged);
            this.txtDateLicenseValid.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateLicenseValid_Validating);
            // 
            // txtDateLicenseIssued
            // 
            this.txtDateLicenseIssued.BackColor = System.Drawing.SystemColors.Info;
            this.txtDateLicenseIssued.Location = new System.Drawing.Point(190, 80);
            this.txtDateLicenseIssued.Name = "txtDateLicenseIssued";
            this.txtDateLicenseIssued.Size = new System.Drawing.Size(180, 40);
            this.txtDateLicenseIssued.TabIndex = 3;
            this.txtDateLicenseIssued.Tag = "Required";
            this.txtDateLicenseIssued.Enter += new System.EventHandler(this.txtDateLicenseIssued_Enter);
            this.txtDateLicenseIssued.TextChanged += new System.EventHandler(this.txtDateLicenseIssued_TextChanged);
            this.txtDateLicenseIssued.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateLicenseIssued_Validating);
            // 
            // txtDateIntentionsFiled
            // 
            this.txtDateIntentionsFiled.BackColor = System.Drawing.SystemColors.Info;
            this.txtDateIntentionsFiled.Location = new System.Drawing.Point(190, 30);
            this.txtDateIntentionsFiled.Name = "txtDateIntentionsFiled";
            this.txtDateIntentionsFiled.Size = new System.Drawing.Size(180, 40);
            this.txtDateIntentionsFiled.TabIndex = 2;
            this.txtDateIntentionsFiled.Tag = "Required";
            this.txtDateIntentionsFiled.Enter += new System.EventHandler(this.txtDateIntentionsFiled_Enter);
            this.txtDateIntentionsFiled.TextChanged += new System.EventHandler(this.txtDateIntentionsFiled_TextChanged);
            this.txtDateIntentionsFiled.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateIntentionsFiled_Validating);
            // 
            // txtCityofIssue
            // 
            this.txtCityofIssue.BackColor = System.Drawing.SystemColors.Window;
            this.txtCityofIssue.Location = new System.Drawing.Point(562, 80);
            this.txtCityofIssue.Name = "txtCityofIssue";
            this.txtCityofIssue.Size = new System.Drawing.Size(190, 40);
            this.txtCityofIssue.TabIndex = 7;
            this.txtCityofIssue.DoubleClick += new System.EventHandler(this.txtCityofIssue_DoubleClick);
            // 
            // Label48
            // 
            this.Label48.Location = new System.Drawing.Point(20, 44);
            this.Label48.Name = "Label48";
            this.Label48.Size = new System.Drawing.Size(140, 16);
            this.Label48.TabIndex = 8;
            this.Label48.Text = "DATE INTENTIONS FILED";
            // 
            // Label47
            // 
            this.Label47.Location = new System.Drawing.Point(20, 94);
            this.Label47.Name = "Label47";
            this.Label47.Size = new System.Drawing.Size(140, 16);
            this.Label47.TabIndex = 1;
            this.Label47.Text = "DATE LICENSE ISSUED";
            // 
            // Label46
            // 
            this.Label46.Location = new System.Drawing.Point(390, 44);
            this.Label46.Name = "Label46";
            this.Label46.Size = new System.Drawing.Size(140, 16);
            this.Label46.TabIndex = 4;
            this.Label46.Text = "LICENSE VALID UNTIL";
            // 
            // Label45
            // 
            this.Label45.Location = new System.Drawing.Point(390, 94);
            this.Label45.Name = "Label45";
            this.Label45.Size = new System.Drawing.Size(140, 16);
            this.Label45.TabIndex = 5;
            this.Label45.Text = "CITY OR TOWN OF ISSUE";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtActualDate);
            this.Frame3.Controls.Add(this.txtTitleOfPersonPerforming);
            this.Frame3.Controls.Add(this.txtDateOfCommision);
            this.Frame3.Controls.Add(this.txtCeremonyDate);
            this.Frame3.Controls.Add(this.txtCityMarried);
            this.Frame3.Controls.Add(this.txtCountyMarried);
            this.Frame3.Controls.Add(this.txtResidenceofPersonPerforming);
            this.Frame3.Controls.Add(this.txtMailingAddressofPersonPerforming);
            this.Frame3.Controls.Add(this.txtPersonPerformingCeremony);
            this.Frame3.Controls.Add(this.Label70);
            this.Frame3.Controls.Add(this.Label44);
            this.Frame3.Controls.Add(this.Label43);
            this.Frame3.Controls.Add(this.Label42);
            this.Frame3.Controls.Add(this.Label41);
            this.Frame3.Controls.Add(this.Label40);
            this.Frame3.Controls.Add(this.Label39);
            this.Frame3.Controls.Add(this.Label38);
            this.Frame3.Controls.Add(this.Label37);
            this.Frame3.Controls.Add(this.Label36);
            this.Frame3.Controls.Add(this.Label35);
            this.Frame3.Location = new System.Drawing.Point(20, 480);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(969, 340);
            this.Frame3.TabIndex = 3;
            this.Frame3.Text = "Ceremony";
            // 
            // txtActualDate
            // 
            this.txtActualDate.Location = new System.Drawing.Point(175, 80);
            this.txtActualDate.MaxLength = 10;
            this.txtActualDate.Name = "txtActualDate";
            this.txtActualDate.Size = new System.Drawing.Size(115, 22);
            this.txtActualDate.TabIndex = 3;
            // 
            // txtTitleOfPersonPerforming
            // 
            this.txtTitleOfPersonPerforming.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitleOfPersonPerforming.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
            this.txtTitleOfPersonPerforming.Location = new System.Drawing.Point(504, 130);
            this.txtTitleOfPersonPerforming.Name = "txtTitleOfPersonPerforming";
            this.txtTitleOfPersonPerforming.Size = new System.Drawing.Size(248, 40);
            this.txtTitleOfPersonPerforming.TabIndex = 13;
            // 
            // txtDateOfCommision
            // 
            this.txtDateOfCommision.BackColor = System.Drawing.SystemColors.Window;
            this.txtDateOfCommision.Location = new System.Drawing.Point(504, 230);
            this.txtDateOfCommision.Name = "txtDateOfCommision";
            this.txtDateOfCommision.Size = new System.Drawing.Size(249, 40);
            this.txtDateOfCommision.TabIndex = 17;
            this.txtDateOfCommision.Enter += new System.EventHandler(this.txtDateOfCommision_Enter);
            this.txtDateOfCommision.TextChanged += new System.EventHandler(this.txtDateOfCommision_TextChanged);
            this.txtDateOfCommision.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateOfCommision_Validating);
            // 
            // txtCeremonyDate
            // 
            this.txtCeremonyDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtCeremonyDate.Location = new System.Drawing.Point(175, 30);
            this.txtCeremonyDate.Name = "txtCeremonyDate";
            this.txtCeremonyDate.Size = new System.Drawing.Size(115, 40);
            this.txtCeremonyDate.TabIndex = 1;
            this.txtCeremonyDate.Enter += new System.EventHandler(this.txtCeremonyDate_Enter);
            this.txtCeremonyDate.TextChanged += new System.EventHandler(this.txtCeremonyDate_TextChanged);
            this.txtCeremonyDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtCeremonyDate_Validating);
            // 
            // txtCityMarried
            // 
            this.txtCityMarried.BackColor = System.Drawing.SystemColors.Window;
            this.txtCityMarried.Location = new System.Drawing.Point(20, 177);
            this.txtCityMarried.Name = "txtCityMarried";
            this.txtCityMarried.Size = new System.Drawing.Size(124, 40);
            this.txtCityMarried.TabIndex = 6;
            this.txtCityMarried.DoubleClick += new System.EventHandler(this.txtCityMarried_DoubleClick);
            // 
            // txtCountyMarried
            // 
            this.txtCountyMarried.BackColor = System.Drawing.SystemColors.Window;
            this.txtCountyMarried.Location = new System.Drawing.Point(164, 177);
            this.txtCountyMarried.Name = "txtCountyMarried";
            this.txtCountyMarried.Size = new System.Drawing.Size(124, 40);
            this.txtCountyMarried.TabIndex = 8;
            this.txtCountyMarried.DoubleClick += new System.EventHandler(this.txtCountyMarried_DoubleClick);
            // 
            // txtResidenceofPersonPerforming
            // 
            this.txtResidenceofPersonPerforming.BackColor = System.Drawing.SystemColors.Window;
            this.txtResidenceofPersonPerforming.Location = new System.Drawing.Point(504, 180);
            this.txtResidenceofPersonPerforming.Name = "txtResidenceofPersonPerforming";
            this.txtResidenceofPersonPerforming.Size = new System.Drawing.Size(249, 40);
            this.txtResidenceofPersonPerforming.TabIndex = 15;
            this.txtResidenceofPersonPerforming.DoubleClick += new System.EventHandler(this.txtResidenceofPersonPerforming_DoubleClick);
            // 
            // txtMailingAddressofPersonPerforming
            // 
            this.txtMailingAddressofPersonPerforming.BackColor = System.Drawing.SystemColors.Window;
            this.txtMailingAddressofPersonPerforming.Location = new System.Drawing.Point(504, 280);
            this.txtMailingAddressofPersonPerforming.Name = "txtMailingAddressofPersonPerforming";
            this.txtMailingAddressofPersonPerforming.Size = new System.Drawing.Size(249, 40);
            this.txtMailingAddressofPersonPerforming.TabIndex = 19;
            // 
            // txtPersonPerformingCeremony
            // 
            this.txtPersonPerformingCeremony.BackColor = System.Drawing.SystemColors.Window;
            this.txtPersonPerformingCeremony.Location = new System.Drawing.Point(504, 80);
            this.txtPersonPerformingCeremony.Name = "txtPersonPerformingCeremony";
            this.txtPersonPerformingCeremony.Size = new System.Drawing.Size(248, 40);
            this.txtPersonPerformingCeremony.TabIndex = 11;
            // 
            // Label70
            // 
            this.Label70.Location = new System.Drawing.Point(20, 94);
            this.Label70.Name = "Label70";
            this.Label70.Size = new System.Drawing.Size(82, 16);
            this.Label70.TabIndex = 2;
            this.Label70.Text = "ACTUAL DATE";
            // 
            // Label44
            // 
            this.Label44.Location = new System.Drawing.Point(20, 44);
            this.Label44.Name = "Label44";
            this.Label44.Size = new System.Drawing.Size(150, 16);
            this.Label44.TabIndex = 20;
            this.Label44.Text = "DATE ON CERTIFICATE";
            // 
            // Label43
            // 
            this.Label43.Location = new System.Drawing.Point(20, 130);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(110, 16);
            this.Label43.TabIndex = 4;
            this.Label43.Text = "WHERE MARRIED";
            // 
            // Label42
            // 
            this.Label42.Location = new System.Drawing.Point(20, 156);
            this.Label42.Name = "Label42";
            this.Label42.Size = new System.Drawing.Size(80, 16);
            this.Label42.TabIndex = 5;
            this.Label42.Text = "CITY / TOWN";
            // 
            // Label41
            // 
            this.Label41.Location = new System.Drawing.Point(164, 156);
            this.Label41.Name = "Label41";
            this.Label41.Size = new System.Drawing.Size(80, 16);
            this.Label41.TabIndex = 7;
            this.Label41.Text = "COUNTY";
            // 
            // Label40
            // 
            this.Label40.Location = new System.Drawing.Point(307, 44);
            this.Label40.Name = "Label40";
            this.Label40.Size = new System.Drawing.Size(220, 16);
            this.Label40.TabIndex = 9;
            this.Label40.Text = "PERSON PERFORMING CEREMONY";
            // 
            // Label39
            // 
            this.Label39.Location = new System.Drawing.Point(307, 94);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(91, 13);
            this.Label39.TabIndex = 10;
            this.Label39.Text = "FULL NAME";
            // 
            // Label38
            // 
            this.Label38.Location = new System.Drawing.Point(307, 144);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(64, 13);
            this.Label38.TabIndex = 12;
            this.Label38.Text = "TITLE";
            // 
            // Label37
            // 
            this.Label37.Location = new System.Drawing.Point(307, 194);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(170, 16);
            this.Label37.TabIndex = 14;
            this.Label37.Text = "CITY OF RESIDENCE";
            // 
            // Label36
            // 
            this.Label36.Location = new System.Drawing.Point(307, 294);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(106, 16);
            this.Label36.TabIndex = 18;
            this.Label36.Text = "MAILING ADDRESS";
            // 
            // Label35
            // 
            this.Label35.Location = new System.Drawing.Point(307, 244);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(170, 16);
            this.Label35.TabIndex = 16;
            this.Label35.Text = "DATE OF COMM OR LICENSE";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtNameofWitnessOne);
            this.Frame4.Controls.Add(this.txtNameofWitnessTwo);
            this.Frame4.Controls.Add(this.Label27);
            this.Frame4.Controls.Add(this.Label8);
            this.Frame4.Location = new System.Drawing.Point(20, 830);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(969, 114);
            this.Frame4.TabIndex = 4;
            this.Frame4.Text = "Witnesses";
            // 
            // txtNameofWitnessOne
            // 
            this.txtNameofWitnessOne.BackColor = System.Drawing.SystemColors.Window;
            this.txtNameofWitnessOne.Location = new System.Drawing.Point(20, 54);
            this.txtNameofWitnessOne.Name = "txtNameofWitnessOne";
            this.txtNameofWitnessOne.Size = new System.Drawing.Size(356, 40);
            this.txtNameofWitnessOne.TabIndex = 1;
            // 
            // txtNameofWitnessTwo
            // 
            this.txtNameofWitnessTwo.BackColor = System.Drawing.SystemColors.Window;
            this.txtNameofWitnessTwo.Location = new System.Drawing.Point(396, 54);
            this.txtNameofWitnessTwo.Name = "txtNameofWitnessTwo";
            this.txtNameofWitnessTwo.Size = new System.Drawing.Size(357, 40);
            this.txtNameofWitnessTwo.TabIndex = 3;
            // 
            // Label27
            // 
            this.Label27.Location = new System.Drawing.Point(20, 30);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(120, 14);
            this.Label27.TabIndex = 4;
            this.Label27.Text = "NAME OF WITNESS";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(396, 30);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(120, 14);
            this.Label8.TabIndex = 2;
            this.Label8.Text = "NAME OF WITNESS";
            // 
            // cmdFind
            // 
            this.cmdFind.Image = ((System.Drawing.Image)(resources.GetObject("cmdFind.Image")));
            this.cmdFind.Location = new System.Drawing.Point(0, 352);
            this.cmdFind.Name = "cmdFind";
            this.cmdFind.Size = new System.Drawing.Size(29, 23);
            this.cmdFind.TabIndex = 83;
            this.ToolTip1.SetToolTip(this.cmdFind, "Find Recording Clerk\'s Name");
            this.cmdFind.Visible = false;
            this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
            // 
            // txtNameOfClerk
            // 
            this.txtNameOfClerk.BackColor = System.Drawing.SystemColors.Window;
            this.txtNameOfClerk.Location = new System.Drawing.Point(435, 954);
            this.txtNameOfClerk.Name = "txtNameOfClerk";
            this.txtNameOfClerk.Size = new System.Drawing.Size(338, 40);
            this.txtNameOfClerk.TabIndex = 8;
            // 
            // Label60
            // 
            this.Label60.Location = new System.Drawing.Point(20, 968);
            this.Label60.Name = "Label60";
            this.Label60.Size = new System.Drawing.Size(70, 16);
            this.Label60.TabIndex = 5;
            this.Label60.Text = "DATE FILED";
            // 
            // Label59
            // 
            this.Label59.Location = new System.Drawing.Point(310, 968);
            this.Label59.Name = "Label59";
            this.Label59.Size = new System.Drawing.Size(112, 16);
            this.Label59.TabIndex = 7;
            this.Label59.Text = "NAME OF CLERK";
            // 
            // tabMarriages_Page3
            // 
            this.tabMarriages_Page3.Controls.Add(this.VSFlexGrid1);
            this.tabMarriages_Page3.Location = new System.Drawing.Point(1, 35);
            this.tabMarriages_Page3.Name = "tabMarriages_Page3";
            this.tabMarriages_Page3.Size = new System.Drawing.Size(914, 1024);
            this.tabMarriages_Page3.Text = "Amendment Information";
            // 
            // VSFlexGrid1
            // 
            this.VSFlexGrid1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.VSFlexGrid1.Cols = 6;
            this.VSFlexGrid1.ExtendLastCol = true;
            this.VSFlexGrid1.FixedCols = 0;
            this.VSFlexGrid1.Location = new System.Drawing.Point(20, 20);
            this.VSFlexGrid1.Name = "VSFlexGrid1";
            this.VSFlexGrid1.RowHeadersVisible = false;
            this.VSFlexGrid1.Rows = 17;
            this.VSFlexGrid1.Size = new System.Drawing.Size(874, 672);
            this.VSFlexGrid1.StandardTab = false;
            this.VSFlexGrid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.VSFlexGrid1.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.VSFlexGrid1_MouseMoveEvent);
            this.VSFlexGrid1.Enter += new System.EventHandler(this.VSFlexGrid1_Enter);
            this.VSFlexGrid1.KeyDown += new Wisej.Web.KeyEventHandler(this.VSFlexGrid1_KeyDownEvent);
            // 
            // mebVoided
            // 
            this.mebVoided.Location = new System.Drawing.Point(682, 104);
            this.mebVoided.MaxLength = 11;
            this.mebVoided.Name = "mebVoided";
            this.mebVoided.Size = new System.Drawing.Size(160, 22);
            this.mebVoided.TabIndex = 13;
            this.mebVoided.TabStop = false;
            this.mebVoided.Visible = false;
            // 
            // gridTownCode
            // 
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.Location = new System.Drawing.Point(30, 90);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.ReadOnly = false;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.Size = new System.Drawing.Size(265, 41);
            this.gridTownCode.TabIndex = 14;
            this.gridTownCode.Tag = "land";
            this.gridTownCode.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.gridTownCode_ChangeEdit);
            // 
            // imgDocuments
            // 
            this.imgDocuments.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDocuments.Image = ((System.Drawing.Image)(resources.GetObject("imgDocuments.Image")));
            this.imgDocuments.Location = new System.Drawing.Point(81, 30);
            this.imgDocuments.Name = "imgDocuments";
            this.imgDocuments.Size = new System.Drawing.Size(40, 40);
            this.imgDocuments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgDocuments, "One or more documents are attached to this record");
            this.imgDocuments.Click += new System.EventHandler(this.imgDocuments_Click);
            // 
            // ImgComment
            // 
            this.ImgComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.ImgComment.Image = ((System.Drawing.Image)(resources.GetObject("ImgComment.Image")));
            this.ImgComment.Location = new System.Drawing.Point(30, 30);
            this.ImgComment.Name = "ImgComment";
            this.ImgComment.Size = new System.Drawing.Size(40, 40);
            this.ImgComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // Label63
            // 
            this.Label63.Location = new System.Drawing.Point(317, 112);
            this.Label63.Name = "Label63";
            this.Label63.Size = new System.Drawing.Size(170, 17);
            this.Label63.TabIndex = 10;
            this.Label63.Text = "VERIFICATION OF FOREIGN";
            // 
            // Label62
            // 
            this.Label62.Location = new System.Drawing.Point(317, 75);
            this.Label62.Name = "Label62";
            this.Label62.Size = new System.Drawing.Size(170, 17);
            this.Label62.TabIndex = 6;
            this.Label62.Text = "BIRTH RECORD ON FILE";
            // 
            // Label30
            // 
            this.Label30.Location = new System.Drawing.Point(317, 38);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(170, 17);
            this.Label30.TabIndex = 2;
            this.Label30.Text = "DEATH RECORD ON FILE";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuStates,
            this.mnuRegistrarNames,
            this.mnuEditTown,
            this.mnuEditTitles,
            this.mnuViewDocs,
            this.mnuPrint,
            this.mnuPrintPreviewCertificate,
            this.mnuPrintIntentions,
            this.mnuPrintPreviewLicense});
            this.MainMenu1.Name = null;
            // 
            // mnuStates
            // 
            this.mnuStates.Index = 0;
            this.mnuStates.Name = "mnuStates";
            this.mnuStates.Text = "Add / Edit State Names";
            this.mnuStates.Visible = false;
            // 
            // mnuRegistrarNames
            // 
            this.mnuRegistrarNames.Index = 1;
            this.mnuRegistrarNames.Name = "mnuRegistrarNames";
            this.mnuRegistrarNames.Text = "Add / Edit Clerk Names";
            this.mnuRegistrarNames.Click += new System.EventHandler(this.mnuRegistrarNames_Click);
            // 
            // mnuEditTown
            // 
            this.mnuEditTown.Index = 2;
            this.mnuEditTown.Name = "mnuEditTown";
            this.mnuEditTown.Text = "Add / Edit Town Names";
            this.mnuEditTown.Click += new System.EventHandler(this.mnuEditTown_Click);
            // 
            // mnuEditTitles
            // 
            this.mnuEditTitles.Index = 3;
            this.mnuEditTitles.Name = "mnuEditTitles";
            this.mnuEditTitles.Text = "Add / Edit Titles";
            this.mnuEditTitles.Click += new System.EventHandler(this.mnuEditTitles_Click);
            // 
            // mnuViewDocs
            // 
            this.mnuViewDocs.Index = 4;
            this.mnuViewDocs.Name = "mnuViewDocs";
            this.mnuViewDocs.Text = "View Attached Documents";
            this.mnuViewDocs.Click += new System.EventHandler(this.mnuViewDocs_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Index = 5;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print Certificate";
            this.mnuPrint.Visible = false;
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintPreviewCertificate
            // 
            this.mnuPrintPreviewCertificate.Index = 6;
            this.mnuPrintPreviewCertificate.Name = "mnuPrintPreviewCertificate";
            this.mnuPrintPreviewCertificate.Text = "Print Certificate";
            this.mnuPrintPreviewCertificate.Click += new System.EventHandler(this.mnuPrintPreviewCertificate_Click);
            // 
            // mnuPrintIntentions
            // 
            this.mnuPrintIntentions.Index = 7;
            this.mnuPrintIntentions.Name = "mnuPrintIntentions";
            this.mnuPrintIntentions.Text = "Print Intentions";
            this.mnuPrintIntentions.Click += new System.EventHandler(this.mnuPrintIntentions_Click);
            // 
            // mnuPrintPreviewLicense
            // 
            this.mnuPrintPreviewLicense.Index = 8;
            this.mnuPrintPreviewLicense.Name = "mnuPrintPreviewLicense";
            this.mnuPrintPreviewLicense.Text = "Print License";
            this.mnuPrintPreviewLicense.Click += new System.EventHandler(this.mnuPrintPreviewLicense_Click);
            // 
            // mnuSP21
            // 
            this.mnuSP21.Index = -1;
            this.mnuSP21.Name = "mnuSP21";
            this.mnuSP21.Text = "-";
            // 
            // Separator1
            // 
            this.Separator1.Index = -1;
            this.Separator1.Name = "Separator1";
            this.Separator1.Text = "-";
            // 
            // mnuComment
            // 
            this.mnuComment.Index = -1;
            this.mnuComment.Name = "mnuComment";
            this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComment.Text = "Comments";
            this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                            ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExitNoValidation
            // 
            this.mnuSaveExitNoValidation.Index = -1;
            this.mnuSaveExitNoValidation.Name = "mnuSaveExitNoValidation";
            this.mnuSaveExitNoValidation.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuSaveExitNoValidation.Text = "Save & Exit (Without Validation)";
            this.mnuSaveExitNoValidation.Click += new System.EventHandler(this.mnuSaveExitNoValidation_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = -1;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Text = "New";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = -1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit                 ";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Separator2
            // 
            this.Separator2.Index = -1;
            this.Separator2.Name = "Separator2";
            this.Separator2.Text = "-";
            // 
            // mnuQuit
            // 
            this.mnuQuit.Index = -1;
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Text = "Exit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.BackColor = System.Drawing.Color.Transparent;
            this.lblFile.Location = new System.Drawing.Point(142, 44);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(48, 17);
            this.lblFile.TabIndex = 165;
            this.lblFile.Text = "FILE #";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAdd.Location = new System.Drawing.Point(927, 29);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(45, 24);
            this.cmdAdd.TabIndex = 1;
            this.cmdAdd.Text = "New";
            this.cmdAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(978, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdComments
            // 
            this.cmdComments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComments.Location = new System.Drawing.Point(1039, 29);
            this.cmdComments.Name = "cmdComments";
            this.cmdComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComments.Size = new System.Drawing.Size(80, 24);
            this.cmdComments.TabIndex = 3;
            this.cmdComments.Text = "Comments";
            this.cmdComments.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(385, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmMarriages
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmMarriages";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Marriages";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmMarriages_Load);
            this.Resize += new System.EventHandler(this.frmMarriages_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMarriages_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMarriages_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkVerificationOfForeignGroom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVerificationOfForeignBride)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeathOnFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBirthOnFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroomBirthOnFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroomDeathOnFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSignitureOnFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoided)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequired)).EndInit();
            this.tabMarriages.ResumeLayout(false);
            this.tabMarriages_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.tabMarriages_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            this.Frame6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBrideDomesticPartner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroomDomesticPartner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtActualDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdFind)).EndInit();
            this.tabMarriages_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VSFlexGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebVoided)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdAdd;
		private FCButton cmdComments;
		private FCButton cmdDelete;
		private FCButton cmdSave;
	}
}
