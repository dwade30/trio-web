﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmPrintMarriageConsent : BaseForm
	{
		public frmPrintMarriageConsent()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintMarriageConsent InstancePtr
		{
			get
			{
				return (frmPrintMarriageConsent)Sys.GetInstance(typeof(frmPrintMarriageConsent));
			}
		}

		protected frmPrintMarriageConsent _InstancePtr = null;
		//=========================================================
		// Private Sub chkIncarcerated_Click()
		// If chkIncarcerated = vbChecked Then
		// fraIncarcerated.Enabled = True
		// Else
		// chkIncarceratedBride = vbUnchecked
		// chkIncarceratedGroom = vbUnchecked
		// fraIncarcerated.Enabled = False
		// End If
		// End Sub
		// Private Sub chkWaiver_Click()
		// If chkWaiver = vbChecked Then
		// fraWaiver.Enabled = True
		// Else
		// chkWaiverBride = vbUnchecked
		// chkWaiverGroom = vbUnchecked
		// fraWaiver.Enabled = False
		// End If
		// End Sub
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// If chkIncarcerated = vbChecked And (chkIncarceratedBride = vbUnchecked And chkIncarceratedGroom = vbUnchecked) Then
			// MsgBox "You must select who was incarcerated before you may continue.", vbInformation, "Invalid Info"
			// Exit Sub
			// End If
			// If chkWaiver = vbChecked And (chkWaiverBride = vbUnchecked And chkWaiverGroom = vbUnchecked) Then
			// MsgBox "You must select who has a waiver before you may continue.", vbInformation, "Invalid Info"
			// Exit Sub
			// End If
			// If chkIncarcerated = vbChecked Then
			if (chkIncarceratedBride.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGNBas.Statics.typMarriageConsent.IncarceratedBride = true;
			}
			else
			{
				modGNBas.Statics.typMarriageConsent.IncarceratedBride = false;
			}
			if (chkIncarceratedGroom.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGNBas.Statics.typMarriageConsent.IncarceratedGroom = true;
			}
			else
			{
				modGNBas.Statics.typMarriageConsent.IncarceratedGroom = false;
			}
			// End If
			// If chkWaiver = vbChecked Then
			if (chkWaiverBride.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGNBas.Statics.typMarriageConsent.WaiverBride = true;
			}
			else
			{
				modGNBas.Statics.typMarriageConsent.WaiverBride = false;
			}
			if (chkWaiverGroom.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGNBas.Statics.typMarriageConsent.WaiverGroom = true;
			}
			else
			{
				modGNBas.Statics.typMarriageConsent.WaiverGroom = false;
			}
			// End If
			if (chkBrideHospitalized.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGNBas.Statics.typMarriageConsent.BrideHospitalized = true;
			}
			else
			{
				modGNBas.Statics.typMarriageConsent.BrideHospitalized = false;
			}
			if (chkGroomHospitalized.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGNBas.Statics.typMarriageConsent.GroomHospitalized = true;
			}
			else
			{
				modGNBas.Statics.typMarriageConsent.GroomHospitalized = false;
			}
			modGNBas.Statics.typMarriageConsent.ClerkName = txtClerk.Text;
			// .DateOfConsent = mebToDate.Text
			modGNBas.Statics.typMarriageConsent.DateCommissionExpires = mebToDate.Text;
			// .TownName = txtTown
			modGNBas.Statics.typMarriageConsent.CountyOfCommission = txtTown.Text;
			// rptMarriageConsent.Show 1
			frmReportViewer.InstancePtr.Init(rptMarriageConsentWordDoc.InstancePtr, showModal: this.Modal);
			//rptMarriageConsentWordDoc.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmPrintMarriageConsent_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGNBas.FormExist(this)))
				return;
			//App.DoEvents();
			//Support.ZOrder(this, 0);
		}

		private void frmPrintMarriageConsent_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmPrintMarriageConsent_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPrintMarriageConsent_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintMarriageConsent properties;
			//frmPrintMarriageConsent.ScaleWidth	= 5940;
			//frmPrintMarriageConsent.ScaleHeight	= 4245;
			//frmPrintMarriageConsent.LinkTopic	= "Form1";
			//frmPrintMarriageConsent.LockControls	= -1  'True;
			//frmPrintMarriageConsent.Moveable	= 0   'False;
			//End Unmaped Properties
			txtClerk.Text = modGNBas.Statics.typMarriageConsent.ClerkName;
			mebToDate.Text = modGNBas.Statics.typMarriageConsent.DateCommissionExpires;
			txtTown.Text = modGNBas.Statics.typMarriageConsent.CountyOfCommission;
			//txtNotes.Text = modGNBas.Statics.typMarriageConsent.Notes;
			modGlobalFunctions.SetFixedSize(this, 1);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}
	}
}
