//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public class modDogs
	{
		//=========================================================
		public struct DogFeeDefaults
		{
			public bool LateFeeOnServiceDog;
			// kk07302015 trocks-9
			// vbPorter upgrade warning: Late_Fee As Decimal	OnWrite(Decimal, string)	OnRead(double, int)
			public Decimal Late_Fee;
			// vbPorter upgrade warning: Warrant_Fee As Decimal	OnWrite(Decimal, string)	OnRead(double, int)
			public Decimal Warrant_Fee;
			// vbPorter upgrade warning: Replacement_Fee As Decimal	OnWrite(Decimal, string)	OnReadFCConvert.ToDouble(
			public Decimal Replacement_Fee;
			// vbPorter upgrade warning: Transfer_Fee As Decimal	OnWrite(Decimal, string)	OnReadFCConvert.ToDouble(
			public Decimal Transfer_Fee;
			// vbPorter upgrade warning: Fixed_Fee As Decimal	OnWriteFCConvert.ToDouble(	OnReadFCConvert.ToDouble(
			public Decimal Fixed_Fee;
			// vbPorter upgrade warning: UN_Fixed_FEE As Decimal	OnWrite(Decimal, double)	OnReadFCConvert.ToDouble(
			public Decimal UN_Fixed_FEE;
			// vbPorter upgrade warning: DogTagFee As Decimal	OnWrite(Decimal, string)	OnReadFCConvert.ToDouble(
			public Decimal DogTagFee;
			public double UserDefinedFee1;
			public string UserDefinedDescription1;
			// vbPorter upgrade warning: DogState As double	OnRead(double, int)
			public double DogState;
			// vbPorter upgrade warning: DogTown As double	OnRead(double, int)
			public double DogTown;
			// vbPorter upgrade warning: DogClerk As double	OnRead(double, int)
			public double DogClerk;
			// vbPorter upgrade warning: FixedState As double	OnRead(double, int)
			public double FixedState;
			// vbPorter upgrade warning: FixedTown As double	OnRead(double, int)
			public double FixedTown;
			// vbPorter upgrade warning: FixedClerk As double	OnRead(double, int)
			public double FixedClerk;
			public double KennelState;
			public double KennelTown;
			public double KennelClerk;
			public double KennelLateFee;
			public double KennelWarrantFee;
            public double DangerousStateFee;
            public double DangerousTownFee;
            public double DangerousClerkFee;
            public double NuisanceStateFee;
            public double NuisanceTownFee;
            public double NuisanceClerkFee;
            public double DangerousLateFee;
            public double NuisanceLateFee;
        };

		public struct ClerkFees
		{
			public Decimal NewBirth;
			public Decimal ReplacementBirth;
			public Decimal BirthStateFee;
			public Decimal BirthReplacementStateFee;
			public Decimal NewDeath;
			public Decimal ReplacementDeath;
			public Decimal DeathStateFee;
			public Decimal DeathReplacementStateFee;
			public Decimal DeathVetStateFee;
			public Decimal BurialPermit;
			public Decimal BurialStateFee;
			public Decimal BurialReplacementStateFee;
			public Decimal NewMarriageCert;
			public Decimal ReplacementMarriageCert;
			public Decimal MarriageCertStateFee;
			public Decimal ReplacementMarriageCertStateFee;
			public Decimal NewMarriageLicense;
			public Decimal ReplacementMarriageLicense;
			public Decimal MarriageLicenseStateFee;
			public Decimal MarriageLicenseReplacementStateFee;
			public Decimal AdditionalFee1;
			public Decimal AdditionalFee2;
			public Decimal AdditionalFee3;
			public Decimal AdditionalFee4;
			public Decimal AdditionalFee5;
		};

		public static void GetClerkFees()
		{
			try
			{
				// On Error GoTo Handler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsFees = new clsDRWrapper();
				rsFees.OpenRecordset("SELECT * FROM ClerkFees;", modGNBas.DEFAULTDATABASE);
				if (rsFees.EndOfFile())
					return;
				Statics.typClerkFees.NewBirth = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("NewBirthFee") + " "));
				Statics.typClerkFees.ReplacementBirth = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("ReplacementBirthFee") + " "));
				Statics.typClerkFees.NewDeath = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("NewDeathFee") + " "));
				Statics.typClerkFees.ReplacementDeath = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("ReplacementDeathFee") + " "));
				Statics.typClerkFees.BurialPermit = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("BurialPermitFee") + " "));
				Statics.typClerkFees.NewMarriageCert = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("NewMarriageCertificateFee") + " "));
				Statics.typClerkFees.ReplacementMarriageCert = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("ReplacementMarriageCertificateFee") + " "));
				Statics.typClerkFees.NewMarriageLicense = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("NewMarriageLicenseFee") + " "));
				Statics.typClerkFees.ReplacementMarriageLicense = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("ReplacementMarriageLicenseFee") + " "));
				Statics.typClerkFees.BirthStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("BirthStateFee")));
				Statics.typClerkFees.BirthReplacementStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("BirthReplacementStateFee")));
				Statics.typClerkFees.DeathStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("DeathStateFee")));
				Statics.typClerkFees.DeathReplacementStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("DeathReplacementStateFee")));
				Statics.typClerkFees.BurialStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("dispositionStateFee")));
				Statics.typClerkFees.MarriageCertStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("MarriageCertStateFee")));
				Statics.typClerkFees.ReplacementMarriageCertStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("MarriageCertReplacementStateFee")));
				Statics.typClerkFees.MarriageLicenseStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("MarriageLicStateFee")));
				Statics.typClerkFees.MarriageLicenseReplacementStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("MarriageLicReplacementStateFee")));
				Statics.typClerkFees.DeathVetStateFee = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("VetDeathStateFee")));
				Statics.typClerkFees.AdditionalFee1 = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("AdditionalFee1") + " "));
				Statics.typClerkFees.AdditionalFee2 = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("AdditionalFee2") + " "));
				Statics.typClerkFees.AdditionalFee3 = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("AdditionalFee3") + " "));
				Statics.typClerkFees.AdditionalFee4 = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("AdditionalFee4") + " "));
				Statics.typClerkFees.AdditionalFee5 = FCConvert.ToDecimal(Conversion.Val(rsFees.Get_Fields_Double("AdditionalFee5") + " "));
				return;
			}
			catch (Exception ex)
			{
				// Handler:
				MessageBox.Show("Error in GetClerkFees: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				{
					/*? Resume Next; */}
			}
		}

		public static void GetDogFees()
		{
			try
			{
				// On Error GoTo Handler
				fecherFoundation.Information.Err().Clear();
                TWSharedLibrary.StaticSettings.GlobalTelemetryService.TrackTrace("modDogs.GetDogFees - start");
				Statics.rsDogs.OpenRecordset("SELECT * FROM SettingsFees;", modGNBas.DEFAULTDATABASE);
				if (Statics.rsDogs.EndOfFile())
					return;
				Statics.DogFee.LateFeeOnServiceDog = FCConvert.CBool(Statics.rsDogs.Get_Fields_Boolean("LateFeeOnServiceDog"));
				Statics.DogFee.Late_Fee = FCConvert.ToDecimal(Conversion.Val(Statics.rsDogs.Get_Fields_Decimal("Late_Dog") + " "));
				Statics.DogFee.Warrant_Fee = FCConvert.ToDecimal(Conversion.Val(Statics.rsDogs.Get_Fields_Decimal("Warrant_Dog") + " "));
				Statics.DogFee.Replacement_Fee = FCConvert.ToDecimal(Conversion.Val(Statics.rsDogs.Get_Fields_Decimal("Replacement_Dog") + " "));
				Statics.DogFee.Transfer_Fee = FCConvert.ToDecimal(Conversion.Val(Statics.rsDogs.Get_Fields_Decimal("Transfer_Dog") + " "));
				Statics.DogFee.UN_Fixed_FEE = FCConvert.ToDecimal(Conversion.Val(Statics.rsDogs.Get_Fields_Decimal("UnFixed_Dog") + " "));
				Statics.DogFee.DogTagFee = FCConvert.ToDecimal(Conversion.Val(Statics.rsDogs.Get_Fields_Decimal("DogTagFee") + " "));
				Statics.DogFee.UserDefinedDescription1 = Statics.rsDogs.Get_Fields_String("userdefineddescription1") + "";
				Statics.DogFee.UserDefinedFee1 = Conversion.Val(Statics.rsDogs.Get_Fields_Double("userdefinedfee1") + "");
				Statics.DogFee.DogClerk = Conversion.Val(Statics.rsDogs.Get_Fields_Double("dogclerk"));
				Statics.DogFee.DogState = Conversion.Val(Statics.rsDogs.Get_Fields_Double("dogstate"));
				Statics.DogFee.DogTown = Conversion.Val(Statics.rsDogs.Get_Fields_Double("dogtown"));
				Statics.DogFee.FixedClerk = Conversion.Val(Statics.rsDogs.Get_Fields_Double("fixedclerk"));
				Statics.DogFee.FixedState = Conversion.Val(Statics.rsDogs.Get_Fields_Double("fixedstate"));
				Statics.DogFee.FixedTown = Conversion.Val(Statics.rsDogs.Get_Fields_Double("fixedtown"));
				Statics.DogFee.KennelClerk = Conversion.Val(Statics.rsDogs.Get_Fields_Double("kennelclerk"));
				Statics.DogFee.KennelLateFee = Conversion.Val(Statics.rsDogs.Get_Fields_Double("kennellatefee"));
				Statics.DogFee.KennelState = Conversion.Val(Statics.rsDogs.Get_Fields_Double("kennelstate"));
				Statics.DogFee.KennelTown = Conversion.Val(Statics.rsDogs.Get_Fields_Double("kenneltown"));
				Statics.DogFee.KennelWarrantFee = Conversion.Val(Statics.rsDogs.Get_Fields_Double("kennelwarrantfee"));
				Statics.DogFee.Fixed_Fee = FCConvert.ToDecimal(Statics.DogFee.FixedClerk + Statics.DogFee.FixedState + Statics.DogFee.FixedTown);
				Statics.DogFee.UN_Fixed_FEE = FCConvert.ToDecimal(Statics.DogFee.DogClerk + Statics.DogFee.DogState + Statics.DogFee.DogTown);
                Statics.DogFee.DangerousClerkFee = Statics.rsDogs.Get_Fields_Double("DangerousClerkFee");
                Statics.DogFee.DangerousLateFee = Statics.rsDogs.Get_Fields_Double("DangerousLateFee");
                Statics.DogFee.DangerousStateFee = Statics.rsDogs.Get_Fields_Double("DangerousStateFee");
                Statics.DogFee.DangerousTownFee = Statics.rsDogs.Get_Fields_Double("DangerousTownFee");
                Statics.DogFee.NuisanceLateFee = Statics.rsDogs.Get_Fields_Double("NuisanceLateFee");
                Statics.DogFee.NuisanceClerkFee = Statics.rsDogs.Get_Fields_Double("NuisanceClerkFee");
                Statics.DogFee.NuisanceStateFee = Statics.rsDogs.Get_Fields_Double("NuisanceStateFee");
                Statics.DogFee.NuisanceTownFee = Statics.rsDogs.Get_Fields_Double("NuisanceTownFee");
                TWSharedLibrary.StaticSettings.GlobalTelemetryService.TrackTrace("modDogs.GetDogFees - end");
                return;
			}
			catch (Exception ex)
			{
				// Handler:
                TWSharedLibrary.StaticSettings.GlobalTelemetryService.TrackException(ex);

				MessageBox.Show("Error in GetDogFees: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				{
					/*? Resume Next; */}
			}
		}

		public static void SetDogFees()
		{
			try
			{
				// On Error GoTo Handler
				fecherFoundation.Information.Err().Clear();
				Statics.rsDogs.OpenRecordset("SELECT * FROM SettingsFees", modGNBas.DEFAULTDATABASE);
				if (Statics.rsDogs.EndOfFile())
				{
					Statics.rsDogs.AddNew();
				}
				else
				{
					Statics.rsDogs.Edit();
				}
				Statics.rsDogs.Set_Fields("LateFeeOnServiceDog", Statics.DogFee.LateFeeOnServiceDog);
				// kk07302015 trocks-9
				Statics.rsDogs.Set_Fields("Late_Dog", Statics.DogFee.Late_Fee);
				Statics.rsDogs.Set_Fields("Warrant_Dog", Statics.DogFee.Warrant_Fee);
				Statics.rsDogs.Set_Fields("Replacement_Dog", Statics.DogFee.Replacement_Fee);
				Statics.rsDogs.Set_Fields("Transfer_Dog", Statics.DogFee.Transfer_Fee);
				Statics.rsDogs.Set_Fields("Fixed_Dog", Statics.DogFee.Fixed_Fee);
				Statics.rsDogs.Set_Fields("UnFixed_Dog", Statics.DogFee.UN_Fixed_FEE);
				Statics.rsDogs.Set_Fields("DogTagFee", Statics.DogFee.DogTagFee);
				Statics.rsDogs.Set_Fields("UserDefinedFee1", Statics.DogFee.UserDefinedFee1);
				Statics.rsDogs.Set_Fields("userdefineddescription1", Statics.DogFee.UserDefinedDescription1);
				Statics.rsDogs.Set_Fields("dogclerk", Statics.DogFee.DogClerk);
				Statics.rsDogs.Set_Fields("dogstate", Statics.DogFee.DogState);
				Statics.rsDogs.Set_Fields("dogtown", Statics.DogFee.DogTown);
				Statics.rsDogs.Set_Fields("fixedclerk", Statics.DogFee.FixedClerk);
				Statics.rsDogs.Set_Fields("fixedstate", Statics.DogFee.FixedState);
				Statics.rsDogs.Set_Fields("fixedtown", Statics.DogFee.FixedTown);
				Statics.rsDogs.Set_Fields("kennelclerk", Statics.DogFee.KennelClerk);
				Statics.rsDogs.Set_Fields("kennellatefee", Statics.DogFee.KennelLateFee);
				Statics.rsDogs.Set_Fields("kennelstate", Statics.DogFee.KennelState);
				Statics.rsDogs.Set_Fields("kenneltown", Statics.DogFee.KennelTown);
				Statics.rsDogs.Set_Fields("kennelwarrantfee", Statics.DogFee.KennelWarrantFee);
                Statics.rsDogs.Set_Fields("DangerousClerkFee",Statics.DogFee.DangerousClerkFee);
                Statics.rsDogs.Set_Fields("DangerousLateFee", Statics.DogFee.DangerousLateFee);
                Statics.rsDogs.Set_Fields("DangerousStateFee",Statics.DogFee.DangerousStateFee);
                Statics.rsDogs.Set_Fields("DangerousTownFee", Statics.DogFee.DangerousTownFee);
                Statics.rsDogs.Set_Fields("NuisanceClerkFee",Statics.DogFee.NuisanceClerkFee);
                Statics.rsDogs.Set_Fields("NuisanceLateFee", Statics.DogFee.NuisanceLateFee);
                Statics.rsDogs.Set_Fields("NuisanceStateFee", Statics.DogFee.NuisanceStateFee);
                Statics.rsDogs.Set_Fields("NuisanceTownFee", Statics.DogFee.NuisanceTownFee);
                Statics.rsDogs.Update();
				MessageBox.Show("Save Completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// Handler:
				MessageBox.Show("Error in SetDogFees Update: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description);
				{
					/*? Resume Next; */}
			}
		}

		public static void dogArchiveRec(ref string transA)
		{
			Statics.rsDogs.OpenRecordset("SELECT * FROM DogArchive", modGNBas.DEFAULTDATABASE);
			Statics.rsDogs.AddNew();
			Statics.rsDogs.Set_Fields("DogNumber", modClerkGeneral.Statics.dInfo.DogNumber);
			Statics.rsDogs.Set_Fields("CurrentONum", modClerkGeneral.Statics.dOwner.NewONum);
			Statics.rsDogs.Set_Fields("OldONum", modClerkGeneral.Statics.dOwner.OwnerNum);
			Statics.rsDogs.Set_Fields("ThisRecDate", Strings.Format(DateTime.Now, "Short Date"));
			Statics.rsDogs.Set_Fields("transaction", transA);
			Statics.rsDogs.Update();
		}

		public static void addOtoD(ref int OwnerNum, ref int DogNum)
		{
			Statics.rsDogs.OpenRecordset("SELECT ownerNum FROM DogInfo WHERE ID = " + FCConvert.ToString(DogNum), modGNBas.DEFAULTDATABASE);
			if (!Statics.rsDogs.EndOfFile())
			{
				Statics.rsDogs.Edit();
				Statics.rsDogs.Set_Fields("OwnerNum", OwnerNum);
				Statics.rsDogs.Update();
			}
		}

		public static void addDogNumsToOwnerT(ref string DogNums, ref int OwnerNum)
		{
			Statics.rsDogs.OpenRecordset("SELECT DogNumbers FROM dogowner where ID = " + FCConvert.ToString(OwnerNum), modGNBas.DEFAULTDATABASE);
			if (!Statics.rsDogs.EndOfFile())
			{
				Statics.rsDogs.Edit();
				Statics.rsDogs.Set_Fields("DogNumbers", DogNums);
				Statics.rsDogs.Update();
			}
		}

		public static string DOGNAME(ref int DogNum)
		{
			string DOGNAME = "";
			Statics.rsDogs.OpenRecordset("SELECT DogName, RabiesTagNo FROM DogInfo WHERE ID = " + FCConvert.ToString(DogNum), modGNBas.DEFAULTDATABASE);
			if (Statics.rsDogs.EndOfFile())
			{
			}
			else
			{
				if (FCConvert.ToString(Statics.rsDogs.Get_Fields_String("RabiesTagNo")).Length > 7)
				{
					DOGNAME = "TAG NO: " + Strings.Left(FCConvert.ToString(Statics.rsDogs.Get_Fields_String("RabiesTagNo")), 6) + " " + Statics.rsDogs.Get_Fields_String("DogName");
				}
				else
				{
					DOGNAME = "TAG NO: " + Statics.rsDogs.Get_Fields_String("RabiesTagNo") + Strings.StrDup(7 - FCConvert.ToString(Statics.rsDogs.Get_Fields_String("RabiesTagNo")).Length, " ") + Statics.rsDogs.Get_Fields_String("DogName");
				}
			}
			return DOGNAME;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public static object GetDogNumbers(ref int Onum)
		{
			object GetDogNumbers = null;
			Statics.rsDogs.OpenRecordset("SELECT DogNumbers FROM dogowner where ID = " + FCConvert.ToString(Onum), modGNBas.DEFAULTDATABASE);
			if (Statics.rsDogs.EndOfFile())
			{
				GetDogNumbers = "";
			}
			else if (fecherFoundation.FCUtils.IsNull(Statics.rsDogs.Get_Fields_String("DogNumbers")))
			{
				GetDogNumbers = "";
			}
			else
			{
				GetDogNumbers = Statics.rsDogs.Get_Fields_String("DogNumbers");
			}
			return GetDogNumbers;
		}

		public static void RemoveDog(ref string DogNumbersField, ref string DogNumber, ref int Onum)
		{
			// **** returns the modified DogNumbersField
			try
			{
				// On Error GoTo Handler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: temp As object	OnWrite(string())
				string[] temp;
				// vbPorter upgrade warning: I As int	OnWriteFCConvert.ToInt32(
				int I;
				string RDog = "";
				temp = Strings.Split(DogNumbersField, ",", -1, CompareConstants.vbBinaryCompare);
				for (I = 0; I <= (Information.UBound(temp, 1)); I++)
				{
					if (FCConvert.ToString(temp[I]) == DogNumber || FCConvert.ToString(temp[I]) == string.Empty)
					{
					}
					else
					{
						RDog += temp[I] + ",";
					}
				}
				// I
				if (RDog != "")
					RDog = Strings.Left(RDog, RDog.Length - 1);
				modClerkGeneral.Statics.dOwner.DogNumbers = RDog;
				Statics.rsDogs.OpenRecordset("SELECT DogNumbers FROM dogowner where ID = " + FCConvert.ToString(Onum), modGNBas.DEFAULTDATABASE);
				Statics.rsDogs.Edit();
				Statics.rsDogs.Set_Fields("DogNumbers", RDog);
				Statics.rsDogs.Update();
				return;
			}
			catch (Exception ex)
			{
				// Handler:
				MessageBox.Show("Unable to update:  " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description);
				{
					/*? Resume Next; */}
			}
		}

		public static string editDogNum(ref string DogNumbersField, ref string DogNumber, ref bool addDog)
		{
			string editDogNum = "";
			// **** returns the modified DogNumbersField
			// **** if addDog is false then REMOVING A DOG is true
			// ****
			// vbPorter upgrade warning: temp As object	OnWrite(string())
			string[] temp;
			// vbPorter upgrade warning: I As int	OnWriteFCConvert.ToInt32(
			int I;
			string tempDogField = "";
			temp = Strings.Split(DogNumbersField, ",", -1, CompareConstants.vbBinaryCompare);
			if (addDog == false)
			{
				// **** CHECK FOR THE DOG TO BE REMOVED OR A NULL STRING
				// **** ELSE PUT THE DOG IN THE DOG LIST
				for (I = 0; I <= (Information.UBound(temp, 1)); I++)
				{
					if ((FCConvert.ToString(temp[I]) == DogNumber) || (FCConvert.ToString(temp[I]) == string.Empty))
					{
					}
					else
					{
						tempDogField += temp[I] + ",";
					}
				}
				// I
				tempDogField = Strings.Left(tempDogField, tempDogField.Length - 1);
			}
			else if (addDog == true)
			{
				if (DogNumbersField != "")
				{
					tempDogField = DogNumbersField + "," + DogNumber;
				}
				else
				{
					tempDogField = DogNumber;
				}
			}
			editDogNum = tempDogField;
			return editDogNum;
		}

		public class StaticVariables
		{
			public DogFeeDefaults DogFee = new DogFeeDefaults();
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsDogs = new clsDRWrapper();
			public clsDRWrapper rsDogs_AutoInitialized = null;
			public clsDRWrapper rsDogs
			{
				get
				{
					if ( rsDogs_AutoInitialized == null)
					{
						 rsDogs_AutoInitialized = new clsDRWrapper();
					}
					return rsDogs_AutoInitialized;
				}
				set
				{
					 rsDogs_AutoInitialized = value;
				}
			}
			public ClerkFees typClerkFees = new ClerkFees();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
