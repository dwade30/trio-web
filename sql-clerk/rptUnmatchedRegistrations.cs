﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptUnmatchedRegistrations.
	/// </summary>
	public partial class rptUnmatchedRegistrations : BaseSectionReport
	{
		public rptUnmatchedRegistrations()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Unmatched Dog Registrations";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptUnmatchedRegistrations InstancePtr
		{
			get
			{
				return (rptUnmatchedRegistrations)Sys.GetInstance(typeof(rptUnmatchedRegistrations));
			}
		}

		protected rptUnmatchedRegistrations _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUnmatchedRegistrations	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngRow;
		const int CNSTGRIDCOLOWNERID = 0;
		const int CNSTGRIDCOLDOGID = 1;
		const int CNSTGRIDCOLDOGNAME = 2;
		const int CNSTGRIDCOLTRANSACTIONDATE = 3;
		const int CNSTGRIDCOLTRANSTYPE = 4;
		const int CNSTGRIDCOLTRANSFEE = 5;
		const int CNSTGRIDCOLOWNERFIRST = 6;
		const int CNSTGRIDCOLOWNERMIDDLE = 7;
		const int CNSTGRIDCOLOWNERLAST = 8;
		const int CNSTGRIDCOLBREED = 9;
		const int CNSTGRIDCOLSEX = 10;
		const int CNSTGRIDCOLCOLOR = 11;
		const int CNSTGRIDCOLNEUTER = 12;
		const int CNSTGRIDCOLVET = 13;
		const int CNSTGRIDCOLRABIESCERT = 14;
		const int CNSTGRIDCOLTAG = 15;
		const int CNSTGRIDCOLADDRESS1 = 16;
		const int CNSTGRIDCOLADDRESS2 = 17;
		const int CNSTGRIDCOLCITY = 18;
		const int CNSTGRIDCOLSTATE = 19;
		const int CNSTGRIDCOLZIP = 20;
		const int CNSTGRIDCOLLOCATION = 21;
		const int CNSTGRIDCOLPHONE = 22;
		const int CNSTGRIDCOLEMAIL = 23;
		const int CNSTGRIDCOLRABIESISSUEDATE = 24;
		const int CNSTGRIDCOLDOGDOB = 25;
		const int CNSTGRIDCOLNEUTERCERT = 26;
		const int CNSTGRIDCOLREASON = 27;

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "UnmatchedRegistrations");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngRow >= frmOnlineDogs.InstancePtr.GridNew.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:dd tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngRow = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow < frmOnlineDogs.InstancePtr.GridNew.Rows)
			{
				txtOwner.Text = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERFIRST) + " " + frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERMIDDLE)) + " " + frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLOWNERLAST));
				txtDog.Text = frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME);
				txtAddress.Text = frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1);
				txtCityStateZip.Text = frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLCITY) + " " + frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLSTATE) + "  " + frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLZIP);
				txtTransactionDate.Text = frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSACTIONDATE);
				txtFee.Text = frmOnlineDogs.InstancePtr.GridNew.TextMatrix(lngRow, CNSTGRIDCOLTRANSFEE);
			}
			lngRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

	
	}
}
