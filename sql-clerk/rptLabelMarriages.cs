//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptLabelMarriages.
	/// </summary>
	public partial class rptLabelMarriages : BaseSectionReport
	{
		public rptLabelMarriages()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Marriage Certificate ";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLabelMarriages InstancePtr
		{
			get
			{
				return (rptLabelMarriages)Sys.GetInstance(typeof(rptLabelMarriages));
			}
		}

		protected rptLabelMarriages _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLabelMarriages	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolPrinted;
		private int intPrinted;
		private bool boolIsR402;
		private bool boolPrintTest;
		private double dblLineAdjust;

		private void ActiveReport_Initialize()
		{
			boolPrinted = false;
			intPrinted = 0;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			boolPrinted = true;
			//intPrinted = this.Document.Printer.DeviceCopies;
		}

		public void Init(bool boolR402, bool boolTestPrint = false, double dblAdjust = 0)
		{
			boolPrintTest = boolTestPrint;
			if (boolPrintTest)
			{
				dblLineAdjust = dblAdjust;
			}
			boolIsR402 = boolR402;
            //FC:FINAL:IPI - #i1705 - show the report viewer on web
            //this.Show(FCForm.FormShowEnum.Modal);
            frmReportViewer.InstancePtr.Init(this, showModal: true);
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Dim rsData As New clsDRWrapper
			double dblLaserLineAdjustment15;
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			// Call SetPrintProperties(Me)
			//Application.DoEvents();
			if (boolIsR402)
			{
				lblTown.Visible = false;
				lblAttest.Visible = false;
				lblIssued.Visible = false;
			}
			else
			{
				lblTown.Visible = true;
				lblAttest.Visible = true;
				lblIssued.Visible = true;
			}

			dblLaserLineAdjustment15 = Conversion.Val(modRegistry.GetRegistryKey("MarriageAdjustment", "CK"));
			if (boolPrintTest)
			{
				dblLaserLineAdjustment15 = dblLineAdjust;
			}
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += FCConvert.ToSingle(240 * dblLaserLineAdjustment15) / 1440F;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// If gboolFromDosTrio = True Or bolFromWindowsCR Then
			if (boolPrinted)
			{
				modCashReceiptingData.Statics.typeCRData.NumFirsts = 1;
				modCashReceiptingData.Statics.typeCRData.NumSubsequents = (intPrinted - 1);

			}
			// End If
			if (!boolPrintTest)
			{
				if (modGNBas.Statics.typMarriageConsent.NeedConsent)
				{
					if (Information.IsDate(modGNBas.Statics.typMarriageConsent.MarriageDate))
					{
						if (fecherFoundation.DateAndTime.DateValue(modGNBas.Statics.typMarriageConsent.MarriageDate).ToOADate() >= DateTime.Today.ToOADate())
						{
							frmPrintMarriageConsent.InstancePtr.Show(FCForm.FormShowEnum.Modal);
						}
					}
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			/*? On Error Resume Next  */
			if (!boolPrintTest)
			{
				txtF1.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsFirstName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsMiddleName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsLastName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsDesignation");
				txtF2.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesFirstName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesMiddleName") + " " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCurrentLastName");
				// changed on 8/13 per request from Deb2 on call id 32713
				// & " " & rsMarriageCertificate.Fields("BridesMaidenSurname")
				txtF3.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsCity") + ", " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsState");
				txtF4.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesCity") + ", " + modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesState");
				txtF5.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int16("GroomsAge"));
				txtF6.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("GroomsBirthplace");
				txtF7.Text = FCConvert.ToString(modGNBas.Statics.rsMarriageCertificate.Get_Fields_Int16("BridesAge"));
				txtF8.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("BridesBirthplace");
				txtF9.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("DateIntentionsFiled");
				txtF10.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("CeremonyDate");
				txtF11.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("PersonPerformingCeremony");
				txtF12.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("TitleofPersonPerforming");
				txtF13.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("CityMarried");
				txtF14.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("NameOfClerk");
				txtF15.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("CityOfIssue");
				txtF16.Text = modGNBas.Statics.rsMarriageCertificate.Get_Fields_String("DateClerkFiled");
				txtF17.Text = Strings.Format(DateTime.Now, "MMMM dd, yyyy");
				if (modGNBas.Statics.ClerkDefaults.boolDontPrintAttested)
				{
					txtF18.Text = "";
				}
				else
				{
					txtF18.Text = modClerkGeneral.Statics.utPrintInfo.AttestedBy;
				}
				txtF19.Text = fecherFoundation.Strings.StrConv(modGlobalConstants.Statics.MuniName, VbStrConv.ProperCase);
				// lblAttest.Visible = False
				// lblIssued.Visible = False
				// lblTown.Visible = False
			}
			else
			{
				txtF1.Text = "Groom's Name";
				txtF2.Text = "Bride's Name";
				txtF3.Text = "Groom's City";
				txtF4.Text = "Bride's City";
				txtF5.Text = "99";
				txtF6.Text = "Groom's Birth Place";
				txtF7.Text = "99";
				txtF8.Text = "Bride's Birth Place";
				txtF9.Text = "00/00/0000";
				txtF10.Text = "00/00/0000";
				txtF11.Text = "Person Performing Ceremony";
				txtF12.Text = "Person's Title";
				txtF13.Text = "City";
				txtF14.Text = "Clerk";
				txtF15.Text = "City";
				txtF16.Text = "00/00/0000";
				txtF17.Text = Strings.Format(DateTime.Now, "MMMM dd, yyyy");
				txtF18.Text = "Attested By";
				txtF19.Text = modGlobalConstants.Statics.MuniName;
			}
		}

		private void rptLabelMarriages_Load(object sender, System.EventArgs e)
		{

		}
	}
}
