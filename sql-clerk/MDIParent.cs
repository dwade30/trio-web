//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Births;
using SharedApplication.Clerk.Burials;
using SharedApplication.Clerk.Deaths;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Marriages;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using TWSharedLibrary;

namespace TWCK0000
{   
    public class MDIParent //: BaseForm
	{
        public FCCommonDialog CommonDialog1;
        public Wisej.Web.ImageList ImageList2;
        private CommandDispatcher commandDispatcher;
        public MDIParent()
        {
            //
            // required for windows form designer support
            //
            //InitializeComponent();
            InitializeComponentEx();
        }

        public MDIParent(CommandDispatcher commandDispatcher) 
        {
            this.commandDispatcher = commandDispatcher;
            InitializeComponentEx();
        }

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.CommonDialog1.Color = System.Drawing.Color.Black;
            this.CommonDialog1.DefaultExt = null;
            this.CommonDialog1.FilterIndex = ((short)(0));
            this.CommonDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CommonDialog1.FontName = "Microsoft Sans Serif";
            this.CommonDialog1.FontSize = 8.25F;
            this.CommonDialog1.ForeColor = System.Drawing.Color.Black;
            this.CommonDialog1.FromPage = 0;
            this.CommonDialog1.Location = new System.Drawing.Point(0, 0);
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.PrinterSettings = null;
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            this.CommonDialog1.TabIndex = 0;
            this.CommonDialog1.ToPage = 0;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent));
            this.ImageList2 = new Wisej.Web.ImageList();
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList2.Images"))));
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList2.Images1"))));
            this.ImageList2.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1,
            imageListEntry2});
            this.ImageList2.ImageSize = new System.Drawing.Size(32, 32);
            this.ImageList2.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);

        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;
		//=========================================================
		private int[] LabelNumber = new int[200 + 1];
		private int intExit;
		const string strTrio = "TRIO Software - Clerk ";

		public void Init(bool addExitMenu, CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
			modGlobalFunctions.LoadTRIOColors();
			App.MainForm.NavigationMenu.Owner = this;
			App.MainForm.menuTree.ImageList = CreateImageList();
			string assemblyName = this.GetType().Assembly.GetName().Name;
			if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
			{
				App.MainForm.ApplicationIcons.Add(assemblyName, "icon-clerk");
			}
			App.MainForm.NavigationMenu.Owner = this;

            App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
            MainCaptions(addExitMenu);
		}

		private ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-births", "births"),
				new ImageListEntry("menutree-deaths", "deaths"),
				new ImageListEntry("menutree-dogs", "dogs"),
				new ImageListEntry("menutree-contractors", "contractors"),
				new ImageListEntry("menutree-marriages", "marriages"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-file-maintenance", "file-maintenance")
			});
			return imageList;
		}

		public void DisableMenuOption(bool boolState, int intRow)
		{
		}

		private void ClearLabels()
		{
		}

		private void SetMenuOptions(string strMenu)
		{
			modGNBas.CloseChildForms();
			ClearLabels();
		}

		public void Menu1()
		{
			string strTemp = "";
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			switch (vbPorterVar)
            {
                case "MAIN":
                    break;
                case "FOREIGNBIRTH":
                    break;
                case "DELAYEDBIRTH":
                    break;
                case "REPORTS":
                    break;
                case "DEATHREPORTS":
                {
                    strTemp = frmGetDateRange.InstancePtr.Init("01/01/" + FCConvert.ToString(DateTime.Today.Year) + ";12/31/" + FCConvert.ToString(DateTime.Today.Year));
                    if (strTemp != string.Empty)
                    {
                        rptDeathSummary.InstancePtr.Init(ref strTemp);
                    }

                    break;
                }

                case "BIRTHREPORTS":
                {
                    strTemp = frmGetDateRange.InstancePtr.Init("01/01/" + FCConvert.ToString(DateTime.Today.Year) + ";12/31/" + FCConvert.ToString(DateTime.Today.Year));
                    if (strTemp != string.Empty)
                    {
                        rptBirthSummary.InstancePtr.Init(ref strTemp);
                    }

                    break;
                }

                case "MARRIAGEREPORTS":
                {
                    strTemp = frmGetDateRange.InstancePtr.Init("01/01/" + FCConvert.ToString(DateTime.Today.Year) + ";12/31/" + FCConvert.ToString(DateTime.Today.Year));
                    if (strTemp != string.Empty)
                    {
                        rptMarriageSummary.InstancePtr.Init(ref strTemp);
                    }

                    break;
                }

                case "DOGREPORTS":
                {
                    clsDRWrapper rsLicense = new clsDRWrapper();
                    rsLicense.OpenRecordset("Select * from PrinterSettings", modGNBas.DEFAULTDATABASE);
                    if (!rsLicense.EndOfFile())
                    {
                        modGNBas.Statics.gintLaserReportYear = FCConvert.ToString(rsLicense.Get_Fields_String("LicenseYear"));
                        modGNBas.Statics.gboolMonthlyLicenseLaser = FCConvert.ToInt32(rsLicense.Get_Fields_Int16("MonthlyLicense")) == 1;
                        frmMonthlyDogReport.InstancePtr.Show();
                    }
                    else
                    {
                        modGNBas.Statics.gintLaserReportYear = FCConvert.ToString(DateTime.Today.Year);
                        frmMonthlyDogReport.InstancePtr.Show();
                    }

                    break;
                }

                case "FILE":
                    frmClerkDefaults.InstancePtr.Show(App.MainForm);

                    break;
                case "BIRTHS":
                    modClerkGeneral.Statics.AddingBirthCertificate = true;
                    // frmBirths.Show 
                    //frmBirths.InstancePtr.Unload();
                    //frmBirths.InstancePtr.Init(0);
                    commandDispatcher.Send(new AddLiveBirth(0));

                    break;
                case "LIVEBIRTH":
                    break;
                case "MARRIAGE":
                    modClerkGeneral.Statics.SearchResponse = 0;
                    modClerkGeneral.Statics.AddingMarriageCertificate = true;
                    //frmMarriages.InstancePtr.Unload();
                    ////App.DoEvents();
                    //frmMarriages.InstancePtr.Show(App.MainForm);
                    commandDispatcher.Send(new AddMarriage(0));

                    break;
                case "DEATH":
                    modClerkGeneral.Statics.AddingDeathCertificate = true;
                    //frmDeaths.InstancePtr.Init(0);
                    commandDispatcher.Send(new AddDeath(0));
                    // frmDeaths.Show 
                    break;
                case "DOG":
                    DogActions(1);

                    break;
                case "GROUP":
                    frmGroupListing.InstancePtr.Show(App.MainForm);

                    break;
                default:
                    break;
            }
		}

		public void Menu10()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "DOGS");
				frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Dog Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Dog Custom Reports";
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu11()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "DOGREPORTS")
			{
				frmCustomReport.InstancePtr.Unload();
				frmRichTextInput.InstancePtr.Init("UnlicensedReminder", "DogDefaults", "twck0000.vb1", "Unlicensed Dog Reminder", true);
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "DOGREMINDERS");
                modGNBas.Statics.strPreSetReport = "Dog Reminders";
                frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Unlicensed Dog Reminders";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Unlicensed Dog Reminders";
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu12()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "DOGREPORTS")
			{
				frmCustomReport.InstancePtr.Unload();
				frmRichTextInput.InstancePtr.Init("WarrantReminder", "DogDefaults", "twck0000.vb1", "Warrant Reminder", true);
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "DOGREMINDERS");
				modGNBas.Statics.strPreSetReport = "Dog Reminders";
				modGNBas.Statics.strPreSetReport = "Warrant Reminder";
                frmCustomReport.InstancePtr.Show(App.MainForm);
                frmCustomReport.InstancePtr.Text = "Warrant Reminders Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Warrant Reminders Custom Reports";
			}
			else
			{
			}
		}

		public void Menu13()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				frmCustomReport.InstancePtr.Unload();
				frmRichTextInput.InstancePtr.Init("ExpiringReminder", "DogDefaults", "twck0000.vb1", "Expiring License Reminder", true);
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "EXPIRINGDOGREMINDERS");
				modGNBas.Statics.strPreSetReport = "Expiring Reminders";
                frmCustomReport.InstancePtr.Show(App.MainForm);
                frmCustomReport.InstancePtr.Text = "Expiring License Reminders";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Expiring License Reminders";
			}
			else
			{
			}
		}

		public void Menu14()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "DOGSLABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Dog Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Dog Custom Labels";
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu15()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				frmDogSummary.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
			ErrorHandler:
			;
			if (fecherFoundation.Information.Err().Number == 3031)
			{
				// not a valid password
				{
					/*? Resume Next; */}
			}
			else
			{
				// GoTo CallErrorRoutine
			}
		}

		public void Menu16()
		{
			string strFrom = "";
			string strTo = "";
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				strTo = "";
				strFrom = "";
				frmAuditDateRange.InstancePtr.Init();
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu17()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				rptDeletedInventory.InstancePtr.Init();
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu18()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				frmDogHistorySetup.InstancePtr.Show(App.MainForm);
			}
			else
			{
			}
		}

		public void Menu19()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
			}
			else
			{
			}
		}

		public void Menu2()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "REPORTS")
			{
			}
			else if (vbPorterVar == "BIRTHS")
			{
				modClerkGeneral.Statics.AddingBirthCertificate = true;
				// frmBirthDelayed.Show 
				frmBirthDelayed.InstancePtr.Unload();
				frmBirthDelayed.InstancePtr.Init(0);
			}
			else if (vbPorterVar == "FILE")
			{
				modStartup.UpdateDatabaseStructure();
			}
			else if (vbPorterVar == "BIRTHREPORTS")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "BIRTHS");
				modGNBas.Statics.strPreSetReport = "BIRTHS";
                frmCustomReport.InstancePtr.Show(App.MainForm);
                frmCustomReport.InstancePtr.Text = "Live Births Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Live Births Custom Reports";
			}
			else if (vbPorterVar == "MARRIAGEREPORTS")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "MARRIAGE");
                modGNBas.Statics.strPreSetReport = "MARRIAGE";
                frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Marriage Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Marriage Custom Reports";
			}
			else if (vbPorterVar == "DEATHREPORTS")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "DEATH");
				modGNBas.Statics.strPreSetReport = "DEATH";
                frmCustomReport.InstancePtr.Show(App.MainForm);
                frmCustomReport.InstancePtr.Text = "Deaths Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Deaths Custom Reports";
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				modGNBas.Statics.gstrReportOption = "Warrant";
				frmReportListing.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "MARRIAGE")
			{
				modClerkGeneral.Statics.SearchMarriageCertificate = true;
                commandDispatcher.Send(new MakeMarriageTransaction(Guid.Empty, Guid.NewGuid()));
            }
			else if (vbPorterVar == "DEATH")
			{
				modClerkGeneral.Statics.SearchDeathCertificate = true;
				//frmDeathSearchA.InstancePtr.Show(App.MainForm);
                commandDispatcher.Send(new MakeDeathTransaction(Guid.Empty, Guid.NewGuid()));
            }
			else if (vbPorterVar == "DOG")
			{
				DogActions(2);
			}
			else if (vbPorterVar == "GROUP")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "GROUP");
				modGNBas.Statics.strPreSetReport = "GROUP";
                frmCustomReport.InstancePtr.Show(App.MainForm);
                frmCustomReport.InstancePtr.Text = "Group / Committee Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Group / Committee Custom Reports";
			}
		}

		public void Menu3()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "FILE")
			{
				frmFacilityNames.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "BIRTHREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "BIRTHLABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Live Births Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Live Births Custom Labels";
			}
			else if (vbPorterVar == "MARRIAGEREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "MARRIAGELABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Marriages Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Marriages Custom Labels";
			}
			else if (vbPorterVar == "DEATHREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "DEATHLABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Deaths Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Deaths Custom Labels";
			}
			else if (vbPorterVar == "LIVEBIRTH")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "BIRTHS");
				frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Births Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Births Custom Reports";
			}
			else if (vbPorterVar == "FOREIGNBIRTH")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "FOREIGNBIRTH");
				frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Foreign Births Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Foreign Births Custom Reports";
			}
			else if (vbPorterVar == "DELAYEDBIRTH")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "DELAYEDBIRTH");
				frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Delayed Births Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Delayed Births Custom Reports";
			}
			else if (vbPorterVar == "MARRIAGE")
			{
			}
			else if (vbPorterVar == "DEATH")
			{
				//modGNBas.CloseChildForms();
				modGNBas.Statics.boolBurialSearch = true;
				modGNBas.Statics.gintBurialID = 0;
				modGNBas.Statics.Response = 0;
				//frmBurialPermit.InstancePtr.Show(App.MainForm);
                commandDispatcher.Send(new AddBurial());
            }
			else if (vbPorterVar == "BIRTHS")
			{
				modClerkGeneral.Statics.AddingBirthCertificate = true;
				frmBirthForeign.InstancePtr.Unload();
				frmBirthForeign.InstancePtr.Init(0);
			}
			else if (vbPorterVar == "DOG")
			{
				DogActions(3);
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				modGNBas.Statics.boolWarrant = true;
                frmDogWarrant.InstancePtr.LoadForm();
                frmDogWarrant.InstancePtr.cmdPrint_Click();
            }
			else if (vbPorterVar == "REPORTS")
			{
			}
			else if (vbPorterVar == "GROUP")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "GROUPLABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Group / Committee Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Group / Committee Custom Labels";
			}
			else
			{
			}
		}

		public void Menu4()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "BIRTHREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "DELAYEDBIRTH");
                modGNBas.Statics.strPreSetReport = "DELAYEDBIRTH";
                frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Delayed Births Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Delayed Births Custom Reports";
			}
			else if (vbPorterVar == "MARRIAGEREPORTS")
			{
				frmMarriageReport.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "DEATHREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "BURIAL");
                modGNBas.Statics.strPreSetReport = "BURIAL";
                frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Burial Custom Report";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Burial Custom Report";
			}
			else if (vbPorterVar == "LIVEBIRTH")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "BIRTHLABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Births Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Births Custom Labels";
			}
			else if (vbPorterVar == "FOREIGNBIRTH")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "FOREIGNBIRTHLABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Foreign Births Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Foreign Births Custom Labels";
			}
			else if (vbPorterVar == "DELAYEDBIRTH")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "DELAYEDBIRTH");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Delayed Births Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Delayed Births Custom Labels";
			}
			else if (vbPorterVar == "MARRIAGE")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "MARRIAGELABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Marriage Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Marriage Custom Labels";
			}
			else if (vbPorterVar == "FILE")
			{
				frmFees.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "DEATH")
			{
				modGNBas.CloseChildForms();
				//frmBurialSearch.InstancePtr.Show(App.MainForm);
                commandDispatcher.Send(new MakeBurialTransaction(Guid.Empty, Guid.NewGuid()));
            }
			else if (vbPorterVar == "BIRTHS")
			{
				modClerkGeneral.Statics.SearchBirthCertificate = true;
				modGNBas.Statics.gboolSearchType = "BIRTH";
				//frmBirthSearchA.InstancePtr.Show(App.MainForm);
                commandDispatcher.Send(new MakeBirthTransaction(Guid.Empty, Guid.NewGuid()));
            }
			else if (vbPorterVar == "DOG")
			{
				DogActions(4);
			}
			else if (vbPorterVar == "REPORTS")
			{
			}
			else if (vbPorterVar == "GROUP")
			{
			}
			else
			{
			}
		}

		public void Menu5()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				modGNBas.Statics.boolWarrant = false;
                frmDogWarrant.InstancePtr.LoadForm();
                frmDogWarrant.InstancePtr.cmdPrint_Click();
			}
			else if (vbPorterVar == "DEATHREPORTS")
			{
				frmDeathReport.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "DOG")
			{
				DogActions(5);
				// frmDogInventory.Show 
			}
			else if (vbPorterVar == "LIVEBIRTH")
			{
			}
			else if (vbPorterVar == "FOREIGNBIRTH")
			{
			}
			else if (vbPorterVar == "DELAYEDBIRTH")
			{
			}
			else if (vbPorterVar == "BIRTHS")
			{
			}
			else if (vbPorterVar == "DEATH")
			{
			}
			else if (vbPorterVar == "FILE")
			{
				frmDogInfo.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "BIRTHREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "DELAYEDBIRTHLABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Delayed Births Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Delayed Births Custom Labels";
			}
			else if (vbPorterVar == "REPORTS")
			{
			}
			else if (vbPorterVar == "MARRIAGEREPORTS")
			{
			}
			else
			{
			}
		}

		public void Menu6()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
				frmCommittee.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "DEATHREPORTS")
			{
			}
			else if (vbPorterVar == "BIRTHREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "FOREIGNBIRTH");
				frmCustomReport.InstancePtr.Show(App.MainForm);
				modGNBas.Statics.strPreSetReport = "FOREIGNBIRTH";
				frmCustomReport.InstancePtr.Text = "Foreign Births Custom Reports";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Foreign Births Custom Reports";
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				frmCustomReport.InstancePtr.Unload();
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "DOGINVENTORY");
                frmCustomReport.InstancePtr.Show(App.MainForm);
                modGNBas.Statics.strPreSetReport = "DOGINVENTORY";
                frmCustomReport.InstancePtr.Text = "Custom Tag Inventory";
				frmCustomReport.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Custom Tag Inventory";
			}
			else if (vbPorterVar == "BIRTHS")
			{
			}
			else if (vbPorterVar == "DOG")
			{
				DogActions(6);
				// frmDeletedDogs.Show 
			}
			else if (vbPorterVar == "FILE")
			{
				frmDogWarrant.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "REPORTS")
			{

			}
			else
			{
			}
		}

		public void Menu7()
		{
			string strResponse = "";
			int lngTemp;
			int lngTemp1;
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "FILE")
			{
				frmPrinterSettings.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				frmTagInventory.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "BIRTHREPORTS")
			{
				modCustomReport.SetFormFieldCaptions(frmCustomLabels.InstancePtr, "FOREIGNBIRTHLABELS");
				frmCustomLabels.InstancePtr.Show(App.MainForm);
				frmCustomLabels.InstancePtr.Text = "Foreign Births Custom Labels";
				frmCustomLabels.InstancePtr.TopPanel.Controls["HeaderText"].Text = "Foreign Births Custom Labels";
			}
			else if (vbPorterVar == "DOG")
			{
				DogActions(7);
				// Call SetMenuOptions("MAIN")
			}
			else if (vbPorterVar == "FILE")
			{
				frmPrinterSettings.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "REPORTS")
			{
			}
			else if (vbPorterVar == "BIRTHS")
			{
			}
			else
			{
			}
		}

		public void Menu8()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
                //- MDIParent.Close();
                //Application.Exit();
                App.MainForm.EndWaitCKModule();
				return;
			}
			else if (vbPorterVar == "BIRTHREPORTS")
			{
				frmBirthReport.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
				frmRegistrarNames.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				modGNBas.Statics.gstrMonthYear = Interaction.InputBox("Input Month/Year in MM/YYYY format", "Dog License # Backup", null);
				if (modGNBas.Statics.gstrMonthYear != "" && (FCConvert.ToDouble(Strings.Right(modGNBas.Statics.gstrMonthYear, 4)) > 1950 && FCConvert.ToDouble(Strings.Right(modGNBas.Statics.gstrMonthYear, 4)) < 3000))
				{
					if (modGNBas.Statics.gstrMonthYear != "" && (FCConvert.ToDouble(Strings.Left(modGNBas.Statics.gstrMonthYear, 2)) > 0 && FCConvert.ToDouble(Strings.Left(modGNBas.Statics.gstrMonthYear, 2)) < 13))
					{
						// rptDogLicenseBackup.Show
						rptDogLicenseBackup.InstancePtr.Init();
					}
					else
					{
						MessageBox.Show("Invalid report Month.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					if (modGNBas.Statics.gstrMonthYear != "")
					{
						MessageBox.Show("Invalid report Year.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
			else if (vbPorterVar == "DOG")
			{
				// gboolfees = True
				// frmDogInfo.Show 
				DogActions(8);
			}
			else
			{
			}
		}

		public void Menu9()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "DOG")
			{
				DogActions(9);
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
				frmStateTownInfo.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
				modGNBas.Statics.gstrReportOption = "Registration";
				frmReportListing.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "BIRTHREPORTS")
			{
			}
			else
			{
			}
		}

		public void ChangeUser()
		{
			// this sub use the security class and refresh the menu options reflecting the new user's security level
			modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
			modGlobalConstants.Statics.clsSecurityClass.Init("CK");
			// refresh the grid
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			// this should be all of your headings and get menu calls, a good example would be to look in your Menu1 Subs
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else if (vbPorterVar == "BIRTHS")
			{
			}
			else if (vbPorterVar == "MARRIAGE")
			{
			}
			else if (vbPorterVar == "DEATH")
			{
			}
			else if (vbPorterVar == "DOG")
			{
			}
			else if (vbPorterVar == "REPORTS")
			{
			}
			else if (vbPorterVar == "DOGREPORTS")
			{
			}
			else
			{
			}
		}

		public void DogReportCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Dog Reports]";
			string strTemp = "";
			string menu = "DOGREPORTS";
			for (int intCount = 1; intCount <= 18; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Monthly Dog License";
							//LabelNumber[Strings.Asc("1")] = 1;
							break;
						}
					case 2:
						{
							strTemp = "Municipal Warrant Listing";
							//LabelNumber[Strings.Asc("2")] = 2;
							break;
						}
					case 3:
						{
							strTemp = "Municipal Warrant";
							//LabelNumber[Strings.Asc("3")] = 3;
							break;
						}
					case 4:
						{
							strTemp = "Warrant Questionnaire";
							//LabelNumber[Strings.Asc("4")] = 4;
							DisableMenuOption(false, intCount);
							break;
						}
					case 5:
						{
							strTemp = "Warrant Return";
							//LabelNumber[Strings.Asc("5")] = 5;
							break;
						}
					case 6:
						{
							strTemp = "Custom Tag Inventory";
							//LabelNumber[Strings.Asc("6")] = 6;
							break;
						}
					case 7:
						{
							strTemp = "Detailed Tag Inventory";
							//LabelNumber[Strings.Asc("7")] = 7;
							break;
						}
					case 8:
						{
							strTemp = "Dog License # Backup";
							//LabelNumber[Strings.Asc("8")] = 8;
							break;
						}
					case 9:
						{
							strTemp = "Registration Listing";
							//LabelNumber[Strings.Asc("9")] = 9;
							break;
						}
					case 10:
						{
							strTemp = "Custom Dog Reports";
							//LabelNumber[Strings.Asc("A")] = 10;
							break;
						}
					case 11:
						{
							strTemp = "Unlicensed Dog Reminders";
							//LabelNumber[Strings.Asc("B")] = 11;
							break;
						}
					case 12:
						{
							strTemp = "Warrant Reminders";
							//LabelNumber[Strings.Asc("C")] = 12;
							break;
						}
					case 13:
						{
							strTemp = "Expiring License Reminders";
							//LabelNumber[Strings.Asc("D")] = 13;
							break;
						}
					case 14:
						{
							strTemp = "Custom Dog Labels";
							//LabelNumber[Strings.Asc("E")] = 14;
							break;
						}
					case 15:
						{
							strTemp = "Dog License Summary";
							//LabelNumber[Strings.Asc("F")] = 15;
							break;
						}
					case 16:
						{
							strTemp = "Transaction Audit";
							//LabelNumber[Strings.Asc("G")] = 16;
							break;
						}
					case 17:
						{
							strTemp = "Deleted Inventory";
							//LabelNumber[Strings.Asc("H")] = 17;
							break;
						}
					case 18:
						{
							strTemp = "Dog History";
							//LabelNumber[Strings.Asc("I")] = 18;
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
			}
		}

		public void ReportCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Printing]";
			bool boolDisable = false;
			string strTemp = "";
			string imageKey = "";
			string menu = "REPORTS";
			for (int intCount = 1; intCount <= 5; intCount++)
			{
				boolDisable = false;
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Birth Reports";
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELDOGSONLY) == modStartup.CKLEVELDOGSONLY)
								boolDisable = true;
							break;
						}
					case 2:
						{
							strTemp = "Marriage Reports";
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELDOGSONLY) == modStartup.CKLEVELDOGSONLY)
								boolDisable = true;
							break;
						}
					case 3:
						{
							strTemp = "Death Reports";
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELDOGSONLY) == modStartup.CKLEVELDOGSONLY)
								boolDisable = true;
							break;
						}
					case 4:
						{
							strTemp = "Dog Reports";
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELVITALSONLY) == modStartup.CKLEVELVITALSONLY)
								boolDisable = true;
							break;
						}
					case 5:
						{
							strTemp = "Group / Committee Reports";
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELDOGSONLY) == modStartup.CKLEVELDOGSONLY)
								boolDisable = true;
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELVITALSONLY) == modStartup.CKLEVELVITALSONLY)
								boolDisable = true;
							break;
						}
				}
				FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2, imageKey);
				switch (intCount)
				{
					case 1:
						BirthReportCaptions(newItem);
						break;
					case 2:
						MarriageReportCaptions(newItem);
						break;
					case 3:
						DeathReportCaptions(newItem);
						break;
					case 4:
						DogReportCaptions(newItem);
						break;
					case 5:
						GroupReportCaptions(newItem);
						break;
				}
			}
		}

		public void BirthReportCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Birth Reports]";
			string strTemp = "";
			string menu = "BIRTHREPORTS";
			for (int intCount = 1; intCount <= 8; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "# of Live Births";
							break;
						}
					case 2:
						{
							strTemp = "Custom Reports (Live)";
							break;
						}
					case 3:
						{
							strTemp = "Custom Labels (Live)";
							break;
						}
					case 4:
						{
							strTemp = "Custom Reports (Delayed)";
							break;
						}
					case 5:
						{
							strTemp = "Custom Labels (Delayed)";
							break;
						}
					case 6:
						{
							strTemp = "Custom Reports (Foreign)";
							break;
						}
					case 7:
						{
							strTemp = "Custom Labels (Foreign)";
							break;
						}
					case 8:
						{
							strTemp = "Birth Report";
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
			}
		}

		public void MarriageReportCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Marriage Reports]";
			string strTemp = "";
			string menu = "MARRIAGEREPORTS";
			for (int intCount = 1; intCount <= 4; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "# of Marriages Report";
							break;
						}
					case 2:
						{
							strTemp = "Custom Reports";
							break;
						}
					case 3:
						{
							strTemp = "Custom Labels";
							break;
						}
					case 4:
						{
							strTemp = "Marriage Report";
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
			}
		}

		public void DeathReportCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Death Reports]";
			string strTemp = "";
			string menu = "DEATHREPORTS";
			for (int intCount = 1; intCount <= 5; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "# of Deaths Report";
							break;
						}
					case 2:
						{
							strTemp = "Custom Reports";
							break;
						}
					case 3:
						{
							strTemp = "Custom Labels";
							break;
						}
					case 4:
						{
							strTemp = "Custom Burial Report";
							break;
						}
					case 5:
						{
							strTemp = "Death Report";
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
			}
		}

		public void GroupReportCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Group / Committee Reports]";
			string strTemp = "";
			string menu = "GROUP";
			for (int intCount = 1; intCount <= 3; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Group Listing";
							break;
						}
					case 2:
						{
							strTemp = "Custom Reports";
							break;
						}
					case 3:
						{
							strTemp = "Custom Labels";
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 3);
			}
		}

		public void DogCaptions(FCMenuItem parent)
		{
			int lngFCode = 0;
			App.MainForm.Text = strTrio + "   [Dogs]";
			string strTemp = "";
			string menu = "DOG";
			bool boolDisable = false;
			for (int intCount = 1; intCount <= 6; intCount++)
			{
				lngFCode = 0;
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Dog Records";
							break;
						}
					case 2:
						{
							strTemp = "Dog Tag / Sticker Inventory";
							lngFCode = modGNBas.CNSTEDITTAGSTICKERINVENTORY;
							break;
						}
					case 3:
						{
							strTemp = "Deleted Dogs";
							break;
						}
					case 4:
						{
							strTemp = "Online Dog Registrations";
							lngFCode = modGNBas.CNSTONLINEDOGTRANSACTIONS;
							break;
						}
					case 5:
						{
							strTemp = "Void Dog Transaction";
							if (!(FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGNBas.CNSTVOIDDOGTRANSACTIONS)) == "F"))
							{
								lngFCode = modGNBas.CNSTVOIDOWNDOGTRANSACTIONS;
							}
							else
							{
								lngFCode = modGNBas.CNSTVOIDDOGTRANSACTIONS;
							}
							break;
						}
					case 6:
						{
							strTemp = "Transaction Audit";
							break;
						}
				}
				if (lngFCode != 0)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2);
			}
		}

		public void DogActions(int intAction)
		{
			string strFilename = "";
			string strCurDir = "";
			switch (intAction)
			{
				case 1:
					{
						//frmNewDogSearch.InstancePtr.Show(App.MainForm);
                        commandDispatcher.Send(new MakeDogTransaction(Guid.Empty, Guid.NewGuid()));
						break;
					}
				case 2:
					{
						frmDogInventory.InstancePtr.Show(App.MainForm);
						break;
					}
				case 3:
					{
						frmDeletedDogs.InstancePtr.Show(App.MainForm);
						break;
					}
				case 4:
					{
						frmOnlineDogs.InstancePtr.Show(App.MainForm);
 
						break;
					}
				case 5:
					{
						frmVoidDog.InstancePtr.Show(App.MainForm);
						break;
					}
				case 6:
                {
                    var startDate = DateTime.Today.OneMonthBefore().StartOfMonth();
					var endDate = DateTime.Today;
                    var result = commandDispatcher.Send(new GetDogAuditOptions(false, startDate, endDate)).Result;
					if (!result.Cancelled)
						frmDogAudit.InstancePtr.Init(result);
                    break;
					}
				case 7:
					{
						break;
					}
			}
		}

		public void MainCaptions(bool addExitMenu = false)
		{
			bool boolDisable = false;
			string strTemp = "";
			string menu = "MAIN";
			string imageKey = "";
			App.MainForm.Caption = "CLERK";
            App.MainForm.NavigationMenu.OriginName = "Clerk";
			int lngFCode = 0;
			App.MainForm.Text = strTrio + "   [Main]";
            int menuItemsCount = addExitMenu ? 8 : 7;
            for (int intCount = 1; intCount <= menuItemsCount; intCount++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (intCount)
				{
					case 1:
						{
							imageKey = "births";
							strTemp = "Births";
							lngFCode = modGNBas.BIRTHS;
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELDOGSONLY) == modStartup.CKLEVELDOGSONLY)
								boolDisable = true;
							break;
						}
					case 2:
						{
							imageKey = "marriages";
							strTemp = "Marriages";
							lngFCode = modGNBas.MARRIAGES;
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELDOGSONLY) == modStartup.CKLEVELDOGSONLY)
								boolDisable = true;
							break;
						}
					case 3:
						{
							imageKey = "deaths";
							strTemp = "Deaths";
							lngFCode = modGNBas.DEATHS;
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELDOGSONLY) == modStartup.CKLEVELDOGSONLY)
								boolDisable = true;
							break;
						}
					case 4:
						{
							imageKey = "dogs";
							strTemp = "Dogs";
							lngFCode = modGNBas.DOGS;
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELVITALSONLY) == modStartup.CKLEVELVITALSONLY)
								boolDisable = true;
							break;
						}
					case 5:
						{
							imageKey = "printing";
							strTemp = "Printing";
							lngFCode = modGNBas.REPORTS;
							break;
						}
					case 6:
						{
							imageKey = "contractors";
							strTemp = "Groups / Committees";
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELDOGSONLY) == modStartup.CKLEVELDOGSONLY)
								boolDisable = true;
							if ((modStartup.Statics.gintClerkLevel & modStartup.CKLEVELVITALSONLY) == modStartup.CKLEVELVITALSONLY)
								boolDisable = true;
							break;
						}
					case 7:
						{
							imageKey = "file-maintenance";
							strTemp = "File Maintenance";
							lngFCode = modGNBas.FILEMAINTENANCE;
							break;
						}
                    case 8:
                        {
                            strTemp = "Exit Clerk";
                            lngFCode = 0;
                            break;
                        }
                }

				if (!boolDisable)
				{
					if (lngFCode != 0)
					{
						boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
					}
					else
					{
						boolDisable = false;
					}
				}
				
				FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 1, imageKey);
				switch (intCount)
				{
					case 1:
						BirthCaptions(newItem);
						break;
					case 2:
						MarriageCaptions(newItem);
						break;
					case 3:
						DeathCaptions(newItem);
						break;
					case 4:
						DogCaptions(newItem);
						break;
					case 5:
						ReportCaptions(newItem);
						break;
					case 7:
						FileMaintCaptions(newItem);
						break;

				}
			}
		}

		public void DeathCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Deaths]";
			// Grid.Rows = 5
			string strTemp = "";
			string menu = "DEATH";
			for (int intCount = 1; intCount <= 4; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Add Death Certificate";
							break;
						}
					case 2:
						{
							strTemp = "Search Death Records";
							break;
						}
					case 3:
						{
							strTemp = "Add Burial Permit";
							break;
						}
					case 4:
						{
							strTemp = "Search Burial Permit";
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 2);
			}
		}

		public void BirthCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Births]";
			string strTemp = "";
			string menu = "BIRTHS";
			for (int intCount = 1; intCount <= 4; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Add Live Birth";
							break;
						}
					case 2:
						{
							strTemp = "Add Delayed Birth";
							break;
						}
					case 3:
						{
							strTemp = "Add Foreign Birth";
							break;
						}
					case 4:
						{
							strTemp = "Search Birth Records";
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 2);
			}
		}

		public void LiveBirthCaptions()
		{
			App.MainForm.Text = strTrio + "   [Live Births]";
			// Grid.Rows = 5
			string strTemp = "";
			for (int intCount = 1; intCount <= 4; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Add Live Birth";
							break;
						}
					case 2:
						{
							strTemp = "Search Live Birth Records";
							break;
						}
					case 3:
						{
							strTemp = "Custom Reports";
							break;
						}
					case 4:
						{
							strTemp = "Custom Labels";
							break;
						}
				}
			}
		}

		public void DelayedBirthCaptions()
		{
			App.MainForm.Text = strTrio + "   [Delayed Births]";
			string strTemp = "";
			for (int intCount = 1; intCount <= 4; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Add Delayed Birth";
							break;
						}
					case 2:
						{
							strTemp = "Search Delayed Birth Records";
							break;
						}
					case 3:
						{
							strTemp = "Custom Reports";
							break;
						}
					case 4:
						{
							strTemp = "Custom Labels";
							break;
						}
				}
			}
		}

		public void ForeignBirthCaptions()
		{
			App.MainForm.Text = strTrio + "   [Foreign Births]";
			string strTemp = "";
			for (int intCount = 1; intCount <= 4; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Add Foreign Birth";
							break;
						}
					case 2:
						{
							strTemp = "Search Foreign Birth Records";
							break;
						}
					case 3:
						{
							strTemp = "Custom Reports";
							break;
						}
					case 4:
						{
							strTemp = "Custom Labels";
							break;
						}
				}
			}
		}

		public void FileMaintCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [File Maintenance]";
			string strTemp = "";
			string menu = "FILE";
			for (int intCount = 1; intCount <= 9; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Customize";
							break;
						}
					case 2:
						{
							strTemp = "Check Database Structure";
							break;
						}
					case 3:
						{
							strTemp = "Funeral Establishment Info";
							break;
						}
					case 4:
						{
							strTemp = "Edit Clerk Fees";
							break;
						}
					case 5:
						{
							strTemp = "Edit Dog Information";
							break;
						}
					case 6:
						{
							strTemp = "Edit Dog Warrant Info";
							break;
						}
					case 7:
						{
							strTemp = "Default Printer Settings";
							break;
						}
					case 8:
						{
							strTemp = "Edit Clerk Names";
							break;
						}
					case 9:
						{
							strTemp = "Edit Town / County / State";
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 2);
			}
		}

		public void MarriageCaptions(FCMenuItem parent)
		{
			App.MainForm.Text = strTrio + "   [Marriages]";
			string strTemp = "";
			string menu = "MARRIAGE";
			for (int intCount = 1; intCount <= 2; intCount++)
			{
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Add Marriage Records";
							break;
						}
					case 2:
						{
							strTemp = "Search Marriage Records";
							break;
						}
				}
				parent.SubItems.Add(strTemp, "Menu" + intCount, menu, true, 2);
			}
		}

		private void MDIForm_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "MDIForm_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call WriteYY
				modReplaceWorkFiles.EntryFlagFile(true, "CK", true);
				if (modCashReceiptingData.Statics.gboolFromDosTrio || modCashReceiptingData.Statics.bolFromWindowsCR)
				{
				}
				else
				{
					//Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
				}
				Application.Exit();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuFExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "MDIForm_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				// Call WriteYY
				modReplaceWorkFiles.EntryFlagFile(true, "CK", true);
				if (modCashReceiptingData.Statics.gboolFromDosTrio == false)
				{
					//Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
				}
				Application.Exit();
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
		}

		private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.HelpShell("TWPY0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.HelpShell("TWPP0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuPrintForms_Click(object sender, System.EventArgs e)
		{
			frmScreenPrint.InstancePtr.Show();
			//Support.ZOrder(this, 1);
		}

		private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.HelpShell("TWRE0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuRedBookHelp_Click(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.HelpShell("TWRB0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.HelpShell("TWTB0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.HelpShell("TWCL0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.HelpShell("TWUT0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			//modGlobalFunctions.HelpShell("TWVR0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		public void Grid_Enter(object sender, System.EventArgs e)
		{
			int a;
		}
	}
}
