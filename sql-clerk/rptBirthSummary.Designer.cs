﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptBirthSummary.
	/// </summary>
	partial class rptBirthSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBirthSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBorn = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBorn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtName,
            this.txtBorn});
			this.Detail.Height = 0.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// txtName
			// 
			this.txtName.Height = 0.2083333F;
			this.txtName.Left = 1.03125F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 3.6875F;
			// 
			// txtBorn
			// 
			this.txtBorn.Height = 0.2083333F;
			this.txtBorn.Left = 4.90625F;
			this.txtBorn.Name = "txtBorn";
			this.txtBorn.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtBorn.Text = null;
			this.txtBorn.Top = 0F;
			this.txtBorn.Width = 1.96875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCaption,
            this.txtCaption2,
            this.txtTotal,
            this.txtTown,
            this.txtTime,
            this.txtDate,
            this.txtPage});
			this.ReportHeader.Height = 0.9895833F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1666667F;
			this.txtCaption.Left = 0.0625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = null;
			this.txtCaption.Top = 0.04166667F;
			this.txtCaption.Width = 7.125F;
			// 
			// txtCaption2
			// 
			this.txtCaption2.Height = 0.1666667F;
			this.txtCaption2.Left = 0.0625F;
			this.txtCaption2.Name = "txtCaption2";
			this.txtCaption2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.txtCaption2.Text = null;
			this.txtCaption2.Top = 0.25F;
			this.txtCaption2.Width = 7.125F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.2083333F;
			this.txtTotal.Left = 0.03125F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; tex" +
    "t-decoration: underline";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0.625F;
			this.txtTotal.Width = 7.15625F;
			// 
			// txtTown
			// 
			this.txtTown.Height = 0.1666667F;
			this.txtTown.Left = 0.03125F;
			this.txtTown.Name = "txtTown";
			this.txtTown.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtTown.Text = null;
			this.txtTown.Top = 0.04166667F;
			this.txtTown.Width = 1.375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1666667F;
			this.txtTime.Left = 0.03125F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.2083333F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 5.8125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.04166667F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 5.6875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.2083333F;
			this.txtPage.Width = 1.5F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field5,
            this.Field6});
			this.PageHeader.Height = 0.2916667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// Field5
			// 
			this.Field5.Height = 0.1666667F;
			this.Field5.Left = 1.375F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-decoration: underl" +
    "ine";
			this.Field5.Text = "NAME";
			this.Field5.Top = 0.125F;
			this.Field5.Width = 0.96875F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1666667F;
			this.Field6.Left = 5.125F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-decoration: underl" +
    "ine";
			this.Field6.Text = "BORN";
			this.Field6.Top = 0.125F;
			this.Field6.Width = 0.8125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptBirthSummary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.302083F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBorn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBorn;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
