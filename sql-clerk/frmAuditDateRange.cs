﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmAuditDateRange : BaseForm
	{
		public frmAuditDateRange()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmAuditDateRange InstancePtr
		{
			get
			{
				return (frmAuditDateRange)Sys.GetInstance(typeof(frmAuditDateRange));
			}
		}

		protected frmAuditDateRange _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 08/31/2005
		// ********************************************************
		string strFromDate;
		string strToDate;
		private bool boolIncludeOnline;

		public void Init()
		{
			strFromDate = "";
			strToDate = "";
			this.Text = "Transaction Audit";
			this.Show(App.MainForm);
		}

		private void frmAuditDateRange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmAuditDateRange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAuditDateRange properties;
			//frmAuditDateRange.FillStyle	= 0;
			//frmAuditDateRange.ScaleWidth	= 3885;
			//frmAuditDateRange.ScaleHeight	= 2415;
			//frmAuditDateRange.LinkTopic	= "Form2";
			//frmAuditDateRange.LockControls	= -1  'True;
			//frmAuditDateRange.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (!Information.IsDate(t2kFrom.Text))
			{
				MessageBox.Show("You must enter a valid start date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// If boolRequireBoth And Not IsDate(t2kTo.Text) Then
			// MsgBox "You must enter a valid end date", vbExclamation, "Invalid Date"
			// Exit Sub
			// End If
			strFromDate = Strings.Format(t2kFrom.Text, "MM/dd/yyyy");
			if (Information.IsDate(t2kTo.Text))
			{
				strToDate = Strings.Format(t2kTo.Text, "MM/dd/yyyy");
			}
			else
			{
				strToDate = "";
			}
			if (chkIncludeOnline.CheckState == CheckState.Checked)
			{
				boolIncludeOnline = true;
			}
			else
			{
				boolIncludeOnline = false;
			}
			int intOrder = 0;
			if (cmbOrder.Text == "Transaction Date")
			{
				intOrder = 0;
			}
			else if (cmbOrder.Text == "Owner")
			{
				intOrder = 1;
			}
			else if (cmbOrder.Text == "Tag")
			{
				intOrder = 2;
			}
			rptDogAudit.InstancePtr.Init("", false, intOrder, strFromDate, strToDate, boolIncludeOnline);
			Close();
		}
	}
}
