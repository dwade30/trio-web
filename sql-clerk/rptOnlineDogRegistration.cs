﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using TWSharedLibrary;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for rptOnlineDogRegistration.
	/// </summary>
	public partial class rptOnlineDogRegistration : BaseSectionReport
	{
		public rptOnlineDogRegistration()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Online Dog Registrations Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptOnlineDogRegistration InstancePtr
		{
			get
			{
				return (rptOnlineDogRegistration)Sys.GetInstance(typeof(rptOnlineDogRegistration));
			}
		}

		protected rptOnlineDogRegistration _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOnlineDogRegistration	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private class TransactionInfo
		{
			public string strDogName = string.Empty;
			// vbPorter upgrade warning: strDOB As DateTime	OnWrite(string)
			public DateTime strDOB;
			// vbPorter upgrade warning: strTransDate As DateTime	OnWrite(string)
			public DateTime strTransDate;
			public string strSex = string.Empty;
			public string strBreed = string.Empty;
			public string strColor = string.Empty;
			public string strSpay = string.Empty;
			public string strVet = string.Empty;
			public string strRabiesCert = string.Empty;
			public string strTransType = string.Empty;
			public Decimal curTransFee;
			public int intOwnerKey;
		};

		private class OwnerInfo
		{
			public int lngID;
			public string strOwner = string.Empty;
			public string strOwnerLast = string.Empty;
			public string strOwnerFirst = string.Empty;
			public string strOwnerMI = string.Empty;
			public string strPhone = string.Empty;
			public string strAddress1 = string.Empty;
			public string strAddress2 = string.Empty;
			public string strCity = string.Empty;
			public string strState = string.Empty;
			public string strZip = string.Empty;
			public string strLocation = string.Empty;
			public TransactionInfo[] udtTransInfo = null;
			public int intTransactions;
			public bool blnFirstTransactionAdded;
		};

		string[] strData = null;
		public string strFileName = "";
		// vbPorter upgrade warning: curOwnerFee As Decimal	OnWrite(Decimal, int)
		Decimal curOwnerFee;
		Decimal curTotalFee;
		string strLine;
		OwnerInfo[] udtOwnInfo = null;
		bool blnFirstOwnerAdded;
		int intOwners;
		int intTransactionCounter;
		int intOwnerCounter;
		bool blnFirstRecord;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				intTransactionCounter += 1;
				if (intTransactionCounter <= udtOwnInfo[intOwnerCounter].intTransactions - 1)
				{
					eArgs.EOF = false;
				}
				else
				{
					intOwnerCounter += 1;
					intTransactionCounter = 0;
					if (intOwnerCounter <= intOwners - 1)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = intOwnerCounter;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			FCFileSystem fs = new FCFileSystem();
			StreamReader tsInfo;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			string strOwner = "";
			bool blnDontAdd = false;
			int intOwnerRecord = 0;
			// vbPorter upgrade warning: strTemp As string	OnRead(DateTime)
			string strTemp = "";
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.Grid);
			Label2.Text = "TRIO Software Corp";
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			;
			this.PrintWidth = 15100;
			blnFirstRecord = true;
			blnFirstOwnerAdded = false;
			intOwners = 0;
			intOwnerCounter = 0;
			intTransactionCounter = 0;
            tsInfo = File.OpenText(strFileName);//, IOMode.ForReading, false, Tristate.TristateUseDefault);
			strLine = tsInfo.ReadLine();
			while (tsInfo.EndOfStream == false)
			{
				strLine = tsInfo.ReadLine();
				strData = modGlobalRoutines.SplitQuotedText(ref strLine, ",");
				blnDontAdd = false;
				if (blnFirstOwnerAdded)
				{
					for (counter = 0; counter <= (Information.UBound(udtOwnInfo, 1)); counter++)
					{
						strOwner = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strData[7].Replace("\"", "")) + ", " + fecherFoundation.Strings.Trim(strData[5].Replace("\"", "")) + " " + fecherFoundation.Strings.Trim(strData[6].Replace("\"", "")));
						if (udtOwnInfo[counter].strOwner == strOwner)
						{
							intOwnerRecord = counter;
							blnDontAdd = true;
							break;
						}
					}
				}
				if (!blnDontAdd)
				{
					if (intOwners == 0 && blnFirstOwnerAdded == false)
					{
						Array.Resize(ref udtOwnInfo, intOwners + 1);
						blnFirstOwnerAdded = true;
						intOwners += 1;
					}
					else
					{
						intOwners += 1;
						Array.Resize(ref udtOwnInfo, intOwners + 1);
					}
					intOwnerRecord = (intOwners - 1);
					strOwner = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strData[7].Replace("\"", "")) + ", " + fecherFoundation.Strings.Trim(strData[5].Replace("\"", "")) + " " + fecherFoundation.Strings.Trim(strData[6].Replace("\"", "")));
					udtOwnInfo[intOwners - 1].strOwner = strOwner;
					udtOwnInfo[intOwners - 1].strOwnerFirst = strData[5];
					udtOwnInfo[intOwners - 1].strOwnerLast = strData[7];
					udtOwnInfo[intOwners - 1].strOwnerMI = strData[6];
					udtOwnInfo[intOwners - 1].strAddress1 = fecherFoundation.Strings.Trim(strData[13].Replace("\"", ""));
					udtOwnInfo[intOwners - 1].strAddress2 = fecherFoundation.Strings.Trim(strData[14].Replace("\"", ""));
					udtOwnInfo[intOwners - 1].strCity = fecherFoundation.Strings.Trim(strData[15].Replace("\"", ""));
					udtOwnInfo[intOwners - 1].strState = fecherFoundation.Strings.Trim(strData[16].Replace("\"", ""));
					udtOwnInfo[intOwners - 1].strZip = Strings.Format(fecherFoundation.Strings.Trim(strData[17].Replace("\"", "")), "00000");
					udtOwnInfo[intOwners - 1].strLocation = fecherFoundation.Strings.Trim(strData[18].Replace("\"", ""));
					udtOwnInfo[intOwners - 1].strPhone = fecherFoundation.Strings.Trim(strData[11]).Replace("-", "").Replace("\"", "");
					udtOwnInfo[intOwners - 1].blnFirstTransactionAdded = false;
					udtOwnInfo[intOwners - 1].intTransactions = 0;
				}
				if (udtOwnInfo[intOwnerRecord].intTransactions == 0 && udtOwnInfo[intOwnerRecord].blnFirstTransactionAdded == false)
				{
					Array.Resize(ref udtOwnInfo[intOwnerRecord].udtTransInfo, udtOwnInfo[intOwnerRecord].intTransactions + 1);
					udtOwnInfo[intOwnerRecord].blnFirstTransactionAdded = true;
					udtOwnInfo[intOwnerRecord].intTransactions += 1;
				}
				else
				{
					udtOwnInfo[intOwnerRecord].intTransactions += 1;
					Array.Resize(ref udtOwnInfo[intOwnerRecord].udtTransInfo, udtOwnInfo[intOwnerRecord].intTransactions + 1);
				}
				strTemp = fecherFoundation.Strings.Trim(strData[20].Replace("\"", ""));
				if (strTemp.Length == 6)
				{
					strTemp = Strings.Right(strTemp, 2) + "/" + Strings.Left(strTemp, 4);
				}
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strDOB = FCConvert.ToDateTime(strTemp);
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strDogName = fecherFoundation.Strings.Trim(strData[19].Replace("\"", ""));
				// 11/03/2005 Call #80998 State changed the file format
				// udtOwnInfo(intOwnerRecord).udtTransInfo(udtOwnInfo(intOwnerRecord).intTransactions - 1).strRabiesCert = Trim(Replace(strData(36), """", ""))
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strRabiesCert = fecherFoundation.Strings.Trim(strData[37].Replace("\"", ""));
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strSex = fecherFoundation.Strings.Trim(strData[21].Replace("\"", ""));
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strSpay = fecherFoundation.Strings.Trim(strData[25].Replace("\"", ""));
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].intOwnerKey = intOwnerRecord;
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strBreed = fecherFoundation.Strings.Trim(strData[22].Replace("\"", ""));
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strColor = fecherFoundation.Strings.Trim(strData[23].Replace("\"", ""));
				// 11/03/2005 Call #80998 State changed the file format
				// udtOwnInfo(intOwnerRecord).udtTransInfo(udtOwnInfo(intOwnerRecord).intTransactions - 1).strTransDate = Trim(Replace(strData(28), """", ""))
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strTransDate = FCConvert.ToDateTime(fecherFoundation.Strings.Trim(strData[29].Replace("\"", "")));
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].curTransFee = FCConvert.ToDecimal(fecherFoundation.Strings.Trim(strData[26].Replace("\"", "")));
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strTransType = fecherFoundation.Strings.Trim(strData[2].Replace("\"", ""));
				udtOwnInfo[intOwnerRecord].udtTransInfo[udtOwnInfo[intOwnerRecord].intTransactions - 1].strVet = fecherFoundation.Strings.Trim(strData[24].Replace("\"", ""));
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldDate.Text = Strings.Format(udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strTransDate, "MM/dd/yyyy");
			fldType.Text = udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strTransType;
			fldName.Text = udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strDogName;
			fldDOB.Text = Strings.Format(udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strDOB, "MM/dd/yyyy");
			fldSex.Text = udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strSex;
			fldBreed.Text = udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strBreed;
			fldColor.Text = udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strColor;
			fldSpay.Text = udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strSpay;
			fldVet.Text = udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strVet;
			fldRabiesCert.Text = udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].strRabiesCert;
			fldFee.Text = Strings.Format(udtOwnInfo[intOwnerCounter].udtTransInfo[intTransactionCounter].curTransFee, "#,##0.00");
			curOwnerFee += FCConvert.ToDecimal(fldFee.Text);
			curTotalFee += FCConvert.ToDecimal(fldFee.Text);
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldOwnerFee.Text = Strings.Format(curOwnerFee, "#,##0.00");
			curOwnerFee = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldTotalFee.Text = Strings.Format(curTotalFee, "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldOwner.Text = udtOwnInfo[intOwnerCounter].strOwner;
			fldAddress1.Text = udtOwnInfo[intOwnerCounter].strAddress1 + " " + udtOwnInfo[intOwnerCounter].strAddress2;
			fldAddress2.Text = udtOwnInfo[intOwnerCounter].strCity + ", " + udtOwnInfo[intOwnerCounter].strState + " " + udtOwnInfo[intOwnerCounter].strZip;
			fldLocation.Text = udtOwnInfo[intOwnerCounter].strLocation;
			fldPhone.Text = Strings.Format(udtOwnInfo[intOwnerCounter].strPhone, "(###) ###-####");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		
	}
}
