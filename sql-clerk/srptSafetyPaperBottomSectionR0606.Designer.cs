﻿namespace TWCK0000
{
	/// <summary>
	/// Summary description for srptSafetyPaperBottomSectionR0606.
	/// </summary>
	partial class srptSafetyPaperBottomSectionR0606
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptSafetyPaperBottomSectionR0606));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAttest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgSig = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			((System.ComponentModel.ISupportInitialize)(this.txtAttest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAttest,
				this.txtDateIssued,
				this.txtMuni,
				this.imgSig
			});
			this.Detail.Height = 0.8854167F;
			this.Detail.Name = "Detail";
			// 
			// txtAttest
			// 
			this.txtAttest.CanGrow = false;
			this.txtAttest.Height = 0.375F;
			this.txtAttest.Left = 1.625F;
			this.txtAttest.Name = "txtAttest";
			this.txtAttest.Text = "Attest";
			this.txtAttest.Top = 0.40625F;
			this.txtAttest.Width = 3.25F;
			// 
			// txtDateIssued
			// 
			this.txtDateIssued.CanGrow = false;
			this.txtDateIssued.Height = 0.1875F;
			this.txtDateIssued.Left = 5.125F;
			this.txtDateIssued.Name = "txtDateIssued";
			this.txtDateIssued.Text = "Date Issued";
			this.txtDateIssued.Top = 0.09375F;
			this.txtDateIssued.Width = 1.875F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 1.625F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Text = "Muni";
			this.txtMuni.Top = 0.09375F;
			this.txtMuni.Width = 2.125F;
			// 
			// imgSig
			// 
			this.imgSig.Height = 0.375F;
			this.imgSig.HyperLink = null;
			this.imgSig.ImageData = null;
			this.imgSig.Left = 4.1875F;
			this.imgSig.LineWeight = 1F;
			this.imgSig.Name = "imgSig";
			this.imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgSig.Top = 0F;
			this.imgSig.Width = 2.125F;
			// 
			// srptSafetyPaperBottomSectionR0606
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.041667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtAttest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateIssued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgSig;
	}
}
