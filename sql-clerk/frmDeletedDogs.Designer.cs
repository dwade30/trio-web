//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	/// <summary>
	/// Summary description for frmDeletedDogs.
	/// </summary>
	partial class frmDeletedDogs
	{
		public fecherFoundation.FCFrame framDogs;
		public fecherFoundation.FCRichTextBox rtbComments;
		public fecherFoundation.FCTextBox txtDogName;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCCheckBox chkSearchRescue;
		public fecherFoundation.FCCheckBox chkHearingGuid;
		public fecherFoundation.FCCheckBox chkWolfHybrid;
		public fecherFoundation.FCCheckBox chkNeuterSpay;
		public fecherFoundation.FCCheckBox chkDeceased;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtTag;
		public fecherFoundation.FCTextBox txtSticker;
		public Global.T2KDateBox T2KIssueDate;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtCertificateNumber;
		public fecherFoundation.FCTextBox txtRabiesTag;
		public Global.T2KDateBox T2KDateVaccinated;
		public Global.T2KDateBox t2kExpirationDate;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public Global.T2KDateBox T2KDOB;
		public fecherFoundation.FCTextBox txtMarkings;
		public fecherFoundation.FCComboBox cmbBreed;
		public fecherFoundation.FCComboBox cmbColor;
		public fecherFoundation.FCTextBox txtSex;
		public fecherFoundation.FCTextBox txtSpayNeuterNumber;
		public fecherFoundation.FCTextBox txtVet;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel lblOwnerName;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame framList;
		public fecherFoundation.FCGrid Grid;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEditOrList;
		public fecherFoundation.FCToolStripMenuItem mnuUndelete;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteFromGrid;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.framDogs = new fecherFoundation.FCFrame();
			this.rtbComments = new fecherFoundation.FCRichTextBox();
			this.txtDogName = new fecherFoundation.FCTextBox();
			this.txtYear = new fecherFoundation.FCTextBox();
			this.chkSearchRescue = new fecherFoundation.FCCheckBox();
			this.chkHearingGuid = new fecherFoundation.FCCheckBox();
			this.chkWolfHybrid = new fecherFoundation.FCCheckBox();
			this.chkNeuterSpay = new fecherFoundation.FCCheckBox();
			this.chkDeceased = new fecherFoundation.FCCheckBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtTag = new fecherFoundation.FCTextBox();
			this.txtSticker = new fecherFoundation.FCTextBox();
			this.T2KIssueDate = new Global.T2KDateBox();
			this.Label16 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtCertificateNumber = new fecherFoundation.FCTextBox();
			this.txtRabiesTag = new fecherFoundation.FCTextBox();
			this.T2KDateVaccinated = new Global.T2KDateBox();
			this.t2kExpirationDate = new Global.T2KDateBox();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.T2KDOB = new Global.T2KDateBox();
			this.txtMarkings = new fecherFoundation.FCTextBox();
			this.cmbBreed = new fecherFoundation.FCComboBox();
			this.cmbColor = new fecherFoundation.FCComboBox();
			this.txtSex = new fecherFoundation.FCTextBox();
			this.txtSpayNeuterNumber = new fecherFoundation.FCTextBox();
			this.txtVet = new fecherFoundation.FCTextBox();
			this.Label17 = new fecherFoundation.FCLabel();
			this.Label18 = new fecherFoundation.FCLabel();
			this.lblOwnerName = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.framList = new fecherFoundation.FCFrame();
			this.Grid = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUndelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteFromGrid = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditOrList = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdEditOrList = new fecherFoundation.FCButton();
			this.cmdDeleteFromGrid = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdUndelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framDogs)).BeginInit();
			this.framDogs.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.rtbComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSearchRescue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHearingGuid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkWolfHybrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNeuterSpay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.T2KIssueDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.T2KDateVaccinated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kExpirationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDOB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framList)).BeginInit();
			this.framList.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditOrList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteFromGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUndelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(698, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framList);
			this.ClientArea.Controls.Add(this.framDogs);
			this.ClientArea.Size = new System.Drawing.Size(698, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdUndelete);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdDeleteFromGrid);
			this.TopPanel.Controls.Add(this.cmdEditOrList);
			this.TopPanel.Size = new System.Drawing.Size(698, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdEditOrList, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteFromGrid, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdUndelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(161, 30);
			this.HeaderText.Text = "Deleted Dogs";
			// 
			// framDogs
			// 
			this.framDogs.AppearanceKey = "groupBoxNoBorders";
			this.framDogs.BackColor = System.Drawing.Color.White;
			this.framDogs.Controls.Add(this.rtbComments);
			this.framDogs.Controls.Add(this.txtDogName);
			this.framDogs.Controls.Add(this.txtYear);
			this.framDogs.Controls.Add(this.chkSearchRescue);
			this.framDogs.Controls.Add(this.chkHearingGuid);
			this.framDogs.Controls.Add(this.chkWolfHybrid);
			this.framDogs.Controls.Add(this.chkNeuterSpay);
			this.framDogs.Controls.Add(this.chkDeceased);
			this.framDogs.Controls.Add(this.Frame2);
			this.framDogs.Controls.Add(this.Frame1);
			this.framDogs.Controls.Add(this.T2KDOB);
			this.framDogs.Controls.Add(this.txtMarkings);
			this.framDogs.Controls.Add(this.cmbBreed);
			this.framDogs.Controls.Add(this.cmbColor);
			this.framDogs.Controls.Add(this.txtSex);
			this.framDogs.Controls.Add(this.txtSpayNeuterNumber);
			this.framDogs.Controls.Add(this.txtVet);
			this.framDogs.Controls.Add(this.Label17);
			this.framDogs.Controls.Add(this.Label18);
			this.framDogs.Controls.Add(this.lblOwnerName);
			this.framDogs.Controls.Add(this.Label9);
			this.framDogs.Controls.Add(this.Label8);
			this.framDogs.Controls.Add(this.Label7);
			this.framDogs.Controls.Add(this.Label6);
			this.framDogs.Controls.Add(this.Label5);
			this.framDogs.Controls.Add(this.Label4);
			this.framDogs.Controls.Add(this.Label3);
			this.framDogs.Controls.Add(this.Label2);
			this.framDogs.Controls.Add(this.Label1);
			this.framDogs.Location = new System.Drawing.Point(10, 10);
			this.framDogs.Name = "framDogs";
			this.framDogs.Size = new System.Drawing.Size(647, 796);
			this.framDogs.TabIndex = 2;
			this.framDogs.Visible = false;
			// 
			// rtbComments
			// 
			this.rtbComments.Location = new System.Drawing.Point(20, 692);
			this.rtbComments.Multiline = true;
			this.rtbComments.Name = "rtbComments";
			this.rtbComments.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.rtbComments.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.rtbComments.SelTabCount = null;
			this.rtbComments.Size = new System.Drawing.Size(586, 104);
			this.rtbComments.TabIndex = 44;
			// 
			// txtDogName
			// 
			this.txtDogName.AutoSize = false;
			this.txtDogName.BackColor = System.Drawing.SystemColors.Window;
			this.txtDogName.LinkItem = null;
			this.txtDogName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDogName.LinkTopic = null;
			this.txtDogName.Location = new System.Drawing.Point(191, 58);
			this.txtDogName.Name = "txtDogName";
			this.txtDogName.Size = new System.Drawing.Size(190, 40);
			this.txtDogName.TabIndex = 43;
			// 
			// txtYear
			// 
			this.txtYear.AutoSize = false;
			this.txtYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtYear.LinkItem = null;
			this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYear.LinkTopic = null;
			this.txtYear.Location = new System.Drawing.Point(456, 58);
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(105, 40);
			this.txtYear.TabIndex = 42;
			// 
			// chkSearchRescue
			// 
			this.chkSearchRescue.Location = new System.Drawing.Point(392, 205);
			this.chkSearchRescue.Name = "chkSearchRescue";
			this.chkSearchRescue.Size = new System.Drawing.Size(245, 27);
			this.chkSearchRescue.TabIndex = 39;
			this.chkSearchRescue.Text = "Service - Search and  Rescue";
			// 
			// chkHearingGuid
			// 
			this.chkHearingGuid.Location = new System.Drawing.Point(392, 242);
			this.chkHearingGuid.Name = "chkHearingGuid";
			this.chkHearingGuid.Size = new System.Drawing.Size(142, 27);
			this.chkHearingGuid.TabIndex = 38;
			this.chkHearingGuid.Text = "Hearing / Guide";
			// 
			// chkWolfHybrid
			// 
			this.chkWolfHybrid.Location = new System.Drawing.Point(392, 279);
			this.chkWolfHybrid.Name = "chkWolfHybrid";
			this.chkWolfHybrid.Size = new System.Drawing.Size(121, 27);
			this.chkWolfHybrid.TabIndex = 37;
			this.chkWolfHybrid.Text = "Wolf / Hybrid";
			// 
			// chkNeuterSpay
			// 
			this.chkNeuterSpay.Location = new System.Drawing.Point(392, 168);
			this.chkNeuterSpay.Name = "chkNeuterSpay";
			this.chkNeuterSpay.Size = new System.Drawing.Size(146, 27);
			this.chkNeuterSpay.TabIndex = 36;
			this.chkNeuterSpay.Text = "Neuter / Spayed";
			// 
			// chkDeceased
			// 
			this.chkDeceased.Location = new System.Drawing.Point(392, 316);
			this.chkDeceased.Name = "chkDeceased";
			this.chkDeceased.Size = new System.Drawing.Size(211, 27);
			this.chkDeceased.TabIndex = 35;
			this.chkDeceased.Text = "Current Dog is Deceased";
			// 
			// Frame2
			// 
			this.Frame2.BackColor = System.Drawing.Color.White;
			this.Frame2.Controls.Add(this.txtTag);
			this.Frame2.Controls.Add(this.txtSticker);
			this.Frame2.Controls.Add(this.T2KIssueDate);
			this.Frame2.Controls.Add(this.Label16);
			this.Frame2.Controls.Add(this.Label14);
			this.Frame2.Controls.Add(this.Label15);
			this.Frame2.Location = new System.Drawing.Point(354, 408);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(252, 189);
			this.Frame2.TabIndex = 28;
			this.Frame2.Text = "City Tag And Sticker Number";
			// 
			// txtTag
			// 
			this.txtTag.AutoSize = false;
			this.txtTag.BackColor = System.Drawing.SystemColors.Window;
			this.txtTag.LinkItem = null;
			this.txtTag.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTag.LinkTopic = null;
			this.txtTag.Location = new System.Drawing.Point(119, 28);
			this.txtTag.Name = "txtTag";
			this.txtTag.Size = new System.Drawing.Size(113, 40);
			this.txtTag.TabIndex = 31;
			// 
			// txtSticker
			// 
			this.txtSticker.AutoSize = false;
			this.txtSticker.BackColor = System.Drawing.SystemColors.Window;
			this.txtSticker.LinkItem = null;
			this.txtSticker.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSticker.LinkTopic = null;
			this.txtSticker.Location = new System.Drawing.Point(119, 78);
			this.txtSticker.Name = "txtSticker";
			this.txtSticker.Size = new System.Drawing.Size(113, 40);
			this.txtSticker.TabIndex = 29;
			// 
			// T2KIssueDate
			// 
			this.T2KIssueDate.Location = new System.Drawing.Point(119, 129);
			this.T2KIssueDate.Mask = "00/00/0000";
			this.T2KIssueDate.MaxLength = 10;
			this.T2KIssueDate.Name = "T2KIssueDate";
			this.T2KIssueDate.Size = new System.Drawing.Size(115, 40);
			this.T2KIssueDate.TabIndex = 30;
			// 
			// Label16
			// 
			this.Label16.Location = new System.Drawing.Point(20, 143);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(90, 18);
			this.Label16.TabIndex = 34;
			this.Label16.Text = "ISSUE DATE";
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(20, 42);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(90, 18);
			this.Label14.TabIndex = 33;
			this.Label14.Text = "TAG";
			// 
			// Label15
			// 
			this.Label15.Location = new System.Drawing.Point(20, 92);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(90, 18);
			this.Label15.TabIndex = 32;
			this.Label15.Text = "STICKER";
			// 
			// Frame1
			// 
			this.Frame1.BackColor = System.Drawing.Color.White;
			this.Frame1.Controls.Add(this.txtCertificateNumber);
			this.Frame1.Controls.Add(this.txtRabiesTag);
			this.Frame1.Controls.Add(this.T2KDateVaccinated);
			this.Frame1.Controls.Add(this.t2kExpirationDate);
			this.Frame1.Controls.Add(this.Label13);
			this.Frame1.Controls.Add(this.Label12);
			this.Frame1.Controls.Add(this.Label11);
			this.Frame1.Controls.Add(this.Label10);
			this.Frame1.Location = new System.Drawing.Point(20, 408);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(314, 238);
			this.Frame1.TabIndex = 19;
			this.Frame1.Text = "Rabies Information";
			// 
			// txtCertificateNumber
			// 
			this.txtCertificateNumber.AutoSize = false;
			this.txtCertificateNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtCertificateNumber.LinkItem = null;
			this.txtCertificateNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCertificateNumber.LinkTopic = null;
			this.txtCertificateNumber.Location = new System.Drawing.Point(180, 180);
			this.txtCertificateNumber.Name = "txtCertificateNumber";
			this.txtCertificateNumber.Size = new System.Drawing.Size(113, 40);
			this.txtCertificateNumber.TabIndex = 23;
			// 
			// txtRabiesTag
			// 
			this.txtRabiesTag.AutoSize = false;
			this.txtRabiesTag.BackColor = System.Drawing.SystemColors.Window;
			this.txtRabiesTag.LinkItem = null;
			this.txtRabiesTag.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRabiesTag.LinkTopic = null;
			this.txtRabiesTag.Location = new System.Drawing.Point(180, 30);
			this.txtRabiesTag.Name = "txtRabiesTag";
			this.txtRabiesTag.Size = new System.Drawing.Size(113, 40);
			this.txtRabiesTag.TabIndex = 20;
			// 
			// T2KDateVaccinated
			// 
			this.T2KDateVaccinated.Location = new System.Drawing.Point(180, 80);
			this.T2KDateVaccinated.Mask = "00/00/0000";
			this.T2KDateVaccinated.Name = "T2KDateVaccinated";
			this.T2KDateVaccinated.Size = new System.Drawing.Size(115, 40);
			this.T2KDateVaccinated.TabIndex = 21;
			// 
			// t2kExpirationDate
			// 
			this.t2kExpirationDate.Location = new System.Drawing.Point(180, 130);
			this.t2kExpirationDate.Mask = "00/00/0000";
			this.t2kExpirationDate.Name = "t2kExpirationDate";
			this.t2kExpirationDate.Size = new System.Drawing.Size(115, 40);
			this.t2kExpirationDate.TabIndex = 22;
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(20, 194);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(140, 18);
			this.Label13.TabIndex = 27;
			this.Label13.Text = "CERTIFICATE NUMBER";
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(20, 144);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(122, 18);
			this.Label12.TabIndex = 26;
			this.Label12.Text = "EXPIRATION DATE";
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(20, 94);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(122, 18);
			this.Label11.TabIndex = 25;
			this.Label11.Text = "DATE VACCINATED";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(20, 44);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(90, 18);
			this.Label10.TabIndex = 24;
			this.Label10.Text = "TAG NUMBER";
			// 
			// T2KDOB
			// 
			this.T2KDOB.Location = new System.Drawing.Point(191, 358);
			this.T2KDOB.Mask = "00/00/0000";
			this.T2KDOB.MaxLength = 10;
			this.T2KDOB.Name = "T2KDOB";
			this.T2KDOB.Size = new System.Drawing.Size(115, 40);
			this.T2KDOB.TabIndex = 17;
			// 
			// txtMarkings
			// 
			this.txtMarkings.AutoSize = false;
			this.txtMarkings.BackColor = System.Drawing.SystemColors.Window;
			this.txtMarkings.LinkItem = null;
			this.txtMarkings.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMarkings.LinkTopic = null;
			this.txtMarkings.Location = new System.Drawing.Point(191, 308);
			this.txtMarkings.Name = "txtMarkings";
			this.txtMarkings.Size = new System.Drawing.Size(179, 40);
			this.txtMarkings.TabIndex = 15;
			// 
			// cmbBreed
			// 
			this.cmbBreed.AutoSize = false;
			this.cmbBreed.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBreed.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBreed.FormattingEnabled = true;
			this.cmbBreed.Location = new System.Drawing.Point(191, 258);
			this.cmbBreed.Name = "cmbBreed";
			this.cmbBreed.Size = new System.Drawing.Size(179, 40);
			this.cmbBreed.TabIndex = 14;
			this.cmbBreed.Text = "Combo1";
			// 
			// cmbColor
			// 
			this.cmbColor.AutoSize = false;
			this.cmbColor.BackColor = System.Drawing.SystemColors.Window;
			this.cmbColor.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbColor.FormattingEnabled = true;
			this.cmbColor.Location = new System.Drawing.Point(191, 208);
			this.cmbColor.Name = "cmbColor";
			this.cmbColor.Size = new System.Drawing.Size(179, 40);
			this.cmbColor.TabIndex = 12;
			this.cmbColor.Text = "Combo1";
			// 
			// txtSex
			// 
			this.txtSex.AutoSize = false;
			this.txtSex.BackColor = System.Drawing.SystemColors.Window;
			this.txtSex.LinkItem = null;
			this.txtSex.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSex.LinkTopic = null;
			this.txtSex.Location = new System.Drawing.Point(456, 108);
			this.txtSex.Name = "txtSex";
			this.txtSex.Size = new System.Drawing.Size(105, 40);
			this.txtSex.TabIndex = 9;
			// 
			// txtSpayNeuterNumber
			// 
			this.txtSpayNeuterNumber.AutoSize = false;
			this.txtSpayNeuterNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtSpayNeuterNumber.LinkItem = null;
			this.txtSpayNeuterNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSpayNeuterNumber.LinkTopic = null;
			this.txtSpayNeuterNumber.Location = new System.Drawing.Point(191, 158);
			this.txtSpayNeuterNumber.Name = "txtSpayNeuterNumber";
			this.txtSpayNeuterNumber.Size = new System.Drawing.Size(114, 40);
			this.txtSpayNeuterNumber.TabIndex = 7;
			// 
			// txtVet
			// 
			this.txtVet.AutoSize = false;
			this.txtVet.BackColor = System.Drawing.SystemColors.Window;
			this.txtVet.LinkItem = null;
			this.txtVet.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtVet.LinkTopic = null;
			this.txtVet.Location = new System.Drawing.Point(191, 108);
			this.txtVet.Name = "txtVet";
			this.txtVet.Size = new System.Drawing.Size(190, 40);
			this.txtVet.TabIndex = 5;
			// 
			// Label17
			// 
			this.Label17.Location = new System.Drawing.Point(20, 20);
			this.Label17.Name = "Label17";
			this.Label17.Size = new System.Drawing.Size(50, 18);
			this.Label17.TabIndex = 40;
			this.Label17.Text = "OWNER";
			// 
			// Label18
			// 
			this.Label18.Location = new System.Drawing.Point(22, 667);
			this.Label18.Name = "Label18";
			this.Label18.Size = new System.Drawing.Size(152, 15);
			this.Label18.TabIndex = 45;
			this.Label18.Text = "COMMENTS";
			// 
			// lblOwnerName
			// 
			this.lblOwnerName.Location = new System.Drawing.Point(86, 20);
			this.lblOwnerName.Name = "lblOwnerName";
			this.lblOwnerName.Size = new System.Drawing.Size(244, 18);
			this.lblOwnerName.TabIndex = 41;
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 372);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(100, 18);
			this.Label9.TabIndex = 18;
			this.Label9.Text = "DATE OF BIRTH";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(20, 322);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(66, 18);
			this.Label8.TabIndex = 16;
			this.Label8.Text = "MARKINGS";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(20, 272);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(66, 18);
			this.Label7.TabIndex = 13;
			this.Label7.Text = "BREED";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(20, 222);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(66, 18);
			this.Label6.TabIndex = 11;
			this.Label6.Text = "COLOR";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(399, 122);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(41, 18);
			this.Label5.TabIndex = 10;
			this.Label5.Text = "SEX";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 172);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(140, 18);
			this.Label4.TabIndex = 8;
			this.Label4.Text = "SPAY / NEUTER NUMBER";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 122);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(90, 18);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "VETERINARIAN";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(399, 72);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(41, 18);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "YEAR";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 72);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(66, 18);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "DOG NAME";
			// 
			// framList
			// 
			this.framList.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.framList.AppearanceKey = "groupBoxNoBorders";
			this.framList.BackColor = System.Drawing.Color.White;
			this.framList.Controls.Add(this.Grid);
			this.framList.Location = new System.Drawing.Point(10, 10);
			this.framList.Name = "framList";
			this.framList.Size = new System.Drawing.Size(661, 485);
			this.framList.TabIndex = 0;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(20, 20);
			this.Grid.Name = "Grid";
			this.Grid.OutlineCol = 0;
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(641, 448);
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.Grid.TabIndex = 1;
			this.Grid.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.Grid_BeforeSort);
			this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
			this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuUndelete,
            this.mnuDelete,
            this.mnuDeleteFromGrid,
            this.mnuSepar1,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar2,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuUndelete
			// 
			this.mnuUndelete.Index = 0;
			this.mnuUndelete.Name = "mnuUndelete";
			this.mnuUndelete.Text = "Undelete Dog";
			this.mnuUndelete.Visible = false;
			this.mnuUndelete.Click += new System.EventHandler(this.mnuUndelete_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Enabled = false;
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete Dog Permanently";
			this.mnuDelete.Visible = false;
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuDeleteFromGrid
			// 
			this.mnuDeleteFromGrid.Index = 2;
			this.mnuDeleteFromGrid.Name = "mnuDeleteFromGrid";
			this.mnuDeleteFromGrid.Text = "Delete Dog Permanently";
			this.mnuDeleteFromGrid.Click += new System.EventHandler(this.mnuDeleteFromGrid_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 3;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Enabled = false;
			this.mnuSave.Index = 4;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Enabled = false;
			this.mnuSaveExit.Index = 5;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 6;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 7;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// mnuEditOrList
			// 
			this.mnuEditOrList.Index = -1;
			this.mnuEditOrList.Name = "mnuEditOrList";
			this.mnuEditOrList.Text = "Edit Dog";
			this.mnuEditOrList.Click += new System.EventHandler(this.mnuEditOrList_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Enabled = false;
			this.cmdSave.Location = new System.Drawing.Point(299, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdEditOrList
			// 
			this.cmdEditOrList.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEditOrList.AppearanceKey = "toolbarButton";
			this.cmdEditOrList.Location = new System.Drawing.Point(324, 29);
			this.cmdEditOrList.Name = "cmdEditOrList";
			this.cmdEditOrList.Size = new System.Drawing.Size(70, 24);
			this.cmdEditOrList.TabIndex = 1;
			this.cmdEditOrList.Text = "Edit Dog";
			this.cmdEditOrList.Click += new System.EventHandler(this.mnuEditOrList_Click);
			// 
			// cmdDeleteFromGrid
			// 
			this.cmdDeleteFromGrid.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteFromGrid.AppearanceKey = "toolbarButton";
			this.cmdDeleteFromGrid.Location = new System.Drawing.Point(400, 29);
			this.cmdDeleteFromGrid.Name = "cmdDeleteFromGrid";
			this.cmdDeleteFromGrid.Size = new System.Drawing.Size(165, 24);
			this.cmdDeleteFromGrid.TabIndex = 2;
			this.cmdDeleteFromGrid.Text = "Delete Dog Permanently";
			this.cmdDeleteFromGrid.Click += new System.EventHandler(this.mnuDeleteFromGrid_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(400, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(165, 24);
			this.cmdDelete.TabIndex = 3;
			this.cmdDelete.Text = "Delete Dog Permanently";
			this.cmdDelete.Visible = false;
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdUndelete
			// 
			this.cmdUndelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdUndelete.AppearanceKey = "toolbarButton";
			this.cmdUndelete.Location = new System.Drawing.Point(571, 29);
			this.cmdUndelete.Name = "cmdUndelete";
			this.cmdUndelete.Size = new System.Drawing.Size(100, 24);
			this.cmdUndelete.TabIndex = 4;
			this.cmdUndelete.Text = "Undelete Dog";
			this.cmdUndelete.Visible = false;
			this.cmdUndelete.Click += new System.EventHandler(this.mnuUndelete_Click);
			// 
			// frmDeletedDogs
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(698, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmDeletedDogs";
			this.Text = "Deleted Dogs";
			this.Load += new System.EventHandler(this.frmDeletedDogs_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDeletedDogs_KeyDown);
			this.Resize += new System.EventHandler(this.frmDeletedDogs_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framDogs)).EndInit();
			this.framDogs.ResumeLayout(false);
			this.framDogs.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.rtbComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSearchRescue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHearingGuid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkWolfHybrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNeuterSpay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeceased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.T2KIssueDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.T2KDateVaccinated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kExpirationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KDOB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framList)).EndInit();
			this.framList.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditOrList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteFromGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUndelete)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdDeleteFromGrid;
		private FCButton cmdEditOrList;
		private FCButton cmdDelete;
		private FCButton cmdUndelete;
	}
}