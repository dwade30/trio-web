//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCK0000
{
	public partial class frmSelectDog : BaseForm
	{
		public frmSelectDog()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSelectDog InstancePtr
		{
			get
			{
				return (frmSelectDog)Sys.GetInstance(typeof(frmSelectDog));
			}
		}

		protected frmSelectDog _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLID = 0;
		const int CNSTGRIDCOLFIRST = 1;
		const int CNSTGRIDCOLLAST = 2;
		const int CNSTGRIDCOLDOGNAME = 3;
		const int CNSTGRIDCOLSTREETNUMBER = 4;
		const int CNSTGRIDCOLSTREETNAME = 5;
		const int CNSTGRIDCOLDOGID = 1;
		private int lngReturn;

		public int Init()
		{
			int Init = 0;
			Init = 0;
			lngReturn = 0;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void frmSelectDog_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmSelectDog_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectDog properties;
			//frmSelectDog.FillStyle	= 0;
			//frmSelectDog.ScaleWidth	= 9300;
			//frmSelectDog.ScaleHeight	= 7680;
			//frmSelectDog.LinkTopic	= "Form2";
			//frmSelectDog.LockControls	= -1  'True;
			//frmSelectDog.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void LoadGrid()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			bool boolUse = false;
			string strSQL;
			Grid.Rows = 1;
			// strSQL = "select *, DogOwner.ID as DogOwnerID, DogInfo.ID as DogNumber from doginfo inner join dogowner on (doginfo.ownernum = dogowner.id) inner join " & rsLoad.CurrentPrefix & "CentralParties.dbo.PartyAddressAndPhoneView on (dogowner.partyid = " & rsLoad.CurrentPrefix & "CentralParties.dbo.PartyAddressAndPhoneView.PartyID) "
			strSQL = "select *, DogOwner.ID as DogOwnerID, DogInfo.ID as DogNumber from doginfo inner join dogowner on (doginfo.ownernum = dogowner.id) inner join " + rsLoad.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView on (dogowner.partyid = " + rsLoad.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView.PartyID) ";
			// strSQL = strSQL & " where doginfo.ownernum > 0 and dogowner.deleted <> 1  order by lastname,firstname,locationstr,locationnum,dogname"
			strSQL += " where doginfo.ownernum > 0 and dogowner.deleted <> 1  order by lastname,firstname,dogname";
			// Call rsLoad.OpenRecordset("select doginfo.ID as dognumber, * from dogowner inner join doginfo on (doginfo.ownernum = dogowner.ID) where dogowner.deleted <> 1 ORDER by lastname,firstname,locationstr,locationnum,dogname", "twck0000.vb1")
			rsLoad.OpenRecordset(strSQL, "twck0000.vb1");
			while (!rsLoad.EndOfFile())
			{
				boolUse = true;
				for (x = 0; x <= (frmOnlineDogs.InstancePtr.Grid.Rows - 1); x++)
				{
					if (Conversion.Val(frmOnlineDogs.InstancePtr.Grid.TextMatrix(x, CNSTGRIDCOLDOGID)) == rsLoad.Get_Fields_Int32("dognumber"))
					{
						boolUse = false;
						break;
					}
				}
				// x
				if (boolUse)
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLID, FCConvert.ToString(rsLoad.Get_Fields_Int32("dognumber")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLAST, FCConvert.ToString(rsLoad.Get_Fields_String("lastname")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFIRST, FCConvert.ToString(rsLoad.Get_Fields_String("firstname")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDOGNAME, FCConvert.ToString(rsLoad.Get_Fields_String("dogname")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSTREETNUMBER, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_String("locationnum"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSTREETNAME, FCConvert.ToString(rsLoad.Get_Fields_String("locationstr")));
				}
				rsLoad.MoveNext();
			}
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLFIRST, "First Name");
			Grid.TextMatrix(0, CNSTGRIDCOLLAST, "Last Name");
			Grid.TextMatrix(0, CNSTGRIDCOLDOGNAME, "Dog Name");
			Grid.TextMatrix(0, CNSTGRIDCOLSTREETNUMBER, "ST #");
			Grid.TextMatrix(0, CNSTGRIDCOLSTREETNAME, "Street Name");
			Grid.ColDataType(CNSTGRIDCOLSTREETNAME, FCGrid.DataTypeSettings.flexDTString);
			Grid.ColDataType(CNSTGRIDCOLSTREETNUMBER, FCGrid.DataTypeSettings.flexDTLong);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLFIRST, FCConvert.ToInt32(0.30 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLLAST, FCConvert.ToInt32(0.23 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDOGNAME, FCConvert.ToInt32(0.15 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSTREETNUMBER, FCConvert.ToInt32(0.05 * GridWidth));
            Grid.ColWidth(CNSTGRIDCOLSTREETNAME, FCConvert.ToInt32(0.27 * GridWidth));
        }

		private void frmSelectDog_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterSort(object sender, EventArgs e)
		{
			int Order = FCConvert.ToInt32(Grid.SortOrder);
			if (Grid.Col == CNSTGRIDCOLSTREETNUMBER)
			{
				Grid.Col = CNSTGRIDCOLSTREETNAME;
				if (Order == 1)
				{
					Grid.Sort = FCGrid.SortSettings.flexSortStringAscending;
				}
				else
				{
					Grid.Sort = FCGrid.SortSettings.flexSortStringDescending;
				}
			}
		}

		private void Grid_BeforeSort(object sender, DataGridViewCellMouseEventArgs e)
		{
			int Order = FCConvert.ToInt32(Grid.SortOrder);
			if (Grid.Col == CNSTGRIDCOLSTREETNAME)
			{
				Grid.Col = CNSTGRIDCOLSTREETNUMBER;
				if (Order == 1)
				{
					Grid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				}
				else
				{
					Grid.Sort = FCGrid.SortSettings.flexSortNumericDescending;
				}
				Grid.Col = CNSTGRIDCOLSTREETNAME;
				if (Order == 1)
				{
					Grid.Sort = FCGrid.SortSettings.flexSortStringAscending;
				}
				else
				{
					Grid.Sort = FCGrid.SortSettings.flexSortStringDescending;
				}
			}
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow > 0)
			{
				lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.MouseRow, CNSTGRIDCOLID))));
				Close();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

        private void cmdSaveExit_Click(object sender, EventArgs e)
        {
           
        }
    }
}
