//Fecher vbPorter - Version 1.0.0.90
using fecherFoundation;
using Global;
using System;
using System.IO;
using Wisej.Web;

namespace TWUT0000
{
    public class cUTOutPrinter
    {

        //=========================================================

        private string strRet1 = "";
        private string strRet2 = "";
        private string strRet3 = "";
        private string strRet4 = "";
        private bool boolUse2Periods;

        const string cnstWaterCode = "W";
        const string cnstSewerCode = "S";

        const short maxAdjustments = 5;

        private bool boolChpt660;
        private bool boolInclChangeOutCons;
        private bool boolSendCopies;
        int lngUnitsToShow;

        public bool BuildOutPrintingFile(string strSQL, cUTBill.UTBillType intType, int lngRK, DateTime dtStatementDate, bool blnExcludeZeroBills, bool boolSendCopiesIfOwnerIsDifferent)
        {
            bool BuildOutPrintingFile = false;
            bool boolOpen = false;
            try
            {   // On Error GoTo ERROR_HANDLER
                // This function will create a flat file with all of the bill amounts for the RK and books selected
                boolSendCopies = boolSendCopiesIfOwnerIsDifferent;


                clsDRWrapper rsData = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsRK = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsMaster = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsCat = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsMeter = new/*AsNew*/ clsDRWrapper();

                string strCatString = "";

                short intAmountFieldLength;
                int lngCT;

                bool boolSameOwner = false;
                bool boolShowOwnerInfo;
                bool boolWBillOwner = false;
                bool boolSBillOwner = false;
                bool boolSplitBill;

                string dblWSplit1 = "";
                string dblWSplit2 = "";
                string dblSSplit1 = "";
                string dblSSplit2 = "";

                string strEBillType = "";
                bool blnContinue = false;

                StreamWriter ts;

                intAmountFieldLength = 8;

                try
                {
                    rsRK.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
                    if (!rsRK.EndOfFile())
                    {
                        boolWBillOwner = FCConvert.ToBoolean(rsRK.Get_Fields("WBillToOwner"));
                        boolSBillOwner = FCConvert.ToBoolean(rsRK.Get_Fields("SBillToOwner"));

                        strRet1 = FCConvert.ToString(rsRK.Get_Fields("UTReturnAddress1"));
                        strRet2 = FCConvert.ToString(rsRK.Get_Fields("UTReturnAddress2"));
                        strRet3 = FCConvert.ToString(rsRK.Get_Fields("UTReturnAddress3"));
                        strRet4 = FCConvert.ToString(rsRK.Get_Fields("UTReturnAddress4"));

                        if (fecherFoundation.Conversion.Val(FCConvert.ToString(rsRK.Get_Fields("ReadingUnitsOnBill"))) != 0)
                        {
                            lngUnitsToShow = (int)Math.Round(fecherFoundation.Conversion.Val(FCConvert.ToString(rsRK.Get_Fields("ReadingUnitsOnBill"))));
                        }
                        else
                        {
                            lngUnitsToShow = 1;
                        }

                        boolChpt660 = FCConvert.ToBoolean(rsRK.Get_Fields("OutprintChapt660"));
                        boolInclChangeOutCons = FCConvert.ToBoolean(rsRK.Get_Fields("OutPrintIncludeChangeOutCons")); // trout-1118
                    }
                    else
                    {
                        boolWBillOwner = true;
                        boolSBillOwner = true;
                    }

                    rsMeter.OpenRecordset("SELECT * FROM MeterTable", modExtraModules.strUTDatabase);
                    rsCat.OpenRecordset("SELECT * FROM Category", modExtraModules.strUTDatabase);


                    cUTBillRateInformation currRate;
                    currRate = GetRateInformation(lngRK);

                    rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Creating Outprinting File", true, rsData.RecordCount(), true);

                    var fileName = GetTempFileName("csv");
                    using (ts = File.CreateText(fileName))
                    {

                        boolOpen = true;
                        WriteHeader(ref ts);

                        cUTBill currentBill;
                        while (!rsData.EndOfFile())
                        {
                            //////Application.DoEvents();
                            currentBill = new cUTBill();
                            currentBill.RateInformation.PeriodDescription = currRate.PeriodDescription;
                            currentBill.RateInformation.DueDate = currRate.DueDate;
                            currentBill.RateInformation.EndDate = currRate.EndDate;
                            currentBill.RateInformation.InterestStartDate = currRate.InterestStartDate;
                            currentBill.RateInformation.StartDate = currRate.StartDate;


                            frmWait.InstancePtr.IncrementProgress();

                            // MAL@20080806: Check for IConnect E-bill Option
                            // Tracker Reference: 10680
                            if (modGlobalConstants.Statics.gboolIC)
                            {
                                if (modUTBilling.AcceptsEBills(rsData.Get_Fields("AccountKey"), ref strEBillType))
                                {
                                    if (strEBillType == "B")
                                    {
                                        // Continue
                                        blnContinue = true;
                                    }
                                    else
                                    {
                                        // E-Bill Only - Don't Include
                                        blnContinue = false;
                                    }
                                }
                                else
                                {
                                    // Continue - Paper Bills
                                    blnContinue = true;
                                }
                            }
                            else
                            {
                                // Continue
                                blnContinue = true;
                            }


                            if (blnContinue)
                            {
                                currentBill.StatementDate = dtStatementDate;
                                FillBill(ref currentBill, ref rsData);
                                boolSameOwner = false;
                                boolSplitBill = false;
                                rsMaster.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsData.Get_Fields("AccountKey"),
                                    modExtraModules.strUTDatabase);
                                if (!rsMaster.EndOfFile())
                                {
                                    boolSameOwner = FCConvert.ToBoolean(rsMaster.Get_Fields("SameBillOwner"));

                                    // trout-718 08-02-2011 kgk  Bill to owner / tenant option by account and service
                                    boolWBillOwner = FCConvert.ToBoolean(rsData.Get_Fields("WBillOwner"));
                                    boolSBillOwner = FCConvert.ToBoolean(rsData.Get_Fields("SBillOwner"));

                                    if (fecherFoundation.Strings.Trim(
                                            FCConvert.ToString(rsMaster.Get_Fields("StreetNumber"))) != "")
                                    {
                                        currentBill.Location = fecherFoundation.Strings.Trim(
                                            rsMaster.Get_Fields("StreetNumber") + " " + rsMaster.Get_Fields("StreetName"));
                                    }
                                    else
                                    {
                                        currentBill.Location =
                                            fecherFoundation.Strings.Trim(
                                                FCConvert.ToString(rsMaster.Get_Fields("StreetName")));
                                    }

                                    currentBill.Message =
                                        fecherFoundation.Strings.Trim(
                                            FCConvert.ToString(rsMaster.Get_Fields("BillMessage")));

                                    if (rsMaster.Get_Fields("ImpervSurfArea").ToString() != "")
                                    {
                                        currentBill.ImperviousSurfaceArea = rsMaster.Get_Fields_Double("ImpervSurfArea");
                                    }
                                    else
                                    {
                                        currentBill.ImperviousSurfaceArea = 0;
                                    }
                                }

                                if (intType == cUTBill.UTBillType.Sewer)
                                {
                                    rsCat.FindFirstRecord("Category", rsData.Get_Fields("SCat"));
                                    if (rsCat.NoMatch)
                                    {
                                        rsCat.FindFirstRecord("Category", rsData.Get_Fields("WCat"));
                                        if (rsCat.NoMatch)
                                        {
                                            strCatString = fecherFoundation.Strings.StrDup(25, " ");
                                        }
                                        else
                                        {
                                            strCatString = FCConvert.ToString(
                                                modGlobalFunctions.PadStringWithSpaces(rsCat.Get_Fields("LongDescription"),
                                                    25));
                                        }
                                    }
                                    else
                                    {
                                        strCatString = FCConvert.ToString(
                                            modGlobalFunctions.PadStringWithSpaces(rsCat.Get_Fields("LongDescription"),
                                                25));
                                    }
                                }
                                else
                                {
                                    rsCat.FindFirstRecord("Code", rsData.Get_Fields("WCat"));
                                    if (rsCat.NoMatch)
                                    {
                                        rsCat.FindFirstRecord("Code", rsData.Get_Fields("SCat"));
                                        if (rsCat.NoMatch)
                                        {
                                            strCatString = "";
                                        }
                                        else
                                        {
                                            strCatString =
                                                fecherFoundation.Strings.Trim(
                                                    FCConvert.ToString(rsCat.Get_Fields("LongDescription")));
                                        }
                                    }
                                    else
                                    {
                                        strCatString =
                                            fecherFoundation.Strings.Trim(
                                                FCConvert.ToString(rsCat.Get_Fields("LongDescription")));
                                    }
                                }

                                currentBill.Category = fecherFoundation.Strings.Trim(strCatString);

                                if (!boolSameOwner)
                                {
                                    if (modUTFunctions.Statics.TownService == "B")
                                    {
                                        if (boolWBillOwner == boolSBillOwner)
                                        {
                                            boolSameOwner = true;
                                        }
                                    }
                                }



                                // Water

                                cUTBillUtility waterDetail;
                                if ((int)intType != 1)
                                {
                                    waterDetail = CreateWaterDetail(ref rsData, dtStatementDate);
                                    currentBill.SetUtilityChargesForType(cnstWaterCode, waterDetail);
                                }
                                else
                                {
                                    waterDetail = new cUTBillUtility();
                                }

                                cUTBillUtility sewerDetail;
                                if (intType != 0)
                                {
                                    sewerDetail = CreateSewerDetail(ref rsData, dtStatementDate);
                                    currentBill.SetUtilityChargesForType(cnstSewerCode, sewerDetail);
                                }
                                else
                                {
                                    sewerDetail = new cUTBillUtility();
                                }


                                rsMeter.OpenRecordset(
                                    "SELECT * FROM MeterTable WHERE ID = " + rsData.Get_Fields("MeterKey"),
                                    modExtraModules.strUTDatabase);
                                if (!rsMeter.EndOfFile())
                                {
                                    for (lngCT = 1; lngCT <= 5; lngCT++)
                                    {
                                        if (FCConvert.ToBoolean(rsMeter.Get_Fields("UseRate" + FCConvert.ToString(lngCT))))
                                        {
                                            if (Convert.ToInt32(
                                                    rsMeter.Get_Fields("WaterType" + FCConvert.ToString(lngCT))) == 3)
                                            {
                                                waterDetail.UnitCount =
                                                    rsMeter.Get_Fields("WaterAmount" + FCConvert.ToString(lngCT));

                                            }

                                            if (Convert.ToInt32(
                                                    rsMeter.Get_Fields("SewerType" + FCConvert.ToString(lngCT))) == 3)
                                            {
                                                sewerDetail.UnitCount =
                                                    rsMeter.Get_Fields("SewerAmount" + FCConvert.ToString(lngCT));
                                            }
                                        }
                                    }
                                }



                                // MAL@20071106
                                if (Convert.ToInt32(rsData.Get_Fields("SewerOverrideCons")) != 0)
                                {
                                    sewerDetail.Consumption = rsData.Get_Fields("SewerOverrideCons");
                                }
                                else
                                {
                                    sewerDetail.Consumption = currentBill.GetTotalConsumption();
                                }

                                if (Convert.ToInt32(rsData.Get_Fields("WaterOverrideCons")) != 0)
                                {
                                    waterDetail.Consumption = rsData.Get_Fields("WaterOverrideCons");
                                }
                                else
                                {
                                    waterDetail.Consumption = currentBill.GetTotalConsumption();
                                }


                                // If Not boolSplitBill Or ((gboolPayWaterFirst And Not boolSecondPass) Or (Not gboolPayWaterFirst And boolSecondPass)) Then
                                waterDetail.ConsumptionAmount = FCConvert.ToDouble(rsData.Get_Fields("WConsumptionAmount"));
                                waterDetail.UnitsAmount = FCConvert.ToDouble(rsData.Get_Fields("WUnitsAmount"));
                                waterDetail.FlatAmount = FCConvert.ToDouble(rsData.Get_Fields("WFlatAmount"));
                                waterDetail.Adjustment = FCConvert.ToDouble(rsData.Get_Fields("WAdjustAmount") +
                                                         rsData.Get_Fields("WDEAdjustAmount") +
                                                         rsData.Get_Fields("WMiscAmount"));
                                // End If

                                // If Not boolSplitBill Or ((gboolPayWaterFirst And boolSecondPass) Or (Not gboolPayWaterFirst And Not boolSecondPass)) Then
                                sewerDetail.ConsumptionAmount = FCConvert.ToDouble(rsData.Get_Fields("SConsumptionAmount"));
                                sewerDetail.UnitsAmount = FCConvert.ToDouble(rsData.Get_Fields("SUnitsAmount"));
                                sewerDetail.FlatAmount = FCConvert.ToDouble(rsData.Get_Fields("SFlatAmount"));
                                sewerDetail.Adjustment = FCConvert.ToDouble(rsData.Get_Fields("SAdjustAmount") +
                                                         rsData.Get_Fields("SDEAdjustAmount") +
                                                         rsData.Get_Fields("SMiscAmount"));
                                // End If

                                if (waterDetail.Adjustment != 0)
                                {
                                    ItemizeAdjustments(rsData.Get_Fields("Bill"), ref waterDetail);

                                    if (rsData.Get_Fields("WDEAdjustAmount") > 0 || rsData.Get_Fields("WMiscAmount") > 0)
                                    {
                                        waterDetail.AddAdjustment("Misc Adj",
                                            FCUtils.Round(
                                                (rsData.Get_Fields("WDEAdjustAmount") + rsData.Get_Fields("WMiscAmount")) *
                                                100, 0));
                                    }
                                }

                                if (sewerDetail.Adjustment != 0)
                                {
                                    ItemizeAdjustments(rsData.Get_Fields("Bill"), ref sewerDetail);

                                    if (rsData.Get_Fields("SDEAdjustAmount") > 0 || rsData.Get_Fields("SMiscAmount") > 0)
                                    {
                                        sewerDetail.AddAdjustment("Misc Adj",
                                            FCUtils.Round(
                                                (rsData.Get_Fields("SDEAdjustAmount") + rsData.Get_Fields("SMiscAmount")) *
                                                100, 0));
                                    }
                                }



                                cUTBillUtility blankSewerDetail = new /*AsNew*/ cUTBillUtility();
                                blankSewerDetail.UtilityType = "S";
                                cUTBillUtility blankWaterDetail = new /*AsNew*/ cUTBillUtility();
                                blankWaterDetail.UtilityType = "W";

                                boolSameOwner = currentBill.TenantIsSameAsOwner();
                                boolWBillOwner = currentBill.WaterBillToOwner;
                                boolSBillOwner = currentBill.SewerBillToOwner;

                                if (boolSameOwner || boolSendCopies)
                                {
                                    currentBill.SetUtilityChargesForType(cnstWaterCode, waterDetail);
                                    currentBill.SetUtilityChargesForType(cnstSewerCode, sewerDetail);
                                    if (!blnExcludeZeroBills || currentBill.GetOverallTotalDue() > 0)
                                    {
                                        currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Owner);
                                        ts.WriteLine(CreateBillLine(ref currentBill));
                                        if (boolSendCopies && !boolSameOwner)
                                        {
                                            currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Tenant);
                                            ts.WriteLine(CreateBillLine(ref currentBill));
                                        }
                                    }
                                }
                                else
                                {
                                    if (modUTFunctions.Statics.TownService == "B")
                                    {
                                        if (boolWBillOwner != boolSBillOwner)
                                        {
                                            if (boolWBillOwner)
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Owner);
                                            }
                                            else
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Tenant);
                                            }

                                            currentBill.SetUtilityChargesForType(cnstWaterCode, waterDetail);
                                            currentBill.SetUtilityChargesForType(cnstSewerCode, blankSewerDetail);
                                            if (!blnExcludeZeroBills || currentBill.GetOverallTotalDue() > 0)
                                            {
                                                ts.WriteLine(CreateBillLine(ref currentBill));
                                            }

                                            if (boolSBillOwner)
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Owner);
                                            }
                                            else
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Tenant);
                                            }

                                            currentBill.SetUtilityChargesForType(cnstWaterCode, blankWaterDetail);
                                            currentBill.SetUtilityChargesForType(cnstSewerCode, sewerDetail);
                                            if (!blnExcludeZeroBills || currentBill.GetOverallTotalDue() > 0)
                                            {
                                                ts.WriteLine(CreateBillLine(ref currentBill));
                                            }
                                        }
                                        else
                                        {
                                            currentBill.SetUtilityChargesForType(cnstWaterCode, waterDetail);
                                            currentBill.SetUtilityChargesForType(cnstSewerCode, sewerDetail);
                                            if (!blnExcludeZeroBills || currentBill.GetOverallTotalDue() > 0)
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Tenant);
                                                ts.WriteLine(CreateBillLine(ref currentBill));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (FCConvert.ToString(rsData.Get_Fields("Service")) == "W")
                                        {
                                            if (boolWBillOwner)
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Owner);
                                            }
                                            else
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Tenant);
                                            }

                                            currentBill.SetUtilityChargesForType(cnstWaterCode, waterDetail);
                                            currentBill.SetUtilityChargesForType(cnstSewerCode, blankSewerDetail);
                                        }
                                        else
                                        {
                                            if (boolSBillOwner)
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Owner);
                                            }
                                            else
                                            {
                                                currentBill.SetPartyToMailTo(cUTBill.UTMailToParty.Tenant);
                                            }

                                            currentBill.SetUtilityChargesForType(cnstWaterCode, blankWaterDetail);
                                            currentBill.SetUtilityChargesForType(cnstSewerCode, sewerDetail);
                                        }

                                        if (!blnExcludeZeroBills || currentBill.GetOverallTotalDue() > 0)
                                        {
                                            ts.WriteLine(CreateBillLine(ref currentBill));
                                        }
                                    }
                                }
                            }

                            rsData.MoveNext();
                        }

                        frmWait.InstancePtr.Unload();
                        boolOpen = false;
                        ts.Close();
                        FCUtils.Download(fileName, "TWUTBillExport.csv");
                    }

                    if (MessageBox.Show("Would you like to see the validation report in full detail?", "Validation Report", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        rptOutprintingFile.InstancePtr.Init(true, true, fileName);
                    }
                    else
                    {
                        rptOutprintingFile.InstancePtr.Init(false, true, fileName);
                    }

                    frmReportViewer.InstancePtr.Init(rptOutprintingFormat.InstancePtr);
                    //////Application.DoEvents();
                    MessageBox.Show("TWUTBillExport.CSV has been created successfully.", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                finally
                {
                    rsData.DisposeOf();
                    rsCat.DisposeOf();
                    rsMaster.DisposeOf();
                    rsMeter.DisposeOf();
                    rsRK.DisposeOf();
                }

                return BuildOutPrintingFile;
            }
            catch (Exception ex)
            {
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Error Building Outprinting File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return BuildOutPrintingFile;
        }


        private void WriteHeader(ref StreamWriter ts)
        {
            // Build a header row for Wise with Muni Name and Program Version starting with an

            ts.WriteLine(CreateFileHeader());
            ts.WriteLine(CreateRecordHeader());

        }

        private string CreateFileHeader()
        {
            string CreateFileHeader = "";
            string strHeader;
            strHeader = WrapString("TRIOHEADER") + ",";
            strHeader += WrapString("Name : " + modGlobalConstants.Statics.MuniName) + ",";
            strHeader += WrapString("UT Version : " + fecherFoundation.Strings.Trim(App.Major + "." + App.Minor + "." + App.Revision)) + ",";
            strHeader += WrapString(strRet1) + ",";
            strHeader += WrapString(strRet2) + ",";
            strHeader += WrapString(strRet3) + ",";
            strHeader += WrapString(strRet4);
            CreateFileHeader = strHeader;
            return CreateFileHeader;
        }

        private string CreateRecordHeader()
        {
            string CreateRecordHeader = "";
            string strHeader;

            // Print header row
            strHeader = WrapString("Account") + ",";
            strHeader += WrapString("Billed Name") + ",";
            strHeader += WrapString("Billed Name 2") + ",";
            strHeader += WrapString("Address 1") + ",";
            strHeader += WrapString("Address 2") + ",";

            strHeader += WrapString("Address 3") + ",";
            strHeader += WrapString("City") + ",";
            strHeader += WrapString("State") + ",";
            strHeader += WrapString("Zip") + ",";
            strHeader += WrapString("Zip 4") + ",";
            strHeader += WrapString("Category") + ",";
            strHeader += WrapString("Map Lot") + ",";
            strHeader += WrapString("Location") + ",";
            strHeader += WrapString("Bill Message") + ",";
            strHeader += WrapString("Statement Date") + ",";
            strHeader += WrapString("Billing Period") + ",";
            strHeader += WrapString("Reading Date") + ",";
            strHeader += WrapString("Start Date") + ",";
            strHeader += WrapString("End Date") + ",";
            strHeader += WrapString("Interest Date") + ",";
            strHeader += WrapString("Total Overall Current") + ",";
            strHeader += WrapString("Total Overall Past Due") + ",";
            strHeader += WrapString("Overall Total") + ",";
            strHeader += WrapString("Book") + ",";
            strHeader += WrapString("Sequence") + ",";
            strHeader += WrapString("Water Consumption") + ",";
            strHeader += WrapString("Water Unit Amount") + ",";
            strHeader += WrapString("Water Consumption Total") + ",";
            strHeader += WrapString("Water Units Total") + ",";
            strHeader += WrapString("Water Flat Total") + ",";
            strHeader += WrapString("Water Adjustment Total") + ",";
            strHeader += WrapString("Water Total w/o Adjs") + ",";
            strHeader += WrapString("Water Regular") + ",";
            strHeader += WrapString("Water Tax") + ",";
            strHeader += WrapString("Water Past Due") + ",";
            strHeader += WrapString("Water Credits") + ",";
            strHeader += WrapString("Water Total Amount Due") + ",";
            strHeader += WrapString("Water Interest") + ",";
            strHeader += WrapString("Water Lien Amount") + ",";
            strHeader += WrapString("Water Current Due") + ",";
            strHeader += WrapString("Total Water Past Due") + ",";
            strHeader += WrapString("Sewer Consumption") + ",";
            strHeader += WrapString("Sewer Unit Amount") + ",";
            strHeader += WrapString("Sewer Consumption Total") + ",";
            strHeader += WrapString("Sewer Units Total") + ",";
            strHeader += WrapString("Sewer Flat Total") + ",";
            strHeader += WrapString("Sewer Adjustment Total") + ",";
            strHeader += WrapString("Sewer Total w/o Adjs") + ",";
            strHeader += WrapString("Sewer Regular") + ",";
            strHeader += WrapString("Sewer Tax") + ",";
            strHeader += WrapString("Sewer Past Due") + ",";
            strHeader += WrapString("Sewer Credits") + ",";
            strHeader += WrapString("Sewer Total Amount Due") + ",";
            strHeader += WrapString("Sewer Interest") + ",";
            strHeader += WrapString("Sewer Lien Amount") + ",";
            strHeader += WrapString("Sewer Current Due") + ",";
            strHeader += WrapString("Total Sewer Past Due") + ",";
            strHeader += WrapString("Meter 1 Consumption") + ",";
            strHeader += WrapString("Meter 1 Previous") + ",";
            strHeader += WrapString("Meter 1 Current") + ",";
            strHeader += WrapString("Meter 2 Consumption") + ",";
            strHeader += WrapString("Meter 2 Previous") + ",";
            strHeader += WrapString("Meter 2 Current") + ",";
            strHeader += WrapString("Meter 3 Consumption") + ",";
            strHeader += WrapString("Meter 3 Previous") + ",";
            strHeader += WrapString("Meter 3 Current") + ",";
            strHeader += WrapString("Amount A") + ",";
            strHeader += WrapString("Amount B") + ","; // split 2 for water
            strHeader += WrapString("Amount C") + ","; // split 1 for sewer
            strHeader += WrapString("Amount D");
            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
            {
                strHeader = AppendToLine(ref strHeader, "Note");
            }
            else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
            {
                strHeader = AppendToLine(ref strHeader, "Actual Consumption Amount");
            }

            strHeader = AppendToLine(ref strHeader, "Due Date");

            if (boolChpt660)
            {
                // kgk trout-764 & 765 10-18-2011  Write the estimated indicator and meter changeout indicator
                // Based on an option in Customize or a separate Outprinting File option in the combobox?
                strHeader = AppendToLine(ref strHeader, "Meter 1 Est/Act");
                strHeader = AppendToLine(ref strHeader, "Meter 1 Changed Out");
                strHeader = AppendToLine(ref strHeader, "Meter 2 Est/Act");
                strHeader = AppendToLine(ref strHeader, "Meter 2 Changed Out");
                strHeader = AppendToLine(ref strHeader, "Meter 3 Est/Act");
                strHeader = AppendToLine(ref strHeader, "Meter 3 Changed Out");

                // Itemize Adjustments
                // We can loop through the adjust table and write all of the adjustments for the service(s)
                // or we can do Adj1,Desc1,Adj2,Desc2,...
                short lngCT;
                for (lngCT = 1; lngCT <= maxAdjustments; lngCT++)
                {
                    //////Application.DoEvents();
                    strHeader = AppendToLine(ref strHeader, "Water Adjustment " + FCConvert.ToString(lngCT) + " Descr");
                    strHeader = AppendToLine(ref strHeader, "Water Adjustment " + FCConvert.ToString(lngCT) + " Amount");
                }
                for (lngCT = 1; lngCT <= maxAdjustments; lngCT++)
                {
                    //////Application.DoEvents();
                    strHeader = AppendToLine(ref strHeader, "Sewer Adjustment " + FCConvert.ToString(lngCT) + " Descr");
                    strHeader = AppendToLine(ref strHeader, "Sewer Adjustement " + FCConvert.ToString(lngCT) + " Amount");
                }
            }

            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
            {
                strHeader = AppendToLine(ref strHeader, "Owner Name");
                strHeader = AppendToLine(ref strHeader, "Owner Name 2");
            }
            else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
            {
                strHeader = AppendToLine(ref strHeader, "Impervious Surface");
                strHeader = AppendToLine(ref strHeader, "Previous Reading Date");
            }

            // kjr 11/7/16 trout-1118
            if (boolInclChangeOutCons)
            {
                strHeader = AppendToLine(ref strHeader, "Meter 1 Change Out Consumption");
                strHeader = AppendToLine(ref strHeader, "Meter 2 Change Out Consumption");
                strHeader = AppendToLine(ref strHeader, "Meter 3 Change Out Consumption");
            }


            CreateRecordHeader = strHeader;
            return CreateRecordHeader;
        }

        private string WrapString(string strText)
        {
            string WrapString = "";
            WrapString = FCConvert.ToString(Convert.ToChar(34)) + strText + FCConvert.ToString(Convert.ToChar(34));
            return WrapString;
        }

        // vbPorter upgrade warning: strText As string	OnWrite(string, double)
        private string AppendToLine(ref string strLine, string strText)
        {
            string AppendToLine = "";
            if (strLine != "")
            {
                AppendToLine = strLine + "," + WrapString(strText);
            }
            else
            {
                AppendToLine = WrapString(strText);
            }
            return AppendToLine;
        }

        private cUTBillUtility CreateWaterDetail(ref clsDRWrapper rsData, DateTime dtStatementDate)
        {
            cUTBillUtility CreateWaterDetail = null;
            cUTBillUtility waterDetail = new cUTBillUtility();
            double dblPastDueCurrentInterest = 0;
            double dblPastDueInterest = 0;
            double dblPastDueCurrentLienInterest = 0;
            double dblPreLienInterest = 0;

            // This will get the current bill amount
            var wCostOwed = rsData.Get_Fields("WCostOwed");
            waterDetail.Regular = rsData.Get_Fields("WPrinOwed") + rsData.Get_Fields("WIntOwed") + wCostOwed;
            waterDetail.TaxOwed = rsData.Get_Fields("WTaxOwed");
            waterDetail.PrincipalPaid = rsData.Get_Fields("WPrinPaid") + rsData.Get_Fields("WTaxPaid") + rsData.Get_Fields("WIntPaid") + rsData.Get_Fields_Double("WCostPaid");
            waterDetail.CurrentDue = (rsData.Get_Fields("WPrinOwed") + rsData.Get_Fields("WTaxOwed") + rsData.Get_Fields("WIntOwed") + wCostOwed - rsData.Get_Fields_Double("WCostAdded") - rsData.Get_Fields_Double("WIntAdded") + waterDetail.Interest) - (waterDetail.PrincipalPaid);
            // dblOverallTotalCurrent = dblWCurrentDue
            double tempTotalCost = 0;
            var acctKey = rsData.Get_Fields("AccountKey");
            waterDetail.LienAmount = modUTCalculations.CalculateAccountUTTotal(acctKey, true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblPastDueCurrentLienInterest, ref dblPastDueInterest, ref dtStatementDate, true, 0, false, ref tempTotalCost, ref dblPreLienInterest);
            dblPastDueInterest = 0;
            waterDetail.TotalDue = modUTCalculations.CalculateAccountUTTotal(acctKey, true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblPastDueCurrentInterest, ref dblPastDueInterest, ref dtStatementDate);
            // this will calculate the non lien past due by taking the total and subtracting the current and the lien amount
            waterDetail.PastDueCurrentInterest = dblPastDueCurrentInterest;
            waterDetail.PastDueInterest = dblPastDueInterest;
            waterDetail.PreLienInterest = dblPreLienInterest;
            waterDetail.PastDueCurrentLienInterest = dblPastDueCurrentLienInterest;
            waterDetail.PastDue = waterDetail.TotalDue - waterDetail.CurrentDue - waterDetail.LienAmount - (waterDetail.PastDueCurrentInterest - waterDetail.PastDueCurrentLienInterest);
            if (boolUse2Periods)
            {
                waterDetail.SetPeriodDueAmount(1, FCUtils.Round(waterDetail.CurrentDue / 2, 2));
                waterDetail.SetPeriodDueAmount(2, FCUtils.Round(waterDetail.CurrentDue - waterDetail.GetPeriodDueAmount(1), 2));
            }
            else
            {
                waterDetail.SetPeriodDueAmount(1, FCUtils.Round(waterDetail.CurrentDue, 2));
            }
            CreateWaterDetail = waterDetail;
            return CreateWaterDetail;
        }

        private cUTBillUtility CreateSewerDetail(ref clsDRWrapper rsData, DateTime dtStatementDate)
        {
            cUTBillUtility CreateSewerDetail = null;
            cUTBillUtility sewerDetail = new/*AsNew*/ cUTBillUtility();
            double dblPastDueCurrentInterest = 0;
            double dblPastDueInterest = 0;
            double dblPastDueCurrentLienInterest = 0;
            double dblLienPastDueInterest;
            double dblPreLienInterest = 0;

            sewerDetail.Regular = rsData.Get_Fields("SPrinOwed") + rsData.Get_Fields("SIntOwed") + rsData.Get_Fields("SCostOwed");
            sewerDetail.TaxOwed = rsData.Get_Fields("STaxOwed");
            sewerDetail.PrincipalPaid = rsData.Get_Fields("SPrinPaid") + rsData.Get_Fields("STaxPaid") + rsData.Get_Fields("SIntPaid") + rsData.Get_Fields_Double("SCostPaid");
            sewerDetail.CurrentDue = (rsData.Get_Fields("SPrinOwed") + rsData.Get_Fields("STaxOwed") + rsData.Get_Fields("SIntOwed") + rsData.Get_Fields_Double("SCostOwed") - rsData.Get_Fields_Double("SCostAdded") - rsData.Get_Fields_Double("SIntAdded") + sewerDetail.Interest) - (sewerDetail.PrincipalPaid);
            double tempTotalCost = 0;
            sewerDetail.LienAmount = modUTCalculations.CalculateAccountUTTotal(rsData.Get_Fields("AccountKey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblPastDueCurrentLienInterest, ref dblPastDueInterest, ref dtStatementDate, true, 0, false, ref tempTotalCost, ref dblPreLienInterest);

            dblPastDueInterest = 0;

            sewerDetail.TotalDue = modUTCalculations.CalculateAccountUTTotal(rsData.Get_Fields("AccountKey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblPastDueCurrentInterest, ref dblPastDueInterest, ref dtStatementDate);

            // this will calculate the non lien past due by taking the total and subtracting the current and the lien amount
            sewerDetail.PastDueCurrentInterest = dblPastDueCurrentInterest;
            sewerDetail.PastDueInterest = dblPastDueInterest;
            sewerDetail.PreLienInterest = dblPreLienInterest;
            sewerDetail.PastDueCurrentLienInterest = dblPastDueCurrentLienInterest;
            sewerDetail.PastDue = sewerDetail.TotalDue - sewerDetail.CurrentDue - sewerDetail.LienAmount - (sewerDetail.PastDueCurrentInterest - sewerDetail.PastDueCurrentLienInterest);
            // dblOverallTotalPastDue = dblOverallTotalPastDue + dblSPastDue + dblSPastDueCurrentInterest + dblSLienAmount - dblSPastDueCurrentLInterest
            if (boolUse2Periods)
            {
                sewerDetail.SetPeriodDueAmount(1, FCUtils.Round(sewerDetail.CurrentDue / 2, 2));
                sewerDetail.SetPeriodDueAmount(2, FCUtils.Round(sewerDetail.CurrentDue - sewerDetail.GetPeriodDueAmount(1), 2));
            }
            else
            {
                sewerDetail.SetPeriodDueAmount(1, FCUtils.Round(sewerDetail.CurrentDue, 2));
            }
            CreateSewerDetail = sewerDetail;
            return CreateSewerDetail;
        }


        public cUTOutPrinter() : base()
        {
            boolUse2Periods = modUTCalculations.Statics.gboolSplitBills;
        }

        private void ItemizeAdjustments(object lngBillId, ref cUTBillUtility billUtility)
        {
            clsDRWrapper rsTemp = new/*AsNew*/ clsDRWrapper();
            rsTemp.OpenRecordset("SELECT Breakdown.Amount as AdjAmount, Adjust.* FROM Breakdown INNER JOIN Adjust ON Adjust.Code = Breakdown.AdjCode WHERE Breakdown.BillKey = " + lngBillId + " AND Breakdown.Service = '" + fecherFoundation.Strings.UCase(billUtility.UtilityType) + "'", "UtilityBilling");
            while (!rsTemp.EndOfFile())
            {
                billUtility.AddAdjustment(rsTemp.Get_Fields("LongDescription"), /*Round*/(rsTemp.Get_Fields("AdjAmount") * 100));
                rsTemp.MoveNext();
            }
        }

        private string CreateBillLine(ref cUTBill bill)
        {
            string CreateBillLine = "";
            string strLine;
            cUTBillMeterDetail meterDetail;
            cUTBillUtility waterDetail;
            cUTBillUtility sewerDetail;

            strLine = "";

            strLine = AppendToLine(ref strLine, bill.AccountNumber.ToString());
            waterDetail = bill.GetUtilityDetailsForType(cnstWaterCode);
            sewerDetail = bill.GetUtilityDetailsForType(cnstSewerCode);
            cUTBillParty MailTo;
            MailTo = bill.GetMailToParty();

            strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(MailTo.Name, 34, false)));
            strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(MailTo.Name2, 34, false)));
            strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(MailTo.Address1, 34, false)));
            strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(MailTo.Address2, 34, false)));
            strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(MailTo.Address3, 34, false)));
            strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(MailTo.City, 24, false)));
            strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(MailTo.State, 2, false)));
            strLine = AppendToLine(ref strLine, MailTo.Zip);
            if (MailTo.Zip4 != "")
            {
                strLine = AppendToLine(ref strLine, "-" + fecherFoundation.Strings.Trim(MailTo.Zip4));
            }
            else
            {
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.StrDup(5, " "));
            }


            // 11
            strLine = AppendToLine(ref strLine, bill.Category);
            // 12
            strLine = AppendToLine(ref strLine, bill.MapLot);
            // 13
            strLine = AppendToLine(ref strLine, bill.Location);
            // 14     'Bill Message
            strLine = AppendToLine(ref strLine, bill.Message);
            // 15
            strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.StatementDate, "MMDDYYYY"));
            // 16
            strLine = AppendToLine(ref strLine, bill.RateInformation.PeriodDescription);
            // 17
            cUTReading currentReading;
            currentReading = bill.GetCurrentReading();
            if (fecherFoundation.Information.IsDate(currentReading.ReadingDate))
            {
                if (currentReading.ReadingDate.ToOADate() == fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
                {
                    strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.EndDate, "MMDDYYYY"));
                }
                else
                {
                    strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(currentReading.ReadingDate, "MMDDYYYY"));
                }
            }
            else
            {
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.EndDate, "MMDDYYYY"));
            }
            // 18, 19, 20
            if (!(bill.RateInformation.StartDate.ToOADate() == fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() || bill.RateInformation.EndDate.ToOADate() == fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() || bill.RateInformation.InterestStartDate.ToOADate() == fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate()))
            {
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.StartDate, "MMDDYYYY"));
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.EndDate, "MMDDYYYY"));
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.InterestStartDate, "MMDDYYYY"));
            }
            else
            {
                // empty string
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.StrDup(8, " "));
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.StrDup(8, " "));
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.StrDup(8, " "));
            }
            // 21
            // Overall Current Due
            strLine = AppendToLine(ref strLine, /*Round*/(bill.GetOverallCurrentDue() * 100).ToString());
            // 22
            // Overall Past Due
            strLine = AppendToLine(ref strLine, /*Round*/(bill.GetOverallTotalPastDue() * 100).ToString());
            // 23
            // Overall Total Due
            strLine = AppendToLine(ref strLine, /*Round*/(bill.GetOverallTotalDue() * 100).ToString());
            // 24
            strLine = AppendToLine(ref strLine, bill.Book.ToString());
            cUTBillMeterDetail firstMeter;
            firstMeter = bill.GetMeterDetailsForMeter(1);
            // 25
            strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Trim(fecherFoundation.Strings.Left(FCConvert.ToString(firstMeter.Sequence) + "     ", 5)));
            // WATER
            // 26
            if (modUTFunctions.Statics.TownService != "S")
            {
                if (bill.ServiceType != cUTBill.UTBillType.Sewer)
                {
                    strLine = AppendToLine(ref strLine, waterDetail.Consumption.ToString());
                }
                else
                {
                    strLine = AppendToLine(ref strLine, "0");
                }
            }
            else
            {
                strLine = AppendToLine(ref strLine, "0");
            }
            // 27
            // Water Unit Amount
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.UnitCount, 2).ToString());
            // 28
            // Water Consumption Total
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.ConsumptionAmount * 100, 0).ToString());
            // 29
            // Water Units Total
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.UnitsAmount * 100, 0).ToString());
            // 30
            // Water Flat Total
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.FlatAmount * 100, 0).ToString());
            // 31
            // Water Adjustment Total
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.Adjustment * 100, 0).ToString());
            // 32
            // Water Cons + Flat + Units
            strLine = AppendToLine(ref strLine, FCUtils.Round((waterDetail.ConsumptionAmount + waterDetail.UnitsAmount + waterDetail.FlatAmount) * 100, 0).ToString());
            // 33
            // Regular                'charged amount on bill
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.Regular * 100, 0).ToString());
            // 34
            // Tax                    'charged tax amount on bill
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.TaxOwed * 100, 0).ToString());
            // 35, 36
            // Past Due and Credit Field  'total due from previous non lien bills
            if (waterDetail.PastDue >= 0)
            {
                strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.PastDue * 100, 0).ToString());
                strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.PrincipalPaid * 100, 0).ToString());
            }
            else
            {
                strLine = AppendToLine(ref strLine, /*Round*/(0).ToString());
                strLine = AppendToLine(ref strLine, FCUtils.Round((waterDetail.PastDue * -100) + (waterDetail.PrincipalPaid * 100), 0).ToString());
            }
            // 37
            // Total Amount Due       'current due + total due from previous bills
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.TotalDue * 100, 0).ToString());
            // 38
            strLine = AppendToLine(ref strLine, FCUtils.Round((waterDetail.PastDueCurrentInterest) * 100, 0).ToString());
            // 39
            // Lien Amount            'total lien amount for this account
            strLine = AppendToLine(ref strLine, FCUtils.Round((waterDetail.LienAmount - waterDetail.PastDueCurrentLienInterest) * 100, 0).ToString());
            // 40
            // Current Due            'amount due from the current bill
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.CurrentDue * 100, 0).ToString());
            // 41
            // Total Past Due         'total past due       'kk10292014 trouts-172  Subtract out Lien Current Interest so it's not doubled
            strLine = AppendToLine(ref strLine, FCUtils.Round(waterDetail.GetTotalPastDue() * 100, 0).ToString());

            // SEWER
            // 42
            if (modUTFunctions.Statics.TownService != "W")
            {
                if (bill.ServiceType != cUTBill.UTBillType.water)
                {
                    strLine = AppendToLine(ref strLine, sewerDetail.Consumption.ToString());
                }
                else
                {
                    strLine = AppendToLine(ref strLine, "0");
                }
            }
            else
            {
                strLine = AppendToLine(ref strLine, "0");
            }
            // 43
            // Sewer Unit Amount
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.UnitCount, 2).ToString());
            // 44
            // Sewer Consumption Total
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.ConsumptionAmount * 100, 0).ToString());
            // 45
            // Sewer Units Total
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.UnitsAmount * 100, 0).ToString());
            // 46
            // Sewer Flat Total
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.FlatAmount * 100, 0).ToString());
            // 47
            // Sewer Adjust Total
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.Adjustment * 100, 0).ToString());
            // 48
            // Sewer Cons + Flat + Units
            strLine = AppendToLine(ref strLine, FCUtils.Round((sewerDetail.ConsumptionAmount + sewerDetail.UnitsAmount + sewerDetail.FlatAmount) * 100, 0).ToString());
            // 49
            // Regular
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.Regular * 100, 0).ToString());
            // 50
            // Tax
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.TaxOwed * 100, 0).ToString());
            // 51, 52
            // Past Due
            if (sewerDetail.PastDue >= 0)
            {
                strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.PastDue * 100, 0).ToString());
                strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.PrincipalPaid * 100, 0).ToString());
            }
            else
            {
                strLine = AppendToLine(ref strLine, /*Round*/(0).ToString());
                strLine = AppendToLine(ref strLine, FCUtils.Round((sewerDetail.PastDue * -100) + (sewerDetail.PrincipalPaid * 100), 0).ToString());
            }
            // 53
            // Total Amount Due
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.TotalDue * 100, 0).ToString());
            // 54
            strLine = AppendToLine(ref strLine, FCUtils.Round((sewerDetail.PastDueCurrentInterest) * 100, 0).ToString());
            // 55
            // Lien Amount
            strLine = AppendToLine(ref strLine, FCUtils.Round((sewerDetail.LienAmount - sewerDetail.PastDueCurrentLienInterest) * 100, 0).ToString());
            // 56
            // Current Due
            strLine = AppendToLine(ref strLine, FCUtils.Round(sewerDetail.CurrentDue * 100, 0).ToString());
            // 57
            // Total Past Due
            strLine = AppendToLine(ref strLine, FCUtils.Round((sewerDetail.GetTotalPastDue()) * 100, 0).ToString());

            // 58, 59, 60
            // print meter prev,curr,cons
            short lngCT;
            for (lngCT = 1; lngCT <= 3; lngCT++)
            {
                meterDetail = bill.GetMeterDetailsForMeter(lngCT);
                strLine = AppendToLine(ref strLine, meterDetail.Consumption.ToString());
                strLine = AppendToLine(ref strLine, meterDetail.previousReading.ToString());
                strLine = AppendToLine(ref strLine, meterDetail.currentReading.ToString());
            }

            // 67, 68, 69
            strLine = AppendToLine(ref strLine, waterDetail.GetPeriodDueAmount(1).ToString());
            strLine = AppendToLine(ref strLine, waterDetail.GetPeriodDueAmount(2).ToString());
            strLine = AppendToLine(ref strLine, sewerDetail.GetPeriodDueAmount(1).ToString());
            strLine = AppendToLine(ref strLine, sewerDetail.GetPeriodDueAmount(2).ToString());

            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
            {
                strLine = AppendToLine(ref strLine, bill.Comment);
            }
            // kgk trout-684 04-21-2011  Add original consumption amount to outprint
            double dblSActualCons = 0;
            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
            {
                var intTempConsumption = bill.Consumption;
                dblSActualCons = FCUtils.Round(modUTBilling.CalculateConsumptionBased(ref intTempConsumption, sewerDetail.GetRateTableID(1), false), 2);
                strLine = AppendToLine(ref strLine, dblSActualCons.ToString());
            }

            if (!boolChpt660)
            {
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.DueDate, "MMDDYYYY"));
                if (!(fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR"))
                {
                    if (boolInclChangeOutCons)
                    {
                        // kjr 11/7/16 trout-1118
                        for (lngCT = 1; lngCT <= 3; lngCT++)
                        {
                            meterDetail = bill.GetMeterDetailsForMeter(lngCT);
                            strLine = AppendToLine(ref strLine, meterDetail.ChangeoutConsumption.ToString());
                        }
                    }
                }
            }
            else
            {
                strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.DueDate, "MMDDYYYY"));

                // kgk trout-764 & 765  10-18-2011  Add Estimated indicator and Meter change out indicator
                cUTBillAdjustment itemAdjustment;
                for (lngCT = 1; lngCT <= 3; lngCT++)
                {
                    meterDetail = bill.GetMeterDetailsForMeter(lngCT);
                    if (meterDetail.IsEstimate)
                    {
                        strLine = AppendToLine(ref strLine, "E");
                    }
                    else
                    {
                        strLine = AppendToLine(ref strLine, "A");
                    }
                    if (meterDetail.WasChangedOut)
                    {
                        strLine = AppendToLine(ref strLine, "Y");
                    }
                    else
                    {
                        strLine = AppendToLine(ref strLine, "N");
                    }
                }
                for (lngCT = 1; lngCT <= maxAdjustments; lngCT++)
                {
                    itemAdjustment = waterDetail.GetItemizedAdjustment(lngCT);
                    strLine = AppendToLine(ref strLine, itemAdjustment.Description);
                    strLine = AppendToLine(ref strLine, FCConvert.ToString(itemAdjustment.Adjustment));
                }
                for (lngCT = 1; lngCT <= maxAdjustments; lngCT++)
                {
                    itemAdjustment = sewerDetail.GetItemizedAdjustment(lngCT);
                    strLine = AppendToLine(ref strLine, itemAdjustment.Description);
                    strLine = AppendToLine(ref strLine, FCConvert.ToString(itemAdjustment.Adjustment));
                }

                // kjr 11/7/16 trout-1118
                if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "GARDINER" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "BANGOR")
                {
                    if (boolInclChangeOutCons)
                    {
                        for (lngCT = 1; lngCT <= 3; lngCT++)
                        {
                            meterDetail = bill.GetMeterDetailsForMeter(lngCT);
                            strLine = AppendToLine(ref strLine, meterDetail.ChangeoutConsumption.ToString());
                        }
                    }
                }
            }

            cUTReading previousReading;

            previousReading = bill.GetPreviousReading();
            currentReading = bill.GetCurrentReading();

            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
            {
                // kjr 11/7/16 trout-1118
                strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(bill.OwnerParty.Name, 34, false)));
                strLine = AppendToLine(ref strLine, FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(bill.OwnerParty.Name2, 34, false)));
                if (boolInclChangeOutCons)
                {
                    for (lngCT = 1; lngCT <= 3; lngCT++)
                    {
                        meterDetail = bill.GetMeterDetailsForMeter(lngCT);
                        strLine = AppendToLine(ref strLine, meterDetail.ChangeoutConsumption.ToString());
                    }
                }
            }
            else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
            {
                if (bill.ImperviousSurfaceArea >= 0)
                { // kk07082015 trouts-149  Indicate account is not billed for SW by setting the Value to -1
                    strLine = AppendToLine(ref strLine, bill.ImperviousSurfaceArea.ToString());
                }
                else
                {
                    strLine = AppendToLine(ref strLine, "0");
                }
                // kjr 11/7/16 trout-1118
                if (fecherFoundation.Information.IsDate(previousReading.ReadingDate))
                {
                    if (previousReading.ReadingDate.ToOADate() == fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
                    {
                        strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.StartDate, "MMDDYYYY"));
                    }
                    else
                    {
                        strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(previousReading.ReadingDate, "MMDDYYYY"));
                    }
                }
                else
                {
                    strLine = AppendToLine(ref strLine, fecherFoundation.Strings.Format(bill.RateInformation.StartDate, "MMDDYYYY"));
                }
                if (boolInclChangeOutCons)
                {
                    // kk07272016 trouts-169  Add previous reading date to outprint file
                    for (lngCT = 1; lngCT <= 3; lngCT++)
                    {
                        meterDetail = bill.GetMeterDetailsForMeter(lngCT);
                        strLine = AppendToLine(ref strLine, meterDetail.ChangeoutConsumption.ToString());
                    }
                }
            }
            CreateBillLine = strLine;
            return CreateBillLine;
        }


        private void FillBill(ref cUTBill bill, ref clsDRWrapper rsLoad)
        {
            bill.Consumption = rsLoad.Get_Fields("Consumption");
            bill.AccountNumber = rsLoad.Get_Fields("ActualAccountNumber");
            bill.Book = rsLoad.Get_Fields("Book");
            bill.Location = rsLoad.Get_Fields("Location");
            bill.ImperviousSurfaceArea = rsLoad.Get_Fields_Double("ImpervSurfArea");
            switch (fecherFoundation.Strings.LCase(rsLoad.Get_Fields_String("Service")))
            {

                case "w":
                    {
                        bill.ServiceType = cUTBill.UTBillType.water;
                        break;
                    }
                case "s":
                    {
                        bill.ServiceType = cUTBill.UTBillType.Sewer;
                        break;
                    }
                case "b":
                    {
                        bill.ServiceType = cUTBill.UTBillType.both;
                        break;
                    }
            } //end switch
            bill.MapLot = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("maplot"));
            bill.Message = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("BillMessage"));
            bill.WaterBillToOwner = rsLoad.Get_Fields("WBillOwner");
            bill.SewerBillToOwner = rsLoad.Get_Fields("SBillOwner");
            cUTBillParty ownerPartyInfo = new/*AsNew*/ cUTBillParty();
            cUTBillParty tenantPartyInfo = new/*AsNew*/ cUTBillParty();
            ownerPartyInfo.Address1 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("OAddress1"));
            ownerPartyInfo.Address2 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("Oaddress2"));
            ownerPartyInfo.Address3 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("Oaddress3"));
            ownerPartyInfo.City = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("OCity"));
            ownerPartyInfo.Name = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("OName"));
            ownerPartyInfo.Name2 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("OName2"));
            ownerPartyInfo.State = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("OState"));
            ownerPartyInfo.Zip = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("Ozip"));
            ownerPartyInfo.Zip4 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("OZip4"));
            bill.SetOwnerInformation(ref ownerPartyInfo);

            tenantPartyInfo.Address1 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("Baddress1"));
            tenantPartyInfo.Address2 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("BAddress2"));
            tenantPartyInfo.Address3 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("BAddress3"));
            tenantPartyInfo.City = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("BCity"));
            tenantPartyInfo.Name = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("BName"));
            tenantPartyInfo.Name2 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("BName2"));
            tenantPartyInfo.State = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("BState"));
            tenantPartyInfo.Zip = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("BZip"));
            tenantPartyInfo.Zip4 = fecherFoundation.Strings.Trim(rsLoad.Get_Fields("Bzip4"));
            bill.SetTenantInformation(ref tenantPartyInfo);

            cUTReading currRead = new/*AsNew*/ cUTReading();
            currRead.Reading = (int)Math.Round(fecherFoundation.Conversion.Val(rsLoad.Get_Fields("curreading")));
            currRead.ReadingCode = rsLoad.Get_Fields("curCode");
            currRead.ReadingDate = rsLoad.Get_Fields("curdate");
            bill.SetCurrentReading(ref currRead);

            cUTReading prevRead = new/*AsNew*/ cUTReading();
            prevRead.Reading = (int)Math.Round(fecherFoundation.Conversion.Val(rsLoad.Get_Fields("prevreading")));
            prevRead.ReadingCode = rsLoad.Get_Fields("PrevCode");
            prevRead.ReadingDate = rsLoad.Get_Fields("PrevDate");
            bill.SetPreviousReading(ref prevRead);

            FillBillMeterDetails(ref bill, Convert.ToInt32(fecherFoundation.Conversion.Val(rsLoad.Get_Fields("accountKey"))));

        }

        // vbPorter upgrade warning: lngAccountID As int	OnWrite(double)
        private void FillBillMeterDetails(ref cUTBill bill, int lngAccountID)
        {
            clsDRWrapper rsMeter = new/*AsNew*/ clsDRWrapper();
            cUTBillMeterDetail currMeterDetail;
            rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAccountID) + " AND (MeterNumber = 1 OR Combine <> 'N')", "UtilityBilling");
            while (!rsMeter.EndOfFile())
            {
                //////Application.DoEvents();
                currMeterDetail = new cUTBillMeterDetail();
                currMeterDetail.Sequence = (int)Math.Round(fecherFoundation.Conversion.Val(FCConvert.ToString(rsMeter.Get_Fields("sequence"))));
                switch (rsMeter.Get_Fields("MeterNumber"))
                {
                    // kgk 09052012 trout-865  Rewrite this section to handle meter roll-over

                    case 1:
                        {

                            if (Convert.ToInt32(rsMeter.Get_Fields("CurrentReading")) == -1)
                            { // kk 110812 trout-884 Change from 0 to -1 for No Read
                                currMeterDetail.Consumption = 0;
                                currMeterDetail.currentReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                                currMeterDetail.previousReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                            }
                            else
                            {
                                if (rsMeter.Get_Fields("CurrentReading") >= rsMeter.Get_Fields("PreviousReading"))
                                {
                                    currMeterDetail.Consumption = (rsMeter.Get_Fields("CurrentReading") - rsMeter.Get_Fields("PreviousReading")) * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow;

                                }
                                else
                                { // Need to handle meter roll-over
                                    currMeterDetail.Consumption = Math.Pow(10, (rsMeter.Get_Fields("Digits"))) - rsMeter.Get_Fields("PreviousReading"); // Consumption up to all 9s, right before meter rolled over
                                    currMeterDetail.Consumption = (currMeterDetail.Consumption + rsMeter.Get_Fields("CurrentReading")) * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow; // Consumption since meter rolled over to zero
                                }

                                currMeterDetail.currentReading = Convert.ToInt32(rsMeter.Get_Fields("CurrentReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                                currMeterDetail.previousReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                            }
                            // kgk trout-764 & 765  10-18-2011  Add Estimated and Change Out indicators to outprintfile
                            currMeterDetail.IsEstimate = fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsMeter.Get_Fields("CurrentCode")))) == "e";
                            if (FCConvert.ToBoolean(rsMeter.Get_Fields("PrevChangeOut")))
                            {
                                currMeterDetail.WasChangedOut = true;
                                currMeterDetail.ChangeoutConsumption = rsMeter.Get_Fields("PrevChangeOutConsumption"); // kjr trout-1118 11/7/2016
                            }
                            else
                            {
                                currMeterDetail.WasChangedOut = false;
                            }
                            bill.SetMeterDetailsForMeter(1, ref currMeterDetail);
                            break;
                        }
                    case 2:
                        {
                            if (Convert.ToInt32(rsMeter.Get_Fields("CurrentReading")) == -1)
                            { // kk 110812 trout-884 Change from 0 to -1 for No Read
                                currMeterDetail.Consumption = 0;
                                currMeterDetail.currentReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                                currMeterDetail.previousReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                            }
                            else
                            {
                                if (rsMeter.Get_Fields("CurrentReading") >= rsMeter.Get_Fields("PreviousReading"))
                                {
                                    currMeterDetail.Consumption = (rsMeter.Get_Fields("CurrentReading") - rsMeter.Get_Fields("PreviousReading")) * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow;

                                }
                                else
                                { // Need to handle meter roll-over
                                    currMeterDetail.Consumption = Math.Pow(10, (rsMeter.Get_Fields("Digits"))) - rsMeter.Get_Fields("PreviousReading"); // Consumption up to all 9s, right before meter rolled over
                                    currMeterDetail.Consumption = (currMeterDetail.Consumption + rsMeter.Get_Fields("CurrentReading")) * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow; // Consumption since meter rolled over to zero
                                }

                                currMeterDetail.currentReading = Convert.ToInt32(rsMeter.Get_Fields("CurrentReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                                currMeterDetail.previousReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);

                                if (FCConvert.ToBoolean(rsMeter.Get_Fields("NegativeConsumption")))
                                {
                                    currMeterDetail.Consumption *= -1;
                                }
                            }
                            currMeterDetail.IsEstimate = fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsMeter.Get_Fields("CurrentCode")))) == "e";
                            if (FCConvert.ToBoolean(rsMeter.Get_Fields("PrevChangeOut")))
                            {
                                currMeterDetail.WasChangedOut = true;
                                currMeterDetail.ChangeoutConsumption = rsMeter.Get_Fields("PrevChangeOutConsumption"); // kjr trout-1118 11/7/2016
                            }
                            else
                            {
                                currMeterDetail.WasChangedOut = false;
                            }
                            bill.SetMeterDetailsForMeter(2, ref currMeterDetail);
                            break;
                        }
                    case 3:
                        {
                            if (Convert.ToInt32(rsMeter.Get_Fields("CurrentReading")) == -1)
                            { // kk 110812 trout-884 Change from 0 to -1 for No Read
                                currMeterDetail.Consumption = 0;
                                currMeterDetail.currentReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                                currMeterDetail.previousReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                            }
                            else
                            {
                                if (rsMeter.Get_Fields("CurrentReading") >= rsMeter.Get_Fields("PreviousReading"))
                                {
                                    currMeterDetail.Consumption = (rsMeter.Get_Fields("CurrentReading") - rsMeter.Get_Fields("PreviousReading")) * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow;

                                }
                                else
                                { // Need to handle meter roll-over
                                    currMeterDetail.Consumption = Math.Pow(10, (rsMeter.Get_Fields("Digits"))) - rsMeter.Get_Fields("PreviousReading"); // Consumption up to all 9s, right before meter rolled over
                                    currMeterDetail.Consumption = (currMeterDetail.Consumption + rsMeter.Get_Fields("CurrentReading")) * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow; // Consumption since meter rolled over to zero
                                }

                                currMeterDetail.currentReading = Convert.ToInt32(rsMeter.Get_Fields("CurrentReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);
                                currMeterDetail.previousReading = Convert.ToInt32(rsMeter.Get_Fields("PreviousReading") * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow);

                                if (FCConvert.ToBoolean(rsMeter.Get_Fields("NegativeConsumption")))
                                {
                                    currMeterDetail.Consumption *= -1;
                                }
                            }
                            currMeterDetail.IsEstimate = fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsMeter.Get_Fields("CurrentCode")))) == "e";
                            if (FCConvert.ToBoolean(rsMeter.Get_Fields("PrevChangeOut")))
                            {
                                currMeterDetail.WasChangedOut = true;
                                currMeterDetail.ChangeoutConsumption = rsMeter.Get_Fields("PrevChangeOutConsumption"); // kjr trout-1118 11/7/2016
                            }
                            else
                            {
                                currMeterDetail.WasChangedOut = false;
                            }
                            bill.SetMeterDetailsForMeter(3, ref currMeterDetail);
                            break;
                        }
                    default:
                        {
                            currMeterDetail = bill.GetMeterDetailsForMeter(3);
                            if (Convert.ToInt32(rsMeter.Get_Fields("CurrentReading")) != -1)
                            { // kk 110812 trout-884 Change from 0 to -1 for No Read
                                if (Convert.ToInt32(rsMeter.Get_Fields("NegativeConsumption")) == 0)
                                {
                                    currMeterDetail.Consumption += ((rsMeter.Get_Fields("CurrentReading") - rsMeter.Get_Fields("PreviousReading")) * -1) * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow;
                                }
                                else
                                {
                                    currMeterDetail.Consumption = (currMeterDetail.Consumption + rsMeter.Get_Fields("CurrentReading") - rsMeter.Get_Fields("PreviousReading")) * (double)modExtraModules.Statics.glngTownReadingUnits / lngUnitsToShow;
                                }
                                currMeterDetail.currentReading = 0; // leave these as 0 if there are more than 3 meters
                                currMeterDetail.previousReading = 0;
                            }
                            currMeterDetail.IsEstimate = fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsMeter.Get_Fields("CurrentCode")))) == "e";
                            if (FCConvert.ToBoolean(rsMeter.Get_Fields("PrevChangeOut")))
                            {
                                currMeterDetail.WasChangedOut = true;
                                currMeterDetail.ChangeoutConsumption = rsMeter.Get_Fields("PrevChangeOutConsumption"); // kjr trout-1118 11/7/2016
                            }
                            else
                            {
                                currMeterDetail.WasChangedOut = false;
                            }
                            bill.SetMeterDetailsForMeter(3, ref currMeterDetail);
                            break;
                        }
                } //end switch
                rsMeter.MoveNext();
            }
        }

        private cUTBillRateInformation GetRateInformation(int lngRateID)
        {
            cUTBillRateInformation GetRateInformation = null;
            cUTBillRateInformation currRate = new/*AsNew*/ cUTBillRateInformation();
            clsDRWrapper rsRK = new/*AsNew*/ clsDRWrapper();
            rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateID), "UtilityBilling");
            currRate.StartDate = fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0));
            currRate.EndDate = fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0));
            currRate.InterestStartDate = fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0));
            currRate.PeriodDescription = "";

            if (!rsRK.EndOfFile())
            {
                currRate.PeriodDescription = fecherFoundation.Strings.Left(fecherFoundation.Strings.Format(rsRK.Get_Fields("Start"), "MMM"), 3) + "-" + fecherFoundation.Strings.Left(fecherFoundation.Strings.Format(rsRK.Get_Fields("End"), "MMM"), 3);
                if (FCConvert.ToString(rsRK.Get_Fields("start")) != "")
                {
                    currRate.StartDate = (DateTime)rsRK.Get_Fields("Start");
                }
                if (FCConvert.ToString(rsRK.Get_Fields("end")) != "")
                {
                    currRate.EndDate = (DateTime)rsRK.Get_Fields("end");
                }
                if (FCConvert.ToString(rsRK.Get_Fields("IntStart")) != "")
                {
                    currRate.InterestStartDate = (DateTime)rsRK.Get_Fields("IntStart");
                }
                if (FCConvert.ToString(rsRK.Get_Fields("duedate")) != "")
                {
                    currRate.DueDate = (DateTime)rsRK.Get_Fields("duedate");
                }
            }
            GetRateInformation = currRate;
            return GetRateInformation;
        }

        private string GetTempFileName(string extension)
        {
            string tempFolder = Path.Combine(Application.StartupPath, "Temp");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }
            Guid guid = Guid.NewGuid();
            string tempFileName = guid.ToString() + extension;
            string tempFilePath = Path.Combine(tempFolder, tempFileName);
            while (File.Exists(tempFilePath))
            {
                guid = Guid.NewGuid();
                tempFileName = guid.ToString() + extension;
                tempFilePath = Path.Combine(tempFolder, tempFileName);
            }
            return tempFilePath;
        }

    }
}
