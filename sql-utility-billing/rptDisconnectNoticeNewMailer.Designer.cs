﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDisconnectNoticeNewMailer.
	/// </summary>
	partial class rptDisconnectNoticeNewMailer
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDisconnectNoticeNewMailer));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldReturnName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNoticeDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldShutOffDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAsterisk = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeperateSewerLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAsterisk2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeperateSewer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPayByDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoticeDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldShutOffDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewerLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayByDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldReturnName,
				this.fldReturnAddress1,
				this.fldReturnAddress2,
				this.fldReturnAddress3,
				this.fldReturnAddress4,
				this.fldBookSequence,
				this.fldAccountNumber,
				this.fldNoticeDate,
				this.fldShutOffDate,
				this.fldWaterTotal,
				this.lblAsterisk,
				this.fldSeperateSewerLabel,
				this.lblAsterisk2,
				this.fldSeperateSewer,
				this.fldName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.fldLocation,
				this.fldPayByDate,
				this.fldAddress5
			});
			this.Detail.Height = 3.65625F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldReturnName
			// 
			this.fldReturnName.Height = 0.1875F;
			this.fldReturnName.Left = 0.46875F;
			this.fldReturnName.Name = "fldReturnName";
			this.fldReturnName.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldReturnName.Text = "Field1";
			this.fldReturnName.Top = 0.71875F;
			this.fldReturnName.Width = 2.40625F;
			// 
			// fldReturnAddress1
			// 
			this.fldReturnAddress1.Height = 0.1875F;
			this.fldReturnAddress1.Left = 0.46875F;
			this.fldReturnAddress1.Name = "fldReturnAddress1";
			this.fldReturnAddress1.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldReturnAddress1.Text = "Field1";
			this.fldReturnAddress1.Top = 0.90625F;
			this.fldReturnAddress1.Width = 2.40625F;
			// 
			// fldReturnAddress2
			// 
			this.fldReturnAddress2.Height = 0.1875F;
			this.fldReturnAddress2.Left = 0.46875F;
			this.fldReturnAddress2.Name = "fldReturnAddress2";
			this.fldReturnAddress2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldReturnAddress2.Text = "Field1";
			this.fldReturnAddress2.Top = 1.09375F;
			this.fldReturnAddress2.Width = 2.40625F;
			// 
			// fldReturnAddress3
			// 
			this.fldReturnAddress3.Height = 0.1875F;
			this.fldReturnAddress3.Left = 0.46875F;
			this.fldReturnAddress3.Name = "fldReturnAddress3";
			this.fldReturnAddress3.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldReturnAddress3.Text = "Field1";
			this.fldReturnAddress3.Top = 1.28125F;
			this.fldReturnAddress3.Width = 2.40625F;
			// 
			// fldReturnAddress4
			// 
			this.fldReturnAddress4.Height = 0.1875F;
			this.fldReturnAddress4.Left = 0.46875F;
			this.fldReturnAddress4.Name = "fldReturnAddress4";
			this.fldReturnAddress4.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldReturnAddress4.Text = "Field1";
			this.fldReturnAddress4.Top = 1.46875F;
			this.fldReturnAddress4.Width = 2.40625F;
			// 
			// fldBookSequence
			// 
			this.fldBookSequence.Height = 0.1875F;
			this.fldBookSequence.Left = 1.59375F;
			this.fldBookSequence.Name = "fldBookSequence";
			this.fldBookSequence.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldBookSequence.Text = "Field1";
			this.fldBookSequence.Top = 1.78125F;
			this.fldBookSequence.Width = 0.84375F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 0.90625F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAccountNumber.Text = "Acct#";
			this.fldAccountNumber.Top = 1.78125F;
			this.fldAccountNumber.Width = 0.5625F;
			// 
			// fldNoticeDate
			// 
			this.fldNoticeDate.Height = 0.1875F;
			this.fldNoticeDate.Left = 2.53125F;
			this.fldNoticeDate.Name = "fldNoticeDate";
			this.fldNoticeDate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldNoticeDate.Text = "Acct#";
			this.fldNoticeDate.Top = 1.78125F;
			this.fldNoticeDate.Width = 0.90625F;
			// 
			// fldShutOffDate
			// 
			this.fldShutOffDate.Height = 0.1875F;
			this.fldShutOffDate.Left = 6.09375F;
			this.fldShutOffDate.Name = "fldShutOffDate";
			this.fldShutOffDate.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
			this.fldShutOffDate.Text = "Acct#";
			this.fldShutOffDate.Top = 3.03125F;
			this.fldShutOffDate.Width = 0.90625F;
			// 
			// fldWaterTotal
			// 
			this.fldWaterTotal.Height = 0.1875F;
			this.fldWaterTotal.Left = 6.09375F;
			this.fldWaterTotal.Name = "fldWaterTotal";
			this.fldWaterTotal.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
			this.fldWaterTotal.Text = "Acct#";
			this.fldWaterTotal.Top = 1.78125F;
			this.fldWaterTotal.Width = 0.90625F;
			// 
			// lblAsterisk
			// 
			this.lblAsterisk.Height = 0.1875F;
			this.lblAsterisk.HyperLink = null;
			this.lblAsterisk.Left = 7.0625F;
			this.lblAsterisk.Name = "lblAsterisk";
			this.lblAsterisk.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblAsterisk.Text = "*";
			this.lblAsterisk.Top = 1.78125F;
			this.lblAsterisk.Width = 0.1875F;
			// 
			// fldSeperateSewerLabel
			// 
			this.fldSeperateSewerLabel.Height = 0.1875F;
			this.fldSeperateSewerLabel.Left = 2.25F;
			this.fldSeperateSewerLabel.Name = "fldSeperateSewerLabel";
			this.fldSeperateSewerLabel.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.fldSeperateSewerLabel.Text = "DOES NOT INCLUDE SEWER CHARGES OF";
			this.fldSeperateSewerLabel.Top = 3.5F;
			this.fldSeperateSewerLabel.Width = 2.9375F;
			// 
			// lblAsterisk2
			// 
			this.lblAsterisk2.Height = 0.1875F;
			this.lblAsterisk2.HyperLink = null;
			this.lblAsterisk2.Left = 2F;
			this.lblAsterisk2.Name = "lblAsterisk2";
			this.lblAsterisk2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblAsterisk2.Text = "*";
			this.lblAsterisk2.Top = 3.5F;
			this.lblAsterisk2.Width = 0.1875F;
			// 
			// fldSeperateSewer
			// 
			this.fldSeperateSewer.Height = 0.1875F;
			this.fldSeperateSewer.Left = 5.25F;
			this.fldSeperateSewer.Name = "fldSeperateSewer";
			this.fldSeperateSewer.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.fldSeperateSewer.Text = "Acct#";
			this.fldSeperateSewer.Top = 3.5F;
			this.fldSeperateSewer.Width = 0.9375F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1.4375F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldName.Text = "Field1";
			this.fldName.Top = 2.3125F;
			this.fldName.Width = 2.375F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 1.4375F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress1.Text = "Field1";
			this.fldAddress1.Top = 2.5F;
			this.fldAddress1.Width = 2.375F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 1.4375F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress2.Text = "Field1";
			this.fldAddress2.Top = 2.6875F;
			this.fldAddress2.Width = 2.375F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 1.4375F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress3.Text = "Field1";
			this.fldAddress3.Top = 2.875F;
			this.fldAddress3.Width = 2.375F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 1.4375F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress4.Text = "Field1";
			this.fldAddress4.Top = 3.0625F;
			this.fldAddress4.Width = 2.375F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 3.59375F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldLocation.Text = "Acct#";
			this.fldLocation.Top = 1.78125F;
			this.fldLocation.Width = 1.96875F;
			// 
			// fldPayByDate
			// 
			this.fldPayByDate.Height = 0.1875F;
			this.fldPayByDate.Left = 6.09375F;
			this.fldPayByDate.Name = "fldPayByDate";
			this.fldPayByDate.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
			this.fldPayByDate.Text = "Acct#";
			this.fldPayByDate.Top = 2.4375F;
			this.fldPayByDate.Width = 0.90625F;
			// 
			// fldAddress5
			// 
			this.fldAddress5.Height = 0.1875F;
			this.fldAddress5.Left = 1.4375F;
			this.fldAddress5.Name = "fldAddress5";
			this.fldAddress5.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress5.Text = "Field1";
			this.fldAddress5.Top = 3.25F;
			this.fldAddress5.Width = 2.375F;
			// 
			// rptDisconnectNoticeNewMailer
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.fldReturnName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoticeDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldShutOffDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewerLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayByDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoticeDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldShutOffDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAsterisk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeperateSewerLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAsterisk2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeperateSewer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayByDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
