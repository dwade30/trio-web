﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmSetupDisconnectEditReport.
	/// </summary>
	partial class frmSetupDisconnectEditReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSelected;
		public fecherFoundation.FCLabel lblSelected;
		public fecherFoundation.FCCheckBox chkExcludeNoBill;
		public fecherFoundation.FCCheckBox chkShowSrvcLoc;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtMinimumAmt;
		public fecherFoundation.FCFrame fraDefaultInfo;
		public FCGrid vsDefaults;
		public fecherFoundation.FCFrame Frame3;
		public Global.T2KDateBox txtShutOffDate;
		public fecherFoundation.FCFrame Frame2;
		public Global.T2KDateBox txtNoticeDate;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraSelectedBooks;
		public fecherFoundation.FCTextBox txtSequenceNumber;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupDisconnectEditReport));
			this.cmbSelected = new fecherFoundation.FCComboBox();
			this.lblSelected = new fecherFoundation.FCLabel();
			this.chkExcludeNoBill = new fecherFoundation.FCCheckBox();
			this.chkShowSrvcLoc = new fecherFoundation.FCCheckBox();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.txtMinimumAmt = new fecherFoundation.FCTextBox();
			this.fraDefaultInfo = new fecherFoundation.FCFrame();
			this.vsDefaults = new fecherFoundation.FCGrid();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtShutOffDate = new Global.T2KDateBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtNoticeDate = new Global.T2KDateBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraSelectedBooks = new fecherFoundation.FCFrame();
			this.txtSequenceNumber = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkExcludeNoBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowSrvcLoc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDefaultInfo)).BeginInit();
			this.fraDefaultInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsDefaults)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtShutOffDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtNoticeDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedBooks)).BeginInit();
			this.fraSelectedBooks.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 471);
			this.BottomPanel.Size = new System.Drawing.Size(655, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkExcludeNoBill);
			this.ClientArea.Controls.Add(this.chkShowSrvcLoc);
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.fraDefaultInfo);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(655, 411);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(655, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(264, 30);
			this.HeaderText.Text = "Disconnect Edit Report";
			// 
			// cmbSelected
			// 
			this.cmbSelected.AutoSize = false;
			this.cmbSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSelected.FormattingEnabled = true;
			this.cmbSelected.Items.AddRange(new object[] {
				"All",
				"Selected Books"
			});
			this.cmbSelected.Location = new System.Drawing.Point(196, 30);
			this.cmbSelected.Name = "cmbSelected";
			this.cmbSelected.Size = new System.Drawing.Size(205, 40);
			this.cmbSelected.TabIndex = 1;
			this.cmbSelected.Text = "All";
			this.cmbSelected.SelectedIndexChanged += new System.EventHandler(this.cmbSelected_SelectedIndexChanged);
			// 
			// lblSelected
			// 
			this.lblSelected.AutoSize = true;
			this.lblSelected.Location = new System.Drawing.Point(20, 44);
			this.lblSelected.Name = "lblSelected";
			this.lblSelected.Size = new System.Drawing.Size(56, 15);
			this.lblSelected.TabIndex = 0;
			this.lblSelected.Text = "SELECT";
			// 
			// chkExcludeNoBill
			// 
			this.chkExcludeNoBill.Location = new System.Drawing.Point(30, 330);
			this.chkExcludeNoBill.Name = "chkExcludeNoBill";
			this.chkExcludeNoBill.Size = new System.Drawing.Size(242, 27);
			this.chkExcludeNoBill.TabIndex = 5;
			this.chkExcludeNoBill.Text = "Exclude No Charge Accounts";
			// 
			// chkShowSrvcLoc
			// 
			this.chkShowSrvcLoc.Location = new System.Drawing.Point(30, 367);
			this.chkShowSrvcLoc.Name = "chkShowSrvcLoc";
			this.chkShowSrvcLoc.Size = new System.Drawing.Size(271, 27);
			this.chkShowSrvcLoc.TabIndex = 6;
			this.chkShowSrvcLoc.Text = "Show Service Location on Report";
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.txtMinimumAmt);
			this.Frame4.Location = new System.Drawing.Point(30, 230);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(170, 90);
			this.Frame4.TabIndex = 2;
			this.Frame4.Text = "Minimum Amount";
			// 
			// txtMinimumAmt
			// 
			this.txtMinimumAmt.AutoSize = false;
			this.txtMinimumAmt.BackColor = System.Drawing.SystemColors.Window;
			this.txtMinimumAmt.LinkItem = null;
			this.txtMinimumAmt.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMinimumAmt.LinkTopic = null;
			this.txtMinimumAmt.Location = new System.Drawing.Point(20, 30);
			this.txtMinimumAmt.Name = "txtMinimumAmt";
			this.txtMinimumAmt.Size = new System.Drawing.Size(130, 40);
			this.txtMinimumAmt.TabIndex = 0;
			this.txtMinimumAmt.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fraDefaultInfo
			// 
			this.fraDefaultInfo.Controls.Add(this.vsDefaults);
			this.fraDefaultInfo.Location = new System.Drawing.Point(222, 208);
			this.fraDefaultInfo.Name = "fraDefaultInfo";
			this.fraDefaultInfo.Size = new System.Drawing.Size(421, 95);
			this.fraDefaultInfo.TabIndex = 4;
			this.fraDefaultInfo.Text = "Default Information";
			// 
			// vsDefaults
			// 
			this.vsDefaults.AllowSelection = false;
			this.vsDefaults.AllowUserToResizeColumns = false;
			this.vsDefaults.AllowUserToResizeRows = false;
			this.vsDefaults.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDefaults.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDefaults.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDefaults.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDefaults.BackColorSel = System.Drawing.Color.Empty;
			this.vsDefaults.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDefaults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsDefaults.ColumnHeadersHeight = 30;
			this.vsDefaults.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsDefaults.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDefaults.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsDefaults.DragIcon = null;
			this.vsDefaults.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDefaults.FixedRows = 0;
			this.vsDefaults.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDefaults.FrozenCols = 0;
			this.vsDefaults.GridColor = System.Drawing.Color.Empty;
			this.vsDefaults.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDefaults.Location = new System.Drawing.Point(20, 30);
			this.vsDefaults.Name = "vsDefaults";
			this.vsDefaults.ReadOnly = true;
			this.vsDefaults.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDefaults.RowHeightMin = 0;
			this.vsDefaults.Rows = 1;
			this.vsDefaults.ScrollTipText = null;
			this.vsDefaults.ShowColumnVisibilityMenu = false;
			this.vsDefaults.Size = new System.Drawing.Size(381, 45);
			this.vsDefaults.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDefaults.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsDefaults.TabIndex = 0;
			this.vsDefaults.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsDefaults_BeforeEdit);
			this.vsDefaults.CurrentCellChanged += new System.EventHandler(this.vsDefaults_RowColChange);
			this.vsDefaults.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDefaults_MouseMoveEvent);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtShutOffDate);
			this.Frame3.Location = new System.Drawing.Point(30, 130);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(170, 90);
			this.Frame3.TabIndex = 1;
			this.Frame3.Text = "Shut Off Date";
			// 
			// txtShutOffDate
			// 
			this.txtShutOffDate.Location = new System.Drawing.Point(20, 30);
			this.txtShutOffDate.Mask = "##/##/####";
			this.txtShutOffDate.MaxLength = 10;
			this.txtShutOffDate.Name = "txtShutOffDate";
			this.txtShutOffDate.Size = new System.Drawing.Size(130, 40);
			this.txtShutOffDate.TabIndex = 0;
			this.txtShutOffDate.Text = "  /  /";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.txtNoticeDate);
			this.Frame2.Location = new System.Drawing.Point(30, 30);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(170, 90);
			this.Frame2.TabIndex = 0;
			this.Frame2.Text = "Notice Date";
			// 
			// txtNoticeDate
			// 
			this.txtNoticeDate.Location = new System.Drawing.Point(20, 30);
			this.txtNoticeDate.Mask = "##/##/####";
			this.txtNoticeDate.MaxLength = 10;
			this.txtNoticeDate.Name = "txtNoticeDate";
			this.txtNoticeDate.Size = new System.Drawing.Size(130, 40);
			this.txtNoticeDate.TabIndex = 0;
			this.txtNoticeDate.Text = "  /  /";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.fraSelectedBooks);
			this.Frame1.Controls.Add(this.cmbSelected);
			this.Frame1.Controls.Add(this.lblSelected);
			this.Frame1.Location = new System.Drawing.Point(222, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(421, 160);
			this.Frame1.TabIndex = 3;
			this.Frame1.Text = "Account Range";
			// 
			// fraSelectedBooks
			// 
			this.fraSelectedBooks.AppearanceKey = "groupBoxNoBorders";
			this.fraSelectedBooks.Controls.Add(this.txtSequenceNumber);
			this.fraSelectedBooks.Controls.Add(this.Label1);
			this.fraSelectedBooks.Enabled = false;
			this.fraSelectedBooks.Location = new System.Drawing.Point(1, 78);
			this.fraSelectedBooks.Name = "fraSelectedBooks";
			this.fraSelectedBooks.Size = new System.Drawing.Size(305, 68);
			this.fraSelectedBooks.TabIndex = 2;
			// 
			// txtSequenceNumber
			// 
			this.txtSequenceNumber.AutoSize = false;
			this.txtSequenceNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtSequenceNumber.LinkItem = null;
			this.txtSequenceNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSequenceNumber.LinkTopic = null;
			this.txtSequenceNumber.Location = new System.Drawing.Point(195, 20);
			this.txtSequenceNumber.Name = "txtSequenceNumber";
			this.txtSequenceNumber.Size = new System.Drawing.Size(89, 40);
			this.txtSequenceNumber.TabIndex = 1;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 34);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(154, 18);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "STARTING SEQUENCE #";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(267, 47);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(112, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmSetupDisconnectEditReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(655, 579);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupDisconnectEditReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Disconnect Edit Report";
			this.Load += new System.EventHandler(this.frmSetupDisconnectEditReport_Load);
			this.Activated += new System.EventHandler(this.frmSetupDisconnectEditReport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupDisconnectEditReport_KeyPress);
			this.Resize += new System.EventHandler(this.frmSetupDisconnectEditReport_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkExcludeNoBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowSrvcLoc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraDefaultInfo)).EndInit();
			this.fraDefaultInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsDefaults)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtShutOffDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtNoticeDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedBooks)).EndInit();
			this.fraSelectedBooks.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}
