﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptReminderForm.
	/// </summary>
	public partial class srptReminderForm : FCSectionReport
	{
		public srptReminderForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Form Breakdown";
		}

		public static srptReminderForm InstancePtr
		{
			get
			{
				return (srptReminderForm)Sys.GetInstance(typeof(srptReminderForm));
			}
		}

		protected srptReminderForm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsLData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptReminderForm	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/21/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/21/2006              *
		// ********************************************************
		int lngAcct;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		int lngHighYear;
		double dblPrin;
		double dblInt;
		double dblTax;
		DateTime dtMailingDate;
		double dblTotalPrin;
		double dblTotalInt;
		double dblTotalTax;
		bool boolWater;
		bool boolBothServices;
		int lngIndex;
		bool boolLienedAccounts;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (Conversion.Val(this.UserData) != 0)
				{
					// if nothing is passed in, then exit this function
					lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
					// this is the account number to process
				}
				else
				{
					this.Close();
				}
				boolBothServices = FCConvert.CBool(frmReminderNotices.InstancePtr.chkCombineService.CheckState == Wisej.Web.CheckState.Checked);
				boolLienedAccounts = frmReminderNotices.InstancePtr.boolLienedRecords;
				boolWater = frmReminderNotices.InstancePtr.boolWater;
				dblTotalPrin = 0;
				dblTotalInt = 0;
				dblTotalTax = 0;
				// this will get the highest year that should be shown on the
				// lngHighYear = Val(frmReminderNotices.vsGrid.TextMatrix(8, 1))
				dtMailingDate = DateAndTime.DateValue(frmReminderNotices.InstancePtr.vsGrid.TextMatrix(3, 1));
				rsLData.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
				if (boolBothServices)
				{
					rsData.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
				}
				else
				{
					if (boolWater)
					{
						rsData.OpenRecordset("SELECT * FROM Bill WHERE Service <> 'S' AND ActualAccountNumber = " + FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
						// & rptReminderForm.strRKSQL
					}
					else
					{
						rsData.OpenRecordset("SELECT * FROM Bill WHERE Service <> 'W' AND ActualAccountNumber = " + FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
					}
				}
				if (rsData.EndOfFile())
				{
					// if I cannot find the account, then exit
					this.Close();
				}
				else
				{
					// rock on
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Subreport", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblTotal;
				double dblPrinToPeriod = 0;
				int intPer;
				double dblPrinAfterPer = 0;
				bool boolCheckPayments;
				double[] dblAbateAmt = new double[4 + 1];
				double dblPrinAmt;
				double dblTaxAmt;
				int intRatePer;
				double dblCurInt = 0;
				string strWS = "";
				string strOppWS = "";
				bool boolTryAgain = false;
				lblYear.Text = "";
				lblDate.Text = "";
				// kgk trout-775 12-15-2011
				lblPrincipal.Text = "";
				lblTax.Text = "";
				lblInterest.Text = "";
				lblTotal.Text = "";
				if (boolWater)
				{
					strWS = "W";
					strOppWS = "S";
				}
				else
				{
					strWS = "S";
					strOppWS = "W";
				}
				TryAgain:
				;
				dblTotal = 0;
				dblAbateAmt[1] = 0;
				dblAbateAmt[2] = 0;
				dblAbateAmt[3] = 0;
				dblAbateAmt[4] = 0;
				if (!rsData.EndOfFile())
				{
					if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
					{
						if (rptReminderForm.InstancePtr.lngLastRateKey != 0 && rsData.Get_Fields_Int32("BillingRateKey") > rptReminderForm.InstancePtr.lngLastRateKey)
						{
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
						boolCheckPayments = true;
						// if it is then check the period that was selected
						// period 1
						if (boolBothServices)
						{
							// DJW@01142013 TROUT-909 Had to check and see if other service is liened when Combine WS is selected and if other service is liened we do not include that services amounts
							if (boolWater)
							{
								if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SLienRecordNumber")) == 0)
								{
									dblPrinToPeriod = rsData.Get_Fields_Double("WPrinOwed") + rsData.Get_Fields_Double("SPrinOwed") - rsData.Get_Fields_Double("WPrinPaid") - rsData.Get_Fields_Double("SPrinPaid");
									dblPrinAmt = rsData.Get_Fields_Double("WPrinOwed") + rsData.Get_Fields_Double("SPrinOwed");
									dblTax = rsData.Get_Fields_Double("WTaxOwed") + rsData.Get_Fields_Double("STaxOwed") - rsData.Get_Fields_Double("WTaxPaid") - rsData.Get_Fields_Double("STaxPaid");
								}
								else
								{
									dblPrinToPeriod = rsData.Get_Fields(strWS + "PrinOwed") - rsData.Get_Fields(strWS + "PrinPaid");
									dblPrinAmt = rsData.Get_Fields(strWS + "PrinOwed");
									dblTax = rsData.Get_Fields(strWS + "TaxOwed") - rsData.Get_Fields(strWS + "TaxPaid");
								}
							}
							else
							{
								if (FCConvert.ToInt32(rsData.Get_Fields_Int32("WLienRecordNumber")) == 0)
								{
									dblPrinToPeriod = rsData.Get_Fields_Double("WPrinOwed") + rsData.Get_Fields_Double("SPrinOwed") - rsData.Get_Fields_Double("WPrinPaid") - rsData.Get_Fields_Double("SPrinPaid");
									dblPrinAmt = rsData.Get_Fields_Double("WPrinOwed") + rsData.Get_Fields_Double("SPrinOwed");
									dblTax = rsData.Get_Fields_Double("WTaxOwed") + rsData.Get_Fields_Double("STaxOwed") - rsData.Get_Fields_Double("WTaxPaid") - rsData.Get_Fields_Double("STaxPaid");
								}
								else
								{
									dblPrinToPeriod = rsData.Get_Fields(strWS + "PrinOwed") - rsData.Get_Fields(strWS + "PrinPaid");
									dblPrinAmt = rsData.Get_Fields(strWS + "PrinOwed");
									dblTax = rsData.Get_Fields(strWS + "TaxOwed") - rsData.Get_Fields(strWS + "TaxPaid");
								}
							}
						}
						else
						{
							dblPrinToPeriod = rsData.Get_Fields(strWS + "PrinOwed") - rsData.Get_Fields(strWS + "PrinPaid");
							dblPrinAmt = rsData.Get_Fields(strWS + "PrinOwed");
							dblTax = rsData.Get_Fields(strWS + "TaxOwed") - rsData.Get_Fields(strWS + "TaxPaid");
						}
						// check to see if this year needs to be shown
						if (FCUtils.Round(dblPrinToPeriod, 2) > 0)
						{
							// there is still tax due
							// so allow the program to
						}
						else
						{
							// there is no tax due, so try the next year
							// then check to make sure that there are no ababtements that would allow this bill to be shown
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
						// non lien year calculations
						dblPrin = FCUtils.Round(dblPrinToPeriod, 2);
						dblInt = 0;
						double dblCurCost = 0;
						double dblCurTax = 0;
						double dblPerDiem = 0;
						bool boolForcePerDiem = false;
						bool blnForceBillIntDate = false;
						//FC:FINAL:DDU:#i924 - fixed calculation error method
						dblCurCost = FCConvert.ToDouble(rsData.Get_Fields(strWS + "CostOwed") - rsData.Get_Fields(strWS + "CostPaid"));
						dblTotal = FCUtils.Round(modUTCalculations.CalculateAccountUT(rsData, ref dtMailingDate, ref dblInt, boolWater, ref dblCurInt, ref dblCurCost), 2);
						if (boolBothServices)
						{
							dblCurCost = FCConvert.ToDouble(rsData.Get_Fields(strWS + "CostOwed") - rsData.Get_Fields(strWS + "CostPaid"));
							dblCurTax = 0;
							dblPerDiem = 0;
							boolForcePerDiem = false;
							blnForceBillIntDate = false;
							dblTotal += FCUtils.Round(modUTCalculations.CalculateAccountUT(rsData, ref dtMailingDate, ref dblInt, !boolWater, ref dblCurInt, ref dblCurCost), 2);
						}
						// add the values into the sums
						dblTotalPrin += dblPrin;
						dblTotalTax += dblTax;
						dblTotalInt += dblTotal - dblTax - dblPrin - dblPrinAfterPer;
						// fill the labels
						lblYear.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillingRateKey"));
						lblDate.Text = Strings.Format(rsData.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
						// kgk trout-775 12-15-2011
						lblPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
						lblTax.Text = Strings.Format(dblTax, "#,##0.00");
						lblInterest.Text = Strings.Format(dblTotal - dblPrin - dblTax - dblPrinAfterPer, "#,##0.00");
						lblTotal.Text = Strings.Format(dblTotal - dblPrinAfterPer, "#,##0.00");
						lngIndex = Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1);
						if (!modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].DoNotOverwrite)
						{
							// arrReminderSummaryList(lngIndex).Name1 = rsData.Fields("BName")
							// arrReminderSummaryList(lngIndex).Addr1 = rsData.Fields("BAddress1")
							// arrReminderSummaryList(lngIndex).Addr2 = rsData.Fields("BAddress2")
							// arrReminderSummaryList(lngIndex).Addr3 = rsData.Fields("BAddress3")
							// arrReminderSummaryList(lngIndex).Addr4 = ""
							// 
							// arrReminderSummaryList(lngIndex).Year = rsData.Fields("BillingYear")
							// arrReminderSummaryList(lngIndex).Account = rsData.Fields("ActualAccountNumber")
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total += dblTotal - dblPrinAfterPer;
						}
						// move to the next year
						rsData.MoveNext();
					}
					else
					{
						// check to make sure this is the main bill for this lean
						if (rsData.Get_Fields(strWS + "CombinationLienKey") != rsData.Get_Fields_Int32("ID"))
						{
							// kk12182015 trouts-181  Changed this from rsData.Fields("Bill")
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
						// lien
						if (boolBothServices)
						{
							// combined amounts
							boolTryAgain = false;
							lblYear.Text = "";
							rsLData.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
							if (!rsLData.NoMatch)
							{
								if (rptReminderForm.InstancePtr.lngLastRateKey != 0 && rsLData.Get_Fields_Int32("RateKey") > rptReminderForm.InstancePtr.lngLastRateKey)
								{
									rsData.MoveNext();
									if (rsData.EndOfFile())
									{
										return;
									}
									else
									{
										goto TryAgain;
									}
								}
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								if (rsLData.Get_Fields("Principal") - rsLData.Get_Fields("PrinPaid") > 0)
								{
									// allow this year to go through
								}
								else
								{
									boolTryAgain = true;
								}
								// lien year calculations
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								dblPrin = rsLData.Get_Fields("Principal") - rsLData.Get_Fields("PrinPaid");
								// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
								dblTax = rsLData.Get_Fields("Tax") - rsLData.Get_Fields("TaxPaid");
								dblInt = 0;
								dblTotal = modUTCalculations.CalculateAccountUTLien(rsLData, dtMailingDate, ref dblInt, boolWater);
								// fill the labels
								lblYear.Text = "LIEN " + rsLData.Get_Fields_Int32("RateKey");
								// this will get the billing year from the BillingMaster even if this is a lien
								lblDate.Text = Strings.Format(rsLData.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy");
								// kgk trout-775 12-15-2011
							}
							else
							{
								// no matching lien record
								boolTryAgain = true;
							}
							rsLData.FindFirstRecord("ID", rsData.Get_Fields(strOppWS + "LienRecordNumber"));
							if (!rsLData.NoMatch)
							{
								if (rptReminderForm.InstancePtr.lngLastRateKey != 0 && rsLData.Get_Fields_Int32("RateKey") > rptReminderForm.InstancePtr.lngLastRateKey)
								{
									rsData.MoveNext();
									if (rsData.EndOfFile())
									{
										return;
									}
									else
									{
										goto TryAgain;
									}
								}
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								if (rsLData.Get_Fields("Principal") - rsLData.Get_Fields("PrinPaid") > 0)
								{
									// allow this year to go through
								}
								else
								{
									if (boolTryAgain)
									{
										rsData.MoveNext();
										if (rsData.EndOfFile())
										{
											return;
										}
										else
										{
											goto TryAgain;
										}
									}
								}
								// lien year calculations
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								dblPrin += rsLData.Get_Fields("Principal") - rsLData.Get_Fields("PrinPaid");
								// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
								dblTax += rsLData.Get_Fields("Tax") - rsLData.Get_Fields("TaxPaid");
								dblInt = 0;
								dblTotal += modUTCalculations.CalculateAccountUTLien(rsLData, dtMailingDate, ref dblInt, !boolWater);
								if (Strings.Trim(lblYear.Text) != "")
								{
									lblYear.Text = " and " + rsLData.Get_Fields_Int32("RateKey");
								}
								else
								{
									lblYear.Text = "LIEN " + rsLData.Get_Fields_Int32("RateKey");
								}
								lblDate.Text = Strings.Format(rsLData.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy");
								// kgk trout-775 12-15-2011
							}
							else
							{
								// no matching lien record
								if (boolTryAgain)
								{
									rsData.MoveNext();
									if (rsData.EndOfFile())
									{
										return;
									}
									else
									{
										goto TryAgain;
									}
								}
							}
							// add the values into the sums
							dblTotalPrin += dblPrin;
							dblTotalTax += dblTax;
							dblTotalInt += dblTotal - dblTax - dblPrin;
							lblPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
							lblTax.Text = Strings.Format(dblTax, "#,##0.00");
							lblInterest.Text = Strings.Format(dblTotal - dblTax - dblPrin, "#,##0.00");
							lblTotal.Text = Strings.Format(dblTotal, "#,##0.00");
							lngIndex = Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1);
							// arrReminderSummaryList(lngIndex).Account = rsData.Fields("ActualAccountNumber")
							// arrReminderSummaryList(lngIndex).Addr1 = rsData.Fields("BAddress1")
							// arrReminderSummaryList(lngIndex).Addr2 = rsData.Fields("BAddress2")
							// arrReminderSummaryList(lngIndex).Addr3 = rsData.Fields("BAddress3")
							// arrReminderSummaryList(lngIndex).Addr4 = ""
							// arrReminderSummaryList(lngIndex).Year = rsData.Fields("BillingYear")
							// arrReminderSummaryList(lngIndex).Name1 = rsData.Fields("BName")
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total += dblTotal;
							rsData.MoveNext();
						}
						else
						{
							// just single type
							rsLData.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
							if (!rsLData.NoMatch)
							{
								if (rptReminderForm.InstancePtr.lngLastRateKey != 0 && rsLData.Get_Fields_Int32("RateKey") > rptReminderForm.InstancePtr.lngLastRateKey)
								{
									rsData.MoveNext();
									if (rsData.EndOfFile())
									{
										return;
									}
									else
									{
										goto TryAgain;
									}
								}
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								if (rsLData.Get_Fields("Principal") - rsLData.Get_Fields("PrinPaid") > 0)
								{
									// allow this year to go through
								}
								else
								{
									rsData.MoveNext();
									if (rsData.EndOfFile())
									{
										return;
									}
									else
									{
										goto TryAgain;
									}
								}
								// lien year calculations
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								dblPrin = rsLData.Get_Fields("Principal") - rsLData.Get_Fields("PrinPaid");
								// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
								dblTax = rsLData.Get_Fields("Tax") - rsLData.Get_Fields("TaxPaid");
								dblInt = 0;
								dblTotal = modUTCalculations.CalculateAccountUTLien(rsLData, dtMailingDate, ref dblInt, boolWater);
								// add the values into the sums
								dblTotalPrin += dblPrin;
								dblTotalTax += dblTax;
								dblTotalInt += dblTotal - dblTax - dblPrin;
								// fill the labels
								lblYear.Text = "LIEN " + rsLData.Get_Fields_Int32("RateKey");
								// this will get the billing year from the BillingMaster even if this is a lien
								lblDate.Text = Strings.Format(rsLData.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy");
								// kgk trout-775 12-15-2011
								lblPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
								lblTax.Text = Strings.Format(dblTax, "#,##0.00");
								lblInterest.Text = Strings.Format(dblTotal - dblTax - dblPrin, "#,##0.00");
								lblTotal.Text = Strings.Format(dblTotal, "#,##0.00");
								lngIndex = Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1);
								// arrReminderSummaryList(lngIndex).Account = rsData.Fields("ActualAccountNumber")
								// arrReminderSummaryList(lngIndex).Addr1 = rsData.Fields("BAddress1")
								// arrReminderSummaryList(lngIndex).Addr2 = rsData.Fields("BAddress2")
								// arrReminderSummaryList(lngIndex).Addr3 = rsData.Fields("BAddress3")
								// arrReminderSummaryList(lngIndex).Addr4 = ""
								// arrReminderSummaryList(lngIndex).Year = rsData.Fields("BillingYear")
								// arrReminderSummaryList(lngIndex).Name1 = rsData.Fields("BName")
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total += dblTotal;
								// move to the next year
								rsData.MoveNext();
							}
							else
							{
								// no matching lien record
								rsData.MoveNext();
								if (rsData.EndOfFile())
								{
									return;
								}
								else
								{
									goto TryAgain;
								}
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Bind Fields - 122", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// this will fill the sums into the footer line
			lblFooterPrincipal.Text = Strings.Format(dblTotalPrin, "#,##0.00");
			lblFooterTax.Text = Strings.Format(dblTotalTax, "#,##0.00");
			lblFooterInterest.Text = Strings.Format(dblTotalInt, "#,##0.00");
			lblFooterTotal.Text = Strings.Format(dblTotalInt + dblTotalTax + dblTotalPrin, "#,##0.00");
		}

		private void srptReminderForm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptReminderForm properties;
			//srptReminderForm.Caption	= "Reminder Form Breakdown";
			//srptReminderForm.Icon	= "srptReminderForm.dsx":0000";
			//srptReminderForm.Left	= 0;
			//srptReminderForm.Top	= 0;
			//srptReminderForm.Width	= 11880;
			//srptReminderForm.Height	= 5295;
			//srptReminderForm.StartUpPosition	= 3;
			//srptReminderForm.SectionData	= "srptReminderForm.dsx":058A;
			//End Unmaped Properties
		}
	}
}
