﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmAccountMaster.
	/// </summary>
	partial class frmAccountMaster : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWBillTo;
		public fecherFoundation.FCComboBox cmbSBillTo;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblBook;
		public fecherFoundation.FCPanel fraMaster;
		public fecherFoundation.FCFrame fraMasterTB;
		public fecherFoundation.FCTextBox txtImpervSurf;
		public fecherFoundation.FCCheckBox chkBillSameAsOwner;
		public fecherFoundation.FCCheckBox chkNoBill;
		public fecherFoundation.FCCheckBox chkFinal;
		public fecherFoundation.FCCheckBox chkEmailBills;
		public fecherFoundation.FCTextBox txtComment;
		public Global.T2KPhoneNumberBox txtPhone;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCTextBox txtBillMessage;
		public fecherFoundation.FCTextBox txtDEMessage;
		public fecherFoundation.FCTextBox txtDirections;
		public fecherFoundation.FCTextBox txtLocationNumber;
		public fecherFoundation.FCTextBox txtLocationStreet;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtTenant2CustNum;
		public fecherFoundation.FCButton cmdTenant2Search;
		public fecherFoundation.FCButton cmdTenant2Edit;
		public fecherFoundation.FCTextBox txtTenant1CustNum;
		public fecherFoundation.FCButton cmdTenant1Search;
		public fecherFoundation.FCButton cmdTenant1Edit;
		public fecherFoundation.FCLabel lblTenantCustInfo;
		public fecherFoundation.FCPictureBox imgTenant2Memo;
		public fecherFoundation.FCPictureBox imgTenant1Memo;
		public fecherFoundation.FCFrame fraOwnerDisp;
		public fecherFoundation.FCTextBox txtOwner2CustNum;
		public fecherFoundation.FCButton cmdOwner2Search;
		public fecherFoundation.FCButton cmdOwner2Edit;
		public fecherFoundation.FCTextBox txtOwner1CustNum;
		public fecherFoundation.FCButton cmdOwner1Search;
		public fecherFoundation.FCButton cmdOwner1Edit;
		public fecherFoundation.FCLabel lblOwnerCustInfo;
		public fecherFoundation.FCPictureBox imgOwner2Memo;
		public fecherFoundation.FCPictureBox imgOwner1Memo;
		public fecherFoundation.FCLabel lblCustomerNumber;
		public fecherFoundation.FCLabel lblImpervSurf;
		public fecherFoundation.FCLabel lblTaxAcquired;
		public fecherFoundation.FCLabel lblBankruptcy;
		public fecherFoundation.FCLabel lblMasterBook;
		public fecherFoundation.FCLabel lblAcctComment;
		public fecherFoundation.FCLabel lblComment;
		public fecherFoundation.FCLabel lblEmail;
		public fecherFoundation.FCLabel lblTelephone;
		public fecherFoundation.FCLabel lblBillMessage;
		public fecherFoundation.FCLabel lblDEMessage;
		public fecherFoundation.FCLabel lblDirections;
		public fecherFoundation.FCLabel lblLocation;
		public fecherFoundation.FCLabel lblAcctNumberText;
		public fecherFoundation.FCLabel lblAcctNumber;
		public fecherFoundation.FCFrame fraREInfo;
		public fecherFoundation.FCCheckBox chkUseRE;
		public fecherFoundation.FCTextBox txtREPage;
		public fecherFoundation.FCTextBox txtREBook;
		public fecherFoundation.FCTextBox txtREMapLot;
		public fecherFoundation.FCTextBox txtREAcct;
		public fecherFoundation.FCCheckBox chkUseREMortgage;
		public fecherFoundation.FCLabel lblREPage;
		public fecherFoundation.FCLabel lblREBook;
		public fecherFoundation.FCLabel lblREMapLot;
		public fecherFoundation.FCLabel lblREAcct;
		public fecherFoundation.FCFrame fraWater;
		public fecherFoundation.FCComboBox cmbWCat;
		public FCGrid txtWaterAcct;
		public fecherFoundation.FCLabel lblWBillTo;
		public fecherFoundation.FCLabel lblWCat;
		public fecherFoundation.FCLabel lblWaterAccountNumber;
		public fecherFoundation.FCFrame fraSewer;
		public fecherFoundation.FCComboBox cmbSCat;
		public FCGrid txtSewerAcct;
		public fecherFoundation.FCLabel lblSBillTo;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraMasterGrid;
		public fecherFoundation.FCGrid vsMaster;
		public fecherFoundation.FCLabel lblAccountNumber;
		public fecherFoundation.FCLabel txtAccountNumber;
		public fecherFoundation.FCFrame fraChangeOutHistory;
		public fecherFoundation.FCButton cmdChangeOutClose;
		public FCGrid vsChangeOut;
		public fecherFoundation.FCFrame fraFinalBillInfo;
		public Global.T2KDateBox txtPeriodStart;
		public Global.T2KDateBox txtPeriodEnd;
		public Global.T2KDateBox txtFinalBillDate;
		public fecherFoundation.FCLabel lblFinalbillDate;
		public fecherFoundation.FCLabel lblPeriodEnd;
		public fecherFoundation.FCLabel lblPerStart;
		public fecherFoundation.FCPanel fraMeter;
		public fecherFoundation.FCFrame fraAccountInfo;
		public FCGrid vsAccountInfo;
		public fecherFoundation.FCFrame fraReading;
		public fecherFoundation.FCTextBox txtLong;
		public fecherFoundation.FCTextBox txtLat;
		public fecherFoundation.FCTextBox txtXRef2;
		public fecherFoundation.FCTextBox txtXRef1;
		public fecherFoundation.FCTextBox txtLocation;
		public FCGrid vsReading;
		public fecherFoundation.FCLabel lblLong;
		public fecherFoundation.FCLabel lblLat;
		public fecherFoundation.FCLabel lblXRef2;
		public fecherFoundation.FCLabel lblXRef1;
		public fecherFoundation.FCLabel lblMeterLocation;
		public fecherFoundation.FCFrame fraMiddle;
		public fecherFoundation.FCTextBox txtReaderCode;
		public fecherFoundation.FCComboBox cmbRadio;
		public fecherFoundation.FCTextBox txtMXU;
		public fecherFoundation.FCCheckBox chkNegativeConsumption;
		public fecherFoundation.FCCheckBox chkIncludeInExtract;
		public fecherFoundation.FCCheckBox chkMeterFinalBill;
		public fecherFoundation.FCCheckBox chkMeterNoBill;
		public fecherFoundation.FCTextBox txtRemoteNumber;
		public fecherFoundation.FCTextBox txtSerialNumber;
		public FCGrid vsPercentage;
		public fecherFoundation.FCLabel lblReaderCode;
		public fecherFoundation.FCLabel lblMXU;
		public fecherFoundation.FCLabel lblRemoteNumber;
		public fecherFoundation.FCLabel lblSerialNumber;
		public fecherFoundation.FCFrame fraCombos;
		public fecherFoundation.FCComboBox cmbCombineMeter;
		public fecherFoundation.FCComboBox cmbBook;
		public fecherFoundation.FCComboBox cmbFrequency;
		public fecherFoundation.FCComboBox cmbService;
		public fecherFoundation.FCComboBox cmbMultiplier;
		public fecherFoundation.FCComboBox cmbMeterDigits;
		public fecherFoundation.FCComboBox cmbMeterSize;
		public fecherFoundation.FCTextBox txtSequence;
		public fecherFoundation.FCLabel lblCombineMeters;
		public fecherFoundation.FCLabel lblFrequency;
		public fecherFoundation.FCLabel lblService;
		public fecherFoundation.FCLabel lblMultiplier;
		public fecherFoundation.FCLabel lblMeterDigits;
		public fecherFoundation.FCLabel lblMeterSize;
		public fecherFoundation.FCLabel lblSequence;
		public fecherFoundation.FCLabel lblBook_0;
		public fecherFoundation.FCFrame fraCurrentMeter;
		public fecherFoundation.FCComboBox cmbMeterNumber;
		public fecherFoundation.FCLabel lblFinalBill;
		public fecherFoundation.FCFrame fraRates;
		public fecherFoundation.FCTextBox txtAdjustDescriptionS;
		public FCGrid vsMeter;
		public fecherFoundation.FCTextBox txtAdjustDescriptionW;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileMeterAdd;
		public fecherFoundation.FCToolStripMenuItem mnuFileMeterDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFileDeleteAccount;
		public fecherFoundation.FCToolStripMenuItem mnuFileComment;
		public fecherFoundation.FCToolStripMenuItem mnuFileFindSeq;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreviousOwner;
		public fecherFoundation.FCToolStripMenuItem mnuFileResetMeterStatus;
		public fecherFoundation.FCToolStripMenuItem mnuFileChangeOutHistory;
		public fecherFoundation.FCToolStripMenuItem mnuFileBankrupcty;
        public fecherFoundation.FCToolStripMenuItem mnuAutoPay;
		public fecherFoundation.FCToolStripMenuItem mnuSpeer;
		public fecherFoundation.FCToolStripMenuItem mnuFileViewLinkedDocument;
		public fecherFoundation.FCToolStripMenuItem seperator3;
		public fecherFoundation.FCToolStripMenuItem mnuFileMeterPrevious;
		public fecherFoundation.FCToolStripMenuItem mnuFileMeterNext;
		public fecherFoundation.FCToolStripMenuItem mnuFileShowMeter;
		public fecherFoundation.FCToolStripMenuItem seperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccountMaster));
			this.txtTenant2CustNum = new fecherFoundation.FCTextBox();
			this.txtTenant1CustNum = new fecherFoundation.FCTextBox();
			this.txtOwner2CustNum = new fecherFoundation.FCTextBox();
			this.txtOwner1CustNum = new fecherFoundation.FCTextBox();
			this.cmbWBillTo = new fecherFoundation.FCComboBox();
			this.cmbSBillTo = new fecherFoundation.FCComboBox();
			this.fraMaster = new fecherFoundation.FCPanel();
			this.fraREInfo = new fecherFoundation.FCFrame();
			this.chkUseRE = new fecherFoundation.FCCheckBox();
			this.txtREPage = new fecherFoundation.FCTextBox();
			this.txtREBook = new fecherFoundation.FCTextBox();
			this.txtREMapLot = new fecherFoundation.FCTextBox();
			this.txtREAcct = new fecherFoundation.FCTextBox();
			this.chkUseREMortgage = new fecherFoundation.FCCheckBox();
			this.lblREPage = new fecherFoundation.FCLabel();
			this.lblREBook = new fecherFoundation.FCLabel();
			this.lblREMapLot = new fecherFoundation.FCLabel();
			this.lblREAcct = new fecherFoundation.FCLabel();
			this.fraMasterTB = new fecherFoundation.FCFrame();
			this.lblACHInfo = new fecherFoundation.FCLabel();
			this.txtImpervSurf = new fecherFoundation.FCTextBox();
			this.chkBillSameAsOwner = new fecherFoundation.FCCheckBox();
			this.chkNoBill = new fecherFoundation.FCCheckBox();
			this.chkFinal = new fecherFoundation.FCCheckBox();
			this.chkEmailBills = new fecherFoundation.FCCheckBox();
			this.txtComment = new fecherFoundation.FCTextBox();
			this.txtPhone = new Global.T2KPhoneNumberBox();
			this.txtEmail = new fecherFoundation.FCTextBox();
			this.txtBillMessage = new fecherFoundation.FCTextBox();
			this.txtDEMessage = new fecherFoundation.FCTextBox();
			this.txtDirections = new fecherFoundation.FCTextBox();
			this.txtLocationNumber = new fecherFoundation.FCTextBox();
			this.txtLocationStreet = new fecherFoundation.FCTextBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmdTenant2Search = new fecherFoundation.FCButton();
			this.cmdTenant2Edit = new fecherFoundation.FCButton();
			this.cmdTenant1Search = new fecherFoundation.FCButton();
			this.cmdTenant1Edit = new fecherFoundation.FCButton();
			this.lblTenantCustInfo = new fecherFoundation.FCLabel();
			this.imgTenant2Memo = new fecherFoundation.FCPictureBox();
			this.imgTenant1Memo = new fecherFoundation.FCPictureBox();
			this.fraOwnerDisp = new fecherFoundation.FCFrame();
			this.txtDeedName2 = new fecherFoundation.FCTextBox();
			this.txtDeedName1 = new fecherFoundation.FCTextBox();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.fcLabel2 = new fecherFoundation.FCLabel();
			this.cmdOwner2Search = new fecherFoundation.FCButton();
			this.cmdOwner2Edit = new fecherFoundation.FCButton();
			this.cmdOwner1Search = new fecherFoundation.FCButton();
			this.cmdOwner1Edit = new fecherFoundation.FCButton();
			this.lblOwnerCustInfo = new fecherFoundation.FCLabel();
			this.imgOwner2Memo = new fecherFoundation.FCPictureBox();
			this.imgOwner1Memo = new fecherFoundation.FCPictureBox();
			this.lblCustomerNumber = new fecherFoundation.FCLabel();
			this.lblImpervSurf = new fecherFoundation.FCLabel();
			this.lblTaxAcquired = new fecherFoundation.FCLabel();
			this.lblBankruptcy = new fecherFoundation.FCLabel();
			this.lblMasterBook = new fecherFoundation.FCLabel();
			this.lblAcctComment = new fecherFoundation.FCLabel();
			this.lblComment = new fecherFoundation.FCLabel();
			this.lblEmail = new fecherFoundation.FCLabel();
			this.lblTelephone = new fecherFoundation.FCLabel();
			this.lblBillMessage = new fecherFoundation.FCLabel();
			this.lblDEMessage = new fecherFoundation.FCLabel();
			this.lblDirections = new fecherFoundation.FCLabel();
			this.lblLocation = new fecherFoundation.FCLabel();
			this.lblAcctNumberText = new fecherFoundation.FCLabel();
			this.lblAcctNumber = new fecherFoundation.FCLabel();
			this.fraWater = new fecherFoundation.FCFrame();
			this.cmbWCat = new fecherFoundation.FCComboBox();
			this.txtWaterAcct = new fecherFoundation.FCGrid();
			this.lblWBillTo = new fecherFoundation.FCLabel();
			this.lblWCat = new fecherFoundation.FCLabel();
			this.lblWaterAccountNumber = new fecherFoundation.FCLabel();
			this.fraSewer = new fecherFoundation.FCFrame();
			this.cmbSCat = new fecherFoundation.FCComboBox();
			this.txtSewerAcct = new fecherFoundation.FCGrid();
			this.lblSBillTo = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.fraMasterGrid = new fecherFoundation.FCFrame();
			this.vsMaster = new fecherFoundation.FCGrid();
			this.lblAccountNumber = new fecherFoundation.FCLabel();
			this.txtAccountNumber = new fecherFoundation.FCLabel();
			this.fraChangeOutHistory = new fecherFoundation.FCFrame();
			this.cmdChangeOutClose = new fecherFoundation.FCButton();
			this.vsChangeOut = new fecherFoundation.FCGrid();
			this.fraFinalBillInfo = new fecherFoundation.FCFrame();
			this.txtPeriodStart = new Global.T2KDateBox();
			this.txtPeriodEnd = new Global.T2KDateBox();
			this.txtFinalBillDate = new Global.T2KDateBox();
			this.lblFinalbillDate = new fecherFoundation.FCLabel();
			this.lblPeriodEnd = new fecherFoundation.FCLabel();
			this.lblPerStart = new fecherFoundation.FCLabel();
			this.fraMeter = new fecherFoundation.FCPanel();
			this.fraAccountInfo = new fecherFoundation.FCFrame();
			this.vsAccountInfo = new fecherFoundation.FCGrid();
			this.fraReading = new fecherFoundation.FCFrame();
			this.txtLong = new fecherFoundation.FCTextBox();
			this.txtLat = new fecherFoundation.FCTextBox();
			this.txtXRef2 = new fecherFoundation.FCTextBox();
			this.txtXRef1 = new fecherFoundation.FCTextBox();
			this.txtLocation = new fecherFoundation.FCTextBox();
			this.vsReading = new fecherFoundation.FCGrid();
			this.lblLong = new fecherFoundation.FCLabel();
			this.lblLat = new fecherFoundation.FCLabel();
			this.lblXRef2 = new fecherFoundation.FCLabel();
			this.lblXRef1 = new fecherFoundation.FCLabel();
			this.lblMeterLocation = new fecherFoundation.FCLabel();
			this.fraMiddle = new fecherFoundation.FCFrame();
			this.txtReaderCode = new fecherFoundation.FCTextBox();
			this.cmbRadio = new fecherFoundation.FCComboBox();
			this.txtMXU = new fecherFoundation.FCTextBox();
			this.chkNegativeConsumption = new fecherFoundation.FCCheckBox();
			this.chkIncludeInExtract = new fecherFoundation.FCCheckBox();
			this.chkMeterFinalBill = new fecherFoundation.FCCheckBox();
			this.chkMeterNoBill = new fecherFoundation.FCCheckBox();
			this.txtRemoteNumber = new fecherFoundation.FCTextBox();
			this.txtSerialNumber = new fecherFoundation.FCTextBox();
			this.vsPercentage = new fecherFoundation.FCGrid();
			this.lblReaderCode = new fecherFoundation.FCLabel();
			this.lblMXU = new fecherFoundation.FCLabel();
			this.lblRemoteNumber = new fecherFoundation.FCLabel();
			this.lblSerialNumber = new fecherFoundation.FCLabel();
			this.fraCombos = new fecherFoundation.FCFrame();
			this.cmbCombineMeter = new fecherFoundation.FCComboBox();
			this.cmbBook = new fecherFoundation.FCComboBox();
			this.cmbFrequency = new fecherFoundation.FCComboBox();
			this.cmbService = new fecherFoundation.FCComboBox();
			this.cmbMultiplier = new fecherFoundation.FCComboBox();
			this.cmbMeterDigits = new fecherFoundation.FCComboBox();
			this.cmbMeterSize = new fecherFoundation.FCComboBox();
			this.txtSequence = new fecherFoundation.FCTextBox();
			this.lblCombineMeters = new fecherFoundation.FCLabel();
			this.lblFrequency = new fecherFoundation.FCLabel();
			this.lblService = new fecherFoundation.FCLabel();
			this.lblMultiplier = new fecherFoundation.FCLabel();
			this.lblMeterDigits = new fecherFoundation.FCLabel();
			this.lblMeterSize = new fecherFoundation.FCLabel();
			this.lblSequence = new fecherFoundation.FCLabel();
			this.lblBook_0 = new fecherFoundation.FCLabel();
			this.fraCurrentMeter = new fecherFoundation.FCFrame();
			this.cmbMeterNumber = new fecherFoundation.FCComboBox();
			this.lblFinalBill = new fecherFoundation.FCLabel();
			this.fraRates = new fecherFoundation.FCFrame();
			this.txtAdjustDescriptionS = new fecherFoundation.FCTextBox();
			this.vsMeter = new fecherFoundation.FCGrid();
			this.txtAdjustDescriptionW = new fecherFoundation.FCTextBox();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuFileComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileFindSeq = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreviousOwner = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileResetMeterStatus = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileChangeOutHistory = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileBankrupcty = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAutoPay = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileViewLinkedDocument = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSpeer = new fecherFoundation.FCToolStripMenuItem();
			this.seperator3 = new fecherFoundation.FCToolStripMenuItem();
			this.seperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileShowMeter = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMeterPrevious = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMeterNext = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMeterAdd = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMeterDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDeleteAccount = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdFileMeterPrevious = new fecherFoundation.FCButton();
			this.cmdFileShowMeter = new fecherFoundation.FCButton();
			this.cmdFileMeterNext = new fecherFoundation.FCButton();
			this.cmdFileMeterAdd = new fecherFoundation.FCButton();
			this.cmdFileMeterDelete = new fecherFoundation.FCButton();
			this.cmdFileDeleteAccount = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMaster)).BeginInit();
			this.fraMaster.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraREInfo)).BeginInit();
			this.fraREInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkUseRE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseREMortgage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMasterTB)).BeginInit();
			this.fraMasterTB.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBillSameAsOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNoBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFinal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEmailBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdTenant2Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTenant2Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTenant1Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTenant1Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTenant2Memo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTenant1Memo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraOwnerDisp)).BeginInit();
			this.fraOwnerDisp.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwner2Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwner2Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwner1Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwner1Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgOwner2Memo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgOwner1Memo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWater)).BeginInit();
			this.fraWater.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtWaterAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSewer)).BeginInit();
			this.fraSewer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtSewerAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMasterGrid)).BeginInit();
			this.fraMasterGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsMaster)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChangeOutHistory)).BeginInit();
			this.fraChangeOutHistory.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdChangeOutClose)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsChangeOut)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFinalBillInfo)).BeginInit();
			this.fraFinalBillInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinalBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMeter)).BeginInit();
			this.fraMeter.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountInfo)).BeginInit();
			this.fraAccountInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccountInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraReading)).BeginInit();
			this.fraReading.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMiddle)).BeginInit();
			this.fraMiddle.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkNegativeConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeInExtract)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMeterFinalBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMeterNoBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPercentage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCombos)).BeginInit();
			this.fraCombos.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCurrentMeter)).BeginInit();
			this.fraCurrentMeter.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRates)).BeginInit();
			this.fraRates.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsMeter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMeterPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileShowMeter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMeterNext)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMeterAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMeterDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteAccount)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 1298);
			this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraMaster);
			this.ClientArea.Controls.Add(this.fraMeter);
			this.ClientArea.Controls.Add(this.fraChangeOutHistory);
			this.ClientArea.Controls.Add(this.fraFinalBillInfo);
			this.ClientArea.Size = new System.Drawing.Size(1078, 606);
			this.ClientArea.Controls.SetChildIndex(this.fraFinalBillInfo, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraChangeOutHistory, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraMeter, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraMaster, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileDeleteAccount);
			this.TopPanel.Controls.Add(this.cmdFileMeterDelete);
			this.TopPanel.Controls.Add(this.cmdFileMeterAdd);
			this.TopPanel.Controls.Add(this.cmdFileMeterPrevious);
			this.TopPanel.Controls.Add(this.cmdFileMeterNext);
			this.TopPanel.Controls.Add(this.cmdFileShowMeter);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileShowMeter, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileMeterNext, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileMeterPrevious, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileMeterAdd, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileMeterDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileDeleteAccount, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(170, 28);
			this.HeaderText.Text = "Account Master";
			// 
			// txtTenant2CustNum
			// 
			this.txtTenant2CustNum.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtTenant2CustNum.Location = new System.Drawing.Point(20, 90);
			this.txtTenant2CustNum.Name = "txtTenant2CustNum";
			this.txtTenant2CustNum.Size = new System.Drawing.Size(122, 40);
			this.txtTenant2CustNum.TabIndex = 136;
			this.txtTenant2CustNum.Validating += new System.ComponentModel.CancelEventHandler(this.txtTenant2CustNum_Validating);
			this.txtTenant2CustNum.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTenant2CustNum_KeyPress);
			// 
			// txtTenant1CustNum
			// 
			this.txtTenant1CustNum.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtTenant1CustNum.Location = new System.Drawing.Point(20, 30);
			this.txtTenant1CustNum.Name = "txtTenant1CustNum";
			this.txtTenant1CustNum.Size = new System.Drawing.Size(122, 40);
			this.txtTenant1CustNum.TabIndex = 133;
			this.txtTenant1CustNum.Validating += new System.ComponentModel.CancelEventHandler(this.txtTenant1CustNum_Validating);
			this.txtTenant1CustNum.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTenant1CustNum_KeyPress);
			// 
			// txtOwner2CustNum
			// 
			this.txtOwner2CustNum.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtOwner2CustNum.Location = new System.Drawing.Point(163, 90);
			this.txtOwner2CustNum.Name = "txtOwner2CustNum";
			this.txtOwner2CustNum.Size = new System.Drawing.Size(122, 40);
			this.txtOwner2CustNum.TabIndex = 129;
			this.txtOwner2CustNum.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwner2CustNum_Validating);
			this.txtOwner2CustNum.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOwner2CustNum_KeyPress);
			// 
			// txtOwner1CustNum
			// 
			this.txtOwner1CustNum.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtOwner1CustNum.Location = new System.Drawing.Point(163, 30);
			this.txtOwner1CustNum.Name = "txtOwner1CustNum";
			this.txtOwner1CustNum.Size = new System.Drawing.Size(122, 40);
			this.txtOwner1CustNum.TabIndex = 125;
			this.txtOwner1CustNum.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwner1CustNum_Validating);
			this.txtOwner1CustNum.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOwner1CustNum_KeyPress);
			// 
			// cmbWBillTo
			// 
			this.cmbWBillTo.Items.AddRange(new object[] {
            "Owner",
            "Tenant"});
			this.cmbWBillTo.Location = new System.Drawing.Point(151, 90);
			this.cmbWBillTo.Name = "cmbWBillTo";
			this.cmbWBillTo.Size = new System.Drawing.Size(180, 40);
			this.cmbWBillTo.TabIndex = 10;
			this.cmbWBillTo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmbSBillTo
			// 
			this.cmbSBillTo.Items.AddRange(new object[] {
            "Owner",
            "Tenant"});
			this.cmbSBillTo.Location = new System.Drawing.Point(150, 90);
			this.cmbSBillTo.Name = "cmbSBillTo";
			this.cmbSBillTo.Size = new System.Drawing.Size(180, 40);
			this.cmbSBillTo.TabIndex = 11;
			this.cmbSBillTo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraMaster
			// 
			this.fraMaster.AutoSize = true;
			this.fraMaster.BackColor = System.Drawing.SystemColors.Menu;
			this.fraMaster.Controls.Add(this.fraREInfo);
			this.fraMaster.Controls.Add(this.fraMasterTB);
			this.fraMaster.Controls.Add(this.fraWater);
			this.fraMaster.Controls.Add(this.fraSewer);
			this.fraMaster.Controls.Add(this.fraMasterGrid);
			this.fraMaster.Location = new System.Drawing.Point(0, -8);
			this.fraMaster.Name = "fraMaster";
			this.fraMaster.Size = new System.Drawing.Size(1005, 1306);
			this.fraMaster.TabIndex = 18;
			this.fraMaster.Visible = false;
			this.fraMaster.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraREInfo
			// 
			this.fraREInfo.AppearanceKey = "groupBoxNoBorders";
			this.fraREInfo.Controls.Add(this.chkUseRE);
			this.fraREInfo.Controls.Add(this.txtREPage);
			this.fraREInfo.Controls.Add(this.txtREBook);
			this.fraREInfo.Controls.Add(this.txtREMapLot);
			this.fraREInfo.Controls.Add(this.txtREAcct);
			this.fraREInfo.Controls.Add(this.chkUseREMortgage);
			this.fraREInfo.Controls.Add(this.lblREPage);
			this.fraREInfo.Controls.Add(this.lblREBook);
			this.fraREInfo.Controls.Add(this.lblREMapLot);
			this.fraREInfo.Controls.Add(this.lblREAcct);
			this.fraREInfo.Location = new System.Drawing.Point(30, 1110);
			this.fraREInfo.Name = "fraREInfo";
			this.fraREInfo.Size = new System.Drawing.Size(562, 156);
			this.fraREInfo.TabIndex = 46;
			this.fraREInfo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// chkUseRE
			// 
			this.chkUseRE.Enabled = false;
			this.chkUseRE.Location = new System.Drawing.Point(-3, 130);
			this.chkUseRE.Name = "chkUseRE";
			this.chkUseRE.Size = new System.Drawing.Size(109, 22);
			this.chkUseRE.TabIndex = 15;
			this.chkUseRE.Text = "Use RE Info?";
			this.chkUseRE.CheckedChanged += new System.EventHandler(this.chkUseRE_CheckedChanged);
			// 
			// txtREPage
			// 
			this.txtREPage.BackColor = System.Drawing.SystemColors.Window;
			this.txtREPage.Location = new System.Drawing.Point(463, 73);
			this.txtREPage.MaxLength = 5;
			this.txtREPage.Name = "txtREPage";
			this.txtREPage.Size = new System.Drawing.Size(89, 40);
			this.txtREPage.TabIndex = 14;
			this.txtREPage.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtREBook
			// 
			this.txtREBook.BackColor = System.Drawing.SystemColors.Window;
			this.txtREBook.Location = new System.Drawing.Point(463, 13);
			this.txtREBook.MaxLength = 5;
			this.txtREBook.Name = "txtREBook";
			this.txtREBook.Size = new System.Drawing.Size(89, 40);
			this.txtREBook.TabIndex = 13;
			this.txtREBook.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtREMapLot
			// 
			this.txtREMapLot.BackColor = System.Drawing.SystemColors.Window;
			this.txtREMapLot.Location = new System.Drawing.Point(142, 73);
			this.txtREMapLot.MaxLength = 17;
			this.txtREMapLot.Name = "txtREMapLot";
			this.txtREMapLot.Size = new System.Drawing.Size(189, 40);
			this.txtREMapLot.TabIndex = 12;
			this.txtREMapLot.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtREAcct
			// 
			this.txtREAcct.BackColor = System.Drawing.SystemColors.Window;
			this.txtREAcct.Location = new System.Drawing.Point(142, 13);
			this.txtREAcct.MaxLength = 6;
			this.txtREAcct.Name = "txtREAcct";
			this.txtREAcct.Size = new System.Drawing.Size(189, 40);
			this.txtREAcct.TabIndex = 11;
			this.txtREAcct.Validating += new System.ComponentModel.CancelEventHandler(this.txtREAcct_Validating);
			// 
			// chkUseREMortgage
			// 
			this.chkUseREMortgage.Enabled = false;
			this.chkUseREMortgage.Location = new System.Drawing.Point(151, 130);
			this.chkUseREMortgage.Name = "chkUseREMortgage";
			this.chkUseREMortgage.Size = new System.Drawing.Size(190, 22);
			this.chkUseREMortgage.TabIndex = 16;
			this.chkUseREMortgage.Text = "Use RE Mortgage Holders?";
			this.chkUseREMortgage.CheckedChanged += new System.EventHandler(this.chkUseREMortgage_CheckedChanged);
			// 
			// lblREPage
			// 
			this.lblREPage.AutoSize = true;
			this.lblREPage.Location = new System.Drawing.Point(361, 87);
			this.lblREPage.Name = "lblREPage";
			this.lblREPage.Size = new System.Drawing.Size(40, 15);
			this.lblREPage.TabIndex = 81;
			this.lblREPage.Text = "PAGE";
			this.lblREPage.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblREBook
			// 
			this.lblREBook.AutoSize = true;
			this.lblREBook.Location = new System.Drawing.Point(361, 27);
			this.lblREBook.Name = "lblREBook";
			this.lblREBook.Size = new System.Drawing.Size(41, 15);
			this.lblREBook.TabIndex = 80;
			this.lblREBook.Text = "BOOK";
			this.lblREBook.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblREMapLot
			// 
			this.lblREMapLot.AutoSize = true;
			this.lblREMapLot.Location = new System.Drawing.Point(0, 87);
			this.lblREMapLot.Name = "lblREMapLot";
			this.lblREMapLot.Size = new System.Drawing.Size(61, 15);
			this.lblREMapLot.TabIndex = 79;
			this.lblREMapLot.Text = "MAP LOT";
			this.lblREMapLot.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblREAcct
			// 
			this.lblREAcct.AutoSize = true;
			this.lblREAcct.Location = new System.Drawing.Point(0, 27);
			this.lblREAcct.Name = "lblREAcct";
			this.lblREAcct.Size = new System.Drawing.Size(89, 15);
			this.lblREAcct.TabIndex = 78;
			this.lblREAcct.Text = "RE ACCOUNT";
			this.lblREAcct.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraMasterTB
			// 
			this.fraMasterTB.AppearanceKey = "groupBoxLeftBorder";
			this.fraMasterTB.Controls.Add(this.lblACHInfo);
			this.fraMasterTB.Controls.Add(this.txtImpervSurf);
			this.fraMasterTB.Controls.Add(this.chkBillSameAsOwner);
			this.fraMasterTB.Controls.Add(this.chkNoBill);
			this.fraMasterTB.Controls.Add(this.chkFinal);
			this.fraMasterTB.Controls.Add(this.chkEmailBills);
			this.fraMasterTB.Controls.Add(this.txtComment);
			this.fraMasterTB.Controls.Add(this.txtPhone);
			this.fraMasterTB.Controls.Add(this.txtEmail);
			this.fraMasterTB.Controls.Add(this.txtBillMessage);
			this.fraMasterTB.Controls.Add(this.txtDEMessage);
			this.fraMasterTB.Controls.Add(this.txtDirections);
			this.fraMasterTB.Controls.Add(this.txtLocationNumber);
			this.fraMasterTB.Controls.Add(this.txtLocationStreet);
			this.fraMasterTB.Controls.Add(this.Frame1);
			this.fraMasterTB.Controls.Add(this.fraOwnerDisp);
			this.fraMasterTB.Controls.Add(this.lblImpervSurf);
			this.fraMasterTB.Controls.Add(this.lblTaxAcquired);
			this.fraMasterTB.Controls.Add(this.lblBankruptcy);
			this.fraMasterTB.Controls.Add(this.lblMasterBook);
			this.fraMasterTB.Controls.Add(this.lblAcctComment);
			this.fraMasterTB.Controls.Add(this.lblComment);
			this.fraMasterTB.Controls.Add(this.lblEmail);
			this.fraMasterTB.Controls.Add(this.lblTelephone);
			this.fraMasterTB.Controls.Add(this.lblBillMessage);
			this.fraMasterTB.Controls.Add(this.lblDEMessage);
			this.fraMasterTB.Controls.Add(this.lblDirections);
			this.fraMasterTB.Controls.Add(this.lblLocation);
			this.fraMasterTB.Controls.Add(this.lblAcctNumberText);
			this.fraMasterTB.Controls.Add(this.lblAcctNumber);
			this.fraMasterTB.Location = new System.Drawing.Point(30, 30);
			this.fraMasterTB.Name = "fraMasterTB";
			this.fraMasterTB.Size = new System.Drawing.Size(972, 853);
			this.fraMasterTB.TabIndex = 67;
			this.fraMasterTB.Text = "Account Information";
			this.fraMasterTB.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblACHInfo
			// 
			this.lblACHInfo.ForeColor = System.Drawing.Color.Crimson;
			this.lblACHInfo.Location = new System.Drawing.Point(229, 45);
			this.lblACHInfo.Name = "lblACHInfo";
			this.lblACHInfo.Size = new System.Drawing.Size(9, 15);
			this.lblACHInfo.TabIndex = 141;
			this.lblACHInfo.Text = "A";
			this.lblACHInfo.Visible = false;
			// 
			// txtImpervSurf
			// 
			this.txtImpervSurf.BackColor = System.Drawing.SystemColors.Window;
			this.txtImpervSurf.Enabled = false;
			this.txtImpervSurf.Location = new System.Drawing.Point(447, 90);
			this.txtImpervSurf.Name = "txtImpervSurf";
			this.txtImpervSurf.Size = new System.Drawing.Size(357, 40);
			this.txtImpervSurf.TabIndex = 2;
			this.txtImpervSurf.Visible = false;
			this.txtImpervSurf.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// chkBillSameAsOwner
			// 
			this.chkBillSameAsOwner.Location = new System.Drawing.Point(497, 150);
			this.chkBillSameAsOwner.Name = "chkBillSameAsOwner";
			this.chkBillSameAsOwner.Size = new System.Drawing.Size(232, 22);
			this.chkBillSameAsOwner.TabIndex = 21;
			this.chkBillSameAsOwner.Text = "Tenant information same as owner";
			this.chkBillSameAsOwner.CheckedChanged += new System.EventHandler(this.chkBillSameAsOwner_CheckedChanged);
			// 
			// chkNoBill
			// 
			this.chkNoBill.Location = new System.Drawing.Point(361, 150);
			this.chkNoBill.Name = "chkNoBill";
			this.chkNoBill.Size = new System.Drawing.Size(94, 22);
			this.chkNoBill.TabIndex = 20;
			this.chkNoBill.Text = "No Charge";
			this.chkNoBill.CheckedChanged += new System.EventHandler(this.chkNoBill_CheckedChanged);
			// 
			// chkFinal
			// 
			this.chkFinal.Location = new System.Drawing.Point(20, 150);
			this.chkFinal.Name = "chkFinal";
			this.chkFinal.Size = new System.Drawing.Size(95, 22);
			this.chkFinal.TabIndex = 18;
			this.chkFinal.Text = "Final Billed";
			this.chkFinal.CheckedChanged += new System.EventHandler(this.chkFinal_CheckedChanged);
			// 
			// chkEmailBills
			// 
			this.chkEmailBills.Location = new System.Drawing.Point(158, 150);
			this.chkEmailBills.Name = "chkEmailBills";
			this.chkEmailBills.Size = new System.Drawing.Size(147, 22);
			this.chkEmailBills.TabIndex = 19;
			this.chkEmailBills.Text = "Send Bills by E-Mail";
			this.chkEmailBills.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtComment
			// 
			this.txtComment.BackColor = System.Drawing.SystemColors.Window;
			this.txtComment.Location = new System.Drawing.Point(169, 797);
			this.txtComment.MaxLength = 255;
			this.txtComment.Name = "txtComment";
			this.txtComment.Size = new System.Drawing.Size(519, 40);
			this.txtComment.TabIndex = 8;
			this.txtComment.TextChanged += new System.EventHandler(this.txtComment_TextChanged);
			// 
			// txtPhone
			// 
			this.txtPhone.Location = new System.Drawing.Point(169, 750);
			this.txtPhone.MaxLength = 13;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Size = new System.Drawing.Size(143, 22);
			this.txtPhone.TabIndex = 6;
			this.txtPhone.Click += new System.EventHandler(this.txtPhone_ClickEvent);
			this.txtPhone.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtEmail
			// 
			this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmail.Location = new System.Drawing.Point(402, 750);
			this.txtEmail.MaxLength = 255;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Size = new System.Drawing.Size(286, 40);
			this.txtEmail.TabIndex = 7;
			this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
			// 
			// txtBillMessage
			// 
			this.txtBillMessage.BackColor = System.Drawing.SystemColors.Window;
			this.txtBillMessage.Location = new System.Drawing.Point(169, 690);
			this.txtBillMessage.MaxLength = 255;
			this.txtBillMessage.Name = "txtBillMessage";
			this.txtBillMessage.Size = new System.Drawing.Size(519, 40);
			this.txtBillMessage.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtBillMessage, "Message to appear on the bill.");
			this.txtBillMessage.TextChanged += new System.EventHandler(this.txtBillMessage_TextChanged);
			// 
			// txtDEMessage
			// 
			this.txtDEMessage.BackColor = System.Drawing.SystemColors.Window;
			this.txtDEMessage.Location = new System.Drawing.Point(169, 630);
			this.txtDEMessage.MaxLength = 255;
			this.txtDEMessage.Name = "txtDEMessage";
			this.txtDEMessage.Size = new System.Drawing.Size(519, 40);
			this.txtDEMessage.TabIndex = 4;
			this.txtDEMessage.TextChanged += new System.EventHandler(this.txtDEMessage_TextChanged);
			// 
			// txtDirections
			// 
			this.txtDirections.BackColor = System.Drawing.SystemColors.Window;
			this.txtDirections.Location = new System.Drawing.Point(169, 570);
			this.txtDirections.MaxLength = 255;
			this.txtDirections.Name = "txtDirections";
			this.txtDirections.Size = new System.Drawing.Size(519, 40);
			this.txtDirections.TabIndex = 3;
			this.txtDirections.TextChanged += new System.EventHandler(this.txtDirections_TextChanged);
			// 
			// txtLocationNumber
			// 
			this.txtLocationNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtLocationNumber.Location = new System.Drawing.Point(447, 30);
			this.txtLocationNumber.MaxLength = 5;
			this.txtLocationNumber.Name = "txtLocationNumber";
			this.txtLocationNumber.Size = new System.Drawing.Size(63, 40);
			this.txtLocationNumber.TabIndex = 142;
			this.txtLocationNumber.TextChanged += new System.EventHandler(this.txtLocationNumber_TextChanged);
			// 
			// txtLocationStreet
			// 
			this.txtLocationStreet.BackColor = System.Drawing.SystemColors.Window;
			this.txtLocationStreet.Location = new System.Drawing.Point(520, 30);
			this.txtLocationStreet.MaxLength = 50;
			this.txtLocationStreet.Name = "txtLocationStreet";
			this.txtLocationStreet.Size = new System.Drawing.Size(284, 40);
			this.txtLocationStreet.TabIndex = 1;
			this.txtLocationStreet.TextChanged += new System.EventHandler(this.txtLocationStreet_TextChanged);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtTenant2CustNum);
			this.Frame1.Controls.Add(this.cmdTenant2Search);
			this.Frame1.Controls.Add(this.cmdTenant2Edit);
			this.Frame1.Controls.Add(this.txtTenant1CustNum);
			this.Frame1.Controls.Add(this.cmdTenant1Search);
			this.Frame1.Controls.Add(this.cmdTenant1Edit);
			this.Frame1.Controls.Add(this.lblTenantCustInfo);
			this.Frame1.Controls.Add(this.imgTenant2Memo);
			this.Frame1.Controls.Add(this.imgTenant1Memo);
			this.Frame1.Location = new System.Drawing.Point(460, 197);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(420, 240);
			this.Frame1.TabIndex = 71;
			this.Frame1.Text = "Tenant Information";
			this.Frame1.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdTenant2Search
			// 
			this.cmdTenant2Search.AppearanceKey = "actionButton";
			this.cmdTenant2Search.ImageSource = "icon - search";
			this.cmdTenant2Search.Location = new System.Drawing.Point(162, 90);
			this.cmdTenant2Search.Name = "cmdTenant2Search";
			this.cmdTenant2Search.Size = new System.Drawing.Size(40, 40);
			this.cmdTenant2Search.TabIndex = 135;
			this.cmdTenant2Search.Click += new System.EventHandler(this.cmdTenant2Search_Click);
			this.cmdTenant2Search.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdTenant2Edit
			// 
			this.cmdTenant2Edit.AppearanceKey = "actionButton";
			this.cmdTenant2Edit.ImageSource = "icon - edit";
			this.cmdTenant2Edit.Location = new System.Drawing.Point(207, 90);
			this.cmdTenant2Edit.Name = "cmdTenant2Edit";
			this.cmdTenant2Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdTenant2Edit.TabIndex = 134;
			this.cmdTenant2Edit.Click += new System.EventHandler(this.cmdTenant2Edit_Click);
			this.cmdTenant2Edit.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdTenant1Search
			// 
			this.cmdTenant1Search.AppearanceKey = "actionButton";
			this.cmdTenant1Search.ImageSource = "icon - search";
			this.cmdTenant1Search.Location = new System.Drawing.Point(162, 30);
			this.cmdTenant1Search.Name = "cmdTenant1Search";
			this.cmdTenant1Search.Size = new System.Drawing.Size(40, 40);
			this.cmdTenant1Search.TabIndex = 132;
			this.cmdTenant1Search.Click += new System.EventHandler(this.cmdTenant1Search_Click);
			this.cmdTenant1Search.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdTenant1Edit
			// 
			this.cmdTenant1Edit.AppearanceKey = "actionButton";
			this.cmdTenant1Edit.ImageSource = "icon - edit";
			this.cmdTenant1Edit.Location = new System.Drawing.Point(207, 30);
			this.cmdTenant1Edit.Name = "cmdTenant1Edit";
			this.cmdTenant1Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdTenant1Edit.TabIndex = 131;
			this.cmdTenant1Edit.Click += new System.EventHandler(this.cmdTenant1Edit_Click);
			this.cmdTenant1Edit.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblTenantCustInfo
			// 
			this.lblTenantCustInfo.Location = new System.Drawing.Point(17, 150);
			this.lblTenantCustInfo.Name = "lblTenantCustInfo";
			this.lblTenantCustInfo.Size = new System.Drawing.Size(230, 85);
			this.lblTenantCustInfo.TabIndex = 137;
			this.lblTenantCustInfo.Text = "OWNER MAILING ADDRESS";
			this.lblTenantCustInfo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// imgTenant2Memo
			// 
			this.imgTenant2Memo.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgTenant2Memo.FillColor = -2147483644;
			this.imgTenant2Memo.Image = ((System.Drawing.Image)(resources.GetObject("imgTenant2Memo.Image")));
			this.imgTenant2Memo.Location = new System.Drawing.Point(217, 90);
			this.imgTenant2Memo.Name = "imgTenant2Memo";
			this.imgTenant2Memo.Size = new System.Drawing.Size(40, 40);
			this.imgTenant2Memo.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgTenant2Memo.Visible = false;
			this.imgTenant2Memo.Click += new System.EventHandler(this.imgTenant2Memo_Click);
			// 
			// imgTenant1Memo
			// 
			this.imgTenant1Memo.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgTenant1Memo.FillColor = -2147483644;
			this.imgTenant1Memo.Image = ((System.Drawing.Image)(resources.GetObject("imgTenant1Memo.Image")));
			this.imgTenant1Memo.Location = new System.Drawing.Point(217, 30);
			this.imgTenant1Memo.Name = "imgTenant1Memo";
			this.imgTenant1Memo.Size = new System.Drawing.Size(40, 40);
			this.imgTenant1Memo.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgTenant1Memo.Visible = false;
			this.imgTenant1Memo.Click += new System.EventHandler(this.imgTenant1Memo_Click);
			// 
			// fraOwnerDisp
			// 
			this.fraOwnerDisp.Controls.Add(this.txtDeedName2);
			this.fraOwnerDisp.Controls.Add(this.txtDeedName1);
			this.fraOwnerDisp.Controls.Add(this.fcLabel1);
			this.fraOwnerDisp.Controls.Add(this.fcLabel2);
			this.fraOwnerDisp.Controls.Add(this.txtOwner2CustNum);
			this.fraOwnerDisp.Controls.Add(this.cmdOwner2Search);
			this.fraOwnerDisp.Controls.Add(this.cmdOwner2Edit);
			this.fraOwnerDisp.Controls.Add(this.txtOwner1CustNum);
			this.fraOwnerDisp.Controls.Add(this.cmdOwner1Search);
			this.fraOwnerDisp.Controls.Add(this.cmdOwner1Edit);
			this.fraOwnerDisp.Controls.Add(this.lblOwnerCustInfo);
			this.fraOwnerDisp.Controls.Add(this.imgOwner2Memo);
			this.fraOwnerDisp.Controls.Add(this.imgOwner1Memo);
			this.fraOwnerDisp.Controls.Add(this.lblCustomerNumber);
			this.fraOwnerDisp.Location = new System.Drawing.Point(20, 197);
			this.fraOwnerDisp.Name = "fraOwnerDisp";
			this.fraOwnerDisp.Size = new System.Drawing.Size(420, 349);
			this.fraOwnerDisp.TabIndex = 70;
			this.fraOwnerDisp.Text = "Owner Information";
			this.fraOwnerDisp.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtDeedName2
			// 
			this.txtDeedName2.BackColor = System.Drawing.SystemColors.Window;
			this.txtDeedName2.Location = new System.Drawing.Point(163, 301);
			this.txtDeedName2.MaxLength = 255;
			this.txtDeedName2.Name = "txtDeedName2";
			this.txtDeedName2.Size = new System.Drawing.Size(242, 40);
			this.txtDeedName2.TabIndex = 134;
			// 
			// txtDeedName1
			// 
			this.txtDeedName1.BackColor = System.Drawing.SystemColors.Window;
			this.txtDeedName1.Location = new System.Drawing.Point(163, 241);
			this.txtDeedName1.MaxLength = 255;
			this.txtDeedName1.Name = "txtDeedName1";
			this.txtDeedName1.Size = new System.Drawing.Size(242, 40);
			this.txtDeedName1.TabIndex = 133;
			// 
			// fcLabel1
			// 
			this.fcLabel1.Location = new System.Drawing.Point(24, 315);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(142, 15);
			this.fcLabel1.TabIndex = 136;
			this.fcLabel1.Text = "DEED NAME 2";
			this.ToolTip1.SetToolTip(this.fcLabel1, "Data Entry Message");
			// 
			// fcLabel2
			// 
			this.fcLabel2.AutoSize = true;
			this.fcLabel2.Location = new System.Drawing.Point(24, 255);
			this.fcLabel2.Name = "fcLabel2";
			this.fcLabel2.Size = new System.Drawing.Size(92, 15);
			this.fcLabel2.TabIndex = 135;
			this.fcLabel2.Text = "DEED NAME 1";
			// 
			// cmdOwner2Search
			// 
			this.cmdOwner2Search.AppearanceKey = "actionButton";
			this.cmdOwner2Search.ImageSource = "icon - search";
			this.cmdOwner2Search.Location = new System.Drawing.Point(305, 90);
			this.cmdOwner2Search.Name = "cmdOwner2Search";
			this.cmdOwner2Search.Size = new System.Drawing.Size(40, 40);
			this.cmdOwner2Search.TabIndex = 128;
			this.cmdOwner2Search.Click += new System.EventHandler(this.cmdOwner2Search_Click);
			this.cmdOwner2Search.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdOwner2Edit
			// 
			this.cmdOwner2Edit.AppearanceKey = "actionButton";
			this.cmdOwner2Edit.ImageSource = "icon - edit";
			this.cmdOwner2Edit.Location = new System.Drawing.Point(350, 90);
			this.cmdOwner2Edit.Name = "cmdOwner2Edit";
			this.cmdOwner2Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdOwner2Edit.TabIndex = 127;
			this.cmdOwner2Edit.Click += new System.EventHandler(this.cmdOwner2Edit_Click);
			this.cmdOwner2Edit.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdOwner1Search
			// 
			this.cmdOwner1Search.AppearanceKey = "actionButton";
			this.cmdOwner1Search.ImageSource = "icon - search";
			this.cmdOwner1Search.Location = new System.Drawing.Point(305, 30);
			this.cmdOwner1Search.Name = "cmdOwner1Search";
			this.cmdOwner1Search.Size = new System.Drawing.Size(40, 40);
			this.cmdOwner1Search.TabIndex = 124;
			this.cmdOwner1Search.Click += new System.EventHandler(this.cmdOwner1Search_Click);
			this.cmdOwner1Search.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdOwner1Edit
			// 
			this.cmdOwner1Edit.AppearanceKey = "actionButton";
			this.cmdOwner1Edit.ImageSource = "icon - edit";
			this.cmdOwner1Edit.Location = new System.Drawing.Point(350, 30);
			this.cmdOwner1Edit.Name = "cmdOwner1Edit";
			this.cmdOwner1Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdOwner1Edit.TabIndex = 123;
			this.cmdOwner1Edit.Click += new System.EventHandler(this.cmdOwner1Edit_Click);
			this.cmdOwner1Edit.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblOwnerCustInfo
			// 
			this.lblOwnerCustInfo.Location = new System.Drawing.Point(20, 150);
			this.lblOwnerCustInfo.Name = "lblOwnerCustInfo";
			this.lblOwnerCustInfo.Size = new System.Drawing.Size(370, 85);
			this.lblOwnerCustInfo.TabIndex = 130;
			this.lblOwnerCustInfo.Text = "OWNER MAILING ADDRESS";
			this.lblOwnerCustInfo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// imgOwner2Memo
			// 
			this.imgOwner2Memo.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgOwner2Memo.FillColor = -2147483644;
			this.imgOwner2Memo.Image = ((System.Drawing.Image)(resources.GetObject("imgOwner2Memo.Image")));
			this.imgOwner2Memo.Location = new System.Drawing.Point(360, 90);
			this.imgOwner2Memo.Name = "imgOwner2Memo";
			this.imgOwner2Memo.Size = new System.Drawing.Size(40, 40);
			this.imgOwner2Memo.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgOwner2Memo.Visible = false;
			this.imgOwner2Memo.Click += new System.EventHandler(this.imgOwner2Memo_Click);
			// 
			// imgOwner1Memo
			// 
			this.imgOwner1Memo.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgOwner1Memo.FillColor = -2147483644;
			this.imgOwner1Memo.Image = ((System.Drawing.Image)(resources.GetObject("imgOwner1Memo.Image")));
			this.imgOwner1Memo.Location = new System.Drawing.Point(360, 30);
			this.imgOwner1Memo.Name = "imgOwner1Memo";
			this.imgOwner1Memo.Size = new System.Drawing.Size(40, 40);
			this.imgOwner1Memo.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgOwner1Memo.Visible = false;
			this.imgOwner1Memo.Click += new System.EventHandler(this.imgOwner1Memo_Click);
			// 
			// lblCustomerNumber
			// 
			this.lblCustomerNumber.AutoSize = true;
			this.lblCustomerNumber.Location = new System.Drawing.Point(20, 44);
			this.lblCustomerNumber.Name = "lblCustomerNumber";
			this.lblCustomerNumber.Size = new System.Drawing.Size(89, 15);
			this.lblCustomerNumber.TabIndex = 126;
			this.lblCustomerNumber.Text = "CUSTOMER #";
			this.lblCustomerNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblImpervSurf
			// 
			this.lblImpervSurf.AutoSize = true;
			this.lblImpervSurf.Location = new System.Drawing.Point(287, 104);
			this.lblImpervSurf.Name = "lblImpervSurf";
			this.lblImpervSurf.Size = new System.Drawing.Size(151, 15);
			this.lblImpervSurf.TabIndex = 140;
			this.lblImpervSurf.Text = "IMPERVIOUS SURFACE";
			this.lblImpervSurf.Visible = false;
			this.lblImpervSurf.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblTaxAcquired
			// 
			this.lblTaxAcquired.Location = new System.Drawing.Point(200, 30);
			this.lblTaxAcquired.Name = "lblTaxAcquired";
			this.lblTaxAcquired.Size = new System.Drawing.Size(19, 15);
			this.lblTaxAcquired.TabIndex = 105;
			this.lblTaxAcquired.Text = "TA";
			this.lblTaxAcquired.Visible = false;
			this.lblTaxAcquired.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblBankruptcy
			// 
			this.lblBankruptcy.Location = new System.Drawing.Point(248, 30);
			this.lblBankruptcy.Name = "lblBankruptcy";
			this.lblBankruptcy.Size = new System.Drawing.Size(9, 15);
			this.lblBankruptcy.TabIndex = 99;
			this.lblBankruptcy.Text = "B";
			this.lblBankruptcy.Visible = false;
			this.lblBankruptcy.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblMasterBook
			// 
			this.lblMasterBook.Location = new System.Drawing.Point(20, 65);
			this.lblMasterBook.Name = "lblMasterBook";
			this.lblMasterBook.Size = new System.Drawing.Size(112, 15);
			this.lblMasterBook.TabIndex = 90;
			this.lblMasterBook.Text = "BOOK";
			this.lblMasterBook.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblAcctComment
			// 
			this.lblAcctComment.Location = new System.Drawing.Point(229, 30);
			this.lblAcctComment.Name = "lblAcctComment";
			this.lblAcctComment.Size = new System.Drawing.Size(9, 15);
			this.lblAcctComment.TabIndex = 86;
			this.lblAcctComment.Text = "C";
			this.lblAcctComment.Visible = false;
			this.lblAcctComment.DoubleClick += new System.EventHandler(this.lblAcctComment_DoubleClick);
			this.lblAcctComment.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblComment
			// 
			this.lblComment.AutoSize = true;
			this.lblComment.Location = new System.Drawing.Point(21, 811);
			this.lblComment.Name = "lblComment";
			this.lblComment.Size = new System.Drawing.Size(40, 15);
			this.lblComment.TabIndex = 85;
			this.lblComment.Text = "NOTE";
			this.ToolTip1.SetToolTip(this.lblComment, "Message to appear on the bill.");
			this.lblComment.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblEmail
			// 
			this.lblEmail.AutoSize = true;
			this.lblEmail.Location = new System.Drawing.Point(342, 764);
			this.lblEmail.Name = "lblEmail";
			this.lblEmail.Size = new System.Drawing.Size(43, 15);
			this.lblEmail.TabIndex = 77;
			this.lblEmail.Text = "EMAIL";
			this.lblEmail.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblTelephone
			// 
			this.lblTelephone.AutoSize = true;
			this.lblTelephone.Location = new System.Drawing.Point(21, 764);
			this.lblTelephone.Name = "lblTelephone";
			this.lblTelephone.Size = new System.Drawing.Size(50, 15);
			this.lblTelephone.TabIndex = 76;
			this.lblTelephone.Text = "PHONE";
			this.lblTelephone.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblBillMessage
			// 
			this.lblBillMessage.AutoSize = true;
			this.lblBillMessage.Location = new System.Drawing.Point(21, 704);
			this.lblBillMessage.Name = "lblBillMessage";
			this.lblBillMessage.Size = new System.Drawing.Size(98, 15);
			this.lblBillMessage.TabIndex = 75;
			this.lblBillMessage.Text = "BILL MESSAGE";
			this.ToolTip1.SetToolTip(this.lblBillMessage, "Message to appear on the bill.");
			this.lblBillMessage.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblDEMessage
			// 
			this.lblDEMessage.Location = new System.Drawing.Point(21, 644);
			this.lblDEMessage.Name = "lblDEMessage";
			this.lblDEMessage.Size = new System.Drawing.Size(142, 15);
			this.lblDEMessage.TabIndex = 74;
			this.lblDEMessage.Text = "DE MESSAGE";
			this.ToolTip1.SetToolTip(this.lblDEMessage, "Data Entry Message");
			this.lblDEMessage.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblDirections
			// 
			this.lblDirections.AutoSize = true;
			this.lblDirections.Location = new System.Drawing.Point(21, 584);
			this.lblDirections.Name = "lblDirections";
			this.lblDirections.Size = new System.Drawing.Size(84, 15);
			this.lblDirections.TabIndex = 73;
			this.lblDirections.Text = "DIRECTIONS";
			this.lblDirections.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblLocation
			// 
			this.lblLocation.AutoSize = true;
			this.lblLocation.Location = new System.Drawing.Point(287, 44);
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Size = new System.Drawing.Size(70, 15);
			this.lblLocation.TabIndex = 72;
			this.lblLocation.Text = "LOCATION";
			this.lblLocation.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblAcctNumberText
			// 
			this.lblAcctNumberText.Location = new System.Drawing.Point(110, 30);
			this.lblAcctNumberText.Name = "lblAcctNumberText";
			this.lblAcctNumberText.Size = new System.Drawing.Size(70, 15);
			this.lblAcctNumberText.TabIndex = 69;
			this.lblAcctNumberText.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblAcctNumber
			// 
			this.lblAcctNumber.Location = new System.Drawing.Point(20, 30);
			this.lblAcctNumber.Name = "lblAcctNumber";
			this.lblAcctNumber.Size = new System.Drawing.Size(70, 15);
			this.lblAcctNumber.TabIndex = 68;
			this.lblAcctNumber.Text = "ACCOUNT";
			this.lblAcctNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraWater
			// 
			this.fraWater.Controls.Add(this.cmbWCat);
			this.fraWater.Controls.Add(this.cmbWBillTo);
			this.fraWater.Controls.Add(this.txtWaterAcct);
			this.fraWater.Controls.Add(this.lblWBillTo);
			this.fraWater.Controls.Add(this.lblWCat);
			this.fraWater.Controls.Add(this.lblWaterAccountNumber);
			this.fraWater.Location = new System.Drawing.Point(30, 895);
			this.fraWater.Name = "fraWater";
			this.fraWater.Size = new System.Drawing.Size(351, 210);
			this.fraWater.TabIndex = 9;
			this.fraWater.Text = "Water Account";
			this.fraWater.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmbWCat
			// 
			this.cmbWCat.BackColor = System.Drawing.SystemColors.Window;
			this.cmbWCat.Location = new System.Drawing.Point(151, 30);
			this.cmbWCat.Name = "cmbWCat";
			this.cmbWCat.Size = new System.Drawing.Size(180, 40);
			this.cmbWCat.Sorted = true;
			this.cmbWCat.TabIndex = 9;
			this.cmbWCat.DropDown += new System.EventHandler(this.cmbWCat_DropDown);
			this.cmbWCat.TextChanged += new System.EventHandler(this.cmbWCat_TextChanged);
			this.cmbWCat.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbWCat_KeyDown);
			// 
			// txtWaterAcct
			// 
			this.txtWaterAcct.Cols = 1;
			this.txtWaterAcct.ColumnHeadersVisible = false;
			this.txtWaterAcct.ExtendLastCol = true;
			this.txtWaterAcct.FixedCols = 0;
			this.txtWaterAcct.FixedRows = 0;
			this.txtWaterAcct.Location = new System.Drawing.Point(151, 150);
			this.txtWaterAcct.Name = "txtWaterAcct";
			this.txtWaterAcct.RowHeadersVisible = false;
			this.txtWaterAcct.Rows = 1;
			this.txtWaterAcct.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
			this.txtWaterAcct.ShowFocusCell = false;
			this.txtWaterAcct.Size = new System.Drawing.Size(180, 42);
			this.txtWaterAcct.TabIndex = 88;
			this.txtWaterAcct.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.txtWaterAcct_ChangeEdit);
			this.txtWaterAcct.Validating += new System.ComponentModel.CancelEventHandler(this.txtWaterAcct_Validating);
			this.txtWaterAcct.KeyDown += new Wisej.Web.KeyEventHandler(this.txtWaterAcct_KeyDownEvent);
			// 
			// lblWBillTo
			// 
			this.lblWBillTo.AutoSize = true;
			this.lblWBillTo.Location = new System.Drawing.Point(20, 104);
			this.lblWBillTo.Name = "lblWBillTo";
			this.lblWBillTo.Size = new System.Drawing.Size(52, 15);
			this.lblWBillTo.TabIndex = 117;
			this.lblWBillTo.Text = "BILL TO";
			this.lblWBillTo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblWCat
			// 
			this.lblWCat.AutoSize = true;
			this.lblWCat.Location = new System.Drawing.Point(20, 44);
			this.lblWCat.Name = "lblWCat";
			this.lblWCat.Size = new System.Drawing.Size(76, 15);
			this.lblWCat.TabIndex = 45;
			this.lblWCat.Text = "CATEGORY";
			this.lblWCat.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblWaterAccountNumber
			// 
			this.lblWaterAccountNumber.AutoSize = true;
			this.lblWaterAccountNumber.Location = new System.Drawing.Point(20, 164);
			this.lblWaterAccountNumber.Name = "lblWaterAccountNumber";
			this.lblWaterAccountNumber.Size = new System.Drawing.Size(68, 15);
			this.lblWaterAccountNumber.TabIndex = 44;
			this.lblWaterAccountNumber.Text = "ACCOUNT";
			this.lblWaterAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraSewer
			// 
			this.fraSewer.Controls.Add(this.cmbSCat);
			this.fraSewer.Controls.Add(this.cmbSBillTo);
			this.fraSewer.Controls.Add(this.txtSewerAcct);
			this.fraSewer.Controls.Add(this.lblSBillTo);
			this.fraSewer.Controls.Add(this.Label2);
			this.fraSewer.Controls.Add(this.Label3);
			this.fraSewer.Location = new System.Drawing.Point(401, 895);
			this.fraSewer.Name = "fraSewer";
			this.fraSewer.Size = new System.Drawing.Size(351, 210);
			this.fraSewer.TabIndex = 10;
			this.fraSewer.Text = "Sewer Account";
			this.fraSewer.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmbSCat
			// 
			this.cmbSCat.BackColor = System.Drawing.SystemColors.Window;
			this.cmbSCat.Location = new System.Drawing.Point(150, 30);
			this.cmbSCat.Name = "cmbSCat";
			this.cmbSCat.Size = new System.Drawing.Size(180, 40);
			this.cmbSCat.Sorted = true;
			this.cmbSCat.TabIndex = 10;
			this.cmbSCat.DropDown += new System.EventHandler(this.cmbSCat_DropDown);
			this.cmbSCat.TextChanged += new System.EventHandler(this.cmbSCat_TextChanged);
			this.cmbSCat.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbSCat_KeyDown);
			// 
			// txtSewerAcct
			// 
			this.txtSewerAcct.Cols = 1;
			this.txtSewerAcct.ColumnHeadersVisible = false;
			this.txtSewerAcct.ExtendLastCol = true;
			this.txtSewerAcct.FixedCols = 0;
			this.txtSewerAcct.FixedRows = 0;
			this.txtSewerAcct.Location = new System.Drawing.Point(150, 150);
			this.txtSewerAcct.Name = "txtSewerAcct";
			this.txtSewerAcct.RowHeadersVisible = false;
			this.txtSewerAcct.Rows = 1;
			this.txtSewerAcct.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
			this.txtSewerAcct.ShowFocusCell = false;
			this.txtSewerAcct.Size = new System.Drawing.Size(180, 42);
			this.txtSewerAcct.TabIndex = 89;
			this.txtSewerAcct.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.txtSewerAcct_ChangeEdit);
			this.txtSewerAcct.Validating += new System.ComponentModel.CancelEventHandler(this.txtSewerAcct_Validating);
			this.txtSewerAcct.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSewerAcct_KeyDownEvent);
			// 
			// lblSBillTo
			// 
			this.lblSBillTo.AutoSize = true;
			this.lblSBillTo.Location = new System.Drawing.Point(20, 104);
			this.lblSBillTo.Name = "lblSBillTo";
			this.lblSBillTo.Size = new System.Drawing.Size(52, 15);
			this.lblSBillTo.TabIndex = 120;
			this.lblSBillTo.Text = "BILL TO";
			this.lblSBillTo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(20, 164);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(68, 15);
			this.Label2.TabIndex = 42;
			this.Label2.Text = "ACCOUNT";
			this.Label2.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.Location = new System.Drawing.Point(20, 44);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(76, 15);
			this.Label3.TabIndex = 41;
			this.Label3.Text = "CATEGORY";
			this.Label3.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraMasterGrid
			// 
			this.fraMasterGrid.AppearanceKey = "groupBoxNoBorders";
			this.fraMasterGrid.Controls.Add(this.vsMaster);
			this.fraMasterGrid.Controls.Add(this.lblAccountNumber);
			this.fraMasterGrid.Controls.Add(this.txtAccountNumber);
			this.fraMasterGrid.Location = new System.Drawing.Point(30, 30);
			this.fraMasterGrid.Name = "fraMasterGrid";
			this.fraMasterGrid.Size = new System.Drawing.Size(565, 364);
			this.fraMasterGrid.TabIndex = 63;
			this.fraMasterGrid.Text = "Account Information";
			this.fraMasterGrid.Visible = false;
			this.fraMasterGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// vsMaster
			// 
			this.vsMaster.Cols = 4;
			this.vsMaster.ColumnHeadersVisible = false;
			this.vsMaster.FixedRows = 0;
			this.vsMaster.Location = new System.Drawing.Point(0, 90);
			this.vsMaster.Name = "vsMaster";
			this.vsMaster.Rows = 1;
			this.vsMaster.ShowFocusCell = false;
			this.vsMaster.Size = new System.Drawing.Size(565, 274);
			this.vsMaster.TabIndex = 64;
			this.vsMaster.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.AutoSize = true;
			this.lblAccountNumber.Location = new System.Drawing.Point(0, 44);
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Size = new System.Drawing.Size(128, 15);
			this.lblAccountNumber.TabIndex = 66;
			this.lblAccountNumber.Text = "ACCOUNT NUMBER";
			this.lblAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtAccountNumber
			// 
			this.txtAccountNumber.Location = new System.Drawing.Point(181, 30);
			this.txtAccountNumber.Name = "txtAccountNumber";
			this.txtAccountNumber.Size = new System.Drawing.Size(90, 40);
			this.txtAccountNumber.TabIndex = 65;
			this.txtAccountNumber.TextChanged += new System.EventHandler(this.txtAccountNumber_TextChanged);
			// 
			// fraChangeOutHistory
			// 
			this.fraChangeOutHistory.AppearanceKey = "groupBoxNoBorders";
			this.fraChangeOutHistory.Controls.Add(this.cmdChangeOutClose);
			this.fraChangeOutHistory.Controls.Add(this.vsChangeOut);
			this.fraChangeOutHistory.Location = new System.Drawing.Point(30, 30);
			this.fraChangeOutHistory.Name = "fraChangeOutHistory";
			this.fraChangeOutHistory.Size = new System.Drawing.Size(842, 251);
			this.fraChangeOutHistory.TabIndex = 106;
			this.fraChangeOutHistory.Visible = false;
			this.fraChangeOutHistory.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdChangeOutClose
			// 
			this.cmdChangeOutClose.AppearanceKey = "actionButton";
			this.cmdChangeOutClose.ForeColor = System.Drawing.Color.White;
			this.cmdChangeOutClose.Location = new System.Drawing.Point(381, 211);
			this.cmdChangeOutClose.Name = "cmdChangeOutClose";
			this.cmdChangeOutClose.Size = new System.Drawing.Size(82, 40);
			this.cmdChangeOutClose.TabIndex = 108;
			this.cmdChangeOutClose.Text = "OK";
			this.cmdChangeOutClose.Click += new System.EventHandler(this.cmdChangeOutClose_Click);
			this.cmdChangeOutClose.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// vsChangeOut
			// 
			this.vsChangeOut.Cols = 3;
			this.vsChangeOut.FixedCols = 0;
			this.vsChangeOut.Name = "vsChangeOut";
			this.vsChangeOut.RowHeadersVisible = false;
			this.vsChangeOut.Rows = 6;
			this.vsChangeOut.ShowFocusCell = false;
			this.vsChangeOut.Size = new System.Drawing.Size(842, 191);
			this.vsChangeOut.StandardTab = false;
			this.vsChangeOut.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsChangeOut.TabIndex = 107;
			this.vsChangeOut.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsChangeOut_MouseMoveEvent);
			this.vsChangeOut.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraFinalBillInfo
			// 
			this.fraFinalBillInfo.Controls.Add(this.txtPeriodStart);
			this.fraFinalBillInfo.Controls.Add(this.txtPeriodEnd);
			this.fraFinalBillInfo.Controls.Add(this.txtFinalBillDate);
			this.fraFinalBillInfo.Controls.Add(this.lblFinalbillDate);
			this.fraFinalBillInfo.Controls.Add(this.lblPeriodEnd);
			this.fraFinalBillInfo.Controls.Add(this.lblPerStart);
			this.fraFinalBillInfo.Location = new System.Drawing.Point(30, 30);
			this.fraFinalBillInfo.Name = "fraFinalBillInfo";
			this.fraFinalBillInfo.Size = new System.Drawing.Size(295, 210);
			this.fraFinalBillInfo.TabIndex = 91;
			this.fraFinalBillInfo.Text = "Final Bill Information";
			this.fraFinalBillInfo.Visible = false;
			this.fraFinalBillInfo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtPeriodStart
			// 
			this.txtPeriodStart.Location = new System.Drawing.Point(169, 30);
			this.txtPeriodStart.Mask = "##/##/####";
			this.txtPeriodStart.Name = "txtPeriodStart";
			this.txtPeriodStart.Size = new System.Drawing.Size(106, 22);
			this.txtPeriodStart.TabIndex = 92;
			this.txtPeriodStart.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtPeriodEnd
			// 
			this.txtPeriodEnd.Location = new System.Drawing.Point(169, 90);
			this.txtPeriodEnd.Mask = "##/##/####";
			this.txtPeriodEnd.Name = "txtPeriodEnd";
			this.txtPeriodEnd.Size = new System.Drawing.Size(106, 22);
			this.txtPeriodEnd.TabIndex = 94;
			this.txtPeriodEnd.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtFinalBillDate
			// 
			this.txtFinalBillDate.Location = new System.Drawing.Point(169, 150);
			this.txtFinalBillDate.Mask = "##/##/####";
			this.txtFinalBillDate.Name = "txtFinalBillDate";
			this.txtFinalBillDate.Size = new System.Drawing.Size(106, 22);
			this.txtFinalBillDate.TabIndex = 96;
			this.txtFinalBillDate.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblFinalbillDate
			// 
			this.lblFinalbillDate.AutoSize = true;
			this.lblFinalbillDate.Location = new System.Drawing.Point(20, 164);
			this.lblFinalbillDate.Name = "lblFinalbillDate";
			this.lblFinalbillDate.Size = new System.Drawing.Size(69, 15);
			this.lblFinalbillDate.TabIndex = 97;
			this.lblFinalbillDate.Text = "BILL DATE";
			this.lblFinalbillDate.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblPeriodEnd
			// 
			this.lblPeriodEnd.AutoSize = true;
			this.lblPeriodEnd.Location = new System.Drawing.Point(20, 104);
			this.lblPeriodEnd.Name = "lblPeriodEnd";
			this.lblPeriodEnd.Size = new System.Drawing.Size(84, 15);
			this.lblPeriodEnd.TabIndex = 95;
			this.lblPeriodEnd.Text = "PERIOD END";
			this.lblPeriodEnd.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblPerStart
			// 
			this.lblPerStart.AutoSize = true;
			this.lblPerStart.Location = new System.Drawing.Point(20, 44);
			this.lblPerStart.Name = "lblPerStart";
			this.lblPerStart.TabIndex = 93;
			this.lblPerStart.Text = "PERIOD START";
			this.lblPerStart.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraMeter
			// 
			this.fraMeter.Controls.Add(this.fraAccountInfo);
			this.fraMeter.Controls.Add(this.fraReading);
			this.fraMeter.Controls.Add(this.fraMiddle);
			this.fraMeter.Controls.Add(this.fraCombos);
			this.fraMeter.Controls.Add(this.fraCurrentMeter);
			this.fraMeter.Controls.Add(this.fraRates);
			this.fraMeter.Location = new System.Drawing.Point(30, 30);
			this.fraMeter.Name = "fraMeter";
			this.fraMeter.Size = new System.Drawing.Size(995, 1168);
			this.fraMeter.TabIndex = 17;
			this.fraMeter.Visible = false;
			this.fraMeter.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraAccountInfo
			// 
			this.fraAccountInfo.AppearanceKey = "groupBoxNoBorders";
			this.fraAccountInfo.Controls.Add(this.vsAccountInfo);
			this.fraAccountInfo.Location = new System.Drawing.Point(355, 0);
			this.fraAccountInfo.Name = "fraAccountInfo";
			this.fraAccountInfo.Size = new System.Drawing.Size(623, 160);
			this.fraAccountInfo.TabIndex = 48;
			this.fraAccountInfo.Text = "Account Information";
			this.fraAccountInfo.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// vsAccountInfo
			// 
			this.vsAccountInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsAccountInfo.Cols = 4;
			this.vsAccountInfo.ColumnHeadersVisible = false;
			this.vsAccountInfo.FixedCols = 0;
			this.vsAccountInfo.FixedRows = 0;
			this.vsAccountInfo.Location = new System.Drawing.Point(0, 30);
			this.vsAccountInfo.Name = "vsAccountInfo";
			this.vsAccountInfo.RowHeadersVisible = false;
			this.vsAccountInfo.Rows = 3;
			this.vsAccountInfo.ShowFocusCell = false;
			this.vsAccountInfo.Size = new System.Drawing.Size(623, 123);
			this.vsAccountInfo.TabIndex = 49;
			this.vsAccountInfo.TabStop = false;
			this.vsAccountInfo.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsAccountInfo_ValidateEdit);
			this.vsAccountInfo.CurrentCellChanged += new System.EventHandler(this.vsAccountInfo_RowColChange);
			// 
			// fraReading
			// 
			this.fraReading.AppearanceKey = "groupBoxNoBorders";
			this.fraReading.Controls.Add(this.txtLong);
			this.fraReading.Controls.Add(this.txtLat);
			this.fraReading.Controls.Add(this.txtXRef2);
			this.fraReading.Controls.Add(this.txtXRef1);
			this.fraReading.Controls.Add(this.txtLocation);
			this.fraReading.Controls.Add(this.vsReading);
			this.fraReading.Controls.Add(this.lblLong);
			this.fraReading.Controls.Add(this.lblLat);
			this.fraReading.Controls.Add(this.lblXRef2);
			this.fraReading.Controls.Add(this.lblXRef1);
			this.fraReading.Controls.Add(this.lblMeterLocation);
			this.fraReading.Enabled = false;
			this.fraReading.Location = new System.Drawing.Point(717, 180);
			this.fraReading.Name = "fraReading";
			this.fraReading.Size = new System.Drawing.Size(263, 530);
			this.fraReading.TabIndex = 60;
			this.fraReading.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtLong
			// 
			this.txtLong.BackColor = System.Drawing.SystemColors.Window;
			this.txtLong.Location = new System.Drawing.Point(99, 490);
			this.txtLong.MaxLength = 15;
			this.txtLong.Name = "txtLong";
			this.txtLong.Size = new System.Drawing.Size(162, 40);
			this.txtLong.TabIndex = 111;
			this.ToolTip1.SetToolTip(this.txtLong, "Reference field mostly used for cross referencing account numbers.");
			this.txtLong.TextChanged += new System.EventHandler(this.txtLong_TextChanged);
			// 
			// txtLat
			// 
			this.txtLat.BackColor = System.Drawing.SystemColors.Window;
			this.txtLat.Location = new System.Drawing.Point(99, 430);
			this.txtLat.MaxLength = 15;
			this.txtLat.Name = "txtLat";
			this.txtLat.Size = new System.Drawing.Size(162, 40);
			this.txtLat.TabIndex = 109;
			this.ToolTip1.SetToolTip(this.txtLat, "Reference field mostly used for cross referencing account numbers.");
			this.txtLat.TextChanged += new System.EventHandler(this.txtLat_TextChanged);
			// 
			// txtXRef2
			// 
			this.txtXRef2.BackColor = System.Drawing.SystemColors.Window;
			this.txtXRef2.Location = new System.Drawing.Point(99, 370);
			this.txtXRef2.MaxLength = 50;
			this.txtXRef2.Name = "txtXRef2";
			this.txtXRef2.Size = new System.Drawing.Size(162, 40);
			this.txtXRef2.TabIndex = 36;
			this.ToolTip1.SetToolTip(this.txtXRef2, "Reference field mostly used for cross referencing account numbers.");
			this.txtXRef2.TextChanged += new System.EventHandler(this.txtXRef2_TextChanged);
			// 
			// txtXRef1
			// 
			this.txtXRef1.BackColor = System.Drawing.SystemColors.Window;
			this.txtXRef1.Location = new System.Drawing.Point(99, 310);
			this.txtXRef1.MaxLength = 50;
			this.txtXRef1.Name = "txtXRef1";
			this.txtXRef1.Size = new System.Drawing.Size(162, 40);
			this.txtXRef1.TabIndex = 35;
			this.ToolTip1.SetToolTip(this.txtXRef1, "Reference field mostly used for cross referencing account numbers.");
			this.txtXRef1.TextChanged += new System.EventHandler(this.txtXRef1_TextChanged);
			// 
			// txtLocation
			// 
			this.txtLocation.BackColor = System.Drawing.SystemColors.Window;
			this.txtLocation.Location = new System.Drawing.Point(99, 250);
			this.txtLocation.MaxLength = 50;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Size = new System.Drawing.Size(162, 40);
			this.txtLocation.TabIndex = 34;
			this.ToolTip1.SetToolTip(this.txtLocation, "Location of the property that the meter is located on.");
			this.txtLocation.TextChanged += new System.EventHandler(this.txtLocation_TextChanged);
			// 
			// vsReading
			// 
			this.vsReading.Cols = 3;
			this.vsReading.FixedCols = 0;
			this.vsReading.Name = "vsReading";
			this.vsReading.RowHeadersVisible = false;
			this.vsReading.Rows = 6;
			this.vsReading.ShowFocusCell = false;
			this.vsReading.Size = new System.Drawing.Size(261, 232);
			this.vsReading.StandardTab = false;
			this.vsReading.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsReading.TabIndex = 33;
			this.vsReading.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsReading_KeyDownEdit);
			this.vsReading.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsReading_KeyPressEdit);
			this.vsReading.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsReading_BeforeEdit);
			this.vsReading.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsReading_ValidateEdit);
			this.vsReading.CurrentCellChanged += new System.EventHandler(this.vsReading_RowColChange);
			this.vsReading.Leave += new System.EventHandler(this.vsReading_Leave);
			// 
			// lblLong
			// 
			this.lblLong.AutoSize = true;
			this.lblLong.Location = new System.Drawing.Point(0, 504);
			this.lblLong.Name = "lblLong";
			this.lblLong.Size = new System.Drawing.Size(40, 15);
			this.lblLong.TabIndex = 112;
			this.lblLong.Text = "LONG";
			this.ToolTip1.SetToolTip(this.lblLong, "Reference field mostly used for cross referencing account numbers.");
			this.lblLong.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblLat
			// 
			this.lblLat.AutoSize = true;
			this.lblLat.Location = new System.Drawing.Point(0, 444);
			this.lblLat.Name = "lblLat";
			this.lblLat.Size = new System.Drawing.Size(27, 15);
			this.lblLat.TabIndex = 110;
			this.lblLat.Text = "LAT";
			this.ToolTip1.SetToolTip(this.lblLat, "Reference field mostly used for cross referencing account numbers.");
			this.lblLat.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblXRef2
			// 
			this.lblXRef2.AutoSize = true;
			this.lblXRef2.Location = new System.Drawing.Point(0, 384);
			this.lblXRef2.Name = "lblXRef2";
			this.lblXRef2.Size = new System.Drawing.Size(40, 15);
			this.lblXRef2.TabIndex = 84;
			this.lblXRef2.Text = "REF 2";
			this.ToolTip1.SetToolTip(this.lblXRef2, "Reference field mostly used for cross referencing account numbers.");
			this.lblXRef2.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblXRef1
			// 
			this.lblXRef1.AutoSize = true;
			this.lblXRef1.Location = new System.Drawing.Point(0, 324);
			this.lblXRef1.Name = "lblXRef1";
			this.lblXRef1.Size = new System.Drawing.Size(40, 15);
			this.lblXRef1.TabIndex = 83;
			this.lblXRef1.Text = "REF 1";
			this.ToolTip1.SetToolTip(this.lblXRef1, "Reference field mostly used for cross referencing account numbers.");
			this.lblXRef1.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblMeterLocation
			// 
			this.lblMeterLocation.AutoSize = true;
			this.lblMeterLocation.Location = new System.Drawing.Point(0, 264);
			this.lblMeterLocation.Name = "lblMeterLocation";
			this.lblMeterLocation.Size = new System.Drawing.Size(30, 15);
			this.lblMeterLocation.TabIndex = 82;
			this.lblMeterLocation.Text = "LOC";
			this.ToolTip1.SetToolTip(this.lblMeterLocation, "Location of the property that the meter is located on.");
			this.lblMeterLocation.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraMiddle
			// 
			this.fraMiddle.AppearanceKey = "groupBoxNoBorders";
			this.fraMiddle.Controls.Add(this.txtReaderCode);
			this.fraMiddle.Controls.Add(this.cmbRadio);
			this.fraMiddle.Controls.Add(this.txtMXU);
			this.fraMiddle.Controls.Add(this.chkNegativeConsumption);
			this.fraMiddle.Controls.Add(this.chkIncludeInExtract);
			this.fraMiddle.Controls.Add(this.chkMeterFinalBill);
			this.fraMiddle.Controls.Add(this.chkMeterNoBill);
			this.fraMiddle.Controls.Add(this.txtRemoteNumber);
			this.fraMiddle.Controls.Add(this.txtSerialNumber);
			this.fraMiddle.Controls.Add(this.vsPercentage);
			this.fraMiddle.Controls.Add(this.lblReaderCode);
			this.fraMiddle.Controls.Add(this.lblMXU);
			this.fraMiddle.Controls.Add(this.lblRemoteNumber);
			this.fraMiddle.Controls.Add(this.lblSerialNumber);
			this.fraMiddle.Enabled = false;
			this.fraMiddle.Location = new System.Drawing.Point(355, 180);
			this.fraMiddle.Name = "fraMiddle";
			this.fraMiddle.Size = new System.Drawing.Size(332, 545);
			this.fraMiddle.TabIndex = 59;
			this.fraMiddle.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtReaderCode
			// 
			this.txtReaderCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtReaderCode.Location = new System.Drawing.Point(168, 505);
			this.txtReaderCode.Name = "txtReaderCode";
			this.txtReaderCode.Size = new System.Drawing.Size(164, 40);
			this.txtReaderCode.TabIndex = 139;
			this.txtReaderCode.Enter += new System.EventHandler(this.txtReaderCode_Enter);
			this.txtReaderCode.TextChanged += new System.EventHandler(this.txtReaderCode_TextChanged);
			this.txtReaderCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtReaderCode_Validating);
			// 
			// cmbRadio
			// 
			this.cmbRadio.BackColor = System.Drawing.SystemColors.Window;
			this.cmbRadio.Items.AddRange(new object[] {
            "Manual"});
			this.cmbRadio.Location = new System.Drawing.Point(168, 310);
			this.cmbRadio.Name = "cmbRadio";
			this.cmbRadio.Size = new System.Drawing.Size(164, 40);
			this.cmbRadio.TabIndex = 104;
			this.cmbRadio.SelectedIndexChanged += new System.EventHandler(this.cmbRadio_SelectedIndexChanged);
			this.cmbRadio.DropDown += new System.EventHandler(this.cmbRadio_DropDown);
			this.cmbRadio.TextChanged += new System.EventHandler(this.cmbRadio_TextChanged);
			// 
			// txtMXU
			// 
			this.txtMXU.BackColor = System.Drawing.SystemColors.Window;
			this.txtMXU.Location = new System.Drawing.Point(168, 250);
			this.txtMXU.MaxLength = 30;
			this.txtMXU.Name = "txtMXU";
			this.txtMXU.Size = new System.Drawing.Size(164, 40);
			this.txtMXU.TabIndex = 102;
			this.txtMXU.Enter += new System.EventHandler(this.txtMXU_Enter);
			this.txtMXU.TextChanged += new System.EventHandler(this.txtMXU_TextChanged);
			this.txtMXU.Validating += new System.ComponentModel.CancelEventHandler(this.txtMXU_Validating);
			// 
			// chkNegativeConsumption
			// 
			this.chkNegativeConsumption.Enabled = false;
			this.chkNegativeConsumption.Location = new System.Drawing.Point(0, 458);
			this.chkNegativeConsumption.Name = "chkNegativeConsumption";
			this.chkNegativeConsumption.Size = new System.Drawing.Size(83, 22);
			this.chkNegativeConsumption.TabIndex = 87;
			this.chkNegativeConsumption.Text = "Negative";
			this.ToolTip1.SetToolTip(this.chkNegativeConsumption, "This meter is a negative consumption meter and any consumption will be subtracted" +
        ".");
			this.chkNegativeConsumption.CheckedChanged += new System.EventHandler(this.chkNegativeConsumption_CheckedChanged);
			// 
			// chkIncludeInExtract
			// 
			this.chkIncludeInExtract.Location = new System.Drawing.Point(0, 317);
			this.chkIncludeInExtract.Name = "chkIncludeInExtract";
			this.chkIncludeInExtract.Size = new System.Drawing.Size(132, 22);
			this.chkIncludeInExtract.TabIndex = 30;
			this.chkIncludeInExtract.Text = "Include In Extract";
			this.ToolTip1.SetToolTip(this.chkIncludeInExtract, "Include in Billing Extract.");
			this.chkIncludeInExtract.CheckedChanged += new System.EventHandler(this.chkIncludeInExtract_CheckedChanged);
			// 
			// chkMeterFinalBill
			// 
			this.chkMeterFinalBill.Location = new System.Drawing.Point(0, 411);
			this.chkMeterFinalBill.Name = "chkMeterFinalBill";
			this.chkMeterFinalBill.Size = new System.Drawing.Size(95, 22);
			this.chkMeterFinalBill.TabIndex = 32;
			this.chkMeterFinalBill.Text = "Final Billed";
			this.ToolTip1.SetToolTip(this.chkMeterFinalBill, "This meter is never to be billed again.");
			this.chkMeterFinalBill.CheckedChanged += new System.EventHandler(this.chkMeterFinalBill_CheckedChanged);
			// 
			// chkMeterNoBill
			// 
			this.chkMeterNoBill.Location = new System.Drawing.Point(0, 364);
			this.chkMeterNoBill.Name = "chkMeterNoBill";
			this.chkMeterNoBill.Size = new System.Drawing.Size(94, 22);
			this.chkMeterNoBill.TabIndex = 31;
			this.chkMeterNoBill.Text = "No Charge";
			this.ToolTip1.SetToolTip(this.chkMeterNoBill, "Do not bill this meter.");
			this.chkMeterNoBill.CheckedChanged += new System.EventHandler(this.chkMeterNoBill_CheckedChanged);
			// 
			// txtRemoteNumber
			// 
			this.txtRemoteNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtRemoteNumber.Location = new System.Drawing.Point(168, 190);
			this.txtRemoteNumber.MaxLength = 30;
			this.txtRemoteNumber.Name = "txtRemoteNumber";
			this.txtRemoteNumber.Size = new System.Drawing.Size(164, 40);
			this.txtRemoteNumber.TabIndex = 29;
			this.txtRemoteNumber.Enter += new System.EventHandler(this.txtRemoteNumber_Enter);
			this.txtRemoteNumber.TextChanged += new System.EventHandler(this.txtRemoteNumber_TextChanged);
			this.txtRemoteNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtRemoteNumber_Validating);
			// 
			// txtSerialNumber
			// 
			this.txtSerialNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtSerialNumber.Location = new System.Drawing.Point(168, 130);
			this.txtSerialNumber.MaxLength = 30;
			this.txtSerialNumber.Name = "txtSerialNumber";
			this.txtSerialNumber.Size = new System.Drawing.Size(164, 40);
			this.txtSerialNumber.TabIndex = 28;
			this.txtSerialNumber.Enter += new System.EventHandler(this.txtSerialNumber_Enter);
			this.txtSerialNumber.TextChanged += new System.EventHandler(this.txtSerialNumber_TextChanged);
			this.txtSerialNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtSerialNumber_Validating);
			// 
			// vsPercentage
			// 
			this.vsPercentage.Anchor = Wisej.Web.AnchorStyles.None;
			this.vsPercentage.Cols = 3;
			this.vsPercentage.FixedCols = 0;
			this.vsPercentage.Name = "vsPercentage";
			this.vsPercentage.RowHeadersVisible = false;
			this.vsPercentage.Rows = 3;
			this.vsPercentage.ShowFocusCell = false;
			this.vsPercentage.Size = new System.Drawing.Size(332, 112);
			this.vsPercentage.StandardTab = false;
			this.vsPercentage.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsPercentage.TabIndex = 27;
			this.vsPercentage.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsPercentage_KeyPressEdit);
			this.vsPercentage.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsPercentage_ValidateEdit);
			this.vsPercentage.CurrentCellChanged += new System.EventHandler(this.vsPercentage_RowColChange);
			this.vsPercentage.Enter += new System.EventHandler(this.vsPercentage_Enter);
			this.vsPercentage.Leave += new System.EventHandler(this.vsPercentage_Leave);
			// 
			// lblReaderCode
			// 
			this.lblReaderCode.AutoSize = true;
			this.lblReaderCode.Location = new System.Drawing.Point(0, 519);
			this.lblReaderCode.Name = "lblReaderCode";
			this.lblReaderCode.Size = new System.Drawing.Size(99, 15);
			this.lblReaderCode.TabIndex = 138;
			this.lblReaderCode.Text = "READER CODE";
			this.lblReaderCode.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblMXU
			// 
			this.lblMXU.AutoSize = true;
			this.lblMXU.Location = new System.Drawing.Point(0, 264);
			this.lblMXU.Name = "lblMXU";
			this.lblMXU.Size = new System.Drawing.Size(92, 15);
			this.lblMXU.TabIndex = 103;
			this.lblMXU.Text = "MXU NUMBER";
			this.lblMXU.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblRemoteNumber
			// 
			this.lblRemoteNumber.AutoSize = true;
			this.lblRemoteNumber.Location = new System.Drawing.Point(0, 204);
			this.lblRemoteNumber.Name = "lblRemoteNumber";
			this.lblRemoteNumber.Size = new System.Drawing.Size(119, 15);
			this.lblRemoteNumber.TabIndex = 62;
			this.lblRemoteNumber.Text = "REMOTE NUMBER";
			this.lblRemoteNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblSerialNumber
			// 
			this.lblSerialNumber.AutoSize = true;
			this.lblSerialNumber.Location = new System.Drawing.Point(0, 144);
			this.lblSerialNumber.Name = "lblSerialNumber";
			this.lblSerialNumber.Size = new System.Drawing.Size(110, 15);
			this.lblSerialNumber.TabIndex = 61;
			this.lblSerialNumber.Text = "SERIAL NUMBER";
			this.lblSerialNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraCombos
			// 
			this.fraCombos.AppearanceKey = "groupBoxNoBorders";
			this.fraCombos.Controls.Add(this.cmbCombineMeter);
			this.fraCombos.Controls.Add(this.cmbBook);
			this.fraCombos.Controls.Add(this.cmbFrequency);
			this.fraCombos.Controls.Add(this.cmbService);
			this.fraCombos.Controls.Add(this.cmbMultiplier);
			this.fraCombos.Controls.Add(this.cmbMeterDigits);
			this.fraCombos.Controls.Add(this.cmbMeterSize);
			this.fraCombos.Controls.Add(this.txtSequence);
			this.fraCombos.Controls.Add(this.lblCombineMeters);
			this.fraCombos.Controls.Add(this.lblFrequency);
			this.fraCombos.Controls.Add(this.lblService);
			this.fraCombos.Controls.Add(this.lblMultiplier);
			this.fraCombos.Controls.Add(this.lblMeterDigits);
			this.fraCombos.Controls.Add(this.lblMeterSize);
			this.fraCombos.Controls.Add(this.lblSequence);
			this.fraCombos.Controls.Add(this.lblBook_0);
			this.fraCombos.Enabled = false;
			this.fraCombos.Location = new System.Drawing.Point(0, 110);
			this.fraCombos.Name = "fraCombos";
			this.fraCombos.Size = new System.Drawing.Size(325, 460);
			this.fraCombos.TabIndex = 50;
			this.fraCombos.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmbCombineMeter
			// 
			this.cmbCombineMeter.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCombineMeter.Location = new System.Drawing.Point(157, 420);
			this.cmbCombineMeter.Name = "cmbCombineMeter";
			this.cmbCombineMeter.Size = new System.Drawing.Size(168, 40);
			this.cmbCombineMeter.TabIndex = 101;
			this.cmbCombineMeter.SelectedIndexChanged += new System.EventHandler(this.cmbCombineMeter_SelectedIndexChanged);
			this.cmbCombineMeter.TextChanged += new System.EventHandler(this.cmbCombineMeter_TextChanged);
			this.cmbCombineMeter.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCombineMeter_KeyDown);
			// 
			// cmbBook
			// 
			this.cmbBook.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBook.Location = new System.Drawing.Point(157, 0);
			this.cmbBook.Name = "cmbBook";
			this.cmbBook.Size = new System.Drawing.Size(168, 40);
			this.cmbBook.Sorted = true;
			this.cmbBook.TabIndex = 20;
			this.cmbBook.SelectedIndexChanged += new System.EventHandler(this.cmbBook_SelectedIndexChanged);
			this.cmbBook.DropDown += new System.EventHandler(this.cmbBook_DropDown);
			this.cmbBook.TextChanged += new System.EventHandler(this.cmbBook_TextChanged);
			this.cmbBook.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBook_KeyDown);
			// 
			// cmbFrequency
			// 
			this.cmbFrequency.BackColor = System.Drawing.SystemColors.Window;
			this.cmbFrequency.Location = new System.Drawing.Point(157, 300);
			this.cmbFrequency.Name = "cmbFrequency";
			this.cmbFrequency.Size = new System.Drawing.Size(168, 40);
			this.cmbFrequency.TabIndex = 26;
			this.cmbFrequency.SelectedIndexChanged += new System.EventHandler(this.cmbFrequency_SelectedIndexChanged);
			this.cmbFrequency.DropDown += new System.EventHandler(this.cmbFrequency_DropDown);
			this.cmbFrequency.TextChanged += new System.EventHandler(this.cmbFrequency_TextChanged);
			this.cmbFrequency.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbFrequency_KeyDown);
			// 
			// cmbService
			// 
			this.cmbService.BackColor = System.Drawing.SystemColors.Window;
			this.cmbService.Location = new System.Drawing.Point(157, 240);
			this.cmbService.Name = "cmbService";
			this.cmbService.Size = new System.Drawing.Size(168, 40);
			this.cmbService.TabIndex = 25;
			this.cmbService.SelectedIndexChanged += new System.EventHandler(this.cmbService_SelectedIndexChanged);
			this.cmbService.TextChanged += new System.EventHandler(this.cmbService_TextChanged);
			this.cmbService.Validating += new System.ComponentModel.CancelEventHandler(this.cmbService_Validating);
			this.cmbService.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbService_KeyDown);
			// 
			// cmbMultiplier
			// 
			this.cmbMultiplier.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMultiplier.Location = new System.Drawing.Point(157, 360);
			this.cmbMultiplier.Name = "cmbMultiplier";
			this.cmbMultiplier.Size = new System.Drawing.Size(168, 40);
			this.cmbMultiplier.TabIndex = 24;
			this.cmbMultiplier.SelectedIndexChanged += new System.EventHandler(this.cmbMultiplier_SelectedIndexChanged);
			this.cmbMultiplier.TextChanged += new System.EventHandler(this.cmbMultiplier_TextChanged);
			this.cmbMultiplier.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbMultiplier_KeyDown);
			// 
			// cmbMeterDigits
			// 
			this.cmbMeterDigits.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMeterDigits.Location = new System.Drawing.Point(157, 180);
			this.cmbMeterDigits.Name = "cmbMeterDigits";
			this.cmbMeterDigits.Size = new System.Drawing.Size(168, 40);
			this.cmbMeterDigits.TabIndex = 23;
			this.cmbMeterDigits.SelectedIndexChanged += new System.EventHandler(this.cmbMeterDigits_SelectedIndexChanged);
			this.cmbMeterDigits.TextChanged += new System.EventHandler(this.cmbMeterDigits_TextChanged);
			this.cmbMeterDigits.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbMeterDigits_KeyDown);
			// 
			// cmbMeterSize
			// 
			this.cmbMeterSize.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMeterSize.Location = new System.Drawing.Point(157, 120);
			this.cmbMeterSize.Name = "cmbMeterSize";
			this.cmbMeterSize.Size = new System.Drawing.Size(168, 40);
			this.cmbMeterSize.TabIndex = 22;
			this.cmbMeterSize.SelectedIndexChanged += new System.EventHandler(this.cmbMeterSize_SelectedIndexChanged);
			this.cmbMeterSize.DropDown += new System.EventHandler(this.cmbMeterSize_DropDown);
			this.cmbMeterSize.TextChanged += new System.EventHandler(this.cmbMeterSize_TextChanged);
			this.cmbMeterSize.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbMeterSize_KeyDown);
			// 
			// txtSequence
			// 
			this.txtSequence.BackColor = System.Drawing.SystemColors.Window;
			this.txtSequence.Location = new System.Drawing.Point(157, 60);
			this.txtSequence.MaxLength = 6;
			this.txtSequence.Name = "txtSequence";
			this.txtSequence.Size = new System.Drawing.Size(168, 40);
			this.txtSequence.TabIndex = 21;
			this.txtSequence.Enter += new System.EventHandler(this.txtSequence_Enter);
			this.txtSequence.Validating += new System.ComponentModel.CancelEventHandler(this.txtSequence_Validating);
			this.txtSequence.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSequence_KeyPress);
			// 
			// lblCombineMeters
			// 
			this.lblCombineMeters.AutoSize = true;
			this.lblCombineMeters.Location = new System.Drawing.Point(0, 434);
			this.lblCombineMeters.Name = "lblCombineMeters";
			this.lblCombineMeters.Size = new System.Drawing.Size(64, 15);
			this.lblCombineMeters.TabIndex = 100;
			this.lblCombineMeters.Text = "COMBINE";
			this.lblCombineMeters.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblFrequency
			// 
			this.lblFrequency.AutoSize = true;
			this.lblFrequency.Location = new System.Drawing.Point(0, 314);
			this.lblFrequency.Name = "lblFrequency";
			this.lblFrequency.Size = new System.Drawing.Size(85, 15);
			this.lblFrequency.TabIndex = 58;
			this.lblFrequency.Text = "FREQUENCY";
			this.lblFrequency.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblService
			// 
			this.lblService.AutoSize = true;
			this.lblService.Location = new System.Drawing.Point(0, 254);
			this.lblService.Name = "lblService";
			this.lblService.Size = new System.Drawing.Size(61, 15);
			this.lblService.TabIndex = 57;
			this.lblService.Text = "SERVICE";
			this.lblService.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblMultiplier
			// 
			this.lblMultiplier.AutoSize = true;
			this.lblMultiplier.Location = new System.Drawing.Point(0, 374);
			this.lblMultiplier.Name = "lblMultiplier";
			this.lblMultiplier.Size = new System.Drawing.Size(105, 15);
			this.lblMultiplier.TabIndex = 56;
			this.lblMultiplier.Text = "READING UNITS";
			this.lblMultiplier.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblMeterDigits
			// 
			this.lblMeterDigits.AutoSize = true;
			this.lblMeterDigits.Location = new System.Drawing.Point(0, 194);
			this.lblMeterDigits.Name = "lblMeterDigits";
			this.lblMeterDigits.Size = new System.Drawing.Size(96, 15);
			this.lblMeterDigits.TabIndex = 55;
			this.lblMeterDigits.Text = "METER DIGITS";
			this.lblMeterDigits.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblMeterSize
			// 
			this.lblMeterSize.AutoSize = true;
			this.lblMeterSize.Location = new System.Drawing.Point(0, 134);
			this.lblMeterSize.Name = "lblMeterSize";
			this.lblMeterSize.Size = new System.Drawing.Size(82, 15);
			this.lblMeterSize.TabIndex = 54;
			this.lblMeterSize.Text = "METER SIZE";
			this.lblMeterSize.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblSequence
			// 
			this.lblSequence.AutoSize = true;
			this.lblSequence.Location = new System.Drawing.Point(0, 74);
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Size = new System.Drawing.Size(76, 15);
			this.lblSequence.TabIndex = 53;
			this.lblSequence.Text = "SEQUENCE";
			this.lblSequence.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// lblBook_0
			// 
			this.lblBook_0.AutoSize = true;
			this.lblBook_0.Location = new System.Drawing.Point(0, 14);
			this.lblBook_0.Name = "lblBook_0";
			this.lblBook_0.Size = new System.Drawing.Size(41, 15);
			this.lblBook_0.TabIndex = 52;
			this.lblBook_0.Text = "BOOK";
			this.lblBook_0.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraCurrentMeter
			// 
			this.fraCurrentMeter.Controls.Add(this.cmbMeterNumber);
			this.fraCurrentMeter.Controls.Add(this.lblFinalBill);
			this.fraCurrentMeter.Name = "fraCurrentMeter";
			this.fraCurrentMeter.Size = new System.Drawing.Size(325, 90);
			this.fraCurrentMeter.TabIndex = 47;
			this.fraCurrentMeter.Text = "Current Meter";
			this.fraCurrentMeter.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmbMeterNumber
			// 
			this.cmbMeterNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMeterNumber.Location = new System.Drawing.Point(20, 30);
			this.cmbMeterNumber.Name = "cmbMeterNumber";
			this.cmbMeterNumber.Size = new System.Drawing.Size(66, 40);
			this.cmbMeterNumber.TabIndex = 19;
			this.cmbMeterNumber.SelectedIndexChanged += new System.EventHandler(this.cmbMeterNumber_SelectedIndexChanged);
			this.cmbMeterNumber.TextChanged += new System.EventHandler(this.cmbMeterNumber_TextChanged);
			this.cmbMeterNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbMeterNumber_KeyDown);
			// 
			// lblFinalBill
			// 
			this.lblFinalBill.AutoSize = true;
			this.lblFinalBill.Location = new System.Drawing.Point(106, 44);
			this.lblFinalBill.Name = "lblFinalBill";
			this.lblFinalBill.Size = new System.Drawing.Size(20, 15);
			this.lblFinalBill.TabIndex = 98;
			this.lblFinalBill.Text = "FB";
			this.ToolTip1.SetToolTip(this.lblFinalBill, "Edit the final bill information.");
			this.lblFinalBill.Click += new System.EventHandler(this.lblFinalBill_Click);
			this.lblFinalBill.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// fraRates
			// 
			this.fraRates.Controls.Add(this.txtAdjustDescriptionS);
			this.fraRates.Controls.Add(this.vsMeter);
			this.fraRates.Controls.Add(this.txtAdjustDescriptionW);
			this.fraRates.Enabled = false;
			this.fraRates.Location = new System.Drawing.Point(0, 745);
			this.fraRates.Name = "fraRates";
			this.fraRates.Size = new System.Drawing.Size(978, 423);
			this.fraRates.TabIndex = 51;
			this.fraRates.Text = "Billing Information";
			this.fraRates.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// txtAdjustDescriptionS
			// 
			this.txtAdjustDescriptionS.BackColor = System.Drawing.SystemColors.Window;
			this.txtAdjustDescriptionS.Location = new System.Drawing.Point(420, 363);
			this.txtAdjustDescriptionS.Name = "txtAdjustDescriptionS";
			this.txtAdjustDescriptionS.Size = new System.Drawing.Size(370, 40);
			this.txtAdjustDescriptionS.TabIndex = 39;
			this.txtAdjustDescriptionS.Visible = false;
			this.txtAdjustDescriptionS.TextChanged += new System.EventHandler(this.txtAdjustDescriptionS_TextChanged);
			// 
			// vsMeter
			// 
			this.vsMeter.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsMeter.Cols = 10;
			this.vsMeter.ColumnHeadersHeight = 60;
			this.vsMeter.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsMeter.FixedCols = 0;
			this.vsMeter.FixedRows = 2;
			this.vsMeter.Location = new System.Drawing.Point(20, 30);
			this.vsMeter.Name = "vsMeter";
			this.vsMeter.ReadOnly = false;
			this.vsMeter.RowHeadersVisible = false;
			this.vsMeter.Rows = 8;
			this.vsMeter.ShowFocusCell = false;
			this.vsMeter.Size = new System.Drawing.Size(938, 313);
			this.vsMeter.StandardTab = false;
			this.vsMeter.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsMeter.TabIndex = 37;
			this.vsMeter.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsMeter_KeyPressEdit);
			this.vsMeter.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsMeter_ChangeEdit);
			this.vsMeter.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsMeter_MouseMoveEvent);
			this.vsMeter.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsMeter_ValidateEdit);
			this.vsMeter.CurrentCellChanged += new System.EventHandler(this.vsMeter_RowColChange);
			this.vsMeter.Leave += new System.EventHandler(this.vsMeter_Leave);
			// 
			// txtAdjustDescriptionW
			// 
			this.txtAdjustDescriptionW.BackColor = System.Drawing.SystemColors.Window;
			this.txtAdjustDescriptionW.Location = new System.Drawing.Point(20, 363);
			this.txtAdjustDescriptionW.Name = "txtAdjustDescriptionW";
			this.txtAdjustDescriptionW.Size = new System.Drawing.Size(370, 40);
			this.txtAdjustDescriptionW.TabIndex = 38;
			this.txtAdjustDescriptionW.Visible = false;
			this.txtAdjustDescriptionW.TextChanged += new System.EventHandler(this.txtAdjustDescriptionW_TextChanged);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileComment,
            this.mnuFileFindSeq,
            this.mnuFilePreviousOwner,
            this.mnuFileResetMeterStatus,
            this.mnuFileChangeOutHistory,
            this.mnuFileBankrupcty,
            this.mnuAutoPay,
            this.mnuFileViewLinkedDocument});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuFileComment
			// 
			this.mnuFileComment.Index = 0;
			this.mnuFileComment.Name = "mnuFileComment";
			this.mnuFileComment.Text = "Comment";
			this.mnuFileComment.Click += new System.EventHandler(this.mnuFileComment_Click);
			// 
			// mnuFileFindSeq
			// 
			this.mnuFileFindSeq.Index = 1;
			this.mnuFileFindSeq.Name = "mnuFileFindSeq";
			this.mnuFileFindSeq.Text = "Find Next Avail Seq";
			this.mnuFileFindSeq.Click += new System.EventHandler(this.mnuFileFindSeq_Click);
			// 
			// mnuFilePreviousOwner
			// 
			this.mnuFilePreviousOwner.Index = 2;
			this.mnuFilePreviousOwner.Name = "mnuFilePreviousOwner";
			this.mnuFilePreviousOwner.Text = "Previous Owner";
			this.mnuFilePreviousOwner.Click += new System.EventHandler(this.mnuFilePreviousOwner_Click);
			// 
			// mnuFileResetMeterStatus
			// 
			this.mnuFileResetMeterStatus.Index = 3;
			this.mnuFileResetMeterStatus.Name = "mnuFileResetMeterStatus";
			this.mnuFileResetMeterStatus.Text = "Reset Meter Status";
			this.mnuFileResetMeterStatus.Click += new System.EventHandler(this.mnuFileResetMeterStatus_Click);
			// 
			// mnuFileChangeOutHistory
			// 
			this.mnuFileChangeOutHistory.Index = 4;
			this.mnuFileChangeOutHistory.Name = "mnuFileChangeOutHistory";
			this.mnuFileChangeOutHistory.Text = "Meter Change Out History";
			this.mnuFileChangeOutHistory.Click += new System.EventHandler(this.mnuFileChangeOutHistory_Click);
			// 
			// mnuFileBankrupcty
			// 
			this.mnuFileBankrupcty.Index = 5;
			this.mnuFileBankrupcty.Name = "mnuFileBankrupcty";
			this.mnuFileBankrupcty.Text = "Toggle Bankruptcy Flag";
			this.mnuFileBankrupcty.Click += new System.EventHandler(this.mnuFileBankrupcty_Click);
			// 
			// mnuAutoPay
			// 
			this.mnuAutoPay.Index = 6;
			this.mnuAutoPay.Name = "mnuAutoPay";
			this.mnuAutoPay.Text = "Auto-Pay Setup";
			this.mnuAutoPay.Visible = false;
			// 
			// mnuFileViewLinkedDocument
			// 
			this.mnuFileViewLinkedDocument.Index = 7;
			this.mnuFileViewLinkedDocument.Name = "mnuFileViewLinkedDocument";
			this.mnuFileViewLinkedDocument.Text = "View Attached Documents";
			this.mnuFileViewLinkedDocument.Click += new System.EventHandler(this.mnuFileViewLinkedDocument_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSpeer,
            this.seperator3,
            this.seperator2,
            this.mnuSeperator});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSpeer
			// 
			this.mnuSpeer.Index = 0;
			this.mnuSpeer.Name = "mnuSpeer";
			this.mnuSpeer.Text = "-";
			// 
			// seperator3
			// 
			this.seperator3.Index = 1;
			this.seperator3.Name = "seperator3";
			this.seperator3.Text = "-";
			// 
			// seperator2
			// 
			this.seperator2.Index = 2;
			this.seperator2.Name = "seperator2";
			this.seperator2.Text = "-";
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = 3;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuFileShowMeter
			// 
			this.mnuFileShowMeter.Index = -1;
			this.mnuFileShowMeter.Name = "mnuFileShowMeter";
			this.mnuFileShowMeter.Shortcut = Wisej.Web.Shortcut.F9;
			this.mnuFileShowMeter.Text = "Meter Screen";
			this.mnuFileShowMeter.Click += new System.EventHandler(this.mnuFileShowMeter_Click);
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = -1;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = -1;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Exit";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = -1;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// mnuFileMeterPrevious
			// 
			this.mnuFileMeterPrevious.Index = -1;
			this.mnuFileMeterPrevious.Name = "mnuFileMeterPrevious";
			this.mnuFileMeterPrevious.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuFileMeterPrevious.Text = "Previous Meter";
			this.mnuFileMeterPrevious.Click += new System.EventHandler(this.mnuFileMeterPrevious_Click);
			// 
			// mnuFileMeterNext
			// 
			this.mnuFileMeterNext.Index = -1;
			this.mnuFileMeterNext.Name = "mnuFileMeterNext";
			this.mnuFileMeterNext.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuFileMeterNext.Text = "Next Meter";
			this.mnuFileMeterNext.Click += new System.EventHandler(this.mnuFileMeterNext_Click);
			// 
			// mnuFileMeterAdd
			// 
			this.mnuFileMeterAdd.Index = -1;
			this.mnuFileMeterAdd.Name = "mnuFileMeterAdd";
			this.mnuFileMeterAdd.Text = "Add Meter";
			this.mnuFileMeterAdd.Click += new System.EventHandler(this.mnuFileMeterAdd_Click);
			// 
			// mnuFileMeterDelete
			// 
			this.mnuFileMeterDelete.Index = -1;
			this.mnuFileMeterDelete.Name = "mnuFileMeterDelete";
			this.mnuFileMeterDelete.Text = "Delete Meter";
			this.mnuFileMeterDelete.Click += new System.EventHandler(this.mnuFileMeterDelete_Click);
			// 
			// mnuFileDeleteAccount
			// 
			this.mnuFileDeleteAccount.Index = -1;
			this.mnuFileDeleteAccount.Name = "mnuFileDeleteAccount";
			this.mnuFileDeleteAccount.Text = "Delete Master Account";
			this.mnuFileDeleteAccount.Click += new System.EventHandler(this.mnuFileDeleteAccount_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(397, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			this.cmdSave.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdFileMeterPrevious
			// 
			this.cmdFileMeterPrevious.Anchor = Wisej.Web.AnchorStyles.Right;
			this.cmdFileMeterPrevious.Location = new System.Drawing.Point(916, 29);
			this.cmdFileMeterPrevious.Name = "cmdFileMeterPrevious";
			this.cmdFileMeterPrevious.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdFileMeterPrevious.Size = new System.Drawing.Size(108, 24);
			this.cmdFileMeterPrevious.TabIndex = 1;
			this.cmdFileMeterPrevious.Text = "Previous Meter";
			this.cmdFileMeterPrevious.Click += new System.EventHandler(this.mnuFileMeterPrevious_Click);
			this.cmdFileMeterPrevious.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdFileShowMeter
			// 
			this.cmdFileShowMeter.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileShowMeter.Location = new System.Drawing.Point(812, 29);
			this.cmdFileShowMeter.Name = "cmdFileShowMeter";
			this.cmdFileShowMeter.Shortcut = Wisej.Web.Shortcut.F9;
			this.cmdFileShowMeter.Size = new System.Drawing.Size(98, 24);
			this.cmdFileShowMeter.TabIndex = 2;
			this.cmdFileShowMeter.Text = "Meter Screen";
			this.cmdFileShowMeter.Click += new System.EventHandler(this.mnuFileShowMeter_Click);
			this.cmdFileShowMeter.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdFileMeterNext
			// 
			this.cmdFileMeterNext.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileMeterNext.Location = new System.Drawing.Point(1030, 29);
			this.cmdFileMeterNext.Name = "cmdFileMeterNext";
			this.cmdFileMeterNext.Shortcut = Wisej.Web.Shortcut.F8;
			this.cmdFileMeterNext.Size = new System.Drawing.Size(84, 24);
			this.cmdFileMeterNext.TabIndex = 3;
			this.cmdFileMeterNext.Text = "Next Meter";
			this.cmdFileMeterNext.Click += new System.EventHandler(this.mnuFileMeterNext_Click);
			this.cmdFileMeterNext.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdFileMeterAdd
			// 
			this.cmdFileMeterAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileMeterAdd.Location = new System.Drawing.Point(467, 29);
			this.cmdFileMeterAdd.Name = "cmdFileMeterAdd";
			this.cmdFileMeterAdd.Size = new System.Drawing.Size(78, 24);
			this.cmdFileMeterAdd.TabIndex = 4;
			this.cmdFileMeterAdd.Text = "Add Meter";
			this.cmdFileMeterAdd.Click += new System.EventHandler(this.mnuFileMeterAdd_Click);
			this.cmdFileMeterAdd.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdFileMeterDelete
			// 
			this.cmdFileMeterDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileMeterDelete.Location = new System.Drawing.Point(551, 29);
			this.cmdFileMeterDelete.Name = "cmdFileMeterDelete";
			this.cmdFileMeterDelete.Size = new System.Drawing.Size(93, 24);
			this.cmdFileMeterDelete.TabIndex = 5;
			this.cmdFileMeterDelete.Text = "Delete Meter";
			this.cmdFileMeterDelete.Click += new System.EventHandler(this.mnuFileMeterDelete_Click);
			this.cmdFileMeterDelete.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// cmdFileDeleteAccount
			// 
			this.cmdFileDeleteAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileDeleteAccount.Location = new System.Drawing.Point(650, 29);
			this.cmdFileDeleteAccount.Name = "cmdFileDeleteAccount";
			this.cmdFileDeleteAccount.Size = new System.Drawing.Size(156, 24);
			this.cmdFileDeleteAccount.TabIndex = 6;
			this.cmdFileDeleteAccount.Text = "Delete Master Account";
			this.cmdFileDeleteAccount.Click += new System.EventHandler(this.mnuFileDeleteAccount_Click);
			this.cmdFileDeleteAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.EmptyHandler_KeyDown);
			// 
			// frmAccountMaster
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmAccountMaster";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Account Master";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmAccountMaster_Load);
			this.Activated += new System.EventHandler(this.frmAccountMaster_Activated);
			this.Resize += new System.EventHandler(this.frmAccountMaster_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAccountMaster_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAccountMaster_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMaster)).EndInit();
			this.fraMaster.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraREInfo)).EndInit();
			this.fraREInfo.ResumeLayout(false);
			this.fraREInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkUseRE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseREMortgage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMasterTB)).EndInit();
			this.fraMasterTB.ResumeLayout(false);
			this.fraMasterTB.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBillSameAsOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNoBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFinal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEmailBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdTenant2Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTenant2Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTenant1Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTenant1Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTenant2Memo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTenant1Memo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraOwnerDisp)).EndInit();
			this.fraOwnerDisp.ResumeLayout(false);
			this.fraOwnerDisp.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwner2Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwner2Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwner1Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOwner1Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgOwner2Memo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgOwner1Memo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWater)).EndInit();
			this.fraWater.ResumeLayout(false);
			this.fraWater.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtWaterAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSewer)).EndInit();
			this.fraSewer.ResumeLayout(false);
			this.fraSewer.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtSewerAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMasterGrid)).EndInit();
			this.fraMasterGrid.ResumeLayout(false);
			this.fraMasterGrid.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsMaster)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChangeOutHistory)).EndInit();
			this.fraChangeOutHistory.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdChangeOutClose)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsChangeOut)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFinalBillInfo)).EndInit();
			this.fraFinalBillInfo.ResumeLayout(false);
			this.fraFinalBillInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriodEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinalBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMeter)).EndInit();
			this.fraMeter.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraAccountInfo)).EndInit();
			this.fraAccountInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAccountInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraReading)).EndInit();
			this.fraReading.ResumeLayout(false);
			this.fraReading.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMiddle)).EndInit();
			this.fraMiddle.ResumeLayout(false);
			this.fraMiddle.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkNegativeConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeInExtract)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMeterFinalBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMeterNoBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPercentage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCombos)).EndInit();
			this.fraCombos.ResumeLayout(false);
			this.fraCombos.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCurrentMeter)).EndInit();
			this.fraCurrentMeter.ResumeLayout(false);
			this.fraCurrentMeter.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRates)).EndInit();
			this.fraRates.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsMeter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMeterPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileShowMeter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMeterNext)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMeterAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMeterDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteAccount)).EndInit();
			this.ResumeLayout(false);

		}

       
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdFileMeterPrevious;
		private FCButton cmdFileMeterNext;
		private FCButton cmdFileShowMeter;
		private FCButton cmdFileDeleteAccount;
		private FCButton cmdFileMeterDelete;
		private FCButton cmdFileMeterAdd;
        public FCTextBox txtDeedName2;
        public FCTextBox txtDeedName1;
        public FCLabel fcLabel1;
        public FCLabel fcLabel2;
        public FCLabel lblACHInfo;
    }
}
