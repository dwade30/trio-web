﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptReminderForm.
	/// </summary>
	public partial class rptReminderForm : BaseSectionReport
	{
		public rptReminderForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Notice - Form";
		}

		public static rptReminderForm InstancePtr
		{
			get
			{
				return (rptReminderForm)Sys.GetInstance(typeof(rptReminderForm));
			}
		}

		protected rptReminderForm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsRate.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderForm	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/02/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/07/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsRate = new clsDRWrapper();
		//clsDRWrapper rsLData = new clsDRWrapper();
		//clsDRWrapper rsTemp = new clsDRWrapper();
		DateTime dtDate;
		string strIntDept;
		string strIntDeptPhone;
		string strMatDept;
		string strMatDeptPhone;
		bool boolReturnAddress;
		string strLastMaturedYear = "";
		string strMod = "";
		string strDataSting;
		bool boolWater;
		int lngIndex;
		bool boolSendTenant;
		bool boolSendOwner;
		bool boolSecondCopy;
		string strReportTitle;
		public bool boolNoSummary;
		public string strHighYear = "";
		public int lngLastRateKey;
		// vbPorter upgrade warning: strPassReportTitle As string	OnWrite(VB.TextBox)
		public void Init(string strSQL, DateTime dtMailingDate, string strOriginalString, ref bool boolPassWater, string strPassReportTitle)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//clsReportPrinterFunctions rpfFont = new clsReportPrinterFunctions();
				string strPrinterFont = "";
				int const_PrintToolID;
				int lngCT;
				boolWater = boolPassWater;
				strDataSting = strOriginalString;
				strReportTitle = strPassReportTitle;
				boolReturnAddress = FCConvert.CBool(frmReminderNotices.InstancePtr.vsGrid.TextMatrix(8, 1) == "Yes");
				frmWait.InstancePtr.Unload();
				lngIndex = 0;
				FCUtils.EraseSafe(modReminderNoticeSummary.Statics.arrReminderSummaryList);
				modReminderNoticeSummary.Statics.arrReminderSummaryList = new modReminderNoticeSummary.ReminderSummaryList[0 + 1];
				// get a printer font
				// rpfFont.ChoosePrinter Me.Printer
				// strPrinterFont = rpfFont.GetFont(Me.FCGlobal.Printer.DeviceName, 10)
				// set the printer font
				// rpfFont.SetReportFontsByTag Me, "TEXT", strPrinterFont
				// 
				// this will turn the print button off
				// const_PrintToolID = 9950
				// For lngCT = 0 To Me.Toolbar.Tools.Count - 1
				// If "Print..." = Me.Toolbar.Tools(lngCT).Caption Then
				// Me.Toolbar.Tools(lngCT).ID = const_PrintToolID
				// Me.Toolbar.Tools(lngCT).Enabled = True
				// End If
				// Next lngCT
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Report"
				// use the info that is passed into the report
				dtDate = dtMailingDate;
				strIntDept = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(4, 1);
				strIntDeptPhone = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(5, 1);
				strMatDept = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(6, 1);
				strMatDeptPhone = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(7, 1);
				if (frmReminderNotices.InstancePtr.chkPastDueOnly.CheckState == Wisej.Web.CheckState.Checked)
				{
					lngLastRateKey = frmReminderNotices.InstancePtr.lngHighestRK;
				}
				else
				{
					lngLastRateKey = 0;
				}
				if (boolWater)
				{
					rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					// kk trouts-6 03012013  Change Water to Stormwater for Bangor
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						strMod = "Stormwater";
					}
					else
					{
						strMod = "Water";
					}
				}
				else
				{
					rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					strMod = "Sewer";
				}
				boolSendTenant = FCConvert.CBool(frmReminderNotices.InstancePtr.chkSendTo[0].CheckState == Wisej.Web.CheckState.Checked);
				boolSendOwner = FCConvert.CBool(frmReminderNotices.InstancePtr.chkSendTo[1].CheckState == Wisej.Web.CheckState.Checked);
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No matching records were found.", strMod + " Reminder Mailer", MessageBoxButtons.OK, MessageBoxIcon.Information);
					boolNoSummary = true;
				}
				else
				{
					boolNoSummary = false;
					// keep going
					rsRate.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
					// If boolWater Then
					// rsTemp.OpenRecordset "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.Key WHERE Service <> 'S'", strUTDatabase
					// Else
					// rsTemp.OpenRecordset "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.Key WHERE Service <> 'W'", strUTDatabase
					// End If
					// Me.PageSettings.PaperHeight = 5.5 * 1440
					// lblMailingAddress4.Top = (4 * 1440) - 256       'this sits on the bottom of the form to make sure that the detail section is still large enough
					// Me.PageSettings.PaperWidth = (8.5 * 1440)
					// Me.Zoom = -1
					frmReportViewer.InstancePtr.Init(this);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Mailers", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) > 0)
			{
				// If boolRetry Then
				// ReDim Preserve arrReminderSummaryList(UBound(arrReminderSummaryList) - 1)
				// Else
				// ReDim Preserve arrReminderSummaryList(UBound(arrReminderSummaryList) - 2)
				// End If
			}
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			srptReminderFormDetail.Report = new srptReminderForm();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
			if (!boolNoSummary)
			{
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                modReminderNoticeSummary.Statics.intRPTReminderSummaryType = 2;
				frmReportViewer.InstancePtr.Init(rptReminderSummary.InstancePtr);
				//rptReminderSummary.InstancePtr.Show(App.MainForm);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					srptReminderFormDetail.Report = new srptReminderForm();
					srptReminderFormDetail.Report.UserData = rsData.Get_Fields_Int32("ActualAccountNumber");
					PrintPostCards();
					if (!boolSecondCopy)
					{
						rsData.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PrintPostCards()
		{
			var rsMaster = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill in the fields for post card
                int intCTRLIndex;
                string str1 = "";
                string str2 = "";
                string str3 = "";
                string str4 = "";
                string str5 = "";
                double dblDue = 0;
                double dblPaid = 0;
                double dblPeriodDue;
                bool boolDoNotOverwrite = false;

                rsMaster.OpenRecordset(modUTStatusPayments.UTMasterQuery(rsData.Get_Fields_Int32("AccountKey")),
                    modExtraModules.strUTDatabase);
                // Location
                // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("StreetNumber"))) != "")
                {
                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("StreetNumber"))) + " " +
                           Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("StreetName")));
                }
                else
                {
                    str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("StreetName")));
                }

                modRhymalReporting.Statics.frfFreeReport[9].DatabaseFieldName = str1;
                str1 = "";
                // MapLot   'kgk 12-20-2011 trout-773  Add MapLot
                str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("MapLot")));
                modRhymalReporting.Statics.frfFreeReport[10].DatabaseFieldName = str1;
                str1 = "";
                boolDoNotOverwrite = boolSecondCopy;
                if (boolSendTenant && !boolSecondCopy)
                {
                    boolSecondCopy = false;
                    if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name2"))) != "")
                    {
                        str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name"))) + " and " +
                               Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name2")));
                    }
                    else
                    {
                        str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name")));
                    }

                    // this will fill the address form the master screen for the mail address
                    str2 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress1")));
                    str3 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress2")));
                    str4 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress3")));
                    str5 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BCity"))) + " " +
                           Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BState"))) + " " +
                           Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BZip")));
                    if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BZip4"))) != "")
                    {
                        str5 += "-" + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BZip4")));
                    }

                    if (boolSendOwner)
                    {
                        // TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
                        if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name"))) !=
                            Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName"))))
                        {
                            boolSecondCopy = true;
                        }
                    }
                }
                else if (boolSendOwner || boolSecondCopy)
                {
                    boolSecondCopy = false;
                    // TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
                    if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("SecondOwnerName"))) != "")
                    {
                        // TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
                        str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName"))) + " and " +
                               Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("SecondOwnerName")));
                    }
                    else
                    {
                        // TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
                        str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName")));
                    }

                    // this will fill the address form the master screen for the mail address
                    str2 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress1")));
                    str3 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress2")));
                    str4 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress3")));
                    str5 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OCity"))) + " " +
                           Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OState"))) + " " +
                           Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip")));
                    if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip4"))) != "")
                    {
                        str5 += "-" + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip4")));
                    }
                }

                modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = str1;
                // condense the labels if some are blank
                if (Strings.Trim(str4) == string.Empty)
                {
                    str4 = str5;
                    str5 = "";
                }

                if (Strings.Trim(str3) == string.Empty)
                {
                    str3 = str4;
                    str4 = str5;
                    str5 = "";
                }

                if (Strings.Trim(str2) == string.Empty)
                {
                    str2 = str3;
                    str3 = str4;
                    str4 = str5;
                    str5 = "";
                }

                if (Strings.Trim(str1) == string.Empty)
                {
                    str1 = str2;
                    str2 = str3;
                    str3 = str4;
                    str4 = str5;
                    str5 = "";
                }

                // Account Number
                // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                lblAccount.Text = "Account: " + rsMaster.Get_Fields("AccountNumber");
                // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName =
                    FCConvert.ToString(rsMaster.Get_Fields("AccountNumber"));
                // Name
                lblName.Text = str1;
                // Mailing Address
                lblMailingAddress1.Text = str2;
                lblMailingAddress2.Text = str3;
                lblMailingAddress3.Text = str4;
                lblMailingAddress4.Text = str5;
                Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
                // fill the struct
                // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account =
                    FCConvert.ToInt32(rsMaster.Get_Fields("AccountNumber"));
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = str1;
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str2;
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr2 = str3;
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr3 = str4;
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr4 = str5;
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Year = 0;
                // Val(strYear)
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblDue - dblPaid;
                modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].DoNotOverwrite = boolDoNotOverwrite;
                lngIndex += 1;
                // Message
                // kgk 12-16-2011  trout-406 and trout-774  Make the report title configurable
                // If CBool(frmReminderNotices.chkCombineService.Value = vbChecked) Then
                // lblHeader.Caption = "************  Past Due Water and Sewer Reminder Statement  ************"
                // Else
                // If boolWater Then
                // lblHeader.Caption = "************  Past Due Water Reminder Statement  ************"
                // Else
                // lblHeader.Caption = "************  Past Due Sewer Reminder Statement  ************"
                // End If
                // End If
                lblHeader.Text = "************  " + strReportTitle + "  ************";
                if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
                {
                    lblPreMessage.Text = "The " + modGlobalConstants.Statics.gstrCityTown + " of " +
                                         modGlobalConstants.Statics.MuniName +
                                         "'s records indicate that you have outstanding " + strMod + " bills due:";
                }
                else
                {
                    lblPreMessage.Text = "The " + modGlobalConstants.Statics.MuniName +
                                         "'s records indicate that you have outstanding " + strMod + " bills due:";
                }

                lblMessage.Text = GetReportString(ref strDataSting);
                // lblMessage.Caption = "Be aware that these amounts are as of " & Format(dtDate, "MM/dd/yyyy") & ".  Please call the " & strIntDept & " at " & strIntDeptPhone & " for current amount due, before sending a payment." & vbCrLf & vbCrLf
                // lblMessage.Caption = lblMessage.Caption & "If you owe taxes for the year " & strLastMaturedYear & " or earlier, you will also be required to pay an additional fee not shown on this statement.  Please contact the " & strMatDept & " at " & strMatDeptPhone & " for additional information." & vbCrLf & vbCrLf
                // lblMessage.Caption = lblMessage.Caption & "If you are making payments under a valid signed workout agreement with the " & gstrCityTown & ", please continue to make your regular payments." & vbCrLf & vbCrLf
                // lblMessage.Caption = lblMessage.Caption & "Thank you."
                // kgk 12-16-2011 trout-772  Add return address header
                if (boolReturnAddress)
                {
                    // Return Address
                    lblRtnAddr1.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(1, 0);
                    lblRtnAddr2.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(2, 0);
                    lblRtnAddr3.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(3, 0);
                    lblRtnAddr4.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(4, 0);
                    lblRtnAddr5.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(5, 0);
                    lblNoticeDate.Text = Strings.Format(dtDate, "MMMM d, yyyy");
                    lblAccount.Top = 1620 / 1440F;
                    lblAccount.Top = 1620 / 1440f;
                }

                // move to the next record
                rsMaster.MoveNext();
                return;
            }
            catch (Exception ex)
            {

                frmWait.InstancePtr.Unload();
                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Printing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				rsMaster.Dispose();
            }
		}

		private string GetReportString(ref string strDataString)
		{
			string GetReportString = "";
			// this routine will fill in all of the variables and create the string to be shown on the form
			CalculateVariableTotals();
			GetReportString = SetupVariablesInString(ref strDataString);
			return GetReportString;
		}

		private void CalculateVariableTotals()
		{
			// this routine will gather all of the correct data for this account and fill in the struct
			// that holds all of the data, this will allow another function to create the string
			// ACCOUNT    - 0
			// NAME       - 1
			// MAILDATE   - 2
			modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = Strings.Format(dtDate, "MM/dd/yyyy");
			// INTDEPT    - 3
			modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = strIntDept;
			// INTDEPTPHONE-4
			modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = strIntDeptPhone;
			// MATDEPT    - 5
			modRhymalReporting.Statics.frfFreeReport[5].DatabaseFieldName = strMatDept;
			// MATDEPTPHONE-6
			modRhymalReporting.Statics.frfFreeReport[6].DatabaseFieldName = strMatDeptPhone;
			// HIGHYEAR   - 7
			modRhymalReporting.Statics.frfFreeReport[7].DatabaseFieldName = strHighYear;
			// LASTYEARMAT- 8
			modRhymalReporting.Statics.frfFreeReport[8].DatabaseFieldName = strLastMaturedYear;
			// LOCATION   - 9
		}

		private string SetupVariablesInString(ref string strOriginal)
		{
			string SetupVariablesInString = "";
			// this will replace all of the variables with the information needed
			string strBuildString;
			// this is the string that will be built and returned at the end
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			lngEndOfLastVariable = 0;
			lngNextVariable = 1;
			strBuildString = "";
			// priming read
			if (Strings.InStr(1, strOriginal, "<", CompareConstants.vbBinaryCompare) > 0)
			{
				lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<", CompareConstants.vbBinaryCompare);
				// do until there are no more variables left
				do
				{
					// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
					strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
					// set the end pointer
					lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">", CompareConstants.vbBinaryCompare);
					// replace the variable
					strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
					// check for another variable
					lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<", CompareConstants.vbBinaryCompare);
				}
				while (!(lngNextVariable == 0));
			}
			// take the last of the string and add it to the end
			strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
			// check for variables
			if (Strings.InStr(1, "<", strOriginal, CompareConstants.vbBinaryCompare) > 0)
			{
				// strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
				MessageBox.Show("ERROR: There are still variables in the report string.", "SetupVariablesInString Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else
			{
				SetupVariablesInString = strBuildString;
			}
			return SetupVariablesInString;
		}

		private string GetVariableValue_2(string strVarName)
		{
			return GetVariableValue(ref strVarName);
		}

		private string GetVariableValue(ref string strVarName)
		{
			string GetVariableValue = "";
			// this function will take a variable name and find it in the array, then get the value and return it as a string
			int intCT;
			string strValue = "";
			// this is a dummy code to show the user where the hard codes are
			if (strVarName == "CRLF")
			{
				GetVariableValue = "";
				return GetVariableValue;
			}
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
				{
					switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
					{
						case 0:
							{
								// value from the static variables in the grid
								switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
								{
								// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
									case 0:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 1:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
											break;
										}
									case 2:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM d, yyyy");
											break;
										}
									case 3:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 4:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
								}
								//end switch
								break;
							}
						case 1:
							{
								// value from the dynamic variables in the database
								strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								break;
							}
						case 2:
							{
								// questions at the bottom of the grid
								strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
								break;
							}
						case 3:
							{
								// calculated values that are stored in the DatabaseFieldName field
								strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								break;
							}
					}
					//end switch
				}
			}
			GetVariableValue = strValue;
			return GetVariableValue;
		}

		private void rptReminderForm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderForm properties;
			//rptReminderForm.Caption	= "Reminder Notice - Form";
			//rptReminderForm.Icon	= "rptReminderForm.dsx":0000";
			//rptReminderForm.Left	= 0;
			//rptReminderForm.Top	= 0;
			//rptReminderForm.Width	= 11880;
			//rptReminderForm.Height	= 8595;
			//rptReminderForm.StartUpPosition	= 3;
			//rptReminderForm.SectionData	= "rptReminderForm.dsx":058A;
			//End Unmaped Properties
		}
	}
}
