﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptEmailSummaryReport.
	/// </summary>
	partial class rptEmailSummaryReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEmailSummaryReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWater = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblFooterTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOverallTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOverallTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcct,
				this.fldName,
				this.fldWater,
				this.fldSewer,
				this.fldTotal
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.75F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.75F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 3.25F;
			// 
			// fldWater
			// 
			this.fldWater.Height = 0.1875F;
			this.fldWater.Left = 4F;
			this.fldWater.Name = "fldWater";
			this.fldWater.OutputFormat = resources.GetString("fldWater.OutputFormat");
			this.fldWater.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWater.Text = " ";
			this.fldWater.Top = 0F;
			this.fldWater.Width = 1.1875F;
			// 
			// fldSewer
			// 
			this.fldSewer.Height = 0.1875F;
			this.fldSewer.Left = 5.1875F;
			this.fldSewer.Name = "fldSewer";
			this.fldSewer.OutputFormat = resources.GetString("fldSewer.OutputFormat");
			this.fldSewer.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSewer.Text = " ";
			this.fldSewer.Top = 0F;
			this.fldSewer.Width = 1.125F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.3125F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.OutputFormat = resources.GetString("fldTotal.OutputFormat");
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotal.Text = " ";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.1875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Line1,
				this.lblAccountNumber,
				this.lblName,
				this.Label9,
				this.Label10,
				this.lblHeaderTotal
			});
			this.PageHeader.Height = 0.6979167F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "E-Mail Bills Summary";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.6875F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.6875F;
			this.Line1.Y2 = 0.6875F;
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.Height = 0.1875F;
			this.lblAccountNumber.HyperLink = null;
			this.lblAccountNumber.Left = 0F;
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblAccountNumber.Text = "Account Number";
			this.lblAccountNumber.Top = 0.5F;
			this.lblAccountNumber.Width = 0.75F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.75F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.5F;
			this.lblName.Width = 1F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.1875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label9.Text = "Water";
			this.Label9.Top = 0.5F;
			this.Label9.Width = 1F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.3125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Sewer";
			this.Label10.Top = 0.5F;
			this.Label10.Width = 1F;
			// 
			// lblHeaderTotal
			// 
			this.lblHeaderTotal.Height = 0.1875F;
			this.lblHeaderTotal.HyperLink = null;
			this.lblHeaderTotal.Left = 6.5F;
			this.lblHeaderTotal.Name = "lblHeaderTotal";
			this.lblHeaderTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.lblHeaderTotal.Text = "Total";
			this.lblHeaderTotal.Top = 0.5F;
			this.lblHeaderTotal.Width = 1F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooterTotals,
				this.fldWTotal,
				this.fldOverallTotal,
				this.Line2,
				this.fldSTotal
			});
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// lblFooterTotals
			// 
			this.lblFooterTotals.Height = 0.1875F;
			this.lblFooterTotals.HyperLink = null;
			this.lblFooterTotals.Left = 2.875F;
			this.lblFooterTotals.MultiLine = false;
			this.lblFooterTotals.Name = "lblFooterTotals";
			this.lblFooterTotals.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblFooterTotals.Text = "Totals:";
			this.lblFooterTotals.Top = 0.0625F;
			this.lblFooterTotals.Width = 1.125F;
			// 
			// fldWTotal
			// 
			this.fldWTotal.Height = 0.1875F;
			this.fldWTotal.Left = 4F;
			this.fldWTotal.Name = "fldWTotal";
			this.fldWTotal.OutputFormat = resources.GetString("fldWTotal.OutputFormat");
			this.fldWTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldWTotal.Text = " ";
			this.fldWTotal.Top = 0.0625F;
			this.fldWTotal.Width = 1.1875F;
			// 
			// fldOverallTotal
			// 
			this.fldOverallTotal.Height = 0.1875F;
			this.fldOverallTotal.Left = 6.3125F;
			this.fldOverallTotal.Name = "fldOverallTotal";
			this.fldOverallTotal.OutputFormat = resources.GetString("fldOverallTotal.OutputFormat");
			this.fldOverallTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldOverallTotal.Text = " ";
			this.fldOverallTotal.Top = 0.0625F;
			this.fldOverallTotal.Width = 1.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.15625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 4.3125F;
			this.Line2.X1 = 3.15625F;
			this.Line2.X2 = 7.46875F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// fldSTotal
			// 
			this.fldSTotal.Height = 0.1875F;
			this.fldSTotal.Left = 5.1875F;
			this.fldSTotal.Name = "fldSTotal";
			this.fldSTotal.OutputFormat = resources.GetString("fldSTotal.OutputFormat");
			this.fldSTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldSTotal.Text = " ";
			this.fldSTotal.Top = 0.0625F;
			this.fldSTotal.Width = 1.125F;
			// 
			// rptEmailSummaryReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptEmailSummaryReport_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOverallTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWater;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOverallTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTotal;
	}
}
