﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmSequenceBook.
	/// </summary>
	partial class frmSequenceBook : BaseForm
	{
		public fecherFoundation.FCFrame fraAutoSequence;
		public fecherFoundation.FCButton cmdSetSequence;
		public fecherFoundation.FCComboBox cboInterval;
		public fecherFoundation.FCTextBox txtStartSeq;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public FCGrid vsResequence;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSequenceBook));
			this.components = new System.ComponentModel.Container();
			this.fraAutoSequence = new fecherFoundation.FCFrame();
			this.cmdSetSequence = new fecherFoundation.FCButton();
			this.cboInterval = new fecherFoundation.FCComboBox();
			this.txtStartSeq = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.vsResequence = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cdmProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAutoSequence)).BeginInit();
			this.fraAutoSequence.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSetSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsResequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cdmProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cdmProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 589);
			this.BottomPanel.Size = new System.Drawing.Size(908, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraAutoSequence);
			this.ClientArea.Controls.Add(this.vsResequence);
			this.ClientArea.Size = new System.Drawing.Size(908, 529);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(908, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(213, 30);
			this.HeaderText.Text = "Resequence Book";
			// 
			// fraAutoSequence
			// 
			this.fraAutoSequence.Controls.Add(this.cmdSetSequence);
			this.fraAutoSequence.Controls.Add(this.cboInterval);
			this.fraAutoSequence.Controls.Add(this.txtStartSeq);
			this.fraAutoSequence.Controls.Add(this.Label2);
			this.fraAutoSequence.Controls.Add(this.Label1);
			this.fraAutoSequence.Location = new System.Drawing.Point(30, 434);
			this.fraAutoSequence.Name = "fraAutoSequence";
			this.fraAutoSequence.Size = new System.Drawing.Size(852, 92);
			this.fraAutoSequence.TabIndex = 1;
			this.fraAutoSequence.Text = "Auto Sequence";
			// 
			// cmdSetSequence
			// 
			this.cmdSetSequence.AppearanceKey = "actionButton";
			this.cmdSetSequence.ForeColor = System.Drawing.Color.White;
			this.cmdSetSequence.Location = new System.Drawing.Point(696, 30);
			this.cmdSetSequence.Name = "cmdSetSequence";
			this.cmdSetSequence.Size = new System.Drawing.Size(135, 40);
			this.cmdSetSequence.TabIndex = 4;
			this.cmdSetSequence.Text = "Set Sequence";
			this.cmdSetSequence.Click += new System.EventHandler(this.cmdSetSequence_Click);
			// 
			// cboInterval
			// 
			this.cboInterval.AutoSize = false;
			this.cboInterval.BackColor = System.Drawing.SystemColors.Window;
			this.cboInterval.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboInterval.FormattingEnabled = true;
			this.cboInterval.Location = new System.Drawing.Point(504, 30);
			this.cboInterval.Name = "cboInterval";
			this.cboInterval.Size = new System.Drawing.Size(114, 40);
			this.cboInterval.TabIndex = 3;
			// 
			// txtStartSeq
			// 
			//FC:FINAL:CHN - issue #1075: allow numbers only
			this.txtStartSeq.MaxLength = 5;
			this.txtStartSeq.AutoSize = false;
			this.txtStartSeq.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartSeq.LinkItem = null;
			this.txtStartSeq.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStartSeq.LinkTopic = null;
			this.txtStartSeq.Location = new System.Drawing.Point(209, 30);
			this.txtStartSeq.Name = "txtStartSeq";
			this.txtStartSeq.Size = new System.Drawing.Size(70, 40);
			this.txtStartSeq.TabIndex = 1;
			this.txtStartSeq.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStartSeq_KeyPress);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(380, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(58, 18);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "INTERVAL";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(131, 18);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "STARTING SEQUENCE";
			// 
			// vsResequence
			// 
			this.vsResequence.AllowSelection = false;
			this.vsResequence.AllowUserToResizeColumns = false;
			this.vsResequence.AllowUserToResizeRows = false;
			this.vsResequence.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsResequence.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsResequence.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsResequence.BackColorBkg = System.Drawing.Color.Empty;
			this.vsResequence.BackColorFixed = System.Drawing.Color.Empty;
			this.vsResequence.BackColorSel = System.Drawing.Color.Empty;
			this.vsResequence.Cols = 8;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsResequence.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsResequence.ColumnHeadersHeight = 30;
			this.vsResequence.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsResequence.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsResequence.DragIcon = null;
			this.vsResequence.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsResequence.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsResequence.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsResequence.FixedCols = 0;
			this.vsResequence.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsResequence.FrozenCols = 0;
			this.vsResequence.GridColor = System.Drawing.Color.Empty;
			this.vsResequence.GridColorFixed = System.Drawing.Color.Empty;
			this.vsResequence.Location = new System.Drawing.Point(30, 30);
			this.vsResequence.Name = "vsResequence";
			this.vsResequence.RowHeadersVisible = false;
			this.vsResequence.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsResequence.RowHeightMin = 0;
			this.vsResequence.Rows = 1;
			this.vsResequence.ScrollTipText = null;
			this.vsResequence.ShowColumnVisibilityMenu = false;
			this.vsResequence.Size = new System.Drawing.Size(852, 388);
			this.vsResequence.StandardTab = true;
			this.vsResequence.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsResequence.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsResequence.TabIndex = 0;
			this.vsResequence.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsResequence_KeyPressEdit);
			this.vsResequence.CurrentCellChanged += new System.EventHandler(this.vsResequence_RowColChange);
			this.vsResequence.Sorted += new System.EventHandler(this.vsResequence_AfterSort);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cdmProcessSave
			// 
			this.cdmProcessSave.AppearanceKey = "acceptButton";
			this.cdmProcessSave.Location = new System.Drawing.Point(375, 29);
			this.cdmProcessSave.Name = "cdmProcessSave";
			this.cdmProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cdmProcessSave.Size = new System.Drawing.Size(137, 48);
			this.cdmProcessSave.TabIndex = 0;
			this.cdmProcessSave.Text = "Save & Exit";
			this.cdmProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmSequenceBook
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(908, 697);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSequenceBook";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Resequence Book";
			this.Load += new System.EventHandler(this.frmSequenceBook_Load);
			this.Activated += new System.EventHandler(this.frmSequenceBook_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSequenceBook_KeyPress);
			this.Resize += new System.EventHandler(this.frmSequenceBook_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAutoSequence)).EndInit();
			this.fraAutoSequence.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSetSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsResequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cdmProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cdmProcessSave;
	}
}
