﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptMeterReadingList.
	/// </summary>
	partial class rptMeterReadingList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMeterReadingList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblReportHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCriteria = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBookNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBookNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrentReading = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPreviousReading = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSequence = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMeterNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrentReading = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPreviousReading = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSerialNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCriteria)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrentReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPreviousReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPreviousReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldCurrentReading,
				this.fldPreviousReading,
				this.fldSequence,
				this.fldSerialNum,
				this.fldLocation,
				this.fldName
			});
			this.Detail.Height = 0.3125F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooter
			});
			this.ReportFooter.Height = 0.5625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblReportHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.lblCriteria
			});
			this.PageHeader.Height = 0.6770833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBookNum,
				this.lblBookNum,
				this.Line2,
				this.lblAccount,
				this.lblCurrentReading,
				this.lblPreviousReading,
				this.lblSequence,
				this.lblMeterNum,
				this.lblAddress,
				this.lblName
			});
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.8541667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0.01041667F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblReportHeader
			// 
			this.lblReportHeader.Height = 0.25F;
			this.lblReportHeader.HyperLink = null;
			this.lblReportHeader.Left = 0F;
			this.lblReportHeader.Name = "lblReportHeader";
			this.lblReportHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblReportHeader.Text = "Meter Reading List";
			this.lblReportHeader.Top = 0F;
			this.lblReportHeader.Width = 7F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1F;
			// 
			// lblCriteria
			// 
			this.lblCriteria.Height = 0.375F;
			this.lblCriteria.HyperLink = null;
			this.lblCriteria.Left = 0F;
			this.lblCriteria.Name = "lblCriteria";
			this.lblCriteria.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblCriteria.Text = null;
			this.lblCriteria.Top = 0.25F;
			this.lblCriteria.Width = 7F;
			// 
			// fldBookNum
			// 
			this.fldBookNum.Height = 0.1875F;
			this.fldBookNum.Left = 0.9375F;
			this.fldBookNum.Name = "fldBookNum";
			this.fldBookNum.Style = "font-family: \'Tahoma\'; font-size: 11.5pt; font-weight: bold";
			this.fldBookNum.Text = null;
			this.fldBookNum.Top = 0.0625F;
			this.fldBookNum.Width = 1F;
			// 
			// lblBookNum
			// 
			this.lblBookNum.Height = 0.1875F;
			this.lblBookNum.HyperLink = null;
			this.lblBookNum.Left = 0.0625F;
			this.lblBookNum.Name = "lblBookNum";
			this.lblBookNum.Style = "font-family: \'Tahoma\'; font-size: 11.5pt; font-weight: bold";
			this.lblBookNum.Text = "Book:";
			this.lblBookNum.Top = 0.0625F;
			this.lblBookNum.Width = 0.8125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.8125F;
			this.Line2.Width = 7F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7F;
			this.Line2.Y1 = 0.8125F;
			this.Line2.Y2 = 0.8125F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.0625F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.625F;
			this.lblAccount.Width = 0.625F;
			// 
			// lblCurrentReading
			// 
			this.lblCurrentReading.Height = 0.3125F;
			this.lblCurrentReading.HyperLink = null;
			this.lblCurrentReading.Left = 0.75F;
			this.lblCurrentReading.Name = "lblCurrentReading";
			this.lblCurrentReading.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.lblCurrentReading.Text = "Current Reading";
			this.lblCurrentReading.Top = 0.5F;
			this.lblCurrentReading.Width = 0.625F;
			// 
			// lblPreviousReading
			// 
			this.lblPreviousReading.Height = 0.3125F;
			this.lblPreviousReading.HyperLink = null;
			this.lblPreviousReading.Left = 1.4375F;
			this.lblPreviousReading.Name = "lblPreviousReading";
			this.lblPreviousReading.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.lblPreviousReading.Text = "Previous Reading";
			this.lblPreviousReading.Top = 0.5F;
			this.lblPreviousReading.Width = 0.75F;
			// 
			// lblSequence
			// 
			this.lblSequence.Height = 0.1875F;
			this.lblSequence.HyperLink = null;
			this.lblSequence.Left = 2.25F;
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.lblSequence.Text = "Seq";
			this.lblSequence.Top = 0.625F;
			this.lblSequence.Width = 0.5F;
			// 
			// lblMeterNum
			// 
			this.lblMeterNum.Height = 0.1875F;
			this.lblMeterNum.HyperLink = null;
			this.lblMeterNum.Left = 2.8125F;
			this.lblMeterNum.Name = "lblMeterNum";
			this.lblMeterNum.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.lblMeterNum.Text = "Serial Num";
			this.lblMeterNum.Top = 0.625F;
			this.lblMeterNum.Width = 0.8125F;
			// 
			// lblAddress
			// 
			this.lblAddress.Height = 0.1875F;
			this.lblAddress.HyperLink = null;
			this.lblAddress.Left = 3.6875F;
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.lblAddress.Text = "Location";
			this.lblAddress.Top = 0.625F;
			this.lblAddress.Width = 1.5625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 5.3125F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.625F;
			this.lblName.Width = 1.625F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0.0625F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0.0625F;
			this.fldAccount.Width = 0.625F;
			// 
			// fldCurrentReading
			// 
			this.fldCurrentReading.Height = 0.1875F;
			this.fldCurrentReading.Left = 0.75F;
			this.fldCurrentReading.Name = "fldCurrentReading";
			this.fldCurrentReading.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.fldCurrentReading.Text = null;
			this.fldCurrentReading.Top = 0.0625F;
			this.fldCurrentReading.Width = 0.625F;
			// 
			// fldPreviousReading
			// 
			this.fldPreviousReading.Height = 0.1875F;
			this.fldPreviousReading.Left = 1.4375F;
			this.fldPreviousReading.Name = "fldPreviousReading";
			this.fldPreviousReading.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center";
			this.fldPreviousReading.Text = null;
			this.fldPreviousReading.Top = 0.0625F;
			this.fldPreviousReading.Width = 0.75F;
			// 
			// fldSequence
			// 
			this.fldSequence.Height = 0.1875F;
			this.fldSequence.Left = 2.25F;
			this.fldSequence.Name = "fldSequence";
			this.fldSequence.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center";
			this.fldSequence.Text = null;
			this.fldSequence.Top = 0.0625F;
			this.fldSequence.Width = 0.5F;
			// 
			// fldSerialNum
			// 
			this.fldSerialNum.Height = 0.1875F;
			this.fldSerialNum.Left = 2.8125F;
			this.fldSerialNum.Name = "fldSerialNum";
			this.fldSerialNum.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.fldSerialNum.Text = null;
			this.fldSerialNum.Top = 0.0625F;
			this.fldSerialNum.Width = 0.8125F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 3.6875F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0.0625F;
			this.fldLocation.Width = 1.5625F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 5.3125F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.fldName.Text = null;
			this.fldName.Top = 0.0625F;
			this.fldName.Width = 1.625F;
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.1875F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 0.0625F;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblFooter.Text = "**Indicates that a meter change out has occurred since the last reading";
			this.lblFooter.Top = 0.3125F;
			this.lblFooter.Width = 4.9375F;
			// 
			// rptMeterReadingList
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.010417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCriteria)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrentReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPreviousReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPreviousReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentReading;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPreviousReading;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSerialNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCriteria;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookNum;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBookNum;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrentReading;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPreviousReading;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSequence;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMeterNum;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
