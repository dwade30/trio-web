﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for ar30DayNotice.
	/// </summary>
	partial class ar30DayNotice
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ar30DayNotice));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.rtbText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDemand = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCertMailFee = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDemand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCertMailFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCollector = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReportHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.imgSig = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCertMailFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCertMailFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCollector)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.rtbText,
				this.lblPrincipal,
				this.lblInterest,
				this.lblDemand,
				this.lblCertMailFee,
				this.lblTotal,
				this.fldPrincipal,
				this.fldInterest,
				this.fldDemand,
				this.fldCertMailFee,
				this.fldTotal,
				this.Line2,
				this.fldCollector,
				this.fldTitle,
				this.fldMuni,
				this.lblReportHeader,
				this.lblPageNumber,
				this.lblDate,
				this.lblTime,
				this.lblMuniName,
				this.Line1,
				this.imgSig,
				this.lblTax,
				this.fldTax
			});
			this.Detail.Height = 4F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// rtbText
			// 
			//this.rtbText.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.rtbText.Height = 1.0625F;
			this.rtbText.Left = 0F;
            this.rtbText.MultiLine = true;
			this.rtbText.Name = "rtbText";
            //this.rtbText.RTF = resources.GetString("rtbText.RTF");
            this.rtbText.WrapMode = GrapeCity.ActiveReports.Document.Section.WrapMode.WordWrap;
			this.rtbText.Top = 1.03125F;
			this.rtbText.Width = 7.4375F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 0.5625F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Courier New\'";
			this.lblPrincipal.Text = "Principal Due";
			this.lblPrincipal.Top = 2.8125F;
			this.lblPrincipal.Width = 1.25F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 0.5625F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Courier New\'";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 3.1875F;
			this.lblInterest.Width = 1.25F;
			// 
			// lblDemand
			// 
			this.lblDemand.Height = 0.1875F;
			this.lblDemand.HyperLink = null;
			this.lblDemand.Left = 0.5625F;
			this.lblDemand.Name = "lblDemand";
			this.lblDemand.Style = "font-family: \'Courier New\'";
			this.lblDemand.Text = "Demand Fee";
			this.lblDemand.Top = 3.375F;
			this.lblDemand.Width = 1.25F;
			// 
			// lblCertMailFee
			// 
			this.lblCertMailFee.Height = 0.1875F;
			this.lblCertMailFee.HyperLink = null;
			this.lblCertMailFee.Left = 0.5625F;
			this.lblCertMailFee.Name = "lblCertMailFee";
			this.lblCertMailFee.Style = "font-family: \'Courier New\'";
			this.lblCertMailFee.Text = "Cert Mail Fee";
			this.lblCertMailFee.Top = 3.5625F;
			this.lblCertMailFee.Width = 1.25F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 0.5625F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Courier New\'";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 3.75F;
			this.lblTotal.Width = 1.25F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 1.8125F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldPrincipal.Text = "0.00";
			this.fldPrincipal.Top = 2.8125F;
			this.fldPrincipal.Width = 1.1875F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 1.8125F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldInterest.Text = "0.00";
			this.fldInterest.Top = 3.1875F;
			this.fldInterest.Width = 1.1875F;
			// 
			// fldDemand
			// 
			this.fldDemand.Height = 0.1875F;
			this.fldDemand.Left = 1.8125F;
			this.fldDemand.Name = "fldDemand";
			this.fldDemand.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldDemand.Text = "0.00";
			this.fldDemand.Top = 3.375F;
			this.fldDemand.Width = 1.1875F;
			// 
			// fldCertMailFee
			// 
			this.fldCertMailFee.Height = 0.1875F;
			this.fldCertMailFee.Left = 1.8125F;
			this.fldCertMailFee.Name = "fldCertMailFee";
			this.fldCertMailFee.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldCertMailFee.Text = "0.00";
			this.fldCertMailFee.Top = 3.5625F;
			this.fldCertMailFee.Width = 1.1875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 1.8125F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 3.75F;
			this.fldTotal.Width = 1.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 2.96875F;
			this.Line2.Width = 2.4375F;
			this.Line2.X1 = 4F;
			this.Line2.X2 = 6.4375F;
			this.Line2.Y1 = 2.96875F;
			this.Line2.Y2 = 2.96875F;
			// 
			// fldCollector
			// 
			this.fldCollector.Height = 0.1875F;
			this.fldCollector.Left = 4F;
			this.fldCollector.Name = "fldCollector";
			this.fldCollector.Style = "font-family: \'Courier New\'";
			this.fldCollector.Text = null;
			this.fldCollector.Top = 3.03125F;
			this.fldCollector.Width = 2.4375F;
			// 
			// fldTitle
			// 
			this.fldTitle.Height = 0.1875F;
			this.fldTitle.Left = 4F;
			this.fldTitle.Name = "fldTitle";
			this.fldTitle.Style = "font-family: \'Courier New\'";
			this.fldTitle.Text = "Collector of Taxes";
			this.fldTitle.Top = 3.21875F;
			this.fldTitle.Width = 2.4375F;
			// 
			// fldMuni
			// 
			this.fldMuni.Height = 0.1875F;
			this.fldMuni.Left = 4F;
			this.fldMuni.Name = "fldMuni";
			this.fldMuni.Style = "font-family: \'Courier New\'";
			this.fldMuni.Text = null;
			this.fldMuni.Top = 3.40625F;
			this.fldMuni.Width = 2.4375F;
			// 
			// lblReportHeader
			// 
			this.lblReportHeader.Height = 0.96875F;
			this.lblReportHeader.HyperLink = null;
			this.lblReportHeader.Left = 0F;
			this.lblReportHeader.Name = "lblReportHeader";
			this.lblReportHeader.Style = "font-family: \'Courier New\'; font-size: 12pt; text-align: center";
			this.lblReportHeader.Text = null;
			this.lblReportHeader.Top = 0F;
			this.lblReportHeader.Width = 7.4375F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 5.9375F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.5F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.9375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.5F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Courier New\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Courier New\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 1.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.5625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3.75F;
			this.Line1.Width = 2.4375F;
			this.Line1.X1 = 0.5625F;
			this.Line1.X2 = 3F;
			this.Line1.Y1 = 3.75F;
			this.Line1.Y2 = 3.75F;
			// 
			// imgSig
			// 
			this.imgSig.Height = 0.5625F;
			this.imgSig.HyperLink = null;
			this.imgSig.ImageData = null;
			this.imgSig.Left = 4F;
			this.imgSig.LineWeight = 1F;
			this.imgSig.Name = "imgSig";
			this.imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgSig.Top = 2.46875F;
			this.imgSig.Width = 2.375F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 0.5625F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Courier New\'";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 3F;
			this.lblTax.Width = 1.25F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 1.8125F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldTax.Text = "0.00";
			this.fldTax.Top = 3F;
			this.fldTax.Width = 1.1875F;
			// 
			// ar30DayNotice
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.45F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCertMailFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCertMailFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCollector)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox rtbText;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDemand;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCertMailFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDemand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCertMailFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCollector;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgSig;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
