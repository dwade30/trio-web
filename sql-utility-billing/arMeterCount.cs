﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arMeterCount.
	/// </summary>
	public partial class arMeterCount : BaseSectionReport
	{
		public arMeterCount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Meter Count Summary";
		}

		public static arMeterCount InstancePtr
		{
			get
			{
				return (arMeterCount)Sys.GetInstance(typeof(arMeterCount));
			}
		}

		protected arMeterCount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsDataS.Dispose();
				rsDataW.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arMeterCount	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/17/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsDataW = new clsDRWrapper();
		clsDRWrapper rsDataS = new clsDRWrapper();
		int lngCountW;
		int lngCountS;
		bool blnHasSewer;
		bool blnHasWater;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsService = new clsDRWrapper();
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				rsData.OpenRecordset("SELECT * FROM Category WHERE LongDescription <> '' ORDER BY Code", modExtraModules.strUTDatabase);
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Data"
				// MAL@20080114: Check for service to determine if both services exist
				rsService.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (FCConvert.ToString(rsService.Get_Fields_String("Service")) == "B")
				{
					blnHasWater = true;
					blnHasSewer = true;
					lblWaterMeter.Visible = true;
					fldW.Visible = true;
					lblSewerMeter.Visible = true;
					fldS.Visible = true;
					fldTotalW.Visible = true;
					fldTotalS.Visible = true;
					lblSewerMeter.Left = 7020 / 1440F;
					fldS.Left = 7470 / 1440F;
					fldTotalS.Left = 7470 / 1440F;
				}
				else if (rsService.Get_Fields_String("Service") == "W")
				{
					blnHasWater = true;
					blnHasSewer = false;
					lblWaterMeter.Visible = true;
					fldW.Visible = true;
					lblSewerMeter.Visible = false;
					fldS.Visible = false;
					fldTotalW.Visible = true;
					fldTotalS.Visible = false;
				}
				else
				{
					blnHasWater = false;
					blnHasSewer = true;
					lblWaterMeter.Visible = false;
					fldW.Visible = false;
					lblSewerMeter.Visible = true;
					fldS.Visible = true;
					fldTotalW.Visible = false;
					fldTotalS.Visible = true;
					lblSewerMeter.Left = 5220 / 1440F;
					fldS.Left = 5670 / 1440F;
					fldTotalS.Left = 5670 / 1440F;
				}
				FillHeaderLabels();
				rsService.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			if (!rsData.EndOfFile())
			{
				// MAL@20080114
				if (blnHasWater && blnHasSewer)
				{
					// MAL@20080212: Change to select count from the MeterTable and to check the Service
					// Tracker Reference: 12307
					// this counts all of the meter records that have the same catagory as the current metersize
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					rsDataS.OpenRecordset("SELECT COUNT(SCat) AS SC FROM MeterTable WHERE SCat = " + rsData.Get_Fields("Code") + " AND (Service = 'B' OR Service = 'S')", modExtraModules.strUTDatabase);
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					rsDataW.OpenRecordset("SELECT COUNT(WCat) AS WC FROM MeterTable WHERE WCat = " + rsData.Get_Fields("Code") + " AND (Service = 'B' OR Service = 'W')", modExtraModules.strUTDatabase);
				}
				else if (blnHasWater && !blnHasSewer)
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					rsDataW.OpenRecordset("SELECT COUNT(WCat) AS WC FROM MeterTable WHERE WCat = " + rsData.Get_Fields("Code") + " AND Service = 'W'", modExtraModules.strUTDatabase);
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					rsDataS.OpenRecordset("SELECT COUNT(SCat) AS SC FROM MeterTable WHERE SCat = " + rsData.Get_Fields("Code") + " AND Service = 'S'", modExtraModules.strUTDatabase);
				}
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCat.Text = FCConvert.ToString(rsData.Get_Fields("Code"));
				fldDesc.Text = rsData.Get_Fields_String("LongDescription");
				if (blnHasSewer)
				{
					if (!rsDataS.EndOfFile())
					{
						// TODO Get_Fields: Field [SC] not found!! (maybe it is an alias?)
						fldS.Text = FCConvert.ToString(rsDataS.Get_Fields("SC"));
						// TODO Get_Fields: Field [SC] not found!! (maybe it is an alias?)
						lngCountS += rsDataS.Get_Fields("SC");
					}
					else
					{
						fldS.Text = "0";
					}
				}
				if (blnHasWater)
				{
					if (!rsDataW.EndOfFile())
					{
						// TODO Get_Fields: Field [WC] not found!! (maybe it is an alias?)
						fldW.Text = FCConvert.ToString(rsDataW.Get_Fields("WC"));
						// TODO Get_Fields: Field [WC] not found!! (maybe it is an alias?)
						lngCountW += rsDataW.Get_Fields("WC");
					}
					else
					{
						fldW.Text = "0";
					}
				}
				rsData.MoveNext();
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// MAL@20080114
			if (blnHasWater)
			{
				fldTotalW.Text = lngCountW.ToString();
			}
			if (blnHasSewer)
			{
				fldTotalS.Text = lngCountS.ToString();
			}
			rsData.OpenRecordset("SELECT * FROM MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID WHERE (SewerCategory = 0 AND Service <> 'W') or (WaterCategory = 0 AND Service <> 'S')", modExtraModules.strUTDatabase);
			if (!rsData.EndOfFile())
			{
				sarMissingCategoryOb.Report = new arMeterCountNoCat();
			}
		}

		private void arMeterCount_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arMeterCount properties;
			//arMeterCount.Caption	= "Meter Count Summary";
			//arMeterCount.Icon	= "arMeterCount.dsx":0000";
			//arMeterCount.Left	= 0;
			//arMeterCount.Top	= 0;
			//arMeterCount.Width	= 11700;
			//arMeterCount.Height	= 7500;
			//arMeterCount.WindowState	= 2;
			//arMeterCount.SectionData	= "arMeterCount.dsx":058A;
			//End Unmaped Properties
		}
	}
}
