﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBookChoice.
	/// </summary>
	partial class frmBookChoice : BaseForm
	{
		public fecherFoundation.FCComboBox cmbInitial;
		public fecherFoundation.FCLabel lblInitial;
		public FCGrid vsBEChoice;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCFrame fraSequence;
		public fecherFoundation.FCTextBox txtEndSeq;
		public fecherFoundation.FCTextBox txtStartSeq;
		public fecherFoundation.FCLabel lblEndSeq;
		public fecherFoundation.FCLabel lblStartSeq;
		public fecherFoundation.FCLabel lblInformation;
		public fecherFoundation.FCToolStripMenuItem mnuProcessBooks;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelectAll;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBookChoice));
			this.cmbInitial = new fecherFoundation.FCComboBox();
			this.lblInitial = new fecherFoundation.FCLabel();
			this.vsBEChoice = new fecherFoundation.FCGrid();
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.fraSequence = new fecherFoundation.FCFrame();
			this.txtEndSeq = new fecherFoundation.FCTextBox();
			this.txtStartSeq = new fecherFoundation.FCTextBox();
			this.lblEndSeq = new fecherFoundation.FCLabel();
			this.lblStartSeq = new fecherFoundation.FCLabel();
			this.lblInformation = new fecherFoundation.FCLabel();
			this.mnuProcessBooks = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSelectAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBEChoice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSequence)).BeginInit();
			this.fraSequence.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsBEChoice);
			this.ClientArea.Controls.Add(this.cmbInitial);
			this.ClientArea.Controls.Add(this.lblInitial);
			this.ClientArea.Controls.Add(this.lblInformation);
			this.ClientArea.Controls.Add(this.fraSequence);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(156, 30);
			//FC:FINAL:MSH - issue #1112: remove text from header
			//this.HeaderText.Text = "Select Books";
			// 
			// cmbInitial
			// 
			this.cmbInitial.AutoSize = false;
			this.cmbInitial.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbInitial.FormattingEnabled = true;
			this.cmbInitial.Items.AddRange(new object[] {
				"Initial",
				"Reprint"
			});
			this.cmbInitial.Location = new System.Drawing.Point(162, 30);
			this.cmbInitial.Name = "cmbInitial";
			this.cmbInitial.Size = new System.Drawing.Size(120, 40);
			this.cmbInitial.TabIndex = 6;
			this.cmbInitial.Text = "Initial";
			this.cmbInitial.Visible = false;
			// 
			// lblInitial
			// 
			this.lblInitial.AutoSize = true;
			this.lblInitial.Location = new System.Drawing.Point(30, 44);
			this.lblInitial.Name = "lblInitial";
			this.lblInitial.Size = new System.Drawing.Size(80, 15);
			this.lblInitial.TabIndex = 7;
			this.lblInitial.Text = "PRINT TYPE";
			this.lblInitial.Visible = false;
			// 
			// vsBEChoice
			// 
			this.vsBEChoice.AllowSelection = false;
			this.vsBEChoice.AllowUserToResizeColumns = false;
			this.vsBEChoice.AllowUserToResizeRows = false;
			this.vsBEChoice.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsBEChoice.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBEChoice.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBEChoice.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBEChoice.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBEChoice.BackColorSel = System.Drawing.Color.Empty;
			this.vsBEChoice.Cols = 8;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBEChoice.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsBEChoice.ColumnHeadersHeight = 30;
			this.vsBEChoice.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBEChoice.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsBEChoice.DragIcon = null;
			this.vsBEChoice.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBEChoice.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsBEChoice.ExtendLastCol = true;
			this.vsBEChoice.FixedCols = 0;
			this.vsBEChoice.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBEChoice.FrozenCols = 0;
			this.vsBEChoice.GridColor = System.Drawing.Color.Empty;
			this.vsBEChoice.GridColorFixed = System.Drawing.Color.Empty;
			this.vsBEChoice.Location = new System.Drawing.Point(30, 140);
			this.vsBEChoice.Name = "vsBEChoice";
			this.vsBEChoice.ReadOnly = true;
			this.vsBEChoice.RowHeadersVisible = false;
			this.vsBEChoice.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBEChoice.RowHeightMin = 0;
			this.vsBEChoice.Rows = 2;
			this.vsBEChoice.ScrollTipText = null;
			this.vsBEChoice.ShowColumnVisibilityMenu = false;
			this.vsBEChoice.Size = new System.Drawing.Size(1008, 355);
			this.vsBEChoice.StandardTab = true;
			this.vsBEChoice.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			//FC:FINAL:MSH - issue #915: change TabBehavior for moving between cells on 'Tab' pressing
			//this.vsBEChoice.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBEChoice.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsBEChoice.TabIndex = 5;
			this.vsBEChoice.Click += new System.EventHandler(this.vsBEChoice_ClickEvent);
			this.vsBEChoice.DoubleClick += new System.EventHandler(this.vsBEChoice_DblClick);
			// 
			// CommonDialog1
			// 
			this.CommonDialog1.Color = System.Drawing.Color.Black;
			this.CommonDialog1.DefaultExt = null;
			this.CommonDialog1.FilterIndex = ((short)(0));
			this.CommonDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.CommonDialog1.FontName = "Microsoft Sans Serif";
			this.CommonDialog1.FontSize = 8.25F;
			this.CommonDialog1.ForeColor = System.Drawing.Color.Black;
			this.CommonDialog1.FromPage = 0;
			this.CommonDialog1.Location = new System.Drawing.Point(0, 0);
			this.CommonDialog1.Name = "CommonDialog1";
			this.CommonDialog1.PrinterSettings = null;
			this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
			this.CommonDialog1.TabIndex = 0;
			this.CommonDialog1.ToPage = 0;
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.ForeColor = System.Drawing.Color.White;
			this.cmdProcess.Location = new System.Drawing.Point(500, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(108, 48);
			this.cmdProcess.TabIndex = 1;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// fraSequence
			// 
			this.fraSequence.Controls.Add(this.txtEndSeq);
			this.fraSequence.Controls.Add(this.txtStartSeq);
			this.fraSequence.Controls.Add(this.lblEndSeq);
			this.fraSequence.Controls.Add(this.lblStartSeq);
			this.fraSequence.Location = new System.Drawing.Point(30, 30);
			this.fraSequence.Name = "fraSequence";
			this.fraSequence.Size = new System.Drawing.Size(428, 90);
			this.fraSequence.TabIndex = 6;
			this.fraSequence.Text = "Sequence Range";
			this.fraSequence.Visible = false;
			// 
			// txtEndSeq
			// 
			this.txtEndSeq.AutoSize = false;
			this.txtEndSeq.BackColor = System.Drawing.SystemColors.Window;
			this.txtEndSeq.LinkItem = null;
			this.txtEndSeq.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEndSeq.LinkTopic = null;
			this.txtEndSeq.Location = new System.Drawing.Point(326, 30);
			this.txtEndSeq.Name = "txtEndSeq";
			this.txtEndSeq.Size = new System.Drawing.Size(82, 40);
			this.txtEndSeq.TabIndex = 9;
			// 
			// txtStartSeq
			// 
			this.txtStartSeq.AutoSize = false;
			this.txtStartSeq.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartSeq.LinkItem = null;
			this.txtStartSeq.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStartSeq.LinkTopic = null;
			this.txtStartSeq.Location = new System.Drawing.Point(123, 30);
			this.txtStartSeq.Name = "txtStartSeq";
			this.txtStartSeq.Size = new System.Drawing.Size(82, 40);
			this.txtStartSeq.TabIndex = 7;
			// 
			// lblEndSeq
			// 
			this.lblEndSeq.AutoSize = true;
			this.lblEndSeq.Location = new System.Drawing.Point(235, 44);
			this.lblEndSeq.Name = "lblEndSeq";
			this.lblEndSeq.Size = new System.Drawing.Size(34, 15);
			this.lblEndSeq.TabIndex = 10;
			this.lblEndSeq.Text = "END";
			// 
			// lblStartSeq
			// 
			this.lblStartSeq.AutoSize = true;
			this.lblStartSeq.Location = new System.Drawing.Point(20, 44);
			this.lblStartSeq.Name = "lblStartSeq";
			this.lblStartSeq.Size = new System.Drawing.Size(48, 15);
			this.lblStartSeq.TabIndex = 8;
			this.lblStartSeq.Text = "START";
			// 
			// lblInformation
			// 
			this.lblInformation.AutoSize = true;
			this.lblInformation.Location = new System.Drawing.Point(10, 36);
			this.lblInformation.Name = "lblInformation";
			this.lblInformation.Size = new System.Drawing.Size(4, 14);
			this.lblInformation.TabIndex = 0;
			// 
			// mnuProcessBooks
			// 
			this.mnuProcessBooks.Index = -1;
			this.mnuProcessBooks.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSelectAll,
				this.mnuProcess,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcessBooks.Name = "mnuProcessBooks";
			this.mnuProcessBooks.Text = "File";
			this.mnuProcessBooks.Click += new System.EventHandler(this.mnuProcessBooks_Click);
			// 
			// mnuFileSelectAll
			// 
			this.mnuFileSelectAll.Index = 0;
			this.mnuFileSelectAll.Name = "mnuFileSelectAll";
			this.mnuFileSelectAll.Text = "Select All";
			this.mnuFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcess.Text = "Process Books";
			this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(978, 29);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(72, 24);
			this.cmdFileSelectAll.TabIndex = 1;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// frmBookChoice
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBookChoice";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select Books";
			this.Load += new System.EventHandler(this.frmBookChoice_Load);
			this.Activated += new System.EventHandler(this.frmBookChoice_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBookChoice_KeyPress);
			this.Resize += new System.EventHandler(this.frmBookChoice_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBEChoice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSequence)).EndInit();
			this.fraSequence.ResumeLayout(false);
			this.fraSequence.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSelectAll;
	}
}
